SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [sitestructure].[tContentPageSeoSettingTranslation]'
GO
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSettingTranslation] ALTER COLUMN [Description] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[tContentPageSeoSetting]'
GO
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ALTER COLUMN [Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [sitestructure].[pContentPageSeoSettingSave]'
GO


/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

ALTER procedure [sitestructure].[pContentPageSeoSettingSave]
	@ContentNodeId	int,
	@Title			nvarchar(250),
	@Description	nvarchar(250),
	@Keywords		nvarchar(250)
as
BEGIN
	update
		[sitestructure].[tContentPageSeoSetting]
	set
		[Title]			= @Title,
		[Description]	= @Description,
		[Keywords]		= @Keywords
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		INSERT [sitestructure].[tContentPageSeoSetting]
		(
			[ContentNodeId],
			[Title],
			[Description],
			[Keywords]
		)
		values
		(
			@ContentNodeId,
			@Title,
			@Description,
			@Keywords
		)
	end
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO