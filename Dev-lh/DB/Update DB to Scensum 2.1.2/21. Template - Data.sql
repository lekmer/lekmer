SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

-- Drop constraints from [template].[tTemplateFragment]
ALTER TABLE [template].[tTemplateFragment] DROP CONSTRAINT [FK_tTemplateFragment_tModelFragment]
ALTER TABLE [template].[tTemplateFragment] DROP CONSTRAINT [FK_tTemplateFragment_tTemplate]

-- Drop constraints from [template].[tTemplate]
ALTER TABLE [template].[tTemplate] DROP CONSTRAINT [FK_tTemplate_tModel]

-- Drop constraints from [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] DROP CONSTRAINT [FK_tModelFragmentRegion_tModel]

-- Drop constraints from [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tFunctionType]
ALTER TABLE [template].[tModelFragmentFunction] DROP CONSTRAINT [FK_tModelFragmentFunction_tModelFragment]

-- Drop constraints from [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tEntity]
ALTER TABLE [template].[tModelFragmentEntity] DROP CONSTRAINT [FK_tModelFragmentEntity_tModelFragment]

-- Drop constraints from [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModel]
ALTER TABLE [template].[tModelFragment] DROP CONSTRAINT [FK_tModelFragment_tModelFragmentRegion]

-- Drop constraints from [template].[tModel]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tModelFolder]
ALTER TABLE [template].[tModel] DROP CONSTRAINT [FK_tModel_tTemplate]

-- Drop constraints from [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tEntity]
ALTER TABLE [template].[tEntityFunction] DROP CONSTRAINT [FK_tEntityFunction_tFunctionType]

-- Drop constraints from [template].[tAlias]
ALTER TABLE [template].[tAlias] DROP CONSTRAINT [FK_tAlias_tAliasFolder]
ALTER TABLE [template].[tAlias] DROP CONSTRAINT [FK_tAlias_tAliasType]

-- Drop constraint FK_tAliasTranslation_tAlias from [template].[tAliasTranslation]
ALTER TABLE [template].[tAliasTranslation] DROP CONSTRAINT [FK_tAliasTranslation_tAlias]

-- Drop constraints from [template].[tAliasFolder]
ALTER TABLE [template].[tAliasFolder] DROP CONSTRAINT [FK_tAliasFolder_tAliasFolder]


-- Delete 5 rows from [template].[tModelFragmentRegion]
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=34
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=35
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=47
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=48
DELETE FROM [template].[tModelFragmentRegion] WHERE [ModelFragmentRegionId]=116

-- Delete 3 rows from [template].[tModelFragmentEntity]
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=139 AND [EntityId]=6
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=139 AND [EntityId]=7
DELETE FROM [template].[tModelFragmentEntity] WHERE [ModelFragmentId]=140 AND [EntityId]=8

-- Delete 1 row from [template].[tEntityFunction]
DELETE FROM [template].[tEntityFunction] WHERE [FunctionId]=35

-- Add 61 rows to [template].[tEntityFunction]
SET IDENTITY_INSERT [template].[tEntityFunction] ON
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (142, 39, 1, N'Store.Id', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (143, 39, 1, N'Store.Title', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (144, 39, 1, N'Store.Description', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (145, 40, 1, N'StoreProduct.StockQuantity', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (146, 40, 2, N'StoreProduct.IsInStock', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (100, 40, 2, N'StoreProduct.StockIsUnknown', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (101, 40, 2, N'StoreProduct.HasLimitedStock', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (102, 40, 2, N'StoreProduct.IsOutOfStock', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (147, 10, 1, N'CustomerInformation.FirstName', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (148, 10, 1, N'CustomerInformation.LastName', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (149, 10, 1, N'CustomerInformation.CivicNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (150, 10, 1, N'CustomerInformation.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (151, 10, 1, N'CustomerInformation.CellPhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (152, 10, 1, N'CustomerInformation.CreatedDate', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (153, 10, 1, N'CustomerInformation.Email', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (154, 10, 1, N'Customer.Id', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (155, 10, 1, N'Customer.ErpId', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (156, 10, 2, N'Customer.HasCustomerInformation', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (157, 10, 2, N'CustomerInformation.HasDeliveryAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (158, 10, 2, N'CustomerInformation.HasBillingAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (159, 10, 1, N'CustomerInformation.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (160, 10, 1, N'CustomerInformation.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (161, 10, 1, N'CustomerInformation.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (162, 10, 1, N'CustomerInformation.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (163, 10, 1, N'CustomerInformation.DeliveryAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (164, 10, 1, N'CustomerInformation.DeliveryAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (165, 10, 1, N'CustomerInformation.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (166, 10, 1, N'CustomerInformation.BillingAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (167, 10, 1, N'CustomerInformation.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (168, 10, 1, N'CustomerInformation.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (169, 10, 1, N'CustomerInformation.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (170, 10, 1, N'CustomerInformation.BillingAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (171, 10, 1, N'CustomerInformation.BillingAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (172, 10, 1, N'CustomerInformation.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (173, 10, 1, N'User.CreatedDate', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (174, 6, 1, N'Order.IP', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (175, 7, 1, N'Order.FreightCost', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (176, 7, 1, N'Order.FreightCostVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (177, 7, 1, N'Order.ItemPriceSummaryIncludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (178, 7, 1, N'Order.ItemPriceSummaryExcludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (179, 7, 1, N'Order.TotalPriceIncludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (180, 7, 1, N'Order.TotalPriceExcludingVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (181, 7, 1, N'Order.TotalVat', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (182, 7, 1, N'Order.TotalQuantity', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (183, 7, 1, N'Order.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (184, 7, 1, N'Order.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (185, 7, 1, N'Order.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (186, 7, 1, N'Order.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (187, 7, 1, N'Order.DeliveryAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (188, 7, 1, N'Order.DeliveryAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (189, 7, 1, N'Order.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (190, 7, 1, N'Order.BillingAddress.Addressee', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (191, 7, 1, N'Order.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (192, 7, 1, N'Order.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (193, 7, 1, N'Order.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (194, 7, 1, N'Order.BillingAddress.City', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (195, 7, 1, N'Order.BillingAddress.Country', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (196, 7, 1, N'Order.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (197, 8, 1, N'OrderItem.PriceSummary', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (198, 10, 2, N'CustomerInformation.DeliveryAddressIsEqualToBillingAddress', N'')
INSERT INTO [template].[tEntityFunction] ([FunctionId], [EntityId], [FunctionTypeId], [CommonName], [Description]) VALUES (199, 7, 2, N'Order.DeliveryAddressIsEqualToBillingAddress', N'')
SET IDENTITY_INSERT [template].[tEntityFunction] OFF



-- Add 33 rows to [template].[tModelFragmentEntity]
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (92, 7)
INSERT INTO [template].[tModelFragmentEntity] ([ModelFragmentId], [EntityId]) VALUES (92, 10)


-- Add 76 rows to [template].[tModelFragmentFunction]
SET IDENTITY_INSERT [template].[tModelFragmentFunction] ON
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (295, 139, 1, N'Order.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (296, 139, 1, N'Order.Number', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (297, 139, 1, N'Order.Email', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (298, 139, 1, N'Order.CreatedDate', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (299, 139, 1, N'Order.Url', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (300, 139, 1, N'Order.IP', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (301, 139, 1, N'Order.FreightCost', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (302, 139, 1, N'Order.FreightCostVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (303, 139, 1, N'Order.ItemPriceSummaryIncludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (304, 139, 1, N'Order.ItemPriceSummaryExcludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (305, 139, 1, N'Order.TotalPriceIncludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (306, 139, 1, N'Order.TotalPriceExcludingVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (307, 139, 1, N'Order.TotalVat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (308, 139, 1, N'Order.TotalQuantity', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (309, 139, 1, N'Order.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (310, 139, 1, N'Order.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (311, 139, 1, N'Order.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (312, 139, 1, N'Order.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (313, 139, 1, N'Order.DeliveryAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (314, 139, 1, N'Order.DeliveryAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (315, 139, 1, N'Order.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (316, 139, 1, N'Order.BillingAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (317, 139, 1, N'Order.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (318, 139, 1, N'Order.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (319, 139, 1, N'Order.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (320, 139, 1, N'Order.BillingAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (321, 139, 1, N'Order.BillingAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (322, 139, 1, N'Order.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (323, 139, 2, N'Order.DeliveryAddressIsEqualToBillingAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (324, 139, 2, N'Customer.HasUser', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (325, 139, 1, N'User.UserName', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (326, 139, 1, N'CustomerInformation.FirstName', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (327, 139, 1, N'CustomerInformation.LastName', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (328, 139, 1, N'CustomerInformation.CivicNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (329, 139, 1, N'CustomerInformation.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (330, 139, 1, N'CustomerInformation.CellPhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (331, 139, 1, N'CustomerInformation.CreatedDate', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (332, 139, 1, N'CustomerInformation.Email', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (333, 139, 1, N'Customer.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (334, 139, 1, N'Customer.ErpId', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (335, 139, 2, N'Customer.HasCustomerInformation', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (336, 139, 2, N'CustomerInformation.HasDeliveryAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (337, 139, 2, N'CustomerInformation.HasBillingAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (338, 139, 1, N'CustomerInformation.DeliveryAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (339, 139, 1, N'CustomerInformation.DeliveryAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (340, 139, 1, N'CustomerInformation.DeliveryAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (341, 139, 1, N'CustomerInformation.DeliveryAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (342, 139, 1, N'CustomerInformation.DeliveryAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (343, 139, 1, N'CustomerInformation.DeliveryAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (344, 139, 1, N'CustomerInformation.DeliveryAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (345, 139, 1, N'CustomerInformation.BillingAddress.Addressee', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (346, 139, 1, N'CustomerInformation.BillingAddress.StreetAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (347, 139, 1, N'CustomerInformation.BillingAddress.StreetAddress2', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (348, 139, 1, N'CustomerInformation.BillingAddress.PostalCode', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (349, 139, 1, N'CustomerInformation.BillingAddress.City', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (350, 139, 1, N'CustomerInformation.BillingAddress.Country', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (351, 139, 1, N'CustomerInformation.BillingAddress.PhoneNumber', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (352, 139, 1, N'User.CreatedDate', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (353, 139, 2, N'CustomerInformation.DeliveryAddressIsEqualToBillingAddress', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (354, 140, 1, N'OrderItem.Id', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (355, 140, 1, N'OrderItem.Title', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (356, 140, 1, N'OrderItem.Price', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (357, 140, 1, N'OrderItem.Vat', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (358, 140, 1, N'OrderItem.Quantity', N'')
INSERT INTO [template].[tModelFragmentFunction] ([FunctionId], [ModelFragmentId], [FunctionTypeId], [CommonName], [Description]) VALUES (359, 140, 1, N'OrderItem.PriceSummary', N'')
SET IDENTITY_INSERT [template].[tModelFragmentFunction] OFF







-- Add constraints to [template].[tTemplateFragment]
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [FK_tTemplateFragment_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [FK_tTemplateFragment_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])

-- Add constraints to [template].[tTemplate]
ALTER TABLE [template].[tTemplate] ADD CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentRegion]
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])

-- Add constraints to [template].[tModelFragmentFunction]
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragmentEntity]
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])

-- Add constraints to [template].[tModelFragment]
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])

-- Add constraints to [template].[tModel]
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tModelFolder] FOREIGN KEY ([ModelFolderId]) REFERENCES [template].[tModelFolder] ([ModelFolderId])
ALTER TABLE [template].[tModel] ADD CONSTRAINT [FK_tModel_tTemplate] FOREIGN KEY ([DefaultTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])

-- Add constraints to [template].[tEntityFunction]
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])

-- Add constraints to [template].[tAlias]
ALTER TABLE [template].[tAlias] ADD CONSTRAINT [FK_tAlias_tAliasFolder] FOREIGN KEY ([AliasFolderId]) REFERENCES [template].[tAliasFolder] ([AliasFolderId])
ALTER TABLE [template].[tAlias] ADD CONSTRAINT [FK_tAlias_tAliasType] FOREIGN KEY ([AliasTypeId]) REFERENCES [template].[tAliasType] ([AliasTypeId])

-- Add constraint FK_tAliasTranslation_tAlias to [template].[tAliasTranslation]
ALTER TABLE [template].[tAliasTranslation] ADD CONSTRAINT [FK_tAliasTranslation_tAlias] FOREIGN KEY ([AliasId]) REFERENCES [template].[tAlias] ([AliasId])

-- Add constraints to [template].[tAliasFolder]
ALTER TABLE [template].[tAliasFolder] ADD CONSTRAINT [FK_tAliasFolder_tAliasFolder] FOREIGN KEY ([ParentAliasFolderId]) REFERENCES [template].[tAliasFolder] ([AliasFolderId])
COMMIT TRANSACTION
GO