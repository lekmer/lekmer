EXEC sp_fulltext_database 'enable'
GO

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [customer].[tAddress]'
GO
ALTER TABLE [customer].[tAddress] DROP
CONSTRAINT [FK_tAddress_tCustomerInformation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] DROP
CONSTRAINT [FK_tCustomerInformation_tAddress_Billing],
CONSTRAINT [FK_tCustomerInformation_tAddress_Delivery],
CONSTRAINT [FK_tCustomerInformation_tCustomer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [order].[tOrder]'
GO
ALTER TABLE [order].[tOrder] DROP
CONSTRAINT [FK_tOrder_tCustomer]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] DROP CONSTRAINT [PK_tCustomerInformation]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [customer].[pCustomerSearch]'
GO

ALTER PROCEDURE [customer].[pCustomerSearch]
	@ErpId          varchar(50),
	@FirstName      nvarchar(64),
	@LastName       nvarchar(64),
	@CivicNumber    varchar(50),
	@Email          nvarchar(64),
	@PhoneHome      varchar(50),
	@PhoneMobile    varchar(50),
	@CreatedFrom    datetime,
	@CreatedTo      datetime,
	@StatusId       int,
	@Separator      char(1),
	@Page           int = NULL,
	@PageSize       int
as   
begin
    set nocount on

	DECLARE @sqlGeneral nvarchar(max)
	declare @sql nvarchar(max)
	declare @sqlCount nvarchar(max)
	declare @sqlFragment nvarchar(max)
	
	set @sqlFragment = '
		select
			row_number() over (order by c.CustomerId desc) AS _Number,
			c.CustomerId
		from
			[customer].[tCustomer] c
			inner join [customer].[tCustomerInformation] ci
				on c.CustomerId = ci.CustomerId
		where
			1 = 1'
			+ case when (@ErpId is not null) then + ' and c.ErpId = @ErpId' else '' end
			+ case when (@StatusId is not null) then + ' and c.CustomerStatusId = @StatusId' else '' end
			+ case when (@FirstName is not null) then + ' and contains(ci.FirstName, @FirstName)' else '' end
			+ case when (@LastName is not null) then + ' and contains(ci.LastName, @LastName)' else '' end
			+ case when (@CivicNumber is not null) then + ' and ci.CivicNumber = @CivicNumber' else '' end
			+ case when (@Email is not null) then + ' and ci.Email = @Email' else '' end
			+ case when (@PhoneHome is not null) then + ' and ci.PhoneNumber = @PhoneHome' else '' end
			+ case when (@PhoneMobile is not null) then + ' and ci.CellPhoneNumber = @PhoneMobile' else '' end
			+ case when (@CreatedFrom is not null) then + ' and ci.InformationCreatedDate >= @CreatedFrom' else '' end
			+ case when (@CreatedTo is not null) then + ' and ci.InformationCreatedDate <= @CreatedTo' else '' end

	SET @sqlGeneral = '
		IF (@FirstName IS NOT NULL)
		BEGIN
			SET @FirstName = generic.fPrepareFulltextSearchParameter(@FirstName)
		END
		IF (@LastName IS NOT NULL)
		BEGIN
			SET @LastName = generic.fPrepareFulltextSearchParameter(@LastName)
		END'

	set @sqlCount = @sqlGeneral + '
		select count(*) from (' + @sqlFragment + ') as CountResults'

	set @sql = @sqlGeneral + '
		select
			c.*
		from
			(' + @sqlFragment + ') as result
			inner join [customer].[vCustomCustomerSecure] c
				on c.[Customer.CustomerId] = result.CustomerId
		where
			_Number > ' + cast((@Page - 1) * @PageSize AS varchar(10)) + '
			and _Number <= ' + cast(@Page * @PageSize AS varchar(10)) + '
		order by
			result.CustomerId desc'
	

	exec sp_executesql @sqlCount,
		N'	@ErpId          varchar(50),
			@FirstName      nvarchar(64),
			@LastName       nvarchar(64),
			@CivicNumber    varchar(50),
			@Email          nvarchar(64),
			@PhoneHome      varchar(50),
			@PhoneMobile    varchar(50),
			@CreatedFrom    datetime,
			@CreatedTo      datetime,
			@StatusId       int',
			@ErpId,          
			@FirstName, 
			@LastName,     
			@CivicNumber,
			@Email,  
			@PhoneHome, 
			@PhoneMobile,  
			@CreatedFrom,  
			@CreatedTo,
			@StatusId

	exec sp_executesql @sql, 
		N'	@ErpId          varchar(50),
			@FirstName      nvarchar(64),
			@LastName       nvarchar(64),
			@CivicNumber    varchar(50),
			@Email          nvarchar(64),
			@PhoneHome      varchar(50),
			@PhoneMobile    varchar(50),
			@CreatedFrom    datetime,
			@CreatedTo      datetime,
			@StatusId       int',
			@ErpId,          
			@FirstName, 
			@LastName,     
			@CivicNumber,
			@Email,  
			@PhoneHome, 
			@PhoneMobile,  
			@CreatedFrom,  
			@CreatedTo,
			@StatusId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
/*
PRINT N'Altering [order].[pOrderSearch]'
GO

alter procedure [order].[pOrderSearch]
	@FirstName		nvarchar(64),
	@LastName		nvarchar(64),
	@CustomerEmail  nvarchar(64),
	@CivicNumber	varchar(50),
	@Page           int = null,
	@PageSize       int

as
begin

    set nocount on

	DECLARE @sqlGeneral nvarchar(max)
	declare @sqlFragment nvarchar(max)
	declare @sqlCount nvarchar(max)
	declare @sql nvarchar(max)

    SET @sqlFragment = '
		select
			row_number() over (order by OrderId desc) as _Number,
			OrderId
		from
			[order].[tOrder] o
			inner join [customer].[tCustomerInformation] ci on o.CustomerId = ci.CustomerId
		where
			1 = 1'
			+ case when (@FirstName is not null) then + ' and contains(ci.FirstName, @FirstName)' else '' end
			+ case when (@LastName is not null) then + ' and contains(ci.LastName, @LastName)' else '' end
			+ case when (@CivicNumber is not null) then + 'and ci.CivicNumber = @CivicNumber' else '' end
			+ case when (@CustomerEmail is not null) then + 'and ci.Email = @CustomerEmail' else '' end

	SET @sqlGeneral = '
		IF (@FirstName IS NOT NULL)
		BEGIN
			SET @FirstName = generic.fPrepareFulltextSearchParameter(@FirstName)
		END

		IF (@LastName IS NOT NULL)
		BEGIN
			SET @LastName = generic.fPrepareFulltextSearchParameter(@LastName)
		END'

	set @sqlCount = @sqlGeneral + '

		select count(*) from (' + @sqlFragment + ') as CountResults'

	set @sql = @sqlGeneral + '
		select
			o.*
		from
			(' + @sqlFragment + ') as result
			inner join [order].[vCustomOrder] o on o.[Order.OrderId] = result.OrderId
		where
			_Number > ' + cast((@Page - 1) * @PageSize as varchar(10)) + '
			and _Number <= ' + cast(@Page * @PageSize as varchar(10)) + '
		order by
			result.OrderId desc'


	exec sp_executesql @sqlCount,
		N'	@FirstName		nvarchar(64),
			@LastName		nvarchar(64),
			@CustomerEmail  nvarchar(64),
			@CivicNumber	varchar(50)',
			@FirstName,
			@LastName,
			@CustomerEmail,
			@CivicNumber

	exec sp_executesql @sql, 
		N'	@FirstName		nvarchar(64),
			@LastName		nvarchar(64),
			@CustomerEmail  nvarchar(64),
			@CivicNumber	varchar(50)',
			@FirstName,
			@LastName,
			@CustomerEmail,
			@CivicNumber
end

GO

IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

*/

PRINT N'Creating primary key [PK_tCustomerInformation] on [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] ADD CONSTRAINT [PK_tCustomerInformation] PRIMARY KEY CLUSTERED  ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tAddress_AddressId_Covered] on [customer].[tAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_AddressId_Covered] ON [customer].[tAddress] ([AddressId]) INCLUDE ([Addressee], [AddressTypeId], [City], [CountryId], [CustomerId], [PhoneNumber], [PostalCode], [StreetAddress], [StreetAddress2])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating index [IX_tAddress_CustomerInformationId] on [customer].[tAddress]'
GO
CREATE NONCLUSTERED INDEX [IX_tAddress_CustomerInformationId] ON [customer].[tAddress] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tAddress]'
GO
ALTER TABLE [customer].[tAddress] ADD
CONSTRAINT [FK_tAddress_tCustomerInformation] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [customer].[tCustomerInformation]'
GO
ALTER TABLE [customer].[tCustomerInformation] ADD
CONSTRAINT [FK_tCustomerInformation_tAddress_Billing] FOREIGN KEY ([CustomerId], [DefaultBillingAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId]),
CONSTRAINT [FK_tCustomerInformation_tAddress_Delivery] FOREIGN KEY ([CustomerId], [DefaultDeliveryAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId]),
CONSTRAINT [FK_tCustomerInformation_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId]) ON DELETE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [order].[tOrder]'
GO
ALTER TABLE [order].[tOrder] ADD
CONSTRAINT [FK_tOrder_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Creating full text catalogs'
GO
CREATE FULLTEXT CATALOG [fulltextcatalog_tcustomerinformation_default]
ON FILEGROUP [PRIMARY]
--IN PATH 'C:\Program Files\Microsoft SQL Server\MSSQL.1\MSSQL\FTData'
WITH ACCENT_SENSITIVITY = ON
AUTHORIZATION [dbo]
GO
PRINT N'Adding full text indexing to tables'
GO
CREATE FULLTEXT INDEX ON [customer].[tCustomerInformation] KEY INDEX [PK_tCustomerInformation] ON [fulltextcatalog_tcustomerinformation_default]
GO
PRINT N'Adding full text indexing to columns'
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ADD (FirstName LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ADD (LastName LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ENABLE
GO