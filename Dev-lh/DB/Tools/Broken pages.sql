/*
Find broken pages with block product filter
*/

DECLARE @BlockTypeID INT

SELECT
	@BlockTypeID = bt.BlockTypeId
FROM
	sitestructure.tBlockType bt
WHERE
	bt.CommonName = 'ProductFilter'

SELECT
	b.BlockId AS 'Block Id',
	b.Title AS 'Block Title',
	ISNULL(cnppp.Title + ' / ', '') +
	ISNULL(cnpp.Title + ' / ', '') +
	ISNULL(cnp.Title + ' / ', '') +
	ISNULL(cn.Title + ' / ', '') AS 'Parent',
	bpf.BlockId AS 'Filter Block Id'
FROM
	sitestructure.tBlock b
	
	LEFT OUTER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = b.ContentNodeId
	LEFT OUTER JOIN sitestructure.tContentNode cnp ON cnp.ContentNodeId = cn.ParentContentNodeId
	LEFT OUTER JOIN sitestructure.tContentNode cnpp ON cnpp.ContentNodeId = cnp.ParentContentNodeId
	LEFT OUTER JOIN sitestructure.tContentNode cnppp ON cnppp.ContentNodeId = cnpp.ParentContentNodeId
	
	LEFT OUTER JOIN lekmer.tBlockProductFilter bpf ON bpf.BlockId = b.BlockId
WHERE
	b.BlockTypeId = @BlockTypeID
	AND
	bpf.BlockId IS NULL
ORDER BY
	ISNULL(cnppp.Title + ' / ', '') +
	ISNULL(cnpp.Title + ' / ', '') +
	ISNULL(cnp.Title + ' / ', '') +
	ISNULL(cn.Title + ' / ', '')