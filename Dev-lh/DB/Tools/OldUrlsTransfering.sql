SELECT
	LP.ProductId,
	Ch.LanguageId,
	LP.OldUrl,
	SUBSTRING(LP.OldUrl, CHARINDEX('/', LP.OldUrl) + 1, LEN(LP.OldUrl))
FROM
	lekmer.tLekmerProduct LP
	INNER JOIN product.tProductRegistryProduct AS PRP ON LP.ProductId = PRP.ProductId
	INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
	INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
WHERE
	LP.OldUrl IS NOT NULL


-- Transfer old urls from tLekmerProduct into tProductUrlHistory
INSERT INTO lekmer.tProductUrlHistory (
	ProductId,
	LanguageId,
	UrlTitleOld,
	HistoryLifeIntervalTypeId
)
SELECT
	LP.ProductId,
	Ch.LanguageId,
	SUBSTRING(LP.OldUrl, CHARINDEX('/', LP.OldUrl) + 1, LEN(LP.OldUrl)),
	1
FROM
	lekmer.tLekmerProduct LP
	INNER JOIN product.tProductRegistryProduct AS PRP ON LP.ProductId = PRP.ProductId
	INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
	INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
WHERE
	LP.OldUrl IS NOT NULL

-- Check if all '/'	are removed
SELECT
	*
FROM
	lekmer.tProductUrlHistory
WHERE
	CHARINDEX('/', UrlTitleOld) > 0