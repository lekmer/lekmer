IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'Vol')
CREATE LOGIN [Vol] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [Vol] FOR LOGIN [Vol]
GO
GRANT CONNECT TO [Vol]
