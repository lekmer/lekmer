SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [productlek].[vRecommenderItem]
AS
	SELECT
		[RecommenderItemId] AS 'RecommenderItem.RecommenderItemId',
		[RecommenderListId] AS 'RecommenderItem.RecommenderListId',
		[ProductId] AS 'RecommenderItem.ProductId',
		[CreatedDate] AS 'RecommenderItem.CreatedDate',
		[UpdatedDate] AS 'RecommenderItem.UpdatedDate'
	FROM
		[productlek].[tRecommenderItem]

GO
