
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailInfo]
AS
	SELECT 
		gcvmi.[GiftCardViaMailInfoId] AS 'GiftCardViaMailInfo.GiftCardViaMailInfoId',
		gcvmi.[OrderId] AS 'GiftCardViaMailInfo.OrderId',
		gcvmi.[CampaignId] AS 'GiftCardViaMailInfo.CampaignId',
		gcvmi.[ActionId] AS 'GiftCardViaMailInfo.ActionId',
		gcvmi.[DiscountValue] AS 'GiftCardViaMailInfo.DiscountValue',
		gcvmi.[DateToSend] AS 'GiftCardViaMailInfo.DateToSend',
		gcvmi.[StatusId] AS 'GiftCardViaMailInfo.StatusId',
		gcvmi.[VoucherInfoId] AS 'GiftCardViaMailInfo.VoucherInfoId',
		gcvmi.[VoucherCode] AS 'GiftCardViaMailInfo.VoucherCode',
		o.[ChannelId] AS 'GiftCardViaMailInfo.ChannelId'
	FROM
		[campaignlek].[tGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[OrderId]
GO
