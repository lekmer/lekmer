
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vBrandTranslation]
AS
SELECT 
	[BrandId]		'BrandTranslation.BrandId',
	[LanguageId]	'BrandTranslation.LanguageId',
	[Title]			'BrandTranslation.Title',
	[Description]	'BrandTranslation.Description',
	[PackageInfo]	'BrandTranslation.PackageInfo'
FROM 
	[lekmer].[tBrandTranslation]
GO
