SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vVariationSecure]
AS
	SELECT 
		   v.[VariationGroupId] 'Variation.GroupId'
		  ,v.[VariationId] 'Variation.Id'
		  ,v.[VariationTypeId] 'Variation.TypeId'
		  ,v.[Value] 'Variation.Value'
		  ,v.[Ordinal] 'Variation.Ordinal'
		  ,vt.[Title] AS 'Variation.TypeName'
	FROM 
		 [product].[tVariation] AS v
	     INNER JOIN [product].tVariationType vt ON vt.VariationTypeId = v.VariationTypeId

GO
