SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomModelFragmentRegion]
as
	select
		*
	from
		[template].[vModelFragmentRegion]
GO
