SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vSizeTableRowSecure]
AS
	SELECT
		[st].[SizeTableRowId]		'SizeTableRow.SizeTableRowId',
		[st].[SizeTableId]			'SizeTableRow.SizeTableId',
		[st].[SizeId]				'SizeTableRow.SizeId',
		[st].[Column1Value]			'SizeTableRow.Column1Value',
		[st].[Column2Value]			'SizeTableRow.Column2Value',
		[st].[Ordinal]				'SizeTableRow.Ordinal'
	FROM 
		[lekmer].[tSizeTableRow] st
GO
