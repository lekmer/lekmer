SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vBlockRichText]
AS
SELECT 
	   b.*
      ,COALESCE (transl.Content, t.Content) AS 'BlockRichText.Content'
FROM  
	sitestructure.tBlockRichText AS t
	CROSS JOIN core.tLanguage AS l
	LEFT JOIN  sitestructure.tBlockRichTextTranslation AS transl ON transl.BlockId = t.BlockId 
		AND transl.LanguageId = l.LanguageId
	INNER JOIN [sitestructure].[vCustomBlock] AS b on t.[BlockId] = b.[Block.BlockId] 
		AND l.LanguageId = b.[Block.LanguageId]

GO
