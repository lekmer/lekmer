
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vCustomProductWithoutStatusFilter]
AS 
	SELECT
		[p].*
	FROM
		[lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]  p
	WHERE
		[p].[Lekmer.SellOnlyInPackage] = 0

GO
