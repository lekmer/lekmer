SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductVariation]
as
	select
		*
	from
		[product].[vProductVariation]
GO
