SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [esales].[vBlockEsalesSetting]
AS
	SELECT
		[bes].[BlockId] AS 'BlockEsalesSetting.BlockId',
		[bes].[EsalesModelComponentId] AS 'BlockEsalesSetting.EsalesModelComponentId'
	FROM
		[esales].[tBlockEsalesSetting] bes

GO
