SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomDeliveryMethodPrice]
as
	select
		*
	from
		[order].[vDeliveryMethodPrice]
GO
