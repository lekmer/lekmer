SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [esales].[vAdChannel]
AS
	SELECT
		[ac].[AdChannelId] 'AdChannel.AdChannelId',
		[ac].[AdId] 'AdChannel.AdId',
		[ac].[RegistryId] 'AdChannel.RegistryId',
		[ac].[Title] 'AdChannel.Title',
		[ac].[ShowOnSite] 'AdChannel.ShowOnSite',
		[ac].[LandingPageUrl] 'AdChannel.LandingPageUrl',
		[ac].[LandingPageId] 'AdChannel.LandingPageId',
		[ac].[ImageUrl] 'AdChannel.ImageUrl',
		[ac].[ImageId] 'AdChannel.ImageId'
	FROM 
		[esales].[tAdChannel] ac

GO
