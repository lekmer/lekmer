SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentPageSecure]
as
	select
		*
	from
		[sitestructure].[vContentPageSecure]
GO
