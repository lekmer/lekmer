
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [customer].[vCustomCustomer]
AS
SELECT
	[c].*,
	
	[ci].[CustomerInformation.GenderTypeId],
	[ci].[CustomerInformation.IsCompany],
	[ci].[CustomerInformation.AlternateAddressId],
	
	[ba].[Address.HouseNumber]		'BillingAddress.HouseNumber',
	[ba].[Address.HouseExtension]	'BillingAddress.HouseExtension',
	[ba].[Address.Reference]		'BillingAddress.Reference',
	[ba].[Address.DoorCode]			'BillingAddress.DoorCode',
	
	[da].[Address.HouseNumber]		'DeliveryAddress.HouseNumber',
	[da].[Address.HouseExtension]	'DeliveryAddress.HouseExtension',
	[da].[Address.Reference]		'DeliveryAddress.Reference',
	[da].[Address.DoorCode]			'DeliveryAddress.DoorCode',
	
	[aa].[Address.AddressId]		'AlternateAddress.AddressId',
	[aa].[Address.CustomerId]		'AlternateAddress.CustomerId',
	[aa].[Address.AddressTypeId]	'AlternateAddress.AddressTypeId',
	[aa].[Address.Addressee]		'AlternateAddress.Addressee',
	[aa].[Address.StreetAddress]	'AlternateAddress.StreetAddress',
	[aa].[Address.StreetAddress2]	'AlternateAddress.StreetAddress2',
	[aa].[Address.PostalCode]		'AlternateAddress.PostalCode',
	[aa].[Address.City]				'AlternateAddress.City',
	[aa].[Address.CountryId]		'AlternateAddress.CountryId',
	[aa].[Address.PhoneNumber]		'AlternateAddress.PhoneNumber',
	[aa].[Address.HouseNumber]		'AlternateAddress.HouseNumber',
	[aa].[Address.HouseExtension]	'AlternateAddress.HouseExtension',
	[aa].[Address.Reference]		'AlternateAddress.Reference',
	[aa].[Address.DoorCode]			'AlternateAddress.DoorCode'
FROM
	[customer].[vCustomer] c
	LEFT JOIN [customerlek].[vCustomerInformation] ci	ON [ci].[CustomerInformation.CustomerId] = [c].[CustomerInformation.InformationId]
	LEFT JOIN [customer].[vCustomAddress] ba			ON [ba].[Address.AddressId] = [c].[CustomerInformation.DefaultBillingAddressId]
	LEFT JOIN [customer].[vCustomAddress] da			ON [da].[Address.AddressId] = [c].[CustomerInformation.DefaultDeliveryAddressId]
	LEFT JOIN [customer].[vCustomAddress] aa			ON [aa].[Address.AddressId] = [ci].[CustomerInformation.AlternateAddressId]
GO
