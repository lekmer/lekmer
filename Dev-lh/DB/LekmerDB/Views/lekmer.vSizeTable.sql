
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vSizeTable]
AS
	SELECT
		[st].[SizeTableId]			'SizeTable.SizeTableId',
		[st].[SizeTableFolderId]	'SizeTable.SizeTableFolderId',
		[st].[CommonName]			'SizeTable.CommonName',
		COALESCE ([stt].[Title], [st].[Title]) 'SizeTable.Title',
		COALESCE ([stt].[Description], [st].[Description]) 'SizeTable.Description',
		COALESCE ([stt].[Column1Title], [st].[Column1Title]) 'SizeTable.Column1Title',
		COALESCE ([stt].[Column2Title], [st].[Column2Title]) 'SizeTable.Column2Title',
		[l].[LanguageId]			'SizeTable.LanguageId'
	FROM 
		[lekmer].[tSizeTable] st
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tSizeTableTranslation] AS stt ON [stt].[SizeTableId] = [st].[SizeTableId] AND [stt].[LanguageId] = l.[LanguageId]
GO
