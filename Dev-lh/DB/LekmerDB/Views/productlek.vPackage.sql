
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [productlek].[vPackage]
AS
	SELECT     
		[p].[PackageId] 'Package.PackageId',
		[p].[MasterProductId] 'Package.MasterProductId',
		COALESCE ([pt].[PackageTranslation.GeneralInfo], [p].[GeneralInfo]) 'Package.GeneralInfo',
		[ch].[ChannelId]
	FROM
		[productlek].[tPackage] p
		CROSS JOIN [core].[tChannel] ch
		LEFT JOIN [productlek].[vPackageTranslation] pt ON [pt].[PackageTranslation.PackageId] = [p].[PackageId]
			AND [pt].[PackageTranslation.LanguageId] = [ch].[LanguageId]
		
GO
