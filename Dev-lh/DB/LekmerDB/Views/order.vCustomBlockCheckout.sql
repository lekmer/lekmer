SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomBlockCheckout]
as
	select
		*
	from
		[order].[vBlockCheckout]
GO
