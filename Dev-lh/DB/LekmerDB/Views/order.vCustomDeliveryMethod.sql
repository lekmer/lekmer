
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCustomDeliveryMethod]
AS
	SELECT
		[dm].*,
		CAST((CASE WHEN [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] IS NULL THEN 0 ELSE 1 END) AS BIT) AS 'DeliveryMethod.ChannelDefault',
		[ldm].[DeliveryMethod.Priority],
		[ldm].[DeliveryMethod.IsCompany],
		[ldm].[DeliveryMethod.DeliveryMethodTypeId]
	FROM
		[order].[vDeliveryMethod] dm
		LEFT JOIN [orderlek].[vDeliveryMethod] ldm ON [ldm].[DeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]
		LEFT JOIN [orderlek].[vChannelDefaultDeliveryMethod] cddm 
			ON [cddm].[ChannelDefaultDeliveryMethod.ChannelId] = [dm].[DeliveryMethod.ChannelId]
			AND [cddm].[ChannelDefaultDeliveryMethod.DeliveryMethodId] = [dm].[DeliveryMethod.DeliveryMethodId]

GO
