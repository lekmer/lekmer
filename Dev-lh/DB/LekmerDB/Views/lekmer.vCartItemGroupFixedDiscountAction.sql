SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vCartItemGroupFixedDiscountAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemGroupFixedDiscountAction.CartActionId',
		cid.[IncludeAllProducts]		AS 'CartItemGroupFixedDiscountAction.IncludeAllProducts',
		ca.*
	FROM
		[lekmer].[tCartItemGroupFixedDiscountAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]



GO
