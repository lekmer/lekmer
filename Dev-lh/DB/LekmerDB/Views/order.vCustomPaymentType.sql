
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCustomPaymentType]
AS
	SELECT
		[pt].*,
		[ptp].[PaymentCost] AS 'PaymentType.PaymentCost'
	FROM
		[order].[vPaymentType] pt
		INNER JOIN [core].[tChannel] ch	ON [ch].[ChannelId] = [pt].[PaymentType.ChannelId]
		INNER JOIN [lekmer].[tPaymentTypePrice] ptp ON [ptp].[PaymentTypeId] = [pt].[PaymentType.PaymentTypeId]
														AND [ch].CountryId = [ptp].[CountryId]
														AND [ch].CurrencyId = [ptp].[CurrencyId]
GO
