SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCampaignStatus]
as
	select
		*
	from
		[campaign].[vCampaignStatus]
GO
