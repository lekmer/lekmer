
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vIconSecure]
AS
SELECT
	ic.IconId AS 'Icon.Id',
	ic.Title AS 'Icon.Title',
	[ic].[Description] AS 'Icon.Description',
	im.*
FROM
	lekmer.tIcon ic INNER JOIN
	media.vCustomImageSecure im ON ic.MediaId = im.[Image.MediaId]
GO
