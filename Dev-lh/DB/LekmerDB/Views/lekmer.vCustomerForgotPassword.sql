
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vCustomerForgotPassword]
AS
SELECT     
      [CustomerId]	'ForgotPassword.CustomerId', 
      [Guid]		'ForgotPassword.Guid', 
      [CreatedDate] 'ForgotPassword.CreatedDate',
      [ValidToDate] 'ForgotPassword.ValidToDate'
FROM
      [lekmer].[tCustomerForgotPassword]
GO
