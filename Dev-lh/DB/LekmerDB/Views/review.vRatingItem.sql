
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingItem]
AS
	SELECT
		ri.[RatingItemId]	'RatingItem.RatingItemId',
		ri.[RatingId]		'RatingItem.RatingId',
		COALESCE(rit.[Title], ri.[Title])	'RatingItem.Title',
		COALESCE(rit.[Description], ri.[Description])	'RatingItem.Description',
		ri.[Score]			'RatingItem.Score',
		c.[ChannelId]		'RatingItem.Channel.Id'
	FROM
		[review].[tRatingItem] ri
		INNER JOIN [review].[tRating] r ON r.[RatingId] = ri.[RatingId]

		INNER JOIN [review].[tRatingModuleChannel] rmc ON rmc.[RatingRegistryId] = r.[RatingRegistryId]
		INNER JOIN [core].[tChannel] c ON c.[ChannelId] = rmc.[ChannelId]

		LEFT OUTER JOIN [review].[tRatingItemTranslation] rit ON rit.[RatingItemId] = ri.[RatingItemId] AND rit.[LanguageId] = c.[LanguageId]
GO
