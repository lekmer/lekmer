SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vContentMessageTranslation]
AS
SELECT 
	[ContentMessageId] AS 'ContentMessageTranslation.ContentMessageId',
	[LanguageId] AS 'ContentMessageTranslation.LanguageId',
	[Message] AS 'ContentMessageTranslation.Message'
FROM 
	[lekmer].[tContentMessageTranslation]

GO
