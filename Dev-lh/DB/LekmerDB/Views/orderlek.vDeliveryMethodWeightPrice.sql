SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [orderlek].[vDeliveryMethodWeightPrice]
AS
	SELECT
		*
	FROM
		[orderlek].[tDeliveryMethodWeightPrice]
GO
