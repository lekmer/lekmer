SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [esales].[vBlockEsalesTopSellersV2Translation]
AS
	SELECT
		[betst].[BlockId],
		[betst].[LanguageId],
		[betst].[UrlTitle]
	FROM
		[esales].[tBlockEsalesTopSellersV2Translation] betst
GO
