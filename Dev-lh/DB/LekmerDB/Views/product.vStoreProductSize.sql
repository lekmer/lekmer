
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vStoreProductSize]
AS
SELECT 
	t.ProductId 'StoreProductSize.ProductId',
	t.SizeId 'StoreProductSize.SizeId',
	t.StoreId 'StoreProduct.StoreId',
	t.Quantity 'StoreProduct.Quantity',
	t.Threshold 'StoreProduct.Threshold'
FROM
	product.tStoreProductSize t


GO
