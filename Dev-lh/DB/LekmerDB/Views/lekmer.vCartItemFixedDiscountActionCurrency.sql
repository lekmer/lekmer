SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vCartItemFixedDiscountActionCurrency]
AS
	SELECT
		ci.[CartActionId]	AS 'CartItemFixedDiscountActionCurrency.CartActionId',
		ci.[CurrencyId]		AS 'CartItemFixedDiscountActionCurrency.CurrencyId',
		ci.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[lekmer].[tCartItemFixedDiscountActionCurrency] ci
		INNER JOIN [core].[vCustomCurrency] c ON ci.[CurrencyId] = c.[Currency.Id]


GO
