SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vSiteStructureModuleChannel]
AS
	select 
		tsmc.ChannelId 'SiteStructureModuleChannel.ChannelId',
		tsmc.SiteStructureRegistryId 'SiteStructureModuleChannel.SiteStructureRegistryId'
	from 
		[sitestructure].tSiteStructureModuleChannel tsmc

GO
