SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create VIEW addon.[vCartItemPriceActionCurrency]
as
	select
		ac.CartActionId AS 'CartItemPriceAction.ActionId',
		ac.CurrencyId AS 'CartItemPriceAction.CurrencyId',
		ac.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		addon.tCartItemPriceActionCurrency ac
		inner join core.vCustomCurrency c on ac.CurrencyId = c.[Currency.Id]


GO
