
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCampaignFolder]
AS
	SELECT
		FolderId as 'CampaignFolder.Id',
		ParentFolderId as 'CampaignFolder.ParentId',
		Title as 'CampaignFolder.Title',
		TypeId AS 'CampaignFolder.TypeId',
		Ordinal AS 'CampaignFolder.Ordinal'
	FROM
		campaign.tCampaignFolder
GO
