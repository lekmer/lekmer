SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductModuleChannel]
as
	select
		*
	from
		[product].[vProductModuleChannel]
GO
