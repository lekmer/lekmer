SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vCategoryTagGroup]
AS
	SELECT
		CategoryId 'CategoryTagGroup.CategoryId',
		TagGroupId 'CategoryTagGroup.TagGroupId'
	FROM
		[lekmer].[tCategoryTagGroup]
GO
