
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vCustomProductRecord]
AS
	SELECT
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.AgeFromMonth],
		lp.[Lekmer.AgeToMonth],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.Measurement],
		lp.[Lekmer.BatteryTypeId],
		lp.[Lekmer.NumberOfBatteries],
		lp.[Lekmer.IsBatteryIncluded],
		lp.[Lekmer.ExpectedBackInStock],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.SizeDeviationId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[lp].[Lekmer.SupplierId],
		[lp].[Lekmer.PurchasePrice],
		[lp].[Lekmer.PurchaseCurrencyId],
		[lp].[Lekmer.SupplierArticleNumber],
		[lp].[Lekmer.IsDropShip],
		[lp].[Lekmer.AveragePrice],
		[dt].*
	FROM
		[product].[vProductRecord] AS p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId]
GO
