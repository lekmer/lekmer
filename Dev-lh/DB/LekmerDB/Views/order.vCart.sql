
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCart]
AS
SELECT	
	[CartId] AS 'Cart.CartId',
	[CartGuid] AS 'Cart.CartGuid',
	[CreatedDate] AS 'Cart.CreatedDate',
	[UpdatedDate] AS 'Cart.UpdatedDate',
	[IPAddress] AS 'Cart.IPAddress'
FROM
	[order].tCart

GO
