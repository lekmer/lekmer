
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vOrderPayment]
AS
	SELECT
		[OrderPaymentId] AS 'OrderPayment.OrderPaymentId',
		[OrderId] AS 'OrderPayment.OrderId',
		[PaymentTypeId] AS 'OrderPayment.PaymentTypeId',
		[Price] AS 'OrderPayment.Price',
		[Vat] AS 'OrderPayment.Vat',
		[ReferenceId] AS 'OrderPayment.ReferenceId',
		[Captured] AS 'OrderPayment.Captured',
		[KlarnaEID] AS 'OrderPayment.KlarnaEID',
		[KlarnaPClass] AS 'OrderPayment.KlarnaPClass',
		[MaksuturvaCode] AS 'OrderPayment.MaksuturvaCode',
		[QliroClientRef] AS 'OrderPayment.QliroClientRef',
		[QliroPaymentCode] AS 'OrderPayment.QliroPaymentCode',
		[CollectorStoreId] AS 'OrderPayment.CollectorStoreId',
		[CollectorPaymentCode] AS 'OrderPayment.CollectorPaymentCode'
	FROM
		[order].[tOrderPayment]

GO
