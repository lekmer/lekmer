SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vFixedDiscountCartAction]
AS
	SELECT
		fdca.[CartActionId]	AS 'FixedDiscountCartAction.CartActionId',
		ca.*
	FROM
		[campaignlek].[tFixedDiscountCartAction] fdca
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = fdca.[CartActionId]
GO
