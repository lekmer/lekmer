
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [customerlek].[vCustomerInformation]
AS
	SELECT
		CustomerId AS 'CustomerInformation.CustomerId',
		GenderTypeId AS 'CustomerInformation.GenderTypeId',
		IsCompany AS 'CustomerInformation.IsCompany',
		AlternateAddressId AS 'CustomerInformation.AlternateAddressId'
	FROM
		[customerlek].[tCustomerInformation]

GO
