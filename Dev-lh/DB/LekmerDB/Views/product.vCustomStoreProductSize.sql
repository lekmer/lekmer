SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [product].[vCustomStoreProductSize]
as
	select
		*
	from
		[product].[vStoreProductSize]

GO
