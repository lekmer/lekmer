
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vCustomBlockProductRelationListItem]
AS
	SELECT
		*
	FROM
		[product].[vBlockProductRelationListItem]
GO
