SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [addon].[vCustomReviewRecord]
as
         select
                   *
         from
                   [addon].[vReviewRecord]
GO
