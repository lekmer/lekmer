
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [customer].[vCustomerSecure]
AS
SELECT     
	[c].[CustomerId]				'Customer.CustomerId',
	[c].[ErpId]						'Customer.ErpId',
	[c].[CustomerStatusId]			'Customer.CustomerStatusId',
	[c].[CustomerRegistryId]		'Customer.CustomerRegistryId',

	[ci].[CustomerId]				'CustomerInformation.InformationId',
	[ci].[FirstName]				'CustomerInformation.FirstName',
	[ci].[LastName]					'CustomerInformation.LastName',
	[ci].[CivicNumber]				'CustomerInformation.CivicNumber',
	[ci].[PhoneNumber]				'CustomerInformation.PhoneNumber',
	[ci].[CellPhoneNumber]			'CustomerInformation.CellPhoneNumber',
	[ci].[Email]					'CustomerInformation.Email',
	[ci].[CreatedDate]				'CustomerInformation.InformationCreatedDate',
	[ci].[DefaultBillingAddressId]	'CustomerInformation.DefaultBillingAddressId',
	[ci].[DefaultDeliveryAddressId]	'CustomerInformation.DefaultDeliveryAddressId',

	[ba].[Address.AddressId]		'BillingAddress.AddressId',
	[ba].[Address.CustomerId]		'BillingAddress.CustomerId',
	[ba].[Address.AddressTypeId]	'BillingAddress.AddressTypeId',
	[ba].[Address.Addressee]		'BillingAddress.Addressee',
	[ba].[Address.StreetAddress]	'BillingAddress.StreetAddress',
	[ba].[Address.StreetAddress2]	'BillingAddress.StreetAddress2',
	[ba].[Address.PostalCode]		'BillingAddress.PostalCode',
	[ba].[Address.City]				'BillingAddress.City',
	[ba].[Address.CountryId]		'BillingAddress.CountryId',
	[ba].[Address.PhoneNumber]		'BillingAddress.PhoneNumber',

	[da].[Address.AddressId]		'DeliveryAddress.AddressId',
	[da].[Address.CustomerId]		'DeliveryAddress.CustomerId',
	[da].[Address.AddressTypeId]	'DeliveryAddress.AddressTypeId',
	[da].[Address.Addressee]		'DeliveryAddress.Addressee',
	[da].[Address.StreetAddress]	'DeliveryAddress.StreetAddress',
	[da].[Address.StreetAddress2]	'DeliveryAddress.StreetAddress2',
	[da].[Address.PostalCode]		'DeliveryAddress.PostalCode',
	[da].[Address.City]				'DeliveryAddress.City',
	[da].[Address.CountryId]		'DeliveryAddress.CountryId',
	[da].[Address.PhoneNumber]		'DeliveryAddress.PhoneNumber',

	[cu].[CustomerId]				'CustomerUser.UserId',
	[cu].[CustomerRegistryId]		'CustomerUser.UserRegistryId',
	[cu].[UserName]					'CustomerUser.UserName',
	[cu].[Password]					'CustomerUser.Password',
	[cu].[PasswordSalt]				'CustomerUser.PasswordSalt',
	[cu].[CreatedDate]				'CustomerUser.UserCreatedDate',
	[cu].[IsAlternateLogin]			'CustomerUser.IsAlternateLogin',

	[fcu].[CustomerId]				'FacebookCustomerUser.CustomerId',
	[fcu].[CustomerRegistryId]		'FacebookCustomerUser.CustomerRegistryId',
	[fcu].[FacebookId]				'FacebookCustomerUser.FacebookId',
	[fcu].[Name]					'FacebookCustomerUser.Name',
	[fcu].[Email]					'FacebookCustomerUser.Email',
	[fcu].[CreatedDate]				'FacebookCustomerUser.CreatedDate',
	[fcu].[IsAlternateLogin]		'FacebookCustomerUser.IsAlternateLogin'
FROM
	[customer].[tCustomer] c
	INNER JOIN [customer].[tCustomerInformation] ci		ON [ci].[CustomerId] = [c].[CustomerId]
	LEFT JOIN [customer].[tCustomerUser] cu				ON [cu].[CustomerId] = [c].[CustomerId]
	LEFT JOIN [customerlek].[tFacebookCustomerUser] fcu ON [fcu].[CustomerId] = [c].[CustomerId]
	LEFT JOIN [customer].[vCustomAddress] ba 			ON [ba].[Address.AddressId] = [ci].[DefaultBillingAddressId]
	LEFT JOIN [customer].[vCustomAddress] da 			ON [da].[Address.AddressId] = [ci].[DefaultDeliveryAddressId]
GO
