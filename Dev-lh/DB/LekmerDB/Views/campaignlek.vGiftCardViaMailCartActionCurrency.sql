SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailCartActionCurrency]
AS
	SELECT
		gccac.[CartActionId] AS 'GiftCardViaMailCartActionCurrency.CartActionId',
		gccac.[CurrencyId] AS 'GiftCardViaMailCartActionCurrency.CurrencyId',
		gccac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tGiftCardViaMailCartActionCurrency] gccac
		INNER JOIN [core].[vCustomCurrency] c ON gccac.[CurrencyId] = c.[Currency.Id]

GO
