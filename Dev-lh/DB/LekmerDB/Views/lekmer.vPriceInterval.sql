SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [lekmer].[vPriceInterval]
AS
SELECT     
	p.PriceIntervalId AS 'PriceInterval.PriceIntervalId',
	p.[From] AS 'PriceInterval.From',
	p.[To] AS 'PriceInterval.To',
	COALESCE (pt.Title, p.Title) AS 'PriceInterval.Title',
	t.LanguageId AS 'PriceInterval.LanguageId',
	c.*,
	Pssr.ContentNodeId AS 'PriceInterval.ContentNodeId'
FROM
	[lekmer].[tPriceInterval] p
	CROSS JOIN core.tChannel AS t
	LEFT JOIN [lekmer].tPriceIntervalTranslation AS pt ON pt.PriceIntervalId = p.PriceIntervalId AND pt.LanguageId = t.LanguageId
	inner join [core].[vCustomCurrency] c on p.CurrencyId = c.[Currency.Id]
	LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc ON Ssmc.ChannelId = t.ChannelId
	LEFT JOIN lekmer.tPriceIntervalSiteStructureRegistry Pssr ON Pssr.PriceIntervalId = p.PriceIntervalId AND Pssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId

GO
