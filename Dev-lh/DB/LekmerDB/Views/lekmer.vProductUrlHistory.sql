SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [lekmer].[vProductUrlHistory]
AS
	SELECT
		[ProductUrlHistoryId] AS 'ProductUrlHistory.Id',
		[ProductId] AS 'ProductUrlHistory.ProductId',
		[LanguageId] AS 'ProductUrlHistory.LanguageId',
		[UrlTitleOld] AS 'ProductUrlHistory.UrlTitleOld',
		[CreationDate] AS 'ProductUrlHistory.CreationDate',
		[LastUsageDate] AS 'ProductUrlHistory.LastUsageDate',
		[UsageAmount] AS 'ProductUrlHistory.UsageAmount',
		[HistoryLifeIntervalTypeId] AS 'ProductUrlHistory.HistoryLifeIntervalTypeId'
	FROM
		[lekmer].[tProductUrlHistory]
GO
