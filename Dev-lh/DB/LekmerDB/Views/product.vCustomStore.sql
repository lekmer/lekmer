SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [product].[vCustomStore]
as
	select
		*
	from
		[product].[vStore]
GO
