
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vCollectorPaymentType]
AS
SELECT
	[cpt].[CollectorPaymentTypeId] AS 'CollectorPaymentType.Id',
	[cpt].[ChannelId] AS 'CollectorPaymentType.ChannelId',
	[cpt].[Code] AS 'CollectorPaymentType.Code',
	[cpt].[Description] AS 'CollectorPaymentType.Description',
	[cpt].[StartFee] AS 'CollectorPaymentType.StartFee',
	[cpt].[InvoiceFee] AS 'CollectorPaymentType.InvoiceFee',
	[cpt].[MonthlyInterestRate] AS 'CollectorPaymentType.MonthlyInterestRate',
	[cpt].[NoOfMonths] AS 'CollectorPaymentType.NoOfMonths',
	[cpt].[MinPurchaseAmount] AS 'CollectorPaymentType.MinPurchaseAmount',
	[cpt].[LowestPayment] AS 'CollectorPaymentType.LowestPayment',
	[cpt].[PartPaymentType] AS 'CollectorPaymentType.PartPaymentType',
	[cpt].[InvoiceType] AS 'CollectorPaymentType.InvoiceType',
	[cpt].[Ordinal] AS 'CollectorPaymentType.Ordinal'
FROM
	[orderlek].[tCollectorPaymentType] cpt

GO
