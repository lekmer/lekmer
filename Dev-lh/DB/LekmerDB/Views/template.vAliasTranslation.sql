SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [template].[vAliasTranslation]
AS
select
	at.[AliasId] AS 'Alias.Id',
	at.[Value] AS 'Alias.Value',
	at.[LanguageId] AS 'Language.Id',
	l.[Title] AS 'Language.Title',
	l.[ISO] AS 'Language.ISO'
from
	[template].[tAliasTranslation] AS at
	INNER JOIN [core].[tLanguage] l ON at.LanguageId = l.LanguageId
GO
