
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vBlockBestRatedProductList]
AS
	SELECT
		[BlockId] AS 'BlockBestRatedProductList.BlockId',
		[CategoryId] AS 'BlockBestRatedProductList.CategoryId',
		[RatingId] AS 'BlockBestRatedProductList.RatingId',
		[bs].*
	FROM
		[review].[tBlockBestRatedProductList] bbrpl
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bbrpl].[BlockId]

GO
