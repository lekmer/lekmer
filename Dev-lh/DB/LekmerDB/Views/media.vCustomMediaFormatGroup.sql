SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [media].[vCustomMediaFormatGroup]
as
	select
		*
	from
		[media].[vMediaFormatGroup]
GO
