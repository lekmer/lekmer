SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vCartActionTargetProductType]
AS
	SELECT
		[CartActionId] AS 'CartActionTargetProductType.CartActionId',
		[ProductTypeId] AS 'CartActionTargetProductType.ProductTypeId'
	FROM
		[campaignlek].[tCartActionTargetProductType] t

GO
