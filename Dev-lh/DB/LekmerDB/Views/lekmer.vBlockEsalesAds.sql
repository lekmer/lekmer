
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vBlockEsalesAds]
AS
	SELECT
		[b].*,
		[bea].[PanelPath] AS 'BlockEsalesAds.PanelPath',
		[bea].[Format] AS 'BlockEsalesAds.Format',
		[bea].[Gender] AS 'BlockEsalesAds.Gender',
		[bea].[ProductCategory] AS 'BlockEsalesAds.ProductCategory',
		[bs].*
	FROM
		[lekmer].[tBlockEsalesAds] bea
		INNER JOIN [sitestructure].[vCustomBlock] b ON b.[Block.BlockId] = bea.[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]

GO
