
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [addon].[vBlockTopList]
AS
	SELECT
		[BlockId]					AS 'BlockTopList.BlockId',
		[IncludeAllCategories]		AS 'BlockTopList.IncludeAllCategories',
		[OrderStatisticsDayCount]	AS 'BlockTopList.OrderStatisticsDayCount',
		[LinkContentNodeId]			AS 'BlockTopList.LinkContentNodeId',
		[CustomUrl]					AS 'BlockTopList.CustomUrl'
	FROM
		[addon].[tBlockTopList]
GO
