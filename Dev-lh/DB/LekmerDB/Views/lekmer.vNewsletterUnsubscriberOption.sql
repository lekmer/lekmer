
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vNewsletterUnsubscriberOption]
AS
SELECT
	[UnsubscriberOprionId] AS 'NewsletterUnsubscriberOption.UnsubscriberOprionId',
	[UnsubscriberId] AS 'NewsletterUnsubscriberOption.UnsubscriberId',
	[NewsletterTypeId] AS 'NewsletterUnsubscriberOption.NewsletterTypeId',
	[CreatedDate] AS 'NewsletterUnsubscriberOption.CreatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriberOption]
GO
