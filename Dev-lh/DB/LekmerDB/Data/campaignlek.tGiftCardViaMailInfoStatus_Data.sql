SET IDENTITY_INSERT [campaignlek].[tGiftCardViaMailInfoStatus] ON
INSERT INTO [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId], [Title], [CommonName]) VALUES (3, N'ExceedsQuantity', N'ExceedsQuantity')
INSERT INTO [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId], [Title], [CommonName]) VALUES (4, N'EmptyVoucherCode', N'EmptyVoucherCode')
SET IDENTITY_INSERT [campaignlek].[tGiftCardViaMailInfoStatus] OFF
SET IDENTITY_INSERT [campaignlek].[tGiftCardViaMailInfoStatus] ON
INSERT INTO [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId], [Title], [CommonName]) VALUES (1, N'Ready to send', N'ReadyToSend')
INSERT INTO [campaignlek].[tGiftCardViaMailInfoStatus] ([StatusId], [Title], [CommonName]) VALUES (2, N'Sent', N'Sent')
SET IDENTITY_INSERT [campaignlek].[tGiftCardViaMailInfoStatus] OFF
