SET IDENTITY_INSERT [product].[tRelationListType] ON
INSERT INTO [product].[tRelationListType] ([RelationListTypeId], [CommonName], [Description]) VALUES (1000002, N'ColorVariant', N'Color Variant')
SET IDENTITY_INSERT [product].[tRelationListType] OFF
SET IDENTITY_INSERT [product].[tRelationListType] ON
INSERT INTO [product].[tRelationListType] ([RelationListTypeId], [CommonName], [Description]) VALUES (1, N'Accessories', N'Accessories')
INSERT INTO [product].[tRelationListType] ([RelationListTypeId], [CommonName], [Description]) VALUES (2, N'Similar', N'Similar')
INSERT INTO [product].[tRelationListType] ([RelationListTypeId], [CommonName], [Description]) VALUES (3, N'UpSell', N'Up Sell')
INSERT INTO [product].[tRelationListType] ([RelationListTypeId], [CommonName], [Description]) VALUES (4, N'CrossSell', N'Cross Sell')
INSERT INTO [product].[tRelationListType] ([RelationListTypeId], [CommonName], [Description]) VALUES (1000001, N'Variant', N'Variant')
SET IDENTITY_INSERT [product].[tRelationListType] OFF
