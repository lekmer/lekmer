SET IDENTITY_INSERT [template].[tComponent] ON
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1000006, N'Save cart form component', N'SaveCartFormComponent')
SET IDENTITY_INSERT [template].[tComponent] OFF
SET IDENTITY_INSERT [template].[tComponent] ON
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1, N'Navigation', N'Navigation')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (2, N'Navigation tree', N'NavigationTree')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (3, N'Cart', N'Cart')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (55, N'Breadcrumb', N'Breadcrumb')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (143, N'Search form', N'SearchForm')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (145, N'Cart campaign', N'CartCampaign')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1000001, N'DibsResponse', N'DibsResponse')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1000002, N'PaymentTypes', N'PaymentTypes')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1000003, N'ReviewMyWishListComponent', N'ReviewMyWishListComponent')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1000004, N'AvailCartPredictionsComponent', N'AvailCartPredictionsComponent')
INSERT INTO [template].[tComponent] ([ComponentId], [Title], [CommonName]) VALUES (1000005, N'Extended mini cart', N'ExtendedMiniCart')
SET IDENTITY_INSERT [template].[tComponent] OFF
