CREATE TABLE [integration].[tCdonExportStatus]
(
[CdonExportStatusId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tCdonExportStatus] ADD CONSTRAINT [PK_tCdonExportStatus] PRIMARY KEY CLUSTERED  ([CdonExportStatusId]) ON [PRIMARY]
GO
