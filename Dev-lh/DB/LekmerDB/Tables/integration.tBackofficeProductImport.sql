CREATE TABLE [integration].[tBackofficeProductImport]
(
[HYErpId] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
