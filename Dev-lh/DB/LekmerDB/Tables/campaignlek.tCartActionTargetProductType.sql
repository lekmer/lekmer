CREATE TABLE [campaignlek].[tCartActionTargetProductType]
(
[CartActionId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartActionTargetProductType] ADD CONSTRAINT [PK_tCartActionTargetProductType] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductTypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartActionTargetProductType] ADD CONSTRAINT [FK_tCartActionTargetProductType_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
ALTER TABLE [campaignlek].[tCartActionTargetProductType] ADD CONSTRAINT [FK_tCartActionTargetProductType_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
