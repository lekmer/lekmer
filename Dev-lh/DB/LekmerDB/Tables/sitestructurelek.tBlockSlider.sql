CREATE TABLE [sitestructurelek].[tBlockSlider]
(
[BlockId] [int] NOT NULL,
[SlideDuration] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructurelek].[tBlockSlider] ADD CONSTRAINT [PK_tBlockSlider] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructurelek].[tBlockSlider] ADD CONSTRAINT [FK_tBlockSlider_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
