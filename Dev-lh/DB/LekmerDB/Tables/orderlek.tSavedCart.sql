CREATE TABLE [orderlek].[tSavedCart]
(
[SavedCartId] [int] NOT NULL IDENTITY(1, 1),
[CartId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[IsReminderSent] [bit] NOT NULL CONSTRAINT [DF_tSavedCart_IsReminderSent] DEFAULT ((0)),
[IsNeedReminder] [bit] NOT NULL CONSTRAINT [DF_tSavedCart_IsNeedReminder] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tSavedCart] ADD CONSTRAINT [PK_tSavedCart] PRIMARY KEY CLUSTERED  ([SavedCartId]) ON [PRIMARY]
GO
