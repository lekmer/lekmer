CREATE TABLE [lekmer].[tWishListItem]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[WishListId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tWishListItem] ADD
CONSTRAINT [FK_tWishListItem_tWishList] FOREIGN KEY ([WishListId]) REFERENCES [lekmer].[tWishList] ([WishListId])
ALTER TABLE [lekmer].[tWishListItem] ADD
CONSTRAINT [FK_tWishListItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tWishListItem] ADD
CONSTRAINT [FK_tWishListItem_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tWishListItem] ADD CONSTRAINT [PK_tWishListItem] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
