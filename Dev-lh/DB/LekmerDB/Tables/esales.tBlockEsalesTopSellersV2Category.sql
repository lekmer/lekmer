CREATE TABLE [esales].[tBlockEsalesTopSellersV2Category]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Category] ADD CONSTRAINT [PK_tBlockEsalesTopSellersV2Category] PRIMARY KEY CLUSTERED  ([BlockId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Category] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2Category_tBlockEsalesTopSellersV2] FOREIGN KEY ([BlockId]) REFERENCES [esales].[tBlockEsalesTopSellersV2] ([BlockId])
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Category] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2Category_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
