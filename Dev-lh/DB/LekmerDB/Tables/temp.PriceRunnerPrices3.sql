CREATE TABLE [temp].[PriceRunnerPrices3]
(
[country] [varchar] (5) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[productId] [varchar] (32) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[price] [decimal] (30, 6) NOT NULL
) ON [PRIMARY]
GO
