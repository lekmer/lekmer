CREATE TABLE [order].[tOrderPayment]
(
[OrderPaymentId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[PaymentTypeId] [int] NOT NULL,
[Price] [decimal] (16, 2) NOT NULL,
[Vat] [decimal] (16, 2) NOT NULL,
[ReferenceId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Captured] [bit] NULL,
[KlarnaEID] [int] NULL,
[KlarnaPClass] [int] NULL,
[MaksuturvaCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[QliroClientRef] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[QliroPaymentCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CollectorStoreId] [int] NULL,
[CollectorPaymentCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [order].[tOrderPayment] ADD 
CONSTRAINT [PK_tOrderPayment] PRIMARY KEY CLUSTERED  ([OrderPaymentId]) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tOrderPayment_OrderId] ON [order].[tOrderPayment] ([OrderId]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tOrderPayment_PaymentTypeId] ON [order].[tOrderPayment] ([PaymentTypeId]) ON [PRIMARY]

ALTER TABLE [order].[tOrderPayment] ADD
CONSTRAINT [FK_tOrderPayment_tOrder1] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [order].[tOrderPayment] ADD
CONSTRAINT [FK_tOrderPayment_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
