CREATE TABLE [integration].[tNlProductData]
(
[HyErpiId] [nvarchar] (25) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tNlProductData] ADD CONSTRAINT [PK__tNlProdu__B58CA49269C0F060] PRIMARY KEY CLUSTERED  ([HyErpiId]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
