CREATE TABLE [product].[tPriceListStatus]
(
[PriceListStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceListStatus] ADD CONSTRAINT [PK_tPriceListStatus] PRIMARY KEY CLUSTERED  ([PriceListStatusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPriceListStatus_CommonName] ON [product].[tPriceListStatus] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPriceListStatus_Title] ON [product].[tPriceListStatus] ([Title]) ON [PRIMARY]
GO
