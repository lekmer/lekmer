CREATE TABLE [lekmer].[tSizeTableRowTranslation]
(
[SizeTableRowId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Column1Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Column2Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableRowTranslation] ADD CONSTRAINT [PK_tSizeTableRowTranslation] PRIMARY KEY CLUSTERED  ([SizeTableRowId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableRowTranslation] ADD CONSTRAINT [FK_tSizeTableRowTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [lekmer].[tSizeTableRowTranslation] ADD CONSTRAINT [FK_tSizeTableRowTranslation_tSizeTableRow] FOREIGN KEY ([SizeTableRowId]) REFERENCES [lekmer].[tSizeTableRow] ([SizeTableRowId])
GO
