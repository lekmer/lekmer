CREATE TABLE [lekmer].[tContentMessageTranslation]
(
[ContentMessageId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageTranslation] ADD CONSTRAINT [PK_tContentMessageTranslation] PRIMARY KEY CLUSTERED  ([ContentMessageId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageTranslation] ADD CONSTRAINT [FK_tContentMessageTranslation_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
GO
ALTER TABLE [lekmer].[tContentMessageTranslation] ADD CONSTRAINT [FK_tContentMessageTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
