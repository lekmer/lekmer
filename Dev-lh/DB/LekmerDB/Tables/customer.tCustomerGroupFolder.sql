CREATE TABLE [customer].[tCustomerGroupFolder]
(
[CustomerGroupFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroupFolder] ADD CONSTRAINT [PK_tCustomerGroupFolder] PRIMARY KEY CLUSTERED  ([CustomerGroupFolderId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroupFolder] ADD CONSTRAINT [FK_tCustomerGroupFolder_tCustomerGroupFolder] FOREIGN KEY ([ParentId]) REFERENCES [customer].[tCustomerGroupFolder] ([CustomerGroupFolderId])
GO
