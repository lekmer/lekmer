CREATE TABLE [media].[tMediaFolder]
(
[MediaFolderId] [int] NOT NULL IDENTITY(1, 1),
[MediaFolderParentId] [int] NULL,
[Title] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tMediaFolder_MediaFolderParentId_Title] ON [media].[tMediaFolder] ([MediaFolderParentId], [Title]) ON [PRIMARY]

GO
ALTER TABLE [media].[tMediaFolder] ADD CONSTRAINT [PK_tMediaFolder] PRIMARY KEY CLUSTERED  ([MediaFolderId]) ON [PRIMARY]
GO

ALTER TABLE [media].[tMediaFolder] ADD CONSTRAINT [FK_tMediaFolder_tMediaFolder] FOREIGN KEY ([MediaFolderParentId]) REFERENCES [media].[tMediaFolder] ([MediaFolderId])
GO
