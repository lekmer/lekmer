CREATE TABLE [customer].[tCustomerGroupCustomer]
(
[CustomerGroupId] [int] NOT NULL,
[CustomerId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [customer].[tCustomerGroupCustomer] ADD
CONSTRAINT [FK_tCustomerGroupCustomer_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] ADD CONSTRAINT [PK_tCustomerGroupCustomer] PRIMARY KEY CLUSTERED  ([CustomerGroupId], [CustomerId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] ADD CONSTRAINT [FK_tCustomerGroupCustomer_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId])
GO
