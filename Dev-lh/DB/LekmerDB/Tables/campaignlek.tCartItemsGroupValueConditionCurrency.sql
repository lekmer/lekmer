CREATE TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency]
(
[ConditionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency] ADD CONSTRAINT [PK_tCartItemsGroupValueConditionCurrency] PRIMARY KEY CLUSTERED  ([ConditionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency] ADD CONSTRAINT [FK_tCartItemsGroupValueConditionCurrency_tCartItemsGroupValueCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCartItemsGroupValueCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tCartItemsGroupValueConditionCurrency] ADD CONSTRAINT [FK_tCartItemsGroupValueConditionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
