CREATE TABLE [integration].[IconImport]
(
[LekmerId] [int] NOT NULL,
[IconType] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[IconScensumId] [int] NULL
) ON [PRIMARY]
GO
