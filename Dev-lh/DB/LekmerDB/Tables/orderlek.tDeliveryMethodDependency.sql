CREATE TABLE [orderlek].[tDeliveryMethodDependency]
(
[DeliveryMethodDependencyId] [int] NOT NULL IDENTITY(1, 1),
[BaseDeliveryMethodId] [int] NOT NULL,
[OptionalDeliveryMethodId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tDeliveryMethodDependency] ADD CONSTRAINT [PK_tDeliveryMethodDependency] PRIMARY KEY CLUSTERED  ([DeliveryMethodDependencyId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tDeliveryMethodDependency] ADD CONSTRAINT [FK_tDeliveryMethodDependency_tDeliveryMethod_Base] FOREIGN KEY ([DeliveryMethodDependencyId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
ALTER TABLE [orderlek].[tDeliveryMethodDependency] ADD CONSTRAINT [FK_tDeliveryMethodDependency_tDeliveryMethod_Optional] FOREIGN KEY ([OptionalDeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
