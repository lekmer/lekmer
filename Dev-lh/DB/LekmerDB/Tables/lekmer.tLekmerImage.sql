CREATE TABLE [lekmer].[tLekmerImage]
(
[MediaId] [int] NOT NULL,
[Link] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Parameter] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[TumbnailImageUrl] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[HasImage] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerImage] ADD CONSTRAINT [PK_tLekmerImage] PRIMARY KEY CLUSTERED  ([MediaId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerImage] ADD CONSTRAINT [FK_tLekmerImage_tMedia] FOREIGN KEY ([MediaId]) REFERENCES [media].[tMedia] ([MediaId]) ON DELETE CASCADE
GO
