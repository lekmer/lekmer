CREATE TABLE [review].[tBlockBestRatedProductList]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NULL,
[RatingId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockBestRatedProductList] ADD CONSTRAINT [PK_tBlockBestRatedProductList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockBestRatedProductList] ADD CONSTRAINT [FK_tBlockBestRatedProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [review].[tBlockBestRatedProductList] ADD CONSTRAINT [FK_tBlockBestRatedProductList_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [review].[tBlockBestRatedProductList] ADD CONSTRAINT [FK_tBlockBestRatedProductList_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
