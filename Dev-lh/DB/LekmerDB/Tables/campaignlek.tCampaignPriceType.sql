CREATE TABLE [campaignlek].[tCampaignPriceType]
(
[CampaignPriceTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignPriceType] ADD CONSTRAINT [PK_tCampaignPriceType] PRIMARY KEY CLUSTERED  ([CampaignPriceTypeId]) ON [PRIMARY]
GO
