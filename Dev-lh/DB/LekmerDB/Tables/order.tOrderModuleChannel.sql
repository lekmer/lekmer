CREATE TABLE [order].[tOrderModuleChannel]
(
[ChannelId] [int] NOT NULL,
[OrderTemplateContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderModuleChannel] ADD CONSTRAINT [PK_tOrderModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderModuleChannel_OrderTemplateContentNodeId] ON [order].[tOrderModuleChannel] ([OrderTemplateContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderModuleChannel] ADD CONSTRAINT [FK_tOrderModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [order].[tOrderModuleChannel] ADD CONSTRAINT [FK_tOrderModuleChannel_tContentPage] FOREIGN KEY ([OrderTemplateContentNodeId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
