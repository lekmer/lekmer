CREATE TABLE [productlek].[tStockRange]
(
[StockRangeId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StartValue] [int] NULL,
[EndValue] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tStockRange] ADD CONSTRAINT [PK_tStockRange] PRIMARY KEY CLUSTERED  ([StockRangeId]) ON [PRIMARY]
GO
