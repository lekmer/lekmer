CREATE TABLE [productlek].[tSupplier]
(
[SupplierId] [int] NOT NULL IDENTITY(1, 1),
[SupplierNo] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ActivePassiveCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Name] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Address] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[ZipArea] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[City] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CountryISO] [char] (2) COLLATE Finnish_Swedish_CI_AS NULL,
[LanguageISO] [char] (2) COLLATE Finnish_Swedish_CI_AS NULL,
[VATRate] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone1] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Phone2] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL,
[IsDropShip] [bit] NOT NULL CONSTRAINT [DF_tSupplier_IsDropShip] DEFAULT ((0)),
[DropShipEmail] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tSupplier] ADD CONSTRAINT [PK_tSupplier_1] PRIMARY KEY CLUSTERED  ([SupplierId]) ON [PRIMARY]
GO
