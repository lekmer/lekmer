CREATE TABLE [orderlek].[tMaksuturvaPendingOrder]
(
[MaksuturvaPendingOrderId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[StatusId] [int] NOT NULL,
[FirstAttempt] [datetime] NULL,
[LastAttempt] [datetime] NULL,
[NextAttempt] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tMaksuturvaPendingOrder] ADD CONSTRAINT [PK_tMaksuturvaPendingOrder] PRIMARY KEY CLUSTERED  ([MaksuturvaPendingOrderId]) ON [PRIMARY]
GO
