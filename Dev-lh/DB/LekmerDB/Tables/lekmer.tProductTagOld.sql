CREATE TABLE [lekmer].[tProductTagOld]
(
[ProductId] [int] NOT NULL,
[TagId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductTagOld] ADD CONSTRAINT [PK_tProductTagOld] PRIMARY KEY CLUSTERED  ([ProductId], [TagId]) ON [PRIMARY]
GO
