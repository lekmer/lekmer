CREATE TABLE [review].[tRatingReviewFeedback]
(
[RatingReviewFeedbackId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[OrderId] [int] NULL,
[RatingReviewStatusId] [int] NOT NULL,
[LikeHit] [int] NOT NULL,
[Inappropriate] [bit] NULL,
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[RatingReviewUserId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [PK_tRatingReviewFeedback] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [FK_tRatingReviewFeedback_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [FK_tRatingReviewFeedback_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [FK_tRatingReviewFeedback_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [FK_tRatingReviewFeedback_tRatingReviewStatus] FOREIGN KEY ([RatingReviewStatusId]) REFERENCES [review].[tRatingReviewStatus] ([RatingReviewStatusId])
GO
ALTER TABLE [review].[tRatingReviewFeedback] ADD CONSTRAINT [FK_tRatingReviewFeedback_tRatingReviewUser] FOREIGN KEY ([RatingReviewUserId]) REFERENCES [review].[tRatingReviewUser] ([RatingReviewUserId])
GO
