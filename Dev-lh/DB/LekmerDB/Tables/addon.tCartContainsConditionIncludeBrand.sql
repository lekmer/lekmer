CREATE TABLE [addon].[tCartContainsConditionIncludeBrand]
(
[ConditionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeBrand] ADD CONSTRAINT [PK_tCartContainsConditionIncludeBrand] PRIMARY KEY CLUSTERED  ([ConditionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeBrand] ADD CONSTRAINT [FK_tCartContainsConditionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeBrand] ADD CONSTRAINT [FK_tCartContainsConditionIncludeBrand_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId])
GO
