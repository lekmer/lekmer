CREATE TABLE [lekmer].[tProductSimilar]
(
[ProductId] [int] NOT NULL,
[SimilarProductId] [int] NOT NULL,
[Score] [decimal] (18, 6) NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tProductSimilar] ADD
CONSTRAINT [FK_tProductSimilar_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductSimilar] ADD
CONSTRAINT [FK_tProductSimilar_tLekmerProduct1] FOREIGN KEY ([SimilarProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])


GO
ALTER TABLE [lekmer].[tProductSimilar] ADD CONSTRAINT [PK_tProductSimilar] PRIMARY KEY CLUSTERED  ([ProductId], [SimilarProductId]) ON [PRIMARY]
GO
