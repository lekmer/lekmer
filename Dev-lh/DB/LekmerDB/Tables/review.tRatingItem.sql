CREATE TABLE [review].[tRatingItem]
(
[RatingItemId] [int] NOT NULL IDENTITY(1, 1),
[RatingId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[Score] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [review].[tRatingItem] ADD
CONSTRAINT [FK_tRatingItem_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
ALTER TABLE [review].[tRatingItem] ADD CONSTRAINT [PK_tRatingItem] PRIMARY KEY CLUSTERED  ([RatingItemId]) ON [PRIMARY]
GO
