CREATE TABLE [lekmer].[tSizeTableRow]
(
[SizeTableRowId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[Column1Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Column2Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableRow] ADD
CONSTRAINT [FK_tSizeTableRow_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
GO
ALTER TABLE [lekmer].[tSizeTableRow] ADD CONSTRAINT [PK_tSizeTableRow] PRIMARY KEY CLUSTERED  ([SizeTableRowId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableRow] ADD CONSTRAINT [FK_tSizeTableRow_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
