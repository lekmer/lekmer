CREATE TABLE [productlek].[tLekmerCategoryTranslation]
(
[CategoryId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [productlek].[tLekmerCategoryTranslation] ADD CONSTRAINT [PK_tLekmerCategoryTranslation] PRIMARY KEY CLUSTERED  ([CategoryId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tLekmerCategoryTranslation] ADD CONSTRAINT [FK_tLekmerCategoryTranslation_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [productlek].[tLekmerCategoryTranslation] ADD CONSTRAINT [FK_tLekmerCategoryTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
