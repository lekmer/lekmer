CREATE TABLE [lekmer].[tContentMessageIncludeProduct]
(
[ContentMessageId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeProduct] ADD CONSTRAINT [PK_tContentMessageIncludeProduct] PRIMARY KEY CLUSTERED  ([ContentMessageId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeProduct] ADD CONSTRAINT [FK_tContentMessageIncludeProduct_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
GO
ALTER TABLE [lekmer].[tContentMessageIncludeProduct] ADD CONSTRAINT [FK_tContentMessageIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
