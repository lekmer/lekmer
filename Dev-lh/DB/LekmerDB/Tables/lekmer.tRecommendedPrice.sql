CREATE TABLE [lekmer].[tRecommendedPrice]
(
[ProductId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Price] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tRecommendedPrice] ADD CONSTRAINT [PK_tRecommendedPrice] PRIMARY KEY CLUSTERED  ([ProductId], [ChannelId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tRecommendedPrice] ADD CONSTRAINT [FK_tRecommendedPrice_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tRecommendedPrice] ADD CONSTRAINT [FK_tRecommendedPrice_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
