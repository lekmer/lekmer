CREATE TABLE [temp].[CdonLekmerProducts]
(
[ProductId] [int] NULL,
[ErpArticleNumber] [varchar] (64) COLLATE Finnish_Swedish_CI_AS NULL,
[CategoryId] [int] NULL,
[Category] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductStatusId] [int] NULL
) ON [PRIMARY]
GO
