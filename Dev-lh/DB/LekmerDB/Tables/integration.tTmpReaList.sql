CREATE TABLE [integration].[tTmpReaList]
(
[HYId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Rea] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceSE] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceDK] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceNO] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceFI] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
