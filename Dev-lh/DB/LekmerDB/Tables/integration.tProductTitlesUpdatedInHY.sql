CREATE TABLE [integration].[tProductTitlesUpdatedInHY]
(
[ProductId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[LanguageId] [int] NOT NULL,
[InsertedDate] [smalldatetime] NOT NULL,
[ProductExistsInHY] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tProductTitlesUpdatedInHY] ADD 
CONSTRAINT [PK_tProductTitlesUpdatedInHY] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
