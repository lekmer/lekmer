CREATE TABLE [campaignlek].[tPercentageDiscountCartAction]
(
[CartActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tPercentageDiscountCartAction] ADD CONSTRAINT [PK_tPercentageDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tPercentageDiscountCartAction] ADD CONSTRAINT [FK_tPercentageDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
