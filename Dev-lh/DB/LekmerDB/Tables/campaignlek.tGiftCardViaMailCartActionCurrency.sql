CREATE TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency] ADD CONSTRAINT [PK_tGiftCardViaMailCartActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency] ADD CONSTRAINT [FK_tGiftCardViaMailCartActionCurrency_tGiftCardViaMailCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tGiftCardViaMailCartAction] ([CartActionId])
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartActionCurrency] ADD CONSTRAINT [FK_tGiftCardViaMailCartActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
