CREATE TABLE [productlek].[tRecommendationProduct]
(
[RecommendationProductId] [int] NOT NULL IDENTITY(1, 1),
[RecommenderItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tRecommendationProduct] ADD CONSTRAINT [PK_tRecommendationProduct] PRIMARY KEY CLUSTERED  ([RecommendationProductId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRecommendationProduct] ON [productlek].[tRecommendationProduct] ([RecommenderItemId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tRecommendationProduct] ADD CONSTRAINT [FK_tRecommendationProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [productlek].[tRecommendationProduct] ADD CONSTRAINT [FK_tRecommendationProduct_tRecommenderItem] FOREIGN KEY ([RecommenderItemId]) REFERENCES [productlek].[tRecommenderItem] ([RecommenderItemId])
GO
