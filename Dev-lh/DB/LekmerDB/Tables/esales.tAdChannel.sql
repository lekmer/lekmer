CREATE TABLE [esales].[tAdChannel]
(
[AdChannelId] [int] NOT NULL IDENTITY(1, 1),
[AdId] [int] NOT NULL,
[RegistryId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[ShowOnSite] [bit] NOT NULL,
[LandingPageUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[LandingPageId] [int] NULL,
[ImageUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [esales].[tAdChannel] ADD CONSTRAINT [PK_tAdChannel] PRIMARY KEY CLUSTERED  ([AdChannelId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tAdChannel_AdId_RegistryId] ON [esales].[tAdChannel] ([AdId], [RegistryId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tAdChannel] ADD CONSTRAINT [FK_tAdChannel_tAd] FOREIGN KEY ([AdId]) REFERENCES [esales].[tAd] ([AdId])
GO
ALTER TABLE [esales].[tAdChannel] ADD CONSTRAINT [FK_tAdChannel_tEsalesRegistry] FOREIGN KEY ([RegistryId]) REFERENCES [esales].[tEsalesRegistry] ([EsalesRegistryId])
GO
