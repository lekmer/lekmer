CREATE TABLE [temp].[CdonProductsNotExported]
(
[ProductId] [int] NOT NULL,
[ErpArticleNumber] [varchar] (64) COLLATE Finnish_Swedish_CI_AS NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
