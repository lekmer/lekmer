CREATE TABLE [orderlek].[tQliroPendingOrder]
(
[QliroPendingOrderId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[FirstAttempt] [datetime] NOT NULL,
[LastAttempt] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tQliroPendingOrder] ADD CONSTRAINT [PK_tQliroPendingOrder] PRIMARY KEY CLUSTERED  ([QliroPendingOrderId]) ON [PRIMARY]
GO
