CREATE TABLE [lekmer].[tVoucherBatchesExclude]
(
[ConditionId] [int] NOT NULL,
[BatchId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherBatchesExclude] ADD CONSTRAINT [PK_tVoucherBatchesExclude] PRIMARY KEY CLUSTERED  ([ConditionId], [BatchId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherBatchesExclude] ADD CONSTRAINT [FK_tVoucherBatchesExclude_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
