CREATE TABLE [review].[tRatingItemProductVote]
(
[RatingReviewFeedbackId] [int] NOT NULL,
[RatingId] [int] NOT NULL,
[RatingItemId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingItemProductVote] ADD CONSTRAINT [PK_tRatingItemProductVote] PRIMARY KEY CLUSTERED  ([RatingReviewFeedbackId], [RatingId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingItemProductVote] ADD CONSTRAINT [FK_tRatingItemProductVote_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
ALTER TABLE [review].[tRatingItemProductVote] ADD CONSTRAINT [FK_tRatingItemProductVote_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId])
GO
ALTER TABLE [review].[tRatingItemProductVote] ADD CONSTRAINT [FK_tRatingItemProductVote_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId])
GO
