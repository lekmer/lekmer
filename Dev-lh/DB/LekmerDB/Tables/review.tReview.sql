CREATE TABLE [review].[tReview]
(
[ReviewId] [int] NOT NULL IDENTITY(1, 1),
[RatingReviewFeedbackId] [int] NOT NULL,
[AuthorName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [nvarchar] (3000) COLLATE Finnish_Swedish_CI_AS NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tReview] ADD CONSTRAINT [PK_tReview_1] PRIMARY KEY CLUSTERED  ([ReviewId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tReview] ADD CONSTRAINT [FK_tReview_tRatingReviewFeedback] FOREIGN KEY ([RatingReviewFeedbackId]) REFERENCES [review].[tRatingReviewFeedback] ([RatingReviewFeedbackId])
GO
