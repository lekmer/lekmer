CREATE TABLE [campaignlek].[tFixedDiscountCartAction]
(
[CartActionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartAction] ADD CONSTRAINT [PK_tFixedDiscountCartAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountCartAction] ADD CONSTRAINT [FK_tFixedDiscountCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
