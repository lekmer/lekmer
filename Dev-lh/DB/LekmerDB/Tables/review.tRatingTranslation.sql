CREATE TABLE [review].[tRatingTranslation]
(
[RatingId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [review].[tRatingTranslation] ADD
CONSTRAINT [FK_tRatingTranslation_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
ALTER TABLE [review].[tRatingTranslation] ADD CONSTRAINT [PK_tRatingInfoTranslation] PRIMARY KEY CLUSTERED  ([RatingId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingTranslation] ADD CONSTRAINT [FK_tRatingInfoTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
