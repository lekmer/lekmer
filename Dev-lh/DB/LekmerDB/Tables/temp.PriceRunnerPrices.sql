CREATE TABLE [temp].[PriceRunnerPrices]
(
[country] [varchar] (5) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[productId] [int] NOT NULL,
[price] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
