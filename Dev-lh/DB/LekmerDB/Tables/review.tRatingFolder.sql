CREATE TABLE [review].[tRatingFolder]
(
[RatingFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentRatingFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingFolder] ADD CONSTRAINT [PK_tRatingFolder] PRIMARY KEY CLUSTERED  ([RatingFolderId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingFolder] ADD CONSTRAINT [FK_tRatingFolder_tRatingFolder] FOREIGN KEY ([ParentRatingFolderId]) REFERENCES [review].[tRatingFolder] ([RatingFolderId])
GO
