CREATE TABLE [orderlek].[tCollectorPaymentType]
(
[CollectorPaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Code] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[StartFee] [decimal] (16, 2) NOT NULL,
[InvoiceFee] [decimal] (16, 2) NOT NULL,
[MonthlyInterestRate] [decimal] (16, 2) NOT NULL,
[NoOfMonths] [int] NOT NULL,
[MinPurchaseAmount] [decimal] (16, 2) NOT NULL,
[LowestPayment] [decimal] (16, 2) NOT NULL,
[PartPaymentType] [int] NOT NULL,
[InvoiceType] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [orderlek].[tCollectorPaymentType] ADD 
CONSTRAINT [PK_tCollectorPaymentType] PRIMARY KEY CLUSTERED  ([CollectorPaymentTypeId]) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tCollectorPaymentType_ChannelId_Code] ON [orderlek].[tCollectorPaymentType] ([ChannelId], [Code]) ON [PRIMARY]

ALTER TABLE [orderlek].[tCollectorPaymentType] ADD
CONSTRAINT [FK_tCollectorPaymentType_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])




GO
