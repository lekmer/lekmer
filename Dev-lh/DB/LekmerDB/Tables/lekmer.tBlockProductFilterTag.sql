CREATE TABLE [lekmer].[tBlockProductFilterTag]
(
[BlockId] [int] NOT NULL,
[TagId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductFilterTag] ADD CONSTRAINT [PK_tBlockProductFilterTag] PRIMARY KEY CLUSTERED  ([BlockId], [TagId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductFilterTag] ADD CONSTRAINT [FK_tBlockProductFilterTag_tBlockProductFilter] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockProductFilter] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockProductFilterTag] ADD CONSTRAINT [FK_tBlockProductFilterTag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
