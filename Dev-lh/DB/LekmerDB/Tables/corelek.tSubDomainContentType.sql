CREATE TABLE [corelek].[tSubDomainContentType]
(
[SubDomainContentTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [corelek].[tSubDomainContentType] ADD CONSTRAINT [PK_tSubDomainContentType] PRIMARY KEY CLUSTERED  ([SubDomainContentTypeId]) ON [PRIMARY]
GO
