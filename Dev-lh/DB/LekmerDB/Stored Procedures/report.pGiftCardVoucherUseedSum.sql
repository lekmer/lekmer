SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [report].[pGiftCardVoucherUseedSum]
	@DateFrom	DATETIME = NULL,
	@DateTo		DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	IF @DateFrom IS NULL
		SET @DateFrom = CONVERT(DATE, DATEADD(DAY, -1, @CurrentDate))
	ELSE
		SET @DateFrom = CONVERT(DATE, @DateFrom)

	IF @DateTo IS NULL
		SET @DateTo = CONVERT(DATE, @CurrentDate)
	ELSE
		SET @DateTo = CONVERT(DATE, @DateTo)

	SELECT
		[ch].[CommonName] 'Channel',
		SUM([vi].[AmountUsed]) 'Total sum of used vouchers'
	FROM
		[orderlek].[tOrderVoucherInfo] vi
		INNER JOIN [campaignlek].[tGiftCardViaMailInfo] gc ON [gc].[VoucherCode] = [vi].[VoucherCode] COLLATE Finnish_Swedish_CI_AS
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [vi].[OrderId]
		INNER JOIN [core].[tChannel] ch ON [ch].[ChannelId] = [o].[ChannelId]
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] >= @DateFrom
		AND [o].[CreatedDate] < @DateTo
		AND [vi].[DiscountType] = 3
		AND [vi].[VoucherStatus] = 3
		AND [vi].[AmountUsed] IS NOT NULL
	GROUP BY
		[ch].[CommonName]
	ORDER BY
		[ch].[CommonName]
END
GO
