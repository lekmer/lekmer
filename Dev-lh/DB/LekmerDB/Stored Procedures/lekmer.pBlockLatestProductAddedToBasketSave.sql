SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure  [lekmer].[pBlockLatestProductAddedToBasketSave]
	@BlockId int,
	@RedirectToCategory bit,
	@RedirectToMainCategory bit,
	@RedirectToStartPage bit
as
begin
	update lekmer.[tBlockLatestProductAddedToBasket]
	set
		RedirectToCategory = @RedirectToCategory,
		RedirectToMainCategory = @RedirectToMainCategory,
		RedirectToStartPage = @RedirectToStartPage
	where
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert lekmer.[tBlockLatestProductAddedToBasket]
	(
		BlockId,
		RedirectToCategory,
		RedirectToMainCategory,
		RedirectToStartPage
	)
	values
	(
		@BlockId,
		@RedirectToCategory,
		@RedirectToMainCategory,
		@RedirectToStartPage
	)
end
GO
