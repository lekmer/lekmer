SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pTopListBrandGetIdAllByBlock]
	@ChannelId				INT,
	@BlockId				INT,
	@IncludeAllCategories	BIT,
	@CategoryIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@CountOrdersFrom		DATETIME,
	@From					INT,
	@To						INT
AS
BEGIN
	DECLARE @sql AS NVARCHAR(MAX)
	DECLARE @sqlCount AS NVARCHAR(MAX)
	DECLARE @sqlFragment AS NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT
			ROW_NUMBER() OVER (ORDER BY SUM(oi.[OrderItem.Quantity]) DESC) AS Number,
			b.[Brand.BrandId] AS BrandId
		FROM
			[lekmer].[vBrand] b
			INNER JOIN [product].[vCustomProduct] p ON p.[Lekmer.BrandId] = b.[Brand.BrandId]
			INNER JOIN [order].[vCustomOrderItem] oi WITH(NOLOCK) ON oi.[OrderItem.ProductId] = p.[Product.Id]
			INNER JOIN [order].[vCustomOrder] o WITH(NOLOCK) ON oi.[OrderItem.OrderId] = o.[Order.OrderId]
		WHERE
			p.[Product.ChannelId] = @ChannelId
			AND o.[Order.CreatedDate] > @CountOrdersFrom
	'
	
	IF @IncludeAllCategories = 0
	BEGIN
		SET @sqlFragment += '
			AND (
				p.[Product.CategoryId] in (
					SELECT
						[Id]
					FROM
						[generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)
				)
			)
	'
	END
	
	SET @sqlFragment += '
			AND NOT EXISTS (
				SELECT *
				FROM
					[lekmer].[vBlockBrandTopListBrand] fb
				where
					fb.[BlockBrandTopListBrand.BlockId] = @BlockId
					AND fb.[BlockBrandTopListBrand.BrandId] = b.[Brand.BrandId]
			)
		GROUP BY
			b.[Brand.BrandId]
	'

	SET @sqlCount = '
		SELECT COUNT(*)
		FROM
			(' + @sqlFragment + ') AS CountResults'

	SET @sql = '
		SELECT [BrandId]
		FROM
			(' + @sqlFragment + ') AS SearchResult
		WHERE
			[Number] BETWEEN @From AND @To'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME,
			@From					INT,
			@To						INT',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To
END
GO
