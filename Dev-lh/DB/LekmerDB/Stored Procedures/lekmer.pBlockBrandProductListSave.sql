
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandProductListSave]
	@BlockId			INT,
	@ProductSortOrderId	INT
AS
BEGIN
	UPDATE
		[lekmer].[tBlockBrandProductList]
	SET
		[ProductSortOrderId]	= @ProductSortOrderId
	WHERE
		[BlockId]		= @BlockId
			
	IF  @@ROWCOUNT = 0 
		BEGIN
			INSERT [lekmer].[tBlockBrandProductList] (
				[BlockId],
				[ProductSortOrderId]
			)
			VALUES (
				@BlockId,
				@ProductSortOrderId
			)
		END
END
GO
