SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockEsalesProductFilterSave]
	@BlockId INT,
	@SecondaryTemplateId INT = NULL,
	@DefaultCategoryId INT = NULL,
	@DefaultPriceIntervalId INT = null,
	@DefaultAgeIntervalId INT = NULL,
	@DefaultSizeId INT = NULL,
	@DefaultSort NVARCHAR(50) = NULL
AS
BEGIN

	UPDATE
		lekmer.[tBlockProductFilter]
	SET
		SecondaryTemplateId	= @SecondaryTemplateId,
		DefaultCategoryId = @DefaultCategoryId,
		DefaultPriceIntervalId = @DefaultPriceIntervalId,
		DefaultAgeIntervalId = @DefaultAgeIntervalId,
		DefaultSizeId = @DefaultSizeId,
		DefaultSort = @DefaultSort
	WHERE
		[BlockId] = @BlockId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			lekmer.[tBlockProductFilter]
		(
			[BlockId],
			SecondaryTemplateId,
			DefaultCategoryId,
			DefaultPriceIntervalId,
			DefaultAgeIntervalId,
			DefaultSizeId,
			DefaultSort
		)
		VALUES
		(
			@BlockId,
			@SecondaryTemplateId,
			@DefaultCategoryId,
			@DefaultPriceIntervalId,
			@DefaultAgeIntervalId,
			@DefaultSizeId,
			@DefaultSort
		)
	END
END
GO
