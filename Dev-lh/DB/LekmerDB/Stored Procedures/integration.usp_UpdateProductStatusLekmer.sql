
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateProductStatusLekmer]
AS
BEGIN

	/*
	When
		product is online
		product is passive
		stock is 0
	Then
		set product offline
		remove tag 'sale'
		remove tag 'outlet'
	*/

	SET NOCOUNT ON
	
	DECLARE @ProductId INT
	DECLARE @NumberInStock INT
	DECLARE @StockStatusId INT
	
	DECLARE @OfflineId INT
	SET @OfflineId = (SELECT [ProductStatusId] FROM [product].[tProductStatus] WHERE [CommonName] = 'Offline')
	
	DECLARE @PassiveId INT
	SET @PassiveId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [CommonName] = 'Passive')
	
	DECLARE @SalesTagId INT
	SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'sale')
	
	DECLARE @OutletTagId INT
	SET @OutletTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'outlet')

	DECLARE cur_product CURSOR FAST_FORWARD FOR
	SELECT 
		[lp].[ProductId],
		[p].[NumberInStock], 
		[lp].[StockStatusId]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
	WHERE
		[lp].[ProductTypeId] = 1
		AND
		p.[ProductStatusId] = 0 -- Online

	OPEN cur_product

	FETCH NEXT FROM cur_product 
	INTO @ProductId,
		 @NumberInStock,
		 @StockStatusId

	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				-- no sizes
				-- set offline
				-- remove sale tag
				
				IF (@NumberInStock <= 0 AND @StockStatusId = @PassiveId)
				BEGIN 
					UPDATE [product].[tProduct]
					SET [ProductStatusId] = @OfflineId
					WHERE [ProductId] = @ProductId
					
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @OutletTagId, NULL
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @SalesTagId, NULL
					
					DELETE FROM lekmer.tProductTag
					WHERE [ProductId] = @ProductId
						  AND TagId IN (@SalesTagId, @OutletTagId)
				END
			END
		ELSE
			BEGIN
				-- product with sizes
				DECLARE 
				@NewFromDate DATETIME,
				@NewToDate DATETIME

				SET @NewFromDate = CONVERT(DATE, GETDATE())
				SET @NewToDate   = DATEADD(WEEK, 4, @NewFromDate)

				--update new products depending on stock 
				UPDATE 
				lp
				SET 
					lp.IsNewFrom = @NewFromDate,
					lp.IsNewTo  = @NewToDate, 
					lp.IsNew = 0
				FROM 
					[integration].tempProduct tp
					INNER JOIN [lekmer].tLekmerProduct lp ON lp.HYErpId = SUBSTRING(tp.HYarticleId, 5,12)
					INNER JOIN product.tProduct p ON p.ProductId = lp.ProductId
				WHERE 
				SUBSTRING(tp.HYarticleId, 3,1) = 1 
				AND(
					lp.IsNew = 1
				)
				AND (
					p.NumberInStock = 0
				)
				AND (
					(SELECT SUM([ps].[NumberInStock])
					FROM [lekmer].[tProductSize] ps
					WHERE [ps].[ProductId] = @ProductId AND [ps].[NumberInStock] >= 0
					) >0
				)
				
				IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND ([StockStatusId] != @PassiveId OR [NumberInStock] > 0))
				BEGIN 
					-- all sizes are passive
					-- set offline
					-- remove sale tag
				
					UPDATE [product].[tProduct]
					SET [ProductStatusId] = @OfflineId
					WHERE [ProductId] = @ProductId
					
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @OutletTagId, NULL
					EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @SalesTagId, NULL
					
					DELETE FROM lekmer.tProductTag
					WHERE [ProductId] = @ProductId
						  AND TagId IN (@SalesTagId, @OutletTagId)
				END
				
				IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND [StockStatusId] != @PassiveId)
				BEGIN
					-- set passive
					UPDATE [lekmer].[tLekmerProduct]
					SET [StockStatusId] = @PassiveId
					WHERE [ProductId] = @ProductId
				END

				-- update number in stock on product level
				UPDATE [product].[tProduct]
				SET [NumberInStock] = (
										SELECT SUM([ps].[NumberInStock])
										FROM [lekmer].[tProductSize] ps
										WHERE [ps].[ProductId] = @ProductId AND [ps].[NumberInStock] >= 0
									  )
				WHERE [ProductId] = @ProductId
			END
		
		FETCH NEXT FROM cur_product 
		INTO @ProductId,
			 @NumberInStock,
			 @StockStatusId
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
