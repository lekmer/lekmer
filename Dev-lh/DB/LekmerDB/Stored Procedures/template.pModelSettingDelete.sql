SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pModelSettingDelete]
    @ModelSettingId INT
AS 
BEGIN
    SET NOCOUNT ON     
        
--    DELETE  [template].[tTemplateSetting]
--    WHERE   [ModelSettingId] = @ModelSettingId
    
    DELETE  [template].[tModelSetting]
    WHERE   [ModelSettingId] = @ModelSettingId

    RETURN 0

END
GO
