
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [integration].[pCampaignInfoToCdon]
as
begin
	set nocount on
	
	set transaction isolation level read uncommitted
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		

	if object_id('tempdb..#Campaign') is not null
		 drop table #Campaign

	create table #Campaign
	(
		CampaignId int,
		PriceListId int primary key (CampaignId, PriceListId) with (ignore_dup_key = on)
	)

	insert into #Campaign ( CampaignId, PriceListId )
	select c.CampaignId, [crc].[CampaignRegistryId]
	from campaign.tCampaign c
	INNER JOIN [campaignlek].[tCampaignRegistryCampaign] crc ON [crc].[CampaignId] = [c].[CampaignId]
	where c.CampaignStatusId = 0
		and (c.StartDate is null or c.StartDate <= getdate())
		and (c.EndDate is null or getdate() <= c.EndDate)



	if object_id('tempdb..#CampaignAction') is not null
		 drop table #CampaignAction

	create table #CampaignAction (
		CampaignId int,
		SortIndex int,   
		ProductActionId int,
		Ordinal int
	)

	declare @Index int
	set @Index = 1

	insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
	select DISTINCT pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
	from #Campaign c
		inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
		left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
			and pa2.Ordinal < pa.Ordinal
	where pa2.ProductActionId is null

	while (@@ROWCOUNT > 0)
	begin
		set @Index = @Index + 1
	      
		insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
		SELECT DISTINCT pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
		from #Campaign c      
			inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
				and tca.SortIndex = @Index - 1
			inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
				and pa.Ordinal > tca.Ordinal
			left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
				and pa2.Ordinal > tca.Ordinal
				and pa2.Ordinal < pa.Ordinal
		where pa2.ProductActionId is null
		option (force order)
	end

	if object_id('tempdb..#CampaignProduct') is not null
		 drop table #CampaignProduct

	create table #CampaignProduct (
		CampaignId int,
		ProductId int,
		Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
	)

	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, ip.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [ca].[ProductActionId]
		inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
		
	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, p.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
					INNER JOIN product.tProduct p ON p.CategoryId = ic.CategoryId

	insert into #CampaignProduct ( CampaignId, ProductId, Price )
	select c.CampaignId, pdai.ProductId, null
	from #Campaign c
		inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
		inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
		inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId

	update cp set Price = pli.PriceIncludingVat
	from #Campaign c
		inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
		inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
			and pli.ProductId = cp.ProductId

	declare @LastIndex int
	set @Index = 1
	select @LastIndex = max(SortIndex)
	from #CampaignAction

	while (@Index <= @LastIndex)
	begin   
		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
			CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionCategories] ([ca].[ProductActionId], 1) ic
						inner join product.tProduct p on p.CategoryId = ic.CategoryId
						inner join #CampaignProduct cp on cp.ProductId = p.ProductId and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index

		update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1
			inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
			inner join [campaignlek].[tCampaignActionIncludeProduct] ip on ip.[ConfigId] = [ppda].[ConfigId]
			inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
				and cp.CampaignId = ca.CampaignId
		where ca.SortIndex = @Index
	   
		update cp set Price = pdai.DiscountPrice
		-- select cp.Price, pdai.DiscountPrice
		from #CampaignAction ca
			inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
				and pa.ProductActionTypeId = 1000001
			inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
			inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
				and cp.CampaignId = ca.CampaignId
			inner join #Campaign c on c.CampaignId = cp.CampaignId
			inner join product.tPriceList pl on pl.PriceListId = c.PriceListId and pl.CurrencyId = pdai.CurrencyId
		where ca.SortIndex = @Index
			and (cp.Price > pdai.DiscountPrice or cp.Price is null)

		set @Index = @Index + 1
	end

	delete cp
	from #CampaignProduct cp 
	where Price is null

	update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
	from #CampaignProduct cp
		inner join #Campaign c on c.CampaignId = cp.CampaignId
	   

	if object_id('tempdb..#BestCampaignPrice') is not null
		 drop table #BestCampaignPrice

	create table #BestCampaignPrice (
		ProductId int,
		PriceListId int primary key (ProductId, PriceListId) with (ignore_dup_key = on),
		CampaignId int,
		Price decimal(16, 2)
	)
	insert into #BestCampaignPrice (ProductId, PriceListId, CampaignId, Price)
	select cp.ProductId, c.PriceListId, cp.CampaignId, cp.Price
	from #CampaignProduct cp
		inner join #Campaign c on c.CampaignId = cp.CampaignId
	order by cp.ProductId, c.PriceListId, cp.Price	

	drop table #Campaign
	drop table #CampaignAction
	drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	
	select case when (p.ProductStatusId = 0) then 'Online' else 'Offline' end as Status,
		p.Title as Product,
--		p.ErpId,
		lp.HyErpId AS ErpId,
		ca.ErpId as CategoryErpId,
		ca.Title as Category,
		p.NumberInStock,
		pl.CommonName as PriceList, 
	   pli.PriceIncludingVat as PriceListPrice, 
	   isnull(pld.Price, pli.PriceIncludingVat) as Price,
	   pld.Price as DiscountPrice, 
	   pli.PriceIncludingVat - pld.Price as Discount,
	   convert(int, round(convert(money, pli.PriceIncludingVat - pld.Price) * 100 / pli.PriceIncludingVat, 0)) as DiscountPercentage,
	   c.Title as Campaign,
	   c.StartDate as CampaignStart,
	   c.EndDate as CampaignEnd
	--into temp.CdonPriceNewCalc3
	from @PriceList tpl
		inner join product.tPriceList pl on pl.PriceListId = tpl.PriceListId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = pli.ProductId
		left outer join campaign.tCampaign c on c.CampaignId = pld.CampaignId
		inner join product.tProduct p on p.ProductId = pli.ProductId
      inner join lekmer.tLekmerProduct lp on lp.ProductId = pli.ProductId
      inner join product.tCategory ca on ca.CategoryId = p.CategoryId            

	
	drop table #BestCampaignPrice
		
  -- select pi.ProductId 
		--  ,lp.HyErpId AS ErpId
		--  ,pi.MediaId
		--  ,pi.ProductImageGroupId
		--  ,mf.Extension
	 --from @AllProductImages pi
	 --     inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	 --     inner join media.tMedia m on pi.MediaId = m.MediaId
		--   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
end
GO
