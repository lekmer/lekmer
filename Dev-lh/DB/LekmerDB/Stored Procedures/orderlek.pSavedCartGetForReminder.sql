
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pSavedCartGetForReminder] 
	@SavedCartReminderInDays	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CurrentDate AS DATETIME 
	SET @CurrentDate = GETDATE()
	
	DECLARE @Date AS DATETIME
	SET @Date = DATEADD(DAY, @SavedCartReminderInDays*(-1), @CurrentDate)
	
	SELECT	
		[sc].*,
		[cc].[Cart.CartGuid] AS 'CartGuid'
	FROM
		[orderlek].[tSavedCart] sc
		INNER JOIN [order].[vCustomCart] cc ON [cc].[Cart.CartId] = [sc].[CartId]
	WHERE
		[sc].[IsReminderSent] = 0
		AND [sc].[IsNeedReminder] = 1
		AND	NOT EXISTS (SELECT 1 FROM [lekmer].[tNewsletterUnsubscriber] nu WHERE [nu].[Email] = [sc].[Email])
		AND EXISTS (SELECT 1 FROM [order].[vCartItem] ci WHERE [CartItem.CartId] = [sc].[CartId])
		AND [cc].[Cart.CreatedDate] < @Date
END
GO
