SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pFixedDiscountCartActionCurrencyDeleteAll] @CartActionId

	DELETE FROM [campaignlek].[tFixedDiscountCartAction]
	WHERE CartActionId = @CartActionId
END
GO
