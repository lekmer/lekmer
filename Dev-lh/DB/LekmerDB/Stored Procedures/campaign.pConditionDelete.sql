SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pConditionDelete]
	@ConditionId int
AS 
BEGIN
	DELETE FROM campaign.tCondition
	WHERE ConditionId = @ConditionId
END
GO
