SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductRegistryProductGetRestrictionsByProductId]
	@ProductId INT
AS
BEGIN
	SELECT
		[pr].[ProductRegistryId]
	FROM
		[product].[tProductRegistry] pr
	WHERE NOT EXISTS (SELECT 1 FROM [product].[tProductRegistryProduct] prp WHERE [prp].[ProductRegistryId] = [pr].[ProductRegistryId] AND [prp].[ProductId] = @ProductId)
END
GO
