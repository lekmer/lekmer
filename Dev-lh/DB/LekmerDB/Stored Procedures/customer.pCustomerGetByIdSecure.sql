
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetByIdSecure] 
	@CustomerId INT
AS	
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[customer].[vCustomCustomerSecure]
	WHERE
		[Customer.CustomerId] = @CustomerId
END
GO
