SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [corelek].[pCacheUpdateDeleteOld]
	@ToDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[corelek].[tCacheUpdate]
	WHERE
		[CreationDate] < @ToDate
END
GO
