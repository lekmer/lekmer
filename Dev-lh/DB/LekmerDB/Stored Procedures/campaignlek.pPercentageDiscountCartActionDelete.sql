SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pPercentageDiscountCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [campaignlek].[tPercentageDiscountCartAction]
	WHERE CartActionId = @CartActionId
END
GO
