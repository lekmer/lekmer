SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllIdsByCategory]
	@CategoryId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		DISTINCT [pt].[TagId]
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tProductTag] pt ON [p].[ProductId] = [pt].[ProductId]
	WHERE
		[p].[CategoryId] = @CategoryId
END
GO
