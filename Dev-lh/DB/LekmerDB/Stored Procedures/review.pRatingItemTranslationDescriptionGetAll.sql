
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemTranslationDescriptionGetAll]
	@RatingItemId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rit.[RatingItemTranslation.RatingItemId] AS 'Id',
		rit.[RatingItemTranslation.LanguageId] AS 'LanguageId',
		rit.[RatingItemTranslation.Description] AS 'Value'
	FROM
	    [review].[vRatingItemTranslation] rit
	WHERE 
		rit.[RatingItemTranslation.RatingItemId] = @RatingItemId
	ORDER BY
		rit.[RatingItemTranslation.LanguageId]
END
GO
