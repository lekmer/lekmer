SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pCustomersForLast6MonthsCount]
	@ChannelId	INT
AS
BEGIN
	-- Number of active customers (<6 month)

	DECLARE @Date DATETIME
	SET @Date = DATEADD(MONTH, -6, GETDATE())

	SELECT
		COUNT(DISTINCT [o].[Email]) 'Customers' 
	FROM
		[order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > @Date
		AND [o].[ChannelId] = @ChannelId
END
GO
