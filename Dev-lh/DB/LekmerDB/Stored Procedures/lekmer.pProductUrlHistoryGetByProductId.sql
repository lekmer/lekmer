SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryGetByProductId]
	@ProductId	INT
AS 
BEGIN 
	SELECT 
		CPUH.*
	FROM 
		[lekmer].[vCustomProductUrlHistory] CPUH
	WHERE
		CPUH.[ProductUrlHistory.ProductId] = @ProductId
END
GO
