
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSettingSave]
	@BlockId				INT,
	@ColumnCount			INT,
	@ColumnCountMobile		INT = NULL,
	@RowCount				INT,
	@RowCountMobile			INT = NULL,
	@TotalItemCount			INT,
	@TotalItemCountMobile	INT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[sitestructurelek].[tBlockSetting]
	SET	
		 [ColumnCount]			= @ColumnCount
		,[ColumnCountMobile]	= @ColumnCountMobile
		,[RowCount]				= @RowCount
		,[RowCountMobile]		= @RowCountMobile
		,[TotalItemCount]		= @TotalItemCount
		,[TotalItemCountMobile]	= @TotalItemCountMobile
	WHERE
		[BlockId] = @BlockId
  
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[sitestructurelek].[tBlockSetting] (
			 [BlockId]
			,[ColumnCount]
			,[ColumnCountMobile]
			,[RowCount]
			,[RowCountMobile]
			,[TotalItemCount]
			,[TotalItemCountMobile]
		)
		VALUES (
			 @BlockId
			,@ColumnCount
			,@ColumnCountMobile
			,@RowCount
			,@RowCountMobile
			,@TotalItemCount
			,@TotalItemCountMobile
		)
	END
END
GO
