SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByIdSecure]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[st].*
	FROM
		[lekmer].[vSizeTableSecure] st
	WHERE
		[st].[SizeTable.SizeTableId] = @SizeTableId
END
GO
