SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionCurrencyDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tGiftCardViaMailCartActionCurrency]
	WHERE
		CartActionId = @CartActionId
END
GO
