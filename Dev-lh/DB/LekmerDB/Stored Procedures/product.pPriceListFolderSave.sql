SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pPriceListFolderSave]
	@PriceListFolderId			int,
	@ParentPriceListFolderId	int,
	@Title						nvarchar(50)
AS
BEGIN
	IF EXISTS(
				SELECT 
					1 
				FROM 
					product.tPriceListFolder 
				WHERE 
					ISNULL(ParentPriceListFolderId, 0) = ISNULL(@ParentPriceListFolderId, 0) 
					AND Title = @Title AND PriceListFolderId <> @PriceListFolderId
				)
		RETURN -1
	ELSE
	BEGIN
			UPDATE 
				product.tPriceListFolder
			SET 
				ParentPriceListFolderId = @ParentPriceListFolderId,
				Title = @Title
			WHERE
				PriceListFolderId = @PriceListFolderId
				
			IF @@ROWCOUNT = 0
			BEGIN
				INSERT INTO product.tPriceListFolder
				(
					ParentPriceListFolderId,
					Title
				)
				VALUES
				(
					@ParentPriceListFolderId,
					@Title
				)
				SET @PriceListFolderId = SCOPE_IDENTITY()					
			END			
			RETURN @PriceListFolderId
		END
END





GO
