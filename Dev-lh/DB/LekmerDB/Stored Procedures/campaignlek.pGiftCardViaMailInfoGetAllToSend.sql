
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoGetAllToSend]
AS
BEGIN
	SELECT 
		gcvmi.*
	FROM
		[campaignlek].[vGiftCardViaMailInfo] gcvmi
		INNER JOIN [order].[tOrder] o ON o.[OrderId] = gcvmi.[GiftCardViaMailInfo.OrderId]
	WHERE
		o.[OrderStatusId] = 4 -- OrderInHY
		  AND gcvmi.[GiftCardViaMailInfo.StatusId] = 1 -- ReadyToSend
		  AND gcvmi.[GiftCardViaMailInfo.DateToSend] < GETDATE()
END
GO
