SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionProductDeleteAllByProduct]
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		rp
	FROM
		[export].[tCdonExportRestrictionProduct] rp
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) AS pl ON pl.[ID] = [rp].[ProductId]
END
GO
