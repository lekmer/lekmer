
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_Delete]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON
	
	-- Remove Options
	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	FROM
		[lekmer].[tNewsletterUnsubscriberOption]
	WHERE
		[UnsubscriberId] = @Id
	
	DELETE
		[lekmer].[tNewsletterUnsubscriber]
	WHERE
		[UnsubscriberId] = @Id
		
	RETURN @@ROWCOUNT
END
GO
