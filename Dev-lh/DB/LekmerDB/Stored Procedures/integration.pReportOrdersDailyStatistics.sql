
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [integration].[pReportOrdersDailyStatistics]
	@Date smalldatetime = null,
	-- @Site varchar(50) = null,
	@ChannelId int = null
	-- @Debug bit = 0
as
begin
	set nocount on
	declare @Debug bit
	set @Debug = 0
	
	create table #tChannel
	(
		ChannelId	int not null
	)
	
	if @ChannelId is not null and @ChannelId > 0
	begin
		--print 'single channel'
		insert into #tChannel (ChannelId) values (@ChannelId)
	end
	else if @ChannelId = 0
	begin
		--print 'lekmer'
		insert into #tChannel (ChannelId) values (1)
		insert into #tChannel (ChannelId) values (2)
		insert into #tChannel (ChannelId) values (3)
		insert into #tChannel (ChannelId) values (4)
		insert into #tChannel (ChannelId) values (1000005)
	end	


	if @Date is null
		set @Date = getdate()
	
	-- Table variable that will be used as the result set.
	declare @tOrderStatsDay table
	(
		Hour int,
		NbrOfOrders int null,
		NbrOfOrdersAccumulated int null,
		NbrOfOrdersTransferredToERP int null,
		NbrOfOrdersPrevYearTransferredToERP int null,
		NbrOfOrdersCancelled int null,
		--NbrOfOrdersAborted int null,
		NbrOfOrdersFailed int null,
		NbrOfOrdersPayFailed int null,
		--NbrOfOrdersOverflow int null,
		--NbrOfOrdersAwaitingPayAuth int null,
		--NbrOfOrdersTempCustomer int null,
		--NbrOfOrdersDiff int null,
		NbrOfOrdersPer15Min nvarchar(22) null,
		NbrOfVisitors int null
		--NbrOfOrdersAlternateTemplate int null,
		--NbrOfVisitorsAlternateTemplate int null
	)
	
	declare
	      @DateHour smalldatetime,
			@Hour nvarchar(4),
			@Count int,
			@DateText varchar(8),
			@DatePart smalldatetime,
			@DatePartTo smalldatetime,
			@NbrOfOrders int,
			@NbrOfOrdersAlt int,
			@NbrOfOrdersAccumulated int,
			@Q1 int,
			@Q2 int,
			@Q3 int,
			@Q4 int,
			@NbrOfVisitors int,
			@NbrOfVisitorsAlt int
			
	if @Debug = 1
	begin
		declare @tDbg table (DbgId int primary key identity(1, 1), Comment nvarchar(50), StartT datetime, EndT datetime)
		insert into @tDbg (Comment, StartT) values ('Complete run.', getdate())
	end
	
	-- Fetch only the year-month-date part of the parameter.
	set @DateText = convert(varchar(8), @Date, 112)
	set @DatePart = cast(@DateText as smalldatetime)
	set @DatePartTo = dateadd(d, 1, @DatePart)

	-- A temporary table that will contain the orders for the specified day (faster than scanning full order table several times).
	create table #tOrdersDay
	(
		OrderId int primary key,
		IsAlternateTemplate bit,
		OrderStatusId int,
		OrderDateTime smalldatetime,
		IsTemporaryOfCustomerId int
	)
	
	-- Orders in ERP same date last year.
	create table #tOrdersDayInERPPrevYear
	(
	   OrderId int primary key,
	   OrderStatusId int,
	   OrderDateTime smalldatetime
	)
	
	-- Fetch necessary information for all orders for the specified date.
	if @Debug = 1
		insert into @tDbg (Comment, StartT) values ('Orders', getdate())
	insert into #tOrdersDay
	(
		OrderId,
		IsAlternateTemplate,
		OrderStatusId,
		OrderDateTime,
		IsTemporaryOfCustomerId
	)
	select
		o.OrderId,
		null, -- o.IsAlternateTemplate,
		o.OrderStatusId,
		o.CreatedDate,
		0 --isnull(c.CustomerId, 0) CustomerId
	from
		[order].tOrder o
		left join customer.tCustomerInformation c
			on o.CustomerId = c.CustomerId
	where
		o.ChannelId in (select ChannelId from #tChannel)
	and o.CreatedDate >= @DatePart
	and o.CreatedDate < @DatePartTo
	if @Debug = 1
		update @tDbg set EndT = getdate() where Comment = 'Orders'

	if @Debug = 1
		insert into @tDbg (Comment, StartT) values ('Orders In ERP Last Year.', getdate())
	insert into #tOrdersDayInERPPrevYear
	(
	   OrderId,
	   OrderStatusId,
	   OrderDateTime
	)
	select
	   OrderId,
	   OrderStatusId,
	   CreatedDate
	from
		[order].tOrder
	where
		ChannelId in (select ChannelId from #tChannel)
	and CreatedDate >= dateadd(week, -52, @DatePart) -- JEE
	and CreatedDate < dateadd(week, -52, @DatePartTo)
	and OrderStatusId in (2, 4)-- (2, 7, 8)

print @DatePart
print @DatePartTo

	if @Debug = 1
		update @tDbg set EndT = getdate() where Comment = 'Orders In ERP Last Year.'
	
	set @Count = 0
	set @NbrOfOrdersAccumulated = 0
	
	while @Count < 24
	begin
		set @Hour = cast(@Count as nvarchar) + ':0'
		set @DateHour = @DatePart + @Hour
		
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Q1.', getdate())
		-- Total nbr of orders, per hour and per 15 minutes.
		set @Q1 = 
		(
			select count(OrderId)
			from #tOrdersDay
			where OrderDateTime >= @DateHour
				and OrderDateTime < dateadd(n, 15, @DateHour)
		)
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Q1.'
		
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Q2.', getdate())
		set @Q2 =
		(
			select count(OrderId)
			from #tOrdersDay
			where OrderDateTime >= dateadd(n, 15, @DateHour)
				and OrderDateTime < dateadd(n, 30, @DateHour)
		)
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Q2.'
		
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Q3.', getdate())
		set @Q3 =
		(
			select count(OrderId)
			from #tOrdersDay
			where OrderDateTime >= dateadd(n, 30, @DateHour)
				and OrderDateTime < dateadd(n, 45, @DateHour)
		)
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Q3.'
		
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Q4.', getdate())
		set @Q4 =
		(
			select count(OrderId)
			from #tOrdersDay
			where OrderDateTime >= dateadd(n, 45, @DateHour)
				and OrderDateTime < dateadd(n, 60, @DateHour)
		)
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Q4.'
		
		set @NbrOfOrders = @Q1 + @Q2 + @Q3 + @Q4
		set @NbrOfOrdersAccumulated = @NbrOfOrdersAccumulated + @NbrOfOrders

		insert into @tOrderStatsDay
			(Hour, NbrOfOrders, NbrOfOrdersAccumulated, NbrOfOrdersPer15Min)
		select
			@Count,
			@NbrOfOrders,
			@NbrOfOrdersAccumulated,
			(cast(@Q1 as nvarchar(4)) + ', ' + cast(@Q2 as nvarchar(4)) + ', ' + cast(@Q3 as nvarchar(4)) + ', ' + cast(@Q4 as nvarchar(4)))
		
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Alternate template.', getdate())		
		-- Total <that are transferred to ERP, per hour.
		update
			@tOrderStatsDay
		set
			NbrOfOrdersTransferredToERP = 
			(
				select
					count(OrderId)
				from
					#tOrdersDay
				where
					OrderDateTime >= @DateHour
					and OrderDateTime < dateadd(hh, 1, @DateHour)
					and OrderStatusId in (4)
			)
		where
			Hour = @Count
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Orders -> ERP.'
		
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Orders Prev Year -> ERP.', getdate())
		-- Total nbr of orders prev year that are transferred to ERP, per hour.
		update
			@tOrderStatsDay
		set
			NbrOfOrdersPrevYearTransferredToERP = 
			(
				select
					count(OrderId)
				from
					#tOrdersDayInERPPrevYear
				where
					OrderDateTime >= dateadd(week, -52, @DateHour)
					and OrderDateTime < dateadd(week, -52, dateadd(hh, 1, @DateHour))
			)
		where
			Hour = @Count
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Orders Prev Year -> ERP.'
			
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Orders cancelled.', getdate())
		-- Total nbr of orders that are cancelled, per hour.
		update
			@tOrderStatsDay
		set
			NbrOfOrdersCancelled = 
			(
				select
					count(OrderId)
				from
					#tOrdersDay
				where
					OrderDateTime >= @DateHour
					and OrderDateTime < dateadd(hh, 1, @DateHour)
					and OrderStatusId = 3
			)
		where
			Hour = @Count
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Orders cancelled.'
			
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Orders aborted.', getdate())		
		-- Total nbr of orders that are failed, per hour.
		update
			@tOrderStatsDay
		set
			NbrOfOrdersFailed = 
			(
				select
					count(OrderId)
				from
					#tOrdersDay
				where
					OrderDateTime >= @DateHour
					and OrderDateTime < dateadd(hh, 1, @DateHour)
					and OrderStatusId in (5,6)
			)
		where
			Hour = @Count
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Orders failed.'
			
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Orders pay failed.', getdate())
		-- Total nbr of orders that are payment failed, per hour.
		update
			@tOrderStatsDay
		set
			NbrOfOrdersPayFailed = 
			(
				select
					count(OrderId)
				from
					#tOrdersDay
				where
					OrderDateTime >= @DateHour
					and OrderDateTime < dateadd(hh, 1, @DateHour)
					and OrderStatusId = 7
			)
		where
			Hour = @Count
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Orders pay failed.'
			
		if @Debug = 1 and @Count = 0
			insert into @tDbg (Comment, StartT) values ('Orders overflow.', getdate())

		--Total nbr of visitors and orders/visitor, per hour.		
		set @NbrOfVisitors = isnull((select sum(VisitCount)
									from [statistics].[tHourlyVisitor]
									where
										ChannelId in (1,2,3,4) and
										[Year] = DATEPART(year, @DateHour) and
										[Month] = DATEPART(month, @DateHour) and
										[Day] = DATEPART(day, @DateHour) and
										[Hour] = DATEPART(hour, @DateHour)
										),0)
			
		update
			@tOrderStatsDay
		set
			NbrOfVisitors = @NbrOfVisitors			
		where
			Hour = @Count
		
		if @Debug = 1 and @Count = 0
			update @tDbg set EndT = getdate() where Comment = 'Visitors.'
		
		set @Count = @Count + 1
	end
		
	drop table #tOrdersDay
	select @DateText as Date, * from @tOrderStatsDay
	
	if @Debug = 1
	begin
		update @tDbg set EndT = getdate() where Comment = 'Complete run.'
		select * from @tDbg
	end
end
GO
