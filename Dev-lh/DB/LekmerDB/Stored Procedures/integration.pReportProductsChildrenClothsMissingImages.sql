
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportProductsChildrenClothsMissingImages]
AS
BEGIN
	WITH UniqueErpIdLekmer AS
	(
		SELECT
			ROW_NUMBER() OVER(PARTITION BY [lp].[HYErpId] ORDER BY [lp].[HYErpId]) RowNo,
			[lp].[HYErpId],
			[psp].[Lagerplats],
			[p].[Title],
			[lp].[BrandId],
			[lp].[LekmerErpId],
			[ps].[ErpId],
			ISNULL([a1].[NumberInStock], [p].[NumberInStock]) 'NumberInStock',
			lp.[PurchasePrice],
			lp.[PurchasePrice] * ISNULL([a1].[NumberInStock], [p].[NumberInStock]) 'TotalValue'
		FROM
			[product].[tProduct] p
			INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
			INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [p].[CategoryId]
			INNER JOIN [product].[tCategory] c2 ON [c2].[CategoryId] = [c].[ParentCategoryId]
			INNER JOIN [product].[tCategory] c3 ON [c3].[CategoryId] = [c2].[ParentCategoryId]
			LEFT JOIN [lekmer].[tProductSize] ps ON [ps].[ProductId] = [p].[ProductId] -- <-- fel
			LEFT JOIN [integration].[tProductStockPosition] psp ON COALESCE([ps].[ErpId], [lp].[HYErpId] + '-000') = [psp].[HYarticleId]
			CROSS APPLY (SELECT SUM([tps].[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] tps WHERE [tps].[ProductId] = [p].[ProductId]) a1
		WHERE
			[p].[MediaId] IS NULL
			AND ([p].[NumberInStock] > 0 AND [ps].[NumberInStock] IS NULL OR [ps].[NumberInStock] > 0)
			AND [c3].[CategoryId] = 1001310 --'barnkläder'
			AND [p].[IsDeleted] = 0
	)

	SELECT
		[u].[HYErpId],
		[u].[Lagerplats],
		[u].[Title],
		[b].[Title] AS BrandTitle,
		[u].[LekmerErpId] AS SupplierArticleNumber,
		[u].[NumberInStock],
		[u].[PurchasePrice],
		[u].[TotalValue]
	FROM
		UniqueErpIdLekmer u
		LEFT OUTER JOIN [lekmer].[tBrand] b ON [b].[BrandId] = [u].[BrandId]
	WHERE
		[u].[RowNo] = 1
	ORDER BY
		[u].[ErpId]
END
GO
