SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 07.10.2008		Time: 10:00
Description:
			Created 
*/

CREATE PROCEDURE [template].[pModelGetAllByModelFolder]
	@ModelFolderId	int
AS
begin
	set nocount on

	select
		*	
	from
		[template].[vCustomModel]
	where	
		 [Model.ModelFolderId] = @ModelFolderId
	order by
		[Model.Title]
end

GO
