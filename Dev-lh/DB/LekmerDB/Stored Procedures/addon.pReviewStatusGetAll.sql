SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pReviewStatusGetAll]
AS    
BEGIN
         SELECT 
                   * 
         FROM 
                   addon.vCustomReviewStatus
END
GO
