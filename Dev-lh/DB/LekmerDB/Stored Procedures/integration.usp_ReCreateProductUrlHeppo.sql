SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ReCreateProductUrlHeppo]

AS
BEGIN

		begin
			begin try
				begin transaction
					
				delete lekmer.tProductUrl
				insert into lekmer.tProductUrl
				select ProductId, LanguageId, convert(varchar(max), ProductId) 
				from product.tProduct, core.tLanguage

				
				commit
			end try
			
			begin catch
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			

		end
		
END
GO
