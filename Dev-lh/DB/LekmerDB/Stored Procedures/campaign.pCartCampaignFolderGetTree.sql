
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartCampaignFolderGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(50))
	--insert root nodes
	INSERT INTO @tTree
		SELECT 
			[CampaignFolder.Id], 
			[CampaignFolder.ParentId], 
			[CampaignFolder.Title]
		FROM 
			campaign.vCustomCampaignFolder
		WHERE 
			[CampaignFolder.ParentId] IS NULL
			AND [CampaignFolder.TypeId] = 2
		ORDER BY [CampaignFolder.Ordinal]
		
	IF (@SelectedId IS NOT NULL)
		BEGIN
			--insert selected node's children
			INSERT INTO @tTree
				SELECT 
					[CampaignFolder.Id], 
					[CampaignFolder.ParentId], 
					[CampaignFolder.Title]
				FROM 
					campaign.vCustomCampaignFolder
				WHERE 
					[CampaignFolder.ParentId] = @SelectedId
				ORDER BY [CampaignFolder.Ordinal]
			
			--insert siblings and parents with their siblings
			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT [CampaignFolder.ParentId]
									 FROM campaign.vCustomCampaignFolder
									 WHERE [CampaignFolder.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT 
							[CampaignFolder.Id], 
							[CampaignFolder.ParentId], 
							[CampaignFolder.Title]
						FROM 
							campaign.vCustomCampaignFolder
						WHERE
							[CampaignFolder.ParentId] = @ParentId
						ORDER BY [CampaignFolder.Ordinal]
					SET @SelectedId = @ParentId
				END
		END
		
		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM campaign.vCustomCampaignFolder
									WHERE [CampaignFolder.ParentId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY ParentId ASC
END

GO
