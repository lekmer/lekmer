SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserStatusGetByCommonName]
	@CommonName varchar(50)
as
begin
	set nocount on

	select
		*
	from
		security.[vCustomSystemUserStatus]
	where
		[SystemUserStatus.CommonName] = @CommonName
end

GO
