SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSliderGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[bs].[SlideDuration],
		[b].*
	FROM
		[sitestructurelek].[tBlockSlider] bs
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bs].[BlockId] = [b].[Block.BlockId]
	WHERE
		[bs].[BlockId] = @BlockId
END
GO
