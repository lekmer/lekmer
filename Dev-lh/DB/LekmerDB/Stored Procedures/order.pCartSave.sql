
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pCartSave]
	@CartGuid		UNIQUEIDENTIFIER,
	@CreatedDate	DATETIME,
	@IPAddress		VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [order].tCart
	(
		[CartGuid],
		[CreatedDate],
		[IPAddress]
	)
	VALUES
	(
		@CartGuid,
		@CreatedDate,
		@IPAddress
	)
	RETURN SCOPE_IDENTITY()
END
GO
