SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [product].[pVariationDeleteAllByVariationGroup]
	@VariationGroupId	int
as
BEGIN    
		DELETE FROM
			product.tVariationTranslation
		FROM
			product.tVariationTranslation vt
			INNER JOIN [product].[tVariation] as v ON v.[VariationId] = vt.[VariationId]
		WHERE
			[VariationGroupId] = @VariationGroupId	
			
		delete from [product].[tVariation] where [VariationGroupId] = @VariationGroupId	
end


GO
