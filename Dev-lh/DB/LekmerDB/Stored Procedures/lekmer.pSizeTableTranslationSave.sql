
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableTranslationSave]
	@SizeTableId	INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(MAX),
	@Column1Title	NVARCHAR(50),
	@Column2Title	NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tSizeTableTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description,
		[Column1Title] = @Column1Title,
		[Column2Title] = @Column2Title
	WHERE
		[SizeTableId] = @SizeTableId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [lekmer].[tSizeTableTranslation] (
			[SizeTableId],
			[LanguageId],
			[Title],
			[Description],
			[Column1Title],
			[Column2Title]
		)
		VALUES (
			@SizeTableId,
			@LanguageId,
			@Title,
			@Description,
			@Column1Title,
			@Column2Title
		)
	END
END
GO
