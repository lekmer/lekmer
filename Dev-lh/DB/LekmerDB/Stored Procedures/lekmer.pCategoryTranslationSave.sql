
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryTranslationSave]
	@CategoryId		INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@PackageInfo	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategoryTranslation]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId AND
		[LanguageId] = @LanguageId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [product].[tCategoryTranslation] (
			[CategoryId],
			[LanguageId],
			[Title]
		)
		VALUES (
			@CategoryId,
			@LanguageId,
			@Title
		)
	END
	

	UPDATE
		[productlek].[tLekmerCategoryTranslation]
	SET
		[PackageInfo] = @PackageInfo
	WHERE
		[CategoryId] = @CategoryId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [productlek].[tLekmerCategoryTranslation] (
			[CategoryId],
			[LanguageId],
			[PackageInfo]				
		)
		VALUES (
			@CategoryId,
			@LanguageId,
			@PackageInfo
		)
	END	
END
GO
