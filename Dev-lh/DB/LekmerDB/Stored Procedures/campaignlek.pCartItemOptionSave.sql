
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionSave]
	@CartItemOptionId	INT,
    @CartId				INT,
    @ProductId			INT,
    @SizeId				INT,
    @Quantity			INT,
    @CreatedDate		DATETIME
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[campaignlek].[tCartItemOption]
	SET
		SizeId = @SizeId,
		Quantity = Quantity + @Quantity
	WHERE
		CartId = @CartId
		AND ProductId = @ProductId
            

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [campaignlek].[tCartItemOption]
		(
			  CartId,
			  ProductId,
			  SizeId,
			  Quantity,
			  CreatedDate            
		)
		VALUES
		(
			  @CartId,
			  @ProductId,
			  @SizeId,
			  @Quantity,
			  @CreatedDate
		)
	END
	
	
	DELETE FROM [campaignlek].[tCartItemOption] WHERE Quantity <= 0
END
GO
