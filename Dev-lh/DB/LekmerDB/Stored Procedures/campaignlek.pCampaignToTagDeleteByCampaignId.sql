SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagDeleteByCampaignId]
	@CampaignId INT
AS
BEGIN
	DELETE FROM [campaignlek].[tCampaignToTag]
	WHERE [CampaignId] = @CampaignId
END
GO
