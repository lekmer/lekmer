SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [integration].[pSirBatchComplete]
	@BatchId INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE [integration].[tSirBatch]
	SET [CompletedDate] = GETDATE()
	WHERE [SirBatchId] = @BatchId
			
	SELECT *
	FROM [integration].[tSirBatch] b
	WHERE b.[SirBatchId] = @BatchId
	
END
GO
