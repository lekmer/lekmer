
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_GetLatest]
	@ChannelId				INT,
	@CategoryIds			VARCHAR(MAX),
	@BrandIds				VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@NumberOfItems			INT,
	@RatingReviewStatusId	INT,
	@IsInappropriate		BIT,
	@CustomerId				INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql NVARCHAR(MAX)
    SET @sql = '
		SELECT 
			TOP(@NumberOfItems) rrf.*
		FROM
			[review].[vRatingReviewFeedback] rrf
			INNER JOIN [product].[vCustomProduct] p 
				ON p.[Product.Id] = rrf.[RatingReviewFeedback.ProductId] 
				AND p.[Product.ChannelId] = rrf.[RatingReviewFeedback.ChannelId]
			INNER JOIN product.vCustomPriceListItem AS pli
				ON pli.[Price.ProductId] = p.[Product.Id]
				AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)'
			+ CASE WHEN (@CategoryIds IS NOT NULL AND @CategoryIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) c ON c.[ID] = p.[Product.CategoryId]' 
			ELSE '' END
			+ CASE WHEN (@BrandIds IS NOT NULL AND @BrandIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) b ON b.[ID] = p.[Lekmer.BrandId]' 
			ELSE '' END
		+ '
		WHERE 
			rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
			AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
			AND rrf.[RatingReviewFeedback.Inappropriate] = @IsInappropriate
		ORDER BY rrf.[RatingReviewFeedback.CreatedDate] DESC'
     
	EXEC sp_executesql @sql, 
		N'      
			@ChannelId				INT,
			@CategoryIds			VARCHAR(MAX),
			@BrandIds				VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@NumberOfItems			INT,
			@RatingReviewStatusId	INT,
			@IsInappropriate		BIT,
			@CustomerId				INT',
            @ChannelId,
            @CategoryIds,
            @BrandIds,
            @Delimiter,
            @NumberOfItems,
            @RatingReviewStatusId,
            @IsInappropriate,
            @CustomerId
END
GO
