SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [order].[pOrderModuleChannelSave]
@ChannelId	INT,
@OrderTemplateContentNodeId INT
AS
BEGIN
	
	UPDATE 
		[order].[tOrderModuleChannel]
	SET 
		OrderTemplateContentNodeId = @OrderTemplateContentNodeId
	WHERE 
		ChannelId = @ChannelId
	
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [order].[tOrderModuleChannel]
		(
			ChannelId,
			OrderTemplateContentNodeId
		)
		VALUES
		(
			@ChannelId,
			@OrderTemplateContentNodeId
		)
	END
END
GO
