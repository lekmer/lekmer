
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsNO]
AS 
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

		-- Insert new contentnodeIds in tContentPageSeoSetting
		INSERT INTO [sitestructure].[tContentPageSeoSetting] (ContentNodeId)
		SELECT
			[n].[ContentNodeId]
		FROM
			[sitestructure].[tContentNode] n --3
		WHERE
			[n].[SiteStructureRegistryId] = 2
			AND [n].[ContentNodeTypeId] = 3 -- detta är contentpages
			AND [n].[ContentNodeId] NOT IN (SELECT ContentNodeId FROM sitestructure.tContentPageSeoSetting)

		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------										
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' Leketøy online fra Lekmer.no – leketøy på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. '
			+ 'Vi tilbyr et stort utvalg av barneleker fra kjente varemerker og levererer dem på 3-5 dager - Lekmer.no din leketøysbutikk.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND cn.ParentContentNodeId = 1004533 -- Leker
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = x.Title + ' ' + cn.Title + ' Leketøy online fra Lekmer.no – leketøy på nettet.',
			cps.[Description] = 'Kjøp ' + x.Title + ' ' + cn.Title + ' på nettet. '
			+ 'Vi tilbyr et stort utvalg av barneleker fra kjente varemerker og levererer dem på 3-5 dager - Lekmer.no din leketøysbutikk.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1004533) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = 'Barn og Baby ' + cn.Title + ' produkter online fra Lekmer.no – Kjøp barnartikler på nettet.',
			cps.[Description] = 'Kjøp Barn og Baby produkter på nettet. Få dine barn - og babyprodukter levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND cn.ParentContentNodeId = 1004415 -- Barn & Baby
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Barn og Baby produkter online fra Lekmer.no – Kjøp barneartikler på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' Barn og Baby produkter på nettet. '
			+ 'Få dine barn- og babyprodukter levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1004415) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' Barneklær online fra Lekmer.no – Barn og Babyklær på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. '
			+ 'Stort utvalg av kule klær til barn og baby – handle dine barneklær online og få dem levert innen 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND cn.ParentContentNodeId = 1007422 -- Barneklær
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Barneklær online fra Lekmer.no – Barn og babyklær på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dine barneklær levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode 
						WHERE ParentContentNodeId = 1007422) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------			HÄR NUUU
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' Interiør online fra Lekmer.no Barneromsinnredning på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. Få ditt barnerom levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND cn.ParentContentNodeId = 1004480 -- Barneværelse
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Interiør online fra Lekmer.no – Interiør på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' og ' + x.Title + ' på nettet. Få ditt barnerom levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1004480) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' og Underholdning og barnespill online fra Lekmer.no – Tv-spill på nettet.',
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. Få dine tv-spill levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND cn.ParentContentNodeId = 1004932 -- Spill & underholdning
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title,
			cps.[Description] = 'Kjøp ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dine tv-spill levert på 3-5 dager fra Lekmer.no.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1004932) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 2
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))
						
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
