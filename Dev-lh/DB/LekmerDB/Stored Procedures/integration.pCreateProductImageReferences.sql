
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pCreateProductImageReferences]
	@ProductXml XML
	/*
	'<products>
		<product hyid="100124-0014">
			<picture id="1" height="360" width="480" extension="jpg"/>
			<picture id="2" height="360" width="480" extension="jpg"/>
			<picture id="3" height="360" width="480" extension="jpg"/>
			<picture id="4" height="360" width="480" extension="jpg"/>
			<picture id="5" height="360" width="480" extension="jpg"/>
			<picture id="6" height="360" width="480" extension="jpg"/>
			<picture id="7" height="360" width="480" extension="jpg"/>
			<picture id="8" height="360" width="480" extension="jpg"/>
		</product>
	</products>'
	*/
AS
BEGIN

	SET NOCOUNT ON

	-- HÅRDKODAT, KOLLA ÖVER SEN
	DECLARE @MEDIAFOLDERPARENTID INT,
			@MEDIAFORMATID INT
	
	SET @MEDIAFOLDERPARENTID = 1005724
	SET @MEDIAFORMATID = 2

	IF OBJECT_ID('tempdb..#tProductPicture') IS NOT NULL
		DROP TABLE #tProductPicture

	CREATE TABLE #tProductPicture
	(
		ProductId INT NOT NULL,
		HYErpId NVARCHAR (50) COLLATE Finnish_Swedish_CI_AS NULL,
		PictureId INT NOT NULL,
		PictureHeight INT NOT NULL,
		PictureWidth INT NOT NULL,
		Extension NVARCHAR (10) COLLATE Finnish_Swedish_CI_AS NOT NULL,
		MediaFolderId INT NULL,
		MediaId INT NULL,
		FolderTitle AS (CAST(ProductId - ProductId % 100 AS NVARCHAR(150))) COLLATE Finnish_Swedish_CI_AS,
		PictureFileName AS (CAST(ProductId AS VARCHAR(10)) + '_' + CAST(PictureId AS VARCHAR(10))) COLLATE Finnish_Swedish_CI_AS, --ProductId_PictureId
		CONSTRAINT PK_#tProductPicture PRIMARY KEY(ProductId, PictureId)
	)
	
	DECLARE @tMediaInsertedInfo TABLE
	(
		MediaId INT,
		PictureFileName NVARCHAR(150)
		PRIMARY KEY(MediaId)
	)
	
	DECLARE @tMediaReplacement TABLE
	(
		MediaId INT NULL,
		MediaIdOld INT,
		PictureFileName NVARCHAR(150)
	)
	
	BEGIN TRY
		BEGIN TRANSACTION

		-- Populate temp table from XML input
		INSERT #tProductPicture (
			ProductId,
			HYErpId,
			PictureId,
			PictureHeight,
			PictureWidth,
			Extension
		)
		SELECT
			p.ProductId,
			c.value('../@hyid[1]', 'nvarchar(50)'),
			c.value('@id[1]', 'int'),
			c.value('@height[1]', 'int'),
			c.value('@width[1]', 'int'),
			c.value('@extension[1]', 'nvarchar(10)')
		FROM
			@ProductXml.nodes('/products/product/picture') T(c)
			INNER JOIN lekmer.tLekmerProduct p ON p.HYErpId = c.value('../@hyid[1]', 'nvarchar(50)')

		-- Insert into tMediaFolder - fel, ska skapas fler underkategorier...
		INSERT media.tMediaFolder (
			MediaFolderParentId,
			Title
		)
		SELECT DISTINCT
			@MEDIAFOLDERPARENTID,
			p.FolderTitle
		FROM
			#tProductPicture p
		WHERE
			NOT EXISTS(SELECT 1
						FROM media.tMediaFolder mf
						WHERE mf.Title = p.FolderTitle AND mf.MediaFolderParentId = @MEDIAFOLDERPARENTID)


		-- Update temp table, set MediaFolderId
		UPDATE p
		SET MediaFolderId = mf.MediaFolderId
		FROM
			#tProductPicture p
			INNER JOIN media.tMediaFolder mf ON mf.Title = p.FolderTitle AND mf.MediaFolderParentId = @MEDIAFOLDERPARENTID


		-- Populate @tMediaReplacement table
		INSERT @tMediaReplacement (
			MediaId,
			MediaIdOld,
			PictureFileName
		)
		SELECT
			NULL,
			m.MediaId,
			m.[FileName]
		FROM
			#tProductPicture pp
			INNER JOIN media.tMedia m ON m.[FileName] = pp.PictureFileName			
			
		
		-- Insert into tMedia, all as new media items --fel lägger in även om det finns!	
		INSERT INTO media.tMedia (
			[MediaFormatId], 
			[MediaFolderId],
			[Title],
			[FileName]
		)
		OUTPUT INSERTED.MediaId, INSERTED.[FileName] INTO @tMediaInsertedInfo
		SELECT
			mf.MediaFormatId,
			t.MediaFolderId,
			CASE WHEN ([l].[BrandId] IS NULL) 
				THEN 
					p.Title + '_' + CAST(t.ProductId AS VARCHAR(10))
				ELSE
					ISNULL(b.Title, ' ') + '_' + p.Title + '_' + CAST(t.ProductId AS VARCHAR(10)) END,
			--ISNULL(b.Title, ' ') + '_' + p.Title + '_' + CAST(t.ProductId AS VARCHAR(10)),	--BrandTitle_ProductTitle_ProductId
			t.PictureFileName
		FROM
			#tProductPicture t
			INNER JOIN product.tProduct p ON p.ProductId = t.ProductId
			INNER JOIN lekmer.tLekmerProduct l ON t.ProductId = l.ProductId
			LEFT JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT OUTER JOIN media.tMediaFormat mf ON mf.Extension = t.Extension


		-- Update temp table - populate all new MediaId
		UPDATE pp
		SET pp.MediaId = m.MediaId
		FROM
			#tProductPicture pp
			INNER JOIN @tMediaInsertedInfo m ON m.PictureFileName = pp.PictureFileName
			
			
		-- Update @tMediaReplacement table - populate new MediaId
		UPDATE mr
		SET mr.MediaId = m.MediaId
		FROM
			@tMediaReplacement mr
			INNER JOIN @tMediaInsertedInfo m ON m.PictureFileName = mr.PictureFileName


		-- Insert into tImage
		INSERT INTO media.tImage (
			MediaId, 
			Height, 
			Width, 
			AlternativeText
		)
		SELECT
			pp.MediaId,
			pp.PictureHeight,
			pp.PictureWidth,
			pp.PictureFileName
		FROM
			#tProductPicture pp


		-- Update tProduct - set new main image
		UPDATE p
		SET p.MediaId = pp.MediaId
		FROM
			#tProductPicture pp	
			INNER JOIN product.tProduct p ON p.ProductId = pp.ProductId
		WHERE
			pp.PictureId = 1


		-- Update tProductImage with new images instead of old
		UPDATE pim
		SET pim.MediaId = mr.MediaId
		FROM
			product.tProductImage pim
			INNER JOIN #tProductPicture pp ON pp.ProductId = pim.ProductId
			INNER JOIN @tMediaReplacement mr ON mr.MediaIdOld = pim.MediaId


		-- Insert into tProductImage
		INSERT INTO product.tProductImage (
			ProductId,
			MediaId,
			ProductImageGroupId,
			Ordinal
		)
		SELECT
			pp.ProductId,
			pp.MediaId,
			(CASE WHEN pp.PictureId = 1 THEN 1 ELSE 4 END), --ImageGroup
			pp.PictureId --1 -- ORDINAL (Sorting)
		FROM
			#tProductPicture pp
		WHERE
			NOT EXISTS
			(
				SELECT 1
				FROM
					product.tProductImage pim
				WHERE
					pim.ProductId = pp.ProductId
				AND pim.MediaId = pp.MediaId
			)

	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH

		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		DECLARE
			@ERROR_MESSAGE NVARCHAR(MAX) = ERROR_MESSAGE(),
			@ERROR_PROCEDURE NVARCHAR(MAX) = ERROR_PROCEDURE()

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES('Fel', @ERROR_MESSAGE, GETDATE(), @ERROR_PROCEDURE)
					
		INSERT INTO [integration].[tImageImportLog](Data, [Date], [Message])
		VALUES(@ERROR_PROCEDURE, GETDATE(), @ERROR_MESSAGE)
	END CATCH
	

		
	SELECT
		pp.PictureId as ImageId,
		i.MediaId,
		i.ProductImageGroupId,
		i.Ordinal
	FROM
		product.tProductImage i
		INNER JOIN #tProductPicture pp ON i.MediaId = pp.MediaId
	ORDER BY
		pp.PictureId

	IF OBJECT_ID('tempdb..#tProductPicture') IS NOT NULL
		DROP TABLE #tProductPicture
END
GO
