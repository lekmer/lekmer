SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListBrandGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[review].[vBlockLatestFeedbackListBrand] b
	WHERE
		b.[BlockLatestFeedbackListBrand.BlockId] = @BlockId
		AND b.[ChannelId] = @ChannelId
END
GO
