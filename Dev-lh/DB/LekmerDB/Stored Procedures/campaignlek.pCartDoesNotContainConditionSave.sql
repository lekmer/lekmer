SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartDoesNotContainConditionSave]
	@ConditionId		INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartDoesNotContainCondition]
	SET
		ConfigId = @ConfigId
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tCartDoesNotContainCondition] (
			ConditionId,
			ConfigId
		)
		VALUES (
			@ConditionId,
			@ConfigId
		)
	END
END
GO
