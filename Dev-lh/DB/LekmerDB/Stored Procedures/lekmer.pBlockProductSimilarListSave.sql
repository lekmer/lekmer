
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockProductSimilarListSave]
	@BlockId INT,
	@PriceRangeType INT
AS
BEGIN
	UPDATE [lekmer].[tBlockProductSimilarList]
	SET
		[PriceRangeType] = @PriceRangeType
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockProductSimilarList] (
		[BlockId],
		[PriceRangeType]
	)
	VALUES (
		@BlockId,
		@PriceRangeType
	)
END
GO
