
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]
	@ChannelId INT,
	@RatingId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[ri].[RatingItem.RatingId] 'RatingItemProductScoreSummary.RatingId',
		[ri].[RatingItem.RatingItemId] 'RatingItemProductScoreSummary.RatingItemId',
		[ri].[RatingItem.Score] 'RatingItemProductScoreSummary.RatingItemScore',
		ISNULL(SUM([ps].[RatingItemProductScore.HitCount]), 0) 'RatingItemProductScoreSummary.TotalHitCount'
	FROM 
		[review].[vRatingItemSecure] ri
		LEFT JOIN [review].[vRatingItemProductScore] ps ON
			[ps].[RatingItemProductScore.RatingId] = [ri].[RatingItem.RatingId]
			AND [ps].[RatingItemProductScore.RatingItemId] = [ri].[RatingItem.RatingItemId]
			AND [ps].[RatingItemProductScore.ProductId] = @ProductId
			AND [ps].[RatingItemProductScore.ChannelId] = @ChannelId
	WHERE
		 [ri].[RatingItem.RatingId] = @RatingId
	GROUP BY
		[ri].[RatingItem.RatingId],
		[ri].[RatingItem.RatingItemId],
		[ri].[RatingItem.Score]
END
GO
