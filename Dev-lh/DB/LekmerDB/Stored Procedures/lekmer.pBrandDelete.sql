
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandDelete]
	@BrandId	INT
AS
BEGIN
	DECLARE @DeliveryTimeId INT
	SET @DeliveryTimeId = (SELECT [DeliveryTimeId] FROM [lekmer].[tBrand] WHERE [BrandId] = @BrandId)
	EXEC [productlek].[pProductRegistryDeliveryTimeDelete] @DeliveryTimeId
	EXEC [productlek].[pDeliveryTimeDelete] @DeliveryTimeId

	EXEC [lekmer].[pProductRegistryRestrictionBrandDelete] @BrandId

	DELETE FROM
		[review].[tBlockBestRatedProductListBrand]
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[review].[tBlockLatestFeedbackListBrand]
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[lekmer].[tBlockBrandListBrand]
	WHERE 
		[BrandId] = @BrandId
	
	DELETE FROM 
		[lekmer].[tBlockBrandProductListBrand]
	WHERE 
		[BrandId] = @BrandId
		
	DELETE FROM
		[lekmer].[tBlockProductFilterBrand]
	WHERE 
		[BrandId] = @BrandId	
	
	UPDATE 
		[lekmer].[tLekmerProduct]
	SET 
		[BrandId] = NULL 
	WHERE 
		[BrandId] = @BrandId

	DELETE FROM 
		[lekmer].[tBrandTranslation]
	WHERE
		[BrandId] = @BrandId
			
	DELETE FROM 
		[lekmer].[tBrand]
	WHERE 
		[BrandId] = @BrandId
END
GO
