SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderItemGetById]
	@OrderItemId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomOrderItem]
	WHERE
		[OrderItem.OrderItemId] = @OrderItemId
END

GO
