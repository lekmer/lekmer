SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMediaItemGetAllBySizeTableSecure]
	@SizeTableId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		m.*
	FROM
		[lekmer].[tSizeTableMedia] stm
		INNER JOIN [media].[vCustomMedia] m ON [m].[Media.Id] = [stm].[MediaId]
	WHERE
		stm.[SizeTableId] = @SizeTableId
END
GO
