SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pAdSave]
	@AdId INT,
	@FolderId INT,
	@Key NVARCHAR(50),
	@Title NVARCHAR(250),
	@DisplayFrom DATETIME,
	@DisplayTo DATETIME,
	@ProductCategory NVARCHAR(250),
	@Gender NVARCHAR(250),
	@Format NVARCHAR(250),
	@SearchTags NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	-- Same keys are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [esales].[tAd]
		WHERE
			[Key] = @Key
			AND
			[AdId] <> @AdId
	)
	RETURN -1

	IF @AdId IS NOT NULL
		UPDATE
			[esales].[tAd]
		SET
			[FolderId] = @FolderId,
			[Key] = @Key,
			[Title] = @Title,
			[DisplayFrom] = @DisplayFrom,
			[DisplayTo] = @DisplayTo,
			[ProductCategory] = @ProductCategory,
			[Gender] = @Gender,
			[Format] = @Format,
			[SearchTags] = @SearchTags,
			[UpdatedDate] = GETDATE()
		WHERE
			[AdId] = @AdId

	IF  @AdId IS NULL OR @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [esales].[tAd] (
			[FolderId],
			[Key],
			[Title],
			[DisplayFrom],
			[DisplayTo],
			[ProductCategory],
			[Gender],
			[Format],
			[SearchTags],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES (
			@FolderId,
			@Key,
			@Title,
			@DisplayFrom,
			@DisplayTo,
			@ProductCategory,
			@Gender,
			@Format,
			@SearchTags,
			GETDATE(),
			GETDATE()
		)

		SET @AdId = SCOPE_IDENTITY()
	END

	RETURN @AdId
END
GO
