
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageSave]
	@ContentMessageId			INT,
	@ContentMessageFolderId		INT,
	@CommonName					VARCHAR(50),
	@Message					NVARCHAR(MAX),
	@IsDropShip					BIT,
	@StartDate					DATETIME = NULL,
	@EndDate					DATETIME = NULL,
	@StartDailyIntervalMinutes	INT = NULL,
	@EndDailyIntervalMinutes	INT = NULL
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tContentMessage]
		WHERE [CommonName] = @CommonName
			AND [ContentMessageId] <> @ContentMessageId
	)
	RETURN -2
	
	UPDATE 
		[lekmer].[tContentMessage]
	SET 
		[ContentMessageFolderId] = @ContentMessageFolderId,
		[CommonName] = @CommonName,
		[Message] = @Message,
		[IsDropShip] = @IsDropShip,
		[StartDate]	= @StartDate,
		[EndDate] = @EndDate,
		[StartDailyIntervalMinutes] = @StartDailyIntervalMinutes,
		[EndDailyIntervalMinutes] = @EndDailyIntervalMinutes
	WHERE
		[ContentMessageId] = @ContentMessageId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tContentMessage] (
			[ContentMessageFolderId],
			[CommonName],
			[Message],
			[IsDropShip],
			[StartDate],
			[EndDate],
			[StartDailyIntervalMinutes],
			[EndDailyIntervalMinutes]
		)
		VALUES (
			@ContentMessageFolderId,
			@CommonName,
			@Message,
			@IsDropShip,
			@StartDate,
			@EndDate,
			@StartDailyIntervalMinutes,
			@EndDailyIntervalMinutes
		)

		SET @ContentMessageId = SCOPE_IDENTITY()
	END

	RETURN @ContentMessageId
END
GO
