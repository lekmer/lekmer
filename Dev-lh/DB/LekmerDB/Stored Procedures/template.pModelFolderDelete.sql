SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFolderDelete]
	@ModelFolderId INT
AS 
BEGIN
    SET NOCOUNT ON	

    DELETE  [template].[tModelFolder]
    WHERE   [ModelFolderId] = @ModelFolderId
    
    RETURN 0
END
GO
