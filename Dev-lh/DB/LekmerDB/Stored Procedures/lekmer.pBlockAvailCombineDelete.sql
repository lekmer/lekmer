
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockAvailCombineDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockAvailCombine]
	WHERE [BlockId] = @BlockId
END
GO
