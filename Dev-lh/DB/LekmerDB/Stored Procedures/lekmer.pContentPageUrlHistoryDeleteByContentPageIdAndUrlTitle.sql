
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteByContentPageIdAndUrlTitle]
	@ContentPageId	INT,
	@UrlTitleOld	NVARCHAR(256)
AS
BEGIN
	DELETE 
		[lekmer].[tContentPageUrlHistory]
	WHERE 
		ContentPageId = @ContentPageId
		AND UrlTitleOld = @UrlTitleOld
END
GO
