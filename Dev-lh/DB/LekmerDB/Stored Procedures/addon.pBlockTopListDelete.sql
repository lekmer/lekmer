
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pBlockTopListDelete]
	@BlockId INT
AS
BEGIN
	DELETE addon.tBlockTopListCategory
	WHERE BlockId = @BlockId
	
	DELETE addon.tBlockTopListProduct
	WHERE BlockId = @BlockId

	DELETE addon.tBlockTopList
	WHERE BlockId = @BlockId
END
GO
