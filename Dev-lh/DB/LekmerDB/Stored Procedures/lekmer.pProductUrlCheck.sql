SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlCheck]
	@ProductId	int,
	@LanguageId	int,
	@UrlTitle	nvarchar(256)
AS 
BEGIN
	IF EXISTS (SELECT * FROM [lekmer].[tProductUrl] WHERE LanguageId = @LanguageId AND UrlTitle = @UrlTitle AND ProductId <> @ProductId)
		RETURN -1
	
	RETURN 1
END
GO
