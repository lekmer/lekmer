SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemUpdate]
	@Id		INT,
	@SizeId	INT
AS
BEGIN
	UPDATE [lekmer].[tWishListPackageItem]
	SET [SizeId] = @SizeId
	WHERE [Id] = @Id
END
GO
