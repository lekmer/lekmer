SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ReportLekmerDailyOrders]

AS
begin
	set nocount on
	begin try
		--begin transaction
		
		--select DATEADD(HOUR , -1, GETDATE())
		
		select COUNT(OrderId) as NumOfOrders
		from [order].torder
		where Createddate > DATEADD(HOUR , -1, GETDATE())
		and 
			(
				OrderStatusId = 4			
			)

	--commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
