SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderStatusUpdate]
	@OrderId INT,
	@OrderStatusId INT
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE
		[order].[tOrder]
	SET
		OrderStatusId = @OrderStatusId
	WHERE
		OrderId = @OrderId
		AND OrderStatusId <> @OrderStatusId

	DECLARE @OrderItemStatusId INT
	SET @OrderItemStatusId = (SELECT [OrderItemStatusId] FROM [order].[tOrderItemStatus] WHERE [CommonName] = (SELECT [CommonName] FROM [order].[tOrderStatus] WHERE [OrderStatusId] = @OrderStatusId))

	UPDATE
		[order].[tOrderItem]
	SET
		OrderItemStatusId = @OrderItemStatusId
	WHERE
		[OrderId] = @OrderId
END
GO
