SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2UrlTitleTranslationGetAll]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [BlockId]		AS 'Id',
		[LanguageId]	AS 'LanguageId',
		[UrlTitle]		AS 'Value'
	FROM
	    [esales].[vBlockEsalesTopSellersV2Translation]
	WHERE 
		[BlockId] = @BlockId
END
GO
