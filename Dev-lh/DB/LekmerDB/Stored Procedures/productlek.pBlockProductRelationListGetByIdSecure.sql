SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[bprl].*,
		[b].*
	FROM
		[productlek].[vBlockProductRelationList] bprl
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bprl].[BlockProductRelationList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bprl].[BlockProductRelationList.BlockId] = @BlockId
END
GO
