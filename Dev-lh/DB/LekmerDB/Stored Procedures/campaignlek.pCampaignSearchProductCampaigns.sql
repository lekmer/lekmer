
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignSearchProductCampaigns]
@SearchString	nvarchar(MAX)
AS
BEGIN
	DECLARE @ProductId VARCHAR(50) = null;
	DECLARE @ProductIds VARCHAR(MAX);
	DECLARE @Title VARCHAR(MAX)
	SET @Title = [generic].[fPrepareSearchParameter](@SearchString)
	SET @ProductIds = @SearchString;

	DECLARE @IncludedCampaign TABLE 
	( 
		Id int NOT NULL IDENTITY(1,1),
		ProductActionId INT,
		ErpId VARCHAR(50),
		CampaignId INT
	)

	DECLARE @ExludedCampaign TABLE 
	( 
		Id int NOT NULL IDENTITY(1,1),
		ProductActionId INT,
		ErpId VARCHAR(50),
		CampaignId INT
	)
	SET @ProductIds = REPLACE(@ProductIds, ' ', '')
	WHILE LEN(@ProductIds) > 0
	BEGIN
		IF PATINDEX('%,%',@ProductIds) > 0
		BEGIN
			SET @ProductId = SUBSTRING(@ProductIds, 0, PATINDEX('%,%',@ProductIds))
			SET @ProductIds = SUBSTRING(@ProductIds, LEN(@ProductId + ',') + 1, LEN(@ProductIds))
		END
		ELSE
		BEGIN
			SET @ProductId = @ProductIds
			SET @ProductIds = NULL
		END

		DECLARE @ProductTypeId INT

		SELECT @ProductTypeId = lp.ProductTypeId
		FROM [lekmer].[tLekmerProduct] lp
		WHERE lp.[HYErpId] = @ProductId;

		--PRODUCT DISCOUNT
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			( pa.ProductActionId IN
				(
					SELECT pai.ProductActionId
					FROM [lekmer].[tProductDiscountActionItem] pai
					INNER JOIN [product].[tProduct] p
					ON p.ProductId = pai.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
			AND
			(
				patt.ProductTypeId = @ProductTypeId
			)

		)


		--PERSENTAGE PRICE DISCOUNT
		--IncludedProducts
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE pa.ProductActionId IN
			(
				SELECT pda.ProductActionId
				FROM [campaign].[tPercentagePriceDiscountAction] pda
				WHERE
				(pda.ConfigId IN
					(
						SELECT caip.ConfigId
						FROM [campaignlek].[tCampaignActionIncludeProduct] caip
						INNER JOIN [product].[tProduct] p
						ON caip.ProductId = p.ProductId
						WHERE p.ErpId = @ProductId
					)
				)
			)
			AND
			(
				patt.ProductTypeId = @ProductTypeId
			)
		)

		--ExludedProducts
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT pda.ProductActionId
				FROM [campaign].[tPercentagePriceDiscountAction] pda
				WHERE pda.ConfigId IN
				(
					SELECT caip.ConfigId
					FROM [campaignlek].[tCampaignActionExcludeProduct] caip
					INNER JOIN [product].[tProduct] p
					ON caip.ProductId = p.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
		)
		DECLARE @CategoryId INT

		SELECT @CategoryId = p.CategoryId 
		FROM [product].[tProduct] p
		WHERE p.ErpId = @ProductId

		CREATE TABLE #CategoriesTemp
		(
			CategoryId INT,
			ParentCategoryId INT
		);

		WITH Categories AS
		(
			SELECT c.CategoryId, c.ParentCategoryId
			FROM [product].[tCategory] c
			WHERE c.CategoryId = @CategoryId
			UNION ALL
			SELECT c.CategoryId, c.ParentCategoryId
			FROM [product].[tCategory] c
			INNER JOIN Categories ci
			ON ci.ParentCategoryId = c.CategoryId
		)

		INSERT INTO #CategoriesTemp (CategoryId, ParentCategoryId)
		SELECT CategoryId, ParentCategoryId
		FROM Categories

		--IncludedCategories
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT 
						pda.ProductActionId
					FROM
						[campaign].[tPercentagePriceDiscountAction] pda
					WHERE
						pda.ConfigId IN
						(
							SELECT cai.ConfigId
							FROM  #CategoriesTemp ct
							INNER JOIN [campaignlek].[tCampaignActionIncludeCategory] cai
							ON ct.CategoryId = cai.CategoryId
							WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
						)
				)
			)
		)

		--ExcludedCategories
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT 
					pda.ProductActionId
				FROM
					[campaign].[tPercentagePriceDiscountAction] pda
				WHERE
					pda.ConfigId IN
					(
						SELECT cai.ConfigId
						FROM  #CategoriesTemp ct
						INNER JOIN [campaignlek].[tCampaignActionExcludeCategory] cai
						ON ct.CategoryId = cai.CategoryId
						WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
					)
			)
		)

		DECLARE @BrandId INT

		SELECT @BrandId = lp.BrandId
		FROM [lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p
		ON lp.ProductId = p.ProductId
		WHERE p.ErpId = @ProductId;

		--IncludedBrands
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT pda.ProductActionId
					FROM [campaign].[tPercentagePriceDiscountAction] pda
					WHERE pda.ConfigId IN
					(
						SELECT caib.ConfigId
						FROM [campaignlek].[tCampaignActionIncludeBrand] caib
						WHERE caib.BrandId = @BrandId
					)
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT pda.ProductActionId
				FROM [campaign].[tPercentagePriceDiscountAction] pda
				WHERE pda.ConfigId IN
				(
					SELECT caib.ConfigId
					FROM [campaignlek].[tCampaignActionExcludeBrand] caib
					WHERE caib.BrandId = @BrandId
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT pda.ProductActionId
					FROM [campaign].[tPercentagePriceDiscountAction] pda
					INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac
					ON pda.ConfigId = cac.CampaignActionConfiguratorId
					WHERE cac.IncludeAllProducts = 1
				)
			)
		)

	--FIXEDDISCOUNT
		--IncludedProducts
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE pa.ProductActionId IN
			(
				SELECT fda.ProductActionId
				FROM [campaignlek].[tFixedDiscountActionIncludeProduct] fda
				INNER JOIN [product].[tProduct] p
				ON fda.ProductId = p.ProductId
				WHERE
				(
					(patt.ProductTypeId = @ProductTypeId)AND(p.ErpId = @ProductId)
				)
			)
		)

		--ExludedProducts
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
			SELECT fda.ProductActionId
				FROM [campaignlek].[tFixedDiscountActionExcludeProduct] fda
				INNER JOIN [product].[tProduct] p
				ON fda.ProductId = p.ProductId
				WHERE
				(
					p.ErpId = @ProductId
				)
			)
		)

		--IncludedCategories
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT 
						fda.ProductActionId
					FROM
						[campaignlek].[tFixedDiscountActionIncludeCategory] fda
					WHERE
						fda.CategoryId IN
						(
							SELECT ct.CategoryId
							FROM  #CategoriesTemp ct
						)
				)
			)
		)

		--ExcludedCategories
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT 
					fda.ProductActionId
				FROM
					[campaignlek].[tFixedDiscountActionExcludeCategory] fda
				WHERE
					fda.CategoryId IN
					(
						SELECT ct.CategoryId
						FROM  #CategoriesTemp ct
					)
			)
		)

		--IncludedBrands
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT fda.ProductActionId
					FROM [campaignlek].[tFixedDiscountActionIncludeBrand] fda
					WHERE fda.BrandId = @BrandId 
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT pda.ProductActionId
				FROM [campaign].[tPercentagePriceDiscountAction] pda
				WHERE pda.ConfigId IN
				(
					SELECT fda.ProductActionId
					FROM [campaignlek].[tFixedDiscountActionExcludeBrand] fda
					WHERE fda.BrandId = @BrandId 
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT fda.ProductActionId
					FROM [campaignlek].[tFixedDiscountAction] fda
					WHERE fda.IncludeAllProducts = 1
				)
			)
		)

		--GIFT CARD VIA EMAIL
		--IncludedProducts
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE pa.ProductActionId IN
			(
				SELECT cvmpa.ProductActionId
				FROM [campaignlek].[tGiftCardViaMailProductAction] cvmpa
				WHERE
				(
					patt.ProductTypeId = @ProductTypeId
				)
				AND
				(cvmpa.ConfigId IN
					(
						SELECT caip.ConfigId
						FROM [campaignlek].[tCampaignActionIncludeProduct] caip
						INNER JOIN [product].[tProduct] p
						ON caip.ProductId = p.ProductId
						WHERE p.ErpId = @ProductId
					)
				)
			)
		)

		--ExludedProducts
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT cvmpa.ProductActionId
				FROM [campaignlek].[tGiftCardViaMailProductAction] cvmpa
				WHERE cvmpa.ConfigId IN
				(
					SELECT caip.ConfigId
					FROM [campaignlek].[tCampaignActionExcludeProduct] caip
					INNER JOIN [product].[tProduct] p
					ON caip.ProductId = p.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
		)

		--IncludedCategories
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT 
						cvmpa.ProductActionId
					FROM
						[campaignlek].[tGiftCardViaMailProductAction] cvmpa
					WHERE
						cvmpa.ConfigId IN
						(
							SELECT cai.ConfigId
							FROM  #CategoriesTemp ct
							INNER JOIN [campaignlek].[tCampaignActionIncludeCategory] cai
							ON ct.CategoryId = cai.CategoryId
							WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
						)
				)
			)
		)

		--ExcludedCategories
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT 
					cvmpa.ProductActionId
				FROM
					[campaignlek].[tGiftCardViaMailProductAction] cvmpa
				WHERE
					cvmpa.ConfigId IN
					(
						SELECT cai.ConfigId
						FROM  #CategoriesTemp ct
						INNER JOIN [campaignlek].[tCampaignActionExcludeCategory] cai
						ON ct.CategoryId = cai.CategoryId
						WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
					)
			)
		)

		SELECT @BrandId = lp.BrandId
		FROM [lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p
		ON lp.ProductId = p.ProductId
		WHERE p.ErpId = @ProductId;

		--IncludedBrands
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT cvmpa.ProductActionId
					FROM [campaignlek].[tGiftCardViaMailProductAction] cvmpa
					WHERE cvmpa.ConfigId IN
					(
						SELECT caib.ConfigId
						FROM [campaignlek].[tCampaignActionIncludeBrand] caib
						WHERE caib.BrandId = @BrandId
					)
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT cvmpa.ProductActionId
				FROM [campaignlek].[tGiftCardViaMailProductAction] cvmpa
				WHERE cvmpa.ConfigId IN
				(
					SELECT caib.ConfigId
					FROM [campaignlek].[tCampaignActionExcludeBrand] caib
					WHERE caib.BrandId = @BrandId
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT cvmpa.ProductActionId
					FROM [campaignlek].[tGiftCardViaMailProductAction] cvmpa
					INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac
					ON cvmpa.ConfigId = cac.CampaignActionConfiguratorId
					WHERE cac.IncludeAllProducts = 1
				)
			)
		)

		--FIXED PRICE
		--IncludedProducts
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE pa.ProductActionId IN
			(
				SELECT fpa.ProductActionId
				FROM [campaignlek].[tFixedPriceActionIncludeProduct] fpa
				INNER JOIN [product].[tProduct] p
				ON fpa.ProductId = p.ProductId
				WHERE
				(
					(patt.ProductTypeId = @ProductTypeId)AND(p.ErpId = @ProductId)
				)
			)
		)

		--ExludedProducts
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
			SELECT fpa.ProductActionId
				FROM [campaignlek].[tFixedPriceActionExcludeProduct] fpa
				INNER JOIN [product].[tProduct] p
				ON fpa.ProductId = p.ProductId
				WHERE
				(
					p.ErpId = @ProductId
				)
			)
		)

		--IncludedCategories
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT 
						fpa.ProductActionId
					FROM
						[campaignlek].[tFixedPriceActionIncludeCategory] fpa
					WHERE
						fpa.CategoryId IN
						(
							SELECT ct.CategoryId
							FROM  #CategoriesTemp ct
						)
				)
			)
		)

		--ExcludedCategories
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT 
					fpa.ProductActionId
				FROM
					[campaignlek].[tFixedPriceActionExcludeCategory] fpa
				WHERE
					fpa.CategoryId IN
					(
						SELECT ct.CategoryId
						FROM  #CategoriesTemp ct
					)
			)
		)

		--IncludedBrands
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT fpa.ProductActionId
					FROM [campaignlek].[tFixedPriceActionIncludeBrand] fpa
					WHERE fpa.BrandId = @BrandId 
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			WHERE pa.ProductActionId IN
			(
				SELECT pda.ProductActionId
				FROM [campaign].[tPercentagePriceDiscountAction] pda
				WHERE pda.ConfigId IN
				(
					SELECT fpa.ProductActionId
					FROM [campaignlek].[tFixedPriceActionExcludeBrand] fpa
					WHERE fpa.BrandId = @BrandId 
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (ProductActionId, ErpId, CampaignId) 
		(
			SELECT pa.ProductActionId, @ProductId, pa.CampaignId
			FROM [campaign].[tProductAction] pa
			LEFT JOIN [campaignlek].[tProductActionTargetProductType] patt
			ON pa.ProductActionId = patt.ProductActionId
			WHERE
			(
				patt.ProductTypeId = @ProductTypeId
			)
			AND
			( pa.ProductActionId IN
				(
					SELECT fpa.ProductActionId
					FROM [campaignlek].[tFixedPriceAction] fpa
					WHERE fpa.IncludeAllProducts = 1
				)
			)
		)

	IF(OBJECT_ID('tempdb..#CategoriesTemp') Is Not Null) DROP TABLE #CategoriesTemp
	END

	DECLARE @ResultTable Table
	(
		Id int NOT NULL IDENTITY(1,1),
		ProductActionId INT,
		ErpId VARCHAR(50),
		CampaignId INT
	)

	--remove dublicates
	DELETE FROM @IncludedCampaign
	WHERE Id NOT IN 
	(
		SELECT MIN(Id) _
		FROM @IncludedCampaign 
		GROUP BY ProductActionId, ErpId, CampaignId
	) 

	DELETE FROM @ExludedCampaign
	WHERE Id NOT IN 
	(
		SELECT MIN(Id) _
		FROM @ExludedCampaign 
		GROUP BY ProductActionId, ErpId, CampaignId
	) 

	--remove excluded
	INSERT INTO @ResultTable (ProductActionId, ErpId, CampaignId)
	(
		SELECT ProductActionId, ErpId, CampaignId
		FROM @IncludedCampaign
		EXCEPT
		SELECT  ProductActionId, ErpId, CampaignId
		FROM @ExludedCampaign
	)

	SELECT 
		c.*, rt.ErpId
	FROM 
		campaign.vCustomCampaign c
	INNER JOIN 
		campaign.tProductCampaign pc
	ON 
		PC.CampaignId = C.[Campaign.Id]
	INNER JOIN
	(
		SELECT  CampaignId, ErpId
		FROM @ResultTable 
		GROUP BY ErpId, CampaignId
	) rt
	ON c.[Campaign.Id] = rt.CampaignId
	UNION ALL
		SELECT 
		C.*, ''
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tProductCampaign PC ON PC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		ErpId ASC
END
GO
