SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure addon.pBlockTopListProductSave
	@BlockId int,
	@ProductId int,
	@Position int
as
begin
	update addon.tBlockTopListProduct
	set
		Position = @Position
	where
		BlockId = @BlockId
		and ProductId = @ProductId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert addon.tBlockTopListProduct
	(
		BlockId,
		ProductId,
		Position
	)
	values
	(
		@BlockId,
		@ProductId,
		@Position
	)
end
GO
