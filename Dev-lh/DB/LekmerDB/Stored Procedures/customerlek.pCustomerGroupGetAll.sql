SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pCustomerGroupGetAll]
AS
BEGIN
	SELECT
		CG.*
	FROM
		[customer].[vCustomCustomerGroup] AS CG
END
GO
