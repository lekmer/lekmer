SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionGetAllByCart] 
	@ChannelId	INT,
	@CustomerId	INT,
	@CartId		INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[campaignlek].[vCartItemOption] cio
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = cio.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				cio.[Product.CurrencyId],
				cio.[Product.Id],
				cio.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		cio.[CartItemOption.CartId] = @CartId 
		AND cio.[Product.ChannelId] = @ChannelId
	ORDER BY cio.[CartItemOption.CreatedDate] ASC
END
GO
