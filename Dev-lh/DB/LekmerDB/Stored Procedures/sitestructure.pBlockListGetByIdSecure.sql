SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [sitestructure].[pBlockListGetByIdSecure]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[sitestructure].[vBlockListSecure] AS ber
	WHERE
		ber.[Block.BlockId] = @BlockId
END


GO
