
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableSave]
	@SizeTableId		INT,
	@SizeTableFolderId	INT,
	@CommonName			VARCHAR(50),
	@Title				NVARCHAR(50),
	@Description		NVARCHAR(MAX),
	@Column1Title		NVARCHAR(50),
	@Column2Title		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTable]
		WHERE [Title] = @Title
			AND [SizeTableId] <> @SizeTableId
			AND	[SizeTableFolderId] = @SizeTableFolderId
	)
	RETURN -1
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTable]
		WHERE [CommonName] = @CommonName
			AND [SizeTableId] <> @SizeTableId
	)
	RETURN -2
	
	UPDATE 
		[lekmer].[tSizeTable]
	SET 
		[SizeTableFolderId] = @SizeTableFolderId,
		[CommonName] = @CommonName,
		[Title] = @Title,
		[Description] = @Description,
		[Column1Title] = @Column1Title,
		[Column2Title] = @Column2Title
	WHERE
		[SizeTableId] = @SizeTableId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTable] (
			[SizeTableFolderId],
			[CommonName],
			[Title],
			[Description],
			[Column1Title],
			[Column2Title]
		)
		VALUES (
			@SizeTableFolderId,
			@CommonName,
			@Title,
			@Description,
			@Column1Title,
			@Column2Title
		)

		SET @SizeTableId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableId
END
GO
