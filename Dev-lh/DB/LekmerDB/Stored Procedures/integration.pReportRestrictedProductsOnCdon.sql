SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportRestrictedProductsOnCdon]
	
AS
BEGIN
	
	select
		r.Title as [Site],
		l.HYErpId,
		e.Reason,
		u.Username as RestrictedBy,
		e.CreatedDate as RestrictionDate
	from 
		export.tCdonExportRestrictionProduct e
		inner join product.tProductRegistry r on e.ProductRegistryId = r.ProductRegistryId
		inner join lekmer.tLekmerProduct l on e.ProductId = l.ProductId
		left join [security].tSystemUser u on e.UserId = u.SystemUserId
	order by
		e.ProductRegistryId
        
END


GO
