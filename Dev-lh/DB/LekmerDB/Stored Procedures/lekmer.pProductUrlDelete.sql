SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlDelete]
	@ProductId	int
AS 
BEGIN
	DELETE
		[lekmer].[tProductUrl]
	WHERE
		[ProductId] = @ProductId
END
GO
