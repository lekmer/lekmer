SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableIncludeProductDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tSizeTableIncludeProduct]
	WHERE [SizeTableId] = @SizeTableId
END
GO
