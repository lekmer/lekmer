
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pProductChangeEventDeleteExpiredItems]
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ExpirationDate DATETIME
	SET @ExpirationDate = DATEADD(DAY, -5, GETDATE())

	DELETE 
		pce
	FROM
		[productlek].[tProductChangeEvent] pce
	WHERE 
		pce.[CreatedDate] < @ExpirationDate
END
GO
