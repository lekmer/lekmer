SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryGetAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[stmpr].*
	FROM
		[lekmer].[vSizeTableMediaProductRegistry] stmpr
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[SizeTableMediaId] = [stmpr].[SizeTableMediaProductRegistry.SizeTableMediaId]
	WHERE
		[stm].[SizeTableId] = @SizeTableId
END
GO
