SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionGetById]
	@ConditionId int
as
begin
	select
		*
	from
		addon.vCustomCartContainsCondition
	where
		[Condition.Id] = @ConditionId
end
GO
