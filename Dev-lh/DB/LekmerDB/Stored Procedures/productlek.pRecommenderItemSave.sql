SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pRecommenderItemSave]
	@RecommenderItemId INT,
	@RecommenderListId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	-- update if exists
	UPDATE 
		[productlek].[tRecommenderItem]
	SET
		[RecommenderListId] = @RecommenderListId,
		[ProductId] = @ProductId,
		[UpdatedDate] = GETDATE()
	WHERE
		[RecommenderItemId] = @RecommenderItemId

	-- find and update if exists
	IF  @@ROWCOUNT = 0
	UPDATE 
		[productlek].[tRecommenderItem]
	SET
		@RecommenderItemId = [RecommenderItemId],
		[UpdatedDate] = GETDATE()
	WHERE
		[RecommenderListId] = @RecommenderListId
		AND
		([ProductId] = @ProductId OR [ProductId] IS NULL AND @ProductId IS NULL)

	-- insert			
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [productlek].[tRecommenderItem] (
			[RecommenderListId],
			[ProductId],
			[CreatedDate]
		)
		VALUES (
			@RecommenderListId,
			@ProductId,
			GETDATE()
		)

		SET @RecommenderItemId = SCOPE_IDENTITY()
	END

	RETURN @RecommenderItemId
END
GO
