SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentDelete] 
	@ModelFragmentId INT
AS 
BEGIN
    SET NOCOUNT ON	

--	DELETE [template].[tTemplateFragment] 
--	WHERE ModelFragmentId = @ModelFragmentId 

    DELETE  [template].[tModelFragment]
    WHERE   [ModelFragmentId] = @ModelFragmentId
    
    RETURN SCOPE_IDENTITY()
END
GO
