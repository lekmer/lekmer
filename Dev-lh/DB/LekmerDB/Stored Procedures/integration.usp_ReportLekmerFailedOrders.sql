
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ReportLekmerFailedOrders]
AS 
BEGIN
	SET NOCOUNT ON
	--BEGIN TRY
		--begin transaction
		
		SELECT
			OrderId,
			CreatedDate,
			Email,
			CASE ChannelId
			  WHEN 1 THEN 'Sweden'
			  WHEN 2 THEN 'Norway'
			  WHEN 3 THEN 'Denmark'
			  WHEN 4 THEN 'Finland'
			  WHEN 1000005 THEN 'Netherlands'
			END AS Channel
		FROM
			[order].tOrder
		WHERE
			--Createddate > DATEADD(day, -1, Convert(varchar, GETDATE(), 112))
		--and CreatedDate < GETDATE()
		--and 
			( OrderStatusId = 5
			  OR OrderStatusId = 6
			)
	

	--commit transaction
	--END TRY
	--BEGIN CATCH
		-- If transaction is active, roll it back.
		--IF @@trancount > 0 
		--	ROLLBACK TRANSACTION
		
	--END CATCH		 
END
GO
