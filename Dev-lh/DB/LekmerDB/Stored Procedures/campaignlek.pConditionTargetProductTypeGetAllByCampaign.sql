SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pConditionTargetProductTypeGetAllByCampaign]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[at].*
	FROM
		[campaignlek].[vConditionTargetProductType] at
		INNER JOIN [campaign].[tCondition] pa ON [pa].[ConditionId] = [at].[ConditionTargetProductType.ConditionId]
	WHERE
		[pa].[CampaignId] = @CampaignId
END
GO
