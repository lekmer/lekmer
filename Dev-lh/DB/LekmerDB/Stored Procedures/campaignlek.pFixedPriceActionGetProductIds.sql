SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionGetProductIds]
	@ActionId INT
AS
BEGIN
	DECLARE @IsIncludeAll BIT
	SET @IsIncludeAll = (SELECT [IncludeAllProducts] FROM [campaignlek].[tFixedPriceAction] WHERE [ProductActionId] = @ActionId)

	DECLARE @IncludedProducts TABLE (ProductId INT)
	DECLARE @ExcludedProducts TABLE (ProductId INT)

	INSERT INTO @ExcludedProducts ([ProductId])
	(
		SELECT DISTINCT [p].[ProductId]
		FROM [lekmer].[tLekmerProduct] p
		WHERE [p].[BrandId] IN (SELECT [eb].[BrandId]
								FROM [campaignlek].[tFixedPriceActionExcludeBrand] eb
								WHERE [eb].[ProductActionId] = @ActionId)

		UNION

		SELECT DISTINCT [ep].[ProductId]
		FROM [campaignlek].[tFixedPriceActionExcludeProduct] ep
		WHERE [ep].[ProductActionId] = @ActionId

		UNION

		SELECT DISTINCT [p].[ProductId]
		FROM [product].[tProduct] p 
		WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tFixedPriceActionExcludeCategory] ec
								   CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) src
								   WHERE [ec].[ProductActionId] = @ActionId)
	)

	IF (@IsIncludeAll = 1)
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		SELECT [ProductId] FROM [product].[tProduct]
	END
	ELSE
	BEGIN
		INSERT INTO @IncludedProducts ([ProductId])
		(
			SELECT DISTINCT [p].[ProductId]
			FROM [lekmer].[tLekmerProduct] p
			WHERE [p].[BrandId] IN (SELECT [ib].[BrandId]
									FROM [campaignlek].[tFixedPriceActionIncludeBrand] ib
									WHERE [ib].[ProductActionId] = @ActionId)

			UNION

			SELECT DISTINCT [ip].[ProductId]
			FROM [campaignlek].[tFixedPriceActionIncludeProduct] ip
			WHERE [ip].[ProductActionId]= @ActionId

			UNION

			SELECT DISTINCT [p].[ProductId]
			FROM [product].[tProduct] p 
			WHERE [p].[CategoryId] IN (SELECT DISTINCT [src].[CategoryId] FROM [campaignlek].[tFixedPriceActionIncludeCategory] ic
									   CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) src
									   WHERE [ic].[ProductActionId] = @ActionId)
		)
	END

	SELECT [ProductId] FROM @IncludedProducts
	EXCEPT
	SELECT [ProductId] FROM @ExcludedProducts
END
GO
