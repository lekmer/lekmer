SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_SetSentStatus]
	@UnubscriberId INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE 
		[lekmer].[tNewsletterUnsubscriber]
	SET
		[SentStatus] = 1
	WHERE
		[UnsubscriberId] = @UnubscriberId
END
GO
