SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderVoucherInfoGetByOrderId]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vOrderVoucherInfo] 
	WHERE
		[OrderVoucherInfo.OrderId] = @OrderId
END
GO
