
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateProductSeoDK]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO [product].[tProductSeoSetting] (ProductId)
		SELECT
			[ProductId]
		FROM
			[product].[tProduct]
		WHERE
			[ProductId] NOT IN (SELECT [ProductId] FROM [product].[tProductSeoSetting])

		-- DEFAULT --
		DECLARE @LanguageId INT
		SET @LanguageId = 1000002 -- Denmark
		
		INSERT INTO [product].[tProductSeoSettingTranslation] (ProductId, LanguageId)
		SELECT
			[p].[ProductId],
			@languageId
		FROM
			[product].[tProduct] p
		WHERE
			NOT EXISTS (SELECT 1 FROM [product].[tProductSeoSettingTranslation] n WHERE [n].[ProductId] = [p].[ProductId] AND [n].[LanguageId] = @LanguageId)


		------------------------------------------------------------
		-- <Produktnivå: Start Leksaker>
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = 'Køb ' + ISNULL(pt.Title, p.Title) + ' - fra Lekmer.dk',
			pss.[Description] = 'Køb ' + ISNULL(pt.Title, p.Title) + ' ' + ISNULL(ct.Title, c.Title) + ' på nettet. '
			+ 'Du finder også andet legetøj fra samme ' + ISNULL(bt.Title, b.Title) + ' hos Lekmer.dk.' 
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			------------------------------------------------------------------
			INNER JOIN product.tCategoryTranslation ct ON c.CategoryId = ct.CategoryId
		WHERE
			c3.CategoryId = 1000533 -- Leksaker
			AND pss.LanguageId = @LanguageId	
			AND ct.LanguageId = @LanguageId	
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Barn & Baby>
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = ISNULL(pt.Title, p.Title) + ' - Lekmer.dk – Køb bøarneartikler online',
			pss.[Description] = 'Kjøp ' + ISNULL(pt.Title, p.Title) + ' ' + ISNULL(bt.Title, b.Title) + ' på nettet. '
			+ 'Du kan også finde andre Barn & Baby produkter från ' + ISNULL(bt.Title, b.Title) + ' hos Lekmer.dk.' 
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
		WHERE
			c3.CategoryId = 1000445 -- Barn och Baby
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Barnkläder>
		------------------------------------------------------------
		UPDATE
			pss
		SET   
			pss.Title = ISNULL(pt.Title, p.Title) + ' ' + ISNULL(ct.Title, c.Title) + ' - Lekmer.dk – Baby og Børnetøj på nettet.',
			pss.[Description] = 'Køb ' + ISNULL(pt.Title, p.Title) + ' ' + ISNULL(ct.Title, c.Title) + ' og ' + ISNULL(ct2.Title, c2.Title) + ' på nettet. '
			+ 'Du også få andet børnetøj fra ' + ISNULL(bt.Title, b.Title) + ' kos Lekmer.dk.'
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			------------------------------------------------------------------
			INNER JOIN product.tCategoryTranslation ct ON c.CategoryId = ct.CategoryId
			INNER JOIN product.tCategoryTranslation ct2 ON c2.CategoryId = ct2.CategoryId
		WHERE
			c3.CategoryId = 1001310 -- Barnkläder
			AND pss.LanguageId = @LanguageId
			AND ct.LanguageId = @LanguageId
			AND ct2.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Inredning>
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = ISNULL(pt.Title, p.Title) + ' '
			+ ISNULL(ct.Title, c.Title) + ' ' + ISNULL(ct2.Title, c2.Title) + ' ' + ISNULL(ct3.Title, c3.Title) + ' på nettet hos Lekmer.dk',
			pss.[Description] = 'Køb ' + ISNULL(pt.Title, p.Title) + ' fra ' + ISNULL(bt.Title, b.Title) + ' på nettet ' 
			+ 'Du kan også købe andet ' + ISNULL(ct.Title, c.Title) + ' fra ' + ISNULL(bt.Title, b.Title) + ' hos Lekmer.dk.' 
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON pss.ProductId = l.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			LEFT JOIN product.tProductTranslation pt ON p.ProductId = pt.ProductId AND pt.LanguageId = @LanguageId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			LEFT JOIN lekmer.tBrandTranslation bt ON bt.BrandId = b.BrandId AND bt.LanguageId = @LanguageId	
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			------------------------------------------------------------------
			INNER JOIN product.tCategoryTranslation ct ON c.CategoryId = ct.CategoryId
			INNER JOIN product.tCategoryTranslation ct2 ON c2.CategoryId = ct2.CategoryId
			INNER JOIN product.tCategoryTranslation ct3 ON c3.CategoryId = ct3.CategoryId
		WHERE
			c3.CategoryId = 1000494 -- Inredning
			AND pss.LanguageId = @LanguageId
			AND ct.LanguageId = @LanguageId
			AND ct2.LanguageId = @LanguageId
			AND ct3.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
