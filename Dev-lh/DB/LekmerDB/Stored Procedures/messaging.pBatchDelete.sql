SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pBatchDelete]
@BatchId	uniqueidentifier
as
begin

	delete from 
		[messaging].[tBatch]
	where
		BatchId = @BatchId

end

GO
