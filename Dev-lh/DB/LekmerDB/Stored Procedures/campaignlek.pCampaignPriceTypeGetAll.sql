SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignPriceTypeGetAll]
AS
BEGIN
	SELECT 
		[cpv].*
	FROM 
		[campaignlek].[vCampaignPriceType] cpv
END
GO
