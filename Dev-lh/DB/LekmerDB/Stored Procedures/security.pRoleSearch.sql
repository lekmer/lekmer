SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pRoleSearch]
	@Title			nvarchar(50),
	@Page           int = NULL,
	@PageSize       int,
	@SortBy         varchar(20) = NULL,
	@SortDescending bit = NULL
AS 
BEGIN 
   SET nocount ON

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	IF (@Title IS NOT NULL)
	BEGIN
		SET @Title = generic.fPrepareSearchParameter(@Title)
	END

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' 
		+ COALESCE(@SortBy, 'Role.Id')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			*
		from
		   [security].[vCustomRole]'
		   + CASE WHEN (@Title IS NOT NULL) THEN + '
		WHERE [Role.Title] LIKE @Title' ELSE '' END
		
	SET @sql = 
		'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END

	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'@Title nvarchar(100)',
		@Title          
			     
	EXEC sp_executesql @sql, 
		N'@Title nvarchar(100)',
		@Title          
end

GO
