
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [test].[pProductSeoSettingInsert]
	@ProductId INT,
	@Title NVARCHAR(500),
	@Description NVARCHAR(500),
	@Keywords NVARCHAR(500)
AS
BEGIN
	INSERT INTO product.tProductSeoSetting
	(
		ProductId,
		Title,
		[Description],
		Keywords
	)
	VALUES
	(
		@ProductId,
		@Title,
		@Description,
		@Keywords
	)

	INSERT INTO product.tProductSeoSettingTranslation
	(
		ProductId,
		LanguageId,
		Title,
		[Description],
		Keywords
	)
	SELECT
		@ProductId,
		LanguageId,
		@Title,
		@Description,
		@Keywords
	FROM
		core.tLanguage
END
GO
