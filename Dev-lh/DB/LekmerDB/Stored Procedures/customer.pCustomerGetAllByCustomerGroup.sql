
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetAllByCustomerGroup] 
	@CustomerGroupId INT
AS	
BEGIN
	SET NOCOUNT ON

	SELECT
		[c].*	
	from
		[customer].[vCustomCustomer] c
		INNER JOIN [customer].[tCustomerGroupCustomer] cgc ON [c].[Customer.CustomerId] = [cgc].[CustomerId]
	WHERE
		[cgc].[CustomerGroupId] = @CustomerGroupId
END
GO
