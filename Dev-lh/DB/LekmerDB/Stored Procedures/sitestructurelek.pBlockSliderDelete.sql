SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSliderDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	EXEC [sitestructurelek].[pSliderGroupDelete] @BlockId
		
	DELETE
		[sitestructurelek].[tBlockSlider]
	WHERE
		[BlockId] = @BlockId
END
GO
