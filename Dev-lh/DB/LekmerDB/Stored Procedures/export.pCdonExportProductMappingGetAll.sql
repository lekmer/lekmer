SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportProductMappingGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[export].[tCdonExportProductMapping]
END
GO
