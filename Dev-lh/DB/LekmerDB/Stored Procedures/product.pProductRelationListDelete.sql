SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/**********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 15:00
Description: delete try - cath  block and error logging 
***********************  Version 1  **********************
User: Roman G.		Date: 12/29/2008		Time: 10:52:20
Description: Created 
*/

CREATE PROCEDURE [product].[pProductRelationListDelete]
	@ProductId		INT	
AS
BEGIN
	DELETE
		product.tProductRelationList			 
	WHERE 
		ProductId = @ProductId
END


GO
