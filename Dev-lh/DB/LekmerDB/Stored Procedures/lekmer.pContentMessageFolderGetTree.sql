SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT [cmf].*
	FROM [lekmer].[vContentMessageFolder] AS cmf
	WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT [cmf].*
		FROM [lekmer].[vContentMessageFolder] AS cmf
		WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT [cmf].[ContentMessageFolder.ParentContentMessageFolderId]
							 FROM [lekmer].[vContentMessageFolder] AS cmf
							 WHERE [cmf].[ContentMessageFolder.ContentMessageFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT [cmf].*
			FROM [lekmer].[vContentMessageFolder] AS cmf
			WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [lekmer].[vContentMessageFolder] AS cmf
								WHERE [cmf].[ContentMessageFolder.ParentContentMessageFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
