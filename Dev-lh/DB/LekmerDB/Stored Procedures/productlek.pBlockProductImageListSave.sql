SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListSave]
	@BlockId				INT,
	@ProductImageGroupId	INT
AS
BEGIN
	UPDATE
		[productlek].[tBlockProductImageList]
	SET
		[ProductImageGroupId] = @ProductImageGroupId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT = 0
		BEGIN
			INSERT [productlek].[tBlockProductImageList] (
				[BlockId],
				[ProductImageGroupId]
			)
			VALUES (
				@BlockId,
				@ProductImageGroupId
			)
		END
END
GO
