SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetAllByContentArea]
	@ContentAreaId	int
as
begin
	select 
		B.*
	from 
		[sitestructure].[vCustomBlockSecure] as B
	where
		[Block.ContentAreaId] = @ContentAreaId
end

GO
