
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGetNewOrdersByList]
	@MaxAllowedFailedAttempts	INT,
	@SleepIntervallInMinutes	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT DISTINCT
		[o].[OrderId]
	FROM
		[order].[tOrder] o
		LEFT JOIN [integration].[tOrderQue] q ON [o].[OrderId] = [q].[OrderId]
	WHERE
		[o].[OrderStatusId] = 2 -- Payment confirmed
		AND
		(
			[q].[OrderId] IS NULL			
			OR 
			(
				[q].[OrderId] IS NOT NULL
				AND
				[q].[NumberOfFailedAttempts] < @MaxAllowedFailedAttempts
				AND
				DATEADD(minute, @SleepIntervallInMinutes, [q].[LastFailedAttempt]) < GETDATE()
			)
		)
END
GO
