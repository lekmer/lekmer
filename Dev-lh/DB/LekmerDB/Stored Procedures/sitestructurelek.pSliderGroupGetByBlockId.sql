SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pSliderGroupGetByBlockId]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[SliderGroupId],
		[BlockId],
		[ImageId1],
		[ThumbnailImageId1],
		[InternalLinkContentNodeId1],
		[ExternalLink1],
		[IsExternalLinkMovie1],
		[ImageId2],
		[ThumbnailImageId2],
		[InternalLinkContentNodeId2],
		[ExternalLink2],
		[IsExternalLinkMovie2],
		[Title1],
		[Title1Size],
		[Title1Color],
		[Title1Link],
		[Title2],
		[Title2Size],
		[Title2Color],
		[Title2Link],
		[Title3],
		[Title3Size],
		[Title3Color],
		[Title3Link],
		[Description],
		[DescriptionSize],
		[DescriptionColor],
		[Disclaimer],
		[DisclaimerSize],
		[DisclaimerColor],
		[Button1Text],
		[Button1Size],
		[Button1Color],
		[Button1Link],
		[Button2Text],
		[Button2Size],
		[Button2Color],
		[Button2Link],
		[Button3Text],
		[Button3Size],
		[Button3Color],
		[Button3Link],
		[DiscountText],
		[DiscountSize],
		[DiscountColor],
		[DiscountPosition],
		[ExtraText],
		[ExtraSize],
		[ExtraColor],
		[ExtraPosition],
		[BackgroundColor1],
		[BackgroundColor2],
		[BackgroundGradient],
		[HolderColor],
		[HolderAlpha],
		[HolderShape],
		[HolderTextAlign],
		[Style],
		[Ordinal],
		[Captions],
		[CampaignId],
		[CampaignTypeId],
		[IsActive],
		[UseCampaignDates],
		[StartDateTime],
		[EndDateTime]
	FROM
		[sitestructurelek].[tSliderGroup]
	WHERE
		[BlockId] = @BlockId
	ORDER BY
		[Ordinal]
END
GO
