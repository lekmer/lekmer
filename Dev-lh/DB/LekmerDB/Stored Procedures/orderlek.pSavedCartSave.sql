SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pSavedCartSave]
	@CartId		INT,
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [orderlek].[tSavedCart] (
		[CartId],
		[ChannelId],
		[Email]
	)
	VALUES (
		@CartId,
		@ChannelId,
		@Email
	)
END
GO
