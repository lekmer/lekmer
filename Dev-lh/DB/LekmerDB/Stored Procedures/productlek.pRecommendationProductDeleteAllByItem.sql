SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pRecommendationProductDeleteAllByItem]
	@RecommenderItemId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[productlek].[tRecommendationProduct]
	WHERE
		[RecommenderItemId] = @RecommenderItemId
END
GO
