SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListCategory]
	WHERE [BlockId] = @BlockId
END
GO
