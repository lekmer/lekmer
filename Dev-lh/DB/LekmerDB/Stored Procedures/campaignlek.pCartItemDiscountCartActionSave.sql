
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionSave]
	@CartActionId				INT,
	@Quantity					INT,
	@ApplyForCheapest			BIT,
	@IsPercentageDiscount		BIT,
	@PercentageDiscountAmount	DECIMAL(16,2),
	@IncludeAllProducts			BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartItemDiscountCartAction]
	SET
		Quantity = @Quantity,
		ApplyForCheapest = @ApplyForCheapest,
		IsPercentageDiscount = @IsPercentageDiscount,
		PercentageDiscountAmount = @PercentageDiscountAmount,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tCartItemDiscountCartAction] (
			CartActionId,
			Quantity,
			ApplyForCheapest,
			IsPercentageDiscount,
			PercentageDiscountAmount,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@Quantity,
			@ApplyForCheapest,
			@IsPercentageDiscount,
			@PercentageDiscountAmount,
			@IncludeAllProducts
		)
	END
END
GO
