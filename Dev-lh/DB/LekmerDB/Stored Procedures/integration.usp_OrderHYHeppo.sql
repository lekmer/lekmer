SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_OrderHYHeppo]

AS
begin
	set nocount on
	begin try
	
		
		--declare @Return int
		--set @Return = 1
			
		-- ändra if-satserna
		--if @Return = 1
		--begin
			SELECT DISTINCT 
			c.CustomerId, o.OrderId,c.FirstName, c.LastName,
            c.CivicNumber as Personnummer, c.PhoneNumber as Telefonnummer,
            c.CellPhoneNumber as Mobil, o.Email, a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, a.City, a.PostalCode, l.ISO as Land, 
            o.OrderStatusId, o.FreightCost, o.ChannelId, z.OrderPaymentId, 
            v.ErpId, z.ReferenceId, z.Price 
            FROM 
            [customer].tCustomerInformation c
				inner join [order].torder o
					on c.CustomerId = o.CustomerId
				inner join [order].tOrderAddress a  
					on o.OrderStatusId = 2
				inner join [core].tCountry l
					on l.CountryId = a.CountryId
				inner join [order].tOrderPayment z
					on z.OrderId = o.OrderId
				inner join [order].tPaymentType v 
					on z.PaymentTypeId = v.PaymentTypeId
            WHERE 
                (a.OrderAddressId = o.BillingAddressId or a.OrderAddressId = o.DeliveryAddressId)
		--end
		-- if customer id is sent search after a specific code valid only to the id
		-- and see if it is still valid
		--else if @Return > 0
		--begin
			
				
		--end
		
	
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		--return @Return
	end catch		 
end
GO
