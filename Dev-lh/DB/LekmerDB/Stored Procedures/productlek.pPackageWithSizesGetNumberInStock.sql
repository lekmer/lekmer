SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageWithSizesGetNumberInStock]
	@ItemsWithoutSizes		VARCHAR(max),
	@ItemsWithSizes			XML,
	@Delimiter				CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @ItemsWithoutSizesNumInStock INT
	DECLARE @ItemsWithSizesNumInStock INT

	-- products without sizes 
	DECLARE @tmpPackageProductWithoutSizes TABLE (ProductId INT)
	INSERT INTO @tmpPackageProductWithoutSizes (ProductId)
	SELECT [Id] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ItemsWithoutSizes, @Delimiter)
	
	SET @ItemsWithoutSizesNumInStock =
	(SELECT
		MIN([p].[NumberInStock])
	FROM 
		[product].[tProduct] p
		INNER JOIN @tmpPackageProductWithoutSizes pp ON [pp].[ProductId] = [p].[ProductId])
	
	
	-- products with sizes 
	DECLARE @tmpPackageProductWithSizes TABLE (ProductId INT, SizeId INT)
	INSERT INTO @tmpPackageProductWithSizes (ProductId, [SizeId])
	SELECT c.value('@productId[1]', 'int'), c.value('@sizeId[1]', 'int') FROM @ItemsWithSizes.nodes('/items/item') T(c)
	
	SET @ItemsWithSizesNumInStock =
	(SELECT
		MIN([ps].[NumberInStock])
	FROM 
		[lekmer].[tProductSize] ps
		INNER JOIN @tmpPackageProductWithSizes pp ON [pp].[ProductId] = [ps].[ProductId] AND [pp].[SizeId] = [ps].[SizeId])
		
	RETURN CASE WHEN @ItemsWithoutSizesNumInStock < @ItemsWithSizesNumInStock THEN @ItemsWithoutSizesNumInStock ELSE @ItemsWithSizesNumInStock END
END
GO
