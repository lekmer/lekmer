SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [product].[pProductGetViewAllByIdList]
	@ChannelId		int,
	@CustomerId		int,
	@ProductIds		varchar(max),
	@Delimiter		char(1),
	@Page			int = NULL,
	@PageSize		int,
	@SortBy varchar(20) = NULL,
	@SortDescending bit = NULL
AS
BEGIN

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFilter nvarchar(max)
	
	DECLARE @CustomerIdString VARCHAR(10)
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		[p].*,
		pli.*
		FROM [product].[vCustomProductView] p
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](''' + @ProductIds + ''', ''' + @Delimiter+ ''') AS pl 
			ON pl.Id = p.[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				' + @CustomerIdString + '
			)
		WHERE
			p.[Product.ChannelId] = ' +  CAST(@ChannelId AS varchar(10)) + '
	)'

	SET @sql = '
		SELECT * FROM
		' + @sqlFilter + '
		AS SearchResult'
	SET @sqlCount = '
		SELECT COUNT(1) FROM
		' + @sqlFilter + '
		AS SearchResultsCount'
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
		AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
