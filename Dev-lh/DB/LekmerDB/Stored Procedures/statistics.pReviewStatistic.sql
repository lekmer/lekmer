
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pReviewStatistic]
	@ChannelId	INT,
	@DateFrom	DATETIME,
	@DateTo		DATETIME
AS
BEGIN
	DECLARE @tReviewStatistic TABLE (
		TotalReviews INT,
		ReviewsInPeriod INT,
		ApprovedReviewsInPeriod INT
	)

	-- How many reviews total on site
	INSERT INTO @tReviewStatistic ([TotalReviews])
	SELECT COUNT(1)
	FROM [review].[tRatingReviewFeedback] rrf
	WHERE
		[rrf].[ChannelId] = @ChannelId
		AND [rrf].[RatingReviewStatusId] = 5
	
	-- How many reviews submitted by customer in period X
	UPDATE @tReviewStatistic
	SET [ReviewsInPeriod] = (
		SELECT COUNT(1)
		FROM [review].[tRatingReviewFeedback] rrf
		WHERE
			[rrf].[ChannelId] = @ChannelId
			AND [rrf].[CreatedDate] >= @DateFrom
			AND [rrf].[CreatedDate] <= @DateTo)

	-- How many reviews approved/published by Lekmer in period X
	UPDATE @tReviewStatistic
	SET [ApprovedReviewsInPeriod] = (
		SELECT COUNT(1)
		FROM [review].[tRatingReviewFeedback] rrf
		WHERE
			[rrf].[ChannelId] = @ChannelId
			AND [rrf].[CreatedDate] >= @DateFrom
			AND [rrf].[CreatedDate] <= @DateTo
			AND [rrf].[RatingReviewStatusId] = 5)

	SELECT * FROM @tReviewStatistic
END
GO
