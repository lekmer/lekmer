
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure  [lekmer].[pWishListGetbyKey]
	@Key uniqueidentifier
as
begin

	select *
	from
		[lekmer].[tWishList] 
	where
		WishListKey = @Key		
end
GO
