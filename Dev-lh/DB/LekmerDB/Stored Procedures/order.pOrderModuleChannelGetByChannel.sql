SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderModuleChannelGetByChannel]
@ChannelId	INT
AS
BEGIN
	SELECT
		*
	FROM
		[order].[vCustomOrderModuleChannel]
	WHERE 
		[OrderModuleChannel.ChannelId] = @ChannelId
END

GO
