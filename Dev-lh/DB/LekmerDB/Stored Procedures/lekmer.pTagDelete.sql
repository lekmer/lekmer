
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagDelete]
	@TagId	INT
AS
BEGIN 
	EXEC [lekmer].[pProductTagUsageDelete] NULL, @TagId, NULL
	
	EXEC [productlek].[pTagFlagDeleteByTag] @TagId	
	
	DELETE FROM lekmer.tProductTag
	WHERE TagId = @TagId
	
	DELETE FROM lekmer.tTagTranslation
	WHERE TagId = @TagId
	
	DELETE FROM lekmer.tTag
	WHERE TagId = @TagId
END 

GO
