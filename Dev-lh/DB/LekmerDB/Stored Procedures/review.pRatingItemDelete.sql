
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemDelete]
	@RatingItemId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [review].[pRatingItemTranslationDelete] @RatingItemId

	DELETE FROM [review].[tRatingItem]
	WHERE [RatingItemId] = @RatingItemId
END
GO
