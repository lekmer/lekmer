
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionSave]
	@ProductActionId	INT,
	@DiscountAmount		DECIMAL(16, 2),
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaign].[tPercentagePriceDiscountAction]
	SET
		DiscountAmount = @DiscountAmount,
		ConfigId = @ConfigId
	WHERE
		ProductActionId = @ProductActionId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaign].[tPercentagePriceDiscountAction] (
			ProductActionId,
			DiscountAmount,
			ConfigId
		)
		VALUES (
			@ProductActionId,
			@DiscountAmount,
			@ConfigId
		)
	END
END
GO
