
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pWishListDelete]
	@Id	INT
AS
BEGIN
	DELETE FROM [lekmer].[tWishList] WHERE [WishListId] = @Id
END
GO
