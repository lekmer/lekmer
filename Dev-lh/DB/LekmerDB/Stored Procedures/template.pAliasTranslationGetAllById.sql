
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pAliasTranslationGetAllById]
	@AliasId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[template].[vCustomAliasTranslation]
	WHERE
		[Alias.Id] = @AliasId
	ORDER BY
		[Language.Id]
END

GO
