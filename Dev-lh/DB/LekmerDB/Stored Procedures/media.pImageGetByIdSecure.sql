SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pImageGetByIdSecure] 
	@MediaId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT *
	FROM   [media].[vCustomImageSecure]
	WHERE  [Image.MediaId] = @MediaId
END

GO
