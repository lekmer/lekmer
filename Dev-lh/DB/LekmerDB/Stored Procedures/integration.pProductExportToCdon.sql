
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductExportToCdon]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000445, 0, 1)	-- Barn och Baby	
	insert into @Tree values (1000494, 0, 1)	-- Inredning
	insert into @Tree values (1000533, 0, 1)	-- Leksaker
	insert into @Tree values (1000856, 0, 1)	-- Underhållning
		
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
		
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 	   
	   c.ParentCategoryId,	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0


   -- Import products using the same third level category	
   if object_id('tempdb..#OnlineCategory') is not null
       drop table #OnlineCategory

   create table #OnlineCategory
   (
      CategoryId int primary key
   )
   
   insert into #OnlineCategory
   select distinct CategoryId
   from product.tProduct p
   where p.ProductStatusId = 0

   if object_id('tempdb..#CategoryLevel') is not null
       drop table #CategoryLevel

   create table #CategoryLevel
   (
      CategoryId int primary key,
      ErpId varchar(13),
      Level1Id int,
      Level2Id int,
      Level3Id int
   )
   	
   insert into #CategoryLevel ( CategoryId, ErpId, Level1Id, Level2Id, Level3Id )
   select c.CategoryId, 
      c.ErpId, 
      convert(int, substring(erpId, 3, 2)) as Level1Id, 
      convert(int, substring(erpId, 6, 3)) as Level2Id, 
      convert(int, substring(erpId, 10, 4)) as Level3Id
   from #OnlineCategory oc
		inner join product.tCategory c on c.CategoryId = oc.CategoryId
   where len(c.ErpId) = 13
 

   if object_id('tempdb..#CategoryMapping') is not null
       drop table #CategoryMapping

   create table #CategoryMapping
   (
      SourceCategoryId int primary key with (ignore_dup_key = on),
      DestinationCategoryId int
   )	

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and cl2.Level2Id = cl.Level2Id
         and not cl2.Level1Id in (30)
   where cl.Level1Id = 30     

   insert into #CategoryMapping ( SourceCategoryId, DestinationCategoryId )
   select cl2.CategoryId, cl.CategoryId
   from #CategoryLevel cl
      inner join #CategoryLevel cl2 on cl2.Level3Id = cl.Level3Id
         and not cl2.Level1Id in (30)
      inner join product.tCategory c on c.CategoryId = cl.CategoryId
		and not c.Title in ('Övrigt', 'Tillbehör och accessoarer', 'Blandade titlar', 'Reservdelar och tillbehör',
			'Plocklådor')
   where cl.Level1Id = 30


--   if object_id('tempdb..#CategoryMappingPath') is not null
--       drop table #CategoryMappingPath
   
--select cm.DestinationCategoryId, ppc.Title + '/' + pc.Title + '/' + c.Title as Path1, 
--   cm.SourceCategoryId, ppc2.Title + '/' +  pc2.Title + '/' +  c2.Title as Path2
----   ,p.Title
--into #CategoryMappingPath
--from #CategoryMapping cm
--   inner join product.tCategory c on c.CategoryId = cm.DestinationCategoryId
--   inner join product.tCategory pc on pc.CategoryId = c.ParentCategoryId
--   inner join product.tCategory ppc on ppc.CategoryId = pc.ParentCategoryId      
--   inner join product.tCategory c2 on c2.CategoryId = cm.SourceCategoryId
--   inner join product.tCategory pc2 on pc2.CategoryId = c2.ParentCategoryId
--   inner join product.tCategory ppc2 on ppc2.CategoryId = pc2.ParentCategoryId      
   

--select cmp.*, p.ProductId, p.Title
--from #CategoryMappingPath cmp 
--   inner join product.tProduct p on p.CategoryId = cmp.DestinationCategoryId
--where p.ProductStatusId = 0
--order by cmp.Path1, cmp.Path2, p.Title
  
   drop table #CategoryLevel
	
	--insert into @Product
	--select p.ProductId
	--from #CategoryMapping cm
	--   inner join product.tProduct p on p.CategoryId = cm.SourceCategoryId
	--where p.IsDeleted = 0

	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)

	delete tp
	from @Product tp
		inner join lekmer.tProductSize ps on ps.ProductId = tp.ProductId
	
	-- Brand Restrictions 
	-- Besafe
	delete from @Product where ProductId in (Select ProductId from lekmer.tLekmerProduct where BrandId = 1000588)
	-- Crescent
	delete from @Product where ProductId in (Select ProductId from lekmer.tLekmerProduct where BrandId = 1000565)
		
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,case 
				when (cm.DestinationCategoryId is not null) then cm.DestinationCategoryId 			
			else 
				p.CategoryId 
			end as CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on t.ProductId = lp.ProductId
          left outer join #CategoryMapping cm on cm.SourceCategoryId = p.CategoryId
			
	  drop table #CategoryMapping

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title


	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join export.tProductPrice pp on pp.ChannelId = c.ChannelId	
			and pp.ProductId = pr.ProductId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	-- Price List Price
	select pp.ChannelId, pp.ProductId, 
	   case when (pp.ChannelId = 4) then pp.PriceIncludingVat else ROUND(pp.PriceIncludingVat, 0) end as Price, 
	   case when (pp.ChannelId = 4) then pp.PriceIncludingVat else ROUND(pp.PriceIncludingVat, 0) end * pli.VatPercentage / (100 + pli.VatPercentage) as Vat, 
	   case when (pp.DiscountPriceIncludingVat is not null and pp.ChannelId <> 4) then ROUND(pp.DiscountPriceIncludingVat, 0) else DiscountPriceIncludingVat end as DiscountPrice, 
	   case when (pp.DiscountPriceIncludingVat is not null and pp.ChannelId <> 4) then ROUND(pp.DiscountPriceIncludingVat, 0) else DiscountPriceIncludingVat end * pli.VatPercentage / (100 + pli.VatPercentage) as DiscountPriceVat
	--into temp.CdonPriceNewCalc6New
	from export.tProductPrice pp
		inner join @Product t on t.ProductId = pp.ProductId
		inner join @PriceList pl on pl.ChannelId = pp.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
			and pli.ProductId = pp.ProductId
--	drop table temp.CdonPriceNewCalc6New

	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
    -- Icons				   
	SELECT [lpi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON [lpi].[ProductId] = [p].[ProductId]
	UNION
	SELECT [bi].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[BrandId] = [lp].[BrandId]
	UNION
	SELECT [ci].[IconId], [p].[ProductId]
	FROM @Product p
		 INNER JOIN [product].[tProduct] p1 ON [p1].[ProductId] = [p].[ProductId]
		 INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](p1.CategoryId))
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
