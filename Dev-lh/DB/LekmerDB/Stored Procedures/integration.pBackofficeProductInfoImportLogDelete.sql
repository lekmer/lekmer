SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pBackofficeProductInfoImportLogDelete]
	@Days INT
AS 
BEGIN
	SET NOCOUNT ON
	
	-- Current date.
	DECLARE	@CurrentDate AS DATETIME
	SET @CurrentDate = GETDATE()
	
	-- Deletion date.
	DECLARE	@DeletionDate AS DATETIME
	--SET @DeletionDate = DATEADD(day, -@Days, @CurrentDate)
	
	/* PT55880074 - Strange product titles
	   Lets keep logs for one year
	*/
	SET @DeletionDate = DATEADD(year, -1, @CurrentDate)
	
	-- Delete old rows.
	DELETE FROM
		[integration].[tBackofficeProductInfoImport]
	WHERE
		[InsertedDate] < @DeletionDate	 
END
GO
