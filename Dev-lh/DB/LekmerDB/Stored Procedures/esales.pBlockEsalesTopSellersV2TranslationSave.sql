SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2TranslationSave]
	@BlockId	INT,
	@LanguageId	INT,
	@UrlTitle	NVARCHAR(max)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[esales].[tBlockEsalesTopSellersV2Translation]
	SET
		[UrlTitle] = @UrlTitle
	WHERE
		[BlockId] = @BlockId
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [esales].[tBlockEsalesTopSellersV2Translation] (
			[BlockId],
			[LanguageId],
			[UrlTitle]
		)
		VALUES (
			@BlockId,
			@LanguageId,
			@UrlTitle
		)
	END
END
GO
