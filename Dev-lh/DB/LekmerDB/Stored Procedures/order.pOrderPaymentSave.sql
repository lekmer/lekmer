
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderPaymentSave]
	@OrderPaymentId INT,
	@OrderId INT,
	@PaymentTypeId INT,
	@Price DECIMAL(16,2),
	@Vat DECIMAL(16,2),
	@ReferenceId VARCHAR(50),
	@Captured BIT = NULL,
	@KlarnaEID INT = NULL,
	@KlarnaPClass INT = NULL,
	@MaksuturvaCode VARCHAR(50) = NULL,
	@QliroClientRef VARCHAR(50) = NULL,
	@QliroPaymentCode VARCHAR(50) = NULL,
	@CollectorStoreId INT = NULL,
	@CollectorPaymentCode VARCHAR(50) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderPayment]
	SET
		[OrderId] = @OrderId,
		[PaymentTypeId] = @PaymentTypeId,
		[Price] = @Price,
		[Vat] = @VAT,
		[ReferenceId] = @ReferenceId,
		[Captured] = @Captured,
		[KlarnaEID] = @KlarnaEID,
		[KlarnaPClass] = @KlarnaPClass,
		[MaksuturvaCode] = @MaksuturvaCode,
		[QliroClientRef] = @QliroClientRef,
		[QliroPaymentCode] = @QliroPaymentCode,
		[CollectorStoreId] = @CollectorStoreId,
		[CollectorPaymentCode] = @CollectorPaymentCode
	WHERE
		[OrderPaymentId] = @OrderPaymentId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderPayment]
		(
			[OrderId],
			[PaymentTypeId],
			[Price],
			[Vat],
			[ReferenceId],
			[Captured],
			[KlarnaEID],
			[KlarnaPClass],
			[MaksuturvaCode],
			[QliroClientRef],
			[QliroPaymentCode],
			[CollectorStoreId],
			[CollectorPaymentCode]
		)
		VALUES
		(
			@OrderId,
			@PaymentTypeId,
			@Price,
			@Vat,
			@ReferenceId,
			@Captured,
			@KlarnaEID,
			@KlarnaPClass,
			@MaksuturvaCode,
			@QliroClientRef,
			@QliroPaymentCode,
			@CollectorStoreId,
			@CollectorPaymentCode
		)

		SET @OrderPaymentId = CAST(SCOPE_IDENTITY() AS INT)
	END

	RETURN @OrderPaymentId
END
GO
