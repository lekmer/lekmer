SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemDiscountCartActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END

GO
