SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageTranslationSave]
	@PackageId		INT,
	@LanguageId		INT,
	@GeneralInfo	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[productlek].[tPackageTranslation]
	SET
		[GeneralInfo] = @GeneralInfo
	WHERE
		[PackageId] = @PackageId AND
		[LanguageId] = @LanguageId

	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [productlek].[tPackageTranslation] (
			[PackageId],
			[LanguageId],
			[GeneralInfo]
		)
		VALUES (
			@PackageId,
			@LanguageId,
			@GeneralInfo
		)
	END
END
GO
