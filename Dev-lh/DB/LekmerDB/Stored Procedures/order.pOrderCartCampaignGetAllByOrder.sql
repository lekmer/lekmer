SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderCartCampaignGetAllByOrder]
	@OrderId	INT
AS
BEGIN
	SELECT
		*
	FROM 
		[order].[vCustomOrderCartCampaign]
	WHERE 
		[OrderCampaign.OrderId] = @OrderId
END

GO
