SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionConfiguratorGetById]
	@ConfiguratorId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCampaignActionConfigurator]
	WHERE
		[CampaignActionConfigurator.CampaignActionConfiguratorId] = @ConfiguratorId
END
GO
