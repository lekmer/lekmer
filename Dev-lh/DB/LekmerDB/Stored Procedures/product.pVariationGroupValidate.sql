SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE procedure [product].[pVariationGroupValidate]
	@VariationGroupId	int,
	@Title				nvarchar(256)
as
begin
	set nocount off

	if exists (select 1 from [product].[tVariationGroup] where [Title] = @Title AND [VariationGroupId] <> @VariationGroupId )
		return -1	
	return 0
end





GO
