
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowGetAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		st.*
	FROM
		[lekmer].[vSizeTableRowSecure] st
	WHERE
		st.[SizeTableRow.SizeTableId] = @SizeTableId
END
GO
