
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemSave]
	@RatingItemId INT,
	@RatingId INT,
	@Title NVARCHAR(50),
	@Description NVARCHAR(255),
	@Score INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[review].[tRatingItem]
	SET 
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[RatingItemId] = @RatingItemId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingItem] (
			[RatingId],
			[Title],
			[Description],
			[Score]
		)
		VALUES (
			@RatingId,
			@Title,
			@Description,
			@Score
		)

		SET @RatingItemId = SCOPE_IDENTITY()
	END

	RETURN @RatingItemId
END
GO
