SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pImportProductStockStoreLekmer]

AS
BEGIN	
		if object_id('tempdb..#tImportProductStockStoreLekmer') is not null
		drop table #tImportProductStockStoreLekmer
		
		create table #tImportProductStockStoreLekmer
		(
			ProductId int null,
			SizeId int  null,
			LagerProductTitle nvarchar(200) COLLATE Finnish_Swedish_CI_AS not null,
			LagerAmmount int not null
		)

		-- Insert all new rows into temp table
		insert into #tImportProductStockStoreLekmer(ProductId, SizeId, LagerProductTitle, LagerAmmount)
		select
			p.ProductId,
			s.[Size.Id],
			tp.ArticleTitle,
			tp.NoInStock
		from 
			[integration].[tempProduct] tp
		left join  [product].[tProduct] p
		on
			p.ErpId = SUBSTRING([tp].[HYarticleId], 5, 12)
		left join [lekmer].[vSize] s
		on
			s.[Size.ErpTitle] = tp.SizeId
		where SUBSTRING([tp].[HYarticleId], 1, 3) = '101' -- only for phisical store

		truncate table [product].[tStoreProductSize]

		insert into [product].[tStoreProductSize] (ProductId, SizeId, Quantity)
		select 
			s.ProductId, s.SizeId, s.LagerAmmount
		from
			#tImportProductStockStoreLekmer s
		where (s.ProductId is not null)and(s.SizeId is not null)

			

END


GO
