SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE
		CartActionId = @CartActionId
END


GO
