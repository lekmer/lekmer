SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pProductRelationListDeleteAllByRelationList]
	@RelationListId		INT	
AS
BEGIN
	DELETE
		product.tProductRelationList			 
	WHERE 
		RelationListId = @RelationListId
END


GO
