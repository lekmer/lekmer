
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerUserDelete]
	@CustomerId INT
AS	
BEGIN
	SET NOCOUNT ON	
	DELETE	[customer].[tCustomerUser]	
	WHERE   CustomerId = @CustomerId
END
GO
