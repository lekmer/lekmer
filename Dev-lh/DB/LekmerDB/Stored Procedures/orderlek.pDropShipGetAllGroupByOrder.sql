
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDropShipGetAllGroupByOrder]
AS 
BEGIN 
	SELECT *
	FROM (SELECT
			ROW_NUMBER() OVER (PARTITION BY [ds].[DropShip.OrderId] ORDER BY [ds].[DropShip.OrderId]) AS Number,
			[ds].*
		  FROM
			[orderlek].[vDropShip] ds) temp
	WHERE [temp].[Number] = 1
END
GO
