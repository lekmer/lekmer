
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetByProduct]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @SizeTableId INT
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeProduct] WHERE [ProductId] = @ProductId ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	/*--------------------------------------------------------------------------------------------------------------------------*/
	
	DECLARE @BrandId INT, @CategoryThirdLevelId INT, @CategorySecondLevelId INT, @CategoryFirstLevelId INT
	
	SET @BrandId = (SELECT [BrandId] FROM [lekmer].[tLekmerProduct] WHERE [ProductId] = @ProductId)
	IF (@BrandId IS NOT NULL)
	BEGIN
		SET @CategoryThirdLevelId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
		-- Size table include category 3 level
		SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand]
							WHERE [CategoryId] = @CategoryThirdLevelId AND [BrandId] = @BrandId 
							ORDER BY Id DESC)
		IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
			RETURN @SizeTableId

		SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
		-- Size table include category 2 level
		SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
							WHERE [CategoryId] = @CategorySecondLevelId AND [BrandId] = @BrandId
							ORDER BY Id DESC)
		IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
			RETURN @SizeTableId

		SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)
		-- Size table include category 1 level
		SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
							WHERE [CategoryId] = @CategoryFirstLevelId AND [BrandId] = @BrandId
							ORDER BY Id DESC)
		IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
			RETURN @SizeTableId
	END
	
	/*--------------------------------------------------------------------------------------------------------------------------*/

	IF (@CategoryThirdLevelId IS NULL)
		SET @CategoryThirdLevelId = (SELECT [CategoryId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)
	-- Size table include category 3 level
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand]
						WHERE [CategoryId] = @CategoryThirdLevelId AND [BrandId] IS NULL 
						ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	IF (@CategorySecondLevelId IS NULL)
		SET @CategorySecondLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategoryThirdLevelId)
	-- Size table include category 2 level
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
						WHERE [CategoryId] = @CategorySecondLevelId AND [BrandId] IS NULL
						ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId

	IF (@CategoryFirstLevelId IS NULL)
		SET @CategoryFirstLevelId = (SELECT [ParentCategoryId] FROM [product].[tCategory] WHERE [CategoryId] = @CategorySecondLevelId)
	-- Size table include category 1 level
	SET @SizeTableId = (SELECT TOP(1) [SizeTableId] FROM [lekmer].[tSizeTableIncludeCategoryBrand] 
						WHERE [CategoryId] = @CategoryFirstLevelId AND [BrandId] IS NULL
						ORDER BY Id DESC)
	IF @SizeTableId IS NOT NULL AND @SizeTableId > 0
		RETURN @SizeTableId
END
GO
