SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pRecommenderListGetBySessionAndProduct]
	@SessionId VARCHAR(50),
	@ProductId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		rl.*
	FROM
		[productlek].[vRecommenderList] rl
	WHERE
		rl.[RecommenderList.SessionId] = @SessionId
		OR
		rl.[RecommenderList.SessionId] IS NULL AND @SessionId IS NULL
		
	SELECT
		ri.*
	FROM
		[productlek].[vRecommenderList] rl
		INNER JOIN [productlek].[vRecommenderItem] ri ON ri.[RecommenderItem.RecommenderListId] = rl.[RecommenderList.RecommenderListId]
	WHERE
		(
			rl.[RecommenderList.SessionId] = @SessionId
			OR
			rl.[RecommenderList.SessionId] IS NULL AND @SessionId IS NULL
		)
		AND
		(
			ri.[RecommenderItem.ProductId] = @ProductId
			OR
			ri.[RecommenderItem.ProductId] IS NULL AND @ProductId IS NULL
		)
		
	SELECT
		rp.*
	FROM
		[productlek].[vRecommenderList] rl
		INNER JOIN [productlek].[vRecommenderItem] ri ON ri.[RecommenderItem.RecommenderListId] = rl.[RecommenderList.RecommenderListId]
		INNER JOIN [productlek].[vRecommendationProduct] rp ON rp.[RecommendationProduct.RecommenderItemId] = ri.[RecommenderItem.RecommenderItemId]
	WHERE
		(
			rl.[RecommenderList.SessionId] = @SessionId
			OR
			rl.[RecommenderList.SessionId] IS NULL AND @SessionId IS NULL
		)
		AND
		(
			ri.[RecommenderItem.ProductId] = @ProductId
			OR
			ri.[RecommenderItem.ProductId] IS NULL AND @ProductId IS NULL
		)

END
GO
