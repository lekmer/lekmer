SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingTranslationDelete]
	@RatingId		INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE [review].[tRatingTranslation]
	WHERE [RatingId] = @RatingId
END
GO
