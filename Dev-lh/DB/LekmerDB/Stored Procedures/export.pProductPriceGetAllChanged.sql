
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pProductPriceGetAllChanged]
	@IsChangedFlagRemove BIT
AS 
BEGIN 
	IF (SELECT COUNT(1) FROM [export].[vProductPrice] pp WHERE [pp].[ProductPrice.IsChanged] = 1) > 65536
	BEGIN
		RETURN
	END

	SELECT
		[c].[CommonName] 'Channel',
		[lp].[HYErpId] 'HYErpId',
		[lp].[SupplierArticleNumber] 'SupplierArtNumber',
		[p].[Title]	'Title',
		[pp].[ProductPrice.PriceIncludingVat] 'OriginalPriceIncludingVat',
		[pp].[ProductPrice.PriceExcludingVat] 'OriginalPriceExcludingVat',
		[pp].[ProductPrice.DiscountPriceIncludingVat] 'DiscountPriceIncludingVat',
		[pp].[ProductPrice.DiscountPriceExcludingVat] 'DiscountPriceExcludingVat'
	FROM
		[export].[vProductPrice] pp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [pp].[ProductPrice.ProductId]
		INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [pp].[ProductPrice.ProductId]
		INNER JOIN [core].[tChannel] c ON [c].[ChannelId] = [pp].[ProductPrice.ChannelId]
	WHERE
		[pp].[ProductPrice.IsChanged] = 1
		AND (
			[p].[ProductStatusId] = 0 -- Online
			OR [lp].[StockStatusId] = 0 -- Active
		)

	IF (@IsChangedFlagRemove = 1)
	BEGIN
		UPDATE [export].[tProductPrice]
		SET [IsChanged] = 0
	END
END
GO
