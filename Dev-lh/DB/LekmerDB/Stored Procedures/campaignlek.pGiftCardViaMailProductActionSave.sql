
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@SendingInterval	INT,
	@TemplateId			INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		[SendingInterval] = @SendingInterval,
		[ConfigId] = @ConfigId,
		[TemplateId] = @TemplateId
	WHERE
		[ProductActionId] = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			[ProductActionId],
			[SendingInterval],
			[ConfigId],
			[TemplateId]
		)
		VALUES (
			@ProductActionId,
			@SendingInterval,
			@ConfigId,
			@TemplateId
		)
	END
END
GO
