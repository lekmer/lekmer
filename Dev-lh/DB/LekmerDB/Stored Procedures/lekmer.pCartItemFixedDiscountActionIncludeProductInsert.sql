SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionIncludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
