SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeProductDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeProduct]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
