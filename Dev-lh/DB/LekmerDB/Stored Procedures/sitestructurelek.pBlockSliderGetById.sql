SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSliderGetById]
	@BlockId	INT,
	@LanguageId	INT
AS	
BEGIN
	SET NOCOUNT ON
	
	SELECT
		[bs].[SlideDuration],
		[b].*
	FROM
		[sitestructurelek].[tBlockSlider] bs
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bs].[BlockId] = [b].[Block.BlockId]
	WHERE
		[bs].[BlockId] = @BlockId
		AND [b].[Block.LanguageId] = @LanguageId 
END
GO
