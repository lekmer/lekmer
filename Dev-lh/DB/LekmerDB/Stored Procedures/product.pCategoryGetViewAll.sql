
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategoryGetViewAll]
	@ChannelId	INT,
	@LanguageId	INT
AS
BEGIN
	SELECT
		c.*
	FROM
		product.vCustomCategoryView AS c
	WHERE
		[c].[Category.ChannelId] = @ChannelId
		AND
		[c].[Category.LanguageId] = @LanguageId
END
GO
