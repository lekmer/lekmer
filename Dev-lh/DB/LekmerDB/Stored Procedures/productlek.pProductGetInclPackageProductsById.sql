SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductGetInclPackageProductsById]
	@ProductId	INT,
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT *		
	FROM 
		[product].[vCustomProductInclPackageProducts] AS p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		[Product.Id] = @ProductId
		AND [Product.ChannelId] = @ChannelId	
END
GO
