SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureProductRegistryWrapperGetAllByBrand]
	@BrandId	INT
AS
BEGIN
	SELECT 
		bssr.BrandId 'BrandSiteStructureProductRegistryWrapper.BrandId',
		bssr.SiteStructureRegistryId 'BrandSiteStructureProductRegistryWrapper.SiteStructureRegistryId',
		bssr.ContentNodeId 'BrandSiteStructureProductRegistryWrapper.ContentNodeId',
		pr.ProductRegistryId 'BrandSiteStructureProductRegistryWrapper.ProductRegistryId',
		pr.Title 'BrandSiteStructureProductRegistryWrapper.Title',
		cn.ContentNodeStatusId 'BrandSiteStructureProductRegistryWrapper.ContentNodeStatusId'		
	FROM 
		[lekmer].[tBrandSiteStructureRegistry] bssr
		INNER JOIN [sitestructure].[tSiteStructureModuleChannel] ssmc ON ssmc.[SiteStructureRegistryId] = bssr.[SiteStructureRegistryId]
		INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.ChannelId = ssmc.[ChannelId]
		INNER JOIN [product].[tProductRegistry] pr ON pr.[ProductRegistryId] = pmc.[ProductRegistryId]
		INNER JOIN [sitestructure].[tContentNode] cn ON cn.[ContentNodeId] = bssr.[ContentNodeId]
	WHERE 
		bssr.[BrandId] = @BrandId
END
GO
