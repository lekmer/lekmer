
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListSave]
	@BlockId INT,
	@CategoryId INT,
	@RatingId INT
AS
BEGIN
	UPDATE [review].[tBlockBestRatedProductList]
	SET
		[CategoryId] = @CategoryId,
		[RatingId] = @RatingId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockBestRatedProductList] (
		[BlockId],
		[CategoryId],
		[RatingId]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@RatingId
	)
END
GO
