SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignToTagDeleteById]
	@Id INT
AS
BEGIN
	DELETE FROM [campaignlek].[tCampaignToTag]
	WHERE [Id] = @Id
END
GO
