SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetById]
	@RatingGroupFolderId	INT
AS
BEGIN
	SELECT
		rgf.*
	FROM
		[review].[vRatingGroupFolder] rgf
	WHERE
		rgf.[RatingGroupFolder.RatingGroupFolderId] = @RatingGroupFolderId
END
GO
