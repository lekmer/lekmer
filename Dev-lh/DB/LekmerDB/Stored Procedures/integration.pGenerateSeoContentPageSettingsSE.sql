
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsSE]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

		-- Insert new contentnodeIds in tContentPageSeoSetting
		INSERT INTO [sitestructure].[tContentPageSeoSetting] (ContentNodeId)
		SELECT
			[n].[ContentNodeId]
		FROM 
			[sitestructure].[tContentNode] n --3
		WHERE 
			[n].[SiteStructureRegistryId] = 1
			AND [n].[ContentNodeTypeId] = 3 -- detta är contentpages
			AND [n].[ContentNodeId] NOT IN (SELECT [ContentNodeId] FROM [sitestructure].[tContentPageSeoSetting])

		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Leksaker online från Lekmer.se – leksaker på nätet.',
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. '
			+ 'Vi erbjuder ett stort utbud av barnleksaker från kända varumärken och levererar dem på 1-3 dagar - Lekmer.se din leksaksbutik.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND cn.ParentContentNodeId = 1000923 -- Leksaker
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Leksaker online från Lekmer.se – leksaker på nätet.',
			cps.[Description] = 'Köp ' + x.Title + ' ' + cn.Title + ' på nätet. '
			+ 'Vi erbjuder ett stort utbud av barnleksaker från kända varumärken och levererar dem på 1-3 dagar - Lekmer.se din leksaksbutik.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title 
						FROM sitestructure.tContentNode 
						WHERE ParentContentNodeId = 1000923) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = 'Barn och Baby ' + cn.Title + ' produkter online från Lekmer.se – Köp barnartiklar på nätet.',
			cps.[Description] = 'Köp Barn och Baby produkter på nätet. Få dina barn - och babyprodukter levererade på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND cn.ParentContentNodeId = 1000835 -- Barn & Baby
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barn och Baby produkter online från Lekmer.se – Köp barnartiklar på nätet.',
			cps.[Description] = 'Köp ' + cn.Title  + ' Barn och Baby produkter på nätet. '
			+ 'Få dina barn - och babyprodukter levererade på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title 
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1000835) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Barnkläder online från Lekmer.se – Barn och Babykläder på nätet.',
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. '
			+ 'Stort utbud av coola kläder till barn och bebisar – handla online dina barnkläder och få dem levererade inom 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND cn.ParentContentNodeId = 1007116 -- Barnkläder
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barnkläder online från Lekmer.se – Barn och babykläder på nätet.',
			cps.[Description] = 'Köp ' + cn.Title + ' och ' + x.Title + ' på nätet. '
			+ 'Få dina barnkläder levererade på 1-3 dagar från Lekmer.se. levererade på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId		
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1007116) x ON cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 1			
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Inredning online från Lekmer.se Barnrumsinredning på nätet.',
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. Få ditt barnrum levererat på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND cn.ParentContentNodeId = 1000884 -- Barnrum
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Inredning online från Lekmer.se – Barnrum på nätet.',
			cps.[Description] = 'Köp ' + cn.Title + ' och ' + x.Title + ' på nätet. Få ditt barnrum levererat på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1000884) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' och Underhållning och barnspel online från Lekmer.se – Tv-spel på nätet.',
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. Få dina tv-spel levererade på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1
			AND cn.ParentContentNodeId = 1001245 -- Spel & Underhållning
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' och underhållning online från Lekmer.se.',
			cps.[Description] = 'Köp ' + cn.Title + ' och ' + x.Title + ' på nätet. Få dina tv-spel levererade på 1-3 dagar från Lekmer.se.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1001245) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 1			
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
