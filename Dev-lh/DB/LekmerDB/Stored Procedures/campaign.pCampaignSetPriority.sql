SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCampaignSetPriority]
	@CampaignIdList		varchar(max),
	@Separator			char(1)
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		Priority = t.Ordinal
	FROM
		[campaign].[tCampaign] c
		INNER JOIN generic.fnConvertIDListToTableWithOrdinal(@CampaignIdList, @Separator) t ON c.CampaignId = t.Id
END
GO
