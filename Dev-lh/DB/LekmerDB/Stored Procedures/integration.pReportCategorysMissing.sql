SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportCategorysMissing]

AS
begin
	set nocount on
	begin try
				
	select HYArticleid, ArticleClassTitle, isnull(ArticleClassId, 'NULL') as ArticleClassId, 
								ArticleGroupTitle, isnull(ArticleGroupId, 'NULL') as ArticleGroupId,
								ArticleCodeTitle, isnull(ArticleCodeId , 'NULL') as ArticleCodeId
			from integration.tempProduct
			where ArticleClassTitle = '***Missing***'
			or ArticleGroupTitle = '***Missing***'
			or ArticleCodeTitle = '***Missing***'
			

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
