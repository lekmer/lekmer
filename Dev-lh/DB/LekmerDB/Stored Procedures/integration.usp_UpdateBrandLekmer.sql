
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateBrandLekmer]
AS
BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
	PRINT '1 inser into temp table'
	
	IF OBJECT_ID('tempdb..#UniqueErpId') IS NOT NULL
		DROP TABLE #UniqueErpId
		
	CREATE TABLE #UniqueErpId
	(
		BrandId NVARCHAR(200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
		BrandTitle NVARCHAR(2500) COLLATE Finnish_Swedish_CI_AS NOT NULL
	);
	
	WITH UniqueErpId
	AS
	(
		SELECT
			BrandId,
			BrandTitle,
			ROW_NUMBER() OVER(PARTITION BY BrandId
							  ORDER BY SUBSTRING(HYarticleId, 3, 1)) RowNo
		FROM
			integration.tempProduct
		WHERE
			BrandId IS NOT NULL
			AND
			BrandTitle IS NOT NULL
			AND
			BrandTitle != '***Missing***'
		GROUP BY
			BrandId,
			BrandTitle,
			SUBSTRING(HYarticleId, 3, 1)
	)
	INSERT INTO #UniqueErpId
	SELECT
		BrandId,
		BrandTitle
	FROM UniqueErpId
	WHERE RowNo = 1 

	
	PRINT '2 update t.brand'
	UPDATE
		b
	SET
		b.[Title] = t.[BrandTitle]
	FROM
		#UniqueErpId t
		INNER JOIN [lekmer].[tBrand] b ON b.[ErpId] = t.[BrandId]
	WHERE
		b.[Title] <> t.[BrandTitle]


	PRINT '3 inser into lekmer.tbrand'
	INSERT INTO
		[lekmer].[tBrand] (ErpId, Title, IsOffline)
	SELECT
		u.[BrandId],
		u.[BrandTitle],
		0 -- set new brands Online by default
	FROM
		#UniqueErpId u
	WHERE
		u.[BrandId] NOT IN (SELECT b.[ErpId] FROM [lekmer].[tBrand] b WHERE b.[ErpId] IS NOT NULL)

	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH

END

GO
