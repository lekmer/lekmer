SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionCreateGetAddress]
	@StoreId INT,
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tCollectorTransaction]
			( [StoreId],
			  [TransactionTypeId],
			  [CivicNumber],
			  [Created]
			)
	VALUES
			( @StoreId,
			  @TransactionTypeId,
			  @CivicNumber,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
