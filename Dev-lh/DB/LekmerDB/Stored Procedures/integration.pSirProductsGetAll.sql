SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pSirProductsGetAll]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	FROM [integration].[tSirProductsFull]
	ORDER BY [ArticleDescription]

END
GO
