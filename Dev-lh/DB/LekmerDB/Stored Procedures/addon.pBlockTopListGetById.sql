SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pBlockTopListGetById]
	@LanguageId int,
	@BlockId int
as
begin
	select 
		tl.*,
		b.*
	from 
		[addon].[vCustomBlockTopList] as tl
		inner join [sitestructure].[vCustomBlock] as b on tl.[BlockTopList.BlockId] = b.[Block.BlockId]
	where
		tl.[BlockTopList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
end
GO
