SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pBlockProductFilterDeleteAllBrands]
	@BlockId int
as
begin
	delete
		lekmer.tBlockProductFilterBrand
	where
		BlockId = @BlockId
end
GO
