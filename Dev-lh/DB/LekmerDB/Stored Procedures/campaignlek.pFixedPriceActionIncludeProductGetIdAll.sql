
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT
		[aip].[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[ProductActionId] = @ProductActionId
		AND [p].[IsDeleted] = 0
END
GO
