SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pPercentageDiscountCartActionGetById]
	@CartActionId INT
AS 
BEGIN 
	SELECT
		ca.*,
		pdca.*
	FROM
		[campaign].[vCustomCartAction] AS ca
		LEFT JOIN [campaignlek].[tPercentageDiscountCartAction] pdca ON pdca.[CartActionId] = ca.[CartAction.Id]
	WHERE
		ca.[CartAction.Id] = @CartActionId
END
GO
