SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductModuleChannelGetByChannel]
	@ChannelId	int
as
begin
	select 
		*
	from 
		[product].[vCustomProductModuleChannel]
	where 
		[ProductModuleChannel.ChannelId] = @ChannelId
end

GO
