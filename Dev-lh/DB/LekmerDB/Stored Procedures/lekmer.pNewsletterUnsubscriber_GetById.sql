SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_GetById]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriber]
	WHERE
		[NewsletterUnsubscriber.UnsubscriberId] = @Id
END
GO
