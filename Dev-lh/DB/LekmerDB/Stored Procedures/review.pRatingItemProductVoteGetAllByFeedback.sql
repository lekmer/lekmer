SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductVoteGetAllByFeedback]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ripv.*
	FROM
		[review].[vRatingItemProductVote] ripv
	WHERE
		ripv.[RatingItemProductVote.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
