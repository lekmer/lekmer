
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageTranslationGetAll]
	@ContentMessageId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [cmt].*
	FROM
	    [lekmer].[vContentMessageTranslation] cmt
	WHERE 
		cmt.[ContentMessageTranslation.ContentMessageId] = @ContentMessageId
	ORDER BY
		[cmt].[ContentMessageTranslation.LanguageId]
END
GO
