
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pAliasGetAllForDeleting]
	@DateTime	DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		a.*
	FROM
		[template].[tAlias] a
	WHERE
		([LastUsedDate] IS NULL OR [LastUsedDate] < @DateTime)
		AND [CommonName] NOT LIKE 'Package_%'
		AND [CommonName] NOT LIKE 'Backoffice.ToolTip%'
		AND NOT EXISTS (SELECT 1 FROM [template].[tTemplateFragment] tf WHERE tf.[Content] LIKE '%' + a.CommonName + '%')
		AND NOT EXISTS (SELECT 1 FROM [template].[tTemplateFragment] tf1 WHERE tf1.AlternateContent LIKE '%' + a.CommonName + '%')
		AND NOT EXISTS (SELECT 1 FROM [template].[tInclude] i WHERE i.[Content] LIKE '%' + a.CommonName + '%')
	ORDER BY
		a.CommonName
END
GO
