SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageSave]
	@CampaignLandingPageId	INT,
	@CampaignId				INT,
	@WebTitle				NVARCHAR(500),
	@Description			NVARCHAR(MAX),
	@CampaignRegistryIds	VARCHAR(MAX),
	@IconIds				VARCHAR(MAX),
	@ImageIds				VARCHAR(MAX),
	@ContentNodeIds		VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[campaignlek].[tCampaignLandingPage]
	SET 
		[WebTitle] = @WebTitle,
		[Description] = @Description
	WHERE
		[CampaignLandingPageId] = @CampaignLandingPageId
		AND [CampaignId] = @CampaignId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignLandingPage] (
			[CampaignId],
			[WebTitle],
			[Description]
		)
		VALUES (
			@CampaignId,
			@WebTitle,
			@Description
		)

		SET @CampaignLandingPageId = SCOPE_IDENTITY()
	END

	EXEC [campaignlek].[pCampaignRegistryCampaignLandingPageSave] @CampaignLandingPageId, @CampaignRegistryIds, @IconIds, @ImageIds, @ContentNodeIds, @Delimiter

	RETURN @CampaignLandingPageId
END
GO
