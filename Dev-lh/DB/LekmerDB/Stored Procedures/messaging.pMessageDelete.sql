SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure [messaging].[pMessageDelete]
@MessageId	uniqueidentifier
as
begin

	delete from 
		[messaging].[tMessageCustomField]
	where
		MessageId = @MessageId

	delete from 
		[messaging].[tMessage]
	where
		MessageId = @MessageId

end


GO
