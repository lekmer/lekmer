SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pCustomerInformationDeleteByCustomer]
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [customerlek].[tCustomerInformation]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tCustomerInformation]
	WHERE [CustomerId] = @CustomerId
END
GO
