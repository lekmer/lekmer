SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pReviewRecordGetByFeedback]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rr.*
	FROM
		[review].[vReviewRecord] rr
	WHERE
		[rr].[RatingReviewFeedback.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
