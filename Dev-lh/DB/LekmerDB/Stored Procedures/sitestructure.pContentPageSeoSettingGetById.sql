SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 12:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentPageSeoSettingGetById]
@ContentNodeId	int,
@LanguageId		int
as
begin
	select 
		cpss.*
	from 
		[sitestructure].[vCustomContentPageSeoSetting] cpss
	where
		cpss.[ContentPageSeoSetting.ContentNodeId] = @ContentNodeId 
		AND cpss.[ContentPageSeoSetting.LanguageId] = @LanguageId
end

GO
