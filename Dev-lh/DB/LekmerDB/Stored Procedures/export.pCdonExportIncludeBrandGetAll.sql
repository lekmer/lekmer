SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeBrandGetAll]
AS
BEGIN
	SELECT
		[ib].CreatedDate 'CreatedDate',
		[ib].BrandId 'ItemId',
		[ib].ProductRegistryId 'ProductRegistryId',
		[ib].Reason 'Reason',
		[ib].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeBrand] ib
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ib].[ProductRegistryId]
END
GO
