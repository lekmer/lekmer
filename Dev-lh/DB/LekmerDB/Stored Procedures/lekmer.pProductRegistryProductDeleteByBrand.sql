
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductDeleteByBrand]
	@BrandId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE prp 
	FROM [product].[tProductRegistryProduct] prp
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
	WHERE lp.[BrandId] = @BrandId
		  AND prp.[ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
		  
	EXEC [productlek].[pPackageRestrictionsUpdate] NULL, @BrandId, NULL, @ProductRegistryIds, @Delimiter
END
GO
