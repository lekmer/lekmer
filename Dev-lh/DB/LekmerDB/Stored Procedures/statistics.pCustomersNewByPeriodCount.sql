SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pCustomersNewByPeriodCount]
	@ChannelId	INT,
	@DateFrom	DATETIME,
	@DateTo		DATETIME
AS
BEGIN
	-- Number of new email addresses in period x

	DECLARE @tInPeriod TABLE (Email VARCHAR(320) primary key with (ignore_dup_key = on))
	INSERT INTO @tInPeriod
	SELECT LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > @DateFrom
		AND [o].[CreatedDate] < @DateTo
		AND [o].[ChannelId] = @ChannelId
		
	DECLARE @tBeforePeriod TABLE (Email VARCHAR(320) primary key with (ignore_dup_key = on))
	INSERT INTO @tBeforePeriod
	SELECT LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] < @DateFrom
		AND [o].[ChannelId] = @ChannelId

	SELECT COUNT(DISTINCT [a].[Email]) 'Customers' FROM (
		SELECT [Email] FROM @tInPeriod
		EXCEPT
		SELECT [Email] FROM @tBeforePeriod
	) a
END
GO
