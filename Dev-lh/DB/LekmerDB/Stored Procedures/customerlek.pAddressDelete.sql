
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customerlek].[pAddressDelete]
	@AddressId	INT
AS	
BEGIN
	SET NOCOUNT ON
	
	-- Delete lekmer part
	DELETE [customerlek].[tAddress]
	WHERE [AddressId] = @AddressId
	
	DELETE [customer].[tAddress]
	WHERE AddressId = @AddressId
END
GO
