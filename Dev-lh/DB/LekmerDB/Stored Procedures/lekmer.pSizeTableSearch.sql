
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableSearch]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		[st].*
	FROM 
		[lekmer].[vSizeTableSecure] st 
	WHERE
		[st].[SizeTable.Title] LIKE @SearchCriteria ESCAPE '\'
		OR [st].[SizeTable.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
