SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityFunctionGetAllByEntityId]
	@EntityId	int
AS
begin
	set nocount on
	select
		F.*
	from
		[template].[vCustomEntityFunction] F
	where
		F.[EntityFunction.EntityId] = @EntityId
	ORDER BY F.[EntityFunction.CommonName]
end

GO
