SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pImportSupplier]
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			-- update existing suppliers
			UPDATE 
				s
			SET  
				[ActivePassiveCode] = [ts].[ActivePassiveCode],
				[Name] = [ts].[Name],
				[Address] = [ts].[Address],
				[ZipArea] = [ts].[ZipArea],
				[City] = [ts].[City],
				[CountryISO] = [ts].[CountryISO],
				[LanguageISO] = [ts].[LanguageISO],
				[VATRate] = [ts].[VATRate],
				[Phone1] = CASE LTRIM(RTRIM([ts].[Phone1])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone1])) END,
				[Phone2] = CASE LTRIM(RTRIM([ts].[Phone2])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone2])) END,
				[Email] = [ts].[Email]
			FROM 
				[productlek].[tSupplier] s
				INNER JOIN [integration].[tempSupplier] ts ON [ts].[SupplierNo] = [s].[SupplierNo]
			
			-- insert new suppliers
			INSERT INTO 
				[productlek].[tSupplier] (
					[SupplierNo],
					[ActivePassiveCode],
					[Name],
					[Address],
					[ZipArea],
					[City],
					[CountryISO],
					[LanguageISO],
					[VATRate],
					[Phone1],
					[Phone2],
					[Email]
				)
				SELECT
					[ts].[SupplierNo],
					[ts].[ActivePassiveCode],
					[ts].[Name],
					[ts].[Address],
					[ts].[ZipArea],
					[ts].[City],
					[ts].[CountryISO],
					[ts].[LanguageISO],
					[ts].[VATRate],
					CASE LTRIM(RTRIM([ts].[Phone1])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone1])) END,
					CASE LTRIM(RTRIM([ts].[Phone2])) WHEN '' THEN NULL ELSE LTRIM(RTRIM([ts].[Phone2])) END,
					[ts].[Email]
				FROM
					[integration].[tempSupplier] ts
				WHERE
					[ts].[SupplierNo] NOT IN (SELECT [s].[SupplierNo] FROM [productlek].[tSupplier] s)

		COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog] (Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
