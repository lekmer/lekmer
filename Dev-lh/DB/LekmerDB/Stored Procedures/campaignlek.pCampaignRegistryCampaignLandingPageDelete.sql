SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignLandingPageDelete]
	@CampaignLandingPageId	INT,
	@CampaignRegistryIds	VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE
		crclp
	FROM
		[campaignlek].[tCampaignRegistryCampaignLandingPage] crclp
		INNER JOIN [generic].[fnConvertIDListToTable](@CampaignRegistryIds, @Delimiter) AS r ON r.[ID] = [crclp].[CampaignRegistryId]
	WHERE
		[crclp].[CampaignLandingPageId] = @CampaignLandingPageId
END
GO
