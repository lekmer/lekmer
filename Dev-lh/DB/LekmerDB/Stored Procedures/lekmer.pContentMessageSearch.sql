SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageSearch]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		[cm].*
	FROM 
		[lekmer].[vContentMessageSecure] cm
	WHERE
		[cm].[ContentMessage.Message] LIKE @SearchCriteria ESCAPE '\'
		OR [cm].[ContentMessage.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
