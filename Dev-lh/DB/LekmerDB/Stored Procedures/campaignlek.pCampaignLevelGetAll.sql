SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLevelGetAll]
AS
BEGIN
	SELECT
		[LevelId]	'CampaignLevel.Id',
		[Title]		'CampaignLevel.Title',
		[Priority]	'CampaignLevel.Priority'
	FROM
		[campaignlek].[tCampaignLevel]
	ORDER BY
		[Priority] ASC
END
GO
