SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGroupGetById]
	@Id INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vTagGroup]
	WHERE
		[TagGroup.TagGroupId] = @Id
END
GO
