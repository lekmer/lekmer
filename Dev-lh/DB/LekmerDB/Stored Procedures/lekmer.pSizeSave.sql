
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeSave]
	@SizeId INT,
	@ErpId NVARCHAR(50),
	@ErpTitle NVARCHAR(50),
	@EU DECIMAL(5,2),
	@EUTitle NVARCHAR(50),
	@USMale DECIMAL(3,1),
	@USFemale DECIMAL(3,1),
	@UKMale DECIMAL(3,1),
	@UKFemale DECIMAL(3,1),
	@Millimeter INT
AS
BEGIN
	SET NOCOUNT ON
	
	-- ErpId should be unique (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSize]
		WHERE 
			[ErpId] = @ErpId 
			AND [SizeId] <> @SizeId
	)
	RETURN -1
	
	UPDATE 
		[lekmer].[tSize]
	SET 
		[ErpId] = @ErpId,
		[ErpTitle] = @ErpTitle,
		[EU] = @EU,
		[EUTitle] = @EUTitle,
		[USMale] = @USMale,
		[USFemale] = @USFemale,
		[UKMale] = @UKMale,
		[Millimeter] = @Millimeter
	WHERE
		[SizeId] = @SizeId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		-- ErpId should be unique (return -1)
		IF EXISTS
		(
			SELECT 1 FROM [lekmer].[tSize]
			WHERE [ErpId] = @ErpId
		)
		RETURN -1
		
		INSERT INTO [lekmer].[tSize] (
			[ErpId],
			[ErpTitle],
			[EU],
			[EUTitle],
			[USMale],
			[USFemale],
			[UKMale],
			[UKFemale],
			[Millimeter],
			[Ordinal]
		)
		VALUES (
			@ErpId,
			@ErpTitle,
			@EU,
			@EUTitle,
			@USMale,
			@USFemale,
			@UKMale,
			@UKFemale,
			@Millimeter,
			(SELECT MAX(Ordinal) + 1 FROM [lekmer].[tSize])
		)
		
		SET @SizeId = SCOPE_IDENTITY()
	END
	RETURN @SizeId
END
GO
