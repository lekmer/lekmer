SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeBrandDeleteAllByBrand]
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		ib
	FROM
		[export].[tCdonExportIncludeBrand] ib
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS pl ON pl.[ID] = [ib].[BrandId]
END
GO
