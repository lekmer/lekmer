
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedDiscountActionExcludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
