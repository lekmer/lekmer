SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pTemplateModuleChannelSave]
@ChannelId	int,
@ThemeId	int
AS
BEGIN
	IF EXISTS (SELECT 1 FROM template.tTemplateModuleChannel WHERE ChannelId = @ChannelId)
		BEGIN
			UPDATE template.tTemplateModuleChannel
			SET ThemeId = @ThemeId
			WHERE ChannelId = @ChannelId
		END
	ELSE
		BEGIN
			INSERT INTO template.tTemplateModuleChannel
			VALUES (@ChannelId, @ThemeId)
		END
END













GO
