SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCampaignSetFolder]
@CampaignId		int,
@FolderId		int
AS
BEGIN
	UPDATE [campaign].[tCampaign]
	SET FolderId = @FolderId
	WHERE CampaignId = @CampaignId
END
GO
