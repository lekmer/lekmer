SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagDelete]
	@FlagId		INT
AS
BEGIN
	DELETE [lekmer].[tFlag]
	WHERE [FlagId] = @FlagId
END

GO
