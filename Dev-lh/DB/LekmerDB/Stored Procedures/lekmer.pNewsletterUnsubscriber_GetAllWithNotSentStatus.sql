SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_GetAllWithNotSentStatus]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriber] nu
	WHERE
		nu.[NewsletterUnsubscriber.SentStatus] = 0
		OR 
		nu.[NewsletterUnsubscriber.SentStatus] IS NULL
END
GO
