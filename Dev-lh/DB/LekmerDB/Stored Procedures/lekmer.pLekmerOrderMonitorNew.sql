
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
--exec [lekmer].[pLekmerOrderMonitorNew] '2014-12-20', 15000, 1
CREATE PROCEDURE [lekmer].[pLekmerOrderMonitorNew]
	@Date				DATETIME,
	@MinAfterPurchase	INT,
	@GetImages			BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
  SET NOCOUNT ON;

	IF OBJECT_ID('tempdb..#tTmpResult') IS NOT NULL
	DROP TABLE #tTmprResult
	
	CREATE TABLE #tTmpResult
	(		
		OrderId INT,	
		[Time] DATETIME NOT NULL,
		Country NVARCHAR (50) NOT NULL,
		ChannelId INT NOT NULL,
		ApplicationName NVARCHAR (100),
		City NVARCHAR (100),
		Total DECIMAL(16,2) NOT NULL
	)

	INSERT INTO #tTmpResult(OrderId, [Time], City, Country, ChannelId, ApplicationName, Total)
	SELECT
		x.OrderId as OrderId,
		x.CreatedDate AS 'Time',
		x.City as City,
		CASE x.ChannelId 
			WHEN 1 THEN 'Sweden'
			WHEN 2 THEN 'Norway'
			WHEN 3 THEN 'Denmark'
			WHEN 4 THEN 'Finland'
			WHEN 1000005 THEN 'Netherlands'
		END AS Country,
		x.ChannelId AS ChannelId, 
		x.ApplicationName AS ApplicationName,
		x.Total AS Total
		FROM
		(
			SELECT
				co.[CreatedDate], co.[ChannelId], c.[ApplicationName], oa.[City], co.[OrderId], 
				 ISNULL(p.[Price],0) 
					+ ISNULL([co].[FreightCost],0)
					+ ISNULL([lo].[PaymentCost],0)
					+ ISNULL([lo].[OptionalFreightCost],0) 
					+ ISNULL([lo].[DiapersFreightCost],0) AS Total
			FROM
				[order].[tOrder] co
			INNER JOIN 	[order].[tOrderAddress] oa
						ON co.[BillingAddressId] = oa.OrderAddressId
			INNER JOIN [lekmer].[tLekmerOrder] lo 
						ON [lo].[OrderId] = [co].[OrderId]
			INNER JOIN [order].[tOrderPayment] p 
						ON [p].[OrderId] = [co].[OrderId]
			INNER JOIN [core].[tChannel] c
						ON co.[ChannelId] = c.[ChannelId]
			WHERE
				co.[CreatedDate]  >= DATEADD(MINUTE, -@MinAfterPurchase, @Date)
				AND [CreatedDate] < @Date
				AND co.[OrderStatusId] = 4
			
		) x

			IF OBJECT_ID('tempdb..#tTmpImageResult') IS NOT NULL
			DROP TABLE #tTmprImageResult
	IF(@GetImages = 1)
	BEGIN
		CREATE TABLE #tTmpImageResult
		(		
				OrderId INT,	
				ImageUrls NVARCHAR (max)
		)

		INSERT INTO #tTmpImageResult(OrderId, ImageUrls)
		SELECT
			x.OrderId as OrderId, x.ImageUrls as ImageUrls
				FROM
				(
					SELECT
						DISTINCT tr.OrderId, 
							STUFF(
									(
										SELECT ';' + CAST(p.[MediaId] AS VARCHAR) + '/' +  LTRIM(CAST(m.[Media.Title] AS VARCHAR)) + '.' +  CAST(mf.[Extension] AS VARCHAR)
											FROM [order].[vOrderItem] AS oi
										LEFT JOIN [product].[tProduct] p
												ON p.[ProductId] = oi.[OrderItem.ProductId]
										INNER JOIN [media].[vMedia] m
												ON p.[MediaId] = m.[Media.Id]
										INNER JOIN [media].[tMediaFormat] mf
												ON m.[Media.FormatId] = mf.[MediaFormatId]
										WHERE oi.[OrderItem.OrderId]= tr.OrderId
										FOR XML PATH('')
								), 1, 1, '') AS ImageUrls
					FROM
						[order].[tOrderItem] tr
					WHERE tr.OrderId IN (SELECT OrderId FROM #tTmpResult)
			

				) x
			

		SELECT tr.*, tri.ImageUrls FROM #tTmpResult tr
		INNER JOIN #tTmpImageResult tri
		ON tr.OrderId = tri.OrderId
	END
	IF(@GetImages = 0)
	BEGIN
		SELECT * FROM #tTmpResult
	END
END
GO
