
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductTagAllForCampaigns]
AS
BEGIN
	SET NOCOUNT ON

--Create temporary table for holding campaigns to tag products for
CREATE TABLE #Campaigns (
CampaignId int
)

--Get all campaigns online, and with landing page selected (we dont care if landing page exists, we just tag?)
insert into #Campaigns select c.CampaignId from campaign.tCampaign c where c.UseLandingPage = 1 and c.CampaignStatusId = 0

--Delete all tags for these campaigns before inserting new tags
delete from lekmer.tProductTag where TagId in (select t.TagId from lekmer.tTag t inner join lekmer.tTagGroup tg on tg.TagGroupId = t.TagGroupId where tg.CommonName = 'auto-kampanj' and t.CommonName in (select CampaignId from #Campaigns))

--Do the major selection and filter. The logic is copy of lekmer.pProductGetIdAllByCampaignId (keep them identical!!)
INSERT INTO lekmer.tProductTag (ProductId, TagId)
select ProductId, TagId from (
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
 union
 select ProductId, CampaignId from campaign.tProductAction pa
inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pa.ProductActionId
where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionIncludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) 
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union 
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionIncludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
except
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = ppda.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = ppda.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = ppda.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedDiscountActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeBrand fdaib on fdaib.ProductActionId = pa.ProductActionId
inner join lekmer.tLekmerProduct lp on lp.BrandId = fdaib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeProduct fdaip on fdaip.ProductActionId = pa.ProductActionId
 where CampaignId in (select CampaignId from #Campaigns)
 union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tFixedPriceActionExcludeCategory fdaic on fdaic.ProductActionId = pa.ProductActionId
inner join product.tProduct p on p.CategoryId = fdaic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) 
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeBrand caib on caib.ConfigId = gcvmpa.ConfigId
inner join lekmer.tLekmerProduct lp on lp.BrandId = caib.BrandId
 where CampaignId in (select CampaignId from #Campaigns)
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeProduct caip on caip.ConfigId = gcvmpa.ConfigId
 where CampaignId in (select CampaignId from #Campaigns)
union 
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId = caic.CategoryId
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 0
union
select ProductId, CampaignId from campaign.tProductAction pa
inner join campaignlek.tGiftCardViaMailProductAction gcvmpa on gcvmpa.ProductActionId = pa.ProductActionId
inner join campaignlek.tCampaignActionExcludeCategory caic on caic.ConfigId = gcvmpa.ConfigId
inner join product.tProduct p on p.CategoryId in (select CategoryId from product.fnGetSubCategories(caic.CategoryId))
 where CampaignId in (select CampaignId from #Campaigns) and caic.IncludeSubCategories = 1

) c
inner join lekmer.tTag t on t.CommonName = c.CampaignId
inner join lekmer.tTagGroup tg on tg.TagGroupId = t.TagGroupId
where tg.CommonName = 'auto-kampanj'


drop table #Campaigns

END
GO
