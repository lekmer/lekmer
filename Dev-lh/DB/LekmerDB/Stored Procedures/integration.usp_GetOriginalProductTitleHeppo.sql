SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_GetOriginalProductTitleHeppo]
	@Channel 	int
AS
BEGIN

		begin
			begin try
				begin transaction
				
				
				select t.ProductId, t.Title, t.WebShopTitle, t.LanguageId, l.HYErpId from
					(select
						p.ProductId,
						coalesce(pt.Title, p.Title) as Title,
						coalesce(pt.WebShopTitle, p.WebShopTitle) as WebShopTitle,
						(select LanguageId from core.tChannel where ChannelId = @Channel ) as LanguageId
					from
						product.tProduct p
						left outer join product.tProductTranslation pt
							on p.ProductId = pt.ProductId
							and pt.LanguageId = (select LanguageId from core.tChannel where ChannelId = @Channel)) t
					--order by
						--p.ProductId,languageId) 
							
				inner join lekmer.tLekmerProduct l
					on t.ProductId = l.ProductId
			
				commit
			end try
			
			begin catch
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			

		end
		
END
GO
