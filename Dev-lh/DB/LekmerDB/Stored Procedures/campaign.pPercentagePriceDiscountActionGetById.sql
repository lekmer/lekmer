
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaign].[vCustomPercentagePriceDiscountAction]
	WHERE
		[PercentagePriceDiscountAction.ProductActionId] = @ProductActionId
END
GO
