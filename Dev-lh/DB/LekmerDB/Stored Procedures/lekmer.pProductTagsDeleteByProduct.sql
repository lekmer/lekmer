SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagsDeleteByProduct]
	@ProductId	INT,
	@TagIds		VARCHAR(MAX),
	@ObjectId	INT,
	@Delimiter	CHAR(1) = ','
AS
BEGIN
	EXEC [lekmer].[pProductTagUsageDelete] @ProductId, @TagIds, @ObjectId, @Delimiter

	DELETE pt
	FROM [lekmer].[tProductTag] pt
	INNER JOIN [generic].[fnConvertIDListToTable](@TagIds, @Delimiter) AS t ON [t].[ID] = [pt].[TagId]
	WHERE [pt].[ProductId] = @ProductId
		  AND NOT EXISTS (  SELECT 1 
							FROM [lekmer].[tProductTagUsage] ptu1
							INNER JOIN [lekmer].[tProductTag] pt1 ON [pt1].[ProductTagId] = [ptu1].[ProductTagId]
							WHERE [pt1].[ProductId] = @ProductId AND [pt1].[TagId] = [t].[ID]
							HAVING COUNT(1) > 0)
END
GO
