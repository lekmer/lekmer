SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreGetById]
	@StoreId	int
as
begin
	select
		*
	from
		[product].[vCustomStore] c
	where
		c.[Store.Id] = @StoreId
end

GO
