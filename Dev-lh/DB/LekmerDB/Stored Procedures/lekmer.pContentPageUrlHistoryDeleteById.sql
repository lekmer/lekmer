SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteById]
	@ContentPageUrlHistoryId INT
AS
BEGIN
	DELETE [lekmer].[tContentPageUrlHistory]
	WHERE ContentPageUrlHistoryId = @ContentPageUrlHistoryId
END
GO
