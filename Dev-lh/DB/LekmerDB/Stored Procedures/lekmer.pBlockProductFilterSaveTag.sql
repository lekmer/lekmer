SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pBlockProductFilterSaveTag]
	@BlockId int,
	@TagId int
as
begin
	insert
		lekmer.tBlockProductFilterTag
	(
		BlockId,
		TagId
	)
	values
	(
		@BlockId,
		@TagId
	)
end
GO
