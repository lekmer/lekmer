SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationSave]
	@CampaignLandingPageId	INT,
	@LanguageId				INT,
	@WebTitle				NVARCHAR(500),
	@Description			NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[campaignlek].[tCampaignLandingPageTranslation]
	SET
		[WebTitle] = @WebTitle,
		[Description] = @Description
	WHERE
		[CampaignLandingPageId] = @CampaignLandingPageId
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [campaignlek].[tCampaignLandingPageTranslation] (
			[CampaignLandingPageId],
			[LanguageId],
			[WebTitle],
			[Description]				
		)
		VALUES (
			@CampaignLandingPageId,
			@LanguageId,
			@WebTitle,
			@Description
		)
	END
END
GO
