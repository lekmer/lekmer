SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityFunctionDelete]
	@FunctionId	int
AS
begin
	set nocount ON
	DELETE  [template].[tEntityFunction] WHERE FunctionId = @FunctionId
end	




GO
