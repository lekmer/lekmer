SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pFacebookUserDeleteAlternateLogin]
	@CustomerId INT,
	@FacebookId NVARCHAR(MAX)
AS	
BEGIN
	SET NOCOUNT ON
	IF EXISTS (SELECT 1 FROM [customer].[tCustomerUser] WHERE [CustomerId] = @CustomerId AND [IsAlternateLogin] = 0)
	BEGIN
		DELETE	[customerlek].[tFacebookCustomerUser]
		WHERE   [CustomerId] = @CustomerId
				AND [FacebookId] = @FacebookId
				AND [IsAlternateLogin] = 1
	END
END
GO
