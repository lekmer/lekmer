SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionIncludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END
GO
