SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateProductDESCRI]
	-- Add the parameters for the stored procedure here
AS
BEGIN

	begin try
	begin transaction

		-- tProduct
		UPDATE 
			--[product].tProduct
			p
		SET 
			[Description] = t.[Text]
			--Uppdatera short description med fast den ska vara max 75 tecken
			--ShortDescription = substring(t.[Text], 0,75) 

			-- språk			
			-- beskrtyp 
			--select *
		FROM 
			[integration].tempProductDescription t
			inner join [integration].tempProduct tt
					on t.HYarticleId = tt.HYFokArticle
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tt.HYarticleId, 5,17)
			inner join [product].tProduct p
					on p.ProductId = lp.ProductId				
			--inner join [product].tProductRegistryProduct pli
					--on pli.ProductId = lp.ProductId and
					--pli.ProductRegistryId = substring(t.HYarticleId, 3,1)
		WHERE 		
			[Description] <> t.[text]
			and substring(t.HYarticleId, 3,1) = 1
		
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        
END
GO
