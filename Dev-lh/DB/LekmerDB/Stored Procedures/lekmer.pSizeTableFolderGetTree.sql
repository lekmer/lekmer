SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT [stf].*
	FROM [lekmer].[vSizeTableFolder] AS stf
	WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT [stf].*
		FROM [lekmer].[vSizeTableFolder] AS stf
		WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT [stf].[SizeTableFolder.ParentSizeTableFolderId]
							 FROM [lekmer].[vSizeTableFolder] AS stf
							 WHERE [stf].[SizeTableFolder.SizeTableFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT [stf].*
			FROM [lekmer].[vSizeTableFolder] AS stf
			WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [lekmer].[vSizeTableFolder] AS stf
								WHERE [stf].[SizeTableFolder.ParentSizeTableFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
