
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRating_GetAllByFolder]
	@RatingFolderId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingSecure] r
	WHERE
		r.[Rating.RatingFolderId] = @RatingFolderId
END
GO
