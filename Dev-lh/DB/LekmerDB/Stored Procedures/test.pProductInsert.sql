SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [test].[pProductInsert]
	@ErpId varchar(50),
	@EanCode varchar(20),
	@IsDeleted bit,
	@CategoryId int,
	@Title nvarchar(256),
	@WebShopTitle nvarchar(256),
	@Description nvarchar(max),
	@ShortDescription nvarchar(max),
	@MediaId int,
	@ProductStatusId int
as
begin
	declare @id int
	insert into product.tProduct
	(
		ItemsInPackage,
		ErpId,
		EanCode,
		IsDeleted,
		NumberInStock,
		CategoryId,
		Title,
		WebShopTitle,
		[Description],
		ShortDescription,
		MediaId,
		ProductStatusId
	)
	values
	(
		1,
		@ErpId,
		@EanCode,
		@IsDeleted,
		1,
		@CategoryId,
		@Title,
		@WebShopTitle,
		@Description,
		@ShortDescription,
		@MediaId,
		@ProductStatusId
	)

	set @id = scope_identity()

	insert into product.tProductTranslation
	(
		ProductId,
		LanguageId,
		Title,
		WebShopTitle,
		[Description],
		ShortDescription
	)
	select
		@id,
		LanguageId,
		@Title,
		@WebShopTitle,
		@Description,
		@ShortDescription
	from
		core.tLanguage		

	return @id
end





GO
