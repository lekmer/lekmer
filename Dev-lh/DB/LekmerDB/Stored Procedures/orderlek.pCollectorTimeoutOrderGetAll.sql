
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCollectorTimeoutOrderGetAll]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[ct].[TransactionId] 'TimeoutOrder.TransactionId',
		[ct].[StatusCode] 'TimeoutOrder.StatusCode',
		[ct].[InvoiceStatus] 'TimeoutOrder.InvoiceStatus',
		[ct].[OrderId] 'TimeoutOrder.OrderId',
		[ct].[InvoiceNo] 'TimeoutOrder.InvoiceNo',
		[o].[ChannelId] 'TimeoutOrder.ChannelId'
	FROM
		[orderlek].[tCollectorTransaction] ct
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [ct].[OrderId]
	WHERE
		[ct].[TransactionTypeId] = 4 -- AddInvoiceTimeout
		AND
		(
			[ct].[TimeoutProcessed] IS NULL
			OR
			[ct].[TimeoutProcessed] <> 1
		)
		AND [o].[OrderStatusId] = 11 --PaymentTimeout
END

GO
