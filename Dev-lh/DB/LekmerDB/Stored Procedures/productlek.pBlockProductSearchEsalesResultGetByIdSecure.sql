SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		ba.* ,
		b.*
	FROM
		[productlek].[vBlockProductSearchEsalesResult] AS ba
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON ba.[BlockProductSearchEsalesResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchEsalesResult.BlockId] = @BlockId
END

GO
