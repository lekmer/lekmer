SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeGetAllByParentId]
	@Id int
as
BEGIN
	
	select 
		*
	from 
		[sitestructure].[vCustomContentNodeSecure]	
	where
		(@Id is NULL AND [ContentNode.ParentContentNodeId] is NULL) or [ContentNode.ParentContentNodeId] = @Id
	order by
		[ContentNode.Ordinal] asc
end

GO
