SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE stmpr FROM
		[lekmer].[tSizeTableMediaProductRegistry] stmpr
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[SizeTableMediaId] = [stmpr].[SizeTableMediaId]
	WHERE
		[stm].[SizeTableId] = @SizeTableId
END
GO
