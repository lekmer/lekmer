SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pSliderGroupDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[sitestructurelek].[tSliderGroup]
	WHERE
		[BlockId] = @BlockId
END
GO
