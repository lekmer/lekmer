
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetById] 
	@CustomerId INT
AS	
BEGIN
	SET NOCOUNT ON

	SELECT
		*	
	FROM
		[customer].[vCustomCustomer] 
	WHERE
		[Customer.CustomerId] = @CustomerId
END
GO
