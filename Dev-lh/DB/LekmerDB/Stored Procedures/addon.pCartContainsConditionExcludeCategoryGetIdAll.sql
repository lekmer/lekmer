
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeCategoryGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[cec].[CategoryId],
		[cec].[IncludeSubcategories]
	FROM 
		[addon].[tCartContainsConditionExcludeCategory] cec
	WHERE 
		[cec].[ConditionId] = @ConditionId
END
GO
