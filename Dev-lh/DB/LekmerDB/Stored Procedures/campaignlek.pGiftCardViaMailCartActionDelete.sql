SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pGiftCardViaMailCartActionCurrencyDeleteAll] @CartActionId

	DELETE FROM [campaignlek].[tGiftCardViaMailCartAction]
	WHERE CartActionId = @CartActionId
END

GO
