SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemDelete]
	@WishListItemId	INT
AS
BEGIN
	DELETE FROM
		[lekmer].[tWishListPackageItem]
	WHERE
		[WishListItemId] = @WishListItemId
END
GO
