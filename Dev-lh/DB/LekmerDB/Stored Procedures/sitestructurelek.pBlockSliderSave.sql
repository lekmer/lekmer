SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructurelek].[pBlockSliderSave]
	@BlockId		INT,
	@SlideDuration	INT
AS	
BEGIN
	SET NOCOUNT ON

	UPDATE
		[sitestructurelek].[tBlockSlider]
	SET
		[SlideDuration]= @SlideDuration
	WHERE
		[BlockId] = @BlockId
	
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [sitestructurelek].[tBlockSlider] (
			[BlockId],
			[SlideDuration]
		)
		VALUES (
			@BlockId,
			@SlideDuration
		)
	END
END
GO
