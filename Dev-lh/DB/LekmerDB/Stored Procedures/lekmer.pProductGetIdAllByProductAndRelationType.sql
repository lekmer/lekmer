
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByProductAndRelationType]
		@ChannelId		INT,
		@CustomerId		INT,
		@ProductId		INT,
		@RelationType	VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @RelationListTypeId INT
	SET @RelationListTypeId = (SELECT RelationListTypeId FROM product.tRelationListType WHERE CommonName = @RelationType)

	SELECT DISTINCT p.[Product.Id]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND rlp.ProductId <> @ProductId
		AND rl.RelationListTypeId = @RelationListTypeId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
