
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductShortDescriptionTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.ShortDescription] AS 'Value'
	FROM
		lekmer.[vProductTranslation]
	WHERE 
		[Product.Id] = @ProductId
	ORDER BY
		[Language.Id]
END

GO
