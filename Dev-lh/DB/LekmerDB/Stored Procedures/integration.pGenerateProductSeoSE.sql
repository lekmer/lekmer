
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateProductSeoSE]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

		INSERT INTO [product].[tProductSeoSetting] (ProductId)
		SELECT
			[ProductId]
		FROM
			[product].[tProduct]
		WHERE
			[ProductId] NOT IN (SELECT [ProductId] FROM [product].[tProductSeoSetting])

		-- DEFAULT --
		DECLARE @LanguageId INT
		SET @LanguageId = 1 -- Sweden
		
		INSERT INTO [product].[tProductSeoSettingTranslation] (ProductId, LanguageId)
		SELECT
			[p].[ProductId],
			@LanguageId
		FROM
			[product].[tProduct] p
		WHERE
			NOT EXISTS (SELECT 1 FROM [product].[tProductSeoSettingTranslation] n WHERE [n].[ProductId] = [p].[ProductId] AND [n].[LanguageId] = @LanguageId)
							

		------------------------------------------------------------
		-- <Produktnivå: Start Leksaker>
		------------------------------------------------------------
		UPDATE
			[pss]
		SET
			[pss].[Title] = 'Köp ' + p.Title + ' - från Lekmer.se',
			[pss].[Description] = 'Köp ' + p.Title + ' ' + c.Title + ' på nätet. '
			+ 'Du hittar även andra leksaker och babyprodukter från ' + b.Title + ' hos Lekmer.se' 
		FROM 
			[product].[tProductSeoSettingTranslation] pss
			INNER JOIN product.tProduct p ON p.ProductId = pss.ProductId
			INNER JOIN lekmer.tLekmerProduct l ON l.ProductId = p.ProductId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c2.CategoryId = c.ParentCategoryId
			INNER JOIN product.tCategory c3 ON c3.CategoryId = c2.ParentCategoryId
		WHERE
			c3.CategoryId = 1000533 -- Leksaker
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Barn & Baby>
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = 'Köp ' + p.Title + ' - Lekmer.se – Köp barnartiklar online',
			pss.[Description] = 'Köp ' + p.Title + ' ' + b.Title + ' på nätet. '
			+ 'Du hittar även andra Barn & Baby produkter från ' + b.Title + ' hos Lekmer.se.' 
		FROM 
			[product].[tProductSeoSettingTranslation] pss
			INNER JOIN product.tProduct p ON p.ProductId = pss.ProductId
			INNER JOIN lekmer.tLekmerProduct l ON l.ProductId = p.ProductId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c2.CategoryId = c.ParentCategoryId
			INNER JOIN product.tCategory c3 ON c3.CategoryId = c2.ParentCategoryId
		WHERE
			c3.CategoryId = 1000445 -- Barn och Baby
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Barnkläder>  
		------------------------------------------------------------
		UPDATE
			pss
		SET   
			pss.Title = p.Title + ' ' + c.Title + ' - Lekmer.se – Baby och ' + c3.Title + ' på nätet.',
			pss.[Description] = 'Köp ' + p.Title + ' ' + c.Title + ' och ' + c2.Title + ' på nätet. '
			+ 'Du hittar även andra barnkläder från ' + b.Title + ' hos Lekmer.se.' 
		FROM 
			[product].[tProductSeoSettingTranslation] pss
			INNER JOIN product.tProduct p ON p.ProductId = pss.ProductId
			INNER JOIN lekmer.tLekmerProduct l ON l.ProductId = p.ProductId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c2.CategoryId = c.ParentCategoryId
			INNER JOIN product.tCategory c3 ON c3.CategoryId = c2.ParentCategoryId
		WHERE
			c3.CategoryId = 1001310 -- Barnkläder
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))


		------------------------------------------------------------
		-- <Produktnivå: Start Inredning>  
		------------------------------------------------------------
		UPDATE
			pss
		SET
			pss.Title = p.Title + ' ' + c.Title + ' ' + c2.Title + ' ' + c3.Title + ' på nätet hos Lekmer.se',
			pss.[Description] = 'Köp ' + p.Title + ' från ' + b.Title + ' på nätet. '
			+ 'Du hittar även andra ' + c.Title + ' från ' + b.Title + ' hos Lekmer.se.' 
		FROM 
			[product].[tProductSeoSettingTranslation] pss
			INNER JOIN product.tProduct p ON p.ProductId = pss.ProductId
			INNER JOIN lekmer.tLekmerProduct l ON l.ProductId = p.ProductId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c.ParentCategoryId = c2.CategoryId
			INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
		WHERE
			c3.CategoryId = 1000494 -- Inredning
			AND pss.LanguageId = @LanguageId
			AND ((pss.Title IS NULL OR pss.Title = '') OR (pss.[Description] IS NULL OR pss.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
