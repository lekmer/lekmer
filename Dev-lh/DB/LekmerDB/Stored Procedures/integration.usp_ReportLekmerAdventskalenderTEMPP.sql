SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ReportLekmerAdventskalenderTEMPP]

AS
begin
	set nocount on
	begin try
		--begin transaction -- dumt med transaktion vid läsning, låser bara massa rader
		
		select 
			p.ProductId, 
			l.HYErpId,
			p.Title,
			p.NumberInStock,
			pli.PriceIncludingVat,
			isnull(z.DiscountPrice, pli.PriceIncludingVat) as DiscountPrice,
			isnull(((z.DiscountPrice/pli.PriceIncludingVat) * 100), 0) as Savings -- fel
		from 
			product.tProduct p
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId 
			inner join product.tPriceListItem pli
				on p.ProductId = pli.ProductId
				and PriceListId = 1
			left join (select distinct ProductId, MIN(DiscountPrice) as DiscountPrice 
			from lekmer.tProductDiscountActionItem
			where ProductActionId in
								(	-- 1000029
									select ProductActionId from campaign.tProductAction 
									where CampaignId in (
														select CampaignId from campaign.tCampaign
														where EndDate > GETDATE()
														and CampaignStatusId = 0
														)														
								)Group by
										ProductId) z
				on z.ProductId = p.ProductId
		where 
			CategoryId in
							(
								select CategoryId from product.tCategory
								where ParentCategoryId = 1001015
							)
		Order by
			p.ProductId							
							
		
		
	--commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
