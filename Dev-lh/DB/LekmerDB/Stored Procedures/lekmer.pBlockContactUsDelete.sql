SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockContactUsDelete]
@BlockId	int
AS 
BEGIN 
	DELETE 
		[lekmer].[tBlockContactUs]
	WHERE 
		[BlockId] = @BlockId
END 
GO
