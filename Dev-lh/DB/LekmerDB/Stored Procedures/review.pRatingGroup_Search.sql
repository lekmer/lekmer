SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroup_Search]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		rg.*
	FROM 
		[review].[vRatingGroup] rg 
	WHERE
		rg.[RatingGroup.Title] LIKE @SearchCriteria ESCAPE '\'
		OR rg.[RatingGroup.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
