SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [addon].[pCartItemPriceActionExcludeProductDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionExcludeProduct
	where CartActionId = @CartActionId
end
GO
