SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pProductWebShopTitleTranslationSave]
	@ProductId		INT,
	@LanguageId		INT,
	@Value			NVARCHAR(MAX)
AS
begin
	set nocount on
	
	update
		product.[tProductTranslation]
	set
		[WebShopTitle] = @Value	
	where
		[ProductId] = @ProductId
		AND [LanguageId] = @LanguageId
		
	if  @@ROWCOUNT = 0
	begin		
		insert into product.[tProductTranslation]
		(
			[ProductId],
			[LanguageId],
			[WebShopTitle]				
		)
		values
		(
			@ProductId,
			@LanguageId,
			@Value
		)
	end
end	
GO
