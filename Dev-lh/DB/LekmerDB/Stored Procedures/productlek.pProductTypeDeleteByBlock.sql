SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pProductTypeDeleteByBlock]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE [productlek].[tBlockProductType]
	WHERE [BlockId] = @BlockId
END
GO
