SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateGetAllByModel]
	@ModelId		int,
	@Page			int = NULL,
	@PageSize		int = NULL,
	@SortBy			varchar(20) = NULL,
	@SortAcsending	bit = NULL
AS
BEGIN
	
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
		
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY T.[' 
		+ COALESCE(@SortBy, 'Template.Title')
		+ ']'
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		T.*
		FROM [template].[vCustomTemplate] T 
		INNER JOIN [template].[vCustomModel] AS M ON  M.[Model.Id] = T.[Template.ModelId]
        WHERE M.[Model.Id] = @ModelId'
        
	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@ModelId int',
			@ModelId

	EXEC sp_executesql @sql, 
		N'	@ModelId int',
			@ModelId
END

GO
