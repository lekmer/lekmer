
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockProductSimilarListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		[psl].*,
		[b].*,
		[bs].*
	FROM 
		lekmer.vCustomBlockProductSimilarList AS psl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON psl.[BlockProductSimilarList.BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		psl.[BlockProductSimilarList.BlockId] = @BlockId
END
GO
