SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pAccessGetAll]
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomAccess]
end

GO
