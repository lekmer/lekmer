
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pImageGetAllBySizeTable]
	@ChannelId INT,
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[i].*
	FROM
		[media].[vCustomImage] i
		INNER JOIN [core].[tChannel] c ON [c].[LanguageId] = [i].[Image.LanguageId]
		INNER JOIN [lekmer].[tSizeTableMedia] stm ON [stm].[MediaId] = [i].[Media.Id]
		INNER JOIN [lekmer].[tSizeTableMediaProductRegistry] stmr ON stmr.[SizeTableMediaId] = stm.[SizeTableMediaId]
		INNER JOIN [product].[tProductModuleChannel] pmc ON pmc.[ProductRegistryId] = stmr.[ProductRegistryId]
	WHERE
		[c].[ChannelId] = @ChannelId
		AND
		[stm].[SizeTableId] = @SizeTableId
		AND
		pmc.[ChannelId] = @ChannelId
END
GO
