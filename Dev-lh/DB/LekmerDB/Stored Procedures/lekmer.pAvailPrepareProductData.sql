SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pAvailPrepareProductData]
AS
BEGIN
	SET NOCOUNT ON;
	
	-- Empty export tables
	TRUNCATE TABLE export.tAvailProductList
	TRUNCATE TABLE export.tAvailCategoryPath
	TRUNCATE TABLE export.tAvailProductData
	
	-- Fill table with products list
	INSERT INTO export.tAvailProductList
	SELECT ProductId FROM product.tProduct
	
	DECLARE
		@ProductId INT,
		@CategoryId INT,
		@Price_SE DECIMAL(16, 2),
		@Price_DK DECIMAL(16, 2),
		@Price_NO DECIMAL(16, 2),
		@Price_FI DECIMAL(16, 2),
		@CategoryUrl_SE VARCHAR(1000),
		@CategoryUrl_NO VARCHAR(1000),
		@CategoryUrl_DA VARCHAR(1000),
		@CategoryUrl_FI VARCHAR(1000),
		@ImageUrl_SE VARCHAR(1000),
		@ImageUrl_NO VARCHAR(1000),
		@ImageUrl_DA VARCHAR(1000),
		@ImageUrl_FI VARCHAR(1000),
		@Title    NVARCHAR(256),
		@Title_SE NVARCHAR(256),
		@Title_NO NVARCHAR(256),
		@Title_DA NVARCHAR(256),
		@Title_FI NVARCHAR(256),
		@NumberInStock INT,
		@NumberInStock_Size INT
	
	DECLARE
		    @Channel_SE INT,          @Channel_NO INT,          @Channel_DA INT,        
		 @LanguageId_SE INT,       @LanguageId_NO INT,       @LanguageId_DA INT,        
		@PriceListId_SE INT,      @PriceListId_NO INT,      @PriceListId_DA INT,        
		    @AppName_SE VARCHAR(100), @AppName_NO VARCHAR(100), @AppName_DA VARCHAR(100),
		    
		    @Channel_FI INT,
		 @LanguageId_FI INT,
		@PriceListId_FI INT,
		    @AppName_FI VARCHAR(100)
		
	SELECT @Channel_SE = ChannelId, @AppName_SE = ApplicationName FROM core.tChannel WHERE CommonName = 'Sweden'
	SELECT @Channel_NO = ChannelId, @AppName_NO = ApplicationName FROM core.tChannel WHERE CommonName = 'Norway'
	SELECT @Channel_DA = ChannelId, @AppName_DA = ApplicationName FROM core.tChannel WHERE CommonName = 'Denmark'
	SELECT @Channel_FI = ChannelId, @AppName_FI = ApplicationName FROM core.tChannel WHERE CommonName = 'Finland'

	SELECT @LanguageId_SE = LanguageId FROM core.tLanguage WHERE Title = 'Swedish'
	SELECT @LanguageId_NO = LanguageId FROM core.tLanguage WHERE Title = 'Norwegian'
	SELECT @LanguageId_DA = LanguageId FROM core.tLanguage WHERE Title = 'Danish'
	SELECT @LanguageId_FI = LanguageId FROM core.tLanguage WHERE Title = 'Finish'

	SELECT @PriceListId_SE = PriceListId FROM product.tPriceList WHERE CommonName = 'Sweden'
	SELECT @PriceListId_DA = PriceListId FROM product.tPriceList WHERE CommonName = 'Denmark'
	SELECT @PriceListId_NO = PriceListId FROM product.tPriceList WHERE CommonName = 'Norway'
	SELECT @PriceListId_FI = PriceListId FROM product.tPriceList WHERE CommonName = 'Finland'

	-- Fill table with categories path
	INSERT INTO export.tAvailCategoryPath
		SELECT CategoryId, @LanguageId_SE, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_SE)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_NO, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_NO)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_DA, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_DA)) FROM product.tCategory UNION ALL
		SELECT CategoryId, @LanguageId_FI, generic.fCleanUrl(generic.fGetCategoryUrlForProduct(CategoryId, @LanguageId_FI)) FROM product.tCategory

	------------------------------------------------------------------------------
	
	DECLARE product_cursor CURSOR FOR 
	SELECT ProductId FROM export.tAvailProductList

	OPEN product_cursor

	FETCH NEXT FROM product_cursor INTO @ProductId

	WHILE @@FETCH_STATUS = 0
	BEGIN

		-- Get product information
		SELECT
			@CategoryId = p.CategoryId,
			@Title = p.Title,
			@NumberInStock = p.NumberInStock
		FROM
			product.tProduct p
		WHERE
			p.ProductId = @ProductId


		-- Get stock number
		SET @NumberInStock_Size = (SELECT SUM(ps.NumberInStock) FROM lekmer.tProductSize ps WHERE ps.ProductId = @ProductId)
		IF @NumberInStock_Size IS NOT NULL
		BEGIN
			SET @NumberInStock = @NumberInStock_Size
		END


		-- Get price info
		SELECT
			@Price_SE = 0,
			@Price_NO = 0,
			@Price_DK = 0,
			@Price_FI = 0
		
		SELECT @Price_SE = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_SE
		SELECT @Price_NO = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_NO
		SELECT @Price_DK = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_DA
		SELECT @Price_FI = PriceIncludingVat FROM product.tPriceListItem WHERE ProductId = @ProductId AND PriceListId = @PriceListId_FI


		-- Get category urls
		SET @CategoryUrl_SE = (SELECT generic.fGetCategoryProductUrl(@AppName_SE, CategoryPath, @LanguageId_SE, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_SE)
		SET @CategoryUrl_NO = (SELECT generic.fGetCategoryProductUrl(@AppName_NO, CategoryPath, @LanguageId_NO, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_NO)
		SET @CategoryUrl_DA = (SELECT generic.fGetCategoryProductUrl(@AppName_DA, CategoryPath, @LanguageId_DA, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_DA)
		SET @CategoryUrl_FI = (SELECT generic.fGetCategoryProductUrl(@AppName_FI, CategoryPath, @LanguageId_FI, @ProductId) FROM export.tAvailCategoryPath WHERE CategoryId = @CategoryId AND LanguageId = @LanguageId_FI)


		-- Get image urls
		SELECT
			@ImageUrl_SE = generic.fGetImageUrl(@ProductId, @AppName_SE),
			@ImageUrl_NO = generic.fGetImageUrl(@ProductId, @AppName_NO),
			@ImageUrl_DA = generic.fGetImageUrl(@ProductId, @AppName_DA),
			@ImageUrl_FI = generic.fGetImageUrl(@ProductId, @AppName_FI)


		-- Get titles
		SET @Title_SE = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_SE)
		SET @Title_NO = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_NO)
		SET @Title_DA = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_DA)
		SET @Title_FI = (SELECT Title FROM product.tProductTranslation WHERE ProductId = @ProductId AND LanguageId = @LanguageId_FI)


		-- Insert avail product info
		INSERT INTO export.tAvailProductData (
			[ProductId],
			[PriceSE],
			[PriceDK],
			[PriceNO],
			[PriceFI],
			[CategoryUrlSE],
			[CategoryUrlNO],
			[CategoryUrlDA],
			[CategoryUrlFI],
			[ImageUrlSE],
			[ImageUrlNO],
			[ImageUrlDA],
			[ImageUrlFI],
			[TitleSE],
			[TitleNO],
			[TitleDA],
			[TitleFI],
			[NumberInStock]
		) VALUES (
			@ProductId,--[ProductId],
			@Price_SE,--[PriceSE],
			@Price_DK,--[PriceDK],
			@Price_NO,--[PriceNO],
			@Price_FI,--[PriceFI],
			@CategoryUrl_SE,--[CategoryUrlSE],
			@CategoryUrl_NO,--[CategoryUrlNO],
			@CategoryUrl_DA,--[CategoryUrlDA],
			@CategoryUrl_FI,--[CategoryUrlFI],
			@ImageUrl_SE,--[ImageUrlSE],
			@ImageUrl_NO,--[ImageUrlNO],
			@ImageUrl_DA,--[ImageUrlDA],
			@ImageUrl_FI,--[ImageUrlFI],
			COALESCE(@Title_SE, @Title),--[TitleSE],
			COALESCE(@Title_NO, @Title),--[TitleNO],
			COALESCE(@Title_DA, @Title),--[TitleDA],
			COALESCE(@Title_FI, @Title),--[TitleFI],
			@NumberInStock--[NumberInStock]
		)

		FETCH NEXT FROM product_cursor INTO @ProductId
	END

	CLOSE product_cursor;
	DEALLOCATE product_cursor;

END
GO
