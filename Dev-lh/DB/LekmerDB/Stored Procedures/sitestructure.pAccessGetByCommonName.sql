SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pAccessGetByCommonName]
@CommonName	varchar(50)
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomAccess]
	where
		[Access.CommonName] = @CommonName
end

GO
