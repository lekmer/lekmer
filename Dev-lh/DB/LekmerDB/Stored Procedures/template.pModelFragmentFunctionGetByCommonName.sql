SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionGetByCommonName]
	@CommonName	varchar(500)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomModelFragmentFunction]
	where
		[ModelFragmentFunction.CommonName] = @CommonName
end

GO
