
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [report].[pGiftCardVoucherUseedStatistic]
	@DateFrom	DATETIME = NULL,
	@DateTo		DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE @TempVouchersInfo TABLE (
		Campaign NVARCHAR(MAX),
		CampaignId INT,
		Channel VARCHAR(50),
		ChannelId INT,
		VouchersCountDay INT,
		VouchersSumDay DECIMAL(16,2)
	)

	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	IF @DateFrom IS NULL
		SET @DateFrom = CONVERT(DATE, DATEADD(DAY, -1, @CurrentDate))
	ELSE
		SET @DateFrom = CONVERT(DATE, @DateFrom)

	IF @DateTo IS NULL
		SET @DateTo = CONVERT(DATE, @CurrentDate)
	ELSE
		SET @DateTo = CONVERT(DATE, @DateTo)

	INSERT INTO @TempVouchersInfo
	SELECT
		ISNULL([c].[Title], 'CAMPAIGN HAS BEEN REMOVED. Discount Value:' + CAST(MAX([vi].[DiscountValue]) AS VARCHAR)),
		[gc].[CampaignId],
		[ch].[CommonName],
		[ch].[ChannelId],
		COUNT([gc].[VoucherCode]),
		SUM([vi].[AmountUsed])
	FROM
		[orderlek].[tOrderVoucherInfo] vi
		INNER JOIN [campaignlek].[tGiftCardViaMailInfo] gc ON [gc].[VoucherCode] = [vi].[VoucherCode] COLLATE Finnish_Swedish_CI_AS
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [vi].[OrderId]
		INNER JOIN [core].[tChannel] ch ON [ch].[ChannelId] = [o].[ChannelId]
		LEFT JOIN [campaign].[tCampaign] c ON [c].[CampaignId] = [gc].[CampaignId]
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] >= @DateFrom
		AND [o].[CreatedDate] < @DateTo
		AND [vi].[DiscountType] = 3
		AND [vi].[VoucherStatus] = 3
		AND [vi].[AmountUsed] IS NOT NULL
	GROUP BY
		[c].[Title], [gc].[CampaignId], [ch].[CommonName], [ch].[ChannelId]
	ORDER BY
		[c].[Title], [ch].[CommonName]

	SELECT
		[tvi].[Campaign] 'Campaign',
		[tvi].[Channel] 'Channel',
		MAX([tvi].[VouchersCountDay]) 'Count of used vouchers (DAY)',
		MAX([tvi].[VouchersSumDay]) 'Sum of used vouchers (DAY)',
		COUNT([vi].[VoucherCode]) 'Count of used vouchers (TOTAL)',
		SUM([vi].[AmountUsed]) 'Sum of used vouchers (TOTAL)'
	FROM
		@TempVouchersInfo tvi
		INNER JOIN [campaignlek].[tGiftCardViaMailInfo] gc ON [gc].[CampaignId] = [tvi].[CampaignId]
		INNER JOIN [orderlek].[tOrderVoucherInfo] vi ON [vi].[VoucherCode] = [gc].[VoucherCode] COLLATE Finnish_Swedish_CI_AS
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [vi].[OrderId] AND [o].[ChannelId] = [tvi].[ChannelId]
	WHERE
		[o].[OrderStatusId] = 4
		AND [vi].[DiscountType] = 3
		AND [vi].[VoucherStatus] = 3
		AND [vi].[AmountUsed] IS NOT NULL	
	GROUP BY
		[tvi].[Campaign], [tvi].[Channel]
	ORDER BY
		[tvi].[Campaign], [tvi].[Channel]
END
GO
