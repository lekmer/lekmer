
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionSaveResult]
	@TransactionId INT,
	@StatusCode INT,
	@FaultCode NVARCHAR(100),
	@FaultMessage NVARCHAR(MAX),
	@ErrorMessage NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tCollectorTransaction]
	SET
		[StatusCode] = @StatusCode,
		[FaultCode] = @FaultCode,
		[FaultMessage] = @FaultMessage,
		[ErrorMessage] = @ErrorMessage,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[TransactionId] = @TransactionId
END
GO
