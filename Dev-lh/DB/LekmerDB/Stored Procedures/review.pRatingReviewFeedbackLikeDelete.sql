SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedbackLikeDelete]
	@RatingReviewFeedbackId	INT
AS    
BEGIN
	DELETE [review].[tRatingReviewFeedbackLike]
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
