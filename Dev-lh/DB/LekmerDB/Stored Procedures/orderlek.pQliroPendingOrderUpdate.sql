SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroPendingOrderUpdate]
	@QliroPendingOrderId INT,
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tQliroPendingOrder]
	SET	
		[ChannelId] = @ChannelId,
		[OrderId] = @OrderId,
		[FirstAttempt] = @FirstAttempt,
		[LastAttempt] = @LastAttempt
	WHERE
		[QliroPendingOrderId] = @QliroPendingOrderId
END
GO
