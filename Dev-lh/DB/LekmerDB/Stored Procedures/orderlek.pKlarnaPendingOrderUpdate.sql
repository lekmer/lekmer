SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderUpdate]
	@KlarnaPendingOrderId INT,
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tKlarnaPendingOrder]
	SET	
		[ChannelId] = @ChannelId,
		[OrderId] = @OrderId,
		[FirstAttempt] = @FirstAttempt,
		[LastAttempt] = @LastAttempt
	WHERE
		[KlarnaPendingOrderId] = @KlarnaPendingOrderId
END
GO
