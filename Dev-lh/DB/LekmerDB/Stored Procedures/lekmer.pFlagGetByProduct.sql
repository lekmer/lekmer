SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pFlagGetByProduct]
	@ProductId INT,
	@LanguageId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT *
	FROM [lekmer].[vFlag]
	WHERE [Flag.LanguageId] = @LanguageId
		  AND [Flag.Id] IN (SELECT DISTINCT [tf].[FlagId]
							FROM [productlek].[tTagFlag] tf
							INNER JOIN [lekmer].[tProductTag] pt ON [pt].[TagId] = [tf].[TagId]
							WHERE [pt].[ProductId] = @ProductId)
END
GO
