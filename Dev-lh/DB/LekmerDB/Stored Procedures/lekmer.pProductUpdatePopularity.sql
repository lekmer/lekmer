
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUpdatePopularity]
	@ChannelId INT,
	@DateTimeFrom DATETIME,
	@DateTimeTo DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @ProductPopularity TABLE(ProductId INT, Popularity INT, SalesAmount INT)
	
	INSERT @ProductPopularity
	(
		ProductId,
		Popularity,
		SalesAmount
	)
	SELECT
		p.[ProductId],
		SUM(oi.[Quantity] * oi.[ActualPriceIncludingVat]),
		SUM(oi.[Quantity])
	FROM
		product.tProduct p WITH(NOLOCK)
		INNER JOIN [order].tOrderItemProduct oip WITH(NOLOCK) on oip.ProductId = p.ProductId
		INNER JOIN [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		INNER JOIN [order].tOrder o WITH(NOLOCK) on o.OrderId = oi.OrderId
	WHERE
		o.ChannelId = @ChannelId
		AND o.CreatedDate BETWEEN ISNULL(@DateTimeFrom, o.CreatedDate) AND ISNULL(@DateTimeTo, o.CreatedDate)
		AND o.OrderStatusId = 4
	GROUP BY
		p.ProductId

	DELETE	[lekmer].[tProductPopularity]
	WHERE	@ChannelId = [ChannelId]

	INSERT INTO [lekmer].[tProductPopularity]
	(
		ChannelId,
		ProductId,
		Popularity,
		SalesAmount
	)
	SELECT
		@ChannelId,
		p.[Product.Id],
		pp.[Popularity],
		pp.[SalesAmount]
	FROM
		@ProductPopularity pp
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = pp.ProductId
		INNER JOIN [product].[vCustomPriceListItem] pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				NULL
			)
	WHERE
		p.[Product.ChannelId] = @ChannelId
END
GO
