SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2ProductGetAllByBlockSecure]
	@BlockId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	SELECT
		*
	FROM
		[esales].[vBlockEsalesTopSellersV2ProductSecure] bp
		-- get price for current channel
		LEFT JOIN [product].[tProductRegistryProduct] prp ON [prp].[ProductId] = [bp].[Product.Id] AND [prp].[ProductRegistryId] = @ProductRegistryId
		LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = @ChannelId
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [bp].[Product.Id]
			AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](@CurrencyId, [bp].[Product.Id], [pmc].[PriceListRegistryId], NULL)
	WHERE
		[BlockEsalesTopSellersV2Product.BlockId] = @BlockId
	ORDER BY
		[BlockEsalesTopSellersV2Product.Position]
END
GO
