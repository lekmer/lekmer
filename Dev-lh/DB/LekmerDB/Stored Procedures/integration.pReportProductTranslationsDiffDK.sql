
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportProductTranslationsDiffDK]
AS 
BEGIN
	/*IN USE*/
	SET NOCOUNT ON
	
	DECLARE @ProductRegistryId INT = 3 -- Lekmer.dk
	DECLARE @LanguageId INT = 1000002  -- Danish

	SELECT
		[l].[HYErpId],
		[p].[Title]			AS 'SE Title' ,
		[p].[Description]	AS 'SE Description' ,
		[pt].[Title]		AS 'DK Title' ,
		[pt].[Description]	AS 'DK Description'
	FROM
		[lekmer].[tLekmerProduct] l
		INNER JOIN [product].[tProduct] p ON [l].[ProductId] = [p].[ProductId]
		LEFT JOIN [product].[tProductTranslation] pt ON [p].[ProductId] = [pt].[ProductId]
		
		LEFT JOIN [lekmer].[tProductRegistryRestrictionProduct] rp ON [rp].[ProductId] = [l].[ProductId] AND [rp].[ProductRegistryId] = @ProductRegistryId
		LEFT JOIN [lekmer].[tProductRegistryRestrictionBrand] rb ON [rb].[BrandId] = [l].[BrandId] AND [rb].[ProductRegistryId] = @ProductRegistryId
		-- Category level 3, 2, 1
		LEFT JOIN [product].[tCategory] c3 ON [c3].[CategoryId] = [p].[CategoryId]
		LEFT JOIN [product].[tCategory] c2 ON [c2].[CategoryId] = [c3].[ParentCategoryId]
		LEFT JOIN [product].[tCategory] c1 ON [c1].[CategoryId] = [c2].[ParentCategoryId]
		-- Category restrictions
		LEFT JOIN [lekmer].[tProductRegistryRestrictionCategory] [rc3] ON [rc3].[CategoryId] = [c3].[CategoryId] AND [rc3].ProductRegistryId = @ProductRegistryId
		LEFT JOIN [lekmer].[tProductRegistryRestrictionCategory] [rc2] ON [rc2].[CategoryId] = [c2].[CategoryId] AND [rc2].ProductRegistryId = @ProductRegistryId
		LEFT JOIN [lekmer].[tProductRegistryRestrictionCategory] [rc1] ON [rc1].[CategoryId] = [c1].[CategoryId] AND [rc1].ProductRegistryId = @ProductRegistryId
	WHERE
		[l].[ProductTypeId] = 1 -- exclude packages
		AND [p].[Title] != ''
		AND [p].[Description] != ''
		AND [pt].[LanguageId] = @LanguageId
		AND (
			[pt].[Description] IS NULL OR [pt].[Description] = ''
			OR
			[pt].[Title] IS NULL OR [pt].[Title] = ''
		)
		AND (
			[rp].[ProductId] IS NULL OR [rp].[RestrictionReason] = 'No Translation'
		)
		AND [rb].[BrandId] IS NULL
		AND [rc3].[CategoryId] IS NULL
		AND [rc2].[CategoryId] IS NULL
		AND [rc1].[CategoryId] IS NULL

	ORDER BY
		[l].[HYErpId]
END
GO
