SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartItemPriceActionExcludeCategoryInsert]
	@CartActionId			int,
	@CategoryId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionExcludeCategory
	(
		CartActionId,
		CategoryId
	)
	VALUES
	(
		@CartActionId,
		@CategoryId
	)
END 



GO
