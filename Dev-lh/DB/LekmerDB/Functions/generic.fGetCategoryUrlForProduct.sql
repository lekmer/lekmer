SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fGetCategoryUrlForProduct](
	@CategoryId INT,
	@LanguageId INT
)
RETURNS VARCHAR(1000)
AS
BEGIN

	DECLARE @CategoryPath VARCHAR(1000);

	WITH CategoryList (CategoryId, Lvl)
	AS
	(
		SELECT
			c.CategoryId,
			0
		FROM
			product.tCategory c
		WHERE
			CategoryId = @CategoryId

		UNION ALL

		SELECT
			cc.ParentCategoryId,
			Lvl+1
		FROM
			product.tCategory cc
			INNER JOIN CategoryList cl ON cc.CategoryId = cl.CategoryId
		WHERE
			cc.ParentCategoryId IS NOT NULL
	)

	SELECT @CategoryPath = (
		SELECT generic.fCleanCategory(COALESCE(ct.Title, cc.Title)) + '/'
		FROM
			CategoryList cl
			INNER JOIN product.tCategory cc ON cc.CategoryId = cl.CategoryId
			LEFT OUTER JOIN product.tCategoryTranslation ct ON ct.CategoryId = cc.CategoryId AND ct.LanguageId = @LanguageId
		ORDER BY
			cl.lvl DESC
		FOR XML PATH (''))

	SET @CategoryPath = REPLACE(@CategoryPath, ' ', '')

	RETURN @CategoryPath
END
GO
