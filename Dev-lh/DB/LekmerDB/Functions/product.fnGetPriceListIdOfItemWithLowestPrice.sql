
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [product].[fnGetPriceListIdOfItemWithLowestPrice]
(
	@CurrencyId INT,
	@ProductId INT,
	@PriceListRegistryId INT,
	@CustomerId INT
)
RETURNS INT
AS
BEGIN
	DECLARE @PriceListId INT
	
	DECLARE @DateNow DATETIME
	SET @DateNow = GETDATE()
	
	SELECT TOP(1)
		@PriceListId = pli.PriceListId		
	FROM
		product.tPriceListItem AS pli
		INNER JOIN product.tPriceList AS pl ON pl.PriceListId = pli.PriceListId
		LEFT JOIN product.tPriceListCustomerGroup AS cgp ON cgp.PriceListId = pl.PriceListId
	WHERE
		pl.PriceListStatusId = 0
		AND pl.PriceListRegistryId = @PriceListRegistryId
		AND pl.CurrencyId = @CurrencyId
		AND (pl.StartDateTime IS NULL OR pl.StartDateTime <= @DateNow)
		AND (pl.EndDateTime   IS NULL OR @DateNow <= pl.EndDateTime)
		AND pli.ProductId = @ProductId
		AND ( -- if customer is not defined then should be available all pricelists that are not assigned to any customer group
			cgp.CustomerGroupId IS NULL
			OR
			(
				@CustomerId IS NOT NULL
				AND
				cgp.CustomerGroupId IN (
					SELECT
						cg.CustomerGroupId
					FROM
						customer.tCustomerGroup AS cg
						INNER JOIN customer.tCustomerGroupCustomer AS cgc ON cgc.CustomerGroupId = cg.CustomerGroupId
					WHERE
						cg.StatusId = 0
						AND cgc.CustomerId = @CustomerId
				)
			)
		)
	ORDER BY
		pli.PriceIncludingVat

	RETURN @PriceListId
END
GO
