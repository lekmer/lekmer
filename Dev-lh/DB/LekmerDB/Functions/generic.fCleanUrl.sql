SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [generic].[fCleanUrl]
(
	@String	VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX)


--Change to lower case

Set @String = LOWER(@String)

--Remove diacritics
	
SET @String = REPLACE(@String,'ñ','n')

SET @String = REPLACE(@String,'è','e')
SET @String = REPLACE(@String,'é','e')
SET @String = REPLACE(@String,'ê','e')

SET @String = REPLACE(@String,'ö','o')
SET @String = REPLACE(@String,'õ','o')
SET @String = REPLACE(@String,'ô','o')
SET @String = REPLACE(@String,'ó','o')
SET @String = REPLACE(@String,'ø','o')

SET @String = REPLACE(@String,'í','i')
SET @String = REPLACE(@String,'ì','i')
SET @String = REPLACE(@String,'î','i')

SET @String = REPLACE(@String,'à','a')
SET @String = REPLACE(@String,'ä','a')
SET @String = REPLACE(@String,'á','a')
SET @String = REPLACE(@String,'å','a')

SET @String = REPLACE(@String,'ý','y')
SET @String = REPLACE(@String,'ÿ','y')

SET @String = REPLACE(@String,'ú','u')
SET @String = REPLACE(@String,'ü','u')
SET @String = REPLACE(@String,'û','u')

--Remove unwanted characters so only a-z, 0-9 and "-" are left

SET @String = REPLACE(@String,'æ','a')
SET @String = REPLACE(@String,'ø','o')
SET @String = REPLACE(@String,'€','e')
SET @String = REPLACE(@String,'£','l')
SET @String = REPLACE(@String,'$','s')
SET @String = REPLACE(@String,'&','o')
SET @String = REPLACE(@String,'*','x')
SET @String = REPLACE(@String,'\','-')
SET @String = REPLACE(@String,' ','-')
SET @String = REPLACE(@String,'+','-')
SET @String = REPLACE(@String,'=','-')
SET @String = REPLACE(@String,'!','-')
SET @String = REPLACE(@String,'?','-')
SET @String = REPLACE(@String,':','-')
SET @String = REPLACE(@String,';','-')
SET @String = REPLACE(@String,',','-')
SET @String = REPLACE(@String,'.','-')
SET @String = REPLACE(@String,'_','-')


SELECT @String = REPLACE(@String,CHAR(badcode),'') from dbo.UrlCleaner

--Remove duplicate "-".
WHILE charindex('--', @string) > 0
BEGIN
  SET @string = REPLACE(@string, '--', '-')
END

SET @String = CASE
				WHEN Left(@String,1) = '-' THEN SUBSTRING(@String,2,LEN(@String)) ELSE @String
				END
SET @String = CASE				
				WHEN Right(@String,1) = '-' THEN SUBSTRING(@String,1,LEN(@String)-1) ELSE @String
				END

RETURN (SELECT @String)
END
GO
