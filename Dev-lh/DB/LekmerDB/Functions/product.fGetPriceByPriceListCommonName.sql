SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE FUNCTION [product].[fGetPriceByPriceListCommonName] 
(
	@productId int,
	@priceListCommonName varchar(50)
)
RETURNS decimal
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result decimal

SELECT @Result =  PLI.PriceIncludingVat
FROM       product.tPriceListItem AS PLI INNER JOIN
           product.tPriceList AS PL ON PLI.PriceListId = PL.PriceListId
WHERE PL.CommonName = @priceListCommonName and PLI.ProductId = @productId

IF @Result IS NULL
SET @Result =0
	-- Return the result of the function
	RETURN @Result

END


GO
