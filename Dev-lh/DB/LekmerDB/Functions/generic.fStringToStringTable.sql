SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [generic].[fStringToStringTable](
   @SepString nvarchar(max),
   @Sep nvarchar(10),
   @RemoveEmptyEntries bit
) returns @t table(SepString nvarchar(max))
as
begin
   declare @Pos int
          ,@PrevPos int
          ,@Line nvarchar(max)
          ,@TmpString nvarchar(max)

   set @TmpString = @SepString

   if right(@TmpString, len(@Sep)) <> @Sep
   begin
      set @TmpString = @TmpString + @Sep
   end

   set @Pos = charindex(@Sep, @TmpString)
   set @PrevPos = 1
   
   while @Pos > 0
   begin
      set @Line = substring(@TmpString, @PrevPos, @Pos - @PrevPos)
      
      if @Line <> '' or @RemoveEmptyEntries = 0
      begin
         insert @t values(@Line)
      end
      
      set @PrevPos = @Pos + len(@Sep)
      set @Pos = charindex(@Sep, @TmpString, @Pos + 1)
   end
   
   return
end
GO
