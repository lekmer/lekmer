SET IDENTITY_INSERT [template].[tModelSetting] ON
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000010, 1000049, N'Page size options', N'PageSizeOptions', 1)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000011, 1000049, N'Default page size', N'DefaultPageSize', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000012, 1000049, N'Number of columns', N'ColumnCount', 2)
SET IDENTITY_INSERT [template].[tModelSetting] OFF
SET IDENTITY_INSERT [template].[tModelSetting] ON
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (25, 54, N'Max display count', N'MaxDisplayCount', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (26, 26, N'Expand folders', N'ExpandFolders', 3)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000001, 1000010, N'Page size options', N'PageSizeOptions', 1)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000002, 1000010, N'Default page size', N'DefaultPageSize', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000003, 1000010, N'Number of columns', N'ColumnCount', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000005, 1000018, N'Max quantity', N'MaxQuantity', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000006, 1000019, N'Secret code', N'SecretCode', 1)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000007, 31, N'Max quantity', N'MaxQuantity', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000008, 1000048, N'Max quantity', N'MaxQuantity', 2)
INSERT INTO [template].[tModelSetting] ([ModelSettingId], [ModelId], [Title], [CommonName], [ModelSettingTypeId]) VALUES (1000009, 1000048, N'Allow postback', N'AllowPostback', 3)
SET IDENTITY_INSERT [template].[tModelSetting] OFF
