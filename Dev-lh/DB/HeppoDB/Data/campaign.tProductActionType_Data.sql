SET IDENTITY_INSERT [campaign].[tProductActionType] ON
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1000005, N'Fixed price', N'FixedPrice')
SET IDENTITY_INSERT [campaign].[tProductActionType] OFF
SET IDENTITY_INSERT [campaign].[tProductActionType] ON
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1, N'Percentage price discount', N'PercentagePriceDiscount')
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1000001, N'Product discount', N'ProductDiscount')
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1000002, N'Fixed discount', N'FixedDiscount')
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1000003, N'Gift card via email', N'GiftCardViaMailProduct')
INSERT INTO [campaign].[tProductActionType] ([ProductActionTypeId], [Title], [CommonName]) VALUES (1000004, N'Free freight flag', N'FreeFreightFlag')
SET IDENTITY_INSERT [campaign].[tProductActionType] OFF
