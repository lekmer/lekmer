SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListDelete]
	@BlockId INT
AS
BEGIN
	EXEC [review].[pBlockLatestFeedbackListBrandDeleteAll] @BlockId

	DELETE [review].[tBlockLatestFeedbackList]
	WHERE BlockId = @BlockId
END
GO
