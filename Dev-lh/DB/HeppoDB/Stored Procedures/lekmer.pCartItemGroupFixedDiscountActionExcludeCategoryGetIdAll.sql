SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
