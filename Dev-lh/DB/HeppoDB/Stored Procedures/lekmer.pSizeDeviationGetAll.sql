
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeDeviationGetAll]
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vSizeDeviation]
END
GO
