SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupCategorySave]
	@RatingGroupId INT,
	@CategoryId INT,
	@IncludeSubCategories BIT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[review].[tRatingGroupCategory]
	SET 
		[IncludeSubCategories] = @IncludeSubCategories
	WHERE
		[RatingGroupId] = @RatingGroupId
		AND
		[CategoryId] = @CategoryId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingGroupCategory] (
			[RatingGroupId],
			[CategoryId],
			[IncludeSubCategories]
		)
		VALUES (
			@RatingGroupId,
			@CategoryId,
			@IncludeSubCategories
		)
	END
END
GO
