SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSettingSave]
    @ModelSettingId INT,
    @ModelId INT,
    @Title NVARCHAR(50),
    @CommonName NVARCHAR(50),
    @ModelSettingTypeId INT
AS 
BEGIN
    SET NOCOUNT ON
    
    UPDATE
		[template].[tModelSetting]
    SET
		[ModelId] = @ModelId,
		[Title] = @Title,
		[CommonName] = @CommonName,
		[ModelSettingTypeId] = @ModelSettingTypeId
    WHERE
		[ModelSettingId] = @ModelSettingId
		
    IF @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tModelSetting]
        (
          [ModelId],
          [Title],
          [CommonName],
          [ModelSettingTypeId]
	    )
        VALUES  
        (
          @ModelId,
          @Title,
          @CommonName,
          @ModelSettingTypeId
        )

        SET @ModelSettingId = SCOPE_IDENTITY()
    END
    
    RETURN @ModelSettingId

END
GO
