SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateSitestructureSeoSmyckenCategories]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@CHannelId			INT
AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Smycken>		
		------------------------------------------------------------
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		
		DECLARE @StartPageJewelleryMenContentPageId INT
		DECLARE @StartPageJewelleryWomenContentPageId INT
		DECLARE @StartPageJewelleryContentPageId INT
		DECLARE @MenJewelleryCategoriesContentPageId INT
		DECLARE @WomenJewelleryCategoriesContentPageId INT
		DECLARE @AllJewelleryCategoriesContentPageId INT
		
		SET @SiteStructureRegistryId = (SELECT SiteStructureRegistryId FROM sitestructure.tSiteStructureModuleChannel WHERE ChannelId = @ChannelId)
												
		SET @StartPageJewelleryMenContentPageId = (SELECT ContentNodeId FROM sitestructure.tContentNode 
												   WHERE SiteStructureRegistryId = @SiteStructureRegistryId
												   AND CommonName = 'startPageJewelleryMen')
									
		SET @StartPageJewelleryWomenContentPageId = (SELECT ContentNodeId FROM sitestructure.tContentNode 
													 where SiteStructureRegistryId = @SiteStructureRegistryId
													 AND CommonName = 'startPageJewelleryWomen')
					
		SET @StartPageJewelleryContentPageId = (SELECT ContentNodeId FROM sitestructure.tContentNode 
												WHERE SiteStructureRegistryId = @SiteStructureRegistryId 
												AND CommonName = 'startPageJewellery')
									
		SET @MenJewelleryCategoriesContentPageId = (SELECT ContentNodeId FROM sitestructure.tContentNode 
													WHERE SiteStructureRegistryId = @SiteStructureRegistryId 
													AND CommonName = 'menJewelleryCategories')
							
		SET @WomenJewelleryCategoriesContentPageId = (SELECT ContentNodeId FROM sitestructure.tContentNode 
													  WHERE SiteStructureRegistryId = @SiteStructureRegistryId 
													  AND CommonName = 'womenJewelleryCategories')
							
		SET @AllJewelleryCategoriesContentPageId = (SELECT ContentNodeId FROM sitestructure.tContentNode 
													WHERE SiteStructureRegistryId = @SiteStructureRegistryId 
													AND CommonName = 'allJewelleryCategories')
									
		-- DEFAULT --
		-- Insert new contentnodeIds in tContentPageSeoSetting
		INSERT INTO sitestructure.tContentPageSeoSetting (ContentNodeId)
		SELECT
			n.ContentNodeId
		FROM 
			sitestructure.tContentNode n
		WHERE 
			n.SiteStructureRegistryId = @SiteStructureRegistryId
			AND n.ContentNodeTypeId = 3 -- contentpages
			AND n.ContentNodeId NOT IN (SELECT ContentNodeId FROM sitestructure.tContentPageSeoSetting)

		UPDATE
			cps
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[Underkategori]', cn1.Title),
			@NewTitle = REPLACE(@NewTitle, '[Huvudkategori]', cn3.Title),
				
			cps.Title = @NewTitle,
			
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[Underkategori]', cn1.Title),
			@NewDescription = REPLACE(@NewDescription, '[Huvudkategori]', cn3.Title),
			
			cps.[Description] = @NewDescription
		FROM			
			sitestructure.tContentPageSeoSetting cps			
			INNER JOIN sitestructure.tContentNode cn1 ON cn1.ContentNodeId = cps.ContentNodeId				
			INNER JOIN sitestructure.tContentNode cn2 ON cn2.ContentNodeId = cn1.ParentContentNodeId
			INNER JOIN sitestructure.tContentNode cn3 ON cn2.ContentNodeId = cn1.ParentContentNodeId
		WHERE
			cn1.SiteStructureRegistryId = @SiteStructureRegistryId
			AND ((cn3.ContentNodeId = @StartPageJewelleryWomenContentPageId AND cn2.ContentNodeId = @WomenJewelleryCategoriesContentPageId) -- damskor, kategorier
			OR   (cn3.ContentNodeId = @StartPageJewelleryMenContentPageId AND cn2.ContentNodeId = @MenJewelleryCategoriesContentPageId) -- herrskor, kategorier
			OR   (cn3.ContentNodeId = @StartPageJewelleryContentPageId AND cn2.ContentNodeId = @AllJewelleryCategoriesContentPageId)) -- Alla kategorier
			--AND
			--	((cps.Title IS NULL OR cps.Title = '')
			--	OR (cps.[Description] IS NULL OR cps.[Description] = ''))

	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] (Data, [Message], [Date], OcuredInProcedure)
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
