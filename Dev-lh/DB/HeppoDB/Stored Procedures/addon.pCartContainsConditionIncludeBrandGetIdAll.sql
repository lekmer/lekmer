SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeBrandGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT
		cib.[BrandId]
	FROM
		[addon].[tCartContainsConditionIncludeBrand] cib
	WHERE
		cib.[ConditionId] = @ConditionId
END
GO
