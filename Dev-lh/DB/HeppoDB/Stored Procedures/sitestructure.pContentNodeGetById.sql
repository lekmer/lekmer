SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeGetById]
@ContentNodeId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentNodeSecure]
	where
		[ContentNode.ContentNodeId] = @ContentNodeId
end

GO
