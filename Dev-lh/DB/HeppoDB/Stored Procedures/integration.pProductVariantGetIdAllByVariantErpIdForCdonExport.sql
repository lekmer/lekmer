SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductVariantGetIdAllByVariantErpIdForCdonExport]
	@ProductId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	-- Allowed product ids
	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId])EXEC [export].[pProductGetIdAllAvaliableForCdonExport]
	
	-- Get variant id
	DECLARE @VariantErpId VARCHAR(50)
	SELECT @VariantErpId = [product].[fGetVariantErpIdByProductId] (@ProductId)
	
	SELECT
		DISTINCT [p].[ProductId]
	FROM
		[product].[tProduct] p
	WHERE
		[p].[ProductId] <> @ProductId
		AND @VariantErpId = (SELECT [product].[fGetVariantErpIdByProductId] ([p].[ProductId]))
		AND [p].[ProductId] IN (
			SELECT DISTINCT ([up].[ProductId])
			FROM (SELECT * FROM #ProductIds) up)

	DROP TABLE #ProductIds
END
GO
