SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionExcludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
