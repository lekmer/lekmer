SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockTypeGetAllByContentPageTypeId]
	@ContentPageTypeId int
as
begin
	SELECT  
		B.*
	FROM 
		[sitestructure].[vCustomBlockType] B
	WHERE 
		B.[BlockType.AvailableForAllPageTypes] = 1
		OR B.[BlockType.BlockTypeId] IN (SELECT pb.BlockTypeId
		                                 FROM [sitestructure].[tContentPageTypeBlockType] pb 
		                                 WHERE pb.ContentPageTypeId = @ContentPageTypeId)
	ORDER BY 
		B.[BlockType.Title]
END

GO
