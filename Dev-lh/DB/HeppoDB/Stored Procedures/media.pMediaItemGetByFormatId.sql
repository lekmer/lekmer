SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-26
-- Description:	Gets a media items by FormatId
-- =============================================
CREATE PROCEDURE [media].[pMediaItemGetByFormatId]
	@MediaFormatId INT
AS
BEGIN
	SET NOCOUNT ON ;
	SELECT 
		*
	FROM   [media].[vCustomMedia]
	WHERE  [Media.FormatId] = @MediaFormatId
END

GO
