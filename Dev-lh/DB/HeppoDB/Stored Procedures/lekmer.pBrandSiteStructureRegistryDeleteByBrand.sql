SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureRegistryDeleteByBrand]
	@BrandId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE lekmer.tBrandSiteStructureRegistry
	WHERE BrandId = @BrandId
END

GO
