
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_REATagProduct]

AS
BEGIN	
	begin try
	begin transaction

		-- 1. untag all products as "rea (Sale)"
		-- 2. get all active campaigns 
		-- 3. get all actions related to those active campaigns
		-- 4. tag all products assosiated with these Actions/campaigns with Rea Tag
		declare @TagId int
		set @TagId = (select TagId from lekmer.tTag where Value = 'Sale')
		
		delete from lekmer.tProductTag
		where TagId = @TagId
			
		INSERT INTO lekmer.tProductTag (
			ProductId, TagId
		)
		SELECT 
			DISTINCT [pdai].[ProductId], @TagId
		FROM 
			campaign.tProductAction pa
			INNER JOIN lekmer.tProductDiscountActionItem pdai ON pdai.ProductActionId = pa.ProductActionId
			WHERE CampaignId IN (SELECT CampaignId FROM campaign.tCampaign WHERE EndDate > GETDATE() AND CampaignStatusId = 0)
		UNION
		SELECT 
			DISTINCT ip.[ProductId], @TagId
		FROM 
			campaign.tProductAction pa
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [pa].[ProductActionId]
			CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ppda].[ProductActionId]) ip
			WHERE CampaignId IN (SELECT CampaignId FROM campaign.tCampaign WHERE EndDate > GETDATE() AND CampaignStatusId = 0)
				
		--rollback
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
END

GO
