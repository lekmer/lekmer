SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeBrandDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedDiscountActionIncludeBrand]
	WHERE
		ProductActionId = @ProductActionId
END
GO
