SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 24.01.2009
Description: Created

*****************  Version 2  *****************
User: Mykhaylo A.		Date: 26.03.2009
Description: changed order by
*/


CREATE PROCEDURE [template].[pAliasFolderGetAll]
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomAliasFolder]
	order by
		[AliasFolder.Title] asc
end

GO
