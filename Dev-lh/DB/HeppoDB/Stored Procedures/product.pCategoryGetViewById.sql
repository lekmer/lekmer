SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategoryGetViewById]
	@CategoryId	int,
	@ChannelId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Category.Id] = @CategoryId
		and c.[Channel.Id] = @ChannelId
end
GO
