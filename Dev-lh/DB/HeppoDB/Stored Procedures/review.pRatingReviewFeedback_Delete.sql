
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_Delete]
	@RatingReviewFeedbackId	INT
AS    
BEGIN
	SET NOCOUNT ON
	
	EXEC [review].[pRatingReviewFeedbackLikeDelete] @RatingReviewFeedbackId
	EXEC [review].[pRatingItemProductVoteDelete] @RatingReviewFeedbackId
	EXEC [review].[pReviewDeleteAll] @RatingReviewFeedbackId
	
	DELETE FROM [review].[tRatingReviewFeedback]
	WHERE RatingReviewFeedbackId = @RatingReviewFeedbackId
END
GO
