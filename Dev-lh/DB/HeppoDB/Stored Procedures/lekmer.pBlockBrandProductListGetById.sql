SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandProductListGetById]
 @LanguageId	int,
	@BlockId	int
as
begin
	select 
		ba.*,
		b.*,
		ps.*
	from 
		[lekmer].[tBlockBrandProductList] as ba
		inner join [sitestructure].[vCustomBlock] as b on ba.[BlockId] = b.[Block.BlockId]
		inner join product.vCustomProductSortOrder as ps on ps.[ProductSortOrder.Id] = ba.[ProductSortOrderId]
	where
		ba.[BlockId] = @BlockId 
		AND b.[Block.LanguageId] = @LanguageId
end


GO
