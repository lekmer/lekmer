SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-20
-- Description:	Saves a mediaFolder
-- =============================================
CREATE PROCEDURE [media].[pMediaFolderSave]
	@MediaFolderId INT,
	@MediaFolderParentId INT,	
	@Title NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON
	
	--Check that parent folder has folders with same name
	IF EXISTS (
		SELECT
			1
		FROM
			[media].[tMediaFolder]
		WHERE
			[Title] = @Title
			AND [MediaFolderId] <> @MediaFolderId
			And	(
				(@MediaFolderParentId IS NULL And MediaFolderParentId IS NULL)
				OR MediaFolderParentId = @MediaFolderParentId
				)
	)
	RETURN -1
	
	UPDATE [media].[tMediaFolder]
	    SET    [MediaFolderParentId] = @MediaFolderParentId,
	           [Title] = @Title	          
	    WHERE  [MediaFolderId] = @MediaFolderId
	IF @@ROWCOUNT = 0
	BEGIN
	    INSERT [media].[tMediaFolder] 
	    (
			MediaFolderParentId,
			Title
		) 
		VALUES 
		( 
			@MediaFolderParentId,
			@Title 
		) 	      
	    
	    SET @MediaFolderId = SCOPE_IDENTITY()
	END
	RETURN @MediaFolderId
END



GO
