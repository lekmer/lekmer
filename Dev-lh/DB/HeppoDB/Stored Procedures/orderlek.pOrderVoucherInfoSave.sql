SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pOrderVoucherInfoSave]
	@OrderId INT,
	@VoucherCode NVARCHAR(100),
	@DiscountType INT,
	@DiscountValue DECIMAL(16,2),
	@AmountUsed DECIMAL(16,2),
	@AmountLeft DECIMAL(16,2),
	@VoucherStatus INT
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderVoucherInfo]
	SET 
			[VoucherCode] = @VoucherCode,
			[DiscountType] = @DiscountType,
			[DiscountValue] = @DiscountValue,
			[AmountUsed] = @AmountUsed,
			[AmountLeft] = @AmountLeft,
			[VoucherStatus] = @VoucherStatus
	WHERE	
			[OrderId] = @OrderId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [orderlek].[tOrderVoucherInfo]
		(
			[OrderId],
			[VoucherCode],
			[DiscountType],
			[DiscountValue],
			[AmountUsed],
			[AmountLeft],
			[VoucherStatus]
		)
		VALUES
		(
			@OrderId,
			@VoucherCode,
			@DiscountType,
			@DiscountValue,
			@AmountUsed,
			@AmountLeft,
			@VoucherStatus
		)
	END
END
GO
