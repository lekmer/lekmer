SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@ProductActionId	INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[campaignlek].[tFixedPriceActionExcludeProduct] fdaep
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = fdaep.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		fdaep.[ProductActionId] = @ProductActionId
		AND p.[Product.ChannelId] = @ChannelId
END

GO
