SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductModuleChannelGetAllByReferentContentNode]
	@ContentNodeId	INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @tContentNode TABLE (tContentNodeId INT, tParentContentNodeId INT);

	WITH ContentNode (ContentNodeId, ParentContentNodeId) AS 
	(
		SELECT ContentNodeId, ParentContentNodeId
		FROM  sitestructure.tContentNode
		WHERE ParentContentNodeId = @ContentNodeId

		UNION ALL

		SELECT C.ContentNodeId, C.ParentContentNodeId
		FROM sitestructure.tContentNode C 
		INNER JOIN ContentNode OuterC ON OuterC.ContentNodeId = C.ParentContentNodeId
	)
	INSERT INTO @tContentNode (tContentNodeId, tParentContentNodeId)
	SELECT ContentNodeId, ParentContentNodeId FROM ContentNode
		
	UNION ALL

	SELECT ContentNodeId, ParentContentNodeId
	FROM  sitestructure.tContentNode
	WHERE ContentNodeId = @ContentNodeId
	
	SELECT 
		vPMC.*
	FROM
		product.vCustomProductModuleChannel vPMC	
		INNER JOIN @tContentNode CN ON vPMC.[ProductModuleChannel.ProductTemplateContentNodeId] = CN.tContentNodeId
	
END

GO
