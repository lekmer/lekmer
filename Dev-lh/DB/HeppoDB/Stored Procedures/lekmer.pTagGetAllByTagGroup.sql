
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByTagGroup]
	@LanguageId INT,
	@TagGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[t].*
	FROM
		lekmer.[vTag] t
	WHERE
		[Tag.TagGroupId] = @TagGroupId
		AND [Tag.LanguageId] = @LanguageId
END
GO
