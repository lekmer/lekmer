SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriber_Save]
	@ChannelId		INT,
	@Email			VARCHAR(320),
	@UnregFromAll	BIT,
	@CreatedDate	DATETIME,
	@UpdateDate		DATETIME
AS
BEGIN
	DECLARE @Id INT
	UPDATE
		[lekmer].[tNewsletterUnsubscriber]
	SET
		[UnregFromAll] = @UnregFromAll,
		[UpdatedDate] = @UpdateDate,
		@Id = [UnsubscriberId]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tNewsletterUnsubscriber]
		(
			[ChannelId],
			[Email],
			[UnregFromAll],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES
		(
			@ChannelId,
			@Email,
			@UnregFromAll,
			@CreatedDate,
			@UpdateDate
		)
		SET @Id = SCOPE_IDENTITY()
	END
	RETURN @Id
END
GO
