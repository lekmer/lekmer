SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionIncludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionIncludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
