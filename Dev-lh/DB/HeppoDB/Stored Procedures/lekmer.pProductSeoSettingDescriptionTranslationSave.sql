SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pProductSeoSettingDescriptionTranslationSave]
	@ProductId	INT,
	@LanguageId	INT,
	@Value		NVARCHAR(MAX)
AS
begin
	set nocount on
	
	update
		product.[tProductSeoSettingTranslation]
	set
		[Description] = @Value	
	where
		[ProductId] = @ProductId
		AND [LanguageId] = @LanguageId
		
	if  @@ROWCOUNT = 0
	begin		
		insert into product.[tProductSeoSettingTranslation]
		(
			[ProductId],
			[LanguageId],
			[Description]				
		)
		values
		(
			@ProductId,
			@LanguageId,
			@Value
		)
	end
end	
GO
