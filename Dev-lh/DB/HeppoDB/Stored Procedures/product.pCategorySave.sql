SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pCategorySave]
	@CategoryId			int,
	@Title				nvarchar(50)
as
begin
	set nocount on	
	
	update
		[product].[tCategory]
	set
		[Title] = @Title
	where
		[CategoryId] = @CategoryId
end
GO
