SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pCultureGetAll]
as
begin
	set nocount on
	
	select
		*
	from
		[core].[vCustomCulture]
end

GO
