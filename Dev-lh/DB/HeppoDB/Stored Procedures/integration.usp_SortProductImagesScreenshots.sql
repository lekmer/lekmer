SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ändra sen så att den kan hantera en lista av Ids
CREATE PROCEDURE [integration].[usp_SortProductImagesScreenshots]
	
AS
begin
	set nocount on
	
	if object_id('tempdb..#tSortProductImagesScreenshots') is not null
		drop table #tSortProductImagesScreenshots
		
	
	create table #tSortProductImagesScreenshots
	(
		ProductId int not null,
		MediaId int not null,
		Ordinal int not null,
		ExpectedOrdinal int null
		constraint PK_#tProductImageMediaIdToDelete primary key(productid, mediaid)
	)
	
	
	begin try
		begin transaction
			--coalesce(ois.ErpId, p.ErpId) ErpId
		
		
		insert into #tSortProductImagesScreenshots(ProductId, MediaId, Ordinal, ExpectedOrdinal)
		select 
			p.ProductId, 
			p.MediaId, 
			Ordinal, 
			RIGHT(im.AlternativeText, 1) as a
		from 
			product.tProductImage p -- i ordinal here
				inner join media.tImage im
				on im.MediaId = p.MediaId
		where 
			im.AlternativeText is not null
	
		
		--select * from #tSortProductImagesScreenshots
	
		
		update 
			p
		set 
			Ordinal = (case when s.ExpectedOrdinal = 0 then p.Ordinal else s.ExpectedOrdinal end)
		from 
			product.tProductImage p
				inner join #tSortProductImagesScreenshots s
				on p.ProductId = s.ProductId
				and p.MediaId = s.MediaId


		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
