SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [customer].[pBlockRegisterSave]
	@BlockId				INT,
	@RedirectContentNodeId	INT
AS
BEGIN
	
	UPDATE
		[customer].[tBlockRegister]
	SET
		[RedirectContentNodeId]	= @RedirectContentNodeId
	WHERE
		[BlockId] = @BlockId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [customer].[tBlockRegister]
		( 
			[BlockId], 
			[RedirectContentNodeId] 
		)
		VALUES
		(
			@BlockId, 
			@RedirectContentNodeId 
		)
	END
END
GO
