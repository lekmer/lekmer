SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure test.pProductConnectAllToRandomRelationList
as
begin
	insert
		product.tProductRelationList
	(
		ProductId,
		RelationListId
	)
	select
		ProductId,
		(
			select top 1
				RelationListId
			from
				product.tRelationList
			order by
				newid()
		)
	from
		product.tProduct
	where
		ProductId not in (
			select
				ProductId
			from
				product.tProductRelationList
		)
end
GO
