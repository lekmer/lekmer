SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateBrandHeppoCreationDate]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	
	begin try
	begin transaction
	
								
	-- Add created date in integration.[tBrandCreation]
	INSERT INTO 
		integration.[tBrandCreation](BrandId, CreatedDate)
	select
		u.BrandId,
		GETDATE()
	from
		lekmer.tBrand u
	Where
		u.BrandId not in (select BrandId 
							from integration.[tBrandCreation])
							
							
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        
END


GO
