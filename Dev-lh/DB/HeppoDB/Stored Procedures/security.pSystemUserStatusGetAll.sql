SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserStatusGetAll]
as
begin
	set nocount on

	select
		*
	from
		[security].[vCustomSystemUserStatus]
end

GO
