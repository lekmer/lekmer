SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreStatusGetById]
	@StoreStatusId	int
as
begin
	select
		*
	from
		[product].[vCustomStoreStatus] c
	where
		c.[StoreStatus.Id] = @StoreStatusId
end

GO
