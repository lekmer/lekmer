
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionGetById]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		vc.*
	FROM
		[lekmer].[vVoucherCondition] vc
	WHERE
		[vc].[Condition.Id] = @ConditionId
END
GO
