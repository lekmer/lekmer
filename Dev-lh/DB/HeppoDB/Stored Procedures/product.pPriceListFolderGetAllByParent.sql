SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListFolderGetAllByParent]
@ParentPriceListFolderId int
as
begin
	select
		*
	from
		[product].[vCustomPriceListFolder] pf
	where 
		pf.[PriceListFolder.ParentPriceListFolderId] = @ParentPriceListFolderId
end

GO
