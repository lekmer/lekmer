SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryIconDeleteAll]
	@CategoryId	INT
AS 
BEGIN 
	DELETE 
		[lekmer].[tCategoryIcon]
	WHERE
		[CategoryId] = @CategoryId
END
GO
