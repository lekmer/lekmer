SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@NumberOfItems INT,
	@CategoryId INT,
	@RatingId INT
AS
BEGIN
	UPDATE [review].[tBlockBestRatedProductList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[NumberOfItems] = @NumberOfItems,
		[CategoryId] = @CategoryId,
		[RatingId] = @RatingId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockBestRatedProductList] (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[NumberOfItems],
		[CategoryId],
		[RatingId]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@NumberOfItems,
		@CategoryId,
		@RatingId
	)
END
GO
