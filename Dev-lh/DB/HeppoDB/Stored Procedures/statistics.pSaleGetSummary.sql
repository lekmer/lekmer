SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [statistics].[pSaleGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN	
	SELECT 
		isnull(sum(oi.[ActualPriceIncludingVat] * oi.[Quantity]), 0)
	FROM
		[order].tOrderItem oi WITH(NOLOCK)
		INNER JOIN [order].tOrder o WITH(NOLOCK) ON oi.OrderId = o.OrderId
	WHERE
		o.[ChannelId] = @ChannelId
		AND o.[CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
END
GO
