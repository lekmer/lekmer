SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeProductGetIdAllSecure]
	@ConfigId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tCampaignActionExcludeProduct] caep
		INNER JOIN [product].[tProduct] p ON caep.[ProductId] = p.[ProductId]
	WHERE
		caep.[ConfigId] = @ConfigId
		AND p.[IsDeleted] = 0
END
GO
