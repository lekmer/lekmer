
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pAvailGetProductData]
AS
BEGIN
	SET NOCOUNT ON;
	
	EXEC [lekmer].[pAvailPrepareProductData]
	
	SELECT * FROM export.tAvailProductData
END
GO
