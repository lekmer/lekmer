SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionConfiguratorSave]
	@ConfiguratorId		INT,
	@IncludeAllProducts	BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tCampaignActionConfigurator]
	SET
		[IncludeAllProducts] = @IncludeAllProducts
	WHERE
		[CampaignActionConfiguratorId] = @ConfiguratorId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT	INTO [campaignlek].[tCampaignActionConfigurator]
				( [IncludeAllProducts] )
		VALUES
				( @IncludeAllProducts )

		SET @ConfiguratorId = SCOPE_IDENTITY()
	END

	RETURN @ConfiguratorId
END
GO
