
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionDelete]
	@ConditionId INT
AS
BEGIN
	EXEC [addon].[pCartContainsConditionIncludeProductDeleteAll] @ConditionId
	EXEC [addon].[pCartContainsConditionExcludeProductDeleteAll] @ConditionId
	
	EXEC [addon].[pCartContainsConditionIncludeCategoryDeleteAll] @ConditionId
	EXEC [addon].[pCartContainsConditionExcludeCategoryDeleteAll] @ConditionId
	
	EXEC [addon].[pCartContainsConditionIncludeBrandDeleteAll] @ConditionId
	EXEC [addon].[pCartContainsConditionExcludeBrandDeleteAll] @ConditionId
	
	DELETE [addon].[tCartContainsCondition]
	WHERE ConditionId = @ConditionId
END
GO
