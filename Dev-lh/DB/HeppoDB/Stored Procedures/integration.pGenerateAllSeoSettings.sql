
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateAllSeoSettings]

AS
begin
	set nocount on
	begin try			
	
		-- SE Seo General
		exec [integration].[pGenerateSeoSettingsSE]
		
		-- SE Seo	
		--exec [integration].[pGenerateProductSeoShoesSE]  -- legacy
		--exec [integration].[pGenerateProductSeoShoesAccessoriesSE] -- legacy
		
		exec [integration].[pGenerateProductSeoSmycken]
			'[BrandTitle] [ProductTitle] - Smycken på nätet - Heppo.se',			
			'Heppo.se – Smycken på nätet - Köp [BrandTitle] [ProductTitle] online. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp och prisgaranti!',
			1
		
		exec [integration].[pGenerateProductSeoShoes]
			'[BrandTitle] [ProductTitle] skor - Heppo.se - [TagColor] [CategoryLevelTwo] online',
			'Heppo.se din skobutik på internet - Köp [BrandTitle] [ProductTitle] på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp och prisgaranti!',
			1
		
		exec [integration].[pGenerateProductSeoAccessories]
			'[BrandTitle] [ProductTitle] - Heppo.se – [CategoryLevelOne] på nätet',
			'Köp [BrandTitle] [ProductTitle] på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp. Heppo.se – skor och accessoarer på nätet.',
			1
			
		exec [integration].[pGenerateProductSeoShoesAccessories]
			'[BrandTitle] [ProductTitle] [CategoryLevelOne] & skovårdsprodukter på nätet. Skor hos Heppo.se',
			'Köp [BrandTitle] [ProductTitle] [CategoryLevelOne] & skovårdsprodukter online på Heppo.se. Fri frakt & Fri retur. Vår skovårdsskola hjälper dig till rätt skovård. Välkommen!',
			1
			
		exec [integration].[pGenerateBrandSeo]
			'Köp [BrandTitle] skor online – Heppo.se skor på nätet',
			'Köp [BrandTitle] skor på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp och prisgaranti! Heppo.se – din skobutik på nätet.',
			1
			
		exec [integration].[pGenerateSitestructureSeoCategories]	 
			'[Underkategori] [Huvudkategori] & Skor online. Heppo.se – köp skor på nätet',
			'Heppo.se din skobutik på internet - Köp [Underkategori] och [Huvudkategori] på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp när du köper 
			[Underkategori] och [Huvudkategori] från Heppo.',
			1
		
		exec [integration].[pGenerateSitestructureSeoAccessoriesCategories]
			'[Underkategori] & [Huvudkategori] online. Heppo.se – skor & [Underkategori] på nätet.',
			'Köp [Underkategori] och [Huvudkategori] på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp. Heppo.se – skor och accessoarer på nätet.',
			1
			
		exec [integration].[pGenerateSitestructureSeoColors]
			'Skor i alla färger. Massor av skor i färgen [Color]. Heppo.se – köp skor på nätet.',
			'Heppo.se din skobutik på internet - Köp skor i färgen [Color] på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp och prisgaranti!',
			1
			
		exec [integration].[pGenerateSitestructureSeoSmyckenCategories]        
			'[Underkategori] [Huvudkategori] online. Heppo.se – köp smycken på nätet',
			'Heppo.se din smyckesbutik på internet - Köp [Underkategori] och [Huvudkategori] på nätet. Prova hemma - Fri frakt & Fri retur. 14 dagars öppet köp när du köper
			[Underkategori] och [Huvudkategori] från Heppo.',
			1
			
		
		-- NO Seo
		--exec [integration].[pGenerateProductSeoShoesNO] -- legacy
		--exec [integration].[pGenerateProductSeoShoesAccessoriesNO] -- legacy
		--exec [integration].[pGenerateBrandSeoNO] -- legacy
		--exec [integration].[pGenerateSitestructureSeoNO] -- legacy
		
		exec [integration].[pGenerateProductSeoSmycken]
			'[BrandTitle] [ProductTitle] - Smykker online - Heppo.no',
			'Heppo.no – Smykker på nett - Kjøp [BrandTitle] [ProductTitle] online. Prøv hjemme - Fri frakt & Fri retur. 14 dagers åpent kjøp og prisgaranti!',
			4
			
		exec [integration].[pGenerateProductSeoShoes]
			'[BrandTitle] [ProductTitle] sko - Heppo.no - [TagColor] [CategoryLevelTwo] online',
			'Heppo.no Din skobutikk på Internett - Kjøp [BrandTitle] [ProductTitle] på nettet. Prøv hjemme - Fri frakt og fri retur. 14 dagers åpent kjøp og prisgaranti!',
			4
		
		exec [integration].[pGenerateProductSeoAccessories]
			'[BrandTitle] [ProductTitle] - Heppo.no – [CategoryLevelOne] på nettet',
			'Kjøp [BrandTitle] [ProductTitle] på nettet. Prøv hjemme - Fri frakt og fri retur. 14 dagers åpent kjøp. Heppo.no – sko og tilbehør på nettet.',
			4
			
		exec [integration].[pGenerateProductSeoShoesAccessories]
			'[BrandTitle] [ProductTitle] [CategoryLevelOne] og skopleieprodukter på nettet. Sko hos Heppo.no',
			'Kjøp [BrandTitle] [ProductTitle] [CategoryLevelOne] og skopleieprodukter online på Heppo.no. Fri frakt og fri retur. Vår veiledning om skopleie hjelper deg med å stelle skoene på riktig måte. Velkommen!',
			4
			
		exec [integration].[pGenerateBrandSeo]
			'Kjøp [BrandTitle] sko online – Heppo.no sko på nettet',
			'Kjøp [BrandTitle] sko på nettet. Prøv hjemme - Fri frakt og fri retur. 14 dagers åpent kjøp og prisgaranti! Heppo.no – din skobutikk på nettet.',
			4
			
		exec [integration].[pGenerateSitestructureSeoCategories]	 
			'[Underkategori] [Huvudkategori] og Sko online. Heppo.no – Kjøp sko på nettet.',
			'Heppo.no Din skobutikk på Internett - Kjøp [Underkategori] og [Huvudkategori] på nettet. Prøv hjemme - Fri frakt og fri retur. 14 dagers åpent kjøp når du kjøper
			[Underkategori] og [Huvudkategori] fra Heppo.',
			4
		
		exec [integration].[pGenerateSitestructureSeoAccessoriesCategories]
			'[Underkategori] og [Huvudkategori] online. Heppo.no – sko & [Underkategori] på nettet.',
			'Kjøp [Underkategori] og [Huvudkategori] på nettet. Prøv hjemme - Fri frakt og fri retur. 14 dagers åpent kjøp. Heppo.no – sko og tilbehør på nettet.',
			4
			
		exec [integration].[pGenerateSitestructureSeoColors]
			'Sko i alle farger. Massevis av sko i fargen [Color]. Heppo.no – Kjøp sko på nettet.',
			'Heppo.no Din skobutikk på Internett - Kjøp sko i fargen [Color] på nettet. Prøv hjemme - Fri frakt og fri retur. 14 dagers åpent kjøp og prisgaranti!',
			4
			
		exec [integration].[pGenerateSitestructureSeoSmyckenCategories]
			'[Underkategori] [Huvudkategori] online. Heppo.no – Kjop smykker online',
			'Heppo.no – Smykker pa nett - Kjop [Underkategori] & [Huvudkategori] online. Prov hjemme - Fri frakt & Fri retur. 14 dagers apent kjop og prisgaranti!',
			4
			
		
		-- DK Seo		
		--exec [integration].[pGenerateProductSeoShoesDK] -- legacy
		--exec [integration].[pGenerateProductSeoShoesAccessoriesDK] -- legacy
		--exec [integration].[pGenerateBrandSeoDK] -- legacy
		--exec [integration].[pGenerateSitestructureSeoDK] -- legacy
		
		exec [integration].[pGenerateProductSeoSmycken]
			'[BrandTitle] [ProductTitle] - Smykker online - Heppo.dk',
			'Heppo.dk – Smykker på nett - Køb [BrandTitle] [ProductTitle] online. Prøv hjemme med gratis fragt og fri retur. 14 dages åbent køb og prisgaranti!',
			657
			
		exec [integration].[pGenerateProductSeoShoes]
			'[BrandTitle] [ProductTitle] sko - Heppo.dk - [TagColor] [CategoryLevelTwo] online',
			'Heppo.dk din skobutik på nettet – Køb [BrandTitle] [ProductTitle] på nettet. Prøv hjemme – Gratis fragt og gratis varereturnering. 14 dages returret og prisgaranti!',
			657
		
		exec [integration].[pGenerateProductSeoAccessories]
			'[BrandTitle] [ProductTitle] - Heppo.dk – [CategoryLevelOne] på nettet',
			'Køb [BrandTitle] [ProductTitle] på nettet. Prøv hjemme – Gratis fragt og gratis varereturnering. 14 dages returret. Heppo.se – sko og tilbehør på nettet.',
			657
			
		exec [integration].[pGenerateProductSeoShoesAccessories]
			'[BrandTitle] [ProductTitle] [CategoryLevelOne] og skoplejeprodukter på nettet. Sko hos Heppo.dk',
			'Køb [BrandTitle] [ProductTitle] [CategoryLevelOne]-skocreme og -skoplejeprodukter på nettet. Sko hos Heppo.dk',
			657
			
		exec [integration].[pGenerateBrandSeo]
			'Køb [BrandTitle] sko online – Heppo.dk sko på nettet',
			'Køb [BrandTitle] sko på nettet. Prøv hjemme – Gratis fragt og gratis varereturnering. 14 dages returret og prisgaranti! Heppo.se – din skobutik på nettet.',
			657
			
		exec [integration].[pGenerateSitestructureSeoCategories]	 
			'[Underkategori] [Huvudkategori] og Sko online. Heppo.dk – køb sko på nettet.',
			'Heppo.dk din skobutik på nettet – Køb [Underkategori] og [Huvudkategori] på nettet. Prøv hjemme – Gratis fragt og gratis varereturnering 14 dages returret, når du køber 
			[Underkategori] og [Huvudkategori] fra Heppo.',
			657
		
		exec [integration].[pGenerateSitestructureSeoAccessoriesCategories]
			'[Underkategori] & [Huvudkategori] online. Heppo.dk – sko og [Underkategori] på nettet.',
			'Køb [Underkategori] og [Huvudkategori] på nettet. Prøv hjemme – Gratis fragt og gratis varereturnering. 14 dages returret. Heppo.se – sko og tilbehør på nettet.',
			657
			
		exec [integration].[pGenerateSitestructureSeoColors]
			'Sko i alle farver. Masser af sko i farven [Color]. Heppo.dk – køb sko på nettet.',
			'Heppo.dk din skobutik på nettet – Køb sko i farven [Color] på nettet. Prøv hjemme – Gratis fragt og gratis varereturnering. 14 dages returret og prisgaranti!',
			657
			
		exec [integration].[pGenerateSitestructureSeoSmyckenCategories]
			'[Underkategori] [Huvudkategori] online. Heppo.dk – Kob smykker online',
			'Heppo.dk - Smykker pa nett - Kob [Underkategori] & [Huvudkategori] online. Prov hjemme med gratis fragt og fri retur. 14 dages abent kob og prisgaranti!',
			657
			
		
		-- FI Seo			
		--exec [integration].[pGenerateProductSeoShoesFI] -- legacy
		--exec [integration].[pGenerateProductSeoShoesAccessoriesFI] -- legacy
		--exec [integration].[pGenerateBrandSeoFI] -- legacy
		--exec [integration].[pGenerateSitestructureSeoFI] -- legacy
		
		exec [integration].[pGenerateProductSeoSmycken]
			'[BrandTitle] [ProductTitle] - Koruja online - Heppo.fi',
			'Heppo.fi – Koruja online - Osta [BrandTitle] & [ProductTitle] netissä. Sovita kotona, ilmainen toimitus ja ilmainen palautus. 14 päivän palautusoikeus ja hintatakuu!',
			1000003
						
		exec [integration].[pGenerateProductSeoShoes]
			'[BrandTitle] [ProductTitle] kengät - Heppo.fi - [TagColor] [CategoryLevelTwo] netistä',
			'Heppo.fi kenkäkauppasi netissä – Osta [BrandTitle] [ProductTitle] netistä. Kokeile kotona – Ilmainen toimitus & Ilmainen palautus. 14 päivän avoin kauppa ja hintatakuu!',
			1000003
		
		exec [integration].[pGenerateProductSeoAccessories]
			'[BrandTitle] [ProductTitle] - Heppo.fi – [CategoryLevelOne] netistä',
			'Osta [BrandTitle] [ProductTitle] netistä. Kokeile kotona – Ilmainen toimitus & Ilmainen palautus. 14 päivän avoin kauppa. Heppo.fi – kenkiä ja asusteita netistä.',
			1000003
			
		exec [integration].[pGenerateProductSeoShoesAccessories]
			'[BrandTitle] [ProductTitle] [CategoryLevelOne] & kenkienhoitotuotteet netistä. Kenkiä osoitteessa Heppo.fi',
			'Osta [BrandTitle] [ProductTitle] [CategoryLevelOne] & kenkienhoitotuotteet netistä osoitteesta Heppo.fi. Ilmainen toimitus & ilmainen palautus. Kenkienhoito-oppaamme auttaa sinua hoitamaan kenkiäsi oikein. Tervetuloa!',
			1000003
			
		exec [integration].[pGenerateBrandSeo]
			'Osta [BrandTitle] kenkiä netistä – Heppo.fi kenkiä netistä.',
			'Osta [BrandTitle] kenkiä netistä. Kokeile kotona – Ilmainen toimitus & Ilmainen palautus. 14 päivän avoin kauppa ja hintatakuu! Heppo.fi – kenkäkauppasi netissä.',
			1000003
			
		exec [integration].[pGenerateSitestructureSeoCategories]	 
			'[Underkategori] [Huvudkategori] & Kenkiä netistä. Heppo.fi – osta kenkiä netistä.',
			'Heppo.fi kenkäkauppasi internetissä – Osta [Underkategori] ja [Huvudkategori] netistä. Kokeile kotona – Ilmainen toimitus & Ilmainen palautus. 14 päivän avoin kauppa, kun ostat 
			[Underkategori] ja [Huvudkategori] Heppolta.',
			1000003
		
		exec [integration].[pGenerateSitestructureSeoAccessoriesCategories]
			'[Underkategori] & [Huvudkategori] netistä. Heppo.fi – kenkiä & [Underkategori] netistä.',
			'Osta [Underkategori] ja [Huvudkategori] netistä. Kokeile kotona – Ilmainen toimitus & Ilmainen palautus. 14 päivän avoin kauppa. Heppo.fi – kenkiä & asusteita netistä.',
			1000003
			
		exec [integration].[pGenerateSitestructureSeoColors]
			'Kenkiä kaikissa väreissä. Paljon kenkiä värissä [Color]. Heppo.fi – osta kenkiä netistä.',
			'Heppo.fi kenkäkauppasi netissä – Osta [Color] netistä. Kokeile kotona – Ilmainen toimitus & Ilmainen palautus. 14 päivän avoin kauppa ja hintatakuu!',
			1000003
			
		exec [integration].[pGenerateSitestructureSeoSmyckenCategories]
			'[Underkategori] [Huvudkategori] online. Heppo.fi – Koruja online',
			'Heppo.fi – koruja online - Osta [Underkategori] & [Huvudkategori] pa natet. Sovita kotona, ilmainen toimitus ja ilmainen palautus. 14 paivan palautusoikeus ja hintatakuu!',
			1000003
			
		
		-- FR Seo
		exec [integration].[pGenerateProductSeoShoes]
			'[BrandTitle] [ProductTitle] chaussures - Heppo.fr - [TagColor] [CategoryLevelTwo] en ligne',
			'Heppo.fr votre boutique de chaussures sur internet - Achetez [BrandTitle] [ProductTitle] sur le net. Essayez chez vous - Livraison & Retour gratuits. 14 jours pour échanger ou vous faire rembourser et prix garantis!',
			1000006
		
		exec [integration].[pGenerateProductSeoAccessories]
			'[BrandTitle] [ProductTitle] - Heppo.fr – [CategoryLevelOne] sur le net',
			'Achetez [BrandTitle] [ProductTitle] sur le net. Essayez chez vous - Livraison & retour gratuits. 14 jours pour échanger ou vous faire rembourser. Heppo.fr – chaussures et accessoires sur le net.',
			1000006
			
		exec [integration].[pGenerateProductSeoShoesAccessories]
			'[BrandTitle] [ProductTitle] [CategoryLevelOne] & soins pour chaussures sur le net. Chaussures chez Heppo.fr',
			'Achetez [BrandTitle] [ProductTitle] [CategoryLevelOne] & soins pour chaussures en ligne sur Heppo.fr. Livraison & retour gratuits. Notre école de soin des chaussures vous aide à apporter le bon soin. Bienvenue!',
			1000006
			
		exec [integration].[pGenerateBrandSeo]
			'Achetez [BrandTitle] chaussures en ligne – Heppo.fr chaussures sur le net',
			'Achetez [BrandTitle] chaussures  sur le net. Essayez chez vous - Livraison & retour gratuits. 14 jours pour échanger ou vous faire rembourser et prix garantis !  Heppo.fr – votre boutique de chaussures sur le net.',
			1000006
			
		exec [integration].[pGenerateSitestructureSeoCategories]	 
			'[Underkategori] [Huvudkategori] & Chaussures en ligne Heppo.fr – achetez vos chaussures sur le net.',
			'Heppo.fr votre boutique de chaussures sur internet - Achetez [Underkategori] et [Huvudkategori] sur le net. Essayez chez vous – Livraison & retour gratuits. 14 jours pour échanger ou vous faire rembourser quand vous achetez 
			[Underkategori] et [Huvudkategori] sur Heppo.',
			1000006
		
		exec [integration].[pGenerateSitestructureSeoAccessoriesCategories]
			'[Underkategori] & [Huvudkategori] en ligne. Heppo.fr – chaussures & [Underkategori] sur le net.',
			'Achetez [Underkategori] et [Huvudkategori] sur le net. Essayez chez vous - Livraison & retour gratuits. 14 jours pour échanger ou vous faire rembourser. Heppo.fr – chaussures et accessoires sur le net.',
			1000006
			
		exec [integration].[pGenerateSitestructureSeoColors]
			'Chaussures de toutes les couleurs. Plein de chaussures  de couleur [Color]. Heppo.fr – achetez vos chaussures sur le net.',
			'Heppo.fr votre boutique de chaussures sur internet - Achetez des chaussures de couleur [Color] sur le net. Essayez chez vous - Livraison & Retour gratuits. 14 jours pour échanger ou vous faire rembourser et prix garantis!',
			1000006
			
			
		-- NL Seo
		
		exec [integration].[pGenerateProductSeoSmycken]
			'[BrandTitle] [ProductTitle] - Sieraden online- Heppo.nl',
			'Heppo.nl – Sieraden online – Koop [BrandTitle] & [ProductTitle] online. Probeer thuis uit – Geen verzend- en retourkosten. 14 dagen bedenktijd en prijsgarantie!',
			1000007
			
		exec [integration].[pGenerateProductSeoShoes]
			'[BrandTitle] [ProductTitle] schoenen - Heppo.nl - [TagColor] [CategoryLevelTwo] online',
			'Heppo.nl jouw schoenenwinkel op internet - Koop [BrandTitle] [ProductTitle] op internet. Thuis uitproberen - Geen verzend- en retourkosten. 14 dagen op zicht en prijsgarantie!',
			1000007
		
		exec [integration].[pGenerateProductSeoAccessories]
			'[BrandTitle] [ProductTitle] - Heppo.nl – [CategoryLevelOne] op internet',
			'Koop [BrandTitle] [ProductTitle] op internet. Thuis uitproberen - Geen verzend- en retourkosten. 14 dagen op zicht. Heppo.nl – schoenen en accessoires op internet.',
			1000007
			
		exec [integration].[pGenerateProductSeoShoesAccessories]
			'[BrandTitle] [ProductTitle] [CategoryLevelOne] & schoenverzorgingsproducten op internet. Schoenen bij Heppo.nl',
			'Koop [BrandTitle] [ProductTitle] [CategoryLevelOne] & schoenverzorgingsproducten online bij Heppo.nl. Geen verzend- en retourkosten. Met onze schoenverzorgingstips kun je je schoenen optimaal onderhouden. Welkom!',
			1000007
		
		exec [integration].[pGenerateBrandSeo]
			'Koop [BrandTitle] schoenen online – Heppo.nl schoenen op internet',
			'Koop [BrandTitle] schoenen op internet. Thuis uitproberen - Geen verzend- en retourkosten. 14 dagen op zicht en prijsgarantie! Heppo.nl - jouw schoenenwinkel op internet.',
			1000007
		
		exec [integration].[pGenerateSitestructureSeoCategories]	 
			'[Underkategori] [Huvudkategori] & Schoenen online. Heppo.nl – schoenen kopen op internet.',
			'Heppo.nl jouw schoenenwinkel op internet - Koop [Underkategori] en [Huvudkategori] op internet. Thuis uitproberen - Geen verzend- en retourkosten. 14 dagen op zicht als je 
			[Underkategori] en [Huvudkategori] koopt bij Heppo.',
			1000007
		
		exec [integration].[pGenerateSitestructureSeoAccessoriesCategories]
			'[Underkategori] & [Huvudkategori] online. Heppo.nl – schoenen & [Underkategori] op internet.',
			'Koop [Underkategori] en [Huvudkategori] op internet. Thuis uitproberen - Geen verzend- en retourkosten. 14 dagen op zicht. Heppo.nl – schoenen en accessoires op internet.',
			1000007
			
		exec [integration].[pGenerateSitestructureSeoColors]
			'Schoenen in alle kleuren. Heel veel schoenen in de kleur [Color]. Heppo.nl – schoenen kopen op internet.',
			'Heppo.nl jouw schoenenwinkel op internet - Koop schoenen in de kleur [Color] op internet. Thuis uitproberen - Geen verzend- en retourkosten. 14 dagen op zicht en prijsgarantie!',
			1000007
			
		exec [integration].[pGenerateSitestructureSeoSmyckenCategories]
			'[Underkategori] [Huvudkategori] online. Heppo.nl – Sieraden online',
			'Heppo.nl - Sieraden online - Koop [Underkategori] & [Huvudkategori] online. Probeer thuis uit – Geen verzend- en retourkosten. 14 dagen bedenktijd en prijsgarantie!',
			1000007
			
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
