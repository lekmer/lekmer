SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedPriceAction]
	WHERE
		[FixedPriceAction.ProductActionId] = @ProductActionId
END

GO
