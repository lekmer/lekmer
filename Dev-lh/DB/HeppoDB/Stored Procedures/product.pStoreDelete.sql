SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreDelete]
	@StoreId	int
as
BEGIN
	DELETE FROM 
		product.tStoreProduct
	WHERE 
		[StoreId] = @StoreId
		
	DELETE FROM 
		product.tStore
	WHERE 
		[StoreId] = @StoreId
END

GO
