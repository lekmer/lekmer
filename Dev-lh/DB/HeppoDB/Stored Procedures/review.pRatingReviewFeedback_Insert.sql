SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_Insert]
	@ChannelId INT,
	@ProductId INT,
	@OrderId INT,
	@RatingReviewStatusId INT,
	@LikeHit INT,
	@Impropriate BIT,
	@IPAddress VARCHAR(50),
	@CreatedDate DATETIME,
	@RatingReviewUserId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingReviewFeedback] (
		[ChannelId],
		[ProductId],
		[OrderId],
		[RatingReviewStatusId],
		[LikeHit],
		[Impropriate],
		[IPAddress],
		[CreatedDate],
		[RatingReviewUserId]
	)
	VALUES (
		@ChannelId,
		@ProductId,
		@OrderId,
		@RatingReviewStatusId,
		@LikeHit,
		@Impropriate,
		@IPAddress,
		@CreatedDate,
		@RatingReviewUserId
	)

	RETURN SCOPE_IDENTITY()
END
GO
