SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateBrandHeppo]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	
	begin try
	begin transaction
	print '1 inser into temp table'
	
	if object_id('tempdb..#UniqueErpId') is not null
		drop table #UniqueErpId
		
	create table #UniqueErpId(BrandId nvarchar(200) COLLATE Finnish_Swedish_CI_AS not null, BrandTitle nvarchar(2500) COLLATE Finnish_Swedish_CI_AS not null);
	
	with UniqueErpId
	as
	(
		select
			BrandId,
			BrandTitle,
			row_number() over(partition by BrandId
							  order by substring(HYarticleId, 3, 1)) RowNo
		from integration.tempProduct
		where BrandId is not null and BrandTitle is not null
		group by BrandId, BrandTitle, substring(HYarticleId, 3, 1)
	)
	insert into #UniqueErpId
	select
		BrandId,
		BrandTitle
	from UniqueErpId
	where RowNo = 1 
	
	--select * from #UniqueErpId
	
	print '2 update t.brand'
	UPDATE 
		b
	SET  
		Title = t.BrandTitle	
	FROM 
		#UniqueErpId t
		inner join [lekmer].tBrand b
				on b.ErpId = t.BrandId

	WHERE 		
		Title <> t.BrandTitle
	
	
	print '3 inser into lekmer.tbrand'	
	INSERT INTO 
		lekmer.tBrand (ErpId, Title)
	select
		u.BrandId,
		u.BrandTitle
	from
		#UniqueErpId u --inner join lekmer.tBrand b
			--on u.BrandId = b.ErpId
	Where
		u.BrandId not in (select b.Erpid
							from lekmer.tBrand b
							where b.ErpId is not null)
	
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
        
END


GO
