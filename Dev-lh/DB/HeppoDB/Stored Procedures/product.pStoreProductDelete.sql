SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreProductDelete]
	@StoreId		int,
	@ProductId		int
AS
BEGIN
	DELETE FROM 
		product.tStoreProduct
	WHERE 
		ProductId = @ProductId 
		AND StoreId = @StoreId
END 
GO
