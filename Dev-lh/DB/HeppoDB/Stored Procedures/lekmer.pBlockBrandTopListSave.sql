SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@TotalBrandCount INT,
	@IncludeAllCategories BIT,
	@OrderStatisticsDayCount INT,
	@LinkContentNodeId INT
AS
BEGIN
	UPDATE lekmer.tBlockBrandTopList
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount,
		[TotalBrandCount] = @TotalBrandCount,
		[IncludeAllCategories] = @IncludeAllCategories,
		[OrderStatisticsDayCount] = @OrderStatisticsDayCount,
		[LinkContentNodeId] = @LinkContentNodeId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0 RETURN
		
	INSERT lekmer.tBlockBrandTopList (
		[BlockId],
		[ColumnCount],
		[RowCount],
		[TotalBrandCount],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalBrandCount,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId
	)
END
GO
