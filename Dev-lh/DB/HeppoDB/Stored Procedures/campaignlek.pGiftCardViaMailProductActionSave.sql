
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT,
	@SendingInterval	INT,
	@ConfigId			INT,
	@TemplateId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tGiftCardViaMailProductAction]
	SET
		SendingInterval = @SendingInterval,
		ConfigId = @ConfigId,
		TemplateId = @TemplateId
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tGiftCardViaMailProductAction] (
			ProductActionId,
			SendingInterval,
			ConfigId,
			TemplateId
		)
		VALUES (
			@ProductActionId,
			@SendingInterval,
			@ConfigId,
			@TemplateId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
