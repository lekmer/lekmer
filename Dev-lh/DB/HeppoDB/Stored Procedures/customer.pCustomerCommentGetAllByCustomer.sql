SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 16.04.2010		Time: 18:40
Description: Created 
*/
CREATE PROCEDURE [customer].[pCustomerCommentGetAllByCustomer]
	@CustomerId	int
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerComment]
	WHERE
		[CustomerComment.CustomerId] = @CustomerId
		order by [CustomerComment.CreationDate] ASC
END


GO
