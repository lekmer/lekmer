SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandSave]
	@BlockId INT,
	@BrandId INT
AS
BEGIN
	INSERT [review].[tBlockBestRatedProductListBrand] (
		[BlockId],
		[BrandId]
	)
	VALUES (
		@BlockId,
		@BrandId
	)
END
GO
