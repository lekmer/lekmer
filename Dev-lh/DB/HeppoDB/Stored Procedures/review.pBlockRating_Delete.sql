SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRating_Delete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockRating]
	WHERE BlockId = @BlockId
END
GO
