SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pReviewGetByFeedback]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vReview] r
	WHERE
		r.[Review.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
