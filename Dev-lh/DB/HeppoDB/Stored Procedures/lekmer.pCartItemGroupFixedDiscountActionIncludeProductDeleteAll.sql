SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]
	WHERE
		CartActionId = @CartActionId
END


GO
