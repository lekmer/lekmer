SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByTagGroupSecure]
	@TagGroupId int
AS
BEGIN 
	SET nocount ON

	SELECT 
		*
	FROM 
		lekmer.[vTagSecure]
	WHERE 
		[Tag.TagGroupId] = @TagGroupId
END 

GO
