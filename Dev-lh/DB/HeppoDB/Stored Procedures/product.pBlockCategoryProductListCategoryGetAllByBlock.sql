SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockCategoryProductListCategoryGetAllByBlock]
	@LanguageId int,
	@BlockId int
as
begin
	
	SELECT 
		bc.*
	from 
		[product].[vCustomBlockCategoryProductListCategory] as bc
		inner join [product].[vCustomCategory] as cv on bc.[BlockCategory.CategoryId] = cv.[Category.Id]
	where
		bc.[BlockCategory.BlockId] = @BlockId and
		cv.[Category.LanguageId] = @LanguageId
end

GO
