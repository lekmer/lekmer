SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGetNewOrdersByList]
	@MaxAllowedFailedAttempts	int,
	@SleepIntervallInMinutes	int
AS
begin
	set nocount on

		select distinct	
			o.OrderId --,
			--q.*
		from
			[order].torder o
			left join [integration].[tOrderQue] q
				on o.OrderId = q.OrderId
		where
			o.OrderStatusId = 2
			and q.OrderId is null			
			or 
				(
					q.OrderId is not null
					and q.NumberOfFailedAttempts < @MaxAllowedFailedAttempts
					and DATEADD(minute, @SleepIntervallInMinutes, q.LastFailedAttempt) < GETDATE()
				)
			
end
GO
