SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionCreateGetAddress]
	@KlarnaShopId INT,
	@KlarnaMode INT,
	@KlarnaTransactionTypeId INT,
	@CreatedDate DATETIME,
	@CivicNumber VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaTransaction]
			( [KlarnaShopId],
			  [KlarnaMode],
			  [KlarnaTransactionTypeId],
			  [Created],
			  [CivicNumber]
			)
	VALUES
			( @KlarnaShopId,
			  @KlarnaMode,
			  @KlarnaTransactionTypeId,
			  @CreatedDate,
			  @CivicNumber
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
