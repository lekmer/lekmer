SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandDelete]
	@BrandId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductRegistryRestrictionBrand]
	WHERE 
		[BrandId] = @BrandId
END
GO
