
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pUpdateProductPopularityAllChannels]
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @DateTo AS DATETIME;
	DECLARE @DateFrom AS DATETIME;
	DECLARE @ChannelId AS INT;
	DECLARE @ChannelTitle AS VARCHAR(80);

	SET @DateTo = GETDATE();
	SET @DateFrom = DATEADD(d, -30, @DateTo);

	DECLARE @cursor CURSOR
	SET @cursor = CURSOR FOR SELECT [ChannelId], [Title] FROM [core].[tChannel];
  
	OPEN @cursor

	FETCH NEXT FROM @cursor 
	INTO @ChannelId, @ChannelTitle;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [lekmer].[pProductUpdatePopularity]
			@ChannelId = @ChannelId,
			@DateTimeFrom = @DateFrom,
			@DateTimeTo = @DateTo;

		FETCH NEXT FROM @cursor 
		INTO @ChannelId, @ChannelTitle;
	END
END
GO
