SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [product].[pProductImageSave]
	@ProductId int,
	@MediaId int,
	@ProductImageGroupId int,
	@Ordinal int
AS
BEGIN
	INSERT [product].[tProductImage]
	(
		ProductId,
		MediaId,
		ProductImageGroupId,
		Ordinal
	)
	VALUES
	(
		@ProductId,
		@MediaId,
		@ProductImageGroupId,
		@Ordinal
	)
END
GO
