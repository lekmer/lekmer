
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_BrandRestrictionsFok]
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRANSACTION

		--------------------------------------------------------------------------------
		---- Delete items from tProductRegistryProduct.
		--------------------------------------------------------------------------------
		-- By Product restroction
		DELETE prp
		FROM [product].[tProductRegistryProduct] prp
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																		 AND prrp.ProductId = prp.ProductId
		-- By Brand restroction
		DELETE prp 
		FROM [product].[tProductRegistryProduct] prp
			 INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = prp.[ProductId]
			 INNER JOIN [lekmer].[tProductRegistryRestrictionBrand] prrb ON prrb.[BrandId] = lp.[BrandId] 
																			AND prrb.[ProductRegistryId] = prp.[ProductRegistryId]
		
		
		----- 0-1 age intervall
		UPDATE
			[lekmer].[tLekmerProduct]
		SET
			AgeFromMonth = 0,
			AgeToMonth = 1
		WHERE
			AgeToMonth = 0
		
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			NULL,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)
	END CATCH		 
END
GO
