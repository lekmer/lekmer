SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [security].[pRoleAddPrivilege]
	@RoleId int,
	@PrivilegeId int
as
begin
	set nocount on

	insert
		[security].[tRolePrivilege]
	(
		[RoleId],
		[PrivilegeId]
	)				
	SELECT
		@RoleId,
		@PrivilegeId
end
GO
