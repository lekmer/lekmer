SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionGetAllByModelFragmentId]
	@ModelFragmentId	int
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomModelFragmentFunction]
	where
		[ModelFragmentFunction.ModelFragmentId] = @ModelFragmentId
	ORDER BY [ModelFragmentFunction.CommonName]
end

GO
