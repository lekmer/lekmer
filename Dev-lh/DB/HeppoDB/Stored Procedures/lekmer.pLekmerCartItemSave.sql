
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerCartItemSave]
	@CartItemId 			INT,
	@ProductId				INT,
	@SizeId					INT,
	@ErpId					VARCHAR(50),
	@IsAffectedByCampaign	BIT
AS
BEGIN
      SET NOCOUNT ON

      UPDATE
            [lekmer].[tLekmerCartItem]
      SET
            SizeId = @SizeId,
            ErpId = @ErpId,
            IsAffectedByCampaign = IsAffectedByCampaign
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [lekmer].[tLekmerCartItem]
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  ErpId,
                  IsAffectedByCampaign
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @ErpId,
                  @IsAffectedByCampaign
            )
      END
END
GO
