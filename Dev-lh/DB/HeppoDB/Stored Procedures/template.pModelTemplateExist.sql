SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelTemplateExist] 
	@ModelId INT
AS 
BEGIN
    SET NOCOUNT ON	

    IF EXISTS ( SELECT  *
                FROM    [template].[tTemplate]
                WHERE   [tTemplate].[ModelId] = @ModelId ) 
        RETURN 1
    RETURN 0    
END
GO
