SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListSave]
	@BlockId INT,
	@NumberOfItems INT
AS
BEGIN
	UPDATE [review].[tBlockProductLatestFeedbackList]
	SET
		[NumberOfItems] = @NumberOfItems
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockProductLatestFeedbackList] (
		[BlockId],
		[NumberOfItems]
	)
	VALUES (
		@BlockId,
		@NumberOfItems
	)
END
GO
