SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionCurrencyDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionCurrency]
	WHERE
		ProductActionId = @ProductActionId
END

GO
