SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBlockImageRotatorGetByIdSecure]
	@BlockId int
as	
begin
	set nocount on
	select
		BIR.*,
		b.*
	from
		[lekmer].[tBlockImageRotator] as BIR
		inner join [sitestructure].[vCustomBlockSecure] as b on BIR.[BlockId] = b.[Block.BlockId]
	where
		BIR.[BlockId] = @BlockId
end
GO
