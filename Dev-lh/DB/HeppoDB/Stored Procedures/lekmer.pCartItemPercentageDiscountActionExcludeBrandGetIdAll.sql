SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
	WHERE 
		CartActionId = @CartActionId
END


GO
