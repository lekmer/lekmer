SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pStoreGetIdAllByChannel]
	@ChannelId		int,
    @Page			int,
    @PageSize		int
as 
begin

	set nocount on

	declare @From as int, @To as int
	set @From = ((@Page - 1) * @PageSize) + 1
	set @To = @Page * @PageSize

	declare @sql as nvarchar(max)
	declare @sqlCount as nvarchar(max)
	declare @sqlFragment as nvarchar(max)

	set @sqlFragment = '
		select
			[Store.Id],
			[Store.Title],
			row_number() over (order by s.[Store.Title]) as Number
		from
			product.vCustomStore s
		where
			s.[Store.StatusId] = 0
	'
	
	set @sqlCount = '
		select count(*)
		from
			(' + @sqlFragment + ') as CountResults'

	set @sql = '
		select [Store.Id]
		from
			(' + @sqlFragment + ') as SearchResult
		where
			Number between @From and @To
		order by
			Number'

	exec sp_executesql @sqlCount
		     
	exec sp_executesql @sql, 
		N'	@From	int,
			@To		int',
			@From,
			@To

end



GO
