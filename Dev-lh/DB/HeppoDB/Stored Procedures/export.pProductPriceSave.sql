SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pProductPriceSave]
	@ChannelId	INT,
	@ProductId	INT,
	@PriceIncludingVat DECIMAL (16, 2),
	@PriceExcludingVat DECIMAL (16, 2),
	@DiscountPriceIncludingVat DECIMAL (16, 2),
	@DiscountPriceExcludingVat DECIMAL (16, 2)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[export].[tProductPrice]
	SET
		[PriceIncludingVat] = @PriceIncludingVat,
		[PriceExcludingVat] = @PriceExcludingVat,
		[DiscountPriceIncludingVat] = @DiscountPriceIncludingVat,
		[DiscountPriceExcludingVat] = @DiscountPriceExcludingVat
	WHERE
		[ChannelId] = @ChannelId
		AND
		[ProductId] = @ProductId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [export].[tProductPrice]
		(
			[ChannelId],
			[ProductId],
			[PriceIncludingVat],
			[PriceExcludingVat],
			[DiscountPriceIncludingVat],
			[DiscountPriceExcludingVat]
		)
		VALUES
		(
			@ChannelId,
			@ProductId,
			@PriceIncludingVat,
			@PriceExcludingVat,
			@DiscountPriceIncludingVat,
			@DiscountPriceExcludingVat
		)
	END
END
GO
