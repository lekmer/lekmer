
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pCreateProductUrls]
	@ProductUrlXml XML,
	@HistoryLifeIntervalTypeId INT = 3

	-- Returns.....: 0 on Valid, 1 on failure
AS
BEGIN

	/*
	<products>
		<product id="103024" title="100124-0014" languageid="1"/>
		<product id="103025" title="100124-0014" languageid="1"/>
		<product id="103026" title="100124-0014" languageid="1"/>
	</products>
	*/

	SET NOCOUNT ON

	IF OBJECT_ID('tempdb..#tProductUrls') IS NOT NULL
		DROP TABLE #tProductUrls
		
	CREATE TABLE #tProductUrls
	(
		ProductId INT NOT NULL,
		Title NVARCHAR (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
		LanguageId INT NOT NULL
		CONSTRAINT PK_#tProductUrls PRIMARY KEY(ProductId, LanguageId)
	)

	BEGIN TRY
		BEGIN TRANSACTION

			-- Populate temp table
			INSERT #tProductUrls (
				ProductId,
				Title,
				LanguageId
			)
			SELECT
				T.c.value('@id[1]', 'int'),
				T.c.value('@title[1]', 'nvarchar(250)'),
				T.c.value('@languageid[1]', 'int')
				--T.c.value('@id[1]', 'int'),
				--T.c.value('@height[1]', 'int'),
				--T.c.value('@width[1]', 'int'),
				--T.c.value('@extension[1]', 'nvarchar(10)')
			FROM
				@ProductUrlXml.nodes('/products/product') T(c)

			--SELECT * FROM #tProductUrls

			--Insert into tProductUlrHistory
			INSERT INTO [lekmer].[tProductUrlHistory] (
				ProductId,
				LanguageId,
				UrlTitleOld,
				HistoryLifeIntervalTypeId
			)
			SELECT
				pu.ProductId,
				pu.LanguageId,
				pu.UrlTitle,
				@HistoryLifeIntervalTypeId
			FROM
				[lekmer].[tProductUrl] pu
				INNER JOIN #tProductUrls ipu ON ipu.ProductId = pu.ProductId AND ipu.LanguageId = pu.LanguageId
			WHERE
				pu.UrlTitle != ipu.Title

			--Remove from tProductUlrHistory
			DELETE
				puh
			FROM
				[lekmer].[tProductUrlHistory] puh				
				INNER JOIN (
					SELECT
						ipu.*
					FROM
						[lekmer].[tProductUrl] pu
						INNER JOIN #tProductUrls ipu ON ipu.ProductId = pu.ProductId AND ipu.LanguageId = pu.LanguageId
					WHERE
						pu.UrlTitle != ipu.Title
				) AS puhNew ON puhNew.ProductId = puh.ProductId AND puhNew.LanguageId = puh.LanguageId AND puhNew.Title = puh.UrlTitleOld
			
			-- Delete from tProductUrl
			DELETE
				p
			FROM
				lekmer.tProductUrl p
				INNER JOIN #tProductUrls purl ON p.ProductId = purl.ProductId AND p.LanguageId = purl.LanguageId

			-- Insert into tProductUrl
			INSERT lekmer.tProductUrl(
				ProductId,
				LanguageId,
				UrlTitle
			)
			SELECT
				ProductId,
				LanguageId,
				Title
			FROM
				#tProductUrls

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH

		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

		--RETURN @Return
	END CATCH		
END
GO
