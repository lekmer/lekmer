SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pBlockImageRotatorSave]
	@BlockId int,
	@SecondaryTemplateId int = null
as	
begin
	
	update
		lekmer.[tBlockImageRotator]
	set
		SecondaryTemplateId	= @SecondaryTemplateId
		
	where
		[BlockId] = @BlockId
	
	if @@ROWCOUNT = 0
	begin
		insert
			lekmer.[tBlockImageRotator ]
		(
			[BlockId],
			SecondaryTemplateId
		)
		values
		(
			@BlockId,
			@SecondaryTemplateId
		)
	end
end
GO
