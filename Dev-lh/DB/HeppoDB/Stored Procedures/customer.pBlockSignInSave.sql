SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [customer].[pBlockSignInSave]
	@BlockId				int,
	@RedirectContentNodeId	int
as	
begin
	
	update
		[customer].[tBlockSignIn]
	set
		[RedirectContentNodeId]	= @RedirectContentNodeId
	where
		[BlockId] = @BlockId
	
	if @@ROWCOUNT = 0
	begin
		insert
			[customer].[tBlockSignIn]
		(
			[BlockId],
			[RedirectContentNodeId]
		)
		values
		(
			@BlockId,
			@RedirectContentNodeId
		)
	end
end
GO
