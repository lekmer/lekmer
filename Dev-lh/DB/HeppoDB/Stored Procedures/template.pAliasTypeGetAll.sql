SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 22.12.2008		Time: 10:40
Description:
			Created 
*/

CREATE PROCEDURE [template].[pAliasTypeGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomAliasType]
	ORDER BY [AliasType.Ordinal]
END

GO
