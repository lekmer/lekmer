
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageDecreaseNumberInStockByProduct]
	@ProductId	INT,
	@NewNumberInStock INT
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] <= @NewNumberInStock THEN [NumberInStock]
								ELSE @NewNumberInStock
						   END)
	WHERE
		[ProductId] IN (SELECT DISTINCT [pak].[MasterProductId]
						FROM [productlek].[tPackage] pak
							 INNER JOIN [productlek].[tPackageProduct] pp ON [pp].[PackageId] = [pak].[PackageId]
						WHERE [pp].[ProductId] = @ProductId)
END
GO
