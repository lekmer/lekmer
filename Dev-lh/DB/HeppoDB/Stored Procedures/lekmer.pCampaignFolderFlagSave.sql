SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pCampaignFolderFlagSave]
	@FolderId	INT,
	@FlagId		INT
AS
BEGIN
	
	IF @FlagId IS NULL
	BEGIN
		DELETE [lekmer].[tCampaignFolderFlag]
		WHERE [FolderId] = @FolderId
		RETURN
	END
	
	UPDATE [lekmer].[tCampaignFolderFlag]
	SET [FlagId] = @FlagId
	WHERE [FolderId] = @FolderId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tCampaignFolderFlag]
		( 
			[FolderId],
			[FlagId]
		)
		VALUES
		(
			@FolderId,
			@FlagId
		)					
	END 
END

GO
