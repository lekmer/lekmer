SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_UpdateInappropriateFlag]
	@RatingReviewFeedbackId	INT,
	@InappropriateFlag BIT
AS    
BEGIN
	UPDATE 
		[review].[tRatingReviewFeedback]
	SET 
		Impropriate = @InappropriateFlag
	WHERE 
		RatingReviewFeedbackId = @RatingReviewFeedbackId
END

GO
