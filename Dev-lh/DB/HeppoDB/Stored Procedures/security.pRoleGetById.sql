SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pRoleGetById]
		@RoleId	int
	as
	begin
		set nocount on

		select
			*
		from
			[security].[vCustomRole]
		where
			[Role.Id] = @RoleId
	end

GO
