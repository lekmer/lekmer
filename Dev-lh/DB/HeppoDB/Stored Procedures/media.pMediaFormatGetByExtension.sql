SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pMediaFormatGetByExtension]
	@Extension nvarchar(8)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomMediaFormat] v
	WHERE  v.[MediaFormat.Extension] = @Extension
END

GO
