SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [product].[pVariationTypeDeleteAllByVariationGroup]
	@VariationGroupId	int
as
BEGIN
	DELETE FROM
			product.tVariationTypeTranslation
		FROM
			product.tVariationTypeTranslation vt
			INNER JOIN [product].[tVariationType] as v ON v.VariationTypeId = vt.VariationTypeId
		WHERE
			[VariationGroupId] = @VariationGroupId	
			
	DELETE FROM
		[product].[tVariationType] 
	WHERE
		[VariationGroupId] = @VariationGroupId		
end

GO
