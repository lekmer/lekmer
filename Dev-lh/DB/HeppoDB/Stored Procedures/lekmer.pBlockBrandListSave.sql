SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListSave]
	@BlockId			int,
	@ColumnCount		int,
	@RowCount			int,
	@IncludeAllBrands	bit,
	@LinkContentNodeId	int
AS 
BEGIN 
	UPDATE lekmer.tBlockBrandList
	SET 
		ColumnCount = @ColumnCount,
		[RowCount] = @RowCount,
		IncludeAllBrands = @IncludeAllBrands,
		LinkContentNodeId = @LinkContentNodeId
	WHERE 
		BlockId = @BlockId
		
	IF @@ROWCOUNT <> 0
		RETURN
		
	INSERT lekmer.tBlockBrandList
	(
		BlockId,
		ColumnCount,
		[RowCount],
		IncludeAllBrands,
		LinkContentNodeId
	)
	VALUES
	(
		@BlockId,
		@ColumnCount,
		@RowCount,
		@IncludeAllBrands,
		@LinkContentNodeId
	)
END 
GO
