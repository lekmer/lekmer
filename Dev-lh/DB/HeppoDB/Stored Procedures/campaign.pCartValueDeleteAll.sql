SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartValueDeleteAll]
	@ConditionId int
AS 
BEGIN
	DELETE FROM campaign.tCartValueConditionCurrency
	WHERE ConditionId = @ConditionId
END
GO
