
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pCreateBrandPagesByTitle]
	@BrandTitle NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @Exists INT
	SET @Exists = (SELECT BrandId FROM lekmer.tBrand WHERE Title = @BrandTitle)
	
	IF @BrandTitle IS NOT NULL AND @BrandTitle != '' AND @Exists IS NOT NULL
	BEGIN
		EXEC integration.pCreateBrandPagesByTitleSE @BrandTitle 
		EXEC integration.pCreateBrandPagesByTitleNO @BrandTitle 
		EXEC integration.pCreateBrandPagesByTitleDK @BrandTitle 
		EXEC integration.pCreateBrandPagesByTitleFI @BrandTitle 
		EXEC integration.pCreateBrandPagesByTitleNL @BrandTitle 
		EXEC integration.pCreateBrandPagesByTitleFR @BrandTitle
		
		PRINT 'Created brandpages for ' + @BrandTitle + ' in all channels.'
	END
	ELSE IF @Exists IS NULL
	BEGIN
		PRINT 'Cant find the brand ' + @BrandTitle
	END
END

GO
