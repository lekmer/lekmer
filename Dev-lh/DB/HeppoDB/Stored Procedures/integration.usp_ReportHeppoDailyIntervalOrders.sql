SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ReportHeppoDailyIntervalOrders]

AS
begin
	set nocount on
	begin try
		begin transaction
			
			declare @date nvarchar(50)
			set @date = CONVERT (date, GETDATE()) 
			set @date = @date + ' 16:45:00.497'
			--select @date

			select o.OrderId, o.Email, op.Price, CreatedDate, case ChannelId when 1 then 'Sweden'
												when 4 then 'Norway'
												when 657 then 'Denmark'
												when 1000003 then 'Finland'
												end as Channel
			from 
					   [order].tOrder o
					   inner join [order].tOrderPayment op
								  on o.OrderId = op.OrderId
								  and o.OrderStatusId = 4
			where 
								  (op.Price > 2
								  and op.Price < 500
								  and o.Createddate > DATEADD(day, -1, Convert(varchar, @date, 112))
								  and ChannelId = 1
								  and o.OrderId in
													(
													select OrderId from Voucher.product.tVoucherLog
													where VoucherId = 39498 and OrderId != -1
													))
					   OR
								  (op.Price > 100
								  and op.Price < 500
								  and o.Createddate > DATEADD(day, -1, Convert(varchar, @date, 112))
								  and ChannelId = 4
								  and o.OrderId in
													(
													select OrderId from Voucher.product.tVoucherLog
													where VoucherId = 39491 and OrderId != -1
													))
						OR
								  (op.Price > 100
								  and op.Price < 500
								  and o.Createddate > DATEADD(day, -1, Convert(varchar, @date, 112))
								  and ChannelId = 657
								  and o.OrderId in
													(
													select OrderId from Voucher.product.tVoucherLog
													where VoucherId = 39492 and OrderId != -1
													))
						OR
								  (op.Price > 2
								  and op.Price < 50
								  and o.Createddate > DATEADD(day, -1, Convert(varchar, @date, 112))
								  and ChannelId = 1000003
								  and o.OrderId in
													(
													select OrderId from Voucher.product.tVoucherLog
													where VoucherId = 39493 and OrderId != -1
													))

	--select * from core.tChannel
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
