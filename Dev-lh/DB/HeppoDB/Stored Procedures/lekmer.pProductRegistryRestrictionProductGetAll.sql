SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAll]
AS
BEGIN
	SELECT ProductId, ProductRegistryId 
	FROM [lekmer].[tProductRegistryRestrictionProduct]
END
GO
