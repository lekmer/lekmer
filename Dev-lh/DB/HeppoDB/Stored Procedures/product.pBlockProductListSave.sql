SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yura P.	Date: 02.12.2008	Time: 13:00
Description:	Created

*****************  Version 2  *****************
User: Yura P.	Date: 22.12.2008	Time: 13:00
Description:	Edited(remove assortments save)
*/

CREATE procedure [product].[pBlockProductListSave]
	@BlockId		int,
	@ColumnCount	int,
	@RowCount		int
as
BEGIN
	
	update
		[product].[tBlockProductList]
	set
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount
	where
		[BlockId]		= @BlockId

	if  @@ROWCOUNT = 0
	begin
		insert
			[product].[tBlockProductList]
			(
				[BlockId],
				[ColumnCount],
				[RowCount]
			)
		values
			(
				@BlockId,
				@ColumnCount,
				@RowCount
			)
	end
end
GO
