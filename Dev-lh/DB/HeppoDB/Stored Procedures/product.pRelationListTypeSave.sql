SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman A.		Date: 04.09.2008		Time: 13:00
Description: 
	Added CommonName
*/

CREATE procedure [product].[pRelationListTypeSave]
	@RelationListTypeId	int,
	@CommonName			varchar(50),
	@Description		nvarchar(50)
as
begin
	set nocount on
	
	if exists(
				select 
					1 
				from 
					product.tRelationListType
				where 
					CommonName = @CommonName 
					and RelationListTypeId <> @RelationListTypeId
				)
		return -1

	update
		product.tRelationListType
	set
		CommonName = @CommonName,
		Description = @Description
	where
		RelationListTypeId = @RelationListTypeId
		
	if  @@ROWCOUNT = 0
	begin
		insert 
			product.tRelationListType
		(
			CommonName,
			Description
		)
		values
		(
			@CommonName,
			@Description
		)

		set @RelationListTypeId = scope_identity()
	end
	return @RelationListTypeId
end




GO
