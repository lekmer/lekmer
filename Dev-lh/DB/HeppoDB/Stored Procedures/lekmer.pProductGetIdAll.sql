SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pProductGetIdAll]
	@ChannelId INT,
	@CustomerId INT,
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)
	
	IF @CustomerId is not null
		BEGIN
			RAISERROR ('Current version doesn''t support CustomerId parameter.',10,1)
		END


	SELECT COUNT(*)
		FROM
			product.tProduct AS p
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			and P.IsDeleted = 0
			AND P.ProductStatusId = 0


	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
			INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
			INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
			INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
		WHERE
			Ch.ChannelId = @ChannelId
			and P.IsDeleted = 0
			AND P.ProductStatusId = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
