SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [customer].[pCustomerInformationSave]
	@CustomerId int,
	@FirstName nvarchar(50),
	@LastName nvarchar(50),
	@CivicNumber nvarchar(50),
	@PhoneNumber nvarchar(50),
	@CellPhoneNumber nvarchar(50),
	@Email varchar(320),
	@CreatedDate DATETIME,
	@DefaultBillingAddressId INT = null,
	@DefaultDeliveryAddressId INT = null

as	
begin
	set nocount on	
	UPDATE
		[customer].[tCustomerInformation]
	SET    
		FirstName = @FirstName, 
		LastName = @LastName,
		CivicNumber = @CivicNumber,
		PhoneNumber = @PhoneNumber,
		CellPhoneNumber = @CellPhoneNumber,
		Email = @Email,
		DefaultBillingAddressId = @DefaultBillingAddressId,
		DefaultDeliveryAddressId = @DefaultDeliveryAddressId
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO	[customer].[tCustomerInformation]
		(
			CustomerId,
			FirstName, 
			LastName,
			CivicNumber,
			PhoneNumber,
			CellPhoneNumber,
			Email,
			CreatedDate,
			DefaultBillingAddressId,
			DefaultDeliveryAddressId
		)
		VALUES
		(
			@CustomerId,
			@FirstName, 
			@LastName,
			@CivicNumber,
			@PhoneNumber,
			@CellPhoneNumber,
			@Email,
			@CreatedDate,
			@DefaultBillingAddressId,
			@DefaultDeliveryAddressId
		)
	END
	RETURN @CustomerId
end

GO
