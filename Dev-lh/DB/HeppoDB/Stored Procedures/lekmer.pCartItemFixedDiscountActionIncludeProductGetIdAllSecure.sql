SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[lekmer].[tCartItemFixedDiscountActionIncludeProduct] c
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = c.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END


GO
