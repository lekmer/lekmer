
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pImportProductSize]
	@HYErpId NVARCHAR(50), -- '123456-0001'
	@HYErpIdSize NVARCHAR(50), -- '123456-0001-234'
	@ChannelId INT, -- 1
	@HYSizeValue NVARCHAR(50), -- 'EU-37'
	@NumberInStock INT,
	@VarugruppId NVARCHAR(10),
	@VaruklassId NVARCHAR(10),
	@VarukodId NVARCHAR(10),
	
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Data NVARCHAR(4000)
	DECLARE @HYSizeId NVARCHAR(50)
	DECLARE @SizeId INT
	DECLARE @CategoryId INT
	DECLARE @NeedToInsertProductSize BIT
	
	SET @HYSizeId = SUBSTRING(@HYErpIdSize, 13, 3)

	IF @ChannelId = 1
	IF @HYSizeId NOT IN ('000', '251', '287') -- '**Storlekslös', 'One size', 'OnesizeStrumpor'
	IF NOT EXISTS(SELECT 1 FROM lekmer.tProductSize WHERE ErpId = @HYErpIdSize)
	BEGIN TRY
		BEGIN TRANSACTION
		
		SET @Data = 'BEGIN: @HYSizeValue ' + @HYSizeValue
		
		SET @NeedToInsertProductSize = 0
		
		SET @SizeId = (SELECT SizeId FROM lekmer.tSize s WHERE s.ErpId = @HYSizeId)
		SET @NeedToInsertProductSize = 1
		
		IF @HYSizeId IN (SELECT SocksHYId FROM integration.tSocks)
		BEGIN
			SET @Data = 'NEW tProductSIZE Tags: @HYErpIdSize ' + @HYErpIdSize + ' @Fok ' + CAST(@ChannelId AS VARCHAR(10)) + ' @HYSizeValue ' + @HYSizeValue
		
			-- Tag socks with the appropriate tags
			-- if it is in socks category
			SET @CategoryId = (SELECT CategoryId 
							   FROM product.tCategory 
							   WHERE ErpId = 'C_' 
									 + ISNULL(@VaruklassId, '') + '-' 
									 + ISNULL(@VarugruppId, '') + '-' 
									 + ISNULL(@VarukodId, ''))
			IF @VaruklassId = '30' -- Accessoarer  / ***  / ***
				OR
				@CategoryId IN (
					1000209, -- Sko          / Skotillbehör       / Strumpor
					1000148, -- Skotilbehör  / Skotillbehör       / Strumpor
					1001182  -- Skotillbehör / Skotillbehör       / Galoscher
				) 
			BEGIN
				-- add tags
				INSERT INTO lekmer.tProductTag (
					ProductId,
					TagId
				)
				SELECT
					@ProductId,
					st.TagId
				FROM
					integration.tSocksTagHYIdMapping st
				WHERE
					st.SocksHYId = @HYSizeId
					AND st.TagId NOT IN
									(SELECT TagId 
									 FROM lekmer.tProductTag
									 WHERE productId = @ProductId)																				
			END
		END
		
		IF (@NeedToInsertProductSize = 1 
			AND @SizeId IS NOT NULL 
			AND @ProductId IS NOT NULL)
		BEGIN
			IF NOT EXISTS (SELECT 1
						   FROM [lekmer].tProductSize ps
						   WHERE ps.ProductId = @ProductId 
						   AND ps.SizeId = @SizeId)
			BEGIN			
				INSERT INTO [lekmer].tProductSize (
					ProductId, 
					SizeId, 
					ErpId, 
					NumberInStock
				)
				VALUES (
					@ProductId,
					@SizeId, 
					@HYErpIdSize,
					@NumberInStock
				)
			END
		END
		
		COMMIT	
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		
		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			@Data,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)

	END CATCH
		
END
GO
