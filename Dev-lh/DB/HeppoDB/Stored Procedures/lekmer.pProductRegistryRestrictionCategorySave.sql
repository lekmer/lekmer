SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategorySave]
	@ProductRegistryId	INT,
	@CategoryId			INT,
	@RestrictionReason	NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME
AS
BEGIN
	INSERT INTO [lekmer].[tProductRegistryRestrictionCategory] (
		ProductRegistryId,
		CategoryId,
		RestrictionReason,
		UserId,
		CreatedDate
	)
	VALUES (
		@ProductRegistryId,
		@CategoryId,
		@RestrictionReason,
		@UserId,
		@CreatedDate
	)
END
GO
