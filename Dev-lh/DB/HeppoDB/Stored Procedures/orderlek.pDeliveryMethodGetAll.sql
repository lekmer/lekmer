SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pDeliveryMethodGetAll]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomDeliveryMethod]
	ORDER BY
		[DeliveryMethod.Priority]
END
GO
