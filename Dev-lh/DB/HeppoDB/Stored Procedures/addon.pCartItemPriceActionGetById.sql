SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceActionGetById]
	@CartActionId int
as
begin
	select
		*
	from
		addon.vCustomCartItemPriceAction
	where
		[CartAction.Id] = @CartActionId
end
GO
