SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagTranslationGetAllByFlag]
	@FlagId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT
	    FlagId AS 'Id',
		LanguageId AS 'LanguageId',
		Title AS 'Value'
	FROM
	    lekmer.tFlagTranslation 
	WHERE 
		FlagId = @FlagId
END

GO
