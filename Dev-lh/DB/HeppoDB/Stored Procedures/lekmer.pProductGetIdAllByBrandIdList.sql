
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByBrandIdList]
	@ChannelId		INT,
	@CustomerId		INT,
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1),
	@Page			INT = NULL,
	@PageSize		INT = 1,
	@SortBy			VARCHAR(50) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @maxCount INT
	SET @maxCount = 2000
	
	DECLARE @sql NVARCHAR(MAX)
	
	IF (generic.fnValidateColumnName(@SortBy) = 0)
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	SET @sql = '
	DECLARE @tResult TABLE (ProductId INT, SortBy NVARCHAR(MAX))
	
	INSERT @tResult	(ProductId, SortBy)
	SELECT TOP (@maxCount) p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	FROM [product].[vCustomProduct] p
	INNER JOIN product.vCustomPriceListItem AS pli
		ON pli.[Price.ProductId] = P.[Product.Id]
		AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
			p.[Product.CurrencyId],
			P.[Product.Id],
			p.[Product.PriceListRegistryId],
			@CustomerId
		)
	WHERE
		p.[Product.ChannelId] = @ChannelId
		AND
		EXISTS (
			SELECT * FROM [generic].[fnConvertIDListToTableWithOrdinal](@BrandIds, @Delimiter) AS pl
			WHERE pl.Id = p.[Lekmer.BrandId]
		)

	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP (@Page * @PageSize) *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP @maxCount ProductId FROM @tResult AS SearchResult'
	END
			
	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@CustomerId	INT,
			@BrandIds	VARCHAR(MAX),
			@Delimiter	CHAR(1),
			@Page		INT,
			@PageSize	INT,
			@maxCount	INT',
			@ChannelId,
			@CustomerId,
			@BrandIds,
			@Delimiter,
			@Page,
			@PageSize,
			@maxCount
END
GO
