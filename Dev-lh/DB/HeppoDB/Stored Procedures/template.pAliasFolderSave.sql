SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pAliasFolderSave]
	@AliasFolderId		INT,
	@ParentAliasFolderId INT,
	@Title				NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		template.tAliasFolder
	SET
		ParentAliasFolderId = @ParentAliasFolderId,
		[Title] = @Title
	WHERE
		AliasFolderId = @AliasFolderId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [template].[tAliasFolder]
		(
			[Title],
			[ParentAliasFolderId]
		)
		VALUES
		(
			@Title,
			@ParentAliasFolderId
		)
		SET @AliasFolderId = scope_identity()
	END
	RETURN @AliasFolderId
END
	











GO
