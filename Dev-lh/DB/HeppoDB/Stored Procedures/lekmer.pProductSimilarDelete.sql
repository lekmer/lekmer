SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSimilarDelete]
	@ProductId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductSimilar]
	WHERE 
		[ProductId] = @ProductId
END 
GO
