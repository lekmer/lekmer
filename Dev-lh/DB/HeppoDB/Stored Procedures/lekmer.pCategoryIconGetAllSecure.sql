SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryIconGetAllSecure]
	@CategoryId	INT
AS 
BEGIN 
	SELECT 
		i.*
	FROM 
		[lekmer].[vIconSecure] i
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE
		[ci].[CategoryId] = @CategoryId
END
GO
