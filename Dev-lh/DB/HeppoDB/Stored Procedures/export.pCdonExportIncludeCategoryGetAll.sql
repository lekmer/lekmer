SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryGetAll]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId] 'ItemId',
		[ic].[ProductRegistryId] 'ProductRegistryId',
		[ic].[Reason] 'Reason',
		[ic].[UserId] 'UserId',
		[ic].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeCategory] ic
		 CROSS APPLY [product].[fnGetSubCategories] ([ic].[CategoryId]) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ic].[ProductRegistryId]
END
GO
