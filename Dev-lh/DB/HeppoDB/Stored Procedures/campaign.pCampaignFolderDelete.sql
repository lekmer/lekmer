SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCampaignFolderDelete]
@FolderId	int
AS
BEGIN
	DELETE FROM campaign.tCampaignFolder
	WHERE FolderId = @FolderId
END
GO
