SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 30.12.2008
Description:
			Created
*/

CREATE procedure [product].[pRelationListProductsDelete]
	@RelationListId int
as
begin	
    delete
		product.tRelationListProduct
	where
		RelationListId=@RelationListId
end









GO
