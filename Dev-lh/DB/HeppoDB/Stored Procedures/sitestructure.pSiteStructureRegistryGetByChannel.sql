SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pSiteStructureRegistryGetByChannel]
@ChannelId	int
as
begin
	select 
		ssr.*
	from 
		[sitestructure].[vCustomSiteStructureRegistry] ssr
		INNER JOIN [sitestructure].[vCustomSiteStructureModuleChannel] ssmc 
		ON ssmc.[SiteStructureModuleChannel.SiteStructureRegistryId] = ssr.[SiteStructureRegistry.Id]
	where 
		ssmc.[SiteStructureModuleChannel.ChannelId] = @ChannelId
end

GO
