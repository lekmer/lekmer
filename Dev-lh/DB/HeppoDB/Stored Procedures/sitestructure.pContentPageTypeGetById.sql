SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 10.02.2009
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentPageTypeGetById]
@ContentPageTypeId int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageType]
	WHERE
		[ContentPageType.Id] = @ContentPageTypeId
end

GO
