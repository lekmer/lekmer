SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pTagTranslationSave]
@TagId		int,
@LanguageId	int,
@Value		nvarchar(max)
AS
BEGIN
	SET nocount ON
	
	UPDATE
		lekmer.tTagTranslation
	SET
		Value = @Value 
	WHERE
		TagId = @TagId AND
		LanguageId = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tTagTranslation
		(
			TagId,
			LanguageId,
			Value				
		)
		VALUES
		(
			@TagId,
			@LanguageId,
			@Value
		)
	END
END




GO
