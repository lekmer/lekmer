SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionSave]
	@CartItemOptionId	INT,
    @CartId				INT,
    @ProductId			INT,
    @SizeId				INT,
	@ErpId				VARCHAR(50),
    @Quantity			INT,
    @CreatedDate		DATETIME
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[campaignlek].[tCartItemOption]
	SET
		SizeId = @SizeId,
		ErpId = @ErpId,
		Quantity = Quantity + @Quantity
	WHERE
		CartId = @CartId
		AND ProductId = @ProductId
            

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [campaignlek].[tCartItemOption]
		(
			  CartId,
			  ProductId,
			  SizeId,
			  ErpId,
			  Quantity,
			  CreatedDate            
		)
		VALUES
		(
			  @CartId,
			  @ProductId,
			  @SizeId,
			  @ErpId,
			  @Quantity,
			  @CreatedDate
		)
	END
	
	
	DELETE FROM [campaignlek].[tCartItemOption] WHERE Quantity <= 0
END
GO
