SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionExcludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END
GO
