SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ImportUpdateProductSizesHeppo]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		
		
		-- Ta numberInStock på allt innan import
		/*
		if object_id('tempdb..#ProductNumberInStockBeforeUpdateSize') is not null
		drop table #ProductNumberInStockBeforeUpdateSize
		
		if object_id('tempdb..#ProductNumberInStockAFTERUpdateSize') is not null
		drop table #ProductNumberInStockAFTERUpdateSize
		
		create table #ProductNumberInStockBeforeUpdateSize
		(
			ProductId int not null,
			NumberInStock int not null
			constraint PK_#ProductNumberInStockBeforeUpdateSize primary key(ProductId)
		)
		
		create table #ProductNumberInStockAFTERUpdateSize
		(
			ProductId int not null,
			NumberInStock int not null
			constraint PK_#ProductNumberInStockAFTERUpdateSize primary key(ProductId)
		)
		
		insert into #ProductNumberInStockBeforeUpdateSize (ProductId, NumberInStock)
		select 
			x.ProductId, 
			sum(x.NumberInStock)
		from 
			(select 
				substring(ErpId, 0,12) as ErpId,
				NumberInStock,
				ProductId
			from
				lekmer.tProductSize) x
		group by
			 x.ProductId			 				

		*/
		
		-- tProductSize update
		--print 'Start update product'
		UPDATE 
			ps
		SET 
			ps.NumberInStock = tp.NoInStock
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		--select ps.Erpid, substring(tp.HYarticleId, 5,15), tp.HYarticleId, substring(tp.HYarticleId, 3,1), tp.NoInStock, ps.numberinstock
		FROM
			integration.tempProduct tp
			inner join lekmer.tProductSize ps
				on ps.ErpId = substring(tp.HYarticleId, 5,17)
		WHERE 
			--substring(tp.HYarticleId, 5,17) = '101040-0001-178'
			ps.NumberInStock <> tp.NoInStock
			AND
			substring(tp.HYarticleId, 3,1) = 1
		
		/*
		-- Ta numberInStock på allt EFTER import
		insert into #ProductNumberInStockAFTERUpdateSize (ProductId, NumberInStock)
		select 
			x.ProductId, 
			sum(x.NumberInStock)
		from 
			(select 
				substring(ErpId, 0,12) as ErpId,
				NumberInStock,
				ProductId
			from
				lekmer.tProductSize) x
		group by
			 x.ProductId 
			 
		
		-- Skapa Nyhet om den har fyllts på med 6 eller mer
		update
			l2
		set
			l2.IsNewFrom = DATEADD(WK, 0, Convert(varchar, GETDATE(), 112)),
			l2.IsNewTo = DATEADD(WK, 2, Convert(varchar, GETDATE(), 112))
		from 
			lekmer.tLekmerProduct l2
			inner join #ProductNumberInStockBeforeUpdateSize pc2
				on l2.ProductId = pc2.ProductId
			inner join #ProductNumberInStockAFTERUpdateSize a2
				on a2.ProductId = pc2.ProductId											
		where 
			-- IsNewTo måste vara mer än dagens datum för att jag vill inte förnya om den är en nyhet
			--l2.IsNewTo < DATEADD(WK, 0, Convert(varchar, GETDATE(), 112))
			--and 
			(pc2.NumberInStock + 6) <= a2.NumberInStock 
		*/			
		
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@NoInStock nvarchar(250),
				@HYSizeId nvarchar (50),	
				@Data nvarchar(4000),
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10)

		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				DECLARE @SizeChar NVARCHAR (20)
				DECLARE @SizeNo NVARCHAR (20)
				------------------------------------
		
		declare cur_productHeppo cursor fast_forward for
			select
				tp.HYarticleId,
				tp.NoInStock,
				tp.SizeId,
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId
			from
				[integration].tempProduct tp
			where not exists(select 1
						from lekmer.tProductSize ps
						where ps.ErpId = substring(tp.HYarticleId, 5,17))
						AND tp.SizeId != 'Onesize' -- ny
						--AND tp.SizeId not in (select SocksHYText from integration.tSocks) -- ny socks tags
						AND substring(tp.HYarticleId, 3,1) = 1 -- ny
			
			
		--print 'Open Cursor'
		open cur_productHeppo
		fetch next from cur_productHeppo
			into @HYArticleId,			 
				 @NoInStock,
				 @HYSizeId,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				--SET @HYarticleSize = substring(@HYarticleId, 17,3)-- FEL
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,11)
				
				
				
				--print 'Start @SizeChar = ' + @Sizechar
				-- splitta sizeid på semi kolon
				SET @String = @HYSizeId -- EU-45-
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeChar = substring(@String,1,@Pos - 1)
				--print 'End @SizeChar = ' + @Sizechar
			
				--print 'Start @SizeNo = ' + @SizeNo
				SET @String = @HYSizeId
				SET @String = @String --+ @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeNo = substring(@String,(@Pos+1),(len(@String)- (@pos)))
				--print 'End @SizeNo = ' + @SizeNo
				
				-- om den specifika sizen av produkten inte finns i tProductSize
				if @SizeChar not in ('XS','S','M','L','XL','Onesize', 'Storlekslös')
				and @HYSizeId not in (select SocksHYText from integration.tSocks) -- NY
				begin
				
					-- Handel the @sizeChar so when it is USM it presents as USMale
					SET @SizeChar = dbo.fx_SizeStandard(@SizeChar)
					
					
					-- Prepare the query that retrives SizeId from tSize
					-- executes a string and returns sizeId
					DECLARE @tSizeIdResult int
					DECLARE @SizeId as int,@sqlQ as nvarchar(200)
					SET @sqlQ=N'select @SizeId = SizeId FROM lekmer.tSize WHERE ' + @SizeChar + ' = ' + REPLACE(@SizeNo, ',', '.')
					EXEC sp_executesql @sqlQ, N'@SizeId int OUTPUT', @SizeId= @SizeId OUTPUT
					--SELECT @SizeId
					SET @tSizeIdResult = @SizeId
					
					set @Data = 'NEW tProductSIZE Shoes: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' @SizeChar ' + @SizeChar + ' @SizeNo ' + @SizeNo
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						@tSizeIdResult, 
						@HYarticleIdNoFoK,
						@NoInStock			
				end
				
				else if @SizeChar not in ('Onesize', 'Storlekslös')
				and @HYSizeId not in (select SocksHYText from integration.tSocks) -- ny
				begin
					
					set @Data = 'NEW tProductSIZE Accessories: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' @SizeChar ' + @SizeChar
					
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						(select AccessoriesSizeId from integration.tAccessoriesSize where AccessoriesSize = @SizeChar), 
						@HYarticleIdNoFoK,
						@NoInStock
				end
				
				else if @HYSizeId in (select SocksHYText from integration.tSocks)
				begin
					-- Tag socks with the appropriet tags
					-- if it is in socks category
					if (SELECT CategoryId FROM product.tCategory 
						WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId) = 1000209
						or
						(SELECT CategoryId FROM product.tCategory 
						WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId) = 1000148
						begin
							-- add tags
							--print @HYsizeId
							--print @HYarticleIdNoFokNoSize
							insert into lekmer.tProductTag(ProductId,TagId)
							select
								(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
								st.TagId
							
							from
								integration.tSocks s
								inner join integration.tSocksTagHYIdMapping st
									on s.SocksHYId = st.SocksHYId
							where
								s.SocksHYText = @HYSizeId
								and st.TagId not in
												(select TagId 
													from lekmer.tProductTag
													where productId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize)
												)
												
								--and not exists (select 1
													--from lekmer.tProductTag
													--where productId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize)
													--and Tagid = st.TagId)
									
							----  nytt endast strumpor
							INSERT INTO 
								[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
								SELECT 
									(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
									(select SizeId from lekmer.tSize where EU =
														(select AccessoriesSizeId 
														from integration.tAccessoriesSize 
														where AccessoriesSize = @HYSizeId)), 
									@HYarticleIdNoFoK,
									@NoInStock
						end
				end
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
				--print 'Loging error'
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_productHeppo into
				 @HYArticleId,	 
				 @NoInStock,	
				 @HYSizeId,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId
	
		end
		
		close cur_productHeppo
		deallocate cur_productHeppo

END
GO
