SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pProductAutoFreeCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [campaignlek].[tProductAutoFreeCartAction]
	WHERE CartActionId = @CartActionId
END
GO
