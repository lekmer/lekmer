SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetIdAllByMedia]
	@MediaId INT
AS
BEGIN
	SELECT 
		t.ProductId
	FROM 
		product.tProduct t
	WHERE 
		t.MediaId = @MediaId
END
GO
