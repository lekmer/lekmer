SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemTranslationSave]
	@RatingItemId	INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[review].[tRatingItemTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[RatingItemId] = @RatingItemId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [review].[tRatingItemTranslation] (
			[RatingItemId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES (
			@RatingItemId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END
GO
