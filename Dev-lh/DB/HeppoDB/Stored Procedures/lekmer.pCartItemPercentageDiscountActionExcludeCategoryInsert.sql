SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END


GO
