SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandTopListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
