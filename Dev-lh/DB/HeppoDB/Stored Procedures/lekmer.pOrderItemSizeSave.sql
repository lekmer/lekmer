SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE lekmer.[pOrderItemSizeSave]
	@OrderItemId INT,
	@SizeId INT,
	@ErpId VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE
		lekmer.[tOrderItemSize]
	SET
		SizeId = @SizeId,
		ErpId = @ErpId
	WHERE
		OrderItemId = @OrderItemId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO lekmer.[tOrderItemSize]
		(
			OrderItemId,
			SizeId,
			ErpId
		)
		VALUES
		(
			@OrderItemId,
			@SizeId,
			@ErpId
		)
	END
END




GO
