SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pFreeFreightFlagActionGetById]
	@Id int
AS
BEGIN
	SELECT
		*
	FROM
		lekmer.vFreeFreightFlagAction
	WHERE
		[ProductAction.Id] = @Id
END

GO
