SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [sitestructure].[pBlockSetStatusAndAccess]
	@BlockId int,
	@StatusId int,
	@AccessId int
as
begin
	UPDATE
		[sitestructure].tBlock
	SET
		BlockStatusId = COALESCE(@StatusId, BlockStatusId),
		AccessId = COALESCE(@AccessId, AccessId)
	WHERE
		BlockId = @BlockId
end
GO
