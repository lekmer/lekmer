SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pProductGetIdAllWithoutAnyFilter]
	@Page INT = NULL,
	@PageSize INT
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFilter NVARCHAR(MAX)
	DECLARE @sqlCountFilter NVARCHAR(MAX)

	SELECT COUNT(*)
		FROM
			product.tProduct AS p
		WHERE
			P.IsDeleted = 0

	SELECT * FROM (
		SELECT ROW_NUMBER() OVER (ORDER BY p.[ProductId]) AS Number,
			p.[ProductId]
		FROM
			product.tProduct AS p
		WHERE
			P.IsDeleted = 0
	) As Result
	WHERE Number > (@Page - 1) * @PageSize
		AND Number <= @Page * @PageSize
END
GO
