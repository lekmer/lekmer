SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeBrandGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		BrandId
	FROM 
		[campaignlek].[tCampaignActionExcludeBrand]
	WHERE 
		ConfigId = @ConfigId
END
GO
