SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRatingGroupSave]
	@BlockId INT,
	@RatingGroupId INT
AS
BEGIN
	INSERT [review].[tBlockRatingGroup] (
		[BlockId],
		[RatingGroupId]
	)
	VALUES (
		@BlockId,
		@RatingGroupId
	)
END
GO
