
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber
		)
	END
END 
GO
