SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMonitorProductDeleteByEmail]
	@ChannelId INT,
	@Email VARCHAR(320)
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[lekmer].[tMonitorProduct]
	WHERE
		[ChannelId] = @ChannelId
		AND [Email] = @Email
END
GO
