SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pImageImportLogMessage]
	@Message	nvarchar(Max),
	@Data		nvarchar(Max)
AS
begin
	set nocount on
			
		INSERT INTO 
			[integration].[tImageImportLog]
			(
				Data, 
				[Message], 
				[Date]
			)
			values
			(
				@Data, 
				@Message, 
				GETDATE()
				)	 
end
GO
