SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductSiteStructureRegistryGetAllByProduct]
	@ProductId	int
as
begin
	select
		P.ProductId 'SiteStructureRegistry.ProductId',
		R.SiteStructureRegistryId 'SiteStructureRegistry.SiteStructureRegistryId',
		vPR.[SiteStructureRegistry.ParentContentNodeId],
		vPR.[SiteStructureRegistry.TemplateContentNodeId]
	FROM
		product.tProduct P
		CROSS JOIN sitestructure.tSiteStructureRegistry R
		LEFT JOIN product.vCustomProductSiteStructureRegistry vPR
			ON vPR.[SiteStructureRegistry.SiteStructureRegistryId] = r.SiteStructureRegistryId
			AND vPR.[SiteStructureRegistry.ProductId] = p.ProductId
	where 
		p.ProductId = @ProductId
	order by
		r.Title
end

GO
