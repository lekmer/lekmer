SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SELECT
		btl.*,
		b.*
	FROM 
		[lekmer].[vBlockBrandTopList] AS btl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON btl.[BlockBrandTopList.BlockId] = b.[Block.BlockId]
	WHERE
		btl.[BlockBrandTopList.BlockId] = @BlockId
END
GO
