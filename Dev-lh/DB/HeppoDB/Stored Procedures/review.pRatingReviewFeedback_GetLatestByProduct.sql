SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_GetLatestByProduct]
	@ChannelId				INT,
	@ProductIds				VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@NumberOfItems			INT,
	@RatingReviewStatusId	INT,
	@IsImpropriate			BIT,
	@CustomerId				INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT 
		TOP(@NumberOfItems) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [product].[vCustomProduct] p 
			ON p.[Product.Id] = rrf.[RatingReviewFeedback.ProductId] 
			AND p.[Product.ChannelId] = rrf.[RatingReviewFeedback.ChannelId]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) pl ON pl.[ID] = p.[Product.Id]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Impropriate] = @IsImpropriate
	ORDER BY rrf.[RatingReviewFeedback.CreatedDate] DESC
END
GO
