SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreGetBestRatedProductsByRating]
	@ChannelId		INT,
	@RatingId		INT,
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@Delimiter		CHAR(1),
	@NumberOfItems	INT,
	@CustomerId		INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @sql NVARCHAR(MAX)
    SET @sql = '
		SELECT 
			TOP(@NumberOfItems) rips.ProductId
		FROM 
			review.tRatingItemProductScore rips
			INNER JOIN review.tRatingItem ri 
				ON ri.RatingItemId = rips.RatingItemId
			INNER JOIN [product].[vCustomProduct] p 
				ON rips.ProductId = p.[Product.Id] 
				AND rips.ChannelId = p.[Product.ChannelId]
			INNER JOIN product.vCustomPriceListItem AS pli
				ON pli.[Price.ProductId] = p.[Product.Id]
				AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)'
			+ CASE WHEN (@CategoryIds IS NOT NULL AND @CategoryIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) c ON c.[ID] = p.[Product.CategoryId]' 
			ELSE '' END
			+ CASE WHEN (@BrandIds IS NOT NULL AND @BrandIds <> '') THEN 
				+ 'INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) b ON b.[ID] = p.[Lekmer.BrandId]' 
			ELSE '' END
		+ '
		WHERE 
			rips.ChannelId = @ChannelId
			AND rips.RatingId = @RatingId
		GROUP BY rips.ProductId
		ORDER BY SUM(ri.Score*rips.HitCount) DESC'
     
	EXEC sp_executesql @sql, 
		N'      
			@ChannelId		INT,
			@RatingId		INT,
			@CategoryIds	VARCHAR(MAX),
			@BrandIds		VARCHAR(MAX),
			@Delimiter		CHAR(1),
			@NumberOfItems	INT,
			@CustomerId		INT',
            @ChannelId,
            @RatingId,
            @CategoryIds,
            @BrandIds,
            @Delimiter,
            @NumberOfItems,
            @CustomerId
END

GO
