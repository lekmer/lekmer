SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductsNotOnline]

AS
begin
	set nocount on
					
		SELECT DISTINCT 
			l.HYErpId, 
			p.Description AS 'Default description',
			g.Title AS [Language],
			pt.[Description],
			h.Title AS 'Status' 
		FROM 
			lekmer.tlekmerproduct l
			INNER JOIN product.tproduct p
				ON p.productid = l.productid		
			LEFT JOIN product.tProductTranslation pt
				ON p.ProductId = pt.ProductId
			INNER JOIN core.tLanguage g
				ON pt.LanguageId = g.LanguageId
			INNER JOIN product.tProductStatus h
				ON h.ProductStatusId = p.ProductStatusId
			LEFT JOIN lekmer.tproductsize s
				ON p.productid = s.productid				
		WHERE 
			p.ProductStatusId BETWEEN 2 AND 6
			AND p.MediaId IS NOT NULL 
			AND pt.LanguageId != 2
			AND (p.NumberInStock > 0 AND s.NumberInStock IS NULL
				OR (s.NumberInStock > 0))
			AND 
				(
					p.[Description] != ''
					OR	
						(
							pt.[Description] IS NOT NULL 
							AND pt.[Description] != ''
						)
				)
						
end
GO
