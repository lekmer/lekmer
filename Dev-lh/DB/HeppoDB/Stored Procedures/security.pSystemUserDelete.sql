SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE procedure [security].[pSystemUserDelete]
	@SystemUserId	int
as
begin
	delete
		[security].[tSystemUser]
	where
		[SystemUserId] = @SystemUserId
end





GO
