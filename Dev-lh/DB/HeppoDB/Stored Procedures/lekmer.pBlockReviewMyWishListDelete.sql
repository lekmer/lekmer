SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE 	[lekmer].[pBlockReviewMyWishListDelete]
@BlockId	int
as
BEGIN
	
	

	DELETE 
		[lekmer].[tBlockReviewMyWishList] 
	where
		[BlockId] = @BlockId
end
GO
