SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListItemGetAllByPriceList]
	@PriceListId int,
	@Page int = null,
	@PageSize int = null
AS
BEGIN
	
	DECLARE @sql NVARCHAR(MAX)
    DECLARE @sqlCount NVARCHAR(MAX)
    DECLARE @sqlFragment NVARCHAR(MAX)
	
	SET @sqlFragment = 'SELECT
			ROW_NUMBER() OVER
			(
				ORDER BY [Price.ProductId] asc
			) as Number,
			*
	FROM 
		product.vCustomPriceListItem
	WHERE
		[Price.PriceListId] = ' + CAST (@PriceListId AS NVARCHAR(10))

	
	  SET @sql = 'SELECT * FROM
			(' + @sqlFragment + '
			)
			AS SearchResult'
			
		IF @Page != 0 AND @Page IS NOT NULL 
		BEGIN
			SET @sql = @sql + '
			WHERE Number > ' + CAST(( @Page - 1 ) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
		END
		SET @sql = @sql + 'ORDER BY
		Number'
		
		SET @sqlCount = 'SELECT COUNT(1) FROM
			(
			' + @sqlFragment + '
			)
			AS CountResults'
			
		EXEC sp_executesql @sqlCount
		EXEC sp_executesql @sql        
	
END

GO
