SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeShortcutLinkGetById]
@ContentNodeId	int
as
begin
	select 
		cns.*
	from 
		[sitestructure].[vCustomContentNodeShortcutLinkSecure] as cns
	where
		cns.[ContentNode.ContentNodeId] = @ContentNodeId
end

GO
