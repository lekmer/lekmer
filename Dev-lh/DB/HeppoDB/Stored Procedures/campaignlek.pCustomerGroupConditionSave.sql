SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionSave]
	@ConditionId INT
AS 
BEGIN
	IF EXISTS (SELECT 1 FROM [campaignlek].[tCustomerGroupCondition] WHERE ConditionId = @ConditionId)
		RETURN
	
	INSERT [campaignlek].[tCustomerGroupCondition]
	( 
		ConditionId
	)
	VALUES 
	(
		@ConditionId
	)
END
GO
