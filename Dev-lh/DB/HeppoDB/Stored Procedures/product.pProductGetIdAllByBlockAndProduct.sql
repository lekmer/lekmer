
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetIdAllByBlockAndProduct]
		@ChannelId int,
		@CustomerId int,
		@BlockId int,
		@ProductId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(50) = NULL,
		@SortDescending bit = NULL
AS
BEGIN
	DECLARE @maxCount INT
	SET @maxCount = 2000

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @sql NVARCHAR(MAX)
	
	SET @sql = '
	DECLARE @tResult TABLE (ProductId INT, SortBy NVARCHAR(MAX))

	INSERT @tResult	(ProductId, SortBy)
	SELECT DISTINCT TOP (@maxCount) p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		prl.ProductId = @ProductId
		AND p.[Product.ChannelId] = @ChannelId
		AND bprli.BlockId = @BlockId


	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP (@Page * @PageSize) *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > (@Page - 1) * @PageSize
			AND Number <= @Page * @PageSize'
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP @maxCount ProductId FROM @tResult AS SearchResult'
	END
			
	EXEC sp_executesql @sql, 
		N'	@ChannelId	INT, 
			@CustomerId	INT,
			@BlockId	INT,
			@ProductId	INT,
			@Page		INT,
			@PageSize	INT,
			@maxCount	INT',
			@ChannelId,
			@CustomerId,
			@BlockId,
			@ProductId,
			@Page,
			@PageSize,
			@maxCount
END
GO
