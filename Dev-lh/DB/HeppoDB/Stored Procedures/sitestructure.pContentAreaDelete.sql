SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 15:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentAreaDelete]
@ContentAreaId	int
as
begin
	DELETE
		sitestructure.tBlock
	WHERE 
		ContentAreaId = @ContentAreaId
	DELETE
		[sitestructure].[tContentArea]
	WHERE
		[ContentAreaId] = @ContentAreaId
	return 1
end
GO
