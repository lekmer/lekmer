SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-23
-- Description:	Gets a media folder by id
-- =============================================
CREATE PROCEDURE [media].[pMediaFolderGetById]
	@MediaFolderId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomMediaFolder] v
	WHERE  v.[MediaFolder.Id] = @MediaFolderId
END

GO
