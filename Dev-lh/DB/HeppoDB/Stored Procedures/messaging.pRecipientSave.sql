SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [messaging].[pRecipientSave]
    @RecipientId UNIQUEIDENTIFIER,
    @MessageId UNIQUEIDENTIFIER,
    @BatchId UNIQUEIDENTIFIER,
    @Chunk INT,
    @Address NVARCHAR(500)
AS 
    UPDATE  [messaging].[tRecipient]
    SET     MessageId = @MessageId,
            BatchId = @BatchId,
            Chunk = @Chunk,
            [Address] = @Address
    WHERE   RecipientId = @RecipientId
        
	IF @@ROWCOUNT = 0
    INSERT  INTO [messaging].[tRecipient]
            (
              RecipientId,
              MessageId,
              BatchId,
              Chunk,
              [Address]
				
            )
    VALUES  (
              @RecipientId,
              @MessageId,
              @BatchId,
              @Chunk,
              @Address
				
            )
GO
