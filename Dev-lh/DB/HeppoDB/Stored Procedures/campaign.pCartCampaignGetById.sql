SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartCampaignGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCartCampaign
	WHERE
		[Campaign.Id] = @Id
end

GO
