SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsGetById]
	@LanguageId INT,
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesAds] AS bea
	WHERE
		bea.[Block.BlockId] = @BlockId
		AND
		bea.[Block.LanguageId] = @LanguageId
END
GO
