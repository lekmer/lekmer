
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pProductGetIdAllAvaliableForCdonExport]
	@MonthPurchasedAgo INT
AS
BEGIN
	SELECT [p1].[ProductId]
	FROM [product].[tProduct] p1
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p1].[ProductId]
	WHERE 
		[p1].[IsDeleted] = 0
		AND [p1].[ProductStatusId] = 0
		AND [lp].[ProductTypeId] = 1
	UNION
	SELECT DISTINCT ([p2].[ProductId])
	FROM [product].[tProduct] p2
		 INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderItemId] = [oip].[OrderItemId]
		 INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
	WHERE 
		[p2].[IsDeleted] = 0
		AND [p2].[ProductStatusId] = 1
		AND [o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > DATEADD(MONTH, -@MonthPurchasedAgo, GETDATE())
		AND [lp].[ProductTypeId] = 1
END
GO
