SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductVariantGetIdAllByProductForCdonExport]
	@ProductId		INT,
	@RelationListTypeId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId])EXEC [export].[pProductGetIdAllAvaliableForCdonExport]
	
	SELECT
		DISTINCT rlp.[ProductId]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.[RelationListId] = rl.[RelationListId]
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.[RelationListId] = rlp.[RelationListId]
	WHERE
		prl.[ProductId] = @ProductId
		AND rlp.[ProductId] <> @ProductId
		AND rl.[RelationListTypeId] = @RelationListTypeId
		AND rlp.[ProductId] IN (
			SELECT DISTINCT ([up].[ProductId])
			FROM (SELECT * FROM #ProductIds) up)

	DROP TABLE #ProductIds
END
GO
