SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pPaymentTypeGetById]
	@PaymentTypeId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	where
		[PaymentType.PaymentTypeId] = @PaymentTypeId
END


GO
