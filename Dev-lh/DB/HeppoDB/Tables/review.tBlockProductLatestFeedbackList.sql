CREATE TABLE [review].[tBlockProductLatestFeedbackList]
(
[BlockId] [int] NOT NULL,
[NumberOfItems] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockProductLatestFeedbackList] ADD CONSTRAINT [PK_tBlockProductLatestFeedbackList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockProductLatestFeedbackList] ADD CONSTRAINT [FK_tBlockProductLatestFeedbackList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
