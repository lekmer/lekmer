CREATE TABLE [integration].[tCdonExport]
(
[CdonExportId] [int] NOT NULL IDENTITY(1, 1),
[ImportId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Type] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NULL,
[Status] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tCdonExport] ADD CONSTRAINT [PK__tCdonExp__5D7427525942F780] PRIMARY KEY CLUSTERED  ([CdonExportId]) ON [PRIMARY]
GO
ALTER TABLE [integration].[tCdonExport] ADD CONSTRAINT [FK_tCdonExport_tCdonExportStatus] FOREIGN KEY ([Status]) REFERENCES [integration].[tCdonExportStatus] ([CdonExportStatusId])
GO
