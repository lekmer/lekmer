CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeProduct_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
