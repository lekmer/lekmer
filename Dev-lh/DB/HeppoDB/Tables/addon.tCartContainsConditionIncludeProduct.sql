CREATE TABLE [addon].[tCartContainsConditionIncludeProduct]
(
[ConditionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeProduct] ADD CONSTRAINT [PK_tCartContainsConditionIncludeProduct] PRIMARY KEY CLUSTERED  ([ConditionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeProduct] ADD CONSTRAINT [FK_tCartContainsConditionIncludeProduct_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId])
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeProduct] ADD CONSTRAINT [FK_tCartContainsConditionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
