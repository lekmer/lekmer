CREATE TABLE [media].[tMediaFormatGroup]
(
[MediaFormatGroupId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [media].[tMediaFormatGroup] ADD CONSTRAINT [PK_tMediaFormatGroup] PRIMARY KEY CLUSTERED  ([MediaFormatGroupId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tMediaFormatGroup_CommonName] ON [media].[tMediaFormatGroup] ([CommonName]) ON [PRIMARY]
GO
