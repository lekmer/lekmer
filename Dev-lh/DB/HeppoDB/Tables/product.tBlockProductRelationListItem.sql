CREATE TABLE [product].[tBlockProductRelationListItem]
(
[BlockId] [int] NOT NULL,
[RelationListTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductRelationListItem] ADD CONSTRAINT [PK_tBlockProductRelationListItem] PRIMARY KEY CLUSTERED  ([BlockId], [RelationListTypeId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductRelationListItem] ADD CONSTRAINT [FK_tBlockProductRelationListItem_tBlockProductRelationList] FOREIGN KEY ([BlockId]) REFERENCES [product].[tBlockProductRelationList] ([BlockId])
GO
ALTER TABLE [product].[tBlockProductRelationListItem] ADD CONSTRAINT [FK_tBlockProductRelationListItem_tRelationListType] FOREIGN KEY ([RelationListTypeId]) REFERENCES [product].[tRelationListType] ([RelationListTypeId])
GO
