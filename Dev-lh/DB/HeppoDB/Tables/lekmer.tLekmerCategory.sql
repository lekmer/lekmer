CREATE TABLE [lekmer].[tLekmerCategory]
(
[CategoryId] [int] NOT NULL,
[AllowMultipleSizesPurchase] [bit] NULL CONSTRAINT [DF_tLekmerCategory_AllowMultipleSizesPurchase] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD CONSTRAINT [PK_tLekmerCategory] PRIMARY KEY CLUSTERED  ([CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD CONSTRAINT [FK_tLekmerCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
