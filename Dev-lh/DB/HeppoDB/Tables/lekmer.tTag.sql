CREATE TABLE [lekmer].[tTag]
(
[TagId] [int] NOT NULL IDENTITY(1, 1),
[TagGroupId] [int] NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[CommonName] [varchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tTag_TagGroupId_CommonName] ON [lekmer].[tTag] ([TagGroupId], [CommonName]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tTag_Value] ON [lekmer].[tTag] ([Value]) ON [PRIMARY]

GO
ALTER TABLE [lekmer].[tTag] ADD CONSTRAINT [PK_tTag] PRIMARY KEY CLUSTERED  ([TagId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tTag_TagGroupId] ON [lekmer].[tTag] ([TagGroupId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTag] ADD CONSTRAINT [FK_tTag_tTagGroup] FOREIGN KEY ([TagGroupId]) REFERENCES [lekmer].[tTagGroup] ([TagGroupId])
GO
