CREATE TABLE [productlek].[tColor]
(
[ColorId] [int] NOT NULL IDENTITY(1, 1),
[HyErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tColor] ADD CONSTRAINT [PK_tColor] PRIMARY KEY CLUSTERED  ([ColorId]) ON [PRIMARY]
GO
