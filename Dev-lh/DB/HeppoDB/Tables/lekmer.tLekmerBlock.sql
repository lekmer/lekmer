CREATE TABLE [lekmer].[tLekmerBlock]
(
[BlockId] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[StartDailyIntervalMinutes] [int] NULL,
[EndDailyIntervalMinutes] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerBlock] ADD CONSTRAINT [PK_tLekmerBlock] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerBlock] ADD CONSTRAINT [FK_tLekmerBlock_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]) ON DELETE CASCADE
GO
