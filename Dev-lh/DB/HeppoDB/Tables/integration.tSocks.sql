CREATE TABLE [integration].[tSocks]
(
[SocksHYId] [int] NOT NULL,
[SocksHYText] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tSocks] ADD CONSTRAINT [PK__tSocks__76FF6B971078CCC7] PRIMARY KEY CLUSTERED  ([SocksHYId]) ON [PRIMARY]
GO
