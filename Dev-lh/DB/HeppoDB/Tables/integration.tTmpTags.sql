CREATE TABLE [integration].[tTmpTags]
(
[Tagid] [int] NOT NULL,
[Value] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tTmpTags] ADD CONSTRAINT [PK__tTmpTags__657DE5C433A9865A] PRIMARY KEY CLUSTERED  ([Tagid]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
