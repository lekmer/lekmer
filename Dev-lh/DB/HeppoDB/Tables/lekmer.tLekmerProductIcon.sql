CREATE TABLE [lekmer].[tLekmerProductIcon]
(
[ProductId] [int] NOT NULL,
[IconId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD
CONSTRAINT [FK_tLekmerProductIcon_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD
CONSTRAINT [FK_tLekmerProductIcon_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tLekmerProductIcon] ADD CONSTRAINT [PK_tLekmerProductIcon] PRIMARY KEY CLUSTERED  ([ProductId], [IconId]) ON [PRIMARY]
GO
