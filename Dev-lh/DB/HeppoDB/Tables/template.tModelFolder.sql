CREATE TABLE [template].[tModelFolder]
(
[ModelFolderId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFolder] ADD CONSTRAINT [PK_tModelFolder] PRIMARY KEY CLUSTERED  ([ModelFolderId]) ON [PRIMARY]
GO
