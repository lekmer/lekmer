CREATE TABLE [campaignlek].[tProductActionTargetProductType]
(
[ProductActionId] [int] NOT NULL,
[ProductTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tProductActionTargetProductType] ADD CONSTRAINT [PK_tProductActionTargetProductType] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductTypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tProductActionTargetProductType] ADD CONSTRAINT [FK_tProductActionTargetProductType_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
ALTER TABLE [campaignlek].[tProductActionTargetProductType] ADD CONSTRAINT [FK_tProductActionTargetProductType_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])
GO
