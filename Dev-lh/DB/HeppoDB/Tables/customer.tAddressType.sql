CREATE TABLE [customer].[tAddressType]
(
[AddressTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tAddressType] ADD CONSTRAINT [PK_tAddressType] PRIMARY KEY CLUSTERED  ([AddressTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_CommonName] ON [customer].[tAddressType] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Title] ON [customer].[tAddressType] ([Title]) ON [PRIMARY]
GO
