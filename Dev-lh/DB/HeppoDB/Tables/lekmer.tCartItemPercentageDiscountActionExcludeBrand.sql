CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeBrand_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
