CREATE TABLE [product].[tProductRegistryProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductId_(ProductRegistryId)] ON [product].[tProductRegistryProduct] ([ProductId]) INCLUDE ([ProductRegistryId])

CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductRegistryId] ON [product].[tProductRegistryProduct] ([ProductRegistryId])

CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct_ProductRegistryId_ProductId] ON [product].[tProductRegistryProduct] ([ProductRegistryId], [ProductId])

GO
ALTER TABLE [product].[tProductRegistryProduct] ADD CONSTRAINT [PK_tProductRegistryProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId]) ON [PRIMARY]
GO

ALTER TABLE [product].[tProductRegistryProduct] ADD CONSTRAINT [FK_tProductRegistryProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tProductRegistryProduct] ADD CONSTRAINT [FK_tProductRegistryProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
