CREATE TABLE [order].[tOrderProductCampaign]
(
[OrderProductCampaignId] [int] NOT NULL IDENTITY(1, 1),
[CampaignId] [int] NOT NULL,
[OrderItemId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderProductCampaign] ADD CONSTRAINT [PK_tOrderProductCampaign] PRIMARY KEY CLUSTERED  ([OrderProductCampaignId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderProductCampaign_OrderItemId] ON [order].[tOrderProductCampaign] ([OrderItemId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderProductCampaign] ADD CONSTRAINT [FK_tOrderProductCampaign_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
GO
