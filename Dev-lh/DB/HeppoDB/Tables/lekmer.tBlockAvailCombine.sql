CREATE TABLE [lekmer].[tBlockAvailCombine]
(
[BlockId] [int] NOT NULL,
[UseCartPredictions] [bit] NOT NULL,
[UseClickStreamPredictions] [bit] NOT NULL,
[UseLogPurchase] [bit] NOT NULL,
[UsePersonalPredictions] [bit] NOT NULL,
[UseProductSearchPredictions] [bit] NOT NULL,
[UseProductsPredictions] [bit] NOT NULL,
[UseProductsPredictionsFromClicksCategory] [bit] NOT NULL,
[UseProductsPredictionsFromClicksProduct] [bit] NOT NULL,
[UseLogClickedOn] [bit] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[GetClickHistoryFromCookie] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockAvailCombine] ADD CONSTRAINT [PK_BlockAvailCombine] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockAvailCombine] ADD CONSTRAINT [FK_BlockAvailCombine_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
