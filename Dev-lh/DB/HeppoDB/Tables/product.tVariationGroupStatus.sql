CREATE TABLE [product].[tVariationGroupStatus]
(
[VariationGroupStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationGroupStatus] ADD CONSTRAINT [PK_tVariationGroupStatus] PRIMARY KEY CLUSTERED  ([VariationGroupStatusId]) ON [PRIMARY]
GO
