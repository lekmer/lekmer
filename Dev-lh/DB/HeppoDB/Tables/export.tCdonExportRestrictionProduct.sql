CREATE TABLE [export].[tCdonExportRestrictionProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportRestrictionProduct] ADD CONSTRAINT [PK_tCdonExportRestrictionProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportRestrictionProduct] ADD CONSTRAINT [FK_tCdonExportRestrictionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [export].[tCdonExportRestrictionProduct] ADD CONSTRAINT [FK_tCdonExportRestrictionProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
