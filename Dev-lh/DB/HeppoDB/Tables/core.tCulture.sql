CREATE TABLE [core].[tCulture]
(
[CultureId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [core].[tCulture] ADD CONSTRAINT [PK_tCulture] PRIMARY KEY CLUSTERED  ([CultureId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCulture_CommonName] ON [core].[tCulture] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCulture_Title] ON [core].[tCulture] ([Title]) ON [PRIMARY]
GO
