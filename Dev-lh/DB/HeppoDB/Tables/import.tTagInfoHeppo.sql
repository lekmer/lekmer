CREATE TABLE [import].[tTagInfoHeppo]
(
[TagGroup.Id] [int] NOT NULL,
[TagGroup.CommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TagGroup.Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Tag.Id] [int] NOT NULL,
[Tag.Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ValueSE] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ValueDK] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ValueNO] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ValueFI] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ValueNL] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ValueFR] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.CommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[Alias.ID] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
