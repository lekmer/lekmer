CREATE TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeCategory_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
