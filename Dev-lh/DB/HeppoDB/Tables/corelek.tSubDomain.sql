CREATE TABLE [corelek].[tSubDomain]
(
[SubDomainId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DomainUrl] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UrlTypeId] [int] NOT NULL,
[ContentTypeId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [corelek].[tSubDomain] ADD
CONSTRAINT [FK_tSubDomain_tSubDomainUrlType] FOREIGN KEY ([UrlTypeId]) REFERENCES [corelek].[tSubDomainUrlType] ([SubDomainUrlTypeId])
GO
ALTER TABLE [corelek].[tSubDomain] ADD CONSTRAINT [PK_tSubDomain] PRIMARY KEY CLUSTERED  ([SubDomainId]) ON [PRIMARY]
GO
ALTER TABLE [corelek].[tSubDomain] ADD CONSTRAINT [FK_tSubDomain_tSubDomain] FOREIGN KEY ([ContentTypeId]) REFERENCES [corelek].[tSubDomainContentType] ([SubDomainContentTypeId])
GO
