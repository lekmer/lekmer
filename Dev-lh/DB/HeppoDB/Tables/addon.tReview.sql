CREATE TABLE [addon].[tReview]
(
[ReviewId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[ReviewStatusId] [int] NOT NULL,
[CustomerId] [int] NULL,
[Rating] [tinyint] NOT NULL,
[Message] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Author] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ChannelId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tReview] ADD CONSTRAINT [PK_tReview] PRIMARY KEY CLUSTERED  ([ReviewId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tReview_CreatedDate] ON [addon].[tReview] ([CreatedDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tReview_CustomerId] ON [addon].[tReview] ([CustomerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tReview_ProductId] ON [addon].[tReview] ([ProductId]) INCLUDE ([Author], [CreatedDate], [CustomerId], [Message], [Rating], [ReviewId], [ReviewStatusId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tReview_ReviewStatusId] ON [addon].[tReview] ([ProductId], [ReviewStatusId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tReview] WITH NOCHECK ADD CONSTRAINT [FK_tReview_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tReview] WITH NOCHECK ADD CONSTRAINT [FK_tReview_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tReview] WITH NOCHECK ADD CONSTRAINT [FK_tReview_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tReview] WITH NOCHECK ADD CONSTRAINT [FK_tReview_tReviewStatus] FOREIGN KEY ([ReviewStatusId]) REFERENCES [addon].[tReviewStatus] ([StatusId])
GO
ALTER TABLE [addon].[tReview] NOCHECK CONSTRAINT [FK_tReview_tChannel]
GO
ALTER TABLE [addon].[tReview] NOCHECK CONSTRAINT [FK_tReview_tCustomer]
GO
ALTER TABLE [addon].[tReview] NOCHECK CONSTRAINT [FK_tReview_tProduct]
GO
ALTER TABLE [addon].[tReview] NOCHECK CONSTRAINT [FK_tReview_tReviewStatus]
GO
