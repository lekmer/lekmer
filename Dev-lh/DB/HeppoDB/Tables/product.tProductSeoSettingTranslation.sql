CREATE TABLE [product].[tProductSeoSettingTranslation]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Keywords] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSeoSettingTranslation] ADD CONSTRAINT [PK_tProductSeoSettingsTranslation] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSeoSettingTranslation] ADD CONSTRAINT [FK_tProductSeoSettingsTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [product].[tProductSeoSettingTranslation] ADD CONSTRAINT [FK_tProductSeoSettingsTranslation_tProductSeoSetting] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProductSeoSetting] ([ProductId])
GO
