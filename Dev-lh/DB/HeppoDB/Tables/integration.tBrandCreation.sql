CREATE TABLE [integration].[tBrandCreation]
(
[BrandId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tBrandCreation] ADD 
CONSTRAINT [PK_tBrandCreation] PRIMARY KEY CLUSTERED  ([BrandId]) ON [PRIMARY]
GO
