CREATE TABLE [addon].[tCartContainsConditionIncludeCategory]
(
[ConditionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeCategory] ADD CONSTRAINT [PK_tCartContainsConditionIncludeCategory] PRIMARY KEY CLUSTERED  ([ConditionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeCategory] ADD CONSTRAINT [FK_tCartContainsConditionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tCartContainsConditionIncludeCategory] ADD CONSTRAINT [FK_tCartContainsConditionIncludeCategory_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId])
GO
