CREATE TABLE [lekmer].[tCampaignFolderFlag]
(
[FolderId] [int] NOT NULL,
[FlagId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCampaignFolderFlag] ADD CONSTRAINT [PK_tCampaignFolderFlag] PRIMARY KEY CLUSTERED  ([FolderId], [FlagId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCampaignFolderFlag] ADD CONSTRAINT [FK_tCampaignFolderFlag_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCampaignFolderFlag] ADD CONSTRAINT [FK_tCampaignFolderFlag_tCampaignFolder] FOREIGN KEY ([FolderId]) REFERENCES [campaign].[tCampaignFolder] ([FolderId]) ON DELETE CASCADE
GO
