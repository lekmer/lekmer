CREATE TABLE [product].[tCategorySiteStructureRegistry]
(
[CategoryId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NOT NULL,
[ProductParentContentNodeId] [int] NULL,
[ProductTemplateContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tCategorySiteStructureRegistry] ADD CONSTRAINT [PK_tCategorySiteStructureRegistry] PRIMARY KEY CLUSTERED  ([CategoryId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tCategorySiteStructureRegistry] ADD CONSTRAINT [FK_tCategorySiteStructureRegistry_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [product].[tCategorySiteStructureRegistry] ADD CONSTRAINT [FK_tCategorySiteStructureRegistry_tContentPage_Parent] FOREIGN KEY ([ProductParentContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId], [SiteStructureRegistryId])
GO
ALTER TABLE [product].[tCategorySiteStructureRegistry] ADD CONSTRAINT [FK_tCategorySiteStructureRegistry_tContentPage_ProductTemplate] FOREIGN KEY ([ProductTemplateContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId], [SiteStructureRegistryId])
GO
ALTER TABLE [product].[tCategorySiteStructureRegistry] ADD CONSTRAINT [FK_tCategorySiteStructureRegistry_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
