CREATE TABLE [order].[tDeliveryMethod]
(
[DeliveryMethodId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[TrackingUrl] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tDeliveryMethod] ADD CONSTRAINT [PK_DeliveryMethod] PRIMARY KEY CLUSTERED  ([DeliveryMethodId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tDeliveryMethod] ADD CONSTRAINT [UQ_tDeliveryMethod_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
ALTER TABLE [order].[tDeliveryMethod] ADD CONSTRAINT [UQ_tDeliveryMethod_Title] UNIQUE NONCLUSTERED  ([Title]) ON [PRIMARY]
GO
