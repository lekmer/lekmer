CREATE TABLE [template].[tThemeModel]
(
[ThemeId] [int] NOT NULL,
[ModelId] [int] NOT NULL,
[TemplateId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tThemeModel] ADD CONSTRAINT [PK_tThemeModel] PRIMARY KEY CLUSTERED  ([ThemeId], [ModelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tThemeModel_TemplateId] ON [template].[tThemeModel] ([TemplateId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tThemeModel] ADD CONSTRAINT [FK_tThemeModel_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
ALTER TABLE [template].[tThemeModel] ADD CONSTRAINT [FK_tThemeModel_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
ALTER TABLE [template].[tThemeModel] ADD CONSTRAINT [FK_tThemeModel_tTheme] FOREIGN KEY ([ThemeId]) REFERENCES [template].[tTheme] ([ThemeId])
GO
