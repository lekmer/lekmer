CREATE TABLE [orderlek].[tOrderVoucherInfo]
(
[OrderId] [int] NOT NULL,
[VoucherCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[DiscountType] [int] NULL,
[DiscountValue] [decimal] (16, 2) NULL,
[AmountUsed] [decimal] (16, 2) NULL,
[AmountLeft] [decimal] (16, 2) NULL,
[VoucherStatus] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tOrderVoucherInfo] ADD CONSTRAINT [PK_tOrderVoucherInfo] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tOrderVoucherInfo] ADD CONSTRAINT [FK_tOrderVoucherInfo_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
