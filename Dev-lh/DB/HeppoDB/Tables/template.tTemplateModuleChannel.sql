CREATE TABLE [template].[tTemplateModuleChannel]
(
[ChannelId] [int] NOT NULL,
[ThemeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplateModuleChannel] ADD CONSTRAINT [PK_tTemplateModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tTemplateModuleChannel_ThemeId] ON [template].[tTemplateModuleChannel] ([ThemeId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplateModuleChannel] ADD CONSTRAINT [FK_tTemplateModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [template].[tTemplateModuleChannel] ADD CONSTRAINT [FK_tTemplateModuleChannel_tTheme] FOREIGN KEY ([ThemeId]) REFERENCES [template].[tTheme] ([ThemeId])
GO
