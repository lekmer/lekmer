CREATE TABLE [template].[tComponentParameter]
(
[ComponentParameterId] [int] NOT NULL IDENTITY(1, 1),
[ComponentId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DataType] [int] NOT NULL,
[Description] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tComponentParameter] ADD CONSTRAINT [PK_tComponentParameter] PRIMARY KEY CLUSTERED  ([ComponentParameterId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tComponentParameter_ComponentId] ON [template].[tComponentParameter] ([ComponentId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tComponentParameter] ADD CONSTRAINT [FK_tComponentParameter_tComponent] FOREIGN KEY ([ComponentId]) REFERENCES [template].[tComponent] ([ComponentId])
GO
