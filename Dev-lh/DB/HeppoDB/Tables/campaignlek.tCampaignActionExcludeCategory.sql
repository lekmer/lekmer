CREATE TABLE [campaignlek].[tCampaignActionExcludeCategory]
(
[ConfigId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubCategories] [bit] NOT NULL CONSTRAINT [DF_tCampaignActionExcludeCategory_IncludeSubCategories] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeCategory] ADD CONSTRAINT [PK_tCampaignActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ConfigId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeCategory] ADD CONSTRAINT [FK_tCampaignActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeCategory] ADD CONSTRAINT [FK_tCampaignActionExcludeCategory_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
