CREATE TABLE [addon].[tCartItemPriceActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemPriceActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeProduct] ADD CONSTRAINT [FK_tCartItemPriceActionProduct_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId])
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeProduct] ADD CONSTRAINT [FK_tCartItemPriceActionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
