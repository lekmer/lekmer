CREATE TABLE [integration].[integrationLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Data] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL,
[OcuredInProcedure] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [integration].[integrationLog] ADD 
CONSTRAINT [PK_integrationLog] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
