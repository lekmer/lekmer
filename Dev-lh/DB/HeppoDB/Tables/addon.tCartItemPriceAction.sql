CREATE TABLE [addon].[tCartItemPriceAction]
(
[CartActionId] [int] NOT NULL,
[MaxQuantity] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tCartItemPriceAction_IncludeAllProducts] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceAction] ADD CONSTRAINT [PK_tCartItemPriceAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceAction] ADD CONSTRAINT [FK_tCartItemPriceAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
