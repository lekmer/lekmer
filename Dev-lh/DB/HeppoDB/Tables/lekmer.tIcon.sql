CREATE TABLE [lekmer].[tIcon]
(
[IconId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[MediaId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [lekmer].[tIcon] ADD 
CONSTRAINT [PK_tIcon] PRIMARY KEY CLUSTERED  ([IconId]) ON [PRIMARY]
ALTER TABLE [lekmer].[tIcon] ADD
CONSTRAINT [FK_tIcon_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId])
GO
