CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeCategory_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
