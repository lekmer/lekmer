CREATE TABLE [campaign].[tFreightValueAction]
(
[CartActionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tFreightValueAction] ADD CONSTRAINT [PK_tFreightValueAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tFreightValueAction] ADD CONSTRAINT [FK_tFreightValueAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
