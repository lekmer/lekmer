CREATE TABLE [order].[tChannelPaymentType]
(
[ChannelId] [int] NOT NULL,
[PaymentTypeId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [order].[tChannelPaymentType] ADD
CONSTRAINT [FK_tChannelPaymentType_tPaymentType] FOREIGN KEY ([PaymentTypeId]) REFERENCES [order].[tPaymentType] ([PaymentTypeId])
GO
ALTER TABLE [order].[tChannelPaymentType] ADD CONSTRAINT [PK_ChannelPaymentType] PRIMARY KEY CLUSTERED  ([ChannelId], [PaymentTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tChannelPaymentType] ON [order].[tChannelPaymentType] ([ChannelId], [PaymentTypeId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tChannelPaymentType] ADD CONSTRAINT [FK_tChannelPaymentType_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
