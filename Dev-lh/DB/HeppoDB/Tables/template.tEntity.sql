CREATE TABLE [template].[tEntity]
(
[EntityId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tEntity] ADD CONSTRAINT [PK_tEntity] PRIMARY KEY CLUSTERED  ([EntityId]) ON [PRIMARY]
GO
