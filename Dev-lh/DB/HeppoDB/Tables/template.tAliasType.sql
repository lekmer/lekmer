CREATE TABLE [template].[tAliasType]
(
[AliasTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tAliasType] ADD CONSTRAINT [PK_tAliasType] PRIMARY KEY CLUSTERED  ([AliasTypeId]) ON [PRIMARY]
GO
