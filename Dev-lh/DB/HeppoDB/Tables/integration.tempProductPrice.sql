CREATE TABLE [integration].[tempProductPrice]
(
[HYarticleId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Price] [decimal] (18, 0) NULL,
[PriceText] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceGroup] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tempProductPrice] ADD 
CONSTRAINT [PK_tempProductPrice] PRIMARY KEY CLUSTERED  ([HYarticleId]) ON [PRIMARY]
GO
