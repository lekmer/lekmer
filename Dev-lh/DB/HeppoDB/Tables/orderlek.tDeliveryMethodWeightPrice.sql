CREATE TABLE [orderlek].[tDeliveryMethodWeightPrice]
(
[CountryId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL,
[WeightFrom] [decimal] (18, 3) NULL,
[WeightTo] [decimal] (18, 3) NULL,
[FreightCost] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tDeliveryMethodWeightPrice] ADD CONSTRAINT [FK_tDeliveryMethodWeightPrice_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
GO
ALTER TABLE [orderlek].[tDeliveryMethodWeightPrice] ADD CONSTRAINT [FK_tDeliveryMethodWeightPrice_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [orderlek].[tDeliveryMethodWeightPrice] ADD CONSTRAINT [FK_tDeliveryMethodWeightPrice_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
