CREATE TABLE [lekmer].[tProductTag]
(
[ProductId] [int] NOT NULL,
[TagId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductTag] ADD CONSTRAINT [PK_tProductTag] PRIMARY KEY CLUSTERED  ([ProductId], [TagId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductTag] ADD CONSTRAINT [FK_tProductTag_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [lekmer].[tProductTag] ADD CONSTRAINT [FK_tProductTag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
