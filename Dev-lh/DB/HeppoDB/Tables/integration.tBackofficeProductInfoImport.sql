CREATE TABLE [integration].[tBackofficeProductInfoImport]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[HYErpId] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DescriptionSE] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[DescriptionDK] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[DescriptionNO] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[DescriptionFI] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[TagIdCollection] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[SizeDeviationId] [int] NULL,
[UserName] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[InsertedDate] [smalldatetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tBackofficeProductInfoImport] ADD CONSTRAINT [PK_tBackofficeProductInfoImport] PRIMARY KEY CLUSTERED  ([Id])
GO
