CREATE TABLE [campaignlek].[tCartItemDiscountCartAction]
(
[CartActionId] [int] NOT NULL,
[Quantity] [int] NOT NULL,
[ApplyForCheapest] [bit] NOT NULL,
[IsPercentageDiscount] [bit] NOT NULL,
[PercentageDiscountAmount] [decimal] (16, 2) NULL,
[IncludeAllProducts] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartAction] ADD CONSTRAINT [PK_tCartItemDiscountCartAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartAction] ADD CONSTRAINT [FK_tCartItemDiscountCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
