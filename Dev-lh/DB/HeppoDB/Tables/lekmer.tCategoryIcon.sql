CREATE TABLE [lekmer].[tCategoryIcon]
(
[CategoryId] [int] NOT NULL,
[IconId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCategoryIcon] ADD CONSTRAINT [PK_tCategoryIcon] PRIMARY KEY CLUSTERED  ([CategoryId], [IconId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCategoryIcon] ADD CONSTRAINT [FK_tCategoryIcon_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCategoryIcon] ADD CONSTRAINT [FK_tCategoryIcon_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
GO
