CREATE TABLE [lekmer].[tTagTranslation]
(
[TagId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTagTranslation] ADD CONSTRAINT [PK_tTagTranslation] PRIMARY KEY CLUSTERED  ([TagId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTagTranslation] ADD CONSTRAINT [FK_tTagTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [lekmer].[tTagTranslation] ADD CONSTRAINT [FK_tTagTranslation_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
