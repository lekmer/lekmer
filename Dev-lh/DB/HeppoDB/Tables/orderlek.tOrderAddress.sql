CREATE TABLE [orderlek].[tOrderAddress]
(
[OrderAddressId] [int] NOT NULL,
[HouseNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[HouseExtension] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[Reference] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tOrderAddress] ADD CONSTRAINT [PK_tOrderAddress] PRIMARY KEY CLUSTERED  ([OrderAddressId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tOrderAddress] ADD CONSTRAINT [FK_tOrderAddress(lek)_tOrderAddress] FOREIGN KEY ([OrderAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId])
GO
