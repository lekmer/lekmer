CREATE TABLE [sitestructure].[tBlockRichTextTranslation]
(
[BlockId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Content] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockRichTextTranslation] ADD CONSTRAINT [PK_tBlockRichTextTranslation] PRIMARY KEY CLUSTERED  ([BlockId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockRichTextTranslation] ADD CONSTRAINT [FK_tBlockRichTextTranslation_tBlockRichText] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlockRichText] ([BlockId])
GO
ALTER TABLE [sitestructure].[tBlockRichTextTranslation] ADD CONSTRAINT [FK_tBlockRichTextTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
