SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vColor]
AS
SELECT     
      [c].[ColorId] AS 'Color.ColorId', 
      [c].[HyErpId] AS 'Color.HyErpId', 
      COALESCE ([ct].[Value], [c].[Value]) AS 'Color.Value',
      [c].[CommonName] AS 'Color.CommonName',
      [l].[LanguageId] AS 'Color.LanguageId'
FROM
      [productlek].[tColor] AS c
      CROSS JOIN [core].[tLanguage] AS l
      LEFT JOIN [productlek].[tColorTranslation] AS ct ON ct.[ColorId] = [c].[ColorId] AND [ct].[LanguageId] = [l].[LanguageId]
GO
