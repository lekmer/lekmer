SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [sitestructure].[vContentNodeSecure]
AS
SELECT     
	cn.ContentNodeId AS 'ContentNode.ContentNodeId', 
	cn.Title AS 'ContentNode.Title', 
	cn.ParentContentNodeId AS 'ContentNode.ParentContentNodeId', 
	cn.ContentNodeTypeId AS 'ContentNodeType.ContentNodeTypeId', 
	cn.ContentNodeStatusId AS 'ContentNode.ContentNodeStatusId', 
	cn.Ordinal AS 'ContentNode.Ordinal', 
	cn.CommonName AS 'ContentNode.CommonName',
	cn.AccessId AS 'ContentNode.AccessId', 
	cn.SiteStructureRegistryId AS 'ContentNode.SiteStructureRegistryId', 
	t.Title AS 'ContentNodeType.Title', 
    t.CommonName AS 'ContentNodeType.CommonName'
FROM
	sitestructure.tContentNode AS cn        
	INNER JOIN sitestructure.tContentNodeType AS t ON t.[ContentNodeTypeId] = cn.ContentNodeTypeId
GO
