SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create VIEW [product].[vProductStatus]
AS
SELECT
	[ProductStatusId] 'ProductStatus.Id',
	[Title] 'ProductStatus.Title',
	[CommonName] 'ProductStatus.CommonName'
FROM
	product.tProductStatus
GO
