SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vProductModuleChannel]
AS
SELECT     
	PMC.ChannelId 'ProductModuleChannel.ChannelId',
	PMC.ProductTemplateContentNodeId 'ProductModuleChannel.ProductTemplateContentNodeId'
FROM
	product.tProductModuleChannel PMC
GO
