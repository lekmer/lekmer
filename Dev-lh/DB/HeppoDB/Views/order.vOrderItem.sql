
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vOrderItem]
AS
SELECT
	OI.OrderItemId AS 'OrderItem.OrderItemId',
	OI.OrderId AS 'OrderItem.OrderId',
	OI.Quantity AS 'OrderItem.Quantity',
	OI.OriginalPriceIncludingVat AS 'OrderItem.OriginalPriceIncludingVat',
	OI.ActualPriceIncludingVat AS 'OrderItem.ActualPriceIncludingVat',
	OI.VAT AS 'OrderItem.VAT',
	OIP.ProductId AS 'OrderItem.ProductId',
	OIP.ErpId AS 'OrderItem.ErpId',
	OIP.EanCode AS 'OrderItem.EanCode',
	OIP.Title AS 'OrderItem.Title',
	[OIP].[ProductTypeId] AS 'OrderItem.ProductTypeId',
	s.*
FROM 
	[order].[tOrderItem] OI
	INNER JOIN [order].[tOrderItemProduct] OIP ON OI.OrderItemId = OIP.OrderItemId
	INNER JOIN [order].vCustomOrderItemStatus s ON oi.OrderItemStatusId = s.[OrderItemStatus.Id]
GO
