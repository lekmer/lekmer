SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [lekmer].[vProductTag]
as
	select
		ProductId 'ProductTag.ProductId',
		TagId 'ProductTag.TagId'
	from
		lekmer.tProductTag
GO
