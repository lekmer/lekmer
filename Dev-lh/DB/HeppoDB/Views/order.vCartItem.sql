SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCartItem]
AS
SELECT 
		C.CartItemId AS 'CartItem.CartItemId',
		C.CartId AS 'CartItem.CartId',
		C.Quantity AS 'CartItem.Quantity', 
		C.CreatedDate AS 'CartItem.CreatedDate',
		P.*		
from
	[order].tCartItem C WITH (NOLOCK)
	inner join product.vCustomProduct P on C.ProductId = P.[Product.Id]
GO
