SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vStore]
AS
SELECT 
	t.StoreId 'Store.Id',
	t.ErpId 'Store.ErpId',
	t.Description 'Store.Description',
	t.StatusId 'Store.StatusId',
	t.Title 'Store.Title'
FROM
	product.tStore t
GO
