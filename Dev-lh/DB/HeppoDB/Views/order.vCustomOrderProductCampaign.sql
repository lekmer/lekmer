SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [order].[vCustomOrderProductCampaign]
as
	select
		*
	from
		[order].[vOrderProductCampaign]

GO
