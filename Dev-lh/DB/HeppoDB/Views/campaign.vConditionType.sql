SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view campaign.vConditionType
as
	select
		ConditionTypeId as 'ConditionType.Id',
		Title as 'ConditionType.Title',
		CommonName as 'ConditionType.CommonName'
	from
		campaign.tConditionType
GO
