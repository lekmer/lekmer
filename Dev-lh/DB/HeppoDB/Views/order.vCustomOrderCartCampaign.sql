SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [order].[vCustomOrderCartCampaign]
as
	select
		*
	from
		[order].[vOrderCartCampaign]

GO
