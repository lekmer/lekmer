SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vCategory]
AS
SELECT     
	c.CategoryId 'Category.Id',
	c.ParentCategoryId 'Category.ParentCategoryId',
	COALESCE (ct.Title, c.Title) 'Category.Title', 
	c.ErpId 'Category.ErpId',
	t.LanguageId 'Category.LanguageId'
FROM
	product.tCategory AS c CROSS JOIN
	core.tLanguage AS t	LEFT JOIN 
	product.tCategoryTranslation AS ct ON ct.CategoryId = c.CategoryId AND ct.LanguageId = t.LanguageId
GO
