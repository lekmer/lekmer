SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vBlockProductRelationList]
AS
	SELECT 
		bprl.[BlockId] 'BlockProductRelationList.BlockId',
		bprl.[ColumnCount] 'BlockProductRelationList.ColumnCount',
		bprl.[RowCount] 'BlockProductRelationList.RowCount',
		pso.*
	FROM 
		[product].[tBlockProductRelationList] as bprl
		inner join product.vCustomProductSortOrder as pso on pso.[ProductSortOrder.Id] = bprl.ProductSortOrderId

GO
