SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [template].[vAliasSecure]
AS
SELECT     
	   [AliasId] AS 'Alias.AliasId'
      ,[AliasFolderId] AS 'Alias.AliasFolderId'
      ,[Description] AS 'Alias.Description'
      ,[CommonName] AS 'Alias.CommonName'
      ,[AliasTypeId] AS 'Alias.AliasTypeId'
      ,[Value] AS 'Alias.Value'
FROM         
    template.tAlias

GO
