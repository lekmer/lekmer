SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingReviewFeedback]
AS
	SELECT
		rrf.[RatingReviewFeedbackId] 'RatingReviewFeedback.RatingReviewFeedbackId',
		rrf.[ChannelId]				 'RatingReviewFeedback.ChannelId',
		rrf.[ProductId]				 'RatingReviewFeedback.ProductId',
		rrf.[OrderId]				 'RatingReviewFeedback.OrderId',
		rrf.[RatingReviewStatusId]	 'RatingReviewFeedback.RatingReviewStatusId',
		rrf.[LikeHit]				 'RatingReviewFeedback.LikeHit',
		rrf.[Impropriate]			 'RatingReviewFeedback.Impropriate',
		rrf.[IPAddress]				 'RatingReviewFeedback.IPAddress',
		rrf.[CreatedDate] 			 'RatingReviewFeedback.CreatedDate',
		rrf.[RatingReviewUserId]	 'RatingReviewFeedback.RatingReviewUserId'
	FROM 
		[review].[tRatingReviewFeedback] rrf
GO
