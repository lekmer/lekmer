
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vFilterProduct]
AS
	SELECT
		p.[ProductId] 'FilterProduct.ProductId',
		p.[CategoryId] 'FilterProduct.CategoryId',
		COALESCE(pt.[Title], p.[Title]) 'FilterProduct.Title',
		lp.[BrandId] 'FilterProduct.BrandId',
		lp.[AgeFromMonth] 'FilterProduct.AgeFromMonth',
		lp.[AgeToMonth] 'FilterProduct.AgeToMonth',
		lp.[IsNewFrom] 'FilterProduct.IsNewFrom',
		ch.[ChannelId] 'FilterProduct.ChannelId',
		ch.[CurrencyId] 'FilterProduct.CurrencyId',
		pmc.[PriceListRegistryId] 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity',
		CASE 
			WHEN EXISTS ( SELECT 1 FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] )
			THEN (SELECT ISNULL(SUM(ps.[NumberInStock]), 0) FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId] AND ps.[NumberInStock] >= 0)
			ELSE p.[NumberInStock]
		END AS 'FilterProduct.NumberInStock',
		lp.[ProductTypeId] AS 'FilterProduct.ProductTypeId'
	FROM
		[product].[tProduct] p
		INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]

		INNER JOIN [product].[tProductRegistryProduct] AS prp ON p.[ProductId] = prp.[ProductId]
		INNER JOIN [product].[tProductModuleChannel] AS pmc ON prp.[ProductRegistryId] = pmc.[ProductRegistryId]
		INNER JOIN [core].[tChannel] AS ch ON pmc.[ChannelId] = ch.[ChannelId]

		LEFT JOIN [product].[tProductTranslation] AS pt ON p.[ProductId] = pt.[ProductId] AND ch.[LanguageId] = pt.[LanguageId]
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.[IsDeleted] = 0
		AND p.[ProductStatusId] = 0

GO
