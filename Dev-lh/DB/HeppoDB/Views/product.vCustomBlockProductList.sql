SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockProductList]
as
	select
		*
	from
		[product].[vBlockProductList]
GO
