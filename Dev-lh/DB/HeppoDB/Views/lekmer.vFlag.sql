SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vFlag]
AS
SELECT     
      f.FlagId AS 'Flag.Id',
      COALESCE (ft.Title, f.Title) AS 'Flag.Title',
      f.Class AS 'Flag.Class', 
      f.Ordinal AS 'Flag.Ordinal',
      l.LanguageId AS 'Flag.LanguageId'
FROM
      lekmer.tFlag AS f
      CROSS JOIN core.tLanguage AS l
      LEFT JOIN lekmer.tFlagTranslation AS ft ON ft.FlagId = f.FlagId AND ft.LanguageId = l.LanguageId

GO
