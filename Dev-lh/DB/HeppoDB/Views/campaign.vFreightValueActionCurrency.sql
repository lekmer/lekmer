SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vFreightValueActionCurrency]
as
	select
		fv.CartActionId AS 'FreightValueAction.ActionId',
		fv.CurrencyId AS 'FreightValueAction.CurrencyId',
		fv.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		campaign.tFreightValueActionCurrency fv
		inner join core.vCustomCurrency c on fv.CurrencyId = c.[Currency.Id]

GO
