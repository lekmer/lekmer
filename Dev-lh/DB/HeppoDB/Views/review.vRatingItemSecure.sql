SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingItemSecure]
AS
	SELECT
		ri.[RatingItemId]	'RatingItem.RatingItemId',
		ri.[RatingId]		'RatingItem.RatingId',
		ri.[Title]			'RatingItem.Title',
		ri.[Description]	'RatingItem.Description',
		ri.[Score]			'RatingItem.Score'
	FROM 
		[review].[tRatingItem] ri
GO
