SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [template].[vCustomAlias]
as
	select
		*
	from
		[template].[vAlias]
GO
