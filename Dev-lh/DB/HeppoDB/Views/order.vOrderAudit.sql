SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vOrderAudit]
AS
SELECT 
	OA.OrderAuditId AS 'OrderAudit.OrderAuditId',
	OA.OrderId AS 'OrderAudit.OrderId',
	OA.SystemUserId AS 'OrderAudit.SystemUserId',
	OA.OrderAuditTypeId AS 'OrderAuditType.OrderAuditTypeId',
	OA.Information AS 'OrderAudit.Information',
	OA.Message AS 'OrderAudit.Message',
	OA.CreatedDate AS 'OrderAudit.CreatedDate',
	OAT.Title AS 'OrderAuditType.Title',
	OAT.CommonName AS 'OrderAuditType.CommonName'
  FROM [order].[tOrderAudit] OA
  LEFT JOIN [order].[tOrderAuditType] OAT ON OAT.OrderAuditTypeId = OA.OrderAuditTypeId

GO
