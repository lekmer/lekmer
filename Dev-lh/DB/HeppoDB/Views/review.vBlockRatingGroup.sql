SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vBlockRatingGroup]
AS
	SELECT
		[BlockId] AS 'BlockRatingGroup.BlockId',
		[RatingGroupId] AS 'BlockRatingGroup.RatingGroupId'
	FROM
		[review].[tBlockRatingGroup]
GO
