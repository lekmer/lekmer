SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vProductSortOrder]
AS
SELECT 
	[ProductSortOrderId] AS  'ProductSortOrder.Id',
	[Title]  AS  'ProductSortOrder.Title',
	[CommonName]  AS  'ProductSortOrder.CommonName',
	[SortByColumn] AS 'ProductSortOrder.SortByColumn',
	[SortDescending] AS 'ProductSortOrder.SortDescending'
  FROM [product].[tProductSortOrder]
GO
