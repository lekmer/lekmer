SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [customer].[vAddress]
AS
select
		AddressId AS 'Address.AddressId',
		CustomerId AS 'Address.CustomerId',
		Addressee AS 'Address.Addressee',
		StreetAddress AS 'Address.StreetAddress',
		StreetAddress2 AS 'Address.StreetAddress2',
		PostalCode AS 'Address.PostalCode',
		City AS 'Address.City',
		CountryId AS 'Address.CountryId',
		PhoneNumber AS 'Address.PhoneNumber',
		AddressTypeId AS 'Address.AddressTypeId'
from
	[customer].[tAddress]
GO
