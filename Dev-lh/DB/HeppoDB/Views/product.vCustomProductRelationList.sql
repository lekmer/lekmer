SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductRelationList]
as
	select
		*
	from
		[product].[vProductRelationList]
GO
