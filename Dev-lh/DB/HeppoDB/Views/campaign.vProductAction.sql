SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vProductAction]
as
	select
		ProductActionId as 'ProductAction.Id',
		ProductActionTypeId as 'ProductAction.ProductActionTypeId',
		CampaignId as 'ProductAction.CampaignId',
		Ordinal as 'ProductAction.Ordinal',
		t.*
	from
		campaign.tProductAction pa
		inner join campaign.vCustomProductActionType t on pa.ProductActionTypeId = t.[ProductActionType.Id]

GO
