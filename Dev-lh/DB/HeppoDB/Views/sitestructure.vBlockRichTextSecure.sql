SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vBlockRichTextSecure]
AS
SELECT   
	   b.*
      ,t.Content AS 'BlockRichText.Content'
FROM         
      sitestructure.tBlockRichText AS t 
	  inner join [sitestructure].[vCustomBlockSecure] as b on t.[BlockId] = b.[Block.BlockId]

GO
