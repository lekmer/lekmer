SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentNodeUrlLinkSecure]
AS
	select 
		cn.*,
		cnu.[LinkUrl] 'ContentNodeUrlLink.LinkUrl'
	from 
		[sitestructure].[tContentNodeUrlLink] as cnu
		inner join [sitestructure].[vCustomContentNodeSecure] as cn on cnu.[ContentNodeId] = cn.[ContentNode.ContentNodeId]

GO
