
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [media].[vCustomImage]
as
	select
		IM.*,
		LI.Link as 'LekmerImage.Link',
		LI.Parameter  as 'LekmerImage.Parameter',
		LI.TumbnailImageUrl  as 'LekmerImage.TumbnailImageUrl',
		LI.HasImage  as 'LekmerImage.HasImage'
	from
		[media].[vImage] as IM
		left join [lekmer].[tLekmerImage] as LI on LI.[MediaId]=IM.[Image.MediaId]
GO
