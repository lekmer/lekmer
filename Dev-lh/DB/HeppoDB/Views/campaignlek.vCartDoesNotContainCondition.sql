SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vCartDoesNotContainCondition]
AS
	SELECT
		[cdncc].[ConditionId] AS 'CartDoesNotContainCondition.ConditionId',
		[cdncc].[ConfigId] AS 'CartDoesNotContainCondition.ConfigId',
		[cc].*
	FROM
		[campaignlek].[tCartDoesNotContainCondition] cdncc
		INNER JOIN [campaign].[vCustomCondition] cc ON [cc].[Condition.Id] = [cdncc].[ConditionId]
GO
