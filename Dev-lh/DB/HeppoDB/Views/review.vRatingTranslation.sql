SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingTranslation]
AS
SELECT 
	[RatingId] AS 'RatingTranslation.RatingId',
	[LanguageId] AS 'RatingTranslation.LanguageId',
	[Title] AS 'RatingTranslation.Title',
	[Description] AS 'RatingTranslation.Description'
FROM 
	[review].[tRatingTranslation]
GO
