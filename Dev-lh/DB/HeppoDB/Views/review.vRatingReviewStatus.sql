SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingReviewStatus]
AS
 SELECT 
         rrs.[RatingReviewStatusId] 'RatingReviewStatus.RatingReviewStatusId', 
         rrs.[CommonName] 'RatingReviewStatus.CommonName',
         rrs.[Title] 'RatingReviewStatus.Title'
 FROM 
         [review].[tRatingReviewStatus] rrs
GO
