SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaign].[vCampaignRegistry]
AS
	SELECT 
		CampaignRegistryId AS 'CampaignRegistry.Id',
		Title AS 'CampaignRegistry.Title', 
		CommonName AS 'CampaignRegistry.CommonName'
	FROM
		campaign.tCampaignRegistry
		


GO
