SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [customer].[vCustomerGroupStatus]
AS
SELECT     
	CGS.CustomerGroupStatusId AS 'CustomerGroupStatus.CustomerGroupStatusId',
	CGS.Title AS 'CustomerGroupStatus.Title',
	CGS.CommonName AS 'CustomerGroupStatus.CommonName'
FROM
	[customer].[tCustomerGroupStatus] as CGS
GO
