SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vReviewRecord]
AS
 SELECT
	r.*,
	(CASE WHEN p.[WebShopTitle] IS NULL THEN p.[Title] ELSE p.[WebShopTitle] END) 'Product.Title',
	c.[UserName] 'Customer.UserName'
 FROM 
	addon.vCustomReview r
	INNER JOIN product.tProduct p ON p.ProductId = r.[Review.ProductId]
	LEFT JOIN customer.tCustomerUser c ON c.CustomerId = r.[Review.CustomerId]

GO
