SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [customer].[vCustomerGroupFolder]
AS
SELECT     
	CGF.CustomerGroupFolderId AS 'CustomerGroupFolder.Id',
	CGF.ParentId AS 'CustomerGroupFolder.ParentId',
	CGF.Title AS 'CustomerGroupFolder.Title'
FROM
	[customer].[tCustomerGroupFolder] as CGF
GO
