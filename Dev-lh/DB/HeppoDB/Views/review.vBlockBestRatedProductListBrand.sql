SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [review].[vBlockBestRatedProductListBrand]
AS
	SELECT
		bbrplb.[BlockId] AS 'BlockBestRatedProductListBrand.BlockId',
		bbrplb.[BrandId] AS 'BlockBestRatedProductListBrand.BrandId',
		b.*
	FROM
		[review].[tBlockBestRatedProductListBrand] bbrplb
		INNER JOIN [lekmer].[vBrand] b ON b.[Brand.BrandId] = bbrplb.[BrandId]


GO
