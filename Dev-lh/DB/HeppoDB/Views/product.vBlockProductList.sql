SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vBlockProductList]
AS
SELECT 
	[BlockId] 'BlockProductList.BlockId',
	[ColumnCount] 'BlockProductList.ColumnCount',
	[RowCount] 'BlockProductList.RowCount'
  FROM [product].[tBlockProductList]
GO
