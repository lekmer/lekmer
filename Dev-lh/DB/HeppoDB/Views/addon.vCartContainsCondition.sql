SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [addon].[vCartContainsCondition]
AS 
	SELECT 
		ccc.MinQuantity AS 'CartContainsCondition.MinQuantity',
		ccc.AllowDuplicates AS 'CartContainsCondition.AllowDuplicates',
		ccc.IncludeAllProducts AS 'CartContainsCondition.IncludeAllProducts',
		cc.*
	FROM 
		addon.tCartContainsCondition ccc
		INNER JOIN campaign.vCustomCondition cc ON ccc.[ConditionId] = cc.[Condition.Id]
GO
