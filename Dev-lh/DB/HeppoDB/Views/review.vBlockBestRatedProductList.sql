SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vBlockBestRatedProductList]
AS
	SELECT
		[BlockId] AS 'BlockBestRatedProductList.BlockId',
		[ColumnCount] AS 'BlockBestRatedProductList.ColumnCount',
		[RowCount] AS 'BlockBestRatedProductList.RowCount',
		[NumberOfItems] AS 'BlockBestRatedProductList.NumberOfItems',
		[CategoryId] AS 'BlockBestRatedProductList.CategoryId',
		[RatingId] AS 'BlockBestRatedProductList.RatingId'
	FROM
		[review].[tBlockBestRatedProductList]
GO
