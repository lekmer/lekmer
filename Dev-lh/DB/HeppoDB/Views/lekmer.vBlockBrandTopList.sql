SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [lekmer].[vBlockBrandTopList]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopList.BlockId',
		b.[ColumnCount] AS 'BlockBrandTopList.ColumnCount',
		b.[RowCount] AS 'BlockBrandTopList.RowCount',
		b.[TotalBrandCount] AS 'BlockBrandTopList.TotalBrandCount',
		b.[IncludeAllCategories] AS 'BlockBrandTopList.IncludeAllCategories',
		b.[OrderStatisticsDayCount] AS 'BlockBrandTopList.OrderStatisticsDayCount',
		b.[LinkContentNodeId] AS 'BlockBrandTopList.LinkContentNodeId'
	FROM
		[lekmer].[tBlockBrandTopList] b
GO
