SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [orderlek].[vOrderVoucherInfo]
AS
SELECT 
	[ovi].[OrderId] AS 'OrderVoucherInfo.OrderId',
	[ovi].[VoucherCode] AS 'OrderVoucherInfo.VoucherCode',
	[ovi].[DiscountType] AS 'OrderVoucherInfo.DiscountType',
	[ovi].[DiscountValue] AS 'OrderVoucherInfo.DiscountValue',
	[ovi].[AmountUsed] AS 'OrderVoucherInfo.AmountUsed',
	[ovi].[AmountLeft] AS 'OrderVoucherInfo.AmountLeft',
	[ovi].[VoucherStatus] AS 'OrderVoucherInfo.VoucherStatus'
FROM 
	[orderlek].[tOrderVoucherInfo] ovi
GO
