SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [security].[vCustomSystemUserStatus]
as
	select
		*
	from
		[security].[vSystemUserStatus]
GO
