SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [product].[fProductHasSizes] 
(
	@ProductId INT
)
RETURNS BIT
AS
BEGIN
	DECLARE @Result BIT

	SELECT TOP 1 @Result = 1
	FROM lekmer.tProductSize
	WHERE ProductId = @ProductId

	IF @Result IS NULL
	SET @Result = 0
	
	-- Return the result of the function
	RETURN @Result
END
GO
