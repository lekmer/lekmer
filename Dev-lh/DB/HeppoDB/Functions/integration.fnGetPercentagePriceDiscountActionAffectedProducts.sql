
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [integration].[fnGetPercentagePriceDiscountActionAffectedProducts](
	@ProductActionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN

	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN [integration].[fnGetPercentagePriceDiscountActionCategories] (@ProductActionId, 1) ic ON ic.CategoryId = p.CategoryId
			
	UNION -- include products
	
		SELECT
			ProductId
		FROM
			[campaignlek].[tCampaignActionIncludeProduct] ip
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ip].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
	
	UNION -- include brand
	
		SELECT
			ProductId
		FROM
			[lekmer].[tLekmerProduct] lp
			INNER JOIN [campaignlek].[tCampaignActionIncludeBrand] ib ON [ib].[BrandId] = [lp].[BrandId]
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ib].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId

	EXCEPT -- exclude products from categories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN [integration].[fnGetPercentagePriceDiscountActionCategories] (@ProductActionId, 0) ec ON ec.CategoryId = p.CategoryId

	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			[campaignlek].[tCampaignActionExcludeProduct] ep
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ep].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
			
	EXCEPT -- exclude brand
	
		SELECT
			ProductId
		FROM
			[lekmer].[tLekmerProduct] lp
			INNER JOIN [campaignlek].[tCampaignActionExcludeBrand] eb ON [eb].[BrandId] = [lp].[BrandId]
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [eb].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId

	RETURN
END
GO
