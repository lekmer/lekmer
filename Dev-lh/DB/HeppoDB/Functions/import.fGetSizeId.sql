SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [import].[fGetSizeId]
( 
	@SizeStandardName NVARCHAR (50),
	@SizeNumber NVARCHAR (50)
)
RETURNS INT
AS
BEGIN
	DECLARE @SizeId INT
	
	IF @SizeStandardName = 'USM'
	BEGIN
		SELECT @SizeId = SizeId 
		FROM lekmer.tSize 
		WHERE USMale = REPLACE(@SizeNumber, ',', '.')
	END
	
	ELSE IF @SizeStandardName = 'USW'
	BEGIN
		SELECT @SizeId = SizeId 
		FROM lekmer.tSize 
		WHERE USFemale = REPLACE(@SizeNumber, ',', '.')
	END
	
	ELSE IF @SizeStandardName = 'UKM'
	BEGIN
		SELECT @SizeId = SizeId 
		FROM lekmer.tSize 
		WHERE UKMale = REPLACE(@SizeNumber, ',', '.')
	END
	
	ELSE IF @SizeStandardName = 'UKW'
	BEGIN
		SELECT @SizeId = SizeId 
		FROM lekmer.tSize 
		WHERE UKFemale = REPLACE(@SizeNumber, ',', '.')
	END	
	
	ELSE IF @SizeStandardName = 'EU'
	BEGIN
		SELECT @SizeId = SizeId 
		FROM lekmer.tSize 
		WHERE EU = REPLACE(@SizeNumber, ',', '.')
	END	
	
	ELSE
	BEGIN
		SET @SizeId = NULL
	END
 
	RETURN @SizeId
END
GO
