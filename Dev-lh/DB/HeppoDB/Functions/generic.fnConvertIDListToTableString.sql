SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [generic].[fnConvertIDListToTableString](
           @IDList varchar(max),
           @Delimiter CHAR(1) = ','
)
returns @tblID table(ID varchar(max))
as
begin
           if @IDList is null or @IDList = ''
                      return

           if substring(@IDList, len(@IDList), 1) <> @Delimiter
                      select @IDList = @IDList + @Delimiter

           declare @ind1 int, @ind2 int
           select @ind1 = 1

           while 1 = 1
           begin
                      select @ind2 = charindex(@Delimiter, @IDList, @ind1)

                      if @ind2 = 0
                                 break

                      insert
                                 @tblID
                      values
                      (
                                 substring(@IDList, @ind1, @ind2 - @ind1)
                      )

                      select @ind1 = @ind2 + 1
           end

           return
end
GO
