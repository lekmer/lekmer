﻿2012-06-08---------------------------------------
-PT30918079 - BO - Campaigns - Include/Exclude products grid - need more items per page
-PT25112987 - Campaign Cart Action / Add product auto free / Fix Klarna Issue with free product.
-Multiple Sizes Purchase CR // From Litium // New template variables //[DB]
-Multiple Sizes Purchase CR - override order merge with cart to separate same product items with different sizes.
-PT30848607 - Add check for vouchers without discount. - [DB]
-PT30848607 - Add check for vouchers without discount.
-Multiple Sizes Purchase CR // From Litium
-PT30459107 - FileExport Heppo takes over 1,5 hours!... // Tags
-PT30701781 - Multiple €-currencies - Add description.

2012-06-07---------------------------------------
-PT30313471 - Sync from LIVE - Tmp tables, SPs, Remova old restriction tables, Seo, Import, pReport, pTagProductSale, New role privilege
-PT24915623 - Missing server side check if order terms are checked or not //[DB]
-PT24915623 - Missing server side check if orderterms are checked or not
-PT25112987 - Campaign Cart Action / Add product auto free - [DB]
-PT25112987 - Campaign Cart Action / Add product auto free

2012-06-06---------------------------------------
-PT28137869 - BO - Site performance // Bestsellers  - Include offline products // [DB]
-PT30313289 - BO, Cant remove tags //[DB]
-PT28137869 - BO - Site performance
-PT30313289 - BO, Cant remove tags // Remove tags from BlockProductFilters

2012-06-05---------------------------------------
-PT29987735 - eSales Notification // WebService
-PT25112987 - Campaign Cart Action / Add product auto free - [DB]

2012-06-01---------------------------------------
-PT25112987 - Campaign Cart Action / Add product auto free

2012-05-31---------------------------------------
-Fix invalid objects in database. //[DB]
-PT30119493 - Interspire - scensum NewsLetterUpdater // Change service behavior
-PT27363055 - Move SSL certifikat into router [Do NOT move me (dino)] // Special check for IsSecureConnection
-PT29987735 - eSales Notification // WebService
-PT25112987 - Campaign Cart Action / Add product auto free
-PT30391005 - FileExport - Error when running TopProductList export

2012-05-30---------------------------------------
-PT29315069 - eSales Scensum implementation
-PT24555815 - Changes to Cart - Buy more than one size of a product // [DB]
-PT30321203 - Allow Multiple Sizes Purchase // [DB]
-PT24555815 - Changes to Cart - Buy more than one size of a product
-PT30321203 - Allow Multiple Sizes Purchase
-PT29315069 - eSales Scensum implementation
-Remove hardcoded restricted objects because can be managed from BO - [DB]

2012-05-29---------------------------------------
-PT30312575 - Keybroker feed brand Url
-PT30119493 - Interspire - scensum NewsLetterUpdater
-PT30315121 - Heppo FR Checkout Down // Exclude Klarna logic when not supported

2012-05-28---------------------------------------
-PT26928637 - Campaign Cart Action / % or value discount on most expensive/cheapest cart product
-PT30119493 - Interspire - scensum NewsLetterUpdater - [DB]
-PT29304461 - eSales Export // More fields //[DB]
-PT29304461 - eSales Export // More fields
-PT30119493 - Interspire - scensum NewsLetterUpdater

2012-05-25---------------------------------------
-PT29304461 - eSales Export // More fields
-PT26928781 - Campaign condition / Customer Group - [DB]
-PT26928781 - Campaign condition / Customer Group
-PT29304461 - eSales Export
-PT29304461 - eSales Export // More fields

2012-05-24---------------------------------------
-PT29304461 - eSales Export
-Settings files with local configuration.
-Settings files with local configuration.
-Web.config files for web projects. (with local configuration)
-Publish.xml setting files for web projects. (with local configuration)
-Remove Oculus setup project from main solution.
-PT29304461 - eSales Export

2012-05-23---------------------------------------
-PT30062769 - Lekmer article restriction logic change - [DB]
-PT29914545 - Klarna PClasses heppo + lekmer
-customUrl1 - needs to be last in Keybroker feed


2012-05-22---------------------------------------
-PT29540015 - ErpId in tProduct needs to be unique
-PT29999301 - Sitemap feed - master pages // Fix error with BlockAvailProductsPredictionsControl
-PT29304461 - eSales Export
-PT29981293 - Server error 500 in LEKMER '+HEPPO NO, DK, FI investigate // Problem with Validation in LekmerCheckoutInformationForm.
-PT29974031 - Keybroker feed - Category url's and xml tags
-PT29921455 - Klarna NL Gender field - is not sent to Klarna
-Remove redundant folders.
-Changes Keybrokers want.

2012-05-21---------------------------------------
-PT25112945 - Campaign Cart Action / Fixed price discount  - [DB]
-PT25112945 - Campaign Cart Action / Fixed price discount


2012-05-18---------------------------------------
-PT29681203 - Klarna NL Gender field // [DB]
-PT29681203 - Klarna NL Gender field
-PT29824181 - Checkout Postback event update quantity was executed several times - [DB]
-PT26928637 - Campaign Cart Action / % or value discount on most expensive/cheapest cart product - [DB]
-PT26928637 - Campaign Cart Action / % or value-discount on cheapest/most expensive
-PT29824181 - checkout Postback event update quantity was executed several times
-eSales libs

2012-05-17---------------------------------------
-PT29315069 - eSales Scensum implementation - new projects
-PT29681203 - Customer gender field, BO
-PT29681203 - Customer gender field

2012-05-16---------------------------------------
-PT29681203 - Klarna NL HouseExtension not required
-PT29683217 - BO change // Product size - edit millimeter. // [DB]
-PT29683217 - BO change // Product size - edit millimeter.
-PT29623181 - Backoffice - Order page - 'Resend order confirmation' doesn't work // Replace 'AddEntity' with explicit variables adding.
-PT29623181 - Backoffice - Order page - 'Resend order confirmation' doesn't work // Code formating.

2012-05-15---------------------------------------
-PT29006091 - Keybroker xml feed - urls to categories of 1 and 2 levels
-PT29242671 - Klarna - MakeAddress - add support for NL // Fix LekmerOrderMessenger issue
-eSales
-PT29297619 - Klarna NL  house_number and house_extension fields. // [DB]
-PT29242671 - Klarna - MakeAddress - add support for NL // Fix ValidationUtil
-PT24566597 - Add more fields to the XML-files (Marin Software)
-PT29242671 - Klarna - MakeAddress - add support for NL // IsRequired condition
-PT29242671 - Klarna - MakeAddress - add support for NL // [DB]
-PT29242671 - Klarna - MakeAddress - add support for NL // Public web

2012-05-14---------------------------------------
-PT29242671 - Klarna - MakeAddress - add support for NL // Public web
-PT29242671 - Klarna - MakeAddress - add support for NL // Backoffice
-PT29539461 - Xml Feeds formation error
-PT29005661 - Investigate monitor email function
-PT29242671 - Klarna - MakeAddress - add support for NL // Services
-PT26637279 - [Restrictioner för produkter per kanal] BO
-Deploy fix - DataExport console, BlockProductFilterService

2012-05-11---------------------------------------
-PT29242671 - Klarna - MakeAddress - add support for NL (house number & extension)
-PT28936721 - Add all in range button - limit 500 articles

2012-05-10---------------------------------------
-PT29400509 - Randomly disappearing products issue
-PT29400509 - Randomly disappearing products issue //[DB]
-PT28223555 - CDON Export prices (Campaigns) - Read offline products //[DB]
-pTagProductSale //DB
-PT28223555 - CDON Export prices (Campaigns) - include off-line products.

2012-05-09---------------------------------------
-PT28223555 - CDON Export prices (Campaigns)
-PT29226721 - R&R Additional caching // BlockRating, BlockRatingItem, BlockRatingGroup - remove unnecessary calls.
-PT29226721 - R&R Additional caching // RatingGroupCache
-PT28223555 - CDON Export prices (Campaigns) // [DB]

2012-05-08---------------------------------------
-PT29226721 - R&R Additional caching // RatingGroupCategoryListCache
-PT29242671 - Klarna - MakeAddress - add support for NL // Render on demand monthly cost on BlockLekmerCheckoutControl, BlockExtendedCartControl
-PT29242671 - Klarna - MakeAddress - add support for NL // MakeAddress for NL
-PT29242671 - Klarna - MakeAddress - add support for NL // Refactoring
-PT28223555 - CDON Export prices (Campaigns)
-PT26637239 - [Restrictioner för produkter per kanal] DB objects - [DB]
-PT29066997 - keybroker feed, roudning issue on FI, too many delimiters

2012-05-07---------------------------------------
-Sync from live - [DB]
-PT26637239 - [Restrictioner för produkter per kanal] DB objects - [DB]
-PT25633201 - [Restrictioner för produkter per kanal] Application
-PT26637279 - [Restrictioner för produkter per kanal] BO
-PT29066997 - keybroker feed, roudning issue on FI, too many delimiters
-PT29078811 - NumberInStock - Validate before placing order
-PT28223555 - CDON Export prices (Campaigns)
-PT29078811 - NumberInStock - Refresh cache

2012-05-04---------------------------------------
-PT29078811 - NumberInStock - Refresh cache after placing order.
-[integration].[pBackofficeProductImportById] - remove logic to set product offline if it has description for all channels - [DB]
-PT29023479 - Xml feeds - discount price and campaign flag; Remove Keybroker xml tags // Products:xmlns // Products:xsi:noNamespaceSchemaLocation
-Keybroker feed fix
-PT29009105 - R&R - Additional variables

2012-05-03---------------------------------------
-PT29009105 - R&R - Additional variables

2012-05-02---------------------------------------
-Added Keybroker Xml Schema Definition in the setting file, so it can be easily changed.

2012-04-30---------------------------------------
-A few minor element changes

2012-04-27---------------------------------------
-Uncommented code
-Add missed IoC files and DLLs

2012-04-26---------------------------------------
-PT24555441 - Ratings and review - Report inappropriate content
-PT28460189 - R&R - Reviews from variants.

2012-04-25---------------------------------------
-PT23001701 - Ratings and review - Like/Dislike a rating
-Keybroker feed - popularity

2012-04-24---------------------------------------
-PT28431917 - Ratings and review - Ratings information on product filter page
-PT27295975 - Review lastest feedback list - product page
-PT28435401 - R&R - Line breaks are removed
-PT28450189 - R&R - Product image in block product most helpful rating.

2012-04-23---------------------------------------
-PT28432863 - R&R - Ratings affect stats when status pending.
-PT28432061 - R&R - Email address

2012-04-20---------------------------------------
-PT28342579 - R&R - Concurrency issue
-PT28351681 - Extend "ValidationUtil" class for new channels NL and FR.
-PT28295117 - BlockExtendedCartControl throw error when BlockVoucherDiscountControl is not used
-R&R Refactoring
-PT28208467 - Extend ExportData API
-Litium.Lekmer.StatisticUtil - Add Missed References

2012-04-19---------------------------------------
-PT23995463 - [Product,Cart] 500 server error + Deadlocks // ProductUrl saving - prevent delete and recreate product urls if there are no changes.

2012-04-18---------------------------------------
-PT27026807 - Media.Web - Unhandled exceptions are not logged
-Fucktions for the new Kaybroker Feed.
-PT28074621 - Wrong url in monitor emails
-PT25112969 - Campaign Cart Action / Percentage discount on order value - [DB]
-PT25112969 - Campaign Cart Action / Percentage discount on order value - [DB]
-PT25113087 - Campaign Condition / Cart contain brand - delete all include/exclude categories does not work
-PT25112969 - Campaign Cart Action / Percentage discount on order value
-PT28035629 - Log files are not used by all sites - Fix by separate log file for each site.
-PT28060173 - 500 server error on Dev, product detail page

2012-04-17---------------------------------------
-PT28036547 - Klarna changes to support new channels (only NL)
-PT23995463 - [Product,Cart] 500 server error + Deadlocks // -ProductRepository.GetAllByIdList - rewrite to reduce deadlock errors // -LekmerProductRepository.GetAllViewByIdList - rewrite to reduce deadlock errors

2012-04-15---------------------------------------
-PT27026807 - BO - Unhandled exceptions are not logged

2012-04-14---------------------------------------
-PT28035629 - Log files are not used by all sites - Fix by separate log file for each site.

2012-04-13---------------------------------------
-PT27634253 - Campaign Product Action / Products in cart can lose discount

2012-04-11---------------------------------------
-PT27633903 - Campaign Product Action / Fixed discount value on prod, cat, brand

2012-04-10---------------------------------------
-PT27761575 - [Session] 500 server error, Avail export //[DB]
-Add RatingReviewFeedback entity for MostHelpfulRating block
-Rating & Review - Template functions - [DB]
-PT27633903 - Campaign Product Action / Fixed discount value on prod, cat, brand - [DB]
-PT27633903 - Campaign Product Action / Fixed discount value on prod, cat, brand - [DB]

2012-04-09---------------------------------------
-PT25112999 - Campaign Product Action / Get X% on brand

2012-04-06---------------------------------------
-Refactoring - Move BrandIdDIctionary to Litium.Lekmer.Contract

2012-04-05---------------------------------------
-PT25113027 - Campaign Condition / Cart value  between
-PT27571285 - Seo column and BO modification (250=>500)

2012-04-04---------------------------------------
-PT25113087 - Campaign Condition / Cart contain brand

2012-04-03---------------------------------------
-PT27462239 - Product.Monitor service stopped working
-Rating & Review - Privilege data - [DB]

2012-04-02---------------------------------------
-PT27273155 - EditTagGroups Layout Changes
-PT24555395 - Ratings and review - Save rating on Product page - fix [review].[vRatingItemProductVote] // [DB]
-Delete [integration].[pCreateImageReferences] // Add [integration].[pCreateProductImageReferences] // [DB]
-Rating & Review - Template functions - [DB]
-Added 2 new channels
-Complete rewrite
-Generate ProductSeo For FR
-Create Index on tmp table for better performance when updating prices.
-Small bugfix and added ShowVariantRelations to new imported products.
-Prep for new SEO Sps
-Now also sets primary and secundary template

2012-03-30---------------------------------------
-ShowVariantRelations change.
-Logic changed to products that generated most money, not number of sales.
-Populates tNewsletterSubscriber before send emials to interspire
-CDON SP
-Changes in [integration].[usp_REATagProduct] (from live)
-Avail full category selection
-Added more restrictions, tmp solution.
-A new Report for ReportManager that lists all products that need Images and are in stock. Lists all products in size 36 for woman
-A new Report for ReportManager that lists all products that need Images and are in stock. Lists all products in size 44 for man
-Creates a brandpage in BO for NL after specifications from Niklas
-Creates a brandpage in BO for FR after specifications from Niklas
-PT24555441 - Ratings and review - Report inappropriate content // [DB]
-PT23000953 - Ratings and review - Show ratings
-PT23001297 - Ratings and review - Orderitems Ratings page
-PT26854733 - Ability to rate offline products

2012-03-29---------------------------------------
-PT27206689 - Tag, alias import [DB]

2012-03-28---------------------------------------
-PT26975077 - Product filter - Product Size Option - Available sizes
-PT26722439 - Variable "Product.HasVariants" and "Iterate:ColorOption" does not work in some cases

2012-03-27---------------------------------------
-Rating & Review - Add new block 'BlockProductRatingSummary'  - [DB]
-PT26414467 - [Cache] Deadlocks - Exclusive lock is added on tCacheUpdate table for delete and insert statements.

2012-03-26---------------------------------------
-PT27023069 - UrlHistoryCleaner - too short time running interval
-Rating & Review - Remove redundant SPs - [DB]
-Rating & Review - Add new block 'BlockProductRatingSummary'  - [DB]
-Rating & Reviews [DB]
-PT26414467 - [Cache] Deadlocks - Exclusive lock is added on tCacheUpdate table for delete and insert statements.
-Ratings and review - Remove redundant SPs [DB]

2012-03-22---------------------------------------
-PT26861229 - Do not allow browsers to cache automatic redirects (301)

2012-03-21---------------------------------------
-PT25631837 - [Cache] Cache
-[DB] - Add product default image to tProductImage table with group "Default" (where it is missed)

2012-03-20---------------------------------------
-PT25631837 - [Cache] Cache
-Add *.user files to exclude list

2012-03-15---------------------------------------
-integration.usp_GetOriginalProductTitleHeppo can be deleted
-integration.usp_CreateProductUrls can be deleted
-PT25631947 - [Cache] Cache not used for Heppo users that are logged in.

2012-03-14---------------------------------------
-PT25632963 - [Replicera Sitestructure] - Put empty settings
-PT26414467 - [Cache] Deadlocks - Exclusive lock is added on tCacheUpdate table for delete and insert statements.
-The file exporter now skips channels that are not in the settings file.

2012-03-13---------------------------------------
-PT25632963 - [Replicera Sitestructure]

2012-03-08---------------------------------------
-PT25191025 - Fetch new Klarna P-classes - fix application references

2012-03-06---------------------------------------
-Deploy to Dev
-PT25824849 - Similar paging bug - wrong paging behavior

2012-03-05---------------------------------------
-PT24555395 - Ratings and review - Save rating on Product page - review status data [DB]

2012-03-04---------------------------------------
-Deploy to Dev
-PT24555395 - Ratings and review - Save rating on Product page

2012-03-02---------------------------------------
-PT24982293 - Lekmer: All articles over 1000 kr and 100 euro should have freight: 0 in XML feed

2012-02-29---------------------------------------
-Deploy to Dev
-PT23001723 - Ratings and review - Rating templates administration [Code]
-PT23001723 - Ratings and review - Rating templates administration [DB]

2012-02-27---------------------------------------
-PT25545437 - ActiveImageImporter (important) - script for removing wrong media items.

2012-02-23---------------------------------------
-PT23735513 - Similar Products - lower/higher price - fix bug to correctly display saved price type on block in bo.
-PT23735513 - Similar Products - lower/higher price // Fix issue when Product is NULL
-PT24350545 - Update Avail categories/data sent to Avail // Fix functionality to select LekmerProductView instead of LekmerProduct

2012-02-22---------------------------------------
PT25114025 - Lekmer: Problem with Avail restrictions on product-box. // Problems with avail block not displaying all recommended products because of NumberInStock issue. // Fix code formatting.

2012-02-21---------------------------------------
-PT25114025 - Lekmer: Problem with Avail restrictions on productbox // Problems with avail block not displaying all recomended products because of NumberInStock issue.

2012-02-10---------------------------------------
-PT24350545 - Update Avail categories/data sent to Avail - Fix pAvailGetProductDiscountPriceOnSite to return correct list of prices.
-PT24687259 - Product Description, not using SE as default language - Remove "bogus" line from editor value.

2012-02-09---------------------------------------
-PT24350545 - Update Avail categories/data sent to Avail - Add discounted price
-PT8666261 - Automatic redirect (301) - fix bug when content page could not be saved when old title already exist in url history.

2012-02-08---------------------------------------
-PT21842135 - Fix bug with zero size images in cache when it does not need to be resized.
-PT23000639 - Size handling

2012-02-07---------------------------------------
-PT23735513 - Similar Products - lower/higher price
-PT23810505 - Brand Toplist

2012-02-06---------------------------------------
-PT23810505 - Brand Toplist

2012-02-03---------------------------------------
-PT23810505 - Brand Toplist

2012-02-02---------------------------------------
-PT24065417 - Clothes gets flagged out of stock on article card
-PT24277939 - News filtering on heppo

2012-02-01---------------------------------------
-PT23810505 - Brand Toplist - Contract project and some implementation
-PT24209661 - Fix issues when Voucher is not consumed with use of credit card.

2012-01-31---------------------------------------
-PT23000883 - Extended product info on product filter page
-PT20836439 - Xml feeds - Fix bug with files saving.

2012-01-18---------------------------------------
-Deploy to Dev
-PT19083605 - Service Center
-PT23000625 - Variant handling