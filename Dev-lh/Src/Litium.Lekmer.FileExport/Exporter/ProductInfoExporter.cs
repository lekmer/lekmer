﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Lekmer.FileExport.Service;
using Litium.Lekmer.FileExport.Setting;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;
using Litium.Lekmer.Product;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport.Exporter
{
	public class ProductInfoExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IEnumerable<IUserContext> _userContexts;
		private Collection<IProductImageGroup> _allowedImageGroups;
		private Collection<IImageSize> _allowedImageSizes;
		private Collection<ITagGroup> _allowedTagGroups;
		private Dictionary<string, string> _allowedCategoryLevelUrls;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected IProductInfoService ProductInfoService { get; private set; }
		protected IFileExportSetting FileExportSetting { get; private set; }
		protected ICampaignSecureService CampaignSecureService { get; private set; }
		private Collection<ICampaign> _campaigns;

		public ProductInfoExporter(ILekmerChannelService lekmerChannelService, IProductInfoService productInfoService, IFileExportSetting fileExportSetting, ICampaignSecureService campaignSecureService)
		{
			LekmerChannelService = lekmerChannelService;
			ProductInfoService = productInfoService;
			FileExportSetting = fileExportSetting;
			CampaignSecureService = campaignSecureService;
		}

		public virtual void Execute()
		{
			_log.Info("Execution is started.");
			_userContexts = GetUserContextForAllChannels();

			_campaigns = CampaignSecureService.GetAllProductCampaigns();
			ExecuteCompleteProductList();
			ExecuteTopProductList();

			_log.Info("Executed.");
		}

		public virtual void ExecuteCompleteProductList()
		{
			_log.Info("Complete product list is started.");

			_allowedImageGroups = ProductInfoService.GetAllowedImageGroups();
			_allowedImageSizes = ProductInfoService.GetAllowedImageSizes();
			_allowedTagGroups = ProductInfoService.GetAllowedTagGroups();
			_allowedCategoryLevelUrls = ProductInfoService.GetAllowedCategoryLevelUrls();

			foreach (IUserContext context in _userContexts)
			{
				if (!FileExportSetting.GetChannelNameReplacementExists(context.Channel.CommonName))
				{
					_log.Info("Skipping channel: " + context.Channel.CommonName);
					continue;
				}

				Stopwatch stopwatch = Stopwatch.StartNew();

				var products = ProductInfoService.GetCompleteProductList(context, true, true, true);

				BuildCompleteProductListFile(context, products);
				BuildSmartlyFile(context, products);

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}

			_log.Info("Complete product list is completed.");
		}

		public virtual void ExecuteTopProductList()
		{
			_log.Info("Top product list is started.");

			var allowedImageGroups = ProductInfoService.GetAllowedImageGroups();
			var allowedImageSizes = ProductInfoService.GetAllowedImageSizes();
			var allowedTagGroups = ProductInfoService.GetAllowedTagGroups();
			var allowedCategoryLevelUrls = ProductInfoService.GetAllowedCategoryLevelUrls();

			foreach (IUserContext context in _userContexts)
			{
				if (!FileExportSetting.GetChannelNameReplacementExists(context.Channel.CommonName))
				{
					_log.Info("Skipping channel: " + context.Channel.CommonName);
					continue;
				}

				var topProducts = ProductInfoService.GetTopProductList(context);

				foreach (var topInfo in topProducts)
				{
					string filePath = GetFilePathForTopProductList(context, topInfo);

					_log.InfoFormat("Top product list - file saving... //{0}", filePath);

					if (File.Exists(filePath))
					{
						File.Delete(filePath);
					}

					using (Stream stream = File.OpenWrite(filePath))
					{
						var productInfoXmlFile = new ProductInfoXmlFile();

						productInfoXmlFile.InitializeImages(allowedImageGroups, allowedImageSizes);
						productInfoXmlFile.InitializeApplicationType(FileExportSetting.ApplicationName);
						productInfoXmlFile.InitializeTagGroups(allowedTagGroups);
						productInfoXmlFile.InitializeCategoryLevels(allowedCategoryLevelUrls);
						productInfoXmlFile.InitializeCampaigns(_campaigns);

						productInfoXmlFile.Save(stream, context, topInfo.Products);
					}
				}
			}

			_log.Info("Top product list is completed.");
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}

		protected virtual string GetFilePathForCompleteProductList(IUserContext context)
		{
			string dirr = FileExportSetting.DestinationDirectoryProduct;
			string name = FileExportSetting.XmlFileNameProductExporter;

			string channelName = FileExportSetting.GetChannelNameReplacement(context.Channel.CommonName);
			name = name.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, name);
		}
		protected virtual string GetFilePathForTopProductList(IUserContext context, ITopProductListExportInfo topListInfo)
		{
			string dirr = FileExportSetting.DestinationDirectoryTopList;
			string name = FileExportSetting.XmlFileNameTopListExporter;

			string channelName = FileExportSetting.GetChannelNameReplacement(context.Channel.CommonName);
			name = name.Replace("[ChannelName]", channelName);

			name = name.Replace("[PageTitle]", topListInfo.PageTitle);
			name = name.Replace("[BlockTitle]", topListInfo.BlockTitle);

			return Path.Combine(dirr, name);
		}
		protected virtual string GetFilePathForSmartlyProductList(IUserContext context)
		{
			string dirr = FileExportSetting.DestinationDirectorySmartly;
			string name = FileExportSetting.XmlFileNameSmartlyExporter;

			string channelName = FileExportSetting.GetChannelNameReplacement(context.Channel.CommonName);
			name = name.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, name);
		}

		protected virtual void BuildCompleteProductListFile(IUserContext context, Collection<IProductExportInfo> products)
		{
			string filePath = GetFilePathForCompleteProductList(context);

			_log.InfoFormat("Complete product list - file saving... //{0}", filePath);

			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				var productInfoXmlFile = new ProductInfoXmlFile();

				productInfoXmlFile.InitializeImages(_allowedImageGroups, _allowedImageSizes);
				productInfoXmlFile.InitializeApplicationType(FileExportSetting.ApplicationName);
				productInfoXmlFile.InitializeTagGroups(_allowedTagGroups);
				productInfoXmlFile.InitializeCategoryLevels(_allowedCategoryLevelUrls);
				productInfoXmlFile.InitializeCampaigns(_campaigns);

				productInfoXmlFile.Save(stream, context, products);
			}
		}
		protected virtual void BuildSmartlyFile(IUserContext context, Collection<IProductExportInfo> products)
		{
			string filePath = GetFilePathForSmartlyProductList(context);

			_log.InfoFormat("Smartly product list - file saving... //{0}", filePath);

			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				var smartlyProductInfoXmlFile = new SmartlyProductInfoXmlFile();
				smartlyProductInfoXmlFile.Save(stream, context, products);
			}
		}
	}
}
