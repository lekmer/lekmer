﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Lekmer.FileExport.Service;
using Litium.Lekmer.FileExport.Setting;
using Litium.Lekmer.FileExport.XmlFile;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.FileExport.Exporter
{
	public class KeybrokerProductInfoExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IEnumerable<IUserContext> _userContexts;
		private Collection<ICampaign> _campaigns;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected IProductInfoService ProductInfoService { get; private set; }
		protected IFileExportSetting FileExportSetting { get; private set; }
		protected ICampaignSecureService CampaignSecureService { get; private set; }

		public KeybrokerProductInfoExporter(ILekmerChannelService lekmerChannelService, IProductInfoService productInfoService, IFileExportSetting fileExportSetting, ICampaignSecureService campaignSecureService)
		{
			LekmerChannelService = lekmerChannelService;
			ProductInfoService = productInfoService;
			FileExportSetting = fileExportSetting;
			CampaignSecureService = campaignSecureService;
		}

		public void Execute()
		{
			if (!FileExportSetting.ApplicationName.Equals(ExporterConstants.HeppoApplication))
			{
				return;
			}

			_log.Info("Execution is started.");
			
			_userContexts = GetUserContextForAllChannels();
			_campaigns = CampaignSecureService.GetAllProductCampaigns();
			
			ExecuteKeybrokersProductList();

			_log.Info("Executed.");
		}

		public virtual void ExecuteKeybrokersProductList()
		{
			_log.Info("Complete product list is started.");

			var allowedTagGroups = ProductInfoService.GetAllowedTagGroups();
			var allowedCategoryLevelUrls = ProductInfoService.GetAllowedCategoryLevelUrls();

			foreach (IUserContext context in _userContexts)
			{
				if (!FileExportSetting.GetChannelNameReplacementExists(context.Channel.CommonName))
				{
					_log.Info("Skipping channel: " + context.Channel.CommonName);
					continue;
				}

				Stopwatch stopwatch = Stopwatch.StartNew();

				var products = ProductInfoService.GetCompleteProductList(context, true, false, true);
				ProductInfoService.SupplementWithPopularity(context, products);

				string filePath = GetFilePathForKeybrokerProductList(context);

				_log.InfoFormat("Complete product list - file saving... //{0}", filePath);

				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}

				using (Stream stream = File.OpenWrite(filePath))
				{
					var keybrokerProductInfoXmlFile = new KeybrokerProductInfoXmlFile();

					keybrokerProductInfoXmlFile.Initialize(allowedTagGroups, _campaigns, allowedCategoryLevelUrls);
					keybrokerProductInfoXmlFile.InitializeApplicationType(FileExportSetting.ApplicationName);
					keybrokerProductInfoXmlFile.Save(stream, context, products);
				}

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}

			_log.Info("Complete product list is completed.");
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}

		protected virtual string GetFilePathForKeybrokerProductList(IUserContext context)
		{
			string dirr = FileExportSetting.DestinationDirectoryProduct;
			string name = FileExportSetting.XmlFileNameKeybrokerProductExporter;

			string channelName = FileExportSetting.GetChannelNameReplacement(context.Channel.CommonName);
			name = name.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, name);
		}
	}
}
