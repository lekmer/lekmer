﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.FileExport.XmlFile
{
	public class KeybrokerProductInfoXmlFile : BaseXmlFile
	{
		private IUserContext _context;

		private Dictionary<string, ITagGroup> _tagGroups;
		private Dictionary<int, ICampaign> _campaigns;
		private Dictionary<string, string> _categoryLevels;

		private bool _isInitialized;
		private bool _isHeppoAplication;
		private bool _isLekmerAplication;
		private bool _applicationTypeInitialized;

		public void Initialize(Collection<ITagGroup> tagGroups, Collection<ICampaign> campaigns, Dictionary<string, string> categoryLevels)
		{
			if (tagGroups == null)
			{
				throw new ArgumentNullException("tagGroups");
			}

			if (campaigns == null)
			{
				throw new ArgumentNullException("campaigns");
			}

			if (categoryLevels == null)
			{
				throw new ArgumentNullException("categoryLevels");
			}

			_tagGroups = tagGroups.ToDictionary(g => g.CommonName);
			_campaigns = campaigns.ToDictionary(c => c.Id);
			_categoryLevels = categoryLevels;

			_isInitialized = true;
		}

		public void InitializeApplicationType(string applicationName)
		{
			_isHeppoAplication = applicationName == ExporterConstants.HeppoApplication;
			_isLekmerAplication = applicationName == ExporterConstants.LekmerApplication;

			_applicationTypeInitialized = true;
		}

		public void Save(Stream savedSiteMap, IUserContext context, IEnumerable<IProductExportInfo> products)
		{
			if (!_isInitialized)
			{
				throw new InvalidOperationException("Initialize() should be called first.");
			}

			if (!_applicationTypeInitialized)
			{
				throw new InvalidOperationException("InitializeApplicationType() should be called first.");
			}

			_context = context;

			var doc = CreateXmlDocument();

			XmlNode productsNode = AddProductsNode(doc);

			foreach (IProductExportInfo productExportInfo in products)
			{
				AddProductNodes(doc, productsNode, productExportInfo);
			}

			doc.Save(savedSiteMap);
		}

		protected XmlNode AddProductsNode(XmlDocument xmlDocument)
		{
			// Products
			XmlNode productsNode = xmlDocument.CreateElement(ExporterConstants.NodeProducts);

			// Products:currency
			XmlAttribute atributeCurrency = xmlDocument.CreateAttribute(ExporterConstants.NodeCurrency);
			atributeCurrency.Value = _context.Channel.Currency.Iso;
			if (productsNode.Attributes != null)
			{
				productsNode.Attributes.Append(atributeCurrency);
			}

			xmlDocument.AppendChild(productsNode);

			return productsNode;
		}

		protected void AddProductNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Product
			XmlNode productNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeProduct);
			// Product:ErpId
			AddAttribute(xmlDocument, productNode, ExporterConstants.NodeSku, productExportInfo.Product.ErpId);

			// Active (If product exists its active by default)
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeActive, ExporterConstants.NodeStatusTrue);

			// Main Category
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeMainCategory, productExportInfo.MainCategoryTitle);

			// Category
			AddNode(xmlDocument, productNode, ExporterConstants.NodeCategory, productExportInfo.ParentCategoryTitle);

			// SubCategory eg Sneakers
			AddNode(xmlDocument, productNode, ExporterConstants.NodeSubCategory, productExportInfo.CategoryTitle);

			// Manufacturer
			if (productExportInfo.Product.Brand != null && productExportInfo.Product.Brand.Title.HasValue())
			{
				string brandTitle = productExportInfo.Product.Brand.Title;
				AddNode(xmlDocument, productNode, ExporterConstants.NodeManufacturer, brandTitle);
			}

			// EAN
			AddEanNode(xmlDocument, productNode, productExportInfo);

			// ModelName
			AddNode(xmlDocument, productNode, ExporterConstants.NodeModelName, productExportInfo.Product.DisplayTitle);

			// Gender
			AddGenderNode(xmlDocument, productNode, productExportInfo);

			// ColorNode
			AddProductColorNode(xmlDocument, productNode, productExportInfo);

			bool affectedByCampaign = productExportInfo.Product.IsPriceAffectedByCampaign;

			// OnPromotion
			string promotion = affectedByCampaign ? "true" : "false";
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeOnPromotion, promotion);

			// PromotionName - CampaignNameNode
			AddCampaignNode(xmlDocument, productNode, productExportInfo);

			// SelectMe, We want all products on campaign to be more visible
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeSelectMe, promotion);

			// PriceWithTax
			decimal priceIncludingVat = affectedByCampaign
				? productExportInfo.Product.CampaignInfo.Price.IncludingVat
				: productExportInfo.Product.Price.PriceIncludingVat;

			string priceText = priceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodePriceWithTax, priceText);

			// OriginalPriceWithTax
			string originalPriceText = productExportInfo.Product.Price.PriceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeOriginalPriceWithTax, originalPriceText);

			// InStock
			string inStock = productExportInfo.Product.TotalNumberInStock > 0 ? ExporterConstants.NodeStatusTrue : ExporterConstants.NodeStatusFalse;
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeInStocks, inStock);

			// QuantityForSale
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeQuantityForSale, productExportInfo.Product.TotalNumberInStock.ToString(CultureInfo.InvariantCulture));

			// ProductUrl
			string productUrl = "http://" + _context.Channel.ApplicationName + "/" + productExportInfo.Product.LekmerUrl;
			AddNode(xmlDocument, productNode, ExporterConstants.NodeProductUrl, productUrl);

			// CategoryUrlsNode
			AddCategoryUrlsNode(xmlDocument, productNode, productExportInfo);

			// CustomUrl3 - Brand
			if (productExportInfo.BrandUrl.HasValue())
			{
				AddNode(xmlDocument, productNode, ExporterConstants.NodeCustomUrl3, productExportInfo.BrandUrl);
			}

			//int salesAmount = productExportInfo.SalesAmount.HasValue ? productExportInfo.SalesAmount.Value : 0;
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeCustomNum1, salesAmount.ToString(CultureInfo.InvariantCulture));

			int popularity = productExportInfo.Popularity.HasValue ? productExportInfo.Popularity.Value : 0;
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeCustomNum2, popularity.ToString(CultureInfo.InvariantCulture));

			// Sizes
			//AddProductSizesNode(xmlDocument, productNode, productExportInfo);

			//<customNum1>24</customNum1> Amount of sales (last 30/90 days)
			//<customNum2>1</customNum2> Popularity ranking (last 30/90 days)
			//<customNum3>20</customNum3> Product margin %
		}

		protected virtual void AddProductSizesNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			var productSizes = productExportInfo.Product.ProductSizes;

			if (productSizes == null) return;

			var sizesNode = AddNode(xmlDocument, productNode, ExporterConstants.NodeSizes);

			foreach (var productSize in productSizes)
			{
				var sizeNode = AddNode(xmlDocument, sizesNode, ExporterConstants.NodeSize);
				AddNode(xmlDocument, sizeNode, ExporterConstants.NameNode, productSize.SizeInfo.EuTitle);
				AddNodeRaw(xmlDocument, sizeNode, ExporterConstants.StockNode, productSize.NumberInStock.ToString(CultureInfo.InvariantCulture));
			}
		}

		protected virtual void AddProductColorNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			string color = "none";

			// tag from groups
			if (productExportInfo.Product != null && productExportInfo.Product.TagGroups != null)
			{
				foreach (ITagGroup tagGroup in productExportInfo.Product.TagGroups)
				{
					if (_tagGroups.ContainsKey(tagGroup.CommonName))
					{
						if (tagGroup.Tags != null && tagGroup.Tags.Count > 0)
						{
							color = tagGroup.Tags[0].Value;
						}
					}
				}
			}

			// Color
			if (!color.Equals("none"))
			{
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeColor, color);
			}
		}

		protected virtual void AddCampaignNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			string campaignName = string.Empty;

			var campaignInfo = (ILekmerProductCampaignInfo) productExportInfo.Product.CampaignInfo;

			if (campaignInfo != null && campaignInfo.CampaignActionsApplied != null && campaignInfo.CampaignActionsApplied.Count > 0)
			{
				var campaignIds = campaignInfo.CampaignActionsApplied.GroupBy(a => a.ProductCampaignId).Select(c => c.First().ProductCampaignId);

				foreach (int campaignId in campaignIds)
				{
					if (_campaigns.ContainsKey(campaignId))
					{
						campaignName += _campaigns[campaignId].Title + ", ";
					}
				}
			}

			int lastCommaPosition = campaignName.LastIndexOf(",", StringComparison.Ordinal);
			if (lastCommaPosition > 0)
			{
				campaignName = campaignName.Substring(0, lastCommaPosition);
			}

			// CampaignName
			if (!string.IsNullOrEmpty(campaignName))
			{
				AddNode(xmlDocument, parentNode, ExporterConstants.NodePromotionName, campaignName);
			}
		}

		protected virtual void AddCategoryUrlsNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			if (_categoryLevels.ContainsKey(ExporterConstants.AllCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CategoryUrl, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}

				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.SubCategoryUrl, productExportInfo.ThirdCategoryLevelUrl); //subCategoryUrl
				}

				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CustomUrl1, productExportInfo.FirstCategoryLevelUrl); // customUrl1 - needs to be last in this feed
				}

				return;
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.SecondCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CategoryUrl, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.ThirdCategoryLevel))
			{
				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.SubCategoryUrl, productExportInfo.ThirdCategoryLevelUrl); //subCategoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.FirstCategoryLevel))
			{
				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CustomUrl1, productExportInfo.FirstCategoryLevelUrl); // customUrl1 - needs to be last in this feed
				}
			}
		}

		protected virtual void AddGenderNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			//TODO: Check if it is ok to use parent category title?
			AddNode(xmlDocument, parentNode, ExporterConstants.NodeGender, productExportInfo.ParentCategoryTitle);
		}

		protected virtual void AddEanNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Heppo has only few EAN codes, so do not need it for Heppo
			if (_isLekmerAplication)
			{
				string eanCode = ExporterConstants.ValueMissing;
				if (!productExportInfo.Product.EanCode.IsEmpty())
				{
					eanCode = productExportInfo.Product.EanCode;
				}
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeEan, eanCode);
			}
		}
	}
}