﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.FileExport.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport.CsvFile
{
	public class MarinProductInfoCsvFile
	{
		private IUserContext _context;
		private readonly string _none = "none";

		private Dictionary<string, ITagGroup> _tagGroups;
		private Dictionary<int, ICampaign> _campaigns;
		private Dictionary<string, string> _categoryLevels;
		private Dictionary<string, IProductImageGroup> _imageGroups;
		private Collection<IImageSize> _imageSizes;

		private string _csvFileDelimiter;
		private bool _isInitialized;
		private bool _isImagesInitialized;
		private string _mediaUrl;

		public void Initialize(Collection<ITagGroup> tagGroups, Collection<ICampaign> campaigns, Dictionary<string, string> categoryLevels, string csvFileDelimiter)
		{
			if (tagGroups == null)
			{
				throw new ArgumentNullException("tagGroups");
			}

			if (campaigns == null)
			{
				throw new ArgumentNullException("campaigns");
			}

			if (categoryLevels == null)
			{
				throw new ArgumentNullException("categoryLevels");
			}

			if (string.IsNullOrEmpty(csvFileDelimiter))
			{
				throw new ArgumentNullException("csvFileDelimiter");
			}

			_tagGroups = tagGroups.ToDictionary(g => g.CommonName);
			_campaigns = campaigns.ToDictionary(c => c.Id);
			_categoryLevels = categoryLevels;
			_csvFileDelimiter = csvFileDelimiter;

			_isInitialized = true;
		}

		public void InitializeImages(Collection<IProductImageGroup> imageGroups, Collection<IImageSize> imageSizes)
		{
			if (imageGroups == null)
			{
				throw new ArgumentNullException("imageGroups");
			}
			if (imageSizes == null)
			{
				throw new ArgumentNullException("imageSizes");
			}

			_imageGroups = imageGroups.ToDictionary(g => g.CommonName);
			_imageSizes = imageSizes;
			_isImagesInitialized = true;
		}

		public void Save(Stream savedSiteMap, IUserContext context, IEnumerable<IProductExportInfo> products)
		{
			if (_isInitialized == false || _isImagesInitialized == false)
			{
				throw new InvalidOperationException("Initialize() and InitializeImages() should be called first.");
			}

			_context = context;
			_mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveExternalUrl(_context.Channel);

			var writer = new StreamWriter(savedSiteMap);

			// write field titles.
			writer.WriteLine(AddHeaderRow());

			foreach (IProductExportInfo productExportInfo in products)
			{
				writer.WriteLine(AddProductRow(productExportInfo));
			}

			writer.Flush();
			writer.Close();
		}

		protected void AddValue(ref string row, string value)
		{
			value = value.Replace(Environment.NewLine, "").Replace("\"", "\"\"");
			row += string.Format("\"{0}\"", value) + _csvFileDelimiter;
		}

		protected string AddHeaderRow()
		{
			string headerRow = string.Empty;

			foreach (FieldNames type in Enum.GetValues(typeof(FieldNames)))
			{
				AddValue(ref headerRow, GetEnumDescription(type));
			}

			int lastDelimiterPosition = headerRow.LastIndexOf(_csvFileDelimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				headerRow = headerRow.Substring(0, lastDelimiterPosition);
			}

			return headerRow;
		}

		protected string AddProductRow(IProductExportInfo productExportInfo)
		{
			string productRow = string.Empty;

			// currency
			AddValue(ref productRow, _context.Channel.Currency.Iso);

			// sku (erpId)
			AddValue(ref productRow, productExportInfo.Product.ErpId);

			// active (If product exists its active by default)
			AddValue(ref productRow, ExporterConstants.NodeStatusTrue);

			//// mainCategory
			//AddValue(ref productRow, productExportInfo.MainCategoryTitle);

			// category
			AddValue(ref productRow, productExportInfo.ParentCategoryTitle);

			// subCategory (eg. Sneakers)
			AddValue(ref productRow, productExportInfo.CategoryTitle);

			// manufacturer
			string manufacturer = productExportInfo.Product.Brand == null || !productExportInfo.Product.Brand.Title.HasValue() ? string.Empty : productExportInfo.Product.Brand.Title;
			AddValue(ref productRow, manufacturer);

			// ean
			string eanCode = productExportInfo.Product.EanCode.IsEmpty() ? ExporterConstants.ValueMissing : productExportInfo.Product.EanCode;
			AddValue(ref productRow, eanCode);

			// modelName
			AddValue(ref productRow, productExportInfo.Product.DisplayTitle);

			// product title (modelName split on "," at the request of Patric)
			AddModelNameSplit(ref productRow, productExportInfo.Product.DisplayTitle);

			// modelnumber (Supplier, Artno)
			string supplierNumber = productExportInfo.Product.LekmerErpId.IsEmpty() ? ExporterConstants.ValueMissing : productExportInfo.Product.LekmerErpId;
			AddValue(ref productRow, supplierNumber);

			// color
			AddColor(ref productRow, productExportInfo);

			// onPromotion
			bool affectedByCampaign = productExportInfo.Product.IsPriceAffectedByCampaign;
			AddValue(ref productRow, affectedByCampaign.ToString().ToLower());

			// promotionName (campaign name)
			AddCampaign(ref productRow, productExportInfo);

			// delivery_time_days
			AddValue(ref productRow, _context.Channel.Currency.Iso.Equals("SEK") ? "1-3" : "3-5");

			// selectMe (We want all products on campaign to be more visible)
			AddValue(ref productRow, affectedByCampaign.ToString().ToLower());

			// priceWithTax
			decimal priceIncludingVat = affectedByCampaign
				? productExportInfo.Product.CampaignInfo.Price.IncludingVat
				: productExportInfo.Product.Price.PriceIncludingVat;
			AddValue(ref productRow, priceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture));

			// originalPriceWithTax
			AddValue(ref productRow, productExportInfo.Product.Price.PriceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture));

			// inStock
			AddValue(ref productRow, (productExportInfo.Product.TotalNumberInStock > 0).ToString().ToLower());

			// freight
			AddFreight(ref productRow, priceIncludingVat);

			// Description
			AddValue(ref productRow, productExportInfo.Product.Description);

			//// QuantityForSale
			//AddValue(ref productRow, productExportInfo.Product.TotalNumberInStock.ToString(CultureInfo.InvariantCulture));

			// ProductUrl
			string productUrl = "http://" + _context.Channel.ApplicationName + "/" + productExportInfo.Product.LekmerUrl;
			AddValue(ref productRow, productUrl);

			// CategoryUrlsNode
			AddCategoryUrls(ref productRow, productExportInfo);

			// CustomUrl3 - Brand  //brand Url
			AddValue(ref productRow, productExportInfo.BrandUrl.HasValue() ? productExportInfo.BrandUrl : string.Empty);

			// Default image with sizes
			AddProductImages(ref productRow, productExportInfo.Product.Image);

			//TODO GroupImages

			int lastDelimiterPosition = productRow.LastIndexOf(_csvFileDelimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				productRow = productRow.Substring(0, lastDelimiterPosition);
			}

			return productRow;
		}

		protected virtual void AddModelNameSplit(ref string row, string displayTitle)
		{
			var titles = displayTitle.Split(',');

			int i = 0;
			foreach (var title in titles)
			{
				if (i >= 3) break;
				AddValue(ref row, title.Trim());
				i++;
			}

			if (i < 3)
			{
				for (int j = 0; j < 3 - i; j++)
				{
					AddValue(ref row, string.Empty);
				}
			}
		}

		protected virtual void AddColor(ref string row, IProductExportInfo productExportInfo)
		{
			string color = _none;

			// tag from tag groups.
			if (productExportInfo.Product != null && productExportInfo.Product.TagGroups != null)
			{
				foreach (ITagGroup tagGroup in productExportInfo.Product.TagGroups)
				{
					if (_tagGroups.ContainsKey(tagGroup.CommonName))
					{
						if (tagGroup.Tags != null && tagGroup.Tags.Count > 0)
						{
							color = tagGroup.Tags[0].Value;
						}
					}
				}
			}

			AddValue(ref row, !color.Equals(_none) ? color : string.Empty);
		}

		protected virtual void AddCampaign(ref string row, IProductExportInfo productExportInfo)
		{
			string campaignName = string.Empty;

			var campaignInfo = (ILekmerProductCampaignInfo) productExportInfo.Product.CampaignInfo;
			if (campaignInfo != null && campaignInfo.CampaignActionsApplied != null && campaignInfo.CampaignActionsApplied.Count > 0)
			{
				var campaignIds = campaignInfo.CampaignActionsApplied.GroupBy(a => a.ProductCampaignId).Select(c => c.First().ProductCampaignId);
				foreach (int campaignId in campaignIds)
				{
					if (_campaigns.ContainsKey(campaignId))
					{
						campaignName += _campaigns[campaignId].Title + ", ";
					}
				}
			}

			int lastCommaPosition = campaignName.LastIndexOf(",", StringComparison.Ordinal);
			if (lastCommaPosition > 0)
			{
				campaignName = campaignName.Substring(0, lastCommaPosition);
			}

			AddValue(ref row, !string.IsNullOrEmpty(campaignName) ? campaignName : string.Empty);
		}

		protected virtual void AddFreight(ref string row, decimal priceIncludingVat)
		{
			string cost = priceIncludingVat > 1000 ? "0.00" : "49.00";

			//if (_context.Channel.Currency.Iso.Equals("SEK"))
			//{
			//	cost = priceIncludingVat > 1000 ? "0.00" : "39.00"; //Schenker
			//}

			if (_context.Channel.Country.Iso.Equals("FI"))
			{
				cost = priceIncludingVat > 100 ? "0.00" : "4.90";
			}

			if (_context.Channel.Country.Iso.Equals("NL"))
			{
				cost = priceIncludingVat > 100 ? "0.00" : "2.90";
			}

			AddValue(ref row, cost);
		}

		protected virtual void AddCategoryUrls(ref string row, IProductExportInfo productExportInfo)
		{
			if (_categoryLevels.ContainsKey(ExporterConstants.AllCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddValue(ref row, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}

				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddValue(ref row, productExportInfo.ThirdCategoryLevelUrl); // subCategoryUrl
				}

				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddValue(ref row, productExportInfo.FirstCategoryLevelUrl); // customUrl1 - needs to be last in this feed
				}

				return;
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.SecondCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddValue(ref row, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.ThirdCategoryLevel))
			{
				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddValue(ref row, productExportInfo.ThirdCategoryLevelUrl); // subCategoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.FirstCategoryLevel))
			{
				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddValue(ref row, productExportInfo.FirstCategoryLevelUrl); // customUrl1 - needs to be last in this feed
				}
			}
		}

		protected virtual void AddProductImages(ref string row, IImage image)
		{
			if (image != null)
			{
				string imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, image);
				AddValue(ref row, imageUrl);

				foreach (IImageSize imageSize in _imageSizes)
				{
					string imageSizeUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(_mediaUrl, image, imageSize.CommonName);
					AddValue(ref row, imageSizeUrl);
				}
			}
			else
			{
				AddValue(ref row, string.Empty);
				foreach (IImageSize imageSize in _imageSizes)
				{
					AddValue(ref row, string.Empty);
				}
			}
		}

		protected virtual string GetEnumDescription(Enum value)
		{
			FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
			var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return attributes.Length > 0 ? attributes[0].Description : value.ToString();
		}
	}
}