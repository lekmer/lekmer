using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Maksuturva.Mapper
{
	public class MaksuturvaCompensationErrorDataMapper : DataMapperBase<IMaksuturvaCompensationError>
	{
		public MaksuturvaCompensationErrorDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IMaksuturvaCompensationError Create()
		{
			var maksuturvaCompensationError = IoC.Resolve<IMaksuturvaCompensationError>();

			maksuturvaCompensationError.Id = MapValue<int>("MaksuturvaCompensationError.MaksuturvaCompensationErrorId");
			maksuturvaCompensationError.ChannelCommonName = MapValue<string>("MaksuturvaCompensationError.ChannelCommonName");
			maksuturvaCompensationError.FetchDate = MapValue<DateTime>("MaksuturvaCompensationError.FetchDate");
			maksuturvaCompensationError.LastAttempt = MapNullableValue<DateTime?>("MaksuturvaCompensationError.LastAttempt");

			return maksuturvaCompensationError;
		}
	}
}