﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Maksuturva.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class MaksuturvaPendingOrderService : IMaksuturvaPendingOrderService
	{
		protected MaksuturvaPendingOrderRepository Repository { get; private set; }

		public MaksuturvaPendingOrderService(MaksuturvaPendingOrderRepository repository)
		{
			Repository = repository;
		}

		public IMaksuturvaPendingOrder Create()
		{
			return IoC.Resolve<IMaksuturvaPendingOrder>();
		}

		public virtual int Insert(IMaksuturvaPendingOrder maksuturvaPendingOrder)
		{
			Repository.EnsureNotNull();
			return Repository.Insert(maksuturvaPendingOrder);
		}

		public virtual void Update(IMaksuturvaPendingOrder maksuturvaPendingOrder)
		{
			Repository.EnsureNotNull();
			Repository.Update(maksuturvaPendingOrder);
		}

		public virtual void Delete(int orderId)
		{
			Repository.EnsureNotNull();
			Repository.Delete(orderId);
		}

		public virtual IMaksuturvaPendingOrder Get(int orderId)
		{
			Repository.EnsureNotNull();
			return Repository.Get(orderId);
		}

		public virtual Collection<IMaksuturvaPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			Repository.EnsureNotNull();
			return Repository.GetPendingOrdersForStatusCheck(checkToDate);
		}
	}
}