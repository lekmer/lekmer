﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Maksuturva.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class MaksuturvaCompensationErrorService : IMaksuturvaCompensationErrorService
	{
		protected MaksuturvaCompensationErrorRepository Repository { get; private set; }

		public MaksuturvaCompensationErrorService(MaksuturvaCompensationErrorRepository repository)
		{
			Repository = repository;
		}

		public IMaksuturvaCompensationError Create(string channelCommonName, DateTime fetchDate)
		{
			var maksuturvaCompensationError = IoC.Resolve<IMaksuturvaCompensationError>();
			maksuturvaCompensationError.ChannelCommonName = channelCommonName;
			maksuturvaCompensationError.FetchDate = fetchDate;
			return maksuturvaCompensationError;
		}

		public virtual void Insert(IMaksuturvaCompensationError maksuturvaCompensationError)
		{
			Repository.EnsureNotNull();
			Repository.Insert(maksuturvaCompensationError);
		}

		public virtual void Update(IMaksuturvaCompensationError maksuturvaCompensationError)
		{
			Repository.EnsureNotNull();
			Repository.Update(maksuturvaCompensationError);
		}

		public virtual void Delete(int id)
		{
			Repository.EnsureNotNull();
			Repository.Delete(id);
		}

		public virtual Collection<IMaksuturvaCompensationError> Get()
		{
			Repository.EnsureNotNull();
			return Repository.Get();
		}
	}
}