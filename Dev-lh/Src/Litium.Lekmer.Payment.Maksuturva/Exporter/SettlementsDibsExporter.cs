﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Xml;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using log4net;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class SettlementsDibsExporter : IExporter
	{
		private static readonly string _channelCommonName = "Finland";
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IMaksuturvaSetting MaksuturvaSetting { get; private set; }
		protected IMaksuturvaPaymentProvider MaksuturvaPaymentProvider { get; private set; }
		protected IMaksuturvaCompensationErrorService CompensationErrorService { get; private set; }

		public SettlementsDibsExporter(IMaksuturvaSetting maksuturvaSetting, IMaksuturvaPaymentProvider maksuturvaPaymentProvider, IMaksuturvaCompensationErrorService compensationErrorService)
		{
			MaksuturvaSetting = maksuturvaSetting;
			MaksuturvaPaymentProvider = maksuturvaPaymentProvider;
			CompensationErrorService = compensationErrorService;

			MaksuturvaSetting.Initialize(_channelCommonName);
		}

		public void Execute()
		{
			_log.Info("Execution started.");
			ExecuteSettlementsDibsExport();
			_log.Info("Execution complited.");

			_log.Info("Execution compensation errors started.");
			ExecuteCompensationErrorExport();
			_log.Info("Execution compensation errors complited.");
		}

		protected virtual void ExecuteSettlementsDibsExport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			DateTime fetchDate = GetFetchDate();
			string filePath = GetFilePath(fetchDate);
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			_log.InfoFormat("File preparing... //{0}", filePath);

			_log.Info("Get compensation by time interval...");
			var compensationFile = MaksuturvaPaymentProvider.GetCompensationByTimeInterval(_channelCommonName, fetchDate);
			if (string.IsNullOrEmpty(compensationFile))
			{
				stopwatch.Stop();
				_log.InfoFormat("GetCompensationsByTimeInterval interface returns empty response for date - {0}", fetchDate);
				CompensationErrorService.Insert(CompensationErrorService.Create(_channelCommonName, fetchDate));
				return;
			}
			
			var doc = new XmlDocument();
			using (Stream stream = File.OpenWrite(filePath))
			{
				doc.LoadXml(compensationFile);
				doc.Save(stream);
			}

			_log.InfoFormat("File saved... //{0}", filePath);

			XmlNode node = doc.SelectSingleNode("bunchreport/error");
			if (node != null)
			{
				stopwatch.Stop();
				_log.InfoFormat("GetCompensationsByTimeInterval interface returns error response for date - {0}", fetchDate);
				CompensationErrorService.Insert(CompensationErrorService.Create(_channelCommonName, fetchDate));
				return;
			}

			_log.Info("File start uploaded to FTP...");
			UploadToFtp(filePath, fetchDate);

			stopwatch.Stop();
			_log.InfoFormat("Elapsed time - {0}", stopwatch.Elapsed);
		}

		protected virtual void ExecuteCompensationErrorExport()
		{
			var compensationErrors = CompensationErrorService.Get();
			foreach (var compensationError in compensationErrors)
			{
				var fetchDate = compensationError.FetchDate;

				string filePath = GetFilePath(fetchDate);
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}

				_log.InfoFormat("File preparing... //{0}", filePath);

				var compensationFile = MaksuturvaPaymentProvider.GetCompensationByTimeInterval(compensationError.ChannelCommonName, fetchDate);
				if (string.IsNullOrEmpty(compensationFile))
				{
					compensationError.LastAttempt = DateTime.Now;
					CompensationErrorService.Update(compensationError);
					continue;
				}

				var doc = new XmlDocument();
				using (Stream stream = File.OpenWrite(filePath))
				{
					doc.LoadXml(compensationFile);
					doc.Save(stream);
				}

				_log.InfoFormat("File saved... //{0}", filePath);

				XmlNode node = doc.SelectSingleNode("bunchreport/error");
				if (node != null)
				{
					compensationError.LastAttempt = DateTime.Now;
					CompensationErrorService.Update(compensationError);
					continue;
				}

				_log.Info("File start uploaded to FTP...");
				UploadToFtp(filePath, fetchDate);

				CompensationErrorService.Delete(compensationError.Id);
			}
		}

		protected virtual DateTime GetFetchDate()
		{
			int daysAfterOrder = MaksuturvaSetting.DaysAfterOrder;
			return DateTime.Now.AddDays((-1)*daysAfterOrder);
		}
		protected virtual string GetFilePath(DateTime date)
		{
			string dirr = MaksuturvaSetting.DestinationDirectory;
			string fileName = string.Format(MaksuturvaSetting.FileName, date.ToString("yyyyMMdd"));
			return Path.Combine(dirr, fileName);
		}
		protected virtual void UploadToFtp(string filePath, DateTime date)
		{
			try
			{
				string fileName = string.Format(MaksuturvaSetting.FileName, date.ToString("yyyyMMdd"));

				var ftpFileUrl = string.Format(MaksuturvaSetting.FtpUrl, fileName);
				var username = MaksuturvaSetting.Username;
				var password = MaksuturvaSetting.Password;

				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpFileUrl));
				ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
				ftpRequest.Proxy = null;
				ftpRequest.UseBinary = true;
				ftpRequest.Credentials = new NetworkCredential(username, password);

				var fileInfo = new FileInfo(filePath);
				var fileContents = new byte[fileInfo.Length];

				using (FileStream stream = fileInfo.OpenRead())
				{
					stream.Read(fileContents, 0, Convert.ToInt32(fileInfo.Length));
				}

				using (Stream writer = ftpRequest.GetRequestStream())
				{
					writer.Write(fileContents, 0, fileContents.Length);
				}
			}
			catch (WebException webex)
			{
				_log.Error(webex);
			}
		}
	}
}