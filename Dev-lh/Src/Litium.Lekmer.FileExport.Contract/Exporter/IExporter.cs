﻿namespace Litium.Lekmer.FileExport.Exporter
{
	public interface IExporter
	{
		void Execute();
	}
}
