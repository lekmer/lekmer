using Litium.Scensum.Core;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListSecureService
	{
		IBlockBrandTopList Create();

		int Save(ISystemUserFull systemUserFull, IChannel channel, IBlockBrandTopList block);

		IBlockBrandTopList GetById(int id);
	}
}