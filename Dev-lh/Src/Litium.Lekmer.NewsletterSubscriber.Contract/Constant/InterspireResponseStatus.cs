﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public enum InterspireResponseStatus
	{
		Unknown = 0,
		Success = 1,
		Error = 2,
		Failed = 3,
		BadWebResponse = 4
	}
}