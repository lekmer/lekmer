﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireResponseAddSubscriber : IInterspireResponse
	{
		string ContactsIdNumber { get; set; }
	}
}