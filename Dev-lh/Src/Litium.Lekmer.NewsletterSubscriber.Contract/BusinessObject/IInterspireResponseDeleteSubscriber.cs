﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireResponseDeleteSubscriber : IInterspireResponse
	{
		int RemovedSubscribers { get; set; }
	}
}