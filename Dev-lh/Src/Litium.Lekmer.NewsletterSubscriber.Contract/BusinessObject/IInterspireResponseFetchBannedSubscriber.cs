﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireResponseFetchBannedSubscriber : IInterspireResponse
	{
		IInterspireBanList BanList { get; set; }
	}
}