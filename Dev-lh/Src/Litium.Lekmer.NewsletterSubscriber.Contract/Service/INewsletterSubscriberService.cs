﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterSubscriberService
	{
		INewsletterSubscriber Create(IUserContext context);
		int Save(IUserContext context, INewsletterSubscriber subscriber);
		bool Delete(IUserContext context, string email);

		void UpdateFromOrders();
		void SetSentStatus(int subscriberId);
		Collection<INewsletterSubscriber> GetAllWithNotSentStatus();
	}
}