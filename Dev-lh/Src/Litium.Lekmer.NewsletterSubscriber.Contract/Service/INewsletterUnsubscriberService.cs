﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriberService
	{
		INewsletterUnsubscriber GetByEmail(IUserContext context, string email);
		INewsletterUnsubscriber Create(IUserContext context);
		Collection<INewsletterUnsubscriber> GetAllWithNotSentStatus();
		int Save(IUserContext context, INewsletterUnsubscriber unsubscriber);
		void SetSentStatus(int unsubscriberId);
		bool Delete(IUserContext context, string email);
	}
}