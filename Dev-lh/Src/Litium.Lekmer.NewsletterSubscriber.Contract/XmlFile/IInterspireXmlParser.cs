﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireXmlParser
	{
		IInterspireResponseAddSubscriber ParseAddSubscriberResponse(string response);
		IInterspireResponseDeleteSubscriber ParseDeleteSubscriberResponse(string response);
		IInterspireResponseUnsubscribeSubscriber ParseUnsubscribeSubscriberResponse(string response);
		IInterspireResponseGetLists ParseGetListsResponse(string response);
		IInterspireResponseIsContactOnList ParseIsContactOnListResponse(string response);
		IInterspireResponse ParseAddBannedSubscriberResponse(string response);
		IInterspireResponseFetchBannedSubscriber ParseFetchBannedSubscriberResponse(string response, string emailAddress);
		IInterspireResponse ParseRemoveBannedSubscriberResponse(string response);
	}
}