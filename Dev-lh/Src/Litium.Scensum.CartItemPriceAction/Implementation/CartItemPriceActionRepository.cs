using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.CartItemPriceAction.Repository
{
	public class CartItemPriceActionRepository
	{
		protected virtual DataMapperBase<ICartItemPriceAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemPriceAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}

		public virtual ICartItemPriceAction GetById(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartActionId", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartItemPriceActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(ICartItemPriceAction cartAction)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartActionId", cartAction.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MaxQuantity", cartAction.MaxQuantity, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IncludeAllProducts", cartAction.IncludeAllProducts, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.Save");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartActionId", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceActionDelete]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int cartActionId)
		{
			var productIds = new ProductIdDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.GetIncludeProductsSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartItemPriceActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}
			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int cartActionId)
		{
			var productIds = new ProductIdDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.GetExcludeProductsSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartItemPriceActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}
			return productIds;
		}

		public virtual CategoryIdDictionary GetIncludeCategories(int cartActionId)
		{
			var categories = new CategoryIdDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.GetIncludeCategoriesSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartItemPriceActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0));
				}
			}
			return categories;
		}

		public virtual CategoryIdDictionary GetExcludeCategories(int cartActionId)
		{
			var categories = new CategoryIdDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.GetExcludeCategoriesSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartItemPriceActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0));
				}
			}
			return categories;
		}

		public virtual void DeleteIncludeProducts(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void InsertIncludeProduct(int cartActionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartItemPriceActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int cartActionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartItemPriceActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertIncludeCategory(int cartActionId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartItemPriceActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int cartActionId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartItemPriceActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCartItemPricesByAction(int cartActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.GetCartItemPricesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartItemPricesGetByActionId]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}
			var dictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValue in currencyValues)
			{
				dictionary.Add(currencyValue.Currency.Id, currencyValue.MonetaryValue);
			}
			return dictionary;
		}

		public virtual void SaveCartItemPrice(int cartActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", cartActionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CurrencyId", currencyValue.Key, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Value", currencyValue.Value, SqlDbType.Decimal)
			};
			var dbSettings = new DatabaseSetting("CartItemPriceActionRepository.SaveCartItemPrice");
			new DataHandler().ExecuteCommand("[addon].[pCartItemPriceSave]", parameters, dbSettings);
		}
	}
}