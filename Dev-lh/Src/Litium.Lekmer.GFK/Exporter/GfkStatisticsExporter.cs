﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using Litium.Lekmer.GFK.Contract;
using Litium.Scensum.Core;
using Renci.SshNet;
using log4net;

namespace Litium.Lekmer.GFK
{
	public class GfkStatisticsExporter : IExporter
	{
		private readonly string _dateFormat = "yyyy-MM-dd";
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IGfkSetting Setting { get; private set; }
		protected IGfkStatisticsExportService GfkStatisticsExportService { get; private set; }

		public GfkStatisticsExporter(IGfkSetting gfkSetting, IGfkStatisticsExportService gfkStatisticsExportService)
		{
			Setting = gfkSetting;
			GfkStatisticsExportService = gfkStatisticsExportService;
		}

		public void Execute()
		{
			_log.Info("Execution started.");
			ExecuteGfkStatisticsExport();
			_log.Info("Execution complited.");
		}

		protected virtual void ExecuteGfkStatisticsExport()
		{
			DateTime startDate;
			DateTime endDate;
			GetDates(out startDate, out endDate);
			var generalFileName = GetGeneralFileName(startDate);

			var userContexts = GfkStatisticsExportService.GetUserContextForAllChannels();
			foreach (IUserContext context in userContexts)
			{
				Stopwatch stopwatch = Stopwatch.StartNew();

				string fileName = GetFileName(context.Channel.CommonName, generalFileName);
				string filePath = Path.Combine(Setting.DestinationDirectory, fileName);
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}

				_log.InfoFormat("File preparing... //{0}", filePath);

				_log.InfoFormat("Get orders from {0} to {1}...", startDate, endDate);
				Dictionary<string, IGfkInfo> gfkInfoCollection = GfkStatisticsExportService.GetGfkInfoCollectionToExport(context, startDate.ToString(_dateFormat), endDate.ToString(_dateFormat));

				if (gfkInfoCollection.Count <= 0)
				{
					stopwatch.Stop();
					_log.InfoFormat("There are no orders for channel {0}", context.Channel.CommonName);
					continue;
				}

				using (Stream stream = File.OpenWrite(filePath))
				{
					var gfkFile = new GfkFile();
					gfkFile.Initialize(Setting, gfkInfoCollection, context.Channel.Currency.Iso);
					gfkFile.Save(stream);
				}

				_log.InfoFormat("File saved... //{0}", filePath);

				_log.Info("File start uploaded to FTP...");
				UploadToFtp(fileName, filePath);

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}
		}

		protected virtual void GetDates(out DateTime startDate, out DateTime endDate)
		{
			var currentDate = DateTime.Now;
			startDate = currentDate.AddDays(Setting.MaxDaysAfterPurchase * (-1));
			endDate = currentDate.AddDays(Setting.MinDaysAfterPurchase * (-1));
		}
		protected virtual string GetGeneralFileName(DateTime date)
		{
			string week = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(date, CalendarWeekRule.FirstDay, DayOfWeek.Monday).ToString("D2", CultureInfo.InvariantCulture);
			string year = date.ToString("yy");
			GfkStatisticsExportService.Period = year + week;

			return Setting.FileName.Replace("[year]", year).Replace("[week]", week);
		}
		protected virtual string GetFileName(string channelCommonName, string fileName)
		{
			string channelName = Setting.GetChannelNameReplacement(channelCommonName);

			return fileName.Replace("[ChannelName]", channelName);
		}
		protected virtual void UploadToFtp(string fileName, string filePath)
		{
			try
			{
				var ftpUrl = Setting.FtpUrl;
				var ftpDirectory = Setting.FtpDirectory;
				var port = Setting.Port;
				var username = Setting.Username;
				var password = Setting.Password;
				using (var client = new SftpClient(ftpUrl, port, username, password))
				{
					client.Connect();
					client.ChangeDirectory(ftpDirectory);
					using (var fileStream = new FileStream(filePath, FileMode.Open))
					{
						client.BufferSize = 4 * 1024;
						client.UploadFile(fileStream, fileName);
					}
				}
			}
			catch (Exception ex)
			{
				_log.ErrorFormat("Error: {0}", ex);
			}
		}
	}
}