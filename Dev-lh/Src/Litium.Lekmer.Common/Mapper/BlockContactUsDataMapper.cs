﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Common.Mapper
{
	public class BlockContactUsDataMapper : DataMapperBase<IBlockContactUs>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockContactUsDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockContactUs Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockContactUs = IoC.Resolve<IBlockContactUs>();
			block.ConvertTo(blockContactUs);
			blockContactUs.Receiver = MapValue<string>("BlockContactUs.Receiver");
			return blockContactUs;
		}
	}
}
