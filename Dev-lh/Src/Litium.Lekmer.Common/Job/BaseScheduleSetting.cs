﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Common.Job
{
	public abstract class BaseScheduleSetting : SettingBase, IScheduleSetting
	{
		protected abstract string GroupName { get; }


		public bool UseWeeklyExecution
		{
			get { return GetBoolean(GroupName, "UseWeeklyExecution", false); }
		}

		public int WeeklyExecutionHour
		{
			get { return GetInt32(GroupName, "WeeklyExecutionHour", 0); }
		}

		public int WeeklyExecutionMinute
		{
			get { return GetInt32(GroupName, "WeeklyExecutionMinute", 0); }
		}

		public bool WeeklyExecutionMonday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionMonday", false); }
		}

		public bool WeeklyExecutionTuesday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionTuesday", false); }
		}

		public bool WeeklyExecutionWednesday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionWednesday", false); }
		}

		public bool WeeklyExecutionThursday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionThursday", false); }
		}

		public bool WeeklyExecutionFriday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionFriday", false); }
		}

		public bool WeeklyExecutionSaturday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionSaturday", false); }
		}

		public bool WeeklyExecutionSunday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionSunday", false); }
		}


		public bool UseDailyExecution
		{
			get { return GetBoolean(GroupName, "UseDailyExecution", false); }
		}

		public int DailyExecutionHour
		{
			get { return GetInt32(GroupName, "DailyExecutionHour", 0); }
		}

		public int DailyExecutionMinute
		{
			get { return GetInt32(GroupName, "DailyExecutionMinute", 0); }
		}


		public bool UseRepeatExecution
		{
			get { return GetBoolean(GroupName, "UseRepeatExecution", false); }
		}

		public int RepeatExecutionIntervalMinutes
		{
			get { return GetInt32(GroupName, "RepeatExecutionIntervalMinutes", 0); }
		}

		public int RepeatExecutionIntervalHours
		{
			get { return GetInt32(GroupName, "RepeatExecutionIntervalHours", 0); }
		}

		public int RepeatExecutionStartMinute
		{
			get { return GetInt32(GroupName, "RepeatExecutionStartMinute", 0); }
		}

		public int RepeatExecutionStartHour
		{
			get { return GetInt32(GroupName, "RepeatExecutionStartHour", 0); }
		}


		public bool ExecuteOnStartUp
		{
			get { return GetBoolean(GroupName, "ExecuteOnStartUp", false); }
		}
	}
}