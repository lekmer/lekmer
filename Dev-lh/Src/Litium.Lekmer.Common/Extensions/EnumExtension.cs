﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Reflection;

namespace Litium.Lekmer.Common.Extensions
{
	public static class EnumExtension
	{
		private static void CheckIsEnum<T>(bool withFlags)
		{
			if (!typeof(T).IsEnum)
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Type '{0}' is not an enum", typeof(T).FullName));
			if (withFlags && !Attribute.IsDefined(typeof(T), typeof(FlagsAttribute)))
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Type '{0}' doesn't have the 'Flags' attribute", typeof(T).FullName));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flag")]
		public static bool IsFlagSet<T>(this T value, T option) where T : struct
		{
			CheckIsEnum<T>(true);
			long lValue = Convert.ToInt64(value, CultureInfo.InvariantCulture);
			long lFlag = Convert.ToInt64(option, CultureInfo.InvariantCulture);
			return (lValue & lFlag) != 0;
		}

		public static string GetEnumDescription(this Enum value)
		{
			FieldInfo fieldInfo = value.GetType().GetField(value.ToString());
			var attributes = (DescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), false);
			return attributes.Length > 0 ? attributes[0].Description : value.ToString();
		}
	}
}