﻿using System;

namespace Litium.Lekmer.Common.Extensions
{
	public static class DecimalExtensions
	{
		public static decimal Round(this decimal value)
		{
			return Math.Round(value, 2, MidpointRounding.AwayFromZero);
		}

		public static decimal RoundToInteger(this decimal value)
		{
			return Math.Round(value, MidpointRounding.AwayFromZero);
		}
	}
}