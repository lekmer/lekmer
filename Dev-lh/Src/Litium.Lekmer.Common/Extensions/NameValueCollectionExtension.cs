﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace Litium.Lekmer.Common.Extensions
{
	public static class NameValueCollectionExtension
	{
		public static bool GetBooleanValue(this NameValueCollection collection, string variableName, string trueLiteral)
		{
			string rawValue = collection[variableName];

			if (rawValue.IsEmpty())
			{
				return false;
			}

			if (rawValue.Equals("on", StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}

			return rawValue.Equals(trueLiteral, StringComparison.OrdinalIgnoreCase);
		}

		public static Collection<string> GetStringValues(this NameValueCollection collection, string variableName)
		{
			string rawValue = collection[variableName];

			if (rawValue.IsEmpty())
			{
				return new Collection<string>();
			}

			return new Collection<string>(rawValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).DefaultIfEmpty().ToArray());
		}
	}
}
