﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Litium.Lekmer.Common
{
	public static class ValidationUtil
	{
		#region Regex

		private static string _emailValidationPattern = @"^([-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
		private static string _postalCodeNlValidationPattern = @"^[1-9][0-9]{3}[- ]?[a-zA-Z]{2}$";

		private const string _doorCodeGeneralPattern = @"^[0-9]+$";
		private const string _doorCodeExtendedGeneralPattern = @"^[*0-9]+$";

		private const string _ipValidationPattern = @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}";
		private const string _urlValidationPattern = @"(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";
		private const string _phoneNumberValidationPattern = @"^(\+(\d{1,3}\s*)|)\d{1,4}(-|/|\s|)(\d\s*){5,10}$"; //Swedish 

		private const string _svPostalNumberValidationPattern = @"^[1-9]\d{4}$"; //Swedish
		private const string _noPostalNumberValidationPattern = @"^\d{4}$"; //Norway
		private const string _dkPostalNumberValidationPattern = @"^\d{4}$"; //Denmark
		private const string _fiPostalNumberValidationPattern = @"^\d{5}$"; //Finland
		private const string _nlPostalNumberValidationPattern = @"^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$"; //Netherlands
		private const string _frPostalNumberValidationPattern = @"^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$"; //France

		//yyyymmdd-XXXX
		//private const string civicNumberValidationPatternSwedish = //	@"^(?<date>(?<year>\d{4})(?<month>\d{2})(?<day>\d{2}))[-\+]?(?<additional>\d{3})(?<checkdigit>\d{1})$"; //Swedish 
		private const string _civicNumberValidationPatternSwedish = @"^(\b(?:19|20)?\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[-+]?\d{4}\b)$"; //Swedish
		//ddmmyy-XXXX
		private const string _civicNumberValidationPatternDanish = @"^(?<date>(?<day>\d{2})(?<month>\d{2})(?<year>\d{2}))[-\+]?(?<additional>\d{3})(?<checkdigit>\d{1})$"; //Danish 
		//ddmmyy-XXXXX
		private const string _civicNumberValidationPatternNorwegian = @"^(?<date>(?<day>\d{2})(?<month>\d{2})(?<year>\d{2}))[-\+]?(?<additional>\d{5})$"; //Norwegian
		//ddmmyy-XXXX
		private const string _civicNumberValidationPatternFinnish = @"^(?<date>(?<day>\d{2})(?<month>\d{2})(?<year>\d{2}))[-\+]?(?<additional>\d{3})(?<checkdigit>[A-Z\d]{1})$"; //Finnish 
		private const string _civicNumberValidationPatternEU = @"^(?<date>(?<year>\d{4})(?<month>\d{2})(?<day>\d{2}))$"; //EU
		private const string _civicNumberValidationPatternDate = @"^(?<year>\d{4})(?<month>\d{2})(?<day>\d{2})$";
		private const string _organizationNumberValidationPattern = @"^\d{6}[-]\d{4}$"; //Swedish 

		#endregion

		/// <summary>
		/// Validates if the input string is a valid e-mail address.
		/// </summary>
		/// <param name="input">An e-mail address to be validated.</param>
		/// <returns>True if the input is a valid e-mail address, false if not.</returns>
		public static bool IsValidEmail(string input)
		{
			if (Regex.Match(input, _emailValidationPattern).Success == false)
			{
				return false;
			}

			/*
				The domain can be bracketed, unbracketed, or an IP address.
				An unbracketed domain consists of labels separated by periods and less than 253 characters.
				-Labels must be less than 63 characters.
				-Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
				A bracketed domain starts with [, ends with ] and the contents (after unescaping) must be less than 245 characters.
				-Labels must be less than 63 characters.
				-Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
			 * 
			 * Go to http://rumkin.com/software/email/rules.php for more details.
			*/

			string domain = input.Substring(input.IndexOf("@", StringComparison.OrdinalIgnoreCase) + 1);

			// No domain can start with a period, end with a period, or have two successive periods.
			if (domain.StartsWith(".", StringComparison.OrdinalIgnoreCase) || domain.EndsWith(".", StringComparison.OrdinalIgnoreCase) || domain.Contains(".."))
			{
				return false;
			}

			domain = domain.TrimStart('[').TrimEnd(']');

			// Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
			if (domain.Contains("--"))
			{
				return false;
			}

			// Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
			foreach (string label in domain.Split('.'))
			{
				if (label.StartsWith("-", StringComparison.OrdinalIgnoreCase) || label.EndsWith("-", StringComparison.OrdinalIgnoreCase))
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsHouseDetailsRequired(string countryIso)
		{
			return countryIso.ToUpperInvariant() == "NL";
		}

		public static bool IsHouseNumberRequired(string countryIso)
		{
			return countryIso.ToUpperInvariant() == "NL";
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "countryIso")]
		public static bool IsHouseExtensionRequired(string countryIso)
		{
			// House extension could be empty in NL, other channels don't need it
			return false;
		}

		public static bool IsGenderTypeRequired(string countryIso)
		{
			return countryIso.ToUpperInvariant() == "NL";
		}

		public static bool IsValidGenderType(int genderTypeId)
		{
			return genderTypeId == (int)GenderTypeInfo.Unknown || genderTypeId == (int)GenderTypeInfo.Man || genderTypeId == (int)GenderTypeInfo.Woman;
		}

		/// <summary>
		/// Validates if the input string is a valid postal code.
		/// </summary>
		/// <param name="countryIso">A country iso for which need to validate input</param>
		/// <param name="input">A postal code to be validated.</param>
		/// <returns>True if the input is a valid postal code, false if not.</returns>
		public static bool IsValidPostalCode(string countryIso, string input)
		{
			return countryIso.ToUpperInvariant() != "NL" || Regex.Match(input, _postalCodeNlValidationPattern).Success;
		}

		/// <summary>
		/// Validates if the input string is a valid door code.
		/// </summary>
		/// <param name="countryIso">A country iso for which need to validate input</param>
		/// <param name="input">A door code to be validated.</param>
		/// <param name="channelsThatApplyStarSymbol">List of Channels that apply (*) for door code</param>
		/// <returns>True if the input is a valid door code, false if not.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
		public static bool IsValidDoorCode(string countryIso, string input, List<string> channelsThatApplyStarSymbol)
		{
			return channelsThatApplyStarSymbol != null && channelsThatApplyStarSymbol.Contains(countryIso)
				? Regex.Match(input, _doorCodeExtendedGeneralPattern).Success
				: Regex.Match(input, _doorCodeGeneralPattern).Success;
		}


		/// <summary>
		/// Validates if the input string is a valid ip address.
		/// </summary>
		/// <param name="input">An ip address to validate.</param>
		/// <returns>True if the input is a valid ip address, false if not.</returns>
		public static bool IsValidIP(string input)
		{
			if (input == null) throw new ArgumentNullException("input");

			// Regexp check.
			if (!Regex.Match(input, _ipValidationPattern).Success)
			{
				return false;
			}

			// Check parts so they are between 0 and 255.
			string[] ipParts = input.Split('.');

			foreach (string part in ipParts)
			{
				int intPart = int.Parse(part, CultureInfo.InvariantCulture);

				if (intPart < 0 || intPart > 255)
				{
					return false;
				}
			}

			// Ip is valid!
			return true;
		}

		public static bool IsValidUrl(string input)
		{
			return Regex.Match(input, _urlValidationPattern).Success;
		}

		public static bool IsValidPostalNumber(string input, CountryIso country)
		{
			string postalNumberMatchPattern;
			switch (country)
			{
				case CountryIso.SE:
					postalNumberMatchPattern = _svPostalNumberValidationPattern;
					break;
				case CountryIso.NO:
					postalNumberMatchPattern = _noPostalNumberValidationPattern;
					break;
				case CountryIso.DK:
					postalNumberMatchPattern = _dkPostalNumberValidationPattern;
					break;
				case CountryIso.FI:
					postalNumberMatchPattern = _fiPostalNumberValidationPattern;
					break;
				case CountryIso.NL:
					postalNumberMatchPattern = _nlPostalNumberValidationPattern;
					break;
				case CountryIso.FR:
					postalNumberMatchPattern = _frPostalNumberValidationPattern;
					break;
				default:
					// IF we don't have a validation pattern return true for every string.
					return true;
			}
			return Regex.Match(input, postalNumberMatchPattern).Success;
		}

		/// <summary>
		/// Validates a civic number.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="country"></param>
		public static bool IsValidCivicNumber(string input, CountryIso country)
		{
			switch (country)
			{
				case CountryIso.SE:
					bool isValid = IsValidSwedishCivicNumber(input);
					if (!isValid)
					{
						isValid = IsValidOrganizationNumber(input);
					}
					return isValid;
				case CountryIso.NO:
					return IsValidNorwegianCivicNumber(input);
				case CountryIso.DK:
					return IsValidDanishCivicNumber(input);
				case CountryIso.FI:
					return IsValidFinnishCivicNumber(input);
				case CountryIso.NL:
					return IsValidNetherlandsCivicNumber();
				case CountryIso.FR:
					return IsValidFranceCivicNumber();
				case CountryIso.EU:
				default:
					return IsValidDateCivicNumber(input);
			}
		}
		/// <summary>
		/// Validates a civic number.
		/// Checks so it is of the format 'yyyymmdd' or 'yyyymmdd'.
		/// Checks so it is a correct birth date that is not in the future.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		private static bool IsValidDateCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, _civicNumberValidationPatternDate);
			if (!match.Success)
			{
				return false;
			}

			// Check date.
			string dateToTest = string.Format(CultureInfo.InvariantCulture, "{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			DateTime birthDate;
			if (!DateTime.TryParse(dateToTest, out birthDate))
			{
				return false;
			}

			// Check if date is in future.
			if (birthDate <= DateTime.Now)
				return true;
			else
				return false;
		}
		/// <summary>
		/// Validates a swedish civic number.
		/// Checks so it is of the format 'yyyymmdd' or 'yyyymmdd'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		private static bool IsValidSwedishCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, _civicNumberValidationPatternSwedish);
			//if (!match.Success)
			//{
			//    return false;
			//}

			return match.Success;
			//// Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			//DateTime birthDate;
			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//{
			//    return false;
			//}

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//// Check digit.
			//int checkDigit = int.Parse(match.Groups["checkdigit"].Value);
			//if (CalculateCheckDigit(match.Groups["date"].Value.Substring(2) + match.Groups["additional"].Value) == checkDigit)
			//    return true;
			//return false;
		}
		/// <summary>
		/// Validates a danish civic number.
		/// Checks so it is of the format 'ddmmyy' or 'ddmmyy'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		private static bool IsValidDanishCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, _civicNumberValidationPatternDanish);
			if (!match.Success)
			{
				return false;
			}
			return true;

			//    // Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			////CultureInfo f = new CultureInfo("da-DK");
			//DateTime birthDate;

			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//{
			//    return false;
			//}

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//string dkCivic = match.Groups["day"].Value
			//    + match.Groups["month"].Value
			//    + match.Groups["year"].Value.Substring(2)
			//    + match.Groups["additional"].Value + match.Groups["checkdigit"].Value;


			//int[] mod11_array = new int[9] { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
			//int sum = 0;

			//for (int i = 0; i < 9; i++)
			//{
			//    sum += int.Parse(dkCivic.Substring(i, 1)) * mod11_array[i];
			//}
			//int modulo = sum % 11;
			//int check_number = 0;

			//if (modulo > 0)
			//    check_number = 11 - modulo;

			//if (int.Parse(match.Groups["checkdigit"].Value) == check_number)
			//    return true;
			//else
			//    return false;

		}
		/// <summary>
		/// Validates a norwegian civic number.
		/// Checks so it is of the format 'ddmmyy' or 'ddmmyy'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		private static bool IsValidNorwegianCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, _civicNumberValidationPatternNorwegian);
			if (!match.Success)
			{
				return false;
			}

			return true;

			//// Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			//DateTime birthDate;
			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//{
			//    return false;
			//}

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//input = match.Groups["day"].Value + match.Groups["month"].Value + match.Groups["year"].Value.Substring(2) + match.Groups["additional"].Value;

			//int sum1 = 0;
			//int sum2 = 0;

			//int[] weight1 = new int[11] { 3, 7, 6, 1, 8, 9, 4, 5, 2, 1, 0 };
			//int[] weight2 = new int[11] { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2, 1 };

			////Length must be 11, ddmmyyXXXXX
			//if (input.Length == 11)
			//{
			//    for (int i = 0; i < 11; i++)
			//    {
			//        sum1 += int.Parse(input.Substring(i, 1)) * weight1[i];
			//        sum2 += int.Parse(input.Substring(i, 1)) * weight2[i];
			//    }
			//    if (sum1 % 11 != 0 || sum2 % 11 != 0)
			//        return false;
			//    return true;
			//}
			//else
			//    return false;
		}
		/// <summary>
		/// Validates a finnish civic number.
		/// Checks so it is of the format 'ddmmyy' or 'ddmmyy'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		private static bool IsValidFinnishCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, _civicNumberValidationPatternFinnish);
			if (!match.Success)
			{
				return false;
			}

			return true;

			//// Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			//DateTime birthDate;
			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//    return false;

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//string ctrlChars = "0123456789ABCDEFHJKLMNPRSTUVWXY";

			//input = match.Groups["day"].Value + match.Groups["month"].Value + match.Groups["year"].Value.Substring(2) +
			//        match.Groups["additional"].Value + match.Groups["checkdigit"].Value;
			//int summa = 0;
			//if (input.Length < 10)
			//    return false;
			//else
			//{
			//    int calc = int.Parse(input.Substring(0, 9));
			//    summa = calc % 31;
			//}

			//if (!match.Groups["checkdigit"].Value.Equals(ctrlChars.Substring(summa, 1)))
			//    return false;
			//return true;

		}
		/// <summary>
		/// Validates a Netherlands civic number.
		/// </summary>
		private static bool IsValidNetherlandsCivicNumber()
		{
			//TODO: Add real validation
			return true;
		}
		/// <summary>
		/// Validates a France civic number.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidFranceCivicNumber()
		{
			//TODO: Add real validation
			return true;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="input">Valideates organization number pattern xxxxxx-xxxx</param>
		/// <returns></returns>
		public static bool IsValidOrganizationNumber(string input)
		{
			return Regex.Match(input, _organizationNumberValidationPattern).Success;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <author>Jonas Ljunggren</author>
		/// <date>2008-06-18</date>
		/// <param name="idNumber">The number to calculate a check digit from.</param>
		/// <returns></returns>
		public static int CalculateCheckDigit(string idNumber)
		{
			int controlNumber = 0;

			char[] idNumberChars = idNumber.ToCharArray();

			int newNumber = 0;

			bool include = true;

			for (int charCount = 0; charCount < idNumberChars.Length; charCount++)
			{
				int number = Convert.ToInt32(idNumberChars[charCount].ToString(), CultureInfo.InvariantCulture);
				if (include)
					number *= 2;

				include = !include;

				if (number > 9)
					number -= 9;

				newNumber += number;
			}

			string newNumberAsString = newNumber.ToString(CultureInfo.InvariantCulture);

			string last = newNumberAsString.Substring(newNumberAsString.Length - 1);
			controlNumber = 10 - Convert.ToInt32(last, CultureInfo.InvariantCulture);

			if (controlNumber == 10)
				controlNumber = 0;

			return controlNumber;
		}
	}
	public enum CountryIso
	{
		SE = 0,
		NO = 1,
		DK = 2,
		FI = 3,
		NL = 4,
		FR = 5,
		EU = 6
	}
}