﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Common
{
	[Serializable]
	public class SpreadsheetException : Exception
	{
		public SpreadsheetException()
		{
		}

		public SpreadsheetException(string message)
			: base(message)
		{
		}

		public SpreadsheetException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		protected SpreadsheetException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
