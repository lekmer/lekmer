﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Lekmer.BackOffice.Controller.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.CommonItems;

namespace Litium.Lekmer.BackOffice.Controller
{
	/// <summary>
	/// Authenticates BackOffice users.
	/// Extends <see cref="LekmerFileSystemStatePage"/>.
	/// </summary>
	public class LekmerSecurePage : LekmerFileSystemStatePage
	{
		protected override void InitializeCulture()
		{
			if (SignInHelper.SignedInSystemUser != null)
			{
				Culture = UICulture = SignInHelper.SignedInSystemUser.CultureInfo.Name;
			}
			base.InitializeCulture();
		}

		protected override void OnInit(EventArgs e)
		{
			if (!ValidateUser())
			{
				Response.Redirect(PathHelper.GetSighInUrl());
			}

			if (!CheckSecurity())
			{
				Response.Redirect(PathHelper.Errors.GetNoPrivilegePageUrl());
			}

			base.OnInit(e);
		}


		protected virtual bool ValidateUser()
		{
			return SignInHelper.SignedInSystemUser != null;
		}

		protected virtual bool CheckSecurity()
		{
			HttpContext context = HttpContext.Current;
			string currentPath = context.Request.AppRelativeCurrentExecutionFilePath;
			Match match = Regex.Match(currentPath, @"^~/Modules/(?<mainTab>\w+)(/(?<subTab>\w+))?/.+");
			string mainTabName = match.Groups["mainTab"].Value;
			string subTabName = match.Groups["subTab"].Value;

			var navigationHelper = new NavigationHelper("Navigation.xml");
			Tab mainTab = navigationHelper.Tabs.FirstOrDefault(tab => tab.Name == mainTabName);
			if (mainTab != null)
			{
				if (!LekmerPrivilegeHelper.CheckSecurity(mainTab))
				{
					return false;
				}
				Tab subTab = mainTab.Children.FirstOrDefault(tab => tab.Name == subTabName);
				if (subTab != null)
				{
					if (!LekmerPrivilegeHelper.CheckSecurity(subTab))
					{
						return false;
					}
				}
			}
			return true;
		}
	}
}