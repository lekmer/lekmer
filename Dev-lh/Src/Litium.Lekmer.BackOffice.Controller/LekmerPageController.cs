﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BackOffice.Controller
{
	/// <summary>
	/// Abstract base page controller class for Scensum.BackOffice page controllers.
	/// Extends <see cref="LekmerSecurePage"/>.
	/// </summary>
	public abstract class LekmerPageController : LekmerSecurePage
	{
		/// <summary>
		/// Return Id from QueryString.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetId()
		{
			return Request.QueryString.GetInt32("Id");
		}

		/// <summary>
		/// Return Id from QueryString or default value if id not exist.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetIdOrNull()
		{
			return Request.QueryString.GetInt32OrNull("Id");
		}

		/// <summary>
		/// Sets event handlers for page controls when 
		/// the System.Web.UI.Control.Init event fires.
		/// </summary>
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			SetEventHandlers();
		}

		/// <summary>
		/// Populates the form or grid when the System.Web.UI.Control.Load event fires,
		/// but only if it is not during a postback.
		/// </summary>
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack)
			{
				PopulateForm();
			}
		}

		/// <summary>
		/// Use this method when you need to set event handlers for your controls. 
		/// </summary>
		protected abstract void SetEventHandlers();

		/// <summary>
		/// Executes during the Page.Load event.
		/// Use this method to implement your loading and databinding of form controls.
		/// </summary>
		protected abstract void PopulateForm();
	}
}