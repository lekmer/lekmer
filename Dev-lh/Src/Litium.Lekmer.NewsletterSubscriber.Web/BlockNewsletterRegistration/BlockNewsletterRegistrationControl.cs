﻿using System.Globalization;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.NewsletterSubscriber.Web
{
	public class BlockNewsletterRegistrationControl : BlockControlBase
	{
		private NewsletterRegistrationForm _newsletterRegistrationForm;

		public BlockNewsletterRegistrationControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		public NewsletterRegistrationForm NewsletterRegistrationForm
		{
			get 
			{
				return _newsletterRegistrationForm ?? (_newsletterRegistrationForm = new NewsletterRegistrationForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			bool registered = false;
			if (NewsletterRegistrationForm.IsFormPostBack)
			{
				NewsletterRegistrationForm.MapFromRequest();
				validationResult = NewsletterRegistrationForm.Validate();
				if (validationResult.IsValid)
				{
					registered = RegisterNewsletterSubscriber();
					if (registered)
					{
						NewsletterRegistrationForm.ClearFrom();
					}
				}
			}
			else
			{
				NewsletterRegistrationForm.ClearFrom();
			}
			return RenderNewsletterRegistrationForm(validationResult, registered);
		}

		private bool RegisterNewsletterSubscriber()
		{
			string email = NewsletterRegistrationForm.Email.ToLower(CultureInfo.InvariantCulture);

			var subscriberService = IoC.Resolve<INewsletterSubscriberService>();
			var subscriber = subscriberService.Create(UserContext.Current);
			subscriber.Email = email;
			subscriber.SubscriberTypeId = (int) NewsletterSubscriberType.Manual;
			int subscriberId = subscriberService.Save(UserContext.Current, subscriber);

			var unsubscriberService = IoC.Resolve<INewsletterUnsubscriberService>();
			unsubscriberService.Delete(UserContext.Current, email);

			return subscriberId > 0;
		}

		private BlockContent RenderNewsletterRegistrationForm(ValidationResult validationResult, bool registered)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			NewsletterRegistrationForm.MapFieldNamesToFragment(fragmentContent);
			NewsletterRegistrationForm.MapFieldValuesToFragment(fragmentContent);
			RenderValidationResult(fragmentContent, validationResult);
			RenderRegisteredNotification(fragmentContent, registered);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			NewsletterRegistrationForm.MapFieldNamesToFragment(fragment);
			NewsletterRegistrationForm.MapFieldValuesToFragment(fragment);
			fragment.AddEntity(Block);
			return fragment.Render();
		}

		private static void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		private static void RenderRegisteredNotification(Fragment fragmentContent, bool registered)
		{
			string message = registered
				? AliasHelper.GetAliasValue("Email.NewsletterRegistration.Registered")
				: string.Empty;
			fragmentContent.AddVariable("RegisteredMessage", message, VariableEncoding.None);
			fragmentContent.AddCondition("RegistationSuccess", registered);
		}
	}
}