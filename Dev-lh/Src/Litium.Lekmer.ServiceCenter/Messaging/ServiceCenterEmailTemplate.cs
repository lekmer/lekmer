﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ServiceCenter
{
	public class ServiceCenterEmailTemplate : Template
	{
		public ServiceCenterEmailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public ServiceCenterEmailTemplate(IChannel channel)
			: base("ServiceCenterEmail", channel, false)
		{
		}
	}
}
