﻿using System;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IImageSize"/>.
	/// </summary>
	public sealed class LekmerImageSizeCache : ScensumCacheBase<LekmerImageSizeKey, IImageSize>
	{
		#region Singleton

		private LekmerImageSizeCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static LekmerImageSizeCache Instance
		{
			get { return SingletonCreator.CreatorInstance; }
		}

		private static class SingletonCreator
		{
			private static readonly LekmerImageSizeCache _instance = new LekmerImageSizeCache();

			public static LekmerImageSizeCache CreatorInstance
			{
				get { return _instance; }
			}
		}

		#endregion
	}

	/// <summary>
	/// Key used for caching <see cref="IImageSize"/>.
	/// </summary>
	public class LekmerImageSizeKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="commonName">Common name of the <see cref="IImageSize"/>.</param>
		/// <param name="mobileVersion">MobileVersion of the <see cref="IImageSize"/>.</param>
		public LekmerImageSizeKey(string commonName, bool mobileVersion)
		{
			if (commonName == null)
			{
				throw new ArgumentNullException("commonName");
			}

			CommonName = commonName;
			MobileVersion = mobileVersion ? "-m" : string.Empty;
		}

		/// <summary>
		/// Common name of the <see cref="IImageSize"/>.
		/// </summary>
		public string CommonName { get; set; }

		/// <summary>
		/// Mobile version of the <see cref="IImageSize"/>.
		/// </summary>
		public string MobileVersion { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get
			{
				return CommonName + MobileVersion;
			}
		}
	}
}