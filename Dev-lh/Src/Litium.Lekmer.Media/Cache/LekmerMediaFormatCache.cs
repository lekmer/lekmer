﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IMediaFormat"/>.
	/// </summary>
	public sealed class LekmerMediaFormatCache : ScensumCacheBase<LekmerMediaFormatKey, IMediaFormat>
	{
		private static readonly LekmerMediaFormatCache _instance = new LekmerMediaFormatCache();

		private LekmerMediaFormatCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static LekmerMediaFormatCache Instance
		{
			get { return _instance; }
		}
	}

	/// <summary>
	/// Key used for caching <see cref="IMediaFormat"/>.
	/// </summary>
	public class LekmerMediaFormatKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="id">Id of the <see cref="IMediaFormat"/>.</param>
		public LekmerMediaFormatKey(int id)
		{
			Id = id;
		}

		/// <summary>
		/// Extension of the <see cref="IMediaFormat"/>.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get { return Id.ToString(CultureInfo.InvariantCulture); }
		}
	}
}