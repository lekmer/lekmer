﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media.Repository
{
	public class LekmerImageRepository : ImageRepository
	{
		public virtual void SaveMovie(ILekmerImage entity)
		{
			if (entity == null) throw new ArgumentNullException("entity");
			IDataParameter[] parameters =
				{ 
					ParameterHelper.CreateParameter("MediaId", entity.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Link", entity.Link, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Parameter", entity.Parameter, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("HasImage", entity.HasImage, SqlDbType.Bit),
					ParameterHelper.CreateParameter("TumbnailImageUrl", entity.TumbnailImageUrl, SqlDbType.NVarChar)
				};

			DatabaseSetting dbSettings = new DatabaseSetting("ImageRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pLekmerImageSaveMovie]", parameters, dbSettings);
		}

		public virtual Collection<ILekmerMovie> GetMoviesByProductId(int productId)
		{		
			IDataParameter[] parameters =
				{ 
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerImageRepository.GetMoviesByProductId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductMovieGetAllByProduct]", parameters, dbSettings))
			{
				try
				{
					var dataMapper = DataMapperResolver.Resolve<ILekmerMovie>(dataReader);
					return dataMapper.ReadMultipleRows();
				}
				catch (Exception exp)
				{
					
					throw;
				}
				
			}
			
		}

		public virtual Collection<IImage> GetAllBySizeTable(int channelId, int sizeTableId)
		{
			Collection<IImage> images;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerImageRepository.GetAllBySizeTable");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pImageGetAllBySizeTable]", parameters, dbSettings))
			{
				images = CreateDataMapper(dataReader).ReadMultipleRows();
			}

			return images;
		}
	}
}
