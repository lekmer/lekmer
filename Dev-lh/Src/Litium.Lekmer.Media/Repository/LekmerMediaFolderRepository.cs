﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media.Repository
{
	public class LekmerMediaFolderRepository : MediaFolderRepository
	{
		public virtual Collection<IMediaFolder> GetAllByTitle(string title)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@Title ", title, SqlDbType.NChar),					
				};
			DatabaseSetting dbSettings = new DatabaseSetting("MediaFolderRepository.GetAllByParentId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[media].[pMediaFolderGetByTitle]",
																							  parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
