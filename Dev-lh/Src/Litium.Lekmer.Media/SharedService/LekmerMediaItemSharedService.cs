﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using log4net;

namespace Litium.Lekmer.Media
{
	public class LekmerMediaItemSharedService : MediaItemSharedService
	{
		private const int BufferSizeBytes = 8 * 1024;
		private static readonly object _lockObject = new object();
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// Loads a media item as a stream.
		/// </summary>
		/// <param name="id">Identifier of the media item.</param>
		/// <param name="mediaFormat">Format of the media item.</param>
		/// <returns>The media item as a <see cref="Stream"/>.</returns>
		public override Stream LoadMedia(int id, IMediaFormat mediaFormat)
		{
			if (id <= 0) throw new ArgumentOutOfRangeException("id");
			if (mediaFormat == null) throw new ArgumentNullException("mediaFormat");

			if (LekmerMediaCacheSetting.Instance.AllowCacheOriginalImage)
			{
				Stream cachedImageStream = LoadImageFromCache(id, mediaFormat.Extension);
				if (cachedImageStream != null)
				{
					return cachedImageStream;
				}
			}

			string mediaPath = GetMediaItemPath(id, mediaFormat.Extension);
			if (!File.Exists(mediaPath))
			{
				return null;
			}

			var imageStream = new FileStream(mediaPath, FileMode.Open, FileAccess.Read);

			if (LekmerMediaCacheSetting.Instance.AllowCacheOriginalImage)
			{
				AddImageToCache(id, mediaFormat.Extension, imageStream);
				imageStream.Seek(0, SeekOrigin.Begin);
			}

			return imageStream;
		}

		/// <summary>
		/// Gets a cached image as a <see cref="Stream"/>.
		/// </summary>
		/// <param name="id">Identifier of the media item.</param>
		/// <param name="extension">Extension of the media item.</param>
		/// <returns>A cached image as a <see cref="Stream"/>.</returns>
		protected virtual Stream LoadImageFromCache(int id, string extension)
		{
			if (id <= 0) throw new ArgumentOutOfRangeException("id");
			if (extension == null) throw new ArgumentNullException("extension");

			string cachedPath = GetImageCachePath(id, extension);

			if (!File.Exists(cachedPath))
			{
				return null;
			}

			return new FileStream(cachedPath, FileMode.Open, FileAccess.Read);
		}

		/// <summary>
		/// Gets the file system cache path of a resized image.
		/// </summary>
		/// <param name="id">The unique identifier of the <see cref="IMediaItem"/> from which to retrieve data.</param>
		/// <param name="extension">The extension of the media file.</param>
		/// <returns>A path to the cached resized image.</returns>
		protected virtual string GetImageCachePath(int id, string extension)
		{
			if (id <= 0) throw new ArgumentOutOfRangeException("id");
			if (extension == null) throw new ArgumentNullException("extension");

			return GetImageCacheDirectoryPath(id) + GetImageCacheFileName(extension);
		}

		/// <summary>
		/// Gets the file system cache folder path for the resized image.
		/// </summary>
		/// <param name="id">The unique identifier of the <see cref="IMediaItem"/> from which to retrieve data.</param>
		/// <returns>A folder path to the cached resized image. Ends with \.</returns>
		protected virtual string GetImageCacheDirectoryPath(int id)
		{
			if (id <= 0) throw new ArgumentOutOfRangeException("id");

			string idPath = CreateIdPartOfPath(id);

			return string.Format(CultureInfo.InvariantCulture, @"{0}{1}\original\", MediaSetting.Instance.CachePath, idPath);
		}

		/// <summary>
		/// Gets the file name of the cached resized image.
		/// </summary>
		/// <param name="extension">The extension of the media file.</param>
		/// <returns>File name of the cached image.</returns>
		public virtual string GetImageCacheFileName(string extension)
		{
			if (extension == null) throw new ArgumentNullException("extension");

			return "data." + extension;
		}

		/// <summary>
		/// Adds an image to the cache.
		/// </summary>
		/// <param name="id">Identifier of the media item.</param>
		/// <param name="extension">Extension of the media item.</param>
		/// <param name="imageStream">The image as a <see cref="Stream"/>.</param>
		protected virtual void AddImageToCache(int id, string extension, Stream imageStream)
		{
			if (id <= 0) throw new ArgumentOutOfRangeException("id");
			if (extension == null) throw new ArgumentNullException("extension");

			try
			{
				string cachedDirectoryPath = GetImageCacheDirectoryPath(id);
				if (!Directory.Exists(cachedDirectoryPath))
				{
					Directory.CreateDirectory(cachedDirectoryPath);
				}

				// Save it to a temporary file first to prevent from multiple writes to the same file.
				string tempPath = cachedDirectoryPath + Guid.NewGuid() + "." + extension;
				using (var fileStream = File.Create(tempPath))
				{
					StreamAppender.AppendToStream(imageStream, fileStream, BufferSizeBytes);
				}

				// Now move/rename it to the correct path.
				lock (_lockObject)
				{
					string cachedPath = cachedDirectoryPath + GetImageCacheFileName(extension);
					if (!File.Exists(cachedPath))
					{
						File.Move(tempPath, cachedPath);
					}
					else
					{
						File.Delete(tempPath);
					}
				}
			}
			catch (Exception ex)
			{
				_log.Warn("An error occured when trying to add an image to the cache.", ex);
			}
		}
	}
}