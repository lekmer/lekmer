﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Media
{
	public sealed class LekmerMediaCacheSetting : SettingBase
	{
		//Singleton

		private static readonly LekmerMediaCacheSetting _instance = new LekmerMediaCacheSetting();

		private LekmerMediaCacheSetting()
		{
		}

		public static LekmerMediaCacheSetting Instance
		{
			get { return _instance; }
		}

		//End of Singleton

		private const string _groupName = "MediaCache";

		protected override string StorageName
		{
			get { return "LekmerCacheManagement"; }
		}

		public bool AllowCacheOriginalImage
		{
			get { return GetBoolean(_groupName, "AllowCacheOriginalImage", false); }
		}
	}
}