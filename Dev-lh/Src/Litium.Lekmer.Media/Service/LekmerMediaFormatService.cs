﻿using System;
using Litium.Lekmer.Media.Cache;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
	public class LekmerMediaFormatService : MediaFormatService
	{
		public LekmerMediaFormatService(MediaFormatRepository repository)
			: base(repository)
		{
		}

		public override IMediaFormat GetById(int id)
		{
			if (id <= 0)
			{
				throw new ArgumentOutOfRangeException("id");
			}

			return LekmerMediaFormatCache.Instance.TryGetItem(
				new LekmerMediaFormatKey(id),
				() => Repository.GetById(id));
		}
	}
}