﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using Litium.Lekmer.Media.Cache;
using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
	public class LekmerImageService : ImageService, ILekmerImageService
	{
		protected LekmerImageRepository LekmerRepository { get; private set; }

		/// <summary>
		/// Gets the <see cref="IImageSizeService"/>.
		/// </summary>
		protected ILekmerImageSizeService LekmerImageSizeService { get; private set; }

		public LekmerImageService(ImageRepository repository, IImageSharedService imageSharedService, IMediaFormatService mediaFormatService,
			IImageSizeService imageSizeService, IMediaItemSharedService mediaItemSharedService)
			: base(repository, imageSharedService, mediaFormatService, imageSizeService, mediaItemSharedService)
		{
			LekmerRepository = (LekmerImageRepository)repository;
			LekmerImageSizeService = (ILekmerImageSizeService)imageSizeService;
		}

		/// <summary>
		///  Retrieves an <see cref="IImage"/> by its unique identifier.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="id">The unique identifier of the <see cref="IImage"/> to be retrieved.</param>
		/// <returns>The <see cref="IImage"/> that was requested, or null if none was found.</returns>
		/// <exception cref="ArgumentOutOfRangeException">The requested identifier was less than or equal to 0.</exception>
		public override IImage GetById(IUserContext context, int id)
		{
			return LekmerImageCache.Instance.TryGetItem(
				new LekmerImageKey(context.Channel.Id, id),
				() => base.GetById(context, id));
		}

		/// <summary>
		///  Retrieves the resized image as a <see cref="Stream"/>.
		/// </summary>
		/// <param name="id">The unique identifier of the <see cref="IMediaItem"/> from which to retrieve data.</param>
		/// <param name="extension">The extension of the media file.</param>
		/// <param name="imageSizeCommonName">Common name of the image size specifiying width/height/quality.</param>
		/// <param name="mobileVersion">When need mobile version of image, send true.</param>
		/// <param name="mime">Returns mime for media.</param>
		/// <returns>A <see cref="Stream"/> containing the raw data of the resized image.</returns>
		/// <exception cref="ArgumentOutOfRangeException">The supplied <paramref name="id"/> was less than or equal to 0.</exception>
		/// <exception cref="ArgumentNullException">The supplied <paramref name="extension"/> was null or empty.</exception>
		public virtual Stream LoadImage(int id, string extension, string imageSizeCommonName, bool mobileVersion, out string mime)
		{
			if (id <= 0)
			{
				throw new ArgumentOutOfRangeException("id");
			}

			if (string.IsNullOrEmpty(extension))
			{
				throw new ArgumentNullException("extension");
			}

			if (MediaFormatService == null)
			{
				throw new InvalidOperationException("MediaFormatService must be set before calling LoadMedia.");
			}

			if (ImageSharedService == null)
			{
				throw new InvalidOperationException("ImageSharedService must be set before calling LoadMedia.");
			}

			IMediaFormat mediaFormat = MediaFormatService.GetByExtension(extension);
			if (mediaFormat == null ||
				!mediaFormat.MediaFormatGroupId.HasValue ||
				(MediaFormatGroupInfo)mediaFormat.MediaFormatGroupId.Value != MediaFormatGroupInfo.Image)
			{
				mime = null;
				return null;
			}

			IImageSize imageSize = LekmerImageSizeService.GetByCommonName(imageSizeCommonName, mobileVersion);

			if (imageSize == null)
			{
				mime = null;
				return null;
			}

			mime = mediaFormat.Mime;
			return ImageSharedService.LoadImage(id, mediaFormat, imageSize);
		}

		public virtual Collection<IImage> GetAllBySizeTable(IUserContext context, int sizeTableId)
		{
			return LekmerRepository.GetAllBySizeTable(context.Channel.Id, sizeTableId);
		}
	}
}