﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
	public class LekmerMediaFolderSecureService : MediaFolderSecureService, ILekmerMediaFolderSecureService
	{
		public LekmerMediaFolderRepository LekmerMediaFolderRepository { get; private set; }

		public LekmerMediaFolderSecureService(IAccessValidator accessValidator, MediaFolderRepository repository, IMediaItemSecureService mediaItemSecureService) : base(accessValidator, repository, mediaItemSecureService)
		{
			LekmerMediaFolderRepository = new LekmerMediaFolderRepository();
		}

		public Collection<IMediaFolder> GetAllByParentId(int? folderId)
		{
			return Repository.GetAllByParentId(folderId);
		}

		public Collection<IMediaFolder> GetAllByTitle(string title)
		{
			return LekmerMediaFolderRepository.GetAllByTitle(title);
		}
	}
}
