﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.GoogleFeed
{
	public class GoogleFeedXmlFile: BaseXmlFile
	{
		private readonly string _channelNameForReplacement = "[ChannelName]";
		private readonly char _erpSeperator = '-';
		private readonly string _noSize = "000";
		private readonly string _productTypePattern = "{0} > {1} > {2}";
		private readonly string _dateTimeFormat = "yyyy-MM-dd\"T\"HH:mmzzz";
		private readonly string _slash = "/";
		private readonly string _httpScheme = "http://";
		private readonly string _priceFormat = "0.00";

		private ILekmerDeliveryMethodService _deliveryMethodService;
		private IGoogleFeedSetting _googleFeedSetting;
		private IUserContext _context;
		private string _mediaUrl;
		private string _shippingPrice;

		private Dictionary<int, ICampaign> _productCampaigns;
		private bool _isInitialized;

		public void Initialize(IGoogleFeedSetting googleFeedSetting, Collection<ICampaign> productCampaigns)
		{
			if (googleFeedSetting == null)
			{
				throw new ArgumentNullException("googleFeedSetting");
			}

			if (productCampaigns == null)
			{
				throw new ArgumentNullException("productCampaigns");
			}

			_googleFeedSetting = googleFeedSetting;
			_productCampaigns = productCampaigns.ToDictionary(c => c.Id);

			_deliveryMethodService = (ILekmerDeliveryMethodService)IoC.Resolve<IDeliveryMethodService>();

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap, IUserContext context, IEnumerable<IGoogleFeedInfo> googleFeedInfoList)
		{
			if (!_isInitialized)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			_context = context;
			_mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveExternalUrl(_context.Channel);

			InitializeShippingPrice();

			var doc = CreateXmlDocument();

			XmlNode rssNode = AddRssNode(doc);
			XmlNode channelNode = AddNode(doc, rssNode, Const.ChannelNode);

			var channelName = _googleFeedSetting.GetChannelNameReplacement(_context.Channel.CommonName).ToLower();
			AddNode(doc, channelNode, Const.TitleNode, _googleFeedSetting.GoogleFeedTitle.Replace(_channelNameForReplacement, channelName));
			AddNode(doc, channelNode, Const.LinkNode, _googleFeedSetting.GoogleFeedLink.Replace(_channelNameForReplacement, channelName));
			AddNode(doc, channelNode, Const.DescriptionNode, _googleFeedSetting.GoogleFeedDescription.Replace(_channelNameForReplacement, channelName));

			foreach (IGoogleFeedInfo googleFeedInfo in googleFeedInfoList)
			{
				if (googleFeedInfo.Product.ProductSizes != null && googleFeedInfo.Product.ProductSizes.Count > 0)
				{
					foreach (var productSize in googleFeedInfo.Product.ProductSizes)
					{
						AddItem(doc, channelNode, googleFeedInfo, productSize);
					}
				}
				else
				{
					AddItem(doc, channelNode, googleFeedInfo, null);
				}
			}

			doc.Save(savedSiteMap);
		}


		protected XmlNode AddRssNode(XmlDocument xmlDocument)
		{
			XmlNode rssNode = xmlDocument.CreateElement(Const.RssNode);

			AddAttribute(xmlDocument, rssNode, Const.VersionAttr, Const.RssVersion);
			AddAttribute(xmlDocument, rssNode, Const.NamespaceAttr, Const.RssNamespaceUri);

			xmlDocument.AppendChild(rssNode);

			return rssNode;
		}

		protected void AddItem(XmlDocument doc, XmlNode parentNode, IGoogleFeedInfo googleFeedInfo, IProductSize productSize)
		{
			XmlNode itemNode = AddNode(doc, parentNode, Const.ItemNode);

			var product = googleFeedInfo.Product;

			// ID
			var id = productSize == null ? GetId(product.ErpId) : productSize.ErpId;
			AddNodeRaw(doc, itemNode, Const.IdNode, id, Const.RssPrefix, Const.RssNamespaceUri);

			// Title
			AddCDataNode(doc, itemNode, Const.TitleNode, product.DisplayTitle);

			// Description
			AddCDataNode(doc, itemNode, Const.DescriptionNode, product.Description);

			// Product Type
			var productType = GetProductType(googleFeedInfo);
			AddCDataNode(doc, itemNode, Const.ProductTypeNode, productType, Const.RssPrefix, Const.RssNamespaceUri);

			// Link
			var link = _httpScheme + _context.Channel.ApplicationName + _slash + googleFeedInfo.Product.LekmerUrl;
			AddNodeRaw(doc, itemNode, Const.LinkNode, link);

			// Image Link
			if (product.Image != null)
			{
				var imageLink = GetImageLink(product);
				AddNodeRaw(doc, itemNode, Const.ImageLinkNode, imageLink, Const.RssPrefix, Const.RssNamespaceUri);
			}

			// Additional Image Link
			if (googleFeedInfo.ImageGroups != null)
			{
				int additionalImagesCount = 0;
				foreach (IProductImageGroupFull imageGroup in googleFeedInfo.ImageGroups)
				{
					if (imageGroup.ProductImages == null || imageGroup.ProductImages.Count <= 0) continue;

					foreach (IProductImage productImage in imageGroup.ProductImages)
					{
						if (additionalImagesCount == 10) break;
						if (productImage == null) continue;

						var additionalImageLink = GetImageLink(productImage.Image);
						AddNodeRaw(doc, itemNode, Const.AdditionalImageLinkNode, additionalImageLink, Const.RssPrefix, Const.RssNamespaceUri);
						additionalImagesCount++;
					}
				}
			}

			// Condition
			AddNodeRaw(doc, itemNode, Const.ConditionNode, Const.Condition, Const.RssPrefix, Const.RssNamespaceUri);

			// Availability
			string availability = productSize == null
				? (product.NumberInStock > 0 ? Const.InStock : Const.OutOfStock)
				: (productSize.NumberInStock > 0 ? Const.InStock : Const.OutOfStock);
			AddNodeRaw(doc, itemNode, Const.AvailabilityNode, availability, Const.RssPrefix, Const.RssNamespaceUri);

			// Price
			string priceText = product.Price.PriceIncludingVat.ToString(_priceFormat, CultureInfo.InvariantCulture);
			priceText = string.Format("{0} {1}", priceText, _context.Channel.Currency.Iso);
			AddNodeRaw(doc, itemNode, Const.PriceNode, priceText, Const.RssPrefix, Const.RssNamespaceUri);

			// Sale Price
			decimal priceIncludingVat = product.IsPriceAffectedByCampaign
				? product.CampaignInfo.Price.IncludingVat
				: product.Price.PriceIncludingVat;
			string salePriceText = priceIncludingVat.ToString(_priceFormat, CultureInfo.InvariantCulture);
			salePriceText = string.Format("{0} {1}", salePriceText, _context.Channel.Currency.Iso);
			AddNodeRaw(doc, itemNode, Const.SalePriceNode, salePriceText, Const.RssPrefix, Const.RssNamespaceUri);

			// Sale Price Effective Date
			var affectedPriceAction = ((ILekmerProductCampaignInfo) product.CampaignInfo).AffectedPriceAction;
			if (affectedPriceAction != null && _productCampaigns.ContainsKey(affectedPriceAction.ProductCampaignId))
			{
				var campaign = _productCampaigns[affectedPriceAction.ProductCampaignId];
				if (campaign != null && campaign.StartDate.HasValue && campaign.EndDate.HasValue)
				{
					string startDate = campaign.StartDate.Value.ToString(_dateTimeFormat);
					string endDate = campaign.EndDate.Value.ToString(_dateTimeFormat);
					string effectiveDate = string.Format("{0}/{1}", startDate, endDate);
					AddNodeRaw(doc, itemNode, Const.EffectiveDateNode, effectiveDate, Const.RssPrefix, Const.RssNamespaceUri);
				}
			}

			// Brand
			if (product.Brand != null)
			{
				AddCDataNode(doc, itemNode, Const.BrandNode, product.Brand.Title, Const.RssPrefix, Const.RssNamespaceUri);
			}

			// Identifier Exists
			AddNodeRaw(doc, itemNode, Const.IdentifierExistsNode, Const.IdentifierFalse, Const.RssPrefix, Const.RssNamespaceUri);

			// Age Group
			AddNodeRaw(doc, itemNode, Const.AgeGroupNode, Const.AgeGroup, Const.RssPrefix, Const.RssNamespaceUri);

			// Adwords Redirect
			//AddNodeRawWithPrefix(doc, itemNode, Const.AdwordsRedirectNode, link);

			// Shipping
			XmlNode shippingNode = AddNode(doc, itemNode, Const.ShippingNode, Const.RssPrefix, Const.RssNamespaceUri);

			// Country
			AddNodeRaw(doc, shippingNode, Const.CountryNode, _context.Channel.Country.Iso, Const.RssPrefix, Const.RssNamespaceUri);

			// Price
			AddNodeRaw(doc, shippingNode, Const.PriceNode, _shippingPrice, Const.RssPrefix, Const.RssNamespaceUri);

			// Item Group Id
			AddNodeRaw(doc, itemNode, Const.ItemGroupIdNode, product.ErpId, Const.RssPrefix, Const.RssNamespaceUri);

			if (productSize != null)
			{
				string size = productSize.SizeInfo.ErpTitle;
				if (googleFeedInfo.SizeTable != null && googleFeedInfo.SizeTable.Rows != null && googleFeedInfo.SizeTable.Rows.Count > 0)
				{
					var sizeTableRow = googleFeedInfo.SizeTable.Rows.FirstOrDefault(s => s.SizeId == productSize.SizeInfo.Id);
					if (sizeTableRow != null && !string.IsNullOrEmpty(sizeTableRow.Column1Value))
					{
						size = sizeTableRow.Column1Value;
					}
				}

				// Size
				AddNodeRaw(doc, itemNode, Const.SizeNode, size, Const.RssPrefix, Const.RssNamespaceUri);
			}
		}

		protected virtual string GetId(string erpId)
		{
			return erpId + _erpSeperator + _noSize;
		}
		protected virtual string GetProductType(IGoogleFeedInfo googleFeedInfo)
		{
			return string.Format(_productTypePattern, googleFeedInfo.FirstLevelCategoryTitle, googleFeedInfo.SecondLevelCategoryTitle, googleFeedInfo.ThirdLevelCategoryTitle);
		}
		protected virtual string GetImageLink(ILekmerProductView product)
		{
			var imageLink = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, product.Image, product.DisplayTitle);
			return PrepareImageLink(imageLink);
		}
		protected virtual string GetImageLink(IImage image)
		{
			var imageLink = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, image);
			return PrepareImageLink(imageLink);
		}
		protected virtual string PrepareImageLink(string imageLink)
		{
			if (imageLink.StartsWith("//"))
			{
				imageLink = imageLink.Substring(2);
			}
			else if (imageLink.StartsWith(_httpScheme))
			{
				return imageLink;
			}

			imageLink = _httpScheme + imageLink;
			return imageLink;
		}
		protected virtual void InitializeShippingPrice()
		{
			string deliveryMethodIdValue = _googleFeedSetting.GetChannelDeliveryMethod(_context.Channel.CommonName);

			int deliveryMethodId;
			if (int.TryParse(deliveryMethodIdValue, out deliveryMethodId) == false)
			{
				deliveryMethodId = 1; //by default
			}

			IDeliveryMethod deliveryMethod = _deliveryMethodService.GetById(_context, deliveryMethodId);
			string shippingPriceText = deliveryMethod != null
				? deliveryMethod.FreightCost.ToString(_priceFormat, CultureInfo.InvariantCulture)
				: "0.00";

			_shippingPrice = string.Format("{0} {1}", shippingPriceText, _context.Channel.Currency.Iso);
		}
	}
}