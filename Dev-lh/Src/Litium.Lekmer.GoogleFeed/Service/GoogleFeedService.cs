﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.GoogleFeed
{
	public class GoogleFeedService : IGoogleFeedService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private ICategoryTree _categoryTree;
		private Dictionary<int, Dictionary<int, IBrand>> _brands; // brand_id -> channel_id -> brand

		protected IGoogleFeedSetting GoogleFeedSetting { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ISizeTableService SizeTableService { get; private set; }		
		protected IProductImageGroupService ProductImageGroupService { get; private set; }

		public GoogleFeedService(
			IGoogleFeedSetting googleFeedSetting,
			ILekmerProductService productService,
			IProductSizeService productSizeService,
			ICategoryService categoryService,
			IBrandService brandService,
			ISizeTableService sizeTableService,
			IProductImageGroupService productImageGroupService)
		{
			GoogleFeedSetting = googleFeedSetting;
			ProductService = productService;
			ProductSizeService = productSizeService;
			CategoryService = categoryService;
			BrandService = brandService;
			SizeTableService = sizeTableService;
			ProductImageGroupService = productImageGroupService;
		}

		public virtual Collection<IGoogleFeedInfo> GetGoogleFeedInfoList(IUserContext userContext)
		{
			var googleFeedInfoFullList = new Collection<IGoogleFeedInfo>();

			string channelName = userContext.Channel.CommonName;
			_log.InfoFormat("Reading data is started. //{0}", channelName);

			PreloadCategories(userContext);
			PreloadBrands(userContext);

			_log.Info("Reading product data - all / all.");
			var pageInfo = IoC.Resolve<IPageTracker>().Initialize(GoogleFeedSetting.PageSize, 1);
			while (pageInfo.HasNext)
			{
				ProductCollection products = ProductService.GetViewAllForExport(userContext, pageInfo.Current, pageInfo.PageSize);
				Collection<IGoogleFeedInfo> googleFeedInfoBatchList = PopulateGoogleFeedtInfo(userContext, products);
				AddToCollection(googleFeedInfoFullList, googleFeedInfoBatchList);

				pageInfo.MoveToNext(products.TotalCount);
				_log.InfoFormat("Reading product data  - {0} / {1}.", pageInfo.ItemsLeft, pageInfo.ItemsTotalCount);
				//break;
			}

			_log.InfoFormat("Reading data is done. //{0}", channelName);
			return googleFeedInfoFullList;
		}

		protected virtual Collection<IGoogleFeedInfo> PopulateGoogleFeedtInfo(IUserContext userContext, ProductCollection productsBatch)
		{
			var googleFeedtInfoList = new Collection<IGoogleFeedInfo>();

			foreach (ILekmerProductView product in productsBatch)
			{
				var googleFeedInfo = IoC.Resolve<IGoogleFeedInfo>();
				googleFeedInfo.Product = product;
				// Brand
				SupplementWithBrandInfo(userContext, googleFeedInfo);
				// Category
				SupplementWithCategoryInfo(googleFeedInfo);
				// Images
				googleFeedInfo.ImageGroups = ProductImageGroupService.GetAllFullByProduct(product.Id);
				// SizeTable
				googleFeedInfo.SizeTable = SizeTableService.GetByProduct(userContext, googleFeedInfo.Product.Id);

				googleFeedtInfoList.Add(googleFeedInfo);
			}

			//Sizes
			SupplementWithProductSizes(productsBatch);

			return googleFeedtInfoList;
		}

		protected virtual void SupplementWithBrandInfo(IUserContext userContext, IGoogleFeedInfo googleFeedInfo)
		{
			if (googleFeedInfo.Product.BrandId.HasValue)
			{
				googleFeedInfo.Product.Brand = FindBrand(userContext.Channel.Id, googleFeedInfo.Product.BrandId.Value);
			}
		}
		protected virtual IBrand FindBrand(int channelId, int brandId)
		{
			if (_brands.ContainsKey(channelId))
			{
				if (_brands[channelId].ContainsKey(brandId))
				{
					return _brands[channelId][brandId];
				}
			}

			return null;
		}

		protected virtual void SupplementWithCategoryInfo(IGoogleFeedInfo googleFeedInfo)
		{
			ICategoryTreeItem categoryTreeItem = _categoryTree.FindItemById(googleFeedInfo.Product.CategoryId);
			if (categoryTreeItem != null && categoryTreeItem.Category != null)
			{
				googleFeedInfo.ThirdLevelCategoryTitle = categoryTreeItem.Category.Title;

				categoryTreeItem = categoryTreeItem.Parent;
				if (categoryTreeItem != null && categoryTreeItem.Category != null)
				{
					googleFeedInfo.SecondLevelCategoryTitle = categoryTreeItem.Category.Title;

					categoryTreeItem = categoryTreeItem.Parent;
					if (categoryTreeItem != null && categoryTreeItem.Category != null)
					{
						googleFeedInfo.FirstLevelCategoryTitle = categoryTreeItem.Category.Title;
					}
				}
			}
		}

		protected virtual void SupplementWithProductSizes(ProductCollection products)
		{
			var productIds = new ProductIdCollection(products.Select(p => p.Id).ToArray());
			Collection<IProductSize> productSizes = ProductSizeService.GetAllByProductIdList(productIds);
			Dictionary<int, Collection<IProductSize>> productSizesByProduct = productSizes.GroupBy(ps => ps.ProductId).ToDictionary(ps => ps.Key, ps => new Collection<IProductSize>(ps.ToArray()));

			foreach (ILekmerProductView product in products)
			{
				if (productSizesByProduct.ContainsKey(product.Id))
				{
					product.ProductSizes = productSizesByProduct[product.Id];
				}
			}
		}

		protected virtual void PreloadCategories(IUserContext userContext)
		{
			_categoryTree = CategoryService.GetAllAsTree(userContext);
		}

		protected virtual void PreloadBrands(IUserContext userContext)
		{
			if (_brands == null)
			{
				_brands = new Dictionary<int, Dictionary<int, IBrand>>();
			}

			if (_brands.ContainsKey(userContext.Channel.Id))
			{
				return;
			}

			var channelBrands = new Dictionary<int, IBrand>();
			_brands[userContext.Channel.Id] = channelBrands;

			BrandCollection brandCollection = BrandService.GetAll(userContext);
			foreach (IBrand brand in brandCollection)
			{
				channelBrands[brand.Id] = brand;
			}
		}

		protected virtual void AddToCollection(Collection<IGoogleFeedInfo> googleFeedInfoFullList, Collection<IGoogleFeedInfo> googleFeedInfoBatchList)
		{
			foreach (IGoogleFeedInfo googleFeedInfo in googleFeedInfoBatchList)
			{
				googleFeedInfoFullList.Add(googleFeedInfo);
			}
		}
	}
}