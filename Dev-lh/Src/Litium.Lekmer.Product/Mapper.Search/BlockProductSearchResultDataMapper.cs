﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockProductSearchResultDataMapper : DataMapperBase<IBlockProductSearchResult>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockProductSearchResultDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockProductSearchResult Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductSearchResult = IoC.Resolve<IBlockProductSearchResult>();
			block.ConvertTo(blockProductSearchResult);
			blockProductSearchResult.Setting = _blockSettingDataMapper.MapRow();
			blockProductSearchResult.Status = BusinessObjectStatus.Untouched;
			return blockProductSearchResult;
		}
	}
}
