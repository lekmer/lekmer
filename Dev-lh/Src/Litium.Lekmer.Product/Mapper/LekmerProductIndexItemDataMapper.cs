﻿using System.Data;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	public class LekmerProductIndexItemDataMapper : ProductIndexItemDataMapper
	{
		public LekmerProductIndexItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
		protected override IProductIndexItem Create()
		{
			var item = (ILekmerProductIndexItem)base.Create();
			item.LekmerErpId = MapNullableValue<string>("Lekmer.LekmerErpId");
			return item;
		}
	}
}