﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableMediaRegistryDataMapper : DataMapperBase<ISizeTableMediaRegistry>
	{
		public SizeTableMediaRegistryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableMediaRegistry Create()
		{
			var sizeTableMediaRegistry = IoC.Resolve<ISizeTableMediaRegistry>();

			sizeTableMediaRegistry.Id = MapValue<int>("SizeTableMediaProductRegistry.SizeTableMediaProductRegistryId");
			sizeTableMediaRegistry.SizeTableMediaId = MapValue<int>("SizeTableMediaProductRegistry.SizeTableMediaId");
			sizeTableMediaRegistry.ProductRegistryId = MapValue<int>("SizeTableMediaProductRegistry.ProductRegistryId");

			sizeTableMediaRegistry.SetUntouched();

			return sizeTableMediaRegistry;
		}
	}
}