﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ContentMessageFolderDataMapper : DataMapperBase<IContentMessageFolder>
	{
		public ContentMessageFolderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IContentMessageFolder Create()
		{
			var contentMessageFolder = IoC.Resolve<IContentMessageFolder>();

			contentMessageFolder.Id = MapValue<int>("ContentMessageFolder.ContentMessageFolderId");
			contentMessageFolder.ParentFolderId = MapNullableValue<int?>("ContentMessageFolder.ParentContentMessageFolderId");
			contentMessageFolder.Title = MapValue<string>("ContentMessageFolder.Title");

			contentMessageFolder.SetUntouched();

			return contentMessageFolder;
		}
	}
}