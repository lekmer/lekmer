﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	internal class LekmerProductDataMapper : ProductDataMapper<IProduct>
	{
		private DataMapperBase<IDeliveryTime> _deliveryTimeDataMapper;

		public LekmerProductDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_deliveryTimeDataMapper = DataMapperResolver.Resolve<IDeliveryTime>(DataReader);
		}

		protected override IProduct Create()
		{
			var product = (ILekmerProduct) base.Create();

			product.BrandId = MapNullableValue<int?>("Lekmer.BrandId");
			product.IsBookable = MapValue<bool>("Lekmer.IsBookable");
			product.IsNewFrom = MapNullableValue<DateTime?>("Lekmer.IsNewFrom");
			product.IsNewTo = MapNullableValue<DateTime?>("Lekmer.IsNewTo");
			product.CreatedDate = MapNullableValue<DateTime?>("Lekmer.CreatedDate");
			product.LekmerErpId = MapNullableValue<string>("Lekmer.LekmerErpId");
			product.UrlTitle = MapValue<string>("Lekmer.UrlTitle");
			product.HasSizes = MapValue<bool>("Lekmer.HasSizes");
			product.ParentContentNodeId = MapNullableValue<int?>("Product.ParentContentNodeId");
			product.ShowVariantRelations = MapValue<bool>("Lekmer.ShowVariantRelations");
			product.Weight = MapNullableValue<decimal?>("Lekmer.Weight");
			product.ProductTypeId = MapValue<int>("Lekmer.ProductTypeId");
			product.IsAbove60L = MapValue<bool>("Lekmer.IsAbove60L");
			product.StockStatusId = MapValue<int>("Lekmer.StockStatusId");
			product.MonitorThreshold = MapNullableValue<int?>("Lekmer.MonitorThreshold");
			product.MaxQuantityPerOrder = MapNullableValue<int?>("Lekmer.MaxQuantityPerOrder");
			product.DeliveryTimeId = MapNullableValue<int?>("Lekmer.DeliveryTimeId");
			product.DeliveryTime = product.DeliveryTimeId.HasValue ? _deliveryTimeDataMapper.MapRow() : null;
			product.RecommendedPrice = MapNullableValue<decimal?>("Lekmer.RecommendedPrice");
			product.SellOnlyInPackage = MapValue<bool>("Lekmer.SellOnlyInPackage");
			product.SupplierArticleNumber = MapNullableValue<string>("Lekmer.SupplierArticleNumber");
			product.IsDropShip = MapValue<bool>("Lekmer.IsDropShip");

			return product;
		}
	}
}