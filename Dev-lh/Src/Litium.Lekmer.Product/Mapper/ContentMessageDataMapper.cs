﻿using System.Data;

namespace Litium.Lekmer.Product.Mapper
{
	public class ContentMessageDataMapper : EntityTimeLimitDataMapper<IContentMessage>
	{
		public ContentMessageDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IContentMessage Create()
		{
			var contentMessage = base.Create();
			contentMessage.Id = MapValue<int>("ContentMessage.ContentMessageId");
			contentMessage.ContentMessageFolderId = MapValue<int>("ContentMessage.ContentMessageFolderId");
			contentMessage.CommonName = MapValue<string>("ContentMessage.CommonName");
			contentMessage.Message = MapNullableValue<string>("ContentMessage.Message");
			contentMessage.IsDropShip = MapValue<bool>("ContentMessage.IsDropShip");
			contentMessage.SetUntouched();
			return contentMessage;
		}
	}
}