﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockProductImageListDataMapper : DataMapperBase<IBlockProductImageList>
	{
		private int _productSortOrderId;

		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockProductImageListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);

			_productSortOrderId = OrdinalOf("BlockProductImageList.ProductImageGroupId");
		}

		protected override IBlockProductImageList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductImageList = IoC.Resolve<IBlockProductImageList>();
			block.ConvertTo(blockProductImageList);
			blockProductImageList.ProductImageGroupId = MapNullableValue<int?>(_productSortOrderId);
			blockProductImageList.Setting = _blockSettingDataMapper.MapRow();
			blockProductImageList.Status = BusinessObjectStatus.Untouched;
			return blockProductImageList;
		}
	}
}
