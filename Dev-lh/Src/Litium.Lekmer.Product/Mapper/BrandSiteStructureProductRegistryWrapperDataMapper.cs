using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class BrandSiteStructureProductRegistryWrapperDataMapper : DataMapperBase<IBrandSiteStructureProductRegistryWrapper>
	{
		public BrandSiteStructureProductRegistryWrapperDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBrandSiteStructureProductRegistryWrapper Create()
		{
			var brandSiteStructureProductRegistryWrapper = IoC.Resolve<IBrandSiteStructureProductRegistryWrapper>();

			brandSiteStructureProductRegistryWrapper.BrandId = MapValue<int>("BrandSiteStructureProductRegistryWrapper.BrandId");
			brandSiteStructureProductRegistryWrapper.SiteStructureRegistryId = MapValue<int>("BrandSiteStructureProductRegistryWrapper.SiteStructureRegistryId");
			brandSiteStructureProductRegistryWrapper.ContentNodeId = MapValue<int>("BrandSiteStructureProductRegistryWrapper.ContentNodeId");
			brandSiteStructureProductRegistryWrapper.ProductRegistryId = MapValue<int>("BrandSiteStructureProductRegistryWrapper.ProductRegistryId");
			brandSiteStructureProductRegistryWrapper.Title = MapValue<string>("BrandSiteStructureProductRegistryWrapper.Title");
			brandSiteStructureProductRegistryWrapper.ContentNodeStatusId = MapValue<int>("BrandSiteStructureProductRegistryWrapper.ContentNodeStatusId");
			brandSiteStructureProductRegistryWrapper.Status = BusinessObjectStatus.Untouched;

			return brandSiteStructureProductRegistryWrapper;
		}
	}
}