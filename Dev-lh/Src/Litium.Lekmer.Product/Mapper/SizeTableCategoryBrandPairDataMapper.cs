using System.Data;
using System.Globalization;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableCategoryBrandPairDataMapper : DataMapperBase<ISizeTableCategoryBrandPair>
	{
		public SizeTableCategoryBrandPairDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableCategoryBrandPair Create()
		{
			var sizeTableCategoryBrandPair = IoC.Resolve<ISizeTableCategoryBrandPair>();

			sizeTableCategoryBrandPair.Id = MapValue<int>("Id").ToString(CultureInfo.InvariantCulture);
			sizeTableCategoryBrandPair.CategoryId = MapValue<int>("CategoryId");
			sizeTableCategoryBrandPair.BrandId = MapNullableValue<int?>("BrandId");

			return sizeTableCategoryBrandPair;
		}
	}
}