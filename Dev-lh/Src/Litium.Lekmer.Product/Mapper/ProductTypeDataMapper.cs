﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductTypeDataMapper : DataMapperBase<IProductType>
	{
		public ProductTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductType Create()
		{
			var productType = IoC.Resolve<IProductType>();
			productType.Id = MapValue<int>("ProductType.Id");
			productType.Title = MapValue<string>("ProductType.Title");
			productType.CommonName = MapValue<string>("ProductType.CommonName");
			productType.SetUntouched();
			return productType;
		}
	}
}