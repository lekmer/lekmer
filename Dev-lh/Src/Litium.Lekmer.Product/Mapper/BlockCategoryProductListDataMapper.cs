﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockCategoryProductListDataMapper : DataMapperBase<IBlockCategoryProductList>
	{
		private int _productSortOrderId;

		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IProductSortOrder> _productSortOrderDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockCategoryProductListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_productSortOrderDataMapper = DataMapperResolver.Resolve<IProductSortOrder>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);

			_productSortOrderId = OrdinalOf("BlockCategoryProductList.ProductSortOrderId");
		}

		protected override IBlockCategoryProductList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockCategoryProductList = IoC.Resolve<IBlockCategoryProductList>();
			block.ConvertTo(blockCategoryProductList);
			blockCategoryProductList.ProductSortOrderId = MapValue<int>(_productSortOrderId);
			blockCategoryProductList.ProductSortOrder = _productSortOrderDataMapper.MapRow();
			blockCategoryProductList.Setting = _blockSettingDataMapper.MapRow();
			blockCategoryProductList.Status = BusinessObjectStatus.Untouched;
			return blockCategoryProductList;
		}
	}
}
