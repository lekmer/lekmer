﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockReviewMyWishListDataMapper : DataMapperBase<IBlockReviewMyWishList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockReviewMyWishListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}
		protected override IBlockReviewMyWishList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockReviewMyWishList = IoC.Resolve<IBlockReviewMyWishList>();
			block.ConvertTo(blockReviewMyWishList);
			blockReviewMyWishList.Setting = _blockSettingDataMapper.MapRow();
			blockReviewMyWishList.Status = BusinessObjectStatus.Untouched;
			return blockReviewMyWishList;
		}
	}
}

