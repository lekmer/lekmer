﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeDeviationDataMapper : DataMapperBase<ISizeDeviation>
	{
		public SizeDeviationDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ISizeDeviation Create()
		{
			var sizeDeviation = IoC.Resolve<ISizeDeviation>();
			sizeDeviation.Id = MapValue<int>("SizeDeviation.Id");
			sizeDeviation.Title = MapValue<string>("SizeDeviation.Title");
			sizeDeviation.CommonName = MapValue<string>("SizeDeviation.CommonName");
			return sizeDeviation;
		}
	}
}
