﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableMediaRecordDataMapper : DataMapperBase<ISizeTableMediaRecord>
	{
		public SizeTableMediaRecordDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableMediaRecord Create()
		{
			var sizeTableMediaRecord = IoC.Resolve<ISizeTableMediaRecord>();

			sizeTableMediaRecord.Id = MapValue<int>("SizeTableMedia.SizeTableMediaId");
			sizeTableMediaRecord.SizeTableId = MapValue<int>("SizeTableMedia.SizeTableId");
			sizeTableMediaRecord.MediaId = MapValue<int>("SizeTableMedia.MediaId");

			sizeTableMediaRecord.SetUntouched();

			return sizeTableMediaRecord;
		}
	}
}