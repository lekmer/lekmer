﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ContentMessageTranslationDataMapper : DataMapperBase<IContentMessageTranslation>
	{
		public ContentMessageTranslationDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IContentMessageTranslation Create()
		{
			var contentMessageTranslation = IoC.Resolve<IContentMessageTranslation>();
			contentMessageTranslation.ContentMessageId = MapValue<int>("ContentMessageTranslation.ContentMessageId");
			contentMessageTranslation.LanguageId = MapValue<int>("ContentMessageTranslation.LanguageId");
			contentMessageTranslation.Message = MapNullableValue<string>("ContentMessageTranslation.Message");
			contentMessageTranslation.SetUntouched();
			return contentMessageTranslation;
		}
	}
}