﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IBlockProductSearchEsalesResult"/>.
	/// </summary>
	public sealed class BlockProductSearchEsalesResultCache :
		ScensumCacheBase<BlockProductSearchEsalesResultKey, IBlockProductSearchEsalesResult>
	{
		private static readonly BlockProductSearchEsalesResultCache _instance = new BlockProductSearchEsalesResultCache();

		private BlockProductSearchEsalesResultCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static BlockProductSearchEsalesResultCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockProductSearchEsalesResultKey(channel.Id, blockId));
			}
		}
	}

	/// <summary>
	/// Key used for caching <see cref="IBlockProductSearchEsalesResult"/>.
	/// </summary>
	public class BlockProductSearchEsalesResultKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="channelId">Channel id of the <see cref="IBlockProductSearchEsalesResult"/>.</param>
		/// <param name="blockId">Id of the <see cref="IBlockProductSearchEsalesResult"/>.</param>
		public BlockProductSearchEsalesResultKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		/// <summary>
		/// Channel id of the <see cref="IBlockProductSearchEsalesResult"/>.
		/// </summary>
		public int ChannelId { get; set; }

		/// <summary>
		/// Id of the <see cref="IBlockProductSearchEsalesResult"/>.
		/// </summary>
		public int BlockId { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}