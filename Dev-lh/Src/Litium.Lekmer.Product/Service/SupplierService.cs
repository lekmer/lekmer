﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class SupplierService : ISupplierService
	{
		protected SupplierRepository Repository { get; private set; }

		public SupplierService(SupplierRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<ISupplier> GetAll()
		{
			return Repository.GetAll();
		}
	}
}