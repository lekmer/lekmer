using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ContentMessageService : IContentMessageService
	{
		protected ContentMessageRepository Repository { get; private set; }

		public ContentMessageService(ContentMessageRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IContentMessage> GetAllByProduct(IUserContext userContext, int productId)
		{
			if (userContext == null)
			{
				throw new ArgumentNullException("userContext");
			}

			return ProductContentMessageCache.Instance.TryGetItem(
				new ProductContentMessageCacheKey(userContext.Channel.Id, productId),
				() => Repository.GetAllByProduct(productId, userContext.Channel.Language.Id));
		}
	}
}