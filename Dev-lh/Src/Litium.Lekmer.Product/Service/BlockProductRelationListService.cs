﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockProductRelationListService : IBlockProductRelationListService
	{
		protected BlockProductRelationListRepository Repository { get; private set; }

		public BlockProductRelationListService(BlockProductRelationListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductRelationList GetById(IUserContext context, int id)
		{
			return BlockProductRelationListCache.Instance.TryGetItem(
				new BlockProductRelationListKey(context.Channel.Id, id),
				delegate { return GetByIdCore(context, id); });
		}

		protected virtual IBlockProductRelationList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}
