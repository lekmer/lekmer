using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class StockRangeService : IStockRangeService
	{
		protected StockRangeRepository Repository { get; private set; }

		public StockRangeService(StockRangeRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IStockRange> GetAll(IUserContext context)
		{
			return StockRangeListCache.Instance.TryGetItem(
				new StockRangeListKey(context.Channel.Id), 
				delegate { return Repository.GetAll(); });
		}
	}
}