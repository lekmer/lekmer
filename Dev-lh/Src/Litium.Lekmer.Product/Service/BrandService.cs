﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class BrandService : IBrandService
	{
		protected BrandRepository BrandRepository { get; private set; }

		protected IBlockBrandProductListBrandService BlockBrandProductListBrandService { get; private set; }

		public BrandService(BrandRepository repository)
		{
			BrandRepository = repository;
		}

		public virtual IBrand Create()
		{
			IBrand brand = IoC.Resolve<IBrand>();

			brand.Status = BusinessObjectStatus.New;

			return brand;
		}

		public virtual IBrandSiteStructureRegistry GetByNode(int nodeId)
		{
			return BrandRepository.GetByNodeId(nodeId);
		}

		public virtual IBrand GetById(IUserContext context, int brandId)
		{
			return BrandCache.Instance.TryGetItem(
				new BrandKey(context.Channel.Id, brandId),
				delegate { return BrandRepository.GetById(context.Channel.Id, brandId); });
		}

		public BrandCollection GetAllByCategory(IUserContext context, int categoryId)
		{
			return BrandRepository.GetAllByCategoryId(context, categoryId);
		}

		public virtual BrandCollection GetAllByBlock(IUserContext context, IBlockBrandList block, int page, int pageSize)
		{
			return BlockBrandListBrandCollectionCache.Instance.TryGetItem(
				new BlockBrandListBrandCollectionKey(context.Channel.Id, block.Id, page, pageSize),
				delegate { return BrandRepository.GetAllByBlock(context.Channel.Id, block.Id, page, pageSize); });
		}

		public virtual BrandCollection GetAllByBlockBrandProductList(IUserContext context, IBlockBrandProductList block)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			var resolvedBrands = BrandRepository.GetAllByBlock(context.Channel.Id, block.Id);

			return resolvedBrands;
		}

		public virtual BrandCollection GetAll(IUserContext context)
		{
			return BrandListCache.Instance.TryGetItem(
				new BrandListKey(context.Channel.Id),
				delegate { return BrandRepository.GetAll(context); });
		}

		public virtual BrandCollection PopulateBrands(IUserContext context, BrandIdCollection brandIds)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (brandIds == null)
			{
				throw new ArgumentNullException("brandIds");
			}
			
			Dictionary<int, IBrand> dictionary = new Dictionary<int, IBrand>();
			BrandIdCollection list = new BrandIdCollection();

			foreach (int brandId in brandIds)
			{
				IBrand data = BrandCache.Instance.GetData(new BrandKey(context.Channel.Id, brandId));
				if (data != null)
				{
					dictionary.Add(brandId, data);
				}
				else
				{
					list.Add(brandId);
				}
			}

			if (list.Count > 0)
			{
				BrandCollection brands = GetAllByIds(context, list);
				foreach (IBrand brand in brands)
				{
					dictionary.Add(brand.Id, brand);
					BrandCache.Instance.Add(new BrandKey(context.Channel.Id, brand.Id), brand);
				}
			}

			BrandCollection brandCollection = new BrandCollection();

			foreach (int key in brandIds)
			{
				IBrand brand;
				if (dictionary.TryGetValue(key, out brand))
				{
					brandCollection.Add(brand);
				}
			}

			brandCollection.TotalCount = brandIds.TotalCount;

			return brandCollection;
		}

		public virtual int? GetIdByTitle(IUserContext context, string title)
		{
			IBrand brand = GetAll(context).FirstOrDefault(b => b.Title.Equals(title, StringComparison.OrdinalIgnoreCase));

			return brand != null ? brand.Id : (int?)null;
		}


		protected virtual BrandCollection GetAllByIds(IUserContext context, BrandIdCollection brandIds)
		{
			return BrandRepository.GetAllByIdList(context.Channel.Id, Scensum.Foundation.Convert.ToStringIdentifierList(brandIds), ',');
		}
	}
}