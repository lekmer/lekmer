﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductUrlService : IProductUrlService
	{
		protected ProductUrlRepository Repository { get; private set; }

		public ProductUrlService(ProductUrlRepository repository)
		{
			Repository = repository;
		}

		public int? GetIdByUrlTitle(IUserContext context, string urlTitle)
		{
			//return Url
			ProductIdWrapper productIdWrapper = ProductUrlIdCache.Instance.TryGetItem(
				new ProductUrlIdKey(context.Channel.Id, urlTitle),
				delegate
					{
						int? productId = Repository.GetIdByUrlTitle(context.Channel.Language.Id, urlTitle);
						if (productId == null) return null;
						return new ProductIdWrapper(productId);
					});
			return productIdWrapper != null ? productIdWrapper.ProductId : null;
		}
	}
}