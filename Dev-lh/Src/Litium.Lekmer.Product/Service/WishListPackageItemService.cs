﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class WishListPackageItemService : IWishListPackageItemService
	{
		protected WishListPackageItemRepository Repository { get; private set; }

		public WishListPackageItemService(WishListPackageItemRepository repository)
		{
			Repository = repository;
		}

		public IWishListPackageItem Create()
		{
			var item = IoC.Resolve<IWishListPackageItem>();
			item.Status = BusinessObjectStatus.New;
			return item;
		}

		public Collection<IWishListPackageItem> GetAllByWishListItem(int wishListItemId)
		{
			var items = Repository.GetAllByWishListItem(wishListItemId);
			return items ?? new Collection<IWishListPackageItem>();
		}

		public int Save(IWishListPackageItem item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			return Repository.Save(item);
		}

		public void Update(IWishListPackageItem item)
		{
			if (item == null)
			{
				throw new ArgumentNullException("item");
			}

			Repository.Update(item);
		}
	}
}