using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product.Cache;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class ProductImageGroupService : IProductImageGroupService
	{
		protected ProductImageGroupRepository Repository { get; private set; }
		protected IProductImageService ProductImageService { get; private set; }

		public ProductImageGroupService(ProductImageGroupRepository repository, IProductImageService productImageService)
		{
			Repository = repository;
			ProductImageService = productImageService;
		}

		public virtual Collection<IProductImageGroup> GetAll()
		{
			return ProductImageGroupCollectionCache<IProductImageGroup>.Instance.TryGetItem(
				new ProductImageGroupCollectionKey(),
				() => Repository.GetAll<IProductImageGroup>()
			);
		}

		public virtual Collection<IProductImageGroupFull> GetAllFullWithoutProductImages()
		{
			return ProductImageGroupCollectionCache<IProductImageGroupFull>.Instance.TryGetItem(
				new ProductImageGroupFullWithoutProductImagesCollectionKey(),
				() => Repository.GetAll<IProductImageGroupFull>()
			);
		}

		public virtual Collection<IProductImageGroupFull> GetAllFullByProduct(int productId)
		{
			return ProductImageGroupCollectionCache<IProductImageGroupFull>.Instance.TryGetItem(
				new ProductImageGroupFullCollectionKey(productId),
				() => GetAllFullByProductCore(productId)
			);
		}


		public virtual TProductImageGroup Clone<TProductImageGroup>(TProductImageGroup sourceProductImageGroup) where TProductImageGroup : IProductImageGroup
		{
			var productImageGroup = IoC.Resolve<TProductImageGroup>();

			productImageGroup.Id = sourceProductImageGroup.Id;
			productImageGroup.Title = sourceProductImageGroup.Title;
			productImageGroup.CommonName = sourceProductImageGroup.CommonName;
			productImageGroup.MaxCount = sourceProductImageGroup.MaxCount;

			productImageGroup.Status = BusinessObjectStatus.Untouched;

			return productImageGroup;
		}

		public virtual Collection<TProductImageGroup> Clone<TProductImageGroup>(Collection<TProductImageGroup> sourceProductImageGroups) where TProductImageGroup : IProductImageGroup
		{
			var productImageGroups = new Collection<TProductImageGroup>();

			foreach (TProductImageGroup sourceProductImageGroup in sourceProductImageGroups)
			{
				productImageGroups.Add(Clone(sourceProductImageGroup));
			}

			return productImageGroups;
		}


		protected virtual Collection<IProductImageGroupFull> GetAllFullByProductCore(int productId)
		{
			Collection<IProductImageGroupFull> groups = GetAllFullWithoutProductImages();
			groups = Clone(groups); //!!!

			var productImages = new Collection<IProductImage>(ProductImageService.GetAllByProduct(productId).Where(item => !((ILekmerImage)item.Image).IsLink).ToArray());
			Dictionary<int, Collection<IProductImage>> productImagesDictionary = productImages.GroupBy(i => i.ProductImageGroupId).ToDictionary(i => i.Key, i => new Collection<IProductImage>(i.ToArray()));

			foreach (IProductImageGroupFull group in groups)
			{
				group.ProductImages = productImagesDictionary.ContainsKey(group.Id) ? productImagesDictionary[group.Id] : new Collection<IProductImage>();
			}

			return groups;
		}
	}
}