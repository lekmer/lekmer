﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class WishListRepository
	{
		protected virtual DataMapperBase<IWishList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IWishList>(dataReader);
		}

		public virtual IWishList GetByKey(Guid key)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Key", key, SqlDbType.UniqueIdentifier)
			};

			DatabaseSetting dbSettings = new DatabaseSetting("WishListRepository.GetByKey");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pWishListGetByKey]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual int Save(IWishList wishList)
		{
			if (wishList == null)
			{
				throw new ArgumentNullException("wishList");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Id", wishList.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Key", wishList.Key, SqlDbType.UniqueIdentifier),
				ParameterHelper.CreateParameter("@Title", wishList.Title, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@CreatedDate", wishList.CreatedDate, SqlDbType.DateTime),
				ParameterHelper.CreateParameter("@UpdatedDate", wishList.UpdatedDate, SqlDbType.DateTime),
				ParameterHelper.CreateParameter("@IpAddress", wishList.IpAddress, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@NumberOfRemovedItems", wishList.NumberOfRemovedItems, SqlDbType.Int)
			};

			DatabaseSetting dbSettings = new DatabaseSetting("WishListRepository.Save");

			wishList.Id = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pWishListSave]", parameters, dbSettings);
			return wishList.Id;
		}

		public virtual void Delete(IWishList wishList)
		{
			if (wishList == null)
			{
				throw new ArgumentNullException("wishList");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Id", wishList.Id, SqlDbType.Int)
			};

			DatabaseSetting dbSettings = new DatabaseSetting("WishListRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pWishListDelete]", parameters, dbSettings);
		}
	}
}