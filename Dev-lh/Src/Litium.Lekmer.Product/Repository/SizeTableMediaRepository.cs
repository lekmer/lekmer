﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeTableMediaRepository
	{
		protected virtual DataMapperBase<ISizeTableMediaRecord> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableMediaRecord>(dataReader);
		}

		public virtual Collection<ISizeTableMediaRecord> GetAllBySizeTable(int sizeTableId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SizeTableMediaRepository.GetAllBySizeTable");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableMediaGetAllBySizeTable]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(ISizeTableMediaRecord sizeTableMediaRecord)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SizeTableMediaId", sizeTableMediaRecord.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("SizeTableId", sizeTableMediaRecord.SizeTableId, SqlDbType.Int),
				ParameterHelper.CreateParameter("MediaId", sizeTableMediaRecord.MediaId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SizeTableMediaRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeTableMediaSave]", parameters, dbSettings);
		}

		public virtual void Delete(int sizeTableMediaId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SizeTableMediaId", sizeTableMediaId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SizeTableMediaRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableMediaDelete]", parameters, dbSettings);
		}
	}
}