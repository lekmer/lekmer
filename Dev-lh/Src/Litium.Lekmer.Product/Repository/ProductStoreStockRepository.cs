﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductStoreStockRepository
	{
		protected virtual DataMapperBase<IProductStoreStock> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductStoreStock>(dataReader);
		}

		public virtual Collection<IProductStoreStock> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ProductStoreStockRepository.GetAllByProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].[pProductStoreStockByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
