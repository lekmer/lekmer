﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductTitleTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductTitleTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get { return string.Empty; }
		}
	}
}
