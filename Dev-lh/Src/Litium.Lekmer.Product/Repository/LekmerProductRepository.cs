﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Litium.Framework.DataAccess;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerProductRepository : ProductRepository
	{
		public override int Save(IProductRecord product)
		{
			if (product == null) throw new ArgumentNullException("product");

			var lekmerProduct = product as ILekmerProductRecord;
			if (lekmerProduct == null) throw new ArgumentException("product should be of ILekmerProductRecord type");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", lekmerProduct.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ItemsInPackage", lekmerProduct.ItemsInPackage, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ErpId", lekmerProduct.ErpId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@EanCode", lekmerProduct.EanCode, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title", lekmerProduct.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@WebShopTitle", lekmerProduct.WebShopTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@ShortDescription", lekmerProduct.ShortDescription, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@CategoryId", lekmerProduct.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MediaId", lekmerProduct.MediaId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Description", lekmerProduct.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@BrandId", lekmerProduct.BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IsBookable", lekmerProduct.IsBookable, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@AgeFromMonth", lekmerProduct.AgeFromMonth, SqlDbType.Int),
					ParameterHelper.CreateParameter("@AgeToMonth", lekmerProduct.AgeToMonth, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IsNewFrom", lekmerProduct.IsNewFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@IsNewTo", lekmerProduct.IsNewTo, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@Measurement", lekmerProduct.Measurement, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@BatteryTypeId", lekmerProduct.BatteryTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@NumberOfBatteries", lekmerProduct.NumberOfBatteries, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IsBatteryIncluded", lekmerProduct.IsBatteryIncluded, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@ExpectedBackInStock", lekmerProduct.ExpectedBackInStock, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@CreatedDate", lekmerProduct.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@SizeDeviationId", lekmerProduct.SizeDeviationId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ShowVariantRelations", lekmerProduct.ShowVariantRelations, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@ProductStatusId", lekmerProduct.ProductStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MonitorThreshold", lekmerProduct.MonitorThreshold, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MaxQuantityPerOrder", lekmerProduct.MaxQuantityPerOrder, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DeliveryTimeId", lekmerProduct.DeliveryTimeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SellOnlyInPackage", lekmerProduct.SellOnlyInPackage, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@IsDropShip", lekmerProduct.IsDropShip, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pProductSave]", parameters, dbSettings);
			return lekmerProduct.Id;
		}

		public virtual void DeletePopularity(int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.UpdatePopularity");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDeletePopularity]", parameters, dbSettings);
		}
		public virtual void UpdatePopularity(int channelId, DateTime dateTimeFrom, DateTime dateTimeTo)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("DateTimeFrom", dateTimeFrom, SqlDbType.SmallDateTime),
					ParameterHelper.CreateParameter("DateTimeTo", dateTimeTo, SqlDbType.SmallDateTime)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.UpdatePopularity");
			new DataHandler().ExecuteCommand("[lekmer].[pProductUpdatePopularity]", parameters, dbSettings);
		}

		public virtual bool IsProductDeleted(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.IsProductDeleted");
			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pProductIsDeleted]", parameters, dbSettings) == 1;
		}

		public override ProductCollection Search(int channelId, IProductSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null) throw new ArgumentNullException("searchCriteria");

			var lekmerProductSearchCriteria = (ILekmerProductSearchCriteria) searchCriteria;

			int itemsCount = 0;
			var products = new ProductCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@SupplierArticleNumber", lekmerProductSearchCriteria.SupplierArticleNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ProductTypeId", lekmerProductSearchCriteria.ProductTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BrandId", lekmerProductSearchCriteria.BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CategoryId", searchCriteria.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title", searchCriteria.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@PriceFrom", searchCriteria.PriceFrom, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@PriceTo", searchCriteria.PriceTo, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@ErpId", searchCriteria.ErpId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@StatusId", searchCriteria.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@EanCode", searchCriteria.EanCode, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].[pProductSearch]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					products = CreateDataMapper(dataReader).ReadMultipleRows<ProductCollection>();
				}
			}

			products.TotalCount = itemsCount;
			return products;
		}

		public virtual Dictionary<string, decimal> GetPriceByChannelAndCampaign(int currencyId, decimal priceLimit, IFixedPriceAction action)
		{
			var products = new Dictionary<string, decimal>();

			var includeCategories = string.Join(",", action.IncludeCategories.Keys.Select(x => x.ToString()).ToArray());
			var includeBrands = string.Join(",", action.IncludeBrands.Keys.Select(x => x.ToString()).ToArray());
			var includeProducts = string.Join(",", action.IncludeProducts.Keys.Select(x => x.ToString()).ToArray());

			var excludeCategories = string.Join(",", action.ExcludeCategories.Keys.Select(x => x.ToString()).ToArray());
			var excludeBrands = string.Join(",", action.ExcludeBrands.Keys.Select(x => x.ToString()).ToArray());
			var excludeProducts = string.Join(",", action.ExcludeProducts.Keys.Select(x => x.ToString()).ToArray());

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CurrencyId",currencyId,SqlDbType.Int),
					ParameterHelper.CreateParameter("@Price",priceLimit,SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@IncludeCategories",includeCategories,SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@IncludeBrands",includeBrands,SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@IncludeProducts",includeProducts,SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ExcludeCategories",excludeCategories,SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ExcludeBrands",excludeBrands,SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ExcludeProducts",excludeProducts,SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.pProductGetPriceByChannelAndCampaign");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetPriceByChannelAndCampaign]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					products.Add(dataReader.GetString(0), dataReader.GetDecimal(1));
				}
			}

			return products;
		}


		public virtual ProductIdCollection GetIdAll(IChannel channel, ICustomer customer, int page, int pageSize)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channel.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAll]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}

			productIds.TotalCount = totalCount;
			return productIds;
		}
		public virtual ProductIdCollection GetIdAllWithoutStatusFilter(IChannel channel, ICustomer customer, int page, int pageSize)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllWithoutStatusFilter");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllWithoutStatusFilter]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}

			productIds.TotalCount = totalCount;
			return productIds;
		}
		/// <summary>
		/// Gets all products ids, by ignoring product registry, product status, product prices. Just raw products !!!
		/// </summary>
		public virtual ProductIdCollection GetIdAllWithoutAnyFilter(int page, int pageSize, bool? isSellOnlyInPackage)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SellOnlyInPackage", isSellOnlyInPackage, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllWithoutAnyFilter");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllWithoutAnyFilter]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}

			productIds.TotalCount = totalCount;
			return productIds;
		}
		public virtual ProductIdCollection GetIdAllWithoutAnyFilterForCdonExport(int page, int pageSize, int monthPurchasedAgo)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MonthPurchasedAgo", monthPurchasedAgo, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllWithoutAnyFilterForCdonExport");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllWithoutAnyFilterForCdonExport]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}

			productIds.TotalCount = totalCount;
			return productIds;
		}
		public virtual ProductIdCollection GetIdAllByBlock(IChannel channel, ICustomer customer, IBlockProductList block, int page, int pageSize)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.GetIdAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].[pProductGetIdAllByBlock]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}
			productIds.TotalCount = totalCount;
			return productIds;
		}
		public virtual ProductIdCollection GetIdAllByBlockAndProduct(int channelId, ICustomer customer, IBlockProductRelationList block, int productId, bool showVariants, int pageNumber, int pageSize)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ShowVariants", showVariants, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@Page", pageNumber, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SortBy", block.ProductSortOrder.SortByColumn, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@SortDescending", block.ProductSortOrder.SortDescending, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllByBlockAndProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllByBlockAndProduct]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}

			productIds.TotalCount = totalCount;
			return productIds;
		}
		public virtual ProductIdCollection GetIdAllByProductAndRelationType(int channelId, ICustomer customer, int productId, string relationType)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@RelationType", relationType, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllByProductAndRelationType");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllByProductAndRelationType]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}
		public virtual ProductIdCollection GetIdAllByBrandIdList(int channelId, ICustomer customer, string categoryIdList, char delimiter, int pageNumber, int pageSize, IProductSortOrder sortOrder)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?) customer.Id : null,SqlDbType.Int),
					ParameterHelper.CreateParameter("@BrandIds", categoryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("@Page", pageNumber, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SortBy", sortOrder.SortByColumn, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@SortDescending", sortOrder.SortDescending, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.GetIdAllByBrandIdList");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllByBrandIdList]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}

			productIds.TotalCount = totalCount;
			return productIds;
		}
		public virtual ProductIdCollection GetIdAllByCampaign(int campaignId)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CampaignId",campaignId,SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllByCampaign");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdAllByCampaignId]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}
		public virtual ProductIdCollection GetVariantIdAllByProduct(int productId, int relationListTypeId)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@RelationListTypeId", relationListTypeId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetVariantIdAllByProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductVariantGetIdAllByProduct]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		public virtual IProductView GetProductViewIncludeOnlyInPackageById(int channelId, ICustomer customer, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetProductViewIncludeOnlyInPackageById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductGetViewInclPackageProductsById]", parameters, dbSettings))
			{
				return CreateViewDataMapper(dataReader).ReadRow();
			}
		}
		public virtual IProduct GetProductIncludeOnlyInPackageById(int channelId, ICustomer customer, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetProductIncludeOnlyInPackageById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductGetInclPackageProductsById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
		public virtual IProduct GetProductByIdWithoutStatusFilter(int channelId, ICustomer customer, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", (customer != null) ? new int?(customer.Id) : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetProductByIdWithoutStatusFilter");
			using (IDataReader reader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetByIdWithoutStatusFilter]", parameters, dbSettings))
			{
				return CreateDataMapper(reader).ReadRow();
			}
		}
		public virtual IProduct GetProductIncludeOnlyInPackageByIdWithoutStatusFilter(int channelId, ICustomer customer, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", (customer != null) ? new int?(customer.Id) : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetProductIncludeOnlyInPackageByIdWithoutStatusFilter");
			using (IDataReader reader = new DataHandler().ExecuteSelect("[productlek].[pProductGetInclPackageProductsByIdWithoutStatusFilter]", parameters, dbSettings))
			{
				return CreateDataMapper(reader).ReadRow();
			}
		}


		/// <summary>
		/// Gets all products by id list. Does not support paging !!!
		/// </summary>
		public virtual ProductCollection GetAllByIdList(int channelId, ICustomer customer, string idList, char delimiter)
		{
			return GetAllByIdList(channelId, customer, idList, delimiter, "[lekmer].[pProductGetAllByIdList]");
		}
		/// <summary>
		/// Gets all products include only package products by id list. Does not support paging !!!
		/// </summary>
		public virtual ProductCollection GetAllWithOnlyInPackageByIdList(int channelId, ICustomer customer, string idList, char delimiter)
		{
			return GetAllByIdList(channelId, customer, idList, delimiter, "[productlek].[pPackageProductGetAllByIdList]");
		}
		protected virtual ProductCollection GetAllByIdList(int channelId, ICustomer customer, string idList, char delimiter, string storedProcedureName)
		{
			ProductCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.ProductGetAllByIdList");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(storedProcedureName, parameters, dbSettings))
			{
				products = CreateDataMapper(dataReader).ReadMultipleRows<ProductCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}
		/// <summary>
		/// Gets all products by id list. Does not support paging !!!
		/// </summary>
		public virtual ProductCollection GetAllByIdListWithoutStatusFilter(int channelId, ICustomer customer, string idList, char delimiter)
		{
			ProductCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetAllByIdListWithoutStatusFilter");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetAllByIdListWithoutStatusFilter]", parameters, dbSettings))
			{
				products = CreateDataMapper(dataReader).ReadMultipleRows<ProductCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}
		/// <summary>
		/// Gets all products by id list without any filter.
		/// </summary>
		public virtual ProductCollection GetAllByIdListWithoutAnyFilter(int channelId, string idList, char delimiter)
		{
			ProductCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetAllByIdListWithoutAnyFilter");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductGetAllByIdListWithoutAnyFilter]", parameters, dbSettings))
			{
				products = CreateDataMapper(dataReader).ReadMultipleRows<ProductCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}


		public virtual LekmerProductViewCollection GetAllViewByIdList(int channelId, ICustomer customer, string idList, char delimiter, int pageNumber, int pageSize)
		{
			int totalCount = 0;
			var products = new LekmerProductViewCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("@Page", pageNumber, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.ProductGetAllByIdList");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].[pProductGetViewAllByIdList]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					products = CreateViewDataMapper(dataReader).ReadMultipleRows<LekmerProductViewCollection>();
				}
			}

			products.TotalCount = totalCount;
			return products;
		}
		/// <summary>
		/// Gets all product views by id list. Does not support paging !!!
		/// </summary>
		public virtual LekmerProductViewCollection GetAllViewByIdList(int channelId, ICustomer customer, string idList, char delimiter)
		{
			return GetAllViewByIdList(channelId, customer, idList, delimiter, "[lekmer].[pProductGetViewAllByIdList]");
		}
		/// <summary>
		/// Gets all package product views by id list. Does not support paging !!!
		/// </summary>
		public virtual LekmerProductViewCollection GetAllViewWithOnlyInPackageByIdList(int channelId, ICustomer customer, string idList, char delimiter)
		{
			return GetAllViewByIdList(channelId, customer, idList, delimiter, "[productlek].[pPackageProductGetViewAllByIdList]");
		}
		protected virtual LekmerProductViewCollection GetAllViewByIdList(int channelId, ICustomer customer, string idList, char delimiter, string storedProcedureName)
		{
			LekmerProductViewCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.ProductGetAllByIdList");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(storedProcedureName, parameters, dbSettings))
			{
				products = CreateViewDataMapper(dataReader).ReadMultipleRows<LekmerProductViewCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}
		/// <summary>
		/// Gets all product views by id list. Does not support paging !!!
		/// </summary>
		public virtual LekmerProductViewCollection GetAllViewByIdListWithoutStatusFilter(int channelId, ICustomer customer, string idList, char delimiter)
		{
			LekmerProductViewCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetAllViewByIdListWithoutStatusFilter");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetViewAllByIdListWithoutStatusFilter]", parameters, dbSettings))
			{
				products = CreateViewDataMapper(dataReader).ReadMultipleRows<LekmerProductViewCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}
		/// <summary>
		/// Gets all product views without any filters/Restrictions by id list. Does not support paging !!!
		/// </summary>
		public virtual LekmerProductViewCollection GetAllViewByIdListWithoutAnyFilter(int channelId, string idList, char delimiter)
		{
			LekmerProductViewCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetAllViewByIdListWithoutAnyFilter");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]", parameters, dbSettings))
			{
				products = CreateViewDataMapper(dataReader).ReadMultipleRows<LekmerProductViewCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}


		#region Package
		public virtual ProductIdCollection GetIdAllByPackageMasterProduct(int channelId, ICustomer customer, int productId)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAllByPackageMasterProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductGetIdAllByPackageMasterProduct]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}
		public virtual ProductCollection GetAllByPackageSecure(int channelId, int packageId)
		{
			ProductCollection products;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PackageId", packageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetAllByPackageSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductGetAllByPackageSecure]", parameters, dbSettings))
			{
				products = CreateDataMapper(dataReader).ReadMultipleRows<ProductCollection>();
			}

			products.TotalCount = products.Count;
			return products;
		}
		#endregion


		#region Import Product Info

		/// <summary>
		/// Update product info.
		/// </summary>
		public string UpdateProductInfo(IImportProductInfo importProductInfo, int channelId, int languageId, string userName)
		{
			string result;

			try
			{
				string tagIds = string.Empty;
				foreach (int tagId in importProductInfo.TagIdCollection)
				{
					tagIds += tagId + ",";
				}

				IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("HYErpId", importProductInfo.HYErpId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Title", importProductInfo.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", importProductInfo.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("TagIds", tagIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("UserName", userName, SqlDbType.NVarChar)
				};
				var dbSettings = new DatabaseSetting("LekmerProductRepository.UpdateProductInfo");
				new DataHandler().ExecuteCommand("[integration].[pBackofficeProductInfoImport]", parameters, dbSettings);

				result = "Updated";
			}
			catch (Exception e)
			{
				result = e.Message;
			}

			return result;
		}

		/// <summary>
		/// Remove old log rows from integration.tBackofficeProductInfoImport table.
		/// </summary>
		public virtual void RemoveOldProductInfoImportLogs(int days)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Days", days, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.RemoveOldProductInfoImportLogs");
			new DataHandler().ExecuteCommand("[integration].[pBackofficeProductInfoImportLogDelete]", parameters, dbSettings);
		}

		#endregion


		#region ProductRecord

		public virtual Collection<IProductRecord> GetProductRecordAllByIdList(string idList, char delimiter)
		{
			Collection<IProductRecord> productRecords;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetProductRecordAllByIdList");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductRecordGetAllByIdList]", parameters, dbSettings))
			{
				productRecords = CreateRecordDataMapper(dataReader).ReadMultipleRows<Collection<IProductRecord>>();
			}

			return productRecords;
		}

		#endregion


		public virtual Dictionary<string, int> GetProductIdsByHyIdList(string hyIdList, char delimiter)
		{
			var productIds = new Dictionary<string, int>();
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductHYIds", hyIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetProductIdsByHyIdList");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductIdsGetByHyIdListSecure]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetString(1), dataReader.GetInt32(0));
				}
			}
			return productIds;
		}
	}
}