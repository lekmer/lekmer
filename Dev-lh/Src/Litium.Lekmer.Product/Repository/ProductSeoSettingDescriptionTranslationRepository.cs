﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductSeoSettingDescriptionTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductSeoSettingDescriptionTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductSeoSettingDescriptionTranslationSave]";
			}
		}
	}
}
