using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class PriceIntervalRepository
	{
		protected virtual DataMapperBase<IPriceInterval> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IPriceInterval>(dataReader);
		}

		public Collection<IPriceInterval> GetAll(int currencyId, int languageId)
		{
			var dbSettings = new DatabaseSetting("PriceIntervalRepository.GetAll");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CurrencyId", currencyId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pPriceIntervalGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<IPriceInterval> GetAllSecure()
		{
			var dbSettings = new DatabaseSetting("PriceIntervalRepository.GetAllSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pPriceIntervalGetAllSecure]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}