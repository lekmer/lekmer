﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockCategoryProductListRepository
	{
		protected virtual DataMapperBase<IBlockCategoryProductList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockCategoryProductList>(dataReader);
		}

		public virtual void Save(IBlockCategoryProductList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductSortOrderId", block.ProductSortOrderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockCategoryProductListRepository.Save");
			new DataHandler().ExecuteCommand("[productlek].[pBlockCategoryProductListSave]", parameters, dbSettings);
		}

		public virtual IBlockCategoryProductList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockCategoryProductListRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockCategoryProductListGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockCategoryProductList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockCategoryProductListRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockCategoryProductListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("BlockCategoryProductListRepository.Delete");
			new DataHandler().ExecuteCommand("[productlek].[pBlockCategoryProductListDelete]", parameters, dbSettings);
		}
	}
}
