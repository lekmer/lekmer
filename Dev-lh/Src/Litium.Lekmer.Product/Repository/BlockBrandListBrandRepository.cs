﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockBrandListBrandRepository
	{
		protected DataMapperBase<IBlockBrandListBrand> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBrandListBrand>(dataReader);
		}

		public void Save(IBlockBrandListBrand blockBrand)
		{
			if (blockBrand == null) throw new ArgumentNullException("blockBrand");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockBrand.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", blockBrand.BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Ordinal", blockBrand.Ordinal, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockBrandListBrandRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandListBrandSave]", parameters, dbSettings);
		}

		public void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockBrandListBrandRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandListBrandDeleteAll]", parameters, dbSettings);
		}

		public Collection<IBlockBrandListBrand> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockBrandListBrandRepository.GetAllByBlockSecure");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[lekmer].[pBlockBrandListBrandGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}