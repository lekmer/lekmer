﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeTableMediaRegistryRepository
	{
		protected virtual DataMapperBase<ISizeTableMediaRegistry> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableMediaRegistry>(dataReader);
		}

		public virtual Collection<ISizeTableMediaRegistry> GetAllBySizeTable(int sizeTableId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SizeTableMediaRegistryRepository.GetAllBySizeTable");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableMediaProductRegistryGetAllBySizeTable]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(ISizeTableMediaRegistry sizeTableMediaRegistry)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SizeTableMediaProductRegistryId", sizeTableMediaRegistry.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("SizeTableMediaId", sizeTableMediaRegistry.SizeTableMediaId, SqlDbType.Int),
				ParameterHelper.CreateParameter("ProductRegistryId", sizeTableMediaRegistry.ProductRegistryId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SizeTableMediaRegistryRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeTableMediaProductRegistrySave]", parameters, dbSettings);
		}

		public virtual void Delete(int sizeTableMediaRegistryId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SizeTableMediaProductRegistryId", sizeTableMediaRegistryId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SizeTableMediaRegistryRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableMediaProductRegistryDelete]", parameters, dbSettings);
		}
	}
}