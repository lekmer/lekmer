﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Litium.Framework.Messaging;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Messenger
{
	public class WishListMessenger : ScensumMessengerBase<WishListMessageArgs>
	{
		protected readonly IFormatter Formatter = IoC.Resolve<IFormatter>();

		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
		protected WishListEmailTemplate Template { get; set; }


		protected virtual WishListEmailTemplate GetTemplate(IChannel channel)
		{
			return new WishListEmailTemplate(channel);
		}

		protected override Message CreateMessage(WishListMessageArgs messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.Channel.Id);
			Template = GetTemplate(channel);

			string body = RenderMessage(messageArgs);

			string subject = Template.GetFragment("Subject").Render();
			string fromName = string.IsNullOrEmpty(messageArgs.SenderName) ? messageArgs.SenderEmail : messageArgs.SenderName;
			string fromEmail = messageArgs.SenderEmail;
			var message = new EmailMessage
			{
				Body = body,
				ProviderName = "email",
				FromName = fromName,
				FromEmail = fromEmail,
				Subject = subject,
				Type = MessageType.Single
			};

			if (messageArgs.MailToMysef)
			{
				if (!messageArgs.Receivers.Contains(messageArgs.Senders))
					messageArgs.Receivers.Add(messageArgs.Senders);
			}

			foreach (var receiver in messageArgs.Receivers)
			{
				message.Recipients.Add(new EmailRecipient
				{
					Name = receiver,
					Address = receiver
				});
			}
			return message;
		}

		protected virtual string RenderSubject(WishListMessageArgs messageArgs)
		{
			Fragment fragmentSubject = Template.GetFragment("Subject");
			fragmentSubject.AddVariable("Subject", messageArgs.Subject, VariableEncoding.None);
			return fragmentSubject.Render();
		}

		protected virtual string RenderMessage(WishListMessageArgs messageArgs)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Message", messageArgs.Message, VariableEncoding.None);
			fragmentContent.AddVariable("SenderName", messageArgs.SenderName);
			fragmentContent.AddVariable("WishListPageUrl", messageArgs.WishListPageUrl, VariableEncoding.None);
			
			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = messageArgs.Channel;

			var wishList = !string.IsNullOrEmpty(messageArgs.WishListKey) 
				? IoC.Resolve<IWishListService>().GetByKey(userContext, new Guid(messageArgs.WishListKey))
				: null;

			var totalCount = GetTotalProductCount(wishList);
			fragmentContent.AddCondition("WishListIsEmpty", !totalCount.HasValue || totalCount.Value == 0);
			fragmentContent.AddVariable("WishList.TotalProductCount", totalCount.HasValue ? totalCount.Value.ToString(CultureInfo.CurrentCulture) : null);

			Price price = GetActualPriceSummary(userContext, wishList);
			fragmentContent.AddVariable("WishList.PriceIncludingVatSummary", Formatter.FormatPrice(messageArgs.Channel, price.IncludingVat));
			fragmentContent.AddVariable("WishList.PriceExcludingVatSummary", Formatter.FormatPrice(messageArgs.Channel, price.ExcludingVat));

			fragmentContent.AddVariable("WishListKey", messageArgs.WishListKey);

			fragmentContent.AddCondition("WishList.HasTitle", wishList != null && !string.IsNullOrEmpty(wishList.Title));
			fragmentContent.AddVariable("WishList.Title", wishList != null ? wishList.Title : null);

			fragmentContent.AddVariable("WishList.AddAllToCartUrl",
				string.Format("http://{0}/wishlisttocart?wishlist={1}", messageArgs.Channel.ApplicationName, messageArgs.WishListKey), VariableEncoding.None);

			return fragmentContent.Render();
		}

		protected override string MessengerName
		{
			get { return "WishListMessenger"; }
		}

		protected int? GetTotalProductCount(IWishList wishList)
		{
			if (wishList == null)
				return null;

			return wishList.GetItems().Count();
		}

		protected Price GetActualPriceSummary(IUserContext userContext, IWishList wishList)
		{
			if (wishList == null)
				return new Price();

			var productService = IoC.Resolve<IProductService>();
			var products = productService.PopulateProducts(userContext, new ProductIdCollection(wishList.GetItems().Select(x => x.ProductId)));
			var priceSummary = new Price();
			foreach (var product in products)
			{
				priceSummary += GetActualPrice(product);
			}
			return priceSummary;
		}

		protected Price GetActualPrice(IProduct product)
		{
			decimal priceIncludingVat;
			decimal priceExcludingVat;

			if (product.CampaignInfo != null)
			{
				priceIncludingVat = product.CampaignInfo.Price.IncludingVat;
				priceExcludingVat = product.CampaignInfo.Price.ExcludingVat;
			}
			else
			{
				priceIncludingVat = product.Price.PriceIncludingVat;
				priceExcludingVat = product.Price.PriceExcludingVat;
			}

			var price = new Price(priceIncludingVat, priceExcludingVat);
			return price;
		}
	}
}
