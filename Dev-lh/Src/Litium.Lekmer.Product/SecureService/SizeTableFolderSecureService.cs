﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Product
{
	public class SizeTableFolderSecureService : ISizeTableFolderSecureService
	{
		protected SizeTableFolderRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected ISizeTableSecureService SizeTableSecureService { get; private set; }

		public SizeTableFolderSecureService(SizeTableFolderRepository sizeTableFolderRepository, IAccessValidator accessValidator, ISizeTableSecureService sizeTableSecureService)
		{
			Repository = sizeTableFolderRepository;
			AccessValidator = accessValidator;
			SizeTableSecureService = sizeTableSecureService;
		}

		public virtual ISizeTableFolder Save(ISystemUserFull systemUserFull, ISizeTableFolder folder)
		{
			if (folder == null)
			{
				throw new ArgumentNullException("folder");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				folder.Id = Repository.Save(folder);
				transaction.Complete();
			}
			return folder;
		}

		public virtual bool TryDelete(ISystemUserFull systemUserFull, int folderId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			SizeTableSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				if (!TryDeleteCore(folderId))
				{
					return false;
				}
				transaction.Complete();
				return true;
			}
		}

		public virtual ISizeTableFolder GetById(int folderId)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(folderId);
		}

		public virtual Collection<ISizeTableFolder> GetAll()
		{
			Repository.EnsureNotNull();
			return Repository.GetAll();
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			Repository.EnsureNotNull();
			return Repository.GetTree(selectedId);
		}


		protected virtual bool TryDeleteCore(int folderId)
		{
			if (SizeTableSecureService.GetAllByFolder(folderId).Count > 0)
			{
				return false;
			}

			if (!TryDeleteChildren(folderId))
			{
				return false;
			}

			Repository.Delete(folderId);
			return true;
		}

		protected virtual bool TryDeleteChildren(int parentFolderId)
		{
			foreach (ISizeTableFolder folder in Repository.GetAllByParent(parentFolderId))
			{
				if (!TryDeleteCore(folder.Id))
				{
					return false;
				}
			}
			return true;
		}
	}
}