﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductUrlSecureService : IProductUrlSecureService
	{
		protected ProductUrlRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IProductUrlHistorySecureService ProductUrlHistorySecureService { get; private set; }

		public ProductUrlSecureService(ProductUrlRepository repository, IAccessValidator accessValidator, IProductUrlHistorySecureService productUrlHistorySecureService)
		{
			Repository = repository;
			AccessValidator = accessValidator;
			ProductUrlHistorySecureService = productUrlHistorySecureService;
		}

		public virtual Collection<IProductUrl> GetAllByProduct(int productId)
		{
			return Repository.GetAllByProduct(productId);
		}

		public virtual IProductUrl Create(int productId, int languageId, string urlTitle)
		{
			var productUrl = IoC.Resolve<IProductUrl>();
			productUrl.ProductId = productId;
			productUrl.LanguageId = languageId;
			productUrl.UrlTitle = urlTitle;
			productUrl.SetNew();
			return productUrl;
		}

		public virtual bool Check(IProductUrl productUrl)
		{
			return Repository.Check(productUrl) > 0;
		}

		public virtual void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<IProductUrl> productUrls)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			var productUrlHistoryCollection = new Collection<IProductUrlHistory>();

			// Get all existed product URLs by product.
			Collection<IProductUrl> existedProductUrlCollection = Repository.GetAllByProduct(productId);

			bool urlsHaveChanged = HasChanged(existedProductUrlCollection, productUrls);

			// Populate collection
			foreach (IProductUrl existedProductUrl in existedProductUrlCollection)
			{
				if (!productUrls.Any(u => u.IsSameAs(existedProductUrl)))
				{
					productUrlHistoryCollection.Add(ProductUrlHistorySecureService.Create(productId, existedProductUrl.LanguageId, existedProductUrl.UrlTitle, (int) HistoryLifeIntervalType.Month));
				}
			}

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				// Save product URl history.
				ProductUrlHistorySecureService.Save(systemUserFull, productUrlHistoryCollection);

				// Get all product URL history by product.
				Collection<IProductUrlHistory> existedProductUrlHistoryCollection = ProductUrlHistorySecureService.GetAllByProduct(productId);

				// Delete existed URL History.
				foreach (IProductUrlHistory productUrlHistory in existedProductUrlHistoryCollection)
				{
					if (productUrls.Any(u => u.IsSameAs(productUrlHistory)))
					{
						ProductUrlHistorySecureService.DeleteById(systemUserFull, productUrlHistory.Id);
					}
				}

				if (urlsHaveChanged)
				{
					// Delete product URLs.
					Repository.DeleteAllByProduct(productId);

					// Save new product URLs.
					foreach (var productUrl in productUrls)
					{
						Repository.Save(productUrl);
					}
				}

				transactedOperation.Complete();
			}

			if (urlsHaveChanged)
			{
				ProductUrlIdCache.Instance.Flush();
			}
		}


		protected virtual bool HasChanged(IEnumerable<IProductUrl> existedProductUrls, IEnumerable<IProductUrl> incomingProductUrls)
		{
			if (existedProductUrls.Count() != incomingProductUrls.Count())
			{
				return true;
			}

			Dictionary<int, string> existedUrlsDictionary = existedProductUrls.ToDictionary(u => u.LanguageId, u => u.UrlTitle);

			foreach (IProductUrl incomingProductUrl in incomingProductUrls)
			{
				if (existedUrlsDictionary.ContainsKey(incomingProductUrl.LanguageId))
				{
					if (existedUrlsDictionary[incomingProductUrl.LanguageId] != incomingProductUrl.UrlTitle)
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}

			return false;
		}
	}
}