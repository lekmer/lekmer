﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class TagGroupSecureService : ITagGroupSecureService
	{
		protected TagGroupRepository Repository { get; private set; }
		protected ITagSecureService TagSecureService { get; private set; }

		public TagGroupSecureService(TagGroupRepository repository, ITagSecureService tagSecureService)
		{
			Repository = repository;
			TagSecureService = tagSecureService;
		}

		public ITagGroup GetById(int id)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(id);
		}

		public Collection<ITagGroup> GetAll()
		{
			var tagGroups = GetAllWithoutTags();
			foreach (var tagGroup in tagGroups)
			{
				tagGroup.Tags = TagSecureService.GetAllByTagGroup(tagGroup.Id);
			}
			return tagGroups;
		}

		public Collection<ITagGroup> GetAllWithoutTags()
		{
			Repository.EnsureNotNull();
			return Repository.GetAll();
		}
	}
}