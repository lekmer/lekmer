﻿using System;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductChangesSecureService : IProductChangesSecureService
	{
		protected ProductChangesRepository Repository { get; private set; }

		public ProductChangesSecureService(ProductChangesRepository repository)
		{
			Repository = repository;
		}

		public virtual IProductChangeEvent Create(ISystemUserFull systemUserFull, int productId)
		{
			var productChangeEvent = IoC.Resolve<IProductChangeEvent>();

			productChangeEvent.ProductId = productId;
			productChangeEvent.EventStatusId = (int)ProductChangeEventStatus.InQueue;
			productChangeEvent.CdonExportEventStatusId = (int)ProductChangeEventStatus.InQueue;
			productChangeEvent.CreatedDate = DateTime.Now;
			productChangeEvent.Reference = "BO - " + systemUserFull.UserName;

			return productChangeEvent;
		}

		public virtual int Insert(IProductChangeEvent productChangeEvent)
		{
			productChangeEvent.Id = Repository.Insert(productChangeEvent);

			productChangeEvent.SetUntouched();

			return productChangeEvent.Id;
		}
	}
}
