﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductTypeSecureService : IProductTypeSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected ProductTypeRepository Repository { get; private set; }

		public ProductTypeSecureService(
			IAccessValidator accessValidator,
			ProductTypeRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual Collection<IProductType> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}

		public virtual Collection<int> GetIdAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetIdAllByBlock(blockId);
		}

		public virtual void SaveByBlock(ISystemUserFull systemUserFull, int blockId, Collection<int> productTypeIds)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			string productTypeIdsRaw = Convert.ToStringIdentifierList(productTypeIds);

			Repository.SaveByBlock(blockId, productTypeIdsRaw, Convert.DefaultListSeparator);
		}

		public virtual void DeleteByBlock(ISystemUserFull systemUserFull, int blockId)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			Repository.DeleteByBlock(blockId);
		}
	}
}