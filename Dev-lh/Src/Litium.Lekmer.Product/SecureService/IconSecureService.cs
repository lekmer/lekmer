﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class IconSecureService : IIconSecureService
	{
		protected IconRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public IconSecureService(IconRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual IIcon Save(ISystemUserFull systemUserFull, IIcon icon, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			if (icon == null)
			{
				throw new ArgumentNullException("icon");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Media);

			using (var transaction = new TransactedOperation())
			{
				// Save icon.
				int iconId = Repository.Save(icon);
				icon.Id = iconId;

				if (iconId > 0)
				{
					// Save icon translations.
					SaveTranslations(systemUserFull, iconId, titleTranslations, descriptionTranslations);
				}

				transaction.Complete();
			}

			return icon;
		}
		public virtual void Delete(ISystemUserFull systemUserFull, int iconId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Media);
			Repository.Delete(iconId);
		}
		public virtual IIcon GetByIdSecure(int iconId)
		{
			return Repository.GetByIdSecure(iconId);
		}
		public virtual Collection<IIcon> GetAll()
		{
			return Repository.GetAll();
		}
		/// <summary>
		/// Gets a icon collection.
		/// </summary>
		/// <param name="page">Table page number.</param>
		/// <param name="pageSize">Table page size.</param>
		/// <param name="sortBy">Icon property to sort by.</param>
		/// <param name="sortByDescending">Sort direction.</param>
		/// <returns>The <see cref="IconCollection"/> object.</returns>
		public virtual IconCollection GetAll(int page, int pageSize, string sortBy, bool? sortByDescending)
		{
			return Repository.GetAll(page, pageSize, sortBy, sortByDescending);
		}

		// Translations.
		protected virtual void SaveTranslations(ISystemUserFull systemUserFull, int iconId, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			for (int index = 0; index < titleTranslations.Count; index++)
			{
				var titleTranslation = titleTranslations[index];
				var descriptionTranslation = descriptionTranslations[index];
				SaveTranslations(systemUserFull, iconId, titleTranslation.LanguageId, titleTranslation.Value, descriptionTranslation.Value);
			}
		}
		public virtual void SaveTranslations(ISystemUserFull systemUserFull, int iconId, int languageId, string title, string description)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Media);

			using (var transaction = new TransactedOperation())
			{
				Repository.SaveTranslations(iconId, languageId, title, description);
				transaction.Complete();
			}
		}
		public virtual Collection<ITranslationGeneric> GetAllTitleTranslations(int iconId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllTitleTranslations(iconId);
		}
		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int iconId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllDescriptionTranslations(iconId);
		}

		// Product Icons.
		public virtual void SaveProductIcon(ISystemUserFull systemUserFull, int productId, IEnumerable<int> iconsId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByProduct(productId);
				foreach (var iconId in iconsId)
				{
					Repository.SaveProductIcon(productId, iconId);
				}
				transactedOperation.Complete();
			}
		}
		public virtual Collection<IIcon> GetAllByProduct(int productId, int categoryId, int? brandId)
		{
			return Repository.GetAllByProductSecure(productId, categoryId, brandId);
		}
		public virtual Collection<IIcon> GetAllByCategoryAndBrand(int categoryId, int? brandId)
		{
			return Repository.GetAllByCategoryAndBrandSecure(categoryId, brandId);
		}

		// Brand Icons.
		public virtual void SaveBrandIcon(ISystemUserFull systemUserFull, int brandId, IEnumerable<int> iconsId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByBrand(brandId);
				foreach (var iconId in iconsId)
				{
					Repository.SaveBrandIcon(brandId, iconId);
				}
				transactedOperation.Complete();
			}
		}
		public virtual Collection<IIcon> GetAllByBrand(int brandId)
		{
			return Repository.GetAllByBrandSecure(brandId);
		}

		// Category Icons.
		public virtual void SaveCategoryIcon(ISystemUserFull systemUserFull, int categoryId, IEnumerable<int> iconsId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentCategory);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByCategory(categoryId);
				foreach (var iconId in iconsId)
				{
					Repository.SaveCategoryIcon(categoryId, iconId);
				}
				transactedOperation.Complete();
			}
		}
		public virtual Collection<IIcon> GetAllByCategory(int categoryId)
		{
			return Repository.GetAllByCategorySecure(categoryId);
		}
	}
}