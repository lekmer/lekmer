﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ImageRotatorGroupSecureService : IImageRotatorGroupSecureService
	{
		protected ImageRotatorGroupRepository Repository { get; private set; }

		public ImageRotatorGroupSecureService(ImageRotatorGroupRepository repository)
		{
			Repository = repository;
		}


		public IImageRotatorGroup Create()
		{
			IImageRotatorGroup imageRotatorGroup = IoC.Resolve<IImageRotatorGroup>();
			imageRotatorGroup.SetNew();
			return imageRotatorGroup;
		}

		public IImageRotatorGroup GetById(ISystemUserFull systemUserFull, int id)
		{
			return Repository.GetById(id);
		}

		public Collection<IImageRotatorGroup> GetByBlockId(ISystemUserFull systemUserFull, int blockId)
		{
			return Repository.GetByBlockId(blockId);
		}

		public void Save(ISystemUserFull systemUserFull, int blockId, Collection<IImageRotatorGroup> imageRotatorGroups)
		{
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByBlockImageRotator(blockId);
				foreach (var imageRotatorGroup in imageRotatorGroups)
				{
					Repository.Save(imageRotatorGroup);
				}
				transactedOperation.Complete();
			}
		}

		public void Save(IImageRotatorGroup imageRotatorGroup)
		{
			Repository.Save(imageRotatorGroup);			
			
		}
	}
}
