﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Product
{
	public class ContentMessageFolderSecureService : IContentMessageFolderSecureService
	{
		protected ContentMessageFolderRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IContentMessageSecureService ContentMessageSecureService { get; private set; }

		public ContentMessageFolderSecureService(ContentMessageFolderRepository contentMessageFolderRepository, IAccessValidator accessValidator, IContentMessageSecureService contentMessageSecureService)
		{
			Repository = contentMessageFolderRepository;
			AccessValidator = accessValidator;
			ContentMessageSecureService = contentMessageSecureService;
		}

		public virtual IContentMessageFolder Save(ISystemUserFull systemUserFull, IContentMessageFolder folder)
		{
			if (folder == null)
			{
				throw new ArgumentNullException("folder");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				folder.Id = Repository.Save(folder);
				transaction.Complete();
			}
			return folder;
		}

		public virtual bool TryDelete(ISystemUserFull systemUserFull, int folderId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			ContentMessageSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				if (!TryDeleteCore(folderId))
				{
					return false;
				}
				transaction.Complete();
				return true;
			}
		}

		public virtual IContentMessageFolder GetById(int folderId)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(folderId);
		}

		public virtual Collection<IContentMessageFolder> GetAll()
		{
			Repository.EnsureNotNull();
			return Repository.GetAll();
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			Repository.EnsureNotNull();
			return Repository.GetTree(selectedId);
		}


		protected virtual bool TryDeleteCore(int folderId)
		{
			if (ContentMessageSecureService.GetAllByFolder(folderId).Count > 0)
			{
				return false;
			}

			if (!TryDeleteChildren(folderId))
			{
				return false;
			}

			Repository.Delete(folderId);
			return true;
		}

		protected virtual bool TryDeleteChildren(int parentFolderId)
		{
			foreach (IContentMessageFolder folder in Repository.GetAllByParent(parentFolderId))
			{
				if (!TryDeleteCore(folder.Id))
				{
					return false;
				}
			}
			return true;
		}
	}
}