using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class CategoryTagGroupSecureService : ICategoryTagGroupSecureService
	{
		protected CategoryTagGroupRepository Repository { get; private set; }

		public CategoryTagGroupSecureService(CategoryTagGroupRepository repository)
		{
			Repository = repository;
		}

		public void SaveAll(int categoryId, IEnumerable<int> tagGroupIds)
		{
			Repository.EnsureNotNull();

			using (var transactedOperation = new TransactedOperation())
			{
				DeleteAll(categoryId);

				foreach (var tagGroupId in tagGroupIds)
				{
					Repository.Save(categoryId, tagGroupId);
				}

				transactedOperation.Complete();
			}
		}

		public void DeleteAll(int categoryId)
		{
			Repository.EnsureNotNull();

			Repository.DeleteAll(categoryId);
		}

		public Collection<ICategoryTagGroup> GetAllByCategory(int categoryId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCategory(categoryId);
		}
	}
}