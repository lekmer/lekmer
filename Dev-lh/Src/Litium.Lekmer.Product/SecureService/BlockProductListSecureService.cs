﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Cache;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockProductListSecureService : IBlockProductListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductListRepository Repository { get; private set; }
		
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IBlockProductListProductSecureService BlockProductListProductSecureService { get; private set; }

		public BlockProductListSecureService(
			IAccessValidator accessValidator,
			BlockProductListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IBlockProductListProductSecureService blockProductListProductSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			BlockProductListProductSecureService = blockProductListProductSecureService;
		}

		/// <summary>
		/// Create new object
		/// </summary>
		public virtual IBlockProductList Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			
			var blockProductList = IoC.Resolve<IBlockProductList>();
			blockProductList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductList.Setting = BlockSettingSecureService.Create();
			blockProductList.Status = BusinessObjectStatus.New;
			return blockProductList;
		}

		public virtual IBlockProductList GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			IBlockProductList blockProductList = Create();
			blockProductList.ContentNodeId = contentNodeId;
			blockProductList.ContentAreaId = contentAreaId;
			blockProductList.BlockTypeId = blockTypeId;
			blockProductList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockProductList.Title = title;
			blockProductList.TemplateId = null;
			blockProductList.Id = Save(systemUserFull, blockProductList, new Collection<IBlockProductListProduct>());
			return blockProductList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductList block, Collection<IBlockProductListProduct> products)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (products == null) throw new ArgumentNullException("products");

			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			BlockProductListProductSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				// Save IBlock.
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				// Save Setting.
				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				// Save list of IBlockProductListProduct.
				BlockProductListProductSecureService.SaveAll(block.Id, products);

				transactedOperation.Complete();
			}
			BlockListProductCollectionCache.Instance.Flush();
			BlockProductListCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockListProductCollectionCache.Instance.Flush();
			BlockProductListCache.Instance.Remove(blockId);
		}
	}
}
