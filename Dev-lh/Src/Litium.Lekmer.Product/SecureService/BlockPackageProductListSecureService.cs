using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockPackageProductListSecureService : IBlockPackageProductListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockPackageProductListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockPackageProductListSecureService(
			IAccessValidator accessValidator,
			BlockPackageProductListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockPackageProductList Create()
		{
			if (AccessSecureService == null)
			{
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			}

			var blockPackageProductList = IoC.Resolve<IBlockPackageProductList>();

			blockPackageProductList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockPackageProductList.Setting = BlockSettingSecureService.Create();
			blockPackageProductList.Status = BusinessObjectStatus.New;

			return blockPackageProductList;
		}

		public virtual IBlockPackageProductList GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockPackageProductList blockPackageProductList = Create();

			blockPackageProductList.ContentNodeId = contentNodeId;
			blockPackageProductList.ContentAreaId = contentAreaId;
			blockPackageProductList.BlockTypeId = blockTypeId;
			blockPackageProductList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockPackageProductList.Title = title;
			blockPackageProductList.TemplateId = null;
			blockPackageProductList.Id = Save(systemUserFull, blockPackageProductList);

			return blockPackageProductList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockPackageProductList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (BlockSecureService == null)
			{
				throw new InvalidOperationException("BlockSecureService must be set before calling Save.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			BlockPackageProductListCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());
			
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				transactedOperation.Complete();
			}

			BlockPackageProductListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}