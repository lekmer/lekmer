﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerProductImageSecureService : ProductImageSecureService, IProductImageSecureService
	{
		protected LekmerProductImageRepository LekmerImageRepository;
		public LekmerProductImageSecureService(ProductImageRepository repository) : base(repository)
		{
			LekmerImageRepository = (LekmerProductImageRepository) repository;
		}

		public virtual void Delete(int productId, int mediaId)
		{
			LekmerImageRepository.Delete(productId, mediaId);
		}

		public virtual void Save(int mediaId, int productId, int groupId, int ordinal)
		{
			LekmerImageRepository.Save(mediaId, productId, groupId, ordinal);
		}


		public virtual Collection<IProductMedia> GetAllProductMediaByProduct(int productId)
		{
			return LekmerImageRepository.GetAllProductMediaByProduct(productId);
		}
	}
}
