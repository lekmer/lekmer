﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.BrandTopList;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Cache;

namespace Litium.Lekmer.Product
{
	public class BrandSecureService : IBrandSecureService
	{
		protected BrandRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IBrandSiteStructureRegistrySecureService BrandSiteStructureRegistrySecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }
		protected IProductRegistryRestrictionBrandSecureService ProductRegistryRestrictionBrandSecureService { get; private set; }
		protected IIconSecureService IconSecureService { get; private set; }

		public BrandSecureService(
			BrandRepository brandRepository,
			IAccessValidator accessValidator,
			IBrandSiteStructureRegistrySecureService brandSiteStructureRegistrySecureService,
			IChannelSecureService channelSecureService,
			IProductRegistryRestrictionBrandSecureService productRegistryRestrictionBrandSecureService,
			IIconSecureService iconSecureService)
		{
			Repository = brandRepository;
			AccessValidator = accessValidator;
			BrandSiteStructureRegistrySecureService = brandSiteStructureRegistrySecureService;
			ChannelSecureService = channelSecureService;
			ProductRegistryRestrictionBrandSecureService = productRegistryRestrictionBrandSecureService;
			IconSecureService = iconSecureService;
		}

		public virtual IBrand Create()
		{
			var brand = IoC.Resolve<IBrand>();
			brand.Status = BusinessObjectStatus.New;
			return brand;
		}

		public virtual int Save(
			ISystemUserFull systemUserFull,
			IBrand brand,
			Collection<IBrandSiteStructureRegistry> brandSiteStructureRegistries,
			IEnumerable<ITranslationGeneric> titleTranslations,
			IEnumerable<ITranslationGeneric> descriptionTranslations,
			IEnumerable<ITranslationGeneric> packageInfoTranslations,
			Collection<IProductRegistryRestrictionItem> registryRestrictions,
			Collection<int> icons)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);
			int brandId;
			using (var transaction = new TransactedOperation())
			{
				brandId = Repository.Save(brand);
				if (brandId > 0)
				{
					BrandSiteStructureRegistrySecureService.DeleteByBrand(systemUserFull, brandId);
					foreach (IBrandSiteStructureRegistry brandSiteStructureRegistry in brandSiteStructureRegistries)
					{
						BrandSiteStructureRegistrySecureService.Save(systemUserFull, brandSiteStructureRegistry);
					}
					SaveTranslations(brandId, titleTranslations, descriptionTranslations, packageInfoTranslations);
					ProductRegistryRestrictionBrandSecureService.Save(systemUserFull, brandId, registryRestrictions);
					IconSecureService.SaveBrandIcon(systemUserFull, brandId, icons);
				}
				transaction.Complete();
			}

			BrandCache.Instance.Remove(brandId, ChannelSecureService.GetAll());
			BlockBrandListBrandCollectionCache.Instance.Flush();
			TopListBrandCollectionCache.Instance.Flush();
			BrandListCache.Instance.Flush();
			ProductCache.Instance.Flush();
			ProductViewCache.Instance.Flush();
			FilterProductCollectionCache.Instance.Flush();

			return brandId;
		}

		public BrandCollection GetAll()
		{
			return Repository.GetAllSecure();
		}

		public IBrand GetById(int brandId)
		{
			return Repository.GetByIdSecure(brandId);
		}

		public BrandCollection GetAll(int page, int pageSize, string sortBy, bool? sortByDescending)
		{
			return Repository.GetAll(page, pageSize, sortBy, sortByDescending);
		}


		public void Delete(ISystemUserFull systemUserFull, int brandId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);
			using (var transaction = new TransactedOperation())
			{
				BrandSiteStructureRegistrySecureService.DeleteByBrand(systemUserFull, brandId);
				Repository.Delete(brandId);
				transaction.Complete();
			}

			BrandCache.Instance.Remove(brandId, ChannelSecureService.GetAll());
			BlockBrandListBrandCollectionCache.Instance.Flush();
			TopListBrandCollectionCache.Instance.Flush();
			BrandListCache.Instance.Flush();
			ProductCache.Instance.Flush();
			ProductViewCache.Instance.Flush();
			FilterProductCollectionCache.Instance.Flush();
		}


		public Collection<ITranslationGeneric> GetAllTitleTranslationsByBrand(int id)
		{
			return Repository.GetAllTitleTranslationsByBrand(id);
		}

		public Collection<ITranslationGeneric> GetAllDescriptionTranslationsByBrand(int id)
		{
			return Repository.GetAllDescriptionTranslationsByBrand(id);
		}

		public Collection<ITranslationGeneric> GetAllPageInfoTranslationsByBrand(int id)
		{
			return Repository.GetAllPackageInfoTranslationsByBrand(id);
		}

		private void SaveTranslations(int brandId,
			IEnumerable<ITranslationGeneric> titleTranslations,
			IEnumerable<ITranslationGeneric> descriptionTranslations,
			IEnumerable<ITranslationGeneric> packageInfoTranslations)
		{
			foreach (var titleTranslation in titleTranslations)
			{
				var languageId = titleTranslation.LanguageId;
				var descriptionTranslation = descriptionTranslations.First(i => i.LanguageId == languageId);
				var packageInfoTranslation = packageInfoTranslations.First(i => i.LanguageId == languageId);
				Repository.SaveTranslation(brandId, languageId, titleTranslation.Value, descriptionTranslation.Value, packageInfoTranslation.Value);
			}
		}
	}
}