﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockCategoryProductList : BlockBase, IBlockCategoryProductList
	{
		private int _productSortOrderId;
		private IBlockSetting _setting;
		private IProductSortOrder _productSortOrder;

		public int ProductSortOrderId
		{
			get { return _productSortOrderId; }
			set
			{
				CheckChanged(_productSortOrderId, value);
				_productSortOrderId = value;
			}
		}

		public IProductSortOrder ProductSortOrder
		{
			get { return _productSortOrder; }
			set
			{
				CheckChanged(_productSortOrder, value);
				_productSortOrder = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}
