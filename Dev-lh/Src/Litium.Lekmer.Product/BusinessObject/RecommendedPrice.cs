﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class RecommendedPrice : BusinessObjectBase, IRecommendedPrice
	{
		private int _channelId;
		private decimal _price;
		private int _productId;

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public decimal Price
		{
			get { return _price; }
			set
			{
				CheckChanged(_price, value);
				_price = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}
	}
}