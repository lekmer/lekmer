using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductChangeEvent : BusinessObjectBase, IProductChangeEvent
	{
		private int _id;
		private int _productId;
		private int _eventStatusId;
		private int _cdonExportEventStatusId;
		private DateTime _createdDate;
		private DateTime _actionAppliedDate;
		private string _reference;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int EventStatusId
		{
			get { return _eventStatusId; }
			set
			{
				CheckChanged(_eventStatusId, value);
				_eventStatusId = value;
			}
		}

		public int CdonExportEventStatusId
		{
			get { return _cdonExportEventStatusId; }
			set
			{
				CheckChanged(_cdonExportEventStatusId, value);
				_cdonExportEventStatusId = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public DateTime ActionAppliedDate
		{
			get { return _actionAppliedDate; }
			set
			{
				CheckChanged(_actionAppliedDate, value);
				_actionAppliedDate = value;
			}
		}

		public string Reference
		{
			get { return _reference; }
			set
			{
				CheckChanged(_reference, value);
				_reference = value;
			}
		}
	}
}
