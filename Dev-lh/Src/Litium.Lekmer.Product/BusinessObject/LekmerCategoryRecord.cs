﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerCategoryRecord : LekmerCategory, ILekmerCategoryRecord
	{
		private Collection<ICategorySiteStructureRegistry> _categorySiteStructureRegistries;
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ICategorySiteStructureRegistry> CategorySiteStructureRegistries
		{
			get
			{
				if (_categorySiteStructureRegistries == null)
				{
					_categorySiteStructureRegistries = new Collection<ICategorySiteStructureRegistry>();
				}
				return _categorySiteStructureRegistries;
			}
			set { _categorySiteStructureRegistries = value; }
		}
	}
}
