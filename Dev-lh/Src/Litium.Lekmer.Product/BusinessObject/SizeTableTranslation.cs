﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableTranslation : BusinessObjectBase, ISizeTableTranslation
	{
		private int _sizeTableId;
		private int _languageId;
		private string _title;
		private string _description;
		private string _column1Title;
		private string _column2Title;

		public int SizeTableId
		{
			get { return _sizeTableId; }
			set
			{
				CheckChanged(_sizeTableId, value);
				_sizeTableId = value;
			}
		}

		public int LanguageId
		{
			get { return _languageId; }
			set
			{
				CheckChanged(_languageId, value);
				_languageId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public string Column1Title
		{
			get { return _column1Title; }
			set
			{
				CheckChanged(_column1Title, value);
				_column1Title = value;
			}
		}

		public string Column2Title
		{
			get { return _column2Title; }
			set
			{
				CheckChanged(_column2Title, value);
				_column2Title = value;
			}
		}
	}
}