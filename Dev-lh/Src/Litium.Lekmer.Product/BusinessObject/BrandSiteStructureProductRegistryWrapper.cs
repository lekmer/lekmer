using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BrandSiteStructureProductRegistryWrapper : BusinessObjectBase, IBrandSiteStructureProductRegistryWrapper
	{
		private int _brandId;
		private int _siteStructureRegistryId;
		private int _contentNodeId;
		private int _productRegistryId;
		private string _title;
		private int _contentNodeStatusId;

		public int BrandId
		{
			get { return _brandId; }
			set
			{
				CheckChanged(_brandId, value);
				_brandId = value;
			}
		}
		public int SiteStructureRegistryId
		{
			get { return _siteStructureRegistryId; }
			set
			{
				CheckChanged(_siteStructureRegistryId, value);
				_siteStructureRegistryId = value;
			}
		}
		public int ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}
		public int ProductRegistryId
		{
			get { return _productRegistryId; }
			set
			{
				CheckChanged(_productRegistryId, value);
				_productRegistryId = value;
			}
		}
		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
		public int ContentNodeStatusId
		{
			get { return _contentNodeStatusId; }
			set
			{
				CheckChanged(_contentNodeStatusId, value);
				_contentNodeStatusId = value;
			}
		}
	}
}