﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Package : BusinessObjectBase, IPackage
	{
		private int _packageId;
		private int _masterProductId;
		private IProduct _masterProduct;
		private string _generalInfo;

		public int PackageId
		{
			get { return _packageId; }
			set
			{
				CheckChanged(_packageId, value);
				_packageId = value;
			}
		}

		public int MasterProductId
		{
			get { return _masterProductId; }
			set
			{
				CheckChanged(_masterProductId, value);
				_masterProductId = value;
			}
		}

		public IProduct MasterProduct
		{
			get { return _masterProduct; }
			set
			{
				CheckChanged(_masterProduct, value);
				_masterProduct = value;
			}
		}

		public string GeneralInfo
		{
			get { return _generalInfo; }
			set
			{
				CheckChanged(_generalInfo, value);
				_generalInfo = value;
			}
		}

		public Collection<IProduct> PackageProducts { get; set; }
	}
}