using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerPriceListItem : BusinessObjectBase, ILekmerPriceListItem
	{
		public int Id { get; set; }
		public int PriceListId { get; set; }
		public int ProductId { get; set; }
		public decimal PriceIncludingVat { get; set; }
		public decimal PriceExcludingVat { get; set; }
		public decimal VatPercentage { get; set; }

		public virtual decimal GetVatAmount()
		{
			return PriceIncludingVat - PriceExcludingVat;
		}

		public virtual decimal GetVatPercent()
		{
			return PriceExcludingVat == 0 ? 0 : (GetVatAmount() / PriceExcludingVat) * 100;
		}
	}
}