﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductType : BusinessObjectBase, IProductType
	{
		private int _id;
		private string _title;
		private string _commonName;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}
	}
}