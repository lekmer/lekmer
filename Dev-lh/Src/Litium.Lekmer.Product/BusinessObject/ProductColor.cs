using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductColor : IProductColor
	{
		public int ColorId { get; set; }
		public int ProductId { get; set; }
	}
}