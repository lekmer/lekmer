﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BrandLite : BusinessObjectBase, IBrandLite
	{
		private int _id;
		private string _title;
		private int _contentNodeId;
		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
		public int ContentNodeId 
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}

	}
}
