using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class IdSuggestionInfo : IIdSuggestionInfo
	{
		public int Id { get; set; }
		public string Ticket { get; set; }
	}
}