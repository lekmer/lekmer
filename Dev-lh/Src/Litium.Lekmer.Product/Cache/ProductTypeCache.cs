using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductTypeCache : ScensumCacheBase<ProductTypeKey, IProductType>
	{
		#region Singleton

		private static readonly ProductTypeCache _instance = new ProductTypeCache();

		private ProductTypeCache()
		{
		}

		public static ProductTypeCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class ProductTypeKey : ICacheKey
	{
		public int ChannelId { get; set; }
		public int ProductTypeId { get; set; }

		public ProductTypeKey(int channelId, int productTypeId)
		{
			ChannelId = channelId;
			ProductTypeId = productTypeId;
		}

		public string Key
		{
			get { return string.Format(CultureInfo.InvariantCulture, "c_{0}-id_{1}", ChannelId, ProductTypeId); }
		}
	}
}