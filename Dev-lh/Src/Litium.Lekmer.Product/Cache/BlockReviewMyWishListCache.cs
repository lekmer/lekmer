﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockReviewMyWishListCache : ScensumCacheBase<BlockReviewMyWishListKey, IBlockReviewMyWishList>
	{
		private static readonly BlockReviewMyWishListCache _instance = new BlockReviewMyWishListCache();

		private BlockReviewMyWishListCache()
		{
		}

		public static BlockReviewMyWishListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockReviewMyWishListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockReviewMyWishListKey : ICacheKey
	{
		public BlockReviewMyWishListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}