﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockBrandProductCollectionCache :
		ScensumCacheBase<BlockBrandProductCollectionKey, ProductIdCollection>
	{
		#region Singleton

		private static readonly BlockBrandProductCollectionCache _instance =
			new BlockBrandProductCollectionCache();

		private BlockBrandProductCollectionCache()
		{
		}

		public static BlockBrandProductCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class BlockBrandProductCollectionKey : ICacheKey
	{
		public BlockBrandProductCollectionKey(int channelId, int blockId, int pageNumber, int pageSize, int productSortOrderId)
		{
			ChannelId = channelId;
			BlockId = blockId;
			PageNumber = pageNumber;
			PageSize = pageSize;
			ProductSortOrderId = productSortOrderId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public int PageNumber { get; set; }

		public int PageSize { get; set; }

		public int ProductSortOrderId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture) + "-" +
					PageNumber.ToString(CultureInfo.InvariantCulture) + "-" +
					PageSize.ToString(CultureInfo.InvariantCulture) + "-" +
					ProductSortOrderId.ToString(CultureInfo.InvariantCulture);
					
			}
		}
	}
}