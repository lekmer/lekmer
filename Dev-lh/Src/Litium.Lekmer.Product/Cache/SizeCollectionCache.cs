﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class SizeCollectionCache : ScensumCacheBase<SizeKey, Collection<ISize>>
	{
		#region Singleton

		private static readonly SizeCollectionCache _instance = new SizeCollectionCache();

		private SizeCollectionCache()
		{
		}

		public static SizeCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class SizeKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public SizeKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}