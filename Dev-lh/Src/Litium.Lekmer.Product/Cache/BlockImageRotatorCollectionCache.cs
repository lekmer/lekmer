﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockImageRotatorCollectionCache :
		ScensumCacheBase<BlockImageRotatorCollectionKey, ImageRotatorGroupCollection>
	{
		#region Singleton

		private static readonly BlockImageRotatorCollectionCache _instance =
			new BlockImageRotatorCollectionCache();

		private BlockImageRotatorCollectionCache()
		{
		}

		public static BlockImageRotatorCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class BlockImageRotatorCollectionKey : ICacheKey
	{
		public BlockImageRotatorCollectionKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
						BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}