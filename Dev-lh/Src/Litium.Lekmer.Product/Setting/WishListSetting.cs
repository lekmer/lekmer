﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Product.Setting
{
	public sealed class WishListSetting : SettingBase
	{
		private static readonly WishListSetting _instance = new WishListSetting();

		private WishListSetting()
		{
		}

		public static WishListSetting Instance
		{
			get { return _instance; }
		}

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "WishList"; }
		}
		
		public int MaxNumberOfItems
		{
			get { return GetInt32(_groupName, "MaxNumberOfItems"); }
		}
	}
}
