using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class EsalesSearchFacetService : IEsalesSearchFacetService
	{
		private const int DefaultPageNumber = 1;
		private const int DefaultPageSize = 10000;

		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ISizeService SizeService { get; private set; }

		public EsalesSearchFacetService(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService, 
			IBrandService brandService, ICategoryService categoryService, ISizeService sizeService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			BrandService = brandService;
			CategoryService = categoryService;
			SizeService = sizeService;
		}


		public virtual ISearchFacet SearchFacetWithFilter(IUserContext context, string text)
		{
			EsalesService.EnsureNotNull();

			ISearchFacetFilterPanelSetting panelSetting = new SearchFacetFilterPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.FilterPhrase = text;
			searchRequest.Paging = new Paging(DefaultPageNumber, DefaultPageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchFacet searchFacet = new SearchFacet();
			searchFacet.AllBrandsWithoutFilter = FindBrandSuggestions(context, panelSetting, esalesResponse);
			searchFacet.AllSizesWithoutFilter = FindSizeSuggestions(context, panelSetting, esalesResponse);

			CategoryCollection allMainCategoriesWithoutFilter;
			CategoryCollection allParentCategoriesWithoutFilter;
			CategoryCollection allCategoriesWithoutFilter;
			FindCategorySuggestions(context, panelSetting, esalesResponse, out allMainCategoriesWithoutFilter, out allParentCategoriesWithoutFilter, out allCategoriesWithoutFilter);
			searchFacet.AllMainCategoriesWithoutFilter = allMainCategoriesWithoutFilter;
			searchFacet.AllParentCategoriesWithoutFilter = allParentCategoriesWithoutFilter;
			searchFacet.AllCategoriesWithoutFilter = allCategoriesWithoutFilter;

			return searchFacet;
		}

		public virtual ISearchFacet SearchFacet(IUserContext context, string text)
		{
			EsalesService.EnsureNotNull();

			ISearchFacetPanelSetting panelSetting = new SearchFacetPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(DefaultPageNumber, DefaultPageSize);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchFacet searchFacet = new SearchFacet();
			searchFacet.AllBrandsWithoutFilter = FindBrandSuggestions(context, panelSetting, esalesResponse);
			searchFacet.AllSizesWithoutFilter = FindSizeSuggestions(context, panelSetting, esalesResponse);

			CategoryCollection allMainCategoriesWithoutFilter;
			CategoryCollection allParentCategoriesWithoutFilter;
			CategoryCollection allCategoriesWithoutFilter;
			FindCategorySuggestions(context, panelSetting, esalesResponse, out allMainCategoriesWithoutFilter, out allParentCategoriesWithoutFilter, out allCategoriesWithoutFilter);
			searchFacet.AllMainCategoriesWithoutFilter = allMainCategoriesWithoutFilter;
			searchFacet.AllParentCategoriesWithoutFilter = allParentCategoriesWithoutFilter;
			searchFacet.AllCategoriesWithoutFilter = allCategoriesWithoutFilter;

			return searchFacet;
		}


		protected virtual BrandCollection FindBrandSuggestions(IUserContext context, ISearchFacetPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			var ids = FindIdSuggestions(context, esalesResponse, panelSetting.BrandFilterPanelName);
			if (ids != null)
			{
				var brandIds = new BrandIdCollection(ids);
				return BrandService.PopulateBrands(context, brandIds);
			}

			return new BrandCollection();
		}

		protected virtual void FindCategorySuggestions(IUserContext context, ISearchFacetPanelSetting panelSetting, IEsalesResponse esalesResponse,
			out CategoryCollection mainCategories, out CategoryCollection parentCategories, out CategoryCollection categories)
		{
			mainCategories = new CategoryCollection();
			parentCategories = new CategoryCollection();
			categories = new CategoryCollection();

			var categoryTree = CategoryService.GetAllAsTree(context);

			var mainCategoryIds = FindIdSuggestions(context, esalesResponse, panelSetting.MainCategoryFilterPanelName);
			GetCategory(mainCategoryIds, categoryTree, ref mainCategories);

			var parentCategoryIds = FindIdSuggestions(context, esalesResponse, panelSetting.ParentCategoryFilterPanelName);
			GetCategory(parentCategoryIds, categoryTree, ref parentCategories);

			var categoryIds = FindIdSuggestions(context, esalesResponse, panelSetting.CategoryFilterPanelName);
			GetCategory(categoryIds, categoryTree, ref categories);
		}
		protected virtual void GetCategory(IdCollection ids, ICategoryTree categoryTree, ref CategoryCollection categories)
		{
			if (ids == null) return;

			foreach (var id in ids)
			{
				var categoryItem = categoryTree.FindItemById(id);
				if (categoryItem != null && categoryItem.Category != null)
				{
					categories.Add(categoryItem.Category);
				}
			}
		}

		protected virtual Collection<ISize> FindSizeSuggestions(IUserContext context, ISearchFacetPanelSetting panelSetting, IEsalesResponse esalesResponse)
		{
			var sizes = new Collection<ISize>();

			IdCollection ids = FindIdSuggestions(context, esalesResponse, panelSetting.SizeFilterPanelName);
			if (ids != null)
			{
				var allSzes = SizeService.GetAll(context);
				if (allSzes == null) return sizes;

				Dictionary<int, int> idDict = ids.ToDictionary(id => id);

				foreach (var size in allSzes)
				{
					if (idDict.ContainsKey(size.Id))
					{
						sizes.Add(size);
					}
				}
			}

			return sizes;
		}

		protected virtual IdCollection FindIdSuggestions(IUserContext context, IEsalesResponse esalesResponse, string panelName)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				var countResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;

				if (result != null)
				{
					var ids = new IdCollection(result.ValuesInfo.Select(c => int.Parse(c.Text)));
					ids.TotalCount = result.ValuesInfo.Count;
					if (countResult != null)
					{
						ids.TotalCount = countResult.TotalHitCount;
					}

					return ids;
				}
			}

			return null;
		}
	}
}