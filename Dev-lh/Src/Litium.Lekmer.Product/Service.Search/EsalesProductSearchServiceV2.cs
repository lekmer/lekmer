using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class EsalesProductSearchServiceV2 : IEsalesProductSearchServiceV2
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ISizeService SizeService { get; private set; }

		public EsalesProductSearchServiceV2(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService,
			IProductService productService, IBrandService brandService, ICategoryService categoryService, ISizeService sizeService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = (ILekmerProductService) productService;
			BrandService = brandService;
			CategoryService = categoryService;
			SizeService = sizeService;
		}

		public virtual ISearchSuggestions SearchProducts(IUserContext context, string text, IFilterQuery query, int pageNumber, int pageSize)
		{
			EsalesService.EnsureNotNull();

			ISearchProductPanelSettingV2 panelSetting = new SearchProductPanelSettingV2(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchFilterRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(pageNumber, pageSize);
			searchRequest.FilterQuery = query;
			searchRequest.Channel = context.Channel;

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			ISearchSuggestions searchSuggestions = new SearchSuggestions();
			int searchHitCount = FindProductSearchHitCount(panelSetting, esalesResponse);
			searchSuggestions.Products = FindProductSuggestions(context, panelSetting, esalesResponse, searchHitCount);
			searchSuggestions.SearchFacetWithFilter = FindSearchFacetWithFilter(context, panelSetting, esalesResponse);
			searchSuggestions.SearchFacetWithoutFilter = FindSearchFacetWithoutFilter(context, panelSetting, esalesResponse);
			searchSuggestions.Corrections = FindStringCorrections(panelSetting, esalesResponse);
			searchSuggestions.TopSearches = FindTopSearchesPhrase(panelSetting, esalesResponse);
			return searchSuggestions;
		}

		protected virtual int FindProductSearchHitCount(ISearchProductPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductCountPanelName);
			if (panelContent != null)
			{
				var productsCountResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;
				if (productsCountResult != null)
				{
					return productsCountResult.TotalHitCount;
				}
			}
			return 0;
		}
		protected virtual ProductCollection FindProductSuggestions(IUserContext context, ISearchProductPanelSettingV2 panelSetting, IEsalesResponse esalesResponse, int searchHitCount)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.ProductPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;
				if (productsResult != null)
				{
					Dictionary<int, string> tickets = productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

					var productIds = new ProductIdCollection(productsResult.ProductsInfo.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
					ProductCollection products = ProductService.PopulateViewProducts(context, productIds);
					foreach (ILekmerProduct product in products)
					{
						product.EsalesTicket = tickets[product.Id];
					}

					products.TotalCount = searchHitCount;
					return products;
				}
			}
			return new ProductCollection();
		}
		protected virtual ISearchFacet FindSearchFacetWithFilter(IUserContext context, ISearchProductPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent facetPanelContent = esalesResponse.FindPanel(panelSetting.FacetsFilterPanelName);
			if (facetPanelContent != null)
			{
				var ageFromMonth = FindIdSuggestions(facetPanelContent, panelSetting.AgeFromMonthFilterPanelName);
				var ageToMonth = FindIdSuggestions(facetPanelContent, panelSetting.AgeToMonthFilterPanelName);
				var prices = FindPriceSuggestions(facetPanelContent, panelSetting.PriceFilterPanelName);

				int? minAge;
				int? maxAge;
				decimal? minPrice;
				decimal? maxPrice;
				FindAgeAndPriceIntervals(ageFromMonth, ageToMonth, prices, out minAge, out maxAge, out minPrice, out maxPrice);

				return new SearchFacet
					{
						BrandsWithFilter = FindIdSuggestions(facetPanelContent, panelSetting.BrandFilterPanelName),
						MainCategoriesWithFilter = FindIdSuggestions(facetPanelContent, panelSetting.MainCategoryFilterPanelName),
						ParentCategoriesWithFilter = FindIdSuggestions(facetPanelContent, panelSetting.ParentCategoryFilterPanelName),
						CategoriesWithFilter = FindIdSuggestions(facetPanelContent, panelSetting.CategoryFilterPanelName),
						AgeInterval = {FromMonth = minAge, ToMonth = maxAge},
						PriceInterval = {From = minPrice, To = maxPrice},
						SizesWithFilter = FindIdSuggestions(facetPanelContent, panelSetting.SizeFilterPanelName),
						TagsWithFilter = FindIdSuggestions(facetPanelContent, panelSetting.TagFilterPanelName)
					};
			}
			return new SearchFacet();
		}
		protected virtual ISearchFacet FindSearchFacetWithoutFilter(IUserContext context, ISearchProductPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent facetPanelContent = esalesResponse.FindPanel(panelSetting.FacetsAllPanelName);
			if (facetPanelContent != null)
			{
				CategoryCollection allMainCategories;
				CategoryCollection allParentCategories;
				CategoryCollection allCategories;
				FindCategorySuggestions(context, panelSetting, facetPanelContent, out allMainCategories, out allParentCategories, out allCategories);

				return new SearchFacet
				{
					AllBrandsWithoutFilter = FindBrandSuggestions(context, panelSetting, facetPanelContent),
					AllSizesWithoutFilter = FindSizeSuggestions(context, panelSetting, facetPanelContent),
					AllMainCategoriesWithoutFilter = allMainCategories,
					AllParentCategoriesWithoutFilter = allParentCategories,
					AllCategoriesWithoutFilter = allCategories
				};
			}
			return new SearchFacet();
		}
		protected virtual Collection<ISearchSuggestionInfo> FindStringCorrections(ISearchProductPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.DidYouMeanPanelName);
			if (panelContent != null)
			{
				var correctionsResult = panelContent.FindResult(EsalesResultType.Corrections) as ICorrectionHits;
				if (correctionsResult != null)
				{
					return new Collection<ISearchSuggestionInfo>(correctionsResult.CorrectionsInfo.Select(c => new SearchSuggestionInfo { Text = c.Text, Ticket = c.Ticket } as ISearchSuggestionInfo).ToList());
				}
			}
			return new Collection<ISearchSuggestionInfo>();
		}
		protected virtual Collection<ISearchSuggestionInfo> FindTopSearchesPhrase(ISearchProductPanelSettingV2 panelSetting, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.TopSearchesPanelName);
			if (panelContent != null)
			{
				var phrasesResult = panelContent.FindResult(EsalesResultType.Phrases) as IPhraseHits;
				if (phrasesResult != null)
				{
					return new Collection<ISearchSuggestionInfo>(phrasesResult.PhrasesInfo.Select(c => new SearchSuggestionInfo { Text = c.Text, Ticket = c.Ticket } as ISearchSuggestionInfo).ToList());
				}
			}
			return new Collection<ISearchSuggestionInfo>();
		}

		protected virtual Dictionary<int, int> FindIdSuggestions(IPanelContent facetPanelContent, string panelName)
		{
			IPanelContent panelContent = facetPanelContent.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					return result.ValuesInfo.ToDictionary(c => int.Parse(c.Text), c => c.Count);
				}
			}
			return new Dictionary<int, int>();
		}
		protected virtual Collection<decimal> FindPriceSuggestions(IPanelContent facetPanelContent, string panelName)
		{
			IPanelContent panelContent = facetPanelContent.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					return new Collection<decimal>(result.ValuesInfo.Select(c => decimal.Parse(c.Text.Replace('.', ','), CultureInfo.CurrentCulture)).ToList());
				}
			}
			return new Collection<decimal>();
		}
		protected virtual void FindCategorySuggestions(IUserContext context, ISearchProductPanelSettingV2 panelSetting, IPanelContent facetPanelContent,
			out CategoryCollection mainCategories, out CategoryCollection parentCategories, out CategoryCollection categories)
		{
			mainCategories = new CategoryCollection();
			parentCategories = new CategoryCollection();
			categories = new CategoryCollection();

			var categoryTree = CategoryService.GetAllAsTree(context);

			var mainCategoryIds = FindIds(facetPanelContent, panelSetting.MainCategoryFilterPanelName);
			GetCategory(mainCategoryIds, categoryTree, ref mainCategories);

			var parentCategoryIds = FindIds(facetPanelContent, panelSetting.ParentCategoryFilterPanelName);
			GetCategory(parentCategoryIds, categoryTree, ref parentCategories);

			var categoryIds = FindIds(facetPanelContent, panelSetting.CategoryFilterPanelName);
			GetCategory(categoryIds, categoryTree, ref categories);
		}
		protected virtual BrandCollection FindBrandSuggestions(IUserContext context, ISearchProductPanelSettingV2 panelSetting, IPanelContent facetPanelContent)
		{
			var ids = FindIds(facetPanelContent, panelSetting.BrandFilterPanelName);
			if (ids != null)
			{
				var brandIds = new BrandIdCollection(ids);
				return BrandService.PopulateBrands(context, brandIds);
			}
			return new BrandCollection();
		}
		protected virtual Collection<ISize> FindSizeSuggestions(IUserContext context, ISearchProductPanelSettingV2 panelSetting, IPanelContent facetPanelContent)
		{
			var sizes = new Collection<ISize>();
			IdCollection ids = FindIds(facetPanelContent, panelSetting.SizeFilterPanelName);
			if (ids != null)
			{
				var allSzes = SizeService.GetAll(context);
				if (allSzes == null) return sizes;

				Dictionary<int, int> idDict = ids.ToDictionary(id => id);
				foreach (var size in allSzes)
				{
					if (idDict.ContainsKey(size.Id))
					{
						sizes.Add(size);
					}
				}
			}
			return sizes;
		}
		protected virtual IdCollection FindIds(IPanelContent facetPanelContent, string panelName)
		{
			IPanelContent panelContent = facetPanelContent.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					var ids = new IdCollection(result.ValuesInfo.Select(c => int.Parse(c.Text)));
					ids.TotalCount = result.ValuesInfo.Count;

					var countResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;
					if (countResult != null)
					{
						ids.TotalCount = countResult.TotalHitCount;
					}
					return ids;
				}
			}
			return null;
		}

		protected virtual void FindAgeAndPriceIntervals(Dictionary<int, int> ageFromMonths, Dictionary<int, int> ageToMonths, Collection<decimal> prices, out int? minAge, out int? maxAge, out decimal? minPrice, out decimal? maxPrice)
		{
			minAge = null;
			maxAge = null;
			minPrice = null;
			maxPrice = null;

			if (ageFromMonths != null && ageFromMonths.Keys.Count > 0)
			{
				minAge = ageFromMonths.Keys.Min();
			}

			if (ageToMonths != null && ageToMonths.Keys.Count > 0)
			{
				maxAge = ageToMonths.Keys.Max();
			}

			if (prices != null && prices.Count > 0)
			{
				minPrice = prices.Min();
				maxPrice = prices.Max();
			}
		}
		protected virtual void GetCategory(IdCollection ids, ICategoryTree categoryTree, ref CategoryCollection categories)
		{
			if (ids == null) return;

			foreach (var id in ids)
			{
				var categoryItem = categoryTree.FindItemById(id);
				if (categoryItem != null && categoryItem.Category != null)
				{
					categories.Add(categoryItem.Category);
				}
			}
		}
	}
}