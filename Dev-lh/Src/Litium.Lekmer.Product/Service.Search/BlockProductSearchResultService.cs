﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockProductSearchResultService : IBlockProductSearchResultService
	{
		protected BlockProductSearchResultRepository Repository { get; private set; }

		public BlockProductSearchResultService(BlockProductSearchResultRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductSearchResult GetById(IUserContext context, int id)
		{
			return BlockProductSearchResultCache.Instance.TryGetItem(
				new BlockProductSearchResultKey(context.Channel.Id, id),
				delegate { return GetByIdCore(context.Channel, id); });
		}

		protected virtual IBlockProductSearchResult GetByIdCore(IChannel channel, int id)
		{
			return Repository.GetById(channel, id);
		}
	}
}
