﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.SiteStructure.Cache;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	public class BlockSliderSecureService : IBlockSliderSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected ISliderGroupSecureService SliderGroupSecureService { get; private set; }
		protected BlockSliderRepository Repository { get; private set; }

		public BlockSliderSecureService(
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			ISliderGroupSecureService sliderGroupSecureService,
			BlockSliderRepository repository)
		{
			AccessValidator = accessValidator;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			SliderGroupSecureService = sliderGroupSecureService;
			Repository = repository;
		}

		public IBlockSlider Create()
		{
			var block = IoC.Resolve<IBlockSlider>();
			block.AccessId = AccessSecureService.GetByCommonName("All").Id;
			block.SetNew();
			return block;
		}

		public IBlockSlider GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public int Save(ISystemUserFull systemUser, IBlockSlider block, Collection<ISliderGroup> groups)
		{
			if (block == null) throw new ArgumentNullException("block");
			AccessValidator.ForceAccess(systemUser, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUser, block);
				if (block.Id == -1) return block.Id;

				Repository.Save(block);
				SliderGroupSecureService.Save(systemUser, block.Id, groups);
				transaction.Complete();
			}

			SliderGroupCollectionCache.Instance.Flush();
			BlockSliderCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			var block = Create();
			block.ContentNodeId = contentNodeId;
			block.ContentAreaId = contentAreaId;
			block.BlockTypeId = blockTypeId;
			block.BlockStatusId = (int)BlockStatusInfo.Offline;
			block.Title = title;
			block.TemplateId = null;
			block.Id = Save(systemUserFull, block, new Collection<ISliderGroup>());
			return block;
		}

		public void Delete(ISystemUserFull systemUser, int blockId)
		{
			AccessValidator.ForceAccess(systemUser, PrivilegeConstant.SiteStructurePages);

			Repository.Delete(blockId);
			SliderGroupCollectionCache.Instance.Flush();
			BlockSliderCache.Instance.Remove(blockId);
		}
	}
}