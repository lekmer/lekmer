﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Lekmer.Template;
using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Cache;
using Litium.Scensum.SiteStructure.Repository;
using Litium.Scensum.Template;

namespace Litium.Lekmer.SiteStructure
{
	public class LekmerBlockSecureService : BlockSecureService
	{
		protected IAliasSecureService AliasSecureService { get; private set; }

		public LekmerBlockSecureService(
			IAccessValidator accessValidator,
			BlockRepository repository,
			IAccessSecureService accessSecureService,
			IAliasSecureService aliasSecureService,
			IBlockTypeSecureService blockTypeSecureService) : base(accessValidator, repository, accessSecureService, blockTypeSecureService)
		{
			AliasSecureService = aliasSecureService;
		}

		public override int Save(ISystemUserFull systemUserFull, IBlock block)
		{
			if (block == null) throw new ArgumentNullException("block");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			int result;
			using (var transactedOperation = new TransactedOperation())
			{
				result = Repository.Save(block);

				//Save properties specific for block of ILekmerBlock type
				if (result > 0)
				{
					var lekmerBlock = block as ILekmerBlock;
					if (lekmerBlock != null)
					{
						((LekmerBlockRepository) Repository).Save(lekmerBlock);
					}
				}
				
				transactedOperation.Complete();
			}
			RemoveCache(block);
			return result;
		}

		public override void SaveAll(ISystemUserFull systemUserFull, System.Collections.ObjectModel.Collection<IBlock> blocks)
		{
			if (blocks == null) throw new ArgumentNullException("blocks");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				foreach (IBlock block in blocks)
				{
					//When save all blocks, save only common properties for all blocks of IBlock type 
					//to not reset specific properties for blocks of ILekmerBlock type
					base.Save(systemUserFull, block);
				}
				transactedOperation.Complete();
			}
		}

		public override void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			base.Delete(systemUserFull, blockId);

			var alias = ((ILekmerAliasSecureService)AliasSecureService).GetByCommonName(string.Format("Sitestructure.Block.ButtonText.{0}", blockId));
			if (alias != null)
			{
				AliasSecureService.Delete(systemUserFull, alias.Id);
			}
		}

		public override void RemoveCache(IBlock block)
		{
			base.RemoveCache(block);
			
			if (block is LekmerBlock)
			{
				BlockCache.Instance.Remove(block.Id);
			}
		}
	}
}