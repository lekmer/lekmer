﻿using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	public class SliderGroupSecureService : ISliderGroupSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected SliderGroupRepository Repository { get; private set; }

		public SliderGroupSecureService(IAccessValidator accessValidator, SliderGroupRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public ISliderGroup Create(int blockId, int sliderGroupId)
		{
			var sliderGroup = IoC.Resolve<ISliderGroup>();
			sliderGroup.BlockId = blockId;
			sliderGroup.SliderGroupId = sliderGroupId;
			sliderGroup.SetNew();
			return sliderGroup;
		}

		public Collection<ISliderGroup> GetByBlockId(int blockId)
		{
			return Repository.GetByBlockId(blockId);
		}

		public void Save(ISystemUserFull systemUser, int blockId, Collection<ISliderGroup> groups)
		{
			AccessValidator.ForceAccess(systemUser, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByBlock(blockId);
				foreach (var group in groups)
				{
					Repository.Save(group);
				}
				transactedOperation.Complete();
			}
		}

		public void Save(ISystemUserFull systemUser, ISliderGroup group)
		{
			AccessValidator.ForceAccess(systemUser, PrivilegeConstant.SiteStructurePages);

			Repository.Save(group);
		}
	}
}