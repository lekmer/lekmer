﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class ContentPageUrlHistoryRepository
	{
		protected DataMapperBase<IContentPageUrlHistory> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IContentPageUrlHistory>(dataReader);
		}


		public Collection<IContentPageUrlHistory> GetAllByContentPage(int contentPageId)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.GetAllByContentPage");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ContentPageId", contentPageId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentPageUrlHistoryGetByPageId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<IContentPageUrlHistory> GetAll(int channelId)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.GetAll");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentPageUrlHistoryGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<IContentPageUrlHistory> GetAllByUrlTitle(int channelId, string urlTitle)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.GetAllByUrlTitle");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitleOld", urlTitle, SqlDbType.NVarChar)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentPageUrlHistoryGetByUrlTitle]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}


		public int Save(IContentPageUrlHistory contentPageUrlHistory)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.Save");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ContentPageId", contentPageUrlHistory.ContentPageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitleOld", contentPageUrlHistory.UrlTitleOld, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@HistoryLifeIntervalTypeId", contentPageUrlHistory.TypeId, SqlDbType.Int)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pContentPageUrlHistorySave]", parameters, dbSettings);
		}


		public void Update(int contentPageUrlHistoryId)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ContentPageUrlHistoryId", contentPageUrlHistoryId, SqlDbType.Int),
				};

			new DataHandler().ExecuteCommand("[lekmer].[pContentPageUrlHistoryUpdate]", parameters, dbSettings);
		}


		public void DeleteById(int contentPageUrlHistoryId)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.DeleteById");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ContentPageUrlHistoryId", contentPageUrlHistoryId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pContentPageUrlHistoryDeleteById]", parameters, dbSettings);
		}

		public void DeleteAllByContentPage(int contentPageId)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.DeleteAllByContentPage");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ContentPageId", contentPageId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pContentPageUrlHistoryDeleteByContentPageId]", parameters, dbSettings);
		}

		public void DeleteAllByContentPageAndUrlTitle(int contentPageId, string urlTitle)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.DeleteAllByContentPageAndUrlTitle");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ContentPageId", contentPageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitleOld", urlTitle, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pContentPageUrlHistoryDeleteByContentPageIdAndUrlTitle]", parameters, dbSettings);
		}


		public void DeleteExpiredItems()
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.DeleteExpiredItems");
			IDataParameter[] parameters = new IDataParameter[0];
			new DataHandler().ExecuteCommand("[lekmer].[pContentPageUrlHistoryDeleteExpiredItems]", parameters, dbSettings);
		}

		public void CleanUp(int maxCount)
		{
			var dbSettings = new DatabaseSetting("ContentPageUrlHistoryRepository.CleanUp");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@MaxCount", maxCount, SqlDbType.Int),
				};

			new DataHandler().ExecuteCommand("[lekmer].[pContentPageUrlHistoryCleanUp]", parameters, dbSettings);
		}
	}
}