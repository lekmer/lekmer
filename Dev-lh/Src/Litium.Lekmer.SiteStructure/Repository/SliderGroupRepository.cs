﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class SliderGroupRepository
	{
		protected DataMapperBase<ISliderGroup> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISliderGroup>(dataReader);
		}

		public Collection<ISliderGroup> GetByBlockId(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SliderGroupRepository.GetByBlockId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[sitestructurelek].[pSliderGroupGetByBlockId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows<Collection<ISliderGroup>>();
			}
		}

		public int Save(ISliderGroup sliderGroup)
		{
			if (sliderGroup == null) throw new ArgumentNullException("sliderGroup");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@SliderGroupId", sliderGroup.SliderGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BlockId", sliderGroup.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ImageId1", sliderGroup.ImageId1, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ThumbnailImageId1", sliderGroup.ThumbnailImageId1, SqlDbType.Int),
					ParameterHelper.CreateParameter("@InternalLinkContentNodeId1", sliderGroup.InternalLinkContentNodeId1, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ExternalLink1", sliderGroup.ExternalLink1, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@IsExternalLinkMovie1", sliderGroup.IsExternalLinkMovie1, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@ImageId2", sliderGroup.ImageId2, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ThumbnailImageId2", sliderGroup.ThumbnailImageId2, SqlDbType.Int),
					ParameterHelper.CreateParameter("@InternalLinkContentNodeId2", sliderGroup.InternalLinkContentNodeId2, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ExternalLink2", sliderGroup.ExternalLink2, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@IsExternalLinkMovie2", sliderGroup.IsExternalLinkMovie2, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@Title1", sliderGroup.Title1, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Title1Size", sliderGroup.Title1Size, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title1Color", sliderGroup.Title1Color, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title1Link", sliderGroup.Title1Link, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title2", sliderGroup.Title2, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Title2Size", sliderGroup.Title2Size, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title2Color", sliderGroup.Title2Color, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title2Link", sliderGroup.Title2Link, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title3", sliderGroup.Title3, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Title3Size", sliderGroup.Title3Size, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title3Color", sliderGroup.Title3Color, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title3Link", sliderGroup.Title3Link, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Description", sliderGroup.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@DescriptionSize", sliderGroup.DescriptionSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DescriptionColor", sliderGroup.DescriptionColor, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Disclaimer", sliderGroup.Disclaimer, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@DisclaimerSize", sliderGroup.DisclaimerSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DisclaimerColor", sliderGroup.DisclaimerColor, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Button1Text", sliderGroup.Button1Text, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Button1Size", sliderGroup.Button1Size, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Button1Color", sliderGroup.Button1Color, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Button1Link", sliderGroup.Button1Link, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Button2Text", sliderGroup.Button2Text, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Button2Size", sliderGroup.Button2Size, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Button2Color", sliderGroup.Button2Color, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Button2Link", sliderGroup.Button2Link, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Button3Text", sliderGroup.Button3Text, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Button3Size", sliderGroup.Button3Size, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Button3Color", sliderGroup.Button3Color, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Button3Link", sliderGroup.Button3Link, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@DiscountText", sliderGroup.DiscountText, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@DiscountSize", sliderGroup.DiscountSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DiscountColor", sliderGroup.DiscountColor, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@DiscountPosition", sliderGroup.DiscountPosition, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ExtraText", sliderGroup.ExtraText, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@ExtraSize", sliderGroup.ExtraSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ExtraColor", sliderGroup.ExtraColor, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ExtraPosition", sliderGroup.ExtraPosition, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BackgroundColor1", sliderGroup.BackgroundColor1, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@BackgroundColor2", sliderGroup.BackgroundColor2, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@BackgroundGradient", sliderGroup.BackgroundGradient, SqlDbType.Int),
					ParameterHelper.CreateParameter("@HolderColor", sliderGroup.HolderColor, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@HolderAlpha", sliderGroup.HolderAlpha, SqlDbType.Int),
					ParameterHelper.CreateParameter("@HolderShape", sliderGroup.HolderShape, SqlDbType.Int),
					ParameterHelper.CreateParameter("@HolderTextAlign", sliderGroup.HolderTextAlign, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Style", sliderGroup.Style, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Ordinal", sliderGroup.Ordinal, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Captions", sliderGroup.Captions, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@CampaignId", sliderGroup.CampaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CampaignTypeId", sliderGroup.CampaignTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IsActive", sliderGroup.IsActive, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@UseCampaignDates", sliderGroup.UseCampaignDates, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@StartDateTime", sliderGroup.StartDateTime, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@EndDateTime", sliderGroup.EndDateTime, SqlDbType.DateTime)
				};
			var dbSettings = new DatabaseSetting("SliderGroupRepository.Save");
			sliderGroup.SliderGroupId = new DataHandler().ExecuteCommandWithReturnValue("[sitestructurelek].[pSliderGroupSave]", parameters, dbSettings);
			return sliderGroup.SliderGroupId;
		}

		public void DeleteAllByBlock(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SliderGroupRepository.Delete");
			new DataHandler().ExecuteCommand("[sitestructurelek].[pSliderGroupDelete]", parameters, dbSettings);
		}
	}
}