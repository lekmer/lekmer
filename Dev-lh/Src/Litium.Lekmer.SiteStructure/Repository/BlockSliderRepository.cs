﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class BlockSliderRepository
	{
		protected DataMapperBase<IBlockSlider> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockSlider>(dataReader);
		}

		public IBlockSlider GetById(int languageId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockSliderRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[sitestructurelek].[pBlockSliderGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public IBlockSlider GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockSliderRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[sitestructurelek].[pBlockSliderGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public void Save(IBlockSlider block)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("SlideDuration", block.SlideDuration, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockSliderRepository.Save");
			new DataHandler().ExecuteCommand("[sitestructurelek].[pBlockSliderSave]", parameters, dbSettings);
		}

		public void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockSliderRepository.Delete");
			new DataHandler().ExecuteCommand("[sitestructurelek].[pBlockSliderDelete]", parameters, dbSettings);
		}
	}
}