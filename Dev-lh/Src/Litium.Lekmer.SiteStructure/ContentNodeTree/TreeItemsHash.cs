﻿using System;
using System.Collections.Generic;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class TreeItemsHash : ITreeItemsHash
	{
		private bool _initialized;
		private Dictionary<string, List<IContentNodeTreeItem>> _items;


		public void Initialize(IContentNodeTree contentNodeTree)
		{
			if (_initialized)
			{
				throw new InvalidOperationException("Hash could be initialized only once.");
			}

			PopulateTreeItemsHash(contentNodeTree);

			_initialized = true;
		}

		public bool ContainsKey(string pageUrlTitle)
		{
			CheckIfInitialized();

			return _items.ContainsKey(pageUrlTitle);
		}

		public IContentNodeTreeItem FirstOrDefault(string pageUrlTitle)
		{
			CheckIfInitialized();

			if (_items.ContainsKey(pageUrlTitle))
			{
				var itemsList = _items[pageUrlTitle];
				if (itemsList != null && itemsList.Count > 0)
				{
					return itemsList[0];
				}
			}

			return null;
		}


		private void PopulateTreeItemsHash(IContentNodeTree contentNodeTree)
		{
			_items = new Dictionary<string, List<IContentNodeTreeItem>>();

			if (contentNodeTree.Root != null)
			{
				AddItem(contentNodeTree.Root);
				AddChildrenItem(contentNodeTree.Root);
			}
		}

		private void AddChildrenItem(IContentNodeTreeItem parentItem)
		{
			if (parentItem.Children == null)
			{
				return;
			}

			if (parentItem.Children.Count == 0)
			{
				return;
			}

			foreach (IContentNodeTreeItem childItem in parentItem.Children)
			{
				AddItem(childItem);
				AddChildrenItem(childItem);
			}
		}

		private void AddItem(IContentNodeTreeItem item)
		{
			var contentPage = item.ContentNode as IContentPage;

			if (contentPage == null)
			{
				return;
			}

			string key = contentPage.UrlTitle;
			if (key.IsEmpty())
			{
				return;
			}

			if (_items.ContainsKey(key))
			{
				_items[key].Add(item);
			}
			else
			{
				_items[key] = new List<IContentNodeTreeItem> { item };
			}
		}

		private void CheckIfInitialized()
		{
			if (!_initialized)
			{
				throw new InvalidOperationException("Hash is not initialized.");
			}
		}
	}
}
