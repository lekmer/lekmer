﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public class SliderGroupService : ISliderGroupService
	{
		protected SliderGroupRepository Repository { get; private set; }

		public SliderGroupService(SliderGroupRepository repository)
		{
			Repository = repository;
		}

		public Collection<ISliderGroup> GetByBlockId(IUserContext context, int blockId)
		{
			return SliderGroupCollectionCache.Instance.TryGetItem(
				new SliderGroupCollectionKey(context.Channel.Id, blockId),
				() => Repository.GetByBlockId(blockId));
		}
	}
}