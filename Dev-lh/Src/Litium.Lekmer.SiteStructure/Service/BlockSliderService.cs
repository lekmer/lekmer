﻿using Litium.Lekmer.SiteStructure.Cache;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public class BlockSliderService : IBlockSliderService
	{
		protected BlockSliderRepository Repository { get; private set; }

		public BlockSliderService(BlockSliderRepository repository)
		{
			Repository = repository;
		}

		public IBlockSlider GetById(IUserContext context, int id)
		{
			return BlockSliderCache.Instance.TryGetItem(
				new BlockSliderKey(context.Channel.Id, id),
				() => Repository.GetById(context.Channel.Language.Id, id));
		}
	}
}