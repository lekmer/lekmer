﻿using Litium.Lekmer.SiteStructure.Cache;
using Litium.Lekmer.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure
{
	public class BlockSettingService : IBlockSettingService
	{
		protected BlockSettingRepository Repository { get; private set; }

		public BlockSettingService(BlockSettingRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockSetting GetByBlock(int blockId)
		{
			return BlockSettingCache.Instance.TryGetItem(
				new BlockSettingKey(blockId),
				delegate { return Repository.GetByBlock(blockId); });
		}
	}
}