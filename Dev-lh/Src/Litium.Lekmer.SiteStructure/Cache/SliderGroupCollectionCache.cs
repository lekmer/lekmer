﻿using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class SliderGroupCollectionCache : ScensumCacheBase<SliderGroupCollectionKey, Collection<ISliderGroup>>
	{
		private static readonly SliderGroupCollectionCache _instance = new SliderGroupCollectionCache();

		private SliderGroupCollectionCache()
		{
		}

		public static SliderGroupCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class SliderGroupCollectionKey : ICacheKey
	{
		public SliderGroupCollectionKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return string.Format("{0} - {1}", ChannelId, BlockId); }
		}
	}
}