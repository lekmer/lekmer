﻿using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.SiteStructure.Cache
{
	public sealed class BlockSliderCache : ScensumCacheBase<BlockSliderKey, IBlockSlider>
	{
		private static readonly BlockSliderCache _instance = new BlockSliderCache();

		private BlockSliderCache()
		{
		}

		public static BlockSliderCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockSliderKey(channel.Id, blockId));
			}
		}
	}

	public class BlockSliderKey : ICacheKey
	{
		public BlockSliderKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return string.Format("{0} - {1}", ChannelId, BlockId); }
		}
	}
}