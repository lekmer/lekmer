﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class ContentPageUrlHistory : BusinessObjectBase, IContentPageUrlHistory
	{
		private int _id;
		private int _contentPageId;
		private string _urlTitleOld;
		private DateTime _creationDate;
		private DateTime _lastUsageDate;
		private int _usageAmount;
		private int _typeId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ContentPageId
		{
			get { return _contentPageId; }
			set 
			{
				CheckChanged(_contentPageId, value); 
				_contentPageId = value; 
			}
		}

		public string UrlTitleOld
		{
			get { return _urlTitleOld; }
			set
			{
				CheckChanged(_urlTitleOld, value);
				_urlTitleOld = value;
			}
		}

		public DateTime CreationDate
		{
			get { return _creationDate; }
			set
			{
				CheckChanged(_creationDate, value);
				_creationDate = value;
			}
		}

		public DateTime LastUsageDate
		{
			get { return _lastUsageDate; }
			set
			{
				CheckChanged(_lastUsageDate, value);
				_lastUsageDate = value;
			}
		}

		public int UsageAmount
		{
			get { return _usageAmount; }
			set
			{
				CheckChanged(_usageAmount, value);
				_usageAmount = value;
			}
		}

		public int TypeId
		{
			get { return _typeId; }
			set
			{
				CheckChanged(_typeId, value);
				_typeId = value;
			}
		}
	}
}