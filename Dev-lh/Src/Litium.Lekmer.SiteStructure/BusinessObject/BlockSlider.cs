﻿using System;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class BlockSlider : LekmerBlockBase, IBlockSlider
	{
		private int _slideDuration;
		public int SlideDuration
		{
			get { return _slideDuration; }
			set
			{
				CheckChanged(_slideDuration, value);
				_slideDuration = value;
			}
		}
	}
}