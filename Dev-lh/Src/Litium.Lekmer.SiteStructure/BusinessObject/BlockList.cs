﻿using System;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class BlockList : LekmerBlockBase, IBlockList
	{
		private IBlockSetting _setting;

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}
