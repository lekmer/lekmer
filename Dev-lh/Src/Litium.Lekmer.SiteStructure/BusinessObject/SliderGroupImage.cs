﻿namespace Litium.Lekmer.SiteStructure
{
	public class SliderGroupImage
	{
		public int GroupId { get; set; }
		public string Type { get; set; }
		public string Url { get; set; }
	}
}