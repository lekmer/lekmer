﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class SliderGroup : BusinessObjectBase, ISliderGroup
	{
		private int _sliderGroupId;
		private int _blockId;
		private int? _imageId1;
		private int? _thumbnailImageId1;
		private int? _internalLinkContentNodeId1;
		private string _externalLink1;
		private bool _isExternalLinkMovie1;
		private int? _imageId2;
		private int? _thumbnailImageId2;
		private int? _internalLinkContentNodeId2;
		private string _externalLink2;
		private bool _isExternalLinkMovie2;
		private string _title1;
		private int? _title1Size;
		private string _title1Color;
		private string _title1Link;
		private string _title2;
		private int? _title2Size;
		private string _title2Color;
		private string _title2Link;
		private string _title3;
		private int? _title3Size;
		private string _title3Color;
		private string _title3Link;
		private string _description;
		private int? _descriptionSize;
		private string _descriptionColor;
		private string _disclaimer;
		private int? _disclaimerSize;
		private string _disclaimerColor;
		private string _button1Text;
		private int? _button1Size;
		private string _button1Color;
		private string _button1Link;
		private string _button2Text;
		private int? _button2Size;
		private string _button2Color;
		private string _button2Link;
		private string _button3Text;
		private int? _button3Size;
		private string _button3Color;
		private string _button3Link;
		private string _discountText;
		private int? _discountSize;
		private string _discountColor;
		private int? _discountPosition;
		private string _extraText;
		private int? _extraSize;
		private string _extraColor;
		private int? _extraPosition;
		private string _backgroundColor1;
		private string _backgroundColor2;
		private int? _backgroundGradient;
		private string _holderColor;
		private int? _holderAlpha;
		private int? _holderShape;
		private int? _holderTextAlign;
		private int? _style;
		private int _ordinal;
		private string _captions;
		private int? _campaignId;
		private int? _campaignTypeId;
		private bool _isActive;
		private bool _useCampaignDates;
		private DateTime? _startDateTime;
		private DateTime? _endDateTime;
		private bool _isOpened = true;

		public int SliderGroupId
		{
			get { return _sliderGroupId; }
			set
			{
				CheckChanged(_sliderGroupId, value);
				_sliderGroupId = value;
			}
		}
		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}
		public int? ImageId1
		{
			get { return _imageId1; }
			set
			{
				CheckChanged(_imageId1, value);
				_imageId1 = value;
			}
		}
		public int? ThumbnailImageId1
		{
			get { return _thumbnailImageId1; }
			set
			{
				CheckChanged(_thumbnailImageId1, value);
				_thumbnailImageId1 = value;
			}
		}
		public int? InternalLinkContentNodeId1
		{
			get { return _internalLinkContentNodeId1; }
			set
			{
				CheckChanged(_internalLinkContentNodeId1, value);
				_internalLinkContentNodeId1 = value;
			}
		}
		public string ExternalLink1
		{
			get { return _externalLink1; }
			set
			{
				CheckChanged(_externalLink1, value);
				_externalLink1 = value;
			}
		}
		public bool IsExternalLinkMovie1
		{
			get { return _isExternalLinkMovie1; }
			set
			{
				CheckChanged(_isExternalLinkMovie1, value);
				_isExternalLinkMovie1 = value;
			}
		}
		public int? ImageId2
		{
			get { return _imageId2; }
			set
			{
				CheckChanged(_imageId2, value);
				_imageId2 = value;
			}
		}
		public int? ThumbnailImageId2
		{
			get { return _thumbnailImageId2; }
			set
			{
				CheckChanged(_thumbnailImageId2, value);
				_thumbnailImageId2 = value;
			}
		}
		public int? InternalLinkContentNodeId2
		{
			get { return _internalLinkContentNodeId2; }
			set
			{
				CheckChanged(_internalLinkContentNodeId2, value);
				_internalLinkContentNodeId2 = value;
			}
		}
		public string ExternalLink2
		{
			get { return _externalLink2; }
			set
			{
				CheckChanged(_externalLink2, value);
				_externalLink2 = value;
			}
		}
		public bool IsExternalLinkMovie2
		{
			get { return _isExternalLinkMovie2; }
			set
			{
				CheckChanged(_isExternalLinkMovie2, value);
				_isExternalLinkMovie2 = value;
			}
		}
		public string Title1
		{
			get { return _title1; }
			set
			{
				CheckChanged(_title1, value);
				_title1 = value;
			}
		}
		public int? Title1Size
		{
			get { return _title1Size; }
			set
			{
				CheckChanged(_title1Size, value);
				_title1Size = value;
			}
		}
		public string Title1Color
		{
			get { return _title1Color; }
			set
			{
				CheckChanged(_title1Color, value);
				_title1Color = value;
			}
		}
		public string Title1Link
		{
			get { return _title1Link; }
			set
			{
				CheckChanged(_title1Link, value);
				_title1Link = value;
			}
		}
		public string Title2
		{
			get { return _title2; }
			set
			{
				CheckChanged(_title2, value);
				_title2 = value;
			}
		}
		public int? Title2Size
		{
			get { return _title2Size; }
			set
			{
				CheckChanged(_title2Size, value);
				_title2Size = value;
			}
		}
		public string Title2Color
		{
			get { return _title2Color; }
			set
			{
				CheckChanged(_title2Color, value);
				_title2Color = value;
			}
		}
		public string Title2Link
		{
			get { return _title2Link; }
			set
			{
				CheckChanged(_title2Link, value);
				_title2Link = value;
			}
		}
		public string Title3
		{
			get { return _title3; }
			set
			{
				CheckChanged(_title3, value);
				_title3 = value;
			}
		}
		public int? Title3Size
		{
			get { return _title3Size; }
			set
			{
				CheckChanged(_title3Size, value);
				_title3Size = value;
			}
		}
		public string Title3Color
		{
			get { return _title3Color; }
			set
			{
				CheckChanged(_title3Color, value);
				_title3Color = value;
			}
		}
		public string Title3Link
		{
			get { return _title3Link; }
			set
			{
				CheckChanged(_title3Link, value);
				_title3Link = value;
			}
		}
		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}
		public int? DescriptionSize
		{
			get { return _descriptionSize; }
			set
			{
				CheckChanged(_descriptionSize, value);
				_descriptionSize = value;
			}
		}
		public string DescriptionColor
		{
			get { return _descriptionColor; }
			set
			{
				CheckChanged(_descriptionColor, value);
				_descriptionColor = value;
			}
		}
		public string Disclaimer
		{
			get { return _disclaimer; }
			set
			{
				CheckChanged(_disclaimer, value);
				_disclaimer = value;
			}
		}
		public int? DisclaimerSize
		{
			get { return _disclaimerSize; }
			set
			{
				CheckChanged(_disclaimerSize, value);
				_disclaimerSize = value;
			}
		}
		public string DisclaimerColor
		{
			get { return _disclaimerColor; }
			set
			{
				CheckChanged(_disclaimerColor, value);
				_disclaimerColor = value;
			}
		}
		public string Button1Text
		{
			get { return _button1Text; }
			set
			{
				CheckChanged(_button1Text, value);
				_button1Text = value;
			}
		}
		public int? Button1Size
		{
			get { return _button1Size; }
			set
			{
				CheckChanged(_button1Size, value);
				_button1Size = value;
			}
		}
		public string Button1Color
		{
			get { return _button1Color; }
			set
			{
				CheckChanged(_button1Color, value);
				_button1Color = value;
			}
		}
		public string Button1Link
		{
			get { return _button1Link; }
			set
			{
				CheckChanged(_button1Link, value);
				_button1Link = value;
			}
		}
		public string Button2Text
		{
			get { return _button2Text; }
			set
			{
				CheckChanged(_button2Text, value);
				_button2Text = value;
			}
		}
		public int? Button2Size
		{
			get { return _button2Size; }
			set
			{
				CheckChanged(_button2Size, value);
				_button2Size = value;
			}
		}
		public string Button2Color
		{
			get { return _button2Color; }
			set
			{
				CheckChanged(_button2Color, value);
				_button2Color = value;
			}
		}
		public string Button2Link
		{
			get { return _button2Link; }
			set
			{
				CheckChanged(_button2Link, value);
				_button2Link = value;
			}
		}
		public string Button3Text
		{
			get { return _button3Text; }
			set
			{
				CheckChanged(_button3Text, value);
				_button3Text = value;
			}
		}
		public int? Button3Size
		{
			get { return _button3Size; }
			set
			{
				CheckChanged(_button3Size, value);
				_button3Size = value;
			}
		}
		public string Button3Color
		{
			get { return _button3Color; }
			set
			{
				CheckChanged(_button3Color, value);
				_button3Color = value;
			}
		}
		public string Button3Link
		{
			get { return _button3Link; }
			set
			{
				CheckChanged(_button3Link, value);
				_button3Link = value;
			}
		}
		public string DiscountText
		{
			get { return _discountText; }
			set
			{
				CheckChanged(_discountText, value);
				_discountText = value;
			}
		}
		public int? DiscountSize
		{
			get { return _discountSize; }
			set
			{
				CheckChanged(_discountSize, value);
				_discountSize = value;
			}
		}
		public string DiscountColor
		{
			get { return _discountColor; }
			set
			{
				CheckChanged(_discountColor, value);
				_discountColor = value;
			}
		}
		public int? DiscountPosition
		{
			get { return _discountPosition; }
			set
			{
				CheckChanged(_discountPosition, value);
				_discountPosition = value;
			}
		}
		public string ExtraText
		{
			get { return _extraText; }
			set
			{
				CheckChanged(_extraText, value);
				_extraText = value;
			}
		}
		public int? ExtraSize
		{
			get { return _extraSize; }
			set
			{
				CheckChanged(_extraSize, value);
				_extraSize = value;
			}
		}
		public string ExtraColor
		{
			get { return _extraColor; }
			set
			{
				CheckChanged(_extraColor, value);
				_extraColor = value;
			}
		}
		public int? ExtraPosition
		{
			get { return _extraPosition; }
			set
			{
				CheckChanged(_extraPosition, value);
				_extraPosition = value;
			}
		}
		public string BackgroundColor1
		{
			get { return _backgroundColor1; }
			set
			{
				CheckChanged(_backgroundColor1, value);
				_backgroundColor1 = value;
			}
		}
		public string BackgroundColor2
		{
			get { return _backgroundColor2; }
			set
			{
				CheckChanged(_backgroundColor2, value);
				_backgroundColor2 = value;
			}
		}
		public int? BackgroundGradient
		{
			get { return _backgroundGradient; }
			set
			{
				CheckChanged(_backgroundGradient, value);
				_backgroundGradient = value;
			}
		}
		public string HolderColor
		{
			get { return _holderColor; }
			set
			{
				CheckChanged(_holderColor, value);
				_holderColor = value;
			}
		}
		public int? HolderAlpha
		{
			get { return _holderAlpha; }
			set
			{
				CheckChanged(_holderAlpha, value);
				_holderAlpha = value;
			}
		}
		public int? HolderShape
		{
			get { return _holderShape; }
			set
			{
				CheckChanged(_holderShape, value);
				_holderShape = value;
			}
		}
		public int? HolderTextAlign
		{
			get { return _holderTextAlign; }
			set
			{
				CheckChanged(_holderTextAlign, value);
				_holderTextAlign = value;
			}
		}
		public int? Style
		{
			get { return _style; }
			set
			{
				CheckChanged(_style, value);
				_style = value;
			}
		}
		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
		public string Captions
		{
			get { return _captions; }
			set
			{
				CheckChanged(_captions, value);
				_captions = value;
			}
		}
		public int? CampaignId
		{
			get { return _campaignId; }
			set
			{
				CheckChanged(_campaignId, value);
				_campaignId = value;
			}
		}
		public int? CampaignTypeId
		{
			get { return _campaignTypeId; }
			set
			{
				CheckChanged(_campaignTypeId, value);
				_campaignTypeId = value;
			}
		}
		public bool IsActive
		{
			get { return _isActive; }
			set
			{
				CheckChanged(_isActive, value);
				_isActive = value;
			}
		}
		public bool UseCampaignDates
		{
			get { return _useCampaignDates; }
			set
			{
				CheckChanged(_useCampaignDates, value);
				_useCampaignDates = value;
			}
		}
		public DateTime? StartDateTime
		{
			get { return _startDateTime; }
			set
			{
				CheckChanged(_startDateTime, value);
				_startDateTime = value;
			}
		}
		public DateTime? EndDateTime
		{
			get { return _endDateTime; }
			set
			{
				CheckChanged(_endDateTime, value);
				_endDateTime = value;
			}
		}
		public bool IsOpened
		{
			get { return _isOpened; }
			set { _isOpened = value; }
		}
	}
}