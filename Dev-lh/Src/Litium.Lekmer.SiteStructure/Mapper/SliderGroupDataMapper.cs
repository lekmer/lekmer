﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Mapper
{
	public class SliderGroupDataMapper : DataMapperBase<ISliderGroup>
	{
		public SliderGroupDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISliderGroup Create()
		{
			var sliderGroup = IoC.Resolve<ISliderGroup>();
			sliderGroup.SliderGroupId = MapValue<int>("SliderGroupId");
			sliderGroup.BlockId = MapValue<int>("BlockId");
			sliderGroup.ImageId1 = MapNullableValue<int?>("ImageId1");
			sliderGroup.ThumbnailImageId1 = MapNullableValue<int?>("ThumbnailImageId1");
			sliderGroup.InternalLinkContentNodeId1 = MapNullableValue<int?>("InternalLinkContentNodeId1");
			sliderGroup.ExternalLink1 = MapNullableValue<string>("ExternalLink1");
			sliderGroup.IsExternalLinkMovie1 = MapValue<bool>("IsExternalLinkMovie1");
			sliderGroup.ImageId2 = MapNullableValue<int?>("ImageId2");
			sliderGroup.ThumbnailImageId2 = MapNullableValue<int?>("ThumbnailImageId2");
			sliderGroup.InternalLinkContentNodeId2 = MapNullableValue<int?>("InternalLinkContentNodeId2");
			sliderGroup.ExternalLink2 = MapNullableValue<string>("ExternalLink2");
			sliderGroup.IsExternalLinkMovie2 = MapValue<bool>("IsExternalLinkMovie2");
			sliderGroup.Title1 = MapNullableValue<string>("Title1");
			sliderGroup.Title1Size = MapNullableValue<int?>("Title1Size");
			sliderGroup.Title1Color = MapNullableValue<string>("Title1Color");
			sliderGroup.Title1Link = MapNullableValue<string>("Title1Link");
			sliderGroup.Title2 = MapNullableValue<string>("Title2");
			sliderGroup.Title2Size = MapNullableValue<int?>("Title2Size");
			sliderGroup.Title2Color = MapNullableValue<string>("Title2Color");
			sliderGroup.Title2Link = MapNullableValue<string>("Title2Link");
			sliderGroup.Title3 = MapNullableValue<string>("Title3");
			sliderGroup.Title3Size = MapNullableValue<int?>("Title3Size");
			sliderGroup.Title3Color = MapNullableValue<string>("Title3Color");
			sliderGroup.Title3Link = MapNullableValue<string>("Title3Link");
			sliderGroup.Description = MapNullableValue<string>("Description");
			sliderGroup.DescriptionSize = MapNullableValue<int?>("DescriptionSize");
			sliderGroup.DescriptionColor = MapNullableValue<string>("DescriptionColor");
			sliderGroup.Disclaimer = MapNullableValue<string>("Disclaimer");
			sliderGroup.DisclaimerSize = MapNullableValue<int?>("DisclaimerSize");
			sliderGroup.DisclaimerColor = MapNullableValue<string>("DisclaimerColor");
			sliderGroup.Button1Text = MapNullableValue<string>("Button1Text");
			sliderGroup.Button1Size = MapNullableValue<int?>("Button1Size");
			sliderGroup.Button1Color = MapNullableValue<string>("Button1Color");
			sliderGroup.Button1Link = MapNullableValue<string>("Button1Link");
			sliderGroup.Button2Text = MapNullableValue<string>("Button2Text");
			sliderGroup.Button2Size = MapNullableValue<int?>("Button2Size");
			sliderGroup.Button2Color = MapNullableValue<string>("Button2Color");
			sliderGroup.Button2Link = MapNullableValue<string>("Button2Link");
			sliderGroup.Button3Text = MapNullableValue<string>("Button3Text");
			sliderGroup.Button3Size = MapNullableValue<int?>("Button3Size");
			sliderGroup.Button3Color = MapNullableValue<string>("Button3Color");
			sliderGroup.Button3Link = MapNullableValue<string>("Button3Link");
			sliderGroup.DiscountText = MapNullableValue<string>("DiscountText");
			sliderGroup.DiscountSize = MapNullableValue<int?>("DiscountSize");
			sliderGroup.DiscountColor = MapNullableValue<string>("DiscountColor");
			sliderGroup.DiscountPosition = MapNullableValue<int?>("DiscountPosition");
			sliderGroup.ExtraText = MapNullableValue<string>("ExtraText");
			sliderGroup.ExtraSize = MapNullableValue<int?>("ExtraSize");
			sliderGroup.ExtraColor = MapNullableValue<string>("ExtraColor");
			sliderGroup.ExtraPosition = MapNullableValue<int?>("ExtraPosition");
			sliderGroup.BackgroundColor1 = MapNullableValue<string>("BackgroundColor1");
			sliderGroup.BackgroundColor2 = MapNullableValue<string>("BackgroundColor2");
			sliderGroup.BackgroundGradient = MapNullableValue<int?>("BackgroundGradient");
			sliderGroup.HolderColor = MapNullableValue<string>("HolderColor");
			sliderGroup.HolderAlpha = MapNullableValue<int?>("HolderAlpha");
			sliderGroup.HolderShape = MapNullableValue<int?>("HolderShape");
			sliderGroup.HolderTextAlign = MapNullableValue<int?>("HolderTextAlign");
			sliderGroup.Style = MapNullableValue<int?>("Style");
			sliderGroup.Ordinal = MapValue<int>("Ordinal");
			sliderGroup.Captions = MapNullableValue<string>("Captions");
			sliderGroup.CampaignId = MapNullableValue<int?>("CampaignId");
			sliderGroup.CampaignTypeId = MapNullableValue<int?>("CampaignTypeId");
			sliderGroup.IsActive = MapValue<bool>("IsActive");
			sliderGroup.UseCampaignDates = MapValue<bool>("UseCampaignDates");
			sliderGroup.StartDateTime = MapNullableValue<DateTime?>("StartDateTime");
			sliderGroup.EndDateTime = MapNullableValue<DateTime?>("EndDateTime");
			sliderGroup.SetUntouched();
			return sliderGroup;
		}
	}
}