﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure.Mapper
{
	public class BlockSliderDataMapper : DataMapperBase<IBlockSlider>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockSliderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockSlider Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockSlider = IoC.Resolve<IBlockSlider>();
			block.ConvertTo(blockSlider);
			blockSlider.SlideDuration = MapValue<int>("SlideDuration");
			return blockSlider;
		}
	}
}