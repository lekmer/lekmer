﻿using System.Collections.Specialized;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Payment
{
	public class DibsResponse
	{
		public DibsResponse(NameValueCollection providerResponse)
		{
			if (providerResponse != null)
			{
				ParseResponse(providerResponse);
			}
		}

		public string Action { get; private set; }
		public string Reply { get; private set; }
		public string ReplyText { get; private set; }
		public string VerifyId { get; private set; }
		public string ReferenceNo { get; private set; }
		public string InvoiceNo { get; private set; }
		public int OrderId { get; private set; }
		public string Sum { get; private set; }
		public string Currency { get; private set; }
		public string AuthenticationCode { get; private set; }
		public string AcquirerResponseCode { get; private set; }
		public string CreditCardType { get; private set; }
		public string CreditCardPart { get; private set; }
		public string CreditCardPrefix { get; private set; }
		public string CreditCardExpMonth { get; private set; }
		public string CreditCardExpYear { get; private set; }
		public string Mac { get; private set; }

		public bool Validate()
		{
			// Required for transaction authenticate
			return
				Reply.HasValue() &&
				VerifyId.HasValue() &&
				OrderId > 0 &&
				Sum.HasValue() &&
				Currency.HasValue() &&
				Mac.HasValue();
		}

		protected void ParseResponse(NameValueCollection providerResponse)
		{
			Action = providerResponse["action"];
			Reply = providerResponse["reply"];
			ReplyText = providerResponse["replyText"];
			VerifyId = providerResponse["verifyid"];

			ReferenceNo = providerResponse["referenceNo"];
			InvoiceNo = providerResponse["invoiceNo"];

			int orderId;
			if (int.TryParse(providerResponse["orderno"], out orderId))
			{
				OrderId = orderId;
			}

			Sum = providerResponse["sum"];
			Currency = providerResponse["currency"];
			AuthenticationCode = providerResponse["authCode"];
			AcquirerResponseCode = providerResponse["acqrCode"];
			CreditCardType = providerResponse["ccType"];
			CreditCardPart = providerResponse["ccPart"];
			CreditCardPrefix = providerResponse["ccPrefix"];
			CreditCardExpMonth = providerResponse["expM"];
			CreditCardExpYear = providerResponse["expY"];
			Mac = providerResponse["MAC"];
		}
	}
}