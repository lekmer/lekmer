﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Litium.Lekmer.Product.MonitorConsole")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: Guid("c557ebfb-147f-4fef-bd0e-43544e6c9e74")]
[assembly: CLSCompliant(true)]

// Configure log4net using the .config file
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
	