﻿using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class OrderShipmentConverter : IOrderShipmentConverter
	{
		public virtual invoiceItemType Convert(ILekmerOrderFull orderFull, bool taxFreeZone, decimal vatPercentage)
		{
			decimal freightCostInclVat = orderFull.GetTotalFreightCost();
			decimal freightCostExclVat = PriceCalc.PriceExcludingVat1(freightCostInclVat, vatPercentage / 100.0m);
			decimal vat = vatPercentage;

			if (taxFreeZone)
			{
				freightCostInclVat = freightCostExclVat;
				vat = 0m;
			}

			var invoiceItem = new invoiceItemType
			{
				itemNo = "9990002",
				description = "Shipping",
				itemPrice = freightCostExclVat, // item price (excl. VAT)
				grossAmount = freightCostInclVat, // item price (incl. VAT) * item count
				netAmount = freightCostExclVat, // item price (excl. VAT) * item count
				vatPercent = vat,
				quantity = 1,
				unit = null,
				discountAmount = 0.0m, // total discount
				feeType = debtFeeType.PackAndShip
			};

			return invoiceItem;
		}
	}
}
