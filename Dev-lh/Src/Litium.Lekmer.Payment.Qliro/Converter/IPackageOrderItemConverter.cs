﻿using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IPackageOrderItemConverter
	{
		invoiceItemType Convert(IPackageOrderItem packageOrderItem, bool taxFreeZone);
	}
}
