﻿using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Qliro
{
	public class OrderDiscountConverter : IOrderDiscountConverter
	{
		public virtual invoiceItemType Convert(ILekmerOrderFull lekmerOrderFull, bool taxFreeZone, decimal vatPercentage)
		{
			IOrderPayment orderPayment = lekmerOrderFull.Payments.FirstOrDefault();
			if (orderPayment == null)
			{
				return null;
			}

			decimal orderPaymentPrice = orderPayment.Price;
			Price actualPriceSummary = lekmerOrderFull.GetBaseActualPriceSummary();
			decimal baseActualPriceSummary = actualPriceSummary.IncludingVat;
			decimal vat = vatPercentage;

			if (taxFreeZone) // ExclVat
			{
				orderPaymentPrice = orderPayment.Price - orderPayment.Vat;
				baseActualPriceSummary = actualPriceSummary.ExcludingVat;
				vat = 0m;
			}

			decimal discountAmount = orderPaymentPrice - baseActualPriceSummary;
			if (discountAmount >= 0)
			{
				return null;
			}

			decimal discountAmountExcludingVat = PriceCalc.PriceExcludingVat1(discountAmount, vat / 100.0m);

			var invoiceItem = new invoiceItemType
			{
				itemNo = "discount",
				description = "Discount",
				itemPrice = discountAmountExcludingVat, // item price (excl. VAT)
				grossAmount = discountAmount, // item price (incl. VAT) * item count
				netAmount = discountAmountExcludingVat, // item price (excl. VAT) * item count
				vatPercent = vat,
				quantity = 1,
				unit = null,
				discountAmount = 0.0m, // total discount
				feeType = debtFeeType.None
			};

			return invoiceItem;
		}
	}
}
