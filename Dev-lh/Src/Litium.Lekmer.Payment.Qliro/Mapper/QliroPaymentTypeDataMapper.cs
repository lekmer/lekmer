using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro.Mapper
{
	public class QliroPaymentTypeDataMapper : DataMapperBase<IQliroPaymentType>
	{
		public QliroPaymentTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IQliroPaymentType Create()
		{
			var paymentType = IoC.Resolve<IQliroPaymentType>();

			paymentType.Id = MapValue<int>("QliroPaymentType.Id");
			paymentType.ChannelId = MapValue<int>("QliroPaymentType.ChannelId");
			paymentType.Code = MapValue<string>("QliroPaymentType.Code");
			paymentType.Description = MapValue<string>("QliroPaymentType.Description");
			paymentType.RegistrationFee = MapValue<decimal>("QliroPaymentType.RegistrationFee");
			paymentType.SettlementFee = MapValue<decimal>("QliroPaymentType.SettlementFee");
			paymentType.InterestRate = MapValue<decimal>("QliroPaymentType.InterestRate");
			paymentType.InterestType = MapValue<int>("QliroPaymentType.InterestType");
			paymentType.InterestCalculation = (QliroInterestCalculationType)MapValue<int>("QliroPaymentType.InterestCalculation");
			paymentType.NoOfMonths = MapValue<int>("QliroPaymentType.NoOfMonths");
			paymentType.MinPurchaseAmount = MapValue<decimal>("QliroPaymentType.MinPurchaseAmount");

			return paymentType;
		}
	}
}