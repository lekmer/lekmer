﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Payment.Qliro.Cache
{
	public sealed class QliroPaymentTypesCache : ScensumCacheBase<QliroPaymentTypeKey, Collection<IQliroPaymentType>>
	{
		#region Singleton

		private static readonly QliroPaymentTypesCache _instance = new QliroPaymentTypesCache();

		private QliroPaymentTypesCache()
		{
		}

		public static QliroPaymentTypesCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class QliroPaymentTypeKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public QliroPaymentTypeKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}