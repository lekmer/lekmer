﻿using System;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroCalculation : IQliroCalculation
	{
		public virtual decimal CalculateCheapestMonthlyInvoiceFee()
		{
			throw new NotImplementedException();
		}

		public virtual decimal CalculateMonthlyCost(decimal sum, IQliroPaymentType paymentType)
		{
			switch (paymentType.InterestCalculation)
			{
				case QliroInterestCalculationType.Annuity:
					return CalculateMonthlyCostForAnnuity(sum, paymentType);
				case QliroInterestCalculationType.Flat:
					return CalculateMonthlyCostForAccount(sum, paymentType);
				default:
					return 0;
			}
		}

		public virtual decimal CalculateLowestAmountToPay(decimal sum)
		{
			throw new NotImplementedException();
		}


		public virtual decimal CalculateMonthlyCostForAnnuity(decimal sum, IQliroPaymentType paymentType)
		{
			if (paymentType.InterestCalculation != QliroInterestCalculationType.Annuity)
			{
				return 0;
			}

			if (paymentType.MinPurchaseAmount > sum)
			{
				return 0;
			}

			// (Order sum + Arrangement fee) / months + (Order sum * Interest / 12) + Administration fee
			decimal interestRate = paymentType.InterestRate / 100m;
			decimal monthlyCost = (sum + paymentType.RegistrationFee) / paymentType.NoOfMonths + (sum * interestRate / paymentType.NoOfMonths) + paymentType.SettlementFee;

			decimal lowestPayment = GetLowestPayment(paymentType.ChannelId);

			if (monthlyCost < lowestPayment)
			{
				monthlyCost = lowestPayment;
			}

			//The amount to pay per month should be rounded up to the nearest integer.
			return Math.Ceiling(monthlyCost);
		}

		public virtual decimal CalculateMonthlyCostForAccount(decimal sum, IQliroPaymentType paymentType)
		{
			if (paymentType.InterestCalculation != QliroInterestCalculationType.Flat)
			{
				return 0;
			}

			decimal settlementFee = GetAccountSettlementFee(paymentType.ChannelId);
			int noOfMonths = GetAccountNoOfMonths(paymentType.ChannelId);

			if (noOfMonths == 0)
			{
				return 0;
			}

			// (Order sum + Administration fee + Order sum * Interest / 12) / months
			// Or LowestPayment
			decimal interestRate = paymentType.InterestRate / 100m;
			decimal monthlyCost = (sum + settlementFee + sum * interestRate / 12) / noOfMonths;

			decimal lowestPayment = GetLowestPayment(paymentType.ChannelId);

			if (monthlyCost < lowestPayment)
			{
				monthlyCost = lowestPayment;
			}

			//The amount to pay per month should be rounded up to the nearest integer.
			return Math.Ceiling(monthlyCost);
		}


		protected virtual decimal GetLowestPayment(int channelId)
		{
			if (channelId == 1) // SE
			{
				return 50m;
			}

			if (channelId == 4) // FI
			{
				return 9m;
			}

			return 0m;
		}

		protected virtual decimal GetAccountSettlementFee(int channelId)
		{
			if (channelId == 1) // SE
			{
				return 29m;
			}

			if (channelId == 4) // FI
			{
				return 3.95m;
			}

			return 0m;
		}

		protected virtual int GetAccountNoOfMonths(int channelId)
		{
			if (channelId == 1) // SE
			{
				return 24;
			}

			if (channelId == 4) // FI
			{
				return 24;
			}

			return 0;
		}
	}
}