﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro.Repository
{
	public class QliroPendingOrderRepository
	{
		protected virtual DataMapperBase<IQliroPendingOrder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IQliroPendingOrder>(dataReader);
		}

		public virtual int Insert(IQliroPendingOrder qliroPendingOrder)
		{
			var dbSettings = new DatabaseSetting("QliroPendingOrderRepository.Insert");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", qliroPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", qliroPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", qliroPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", qliroPendingOrder.LastAttempt, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pQliroPendingOrderInsert]", parameters, dbSettings);
		}

		public virtual void Update(IQliroPendingOrder qliroPendingOrder)
		{
			var dbSettings = new DatabaseSetting("QliroPendingOrderRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("QliroPendingOrderId", qliroPendingOrder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", qliroPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", qliroPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", qliroPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", qliroPendingOrder.LastAttempt, SqlDbType.DateTime)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pQliroPendingOrderUpdate]", parameters, dbSettings);
		}

		public virtual void Delete(int qliroPendingOrderId)
		{
			var dbSettings = new DatabaseSetting("QliroPendingOrderRepository.Delete");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("QliroPendingOrderId", qliroPendingOrderId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pQliroPendingOrderDelete]", parameters, dbSettings);
		}

		public virtual Collection<IQliroPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			var dbSettings = new DatabaseSetting("QliroPendingOrderRepository.GetPendingOrdersForStatusCheck");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CheckToDate", checkToDate, SqlDbType.DateTime)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pQliroPendingOrderGetPendingOrdersForStatusCheck]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
