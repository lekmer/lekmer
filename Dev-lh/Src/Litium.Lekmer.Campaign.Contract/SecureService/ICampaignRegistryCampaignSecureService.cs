﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignRegistryCampaignSecureService
	{
		Collection<ICampaignRegistryCampaign> GetRegistriesByCampaignId(int campaignId);
		void Save(int campaignId, Collection<int> campaignRegistries);
		void Delete(int campaignId);
	}
}