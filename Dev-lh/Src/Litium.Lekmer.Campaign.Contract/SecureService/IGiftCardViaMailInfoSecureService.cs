﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface IGiftCardViaMailInfoSecureService
	{
		Collection<IGiftCardViaMailInfo> GetAllByOrder(int orderId);
		void DeleteByOrder(ISystemUserFull systemUserFull, int orderId);
	}
}