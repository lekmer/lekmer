﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface IFixedDiscountActionService
	{
		Collection<int> GetProductIds(int productActionId);
	}
}