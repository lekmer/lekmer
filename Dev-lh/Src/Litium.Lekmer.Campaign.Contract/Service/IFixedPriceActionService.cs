﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface IFixedPriceActionService
	{
		Collection<int> GetProductIds(int productActionId);
	}
}