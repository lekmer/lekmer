﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface IGiftCardViaMailProductActionService
	{
		Collection<int> GetProductIds(int productActionId);
	}
}