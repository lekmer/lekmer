﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	public interface IGiftCardViaMailInfoService
	{
		IGiftCardViaMailInfo Create(int orderId, int campaignId, int actionId, DateTime dateToSend, decimal discountValue);
		void Save(IGiftCardViaMailInfo giftCardViaMailInfo);
		void Save(IUserContext context, IOrderFull order);
		void Update(IGiftCardViaMailInfo giftCardViaMailInfo);
		Collection<IGiftCardViaMailInfo> GetAllToSend();
		Collection<IGiftCardViaMailInfo> GetAllByOrder(int orderId);
	}
}