﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface IFixedDiscountAction : ILekmerProductAction
	{
		bool IncludeAllProducts { get; set; }
		LekmerCurrencyValueDictionary Amounts { get; set; }

		ProductIdDictionary IncludeProducts { get; set; }
		ProductIdDictionary ExcludeProducts { get; set; }

		CategoryIdDictionary IncludeCategories { get; set; }
		CategoryIdDictionary ExcludeCategories { get; set; }

		BrandIdDictionary IncludeBrands { get; set; }
		BrandIdDictionary ExcludeBrands { get; set; }
	}
}