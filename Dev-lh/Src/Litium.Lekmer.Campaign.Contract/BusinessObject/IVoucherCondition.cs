﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface IVoucherCondition : ICondition
	{
		bool IncludeAllBatchIds { get; set; }
		Collection<int> BatchIdsInclude { get; set; }
		Collection<int> BatchIdsExclude { get; set; }
	}
}