﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignLevel : IBusinessObjectBase
	{
		int Id { get; set; }

		string Title { get; set; }

		int Priority { get; set; }
	}
}