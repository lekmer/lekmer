﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCampaign : ICampaign
	{
		int LevelId { get; set; }
		bool UseLandingPage { get; set; }
		int? PriceTypeId { get; set; }
		int? TagId { get; set; }
	}
}