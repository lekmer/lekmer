namespace Litium.Lekmer.Campaign
{
	public interface IFixedDiscountCartAction : ILekmerCartAction
	{
		LekmerCurrencyValueDictionary Amounts { get; set; }
	}
}