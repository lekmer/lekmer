﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICartActionTargetProductType : IBusinessObjectBase
	{
		int CartActionId { get; set; }
		int ProductTypeId { get; set; }
	}
}