﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface IGiftCardViaMailInfo : IBusinessObjectBase
	{
		int Id { get; set; }
		int OrderId { get; set; }
		int CampaignId { get; set; }
		int ActionId { get; set; }
		decimal DiscountValue { get; set; }
		DateTime DateToSend { get; set; }
		int StatusId { get; set; }
		int ChannelId { get; set; }
		int? VoucherInfoId { get; set; }
		string VoucherCode { get; set; }
	}
}