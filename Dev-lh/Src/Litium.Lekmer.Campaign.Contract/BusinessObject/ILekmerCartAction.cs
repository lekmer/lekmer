﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCartAction : ICartAction
	{
		IdDictionary TargetProductTypes { get; set; }
		Price ApplyDiscount(Price priceSummary, ICurrency currency);
	}
}