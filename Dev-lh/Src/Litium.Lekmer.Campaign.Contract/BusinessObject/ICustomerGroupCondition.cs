﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ICustomerGroupCondition : ICondition
	{
		CustomerGroupIdDictionary IncludeCustomerGroups { get; set; }
		CustomerGroupIdDictionary ExcludeCustomerGroups { get; set; }
	}
}