﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCartCampaign : ICartCampaign, ILekmerCampaign
	{
	}
}