﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface IProductActionTargetProductType : IBusinessObjectBase
	{
		int ProductActionId { get; set; }
		int ProductTypeId { get; set; }
	}
}