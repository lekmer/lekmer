﻿namespace Litium.Lekmer.Campaign
{
	public interface IGiftCardViaMailCartAction : ILekmerCartAction
	{
		int SendingInterval { get; set; }
		int? TemplateId { get; set; }
		LekmerCurrencyValueDictionary Amounts { get; set; }
	}
}