﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ICartValueRangeCondition : ICondition
	{
		Collection<CurrencyValueRange> CartValueRangeList { get; set; }
	}
}