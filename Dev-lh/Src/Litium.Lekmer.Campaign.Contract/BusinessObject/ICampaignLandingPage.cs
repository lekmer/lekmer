﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignLandingPage : IBusinessObjectBase
	{
		int CampaignLandingPageId { get; set; }
		int CampaignId { get; set; }
		string WebTitle { get; set; }
		string Description { get; set; }

		Collection<ICampaignRegistryCampaignLandingPage> RegistryDataList { get; set; }
	}
}