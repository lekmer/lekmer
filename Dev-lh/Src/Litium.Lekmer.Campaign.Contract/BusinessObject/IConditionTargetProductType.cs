﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface IConditionTargetProductType : IBusinessObjectBase
	{
		int ConditionId { get; set; }
		int ProductTypeId { get; set; }
	}
}