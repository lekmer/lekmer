﻿namespace Litium.Lekmer.Oculus
{
	public enum PriceRangeType
	{
		None = 0,
		All = 1,
		Lower = 2,
		LowerOrSame = 3,
		Higher = 4,
		HigherOrSame = 5
	}
}