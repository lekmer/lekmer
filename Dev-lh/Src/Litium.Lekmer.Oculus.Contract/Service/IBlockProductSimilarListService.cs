using Litium.Scensum.Core;

namespace Litium.Lekmer.Oculus
{
	public interface IBlockProductSimilarListService
	{
		IBlockProductSimilarList GetById(IUserContext context, int id);
	}
}