﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Oculus
{
	public interface IProductSimilar
	{
		int ProductId { get; set; }
		string ProductErpId { get; set; }
		Collection<ISimilarProductItem> SimilarProductCollection { get; set; }
	}
}