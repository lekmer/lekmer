﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class EsalesRecommendationTypeRepository
	{
		protected virtual DataMapperBase<IEsalesRecommendationType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IEsalesRecommendationType>(dataReader);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IEsalesRecommendationType> GetAll()
		{
			var dbSetting = new DatabaseSetting("EsalesRecommendationTypeRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pEsalesRecommendationTypeGetAll]", dbSetting))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}