﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesTagInfoList : IEsalesTagInfoList
	{
		private readonly Dictionary<int, IEsalesTagInfo> _tagInfoDict = new Dictionary<int, IEsalesTagInfo>();

		public void Add(IChannel channel, Collection<ITag> tagCollection)
		{
			foreach (ITag tag in tagCollection)
			{
				if (!_tagInfoDict.ContainsKey(tag.Id))
				{
					var tagInfo = IoC.Resolve<IEsalesTagInfo>();

					tagInfo.TagId = tag.Id;
					_tagInfoDict[tag.Id] = tagInfo;
				}

				_tagInfoDict[tag.Id].Add(channel, tag);
			}
		}

		public Collection<IEsalesTagInfo> GetEsalesTagInfoCollection()
		{
			var esalesTagInfoCollection = new Collection<IEsalesTagInfo>();

			esalesTagInfoCollection.AddRange(_tagInfoDict.Values);

			return esalesTagInfoCollection;
		}
	}
}