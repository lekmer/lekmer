﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesAds : BlockBase, IBlockEsalesAds
	{
		private string _panelPath;
		private string _format;
		private string _gender;
		private string _productCategory;
		private IBlockSetting _setting;

		public string PanelPath
		{
			get { return _panelPath; }
			set
			{
				CheckChanged(_panelPath, value);
				_panelPath = value;
			}
		}

		public string Format
		{
			get { return _format; }
			set
			{
				CheckChanged(_format, value);
				_format = value;
			}
		}

		public string Gender
		{
			get { return _gender; }
			set
			{
				CheckChanged(_gender, value);
				_gender = value;
			}
		}

		public string ProductCategory
		{
			get { return _productCategory; }
			set
			{
				CheckChanged(_productCategory, value);
				_productCategory = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}
