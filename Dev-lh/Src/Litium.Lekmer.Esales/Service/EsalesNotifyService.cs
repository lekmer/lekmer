﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Apptus.ESales.Connector;
using Litium.Lekmer.Esales.Connector;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Esales
{
	public class EsalesNotifyService : IEsalesNotifyService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IEsalesConnector EsalesConnector { get; private set; }

		public EsalesNotifyService(IEsalesConnector connector)
		{
			EsalesConnector = connector;
		}


		public virtual void NotifyCustomerKey(string value)
		{
			NotifyProperty(EsalesXmlNodes.NodeCustomerKey, value);
		}

		public virtual void NotifyProperty(string name, string value)
		{
			try
			{
				Session eSalesSession = EsalesConnector.EsalesSession;

				eSalesSession.NotifyProperty(name, value);
			}
			catch (Exception ex)
			{
				_log.Error("Esales failed.", ex);
			}
		}

		public virtual void NotifyPayment(IOrderFull order)
		{
			try
			{
				var esalesOrder = new Apptus.ESales.Connector.Order();

				Collection<IOrderItem> orderItems = order.GetOrderItems();
				foreach (IOrderItem orderItem in orderItems)
				{
					Apptus.ESales.Connector.Order.Line esalesOrderLine = esalesOrder.AddProduct(orderItem.ProductId.ToString(CultureInfo.InvariantCulture));

					esalesOrderLine.PutQuantity(orderItem.Quantity);
					esalesOrderLine.PutUnitCost((double)orderItem.OriginalPrice.IncludingVat);
					esalesOrderLine.PutUnitSellingPrice((double)orderItem.ActualPrice.IncludingVat);
				}

				Session eSalesSession = EsalesConnector.EsalesSession;
				eSalesSession.NotifyPayment(esalesOrder);
			}
			catch (Exception ex)
			{
				_log.Error("Esales failed.", ex);
			}
		}

		public virtual void NotifySessionEnd()
		{
			try
			{
				Session eSalesSession = EsalesConnector.EsalesSession;

				eSalesSession.End();
			}
			catch (Exception ex)
			{
				_log.Error("Esales failed.", ex);
			}
		}
	}
}
