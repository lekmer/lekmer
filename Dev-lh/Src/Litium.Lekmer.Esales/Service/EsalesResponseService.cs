﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public class EsalesResponseService : IEsalesResponseService
	{
		public virtual IEsalesResponse ConvertToSearchResponse(Apptus.ESales.Connector.PanelContent panelContent)
		{
			var searchResponse = IoC.Resolve<IEsalesResponse>();

			ReadResult(panelContent, searchResponse);

			return searchResponse;
		}


		protected virtual void ReadResult(Apptus.ESales.Connector.PanelContent pc, IPanelContent resultPanelContent)
		{
			Convert(pc, resultPanelContent);

			//panel has been associated with a "result_type" in the configuration
			//IDictionary<string, string> attributes = pc.Attributes;
			if (pc.IsZone)
			{
				//string heading = pc.Attributes["heading"];//fetch panel attribute
				foreach (Apptus.ESales.Connector.PanelContent child in pc.Subpanels)
				{
					var subPanel = IoC.Resolve<IPanelContent>();
					resultPanelContent.SubPanels.Add(subPanel);

					ReadResult(child, subPanel);
				}
			}
			else
			{
				Apptus.ESales.Connector.Result result = pc.Result();
				if (result != null)
				{
					Apptus.ESales.Connector.Result.ResultType type = result.Type;
					switch (type)
					{
						case Apptus.ESales.Connector.Result.ResultType.Products:
							ReadProducts((Apptus.ESales.Connector.Result.Products) result, resultPanelContent);
							break;
						case Apptus.ESales.Connector.Result.ResultType.Completions:
							ReadCompletions((Apptus.ESales.Connector.Result.Completions) result, resultPanelContent);
							break;
						case Apptus.ESales.Connector.Result.ResultType.Corrections:
							ReadCorrections((Apptus.ESales.Connector.Result.Corrections)result, resultPanelContent);
							break;
						case Apptus.ESales.Connector.Result.ResultType.Count:
							ReadCount((Apptus.ESales.Connector.Result.Count) result, resultPanelContent);
							break;
						case Apptus.ESales.Connector.Result.ResultType.Values:
							ReadValues((Apptus.ESales.Connector.Result.Values)result, resultPanelContent);
							break;
						case Apptus.ESales.Connector.Result.ResultType.Ads:
							ReadAds((Apptus.ESales.Connector.Result.Ads)result, resultPanelContent);
							break;
						case Apptus.ESales.Connector.Result.ResultType.Phrases:
							ReadPhrases((Apptus.ESales.Connector.Result.Phrases)result, resultPanelContent);
							break;
						default:
							throw new InvalidOperationException("Unknown Type: " + type);
					}
				}
			}
		}


		protected virtual void ReadProducts(Apptus.ESales.Connector.Result.Products products, IPanelContent resultPanelContent)
		{
			var productHits = IoC.Resolve<IProductHits>();
			productHits.ProductsInfo = new Collection<IProductHitInfo>();

			//iterate through the result
			foreach (Apptus.ESales.Connector.Result.Product product in products)
			{
				var productHitInfo = IoC.Resolve<IProductHitInfo>();

				productHitInfo.Rank = int.Parse(product.GetAttribute("rank").Value);
				productHitInfo.Key = product.Key;
				productHitInfo.Ticket = product.Ticket;

				productHits.ProductsInfo.Add(productHitInfo);

				//foreach (Apptus.ESales.Connector.Result.Attribute attribute in product)
				//{
				//    string productInfo = attribute.Name + " : " + attribute.Value;
				//}
			}

			resultPanelContent.Result = productHits;
		}

		protected virtual void ReadValues(Apptus.ESales.Connector.Result.Values values, IPanelContent resultPanelContent)
		{
			var valueHits = IoC.Resolve<IValueHits>();
			valueHits.ValuesInfo = new Collection<IValueInfo>();

			//iterate through the result
			foreach (Apptus.ESales.Connector.Result.Value value in values)
			{
				var valueInfo = IoC.Resolve<IValueInfo>();

				valueInfo.Text = value.Text;
				valueInfo.Count = value.Count;
				valueInfo.Ticket = value.Ticket;

				valueHits.ValuesInfo.Add(valueInfo);
			}

			resultPanelContent.Result = valueHits;
		}

		protected virtual void ReadCount(Apptus.ESales.Connector.Result.Count result, IPanelContent resultPanelContent)
		{
			var countHits = IoC.Resolve<ICountHits>();

			countHits.TotalHitCount = result.GetCount();

			resultPanelContent.Result = countHits;
		}

		protected virtual void ReadCompletions(Apptus.ESales.Connector.Result.Completions result, IPanelContent resultPanelContent)
		{
			var completionHits = IoC.Resolve<ICompletionHits>();
			completionHits.CompletionsInfo = new Collection<ICompletionInfo>();

			foreach (Apptus.ESales.Connector.Result.Completion c in result)
			{
				var completionInfo = IoC.Resolve<ICompletionInfo>();

				completionInfo.Rank = c.Rank;
				completionInfo.Relevance = c.Relevance;
				completionInfo.Text = c.Text;
				completionInfo.Ticket = c.Ticket;

				completionHits.CompletionsInfo.Add(completionInfo);
			}

			resultPanelContent.Result = completionHits;
		}

		protected virtual void ReadCorrections(Apptus.ESales.Connector.Result.Corrections result, IPanelContent resultPanelContent)
		{
			var correctionHits = IoC.Resolve<ICorrectionHits>();
			correctionHits.CorrectionsInfo = new Collection<ICorrectionInfo>();

			foreach (Apptus.ESales.Connector.Result.Correction c in result)
			{
				var correctionInfo = IoC.Resolve<ICorrectionInfo>();

				correctionInfo.Rank = c.Rank;
				correctionInfo.Relevance = c.Relevance;
				correctionInfo.Text = c.Text;
				correctionInfo.Ticket = c.Ticket;

				correctionHits.CorrectionsInfo.Add(correctionInfo);
			}

			resultPanelContent.Result = correctionHits;
		}

		protected virtual void ReadAds(Apptus.ESales.Connector.Result.Ads result, IPanelContent resultPanelContent)
		{
			var adHits = IoC.Resolve<IAdHits>();
			adHits.AdsInfo = new Collection<IAdInfo>();

			foreach (Apptus.ESales.Connector.Result.Ad ad in result)
			{
				var adInfo = IoC.Resolve<IAdInfo>();

				adInfo.Key = ad.Key;
				adInfo.Ticket = ad.Ticket;
				adInfo.CampaignKey = ad.CampaignKey;

				adInfo.Format = ad.GetValue("format");
				adInfo.Site = ad.GetValue("site");
				adInfo.Gender = ad.GetValue("gender");
				adInfo.ProductCategory = ad.GetValue("product_category");
				adInfo.Title = ad.GetValue("title");
				adInfo.LandingPageUrl = ad.GetValue("landing_page_url");
				adInfo.ImageUrl = ad.GetValue("image_url");
				adInfo.Tags = ad.GetValue("tags");

				adHits.AdsInfo.Add(adInfo);
			}

			resultPanelContent.Result = adHits;
		}

		protected virtual void ReadPhrases(Apptus.ESales.Connector.Result.Phrases result, IPanelContent resultPanelContent)
		{
			var phraseHits = IoC.Resolve<IPhraseHits>();
			phraseHits.PhrasesInfo = new Collection<IPhraseInfo>();

			foreach (Apptus.ESales.Connector.Result.Phrase p in result)
			{
				var phraseInfo = IoC.Resolve<IPhraseInfo>();

				phraseInfo.Rank = p.Rank;
				phraseInfo.Relevance = p.Relevance;
				phraseInfo.Text = p.Text;
				phraseInfo.Ticket = p.Ticket;

				phraseHits.PhrasesInfo.Add(phraseInfo);
			}

			resultPanelContent.Result = phraseHits;
		}

		protected virtual void Convert(Apptus.ESales.Connector.PanelContent panelContent, IPanelContent resultPanelContent)
		{
			resultPanelContent.Name = panelContent.Attributes["display_name"];
			if (panelContent.Attributes.ContainsKey("description"))
			{
				resultPanelContent.Description = panelContent.Attributes["description"];
			}

			resultPanelContent.IsZone = panelContent.IsZone;
			resultPanelContent.HasRezult = panelContent.HasResult;

			resultPanelContent.SubPanels = new Collection<IPanelContent>();
		}
	}
}