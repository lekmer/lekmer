﻿using System;
using System.Reflection;
using System.Threading;
using Apptus.ESales.Connector;
using Litium.Lekmer.Esales.Connector;
using Litium.Lekmer.Esales.Setting;
using log4net;

namespace Litium.Lekmer.Esales
{
	public class EsalesService : IEsalesService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IEsalesSetting EsalesSetting { get; set; }

		protected IEsalesConnector EsalesConnector { get; private set; }

		protected IEsalesResponseService EsalesResponseService { get; private set; }

		public EsalesService(IEsalesSetting esalesSetting, IEsalesConnector connector, IEsalesResponseService responseService)
		{
			EsalesSetting = esalesSetting;
			EsalesConnector = connector;
			EsalesResponseService = responseService;
		}

		public virtual IEsalesResponse RetrieveContent(IEsalesRequest esalesRequest)
		{
			if (EsalesSetting.Enabled)
			{
				try
				{
					Session eSalesSession = EsalesConnector.EsalesSession;

					Panel p = eSalesSession.Panel(esalesRequest.PanelPath);

					var argMap = esalesRequest.ComposeArguments();

					Apptus.ESales.Connector.PanelContent retrieveContent = p.RetrieveContent(argMap);

					return EsalesResponseService.ConvertToSearchResponse(retrieveContent);
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					_log.Error("Esales failed.", ex);
				}
			}

			return new EsalesResponse();
		}

		public virtual IEsalesResponse RetrieveContent(IEsalesPanelRequest esalesPanelRequest)
		{
			if (EsalesSetting.Enabled)
			{
				try
				{
					Session eSalesSession = EsalesConnector.EsalesSession;

					Panel p = eSalesSession.Panel(esalesPanelRequest.PanelPath);

					var argMap = esalesPanelRequest.PanelArguments;

					Apptus.ESales.Connector.PanelContent retrieveContent = p.RetrieveContent(argMap);

					return EsalesResponseService.ConvertToSearchResponse(retrieveContent);
				}
				catch (ThreadAbortException)
				{
					throw;
				}
				catch (Exception ex)
				{
					_log.Error("Esales failed.", ex);
				}
			}

			return new EsalesResponse();
		}
	}
}