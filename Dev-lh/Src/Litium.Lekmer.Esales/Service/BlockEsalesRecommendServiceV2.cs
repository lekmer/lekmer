﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesRecommendServiceV2 : IBlockEsalesRecommendServiceV2
	{
		protected BlockEsalesRecommendRepositoryV2 Repository { get; private set; }

		public BlockEsalesRecommendServiceV2(BlockEsalesRecommendRepositoryV2 repository)
		{
			Repository = repository;
		}

		public virtual IBlockEsalesRecommendV2 GetById(IUserContext context, int blockId)
		{
			Repository.EnsureNotNull();

			return BlockEsalesRecommendCacheV2.Instance.TryGetItem(
				new BlockEsalesRecommendKeyV2(context.Channel.Id, blockId),
				() => GetByIdCore(context, blockId));
		}

		protected virtual IBlockEsalesRecommendV2 GetByIdCore(IUserContext context, int blockId)
		{
			return Repository.GetById(context.Channel, blockId);
		}
	}
}