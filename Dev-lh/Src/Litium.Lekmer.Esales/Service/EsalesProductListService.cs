﻿using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public class EsalesProductListService : IEsalesProductListService
	{
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }

		public EsalesProductListService(
			IEsalesProductKeyService esalesProductKeyService,
			IProductService productService)
		{
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = (ILekmerProductService)productService;
		}

		public virtual ProductCollection GetProducts(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent)
		{
			string panelName = esalesModelComponent.TryGetParameterValue("panel-name", "Recommendations");

			return FindProducts(context, panelName, esalesResponse);
		}

		protected virtual ProductCollection FindProducts(IUserContext context, string productPanelName, IEsalesResponse esalesResponse)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(productPanelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;
				var productsCountResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;

				if (productsResult != null)
				{
					var productIds = new ProductIdCollection(productsResult.ProductsInfo.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
					productIds.TotalCount = productsResult.ProductsInfo.Count;
					if (productsCountResult != null)
					{
						productIds.TotalCount = productsCountResult.TotalHitCount;
					}

					ProductCollection products = ProductService.PopulateViewProducts(context, productIds);

					Dictionary<int, string> tickets = productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

					foreach (IProduct product in products)
					{
						var lekmerProduct = (ILekmerProduct)product;
						lekmerProduct.EsalesTicket = tickets[product.Id];
					}

					return products;
				}
			}

			return new ProductCollection();
		}
	}
}