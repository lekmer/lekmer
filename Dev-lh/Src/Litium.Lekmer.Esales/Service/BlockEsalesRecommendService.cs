﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesRecommendService : IBlockEsalesRecommendService
	{
		protected BlockEsalesRecommendRepository Repository { get; private set; }

		public BlockEsalesRecommendService(BlockEsalesRecommendRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockEsalesRecommend GetById(IUserContext context, int blockId)
		{
			Repository.EnsureNotNull();

			return BlockEsalesRecommendCache.Instance.TryGetItem(
				new BlockEsalesRecommendKey(context.Channel.Id, blockId),
				() => GetByIdCore(context, blockId));
		}

		protected virtual IBlockEsalesRecommend GetByIdCore(IUserContext context, int blockId)
		{
			return Repository.GetById(context.Channel, blockId);
		}
	}
}