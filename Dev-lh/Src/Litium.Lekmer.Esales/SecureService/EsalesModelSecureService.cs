﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class EsalesModelSecureService : IEsalesModelSecureService
	{
		protected EsalesModelRepository Repository { get; private set; }

		public EsalesModelSecureService(EsalesModelRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IEsalesModelComponent> GetModelComponentsAllByBlockType(int blockTypeId)
		{
			return Repository.GetModelComponentsAllByBlockType(blockTypeId);
		}
	}
}