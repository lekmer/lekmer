using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesRecommendSecureService : IBlockEsalesRecommendSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockEsalesRecommendRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockEsalesRecommendSecureService(
			IAccessValidator accessValidator,
			BlockEsalesRecommendRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockEsalesRecommend Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockEsalesRecommend = IoC.Resolve<IBlockEsalesRecommend>();

			blockEsalesRecommend.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockEsalesRecommend.Setting = BlockSettingSecureService.Create();
			blockEsalesRecommend.Status = BusinessObjectStatus.New;

			return blockEsalesRecommend;
		}

		public virtual IBlockEsalesRecommend GetById(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetByIdSecure(blockId);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockEsalesRecommend blockEsalesRecommend = Create();

			blockEsalesRecommend.ContentNodeId = contentNodeId;
			blockEsalesRecommend.ContentAreaId = contentAreaId;
			blockEsalesRecommend.BlockTypeId = blockTypeId;
			blockEsalesRecommend.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockEsalesRecommend.Title = title;
			blockEsalesRecommend.TemplateId = null;
			blockEsalesRecommend.Id = Save(systemUserFull, blockEsalesRecommend);

			return blockEsalesRecommend;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockEsalesRecommend block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			BlockEsalesRecommendCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockEsalesRecommendCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}