﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public class AdSecureService : IAdSecureService
	{
		protected AdRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAdChannelSecureService AdChannelSecureService { get; private set; }

		public AdSecureService(AdRepository adRepository, IAccessValidator accessValidator, IAdChannelSecureService adChannelSecureService)
		{
			Repository = adRepository;
			AccessValidator = accessValidator;
			AdChannelSecureService = adChannelSecureService;
		}

		public virtual IAd Create()
		{
			var ad = IoC.Resolve<IAd>();
			ad.Status = BusinessObjectStatus.New;
			return ad;
		}

		public virtual IAd Save(ISystemUserFull systemUserFull, IAd ad)
		{
			if (ad == null)
			{
				throw new ArgumentNullException("ad");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			AdChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.CampaignAds);

			using (var transaction = new TransactedOperation())
			{
				ad.Id = Repository.Save(ad);

				foreach (var adChannel in ad.AdChannelList)
				{
					adChannel.AdId = ad.Id;
					AdChannelSecureService.Save(systemUserFull, adChannel);
				}

				transaction.Complete();
			}

			return ad;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int adId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			AdChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				AdChannelSecureService.DeleteAllByAd(systemUserFull, adId);

				Repository.Delete(adId);

				transaction.Complete();
			}
		}

		public virtual IAd GetById(int adId)
		{
			Repository.EnsureNotNull();
			AdChannelSecureService.EnsureNotNull();

			IAd ad = Repository.GetById(adId);

			if (ad != null)
			{
				ad.AdChannelList = AdChannelSecureService.GetAllByAd(adId);
			}

			return ad;
		}

		public virtual Collection<IAd> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}

		public virtual Collection<IAd> GetAllByFolder(int folderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByFolder(folderId);
		}

		public virtual bool IsUniqueKey(int? adId, string key)
		{
			Repository.EnsureNotNull();

			return Repository.IsUniqueKey(adId, key);
		}

		public Collection<IAd> Search(string searchCriteria)
		{
			throw new NotImplementedException();
		}
	}
}