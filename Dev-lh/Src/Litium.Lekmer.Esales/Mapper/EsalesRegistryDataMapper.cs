﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class EsalesRegistryDataMapper : DataMapperBase<IEsalesRegistry>
	{
		public EsalesRegistryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IEsalesRegistry Create()
		{
			var esalesRegistry = IoC.Resolve<IEsalesRegistry>();

			esalesRegistry.Id = MapValue<int>("EsalesRegistry.EsalesRegistryId");
			esalesRegistry.CommonName = MapValue<string>("EsalesRegistry.CommonName");
			esalesRegistry.Title = MapValue<string>("EsalesRegistry.Title");

			esalesRegistry.ChannelId = MapNullableValue<int?>("EsalesRegistry.ChannelId");
			esalesRegistry.SiteStructureRegistryId = MapNullableValue<int?>("EsalesRegistry.SiteStructureRegistryId");

			esalesRegistry.SetUntouched();

			return esalesRegistry;
		}
	}
}