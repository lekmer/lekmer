﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesAdsDataMapper : DataMapperBase<IBlockEsalesAds>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockEsalesAdsDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockEsalesAds Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockEsalesRecommend = IoC.Resolve<IBlockEsalesAds>();

			block.ConvertTo(blockEsalesRecommend);

			blockEsalesRecommend.PanelPath = MapNullableValue<string>("BlockEsalesAds.PanelPath");
			blockEsalesRecommend.Format = MapNullableValue<string>("BlockEsalesAds.Format");
			blockEsalesRecommend.Gender = MapNullableValue<string>("BlockEsalesAds.Gender");
			blockEsalesRecommend.ProductCategory = MapNullableValue<string>("BlockEsalesAds.ProductCategory");
			blockEsalesRecommend.Setting = _blockSettingDataMapper.MapRow();

			blockEsalesRecommend.SetUntouched();

			return blockEsalesRecommend;
		}
	}
}