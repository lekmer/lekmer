﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class EsalesModelParameterDataMapper : DataMapperBase<IEsalesModelParameter>
	{
		public EsalesModelParameterDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IEsalesModelParameter Create()
		{
			var esalesModelParameter = IoC.Resolve<IEsalesModelParameter>();

			esalesModelParameter.Name = MapValue<string>("EsalesModelParameter.Name");
			esalesModelParameter.Value = MapValue<string>("EsalesModelParameter.Value");

			return esalesModelParameter;
		}
	}
}
