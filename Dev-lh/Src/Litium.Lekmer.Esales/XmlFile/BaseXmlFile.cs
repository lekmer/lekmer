﻿using System;
using System.Globalization;
using System.Xml;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Esales
{
	public class BaseXmlFile
	{
		protected virtual XmlDocument CreateXmlDocument()
		{
			var doc = new XmlDocument();

			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);

			return doc;
		}


		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName)
		{
			XmlNode node = xmlDocument.CreateElement(elementName);
			parentNode.AppendChild(node);
			return node;
		}

		protected virtual XmlNode AddNodeRaw(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlNode node = xmlDocument.CreateElement(elementName);
			node.AppendChild(xmlDocument.CreateTextNode(elementValue));
			parentNode.AppendChild(node);
			return node;
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			return AddNodeRaw(xmlDocument, parentNode, elementName, elementValue.StripIllegalXmlCharacters());
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, int elementValue)
		{
			return AddNodeRaw(xmlDocument, parentNode, elementName, elementValue.ToString(CultureInfo.InvariantCulture));
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, bool elementValue)
		{
			return AddNodeRaw(xmlDocument, parentNode, elementName, elementValue ? EsalesXmlNodes.ValueTrue : EsalesXmlNodes.ValueFalse);
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, DateTime? elementValue)
		{
			string nodeValue = elementValue.HasValue ? elementValue.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) : string.Empty;
			return AddNodeRaw(xmlDocument, parentNode, elementName, nodeValue);
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, decimal elementValue)
		{
			return AddNodeRaw(xmlDocument, parentNode, elementName, elementValue.ToString("0.00", CultureInfo.InvariantCulture));
		}


		protected virtual void AddAttributeRaw(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlAttribute attribute = xmlDocument.CreateAttribute(elementName);
			attribute.Value = elementValue;
			if (parentNode.Attributes != null)
			{
				parentNode.Attributes.Append(attribute);
			}
		}

		protected virtual void AddAttribute(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			AddAttributeRaw(xmlDocument, parentNode, elementName, elementValue.StripIllegalXmlCharacters());
		}
	}
}
