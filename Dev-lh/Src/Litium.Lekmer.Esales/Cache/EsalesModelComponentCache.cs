﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales
{
	public sealed class EsalesModelComponentCache : ScensumCacheBase<EsalesModelComponentKey, IEsalesModelComponent>
	{
		private static readonly EsalesModelComponentCache _instance = new EsalesModelComponentCache();

		private EsalesModelComponentCache()
		{
		}

		public static EsalesModelComponentCache Instance
		{
			get { return _instance; }
		}
	}

	public class EsalesModelComponentKey : ICacheKey
	{
		public EsalesModelComponentKey(int siteStructureRegistryId, int modelComponentById)
		{
			SiteStructureRegistryId = siteStructureRegistryId;
			ModelComponentById = modelComponentById;
		}

		public int SiteStructureRegistryId { get; set; }
		public int ModelComponentById { get; set; }

		public string Key
		{
			get { return SiteStructureRegistryId + "-" + ModelComponentById; }
		}
	}
}