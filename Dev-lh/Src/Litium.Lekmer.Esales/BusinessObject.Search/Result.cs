using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public abstract class Result : IResult
	{
		public abstract EsalesResultType Type { get; }
	}
}