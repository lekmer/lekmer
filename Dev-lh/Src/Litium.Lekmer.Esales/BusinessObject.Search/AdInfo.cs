using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class AdInfo : IAdInfo
	{
		public string Key { get; set; }
		public string Ticket { get; set; }
		public string CampaignKey { get; set; }

		public string Format { get; set; }
		public string Site { get; set; }
		public string Gender { get; set; }
		public string ProductCategory { get; set; }
		public string Title { get; set; }
		public string LandingPageUrl { get; set; }
		public string ImageUrl { get; set; }
		public string Tags { get; set; }
	}
}