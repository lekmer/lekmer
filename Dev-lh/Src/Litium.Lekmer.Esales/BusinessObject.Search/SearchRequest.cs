using System;
using System.Collections.Generic;
using System.Globalization;
using Apptus.ESales.Connector;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	[Serializable]
	public class SearchRequest : EsalesRequest, ISearchRequest
	{
		/// <summary>
		/// The text to search for. The value can be a single word or a phrase.
		/// </summary>
		public virtual string SearchPhrase { get; set; }

		/// <summary>
		/// The text to filter for. The value can be a single word or a phrase.
		/// </summary>
		public virtual string FilterPhrase { get; set; }

		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		public virtual IPaging Paging { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		/// <returns></returns>
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();

			if (SearchPhrase.HasValue())
			{
				argMap[EsalesArguments.SearchPhrase] = SearchPhrase;
			}

			if (FilterPhrase.HasValue())
			{
				argMap[EsalesArguments.FilterPhrase] = FilterPhrase;
			}

			argMap.Add(EsalesArguments.WindowFirst, Paging.FirstItemNumber.ToString(CultureInfo.InvariantCulture));
			argMap.Add(EsalesArguments.WindowLast, Paging.LastItemNumber.ToString(CultureInfo.InvariantCulture));

			return argMap;
		}
	}
}