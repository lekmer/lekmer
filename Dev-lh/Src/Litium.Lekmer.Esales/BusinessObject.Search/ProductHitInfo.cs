using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class ProductHitInfo : IProductHitInfo
	{
		public int Rank { get; set; }
		public string Key { get; set; }
		public string Ticket { get; set; }
	}
}