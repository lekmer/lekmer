using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class PhraseHits : Result, IPhraseHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Phrases; }
		}

		public Collection<IPhraseInfo> PhrasesInfo { get; set; }
	}
}