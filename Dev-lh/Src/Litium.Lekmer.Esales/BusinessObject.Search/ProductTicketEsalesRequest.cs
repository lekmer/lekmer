using System;
using System.Collections.Generic;
using Apptus.ESales.Connector;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class ProductTicketEsalesRequest : EsalesRequest, IProductTicketEsalesRequest
	{
		/// <summary>
		/// A comma separated list of product keys of the products that should be listed.
		/// </summary>
		public string Products { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		/// <returns></returns>
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();

			argMap[EsalesArguments.Products] = Products;

			return argMap;
		}
	}
}