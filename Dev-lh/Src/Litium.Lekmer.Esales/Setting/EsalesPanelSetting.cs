﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Esales.Setting
{
	public abstract class EsalesPanelSetting : SettingBase, IEsalesPanelSetting
	{
		protected string ChannelCommonName { get; set; }

		protected override string StorageName
		{
			get { return "EsalesPanels"; }
		}

		protected abstract string GroupName { get; }

		protected EsalesPanelSetting(string channelCommonName)
		{
			ChannelCommonName = channelCommonName;
		}

		public virtual string PanelPath
		{
			get { return GetString(GroupName, "PanelPath"); }
		}
	}
}
