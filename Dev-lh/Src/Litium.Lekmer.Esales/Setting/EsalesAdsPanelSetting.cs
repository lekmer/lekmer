﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Esales.Setting
{
	public class EsalesAdsPanelSetting : SettingBase, IEsalesAdsPanelSetting
	{
		protected override string StorageName
		{
			get { return "EsalesPanels"; }
		}

		protected virtual string GroupName
		{
			get { return "AdsPanel"; }
		}

		public string PanelPath
		{
			get { return GetString(GroupName, "PanelPath"); }
		}
	}
}
