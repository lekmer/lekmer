﻿namespace Litium.Lekmer.Esales.Setting
{
	public class EsalesCartPanelSetting : EsalesPanelSettingV2 
	{
		public EsalesCartPanelSetting(string channelCommonName) : base(channelCommonName)
		{
		}

		protected override string GroupName
		{
			get { return string.Format("CartPagePanel-{0}",  ChannelCommonName); }
		}

		public virtual string FallbackPanelPath
		{
			get { return GetString(GroupName, "FallbackPanelPath"); }
		}
	}
}
