﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchProductPanelSettingV2 : SearchFacetPanelSettingV2, ISearchProductPanelSettingV2
	{
		protected override string GroupName
		{
			get { return "SearchProductPanel-" + ChannelCommonName; }
		}

		public SearchProductPanelSettingV2(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public virtual string ProductCountPanelName
		{
			get { return GetString(GroupName, "ProductCountPanelName"); }
		}
		public virtual string ProductPanelName
		{
			get { return GetString(GroupName, "ProductPanelName"); }
		}
		public virtual string FacetsFilterPanelName
		{
			get { return GetString(GroupName, "FacetsFilterPanelName"); }
		}
		public virtual string FacetsAllPanelName
		{
			get { return GetString(GroupName, "FacetsAllPanelName"); }
		}
		public virtual string DidYouMeanPanelName
		{
			get { return GetString(GroupName, "DidYouMeanPanelName"); }
		}
		public virtual string TopSearchesPanelName
		{
			get { return GetString(GroupName, "TopSearchesPanelName"); }
		}
	}
}