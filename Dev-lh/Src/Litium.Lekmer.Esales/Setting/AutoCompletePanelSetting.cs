﻿namespace Litium.Lekmer.Esales.Setting
{
	public class AutoCompletePanelSetting : EsalesPanelSetting, IAutoCompletePanelSetting
	{
		protected override string GroupName
		{
			get { return "AutoCompletePanel-" + ChannelCommonName; }
		}

		public AutoCompletePanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string AutoCompletePanelName
		{
			get { return GetString(GroupName, "AutoCompletePanelName"); }
		}
	}
}
