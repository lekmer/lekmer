﻿namespace Litium.Lekmer.Esales.Setting
{
	public class ProductPagePanelSettingV2 : EsalesPanelSettingV2, IProductPagePanelSettingV2
	{
		protected override string GroupName
		{
			get { return "ProductPagePanel-" + ChannelCommonName; }
		}

		public ProductPagePanelSettingV2(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string ProductInformationPanelName
		{
			get { return GetString(GroupName, "ProductInformationPanelName"); }
		}
	}
}
