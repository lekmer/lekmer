﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchIncrementalProductFilterPanelSetting : SearchIncrementalAutocompletePanelSetting, ISearchIncrementalProductFilterPanelSetting
	{
		public SearchIncrementalProductFilterPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public override string PanelPath
		{
			get { return GetString(GroupName, "PanelPathProductFilter"); }
		}

		public override string ProductPanelName
		{
			get { return GetString(GroupName, "ProductFilterPanelName"); }
		}
	}
}
