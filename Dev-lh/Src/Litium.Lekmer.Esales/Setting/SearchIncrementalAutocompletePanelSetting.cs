﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchIncrementalAutocompletePanelSetting : SearchProductPanelSetting, ISearchIncrementalAutocompletePanelSetting
	{
		protected override string GroupName
		{
			get { return "SearchIncrementalPanel-" + ChannelCommonName; }
		}

		public SearchIncrementalAutocompletePanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public override string PanelPath
		{
			get { return GetString(GroupName, "PanelPathAutocomplete"); }
		}
	}
}
