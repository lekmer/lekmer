﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchFacetFilterPanelSetting : SearchFacetPanelSetting, ISearchFacetFilterPanelSetting
	{
		public SearchFacetFilterPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public override string PanelPath
		{
			get { return GetString(GroupName, "PanelPathFilter"); }
		}
	}
}