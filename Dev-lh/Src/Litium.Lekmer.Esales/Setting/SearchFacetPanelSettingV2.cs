﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchFacetPanelSettingV2 : EsalesPanelSettingV2, ISearchFacetPanelSetting
	{
		protected override string GroupName
		{
			get { return string.Empty; }
		}

		public SearchFacetPanelSettingV2(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string BrandFilterPanelName
		{
			get { return GetString(GroupName, "BrandFilterPanelName"); }
		}
		public string MainCategoryFilterPanelName
		{
			get { return GetString(GroupName, "MainCategoryFilterPanelName"); }
		}
		public string ParentCategoryFilterPanelName
		{
			get { return GetString(GroupName, "ParentCategoryFilterPanelName"); }
		}
		public string CategoryFilterPanelName
		{
			get { return GetString(GroupName, "CategoryFilterPanelName"); }
		}
		public string AgeFromMonthFilterPanelName
		{
			get { return GetString(GroupName, "AgeFromMonthFilterPanelName"); }
		}
		public string AgeToMonthFilterPanelName
		{
			get { return GetString(GroupName, "AgeToMonthFilterPanelName"); }
		}
		public string PriceFilterPanelName
		{
			get { return GetString(GroupName, "PriceFilterPanelName"); }
		}
		public string SizeFilterPanelName
		{
			get { return GetString(GroupName, "SizeFilterPanelName"); }
		}
		public string TagFilterPanelName
		{
			get { return GetString(GroupName, "TagFilterPanelName"); }
		}
	}
}