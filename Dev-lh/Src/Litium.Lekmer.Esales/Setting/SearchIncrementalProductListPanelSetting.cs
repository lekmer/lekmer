﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchIncrementalProductListPanelSetting : SearchIncrementalProductFilterPanelSetting, ISearchIncrementalProductListPanelSetting
	{
		public SearchIncrementalProductListPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public override string PanelPath
		{
			get { return GetString(GroupName, "PanelPathProductList"); }
		}

		public override string ProductPanelName
		{
			get { return GetString(GroupName, "ProductListPanelName"); }
		}
	}
}
