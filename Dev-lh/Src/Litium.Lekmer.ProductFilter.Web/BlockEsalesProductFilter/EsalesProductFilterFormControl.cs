using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;

namespace Litium.Lekmer.ProductFilter.Web.BlockEsalesProductFilter
{
	public class EsalesProductFilterFormControl : ControlBase
	{
		private readonly Regex _tagGroupParameterRegex = new Regex(@"^taggroup(\d+)-tag-id$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		public Template Template { get; set; }

		public BlockFilterOptions BlockFilterOptions { get; set; }
		public FormFilterOptions FormFilterOptions { get; set; }

		public AvailableFilterOptions AvailableFilterOptions { get; set; }

		public IEsalesProductFilterFacet FilterStatistics { get; set; }

		public bool UseSecondaryTemplate { get; set; }

		public bool FilterMode { get; set; }

		public SortOption SortOption { get; set; }

		public int PageSize { get; set; }

		private IAgeIntervalService _ageIntervalService;
		public IAgeIntervalService AgeIntervalService
		{
			get { return _ageIntervalService ?? (_ageIntervalService = IoC.Resolve<IAgeIntervalService>()); }
			set { _ageIntervalService = value; }
		}

		private IPriceIntervalService _priceIntervalService;
		public IPriceIntervalService PriceIntervalService
		{
			get { return _priceIntervalService ?? (_priceIntervalService = IoC.Resolve<IPriceIntervalService>()); }
			set { _priceIntervalService = value; }
		}


		public virtual string Render()
		{
			if (Template == null) throw new InvalidOperationException("Template is not set.");
			if (FormFilterOptions == null) throw new InvalidOperationException("FormFilterOptions is not set.");
			if (AvailableFilterOptions == null) throw new InvalidOperationException("AvailableFilterOptions is not set.");
			if (FilterStatistics == null) throw new InvalidOperationException("FilterStatistics is not set.");

			var fragmentFilterForm = Template.GetFragment("FilterForm");
			fragmentFilterForm.AddVariable("Iterate:BrandOption", RenderBrandOptions(), VariableEncoding.None);
			fragmentFilterForm.AddVariable("Iterate:AgeIntervalOption", RenderAgeIntervalOptions(), VariableEncoding.None);
			fragmentFilterForm.AddVariable("Iterate:PriceIntervalOption", RenderPriceIntervalOptions(), VariableEncoding.None);

			fragmentFilterForm.AddRegexVariable(@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;

					return RenderTagGroup(tagGroupCommonName);
				},
				VariableEncoding.None);

			fragmentFilterForm.AddVariable("Iterate:Level1CategoryOption", RenderLevel1CategoryOptions(), VariableEncoding.None);

			bool hasLevel2Categories = AvailableFilterOptions.Level2Categories != null && AvailableFilterOptions.Level2Categories.Any();
			fragmentFilterForm.AddCondition("HasLevel2CategoryOptions", hasLevel2Categories);
			if (hasLevel2Categories)
			{
				fragmentFilterForm.AddVariable("Iterate:Level2CategoryOption", RenderLevel2CategoryOptions(), VariableEncoding.None);
			}

			bool hasLevel3Categories = AvailableFilterOptions.Level3Categories != null && AvailableFilterOptions.Level3Categories.Any();
			fragmentFilterForm.AddCondition("HasLevel3CategoryOptions", hasLevel3Categories);
			if (hasLevel3Categories)
			{
				fragmentFilterForm.AddVariable("Iterate:Level3CategoryOption", RenderLevel3CategoryOptions(), VariableEncoding.None);
			}

			fragmentFilterForm.AddVariable("Iterate:SizeOption", RenderSizeOptions(), VariableEncoding.None);

			fragmentFilterForm.AddVariable("Iterate:SortOption", RenderSortOptions(), VariableEncoding.None);
			fragmentFilterForm.AddVariable("Iterate:PageSizeOption", RenderPageSizeOptions(), VariableEncoding.None);
			fragmentFilterForm.AddCondition("HasSecondaryTemplate", AvailableFilterOptions.HasSecondaryTemplate);
			fragmentFilterForm.AddCondition("UseSecondaryTemplate", UseSecondaryTemplate);
			AddTemplateLinks(fragmentFilterForm);

			return fragmentFilterForm.Render();
		}

		public virtual void MapFromRequest()
		{
			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			UseSecondaryTemplate = useSecondaryTemplate ?? false;

			string mode = Request.QueryString["mode"];
			FilterMode = mode.HasValue() && mode.Equals("filter", StringComparison.OrdinalIgnoreCase);

			if (FilterMode)
			{
				MapFromRequestQuery(FormFilterOptions.Level1CategoryIds, "level1category-id");
				MapFromRequestQuery(FormFilterOptions.Level2CategoryIds, "level2category-id");
				MapFromRequestQuery(FormFilterOptions.Level3CategoryIds, "level3category-id");

				MapFromRequestQuery(FormFilterOptions.BrandIds, "brand-id");

				MapTagIdsFromRequestQuery();

				MapAgeFromRequestQuery("age_month-from", "age_month-to");
				MapPriceFromRequestQuery("price-from", "price-to");

				MapFromRequestQuery(FormFilterOptions.SizeIds, "size-id");

				MapSortOptionFromRequestQuery();

				PageSize = Request.QueryString.GetInt32OrNull("pagesize") ?? 0;
			}
		}


		private void AddTemplateLinks(Fragment fragment)
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			queryBuilder.Remove("usesecondarytemplate");
			string primaryQuery = queryBuilder.ToString();
			queryBuilder.Add("usesecondarytemplate", "true");
			string secondaryQuery = queryBuilder.ToString();
			string baseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			fragment.AddVariable("PrimaryTemplateLink", baseUrl + primaryQuery);
			fragment.AddVariable("SecondaryTemplateLink", baseUrl + secondaryQuery);
		}

		private string RenderTagGroup(string commonName)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var tagGroup = AvailableFilterOptions.TagGroups
				.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			var fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("Iterate:TagOption", RenderTagOptions(tagGroup), VariableEncoding.None);
			fragmentTagGroup.AddVariable("TagGroup.Id", tagGroup.Id.ToString(CultureInfo.InvariantCulture));
			return fragmentTagGroup.Render();
		}

		private string RenderTagOptions(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();
			foreach (ITag tag in tagGroup.Tags)
			{
				//Only render campaign tags if campaign is active/valid/online/correct channel (since we dont have channel specific tags)
				if (tag.TagGroupId == AutoCampaignPageSetting.Instance.CampaignTagGroupId)
				{
					var campaign = IoC.Resolve<IProductCampaignService>().GetById(UserContext.Current,(int.Parse(tag.CommonName, CultureInfo.CurrentCulture)));
					if (campaign == null) continue;

					var registries = IoC.Resolve<ICampaignRegistryCampaignService>().GetRegistriesByCampaignId(campaign.Id);
					if (registries.FirstOrDefault(r => r.ChannelId == UserContext.Current.Channel.Id) == null ||
						campaign.CampaignStatus.Id != 0 ||
						(campaign.StartDate.HasValue && campaign.StartDate > DateTime.Now) ||
						(campaign.EndDate.HasValue && campaign.EndDate < DateTime.Now))
					{
						continue;
					}
				}

				int tagId = tag.Id;
				int count;
				FilterStatistics.TagStats.TryGetValue(tagId, out count);
				bool isSelected = FormFilterOptions.GroupedTagIds.Any(group => group != null && group.Any(id => id == tagId));
				tagBuilder.AppendLine(RenderOption("TagOption", tagId, tag.Value, isSelected, count));
			}
			return tagBuilder.ToString();
		}

		private string RenderSortOptions()
		{
			var sortOptionBuilder = new StringBuilder();
			foreach (var sortOption in AvailableFilterOptions.SortOptions)
			{
				var fragmentSortOption = Template.GetFragment("SortOption");
				fragmentSortOption.AddVariable("Name", sortOption.Value, VariableEncoding.None);
				fragmentSortOption.AddVariable("Value", sortOption.Key.ToString());
				fragmentSortOption.AddCondition("IsSelected", sortOption.Key == SortOption);
				sortOptionBuilder.AppendLine(fragmentSortOption.Render());
			}
			return sortOptionBuilder.ToString();
		}

		private string RenderPageSizeOptions()
		{
			var pageSizeBuilder = new StringBuilder();
			foreach (var pageSize in AvailableFilterOptions.PageSizeOptions)
			{
				var fragmentPageSizeOption = Template.GetFragment("PageSizeOption");
				fragmentPageSizeOption.AddVariable("Name", pageSize.ToString(CultureInfo.InvariantCulture));
				fragmentPageSizeOption.AddVariable("Value", pageSize.ToString(CultureInfo.InvariantCulture));
				fragmentPageSizeOption.AddCondition("IsSelected", pageSize == PageSize);
				pageSizeBuilder.AppendLine(fragmentPageSizeOption.Render());
			}
			return pageSizeBuilder.ToString();
		}

		private string RenderAgeIntervalOptions()
		{
			var ageIntervalAvailable = AvailableFilterOptions.AgeIntervalOuter;
			var ageIntervalFiltered = FilterStatistics.AgeInterval;
			var ageIntervalSelected = FormFilterOptions.AgeInterval ?? ageIntervalAvailable;

			var fragmentOption = Template.GetFragment("AgeIntervalOption");

			if (ageIntervalAvailable != null)
			{
				fragmentOption.AddVariable("AgeAvailable.FromMonth", RenderNullableVariable(ageIntervalAvailable.FromMonth));
				fragmentOption.AddVariable("AgeAvailable.ToMonth", RenderNullableVariable(ageIntervalAvailable.ToMonth));
				fragmentOption.AddCondition("AgeAvailable.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("AgeAvailable.Exist", false);
			}

			if (ageIntervalFiltered != null)
			{
				fragmentOption.AddVariable("AgeFiltered.FromMonth", RenderNullableVariable(ageIntervalFiltered.FromMonth));
				fragmentOption.AddVariable("AgeFiltered.ToMonth", RenderNullableVariable(ageIntervalFiltered.ToMonth));
				fragmentOption.AddCondition("AgeFiltered.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("AgeFiltered.Exist", false);
			}

			if (ageIntervalSelected != null)
			{
				fragmentOption.AddVariable("AgeSelected.FromMonth", RenderNullableVariable(ageIntervalSelected.FromMonth));
				fragmentOption.AddVariable("AgeSelected.ToMonth", RenderNullableVariable(ageIntervalSelected.ToMonth));
				fragmentOption.AddCondition("AgeSelected.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("AgeSelected.Exist", false);
			}

			return fragmentOption.Render();
		}

		private string RenderPriceIntervalOptions()
		{
			var priceIntervalAvailable = AvailableFilterOptions.PriceIntervalOuter;
			var priceIntervalFiltered = FilterStatistics.PriceInterval;
			var priceIntervalSelected = FormFilterOptions.PriceInterval ?? priceIntervalAvailable;

			var fragmentOption = Template.GetFragment("PriceIntervalOption");

			if (priceIntervalAvailable != null)
			{
				fragmentOption.AddVariable("PriceAvailable.From", RenderNullableVariable(priceIntervalAvailable.From));
				fragmentOption.AddVariable("PriceAvailable.To", RenderNullableVariable(priceIntervalAvailable.To));
				fragmentOption.AddCondition("PriceAvailable.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("PriceAvailable.Exist", false);
			}

			if (priceIntervalFiltered != null)
			{
				fragmentOption.AddVariable("PriceFiltered.From", RenderNullableVariable(priceIntervalFiltered.From));
				fragmentOption.AddVariable("PriceFiltered.To", RenderNullableVariable(priceIntervalFiltered.To));
				fragmentOption.AddCondition("PriceFiltered.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("PriceFiltered.Exist", false);
			}

			if (priceIntervalSelected != null)
			{
				fragmentOption.AddVariable("PriceSelected.From", RenderNullableVariable(priceIntervalSelected.From));
				fragmentOption.AddVariable("PriceSelected.To", RenderNullableVariable(priceIntervalSelected.To));
				fragmentOption.AddCondition("PriceSelected.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("PriceSelected.Exist", true);
			}

			return fragmentOption.Render();
		}

		private string RenderBrandOptions()
		{
			var brandBuilder = new StringBuilder();
			foreach (IBrand brand in AvailableFilterOptions.Brands)
			{
				int brandId = brand.Id;

				int count;
				FilterStatistics.BrandStats.TryGetValue(brandId, out count);

				bool isSelected = FormFilterOptions.BrandIds.Any(id => id == brandId);

				brandBuilder.AppendLine(RenderOption("BrandOption", brandId, brand.Title, isSelected, count));
			}
			return brandBuilder.ToString();
		}

		private string RenderLevel1CategoryOptions()
		{
			return RenderCategoryOptions("Level1CategoryOption", AvailableFilterOptions.Level1Categories, FormFilterOptions.Level1CategoryIds, FilterStatistics.MainCategoryStats);
		}

		private string RenderLevel2CategoryOptions()
		{
			return RenderCategoryOptions("Level2CategoryOption", AvailableFilterOptions.Level2Categories, FormFilterOptions.Level2CategoryIds, FilterStatistics.ParentCategoryStats);
		}

		private string RenderLevel3CategoryOptions()
		{
			return RenderCategoryOptions("Level3CategoryOption", AvailableFilterOptions.Level3Categories, FormFilterOptions.Level3CategoryIds, FilterStatistics.CategoryStats);
		}

		private string RenderCategoryOptions(string optionFragmentName, IEnumerable<ICategory> availableCategories, IEnumerable<int> selectedCategoryIds, Dictionary<int, int> categoryStats)
		{
			if (optionFragmentName == null) throw new ArgumentNullException("optionFragmentName");
			if (availableCategories == null) throw new ArgumentNullException("availableCategories");
			if (selectedCategoryIds == null) throw new ArgumentNullException("selectedCategoryIds");

			var categoryBuilder = new StringBuilder();

			Dictionary<int, int> selectedCategoryIdDictionary = selectedCategoryIds.ToDictionary(id => id, id => id);

			foreach (ICategory category in availableCategories.OrderBy(c => c.Title))
			{
				int categoryId = category.Id;

				int count;
				categoryStats.TryGetValue(categoryId, out count);

				bool isSelected = selectedCategoryIdDictionary.ContainsKey(categoryId);

				categoryBuilder.AppendLine(RenderOption(optionFragmentName, categoryId, category.Title, isSelected, count));
			}
			return categoryBuilder.ToString();
		}

		private string RenderSizeOptions()
		{
			var sizeBuilder = new StringBuilder();
			foreach (ISize size in AvailableFilterOptions.Sizes)
			{
				int sizeId = size.Id;
				int count;
				FilterStatistics.SizeStats.TryGetValue(sizeId, out count);
				bool isSelected = FormFilterOptions.SizeIds.Any(id => id == sizeId);
				sizeBuilder.AppendLine(RenderOption("SizeOption", sizeId, size.EuTitle ?? size.Eu.ToString(CultureInfo.InvariantCulture), isSelected, count));
			}
			return sizeBuilder.ToString();
		}

		private string RenderOption(string optionFragmentName, int id, string name, bool isSelected, int count)
		{
			var fragmentOption = Template.GetFragment(optionFragmentName);
			fragmentOption.AddVariable("Name", name);
			fragmentOption.AddVariable("Value", id.ToString(CultureInfo.InvariantCulture));
			fragmentOption.AddCondition("IsSelected", isSelected);
			fragmentOption.AddVariable("Count", count.ToString(CultureInfo.InvariantCulture));
			fragmentOption.AddCondition("CountIsZero", count == 0);
			return fragmentOption.Render();
		}

		private static string RenderNullableVariable<T>(T? variable) where  T: struct, IConvertible
		{
			if (variable.HasValue)
			{
				return variable.Value.ToString(CultureInfo.InvariantCulture);
			}

			return string.Empty;
		}


		private void MapFromRequestQuery(ICollection<int> items, string parameter)
		{
			IEnumerable<int> requestItems = MapItemsFromRequestQuery(parameter);
			foreach (int item in requestItems)
			{
				items.Add(item);
			}
		}

		private void MapTagIdsFromRequestQuery()
		{
			var parameters = Request.QueryString.AllKeys.Distinct();
			foreach (string parameter in parameters)
			{
				if (parameter.IsEmpty()) continue;

				Match match = _tagGroupParameterRegex.Match(parameter);
				if (!match.Success) continue;

				int tagGroupId;
				if (!int.TryParse(match.Groups[1].Value, out tagGroupId))
				{
					continue;
				}

				IEnumerable<int> tagIds = MapItemsFromRequestQuery(parameter);
				FormFilterOptions.GroupedTagIds.Add(tagIds);
			}
		}

		private void MapAgeFromRequestQuery(string parameterFrom, string parameterTo)
		{
			int? fromMonth = Request.QueryString.GetInt32OrNull(parameterFrom);
			int? toMonth = Request.QueryString.GetInt32OrNull(parameterTo);

			if (fromMonth.HasValue || toMonth.HasValue)
			{
				FormFilterOptions.AgeInterval = AgeIntervalService.GetAgeInterval(fromMonth, toMonth);
			}
		}

		private void MapPriceFromRequestQuery(string parameterFrom, string parameterTo)
		{
			decimal? from = null;
			decimal? to = null;
			decimal parsedValue;

			if (!Request.QueryString[parameterFrom].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterFrom], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					from = parsedValue;
				}
			}

			if (!Request.QueryString[parameterTo].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterTo], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					to = parsedValue;
				}
			}

			if (from.HasValue || to.HasValue)
			{
				FormFilterOptions.PriceInterval = PriceIntervalService.GetPriceInterval(from, to);
			}
		}

		private IEnumerable<int> MapItemsFromRequestQuery(string parameter)
		{
			if (Request.QueryString[parameter].IsEmpty())
			{
				return new int[0];
			}

			return Request.QueryString[parameter]
				.Split(',')
				.Select(value =>
				{
					int intValue;
					return int.TryParse(value, out intValue) ? intValue : 0;
				})
				.Where(value => value != 0).Distinct();
		}

		private void MapSortOptionFromRequestQuery()
		{
			string sortOptionValue = Request.QueryString["sort"];

			if (sortOptionValue == null || !Enum.IsDefined(typeof(SortOption), sortOptionValue))
			{
				return;
			}

			SortOption = (SortOption)Enum.Parse(typeof(SortOption), sortOptionValue);
		}

		public virtual void AdjustSortOption(string defaultSort)
		{
			if (SortOption == SortOption.Unknown)
			{
				SortOption = SortOption.Popularity;
				if (defaultSort.HasValue())
				{
					if (Enum.IsDefined(typeof(SortOption), defaultSort))
					{
						SortOption = (SortOption)Enum.Parse(typeof(SortOption), defaultSort);
					}
				}
			}
		}

		public virtual void AdjustPageSize(IEnumerable<int> pageSizeOptions, int defaultPageSize)
		{
			if (PageSize == 0)
			{
				PageSize = defaultPageSize;
			}
			else if (pageSizeOptions.Any(ps => ps == PageSize) == false)
			{
				PageSize = defaultPageSize;
			}
		}
	}
}
