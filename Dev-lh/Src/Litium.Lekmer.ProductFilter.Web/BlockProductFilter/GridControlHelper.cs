﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter
{
	public class GridControlHelper
	{
		private Dictionary<int, IProductRatingSummaryInfo> _productRatingSummaries;
		private readonly IBlockRating _blockRating;
		private readonly Template _template;
		private readonly IRatingService _ratingService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingItemProductScoreService _ratingItemProductScoreService;

		public GridControlHelper(
			IBlockRating blockRating,
			Template template,
			IRatingService ratingService,
			IRatingGroupService ratingGroupService,
			IRatingItemProductScoreService ratingItemProductScoreService)
		{
			_blockRating = blockRating;
			_template = template;
			_ratingService = ratingService;
			_ratingGroupService = ratingGroupService;
			_ratingItemProductScoreService = ratingItemProductScoreService;
		}

		protected virtual IRating InitializeRating(int productId, int categoryId)
		{
			_productRatingSummaries = new Dictionary<int, IProductRatingSummaryInfo>();

			var ratings = new Collection<IRating>();

			// Get ratings and groups linked to Block
			if(_blockRating != null)
			{
				ratings.Add(_ratingService.GetById(UserContext.Current, _blockRating.RatingId));
			}

			// Get rating groups linked to Product
			var productRatingDictionaty = new Dictionary<int, IRating>();
			var productRatingGroups = _ratingGroupService.GetAllByProduct(UserContext.Current, productId, categoryId);
			foreach (IRatingGroup productRatingGroup in productRatingGroups)
			{
				foreach (IRating productRating in _ratingService.GetAllByGroup(UserContext.Current, productRatingGroup.Id))
				{
					productRatingDictionaty[productRating.Id] = productRating;
				}
			}

			IRating rating = null;

			if (ratings.Count > 0)
			{
				if (productRatingDictionaty.ContainsKey(ratings[0].Id))
				{
					rating = ratings[0];
				}
			}

			return rating;
		}

		public virtual string RenderRating(int productId, int categoryId)
		{
			var rating = InitializeRating(productId, categoryId);

			if (rating == null)
			{
				return string.Empty;
			}

			Fragment fragment = _template.GetFragment("Rating");
			fragment.AddEntity(rating);

			IProductRatingSummaryInfo productRatingSummaryInfo = GetProductRatingSummary(rating.Id, productId);

			fragment.AddVariable("Rating.AverageRatingScoreRaw", productRatingSummaryInfo.AverageRatingScore.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.AverageRatingScore", Math.Round(productRatingSummaryInfo.AverageRatingScore, 1).ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.AverageRatingItemId", productRatingSummaryInfo.AverageRatingItemId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.TotalNumberOfVotes", productRatingSummaryInfo.TotalNumberOfVotes.ToString(CultureInfo.InvariantCulture));

			fragment.AddVariable("RatingItemList", RenderRatingItemList(rating, productId).Render(), VariableEncoding.None);

			return fragment.Render();
		}

		protected virtual Fragment RenderRatingItemList(IRating rating, int productId)
		{
			Fragment fragment = _template.GetFragment("RatingItemList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingItem ratingItem in rating.RatingItems)
			{
				stringBuilder.AppendLine(RenderRatingItem(rating, ratingItem, productId).Render());
			}

			fragment.AddVariable("Iterate:RatingItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItem(IRating rating, IRatingItem ratingItem, int productId)
		{
			Fragment fragment = _template.GetFragment("RatingItem");

			fragment.AddEntity(rating);
			fragment.AddEntity(ratingItem);

			IProductRatingSummaryInfo productRatingSummaryInfo = GetProductRatingSummary(rating.Id, productId);
			IRatingItemSummary ratingItemSummary = GetRatingItemSummary(rating.Id, ratingItem.Id, productId);

			fragment.AddCondition("RatingItem.IsAverage", productRatingSummaryInfo.AverageRatingItemId == ratingItem.Id);
			fragment.AddVariable("RatingItem.TotalNumberOfVotes", ratingItemSummary.TotalHitCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItem.PersentageNumberOfVotesRaw", ratingItemSummary.PercentageHitValue.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItem.PersentageNumberOfVotes", Math.Round(ratingItemSummary.PercentageHitValue, 0).ToString(CultureInfo.InvariantCulture));

			return fragment;
		}

		protected virtual IProductRatingSummaryInfo GetProductRatingSummary(int ratingId, int productId)
		{
			if (_productRatingSummaries.ContainsKey(ratingId))
			{
				return _productRatingSummaries[ratingId];
			}

			IProductRatingSummaryInfo productRatingSummaryInfo = _ratingItemProductScoreService.GetProductRatingSummary(UserContext.Current, ratingId, productId);

			_productRatingSummaries[ratingId] = productRatingSummaryInfo;

			return productRatingSummaryInfo;
		}

		protected virtual IRatingItemSummary GetRatingItemSummary(int ratingId, int ratingItemId, int productId)
		{
			return GetProductRatingSummary(ratingId, productId).RatingItemSummaryCollection.FirstOrDefault(ri => ri.RatingItemId == ratingItemId);
		}
	}
}