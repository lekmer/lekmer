﻿namespace Litium.Lekmer.SiteStructure
{
	public enum ContentPageType
	{
		ContentPage = 1,
		ProductPage = 2,
		OrderHistoryDetailPage = 3
	}
}
