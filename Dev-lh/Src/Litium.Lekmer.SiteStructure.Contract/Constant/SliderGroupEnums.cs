﻿using System.ComponentModel;

namespace Litium.Lekmer.SiteStructure
{
	public enum GradientEnum
	{
		[Description("horizontal")]
		horizontal = 1,
		[Description("vertical")]
		vertical = 2,
		[Description("diagonal-1")]
		diagonal1 = 3,
		[Description("diagonal-2")]
		diagonal2 = 4,
		[Description("radial")]
		radial = 5
	}

	public enum ShapeEnum
	{
		[Description("circle")]
		circle = 1,
		[Description("square")]
		square = 2,
		[Description("custom-1")]
		custom1 = 3,
		[Description("custom-2")]
		custom2 = 4,
		[Description("custom-3")]
		custom3 = 5
	}

	public enum TextAlignEnum
	{
		[Description("default")]
		def = 1,
		[Description("left")]
		left = 2,
		[Description("center")]
		center = 3,
		[Description("right")]
		right = 4,
		[Description("top")]
		top = 5,
		[Description("bottom")]
		bottom = 6
	}
}