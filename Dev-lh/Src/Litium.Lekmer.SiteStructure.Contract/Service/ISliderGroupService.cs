﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface ISliderGroupService
	{
		Collection<ISliderGroup> GetByBlockId(IUserContext context, int blockId);
	}
}