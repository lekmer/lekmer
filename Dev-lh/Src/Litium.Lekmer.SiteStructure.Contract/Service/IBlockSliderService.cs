﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockSliderService
	{
		IBlockSlider GetById(IUserContext context, int id);
	}
}