﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockSliderSecureService
	{
		IBlockSlider Create();
		IBlockSlider GetById(int id);
		int Save(ISystemUserFull systemUser, IBlockSlider block, Collection<ISliderGroup> groups);
		void Delete(ISystemUserFull systemUser, int blockId);
	}
}