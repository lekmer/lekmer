﻿using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface ILekmerContentNodeSecureService
	{
		void ChangeStatus(ISystemUserFull systemUserFull, List<int> contentPageIds, bool isSetOnline);
	}
}