﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface ISliderGroupSecureService
	{
		ISliderGroup Create(int blockId, int sliderGroupId);
		Collection<ISliderGroup> GetByBlockId(int blockId);
		void Save(ISystemUserFull systemUser, int blockId, Collection<ISliderGroup> groups);
		void Save(ISystemUserFull systemUser, ISliderGroup group);
	}
}