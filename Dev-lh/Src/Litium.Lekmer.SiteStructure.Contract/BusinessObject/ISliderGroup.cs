﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	public interface ISliderGroup : IBusinessObjectBase
	{
		int SliderGroupId { get; set; }
		int BlockId { get; set; }
		int? ImageId1 { get; set; }
		int? ThumbnailImageId1 { get; set; }
		int? InternalLinkContentNodeId1 { get; set; }
		string ExternalLink1 { get; set; }
		bool IsExternalLinkMovie1 { get; set; }
		int? ImageId2 { get; set; }
		int? ThumbnailImageId2 { get; set; }
		int? InternalLinkContentNodeId2 { get; set; }
		string ExternalLink2 { get; set; }
		bool IsExternalLinkMovie2 { get; set; }
		string Title1 { get; set; }
		int? Title1Size { get; set; }
		string Title1Color { get; set; }
		string Title1Link { get; set; }
		string Title2 { get; set; }
		int? Title2Size { get; set; }
		string Title2Color { get; set; }
		string Title2Link { get; set; }
		string Title3 { get; set; }
		int? Title3Size { get; set; }
		string Title3Color { get; set; }
		string Title3Link { get; set; }
		string Description { get; set; }
		int? DescriptionSize { get; set; }
		string DescriptionColor { get; set; }
		string Disclaimer { get; set; }
		int? DisclaimerSize { get; set; }
		string DisclaimerColor { get; set; }
		string Button1Text { get; set; }
		int? Button1Size { get; set; }
		string Button1Color { get; set; }
		string Button1Link { get; set; }
		string Button2Text { get; set; }
		int? Button2Size { get; set; }
		string Button2Color { get; set; }
		string Button2Link { get; set; }
		string Button3Text { get; set; }
		int? Button3Size { get; set; }
		string Button3Color { get; set; }
		string Button3Link { get; set; }
		string DiscountText { get; set; }
		int? DiscountSize { get; set; }
		string DiscountColor { get; set; }
		int? DiscountPosition { get; set; }
		string ExtraText { get; set; }
		int? ExtraSize { get; set; }
		string ExtraColor { get; set; }
		int? ExtraPosition { get; set; }
		string BackgroundColor1 { get; set; }
		string BackgroundColor2 { get; set; }
		int? BackgroundGradient { get; set; }
		string HolderColor { get; set; }
		int? HolderAlpha { get; set; }
		int? HolderShape { get; set; }
		int? HolderTextAlign { get; set; }
		int? Style { get; set; }
		int Ordinal { get; set; }
		string Captions { get; set; }
		int? CampaignId { get; set; }
		int? CampaignTypeId { get; set; }
		bool IsActive { get; set; }
		bool UseCampaignDates { get; set; }
		DateTime? StartDateTime { get; set; }
		DateTime? EndDateTime { get; set; }
		bool IsOpened { get; set; }
	}
}