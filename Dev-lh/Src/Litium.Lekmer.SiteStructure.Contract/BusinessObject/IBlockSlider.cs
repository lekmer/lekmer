﻿namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockSlider : ILekmerBlock
	{
		int SlideDuration { get; set; }
	}
}