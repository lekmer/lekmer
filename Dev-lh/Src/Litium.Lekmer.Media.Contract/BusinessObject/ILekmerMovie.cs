﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.Media
{
	public interface ILekmerMovie 
	{
		ILekmerImage LekmerImage { get; set; }
		IProductMedia ProductMedia { get; set; }
	}
}
