﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerMediaItemService : IMediaItemService
	{
		/// <summary>
		/// Get's media items by size table id
		/// </summary>
		Collection<IMediaItem> GetAllBySizeTable(IUserContext userContext, int sizeTableId);
	}
}