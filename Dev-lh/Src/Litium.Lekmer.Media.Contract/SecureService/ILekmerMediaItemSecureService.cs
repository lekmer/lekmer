﻿using System.Collections.ObjectModel;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerMediaItemSecureService : IMediaItemSecureService
	{
		/// <summary>
		/// Get's media items by size table id
		/// </summary>
		Collection<IMediaItem> GetAllBySizeTable(int sizeTableId);
	}
}