﻿using System.Web;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Avail.Web
{
	public static class AvailSessionCookie
	{
		public static string AvailSessionCookieKey = "__avail_session__";

		public static string GetAvailSession()
		{
			HttpCookie cookie = HttpContext.Current.Request.Cookies[AvailSessionCookieKey];
			if (cookie != null)
			{
				return cookie.Value;
			}

			var availSessionId = IoC.Resolve<IAvailSession>().AvailSessionId;
			if (availSessionId != null)
			{
				return availSessionId;
			}

			return null;
		}
	}
}
