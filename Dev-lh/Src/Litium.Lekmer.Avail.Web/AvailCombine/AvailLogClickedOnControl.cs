﻿using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
  public class AvailLogClickedOnControl
  {
	  public void Render(Fragment fragmentHead, Fragment fragmentContent, IProduct product, string trackingCode)
	  {
		  if (product == null | string.IsNullOrEmpty(trackingCode))
		  {
			  fragmentHead.AddCondition("UseLogClickedOn", false);
			  fragmentContent.AddCondition("UseLogClickedOn", false);
			  return;
		  }

		  fragmentHead.AddVariable("TrackingCode", trackingCode);
		  fragmentHead.AddCondition("UseLogClickedOn", true);
		  fragmentContent.AddCondition("UseLogClickedOn", true);
	  }
	}
}
