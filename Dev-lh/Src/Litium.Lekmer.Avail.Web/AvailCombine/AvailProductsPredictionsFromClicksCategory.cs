﻿using System.Globalization;
using System.Text;
using Litium.Lekmer.Product.Web.ProductPage.Cookie;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailProductsPredictionsFromClicksCategory
	{
        public void Render(Fragment fragmentHead, Fragment fragmentContent, bool getClickHistoryFromCookie)
		{
			fragmentHead.AddCondition("UseProductsPredictionsFromClicksCategory", true);
			fragmentContent.AddCondition("UseProductsPredictionsFromClicksCategory", true);

            fragmentHead.AddCondition("GetClickHistoryFromCookie", getClickHistoryFromCookie);
            fragmentContent.AddCondition("GetClickHistoryFromCookie", getClickHistoryFromCookie);
            if(getClickHistoryFromCookie)
            {
                ProductCollection existProducts=new ProductCollection();
                var productIds = new ProductIdCollection(ClickHistoryCookie.GetProductsFromClickHistory());
                if (productIds.Count > 0)
                {
                    existProducts = IoC.Resolve<IProductService>().PopulateProducts(UserContext.Current, productIds);
                    if (existProducts.Count > 0)
                    {
                        var itemBuilder = new StringBuilder();
                        itemBuilder.Append("new Array(");
                        foreach (var item in productIds)
                        {
                            itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}',", item));
                        }
                        if (itemBuilder.Length > 0)
                            itemBuilder.Remove(itemBuilder.Length - 1, 1);
                        itemBuilder.Append(")");
                        fragmentHead.AddVariable("ClickHistoryProduct.Ids", itemBuilder.ToString());
                        fragmentHead.AddCondition("HasClickHistoryProduct", true);
                        fragmentContent.AddCondition("HasClickHistoryProduct", true);
                        ClickHistoryCookie.AddProductToClickHistory(existProducts);
                        return;
                    }
                }
                fragmentHead.AddCondition("HasClickHistoryProduct", false);
                fragmentContent.AddCondition("HasClickHistoryProduct", false);
                ClickHistoryCookie.AddProductToClickHistory(existProducts);
            }
		}
	}
}
