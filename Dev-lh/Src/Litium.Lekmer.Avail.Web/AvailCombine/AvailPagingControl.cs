﻿using System.Globalization;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core.Web;

namespace Litium.Lekmer.Avail.Web.AvailCombine
{
	public class AvailPagingControl : LekmerPagingControl
	{
		public AvailPagingControl(
			ITemplateFactory templateFactory,
			IPagingCalculator pagingCalculator)
			: base(templateFactory, pagingCalculator)
		{
		}

		/// <summary>
		/// Gets the url to a certain page number.
		/// </summary>
		/// <param name="page">The page number.</param>
		/// <returns>Url for one page.</returns>
		protected override string GetPageUrl(int page)
		{
			var queryBuilder = new QueryBuilder();
			if (page != 1)
			{
				queryBuilder.ReplaceOrAdd(PageQueryStringParameterName, page.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				queryBuilder.Remove(PageQueryStringParameterName);
			}

			return PageBaseUrl + queryBuilder;
		}
	}
}
