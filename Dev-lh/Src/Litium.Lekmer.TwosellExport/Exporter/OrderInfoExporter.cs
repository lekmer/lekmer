﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using Litium.Lekmer.TwosellExport.Contract;
using log4net;

namespace Litium.Lekmer.TwosellExport
{
	public class OrderInfoExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected ITwosellExportSetting Setting { get; private set; }
		protected ITwosellExportService TwosellExportService { get; private set; }

		public OrderInfoExporter(ITwosellExportSetting setting, ITwosellExportService twosellExportService)
		{
			Setting = setting;
			TwosellExportService = twosellExportService;
		}

		public void Execute()
		{
			_log.Info("Execution started.");

			ExecuteOrderInfoExport();

			_log.Info("Execution complited.");
		}

		protected virtual void ExecuteOrderInfoExport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			string filePath = TwosellExportService.GetOrderInfoFilePath();
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			_log.InfoFormat("File preparing... //{0}", filePath);

			_log.Info("Get orders info...");
			var orderInfoCollection = TwosellExportService.GetOrderInfoCollectionToExport();
			if (orderInfoCollection.Count <= 0)
			{
				stopwatch.Stop();
				_log.Info("There are no orders info");
				return;
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				var orderInfoFile = new OrderInfoFile();
				orderInfoFile.Initialize(Setting, orderInfoCollection);
				orderInfoFile.Save(stream);
			}

			_log.InfoFormat("File saved... //{0}", filePath);
			stopwatch.Stop();
			_log.InfoFormat("Elapsed time - {0}", stopwatch.Elapsed);
		}
	}
}