using System;
using Litium.Lekmer.TwosellExport.Contract;

namespace Litium.Lekmer.TwosellExport
{
	[Serializable]
	public class OrderInfo : IOrderInfo
	{
		public DateTime Date { get; set; }
		public int OrderId { get; set; }
		public int ProductId { get; set; }
		public string ErpId { get; set; }
		public int Quantity { get; set; }
		public decimal Price { get; set; }
		public string CustomerIp { get; set; }
		public string Email { get; set; }
		public string LekmerId { get; set; }
	}
}