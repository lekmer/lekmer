using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Product;

namespace Litium.Lekmer.TwosellExport
{
	[Serializable]
	public class ProductInfo : IProductInfo
	{
		public ILekmerProductView Product { get; set; }
		public string CategoryTitle { get; set; }
		public string ParentCategoryTitle { get; set; }
		public string MainCategoryTitle { get; set; }
		public Collection<IProductImageGroupFull> ImageGroups { get; set; }
		public string FirstCategoryLevelUrl { get; set; }
		public string SecondCategoryLevelUrl { get; set; }
		public string ThirdCategoryLevelUrl { get; set; }
		public int? Popularity { get; set; }
		public int? SalesAmount { get; set; }
		public string BrandUrl { get; set; }
	}
}