﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Framework.Statistics;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.ProductFilter.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Statistics.TrackerEntity;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.ProductSearch
{
	public class BlockProductSearchEsalesResultControl : BlockControlBase<IBlockProductSearchEsalesResult>
	{
		private readonly IBlockProductSearchEsalesResultService _blockProductSearchEsalesResultService;
		private readonly IEsalesProductSearchService _esalesProductSearchService;
		private readonly IEsalesSearchFacetService _esalesSearchFacetService;
		private readonly ITemplateFactory _templateFactory;
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IPriceIntervalService _priceIntervalService;
		private readonly ITagGroupService _tagGroupService;
		private readonly ILekmerProductService _lekmerProductService;

		private readonly SelectedFilterOptions _selectedFilterOptions = new SelectedFilterOptions();
		private readonly AvailableFilterOptions _availableFilterOptions = new AvailableFilterOptions();
		private readonly Regex _tagGroupParameterRegex = new Regex(@"^taggroup(\d+)-tag-id$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		private EsalesFilterFormControl _filterForm;
		private ICommonSession _commonSession;

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ProductCollection Products { get; set; }
		protected ISearchFacet SearchFacetByFilter { get; set; }
		protected ISearchFacet SearchFacetGeneral { get; set; }

		protected IPagingControl PagingControl { get; set; }

		private string _searchPhrase;
		protected string SearchPhrase
		{
			get
			{
				if (string.IsNullOrEmpty(_searchPhrase))
				{
					_searchPhrase = IsXmlHttpRequest(Request) ? HttpUtility.HtmlDecode(Request.QueryString["q"]) : Request.QueryString["q"];
				}
				return _searchPhrase;
			}
		}

		protected virtual EsalesFilterFormControl FilterForm
		{
			get { return _filterForm ?? (_filterForm = CreateFilterFormControl()); }
		}
		public ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
			set { _commonSession = value; }
		}

		/// <summary>
		/// Initializes the <see cref="BlockProductSearchEsalesResultControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="blockProductSearchEsalesResultService"></param>
		/// <param name="esalesProductSearchService"></param>
		/// <param name="esalesSearchFacetService"> </param>
		/// <param name="ageIntervalService"> </param>
		/// <param name="priceIntervalService"> </param>
		/// <param name="tagGroupService"> </param>
		public BlockProductSearchEsalesResultControl(ITemplateFactory templateFactory,
			IBlockProductSearchEsalesResultService blockProductSearchEsalesResultService, IEsalesProductSearchService esalesProductSearchService,
			IEsalesSearchFacetService esalesSearchFacetService, IAgeIntervalService ageIntervalService, IPriceIntervalService priceIntervalService, ITagGroupService tagGroupService,  ILekmerProductService lekmerProductService)
			: base(templateFactory)
		{
			_templateFactory = templateFactory;
			_blockProductSearchEsalesResultService = blockProductSearchEsalesResultService;
			_esalesProductSearchService = esalesProductSearchService;
			_esalesSearchFacetService = esalesSearchFacetService;
			_ageIntervalService = ageIntervalService;
			_priceIntervalService = priceIntervalService;
			_tagGroupService = tagGroupService;
			_lekmerProductService = lekmerProductService;
		}

		protected override IBlockProductSearchEsalesResult GetBlockById(int blockId)
		{
			return _blockProductSearchEsalesResultService.GetById(UserContext.Current, blockId);
		}

		protected virtual void Initialize()
		{
			SearchFacetGeneral = new SearchFacet();
			SearchFacetByFilter = new SearchFacet();

			PopulateSelectedFilterOptions();
			PopulateAvailableAgeAndPriceFilterOptions();
			PagingControl = CreatePagingControl();

			Products = SearchPhrase.HasValue() ? SearchProducts(SearchPhrase) : new ProductCollection();

			PopulateAvailableFilterOptions();
		}

		protected virtual ProductCollection SearchProducts(string searchPhrase)
		{
			var query = _selectedFilterOptions.BuildEsalesQuery(_availableFilterOptions);

			ISearchSuggestions searchSuggestions = null;

			if (_selectedFilterOptions.SearchMethodType == SearchMethodType.None)
			{
				searchSuggestions = _esalesProductSearchService.SearchProducts(UserContext.Current, searchPhrase, query, PagingControl.SelectedPage, PagingControl.PageSize);
				if (searchSuggestions.Products.Count <= 0)
				{
					searchSuggestions = _esalesProductSearchService.SearchProductsWithFilter(UserContext.Current, searchPhrase, query, PagingControl.SelectedPage, PagingControl.PageSize);
					SearchFacetGeneral = _esalesSearchFacetService.SearchFacetWithFilter(UserContext.Current, searchPhrase);
					_selectedFilterOptions.SearchMethodType = SearchMethodType.Filter;
				}
				else
				{
					SearchFacetGeneral = _esalesSearchFacetService.SearchFacet(UserContext.Current, searchPhrase);
					_selectedFilterOptions.SearchMethodType = SearchMethodType.Regular;
				}
			}
			else if (_selectedFilterOptions.SearchMethodType == SearchMethodType.Regular)
			{
				searchSuggestions = _esalesProductSearchService.SearchProducts(UserContext.Current, searchPhrase, query, PagingControl.SelectedPage, PagingControl.PageSize);
				SearchFacetGeneral = _esalesSearchFacetService.SearchFacet(UserContext.Current, searchPhrase);
			}
			else if (_selectedFilterOptions.SearchMethodType == SearchMethodType.Filter)
			{
				searchSuggestions = _esalesProductSearchService.SearchProductsWithFilter(UserContext.Current, searchPhrase, query, PagingControl.SelectedPage, PagingControl.PageSize);
				SearchFacetGeneral = _esalesSearchFacetService.SearchFacetWithFilter(UserContext.Current, searchPhrase);
			}

			FilterForm.FilterStatistics = SearchFacetByFilter = searchSuggestions.SearchFacetWithFilter;
			ProductCollection products = searchSuggestions.Products;

			var searchStatisticsEntity = new Search
			{
				ChannelId = Channel.Current.Id,
				Date = DateTime.Now,
				HitCount = products.TotalCount,
				Query = searchPhrase
			};

			StatisticsTracker.Track(searchStatisticsEntity);

			return products;
		}

		/// <summary>
		/// Initializes the paging control.
		/// </summary>
		/// <returns>Instance of <see cref="IPagingControl"/>.</returns>
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = _selectedFilterOptions.PageSize;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Page url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("FilterForm", FilterForm.Render(), VariableEncoding.None);
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("ProductTotalCount", Products.TotalCount.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("ProductDisplayedCount", Products.Count.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageSize", _selectedFilterOptions.PageSize.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageCount", ((int) Math.Ceiling((decimal) Products.TotalCount/PagingControl.PageSize)).ToString(CultureInfo.InvariantCulture));

			fragmentContent.AddEntity(Block);

			string head;
			string footer;
			string searchQuery = SearchPhrase.HasValue() ? SearchPhrase : string.Empty;
			RenderHeadAndFooter(searchQuery, pagingContent, out head, out footer);

			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#"),
		 SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
		protected virtual void RenderHeadAndFooter(string searchQuery, PagingContent pagingContent, out string head, out string footer)
		{
			var idsBuilder = new StringBuilder();
			var idsCriteoBuilder = new StringBuilder();
			var erpIdsBuilder = new StringBuilder();
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			for (int i = 0; i < Products.Count; i++)
			{
				if (i > 2) break;

				var product = Products[i];

				Fragment headProductItem = Template.GetFragment("HeadProductItem");
				Fragment footerProductItem = Template.GetFragment("FooterProductItem");
				AddVariable("ProductItem.Id", product.Id.ToString(CultureInfo.InvariantCulture), headProductItem, footerProductItem);
				AddVariable("ProductItem.ErpId", product.ErpId, headProductItem, footerProductItem);
				headItemBuilder.AppendLine(headProductItem.Render());
				footerItemBuilder.AppendLine(footerProductItem.Render());

				if (i == 0)//first or the only item in array
				{
					idsCriteoBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", product.Id));
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "\'{0}\'", product.ErpId));
				}
				else
				{
					idsCriteoBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", product.Id));
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",\'{0}\'", product.ErpId));
				}
			}

			for (int i = 0; i < Products.Count; i++)
			{
				var product = Products[i];

				if (i == 0)//first or the only item in array
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", product.Id));
				}
				else
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", product.Id));
				}
			}

			var ids = idsBuilder.ToString();
			var idsCriteo = idsCriteoBuilder.ToString();
			var erpIds = erpIdsBuilder.ToString();
			var headProductItems = headItemBuilder.ToString();
			var footerProductItems = footerItemBuilder.ToString();

			head = RenderFragment("Head", searchQuery, ids, idsCriteo, erpIds, headProductItems, pagingContent.Head);
			footer = RenderFragment("Footer", searchQuery, ids, idsCriteo, erpIds, footerProductItems, pagingContent.Head);
		}
		protected virtual void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}
		protected virtual string RenderFragment(string fragmentName, string searchQuery, string ids, string idsCriteo, string erpIds, string productItems, string pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", ids);
			fragment.AddVariable("Param.ProductIds.Criteo", idsCriteo);
			fragment.AddVariable("Param.ProductErpIds", erpIds, VariableEncoding.None);
			fragment.AddVariable("SearchQuery", searchQuery.EncodeHtml(), VariableEncoding.JavaScriptStringEncode);
			fragment.AddVariable(fragmentName == "Head" ? "Iterate:HeadProductItem" : "Iterate:FooterProductItem", productItems, VariableEncoding.None);
			return (fragment.Render() ?? string.Empty) + (pagingContent ?? string.Empty);
		}

		/// <summary>
		/// Renders the list of products.
		/// </summary>
		/// <returns>Rendered products.</returns>
		protected virtual string RenderProductList()
		{
			return CreateGridControl().Render();
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual GridControl<IProduct> CreateGridControl()
		{
			string settingName = CommonSession.IsViewportMobile ? "ColumnCountMobile" : "ColumnCount";
			int columnCount;
			if (!int.TryParse(Template.GetSettingOrNull(settingName), out columnCount))
			{
				columnCount = 1;
			}
				return new ColorVarialtsGridControl
				{
					LekmerProductService = _lekmerProductService,
					Items = Products,
					ColumnCount = columnCount,
					RowCount = (int) Math.Ceiling((decimal) _selectedFilterOptions.PageSize/columnCount),
					Template = Template,
					ListFragmentName = "ProductList",
					RowFragmentName = "ProductRow",
					ItemFragmentName = "Product",
					EmptySpaceFragmentName = "EmptySpace"
				};
		}

		private void PopulateSelectedFilterOptions()
		{
			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			_selectedFilterOptions.UseSecondaryTemplate = useSecondaryTemplate ?? false;

			_selectedFilterOptions.PageSize = GetDefaultPageSize();
			_selectedFilterOptions.SetSortOption(SortOption.Popularity.ToString());

			string mode = Request.QueryString[FilterForm.ModeFormName];
			if (mode != null && mode.Equals(FilterForm.ModeValueFilter, StringComparison.InvariantCultureIgnoreCase))
			{
				SetSelectedFromQuery(_selectedFilterOptions.BrandIds, "brand-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level1CategoryIds, "level1category-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level2CategoryIds, "level2category-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level3CategoryIds, "level3category-id");
				SetSelectedAgeFromQuery(_selectedFilterOptions, "age_month-from", "age_month-to");
				SetSelectedPriceFromQuery(_selectedFilterOptions, "price-from", "price-to");
				SetSelectedFromQuery(_selectedFilterOptions.SizeIds, "size-id");
				SetSelectedTagIds();

				int? pageSize = Request.QueryString.GetInt32OrNull("pagesize");
				_selectedFilterOptions.PageSize = pageSize ?? GetDefaultPageSize();
				_selectedFilterOptions.SetSortOption(Request.QueryString["sort"]);

				SetSearchMethodType();
			}
		}
		private void PopulateAvailableFilterOptions()
		{
			var context = UserContext.Current;

			_availableFilterOptions.Brands = SearchFacetGeneral.AllBrandsWithoutFilter;
			_availableFilterOptions.Sizes = SearchFacetGeneral.AllSizesWithoutFilter;
			_availableFilterOptions.TagGroups = _tagGroupService.GetAll(context);

			// Level 1 categories.
			_availableFilterOptions.Level1Categories = SearchFacetGeneral.AllMainCategoriesWithoutFilter;

			// Level 2 categories.
			if (_selectedFilterOptions.Level1CategoryIds.Count == 1)
			{
				_availableFilterOptions.Level2Categories = SearchFacetGeneral.AllParentCategoriesWithoutFilter;
			}

			// Level 3 categories.
			if (_selectedFilterOptions.Level2CategoryIds.Count == 1)
			{
				_availableFilterOptions.Level3Categories = SearchFacetGeneral.AllCategoriesWithoutFilter;
			}

			int? secondaryTemplateId = Block.SecondaryTemplateId;
			_availableFilterOptions.HasSecondaryTemplate = secondaryTemplateId.HasValue;

			// Page size options.
			_availableFilterOptions.PageSizeOptions = GetPageSizeOptions();

			// Sort options.
			_availableFilterOptions.SortOptions = new Dictionary<SortOption, string>
				{
					{SortOption.Popularity, AliasHelper.GetAliasValue("ProductFilter.SortOption.Popularity")},
					{SortOption.IsNewFrom, AliasHelper.GetAliasValue("ProductFilter.SortOption.IsNewFrom")},
					{SortOption.TitleAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleAsc")},
					{SortOption.TitleDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleDesc")},
					{SortOption.PriceAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceAsc")},
					{SortOption.PriceDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceDesc")},
					{SortOption.DiscountPercentDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.DiscountPercentDesc")}
				};
		}
		private void PopulateAvailableAgeAndPriceFilterOptions()
		{
			var context = UserContext.Current;
			Collection<IAgeInterval> ageIntervals = _ageIntervalService.GetAll(context);
			Collection<IPriceInterval> priceIntervals = _priceIntervalService.GetAll(context);
			_availableFilterOptions.AgeIntervalOuter = _ageIntervalService.GetAgeIntervalOuter(ageIntervals);
			_availableFilterOptions.PriceIntervalOuter = _priceIntervalService.GetPriceIntervalOuter(priceIntervals);
		}

		private IEnumerable<int> GetPageSizeOptions()
		{
			string settingName = CommonSession.IsViewportMobile ? "PageSizeOptionsMobile" : "PageSizeOptions";
			string settingPageSizeOptions = Template.GetSettingOrNull(settingName) ?? string.Empty;
			IEnumerable<int> pageSizeOptions = settingPageSizeOptions
				.Split(new[] { ',' })
				.Select(pageSize =>
				{
					int intPageSize;
					return int.TryParse(pageSize, out intPageSize) ? intPageSize : 0;
				})
				.Where(pageSize => pageSize > 0);

			if (!pageSizeOptions.Any())
			{
				return new List<int> { GetDefaultPageSize() };
			}
			return pageSizeOptions;
		}

		private int GetDefaultPageSize()
		{
			string settingName = CommonSession.IsViewportMobile ? "DefaultPageSizeMobile" : "DefaultPageSize";
			string settingDefaultPageSize = Template.GetSettingOrNull(settingName);
			int defaultPageSize;
			if (!int.TryParse(settingDefaultPageSize, out defaultPageSize))
			{
				defaultPageSize = 20;
			}
			return defaultPageSize;
		}

		private void SetSelectedFromQuery(ICollection<int> selectedItems, string parameter)
		{
			IEnumerable<int> items = GetSelectedFromQuery(parameter);
			foreach (int item in items)
			{
				selectedItems.Add(item);
			}
		}
		private IEnumerable<int> GetSelectedFromQuery(string parameter)
		{
			if (Request.QueryString[parameter].IsNullOrTrimmedEmpty())
			{
				return new int[0];
			}

			return Request.QueryString[parameter]
				.Split(new[] { ',' })
				.Select(value =>
				{
					int intValue;
					return int.TryParse(value, out intValue) ? intValue : 0;
				})
				.Where(value => value != 0).Distinct();
		}
		private void SetSelectedAgeFromQuery(SelectedFilterOptions selectedFilterOptions, string parameterFrom, string parameterTo)
		{
			int? fromMonth = Request.QueryString.GetInt32OrNull(parameterFrom);
			int? toMonth = Request.QueryString.GetInt32OrNull(parameterTo);

			if (fromMonth.HasValue || toMonth.HasValue)
			{
				selectedFilterOptions.AgeInterval = _ageIntervalService.GetAgeInterval(fromMonth, toMonth);
			}
		}
		private void SetSelectedPriceFromQuery(SelectedFilterOptions selectedFilterOptions, string parameterFrom, string parameterTo)
		{
			decimal? from = null;
			decimal? to = null;
			decimal parsedValue;

			if (!Request.QueryString[parameterFrom].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterFrom], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					from = parsedValue;
				}
			}

			if (!Request.QueryString[parameterTo].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterTo], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					to = parsedValue;
				}
			}

			if (from.HasValue || to.HasValue)
			{
				selectedFilterOptions.PriceInterval = _priceIntervalService.GetPriceInterval(from, to);
			}
		}
		private void SetSelectedTagIds()
		{
			var parameters = Request.QueryString.AllKeys.Distinct();
			foreach (string parameter in parameters)
			{
				if (parameter.IsEmpty()) continue;

				Match match = _tagGroupParameterRegex.Match(parameter);
				if (!match.Success) continue;

				int tagGroupId;
				if (!int.TryParse(match.Groups[1].Value, out tagGroupId)) continue;

				IEnumerable<int> tagIds = GetSelectedFromQuery(parameter);
				_selectedFilterOptions.GroupedTagIds.Add(tagIds);
			}
		}
		private void SetSearchMethodType()
		{
			var searchMethodTypeValue = Request.QueryString[FilterForm.SearchMethodTypeFormName];
			if (searchMethodTypeValue.HasValue())
			{
				if (searchMethodTypeValue.Equals(SearchMethodType.Regular.ToString("D"), StringComparison.InvariantCultureIgnoreCase))
				{
					_selectedFilterOptions.SearchMethodType = SearchMethodType.Regular;
				}
				else if (searchMethodTypeValue.Equals(SearchMethodType.Filter.ToString("D"), StringComparison.InvariantCultureIgnoreCase))
				{
					_selectedFilterOptions.SearchMethodType = SearchMethodType.Filter;
				}
			}
		}

		private EsalesFilterFormControl CreateFilterFormControl()
		{
			return new EsalesFilterFormControl
			{
				Template = Template,
				AvailableFilterOptions = _availableFilterOptions,
				FilterStatistics = SearchFacetByFilter,
				SelectedFilterOptions = _selectedFilterOptions
			};
		}

		protected override Template CreateTemplate()
		{
			if (_selectedFilterOptions.UseSecondaryTemplate && Block.SecondaryTemplateId.HasValue)
			{
				return _templateFactory.Create(Block.SecondaryTemplateId.Value);
			}

			return base.CreateTemplate();
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "BlockProductSearchEsales"; }
		}

		private bool IsXmlHttpRequest(HttpRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			string header = request.Headers["X-Requested-With"];
			if (header == null) return false;

			return header.Equals("XMLHttpRequest", StringComparison.OrdinalIgnoreCase);
		}
	}
}
