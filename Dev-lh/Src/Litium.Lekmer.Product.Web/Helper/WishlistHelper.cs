﻿namespace Litium.Lekmer.Product.Web.Helper
{
	public class WishListHelper
	{
		public string PostModeValue
		{
			get
			{
				return "wishlist-send";
			}
		}

		public string ProductIdName
		{
			get
			{
				return "id";
			}
		}

		public string SizetIdName
		{
			get
			{
				return "sizeid";
			}
		}

		public string ReferrerName
		{
			get
			{
				return "referrer";
			}
		}

		public string PackageProductIdsName
		{
			get
			{
				return "wishlist-package-product-ids";
			}
		}

		public string PackageProductSizetIdsName
		{
			get
			{
				return "wishlist-package-product-size-ids";
			}
		}
	}
}