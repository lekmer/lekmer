﻿using System;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockWishListSenderReceiver
{
	public class BlockWishListSenderSingleReceiverForm : ControlBase
	{
		private readonly string _postUrl;

		public BlockWishListSenderSingleReceiverForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string SenderEmailFormName
		{
			get { return "senderemail"; }
		}
		public string SenderNameFormName
		{
			get { return "sendername"; }
		}

		public string ReceiverFormName
		{
			get { return "receiver"; }
		}
		public string SenderFormName
		{
			get { return "sender"; }
		}
		public string MessageFormName
		{
			get { return "message"; }
		}

		public string MailToMyselfFormName
		{
			get { return "mailtomyself"; }
		}
		public string WishListPageUrlFormName
		{
			get { return "wishlistpageurl"; }
		}
		public string PostUrl
		{
			get { return _postUrl; }
		}
		public string PostModeValue
		{
			get { return "sendwishlist"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string WishListPageUrl { get; set; }
		public string Receiver { get; set; }
		public string Message { get; set; }
		public string Sender { get; set; }
		public bool MailToMyself { get; set; }
		public string SenderEmail { get; set; }
		public string SenderName { get; set; }
		public Guid WishListKey { get; set; }

		public void MapFromRequest()
		{
			Receiver = Request.Form[ReceiverFormName];
			Sender = Request.Form[SenderFormName];
			Message = Request.Form[MessageFormName];
			SenderEmail = Request.Form[SenderEmailFormName];
			SenderName = Request.Form[SenderNameFormName];
			var mailToMyself = Request.Form.GetBooleanOrNull(MailToMyselfFormName);
			if (mailToMyself.HasValue)
			{
				MailToMyself = mailToMyself.Value;
			}
			WishListPageUrl = Request.Form[WishListPageUrlFormName];
		}

		public void ClearFrom()
		{
			Receiver = string.Empty;
			Sender = string.Empty;
			Message = string.Empty;
			MailToMyself = false;
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Receiver.Name", ReceiverFormName);
			fragment.AddVariable("Form.Sender.Name", SenderFormName);
			fragment.AddVariable("Form.Message.Name", MessageFormName);
			fragment.AddVariable("Form.MailToMyself.Name", MailToMyselfFormName);
		}
		public void MapFieldsValueToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Receiver.Value", Receiver);
			fragment.AddVariable("Form.Sender.Value", Sender);
			fragment.AddVariable("Form.Message.Value", Message);
			fragment.AddCondition("MailToMyself", MailToMyself);
			fragment.AddVariable("WishListKey", WishListKey.ToString());
			fragment.AddCondition("Form.IsPostBack", IsFormPostBack);
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();
			ValidateSenderName(validationResult);
			ValidateEmail(validationResult);
			return validationResult;
		}

		private void ValidateEmail(ValidationResult validationResult)
		{
			if (Receiver.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Receiver.Validation.EmailEmpty"));
			}
			else if (!ValidationUtil.IsValidEmail(Receiver))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Receiver.Validation.EmailIncorrect"));
			}

			if (SenderEmail.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Sender.Validation.EmailEmpty"));
			}
			else if (!ValidationUtil.IsValidEmail(SenderEmail))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Sender.Validation.EmailIncorrect"));
			}
		}


		public void ValidateSenderName(ValidationResult validationResult)
		{
			if (SenderName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Sender.Validation.SenderEmpty"));
			}
		}
	}
}