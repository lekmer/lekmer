﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	/// <summary>
	/// Control for product relation list.
	/// </summary>
	public class BlockProductRelationListControl : BlockProductPageControlBase<IBlockProductRelationList>
	{
		private readonly IBlockProductRelationListService _blockProductRelationListService;
		private readonly ILekmerProductService _productService;
		private bool _showVariants;

		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods"),
		 SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ProductCollection Products { get; set; }

		protected IPagingControl PagingControl { get; set; }

		/// <summary>
		/// Initializes the <see cref="BlockProductRelationListControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="blockProductRelationListService"></param>
		/// <param name="productService"></param>
		public BlockProductRelationListControl(
			ITemplateFactory templateFactory,
			IBlockProductRelationListService blockProductRelationListService,
			ILekmerProductService productService) : base(templateFactory)
		{
			_blockProductRelationListService = blockProductRelationListService;
			_productService = productService;
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		/// <param name="blockId">Id of the block.</param>
		/// <returns>An instance of <see cref="IBlockProductRelationList"/>.</returns>
		protected override IBlockProductRelationList GetBlockById(int blockId)
		{
			return _blockProductRelationListService.GetById(UserContext.Current, blockId);
		}

		protected virtual void Initialize()
		{
			PagingControl = CreatePagingControl();
			Products = GetProducts();

			var currentBlock = ((IContentAreaFull)ContentArea).Blocks.FirstOrDefault(b => b.Id == Block.Id);
			if (currentBlock != null)
			{
				((ILekmerBlock) currentBlock).UseFallback = Products.Count < 1 && (Block.CanBeUsedAsFallbackOption ?? false);
			}
		}

		/// <summary>
		/// Gets the related products.
		/// </summary>
		/// <returns>Collection of products.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ProductCollection GetProducts()
		{
			return _productService.GetAllByBlockAndProduct(UserContext.Current, Block, Product.Id, _showVariants, PagingControl.SelectedPage, PagingControl.PageSize);
		}

		/// <summary>
		/// Initializes the paging control.
		/// </summary>
		/// <returns>Instance of <see cref="IPagingControl"/>.</returns>
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Product url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		/// <summary>
		/// Renders the control content.
		/// </summary>
		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render product relation list on this page ]");
			}

			// Check if need to show variants.
			_showVariants = ((ILekmerProductView) Product).ShowVariantRelations;

			Initialize();

			if (Products.Count < 1)
			{
				return new BlockContent();
			}

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		/// <summary>
		/// Renders the list of products.
		/// </summary>
		/// <returns>Rendered products.</returns>
		protected virtual string RenderProductList()
		{
			return CreateGridControl().Render();
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual GridControl<IProduct> CreateGridControl()
		{
			return new GridControl<IProduct>
			{
				Items = Products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};
		}
	}
}