using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Common.Web;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.Product.Web.BlockMonitorProduct;
using Litium.Lekmer.Product.Web.BlockProductCampaignList;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Lekmer.Product.Web.ProductPage.Cookie;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;
using ProductTypeConst = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockProductDetailControl : LekmerBlockControlBase<IBlock>
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IBlockService _blockService;
		private readonly IContentNodeService _contentNodeService;
		private readonly ILekmerProductService _productService;
		private readonly ISizeTableService _sizeTableService;
		private readonly IContentMessageService _contentMessageService;
		private readonly IEsalesGeneralServiceV2 _esalesGeneralServiceV2;
		private readonly ILekmerFormatter _formatter;
		private readonly IProductStoreStockService _productStoreStockService;
		private readonly List<string> _emptyTagGroups = new List<string>();
		private int _maxQuantity;
		private int? _sizeId;
		private bool _hasSizeTable;
		private ISizeTable _sizeTable;
		private Collection<IContentMessage> _contentMessages;
		private List<IProductStoreStock> _productStoreStock;
		private ILekmerProductView _lekmerProductView;
		private ICategoryTree _categoryTree;

		public LekmerBlockProductDetailControl(
			IBlockService blockService,
			ITemplateFactory templateFactory,
			IContentNodeService contentNodeService,
			ILekmerProductService productService,
			ISizeTableService sizeTableService,
			IContentMessageService contentMessageService,
			IEsalesGeneralServiceV2 esalesGeneralServiceV2,
			IFormatter formatter,
			IProductStoreStockService productStoreStockService) : base(templateFactory)
		{
			_blockService = blockService;
			_contentNodeService = contentNodeService;
			_productService = productService;
			_sizeTableService = sizeTableService;
			_contentMessageService = contentMessageService;
			_esalesGeneralServiceV2 = esalesGeneralServiceV2;
			_formatter = (ILekmerFormatter)formatter;
			_productStoreStockService = productStoreStockService;
		}

		protected ILekmerProductView LekmerProduct
		{
			get
			{
				return _lekmerProductView ?? (_lekmerProductView = (ILekmerProductView)Product);
			}
		}

		protected ICategoryTree CategoryTree
		{
			get
			{
				return _categoryTree ?? (_categoryTree = IoC.Resolve<ICategoryService>().GetAllAsTree(UserContext.Current));
			}
		}

		protected List<IProductStoreStock> ProductStoreStock
		{
			get
			{
				return _productStoreStock ?? (_productStoreStock = GetProductStoreStock());
			}
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		/// <param name="blockId">Id of the block.</param>
		/// <returns>An instance of <see cref="IBlock"/>.</returns>
		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render product on this page ]");
			}

			Initialize();

			string genericFilterPageUrl = GetGenericFilterPageUrl();

			Fragment fragmentContent = Template.GetFragment("Content");
			var fragmentContentInfo = new FragmentVariableInfo(fragmentContent);

			string trackingCode = Request.QueryString["trackingcode"];
			fragmentContent.AddVariable("TrackingCode", trackingCode);

			RenderMonthlypartPaymentCost(fragmentContent, fragmentContentInfo);

			fragmentContent.AddCondition("Product.IsRecommended", trackingCode.HasValue());
			fragmentContent.AddCondition("WasSearched", WasSearched);

			fragmentContent.AddVariable("Setting.MaxQuantity", _maxQuantity.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:QuantityOption", RenderQuantityOptions(LekmerProduct), VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:SizeOption", RenderSizeOptions(), VariableEncoding.None);
			
			fragmentContent.AddCondition("Product.HasIcons", LekmerProduct.Icons.Count > 0);
			fragmentContent.AddVariable("Iterate:Icon", RenderIcons(LekmerProduct.Icons), VariableEncoding.None);

			var isPackage = LekmerProduct.IsPackage();
			ProductCollection packageProducts;
			bool isPackageWithSizes = IsPackageWithSizes(isPackage, out packageProducts);
			fragmentContent.AddCondition("IsPackageWithSizes", isPackageWithSizes);
			fragmentContent.AddVariable("PackageProductList", isPackage ? RenderPackageProductList(packageProducts).Render() : string.Empty, VariableEncoding.None);
			RenderPackageGeneralInfo(fragmentContent, isPackage, LekmerProduct);

			RenderProductStoreStocks(fragmentContent, fragmentContentInfo);

			fragmentContent.AddRegexVariable("\\[TagGroup\\(\\\"(.+)\\\"\\)\\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;

					return RenderTagGroup(tagGroupCommonName, genericFilterPageUrl);
				},
				VariableEncoding.None
			);

			// Size Table.
			fragmentContent.AddCondition("HasSizeTable", _hasSizeTable);
			fragmentContent.AddVariable("SizeTable", _hasSizeTable ? RenderSizeTable() : string.Empty, VariableEncoding.None);

			// Content Message.
			RenderContentMessages(fragmentContent, fragmentContentInfo);

			// Monitor Product.
			RenderMonitorProduct(fragmentContent, fragmentContentInfo);

			// Wishlist..
			RenderWishListInfo(fragmentContent);

			// Campaigns
			RenderProductCampaignList(LekmerProduct, fragmentContent);

			// Add Product and Block entities at the end of rendering flow to map corresponding entities-variables in other fragments rendered into the Content fragment.
			fragmentContent.AddEntity(Product);
			fragmentContent.AddEntity(Block);

			var orderItemsArray = BuildOrderItemsArray();
			string head = RenderFragment("Head", trackingCode, orderItemsArray);
			string footer = RenderFragment("Footer", trackingCode, orderItemsArray);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}


		protected virtual void RenderContentMessages(Fragment fragmentContent, FragmentVariableInfo fragmentContentInfo)
		{
			if (fragmentContentInfo.HasCondition("HasContentMessage") ||
				fragmentContentInfo.HasVariable("ContentMessage") ||
				fragmentContentInfo.HasVariable("Iterate:ContentMessageOption"))
			{
				fragmentContent.AddCondition("HasContentMessage", _contentMessages.Count > 0);
				fragmentContent.AddVariable("ContentMessage", _contentMessages.Count > 0 ? _contentMessages.First().Message : string.Empty, VariableEncoding.None);
				fragmentContent.AddVariable("Iterate:ContentMessageOption", _contentMessages.Count > 0 ? RenderContentMessages() : string.Empty, VariableEncoding.None);
			}
		}

		protected virtual string RenderContentMessages()
		{
			var optionBuilder = new StringBuilder();
			foreach (var message in _contentMessages)
			{
				optionBuilder.AppendLine(RenderContentMessage(message));
			}
			return optionBuilder.ToString();
		}

		protected virtual string RenderContentMessage(IContentMessage message)
		{
			var fragmentOption = Template.GetFragment("ContentMessageOption");
			fragmentOption.AddVariable("Message", message.Message, VariableEncoding.None);
			return fragmentOption.Render();
		}


		private void RenderMonthlypartPaymentCost(Fragment fragmentContent, FragmentVariableInfo fragmentContentInfo)
		{
			if (PaymentHelper.IsKlarnaSupported(Channel.Current))
			{
				if (fragmentContentInfo.HasCondition("ShowMonthlyPartPaymentCost") || fragmentContentInfo.HasVariable("MonthlyPartPaymentCost"))
				{
					try
					{
						decimal monthlyPayment = (decimal)PaymentHelper.GetLowestPartPayment(Channel.Current, Product.CampaignInfo.Price.IncludingVat);

						fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", monthlyPayment > 0);
						fragmentContent.AddVariable("MonthlyPartPaymentCost", _formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyPayment));
					}
					catch (Exception ex)
					{
						_log.Error("Error while getting lowest part payment.", ex);

						fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", false);
						fragmentContent.AddVariable("MonthlyPartPaymentCost", "[ Error while getting lowest part payment. ]", VariableEncoding.None);
					}
				}
			}
			else
			{
				fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", false);
				fragmentContent.AddVariable("MonthlyPartPaymentCost", "[ Klarna payment not supported in current channel. ]");
			}
		}

		private string GetGenericFilterPageUrl()
		{
			var item = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, "filter");
			if (item == null || item.Url.IsEmpty())
			{
				return null;
			}

			return UrlHelper.ResolveUrlHttp(item.Url);
		}

		private string RenderFragment(string fragmentName, string trackingCode, string orderItemsArray)
		{
			Fragment fragment = Template.GetFragment(fragmentName);
			if (WasSearched)
			{
				fragment.AddVariable("SearchQuery", SearchQuery.EncodeHtml(), VariableEncoding.JavaScriptStringEncode);
			}
			fragment.AddCondition("WasSearched", WasSearched);
			fragment.AddVariable("TrackingCode", trackingCode);
			fragment.AddCondition("Product.IsRecommended", trackingCode.HasValue());
			fragment.AddEntity(Product);
			fragment.AddVariable("RemoveTagGroups", orderItemsArray, VariableEncoding.HtmlEncode);
			return fragment.Render();
		}

		private string RenderTagGroup(string commonName, string genericFilterPageUrl)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var tagGroup = LekmerProduct.TagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				_emptyTagGroups.Add(commonName);
				return null;
			}

			var fragmentTagGroup = Template.GetFragment("TagGroup");

			fragmentTagGroup.AddCondition("CountIsZero", tagGroup.Tags.Count == 0);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup, genericFilterPageUrl), VariableEncoding.None);

			return fragmentTagGroup.Render();
		}

		private string RenderTags(ITagGroup tagGroup, string genericFilterPageUrl)
		{
			var tagBuilder = new StringBuilder();
			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				string tagUrl = GetTagUrl(tag, genericFilterPageUrl);

				var fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				fragmentTag.AddVariable("Tag.Url", tagUrl);
				fragmentTag.AddCondition("Tag.HasUrl", tagUrl != null);

				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}
			return tagBuilder.ToString();
		}

		private static string GetTagUrl(ITag tag, string genericFilterPageUrl)
		{
			if (genericFilterPageUrl.IsEmpty())
			{
				return null;
			}

			var query = new QueryBuilder();

			query.Add("mode", "filter");
			query.Add("taggroup" + tag.TagGroupId + "-tag-id", tag.Id.ToString(CultureInfo.InvariantCulture));

			return genericFilterPageUrl + query;
		}

		private static void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}

		private void Initialize()
		{
			// Esales ticket V2
			InitializeEsalesTicketV2();

			if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
			{
				_maxQuantity = 10;
			}

			_sizeId = GetSizeId();

			if(!IsPostBack)
			{
				ClickHistoryCookie.AddProductToClickHistory(Product.Id);
			}

			if (LekmerProduct.HasSizes)
			{
				_sizeTable = _sizeTableService.GetByProduct(UserContext.Current, Product.Id);
				_hasSizeTable = _sizeTable != null;
			}
			else
			{
				_hasSizeTable = false;
			}

			_contentMessages = GetContentMessages();
		}

		protected virtual void InitializeEsalesTicketV2()
		{
			IProductPagePanelSettingV2 panelSetting = new ProductPagePanelSettingV2(UserContext.Current.Channel.CommonName);
			LekmerProduct.EsalesTicket = EsalesGeneralResponse == null
				? string.Empty
				: _esalesGeneralServiceV2.FindTicket(EsalesGeneralResponse, panelSetting.ProductInformationPanelName);
		}

		private string RenderQuantityOptions(ILekmerProductView lekmerProduct)
		{
			var maxItems = 0;
			if (lekmerProduct.IsBuyable)
			{
				maxItems = Math.Min(_maxQuantity, Product.NumberInStock);
			}
			else if (lekmerProduct.IsBookable)
			{
				maxItems = _maxQuantity;
			}

			var optionBuilder = new StringBuilder();

			for (int i = 1; i <= maxItems; i++)
			{
				var fragmentOption = Template.GetFragment("QuantityOption");

				fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));

				optionBuilder.AppendLine(fragmentOption.Render());
			}

			return optionBuilder.ToString();
		}

		private string RenderSizeOptions()
		{
			var optionBuilder = new StringBuilder();

			foreach (var size in LekmerProduct.ProductSizes)
			{
				optionBuilder.AppendLine(RenderSizeOption(size).Render());
			}

			return optionBuilder.ToString();
		}

		private Fragment RenderSizeOption(IProductSize productSize)
		{
			var fragmentOption = Template.GetFragment("SizeOption");

			fragmentOption.AddCondition("selected", _sizeId.HasValue ? _sizeId.Value == productSize.SizeInfo.Id : false);
			fragmentOption.AddEntity(productSize);

			// Size table

			bool hasSizeTableRow = false;
			string sizeTableRowColumn1Value = string.Empty;
			string sizeTableRowColumn2Value = string.Empty;

			if (_hasSizeTable)
			{
				ISizeTableRow sizeTableRow = _sizeTable.Rows.FirstOrDefault(r => r.SizeId == productSize.SizeInfo.Id);

				hasSizeTableRow = sizeTableRow != null;

				if (hasSizeTableRow)
				{
					sizeTableRowColumn1Value = sizeTableRow.Column1Value;
					sizeTableRowColumn2Value = sizeTableRow.Column2Value;
				}
			}

			fragmentOption.AddCondition("HasSizeTable", _hasSizeTable);
			fragmentOption.AddCondition("HasSizeTableRow", hasSizeTableRow);
			fragmentOption.AddVariable("SizeTableRow.Column1Value", sizeTableRowColumn1Value, VariableEncoding.None);
			fragmentOption.AddVariable("SizeTableRow.Column2Value", sizeTableRowColumn2Value, VariableEncoding.None);

			return fragmentOption;
		}

		private string RenderIcons(IEnumerable<IIcon> icons)
		{
			var iconBuilder = new StringBuilder();

			foreach (IIcon icon in icons)
			{
				var fragmentIcon = Template.GetFragment("Icon");

				fragmentIcon.AddEntity(icon);

				iconBuilder.AppendLine(fragmentIcon.Render());
			}

			return iconBuilder.ToString();
		}

		private string BuildOrderItemsArray()
		{
			var itemBuilder = new StringBuilder();
			itemBuilder.Append("new Array(");
			for (int i = 0; i < _emptyTagGroups.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", _emptyTagGroups[i]));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", _emptyTagGroups[i]));
				}
			}
			itemBuilder.Append(")");
			return itemBuilder.ToString();
		}

		private bool IsPackageWithSizes(bool isPackage, out ProductCollection packageProducts)
		{
			bool isPackageWithSizes = false;
			packageProducts = new ProductCollection();

			if (isPackage)
			{
				var productIds = _productService.GetIdAllByPackageMasterProduct(UserContext.Current, Product.Id);
				packageProducts = _productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
				if (packageProducts.Any(product => ((ILekmerProduct)product).HasSizes))
				{
					isPackageWithSizes = true;
				}
			}

			return isPackageWithSizes;
		}

		private Fragment RenderProductStoreStockOptions()
		{
			Fragment fragment = Template.GetFragment("ProductStoreStockList");
			var stringBuilder = new StringBuilder();
			if (LekmerProduct.HasSizes)
			{
				foreach (var size in LekmerProduct.ProductSizes)
				{
					var present = ProductStoreStock.Any(m => m.SizeInfo.Id == size.SizeInfo.Id);
					var fragmentOption = Template.GetFragment("ProductStoreStockOption");
					fragmentOption.AddVariable("ProductStoreStock.SizeId", size.SizeInfo.Id.ToString(CultureInfo.InvariantCulture));
					fragmentOption.AddVariable("ProductStoreStock.SizeTitle", size.SizeInfo.EuTitle.ToString(CultureInfo.InvariantCulture));
					fragmentOption.AddCondition("ProductStoreStock.IsInStock", present);
					stringBuilder.AppendLine(fragmentOption.Render());
				}
				fragment.AddVariable("Iterate:ProductStoreStockOption", stringBuilder.ToString(), VariableEncoding.None);
			}
			else
			{
				var stock = ProductStoreStock.FirstOrDefault();
				if (stock != null)
				{
					var fragmentOption = Template.GetFragment("ProductStoreStockOption");
					fragmentOption.AddVariable("ProductStoreStock.SizeId", stock.SizeInfo.Id.ToString(CultureInfo.InvariantCulture));
					fragmentOption.AddVariable("ProductStoreStock.SizeTitle", stock.SizeInfo.EuTitle.ToString(CultureInfo.InvariantCulture));
					fragmentOption.AddCondition("ProductStoreStock.IsInStock", true);
					stringBuilder.AppendLine(fragmentOption.Render());
					fragment.AddVariable("Iterate:ProductStoreStockOption", stringBuilder.ToString(), VariableEncoding.None);
				}
			}
			return fragment;
		}

		protected virtual Fragment RenderPackageProductList(ProductCollection products)
		{
			Fragment fragment = Template.GetFragment("PackageProductList");

			var stringBuilder = new StringBuilder();
			foreach (var product in products)
			{
				stringBuilder.AppendLine(RenderPackageProduct(product).Render());
			}

			fragment.AddVariable("Iterate:PackageProduct", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderPackageProduct(IProduct product)
		{
			Fragment fragment = Template.GetFragment("PackageProduct");

			fragment.AddEntity(product);

			var lekmerProductView = (ILekmerProductView)product;

			ISizeTable sizeTable = null;
			if (lekmerProductView.HasSizes)
			{
				sizeTable = _sizeTableService.GetByProduct(UserContext.Current, product.Id);
			}

			fragment.AddVariable("PackageProductSizeList", RenderPackageProductSizeList(lekmerProductView.ProductSizes, sizeTable).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderPackageProductSizeList(Collection<IProductSize> sizes, ISizeTable sizeTable)
		{
			Fragment fragment = Template.GetFragment("PackageProductSizeList");

			var stringBuilder = new StringBuilder();

			foreach (var size in sizes)
			{
				stringBuilder.AppendLine(RenderPackageProductSize(size, sizeTable).Render());
			}

			fragment.AddVariable("Iterate:PackageProductSize", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderPackageProductSize(IProductSize size, ISizeTable sizeTable)
		{
			Fragment fragment = Template.GetFragment("PackageProductSize");

			fragment.AddEntity(size);

			bool hasSizeTable = sizeTable != null;
			bool hasSizeTableRow = false;
			string sizeTableRowColumn1Value = string.Empty;
			string sizeTableRowColumn2Value = string.Empty;

			if (hasSizeTable)
			{
				ISizeTableRow sizeTableRow = sizeTable.Rows.FirstOrDefault(r => r.SizeId == size.SizeInfo.Id);

				hasSizeTableRow = sizeTableRow != null;

				if (hasSizeTableRow)
				{
					sizeTableRowColumn1Value = sizeTableRow.Column1Value;
					sizeTableRowColumn2Value = sizeTableRow.Column2Value;
					
				}
			}

			fragment.AddCondition("HasSizeTable", hasSizeTable);
			fragment.AddCondition("HasSizeTableRow", hasSizeTableRow);
			fragment.AddVariable("SizeTableRow.Column1Value", sizeTableRowColumn1Value, VariableEncoding.None);
			fragment.AddVariable("SizeTableRow.Column2Value", sizeTableRowColumn2Value, VariableEncoding.None);

			return fragment;
		}

		protected virtual void RenderProductCampaignList(ILekmerProduct product, Fragment fragmentContent)
		{
			var productCampaignListControl = IoC.Resolve<IProductCampaignListControl>();

			productCampaignListControl.Product = product;
			productCampaignListControl.Template = Template;
			productCampaignListControl.FragmentContent = fragmentContent;

			productCampaignListControl.Render();
		}

		protected virtual string RenderSizeTable()
		{
			Fragment fragment = Template.GetFragment("SizeTable");

			fragment.AddVariable("SizeTable.Title", _sizeTable.Title, VariableEncoding.None);
			fragment.AddVariable("SizeTable.Description", _sizeTable.Description, VariableEncoding.None);
			fragment.AddVariable("SizeTable.Column1Title", _sizeTable.Column1Title, VariableEncoding.None);
			fragment.AddVariable("SizeTable.Column2Title", _sizeTable.Column2Title, VariableEncoding.None);

			fragment.AddCondition("SizeTable.Description.HasValue", _sizeTable.Description.HasValue());

			fragment.AddVariable("Iterate:SizeTableRow", RenderSizeTableRows(), VariableEncoding.None);

			fragment.AddVariable("Iterate:SizeTableMedia", RenderSizeTableMediaItems(), VariableEncoding.None);

			return fragment.Render();
		}

		protected virtual string RenderSizeTableRows()
		{
			var stringBuilder = new StringBuilder();

			foreach (var sizeTableRow in _sizeTable.Rows)
			{
				stringBuilder.AppendLine(RenderSizeTableRow(sizeTableRow).Render());
			}

			return stringBuilder.ToString();
		}

		protected virtual Fragment RenderSizeTableRow(ISizeTableRow sizeTableRow)
		{
			Fragment fragment = Template.GetFragment("SizeTableRow");

			fragment.AddVariable("SizeTableRow.Column1Value", sizeTableRow.Column1Value, VariableEncoding.None);
			fragment.AddVariable("SizeTableRow.Column2Value", sizeTableRow.Column2Value, VariableEncoding.None);

			return fragment;
		}

		protected virtual string RenderSizeTableMediaItems()
		{
			var stringBuilder = new StringBuilder();

			foreach (var sizeTableMediaItem in _sizeTable.MediaItems)
			{
				if (sizeTableMediaItem.Image == null)
				{
					stringBuilder.AppendLine(RenderSizeTableMediaItem(sizeTableMediaItem.MediaItem).Render());
				}
				else
				{
					stringBuilder.AppendLine(RenderSizeTableImage(sizeTableMediaItem.Image).Render());
				}
			}

			return stringBuilder.ToString();
		}

		protected virtual Fragment RenderSizeTableMediaItem(IMediaItem mediaItem)
		{
			Fragment fragment = Template.GetFragment("SizeTableMediaItem");

			fragment.AddEntity(mediaItem);

			return fragment;
		}

		protected virtual Fragment RenderSizeTableImage(IImage image)
		{
			Fragment fragment = Template.GetFragment("SizeTableImage");

			fragment.AddEntity(image);

			return fragment;
		}

		protected virtual void RenderPackageGeneralInfo(Fragment content, bool isPackage, ILekmerProduct product)
		{
			if (isPackage)
			{
				// Populate package general info.
				var package = IoC.Resolve<IPackageService>().GetByMasterProduct(UserContext.Current, Product.Id);
				if (package.GeneralInfo != null)
				{
					PopulateBrandPackageInfo(content, false, string.Empty);
					PopulateCategoryPackageInfo(content, false, string.Empty);
					PopulatePackageGeneralInfo(content, true, package.GeneralInfo);
					return;
				}
				PopulatePackageGeneralInfo(content, false, string.Empty);

				// Populate brand package info.
				var brand = product.Brand;
				if (brand != null && brand.PackageInfo.HasValue())
				{
					PopulateBrandPackageInfo(content, true, brand.PackageInfo);
				}
				else
				{
					PopulateBrandPackageInfo(content, false, string.Empty);
				}

				// Populate category package info.
				var packageInfo = GetCategoryPackageInfo(CategoryTree, Product.CategoryId);
				if (packageInfo.HasValue())
				{
					PopulateCategoryPackageInfo(content, true, packageInfo);
				}
				else
				{
					PopulateCategoryPackageInfo(content, false, string.Empty);
				}
			}
			else
			{
				PopulateBrandPackageInfo(content, false, string.Empty);
				PopulateCategoryPackageInfo(content, false, string.Empty);
				PopulatePackageGeneralInfo(content, false, string.Empty);
			}
		}
		protected virtual string GetCategoryPackageInfo(ICategoryTree categoryTree, int categoryId)
		{
			var categoryTreeItem = categoryTree.FindItemById(categoryId);
			if (categoryTreeItem != null && categoryTreeItem.Category != null)
			{
				var category = (ILekmerCategory)categoryTreeItem.Category;
				if (category.PackageInfo.HasValue())
				{
					return category.PackageInfo;
				}
				
				if (category.ParentCategoryId.HasValue)
				{
					return GetCategoryPackageInfo(categoryTree, category.ParentCategoryId.Value);
				}

				return null;
			}

			return null;
		}
		protected virtual void PopulateBrandPackageInfo(Fragment content, bool conditionValue, string variableValue)
		{
			content.AddCondition("HasBrandPackageInfo", conditionValue);
			content.AddVariable("BrandPackageInfo", variableValue, VariableEncoding.None);
		}
		protected virtual void PopulateCategoryPackageInfo(Fragment content, bool conditionValue, string variableValue)
		{
			content.AddCondition("HasCategoryPackageInfo", conditionValue);
			content.AddVariable("CategoryPackageInfo", variableValue, VariableEncoding.None);
		}
		protected virtual void PopulatePackageGeneralInfo(Fragment content, bool conditionValue, string variableValue)
		{
			content.AddCondition("HasPackagGeneralInfo", conditionValue);
			content.AddVariable("PackagGeneralInfo", variableValue, VariableEncoding.None);
		}

		protected virtual void RenderMonitorProduct(Fragment fragmentContent, FragmentVariableInfo fragmentContentInfo)
		{
			if (fragmentContentInfo.HasVariable("MonitorProduct"))
			{
				fragmentContent.AddVariable("MonitorProduct", RenderMonitorProduct(), VariableEncoding.None);
			}
		}
		protected virtual string RenderMonitorProduct()
		{
			var fragment = Template.GetFragment("MonitorProduct");
			
			var control = new MonitorProductControl((ILekmerProduct)Product, fragment);
			control.Render();

			return fragment.Render();
		}

		protected virtual void RenderWishListInfo(Fragment fragmentContent)
		{
			var helper = new WishListHelper();

			var postUrl = UrlHelper.BaseUrlHttp + "/wishlist";
			fragmentContent.AddVariable("WishList.Form.PostUrl", postUrl, VariableEncoding.None);
			fragmentContent.AddVariable("WishList.Form.PostMode.Name", PostModeName);
			fragmentContent.AddVariable("WishList.Form.PostMode.Value", helper.PostModeValue);
			fragmentContent.AddVariable("WishList.Form.ProductId.Name", helper.ProductIdName);
			fragmentContent.AddVariable("WishList.Form.SizeId.Name", helper.SizetIdName);
			fragmentContent.AddVariable("WishList.Form.PackageProductIds.Name", helper.PackageProductIdsName);
			fragmentContent.AddVariable("WishList.Form.PackageProductSizeIds.Name", helper.PackageProductSizetIdsName);
			fragmentContent.AddVariable("WishList.Form.Referrer.Name", helper.ReferrerName);
		}

		private bool WasSearched
		{
			get
			{
				return Request.UrlReferrer == null ? false : Request.UrlReferrer.Query.Contains("q");
			}
		}

		private string SearchQuery
		{
			get
			{
				var urlString = Request.UrlReferrer.ToString();
				int startIndex = urlString.IndexOf("q=") + 2;
				int length = urlString.Substring(startIndex).IndexOf("&") == -1
								? urlString.Length - startIndex
								: urlString.Substring(startIndex).IndexOf("&");
				return urlString.Substring(startIndex, length);
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected int? GetSizeId()
		{
			int sizeId;
			if (!int.TryParse(Request.QueryString["sizeid"], out sizeId))
			{
				return null;
			}
			return sizeId;
		}

		protected Collection<IContentMessage> GetContentMessages()
		{
			var result = new Collection<IContentMessage>();

			var contentMessagesAll = _contentMessageService.GetAllByProduct(UserContext.Current, Product.Id).Where(cm => cm.IsInTimeLimit);

			if (!LekmerProduct.IsDropShip)
			{
				foreach (var contentMessage in contentMessagesAll)
				{
					if (contentMessage.IsDropShip) continue;
					result.Add(contentMessage);
				}
			}
			else
			{
				result = new Collection<IContentMessage>(contentMessagesAll.ToList());
			}

			return result;
		}
		
		protected List<IProductStoreStock> GetProductStoreStock()
		{
			var categoryTreeItem = CategoryTree.FindItemById(Product.CategoryId);
			var category = (ILekmerCategory)categoryTreeItem.Category;
			
			return _productStoreStockService.GetAllByProduct(Product.Id).Where(m => m.NumberInStock >= category.MinQuantityInStock).ToList();
		}

		protected virtual void RenderProductStoreStocks(Fragment fragmentContent, FragmentVariableInfo fragmentContentInfo)
		{
			if (fragmentContentInfo.HasCondition("IsInPhysicalStore"))
			{
				fragmentContent.AddCondition("IsInPhysicalStore", ProductStoreStock.Any());
			}

			if (fragmentContentInfo.HasVariable("ProductStoreStockList"))
			{
				fragmentContent.AddVariable("ProductStoreStockList", RenderProductStoreStockOptions().Render(), VariableEncoding.None);
			}
		}
	}
}