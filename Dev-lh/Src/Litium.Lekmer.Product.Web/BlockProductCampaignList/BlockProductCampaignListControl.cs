﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockProductCampaignList
{
	public class BlockProductCampaignListControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IBlockService _blockService;

		public BlockProductCampaignListControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory)
		{
			_blockService = blockService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var lekmerProduct = (ILekmerProduct)Product;

			if (lekmerProduct == null)
			{
				return new BlockContent(null, "[ Can't render product on this page ]");
			}

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			Fragment fragmentContent = Template.GetFragment("Content");

			var productCampaignListControl = IoC.Resolve<IProductCampaignListControl>();

			productCampaignListControl.Product = lekmerProduct;
			productCampaignListControl.Template = Template;
			productCampaignListControl.FragmentContent = fragmentContent;

			productCampaignListControl.Render();

			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}
	}
}