﻿using System;
using System.Text;
using Litium.Scensum.Core.Web;

namespace Litium.Lekmer.Product.Web
{
	public class FlagListControl<TItem> : ListControl<TItem> where TItem : class
	{
		/// <summary>
		/// Renders the items.
		/// Calls RenderItem for each item.
		/// </summary>
		/// <returns>Rendered items.</returns>
		protected override string RenderItems()
		{
			var itemBuilder = new StringBuilder();

			int i = 0;
			foreach (TItem item in Items)
			{
				itemBuilder.AppendLine(RenderItem(i++, item));
			}

			itemBuilder.Length -= Environment.NewLine.Length;

			return itemBuilder.ToString();
		}
	}
}
