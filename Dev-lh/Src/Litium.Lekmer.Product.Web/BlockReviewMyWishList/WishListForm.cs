﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockReviewMyWishList
{
	public class WishListForm : ControlBase
	{
		private readonly string _postUrl;

		public WishListForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string WishListTitleFormName
		{
			get { return "wishlisttitle"; }
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "wishlist"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string WishListTitle { get; set; }

		public void MapFromRequest()
		{
			WishListTitle = Request.Form[WishListTitleFormName];
		}

		public void ClearFrom()
		{
			WishListTitle = string.Empty;
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.WishListTitle.Name", WishListTitleFormName);
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.WishListTitle.Value", WishListTitle);
			fragment.AddCondition("Form.IsPostBack", IsFormPostBack);
		}
	}
}