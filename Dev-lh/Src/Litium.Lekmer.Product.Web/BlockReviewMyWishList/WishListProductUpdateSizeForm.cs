﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockReviewMyWishList
{
	public class WishListProductUpdateSizeForm : ControlBase
	{
		private readonly string _postUrl;

		public WishListProductUpdateSizeForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "wishlist_product_update_size"; }
		}

		public string WishListItemIdName
		{
			get { return "wishlistitem_id"; }
		}

		public string WishListItemProductIdName
		{
			get { return "wishlistitem_product_id"; }
		}

		public string WishListItemProductSizeIdName
		{
			get { return "wishlistitem_product_size_id"; }
		}

		public int WishListItemId { get; set; }
		public int ProductId { get; set; }
		public int ProductSizeId { get; set; }

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("ProductUpdateSizeForm.PostMode.Name", PostModeName);
			fragment.AddVariable("ProductUpdateSizeForm.PostMode.Value", PostModeValue);
			fragment.AddVariable("ProductUpdateSizeForm.PostUrl", PostUrl);
			fragment.AddVariable("ProductUpdateSizeForm.WishListItemId.Name", WishListItemIdName);
			fragment.AddVariable("ProductUpdateSizeForm.ProductId.Name", WishListItemProductIdName);
			fragment.AddVariable("ProductUpdateSizeForm.ProductSizeId.Name", WishListItemProductSizeIdName);
		}

		public bool MapFromRequest()
		{
			int wishListItemId;
			if (int.TryParse(Request.Form[WishListItemIdName], out wishListItemId))
			{
				WishListItemId = wishListItemId;
			}
			else
			{
				return false;
			}

			int productId;
			if (int.TryParse(Request.Form[WishListItemProductIdName], out productId))
			{
				ProductId = productId;
			}
			else
			{
				return false;
			}

			int sizeId;
			if (int.TryParse(Request.Form[WishListItemProductSizeIdName], out sizeId))
			{
				ProductSizeId = sizeId;
			}
			else
			{
				return false;
			}

			return true;
		}
	}
}