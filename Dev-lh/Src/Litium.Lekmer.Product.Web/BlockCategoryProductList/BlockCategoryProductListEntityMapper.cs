using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockCategoryProductListEntityMapper : BlockEntityMapper<IBlockCategoryProductList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockCategoryProductList item)
		{
			base.AddEntityVariables(fragment, item);
			LekmerBlockHelper.AddLekmerBlockVariables(fragment, item);
		}
	}
}