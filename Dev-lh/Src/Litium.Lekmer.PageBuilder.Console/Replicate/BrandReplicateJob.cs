using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class BrandReplicateJob : JobBase
	{
		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();

		private readonly IBrandSiteStructureRegistrySecureService _brandSiteStructureRegistrySecureService = IoC.Resolve<IBrandSiteStructureRegistrySecureService>();

		private readonly IBrandSecureService _brandSecureService = IoC.Resolve<IBrandSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _siteStructureRegistryId;
		private readonly int _siteStructureRegistryIdSource;
		private NodeTreeReplicationIndex _replicationIndex;

		public BrandReplicateJob(Configuration configuration, ISystemUserFull systemUser, int siteStructureRegistryId, int siteStructureRegistryIdSource)
			: base(configuration)
		{
			_systemUser = systemUser;
			_siteStructureRegistryId = siteStructureRegistryId;
			_siteStructureRegistryIdSource = siteStructureRegistryIdSource;
		}

		internal override void Initialize()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			_replicationIndex = new NodeTreeReplicationIndex();

			Collection<IContentNode> replicatedNodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryId);
			_replicationIndex.Initialize(replicatedNodes);
		}

		protected override void ExecuteCore()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			Console.WriteLine("-replication brands starts");

			var brands = _brandSecureService.GetAll();

			foreach (IBrand brand in brands)
			{
				SaveBrandContentNodeConnection(brand);
			}

			Console.WriteLine("-replication brands done");
		}


		private void SaveBrandContentNodeConnection(IBrand brand)
		{
			var brandSiteStructureRegistrySource = _brandSiteStructureRegistrySecureService.GetAllByBrand(brand.Id)
				.FirstOrDefault(creg => creg.SiteStructureRegistryId == _siteStructureRegistryIdSource);

			if (brandSiteStructureRegistrySource != null)
			{
				IBrandSiteStructureRegistry replicatedBrandSiteStructureRegistry = _brandSiteStructureRegistrySecureService.Create();

				replicatedBrandSiteStructureRegistry.BrandId = brand.Id;
				replicatedBrandSiteStructureRegistry.SiteStructureRegistryId = _siteStructureRegistryId;
				replicatedBrandSiteStructureRegistry.ContentNodeId = _replicationIndex.GetReplicatedNodeId(brandSiteStructureRegistrySource.ContentNodeId);

				if (replicatedBrandSiteStructureRegistry.ContentNodeId > 0)
				{
					_brandSiteStructureRegistrySecureService.Save(_systemUser, replicatedBrandSiteStructureRegistry);
				}
				else
				{
					Console.WriteLine("--Warning, some BrandSiteStructureRegistry is not replicated !!!");
				}
			}
		}
	}
}