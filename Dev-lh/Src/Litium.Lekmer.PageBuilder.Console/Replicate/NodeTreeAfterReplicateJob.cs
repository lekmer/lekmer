using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class NodeTreeAfterReplicateJob : JobBase
	{
		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _siteStructureRegistryId;
		private readonly int _siteStructureRegistryIdSource;
		private NodeTreeReplicationIndex _replicationIndex;

		public NodeTreeAfterReplicateJob(Configuration configuration, ISystemUserFull systemUser, int siteStructureRegistryId, int siteStructureRegistryIdSource)
			: base(configuration)
		{
			_systemUser = systemUser;
			_siteStructureRegistryId = siteStructureRegistryId;
			_siteStructureRegistryIdSource = siteStructureRegistryIdSource;
		}

		internal override void Initialize()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			_replicationIndex = new NodeTreeReplicationIndex();

			Collection<IContentNode> replicatedNodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryId);
			_replicationIndex.Initialize(replicatedNodes);
		}

		protected override void ExecuteCore()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			Console.WriteLine("-replication fix starts");

			Collection<IContentNode> nodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryIdSource);

			foreach (IContentNode node in nodes)
			{
				FixReplicatedNode(node);
			}

			Console.WriteLine("-replication fix done");
		}


		private void FixReplicatedNode(IContentNode sourceNode)
		{
			IContentNode replicatedNode = _replicationIndex.GetReplicatedNode(sourceNode.Id);

			if (replicatedNode != null)
			{
				replicatedNode.CommonName = _replicationIndex.GetNormalCommonName(replicatedNode.CommonName);

				_contentNodeSecureService.Save(_systemUser, replicatedNode);
			}
		}
	}
}