using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.TopList;

namespace Litium.Lekmer.PageBuilder
{
	internal class BlockReplicateJob : JobBase
	{
		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();

		private readonly IBlockSecureService _blockSecureService = IoC.Resolve<IBlockSecureService>();

		private readonly IBlockProductFilterSecureService _blockProductFilterSecureService = IoC.Resolve<IBlockProductFilterSecureService>();

		private readonly IBlockBrandListSecureService _blockBrandListSecureService = IoC.Resolve<IBlockBrandListSecureService>();
		private readonly IBlockBrandListBrandSecureService _blockBrandListBrandSecureService = IoC.Resolve<IBlockBrandListBrandSecureService>();
		
		private readonly IBlockRichTextSecureService _blockRichTextSecureService = IoC.Resolve<IBlockRichTextSecureService>();
		
		private readonly IBlockTopListSecureService _blockTopListSecureService = IoC.Resolve<IBlockTopListSecureService>();
		private readonly IBlockTopListCategorySecureService _blockTopListCategorySecureService = IoC.Resolve<IBlockTopListCategorySecureService>();
		private readonly IBlockTopListProductSecureService _blockTopListProductSecureService = IoC.Resolve<IBlockTopListProductSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _siteStructureRegistryId;
		private readonly int _siteStructureRegistryIdSource;
		private NodeTreeReplicationIndex _replicationIndex;

		public BlockReplicateJob(Configuration configuration, ISystemUserFull systemUser, int siteStructureRegistryId, int siteStructureRegistryIdSource)
			: base(configuration)
		{
			_systemUser = systemUser;
			_siteStructureRegistryId = siteStructureRegistryId;
			_siteStructureRegistryIdSource = siteStructureRegistryIdSource;
		}

		internal override void Initialize()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			_replicationIndex = new NodeTreeReplicationIndex();

			Collection<IContentNode> replicatedNodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryId);
			_replicationIndex.Initialize(replicatedNodes);
		}

		protected override void ExecuteCore()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			Console.WriteLine("-replication blocks starts");

			Collection<IContentNode> nodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryIdSource);

			foreach (IContentNode node in nodes)
			{
				if (node.ContentNodeTypeId == (int)ContentNodeTypeInfo.Page)
				{
					ReplicatePageBlocks(node);
				}
			}

			Console.WriteLine("-replication blocks done");
		}


		private void ReplicatePageBlocks(IContentNode node)
		{
			Collection<IBlock> blocks = _blockSecureService.GetAllByContentNode(node.Id);

			if (blocks != null)
			{
				foreach (IBlock block in blocks)
				{
					ReplicateBlock(block);
				}
			}
		}

		private void ReplicateBlock(IBlock block)
		{
			if (block.BlockType.CommonName == BlockTypeInfo.ProductFilter.ToString())
			{
				ReplicateBlockProductFilter(block);
			}
			else if (block.BlockType.CommonName == BlockTypeInfo.BrandList.ToString())
			{
				ReplicateBlockBrandList(block);
			}
			else if (block.BlockType.CommonName == BlockTypeInfo.ProductTopList.ToString())
			{
				ReplicateBlockProductTopList(block);
			}
			else if (block.BlockType.CommonName == BlockTypeInfo.HelloWorld.ToString())
			{
				ReplicateBlockHelloWorld(block);
			}
			else if (block.BlockType.CommonName == BlockTypeInfo.RichText.ToString())
			{
				ReplicateBlockRichText(block);
			}
		}


		private void ReplicateBlockProductFilter(IBlock block)
		{
			IBlockProductFilter sourceBlock = _blockProductFilterSecureService.GetByIdSecure(block.Id);
			IBlockProductFilter replicatedBlock = _blockProductFilterSecureService.Create();

			ReplicateBlockProperties(sourceBlock, replicatedBlock);

			replicatedBlock.SecondaryTemplateId = sourceBlock.SecondaryTemplateId;
			replicatedBlock.DefaultCategoryId = sourceBlock.DefaultCategoryId;
			replicatedBlock.DefaultPriceIntervalId = sourceBlock.DefaultPriceIntervalId;
			replicatedBlock.DefaultAgeIntervalId = sourceBlock.DefaultAgeIntervalId;
			replicatedBlock.DefaultSizeId = sourceBlock.DefaultSizeId;
			replicatedBlock.DefaultBrandIds = sourceBlock.DefaultBrandIds;
			replicatedBlock.DefaultTagIds = sourceBlock.DefaultTagIds;
			replicatedBlock.ProductListCookie = sourceBlock.ProductListCookie;
			replicatedBlock.DefaultSort = sourceBlock.DefaultSort;

			_blockProductFilterSecureService.Save(_systemUser, replicatedBlock);
		}

		private void ReplicateBlockBrandList(IBlock block)
		{
			IBlockBrandList sourceBlock = _blockBrandListSecureService.GetById(block.Id);
			IBlockBrandList replicatedBlock = _blockBrandListSecureService.Create();

			ReplicateBlockProperties(sourceBlock, replicatedBlock);
			replicatedBlock.Setting.SetValues(sourceBlock.Setting);
			replicatedBlock.IncludeAllBrands = sourceBlock.IncludeAllBrands;
			if (sourceBlock.LinkContentNodeId.HasValue)
			{
				replicatedBlock.LinkContentNodeId = _replicationIndex.GetReplicatedNodeId(sourceBlock.LinkContentNodeId.Value);
			}

			Collection<IBlockBrandListBrand> blockBrandListBrands = _blockBrandListBrandSecureService.GetAllByBlock(sourceBlock.Id);
			foreach (IBlockBrandListBrand blockBrandListBrand in blockBrandListBrands)
			{
				blockBrandListBrand.BlockId = 0;
			}

			_blockBrandListSecureService.Save(_systemUser, replicatedBlock, blockBrandListBrands);
		}

		private void ReplicateBlockProductTopList(IBlock block)
		{
			IBlockTopList sourceBlock = _blockTopListSecureService.GetById(block.Id);
			IBlockTopList replicatedBlock = _blockTopListSecureService.Create();

			ReplicateBlockProperties(sourceBlock, replicatedBlock);

			replicatedBlock.Setting.SetValues(sourceBlock.Setting);
			replicatedBlock.IncludeAllCategories = sourceBlock.IncludeAllCategories;
			replicatedBlock.OrderStatisticsDayCount = sourceBlock.OrderStatisticsDayCount;

			replicatedBlock.Id = _blockTopListSecureService.Save(_systemUser, replicatedBlock);

			Collection<IBlockTopListCategory> topListCategories = _blockTopListCategorySecureService.GetAllByBlock(sourceBlock.Id);
			Collection<IBlockTopListProduct> topListProducts = _blockTopListProductSecureService.GetAllByBlock(Configuration.ChannelId, sourceBlock.Id);

			_blockTopListCategorySecureService.Save(replicatedBlock.Id, topListCategories);
			_blockTopListProductSecureService.Save(replicatedBlock.Id, topListProducts);
		}

		private void ReplicateBlockHelloWorld(IBlock block)
		{
			IBlock sourceBlock = _blockSecureService.GetById(block.Id);
			IBlock replicatedBlock = _blockSecureService.Create();

			ReplicateBlockProperties(sourceBlock, replicatedBlock);

			_blockSecureService.Save(_systemUser, replicatedBlock);
		}

		private void ReplicateBlockRichText(IBlock block)
		{
			IBlockRichText sourceBlock = _blockRichTextSecureService.GetById(block.Id);

			if (sourceBlock != null)
			{
				IBlockRichText replicatedBlock = _blockRichTextSecureService.Create();

				ReplicateBlockProperties(sourceBlock, replicatedBlock);

				replicatedBlock.Content = sourceBlock.Content;

				_blockRichTextSecureService.Save(_systemUser, replicatedBlock);
			}
		}

		private void ReplicateBlockProperties(IBlock sourceBlock, IBlock replicatedBlock)
		{
			replicatedBlock.BlockStatusId = sourceBlock.BlockStatusId;
			replicatedBlock.BlockTypeId = sourceBlock.BlockTypeId;
			replicatedBlock.ContentAreaId = sourceBlock.ContentAreaId;
			replicatedBlock.ContentNodeId = _replicationIndex.GetReplicatedNodeId(sourceBlock.ContentNodeId);
			replicatedBlock.Ordinal = sourceBlock.Ordinal;
			replicatedBlock.Title = sourceBlock.Title;
			replicatedBlock.AccessId = sourceBlock.AccessId;
			replicatedBlock.TemplateId = sourceBlock.TemplateId;
		}
	}
}