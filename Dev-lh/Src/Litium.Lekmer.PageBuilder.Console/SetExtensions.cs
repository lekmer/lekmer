using System;

namespace Litium.Lekmer.PageBuilder
{
	public static class SetExtensions
	{
		public static T Set<T>(this T obj, Action<T> action)
		{
			action(obj);
			return obj;
		}
	}
}