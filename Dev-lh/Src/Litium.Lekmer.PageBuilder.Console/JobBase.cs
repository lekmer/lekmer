namespace Litium.Lekmer.PageBuilder
{
	internal abstract class JobBase
	{
		internal JobBase(Configuration configuration)
		{
			Configuration = configuration;
		}

		protected Configuration Configuration { get; private set; }

		internal virtual void Initialize()
		{
		}

		protected abstract void ExecuteCore();

		internal void Execute()
		{
			Initialize();

			ExecuteCore();
		}
	}
}