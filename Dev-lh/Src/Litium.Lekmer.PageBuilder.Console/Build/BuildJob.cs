using System;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class BuildJob : JobBase
	{
		private readonly IChannelSecureService _channelSecureService =
			IoC.Resolve<IChannelSecureService>();

		private readonly ISiteStructureRegistrySecureService _siteStructureRegistrySecureService =
			IoC.Resolve<ISiteStructureRegistrySecureService>();

		private readonly ISystemUserSecureService _systemUserSecureService =
			IoC.Resolve<ISystemUserSecureService>();

		private int _languageId;
		private int _siteStructureRegistryId;
		private ISystemUserFull _systemUser;

		public BuildJob(Configuration configuration) : base(configuration)
		{
		}

		protected override void ExecuteCore()
		{
			var categoryBuildJob = new CategoryBuildJob(Configuration, _systemUser, _languageId, _siteStructureRegistryId);
			
			try
			{
				categoryBuildJob.Execute();
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex);
				Console.WriteLine("Press any key to continue");
				Console.ReadLine();
			}

			//var brandBuildJob = new BrandBuildJob(Configuration, _systemUser, _languageId, _siteStructureRegistryId);
			//brandBuildJob.Execute();
		}

		internal override void Initialize()
		{
			IChannel channel = _channelSecureService.GetById(Configuration.ChannelId);
			_languageId = channel.Language.Id;

			ISiteStructureRegistry siteStructureRegistry =
				_siteStructureRegistrySecureService.GetByChannel(Configuration.ChannelId);
			_siteStructureRegistryId = siteStructureRegistry.Id;

			_systemUser = _systemUserSecureService.GetFullByUserName(Configuration.SystemUserName);
		}
	}
}