﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IMonitorProduct : IBusinessObjectBase
	{
		int Id { get; set; }
		int ProductId { get; set; }
		int ChannelId { get; set; }
		string Email { get; set; }
		DateTime CreatedDate { get; set; }
		int? SizeId { get; set; }
	}
}
