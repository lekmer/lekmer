﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableMediaItem : IBusinessObjectBase
	{
		IMediaItem MediaItem { get; set; }
		IImage Image { get; set; }
	}
}