﻿using System;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProduct : IProduct
	{
		int? BrandId { get; set; }
		IBrand Brand { get; set; }
		bool IsBookable { get; set; }
		bool IsBuyable { get; }
		bool IsMonitorable { get; }
		DateTime? IsNewFrom { get; set; }
		DateTime? IsNewTo { get; set; }
		bool IsNewProduct { get; }
		DateTime? CreatedDate { get; set; }
		string LekmerErpId { get; set; }
		string UrlTitle { get; set; }
		string LekmerUrl { get; }
		/// <summary>
		/// LekmerUrl contains only encoded Product UrlTitle but not all (with category part) url. 
		/// </summary>
		string LekmerUrlEncode { get; }
		string ParentPageUrl { get; set; }
		bool HasSizes { get; set; }
		int? ParentContentNodeId { get; set; }
		bool ShowVariantRelations { get; set; }
		decimal? Weight { get; set; }
		int ProductTypeId { get; set; }
		bool IsAbove60L { get; set; }
		int StockStatusId { get; set; }
		/// <summary>
		/// The minimum number in stock value for sending monitor product mails
		/// </summary>
		int? MonitorThreshold { get; set; }
		/// <summary>
		/// The maximum quantity allowed in a single order
		/// </summary>
		int? MaxQuantityPerOrder { get; set; }
		int? DeliveryTimeId { get; set; }
		IDeliveryTime DeliveryTime { get; set; }
		bool SellOnlyInPackage { get; set; }
		string SupplierArticleNumber { get; set; }
		bool IsDropShip { get; set; }

		/// <summary>
		/// Gets or sets the esales ticket. The property is runtime property.
		/// </summary>
		/// <value>
		/// The esales ticket.
		/// </value>
		string EsalesTicket { set; get; }
		/// <summary>
		/// Indicates item warehouse.
		/// When true, item will be sent from Fagerdala ("G" in HY file).
		/// Otherwise the item will be sent from Falkenberg.
		/// </summary>
		decimal? RecommendedPrice { get; set; }
		bool IsPriceAffectedByCampaign { get; }
		decimal? AbsoluteSave { get; }
		decimal? PercentageSave { get; }
	}
}