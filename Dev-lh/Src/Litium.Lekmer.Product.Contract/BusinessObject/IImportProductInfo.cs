﻿using System.Collections.Generic;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IImportProductInfo : IBusinessObjectBase
	{
		string HYErpId { get; set; }
		string Title { get; set; }
		string Description { get; set; }
		List<int> TagIdCollection { get; set; }
	}
}