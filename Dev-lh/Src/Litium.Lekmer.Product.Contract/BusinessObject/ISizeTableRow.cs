﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableRow : IBusinessObjectBase
	{
		int Id { get; set; }
		int SizeTableId { get; set; }
		int SizeId { get; set; }
		string Column1Value { get; set; }
		string Column2Value { get; set; }
		int Ordinal { get; set; }
	}
}