﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductStoreStock : IBusinessObjectBase
	{
		int ProductId { get; set; }
		int NumberInStock { get; set; }
		ISize SizeInfo { get; set; }
	}
}
