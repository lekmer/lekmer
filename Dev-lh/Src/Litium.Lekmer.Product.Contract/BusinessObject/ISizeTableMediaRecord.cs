﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableMediaRecord : IBusinessObjectBase
	{
		int Id { get; set; }
		int SizeTableId { get; set; }
		int MediaId { get; set; }

		IMediaItem MediaItem { get; set; }

		Collection<ISizeTableMediaRegistry> MediaRegistries { get; set; }
	}
}