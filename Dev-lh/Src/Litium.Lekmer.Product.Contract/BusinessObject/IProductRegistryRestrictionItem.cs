﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistryRestrictionItem : IBusinessObjectBase
	{
		int ProductRegistryId { get; set; }
		int ItemId { get; set; }
		string RestrictionReason { get; set; }
		int? UserId { get; set; }
		DateTime CreatedDate { get; set; }
		int ChannelId { get; set; }
	}
}