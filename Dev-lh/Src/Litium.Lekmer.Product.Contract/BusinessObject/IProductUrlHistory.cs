﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductUrlHistory : IBusinessObjectBase
	{
		int Id { get; set; }
		int ProductId { get; set; }
		int LanguageId { get; set; }
		string UrlTitleOld { get; set; }
		DateTime CreationDate { get; set; }
		DateTime LastUsageDate { get; set; }
		int UsageAmount { get; set; }
		int TypeId { get; set; }
	}
}