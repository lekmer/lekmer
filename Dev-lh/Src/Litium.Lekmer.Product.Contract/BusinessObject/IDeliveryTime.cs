using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IDeliveryTime : IBusinessObjectBase
	{
		int Id { get; set; }

		/// <summary>
		/// Min delivery time in days
		/// </summary>
		int? FromDays { get; set; }

		/// <summary>
		/// Max delivery time in days
		/// </summary>
		int? ToDays { get; set; }

		/// <summary>
		/// Delivery time format
		/// </summary>
		string Format { get; set; }

		int? ProductRegistryid { get; set; }
		int? ChannelId { get; set; }
	}
}