﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IWishListPackageItem : IBusinessObjectBase
	{
		int Id { get; set; }
		int WishListItemId { get; set; }
		int ProductId { get; set; }
		int? SizeId { get; set; }
	}
}