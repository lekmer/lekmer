using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IAgeIntervalBand : IBusinessObjectBase
	{
		int? FromMonth { get; set; }
		int? ToMonth { get; set; }
	}
}