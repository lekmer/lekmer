﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerCategoryRecord : ILekmerCategory, ICategoryRecord
	{
	}
}