﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IBrandLite : IBusinessObjectBase
	{
		int Id { get; set; }

		string Title { get; set; }

		int ContentNodeId { set; get; }
	}
}
