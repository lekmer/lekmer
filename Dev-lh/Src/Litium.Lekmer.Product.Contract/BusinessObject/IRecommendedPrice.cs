﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IRecommendedPrice : IBusinessObjectBase
	{
		int ChannelId { get; set; }
		decimal Price { get; set; }
		int ProductId { get; set; }
	}
}