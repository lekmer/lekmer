﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class IconCollection : Collection<IIcon>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="IconCollection"/> class that is empty.
		/// </summary>
		public IconCollection()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="IconCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public IconCollection(IList<IIcon> list)
			: base(list)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="IconCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public IconCollection(IEnumerable<IIcon> list)
			: base(new List<IIcon>(list))
		{
		}

		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		public int TotalCount { get; set; }
	}
}