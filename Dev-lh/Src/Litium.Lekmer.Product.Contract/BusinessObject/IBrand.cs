﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	public interface IBrand : IBusinessObjectBase
	{
		int Id { get; set; }

		int? MediaId { get; set; }

		string Title { get; set; }

		string ExternalUrl { get; set; }

		string Description { get; set; }

		IImage Image { get; set; }

		string ErpId { get; set; }

		int? ContentNodeId { get; set; }

		string PackageInfo { get; set; }

		/// <summary>
		/// The minimum number in stock value for sending monitor product mails
		/// </summary>
		int? MonitorThreshold { get; set; }

		/// <summary>
		/// The maximum quantity of product allowed in a single order
		/// </summary>
		int? MaxQuantityPerOrder { get; set; }

		int? DeliveryTimeId { get; set; }
		IDeliveryTime DeliveryTime { get; set; }

		bool IsOffline { get; set; }
	}
}