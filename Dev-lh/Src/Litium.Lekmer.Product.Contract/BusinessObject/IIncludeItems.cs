﻿using Litium.Lekmer.Campaign;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Product
{
	public interface IIncludeItems
	{
		ProductIdDictionary IncludeProducts { get; set; }
		CampaignCategoryDictionary IncludeCategories { get; set; }
		BrandIdDictionary IncludeBrands { get; set; }
		IdDictionary IncludeSuppliers { get; set; }
	}
}