﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandList : IBlock
	{
		bool IncludeAllBrands { get; set; }
		int? LinkContentNodeId { get; set; }
		IBlockSetting Setting { get; set; }
	}
}