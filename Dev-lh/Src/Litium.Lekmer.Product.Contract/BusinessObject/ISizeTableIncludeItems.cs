﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableIncludeItems
	{
		ProductIdDictionary IncludeProducts { get; set; }
		Collection<ISizeTableCategoryBrandPair> IncludeCategoryBrandPairs { get; set; }
	}
}