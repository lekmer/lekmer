﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductSearchResult : IBlock
	{
		IBlockSetting Setting { get; set; }
	}
}