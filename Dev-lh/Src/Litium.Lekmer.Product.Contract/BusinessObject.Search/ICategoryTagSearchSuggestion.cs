using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ICategoryTagSearchSuggestion
	{
		CategoryCollection ParentCategories { get; set; }
		ICategory Category { get; set; }
		string CategoryTicket { get; set; }
		Collection<ITag> Tags { get; set; }
	}
}