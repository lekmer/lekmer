﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductSearchResultSecureService
	{
		IBlockProductSearchResult Create();
		int Save(ISystemUserFull systemUserFull, IBlockProductSearchResult block);
		IBlockProductSearchResult GetById(int id);
	}
}
