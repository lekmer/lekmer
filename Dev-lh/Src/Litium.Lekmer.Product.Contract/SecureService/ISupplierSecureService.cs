﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISupplierSecureService
	{
		Collection<ISupplier> Search(string searchCriteria);
		Collection<ISupplier> GetAll();
		ISupplier GetById(int id);
		ISupplier GetByNo(string supplierNo);
		void Save(ISystemUserFull systemUserFull, ISupplier supplier);
	}
}