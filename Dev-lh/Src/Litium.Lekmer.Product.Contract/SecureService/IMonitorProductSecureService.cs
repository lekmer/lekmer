﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IMonitorProductSecureService
	{
		void Delete(ISystemUserFull user, int channelId, string email);
	}
}