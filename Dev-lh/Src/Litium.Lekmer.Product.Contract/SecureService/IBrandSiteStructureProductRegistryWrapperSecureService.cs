using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IBrandSiteStructureProductRegistryWrapperSecureService
	{
		IBrandSiteStructureProductRegistryWrapper Create();
		Collection<IBrandSiteStructureProductRegistryWrapper> GetAllByBrand(int brandId);
	}
}