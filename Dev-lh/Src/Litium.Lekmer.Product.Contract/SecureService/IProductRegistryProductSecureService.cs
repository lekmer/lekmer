﻿using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistryProductSecureService
	{
		void Insert(ISystemUserFull systemUserFull, int? productId, int? brandId, int? categoryId, List<int> productRegistryIds);
		void DeleteByProduct(ISystemUserFull systemUserFull, int productId, List<int> productRegistryIds);
		void DeleteByBrand(ISystemUserFull systemUserFull, int brandId, List<int> productRegistryIds);
		void DeleteByCategory(ISystemUserFull systemUserFull, int categoryId, List<int> productRegistryIds);
		List<int> GetRestrictionsByProductId(int productId);
	}
}