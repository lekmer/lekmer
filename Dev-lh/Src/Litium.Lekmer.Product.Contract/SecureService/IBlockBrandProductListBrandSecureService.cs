﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandProductListBrandSecureService
	{
		IBlockBrandProductListBrand Create();
		Collection<IBrand> GetAllByBlock(int channelId, int blockId);
		Collection<IBrand> GetAllByBlockInverted(int channelId, int blockId);
		void Save(int blockId, Collection<IBlockBrandProductListBrand> blockBrands);
		void Delete(int blockId);
	}
}