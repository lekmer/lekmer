﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IDeliveryTimeSecureService
	{
		int Insert();
		void Delete(int id);
		void SaveProductRegistryDeliveryTime(int id, int? fromDays, int? toDays, string format, int? productRegistryId);
		Collection<IDeliveryTime> GetAllProductRegistryDeliveryTimeById(int id);
	}
}