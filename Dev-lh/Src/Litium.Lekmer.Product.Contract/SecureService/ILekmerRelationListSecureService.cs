﻿using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerRelationListSecureService : IRelationListSecureService
	{
		Collection<IRelationList> GetAllByProductAndType(int productId, string commonName);
		RelationListCollection SearchWithoutColorVariant(IRelationListSearchCriteria searchCriteria, int page, int pageSize);
	}
}