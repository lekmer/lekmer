﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableFolderSecureService
	{
		ISizeTableFolder Save(ISystemUserFull systemUserFull, ISizeTableFolder folder);

		bool TryDelete(ISystemUserFull systemUserFull, int folderId);

		ISizeTableFolder GetById(int folderId);

		Collection<ISizeTableFolder> GetAll();

		Collection<INode> GetTree(int? selectedId);
	}
}