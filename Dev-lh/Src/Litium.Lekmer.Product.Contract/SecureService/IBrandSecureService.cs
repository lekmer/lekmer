﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBrandSecureService
	{
		IBrand Create();

		int Save(ISystemUserFull systemUserFull, IBrand brand,
				Collection<IBrandSiteStructureRegistry> brandSiteStructureRegistries,
				IEnumerable<ITranslationGeneric> titleTranslations,
				IEnumerable<ITranslationGeneric> descriptionTranslations,
				IEnumerable<ITranslationGeneric> packageInfoTranslations,
				Collection<IProductRegistryRestrictionItem> registryRestrictions,
				Collection<int> icons);

		BrandCollection GetAll();
		BrandCollection GetAll(int page, int pageSize, string sortBy, bool? sortByDescending);

		IBrand GetById(int brandId);
		void Delete(ISystemUserFull systemUserFull, int brandId);

		Collection<ITranslationGeneric> GetAllTitleTranslationsByBrand(int id);
		Collection<ITranslationGeneric> GetAllDescriptionTranslationsByBrand(int id);
		Collection<ITranslationGeneric> GetAllPageInfoTranslationsByBrand(int id);
	}
}