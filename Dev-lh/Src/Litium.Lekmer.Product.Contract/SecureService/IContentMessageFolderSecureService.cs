﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Product
{
	public interface IContentMessageFolderSecureService
	{
		IContentMessageFolder Save(ISystemUserFull systemUserFull, IContentMessageFolder folder);

		bool TryDelete(ISystemUserFull systemUserFull, int folderId);

		IContentMessageFolder GetById(int folderId);

		Collection<IContentMessageFolder> GetAll();

		Collection<INode> GetTree(int? selectedId);
	}
}