using System.Collections.Generic;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IEsalesProductTicketService
	{
		string FindTicket(IUserContext context, int productId);
		Dictionary<int, string> FindTickets(IUserContext context, ProductIdCollection productIds);
	}
}