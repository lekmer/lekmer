﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductTypeService
	{
		Collection<IProductType> GetAll(IUserContext context);
		IProductType GetById(IUserContext context, int productTypeId);

		Collection<int> GetIdAllByBlock(int blockId);
	}
}
