using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductPopularityService
	{
		Collection<IProductPopularity> GetAll(IUserContext context);
	}
}