﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface ISupplierService
	{
		Collection<ISupplier> GetAll();
	}
}