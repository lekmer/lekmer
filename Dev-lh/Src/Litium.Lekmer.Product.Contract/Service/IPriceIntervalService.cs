using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IPriceIntervalService
	{
		IPriceIntervalBand GetPriceInterval(decimal? priceFrom, decimal? priceTo);
		IPriceIntervalBand GetPriceInterval(IUserContext context, int priceIntervalId);
		IPriceIntervalBand GetPriceIntervalOuter(Collection<IPriceInterval> priceIntervals);
		Collection<IPriceInterval> GetAll(IUserContext context);
		IPriceInterval GetMatching(IUserContext context, decimal price);
	}
}