using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IPriceListItemService
	{
		IPriceListItem Create();
	}
}