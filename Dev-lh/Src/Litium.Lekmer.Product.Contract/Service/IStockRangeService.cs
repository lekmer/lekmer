using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IStockRangeService
	{
		Collection<IStockRange> GetAll(IUserContext context);
	}
}