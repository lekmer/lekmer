﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductListService
	{
		IBlockProductList GetById(IUserContext context, int id);
	}
}
