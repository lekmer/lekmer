using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IProductImageService
	{
		Collection<IProductImage> GetAllByProduct(int productId);
	}
}