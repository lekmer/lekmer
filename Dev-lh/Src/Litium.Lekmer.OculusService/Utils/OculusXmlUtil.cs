﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Xml;
using Litium.Lekmer.Oculus;
using Litium.Lekmer.Oculus.Setting;
using Litium.Scensum.Foundation;
using log4net;
using Convert = System.Convert;

namespace Litium.Lekmer.OculusService.Utils
{
	public class OculusXmlUtil
	{
		private readonly ILog _log;
		private readonly OculusSetting oculusSetting = new OculusSetting();

		public OculusXmlUtil(ILog log)
		{
			_log = log;
		}

		public void ImportProductSimilar()
		{
			string fileName = oculusSetting.FileName();
			if (string.IsNullOrEmpty(fileName)) throw new ArgumentNullException("fileName");

			string filePath = oculusSetting.FilePath();
			if (string.IsNullOrEmpty(filePath)) throw new ArgumentNullException("filePath");

			if (!Directory.Exists(filePath))
			{
				Directory.CreateDirectory(filePath);
			}

			string file = Path.Combine(filePath, fileName);

			if (!GetFile(file)) return;

			Collection<IProductSimilar> productSimilarCollection = ParseProductSimilarXml(file);

			SaveProductSimilar(productSimilarCollection);
		}

		private bool GetFile(string filePath)
		{
			try
			{
				string oculusServiceUrl = oculusSetting.OculusServiceUrl();
				if (string.IsNullOrEmpty(oculusServiceUrl)) throw new ArgumentNullException("OculusServiceUrl");

				int serviceTimeout = oculusSetting.OculusServiceTimeout();

				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(oculusServiceUrl);
				request.Timeout = serviceTimeout * 1000;

				HttpWebResponse response = (HttpWebResponse)request.GetResponse();
				if (response.StatusCode == HttpStatusCode.OK)
				{
					using (Stream stream = response.GetResponseStream())
					{
						using (FileStream fileStream = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write))
						{
							byte[] buffer = new byte[4096];
							int readBytes;
							while (0 < (readBytes = stream.Read(buffer, 0, buffer.Length)))
							{
								fileStream.Write(buffer, 0, readBytes);
							}
						}
					}
				}
			}
			catch (Exception exception)
			{
				_log.Error("Failed to get file by Oculus URL.", exception);
				throw new Exception("Failed to get file." + exception.Message, exception);
			}

			return true;
		}

		private Collection<IProductSimilar> ParseProductSimilarXml(string filePath)
		{
			if (string.IsNullOrEmpty(filePath)) throw new NullReferenceException("filePath");

			string productNodePath = oculusSetting.ProductNodePath();
			if (string.IsNullOrEmpty(productNodePath)) productNodePath = "products/product";

			string similarNodePath = oculusSetting.SimilarNodePath();
			if (string.IsNullOrEmpty(similarNodePath)) similarNodePath = "similars/similar";

			string productErpIdAttribute = oculusSetting.ProductErpIdAttribute();
			if (string.IsNullOrEmpty(productErpIdAttribute)) productErpIdAttribute = "id";

			string productScoreAttribute = oculusSetting.ProductScoreAttribute();
			if (string.IsNullOrEmpty(productScoreAttribute)) productScoreAttribute = "score";

			Collection<IProductSimilar> productSimilarCollection = new Collection<IProductSimilar>();

			XmlDocument file = new XmlDocument();
			file.Load(filePath);

			XmlNodeList productCollection = file.SelectNodes(productNodePath);
			if (productCollection == null) return productSimilarCollection;

			foreach (XmlNode product in productCollection)
			{
				IProductSimilar productSimilar = IoC.Resolve<IProductSimilar>();
				productSimilar.ProductErpId = product.Attributes[productErpIdAttribute].Value;

				XmlNodeList similarCollection = product.SelectNodes(similarNodePath);
				foreach (XmlNode similar in similarCollection)
				{
					ISimilarProductItem similarProductItem = IoC.Resolve<ISimilarProductItem>();
					similarProductItem.ProductErpId = similar.Attributes[productErpIdAttribute].Value;
					similarProductItem.Score = Convert.ToDecimal(similar.Attributes[productScoreAttribute].Value, CultureInfo.InvariantCulture);

					productSimilar.SimilarProductCollection.Add(similarProductItem);
				}

				productSimilarCollection.Add(productSimilar);
			}

			return productSimilarCollection;
		}

		private void SaveProductSimilar(Collection<IProductSimilar> productSimilarCollection)
		{
			try
			{
				IProductSimilarSecureService productSimilarSecureService = IoC.Resolve<IProductSimilarSecureService>();

				foreach (IProductSimilar productSimilar in productSimilarCollection)
				{
					productSimilarSecureService.Save(productSimilar);
				}
			}
			catch (Exception exception)
			{
				_log.Error("Failed to save similar products into database.", exception);
				throw new Exception("Failed to get file." + exception.Message, exception);
			}
		}
	}
}