﻿using System.ComponentModel;

namespace Litium.Lekmer.NewsletterSubscriber.Service
{
	[RunInstaller(true)]
	public partial class NewsletterSubscriberServiceHostInstaller : System.Configuration.Install.Installer
	{
		public NewsletterSubscriberServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}