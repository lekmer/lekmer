﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Litium.Lekmer.Campaign.CampaignTagging.Service
{
	[RunInstaller(true)]
	public partial class CampaignTaggingServiceHostInstaller : Installer
	{
		public CampaignTaggingServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}