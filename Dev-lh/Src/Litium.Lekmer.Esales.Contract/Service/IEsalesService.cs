﻿namespace Litium.Lekmer.Esales
{
	public interface IEsalesService
	{
		IEsalesResponse RetrieveContent(IEsalesRequest esalesRequest);
		IEsalesResponse RetrieveContent(IEsalesPanelRequest esalesPanelRequest);
	}
}