using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesRecommendServiceV2
	{
		IBlockEsalesRecommendV2 GetById(IUserContext context, int blockId);
	}
}