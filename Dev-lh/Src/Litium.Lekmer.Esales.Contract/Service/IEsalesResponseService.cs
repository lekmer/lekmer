﻿namespace Litium.Lekmer.Esales
{
	public interface IEsalesResponseService
	{
		IEsalesResponse ConvertToSearchResponse(Apptus.ESales.Connector.PanelContent panelContent);
	}
}