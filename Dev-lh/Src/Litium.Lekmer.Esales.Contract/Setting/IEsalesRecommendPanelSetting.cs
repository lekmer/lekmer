﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IEsalesRecommendPanelSetting
	{
		string AbandonedCartsPanelPath { get; }
		string RecentlyViewedPanelPath { get; }
		string TopSellersPanelPath { get; }
		string ThoseWhoBoughtAlsoBoughtPanelPath { get; }
		string ThoseWhoViewedAlsoViewedPanelPath { get; }
		string ThoseWhoViewedBoughtPanelPath { get; }
		string RecommendBasedOnCartPanelPath { get; }
		string RecommendBasedOnCustomerPanelPath { get; }
		string RecommendBasedOnProductPanelPath { get; }
		string FallbackPanelPath { get; }
	}
}
