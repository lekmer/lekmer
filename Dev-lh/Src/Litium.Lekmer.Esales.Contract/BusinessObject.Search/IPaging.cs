namespace Litium.Lekmer.Esales
{
	public interface IPaging
	{
		int PageNumber { get; }
		int PageSize { get; }

		int FirstItemNumber { get; }
		int LastItemNumber { get; }
	}
}