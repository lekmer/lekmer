﻿using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	public interface ICartEsalesRequest : IEsalesRequest
	{
		string ProductKey { get; set; }
		string CartProductIds { get; set; }
		List<int> ExcludeProductIds { get; set; }
		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		IPaging Paging { get; set; }
	}
}
