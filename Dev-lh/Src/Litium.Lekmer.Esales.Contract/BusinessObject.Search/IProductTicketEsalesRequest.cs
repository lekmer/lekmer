using Litium.Lekmer.Product;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesProductTicketService"/> to build a query and execute it on one or more indexes. 
	/// </summary>
	public interface IProductTicketEsalesRequest : IEsalesRequest
	{
		/// <summary>
		/// A comma separated list of product keys of the products that should be listed.
		/// </summary>
		string Products { get; set; }
	}
}