namespace Litium.Lekmer.Esales
{
	public interface IAdInfo
	{
		string Key { get; set; }
		string Ticket { get; set; }
		string CampaignKey { get; set; }

		string Format { get; set; }
		string Site { get; set; }
		string Gender { get; set; }
		string ProductCategory { get; set; }
		string Title { get; set; }
		string LandingPageUrl { get; set; }
		string ImageUrl { get; set; }
		string Tags { get; set; }
	}
}