using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IPanelContent
	{
		string Name { get; set; }
		string Description { get; set; }

		bool HasRezult { get; set; }
		bool IsZone { get; set; }

		Collection<IPanelContent> SubPanels { get; set; }

		IResult Result { get; set; }

		IPanelContent FindPanel(string panelName);
		IResult FindResult(EsalesResultType resultType);
		Collection<IResult> FindResults(EsalesResultType resultType);
	}
}