using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IPhraseHits : IResult
	{
		Collection<IPhraseInfo> PhrasesInfo { get; set; }
	}
}