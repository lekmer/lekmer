﻿namespace Litium.Lekmer.Esales
{
	public enum EsalesRecomendationTypeInfo
	{
		Unknown,
		AbandonedCarts,
		RecentlyViewed,
		TopSellers,
		ThoseWhoBoughtAlsoBought,
		ThoseWhoViewedAlsoViewed,
		ThoseWhoViewedBought,
		RecommendBasedOnCart,
		RecommendBasedOnCustomer,
		RecommendBasedOnProduct
	}
}
