﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersProductV2 : IBusinessObjectBase
	{
		int BlockId { get; set; }
		IProduct Product { get; set; }
		int Position { get; set; }
	}
}