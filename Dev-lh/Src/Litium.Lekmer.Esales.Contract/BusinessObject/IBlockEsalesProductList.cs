﻿using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesProductList : ILekmerBlock
	{
		IBlockSetting Setting { get; set; }
		IBlockEsalesSetting EsalesSetting { get; set; }
	}
}
