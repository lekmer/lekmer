﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public interface IAd : IBusinessObjectBase
	{
		int Id { get; set; }
		int FolderId { get; set; }

		string Key { get; set; }
		string Title { get; set; }
		DateTime? DisplayFrom { get; set; }
		DateTime? DisplayTo { get; set; }
		string ProductCategory { get; set; }
		string Gender { get; set; }
		string Format { get; set; }
		string SearchTags { get; set; }

		DateTime CreatedDate { get; set; }
		DateTime UpdatedDate { get; set; }

		Collection<IAdChannel> AdChannelList { get; set; }
	}
}