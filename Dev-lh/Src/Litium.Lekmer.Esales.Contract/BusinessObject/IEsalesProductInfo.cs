using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesProductInfo
	{
		int ProductId { get; set; }
		bool Available { get; }

		string EsalesKey { get; set; }

		IEsalesChannelProductInfo DefaultProductInfo { get; }

		Collection<IEsalesChannelProductInfo> ChannelProductInfoCollection { get; }

		void Add(IChannel channel, ILekmerProductView product);
	}
}