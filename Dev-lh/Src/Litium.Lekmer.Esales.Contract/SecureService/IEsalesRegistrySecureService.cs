﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesRegistrySecureService
	{
		Collection<IEsalesRegistry> GetAll();
	}
}