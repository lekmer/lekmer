﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersCategorySecureServiceV2
	{
		Collection<IBlockEsalesTopSellersCategoryV2> GetAllByBlock(int blockId);
		void Save(int blockId, Collection<IBlockEsalesTopSellersCategoryV2> blockCategories);
	}
}