﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesProductListSecureService
	{
		IBlockEsalesProductList Create();
		IBlockEsalesProductList GetById(int id);
		int Save(ISystemUserFull systemUserFull, IBlockEsalesProductList block);
	}
}
