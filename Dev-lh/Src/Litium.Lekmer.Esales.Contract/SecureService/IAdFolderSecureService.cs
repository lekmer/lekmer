﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Esales
{
	public interface IAdFolderSecureService
	{
		IAdFolder Create();

		IAdFolder Save(ISystemUserFull systemUserFull, IAdFolder adFolder);

		bool TryDelete(ISystemUserFull systemUserFull, int adFolderId);

		IAdFolder GetById(int adFolderId);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IAdFolder> GetAll();

		Collection<IAdFolder> GetAllByParent(int parentFolderId);

		Collection<INode> GetTree(int? selectedId);
	}
}