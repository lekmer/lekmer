﻿using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesRecommendV2Control : LekmerBlockControlBase<IBlockEsalesRecommendV2>
	{
		private readonly IBlockEsalesRecommendServiceV2 _blockEsalesRecommendServiceV2;
		private readonly IEsalesGeneralServiceV2 _esalesGeneralServiceV2;

		private ProductCollection _products;

		public BlockEsalesRecommendV2Control(
			ITemplateFactory templateFactory,
			IBlockEsalesRecommendServiceV2 blockEsalesRecommendServiceV2,
			IEsalesGeneralServiceV2 esalesGeneralServiceV2)
			: base(templateFactory)
		{
			_blockEsalesRecommendServiceV2 = blockEsalesRecommendServiceV2;
			_esalesGeneralServiceV2 = esalesGeneralServiceV2;
		}

		protected override IBlockEsalesRecommendV2 GetBlockById(int blockId)
		{
			return _blockEsalesRecommendServiceV2.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			if (_products.Count <= 0)
			{
				return new BlockContent();
			}

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render() ?? string.Empty;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		private void Initialize()
		{
			var returnItemsCount = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;

			_products = EsalesGeneralResponse == null 
				? new ProductCollection()
				: _esalesGeneralServiceV2.FindProducts(UserContext.Current, EsalesGeneralResponse, Block.PanelName, returnItemsCount);
		}
	}
}