﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Esales.Web
{
	/// <summary>
	/// General Product page control.
	/// </summary> 
	public class BlockEsalesDataSourceControl : BlockControlBase<IBlock>, IBlockEsalesDataSourceControl
	{
		private readonly IBlockService _blockService;
		private readonly Collection<string> _blockContentMessages;
		private IEsalesPanelRequest _esalesPanelRequest;
		private IEsalesResponse _esalesResponse;

		protected IEsalesService EsalesService { get; private set; }

		public BlockEsalesDataSourceControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IEsalesService esalesService
			) : base(templateFactory)
		{
			_blockService = blockService;
			_blockContentMessages = new Collection<string>();
			EsalesService = esalesService;
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			ILekmerContentPageHandler lekmerContentPageHandler = ContentPageHandler.Cast<ILekmerContentPageHandler>();
			if (lekmerContentPageHandler == null)
			{
				return RenderBlockContent("[ Page handler doesn't support eSales blocks ]");
			}

			// Scan page for esales blocks
			Collection<IBlockEsalesControl> blockEsalesControls = lekmerContentPageHandler.FindBlockControls<IBlockEsalesControl>();

			if (blockEsalesControls == null || blockEsalesControls.Count == 0)
			{
				return RenderBlockContent();  //No eSales blocks were found.
			}

			InitializeEsalesPanelRequest();

			foreach (IBlockEsalesControl blockEsalesControl in blockEsalesControls)
			{
				IEsalesPanelRequest blockPanelRequest = blockEsalesControl.EsalesPanelRequest;
				ComposeEsalesRequest(blockPanelRequest);
			}

			MakeEsalesCall();

			foreach (IBlockEsalesControl blockEsalesControl in blockEsalesControls)
			{
				if (blockEsalesControl.EsalesPanelRequest.PanelPath.Equals(_esalesPanelRequest.PanelPath))
				{
					blockEsalesControl.EsalesResponse = _esalesResponse;
				}
			}

			return RenderBlockContent();
		}

		protected virtual void InitializeEsalesPanelRequest()
		{
			_esalesPanelRequest = IoC.Resolve<IEsalesPanelRequest>();
			_esalesPanelRequest.PanelArguments = new Dictionary<string, string>();
		}

		protected virtual void ComposeEsalesRequest(IEsalesPanelRequest blockPanelRequest)
		{
			if (_esalesPanelRequest.PanelPath.IsEmpty())
			{
				_esalesPanelRequest.PanelPath = blockPanelRequest.PanelPath;
			}

			if (_esalesPanelRequest.PanelPath.Equals(blockPanelRequest.PanelPath))
			{
				foreach (KeyValuePair<string, string> blockArgument in blockPanelRequest.PanelArguments)
				{
					_esalesPanelRequest.PanelArguments[blockArgument.Key] = blockArgument.Value;
				}
			}
			else
			{
				AddContentMessage("[ Some eSales blocks have different panel path. Only one panel could be called. ]");
				AddContentMessage(string.Format("[ Panel path '{0}' and '{1}' ]", _esalesPanelRequest.PanelPath, blockPanelRequest.PanelPath));
			}
		}

		protected virtual void MakeEsalesCall()
		{
			try
			{
				_esalesResponse = EsalesService.RetrieveContent(_esalesPanelRequest);
			}
			catch (Exception ex)
			{
				_esalesResponse = null;
				AddContentMessage(string.Format("[ eSales call error message: {0}. ]", ex.Message));
			}
		}

		protected virtual void AddContentMessage(string contentMessage)
		{
			_blockContentMessages.Add(contentMessage);
		}

		protected virtual BlockContent RenderBlockContent()
		{
			if (_blockContentMessages.Count == 0)
			{
				return new BlockContent(string.Empty, string.Empty, string.Empty);
			}

			string contentMessages = string.Concat(_blockContentMessages.ToArray());
			return new BlockContent(string.Empty, string.Concat("<!--", contentMessages, "-->"), string.Empty);
		}

		protected virtual BlockContent RenderBlockContent(string contentMessage)
		{
			AddContentMessage(contentMessage);
			return RenderBlockContent();
		}
	}
}
