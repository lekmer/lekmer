﻿using Litium.Scensum.Core;
using Litium.Scensum.Template;

namespace Litium.Lekmer.Template
{
	public interface ILekmerAliasSecureService
	{
		/// <summary>
		/// Gets alias by common name
		///  </summary>
		IAlias GetByCommonName(string commonName);
	}
}