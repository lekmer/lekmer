﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Collector
{
	public interface IOrderItemConverter
	{
		Collection<InvoiceRow> Convert(IOrderItem orderItem, bool taxFreeZone);
	}
}
