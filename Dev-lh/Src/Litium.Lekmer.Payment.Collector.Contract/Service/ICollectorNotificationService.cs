﻿using System.Collections.Specialized;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorNotificationService
	{
		ICollectorNotificationInfo ParseFromQueryString(NameValueCollection queryString);
		bool Validate(ICollectorNotificationInfo notificationInfo);
	}
}
