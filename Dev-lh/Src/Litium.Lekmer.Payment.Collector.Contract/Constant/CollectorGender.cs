﻿namespace Litium.Lekmer.Payment.Collector
{
	/// <summary>
	/// Persons gender according to the ISO/IEC 5218 standard.
	/// </summary>
	public enum CollectorGender
	{
		Unknown = 0,
		Male = 1,
		Female = 2,
		NotApplicable = 9
	}
}
