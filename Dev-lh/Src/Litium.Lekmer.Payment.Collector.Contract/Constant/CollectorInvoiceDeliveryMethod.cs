﻿namespace Litium.Lekmer.Payment.Collector
{
	/// <summary>
	/// The way to send the invoice. By default this is determined by Collector, but can be negotiated with Collector.
	/// If a current invoice exists, the latest information will be used next time the invoice is sent.
	///  The default value is 1.
	/// </summary>
	public enum CollectorInvoiceDeliveryMethod
	{
		/// <summary>
		/// 1 Normal mail or in package based on invoice type.  The default value.
		/// </summary>
		NormalMailOrInPackage = 1,
		/// <summary>
		/// 2 E-mail.
		/// </summary>
		Email = 2,
		/// <summary>
		/// 3 Both Normal mail and e-mail should be used.
		/// </summary>
		Both = 3
	}
}
