﻿namespace Litium.Lekmer.Payment.Collector
{
	/// <summary>
	/// Different options for affecting a purchase.
	/// </summary>
	public enum CollectorActivationOption
	{
		/// <summary>
		/// Purchase will be preliminary and must be activated, by using the ActivateInvoice, PartActivateInvoice or through the Collector Partner Portal.
		/// </summary>
		Default = 0,
		/// <summary>
		/// Auto activation, will automatically active the invoice so it can be sent out directly. This can only be used when the order can be delivered directly.
		/// </summary>
		AutoActivation = 1,
		/// <summary>
		/// Pre-Paid invoice. The purchase will be activated first when an invoice is paid. Not used at the moment.
		/// </summary>
		PrePaidInvoice = 2
	}
}
