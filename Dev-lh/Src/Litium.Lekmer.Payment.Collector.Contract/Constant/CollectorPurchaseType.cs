﻿namespace Litium.Lekmer.Payment.Collector
{
	/// <summary>
	/// The type of the purchase, to specify where the purchase originates from.
	/// </summary>
	public enum CollectorPurchaseType
	{
		ECommerce = 0,
		Store = 1,
		InvoicePayment = 2,
		P2P = 3,
		Sms = 4
	}
}
