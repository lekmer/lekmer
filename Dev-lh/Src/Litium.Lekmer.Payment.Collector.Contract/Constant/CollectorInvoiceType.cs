﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorInvoiceType
	{
		/// <summary>
		/// 0 Invoice will be in the package and/or directly sent with e-mail if InvoiceDeliveryMethod is set to 1 (normal mail), Collector will not send this invoice to the customer, you will send it as part of the package.
		/// </summary>
		Default = 0,
		/// <summary>
		/// 1 Collector will send this invoice.
		/// </summary>
		MonthlyInvoice = 1,
		/// <summary>
		/// 3 Collector will send the invoice. All invoices incoming during the same month with this flag will be aggregated to one invoice.
		/// </summary>
		AggregatedInvoice = 3,
		/// <summary>
		/// 4 Collector will send this invoice.
		/// </summary>
		InterestFreeInvoice = 4,
		/// <summary>
		/// 5 Collector will send this invoice.
		/// </summary>
		AnnuityInvoice = 5
	}
}
