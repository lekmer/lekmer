﻿namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorResult
	{
		int TransactionId { get; set; }
		int StatusCode { get; set; }
		long Duration { get; set; }
		string ErrorMessage { get; set; }
		string ResponseContent { get; set; }
		string FaultCode { get; set; }
		string FaultMessage { get; set; }
	}
}
