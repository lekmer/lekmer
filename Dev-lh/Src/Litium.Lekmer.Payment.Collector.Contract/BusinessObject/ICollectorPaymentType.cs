namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorPaymentType
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		string Code { get; set; }
		string Description { get; set; }

		decimal StartFee { get; set; }
		decimal InvoiceFee { get; set; }
		decimal MonthlyInterestRate { get; set; }

		int NoOfMonths { get; set; }
		decimal MinPurchaseAmount { get; set; }
		decimal LowestPayment { get; set; }

		CollectorPartPaymentType PartPaymentType { get; set; }
		CollectorInvoiceType InvoiceType { get; set; }

		int Ordinal { get; set; }
	}
}