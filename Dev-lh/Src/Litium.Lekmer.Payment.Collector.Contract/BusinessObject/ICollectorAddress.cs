using Litium.Scensum.Customer;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorAddress : IAddress
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		bool IsCompany { get; set; }
		string CompanyName { get; set; }
	}
}