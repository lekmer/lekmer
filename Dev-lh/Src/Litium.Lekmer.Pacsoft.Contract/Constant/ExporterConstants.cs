﻿namespace Litium.Lekmer.Pacsoft
{
	public static class ExporterConstants
	{
		public const string NodeData = "data";
		public const string NodeReceiver = "receiver";
		public const string NodeShipment = "shipment";
		public const string NodeService = "service";
		public const string NodeAddon = "addon";
		public const string NodeUfonline = "ufonline";
		public const string NodeOption = "option";
		public const string NodeContainer = "container";
		public const string NodeVal = "val";


		public const string RcvIdAttribute = "rcvid";
		public const string OrdernoAttribute = "orderno";
		public const string SrvIdAttribute = "srvid";
		public const string AdnIdAttribute = "adnid";
		public const string OptidAttribute = "optid";
		public const string TypeAttribute = "type";
		public const string NAttribute = "n";

		public const string NameAttribute = "name";
		public const string Address1Attribute = "address1";
		public const string Address2Attribute = "address2";
		public const string CityAttribute = "city";
		public const string CountryAttribute = "country";
		public const string ZipcodeAttribute = "zipcode";
		public const string EmailAttribute = "email";
		public const string PhoneAttribute = "phone";
		public const string SmsAttribute = "sms";
		public const string FromAttribute = "from";
		public const string ToAttribute = "to";
		public const string CopiesAttribute = "copies";
		public const string ContentsAttribute = "contents";


		public const string AdnidAttributeValue = "notsms";
		public const string OptidAttributeValue = "ENOT";
		public const string TypeAttributeValue = "parcel";
	}
}
