﻿namespace Litium.Lekmer.Pacsoft.Contract
{
	public interface IPacsoftInfo
	{
		int OrderId { get; set; }
		int QuickId { get; set; }
		string Name { get; set; }
		string Address1 { get; set; }
		string Address2 { get; set; }
		string PostalCode { get; set; }
		string City { get; set; }
		string Country { get; set; }
		string Email { get; set; }
		string Phone { get; set; }
		string Sms { get; set; }
		int ItemsCount { get; set; }
	}
}