﻿namespace Litium.Lekmer.Pacsoft.Contract
{
	public interface IExporter
	{
		void Execute();
	}
}