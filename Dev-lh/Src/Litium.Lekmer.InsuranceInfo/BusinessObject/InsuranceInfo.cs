using System;
using Litium.Lekmer.InsuranceInfo.Contract;

namespace Litium.Lekmer.InsuranceInfo
{
	[Serializable]
	public class InsuranceInfo : IInsuranceInfo
	{
		public int OrderId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string CivicNumber { get; set; }
		public string Adress { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string PhoneNumber { get; set; }
		public string Email { get; set; }
		public DateTime PurchaseDate { get; set; }
		public string ParentCategory { get; set; }
		public string Category { get; set; }
		public string CategoryId { get; set; }
		public string Brand { get; set; }
		public string Modell { get; set; }
		public decimal PurchaseAmount { get; set; }
		public int Quantity { get; set; }
		public decimal Premie { get; set; }
	}
}