﻿using System;

namespace Litium.Lekmer.Customer
{
	public interface ICustomerForgotPasswordService
	{
		ICustomerForgotPassword Create(int customerId, int validPeriod);
		ICustomerForgotPassword GetByGuid(Guid id);
		void Save(ICustomerForgotPassword customerForgotPassword);
		void Delete(int customerId);
	}
}