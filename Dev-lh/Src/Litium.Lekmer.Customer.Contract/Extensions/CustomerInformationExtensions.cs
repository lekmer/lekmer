﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public static class CustomerInformationExtensions
	{
		/// <summary>
		/// Determines whether type of specified product is 'product'.
		/// </summary>
		public static bool IsCompany(this ICustomerInformation customerInformation)
		{
			bool isCompany = false;

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				isCompany = lekmerCustomerInformation.IsCompany;
			}

			return isCompany;
		}
	}
}
