﻿namespace Litium.Lekmer.Customer
{
	public enum CustomerLoginType
	{
		None = 0,
		LekmerAccount = 1,
		FacebookAccount = 2
	}
}