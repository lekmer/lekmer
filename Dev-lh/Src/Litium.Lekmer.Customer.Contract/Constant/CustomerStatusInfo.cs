﻿namespace Litium.Lekmer.Customer
{
	public enum CustomerStatusInfo
	{
		Online = 0,
		Offline = 1,
		Blocked = 2,
	}
}
