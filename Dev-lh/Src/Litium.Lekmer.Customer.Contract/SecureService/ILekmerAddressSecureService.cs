﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerAddressSecureService : IAddressSecureService
	{
		void DeleteByCustomer(ISystemUserFull systemUserFull, int customerId);
	}
}