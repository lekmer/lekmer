﻿using System.Collections.ObjectModel;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomerGroupSecureService : ICustomerGroupSecureService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<ICustomerGroup> GetAll();
	}
}