﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Litium.Lekmer.Order.Service
{
	[RunInstaller(true)]
	public partial class OrderServiceHosttInstaller : System.Configuration.Install.Installer
	{
		public OrderServiceHosttInstaller()
		{
			InitializeComponent();
		}
	}
}
