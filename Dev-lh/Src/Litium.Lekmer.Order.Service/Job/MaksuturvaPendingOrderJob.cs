﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Service
{
	public class MaksuturvaPendingOrderJob : BaseJob
	{
		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "MaksuturvaPendingOrderJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<IPendingOrderService>().CheckMaksuturvaOrders();
		}
	}
}