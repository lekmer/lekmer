﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Order.Service
{
	public class KlarnaDeletedOrderScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "OrderService"; }
		}

		protected override string GroupName
		{
			get { return "KlarnaDeletedOrderJob"; }
		}

		public int DeletedOrderLifeCycleInDays
		{
			get { return GetInt32(GroupName, "DeletedOrderLifeCycleInDays", 10); }
		}

		public int PendingOrderLifeCycleInDays
		{
			get { return GetInt32(GroupName, "PendingOrderLifeCycleInDays", 10); }
		}

		public string SystemUserName
		{
			get { return GetString(GroupName, "SystemUserName"); }
		}
	}
}