﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Order.Service
{
	public class QliroTimeoutOrderScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "OrderService"; }
		}

		protected override string GroupName
		{
			get { return "QliroTimeoutOrderJob"; }
		}
	}
}