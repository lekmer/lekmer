﻿using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.Foundation;
using Litium.Scensum.FtpServer;

namespace Litium.Lekmer.FtpServer.Console
{
	class Program
	{
		static void Main(string[] args)
		{
			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();

			Initializer.InitializeAll();

			RoutingRegistration.RegisterRoutes();

			ServerStarter.Start(new ConsoleLog());

			System.Console.ReadKey(true);
		}
	}
}
