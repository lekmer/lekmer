using Litium.Framework.Setting;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order.Setting
{
	public class TrustPilotSetting : SettingBase, ITrustPilotSetting
	{
		protected override string StorageName
		{
			get { return "TrustPilot"; }
		}

		public virtual bool Active(IChannel channel)
		{
			return GetBoolean(channel.CommonName, "Active", false);
		}

		public virtual string RecipientName(IChannel channel)
		{
			return GetString(channel.CommonName, "RecipientName");
		}

		public virtual string RecipientAddress(IChannel channel)
		{
			return GetString(channel.CommonName, "RecipientAddress");
		}
	}
}