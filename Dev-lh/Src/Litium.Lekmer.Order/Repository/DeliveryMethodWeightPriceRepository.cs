using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class DeliveryMethodWeightPriceRepository
	{
		protected virtual DataMapperBase<IDeliveryMethodWeightPrice> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IDeliveryMethodWeightPrice>(dataReader);
		}

		public virtual Collection<IDeliveryMethodWeightPrice> GetAllByDeliveryMethod(int deliveryMethodId, int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("DeliveryMethodId", deliveryMethodId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("DeliveryMethodWeightPriceRepository.GetAllByDeliveryMethod");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDeliveryMethodWeightPriceGetAllByDeliveryMethod]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}