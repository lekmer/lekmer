﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class SubPaymentTypeRepository
	{
		protected virtual DataMapperBase<ISubPaymentType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISubPaymentType>(dataReader);
		}

		public virtual Collection<ISubPaymentType> GetByPayment(int paymentTypeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PaymentTypeId", paymentTypeId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SubPaymentTypeRepository.GetByPayment");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pSubPaymentTypeGetByPayment]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}