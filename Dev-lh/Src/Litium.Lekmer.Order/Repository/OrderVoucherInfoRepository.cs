using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class OrderVoucherInfoRepository
	{
		protected virtual DataMapperBase<IOrderVoucherInfo> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IOrderVoucherInfo>(dataReader);
		}

		public virtual IOrderVoucherInfo GetByOrderId(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("OrderVoucherInfoRepository.GetByOrderId");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pOrderVoucherInfoGetByOrderId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(int orderId, IOrderVoucherInfo orderVoucherInfo)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("VoucherCode", orderVoucherInfo.VoucherCode, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("DiscountType", orderVoucherInfo.DiscountType, SqlDbType.Int),
					ParameterHelper.CreateParameter("DiscountValue", orderVoucherInfo.DiscountValue, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("AmountUsed", orderVoucherInfo.AmountUsed, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("AmountLeft", orderVoucherInfo.AmountLeft, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("VoucherStatus", orderVoucherInfo.VoucherStatus, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("OrderVoucherInfoRepository.Save");

			new DataHandler().ExecuteCommand("[orderlek].[pOrderVoucherInfoSave]", parameters, dbSettings);
		}

		public virtual void Delete(int orderId)
		{
			IDataParameter[] parameters = 
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("OrderVoucherInfoRepository.Delete");

			new DataHandler().ExecuteCommand("[orderlek].[pOrderVoucherInfoDelete]", parameters, dbSettings);
		}

		public virtual void UpdateStatus(int orderId, IOrderVoucherInfo orderVoucherInfo)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("VoucherStatus", orderVoucherInfo.VoucherStatus, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("OrderVoucherInfoRepository.UpdateStatus");

			new DataHandler().ExecuteCommand("[orderlek].[pOrderVoucherInfoUpdateStatus]", parameters, dbSettings);
		}
	}
}