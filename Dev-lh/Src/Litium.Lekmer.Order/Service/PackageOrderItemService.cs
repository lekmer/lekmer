using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class PackageOrderItemService : IPackageOrderItemService
	{
		protected PackageOrderItemRepository Repository { get; private set; }

		public PackageOrderItemService(PackageOrderItemRepository repository)
		{
			Repository = repository;
		}

		public virtual int Save(IPackageOrderItem packageOrderItem)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				packageOrderItem.PackageOrderItemId = Repository.Save(packageOrderItem);
				transactedOperation.Complete();
			}
			return packageOrderItem.PackageOrderItemId;
		}

		public virtual Collection<IPackageOrderItem> GetAllByOrder(int orderId)
		{
			return Repository.GetAllByOrder(orderId);
		}

		public virtual Collection<IPackageOrderItem> GetAllByOrderItem(int orderItemId)
		{
			return Repository.GetAllByOrderItem(orderItemId);
		}
	}
}