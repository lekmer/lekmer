﻿using Litium.Lekmer.Order.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Service
{
	public class LekmerBlockCheckoutService : BlockCheckoutService
	{
		public LekmerBlockCheckoutService(BlockCheckoutRepository blockCheckoutRepository)
			: base(blockCheckoutRepository)
		{
		}

		public override IBlockCheckout GetById(IUserContext context, int id)
		{
			return BlockCheckoutCache.Instance.TryGetItem(
				new BlockCheckoutKey(context.Channel.Id, id),
				() => base.GetById(context, id));
		}
	}
}