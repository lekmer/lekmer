﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public class LekmerOrderCampaignInfoService : ILekmerOrderCampaignInfoService
	{
		protected IOrderCartCampaignService OrderCartCampaignService { get; private set; }

		public LekmerOrderCampaignInfoService(IOrderCartCampaignService orderCartCampaignSecureService)
		{
			OrderCartCampaignService = orderCartCampaignSecureService;
		}

		public virtual IOrderCampaignInfo GetByOrder(int orderId)
		{
			if (OrderCartCampaignService == null) throw new InvalidOperationException("OrderCartCampaignService cannot be null.");

			var orderCampaignInfo = IoC.Resolve<IOrderCampaignInfo>();
			orderCampaignInfo.Campaigns = ((ILekmerOrderCartCampaignService)OrderCartCampaignService).GetAllByOrder(orderId);
			return orderCampaignInfo;
		}
	}
}
