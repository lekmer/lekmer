﻿using System.Collections.ObjectModel;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public class LekmerOrderCartCampaignService : OrderCartCampaignService, ILekmerOrderCartCampaignService
	{
		public LekmerOrderCartCampaignService(OrderCartCampaignRepository repository)
			: base(repository)
		{
			Repository = repository;
		}

		protected OrderCartCampaignRepository Repository { get; private set; }

		public virtual Collection<IOrderCampaign> GetAllByOrder(int orderId)
		{
			return Repository.GetAllByOrder(orderId);
		}
	}
}
