using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Mapper
{
	public class PackageOrderItemDataMapper : DataMapperBase<IPackageOrderItem>
	{
		private DataMapperBase<IOrderItemStatus> _orderItemStatusDataMapper;

		public PackageOrderItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_orderItemStatusDataMapper = DataMapperResolver.Resolve<IOrderItemStatus>(DataReader);
		}

		protected override IPackageOrderItem Create()
		{
			var packageOrderItem = IoC.Resolve<IPackageOrderItem>();
			packageOrderItem.PackageOrderItemId = MapValue<int>("PackageOrderItem.PackageOrderItemId");
			packageOrderItem.OrderItemId = MapValue<int>("PackageOrderItem.OrderItemId");
			packageOrderItem.OrderId = MapValue<int>("PackageOrderItem.OrderId");
			packageOrderItem.Quantity = MapValue<int>("PackageOrderItem.Quantity");
			packageOrderItem.SizeId = MapNullableValue<int?>("PackageOrderItem.SizeId");
			packageOrderItem.SizeErpId = MapNullableValue<string>("PackageOrderItem.SizeErpId");
			packageOrderItem.ProductId = MapValue<int>("PackageOrderItem.ProductId");
			packageOrderItem.ErpId = MapNullableValue<string>("PackageOrderItem.ErpId");
			packageOrderItem.EanCode = MapNullableValue<string>("PackageOrderItem.EanCode");
			packageOrderItem.Title = MapValue<string>("PackageOrderItem.Title");
			packageOrderItem.OrderItemStatus = _orderItemStatusDataMapper.MapRow();

			decimal vatAmount = MapValue<decimal>("PackageOrderItem.VAT");
			decimal packagePriceIncludingVat = MapValue<decimal>("PackageOrderItem.PackagePriceIncludingVat");
			decimal actualPriceIncludingVat = MapValue<decimal>("PackageOrderItem.ActualPriceIncludingVat");
			decimal originalPriceIncludingVat = MapValue<decimal>("PackageOrderItem.OriginalPriceIncludingVat");

			decimal vatPercentage = PriceCalc.VatPercentage1(packagePriceIncludingVat, vatAmount);

			decimal packagePriceExcludingVat = packagePriceIncludingVat - vatAmount;
			decimal actualPriceExcludingVat = PriceCalc.PriceExcludingVat1(actualPriceIncludingVat, vatPercentage);
			decimal originalPriceExcludingVat = PriceCalc.PriceExcludingVat1(originalPriceIncludingVat, vatPercentage);

			packageOrderItem.PackagePrice = new Price(packagePriceIncludingVat, packagePriceExcludingVat);
			packageOrderItem.ActualPrice = new Price(actualPriceIncludingVat, actualPriceExcludingVat);
			packageOrderItem.OriginalPrice = new Price(originalPriceIncludingVat, originalPriceExcludingVat);

			packageOrderItem.Status = BusinessObjectStatus.Untouched;
			return packageOrderItem;
		}
	}
}