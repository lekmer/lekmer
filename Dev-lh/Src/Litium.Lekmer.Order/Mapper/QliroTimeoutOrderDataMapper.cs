﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Payment.Qliro;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class QliroTimeoutOrderDataMapper : DataMapperBase<IQliroTimeoutOrder>
	{
		public QliroTimeoutOrderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IQliroTimeoutOrder Create()
		{
			var timeoutOrder = IoC.Resolve<IQliroTimeoutOrder>();

			timeoutOrder.TransactionId = MapValue<int>("TimeoutOrder.TransactionId");
			timeoutOrder.StatusCode = (QliroTransactionStatus?)MapNullableValue<int?>("TimeoutOrder.StatusCode");
			timeoutOrder.InvoiceStatus = (QliroInvoiceStatus?)MapNullableValue<int?>("TimeoutOrder.InvoiceStatus");
			timeoutOrder.OrderId = MapValue<int>("TimeoutOrder.OrderId");
			timeoutOrder.ReservationNumber = MapNullableValue<string>("TimeoutOrder.ReservationNumber");
			timeoutOrder.ChannelId = MapValue<int>("TimeoutOrder.ChannelId");

			return timeoutOrder;
		}
	}
}