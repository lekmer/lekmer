﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class SubPaymentTypeDataMapper : DataMapperBase<ISubPaymentType>
	{
		private int _subPaymentTypeId;
		private int _paymentTypeId;
		private int _commonName;
		private int _title;
		private int _code;

		public SubPaymentTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_subPaymentTypeId = OrdinalOf("SubPaymentTypeId");
			_paymentTypeId = OrdinalOf("PaymentTypeId");
			_commonName = OrdinalOf("CommonName");
			_title = OrdinalOf("Title");
			_code = OrdinalOf("Code");
		}

		protected override ISubPaymentType Create()
		{
			var subPaymentType = IoC.Resolve<ISubPaymentType>();
			subPaymentType.Id = MapValue<int>(_subPaymentTypeId);
			subPaymentType.PaymentId = MapValue<int>(_paymentTypeId);
			subPaymentType.CommonName = MapValue<string>(_commonName);
			subPaymentType.Title = MapValue<string>(_title);
			subPaymentType.Code = MapValue<string>(_code);
			subPaymentType.Status = BusinessObjectStatus.Untouched;
			return subPaymentType;
		}
	}
}