﻿using System.Data;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerDeliveryMethodDataMapper : DeliveryMethodDataMapper
	{
		public LekmerDeliveryMethodDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IDeliveryMethod Create()
		{
			var deliveryMethod = (ILekmerDeliveryMethod)base.Create();

			deliveryMethod.ChannelDefault = MapValue<bool>("DeliveryMethod.ChannelDefault");
			deliveryMethod.Priority = MapValue<int>("DeliveryMethod.Priority");
			deliveryMethod.IsCompany = MapValue<bool>("DeliveryMethod.IsCompany");
			deliveryMethod.DeliveryMethodTypeId = MapValue<int>("DeliveryMethod.DeliveryMethodTypeId");

			deliveryMethod.SetUntouched();

			return deliveryMethod;
		}
	}
}