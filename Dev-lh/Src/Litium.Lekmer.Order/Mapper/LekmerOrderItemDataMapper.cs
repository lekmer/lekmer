using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerOrderItemDataMapper : OrderItemDataMapper
	{
		private DataMapperBase<IOrderItemSize> _orderItemSizeDataMapper;
		private DataMapperBase<IProductSize> _sizeDataMapper;
		private DataMapperBase<IImage> _imageDataMapper;

		public LekmerOrderItemDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_orderItemSizeDataMapper = DataMapperResolver.Resolve<IOrderItemSize>(DataReader);
			_sizeDataMapper = DataMapperResolver.Resolve<IProductSize>(DataReader);
			_imageDataMapper = DataMapperResolver.Resolve<IImage>(DataReader);
		}

		protected override IOrderItem Create()
		{
			var orderItem = (ILekmerOrderItem)base.Create();

			decimal actualVatAmount = MapValue<decimal>("OrderItem.Vat");
			decimal actualPriceInclVat = MapValue<decimal>("OrderItem.ActualPriceIncludingVat");
			decimal originalPriceInclVat = MapValue<decimal>("OrderItem.OriginalPriceIncludingVat");

			decimal actualVatPercentage = PriceCalc.VatPercentage1(actualPriceInclVat, actualVatAmount);

			decimal actualPriceExclVat = actualPriceInclVat - actualVatAmount;
			decimal originalPriceExcludingVat = PriceCalc.PriceExcludingVat1(originalPriceInclVat, actualVatPercentage);

			orderItem.ActualPrice = new Price(actualPriceInclVat, actualPriceExclVat);
			orderItem.OriginalPrice = new Price(originalPriceInclVat, originalPriceExcludingVat);

			orderItem.OrderItemSize = _orderItemSizeDataMapper.MapRow();
			var sizeId = MapNullableValue<int?>("Size.Id");
			if (sizeId.HasValue)
			{
				orderItem.Size = _sizeDataMapper.MapRow();
			}

			orderItem.BrandId = MapNullableValue<int?>("BrandId");

			var imageId = MapNullableValue<int?>("Image.MediaId");
			if (imageId.HasValue)
			{
				orderItem.Image = _imageDataMapper.MapRow();
			}

			orderItem.ProductTypeId = MapValue<int>("OrderItem.ProductTypeId");
			orderItem.IsDropShip = MapValue<bool>("OrderItem.IsDropShip");

			return orderItem;
		}
	}
}