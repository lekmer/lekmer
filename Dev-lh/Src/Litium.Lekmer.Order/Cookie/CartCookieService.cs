﻿using System;
using System.Web;
using Litium.Lekmer.Common;
using Litium.Lekmer.Order.Setting;

namespace Litium.Lekmer.Order.Cookie
{
	public class CartCookieService : ICartCookieService
	{
		protected ICookieService CookieService { get; private set; }
		protected ILekmerCartSetting CartSetting { get; private set; }

		public CartCookieService(ICookieService cookieService, ILekmerCartSetting cartSetting)
		{
			CookieService = cookieService;
			CartSetting = cartSetting;
		}

		public virtual Guid GetShoppingCardGuid()
		{
			if (CookieService == null)
			{
				throw new InvalidOperationException("CookieService must be set before calling GetWishListKey.");
			}

			HttpCookie cookie = CookieService.GetRequestCookie(CartSetting.ShoppingCardCookiename);

			if (cookie == null)
			{
				return Guid.Empty;
			}

			try
			{
				var cardGuid = new Guid(cookie.Value);
				return cardGuid;
			}
			catch (FormatException)
			{
				return Guid.Empty;
			}
		}

		public virtual void SetShoppingCardGuid(Guid shoppingCardGuid)
		{
			if (CookieService == null)
			{
				throw new InvalidOperationException("CookieService must be set before calling GetWishListKey.");
			}

			HttpCookie cookie = CookieService.GetRequestCookie(CartSetting.ShoppingCardCookiename);

			if (cookie == null)
			{
				cookie = new HttpCookie(CartSetting.ShoppingCardCookiename);
			}

			cookie.Expires = DateTime.Now.AddYears(1);
			cookie.Value = shoppingCardGuid.ToString();

			CookieService.SetResponseCookie(cookie);
		}

		public virtual void DeleteShoppingCardGuid()
		{
			if (CookieService == null)
			{
				throw new InvalidOperationException("CookieService must be set before calling GetWishListKey.");
			}

			HttpCookie cookie = CookieService.GetRequestCookie(CartSetting.ShoppingCardCookiename);

			if (cookie != null)
			{
				cookie.Value = Guid.Empty.ToString();
				cookie.Expires = DateTime.Now.AddYears(-1);

				CookieService.SetResponseCookie(cookie);
			}
		}
	}
}