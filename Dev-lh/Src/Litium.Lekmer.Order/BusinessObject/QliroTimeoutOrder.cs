﻿using Litium.Lekmer.Payment.Qliro;

namespace Litium.Lekmer.Order
{
	public class QliroTimeoutOrder : IQliroTimeoutOrder
	{
		public int TransactionId { get; set; }
		public QliroTransactionStatus? StatusCode { get; set; }
		public QliroInvoiceStatus? InvoiceStatus { get; set; }
		public int OrderId { get; set; }
		public string ReservationNumber { get; set; }
		public int ChannelId { get; set; }
	}
}
