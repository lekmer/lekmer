using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class SavedCartObject : BusinessObjectBase, ISavedCartObject
	{
		private int _id;
		private int _cartId;
		private Guid _cartGuid;
		private int _channelId;
		private string _email;
		private bool _isReminderSent;
		private bool _isNeedReminder;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}
		public int CartId
		{
			get { return _cartId; }
			set
			{
				CheckChanged(_cartId, value);
				_cartId = value;
			}
		}
		public Guid CartGuid
		{
			get { return _cartGuid; }
			set
			{
				CheckChanged(_cartGuid, value);
				_cartGuid = value;
			}
		}
		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}
		public string Email
		{
			get { return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}
		public bool IsReminderSent
		{
			get { return _isReminderSent; }
			set
			{
				CheckChanged(_isReminderSent, value);
				_isReminderSent = value;
			}
		}
		public bool IsNeedReminder
		{
			get { return _isNeedReminder; }
			set
			{
				CheckChanged(_isNeedReminder, value);
				_isNeedReminder = value;
			}
		}
	}
}