using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public class PackageOrderItemSecureService : IPackageOrderItemSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected PackageOrderItemRepository Repository { get; private set; }

		public PackageOrderItemSecureService(IAccessValidator accessValidator, PackageOrderItemRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IPackageOrderItem packageOrderItem)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);
			using (var transactedOperation = new TransactedOperation())
			{
				packageOrderItem.PackageOrderItemId = Repository.Save(packageOrderItem);
				transactedOperation.Complete();
			}
			return packageOrderItem.PackageOrderItemId;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int orderItemId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentPackage);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(orderItemId);
				transactedOperation.Complete();
			}
		}

		public virtual Collection<IPackageOrderItem> GetAllByOrder(int orderId)
		{
			return Repository.GetAllByOrder(orderId);
		}

		public virtual Collection<IPackageOrderItem> GetAllByOrderItem(int orderItemId)
		{
			return Repository.GetAllByOrderItem(orderItemId);
		}
	}
}