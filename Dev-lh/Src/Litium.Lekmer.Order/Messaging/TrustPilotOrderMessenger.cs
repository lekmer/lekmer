﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class TrustPilotOrderMessenger : LekmerOrderMessenger
	{
		protected ITrustPilotSetting _trustPilotSetting { get; set; }
		protected TrustPilotOrderConfirmEmailTemplate _template { get; set; }

		public TrustPilotOrderMessenger(IFormatter formatter, ICountryService countryService, ITrustPilotSetting trustPilotSetting)
			: base(formatter, countryService)
		{
			_trustPilotSetting = trustPilotSetting;
		}

		protected override Message CreateMessage(IOrderFull messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");
			if (messageArgs.Customer == null) throw new ArgumentException("messageArgs.Customer can't be null.", "messageArgs");
			if (messageArgs.Customer.CustomerInformation == null) throw new ArgumentException("messageArgs.Customer.CustomerInformation can't be null.", "messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.ChannelId);
			_template = new TrustPilotOrderConfirmEmailTemplate(channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				ProviderName = "email",
				FromName = GetContent("FromName"),
				FromEmail = GetContent("FromEmail"),
				Subject = RenderSubject(messageArgs),
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name = _trustPilotSetting.RecipientName(channel),
				Address = _trustPilotSetting.RecipientAddress(channel)
			});

			return message;
		}

		protected override string RenderSubject(IOrderFull messageArgs)
		{
			Fragment fragmentSubject = _template.GetFragment("Subject");
			fragmentSubject.AddVariable("Order.Number", messageArgs.Number, VariableEncoding.None);
			return fragmentSubject.Render();
		}

		private string RenderMessage(IOrderFull messageArgs)
		{
			var htmlFragment = _template.GetFragment("Content");

			string firstName = messageArgs.Customer.CustomerInformation.FirstName ?? string.Empty;
			string lastName = messageArgs.Customer.CustomerInformation.LastName ?? string.Empty;
			string customerName = firstName + " " + lastName;

			htmlFragment.AddVariable("Customer.Name", customerName);
			htmlFragment.AddVariable("Customer.Email", messageArgs.Customer.CustomerInformation.Email);
			htmlFragment.AddVariable("Order.Number", messageArgs.Number);
			htmlFragment.AddVariable("Order.Id", messageArgs.Id.ToString(CultureInfo.InvariantCulture));
			htmlFragment.AddVariable("Order.Email", messageArgs.Email);

			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		protected override string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(_template.GetFragment(name).Render());
		}
	}
}