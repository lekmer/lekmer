﻿using System;
using System.Reflection;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Order
{
	public class SaveCartMessengerHelper
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IChannel _channel;
		private string _email;

		private ILekmerCartService _cartService;
		private ISavedCartObjectService _savedCartObjectService;

		public SaveCartMessengerHelper(IChannel channel, string email)
		{
			_channel = channel;
			_email = email;

			_cartService = (ILekmerCartService) IoC.Resolve<ICartService>();
			_savedCartObjectService = IoC.Resolve<ISavedCartObjectService>();
		}

		public bool ProcessSaveCartMessage()
		{
			int cartId = 0;
			try
			{
				LekmerCartHelper.RefreshSessionCart();
				var cart = IoC.Resolve<ICartSession>().Cart as ILekmerCartFull;

				if (cart == null || cart.GetCartItems().Count <= 0)
				{
					return false;
				}

				var cloneCart = _cartService.CloneCart(cart);
				cartId = _cartService.SaveCloneCart(CreateUserContext(), cloneCart);
				if (cartId > 0)
				{
					SendMessage(cloneCart);
					var savedCart = _savedCartObjectService.Create(cartId, _channel.Id, _email);
					_savedCartObjectService.InsertSavedCart(savedCart);
					return true;
				}

				return false;
			}
			catch (Exception exception)
			{
				var savedCartEmptyException = exception as SavedCartEmptyException;
				if (savedCartEmptyException != null)
				{
					_log.InfoFormat("SavedCart with cartId = {0} is empty", cartId);
				}
				else
				{
					_log.ErrorFormat("Error during save cart functionality. CartId = {0}. Error message: {1}", cartId, exception);
				}
				return false;
			}
		}

		protected void SendMessage(ILekmerCartFull cart)
		{
			var messageArgs = new SaveCartMessageArgs(_channel, cart, _email);
			var messenger = new SaveCartMessenger();
			messenger.Send(messageArgs);
		}

		protected IUserContext CreateUserContext()
		{
			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = _channel;

			return userContext;
		}
	}
}