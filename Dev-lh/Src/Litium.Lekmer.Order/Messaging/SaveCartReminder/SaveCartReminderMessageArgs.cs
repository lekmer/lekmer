﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public class SaveCartReminderMessageArgs
	{
		public IChannel Channel { get; set; }
		public ILekmerCartFull Cart { get; set; }
		public string Email { get; set; }

		public SaveCartReminderMessageArgs(
			IChannel channel,
			ILekmerCartFull cart,
			string email)
		{
			Channel = channel;
			Cart = cart;
			Email = email;
		}
	}
}