﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Messenger;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class SaveCartReminderMessenger : MessengerBase<SaveCartReminderMessageArgs>
	{
		private readonly string _httpScheme = "http://";
		private readonly string _imageUrlMatchPattern = @"\[Product.ExternalImageUrl\(""(.*?)""\)\]";
		private readonly int _imageUrlMatchCommonNamePosition = 1;

		private ILekmerCartFull _cart;
		private Collection<ILekmerCartItem> _cartItems;
		private IChannel _channel;
		private string _pageUrl;
		private string _mediaUrl;

		private ILekmerFormatter _formatter;
		private IProductSizeService _productSizeService;
		private ITagGroupService _tagGroupService;

		private CartHelper _cartHelper;

		protected ILekmerFormatter Formatter
		{
			get { return _formatter ?? (_formatter = (ILekmerFormatter) IoC.Resolve<IFormatter>()); }
		}
		protected IProductSizeService ProductSizeService
		{
			get { return _productSizeService ?? (_productSizeService = IoC.Resolve<IProductSizeService>()); }
		}
		protected ITagGroupService TagGroupService
		{
			get { return _tagGroupService ?? (_tagGroupService = IoC.Resolve<ITagGroupService>()); }
		}
		protected CartHelper CartHelper
		{
			get { return _cartHelper ?? (_cartHelper = new CartHelper()); }
		}

		protected SaveCartReminderTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "SaveCartReminder"; }
		}

		protected override Message CreateMessage(SaveCartReminderMessageArgs messageArgs)
		{
			if (messageArgs == null || messageArgs.Cart == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			if (messageArgs.Email.IsNullOrTrimmedEmpty())
			{
				throw new ArgumentException("Empty Email");
			}

			_channel = messageArgs.Channel;
			_cart = messageArgs.Cart;
			_cartItems = _cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (_cartItems.Count == 0)
			{
				throw new SavedCartEmptyException("Empty cart");
			}

			Template = new SaveCartReminderTemplate(messageArgs.Channel);
			_pageUrl = Template.GetSetting("PageUrl") ?? string.Empty;
			if (_pageUrl.IsNullOrTrimmedEmpty())
			{
				throw new ArgumentException("Empty PageUrl");
			}

			_mediaUrl = LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);

			var message = new EmailMessage
			{
				Body = RenderMessage(),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			string email = messageArgs.Email;
			message.Recipients.Add(new EmailRecipient
			{
				Name = email,
				Address = email
			});

			return message;
		}

		private string RenderMessage()
		{
			var htmlFragment = Template.GetFragment("Content");

			htmlFragment.AddVariable("CartUrl", string.Format(CultureInfo.CurrentCulture,
				"http://{0}/{1}/?id={2}&guid={3}&tp=tmp", _channel.ApplicationName, _pageUrl, _cart.Id, _cart.CartGuid));

			htmlFragment.AddVariable("Iterate:CartItem", RenderCartItems(_cartItems), VariableEncoding.None);

			// Cart info
			CartHelper.RenderCartInfo(htmlFragment, _cart, Formatter, _channel);

			// Cart price
			CartHelper.RenderCartPrices(htmlFragment, _cart, Formatter, _channel);

			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		protected virtual string RenderCartItems(Collection<ILekmerCartItem> lekmerCartItems)
		{
			var itemBuilder = new StringBuilder();
			foreach (ILekmerCartItem cartItem in lekmerCartItems)
			{
				itemBuilder.Append(RenderCartItem(cartItem, GroupByProductAndSizeAndHashCode(lekmerCartItems)));
			}
			return itemBuilder.ToString();
		}
		protected virtual string RenderCartItem(ILekmerCartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems)
		{
			Fragment fragment = Template.GetFragment("CartItem");

			// Product.
			var product = (ILekmerProduct) cartItem.Product;
			var messageArgs = new ProductMessageArgsSecure(_channel, null, product, string.Empty);
			fragment.AddEntity(messageArgs);

			// Size.
			AddSizeVariavle(cartItem, fragment);

			fragment.AddVariable("CartItem.Quantity", Formatter.FormatNumber(_channel, cartItem.Quantity));

			// Price
			CartHelper.RenderCartItemPrices(fragment, cartItem, Formatter, _channel);

			// Image.
			AddImageVariables(fragment, product.Image, product.DisplayTitle);

			fragment.AddRegexVariable(
				@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;
					return RenderTagGroup(tagGroupCommonName, cartItem);
				},
				VariableEncoding.None);

			return fragment.Render();
		}
		protected virtual string RenderTagGroup(string commonName, ILekmerCartItem cartItem)
		{
			if (commonName == null)
			{
				throw new ArgumentNullException("commonName");
			}

			Collection<ITagGroup> tagGroups = TagGroupService.GetAllByProduct(CreateUserContext(), cartItem.Product.Id);
			ITagGroup tagGroup = tagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}

			Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("TagGroup.Title", AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName), VariableEncoding.None);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);

			return fragmentTagGroup.Render();
		}
		protected virtual string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();

			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				Fragment fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}

			return tagBuilder.ToString();
		}
		protected virtual void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}
		protected virtual void AddImageVariables(Fragment fragment, IImage image, string productTitle)
		{
			string originalSizeImageUrl = null;
			if (image != null)
			{
				var imageLink = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, image, productTitle);
				originalSizeImageUrl = _httpScheme + _channel.ApplicationName + "/" + imageLink.Trim('/');
			}
			fragment.AddVariable("Product.ExternalImageUrl", originalSizeImageUrl);

			fragment.AddRegexVariable(
				_imageUrlMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;
					string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					var commonSizeImageUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(_mediaUrl, image, productTitle, commonName);
					return _httpScheme + _channel.ApplicationName + "/" + commonSizeImageUrl.Trim('/');
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);
		}
		protected virtual void AddSizeVariavle(ILekmerCartItem cartItem, Fragment fragment)
		{
			fragment.AddCondition("CartItem.HasSize", cartItem.SizeId.HasValue);

			var size = GetSize(cartItem);
			var sizeInfo = size.SizeInfo;
			if (sizeInfo != null)
			{
				fragment.AddVariable("Size.Id", sizeInfo.Id.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.EU", FormatSize(size.OverrideEu ?? sizeInfo.Eu));
				fragment.AddVariable("Size.EUTitle", sizeInfo.EuTitle);
				fragment.AddVariable("Size.ErpId", size.ErpId);
			}
		}

		protected virtual IEnumerable<IGrouping<string, ICartItem>> GroupByProductAndSizeAndHashCode(Collection<ILekmerCartItem> lekmerCartItems)
		{
			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var cartItems = new Collection<ICartItem>(lekmerCartItems.Cast<ICartItem>().ToArray());
			return cartItems.GroupBy(item => string.Format("{0}-{1}-{2}", item.Product.Id, ((ILekmerCartItem)item).SizeId, ((ILekmerCartItem)item).PackageElements.GetPackageElementsHashCode()));
		}
		protected virtual IProductSize GetSize(ILekmerCartItem item)
		{
			return item.SizeId.HasValue
				? ProductSizeService.GetById(item.Product.Id, item.SizeId.Value)
				: IoC.Resolve<IProductSize>();
		}
		private static string FormatSize(decimal size)
		{
			var intSize = decimal.ToInt32(size);
			return size - intSize == 0
				? intSize.ToString(CultureInfo.CurrentCulture)
				: size.ToString(CultureInfo.CurrentCulture);
		}

		protected string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}

		protected IUserContext CreateUserContext()
		{
			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = _channel;

			return userContext;
		}
	}
}