﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Media;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Product;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	//The reason we are rendering items by methods here is using the entity mapper will force us to add EntityMappers, 
	//ChannelService and other as components to IOC to be used in a bo and even create a new channel for BO which we do not want
	public class LekmerOrderMessengerSecure : ScensumMessengerBase<IOrderFull>
	{
		protected ILekmerFormatter Formatter { get; private set; }
		protected IMediaUrlSecureService MediaUrlSecureService { get; private set; }
		protected IProductSecureService ProductSecureService { get; private set; }
		protected IPackageSecureService PackageSecureService { get; private set; }
		protected IAliasSharedService AliasSharedService { get; private set; }

		protected override string MessengerName
		{
			get { return "OrderConfirm"; }
		}

		public LekmerOrderMessengerSecure(
			IFormatter formatter,
			IMediaUrlSecureService mediaUrlSecureService,
			IProductSecureService productSecureService,
			IPackageSecureService packageSecureService,
			IAliasSharedService aliasSharedService)
		{
			if (formatter == null) throw new ArgumentNullException("formatter");

			Formatter = (ILekmerFormatter)formatter;
			MediaUrlSecureService = mediaUrlSecureService;
			ProductSecureService = productSecureService;
			PackageSecureService = packageSecureService;
			AliasSharedService = aliasSharedService;
		}

		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
		protected OrderConfirmEmailTemplate Template { get; set; }

		protected virtual OrderConfirmEmailTemplate GetTemplate(IChannel channel)
		{
			return new OrderConfirmEmailTemplate(channel);
		}

		protected override Message CreateMessage(IOrderFull messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");
			if (messageArgs.Customer == null) throw new ArgumentException("messageArgs.Customer can't be null.", "messageArgs");
			if (messageArgs.Customer.CustomerInformation == null) throw new ArgumentException("messageArgs.Customer.CustomerInformation can't be null.", "messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.ChannelId);
			Template = GetTemplate(channel);

			string body = RenderOrder(channel, messageArgs);

			string subject = RenderSubject(messageArgs);

			string fromName = GetContent("FromName");
			string fromEmail = GetContent("FromEmail");

			var message = new EmailMessage
			{
				Body = body,
				ProviderName = "email",
				FromName = fromName,
				FromEmail = fromEmail,
				Subject = subject,
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name =
					messageArgs.Customer.CustomerInformation.FirstName + " " +
					messageArgs.Customer.CustomerInformation.LastName,
				Address = messageArgs.Customer.CustomerInformation.Email
			});

			return message;
		}

		protected virtual string RenderOrder(IChannel channel, IOrderFull order)
		{
			var lekmerOrderFull = (ILekmerOrderFull)order;

			var orderItems = order.GetOrderItems();

			Fragment fragmentContent = Template.GetFragment("Content");

			string paymentTypeTitle = string.Empty;
			string paymentTypeAlias = string.Empty;
			if (order.Payments.Count > 0)
			{
				IPaymentType paymentType = IoC.Resolve<IPaymentTypeSecureService>().GetAll().FirstOrDefault(pt => pt.Id == order.Payments[0].PaymentTypeId);
				if (paymentType != null)
				{
					paymentTypeTitle = paymentType.Title;
					paymentTypeAlias = AliasSharedService.GetValueEncodedByCommonName(channel, "Email.OrderConfirm.PaymentType_" + paymentType.CommonName);
				}
			}
			fragmentContent.AddVariable("Order.PaymentTypeTitle", paymentTypeTitle);
			fragmentContent.AddVariable("Order.PaymentType.Alias", paymentTypeAlias);

			// Delivery method variables.
			AddDeliveryMethodVariables(fragmentContent, order.DeliveryMethodId, channel);

			// Optional delivery method variables.
			AddOptionalDeliveryMethodVariables(fragmentContent, lekmerOrderFull.OptionalDeliveryMethodId, channel);

			// Diapers delivery method variables.
			AddDiapersDeliveryMethodVariables(fragmentContent, lekmerOrderFull.DiapersDeliveryMethodId, channel);

			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(channel, orderItems), VariableEncoding.None);
			AddOrderVariables(channel, fragmentContent, order);//fragmentContent.AddEntity(order);

			var orderCampaignInfo = IoC.Resolve<IOrderCampaignInfoSecureService>().GetByOrder(order.Id);
			if (orderCampaignInfo != null && orderCampaignInfo.Campaigns.Count > 0)
			{
				fragmentContent.AddVariable("Iterate:CartCampaign", RenderCartCampaigns(orderCampaignInfo.Campaigns), VariableEncoding.None);
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", true);
			}
			else
			{
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", false);
			}

			RenderCampaignDiscount(fragmentContent, channel, order, orderCampaignInfo);
			RenderVoucherDiscount(fragmentContent, channel, order);

			var giftCardViaMailInfoCollection = IoC.Resolve<IGiftCardViaMailInfoSecureService>().GetAllByOrder(order.Id);
			if (giftCardViaMailInfoCollection != null && giftCardViaMailInfoCollection.Count > 0)
			{
				fragmentContent.AddCondition("IsGiftCardWillBeSent", true);
				fragmentContent.AddVariable("Iterate:GiftCardViaEmail", RenderGiftCardsViaEmailList(channel, giftCardViaMailInfoCollection), VariableEncoding.None);
			}
			else
			{
				fragmentContent.AddCondition("IsGiftCardWillBeSent", false);
			}

			fragmentContent.AddCondition("Order.IsProductAbove60LExist", IsProductAbove60LExist(channel.Id, orderItems));
			fragmentContent.AddCondition("Order.ContainsMixedItems", CheckForMixedItems(channel.Id, orderItems));
			fragmentContent.AddCondition("Order.HasDropShipItems", lekmerOrderFull.IsDropShipItemExist(orderItems));

			return HttpUtility.HtmlDecode(fragmentContent.Render());
		}

		protected virtual string RenderCartCampaigns(Collection<IOrderCampaign> campaigns)
		{
			if (campaigns == null) return null;

			var campaignBuilder = new StringBuilder();

			foreach (IOrderCampaign campaign in campaigns)
			{
				Fragment fragmentCartCampaign = Template.GetFragment("CartCampaign");
				fragmentCartCampaign.AddVariable("Campaign.Title", campaign.Title);
				campaignBuilder.AppendLine(fragmentCartCampaign.Render());
			}
			return campaignBuilder.ToString();
		}

		protected virtual string RenderOrderItemList(IChannel channel, IEnumerable<IOrderItem> orderItems)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in orderItems)
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				AddOrderItemVariables(channel, fragmentOrderItem, orderItem);//fragmentOrderItem.AddEntity(orderItem);
				fragmentOrderItem.AddVariable("Channel.BaseUrlHttp", GetBaseUrlHttp(channel), VariableEncoding.None);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

		protected virtual void AddDeliveryMethodVariables(Fragment fragment, int deliveryMethodId, IChannel channel)
		{
			IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodSecureService>().GetById(channel.Id, deliveryMethodId);

			fragment.AddVariable("Order.DeliveryMethod.Id", deliveryMethodId.ToString(CultureInfo.InvariantCulture));

			if (deliveryMethod != null)
			{
				fragment.AddVariable("Order.DeliveryMethod.Title", deliveryMethod.Title);
				fragment.AddVariable("Order.DeliveryMethod.Alias", AliasSharedService.GetValueEncodedByCommonName(channel, "Email.OrderConfirm.DeliveryMethod_" + deliveryMethod.CommonName));
			}
			else
			{
				fragment.AddVariable("Order.DeliveryMethod.Title", string.Empty);
				fragment.AddVariable("Order.DeliveryMethod.Alias", string.Empty);
			}
		}

		protected virtual void AddOptionalDeliveryMethodVariables(Fragment fragment, int? deliveryMethodId, IChannel channel)
		{
			fragment.AddCondition("Order.OptionalDeliveryMethod.IsUsed", deliveryMethodId.HasValue);

			if (deliveryMethodId.HasValue)
			{
				IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodSecureService>().GetById(channel.Id, deliveryMethodId.Value);

				fragment.AddVariable("Order.OptionalDeliveryMethod.Id", deliveryMethodId.Value.ToString(CultureInfo.InvariantCulture));

				if (deliveryMethod != null)
				{
					fragment.AddVariable("Order.OptionalDeliveryMethod.Title", deliveryMethod.Title);
					fragment.AddVariable("Order.OptionalDeliveryMethod.Alias", AliasSharedService.GetValueEncodedByCommonName(channel, "Email.OrderConfirm.DeliveryMethod_" + deliveryMethod.CommonName));
				}
				else
				{
					fragment.AddVariable("Order.OptionalDeliveryMethod.Title", string.Empty);
					fragment.AddVariable("Order.OptionalDeliveryMethod.Alias", string.Empty);
				}
			}
		}

		protected virtual void AddDiapersDeliveryMethodVariables(Fragment fragment, int? deliveryMethodId, IChannel channel)
		{
			fragment.AddCondition("Order.DiapersDeliveryMethod.IsUsed", deliveryMethodId.HasValue);

			if (deliveryMethodId.HasValue)
			{
				IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodSecureService>().GetById(channel.Id, deliveryMethodId.Value);

				fragment.AddVariable("Order.DiapersDeliveryMethod.Id", deliveryMethodId.Value.ToString(CultureInfo.InvariantCulture));

				if (deliveryMethod != null)
				{
					fragment.AddVariable("Order.DiapersDeliveryMethod.Title", deliveryMethod.Title);
					fragment.AddVariable("Order.DiapersDeliveryMethod.Alias", AliasSharedService.GetValueEncodedByCommonName(channel, "Email.OrderConfirm.DeliveryMethod_" + deliveryMethod.CommonName));
				}
				else
				{
					fragment.AddVariable("Order.DiapersDeliveryMethod.Title", string.Empty);
					fragment.AddVariable("Order.DiapersDeliveryMethod.Alias", string.Empty);
				}
			}
		}

		protected virtual void AddOrderVariables(IChannel channel, Fragment fragment, IOrderFull order)
		{
			fragment.AddVariable("Order.Id", order.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Order.Number", order.Number);
			fragment.AddVariable("Order.Email", order.Email);
			fragment.AddVariable("Order.IP", order.IP);
			fragment.AddVariable("Order.CreatedDate", Formatter.FormatDateTime(channel, order.CreatedDate));

			var lekmerOrder = (ILekmerOrderFull)order;
			fragment.AddVariable("Order.FreightCost", Formatter.FormatPrice(channel, lekmerOrder.GetActualFreightCost()));
			fragment.AddVariable("Order.FreightCostVat", Formatter.FormatPrice(channel, lekmerOrder.GetActualFreightCostVat()));
			fragment.AddVariable("Order.FreightVat", Formatter.FormatPrice(channel, lekmerOrder.GetActualFreightCostVat()));

			// Optional freight
			fragment.AddCondition("Order.HasOptionalFreightCost", lekmerOrder.OptionalFreightCost.HasValue);
			fragment.AddVariable("Order.OptionalFreightCost", Formatter.FormatPrice(channel, lekmerOrder.GetOptionalFreightCost()));
			fragment.AddVariable("Order.OptionalFreightCostVat", Formatter.FormatPrice(channel, lekmerOrder.GetOptionalFreightCostVat()));

			// Diapers freight
			fragment.AddCondition("Order.HasDiapersFreightCost", lekmerOrder.DiapersFreightCost.HasValue);
			fragment.AddVariable("Order.DiapersFreightCost", Formatter.FormatPrice(channel, lekmerOrder.GetDiapersFreightCost()));
			fragment.AddVariable("Order.DiapersFreightCostVat", Formatter.FormatPrice(channel, lekmerOrder.GetDiapersFreightCostVat()));

			fragment.AddVariable("Order.PaymentCost", Formatter.FormatPrice(channel, lekmerOrder.PaymentCost));
			fragment.AddVariable("Order.PaymentVat", Formatter.FormatPrice(channel, lekmerOrder.GetPaymentCostVat()));
			fragment.AddVariable("Order.TotalQuantity", Formatter.FormatNumber(channel, lekmerOrder.GetOrderItems().Sum(i => i.Quantity)));
			fragment.AddVariable("Order.Status", GetAliasValue(channel, "Order.Status." + lekmerOrder.OrderStatus.CommonName), VariableEncoding.None);

			var orderHelper = new OrderHelper();
			orderHelper.RenderOrderPrices(fragment, lekmerOrder, Formatter, channel);

			AddAddressVariables(fragment, "Order.DeliveryAddress", order.DeliveryAddress);
			AddAddressVariables(fragment, "Order.BillingAddress", order.BillingAddress);

			bool deliveryEqualsBilling = (order.DeliveryAddressId == order.BillingAddressId);
			fragment.AddCondition("Order.DeliveryAddressIsEqualToBillingAddress", deliveryEqualsBilling);

			fragment.AddCondition("Order.HasAlternateAddress", lekmerOrder.AlternateAddress != null);
			if (lekmerOrder.AlternateAddress != null)
			{
				AddAddressVariables(fragment, "Order.AlternateAddress", lekmerOrder.AlternateAddress);
			}

			AddCustomerVariables(fragment, channel, order.Customer);
		}

		protected virtual void AddOrderItemVariables(IChannel channel, Fragment fragment, IOrderItem orderItem)
		{
			//standard order item variables
			fragment.AddVariable("OrderItem.Id", orderItem.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("OrderItem.Title", orderItem.Title);
			fragment.AddVariable("OrderItem.Quantity", Formatter.FormatNumber(channel, orderItem.Quantity));

			//Lekmer specefic order item variables
			fragment.AddVariable("OrderItem.ErpId", orderItem.ErpId);
			fragment.AddVariable("OrderItem.Status", GetAliasValue(channel,"Order.Item.Status." + orderItem.OrderItemStatus.CommonName), VariableEncoding.None);

			var lekmerItem = (ILekmerOrderItem)orderItem;
			fragment.AddCondition("OrderItem.HasSize", lekmerItem.HasSize);
			if (lekmerItem.HasSize)
			{
				AddProductSizeVariables(fragment, lekmerItem.Size); //fragment.AddEntity(lekmerItem.Size);
			}
			fragment.AddCondition("OrderItem.HasBrand", lekmerItem.HasBrand);
			if (lekmerItem.HasBrand)
			{
				AddBrandVariables(channel, fragment, lekmerItem.Brand); //fragment.AddEntity(lekmerItem.Brand);
			}
			fragment.AddCondition("OrderItem.HasImage", lekmerItem.HasImage);
			if (lekmerItem.HasImage)
			{
				AddImageVariables(channel, fragment, lekmerItem.Image);
			}
			fragment.AddCondition("OrderItem.IsDropShip", lekmerItem.IsDropShip);

			var orderItemHelper = new OrderItemHelper();
			orderItemHelper.RenderOrderItemPrices(fragment, orderItem, Formatter, channel);
		}

		protected virtual void AddAddressVariables(Fragment fragment, string prefix, IOrderAddress address)
		{
			var country = IoC.Resolve<ICountrySecureService>().GetById(address.CountryId);
			string countryTitle = country != null ? country.Title : null;

			fragment.AddVariable(prefix + ".Addressee", address.Addressee);
			fragment.AddVariable(prefix + ".StreetAddress", address.StreetAddress);
			fragment.AddVariable(prefix + ".StreetAddress2", address.StreetAddress2);
			fragment.AddVariable(prefix + ".PostalCode", address.PostalCode);
			fragment.AddVariable(prefix + ".City", address.City);
			fragment.AddVariable(prefix + ".Country", countryTitle);
			fragment.AddVariable(prefix + ".PhoneNumber", address.PhoneNumber);

			var lekmerAddress = address as ILekmerOrderAddress;

			if (lekmerAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerAddress.HouseExtension);
				fragment.AddVariable(prefix + ".Reference", lekmerAddress.Reference);
			}
		}

		protected virtual void AddCustomerVariables(Fragment fragment, IChannel channel, ICustomer customer)
		{
			if (fragment == null) throw new ArgumentNullException("fragment");
			if (channel == null) throw new ArgumentNullException("channel");
			if (customer == null) throw new ArgumentNullException("customer");

			fragment.AddVariable("CustomerInformation.Id", customer.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("CustomerInformation.ErpId", customer.ErpId);

			ICustomerInformation info = customer.CustomerInformation;
			fragment.AddCondition("Customer.HasCustomerInformation", info != null);
			if (info != null)
			{
				fragment.AddVariable("CustomerInformation.FirstName", info.FirstName);
				fragment.AddVariable("CustomerInformation.LastName", info.LastName);
				fragment.AddVariable("CustomerInformation.CivicNumber", info.CivicNumber);
				fragment.AddVariable("CustomerInformation.PhoneNumber", info.PhoneNumber);
				fragment.AddVariable("CustomerInformation.CellPhoneNumber", info.CellPhoneNumber);
				fragment.AddVariable("CustomerInformation.Email", info.Email);
				fragment.AddVariable("CustomerInformation.CreatedDate", Formatter.FormatDateTime(channel, info.CreatedDate));

				fragment.AddCondition("CustomerInformation.HasDeliveryAddress", info.DefaultDeliveryAddress != null);
				if (info.DefaultDeliveryAddress != null)
				{
					AddCustomerAddressVariables(fragment, "CustomerInformation.DeliveryAddress", info.DefaultDeliveryAddress);
				}

				fragment.AddCondition("CustomerInformation.HasBillingAddress", info.DefaultBillingAddress != null);
				if (info.DefaultBillingAddress != null)
				{
					AddCustomerAddressVariables(fragment, "CustomerInformation.BillingAddress", info.DefaultBillingAddress);
				}

				bool deliveryEqualsBilling = (info.DefaultDeliveryAddressId == info.DefaultBillingAddressId);
				fragment.AddCondition("CustomerInformation.DeliveryAddressIsEqualToBillingAddress", deliveryEqualsBilling);

				var lekmerInfo = customer.CustomerInformation as ILekmerCustomerInformation;
				if (lekmerInfo != null)
				{
					fragment.AddCondition("CustomerInformation.IsCompany", lekmerInfo.IsCompany);
					fragment.AddCondition("CustomerInformation.HasAlternateAddress", lekmerInfo.AlternateAddress != null);
					if (lekmerInfo.AlternateAddress != null)
					{
						AddCustomerAddressVariables(fragment, "CustomerInformation.AlternateAddress", lekmerInfo.AlternateAddress);
					}
				}
			}
		}

		protected virtual void AddCustomerAddressVariables(Fragment fragment, string prefix, IAddress address)
		{
			var country = IoC.Resolve<ICountrySecureService>().GetById(address.CountryId);
			string countryTitle = country != null ? country.Title : null;

			fragment.AddVariable(prefix + ".Addressee", address.Addressee);
			fragment.AddVariable(prefix + ".StreetAddress", address.StreetAddress);
			fragment.AddVariable(prefix + ".StreetAddress2", address.StreetAddress2);
			fragment.AddVariable(prefix + ".PostalCode", address.PostalCode);
			fragment.AddVariable(prefix + ".City", address.City);
			fragment.AddVariable(prefix + ".Country", countryTitle);
			fragment.AddVariable(prefix + ".PhoneNumber", address.PhoneNumber);

			var lekmerAddress = address as ILekmerAddress;

			if (lekmerAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerAddress.HouseExtension);
				fragment.AddVariable(prefix + ".Reference", lekmerAddress.Reference);
				fragment.AddVariable(prefix + ".DoorCode", lekmerAddress.DoorCode);
			}
		}

		protected virtual void AddProductSizeVariables(Fragment fragment, IProductSize productSize)
		{
			var size = productSize.SizeInfo;
			if (size != null)
			{
				fragment.AddVariable("Size.Id", size.Id.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.EU", FormatSize(productSize.OverrideEu ?? size.Eu));
				fragment.AddVariable("Size.EUTitle", size.EuTitle);
				fragment.AddVariable("Size.USMale", FormatSize(size.UsMale));
				fragment.AddVariable("Size.USFemale", FormatSize(size.UsFemale));
				fragment.AddVariable("Size.UKMale", FormatSize(size.UkMale));
				fragment.AddVariable("Size.UKFemale", FormatSize(size.UkFemale));
				fragment.AddVariable("Size.Millimeter", (productSize.OverrideMillimeter ?? size.Millimeter + (productSize.MillimeterDeviation ?? 0)).ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.NumberInStock", productSize.NumberInStock.ToString(CultureInfo.InvariantCulture));
				fragment.AddVariable("Size.ErpId", productSize.ErpId);

				fragment.AddCondition("Size.IsInStock", productSize.NumberInStock > 0);
				fragment.AddCondition("Size.IsPassive", productSize.IsPassiveSize());
			}
		}

		protected virtual void AddBrandVariables(IChannel channel, Fragment fragment, IBrand brand)
		{
			fragment.AddVariable("Brand.Id", brand.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Brand.Title", brand.Title);
			fragment.AddVariable("Brand.ExternalUrl", brand.ExternalUrl);

			fragment.AddCondition("Brand.HasUrl", false);
			fragment.AddVariable("Brand.Url", string.Empty);

			fragment.AddVariable("Brand.Description", brand.Description, VariableEncoding.None);

			AddBrandImageVariables(channel, fragment, brand.Image, brand.Title);
		}

		protected virtual void AddImageVariables(IChannel channel, Fragment fragment, IImage item)
		{
			const string imageUrlMatchPattern = @"\[Image.Url\(""(.*?)""\)\]";
			const string imageWidthMatchPattern = @"\[Image.Width\(""(.*?)""\)\]";
			const string imageHeightMatchPattern = @"\[Image.Height\(""(.*?)""\)\]";
			const int imageUrlMatchCommonNamePosition = 1;

			fragment.AddVariable("Image.Link", ((ILekmerImage)item).Link);
			fragment.AddVariable("Image.Parameter", ((ILekmerImage)item).Parameter);
			fragment.AddCondition("Image.HasTumbnailImageUrl", !string.IsNullOrEmpty(((ILekmerImage)item).TumbnailImageUrl));
			fragment.AddVariable("Image.TumbnailImageUrl", ((ILekmerImage)item).TumbnailImageUrl);
			fragment.AddCondition("Image.IsLink", ((ILekmerImage)item).IsLink);
			fragment.AddCondition("Image.HasImage", ((ILekmerImage)item).HasImage);

			fragment.AddVariable("Image.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Image.Title", item.Title);
			fragment.AddVariable("Image.AlternativeText", item.AlternativeText);

			fragment.AddVariable("Image.Width", item.Width.ToString(CultureInfo.CurrentCulture));
			fragment.AddVariable("Image.Height", item.Height.ToString(CultureInfo.CurrentCulture));

			string mediaUrl = MediaUrlSecureService.ResolveMediaArchiveExternalUrl(channel, item);

			fragment.AddVariable("Image.Url", MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, item));

			fragment.AddRegexVariable(
				imageUrlMatchPattern,
				delegate(Match match)
				{
					string commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;

					return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, item, commonName);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				imageWidthMatchPattern,
				delegate(Match match)
				{
					var commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(item, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				imageHeightMatchPattern,
				delegate(Match match)
				{
					var commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(item, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}

		protected virtual void AddBrandImageVariables(IChannel channel, Fragment fragment, IImage image, string brandTitle)
		{
			const string imageUrlMatchPattern = @"\[Brand.ImageUrl\(""(.*?)""\)\]";
			const string imageWidthMatchPattern = @"\[Brand.ImageWidth\(""(.*?)""\)\]";
			const string imageHeightMatchPattern = @"\[Brand.ImageHeight\(""(.*?)""\)\]";
			const int imageUrlMatchCommonNamePosition = 1;

			string originalSizeImageUrl = null;
			string alternativeText = null;
			string mediaUrl = null;

			if (image != null)
			{
				alternativeText = image.AlternativeText;
				mediaUrl = MediaUrlSecureService.ResolveMediaArchiveExternalUrl(channel, image);

				originalSizeImageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image, brandTitle);
			}

			fragment.AddCondition("Brand.HasImage", image != null);
			fragment.AddVariable("Brand.ImageUrl", originalSizeImageUrl);
			fragment.AddCondition("Brand.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
			fragment.AddVariable("Brand.ImageAlternativeText", alternativeText);
			fragment.AddVariable("Brand.ImageWidth", image != null ? image.Width.ToString(CultureInfo.CurrentCulture) : null);
			fragment.AddVariable("Brand.ImageHeight", image != null ? image.Height.ToString(CultureInfo.CurrentCulture) : null);

			fragment.AddRegexVariable(
				imageUrlMatchPattern,
				delegate(Match match)
					{
						if (image == null) return null;

						string commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;

						return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, image, brandTitle, commonName);
					},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				imageWidthMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				imageHeightMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}

		protected string RenderSubject(IOrderFull order)
		{
			Fragment fragmentSubject = Template.GetFragment("Subject");
			fragmentSubject.AddVariable("Order.Number", order.Number, VariableEncoding.None);
			return HttpUtility.HtmlDecode(fragmentSubject.Render());
		}

		protected virtual string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}

		protected virtual string GetAliasValue(IChannel channel, string commonName)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var aliasService = IoC.Resolve<IAliasSharedService>();
			string value = aliasService.GetValueEncodedByCommonName(channel, commonName);
			return value ?? string.Format(CultureInfo.InvariantCulture, "[ Alias '{0}' could not be found. ]", commonName);
		}

		protected virtual string GetBaseUrlHttp(IChannel channel)
		{
			return "http://" + channel.ApplicationName;
		}

		protected virtual string FormatSize(decimal size)
		{
			var intSize = decimal.ToInt32(size);
			return size - intSize == 0
				? intSize.ToString(CultureInfo.CurrentCulture)
				: size.ToString(CultureInfo.CurrentCulture);
		}

		protected virtual void RenderCampaignDiscount(Fragment fragmentContent, IChannel channel, IOrderFull order, IOrderCampaignInfo orderCampaignInfo)
		{
			var lekmerOrderFull = (ILekmerOrderFull)order;

			bool isFixedDiscount = false;
			decimal totalFixedDiscount = 0;

			bool isPercentageDiscount = false;
			decimal totalPercentageDiscount = 0;

			if (orderCampaignInfo != null && orderCampaignInfo.Campaigns.Count > 0)
			{
				lekmerOrderFull.GetTotalDiscount(
					channel,
					orderCampaignInfo,
					out isFixedDiscount,
					out totalFixedDiscount,
					out isPercentageDiscount,
					out totalPercentageDiscount);
			}

			fragmentContent.AddCondition("IsFixedDiscountApplied", isFixedDiscount);
			if (isFixedDiscount)
			{
				fragmentContent.AddVariable("FixedDiscount", Formatter.FormatPriceTwoOrLessDecimals(channel, totalFixedDiscount));
			}

			fragmentContent.AddCondition("IsPercentageDiscountApplied", isPercentageDiscount);
			if (isPercentageDiscount)
			{
				fragmentContent.AddVariable("PercentageDiscount", string.Format("{0} %", Math.Round(totalPercentageDiscount, 0, MidpointRounding.AwayFromZero)));
			}
		}

		protected virtual void RenderVoucherDiscount(Fragment fragmentContent, IChannel channel, IOrderFull order)
		{
			var lekmerOrder = (ILekmerOrderFull)order;

			IOrderVoucherInfo voucherInfo = lekmerOrder.VoucherInfo;

			if (voucherInfo != null && voucherInfo.VoucherCode.HasValue())
			{
				fragmentContent.AddCondition("IsVoucherUsed", true);
				fragmentContent.AddCondition("IsVoucherApplied", voucherInfo.DiscountValue.HasValue);

				bool hasAmountUsed = voucherInfo.AmountUsed.HasValue && voucherInfo.AmountUsed.Value > 0m;
				fragmentContent.AddCondition("VoucherDiscount.HasAmountUsed", hasAmountUsed);

				if (voucherInfo.AmountUsed.HasValue)
				{
					fragmentContent.AddVariable("VoucherDiscount", Formatter.FormatPriceTwoOrLessDecimals(channel, voucherInfo.AmountUsed.Value));
				}
				else
				{
					fragmentContent.AddVariable("VoucherDiscount", Formatter.FormatPriceTwoOrLessDecimals(channel, 0m));
				}

				fragmentContent.AddCondition("IsVoucherWithoutDiscount", voucherInfo.DiscountValue.HasValue && voucherInfo.DiscountValue <= 0);
			}
			else
			{
				fragmentContent.AddCondition("IsVoucherUsed", false);
				fragmentContent.AddCondition("IsVoucherApplied", false);
				fragmentContent.AddCondition("VoucherDiscount.HasAmountUsed", false);
			}
		}

		private string RenderGiftCardsViaEmailList(IChannel channel, IEnumerable<IGiftCardViaMailInfo> giftCardViaMailInfoCollection)
		{
			var itemBuilder = new StringBuilder();

			foreach (IGiftCardViaMailInfo giftCardViaMailInfo in giftCardViaMailInfoCollection)
			{
				Fragment giftCardViaEmail = Template.GetFragment("GiftCardViaEmail");

				giftCardViaEmail.AddVariable("GiftCard.DiscountAmount", Formatter.FormatPriceChannelOrLessDecimals(channel, giftCardViaMailInfo.DiscountValue));
				giftCardViaEmail.AddVariable("GiftCard.DiscountAmount_Formatted", Formatter.FormatPriceTwoOrLessDecimals(channel, giftCardViaMailInfo.DiscountValue));
				giftCardViaEmail.AddVariable("GiftCard.DateToSend", Formatter.FormatShortDate(channel, giftCardViaMailInfo.DateToSend));
				giftCardViaEmail.AddVariable("GiftCard.Currency", channel.Currency.Iso);
				giftCardViaEmail.AddVariable("GiftCard.CampaignTitle", GetCampaignTitle(giftCardViaMailInfo.CampaignId));

				itemBuilder.AppendLine(giftCardViaEmail.Render());
			}

			return itemBuilder.ToString();
		}

		private string GetCampaignTitle(int campaignId)
		{
			string title = string.Empty;

			var cartCampaign = IoC.Resolve<ICartCampaignSharedService>().GetById(campaignId);
			if (cartCampaign != null)
			{
				title = cartCampaign.Title;
			}
			else
			{
				var productCampaign = IoC.Resolve<IProductCampaignSecureService>().GetById(campaignId);
				if (productCampaign != null)
				{
					title = productCampaign.Title;
				}
			}

			return title;
		}

		protected virtual bool IsProductAbove60LExist(int channelId, IEnumerable<IOrderItem> orderItems)
		{
			bool isProductAbove60L = false;
			bool isProductUnder60L = false;

			foreach (var item in orderItems)
			{
				if (isProductAbove60L && isProductUnder60L) break;

				var product = ProductSecureService.GetById(channelId, item.ProductId);
				CheckWarehouse(channelId, product, ref isProductAbove60L, ref isProductUnder60L);
			}

			return isProductAbove60L && isProductUnder60L;
		}
		protected virtual void CheckWarehouse(int channelId, IProduct product, ref bool isProductAbove60L, ref bool isProductUnder60L)
		{
			if (product.IsPackage())
			{
				var package = PackageSecureService.GetByProductIdSecure(channelId, product.Id);
				var packageProducts = ((ILekmerProductSecureService)ProductSecureService).GetAllByPackage(channelId, package.PackageId);
				foreach (var packageProduct in packageProducts)
				{
					if (isProductAbove60L && isProductUnder60L) break;
					IsProductAbove60(packageProduct, ref isProductAbove60L, ref isProductUnder60L);
				}
			}
			else
			{
				IsProductAbove60(product, ref isProductAbove60L, ref isProductUnder60L);
			}
		}
		protected virtual void IsProductAbove60(IProduct product, ref bool isProductAbove60L, ref bool isProductUnder60L)
		{
			if (((ILekmerProduct)product).IsAbove60L)
			{
				isProductAbove60L = true;
			}
			else
			{
				isProductUnder60L = true;
			}
		}

		protected virtual bool CheckForMixedItems(int channelId, IEnumerable<IOrderItem> orderItems)
		{
			bool regularItems = false;
			bool diapers = false;

			int diapersBrand = IoC.Resolve<ILekmerCartSetting>().DiapersBrand;

			foreach (var item in orderItems)
			{
				if (regularItems && diapers) break;

				var product = (ILekmerProduct) ProductSecureService.GetById(channelId, item.ProductId);
				if (product == null || !product.BrandId.HasValue) continue;

				if (product.BrandId.Value == diapersBrand)
				{
					diapers = true;
				}
				else
				{
					regularItems = true;
				}
			}

			return regularItems && diapers;
		}
	}
}