﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class OrderRejectMessenger : ScensumMessengerBase<IOrderFull>
	{
		protected OrderRejectEmailTemplate Template { get; set; }
		protected IFormatter Formatter { get; set; }

		public OrderRejectMessenger(IFormatter formatter)
		{
			if (formatter == null) throw new ArgumentNullException("formatter");

			Formatter = formatter;
		}

		protected override string MessengerName
		{
			get { return "OrderReject"; }
		}

		protected virtual OrderRejectEmailTemplate GetTemplate(IChannel channel)
		{
			return new OrderRejectEmailTemplate(channel);
		}

		protected override Message CreateMessage(IOrderFull messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");
			if (messageArgs.Customer == null) throw new ArgumentException("messageArgs.Customer can't be null.", "messageArgs");
			if (messageArgs.Customer.CustomerInformation == null) throw new ArgumentException("messageArgs.Customer.CustomerInformation can't be null.", "messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.ChannelId);
			Template = GetTemplate(channel);

			string body = RenderMessage(channel, messageArgs);
			string subject = RenderSubject(messageArgs);
			string fromName = GetContent("FromName");
			string fromEmail = GetContent("FromEmail");

			var message = new EmailMessage
			{
				Body = body,
				ProviderName = "email",
				FromName = fromName,
				FromEmail = fromEmail,
				Subject = subject,
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name =
					messageArgs.Customer.CustomerInformation.FirstName + " " +
					messageArgs.Customer.CustomerInformation.LastName,
				Address = messageArgs.Customer.CustomerInformation.Email
			});

			return message;
		}

		protected virtual string RenderSubject(IOrderFull messageArgs)
		{
			Fragment fragmentSubject = Template.GetFragment("Subject");

			fragmentSubject.AddVariable("Order.Number", messageArgs.Number, VariableEncoding.None);

			return fragmentSubject.Render();
		}

		protected virtual string RenderMessage(IChannel channel, IOrderFull messageArgs)
		{
			var htmlFragment = Template.GetFragment("Content");

			string firstName = messageArgs.Customer.CustomerInformation.FirstName ?? string.Empty;
			string lastName = messageArgs.Customer.CustomerInformation.LastName ?? string.Empty;
			string customerName = firstName + " " + lastName;

			htmlFragment.AddVariable("Customer.Name", customerName);
			htmlFragment.AddVariable("Customer.Email", messageArgs.Customer.CustomerInformation.Email);
			htmlFragment.AddVariable("Order.Number", messageArgs.Number);
			htmlFragment.AddVariable("Order.Id", messageArgs.Id.ToString(CultureInfo.InvariantCulture));
			htmlFragment.AddVariable("Order.Email", messageArgs.Email);
			htmlFragment.AddVariable("Order.CreatedDate", Formatter.FormatDateTime(channel, messageArgs.CreatedDate));

			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		protected virtual string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}