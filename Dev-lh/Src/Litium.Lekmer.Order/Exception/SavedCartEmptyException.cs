﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class SavedCartEmptyException : System.Exception
	{
		public SavedCartEmptyException()
		{
		}

		public SavedCartEmptyException(string message)
			: base(message)
		{
		}

		public SavedCartEmptyException(string message, System.Exception innerException)
			: base(message, innerException)
		{
		}

		protected SavedCartEmptyException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
