﻿using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockSkrillResponseUnsuccessfulControl : BlockControlBase
	{
		private readonly IOrderSession _orderSession;
		private readonly IContentNodeService _contentNodeService;
		private readonly ILekmerOrderService _orderService;
		private readonly IOrderStatusService _orderStatusService;

		public BlockSkrillResponseUnsuccessfulControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IOrderSession orderSession,
			IContentNodeService contentNodeService,
			IOrderService orderService,
			IOrderStatusService orderStatusService)
			: base(templateFactory, blockService)
		{
			_contentNodeService = contentNodeService;
			_orderSession = orderSession;
			_orderService = (ILekmerOrderService) orderService;
			_orderStatusService = orderStatusService;
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "SkrillResponseUnsuccessful"; }
		}

		protected override BlockContent RenderCore()
		{
			RejectOrder();

			Response.Redirect(GetRedirectUrl("Checkout"));

			return new BlockContent("", "", ""); 
		}

		private void RejectOrder()
		{
			if (_orderSession.OrderId != null)
			{
				var lekmerOrder = (ILekmerOrderFull)_orderService.GetFullById(UserContext.Current, _orderSession.OrderId.Value);
				
				_orderService.OrderVoucherRelease(UserContext.Current, lekmerOrder);

				if (lekmerOrder.OrderStatus.CommonName == OrderStatusName.PaymentPending)
				{
					lekmerOrder.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.Canceled);
					_orderService.SaveFull(UserContext.Current, lekmerOrder);
				}
			}

			// Session clean

			_orderSession.OrderId = null;

			IoC.Resolve<IVoucherSession>().Voucher = null;
			IoC.Resolve<IUserContextFactory>().FlushCache();
		}

		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

			if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
			{
				return UrlHelper.ResolveUrlHttps(contentNodeTreeItem.Url);
			}

			return UrlHelper.BaseUrlHttp;
		}
	}
}