﻿using System.Text;
using Litium.Lekmer.Order;
using Litium.Lekmer.Order.Campaign;
using Litium.Lekmer.Payment.Maksuturva;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockMaksuturvaPaymentControl : BlockControlBase
	{
		private readonly IContentNodeService _contentNodeService;
		private readonly IMaksuturvaPaymentProvider _maksuturvaPaymentProvider;
		private readonly IOrderSession _orderSession;
		private IOrderFull _order;

		public BlockMaksuturvaPaymentControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IMaksuturvaPaymentProvider maksuturvaPaymentProvider,
			IOrderSession orderSession,
			IContentNodeService contentNodeService) : base(templateFactory, blockService)
		{
			_maksuturvaPaymentProvider = maksuturvaPaymentProvider;
			_orderSession = orderSession;
			_contentNodeService = contentNodeService;
		}

		protected override string ModelCommonName
		{
			get { return "MaksuturvaPayment"; }
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			if (_order == null)
			{
				Response.Redirect(GetRedirectUrl("Checkout"));
			}
			else if (_order.OrderStatus.CommonName != OrderStatusName.PaymentPending)
			{
				Response.Redirect(GetRedirectUrl("Checkout"));
			}

			string paymentUrl = _maksuturvaPaymentProvider.GetMaksuturvaPaymentUrl(_order.ChannelId);
			string paymentData = RenderFields();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Maksuturva.Payment.Url", paymentUrl, VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:Field", paymentData, VariableEncoding.None);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");

			return new BlockContent(head, fragmentContent.Render() ?? string.Empty, footer);
		}

		private void Initialize()
		{
			_order = GetOrder();
			if (_order != null)
			{
				FullFillOrderWithCampaignInfo(_order);
			}
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			return fragment.Render() ?? string.Empty;
		}

		private string RenderFields()
		{
			var fieldBuilder = new StringBuilder();

			var parameters = _maksuturvaPaymentProvider.GetMaksuturvaPaymentData(UserContext.Current, _order);
			foreach (var parameter in parameters)
			{
				var fragmentField = Template.GetFragment("Field");
				fragmentField.AddVariable("Maksuturva.Field.Name", parameter.Key, VariableEncoding.None);
				fragmentField.AddVariable("Maksuturva.Field.Value", parameter.Value, VariableEncoding.HtmlEncode);
				fieldBuilder.AppendLine(fragmentField.Render());
			}

			return fieldBuilder.ToString();
		}

		private IOrderFull GetOrder()
		{
			int? orderId = _orderSession.OrderId;
			return orderId.HasValue ? IoC.Resolve<IOrderService>().GetFullById(UserContext.Current, orderId.Value) : null;
		}

		private void FullFillOrderWithCampaignInfo(IOrderFull order)
		{
			if (order.CampaignInfo == null)
			{
				order.CampaignInfo = IoC.Resolve<ILekmerOrderCampaignInfoService>().GetByOrder(order.Id);
			}
		}

		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

			if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
			{
				return UrlHelper.ResolveUrlHttps(contentNodeTreeItem.Url);
			}

			return UrlHelper.BaseUrlHttp;
		}
	}
}