﻿using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order;
using Litium.Lekmer.Order.Cookie;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockSkrillResponseSuccessfulControl : BlockControlBase
	{
		private readonly ICartSession _cartSession;
		private readonly IOrderSession _orderSession;
		private readonly ILekmerOrderService _orderService;
		private readonly IContentNodeService _contentNodeService;

		public BlockSkrillResponseSuccessfulControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICartSession cartSession,
			IOrderSession orderSession,
			IOrderService orderService,
			IContentNodeService contentNodeService)
			: base(templateFactory, blockService)
		{
			_contentNodeService = contentNodeService;
			_cartSession = cartSession;
			_orderSession = orderSession;
			_orderService = (ILekmerOrderService) orderService;
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "SkrillResponseSuccessful"; }
		}

		protected override BlockContent RenderCore()
		{
			ConfirmOrder();
			ResetCart();

			Response.Redirect(GetRedirectUrl("CheckoutConfirm"));

			return new BlockContent("", "", ""); 
		}

		private void ResetCart()
		{
			IoC.Resolve<ICartCookieService>().DeleteShoppingCardGuid();
			_cartSession.Cart = null;
			IoC.Resolve<ICheckoutSession>().CleanProfile();
		}

		private void ConfirmOrder()
		{
			if (_orderSession.OrderId != null)
			{
				var order = (ILekmerOrderFull)_orderService.GetFullById(UserContext.Current, _orderSession.OrderId.Value);
				_orderService.OrderVoucherConsume(UserContext.Current, order);
			}

			IoC.Resolve<IVoucherSession>().Voucher = null;
			IoC.Resolve<IUserContextFactory>().FlushCache();
		}

		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

			if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
			{
				return UrlHelper.ResolveUrlHttp(contentNodeTreeItem.Url);
			}

			return UrlHelper.BaseUrlHttp;
		}
	}
}