﻿using System;
using System.Globalization;

namespace Litium.Scensum.BackOffice.CommonItems
{
	public static class Converter
	{
		public static string ToLocalStringAsNumber(this decimal value)
		{
			return value.ToString("N", CultureInfo.CurrentCulture);
		}

		public static string ToLocalString(this decimal value)
		{
			return value.ToString(CultureInfo.CurrentCulture);
		}

		public static string ToLocalString(this int value)
		{
			return value.ToString(CultureInfo.CurrentCulture);
		}

		public static decimal ToLocalDecimal(this string value)
		{
			decimal result;
			if (decimal.TryParse(value, NumberStyles.Number, CultureInfo.CurrentCulture, out result))
			{
				return result;
			}
			throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Cannot convert string value \"{0}\" to decimal", value));
		}

		public static int ToLocalInt32(this string value)
		{
			int result;
			if (int.TryParse(value, NumberStyles.Number, CultureInfo.CurrentCulture, out result))
			{
				return result;
			}
			throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Cannot convert string value \"{0}\" to integer", value));
		}

		public static bool TryParseLocalDecimalAsNumber(this string value, out decimal result)
		{
			return decimal.TryParse(value, NumberStyles.Number, CultureInfo.CurrentCulture, out result);
		}
	}
}
