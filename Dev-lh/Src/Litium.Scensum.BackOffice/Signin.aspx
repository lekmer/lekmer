<%@ Page Language="C#"  CodeBehind="Signin.aspx.cs" Inherits="Litium.Scensum.BackOffice.Signin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
	<title>Sign in</title>
	<link href="~/Media/Css/Signin.css" rel="stylesheet" type="text/css" />
	<link href="~/Media/Css/content.css" rel="stylesheet" type="text/css" />
	<link href="~/Media/Css/Assortment.css" rel="stylesheet" type="text/css" />
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>"
		            type="text/javascript"></script> 
</head>
<body>

	<script type='text/javascript' language='javascript'>

		function ShowEmail() {
			Email = document.getElementById('divEmail');
			Email.className = "show";
			ErrorMessage = document.getElementById('errors_divMessages');
			if(ErrorMessage!=null)
			ErrorMessage.className = "hide";
		} 
	</script>

	<form id="form1" runat="server">
	<div id="signin">
		<div id="left-side" class="left">
			<img src="Media/Images/SignIn/login-img.png" alt="" />
		</div>
		<div id="right-side" class="left">
		    <img src="Media/Images/MasterPage/logo.gif" alt="" />
			<br class="clear" />
			<div id="signin-form" class="left clear">
				<span class="administration-header">Administration</span>
				<div id="signin-description">
					<span>Sign in by entering your user name and password.</span>
				</div>
				<div>
				    <asp:Panel ID="FormPanel" DefaultButton="btnSignIn" runat="server">
					    <div class="input-box">
						    <asp:Label ID="lblUserName" runat="server" Text="User name *" />
						    <asp:RequiredFieldValidator ID="rfvUserName" ValidationGroup="SignIn" ControlToValidate="tbUserName" runat="server" Display ="None" ErrorMessage="" />
						    <asp:RegularExpressionValidator ID="revUserName" runat="server" ValidationGroup="SignIn" ControlToValidate="tbUserName" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage='<%$ Resources:GeneralMessage, InvalidUserName %>'></asp:RegularExpressionValidator>
						    <br />
						    <asp:TextBox ID="tbUserName" Width="250px" runat="server" />
					    </div>
					    <div class="input-box">
						    <asp:Label ID="lblPassword" runat="server" Text="Password *" />
						    <asp:RequiredFieldValidator ID="rfvPassword" ControlToValidate="tbPassword" ValidationGroup="SignIn" runat="server" Display ="None" ErrorMessage="" />
						    <br />
						    <asp:TextBox ID="tbPassword" Width="250px"  TextMode="Password" runat="server" />
						     <br />
						    <uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnSignIn" ValidationGroup="SignIn" SkinID="DefaultButton" runat="server" Text="Sign in" />
					    </div>
					</asp:Panel>				
					<uc:MessageContainer ID="errors" MessageType="Warning" HideMessagesControlId="btnSignIn" runat="server" />
				</div>
			</div>
		</div>
		<br class="clear"/>
		<div id="bottom-line">
			E-commerce platform for professionals. Powered by Litium. <a href="http://www.litiumscensum.se/" target="_parent">www.litiumscensum.se</a>
		</div>
	</div>
	</form>
</body>
</html>
