﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Campaigns.Flags
{
	public partial class FlagEdit : LekmerPageController, IEditor
	{
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			var flagId = GetIdOrNull();
			if (!flagId.HasValue)
			{
				FlagHeader.Text = Resources.Campaign.Literal_CreateFlag;
			}
			else
			{
				var flag = IoC.Resolve<IFlagSecureService>().GetById(flagId.Value);
				FlagTitleBox.Text = flag.Title;
				FlagClassBox.Text = flag.Class;
				FlagHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"",
											 Resources.Campaign.Literal_EditFlag, flag.Title);
			}
			PopulateTranslations();
		}

		public void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx");
		}

		public void OnSave(object sender, EventArgs e)
		{
			Page.Validate("FlagValidationGroup");
			if (!Page.IsValid)
			{
				return;
			}
			var flagId = GetIdOrNull();
			var service = IoC.Resolve<IFlagSecureService>();
			var flag = flagId.HasValue ? service.GetById(flagId.Value) : service.Create();
			if (flagId.HasValue && flag == null)
			{
				throw new BusinessObjectNotExistsException(flagId.Value);
			}
			flag.Title = FlagTitleBox.Text.Trim();
			flag.Class = FlagClassBox.Text.Trim();
			flag.Id = service.Save(SignInHelper.SignedInSystemUser, flag, FlagTitleTranslator.GetTranslations());
			if (flag.Id < 0)
			{
				Messager.Add(CampaignMessage.SameFlagExists, InfoType.Failure);
			}
			else
			{
				Response.Redirect("Default.aspx");
			}
		}

		private void PopulateTranslations()
		{
			var flagId = GetIdOrNull();
			var service = IoC.Resolve<IFlagSecureService>();
			FlagTitleTranslator.DefaultValueControlClientId = FlagTitleBox.ClientID;
			FlagTitleTranslator.BusinessObjectId = flagId ?? 0;
			FlagTitleTranslator.DataSource = flagId.HasValue
				? service.GetAllTranslationsByFlag(flagId.Value)
				: new Collection<ITranslationGeneric>();
			FlagTitleTranslator.DataBind();
		}
	}
}