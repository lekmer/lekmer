﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Campaigns.Flags
{
	public partial class Default : LekmerStatePageController<FlagsOrderState>, IEditor 
	{
		private const int MoveUpStep = -15;
		private const int MoveDownStep = 15;
		private const int DifferenceStep = 10;
		private const int MinStep = 1;

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			DeleteFlagButton.Click += OnDelete;
			FlagsGrid.RowCommand += OnRowCommand;
			FlagsGrid.RowDataBound += OnRowDataBound;
		}

		protected override void PopulateForm()
		{
			PopulateData();
		}

		public void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx");
		}

		public void OnSave(object sender, EventArgs e)
		{
			RefreshPriority();
			var flags = Sort(State.Flags, MinStep);
			var service = IoC.Resolve<IFlagSecureService>();
			service.SetOrder(SignInHelper.SignedInSystemUser, flags);
			SuccessMessager.Add(Resources.CampaignMessage.SetFlagsOrdinalSuccess);
		}

		public void OnDelete(object sender, EventArgs e)
		{
			IEnumerable<int> ids = FlagsGrid.GetSelectedIds();
			if (ids.Count() <= 0)
			{
				ErrorMessager.Add(Resources.CampaignMessage.NoneFlagSelected);
				return;
			}
			DeleteFlags(ids);

			SuccessMessager.Add(Resources.GeneralMessage.DeleteSeccessful);

			PopulateData();
		}

		protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected void OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id)) return;

			switch (e.CommandName)
			{
				case "UpOrdinal":
					Move(id, MoveUpStep);
					break;
				case "DownOrdinal":
					Move(id, MoveDownStep);
					break;
				case "RefreshOrdinal":
					RefreshPriority();
					break;
				case "DeleteFlag":
					DeleteFlag(id);
					SuccessMessager.Add(Resources.GeneralMessage.DeleteSeccessful);
					PopulateData();
					break;
				case "NavigateFlag":
					Response.Redirect(string.Format("FlagEdit.aspx?Id={0}", id));
					break;
			}
		}

		protected void Move(int elementId, int step)
		{
			var flags = State.Flags;
			var flag = flags.FirstOrDefault(x => x.Id == elementId);
			if (flag == null) return;
			flag.Ordinal += step;
			State.Flags = Sort(flags, DifferenceStep);
			PopulateGrid();
		}

		protected void RefreshPriority()
		{
			if (!ValidatePriorities())
			{
				ErrorMessager.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return;
			}
			var flags = State.Flags;
			foreach (GridViewRow row in FlagsGrid.Rows)
			{
				var id = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var flag = flags.FirstOrDefault(c => c.Id == id);
				if (flag == null) continue;
				flag.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
			}
			State.Flags = Sort(flags, DifferenceStep);
			PopulateGrid();
		}

		protected void PopulateData()
		{
			var service = IoC.Resolve<IFlagSecureService>();
			var flags = service.GetAll();
			State = new FlagsOrderState(Sort(flags, DifferenceStep));
			PopulateGrid();
			AllSelectedDiv.Visible = SavePanel.Visible = FlagsGrid.Rows.Count > 0;
		}

		protected void PopulateGrid()
		{
			FlagsGrid.DataSource = State.Flags;
			FlagsGrid.DataBind();
		}

		protected bool ValidatePriorities()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}

		protected static Collection<IFlag> Sort(IEnumerable<IFlag> list, int step)
		{
			var flags = new List<IFlag>(list);
			flags.Sort(delegate(IFlag element1, IFlag element2)
			{
				if (element1.Ordinal < element2.Ordinal)
					return -1;
				if (element1.Ordinal > element2.Ordinal)
					return 1;
				return 0;
			});
			for (int i = 0; i < list.Count(); i++)
			{
				flags[i].Ordinal = (i + 1) * step;
			}
			return new Collection<IFlag>(flags);
		}

		protected void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected void DeleteFlag(int flagId)
		{
			IoC.Resolve<IFlagSecureService>().Delete(SignInHelper.SignedInSystemUser, flagId);
		}

		protected virtual void DeleteFlags(IEnumerable<int> flagIds)
		{
			IoC.Resolve<IFlagSecureService>().Delete(SignInHelper.SignedInSystemUser, flagIds);
		}
	}

	[Serializable]
	public sealed class FlagsOrderState
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IFlag> Flags { get; set; }

		public FlagsOrderState(Collection<IFlag> flags)
		{
			Flags = flags;
		}
	}
}