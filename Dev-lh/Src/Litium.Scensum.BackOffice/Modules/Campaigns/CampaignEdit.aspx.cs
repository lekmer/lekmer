﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Validator;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Campaign;
using Litium.Scensum.BackOffice.UserControls.GridView2;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Resources;
using log4net;
using Image = System.Web.UI.WebControls.Image;
using UrlCleaner = Litium.Lekmer.Common.UrlCleaner;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public partial class CampaignEdit : LekmerStatePageController<CampaignState>, IEditor
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected const string ConditionControlId = "ConditionControl";
		protected const string ActionControlId = "ActionControl";

		protected virtual CampaignsMaster MasterPage
		{
			get { return (CampaignsMaster) Master; }
		}
		protected CampaignType CurrentCampaignType
		{
			get { return MasterPage.CurrentCampaignType; }
		}


		private ICartCampaignSecureService _cartCampaignSecureService;
		public ICartCampaignSecureService CartCampaignSecureService
		{
			get { return _cartCampaignSecureService ?? (_cartCampaignSecureService = IoC.Resolve<ICartCampaignSecureService>()); }
		}

		private IProductCampaignSecureService _productCampaignSecureService;
		public IProductCampaignSecureService ProductCampaignSecureService
		{
			get { return _productCampaignSecureService ?? (_productCampaignSecureService = IoC.Resolve<IProductCampaignSecureService>()); }
		}

		private ICampaignRegistryCampaignSecureService _campaignRegistryCampaignSecureService;
		public ICampaignRegistryCampaignSecureService CampaignRegistryCampaignSecureService
		{
			get { return _campaignRegistryCampaignSecureService ?? (_campaignRegistryCampaignSecureService = IoC.Resolve<ICampaignRegistryCampaignSecureService>()); }
		}

		private ICampaignLandingPageSecureService _campaignLandingPageSecureService;
		public ICampaignLandingPageSecureService CampaignLandingPageSecureService
		{
			get { return _campaignLandingPageSecureService ?? (_campaignLandingPageSecureService = IoC.Resolve<ICampaignLandingPageSecureService>()); }
		}

		private IMediaItemSecureService _mediaItemSecureService;
		public IMediaItemSecureService MediaItemSecureService
		{
			get { return _mediaItemSecureService ?? (_mediaItemSecureService = IoC.Resolve<IMediaItemSecureService>()); }
		}

		private ITagSecureService _tagSecureService;
		public ITagSecureService TagSecureService
		{
			get { return _tagSecureService ?? (_tagSecureService = IoC.Resolve<ITagSecureService>()); }
		}

		private ILekmerProductSecureService _productSecureService;
		public ILekmerProductSecureService ProductSecureService
		{
			get { return _productSecureService ?? (_productSecureService = (ILekmerProductSecureService) IoC.Resolve<IProductSecureService>()); }
		}

		private IBlockSecureService _blockSecureService;
		public IBlockSecureService BlockSecureService
		{
			get { return _blockSecureService ?? (_blockSecureService = IoC.Resolve<IBlockSecureService>()); }
		}

		private IBlockProductFilterSecureService _blockProductFilterSecureService;
		public IBlockProductFilterSecureService BlockProductFilterSecureService
		{
			get { return _blockProductFilterSecureService ?? (_blockProductFilterSecureService = IoC.Resolve<IBlockProductFilterSecureService>()); }
		}

		private IAliasSharedService _aliasSharedService;
		public IAliasSharedService AliasSharedService
		{
			get { return _aliasSharedService ?? (_aliasSharedService = IoC.Resolve<IAliasSharedService>()); }
		}


		private Collection<IChannel> _channels;
		public Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = IoC.Resolve<IChannelSecureService>().GetAll()); }
		}

		private Collection<ICampaignRegistry> _campaignRegistries;
		public Collection<ICampaignRegistry> CampaignRegistries
		{
			get { return _campaignRegistries ?? (_campaignRegistries = IoC.Resolve<ICampaignRegistrySecureService>().GetAll()); }
		}

		private ICollection<IMediaFormat> _mediaFormats;
		protected ICollection<IMediaFormat> MediaFormats
		{
			get { return _mediaFormats ?? (_mediaFormats = IoC.Resolve<IMediaFormatSecureService>().GetAll()); }
		}

		private ICollection<IConditionType> _conditionTypes;
		protected ICollection<IConditionType> ConditionTypes
		{
			get { return _conditionTypes ?? (_conditionTypes = IoC.Resolve<IConditionTypeSecureService>().GetAll()); }
		}

		private ICollection<IActionType> _actionTypes;
		protected ICollection<IActionType> ActionTypes
		{
			get
			{
				if (_actionTypes != null)
				{
					return _actionTypes;
				}
				
				if (IsCartCampaign)
				{
					_actionTypes =  ConvertActionTypes(IoC.Resolve<ICartActionTypeSecureService>().GetAll());
				}
				else if (IsProductCampaign)
				{
					_actionTypes = ConvertActionTypes(IoC.Resolve<IProductActionTypeSecureService>().GetAll());
				}
				else
				{
					throw new InvalidOperationException("Unknown campaign.");
				}

				return _actionTypes;
			}
		}


		private bool? _isCartCampaign;
		protected bool IsCartCampaign
		{
			get
			{
				if (!_isCartCampaign.HasValue)
				{
					_isCartCampaign = CurrentCampaignType.Equals(CampaignType.CartCampaign);
				}
				return _isCartCampaign.Value;
			}
		}

		private bool? _isProductCampaign;
		protected bool IsProductCampaign
		{
			get
			{
				if (!_isProductCampaign.HasValue)
				{
					_isProductCampaign = CurrentCampaignType.Equals(CampaignType.ProductCampaign);
				}
				return _isProductCampaign.Value;
			}
		}

		protected string ConditionsHelpTexts
		{
			get
			{
				var stringBuilder = new StringBuilder();
				foreach (var condition in ConditionTypes)
				{
					IAlias alias = AliasSharedService.GetByCommonName(ChannelHelper.CurrentChannel, "Backoffice.ToolTip." + condition.CommonName);
					if (alias != null)
					{
						stringBuilder.AppendLine("conditionsHelpText[\"" + condition.Id + "\"] = '" + alias.Value + "';");
					}
				}
				return stringBuilder.ToString();
			}
		}

		protected string ActionsHelpTexts
		{
			get
			{
				var stringBuilder = new StringBuilder();
				foreach (var action in ActionTypes)
				{
					IAlias alias = AliasSharedService.GetByCommonName(ChannelHelper.CurrentChannel, "Backoffice.ToolTip." + action.CommonName);
					if (alias != null)
					{
						stringBuilder.AppendLine("actionsHelpText[\"" + action.Id + "\"] = '" + alias.Value + "';");
					}
				}
				return stringBuilder.ToString();
			}
		}

		#region State
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ICondition> GetConditionsFromState()
		{
			if (IsCartCampaign)
			{
				return ((ICartCampaign)State.Campaign).Conditions;
			}
			if (IsProductCampaign)
			{
				return ((IProductCampaign)State.Campaign).Conditions;
			}
			throw new InvalidOperationException("Unknown campaign.");
		}
		protected virtual void PersistInState(ICondition condition)
		{
			if (condition == null) throw new ArgumentNullException("condition");

			var conditions = GetConditionsFromState();
			var conditionFromState = conditions.FirstOrDefault(item => item.Guid.Equals(condition.Guid));
			if (conditionFromState == null)
			{
				conditions.Add(condition);
			}
			else
			{
				conditions[conditions.IndexOf(conditionFromState)] = condition;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IAction> GetActionsFromState()
		{
			if (IsCartCampaign)
			{
				return ConvertActions(((ICartCampaign)State.Campaign).Actions);
			}
			if (IsProductCampaign)
			{
				return ConvertActions(((IProductCampaign)State.Campaign).Actions);
			}
			throw new InvalidOperationException("Unknown campaign.");
		}
		protected virtual void PersistInState(ICartAction action)
		{
			if (action == null) throw new ArgumentNullException("action");

			var campaign = (ICartCampaign)State.Campaign;
			var actionFromState = campaign.Actions.FirstOrDefault(item => item.Guid.Equals(action.Guid));
			if (actionFromState == null)
			{
				campaign.Actions.Add(action);
			}
			else
			{
				campaign.Actions[campaign.Actions.IndexOf(actionFromState)] = action;
			}
		}
		protected virtual void PersistInState(IProductAction action)
		{
			if (action == null) throw new ArgumentNullException("action");

			var campaign = (IProductCampaign)State.Campaign;
			var actionFromState = campaign.Actions.FirstOrDefault(item => item.Guid.Equals(action.Guid));
			if (actionFromState == null)
			{
				campaign.Actions.Add(action);
			}
			else
			{
				campaign.Actions[campaign.Actions.IndexOf(actionFromState)] = action;
			}
		}
		#endregion

		#region Convert
		protected virtual Collection<IAction> ConvertActions<T>(Collection<T> actions) where T : IAction
		{
			var list = new Collection<IAction>();
			foreach (var action in actions)
			{
				list.Add(action);
			}
			return list;
		}
		protected virtual Collection<IActionType> ConvertActionTypes<T>(Collection<T> actionTypes) where T : IActionType
		{
			var list = new Collection<IActionType>();
			foreach (var action in actionTypes)
			{
				list.Add(action);
			}
			return list;
		}
		#endregion


		protected override void OnInit(EventArgs e)
		{
			CampaignDescriptionTranslator.TriggerImageButton = CampaignDescriptionTranslateButton;

			if (IsPostBack)
			{
				EnsureConditionControl();
				EnsureActionControl();
			}

			base.OnInit(e);
		}
		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			DeleteCampaignButton.Click += OnCampaignDelete;

			OpenConditionPopupButton.Click += OnOpenEmptyConditionPopup;
			ConditionListControl.ListItemEdit += OnOpenConditionPopup;
			ConditionListControl.ListItemRemove += OnRemoveCondition;
			ConditionPopupOkButton.Click += OnSaveCondition;

			OpenActionPopupButton.Click += OnOpenEmptyActionPopup;
			ActionListControl.ListItemEdit += OnOpenActionPopup;
			ActionListControl.ListItemRemove += OnRemoveAction;
			ActionListControl.ListItemOrdinalChange += OnActionOrdinalChange;
			ActionPopupOkButton.Click += OnSaveAction;
			ActionPopupUploadExcelButton.Click += ImportPrices;
			
			ImageMediaItemsGrid.RowCommand += OnImageMediaItemsRowCommand;
			ImageMediaItemsGrid.PageIndexChanging += OnImageMediaItemsPageIndexChanging;
			ImageMediaItemsGrid.RowDataBound += OnImageMediaItemsRowDataBound;
			ImageMediaItemSelectControl.Selected += ImageMediaItemSelected;
			ImageMediaItemSelectControl.FileUploadError += ImageMediaItemUploadError;

			IconMediaItemsGrid.RowCommand += OnIconMediaItemsRowCommand;
			IconMediaItemsGrid.PageIndexChanging += OnIconMediaItemsPageIndexChanging;
			IconMediaItemsGrid.RowDataBound += OnIconMediaItemsRowDataBound;
			IconMediaItemSelectControl.Selected += IconMediaItemSelected;
			IconMediaItemSelectControl.FileUploadError += IconMediaItemUploadError;
		}
		protected override void PopulateForm()
		{
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
			{
				CampaignValidationMessage.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
			}

			int? campaignId = Request.QueryString.GetInt32OrNull("CampaignId");
			var campaign = GetCampaign(campaignId);
			var landingPage = campaignId.HasValue ? CampaignLandingPageSecureService.GetByCampaignIdSecure(campaignId.Value) : null;

			RenderConditionTypes();
			RenderActionTypes();
			LoadForm(campaign, landingPage);
			PopulateFlag(campaign);
			PopulateCampaignLevel(campaign);
			PopulatePriceType(campaign);
			PopulateTags(campaign);
			PopulateMultipleRegistries(campaignId);
			PopulateAutoCampaign((ILekmerCampaign) campaign, landingPage);

			DeleteCampaignButton.Visible = campaign != null;

			if (campaign != null && campaign.FolderId > 0)
			{
				MasterPage.SelectedFolderId = campaign.FolderId;
				MasterPage.PopulateTree(campaign.FolderId);
			}
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			RegisterCollapsiblePanelScripts();
			MasterPage.SetupTabAndPanel();
		}


		public void OnSave(object sender, EventArgs e)
		{
			if (State == null) throw new InvalidOperationException("No campaign was persisted in State.");

			ICampaign campaignGeneralInfo;
			if (!CampaignGeneralControl.TryGet(out campaignGeneralInfo))
			{
				CampaignValidationMessage.Add(CampaignGeneralControl.ValidationMessage, InfoType.Warning);
				return;
			}

			var selectedRegistries = GetRegistries();
			if (selectedRegistries.Count == 0)
			{
				CampaignValidationMessage.Add(Resources.LekmerMessage.Campaigns_NoneChannelSelected, InfoType.Warning);
				return;
			}

			if (CheckboxUseCampaignPage.Checked)
			{
				var isImageRegistriesCorrect = ValidateMediaItemRegistries(ImageMediaItemsGrid, "ImageChannels", "hfImgChannelId", "cbImgChannel", selectedRegistries);
				if (!isImageRegistriesCorrect) return;

				var isIconRegistriesCorrect = ValidateMediaItemRegistries(IconMediaItemsGrid, "IconChannels", "hfIconChannelId", "cbIconChannel", selectedRegistries);
				if (!isIconRegistriesCorrect) return;
			}

			bool isTagDeleted = false;

			State.Campaign.Title = campaignGeneralInfo.Title;
			State.Campaign.FolderId = campaignGeneralInfo.FolderId;
			State.Campaign.CampaignStatus.Id = campaignGeneralInfo.CampaignStatus.Id;
			State.Campaign.StartDate = campaignGeneralInfo.StartDate;
			State.Campaign.EndDate = campaignGeneralInfo.EndDate;
			State.Campaign.Exclusive = campaignGeneralInfo.Exclusive;
			var lekmerCampaign = (ILekmerCampaign) State.Campaign;
			lekmerCampaign.LevelId = GetSelectedLevelId();
			lekmerCampaign.UseLandingPage = CheckboxUseCampaignPage.Checked;
			lekmerCampaign.PriceTypeId = GetSelectedPriceTypeId();
			lekmerCampaign.TagId = GetSelectedTagId();

			Collection<ITranslationGeneric> webTitleTranslations;
			Collection<ITranslationGeneric> descriptionTranslations;
			GetWebTitleAndDescription(out webTitleTranslations, out descriptionTranslations);

			if (State.Campaign.Id == 0) //Create new campaign mode
			{
				SetPreferredChannelsFoldersToCookies(selectedRegistries);
			}

			if (CheckboxUseCampaignPage.Checked) // If we want to use campaign page.
			{
				bool isLandingPageRegestriesCorrect = FillStateLandingPageRegestries(selectedRegistries, webTitleTranslations);
				if (!isLandingPageRegestriesCorrect) return;

				foreach (var registryData in State.LandingPage.RegistryDataList)
				{
					var removedRegistry = selectedRegistries.Contains(registryData.CampaignRegistryId);
					if (!removedRegistry)
					{
						SetCampaignPageOffline(registryData.ContentNodeId);
					}
					else
					{
						foreach (GridViewRow row in ImageMediaItemsGrid.Rows)
						{
							if (row.RowType != DataControlRowType.DataRow) continue;
							UpdateStateMediaItemRegistries(row, State.ImageMediaItems, "imgIdHiddenField", "ImageChannels", "hfImgChannelId", "cbImgChannel");
						}

						foreach (GridViewRow row in IconMediaItemsGrid.Rows)
						{
							if (row.RowType != DataControlRowType.DataRow) continue;
							UpdateStateMediaItemRegistries(row, State.IconMediaItems, "iconIdHiddenField", "IconChannels", "hfIconChannelId", "cbIconChannel");
						}

						registryData.ImageMediaId = null;
						registryData.IconMediaId = null;
						foreach (var img in State.ImageMediaItems)
						{
							if (img.CustomCampaignRegistries.FirstOrDefault(r => r.Id == registryData.CampaignRegistryId && r.IsSelected) != null)
							{
								registryData.ImageMediaId = img.MediaItem.Id;
								break;
							}
						}
						foreach (var icon in State.IconMediaItems)
						{
							if (icon.CustomCampaignRegistries.FirstOrDefault(r => r.Id == registryData.CampaignRegistryId && r.IsSelected) != null)
							{
								registryData.IconMediaId = icon.MediaItem.Id;
								break;
							}
						}
					}
				}
			}
			else // If we had a campaign page and no longer want to use it.
			{
				if (State.LandingPage != null && State.LandingPage.RegistryDataList != null)
				{
					foreach (var page in State.LandingPage.RegistryDataList)
					{
						SetCampaignPageOffline(page.ContentNodeId);
					}
				}

				DeleteTags(State.Campaign.Id);
				isTagDeleted = true;
			}

			ILekmerCampaignSecureService service = State.Campaign is ICartCampaign
				? (ILekmerCampaignSecureService)CartCampaignSecureService
				: (ILekmerCampaignSecureService)ProductCampaignSecureService;

			ICampaign campaign = service.Save(
				SignInHelper.SignedInSystemUser,
				State.Campaign,
				GetSelectedFlagId(),
				selectedRegistries,
				State.LandingPage,
				webTitleTranslations,
				descriptionTranslations);

			if (campaign == null) return;

			var landingPage = CampaignLandingPageSecureService.GetByCampaignIdSecure(campaign.Id);
			if (CheckboxUseCampaignPage.Checked && ((ILekmerCampaign) campaign).UseLandingPage && landingPage != null)
			{
				if (!isTagDeleted)
				{
					DeleteTags(campaign.Id);
				}
				AddTag(campaign.Id, landingPage, webTitleTranslations);
			}
			UpdateMaster(campaign.FolderId);
			var editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", MasterPage.GetUrlWithType(PathHelper.Campaign.GetEditUrl(campaign.Id)));
			Response.Redirect(editUrl);
		}
		protected virtual void OnCampaignDelete(object sender, EventArgs e)
		{
			if (State.Campaign is ICartCampaign)
			{
				CartCampaignSecureService.Delete(SignInHelper.SignedInSystemUser, State.Campaign.Id);
			}
			else if (State.Campaign is IProductCampaign)
			{
				ProductCampaignSecureService.Delete(SignInHelper.SignedInSystemUser, State.Campaign.Id);
			}
			else
			{
				throw new InvalidOperationException("Unknown campaign.");
			}

			string redirectUrl = string.Empty;
			if (IsCartCampaign)
			{
				redirectUrl = PathHelper.Campaign.GetDefaultUrlForCartCampaigns();
			}
			if (IsProductCampaign)
			{
				redirectUrl = PathHelper.Campaign.GetDefaultUrlForProductCampaigns();
			}

			Response.Redirect(redirectUrl);
		}
		public void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}
		public void ToggleAutoCampaignContent(object sender, EventArgs e)
		{
			AutoCampaignContent.Visible = CheckboxUseCampaignPage.Checked;
		}

		// Condition event.
		public void OnOpenEmptyConditionPopup(object sender, EventArgs e)
		{
			if (ConditionTypeList.Items.Count == 0)
			{
				throw new InvalidOperationException("The list of available condition types is empty.");
			}

			if (ConditionTypeList.SelectedValue == null)
			{
				throw new InvalidOperationException("Condition type has not been selected.");
			}

			int conditionTypeId = int.Parse(ConditionTypeList.SelectedValue, CultureInfo.CurrentCulture);
			var conditionType = IoC.Resolve<IConditionTypeSecureService>().GetById(conditionTypeId);
			var condition = IoC.Resolve<IConditionSecureService>().Create(conditionType.CommonName);
			OpenConditionPopup(condition);
		}
		public void OnOpenConditionPopup(object sender, CommandEventArgs e)
		{
			if (e.CommandArgument == null)
			{
				throw new ArgumentException("CommandArgument does not contain condition identifier.");
			}

			var condition = GetConditionsFromState().FirstOrDefault(item => item.Guid.Equals(new Guid(e.CommandArgument.ToString())));
			OpenConditionPopup(condition);
		}
		public void OnRemoveCondition(object sender, CommandEventArgs e)
		{
			if (e.CommandArgument == null)
			{
				throw new ArgumentException("CommandArgument does not contain condition identifier.");
			}

			MarkConditionAsDeleted(new Guid(e.CommandArgument.ToString()));
			RenderConditionList(GetConditionsFromState());
		}
		public void OnSaveCondition(object sender, EventArgs e)
		{
			var conditionControl = ConditionControlPlaceHolder.FindControl(ConditionControlId) as ICampaignPluginControl<ICondition>;
			if (conditionControl == null)
			{
				throw new InvalidOperationException(
					"The operation requires a condition plugin control, " +
					"but no such control was added to the ConditionControlPlaceHolder control collection.");
			}

			ICondition condition;
			if (conditionControl.TryGet(out condition))
			{
				PersistInState(condition);
				RenderConditionList(GetConditionsFromState());
			}
			else
			{
				ConditionPopupValidationMessage.AddRange(conditionControl.ValidationMessages);
				ConditionPopup.Show();
			}
		}
		protected virtual void OpenConditionPopup(ICondition condition)
		{
			if (condition == null) throw new ArgumentNullException("condition");

			var setting = new CampaignControlSetting(condition.ConditionType.CommonName);
			var virtualPath = setting.ControlVirtualPath;
			var control = CreateControl(ConditionControlId, virtualPath);
			LoadControl(ConditionControlPlaceHolder, control);
			RegisterConditionControl(virtualPath);
			((ICampaignPluginControl<ICondition>) control).DataBind(condition);
			ConditionPopup.Show();
		}
		protected virtual void MarkConditionAsDeleted(Guid id)
		{
			foreach (var condition in GetConditionsFromState())
			{
				if (!condition.Guid.Equals(id)) continue;

				condition.Status = BusinessObjectStatus.Deleted;
				return;
			}
			throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Condition with Guid {0} does not exist in State.", id));
		}
		protected virtual void RegisterConditionControl(string virtualPath)
		{
			if (virtualPath == null) throw new ArgumentNullException("virtualPath");

			ConditionControlToInitializeHiddenField.Value = virtualPath;
		}

		// Action event.
		public void OnActionOrdinalChange(object sender, ActionOrdinalEventArgs e)
		{
			foreach (var action in GetActionsFromState())
			{
				int ordinal;
				if (e.ActionOrdinals.TryGetValue(action.Guid, out ordinal))
				{
					action.Ordinal = ordinal;
				}
			}
			RenderActionList(GetActionsFromState());
		}
		public void OnOpenEmptyActionPopup(object sender, EventArgs e)
		{
			if (ActionTypeList.Items.Count == 0)
			{
				throw new InvalidOperationException("The list of available action types is empty.");
			}

			if (ActionTypeList.SelectedValue == null)
			{
				throw new InvalidOperationException("Action type has not been selected.");
			}

			OpenActionPopup(CreateAction(), true);
		}
		public void OnOpenActionPopup(object sender, CommandEventArgs e)
		{
			if (e.CommandArgument == null)
			{
				throw new ArgumentException("CommandArgument does not contain action identifier.");
			}

			var action = GetActionsFromState().FirstOrDefault(item => item.Guid.Equals(new Guid(e.CommandArgument.ToString())));
			OpenActionPopup(action, false);
		}
		public void OnRemoveAction(object sender, CommandEventArgs e)
		{
			if (e.CommandArgument == null)
			{
				throw new ArgumentException("CommandArgument does not contain action identifier.");
			}

			MarkActionAsDeleted(new Guid(e.CommandArgument.ToString()));
			RenderActionList(GetActionsFromState());
		}
		public void OnSaveAction(object sender, EventArgs e)
		{
			if (IsCartCampaign)
			{
				OnSaveCartAction();
			}
			else if (IsProductCampaign)
			{
				OnSaveProductAction();
			}
			else
			{
				throw new InvalidOperationException("Unknown campaign.");
			}
		}
		protected virtual void OnSaveCartAction()
		{
			var actionControl = ActionControlPlaceHolder.FindControl(ActionControlId) as ICampaignPluginControl<ICartAction>;
			if (actionControl == null)
			{
				throw new InvalidOperationException(
					"The operation requires an action plugin control, " +
					"but no such control was registered to the ActionControlPlaceHolder control collection.");
			}

			ICartAction action;
			if (actionControl.TryGet(out action))
			{
				PersistInState(action);
				RenderActionList(GetActionsFromState());
			}
			else
			{
				ActionPopupValidationMessage.AddRange(actionControl.ValidationMessages);
				ActionPopup.Show();
			}
		}
		protected virtual void OnSaveProductAction()
		{
			var actionControl = ActionControlPlaceHolder.FindControl(ActionControlId) as ICampaignPluginControl<IProductAction>;
			if (actionControl == null)
			{
				throw new InvalidOperationException(
					"The operation requires an action plugin control, " +
					"but no such control was registered to the ActionControlPlaceHolder control collection.");
			}

			IProductAction action;
			if (actionControl.TryGet(out action))
			{
				PersistInState(action);
				RenderActionList(GetActionsFromState());
				ActionPopupAdditionalValidationMessageDiv.Visible = false;
			}
			else
			{
				ActionPopupValidationMessage.AddRange(actionControl.ValidationMessages);
				if (actionControl.AdditionalValidationMessage.Length > 0)
				{
					ActionPopupAdditionalValidationMessageDiv.Visible = true;
					ActionPopupAdditionalValidationMessage.Text = actionControl.AdditionalValidationMessage;
				}
				ActionPopup.Show();
			}
		}
		protected virtual IAction CreateAction()
		{
			int actionTypeId = int.Parse(ActionTypeList.SelectedValue, CultureInfo.CurrentCulture);
			if (IsCartCampaign)
			{
				var actionType = IoC.Resolve<ICartActionTypeSecureService>().GetById(actionTypeId);
				return IoC.Resolve<ICartActionSecureService>().Create(actionType.CommonName);
			}
			if (IsProductCampaign)
			{
				var actionType = IoC.Resolve<IProductActionTypeSecureService>().GetById(actionTypeId);
				return IoC.Resolve<IProductActionSecureService>().Create(actionType.CommonName);
			}
			throw new InvalidOperationException("Unknown campaign.");
		}
		protected virtual void OpenActionPopup(IAction action, bool isNew)
		{
			if (action == null) throw new ArgumentNullException("action");

			bool showExcelFileUpload = false;

			if (action is ICartAction)
			{
				var typedAction = (ICartAction) action;
				var setting = new CampaignControlSetting(typedAction.ActionType.CommonName);
				var virtualPath = setting.ControlVirtualPath;
				var control = CreateControl(ActionControlId, virtualPath);
				LoadControl(ActionControlPlaceHolder, control);
				RegisterActionControl(virtualPath);
				((ICampaignPluginControl<ICartAction>)control).DataBind(typedAction);
			}
			else if (State.Campaign is IProductCampaign)
			{
				var typedAction = (IProductAction)action;
				showExcelFileUpload = typedAction.ActionType.CommonName == "ProductDiscount";
				if (isNew && typedAction.ActionType.CommonName == "GiftCardViaMailProduct")
				{
					var actions = ((IProductCampaign) State.Campaign).Actions;
					if (actions.FirstOrDefault(a => a.ActionType.CommonName == "GiftCardViaMailProduct") != null)
					{
						errorDiv.Visible = true;
						return;
					}
				}

				var setting = new CampaignControlSetting(typedAction.ActionType.CommonName);
				var virtualPath = setting.ControlVirtualPath;
				var control = CreateControl(ActionControlId, virtualPath);
				LoadControl(ActionControlPlaceHolder, control);
				RegisterActionControl(virtualPath);
				((ICampaignPluginControl<IProductAction>)control).DataBind(typedAction);
			}
			else
			{
				throw new InvalidOperationException("Unknown campaign.");
			}

			UploadExcelDiv.Visible = showExcelFileUpload;
			errorDiv.Visible = false;
			ActionPopup.Show();
		}
		protected virtual void MarkActionAsDeleted(Guid id)
		{
			foreach (var action in GetActionsFromState())
			{
				if (action.Guid.Equals(id))
				{
					action.Status = BusinessObjectStatus.Deleted;
					return;
				}
			}
			throw new ArgumentException(string.Format(CultureInfo.CurrentCulture, "Action with Guid {0} does not exist in State.", id));
		}
		protected virtual void RegisterActionControl(string virtualPath)
		{
			if (virtualPath == null) throw new ArgumentNullException("virtualPath");

			ActionControlToInitializeHiddenField.Value = virtualPath;
		}


		protected virtual ICampaign GetCampaign(int? campaignId)
		{
			if (IsCartCampaign)
			{
				return GetCartCampaign(campaignId);
			}
			if (IsProductCampaign)
			{
				return GetProductCampaign(campaignId);
			}
			throw new InvalidOperationException();
		}
		protected virtual ICartCampaign GetCartCampaign(int? campaignId)
		{
			if (!campaignId.HasValue)
			{
				var newCampaign = CartCampaignSecureService.Create(SignInHelper.SignedInSystemUser);
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					newCampaign.FolderId = MasterPage.SelectedFolderId.Value;
				}
				return newCampaign;
			}

			var campaign = CartCampaignSecureService.GetById(campaignId.Value);
			if (campaign == null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Cart campaign with id {0} does not exist.", campaignId));
			}
			return campaign;
		}
		protected virtual IProductCampaign GetProductCampaign(int? campaignId)
		{
			if (!campaignId.HasValue)
			{
				var newCampaign = ProductCampaignSecureService.Create(SignInHelper.SignedInSystemUser);
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					newCampaign.FolderId = MasterPage.SelectedFolderId.Value;
				}
				return newCampaign;
			}

			var campaign = ProductCampaignSecureService.GetById(campaignId.Value);
			if (campaign == null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Product campaign with id {0} does not exist.", campaignId));
			}
			return campaign;
		}

		protected virtual void RenderConditionTypes()
		{
			ConditionTypeList.DataSource = ConditionTypes;
			ConditionTypeList.DataValueField = "Id";
			ConditionTypeList.DataTextField = "Title";
			ConditionTypeList.DataBind();
		}
		protected virtual void RenderActionTypes()
		{
			ActionTypeList.DataSource = ActionTypes;
			ActionTypeList.DataValueField = "Id";
			ActionTypeList.DataTextField = "Title";
			ActionTypeList.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
		protected virtual void LoadForm(ICampaign campaign, ICampaignLandingPage landingPage)
		{
			if (campaign == null) throw new ArgumentNullException("campaign");

			RenderGeneralInfo(campaign);

			if (campaign is ICartCampaign)
			{
				var typedCampaign = (ICartCampaign) campaign;
				RenderConditionList(typedCampaign.Conditions);
				RenderActionList(ConvertActions(typedCampaign.Actions));
			}
			else if (campaign is IProductCampaign)
			{
				var typedCampaign = (IProductCampaign) campaign;
				RenderConditionList(typedCampaign.Conditions);
				RenderActionList(ConvertActions(typedCampaign.Actions));
			}
			else
			{
				throw new InvalidOperationException("Unknown campaign.");
			}

			State = new CampaignState(campaign, landingPage);
		}
		protected virtual void RenderGeneralInfo(ICampaign campaign)
		{
			if (campaign == null) throw new ArgumentNullException("campaign");

			CampaignGeneralControl.DataBind(campaign, CurrentCampaignType);
		}
		protected virtual void RenderConditionList(Collection<ICondition> conditions)
		{
			if (conditions == null) throw new ArgumentNullException("conditions");

			ConditionListControl.DataBind(conditions);
		}
		protected virtual void RenderActionList(Collection<IAction> actions)
		{
			if (actions == null) throw new ArgumentNullException("actions");

			ActionListControl.DataBind(actions);
		}

		#region Campaign Flags CR
		protected virtual void PopulateFlag(ICampaign campaign)
		{
			// Currently flags work only for product campaigns
			if (IsProductCampaign)
			{
				var service = IoC.Resolve<IFlagSecureService>();
				var selectedFlag = campaign != null ? service.GetByCampaign(campaign.Id) : null;

				FlagDropDownList.DataTextField = "Title";
				FlagDropDownList.DataValueField = "Id";
				FlagDropDownList.Items.Clear();
				FlagDropDownList.DataSource = service.GetAll();
				FlagDropDownList.DataBind();
				FlagDropDownList.Items.Insert(0, new ListItem(Resources.Campaign.Literal_InheritFromFolder, string.Empty));
				FlagDropDownList.SelectedValue = selectedFlag != null ? selectedFlag.Id.ToString(CultureInfo.InvariantCulture) : string.Empty;
			}
			else
			{
				FlagsPanel.Visible = false;
			}
		}
		private int? GetSelectedFlagId()
		{
			int id;
			return int.TryParse(FlagDropDownList.SelectedValue, out id) ? (int?) id : null;
		}
		#endregion

		#region Campaign Level CR
		protected virtual void PopulateCampaignLevel(ICampaign campaign)
		{
			var allLevels = IoC.Resolve<ICampaignLevelSecureService>().GetAll();

			ICampaignLevel selectedLevel = null;
			if (campaign != null)
			{
				selectedLevel = allLevels.FirstOrDefault(cl => cl.Id == ((ILekmerCampaign) campaign).LevelId);
			}

			CampaignLevel.DataTextField = "Title";
			CampaignLevel.DataValueField = "Id";
			CampaignLevel.Items.Clear();
			CampaignLevel.DataSource = allLevels;
			CampaignLevel.DataBind();
			CampaignLevel.SelectedValue = selectedLevel != null ? selectedLevel.Id.ToString(CultureInfo.InvariantCulture) : string.Empty;
		}
		private int GetSelectedLevelId()
		{
			int id;
			return int.TryParse(CampaignLevel.SelectedValue, out id) ? id : IoC.Resolve<ICampaignLevelSecureService>().GetAll().First().Id;
		}
		#endregion

		#region Campaign Price Type CR
		protected virtual void PopulatePriceType(ICampaign campaign)
		{
			// Currently price types work only for product campaigns
			if (IsProductCampaign)
			{
				var selectedPriceType = campaign != null ? ((ILekmerCampaign) campaign).PriceTypeId : null;

				PriceType.DataTextField = "Title";
				PriceType.DataValueField = "Id";
				PriceType.Items.Clear();
				PriceType.DataSource = IoC.Resolve<ICampaignPriceTypeSecureService>().GetAll();
				PriceType.DataBind();
				PriceType.Items.Insert(0, new ListItem("- Select Price Type -", string.Empty));
				PriceType.SelectedValue = selectedPriceType != null ? selectedPriceType.ToString() : string.Empty;
			}
			else
			{
				PriceTypePanel.Visible = false;
			}
		}
		private int? GetSelectedPriceTypeId()
		{
			int id;
			return int.TryParse(PriceType.SelectedValue, out id) ? (int?)id : null;
		}
		#endregion

		#region Campaign Tag CR
		protected virtual void PopulateTags(ICampaign campaign)
		{
			// Currently campaign tag works only for product campaigns.
			if (IsProductCampaign)
			{
				var allTags = new Dictionary<int, string>();
				var tagGroups = IoC.Resolve<ITagGroupSecureService>().GetAll();
				foreach (var tagGroup in tagGroups)
				{
					if (tagGroup.CommonName == "auto-kampanj") continue;

					var tags = IoC.Resolve<ITagSecureService>().GetAllByTagGroup(tagGroup.Id);
					foreach (var tag in tags)
					{
						if (!allTags.ContainsKey(tag.Id))
						{
							allTags.Add(tag.Id, tagGroup.Title + " - " + tag.Value);
						}
					}
				}

				var selectedTag = campaign != null ? ((ILekmerCampaign) campaign).TagId : null;

				CampaignTag.DataTextField = "Value";
				CampaignTag.DataValueField = "Key";
				CampaignTag.Items.Clear();
				CampaignTag.DataSource = allTags;
				CampaignTag.DataBind();
				CampaignTag.Items.Insert(0, new ListItem("- Select Tag -", string.Empty));
				CampaignTag.SelectedValue = selectedTag != null ? selectedTag.ToString() : string.Empty;
			}
			else
			{
				TagPanel.Visible = false;
			}
		}
		private int? GetSelectedTagId()
		{
			int id;
			return int.TryParse(CampaignTag.SelectedValue, out id) ? (int?)id : null;
		}
		#endregion

		#region Multiple Channels CR
		protected virtual void PopulateMultipleRegistries(int? campaignId)
		{
			var preferredSelection = new Collection<int>();
			if (campaignId.HasValue)
			{
				var registries = CampaignRegistryCampaignSecureService.GetRegistriesByCampaignId(campaignId.Value);
				foreach (var campaignRegistry in registries)
				{
					preferredSelection.Add(campaignRegistry.CampaignRegistryId);
				}
			}
			else
			{
				preferredSelection = GetPreferredChannelsFoldersFromCookies();
			}

			RegistriesRepeater.DataSource = CampaignRegistries.Select(x => preferredSelection.Contains(x.Id) ? new CampaignChannel(x, true) : new CampaignChannel(x, false));
			RegistriesRepeater.DataBind();
		}
		private void SetPreferredChannelsFoldersToCookies(IEnumerable<int> registries)
		{
			if (!PreferredFoldersCookieSetting.Instance.StoreInCookies) return;

			if (Request.Browser.Cookies)
			{
				string key = string.Format("{0}_{1}", PreferredFoldersCookieSetting.Instance.CookiesKey, CurrentCampaignType);
				string value = string.Join(",", registries.Select(x => string.Format("{0}", x)).ToArray());
				var cookie = new HttpCookie(key) { Value = value };
				if (PreferredFoldersCookieSetting.Instance.CookiesExpiresInMinutes != 0)
				{
					cookie.Expires = DateTime.Now.AddMinutes(PreferredFoldersCookieSetting.Instance.CookiesExpiresInMinutes);
				}
				Response.Cookies.Add(cookie);
			}
		}
		private Collection<int> GetPreferredChannelsFoldersFromCookies()
		{
			var result = new Collection<int>();
			if (!PreferredFoldersCookieSetting.Instance.StoreInCookies) return result;

			if (Request.Browser.Cookies)
			{
				string key = string.Format("{0}_{1}", PreferredFoldersCookieSetting.Instance.CookiesKey, CurrentCampaignType);
				HttpCookie cookies = HttpContext.Current.Request.Cookies[key];
				if (cookies != null)
				{
					var stringValues = cookies.Value.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
					foreach (var stringValue in stringValues)
					{
						result.Add(int.Parse(stringValue));
					}
				}
			}
			return result;
		}
		private Collection<int> GetRegistries()
		{
			var registries = new Collection<int>();
			foreach (RepeaterItem item in RegistriesRepeater.Items)
			{
				var isSelected = ((CheckBox) item.FindControl("ChannelCheckbox")).Checked;
				if (!isSelected) continue;

				var registryId = int.Parse(((HiddenField) item.FindControl("ChannelIdHidden")).Value);
				registries.Add(registryId);
			}
			return registries;
		}
		#endregion

		protected virtual void PopulateAutoCampaign(ILekmerCampaign campaign, ICampaignLandingPage landingPage)
		{
			CheckboxUseCampaignPage.Checked = campaign.UseLandingPage;
			AutoCampaignContent.Visible = campaign.UseLandingPage;

			PopulateTranslations(landingPage);

			if (landingPage == null)
			{
				PopulateImageMediaItemsDefault();
				PopulateIconMediaItemsDefault();
				return;
			}

			CampaignWebTitle.Text = landingPage.WebTitle;
			CampaignDescriptionEditor.SetValue(landingPage.Description);

			foreach (var registryData in landingPage.RegistryDataList)
			{
				UpdateStateMediaItems(registryData.ImageMediaId, State.ImageMediaItems, registryData.CampaignRegistryId);
				UpdateStateMediaItems(registryData.IconMediaId, State.IconMediaItems, registryData.CampaignRegistryId);
			}

			PopulateImageMediaItemsGrid();
			PopulateIconMediaItemsGrid();
		}
		protected virtual void PopulateTranslations(ICampaignLandingPage landingPage)
		{
			CampaignWebTitleTranslator.DefaultValueControlClientId = CampaignWebTitle.ClientID;
			CampaignWebTitleTranslator.BusinessObjectId = landingPage != null ? landingPage.CampaignLandingPageId : 0;
			CampaignWebTitleTranslator.DataSource = landingPage != null
				? CampaignLandingPageSecureService.GetAllWebTitleTranslations(landingPage.CampaignLandingPageId)
				: new Collection<ITranslationGeneric>();
			CampaignWebTitleTranslator.DataBind();

			CampaignDescriptionTranslator.DefaultValueControlClientId = CampaignDescriptionEditor.ClientID;
			CampaignDescriptionTranslator.BusinessObjectId = landingPage != null ? landingPage.CampaignLandingPageId : 0;
			CampaignDescriptionTranslator.DataSource = landingPage != null
				? CampaignLandingPageSecureService.GetAllDescriptionTranslations(landingPage.CampaignLandingPageId)
				: new Collection<ITranslationGeneric>();
			CampaignDescriptionTranslator.DataBind();
		}

		// Landing Page Image media item.
		protected virtual void ImageMediaItemSelected(object sender, MediaItemSelectEventArgs e)
		{
			ImageMediaItemsPopup.Hide();

			var landingPageMediaItem = CreateLandingPageMediaItem(e.MediaItem);
			State.ImageMediaItems.Add(landingPageMediaItem);
			PopulateImageMediaItemsGrid();
		}
		protected virtual void ImageMediaItemUploadError(object sender, EventArgs e)
		{
			ImageMediaItemsPopup.X = 227;
			ImageMediaItemsPopup.Show();
		}
		protected virtual void OnImageMediaItemsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "DeleteMediaItem") return;

			string id = e.CommandArgument.ToString();
			var itemToDelate = State.ImageMediaItems.FirstOrDefault(i => i.Id == id);
			if (itemToDelate == null) return;

			State.ImageMediaItems.Remove(itemToDelate);
			PopulateImageMediaItemsGrid();
		}
		protected virtual void OnImageMediaItemsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ImageMediaItemsGrid.PageIndex = e.NewPageIndex;
			PopulateImageMediaItemsGrid();
		}
		protected virtual void OnImageMediaItemsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			MediaItemsRowDataBound(row, "imgArchiveImage", "ImageChannels");
		}
		protected virtual void PopulateImageMediaItemsGrid()
		{
			foreach (GridViewRow row in ImageMediaItemsGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				UpdateStateMediaItemRegistries(row, State.ImageMediaItems, "imgIdHiddenField", "ImageChannels", "hfImgChannelId", "cbImgChannel");
			}

			ImageMediaItemsGrid.DataSource = State.ImageMediaItems;
			ImageMediaItemsGrid.DataBind();
			ImageMediaUpdatePanel.Update();
		}
		protected virtual void PopulateImageMediaItemsDefault()
		{
			ImageMediaItemsGrid.DataSource = null;
			ImageMediaItemsGrid.DataBind();
		}

		// Landing Page Icon media item.
		protected virtual void IconMediaItemSelected(object sender, MediaItemSelectEventArgs e)
		{
			IconMediaItemsPopup.Hide();

			var landingPageMediaItem = CreateLandingPageMediaItem(e.MediaItem);
			State.IconMediaItems.Add(landingPageMediaItem);
			PopulateIconMediaItemsGrid();
		}
		protected virtual void IconMediaItemUploadError(object sender, EventArgs e)
		{
			IconMediaItemsPopup.X = 227;
			IconMediaItemsPopup.Show();
		}
		protected virtual void OnIconMediaItemsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "DeleteMediaItem") return;

			string id = e.CommandArgument.ToString();
			var itemToDelate = State.IconMediaItems.FirstOrDefault(i => i.Id == id);
			if (itemToDelate == null) return;

			State.IconMediaItems.Remove(itemToDelate);
			PopulateIconMediaItemsGrid();
		}
		protected virtual void OnIconMediaItemsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			IconMediaItemsGrid.PageIndex = e.NewPageIndex;
			PopulateIconMediaItemsGrid();
		}
		protected virtual void OnIconMediaItemsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			MediaItemsRowDataBound(row, "imgArchiveIcon", "IconChannels");
		}
		protected virtual void PopulateIconMediaItemsGrid()
		{
			foreach (GridViewRow row in IconMediaItemsGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				UpdateStateMediaItemRegistries(row, State.IconMediaItems, "iconIdHiddenField", "IconChannels", "hfIconChannelId", "cbIconChannel");
			}

			IconMediaItemsGrid.DataSource = State.IconMediaItems;
			IconMediaItemsGrid.DataBind();
			IconMediaUpdatePanel.Update();
		}
		protected virtual void PopulateIconMediaItemsDefault()
		{
			IconMediaItemsGrid.DataSource = null;
			IconMediaItemsGrid.DataBind();
		}

		private LandingPageMediaItem CreateLandingPageMediaItem(IMediaItem mediaItem)
		{
			var landingPageMediaItem = new LandingPageMediaItem(Guid.NewGuid().ToString(), mediaItem, new Collection<CustomCampaignRegistry>());
			foreach (var campaignRegistry in CampaignRegistries)
			{
				var customCampaignRegistry = CastCampaignRegistry(campaignRegistry, false);
				landingPageMediaItem.CustomCampaignRegistries.Add(customCampaignRegistry);
			}
			return landingPageMediaItem;
		}
		private void MediaItemsRowDataBound(GridViewRow row, string mediaItemImageControlName, string registriesRepeaterControlName)
		{
			var mediaItem = (LandingPageMediaItem) row.DataItem;
			var mediaFormat = MediaFormats.FirstOrDefault(i => i.Id == mediaItem.MediaItem.MediaFormatId);

			var mediaItemImageControl = (Image) row.FindControl(mediaItemImageControlName);
			var mediaItemImage = IoC.Resolve<IImageSecureService>().GetById(mediaItem.MediaItem.Id);
			if (mediaItemImage != null)
			{
				mediaItemImageControl.ImageUrl = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(mediaItemImage.Id, mediaItemImage.FormatExtension));
			}
			else if (mediaFormat != null)
			{
				mediaItemImageControl.ImageUrl = ResolveUrl(PathHelper.Media.GetImageThumbnailLoaderUrl(mediaItem.MediaItem.Id, mediaFormat.Extension));
			}

			var registriesRepeaterControl = (Repeater)row.FindControl(registriesRepeaterControlName);
			if (registriesRepeaterControl != null)
			{
				registriesRepeaterControl.DataSource = mediaItem.CustomCampaignRegistries;
				registriesRepeaterControl.DataBind();
			}
		}
		private void UpdateStateMediaItems(int? mediaItemId, Collection<LandingPageMediaItem> mediaItems, int campaignRegistryId)
		{
			if (!mediaItemId.HasValue || mediaItemId <= 0) return;

			var mediaItem = MediaItemSecureService.GetById(mediaItemId.Value);
			if (mediaItem == null) return;

			var id = mediaItem.Id.ToString(CultureInfo.InvariantCulture);

			var existingMediaItem = mediaItems.FirstOrDefault(mi => mi.Id == id);
			if (existingMediaItem == null)
			{
				var newMediaItem = new LandingPageMediaItem(id, mediaItem, new Collection<CustomCampaignRegistry>());
				foreach (var campaignRegistry in CampaignRegistries)
				{
					var customCampaignRegistry = CastCampaignRegistry(campaignRegistry, campaignRegistry.Id == campaignRegistryId);
					newMediaItem.CustomCampaignRegistries.Add(customCampaignRegistry);
				}
				mediaItems.Add(newMediaItem);
			}
			else
			{
				var customCampaignRegistry = existingMediaItem.CustomCampaignRegistries.FirstOrDefault(r => r.Id == campaignRegistryId);
				if (customCampaignRegistry != null)
				{
					customCampaignRegistry.IsSelected = true;
				}
			}
		}
		private void UpdateStateMediaItemRegistries(
			GridViewRow row,
			IEnumerable<LandingPageMediaItem> mediaItems,
			string mediaItemIdControlName,
			string registriesRepeaterControlName,
			string registryIdControlName,
			string registryCheckBoxControlName)
		{
			var mediaItemIdControl = (HiddenField) row.FindControl(mediaItemIdControlName);
			var mediaItem = mediaItems.FirstOrDefault(imi => imi.Id == mediaItemIdControl.Value);
			var registriesRepeaterControl = (Repeater) row.FindControl(registriesRepeaterControlName);
			if (mediaItem == null || registriesRepeaterControl == null) return;

			if (mediaItem.CustomCampaignRegistries == null)
			{
				mediaItem.CustomCampaignRegistries = new Collection<CustomCampaignRegistry>();
			}
			else
			{
				mediaItem.CustomCampaignRegistries.Clear();
			}

			foreach (RepeaterItem item in registriesRepeaterControl.Items)
			{
				var registryIdControl = (HiddenField)item.FindControl(registryIdControlName);
				var registryCheckBoxControl = (CheckBox)item.FindControl(registryCheckBoxControlName);

				int campaignRegistryId;
				if (registryIdControl == null || registryCheckBoxControl == null || !int.TryParse(registryIdControl.Value, out campaignRegistryId)) continue;

				var campaignRegistry = CampaignRegistries.FirstOrDefault(cr => cr.Id == campaignRegistryId);
				var customCampaignRegistry = CastCampaignRegistry(campaignRegistry, registryCheckBoxControl.Checked);

				mediaItem.CustomCampaignRegistries.Add(customCampaignRegistry);
			}
		}


		protected virtual void EnsureConditionControl()
		{
			var virtualPath = GetFormElementValue(ConditionControlToInitializeHiddenField.UniqueID);
			if (!virtualPath.IsNullOrEmpty())
			{
				var control = CreateControl(ConditionControlId, virtualPath);
				LoadControl(ConditionControlPlaceHolder, control);
			}
		}
		protected virtual void EnsureActionControl()
		{
			var virtualPath = GetFormElementValue(ActionControlToInitializeHiddenField.UniqueID);
			if (!virtualPath.IsNullOrEmpty())
			{
				var control = CreateControl(ActionControlId, virtualPath);
				LoadControl(ActionControlPlaceHolder, control);
			}
		}
		protected virtual string GetFormElementValue(string controlId)
		{
			return Request.Form[controlId];
		}
		protected virtual Control CreateControl(string controlId, string virtualPath)
		{
			if (virtualPath == null) throw new ArgumentNullException("virtualPath");

			var control = LoadControl(virtualPath);
			control.ID = controlId;
			return control;
		}
		protected virtual void LoadControl(PlaceHolder placeHolder, Control control)
		{
			if (control == null) throw new ArgumentNullException("control");

			placeHolder.Controls.Clear();
			placeHolder.Controls.Add(control);
		}


		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string actionCollapsiblescript = "OpenClose('" + ActionPanelDiv.ClientID + "','" + ActionPanelStateHidden.ClientID + "','" + ActionIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			ActionCollapsibleDiv.Attributes.Add("onclick", actionCollapsiblescript);
			ActionCollapsibleCenterDiv.Attributes.Add("onclick", actionCollapsiblescript);

			string conditionCollapsiblescript = "OpenClose('" + ConditionPanelDiv.ClientID + "','" + ConditionPanelStateHidden.ClientID + "','" + ConditionIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			ConditionCollapsibleDiv.Attributes.Add("onclick", conditionCollapsiblescript);
			ConditionCollapsibleCenterDiv.Attributes.Add("onclick", conditionCollapsiblescript);

			string landingPageCollapsibleScript = "OpenClose('" + LandingPageDiv.ClientID + "','" + LandingPagePanelStateHidden.ClientID + "','" + LandingPageIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			LandingPageCollapsibleDiv.Attributes.Add("onclick", landingPageCollapsibleScript);
		}
		protected virtual void UpdateMaster(int folderId)
		{
			MasterPage.SelectedFolderId = folderId;
			MasterPage.PopulateTree(folderId);
			MasterPage.UpdateBreadcrumbs(folderId);
		}
		protected virtual bool ValidateMediaItemRegistries(
			GridViewWithCustomPager gridView,
			string registriesRepeaterControlName,
			string registryIdControlName,
			string registryCheckBoxControlName,
			Collection<int> selectedRegistries)
		{
			foreach (GridViewRow row in gridView.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				var registriesRepeaterControl = (Repeater) row.FindControl(registriesRepeaterControlName);
				if (registriesRepeaterControl == null) continue;

				bool hasCorrectRegistries = false;
				foreach (RepeaterItem item in registriesRepeaterControl.Items)
				{
					var registryIdControl = (HiddenField) item.FindControl(registryIdControlName);
					int regestryId = int.Parse(registryIdControl.Value);

					var registryCheckBoxControl = (CheckBox) item.FindControl(registryCheckBoxControlName);
					if (registryCheckBoxControl != null && registryCheckBoxControl.Checked && selectedRegistries.Contains(regestryId))
					{
						hasCorrectRegistries = true;
						break;
					}
				}

				if (!hasCorrectRegistries)
				{
					CampaignValidationMessage.Add("Please select channels for each image/icon of landing page", InfoType.Warning);
					return false;
				}
			}

			return true;
		}
		protected virtual CustomCampaignRegistry CastCampaignRegistry(ICampaignRegistry registry, bool isSelected)
		{
			var campaignRegistry = new CustomCampaignRegistry
			{
				Id = registry.Id,
				Title = registry.Title,
				CommonName = registry.CommonName,
				IsSelected = isSelected
			};

			switch (registry.CommonName)
			{
				case "Sweden":
					campaignRegistry.ShortName = "SE";
					break;
				case "Norway":
					campaignRegistry.ShortName = "NO";
					break;
				case "Denmark":
					campaignRegistry.ShortName = "DK";
					break;
				case "Finland":
					campaignRegistry.ShortName = "FI";
					break;
				case "Netherlands":
					campaignRegistry.ShortName = "NL";
					break;
				case "Europe":
					campaignRegistry.ShortName = "COM";
					break;
			}

			return campaignRegistry;
		}
		protected virtual void GetWebTitleAndDescription(out Collection<ITranslationGeneric> webTitleTranslations, out Collection<ITranslationGeneric> descriptionTranslations)
		{
			webTitleTranslations = null;
			descriptionTranslations = null;

			if (!CheckboxUseCampaignPage.Checked && State.LandingPage == null) return;

			if (State.LandingPage == null)
			{
				State.LandingPage = IoC.Resolve<ICampaignLandingPage>();
			}
			if (State.LandingPage.RegistryDataList == null)
			{
				State.LandingPage.RegistryDataList = new Collection<ICampaignRegistryCampaignLandingPage>();
			}
			State.LandingPage.WebTitle = CampaignWebTitle.Text;
			State.LandingPage.Description = CampaignDescriptionEditor.GetValue();
			webTitleTranslations = CampaignWebTitleTranslator.GetTranslations();
			descriptionTranslations = CampaignDescriptionTranslator.GetTranslations();
		}
		private void SetCampaignPageOffline(int contentNodeId)
		{
			var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			IContentPage contentPage = contentPageSecureService.GetById(contentNodeId);
			if (contentPage != null)
			{
				contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Offline;
				contentPageSecureService.Save(SignInHelper.SignedInSystemUser, contentPage);
			}
		}
		private void DeleteTags(int campaignId)
		{
			var tagGroupId = AutoCampaignPageSetting.Instance.CampaignTagGroupId;
			var oldTags = TagSecureService.GetAllByTagGroup(tagGroupId);
			foreach (var oldTag in oldTags)
			{
				if (oldTag.CommonName == campaignId.ToString(CultureInfo.InvariantCulture))
				{
					TagSecureService.Delete(SignInHelper.SignedInSystemUser, oldTag.Id);
				}
			}
		}
		private void AddTag(int campaignId, ICampaignLandingPage landingPage, IEnumerable<ITranslationGeneric> webTitleTranslations)
		{
			var tagGroupId = AutoCampaignPageSetting.Instance.CampaignTagGroupId;

			// Create new tag and add to products.
			var tag = TagSecureService.Create(tagGroupId, campaignId.ToString(CultureInfo.InvariantCulture), campaignId.ToString(CultureInfo.InvariantCulture));
			// Create translation for tag.
			var tagId = TagSecureService.Save(SignInHelper.SignedInSystemUser, tag, webTitleTranslations);

			var productsToTag = ProductSecureService.GetIdAllByCampaign(campaignId);
			foreach (var product in productsToTag)
			{
				TagSecureService.AddAutoCampaignTag(SignInHelper.SignedInSystemUser, product, tagId);
			}

			foreach (var landingPageRegistryData in landingPage.RegistryDataList)
			{
				//Set the tag on productfilter
				var blocks = BlockSecureService.GetAllByContentNode(landingPageRegistryData.ContentNodeId);
				foreach (var block in blocks)
				{
					if (block.BlockType.CommonName != "ProductFilter") continue;

					var productFilterBlock = BlockProductFilterSecureService.GetByIdSecure(block.Id);
					productFilterBlock.DefaultTagIds.Clear();
					productFilterBlock.DefaultTagIds.Add(tagId);
					BlockProductFilterSecureService.Save(SignInHelper.SignedInSystemUser, productFilterBlock);
					break;
				}
			}
		}
		private bool FillStateLandingPageRegestries(IEnumerable<int> selectedRegistries, Collection<ITranslationGeneric> webTitleTranslations)
		{
			foreach (var registryId in selectedRegistries)
			{
				IContentPage contentPage = null;

				var registryData = State.LandingPage.RegistryDataList.FirstOrDefault(p => p.CampaignRegistryId == registryId);
				if (registryData != null)
				{
					contentPage = IoC.Resolve<IContentPageSecureService>().GetById(registryData.ContentNodeId);
				}

				var channel = Channels.FirstOrDefault(c => c.Id == registryId);
				var webTitleTranslation = webTitleTranslations.FirstOrDefault(t => t.LanguageId == channel.Language.Id);
				var webTitle = webTitleTranslation == null || string.IsNullOrEmpty(webTitleTranslation.Value) ? CampaignWebTitle.Text : webTitleTranslation.Value;
				if (contentPage != null)
				{
					var isUpdated = UpdateAutoCampaignPage(State.Campaign, contentPage, webTitle);
					if (!isUpdated) return false;
				}
				else
				{
					var autoCampaignParentPage = AutoCampaignPageSetting.Instance.AutoCampaignParentPage(registryId);
					var landingPageId = CreateAutoCampaignPage(autoCampaignParentPage, (ILekmerCampaign)State.Campaign, webTitle);
					if (landingPageId < 0) return false;

					if (registryData == null)
					{
						registryData = IoC.Resolve<ICampaignRegistryCampaignLandingPage>();
						registryData.CampaignRegistryId = registryId;
						State.LandingPage.RegistryDataList.Add(registryData);
					}
					registryData.ContentNodeId = landingPageId;
				}
			}

			return true;
		}
		private bool UpdateAutoCampaignPage(ICampaign campaign, IContentPage contentPage, string webTitle)
		{
			if (webTitle.Length == 0)
			{
				CampaignValidationMessage.Add("Must atleast have title to be able to save landing page", InfoType.Failure);
				return false;
			}

			if (contentPage == null)
			{
				CampaignValidationMessage.Add("Unable to complete the update of auto generated campaign landing page. Reason: Landing page has been removed by user.", InfoType.Warning);
				return false;
			}

			contentPage.Title = webTitle;
			contentPage.PageTitle = webTitle;
			contentPage.CommonName = UrlCleaner.CleanUp(webTitle);

			int attempts = 1;
			while (!ContentNodeSaveValidator.IsCommonNameUnique(contentPage))
			{
				contentPage.CommonName = contentPage.CommonName + attempts;
				attempts++;
				if (attempts > AutoCampaignPageSetting.Instance.MaximumAttemptsAtCommonName)
				{
					throw new Exception("Could not create unique commonName for auto-generated campaign landing page");
				}
			}

			contentPage.UrlTitle = contentPage.CommonName;
			contentPage.Ordinal = 0;
			contentPage.ContentNodeStatusId = campaign.CampaignStatus.Id == 0 ? (int) ContentNodeStatusInfo.Online : (int) ContentNodeStatusInfo.Offline;

			var contentPageId = IoC.Resolve<IContentPageSecureService>().Save(SignInHelper.SignedInSystemUser, contentPage);

			IBlockProductFilter productFilterBlock = null;
			var blocks = BlockSecureService.GetAllByContentNode(contentPageId);
			foreach (var block in blocks)
			{
				if (block.BlockType.CommonName == "ProductFilter")
				{
					productFilterBlock = BlockProductFilterSecureService.GetByIdSecure(block.Id);
					break;
				}
			}

			if (productFilterBlock != null)
			{
				productFilterBlock.StartDate = campaign.StartDate;
				productFilterBlock.EndDate = campaign.EndDate;
				BlockProductFilterSecureService.Save(SignInHelper.SignedInSystemUser, productFilterBlock);
			}
			else
			{
				CampaignValidationMessage.Add("Unable to complete the update of auto generated campaign landing page. Reason: Unable to find Product Filter block.", InfoType.Warning);
				return false;
			}

			return true;
		}
		private int CreateAutoCampaignPage(int autoCampaignParentPage, ILekmerCampaign campaign, string webTitle)
		{
			if (webTitle.Length == 0)
			{
				CampaignValidationMessage.Add("Must atleast have title to be able to save landing page", InfoType.Failure);
				return -1;
			}

			var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();

			IContentPage contentPage = contentPageSecureService.Create();
			contentPage.Id = -1;
			contentPage.ParentContentNodeId = autoCampaignParentPage;
			contentPage.SiteStructureRegistryId = IoC.Resolve<IContentNodeSecureService>().GetById(autoCampaignParentPage).SiteStructureRegistryId;
			contentPage.ContentNodeTypeId = (int) ContentNodeTypeInfo.Page;
			contentPage.MasterPageId = AutoCampaignPageSetting.Instance.UseMasterPageId;
			contentPage.Ordinal = 0;
			contentPage.Title = webTitle;
			contentPage.PageTitle = webTitle;
			contentPage.AccessId = IoC.Resolve<IAccessSecureService>().GetByCommonName("All").Id;
			contentPage.ContentNodeStatusId = campaign.CampaignStatus.Id == 0 ? (int) ContentNodeStatusInfo.Online : (int) ContentNodeStatusInfo.Offline;

			var masterPage = contentPageSecureService.GetById(contentPage.MasterPageId.Value);
			contentPage.ContentPageTypeId = masterPage.ContentPageTypeId;
			contentPage.TemplateId = masterPage.TemplateId;
			contentPage.MasterTemplateId = masterPage.MasterTemplateId;

			var commonName = UrlCleaner.CleanUp(contentPage.Title);
			contentPage.CommonName = commonName;
			contentPage.UrlTitle = commonName;

			int attempts = 1;
			while (!ContentNodeSaveValidator.IsCommonNameUnique(contentPage))
			{
				contentPage.CommonName = commonName + attempts;
				contentPage.UrlTitle = commonName + attempts;
				attempts++;
				if (attempts > AutoCampaignPageSetting.Instance.MaximumAttemptsAtCommonName)
				{
					throw new Exception("Could not create unique commonName for auto-generated campaign landing page");
				}
			}

			var contentPageId = contentPageSecureService.Save(SignInHelper.SignedInSystemUser, contentPage);
			if (contentPageId < 0)
			{
				CampaignValidationMessage.Add("Unable to automatically create campaign landing page. Reason: unknown", InfoType.Warning);
				return -1;
			}

			Collection<IContentArea> areas = IoC.Resolve<IContentAreaSecureService>().GetAllByTemplate(contentPage.TemplateId);
			int areaId = areas.First().Id;

			const string blockTitle = "AutoCreatedLandingPageProductFilter";
			int blockTypeId = IoC.Resolve<IBlockTypeSecureService>().GetByCommonName("ProductFilter").Id;
			
			var service = IoC.Resolve<IBlockCreateSecureService>("ProductFilter");
			IBlock newBlock = service.SaveNew(SignInHelper.SignedInSystemUser, contentPageId, areaId, blockTypeId, blockTitle);
			var productFilterBlock = BlockProductFilterSecureService.GetByIdSecure(newBlock.Id);
			productFilterBlock.BlockStatusId = (int) BlockStatusInfo.Online;
			productFilterBlock.StartDate = campaign.StartDate;
			productFilterBlock.EndDate = campaign.EndDate;
			productFilterBlock.TemplateId = AutoCampaignPageSetting.Instance.ProductFilterPrimaryTemplateId;
			productFilterBlock.SecondaryTemplateId = AutoCampaignPageSetting.Instance.ProductFilterSecondaryTemplateId;

			BlockProductFilterSecureService.Save(SignInHelper.SignedInSystemUser, productFilterBlock);

			return contentPageId;
		}

		// Excel import.
		protected virtual void ImportPrices(object sender, EventArgs e)
		{
			ICampaignPluginControl<IProductAction> actionControl;
			IProductAction action;
			bool isActionValid = ValidateAction(out actionControl, out action);
			if (!isActionValid)
			{
				ActionPopup.Show();
				return;
			}

			try
			{
				HttpPostedFile file = FileUploadControl.PostedFile;

				if (!ValidateFileToUpload(file))
				{
					ActionPopup.Show();
					return;
				}

				var filePath = GetImportFilePath(file.FileName);
				if (File.Exists(filePath))
				{
					RemoveTempFile(filePath);
				}
				using (var fileStream = File.Create(filePath))
				{
					StreamAppender.AppendToStream(file.InputStream, fileStream, 8 * 1024);
				}

				var returnColumns = new Collection<string>
					{
						ProductDiscountPricesSpreadsheetColumns.HYID.GetEnumDescription(),
						ProductDiscountPricesSpreadsheetColumns.PrisSE.GetEnumDescription(),
						ProductDiscountPricesSpreadsheetColumns.PrisNO.GetEnumDescription(),
						ProductDiscountPricesSpreadsheetColumns.PrisDK.GetEnumDescription(),
						ProductDiscountPricesSpreadsheetColumns.PrisFI.GetEnumDescription(),
						ProductDiscountPricesSpreadsheetColumns.PrisNL.GetEnumDescription()
					};
				var spreadsheetHelper = new SpreadsheetHelper();
				var productPrices = spreadsheetHelper.ReadSpreadsheetDom(file.InputStream, returnColumns, true); //File does not contain all needed information. Please check if imported file contains columns with titles: 'HYID', 'Pris SE', 'Pris NO', 'Pris DK', 'Pris FI', 'Pris NL'
				file.InputStream.Close();

				ManageProductPrices(returnColumns, productPrices, (IProductDiscountAction) action, actionControl);

				ActionPopupValidationMessage.Add("Files data was uploaded successfully", InfoType.Success);
			}
			catch (Exception exception)
			{
				ActionPopupValidationMessage.Add(string.Format("ERROR: {0}", exception.Message), InfoType.Warning);
				_log.Error(exception);
			}
			finally
			{
				ActionPopup.Show();
			}
		}
		protected virtual bool ValidateAction(out ICampaignPluginControl<IProductAction> actionControl, out IProductAction action)
		{
			bool isValid = false;

			actionControl = ActionControlPlaceHolder.FindControl(ActionControlId) as ICampaignPluginControl<IProductAction>;
			if (actionControl == null)
			{
				throw new InvalidOperationException(
					"The operation requires an action plugin control, " +
					"but no such control was registered to the ActionControlPlaceHolder control collection.");
			}

			if (actionControl.TryGet(out action))
			{
				PersistInState(action);
				ActionPopupAdditionalValidationMessageDiv.Visible = false;
				isValid = true;
			}
			else
			{
				ActionPopupValidationMessage.AddRange(actionControl.ValidationMessages);
				if (actionControl.AdditionalValidationMessage.Length > 0)
				{
					ActionPopupAdditionalValidationMessageDiv.Visible = true;
					ActionPopupAdditionalValidationMessage.Text = actionControl.AdditionalValidationMessage;
				}
			}

			return isValid;
		}
		protected virtual bool ValidateFileToUpload(HttpPostedFile file)
		{
			bool isValid = true;

			if (!FileUploadControl.HasFile)
			{
				ActionPopupValidationMessage.Add("Please select excel file for upload", InfoType.Warning);
				isValid = false;
			}

			int maxFileSize = HttpRuntimeSetting.MaxRequestLength;
			if (maxFileSize <= 0)
			{
				ActionPopupValidationMessage.Add(MediaMessage.NoConfirmMaxFileSize, InfoType.Warning);
				isValid = false;
			}

			if (FileUploadControl.PostedFile.ContentLength / 1024 > maxFileSize)
			{
				ActionPopupValidationMessage.Add("There is large file to upload", InfoType.Warning);
				isValid = false;
			}

			var extension = Path.GetExtension(file.FileName);
			if (string.IsNullOrEmpty(extension) || (extension != ".xls" && extension != ".xlsx"))
			{
				ActionPopupValidationMessage.Add(MediaMessage.IncorrectFileFormat, InfoType.Warning);
				isValid = false;
			}

			return isValid;
		}
		protected virtual string GetImportFilePath(string fileName)
		{
			var path = BackOfficeSetting.Instance.TempProductDiscountPricesExcelFilePathImport;
			return string.Format(path, SignInHelper.SignedInSystemUser.Id, DateTime.Now.ToString("yyyyMMdd-HHmmss"), fileName);
		}
		protected virtual void RemoveTempFile(string filePath)
		{
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}
		}
		private void ManageProductPrices(Collection<string> returnColumns, IEnumerable<Collection<string>> productPrices, IProductDiscountAction action, ICampaignPluginControl<IProductAction> actionControl)
		{
			var productDiscountPrices = GetProductDiscountPrices(returnColumns, productPrices);
			foreach (var productDiscountPrice in productDiscountPrices)
			{
				if (action.ProductDiscountPrices.ContainsKey(productDiscountPrice.Key))
				{
					action.ProductDiscountPrices.Remove(productDiscountPrice.Key);
				}

				action.ProductDiscountPrices.Add(productDiscountPrice.Key, productDiscountPrice.Value);
			}

			actionControl.DataBind(action);
		}
		private Dictionary<int, LekmerCurrencyValueDictionary> GetProductDiscountPrices(Collection<string> returnColumns, IEnumerable<Collection<string>> productPrices)
		{
			var productDiscountPrices = new Dictionary<int, LekmerCurrencyValueDictionary>();

			int hyidIndex = returnColumns.IndexOf(ProductDiscountPricesSpreadsheetColumns.HYID.GetEnumDescription());
			int sePriceIndex = returnColumns.IndexOf(ProductDiscountPricesSpreadsheetColumns.PrisSE.GetEnumDescription());
			int noPriceIndex = returnColumns.IndexOf(ProductDiscountPricesSpreadsheetColumns.PrisNO.GetEnumDescription());
			int dkPriceIndex = returnColumns.IndexOf(ProductDiscountPricesSpreadsheetColumns.PrisDK.GetEnumDescription());
			int fiPriceIndex = returnColumns.IndexOf(ProductDiscountPricesSpreadsheetColumns.PrisFI.GetEnumDescription());
			int nlPriceIndex = returnColumns.IndexOf(ProductDiscountPricesSpreadsheetColumns.PrisNL.GetEnumDescription());

			var productPricesTemp = new Dictionary<string, LekmerCurrencyValueDictionary>();

			foreach (var productPrice in productPrices)
			{
				decimal sePrice, noPrice, dkPrice, fiPrice, nlPrice;

				string hyId = productPrice[hyidIndex];
				if (string.IsNullOrEmpty(hyId)) continue;

				bool isCorrectSePrice = IsCorrectPriceValue(productPrice[sePriceIndex], out sePrice);
				bool isCorrectNoPrice = IsCorrectPriceValue(productPrice[noPriceIndex], out noPrice);
				bool isCorrectDkPrice = IsCorrectPriceValue(productPrice[dkPriceIndex], out dkPrice);
				bool isCorrectFiPrice = IsCorrectPriceValue(productPrice[fiPriceIndex], out fiPrice);
				bool isCorrectNlPrice = IsCorrectPriceValue(productPrice[nlPriceIndex], out nlPrice);

				if (string.IsNullOrEmpty(hyId) || !isCorrectSePrice || !isCorrectNoPrice || !isCorrectDkPrice || !isCorrectFiPrice || !isCorrectNlPrice)
				{
					throw new Exception("Some of the data in the file are incorrect");
				}

				var currencyValues = new LekmerCurrencyValueDictionary();
				AddToCurrencyValueDictionary((int) ProductDiscountPricesSpreadsheetColumns.PrisSE, sePrice, currencyValues);
				AddToCurrencyValueDictionary((int)ProductDiscountPricesSpreadsheetColumns.PrisNO, noPrice, currencyValues);
				AddToCurrencyValueDictionary((int)ProductDiscountPricesSpreadsheetColumns.PrisDK, dkPrice, currencyValues);
				AddToCurrencyValueDictionary((int)ProductDiscountPricesSpreadsheetColumns.PrisFI, fiPrice, currencyValues);
				AddToCurrencyValueDictionary((int)ProductDiscountPricesSpreadsheetColumns.PrisNL, nlPrice, currencyValues);

				productPricesTemp.Add(hyId, currencyValues);
			}

			var productIds = ProductSecureService.GetProductIdsByHyIdList(productPricesTemp.Keys);
			foreach (var product in productIds)
			{
				productDiscountPrices.Add(product.Value, productPricesTemp[product.Key]);
			}
			
			return productDiscountPrices;
		}
		private bool IsCorrectPriceValue(string priceValue, out decimal price)
		{
			price = -1;
			if (string.IsNullOrEmpty(priceValue))
			{
				return true;
			}

			return decimal.TryParse(priceValue.Replace(".",","), out price);
		}
		private void AddToCurrencyValueDictionary(int currencyId, decimal price, LekmerCurrencyValueDictionary currencyValueDictionary)
		{
			if (price > 0)
			{
				currencyValueDictionary.AddItem(currencyId, price);
			}
		}
	}

	[Serializable]
	public class CampaignState
	{
		public ICampaign Campaign { get; set; }
		public ICampaignLandingPage LandingPage { get; set; }
		public Collection<LandingPageMediaItem> ImageMediaItems { get; set; }
		public Collection<LandingPageMediaItem> IconMediaItems { get; set; }

		public CampaignState(ICampaign campaign, ICampaignLandingPage campaignLandingPage)
		{
			Campaign = campaign;
			LandingPage = campaignLandingPage;
			ImageMediaItems = new Collection<LandingPageMediaItem>();
			IconMediaItems = new Collection<LandingPageMediaItem>();
		}
	}

	public class CampaignChannel : CampaignRegistry
	{
		public bool IsSelected { get; set; }

		public CampaignChannel(ICampaignRegistry registry, bool isSelected)
		{
			Id = registry.Id;
			CommonName = registry.CommonName;
			Title = registry.Title;
			IsSelected = isSelected;
		}
	}

	[Serializable]
	public class LandingPageMediaItem
	{
		public string Id { get; set; }
		public IMediaItem MediaItem { get; set; }
		public Collection<CustomCampaignRegistry> CustomCampaignRegistries { get; set; }

		public LandingPageMediaItem(string id, IMediaItem mediaItem, Collection<CustomCampaignRegistry> customCampaignRegistries)
		{
			Id = id;
			MediaItem = mediaItem;
			CustomCampaignRegistries = customCampaignRegistries;
		}
	}

	[Serializable]
	public class CustomCampaignRegistry : CampaignRegistry
	{
		public bool IsSelected { get; set; }
		public string ShortName { get; set; }
	}
}