using System;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Aliases
{
    public partial class FolderEdit : LekmerPageController,IEditor 
    {
		protected virtual bool? IsSuccessMessage()
		{
			return Request.QueryString.GetBooleanOrNull("HasMessage");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			Alias.RegisterStartupScript(this, GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", ParentAliasTree.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", ParentAliasTree.ClientID), true);
		}
    	
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
    		ParentAliasTree.NodeCommand += OnNodeCommand;
		}
		
		protected override void PopulateForm()
		{
			var aliasFolderService = IoC.Resolve<IAliasFolderSecureService>();

			IAliasFolder aliasFolder = aliasFolderService.GetById(((Alias)Master).SelectedFolderId.Value);
			TitleTextBox.Text = aliasFolder.Title;

			PopulateTreeView(null);

			if (IsSuccessMessage() == null || !((bool)IsSuccessMessage())) return;
			ShowSuccessMessage();
		}
		
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.Alias.GetDefaultUrl());
		}
		
		public virtual void OnSave(object sender, EventArgs e)
		{
			var aliasFolderService = IoC.Resolve<IAliasFolderSecureService>();
			
			IAliasFolder aliasFolder = aliasFolderService.GetById(((Alias)Master).SelectedFolderId.Value);
            if (aliasFolder == null)
			{
				throw new BusinessObjectNotExistsException(((Alias)Master).SelectedFolderId.Value);
			}
			aliasFolder.Title = TitleTextBox.Text;
			if (ParentAliasTree.SelectedNodeId.HasValue)
			{
				aliasFolder.ParentAliasFolderId = ParentAliasTree.SelectedNodeId.Value == ParentAliasTree.RootNodeId ? null : ParentAliasTree.SelectedNodeId;
			}

			ValidationResult result = aliasFolderService.Validate(aliasFolder);
			if (!result.IsValid)
			{
				messager.AddRange(result.Errors);
				return;
			}

			aliasFolderService.Save(SignInHelper.SignedInSystemUser, aliasFolder);

			((Alias)Page.Master).UpdateMasterTree();
			ShowSuccessMessage();
		}

		private void ShowSuccessMessage()
		{
			messager.Add(Resources.GeneralMessage.SaveSuccessAliasFolder);
			messager.MessageType = InfoType.Success;
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var master = (Alias)Page.Master;
			var folders = master.GetTreeDataSource(folderId ?? master.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;
			var editedFolder = folders.FirstOrDefault(f => f.Id == master.SelectedFolderId);

			ParentAliasTree.DataSource = editedFolder != null
				? ParentAliasTree.GetFilteredSource(folders, editedFolder.Id)
				: folders;
			ParentAliasTree.RootNodeTitle = Resources.Interface.Literal_Aliases;
			ParentAliasTree.DataBind();
			ParentAliasTree.SelectedNodeId = folderId ?? (editedFolder != null ? editedFolder.ParentId : ParentAliasTree.RootNodeId) ?? ParentAliasTree.RootNodeId;
		}
    }
}
