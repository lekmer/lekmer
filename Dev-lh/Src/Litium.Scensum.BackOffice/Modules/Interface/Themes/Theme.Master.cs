﻿using System;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Interface.Themes
{
	public partial class Theme : System.Web.UI.MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			ThemeCreateButton.Click += ThemeCreateButton_Click;
		}

		protected void ThemeCreateButton_Click(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.Theme.GetCreateUrl());
		}
	}
}
