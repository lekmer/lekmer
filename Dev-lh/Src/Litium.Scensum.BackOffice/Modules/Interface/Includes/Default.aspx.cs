using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Includes
{
	public partial class Default : LekmerPageController
	{

		protected override void SetEventHandlers()
		{
			IncludeGrid.RowDataBound += IncludeGridRowDataBound;
			IncludeGrid.RowCommand += IncludeGridRowCommand;
		}

		protected override void PopulateForm()
		{
			if (Request.QueryString.GetBooleanOrNull("JustCreated") ?? false)
			{
				messager.MessageType = InfoType.Success;
				messager.Add(Resources.GeneralMessage.SaveSuccessInclude);
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if(IsSearchResult().HasValue && IsSearchResult().Value)
			{
				var master = Master as Include;
				if (master == null) return;
				master.BreadcrumbAppend.Clear();
				master.BreadcrumbAppend.Add("Includes");
				master.BreadcrumbAppend.Add("Search");
			}
			
		}

		protected virtual bool? ShowFolderContent()
		{
			return Request.QueryString.GetBooleanOrNull("ShowFolderContent");
		}

		protected virtual bool? IsSearchResult()
		{
			return Request.QueryString.GetBooleanOrNull("IsSearchResult");
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
            
			
			if (ShowFolderContent().HasValue && ShowFolderContent().Value)
			{
				((Include) Master).ShowFolderContent = true;
				PopulateIncludeGrid(((Include) Page.Master).SelectedFolderId);
				return;
			}

			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				PopulateIncludeGrid(((Include)Page.Master).SelectedFolderId);
				return;
			}

			if (!((Include)Master).ShowFolderContent)
			{
				((Include)Master).DenySelection = true;
			}
		}

		protected virtual void IncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var hiddenId = (HiddenField)e.Row.FindControl("IdHiddenField");
			var hlEdit = (HyperLink)e.Row.FindControl("EditLink");
			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				hlEdit.NavigateUrl = PathHelper.Template.Include.GetEditUrlForSearch(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
			}
			else
			{
				hlEdit.NavigateUrl = PathHelper.Template.Include.GetEditUrlForDefault(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
			}
			
		}

		protected virtual void IncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			int includeId;

			if (!int.TryParse(e.CommandArgument.ToString(), out includeId))
			{
				includeId = 0;
			}
			switch (e.CommandName)
			{
				case "CopyInclude":
					Response.Redirect(PathHelper.Template.Include.GetCreateCopyOfUrl(includeId));
					break;
				case "DeleteInclude":
					DeleteInclude(includeId);
					break;
			}
		}

		protected void PopulateIncludeGrid(int? includeFolderId)
		{
			var includeService = IoC.Resolve<IIncludeSecureService>();
			if (includeFolderId != null)
			{
				IncludeGrid.DataSource = includeService.GetAllByIncludeFolder((int)includeFolderId);
				IncludeGrid.Columns[2].Visible = false;
				IncludeGrid.DataBind();
			}
			else
			{
				string searchTitle = SearchCriteriaState<string>.Instance.Get(PathHelper.Template.Include.GetSearchResultUrl());
				if (!string.IsNullOrEmpty(searchTitle))
				{
					IncludeGrid.DataSource = includeService.Search(searchTitle);
				}
				IncludeGrid.DataBind();
			}
		}

		protected static void DeleteInclude(int id)
		{
			var includeService = IoC.Resolve<IIncludeSecureService>();
			includeService.Delete(SignInHelper.SignedInSystemUser, id);
		}

		protected string GetPath(int includeFolderId)
		{
			Collection<IIncludeFolder> includeFolders = ((Include)Page.Master).GetAllIncludeFolders();
			IIncludeFolder includeFolder = includeFolders.FirstOrDefault(f => f.Id == includeFolderId);
			var stack = new Stack<string>();
			stack.Push(includeFolder.Title);
			while (includeFolder.ParentIncludeFolderId.HasValue)
			{
				IIncludeFolder folder = includeFolder;
				includeFolder = includeFolders.FirstOrDefault(f => f.Id == folder.ParentIncludeFolderId.Value);
				stack.Push(includeFolder.Title);
			}
			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString();
		}
	}
}
