<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Includes/Include.Master"   CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Includes.Default" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="IncludeTabContent" ContentPlaceHolderID="IncludeContent" runat="server">
	<div id="include-list">
	    <asp:UpdatePanel ID="IncludesUpdatePanel" runat="server" UpdateMode="Always" >
	    
		    <ContentTemplate>
			    <asp:GridView
			    ID="IncludeGrid" 
			    SkinID="grid" 
			    runat="server" 
			    AutoGenerateColumns="false" 
			    Width="100%">
			    <Columns>
				    <asp:TemplateField HeaderText="<%$ Resources:Interface, Literal_SystemName %>" ItemStyle-Width="50%">
					    <ItemTemplate>
						    <div style="width: 100%; overflow:hidden;">
					            <div style="float:left; max-width:230px; overflow:hidden;">
						            <asp:Image ID="img" runat="server" ImageUrl="~/Media/Images/Interface/include.png" />
			                        <asp:HiddenField ID="IdHiddenField" runat="server" Value='<%# Eval("Id") %>' />
			                        <uc:HyperLinkEncoded ID="EditLink" runat="server" Text='<%# Eval("CommonName")%>' />
					                </div>
					                <div style="float:left;">
						                <Scensum:ContextMenu ID="ContextMenuControl" runat="server" 
				                            CallerImageSrc="~/Media/Images/Common/context-menu.png" 
				                            CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png"
				                            MenuContainerCssClass="context-menu-container"
						                    MenuShadowCssClass="context-menu-shadow"
						                    MenuCallerCssClass="context-menu-caller" 
				                            >  
                                            <div class="context-menu-header">
					                            <%= Resources.General.Literal_Manage %>
				                            </div>
				                            <div class="menu-row-separator">
				                            </div>
				                            <div class="context-menu-row">
					                            <img src="<%=ResolveUrl("~/Media/Images/interface/move-copy.png") %>" />
					                            <asp:LinkButton ID="IncludeMenuCopyButton" runat="server" CommandArgument = '<%# Eval("Id") %>' CommandName="CopyInclude"  Text="<%$ Resources:Interface, Button_Copy %>"/>
				                            </div>
                    						
				                            <div class="context-menu-row">
                                                <img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
					                            <asp:LinkButton ID="IncludeMenuDeleteButton" runat="server" OnClientClick="return DeleteConfirmation('include')" CommandArgument='<%# Eval("Id") %>' CommandName="DeleteInclude" Text = "<%$ Resources:General, Button_Delete %>"/>
				                            </div>   
                                    </Scensum:ContextMenu>
					            </div>
						    </div>	
					    </ItemTemplate>
				    </asp:TemplateField>
				   <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Description %>" ItemStyle-Width="45%">
						<ItemTemplate>
							<uc:LiteralEncoded ID="DescriptionLiteral" runat="server" Text='<%# Eval("Description") != null && Eval("Description").ToString().Length > 50 ? Eval("Description").ToString().Substring(0, 50) : Eval("Description") %>'></uc:LiteralEncoded>
						</ItemTemplate>
					</asp:TemplateField>
			    	<asp:TemplateField HeaderText="Path" ItemStyle-Width="25%">
						<ItemTemplate>
							<asp:Label ID="PathLabel" runat="server" Text='<%# GetPath(int.Parse(Eval("IncludeFolderId").ToString())) %>' ></asp:Label>							
						</ItemTemplate>
					</asp:TemplateField>
					
				    <asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
					    <ItemTemplate>
						    <asp:ImageButton runat="server" ID="DeleteAliasButton" OnClientClick='<%# " return DeleteConfirmation(\""+ Resources.Interface.Literal_includeConfirmDelete +"\");"%>' CommandName="DeleteInclude" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" />
					    </ItemTemplate>
				    </asp:TemplateField>
			    </Columns>
		    </asp:GridView>
		    
		    <div class="interface-errors">			
				<uc:MessageContainer ID="messager"  MessageType="Failure" HideMessagesControlId="DeleteAliasButton" runat="server" />
			</div>
		    
		    </ContentTemplate>
		</asp:UpdatePanel>
	</div>
</asp:Content>
