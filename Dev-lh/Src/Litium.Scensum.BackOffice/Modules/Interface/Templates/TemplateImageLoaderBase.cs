using System;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
	public abstract class TemplateImageLoaderBase : IHttpHandler
	{
		private const int _bufferSizeBytes = 8 * 1024;

		protected abstract int Width { get; }

		protected abstract int Height { get; }

		protected abstract int Quality { get; }

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			int id;
			string fileName = int.TryParse(context.Request["id"], out id) ? ProcessImageByTemplateId(id) : context.Request.QueryString["name"];

			LoadImage(context, fileName, Width, Height, Quality);
			
		}

		private static void LoadImage(HttpContext context, string fileName, int width, int height, int quality)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (fileName == null) throw new ArgumentNullException("fileName");

			string etag = string.Format(
				CultureInfo.CurrentCulture, "{0}-{1}-{2}-{3}",
				fileName, width, height, quality);
			var lastModified = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			if (HasMatchingETag(context, etag))
			{
				SendNotModifiedHeader(context);
				return;
			}

			if (HasMatchingLastModified(context, lastModified))
			{
				SendNotModifiedHeader(context);
				return;
			}

			var templateService = IoC.Resolve<ITemplateSecureService>();

			string mime;
			using (Stream imageStream = templateService.LoadImage(fileName, width, height, quality, out mime))
			{
				if (imageStream == null)
				{
					throw new HttpException(404, "Page not found");
				}

				context.Response.Cache.SetLastModified(lastModified);
				context.Response.Cache.SetETag(etag);
				context.Response.ContentType = mime;

				StreamAppender.AppendToStream(imageStream, context.Response.OutputStream, _bufferSizeBytes);
			}
		}

		private static bool HasMatchingETag(HttpContext context, string etag)
		{
			string ifNoneMatchHeader = context.Request.Headers["If-None-Match"];
			return ifNoneMatchHeader != null && ifNoneMatchHeader.Equals(etag);
		}

		private static bool HasMatchingLastModified(HttpContext context, DateTime lastModified)
		{
			string lastModifiedHeader = context.Request.Headers["If-Modified-Since"];
			if (lastModifiedHeader == null) return false;

			DateTime clientLastModified = DateTime.Parse(lastModifiedHeader, CultureInfo.CurrentCulture);
			return clientLastModified.ToUniversalTime() == lastModified;
		}

		private static void SendNotModifiedHeader(HttpContext context)
		{
			context.Response.StatusCode = 304;
			context.Response.StatusDescription = "Not modified";
		}

		public bool IsReusable
		{
			get { return true; }
		}

		private static string ProcessImageByTemplateId(int id)
		{
		    ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
		    ITemplate template = templateSecureService.GetById(id);
			return template.ImageFileName;
		}
	}
}
