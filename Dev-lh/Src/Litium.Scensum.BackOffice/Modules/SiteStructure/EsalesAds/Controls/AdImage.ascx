﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdImage.ascx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds.Controls.AdImage" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>

<div>

	<asp:UpdatePanel ID="ImageUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:HiddenField runat="server" ID="ImageIdHidden" />

			<div class="ad-image row">

				<div class="column">

					<div class="input-box">
						<span class="bold"><%= Resources.EsalesAds.Literal_Image %> *</span>
						<div class="default-image">
							<div class="image-show">
								<a id="ZoomProductImageAnchor" runat="server" target="blank">
									<asp:Image ID="DefaultImage" ImageUrl="~/Media/Images/Assortment/defaultProduct.jpg" runat="server" CssClass="image" />
									<img ID="ZoomProductImage" src="~/Media/Images/Common/zoom.png" runat="server" class="zoom-image" />
								</a>
							</div>
						</div>
					</div>

					<div class="clear-zero-div">&nbsp;</div>

				</div>

				<div class="column image-description">

					<div class="input-box">
						<span class="bold"><%= Resources.General.Literal_Title %></span>
						<uc:LiteralEncoded ID="ImageTitleLiteral" runat="server" ></uc:LiteralEncoded>
					</div>

					<div class="input-box">
						<span class="bold"><%= Resources.General.Literal_AlternativeText %></span>
						<uc:LiteralEncoded ID="ImageAlternativeTextLiteral" runat="server" ></uc:LiteralEncoded>
					</div>

					<div class="input-box">
						<span class="bold"><%= Resources.General.Literal_FileExtension %></span>
						<asp:Label ID="ImageFileExtensionLabel" runat="server" ></asp:Label>
					</div>

					<div class="input-box">
						<span class="bold"><%= Resources.General.Literal_Dimensions %></span>
						<asp:Label ID="ImageDimensionsLabel" runat="server" ></asp:Label>
					</div>

					<div class="input-box">
						<uc:ImageLinkButton runat="server" ID="ImgBrowseButton" Text="<%$ Resources:Product,Button_ChangeImage %>" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();"/>
						<div class="clear-zero-div">&nbsp;</div>
					</div>

					<div class="input-box">
						<uc:ImageLinkButton runat="server" ID="DeleteImageButton" Text="<%$ Resources:Product,Button_DeleteImage %>" SkinID="DefaultButton" />
						<div class="clear-zero-div">&nbsp;</div>
					</div>

				</div>

				<div class="clear-zero-div">&nbsp;</div>

			</div>

			<ajaxToolkit:ModalPopupExtender ID="ImagesPopup" runat="server" TargetControlID="ImgBrowseButton" PopupControlID="ImagesDiv" 
				BackgroundCssClass="popup-background" CancelControlID="_inpCloseImages" OnCancelScript="ResetDefaultMediaMessages();">
			</ajaxToolkit:ModalPopupExtender>

		</ContentTemplate>
	</asp:UpdatePanel>

	<div id="ImagesDiv" runat="server" class="product-popup-images-container" style="z-index: 10010; display: none;">
		<div class="product-popup-images-header">
			<div class="product-popup-images-header-left">
			</div>
			<div class="product-popup-images-header-center">
				<span><%= Resources.Product.Literal_AddImages %></span>
				<input type="button" id="_inpCloseImages" runat="server" class="_inpClose" value="x" />
			</div>
			<div class="product-popup-images-header-right">
			</div>
		</div>
		<br clear="all" />
		<div class="product-popup-images-body">
			<uc:ImageSelect id="ImageSelectControl" runat="server"/>
		</div>
	</div>

</div>