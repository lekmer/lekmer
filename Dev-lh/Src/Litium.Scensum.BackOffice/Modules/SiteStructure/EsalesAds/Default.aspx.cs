﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds
{
	public partial class Default : LekmerPageController
	{
		private Collection<IAdFolder> _allFolders;

		public virtual AdsMaster MasterPage
		{
			get
			{
				return (AdsMaster)Master;
			}
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected override void SetEventHandlers()
		{
			AdsGrid.RowCommand += OnRowCommand;
			AdsGrid.RowDataBound += OnRowDataBound;
			AdsGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateAds();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			try
			{
				switch (e.CommandName)
				{
					case "DeleteAd":
						DeleteAd(id);
						PopulateAds();
						SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
						break;
				}
			}
			catch
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingUpdateFailed);
			}
			
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			AdsGrid.PageIndex = e.NewPageIndex;

			PopulateAds();
		}

		protected virtual void DeleteAd(int adId)
		{
			IoC.Resolve<IAdSecureService>().Delete(SignInHelper.SignedInSystemUser, adId);
		}

		protected virtual void PopulateAds()
		{
			if (IsSearchResult)
			{
				AdsGrid.DataSource = SearchAds();
				AdsGrid.Columns[3].Visible = true;
			}
			else
			{
				AdsGrid.DataSource = RetrieveAds();
				AdsGrid.Columns[3].Visible = false;
			}

			AdsGrid.DataBind();

			AdsGrid.Visible = AdsGrid.DataSource != null;
		}

		protected virtual string GetEditUrl(object adId)
		{
			return LekmerPathHelper.Ad.GetEditUrl(System.Convert.ToInt32(adId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<IAd> RetrieveAds()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return IoC.Resolve<IAdSecureService>().GetAllByFolder(folderId.Value);
			}

			return null;
		}

		protected virtual Collection<IAd> SearchAds()
		{
			var searchUrl = LekmerPathHelper.Ad.GetSearchResultUrl();

			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);

			if (string.IsNullOrEmpty(searchCriteria))
			{
				return new Collection<IAd>();
			}

			return IoC.Resolve<IAdSecureService>().Search(searchCriteria);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<IAdFolderSecureService>();
				_allFolders = service.GetAll();
			}

			var folderHelper = new FolderPathHelper();
			return folderHelper.GetPath(_allFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");

				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");

				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');");
			}
		}
	}
}