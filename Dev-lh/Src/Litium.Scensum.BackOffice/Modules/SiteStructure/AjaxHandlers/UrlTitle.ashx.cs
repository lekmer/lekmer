using System.Web;
using Litium.Lekmer.Common;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.AjaxHandlers
{
	/// <summary>
	/// Summary description for $codebehindclassname$
	/// </summary>
	public class UrlTitle : IHttpHandler
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
		public void ProcessRequest(HttpContext context)
		{
			string title = context.Request.Params["title"];
			string urlTitle = context.Request.Params["urlTitle"];

			if (string.IsNullOrEmpty(urlTitle))
			{
				urlTitle = title;
			}

			if (!string.IsNullOrEmpty(urlTitle))
			{
				urlTitle = UrlCleaner.CleanUp(urlTitle);
			}

			context.Response.Write(urlTitle);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}
