using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class Pages : MasterPage
	{
		public event EventHandler StatusChanged;
		private const string BreadcrumbsSeparator = "&nbsp;>&nbsp;";

		private Collection<string> _breadcrumbs;
		public virtual Collection<string> Breadcrumbs
		{
			get
			{
				if(_breadcrumbs == null)
				{
					_breadcrumbs = new Collection<string>();
				}
				return _breadcrumbs;
			}
		}

		private int? _currentRootId;
		public int? CurrentRootId
		{
			get
			{
				if (!_currentRootId.HasValue)
				{
					var registry = IoC.Resolve<ISiteStructureRegistrySecureService>().GetByChannel(ChannelHelper.CurrentChannel.Id);
					_currentRootId = registry != null ? (int?)(-1 * registry.Id) : null;
				}
				return _currentRootId;
			}
		}

		protected int CurrentChannelId
		{
			get
			{
				return Session["Sitestructure_Current_ChannelId"] != null
				       	? (int) Session["Sitestructure_Current_ChannelId"]
				       	: (CurrentChannelId = ChannelHelper.CurrentChannel.Id);
			}
			set
			{
				Session["Sitestructure_Current_ChannelId"] = value;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			CreateFolderButton.Click += CreateFolderClick;
			CreatePageButton.Click += CreatePageClick;
			CreateLinkButton.Click += CreateLinkClick;
			CreateShortcutButton.Click += CreateShortcutClick;
			CreateMasterPageButton.Click += CreateMasterPageClick;
			CreatePageFromMasterButton.Click += CreatePageFromMasterClick;

			SiteStructureTreeViewControl.NodeCommand += OnSiteStructureTreeNodeCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!Page.IsPostBack)
			{
				//Channel has beed changed
				if (CurrentChannelId != ChannelHelper.CurrentChannel.Id)
				{
					SiteStructureTreeViewControl.SelectedNodeId = null;
					CurrentChannelId = ChannelHelper.CurrentChannel.Id;
					Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
				}

				PopulateTreeView(GetNodeId() ?? GetParentNodeId() ?? SiteStructureTreeViewControl.SelectedNodeId ?? CurrentRootId);

				if (!SiteStructureTreeViewControl.DenySelection)
				{
                    int? nodeId = GetNodeId() ?? GetParentNodeId();
                    if (nodeId.HasValue)
                        PopulateBreadcrumbs(nodeId.Value);
				}
                else
                    PopulateBreadcrumbs(null);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (SiteStructureTreeViewControl.SelectedNodeId <= 0)
			{
				SiteStructureTreeViewControl.DenySelection = true;
			}

			ScriptManager.RegisterStartupScript(SortNodesUpdatePanel, SortNodesUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}');", SiteStructureTreeViewControl.ClientID), true);
		}

		protected virtual void OnSiteStructureTreeNodeCommand(object sender, Web.Controls.Tree.TemplatedTree.TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
			SiteStructureTreeViewControl.DenySelection = true;
		}
        
		public virtual void PopulateTreeView(int? nodeId)
		{
			SiteStructureTreeViewControl.DataSource = GetTreeNodes(nodeId ?? CurrentRootId, true);
			SiteStructureTreeViewControl.DataBind();
			SiteStructureTreeViewControl.SelectedNodeId = nodeId ?? CurrentRootId;
		}

		public virtual IEnumerable<ISiteStructureNode> GetTreeNodes(int? selectedId, bool getAllRegistries)
		{
			return IoC.Resolve<IContentNodeSecureService>().GetTree(selectedId, getAllRegistries);
		}

		public virtual void UpdateMasterTreeAndBreadcrumbs(int? id)
		{
			PopulateTreeView(id);

			var breadcrumbs = string.Empty;
			var source = SiteStructureTreeViewControl.DataSource;
			var nodeId = id ?? CurrentRootId;
			ISiteStructureNode node;
			while (nodeId.HasValue && (node = source.FirstOrDefault(n => n.Id == nodeId)) != null)
			{
				breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", node.Title, BreadcrumbsSeparator, breadcrumbs);
				nodeId = node.ParentId;
			}
			BreadcrumbLabel.Text = !string.IsNullOrEmpty(breadcrumbs) ? breadcrumbs.Substring(0, breadcrumbs.Length - BreadcrumbsSeparator.Length) : string.Empty;
		}

		protected virtual void CreateFolderClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetFolderCreateUrl());
		}
		protected virtual void CreatePageClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageCreateUrl());
		}
		protected virtual void CreateMasterPageClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetMasterPageCreateUrl());
		}
		protected virtual void CreatePageFromMasterClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageFromMasterCreateUrl());
		}
		protected virtual void CreateLinkClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetLinkCreateUrl());
		}
		protected virtual void CreateShortcutClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetShortcutCreateUrl());
		}

		protected virtual void CreateChildFolderClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetFolderCreateUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void CreateChildPageClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageCreateUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void CreateChildPageFromMasterClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageFromMasterCreateUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void CreateChildMasterPageClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetMasterPageCreateUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void CreateChildLinkClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetLinkCreateUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void CreateChildShortcutClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetShortcutCreateUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}

		protected virtual void MoveClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetContentNodesMoveUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void SortChildrenClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetContentNodeSortUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void SetStartPageClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetStartPageSetUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void BringOnlineClick(object sender, EventArgs e)
		{
			ChangeNodeStatus(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value, ContentNodeStatusInfo.Online);
			PopulateTreeView(SiteStructureTreeViewControl.MenuLastClickedNodeId);
			if(StatusChanged!=null)
			{
				StatusChanged(this, EventArgs.Empty);
			}
		}
		protected virtual void SetOfflineClick(object sender, EventArgs e)
		{
			ChangeNodeStatus(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value, ContentNodeStatusInfo.Offline);
			PopulateTreeView(SiteStructureTreeViewControl.MenuLastClickedNodeId);
			if (StatusChanged != null)
			{
				StatusChanged(this, EventArgs.Empty);
			}
		}
		protected virtual void MakeHiddenClick(object sender, EventArgs e)
		{
			ChangeNodeStatus(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value, ContentNodeStatusInfo.Hidden);
			PopulateTreeView(SiteStructureTreeViewControl.MenuLastClickedNodeId);
			if (StatusChanged != null)
			{
				StatusChanged(this, EventArgs.Empty);
			}
		}
		protected virtual void DeleteClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDeleteUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}

		protected virtual void SeoClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetSeoSettingUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void PageSettingsClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageSettingsUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void ChangeLayoutClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageLayoutUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		protected virtual void ChangePageTypeClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageTypeChangeUrl(SiteStructureTreeViewControl.MenuLastClickedNodeId.Value));
		}
		
		protected static void ChangeNodeStatus(int nodeId, ContentNodeStatusInfo status)
		{
			var contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
			IContentNode node = contentNodeSecureService.GetById(nodeId);
			node.ContentNodeStatusId = (int)status;
			contentNodeSecureService.Save(SignInHelper.SignedInSystemUser, node);
		}

		protected virtual int? GetNodeId()
		{
			return Request.QueryString.GetInt32OrNull("id");
		}
		protected virtual int? GetParentNodeId()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}

		public virtual void DenyTreeSelection()
		{
			SiteStructureTreeViewControl.DenySelection = true;
		}

        private void PopulateBreadcrumbs(int? nodeId)
		{
            var breadcrumbs = new StringBuilder();

            if (nodeId != null)
            {
                int registryId = nodeId > 0 ?
                                          IoC.Resolve<IContentNodeSecureService>().GetById((int)nodeId).SiteStructureRegistryId
                                        : -1*(int)nodeId;
                Collection<IContentNode> nodes = IoC.Resolve<IContentNodeSecureService>().GetAllByRegistry(registryId);

                IContentNode node = nodes.FirstOrDefault(item => item.Id == nodeId);

                while (node != null)
                {
                    breadcrumbs.Insert(0, BreadcrumbsSeparator);
                    breadcrumbs.Insert(0, node.Title);

                    int? parentNodeId = node.ParentContentNodeId;
                    node = parentNodeId == null ? null : nodes.First(item => item.Id == parentNodeId);
                }
            }

            if (Breadcrumbs != null && Breadcrumbs.Count > 0)
			{
				foreach (string breadcrumb in Breadcrumbs)
				{
					if (!string.IsNullOrEmpty(breadcrumb))
					{
						breadcrumbs.Append(breadcrumb);
						breadcrumbs.Append(BreadcrumbsSeparator);
					}
				}
			}

			string breadcrumbString = breadcrumbs.ToString();
			if (breadcrumbString.Length > 0)
			{
				breadcrumbString = breadcrumbString.Substring(0, breadcrumbs.Length - BreadcrumbsSeparator.Length);
			}

			BreadcrumbLabel.Text = breadcrumbString;
		}
	}
}
