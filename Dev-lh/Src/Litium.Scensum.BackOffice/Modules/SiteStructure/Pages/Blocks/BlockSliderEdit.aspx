﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="BlockSliderEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks.BlockSliderEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockTimeLimiter" Src="~/UserControls/SiteStructure/BlockTimeLimiter.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockTargetDevice" Src="~/UserControls/SiteStructure/BlockTargetDevice.ascx" %>
<%@ Register TagPrefix="uc" TagName="Group" Src="~/UserControls/SiteStructure/Group.ascx" %>

<asp:Content ID="SliderMessages" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary  ID="ValidationSummary" ForeColor= "Black" CssClass="advance-validation-summary "  DisplayMode="List"  ValidationGroup="vgSlider" runat="server" />
</asp:Content>

<asp:Content ID="SliderForm" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<div class="column">
				<div class="input-box">
					<span><%= Resources.General.Literal_Title %></span>&nbsp;
					<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
					<asp:RequiredFieldValidator
						ID="BlockTitleValidator"
						ControlToValidate="BlockTitleTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"
						Display="None"
						ValidationGroup="vgSlider"
						runat="server" />
					<br />
					<asp:TextBox ID="BlockTitleTextBox" Width="200px" runat="server" />
				</div>
			</div>
			<div class="column">
				<div class="input-box" style="margin-top: 11px;">
					<span>Slide duration</span>&nbsp;
					<asp:RequiredFieldValidator
						ID="SlideDurationValidator"
						ControlToValidate="SlideDurationTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage,SlideDurationEmpty %>"
						Display="None"
						ValidationGroup="vgSlider"
						runat="server" />
					<asp:RangeValidator
						ID="SlideDurationRangeValidator"
						ControlToValidate="SlideDurationTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage,SlideDurationGreaterZero %>"
						Type="Integer"
						MinimumValue="0"
						MaximumValue="2147483647"
						Display="None"
						ValidationGroup="vgSlider"
						runat="server" />
					<br />
					<asp:TextBox ID="SlideDurationTextBox" Width="200px" runat="server" />
				</div>
			</div>

			<br clear="all" />

			<div>
				<uc:Group ID="GroupControl" runat="server" />
			</div>

			<br clear="all" />

			<div>
				<uc:BlockTargetDevice runat="server" Id="TargetDevice"/>
			</div>

			<br clear="all" />

			<div>
				<uc:BlockTimeLimiter runat="server" Id="TimeLimiter"/>
			</div>

			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content">
					<span><%= Resources.General.Literal_ChooseTemplate %></span>
					<br />
					<asp:DropDownList ID="TemplateList" DataTextField="Title" DataValueField="Id" Width="200px" runat="server" />
				</div>
			</div>
		</div>

		<br clear="all" />

		<div class="right">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgSlider" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>"  SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>