﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks
{
	public partial class BlockSliderEdit : LekmerPageController, IEditor
	{
		private IBlockSliderSecureService _blockSliderService;
		protected IBlockSliderSecureService BlockSliderService
		{
			get { return _blockSliderService ?? (_blockSliderService = IoC.Resolve<IBlockSliderSecureService>()); }
		}

		private IBlockTitleTranslationSecureService _blockTitleTranslationService;
		protected IBlockTitleTranslationSecureService BlockTitleTranslationService
		{
			get { return _blockTitleTranslationService ?? (_blockTitleTranslationService = IoC.Resolve<IBlockTitleTranslationSecureService>()); }
		}

		private int _blockId;
		public int BlockId
		{
			get
			{
				if (_blockId <= 0)
				{
					_blockId = Request.QueryString.GetInt32("BlockId");
				}
				return _blockId;
			}
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			PopulateData();

			var master = Master as Pages;
			if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.SiteStructureMessage.BlockSliderEdit);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var block = BlockSliderService.GetById(BlockId);
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockSlider = BlockSliderService.GetById(BlockId);
			if (blockSlider == null)
			{
				throw new BusinessObjectNotExistsException(BlockId);
			}

			int slideDuration;
			if (!int.TryParse(SlideDurationTextBox.Text, out slideDuration))
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.SlideDurationGreaterZero, InfoType.Warning);
			}

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(blockSlider);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			var imageRotatorErrors = GroupControl.Validate();
			if (imageRotatorErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(imageRotatorErrors, InfoType.Warning);
				return;
			}

			blockSlider.Title = BlockTitleTextBox.Text;
			blockSlider.SlideDuration = slideDuration;
			int templateId;
			blockSlider.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			TargetDevice.SetTargetDevices(blockSlider);

			BlockSliderService.Save(SignInHelper.SignedInSystemUser, blockSlider, GroupControl.GetSliderGroups());
			if (blockSlider.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist, InfoType.Failure);
			}
			else
			{
				var translations = Translator.GetTranslations();
				BlockTitleTranslationService.Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockImageRotator, InfoType.Success);

				GroupControl.Refresh();
			}
		}

		protected void PopulateData()
		{
			var blockId = BlockId;
			var block = BlockSliderService.GetById(blockId);
			BlockTitleTextBox.Text = block.Title;
			SlideDurationTextBox.Text = block.SlideDuration.ToString(CultureInfo.InvariantCulture);

			GroupControl.BlockId = blockId;

			var templates = IoC.Resolve<ITemplateSecureService>().GetAllByModel("Slider");
			TemplateList.DataSource = templates;
			TemplateList.DataBind();
			var useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);
			var item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}

			PopulateTranslation(blockId);

			TimeLimiter.Block = block;
			TargetDevice.Block = block;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = BlockTitleTranslationService.GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}