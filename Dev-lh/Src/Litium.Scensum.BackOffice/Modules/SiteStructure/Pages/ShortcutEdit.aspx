<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="ShortcutEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.ShortcutEdit" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" TagName="SiteStructureNodeSelector" TagPrefix="uc" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
			runat="server" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary "
			ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgShortcut" />

</asp:Content>
<asp:Content ID="ShortcutEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">	
	<script type='text/javascript' language='javascript'>
		function ForbidSelection() 
		{
			var roots = $("div#link-destination-tree div[i_id^='-']");
			for (var i = 0; i <= roots.length; i++) {
				$(roots[i]).find('a:first').removeAttr("href");
			}
		}

	</script>
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div class="content-box">
				<div class="column">
					<div class="input-box">
						<span><%= Resources.General.Literal_Title %>&nbsp; *</span>&nbsp
						<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>"
							Display ="None" ValidationGroup="vgShortcut" />
						<br />
						<asp:TextBox ID="TitleTextBox" runat="server" />
					</div>
					<div class="input-box">
						<span><%= Resources.General.Literal_CommonName %></span>&nbsp
						<br />
						<asp:TextBox ID="CommonNameTextBox" runat="server" />
					</div>
					<div id="AccessDiv" runat="server">
						<Scensum:GroupBox ID="AccessGroupBox" runat="server" Text="<%$ Resources:SiteStructure,Literal_Access %>" Width="221">
							<asp:RadioButton ID="AllRadioButton" runat="server" GroupName="Access" Checked="true" /><%= Resources.SiteStructure.Literal_All %>
							<asp:RadioButton ID="SignedInRadioButton" runat="server" GroupName="Access" /><%= Resources.SiteStructure.Literal_SignedIn %>
							<asp:RadioButton ID="NotSignedInRadioButton" runat="server" GroupName="Access" /><%= Resources.SiteStructure.Literal_NotSignedIn %>
						</Scensum:GroupBox>
					</div>
					<div class="link-settings-separator">
						&nbsp;</div>
					<div id="StatusDiv" runat="server">
						<Scensum:GroupBox ID="StatusGroupBox" runat="server" Text="<%$ Resources:General,Literal_Status %>" Width="149">
							<asp:RadioButton ID="OnLineRadioButton" CssClass="padding-px" runat="server" GroupName="Status" /><%= Resources.SiteStructure.Literal_Online %> 
							<asp:RadioButton ID="OffLineRadioButton" runat="server" GroupName="Status" Checked="true" /><%= Resources.SiteStructure.Literal_Offline %>
						</Scensum:GroupBox>
					</div>
				</div>
				<br class="clear" />
			</div>
			<br />
			<br />
			<div>
				<asp:UpdatePanel runat="server" ID="DestinationUpdatePanel" UpdateMode="Conditional">
			    <ContentTemplate>
				    <div class=" left  link-destination">
					    <label>
						    <%= Resources.SiteStructure.Literal_Destination %></label><asp:Label runat="server" ID="DestinationasterixLabel" ForeColor="Red"
							    Visible="false">&nbsp*</asp:Label> 
						<div id="link-placement-tree" class="site-structure-link-placement-tree">						
							<uc:SiteStructureNodeSelector ID="DistinationTreeViewControl" runat="server" />						
						</div>
					  
				    </div>
				    <asp:Panel ID="PlacementPanel" CssClass="right" runat="server" Style="margin-top: -6px;">
					    <label>
						    <%= Resources.SiteStructure.Literal_SelectPlacement %></label><asp:Label runat="server" ID="PlacementasterixLabel" ForeColor="Red" Visible="false">&nbsp*</asp:Label>
					    	<div id="link-placement-tree" class=" site-structure-link-placement-tree">						
							<uc:SiteStructureNodeSelector ID="ParentTreeViewControl" runat="server" AllowClearSelection="false"/>						
						</div>
				    </asp:Panel>
				</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
		<br class="clear" />
		<br />
		<div class="buttons left">
                    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmationContentNode();" />
                </div>
		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>"
				ValidationGroup="vgShortcut" SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				CausesValidation="false" SkinID="DefaultButton" />
		</div>
		
	</asp:Panel>
</asp:Content>
