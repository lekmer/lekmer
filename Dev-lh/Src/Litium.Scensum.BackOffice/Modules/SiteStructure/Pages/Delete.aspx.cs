using System;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.DeleteValidator;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class Delete : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			DeletePageButton.Click += DeleteParentOrTemplatePage;
		}

		protected override void PopulateForm()
		{
			if (!ValidateContentNode())
			{
				return;
			}
			if (IsParentOrTemplatePage())
			{
				return;
			}
			DeletePage();
		}

		private bool ValidateContentNode()
		{
			var contentNodeDeleteValidator = new ContentNodeDeleteValidator(GetId());
			contentNodeDeleteValidator
				.HasReferencingShortcutValidate(ResolveUrl)
				.HasReferencingBlockCheckoutValidate()
				.HasReferencingOrderModuleChannelValidate()
				.HasReferencingProductModuleChannelValidate()
				.IsStartPageValidate()
				.IsMasterWithSubPagesValidate(ResolveUrl);
			PopulateMessages(contentNodeDeleteValidator);
			return contentNodeDeleteValidator.IsValid;
		}

		private void PopulateMessages(ContentNodeDeleteValidator deleteValidator)
		{
			if (!deleteValidator.IsValid)
			{
				errors.Add(Resources.SiteStructureMessage.NotNodeDelete);
				errors.AddRange(deleteValidator.Messages, false);
			}
		}

		private void Redirect(IContentNode node)
		{
			var master = Master as Pages;
			if (master == null)
			{
				return;
			}
			int? parentId = node.ParentContentNodeId;
			master.PopulateTreeView(parentId);
			if (parentId == null)
			{
				Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
				return;
			}

			IContentNode nodeSelected = IoC.Resolve<IContentNodeSecureService>().GetById((int)parentId);
			if (nodeSelected == null)
			{
				Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
				return;
			}
			switch (nodeSelected.ContentNodeTypeId)
			{
				case (int)ContentNodeTypeInfo.Folder:
					Response.Redirect(PathHelper.SiteStructure.Page.GetFolderEditUrl(nodeSelected.Id));
					break;
				case (int)ContentNodeTypeInfo.Page:
					Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(nodeSelected.Id));
					break;
				case (int)ContentNodeTypeInfo.ShortcutLink:
					Response.Redirect(PathHelper.SiteStructure.Page.GetShortcutEditUrl(nodeSelected.Id));
					break;
				case (int)ContentNodeTypeInfo.UrlLink:
					Response.Redirect(PathHelper.SiteStructure.Page.GetLinkEditUrl(nodeSelected.Id));
					break;
			}
		}

		protected virtual bool IsParentOrTemplatePage()
		{
			var productSiteStructureRegistryService = IoC.Resolve<IProductSiteStructureRegistrySecureService>();
			var productList = productSiteStructureRegistryService.GetAllByReferentContentNode(GetId());
			var categorySiteStructureRegistryService = IoC.Resolve<ICategorySiteStructureRegistrySecureService>();
			var categoryList = categorySiteStructureRegistryService.GetAllByReferentContentNode(GetId());
			if (productList.Count == 0 && categoryList.Count == 0) return false;
			var message = Resources.SiteStructureMessage.IsParentOrTemplatePage;
			if (productList.Count > 0)
			{
				var productTitles = ContentNodeDeleteHelper.GetTitles(productList, GetReferencingProductTitleCallback);
				message = message + Resources.SiteStructureMessage.IsParentOrTemplatePageProducts + productTitles;
			}
			if (categoryList.Count > 0)
			{
				var categoryTitles = ContentNodeDeleteHelper.GetTitles(categoryList, GetReferencingCategoryTitleCallback);
				message = message + Resources.SiteStructureMessage.IsParentOrTemplatePageCategories + categoryTitles;
			}
			message = message + Resources.SiteStructureMessage.IsParentOrTemplatePageConfirmation;
			ClientScript.RegisterStartupScript(typeof(string), Guid.NewGuid().ToString(), "$(function(){ConfirmParentPageDelete(\"" + message + "\");});", true);
			return true;
		}

		protected virtual string GetReferencingProductTitleCallback(IProductSiteStructureRegistry productSiteStructureRegistry)
		{
			IProduct product = IoC.Resolve<IProductSecureService>().GetById(ChannelHelper.CurrentChannel.Id, productSiteStructureRegistry.ProductId);
			return product.DisplayTitle;
		}
		protected virtual string GetReferencingCategoryTitleCallback(ICategorySiteStructureRegistry categorySiteStructureRegistry)
		{
			ICategory category = IoC.Resolve<ICategorySecureService>().GetById(categorySiteStructureRegistry.CategoryId);
			return category.Title;
		}

		protected virtual void DeletePage()
		{
			IContentNode node = IoC.Resolve<IContentNodeSecureService>().GetById(GetId());
			var service = IoC.Resolve<IContentNodeDeleteSecureService>(node.ContentNodeType.CommonName);
			service.Delete(SignInHelper.SignedInSystemUser, node.Id);
			Redirect(node);
		}

		protected void DeleteParentOrTemplatePage(object sender, EventArgs e)
		{
			var productSiteStructureRegistryService = IoC.Resolve<IProductSiteStructureRegistrySecureService>();
			productSiteStructureRegistryService.Delete(SignInHelper.SignedInSystemUser, GetId());
			var categorySiteStructureRegistryService = IoC.Resolve<ICategorySiteStructureRegistrySecureService>();
			categorySiteStructureRegistryService.Delete(SignInHelper.SignedInSystemUser, GetId());
            DeletePage();
		}
	}
}
