using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.Modules.General.Channels
{
	public partial class Default : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			ChannelGrid.RowCommand += ChannelGridRowCommand;
		}
		protected override void PopulateForm()
		{
			PopulateChannelGrid();
		}
		
		/// <summary>
		/// Edit or Remove command from table of Channels.
		/// </summary>
		protected virtual void ChannelGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case "Edit":
					{
						Response.Redirect(PathHelper.Channel.GetEditUrl(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture)));
						break;
					}
			}
		}

		protected void PopulateChannelGrid()
		{
			var channelSecureService = IoC.Resolve<IChannelSecureService>();
			ChannelGrid.DataSource = channelSecureService.GetAll();
			ChannelGrid.DataBind();

			if ((ChannelGrid.Rows.Count > 0)) return;

			ChannelGrid.Visible = false;
			SystemMessageContainer.Add("There are not Channels in DataBase");
		}
	}
}