<%@ Page 
	Language="C#" 
	MasterPageFile="~/Master/Main.Master" 
	CodeBehind="Default.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.General.Channels.Default" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="ChannelsTabContent" ContentPlaceHolderID="body" runat="server">
	<div style="float: left; width:984px;">
	   <uc:MessageContainer ID="SystemMessageContainer"  MessageType="Failure"  runat="server" />
	   </div>
	<div id="channel-list">
		<div>
			<Scensum:Grid runat="server" ID="ChannelGridExtension" FixedColumns="Channel">
				<asp:GridView 
					ID="ChannelGrid" 
					SkinID="grid"
					runat="server" 
					AutoGenerateColumns="false"
					Width="100%" >
					<Columns>
						<asp:TemplateField HeaderText="Channel" ItemStyle-Width="25%">
							<ItemTemplate>
								<asp:LinkButton ID="EditButton" CommandName="Edit" CommandArgument='<%#Eval("Id") %>' Text='<%#Eval("Title")%>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Country" ItemStyle-Width="15%">
							<ItemTemplate>
								<uc:LiteralEncoded ID="CountryTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Country.Title") %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Currency" ItemStyle-Width="15%">
							<ItemTemplate>
								<uc:LiteralEncoded ID="CurrencyTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Currency.ISO") %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Language" ItemStyle-Width="15%">
							<ItemTemplate>
								<uc:LiteralEncoded ID="LanguageTitle" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Language.Title") %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField HeaderText="URL" DataField="ApplicationName" ItemStyle-Width="30%" />
					</Columns>
				</asp:GridView>
			</Scensum:Grid>
		</div>
	</div>
</asp:Content>