using System;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Security.Roles
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
	public partial class Role : MasterPage
	{
		protected override void OnLoad(EventArgs e)
		{
			CreateRoleButton.Click += CreateRoleButtonClick;
			SearchButton.Click += SearchButtonClick;
			base.OnLoad(e);
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!IsPostBack)
			{
				RestoreSearchFields();
			}
		}

		private void SearchButtonClick(object sender, EventArgs e)
		{
			SearchCriteriaState<string>.Instance.Save(PathHelper.Role.GetDefaultUrl(), SearchTextBox.Text);
			Response.Redirect(PathHelper.Role.GetDefaultUrl());
		}

		private void CreateRoleButtonClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Role.GetEditUrl());
		}

		private void RestoreSearchFields()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Role.GetDefaultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}
	}
}