﻿<%@ Page Language="C#" MasterPageFile="RatingGroups.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="DefaultContent" runat="server" ContentPlaceHolderID="RatingGroupPlaceHolder">
	<script type="text/javascript">
		function confirmDelete() {
			return DeleteConfirmation("<%= Resources.RatingReview.Literal_RatingGroup %>");
		}
	</script>

	<div class="product-campaigns">
		<sc:GridViewWithCustomPager 
			ID="RatingGroupsGrid"
			SkinID="grid"
			runat="server"
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			Width="100%">

			<Columns>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<HeaderTemplate>
						<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
					</HeaderTemplate>
					<ItemTemplate>
						<asp:CheckBox ID="SelectCheckBox" runat="server" />
						<asp:HiddenField ID="IdHiddenField" Value='<%# Eval("Id") %>' runat="server" />
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
					<ItemTemplate>
						<uc:LiteralEncoded ID="CommonNameLiteral" runat="server" Text='<%# Eval("CommonName") %>' />
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField HeaderText="Path">
					<ItemTemplate>
						<asp:Label ID="PathLabel" runat="server" Text='<%# IsSearchResult ? GetPath(Eval("RatingGroupFolderId")) : string.Empty %>' />
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<ItemTemplate>
						<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteRatingGroup" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
</asp:Content>