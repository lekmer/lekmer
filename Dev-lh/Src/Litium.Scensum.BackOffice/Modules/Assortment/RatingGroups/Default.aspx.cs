﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups
{
	public partial class Default : LekmerPageController
	{
		private Collection<IRatingGroupFolder> _allFolders;

		public virtual RatingGroupsMaster MasterPage
		{
			get
			{
				return (RatingGroupsMaster)Master;
			}
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected override void SetEventHandlers()
		{
			RatingGroupsGrid.RowCommand += OnRowCommand;
			RatingGroupsGrid.RowDataBound += OnRowDataBound;
			RatingGroupsGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateRatingGroups();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteRatingGroup":
					DeleteRating(id);
					SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
					break;
			}

			PopulateRatingGroups();
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGroupsGrid.PageIndex = e.NewPageIndex;

			PopulateRatingGroups();
		}

		protected virtual void DeleteRating(int ratingId)
		{
			IoC.Resolve<IRatingGroupSecureService>().Delete(SignInHelper.SignedInSystemUser, ratingId);
		}

		protected virtual void PopulateRatingGroups()
		{
			if (IsSearchResult)
			{
				RatingGroupsGrid.DataSource = SearchRatingGroups();
				RatingGroupsGrid.Columns[3].Visible = true;
			}
			else
			{
				RatingGroupsGrid.DataSource = RetrieveRatingGroups();
				RatingGroupsGrid.Columns[3].Visible = false;
			}

			RatingGroupsGrid.DataBind();

			RatingGroupsGrid.Visible = RatingGroupsGrid.DataSource != null;
		}

		protected virtual string GetEditUrl(object ratingGroupId)
		{
			return LekmerPathHelper.RatingGroup.GetEditUrl(System.Convert.ToInt32(ratingGroupId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<IRatingGroup> RetrieveRatingGroups()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return IoC.Resolve<IRatingGroupSecureService>().GetAllByFolder(folderId.Value);
			}

			return null;
		}

		protected virtual Collection<IRatingGroup> SearchRatingGroups()
		{
			var searchUrl = LekmerPathHelper.RatingGroup.GetSearchResultUrl();

			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);

			if (string.IsNullOrEmpty(searchCriteria))
			{
				return new Collection<IRatingGroup>();
			}

			return IoC.Resolve<IRatingGroupSecureService>().Search(searchCriteria);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<IRatingGroupFolderSecureService>();
				_allFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");

				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");

				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');");
			}
		}
	}
}