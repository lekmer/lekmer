﻿using System;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups
{
	public partial class RatingGroupFolderEdit : LekmerPageController, IEditor
	{
		protected virtual RatingGroupsMaster MasterPage
		{
			get
			{
				return (RatingGroupsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();
			if (id.HasValue)
			{
				var folder = IoC.Resolve<IRatingGroupFolderSecureService>().GetById(id.Value);
				RatingGroupFolderTitleTextBox.Text = folder.Title;

				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_EditRatingGroupFolder);

				PopulateTreeView(null);
			}
			else
			{
				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_CreateRatingGroupFolder);

				DestinationDiv.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.RatingReview.RatingGroupFolderSaveSuccess);
				}
			}

			System.Web.UI.ScriptManager.RegisterStartupScript(
				this,
				GetType(),
				string.Format(CultureInfo.CurrentCulture, "root menu {0}", FolderTree.ClientID),
				string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", FolderTree.ClientID),
				true);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			int? id = GetIdOrNull();
			int? parentId = Request.QueryString.GetInt32OrNull("parentFolderId");

			var ratingGroupFolderSecureService = IoC.Resolve<IRatingGroupFolderSecureService>();

			var ratingGroupFolder = id.HasValue ? ratingGroupFolderSecureService.GetById(id.Value) : IoC.Resolve<IRatingGroupFolder>();
			ratingGroupFolder.Title = RatingGroupFolderTitleTextBox.Text;

			if (id == null)
			{
				ratingGroupFolder.ParentRatingGroupFolderId = parentId != MasterPage.RootNodeId ? parentId : null;
			}
			else if (FolderTree.SelectedNodeId.HasValue)
			{
				ratingGroupFolder.ParentRatingGroupFolderId = FolderTree.SelectedNodeId.Value == FolderTree.RootNodeId ? null : FolderTree.SelectedNodeId;
			}

			ratingGroupFolder = ratingGroupFolderSecureService.Save(SignInHelper.SignedInSystemUser, ratingGroupFolder);

			if (ratingGroupFolder.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.FolderTitleExist);
			}
			else
			{
				MasterPage.PopulateTree(null);
				MasterPage.SelectedFolderId = ratingGroupFolder.Id;

				Response.Redirect(LekmerPathHelper.RatingGroupFolder.GetEditUrlWithMessage(ratingGroupFolder.Id));
			}
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}

		protected void PopulateTreeView(int? folderId)
		{
			var ratingGroupFolders = MasterPage.GetNodes(folderId ?? MasterPage.SelectedFolderId);

			if (ratingGroupFolders == null || ratingGroupFolders.Count <= 0)
			{
				return;
			}

			var editedRatingGroupFolder = ratingGroupFolders.FirstOrDefault(f => f.Id == MasterPage.SelectedFolderId);

			FolderTree.DataSource = editedRatingGroupFolder != null
				? FolderTree.GetFilteredSource(ratingGroupFolders, editedRatingGroupFolder.Id)
				: ratingGroupFolders;
			FolderTree.RootNodeTitle = Resources.RatingReview.Literal_RatingGroupFolders;
			FolderTree.DataBind();

			FolderTree.SelectedNodeId = folderId ?? (editedRatingGroupFolder != null ? editedRatingGroupFolder.ParentId : FolderTree.RootNodeId) ?? FolderTree.RootNodeId;
		}
	}
}