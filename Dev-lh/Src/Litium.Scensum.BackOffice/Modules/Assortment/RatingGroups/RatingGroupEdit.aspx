﻿<%@ Page Language="C#" MasterPageFile="RatingGroups.Master" CodeBehind="RatingGroupEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups.RatingGroupEdit" AutoEventWireup="true"%>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="RatingSelector" Src="~/UserControls/Assortment/RatingSelector.ascx" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="RatingGroupEditContent" runat="server" ContentPlaceHolderID="RatingGroupPlaceHolder">
	<script type="text/javascript">
		function confirmRatingDelete() {
			return DeleteConfirmation("<%= Resources.RatingReview.Literal_Rating %>");
		}
	</script>

	<div>
		<div class="rating-groups">
			<div class="column">
				<div class="input-box">
					<span><%=Resources.General.Literal_Title%> *</span>
					<br />
					<asp:TextBox ID="TitleTextBox" runat="server" />
				</div>

				<div class="input-box">
					<span><%=Resources.General.Literal_CommonName%> *</span>
					<br />
					<asp:TextBox ID="CommonNameTextBox" runat="server" />
				</div>
			</div>
			
			<div class="column">
				<div class="input-box">
					<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
					<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
				</div>
			</div>
		</div>
		
		<div class="rating-groups">
			<div class="column">
				<div class="input-box">
					<span><%=Resources.General.Literal_Description%></span>
					<br />
					<asp:TextBox ID="DescriptionTextBox" runat="server" TextMode="MultiLine" Rows="5" />
				</div>
			</div>
		</div>
	</div>

	<br class="clear" />

	<div style="margin-top: 10px;">
		<span class="bold"><%=Resources.RatingReview.Literal_Ratings%></span>
		<br class="clear" />
		<asp:UpdatePanel ID="RatingsUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div id="sort-content">
					<sc:GridViewWithCustomPager 
						ID="RatingsGrid"
						SkinID="grid"
						runat="server"
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>"
						Width="100%">

						<Columns>
							<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<HeaderTemplate>
									<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
								</HeaderTemplate>
								<ItemTemplate>
									<asp:CheckBox ID="SelectCheckBox" runat="server" />
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
								<ItemTemplate>
									<uc:HyperLinkEncoded ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
								<ItemTemplate>
									<uc:LiteralEncoded ID="CommonNameLiteral" runat="server" Text='<%# Eval("CommonName") %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Path">
								<ItemTemplate>
									<asp:Label ID="PathLabel" runat="server" Text='<%# GetPath(Eval("RatingFolderId")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="11%">
								<HeaderTemplate>
									<div class="inline">
										<%= Resources.General.Literal_SortOrder %>
										<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" CommandArgument="0" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
									</div>
								</HeaderTemplate>
								<ItemTemplate>
									<div class="inline">
										<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
										<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
										<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%# Eval("Ordinal") %>' />
										<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
										<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
									</div>
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteRating" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmRatingDelete();" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>

					<div id="AllSelectedDivRating" runat="server" style="display: none;" class="apply-to-all-selected-2">
						<uc:ImageLinkButton runat="server" ID="RemoveRatingsButton"
							Text="<%$ Resources:General, Button_RemoveSelected %>" SkinID="DefaultButton" OnClientClick="return ConfirmRatingDelete();" />
					</div>

					<br />

					<div class="right">
						<uc:RatingSelector ID="RatingSelector" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>

	<br class="clear" />

	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
	</div>
</asp:Content>