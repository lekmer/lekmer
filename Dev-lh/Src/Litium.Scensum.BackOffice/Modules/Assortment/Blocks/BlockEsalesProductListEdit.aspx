﻿<%@ Page
	Language="C#"
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockEsalesProductListEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockEsalesProductListEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockTimeLimiter" Src="~/UserControls/SiteStructure/BlockTimeLimiter.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockTargetDevice" Src="~/UserControls/SiteStructure/BlockTargetDevice.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockEsalesSetting" Src="~/UserControls/SiteStructure/BlockEsalesSetting.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValdationSummary" DisplayMode="List" ValidationGroup="vgBlockEsalesProductList" />
</asp:Content>

<asp:Content ID="BlockEsalesProductListContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="block-assortment">
			<div class="input-box">
				<span>
					<asp:Literal ID="TitleLiteral" runat="server" Text="<%$ Resources:General,Literal_Title%>" /></span>&nbsp;
					<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
					<asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" Display="None" ValidationGroup="vgBlockEsalesProductList" />
				<br />
				<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" />
			</div>

			<br clear="all" />
			<br />

			<uc:BlockEsalesSetting runat="server" Id="BlockEsalesSetting" ValidationGroup="vgBlockEsalesProductList" />

			<div id="Settings" runat="server" class="content-box">
				<label class="block-header">
					<asp:Literal runat="server" Text="<%$ Resources:General,Label_Settings%>" />
				</label>
				<br class="clear" />
				<div class="column">
					<div class="input-box">
						<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate%>" /></span><br />
						<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
					</div>
				</div>
				<br class="clear" />
				<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgBlockEsalesProductList" />
				<br class="clear" />
			</div>

			<br class="clear"/>

			<div>
				<uc:BlockTimeLimiter runat="server" Id="TimeLimiter"/>
			</div>

			<br class="clear"/>

			<div>
				<uc:BlockTargetDevice runat="server" Id="TargetDevice"/>
			</div>
		</div>

		<br class="clear"/>

		<div class="right">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>"
				SkinID="DefaultButton" ValidationGroup="vgBlockEsalesProductList" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>"
				SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>
