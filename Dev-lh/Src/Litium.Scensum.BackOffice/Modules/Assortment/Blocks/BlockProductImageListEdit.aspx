﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" AutoEventWireup="true" CodeBehind="BlockProductImageListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductImageListEdit" %>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Warning"  HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValdationSummary" DisplayMode="List"  ValidationGroup="vgCount" />
</asp:Content>

<asp:Content ID="cBPIL" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<script type='text/javascript' language='javascript' >
		function SelectRadioButton(rbId, gridId) {
			var grid = document.getElementById(gridId);
			var gridRadioButtons = grid.getElementsByTagName('input');
			for (var i = 0; i < gridRadioButtons.length; i++) {
				var rb = gridRadioButtons[i];
				if (rb.type === "radio") {
					rb.checked = false;
				}
			}
			var selectedRadioButton = document.getElementById(rbId);
			selectedRadioButton.checked = true;
		}
	</script>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_Title%>"/></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />
			<asp:RequiredFieldValidator runat="server" 
				ID="BlockTitleValidator" 
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" 
				Display ="None" ValidationGroup="vgCount" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br /><br />
			<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_ImageGroup%>"/></span>
			<div class="input-box">
				<asp:GridView
					ID="ImageGroupGrid" 
					SkinID="grid" 
					runat="server" 
					Width="99%"
					AutoGenerateColumns="false">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
							<ItemTemplate>
								<asp:RadioButton ID="SelectGroupRadioButton" runat="server" GroupName="vgSelect" />
								<asp:HiddenField ID="GroupIdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>									
						<asp:BoundField HeaderText="<%$ Resources:Product,Literal_GroupeName%>" DataField="Title" ItemStyle-Width="95%" />
					</Columns>
				</asp:GridView>
			</div>
			<br />
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
						<span><asp:Literal  runat="server" Text= "<%$ Resources:General,Literal_ChooseTemplate%>" /></span><br />
						<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" /><br />
						</div>
					</div>
					<br class="clear" />
					<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgCount" />
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="vgCount" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>
