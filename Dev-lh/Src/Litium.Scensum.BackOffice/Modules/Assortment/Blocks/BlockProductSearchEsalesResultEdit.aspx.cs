using System;
using System.Collections.ObjectModel;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductSearchEsalesResultEdit : LekmerPageController, IEditor
	{
		private const string EmptyItemValue = "0";

		private IBlockProductSearchEsalesResultSecureService _blockService;
		private IBlockTitleTranslationSecureService _blockTitleTranslationService;

		protected IBlockProductSearchEsalesResultSecureService BlockService
		{
			get { return _blockService ?? (_blockService = IoC.Resolve<IBlockProductSearchEsalesResultSecureService>()); }
		}
		protected IBlockTitleTranslationSecureService BlockTitleTranslationService
		{
			get { return _blockTitleTranslationService ?? (_blockTitleTranslationService = IoC.Resolve<IBlockTitleTranslationSecureService>()); }
		}

		private string _blockType;
		protected string BlockType
		{
			get { return _blockType ?? (_blockType = BlockService.GetById(GetBlockId()).BlockType.CommonName); }
			set { _blockType = value; }
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				if (BlockType == "ProductSearchEsalesResult")
				{
					master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductSearchEsalesResultEdit);
				}
				else if (BlockType == "ProductSearchEsalesResultV2")
				{
					master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductSearchEsalesResultV2Edit);
				}
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			IBlockProductSearchEsalesResult blockProductSearchEsalesResult = BlockService.GetById(GetBlockId());

			BlockType = blockProductSearchEsalesResult.BlockType.CommonName;

			BlockTitleTextBox.Text = blockProductSearchEsalesResult.Title;

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			var templates = new Collection<ITemplate>();
			if (BlockType == "ProductSearchEsalesResult")
			{
				templates = templateSecureService.GetAllByModel("BlockProductSearchEsales");
			}
			else if (BlockType == "ProductSearchEsalesResultV2")
			{
				templates = templateSecureService.GetAllByModel("BlockProductSearchEsalesV2");
			}

			var primaryDefaultItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			primaryDefaultItem.Attributes.Add("class", "use-theme");

			PopulateTemplateList(TemplateList, templates, primaryDefaultItem, blockProductSearchEsalesResult.TemplateId);
			PopulateTemplateList(SecondaryTemplateList, templates, new ListItem(string.Empty, EmptyItemValue), blockProductSearchEsalesResult.SecondaryTemplateId);

			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockProductSearchEsalesResult blockProductSearchEsalesResult = BlockService.GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductSearchEsalesResult.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockProductSearchEsalesResult blockProductSearchEsalesResult = BlockService.GetById(GetBlockId());
			if (blockProductSearchEsalesResult == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			blockProductSearchEsalesResult.TemplateId = GetDropDownListValue(TemplateList);
			blockProductSearchEsalesResult.SecondaryTemplateId = GetDropDownListValue(SecondaryTemplateList);
			blockProductSearchEsalesResult.Title = BlockTitleTextBox.Text;

			BlockService.Save(SignInHelper.SignedInSystemUser, blockProductSearchEsalesResult);
			if (blockProductSearchEsalesResult.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				BlockTitleTranslationService.Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductSearchEsalesResult);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTemplateList(DropDownList dropDownList, object templates, ListItem defaultItem, int? selectedId)
		{
			dropDownList.DataSource = templates;
			dropDownList.DataBind();
			dropDownList.Items.Insert(0, defaultItem);

			if (!selectedId.HasValue)
			{
				defaultItem.Selected = true;
			}
			else
			{
				var selectedItem = dropDownList.Items.FindByValue(selectedId.ToString());
				if (selectedItem != null)
				{
					selectedItem.Selected = true;
				}
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = BlockTitleTranslationService.GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected virtual int? GetDropDownListValue(DropDownList dropDownList)
		{
			var selectedValue = dropDownList.SelectedValue;
			if (selectedValue == EmptyItemValue)
			{
				return null;
			}

			int value;
			if (int.TryParse(dropDownList.SelectedValue, out value))
			{
				return value;
			}

			return null;
		}
	}
}