﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductLatestFeedbackListEdit : LekmerPageController, IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.RatingReview.Literal_BlockProductLatestFeedbackListEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			int blockId = GetBlockId();
			var block = IoC.Resolve<IBlockProductLatestFeedbackListSecureService>().GetById(blockId);

			BlockTitleTextBox.Text = block.Title;
			NumberOfItemsTextBox.Text = block.NumberOfItems.ToString(CultureInfo.InvariantCulture);
			PopulateTemplateList(block);
			PopulateTranslation(blockId);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var block = IoC.Resolve<IBlockProductLatestFeedbackListSecureService>().GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductLatestFeedbackListSecureService>();

			int blockId = GetBlockId();
			var block = blockService.GetById(blockId);
			if (block == null)
			{
				throw new BusinessObjectNotExistsException(blockId);
			}

			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			block.Title = BlockTitleTextBox.Text;
			block.NumberOfItems = int.Parse(NumberOfItemsTextBox.Text, CultureInfo.CurrentCulture);

			blockService.Save(SignInHelper.SignedInSystemUser, block);
			if (block.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockProductLatestFeedbackListSavedSuccessfully, InfoType.Success);
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTemplateList(IBlockProductLatestFeedbackList block)
		{
			TemplateList.DataSource = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockProductLatestFeedbackList");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");

			TemplateList.Items.Insert(0, listItem);

			var item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}