﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductMostHelpfulRatingEdit : LekmerStatePageController<ProductMostHelpfulRatingState>, IEditor
	{
		private Collection<IRatingFolder> _allRatingFolders;

		private Collection<IBlockRatingItem> _blockRatingItems = new Collection<IBlockRatingItem>();

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.RatingReview.Literal_BlockProductMostHelpfulRatingEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			RatingGrid.RowCommand += OnRowCommand;
			RatingGrid.PageIndexChanging += OnPageIndexChanging;
			RatingSelector.SelectEvent += OnRatingsAdd;

			RatingItemsGrid.RowDataBound += OnRatingItemsRowDataBound;
			RatingItemsGrid.PageIndexChanging += OnRatingGroupsPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			int blockId = GetBlockId();

			var blockProductMostHelpfulRating = IoC.Resolve<IBlockProductMostHelpfulRatingSecureService>().GetById(blockId);
			BlockTitleTextBox.Text = blockProductMostHelpfulRating.Title;

			TemplateList.DataSource = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockProductMostHelpfulRating");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");

			TemplateList.Items.Insert(0, listItem);

			var item = TemplateList.Items.FindByValue(blockProductMostHelpfulRating.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}

			PopulateTranslation(blockId);

			InitializeState();

			var rating = PopulateRating();

			PopulateRatingItems(rating);
		}
		
		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockProductMostHelpfulRating = IoC.Resolve<IBlockProductMostHelpfulRatingSecureService>().GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductMostHelpfulRating.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductMostHelpfulRatingSecureService>();

			var blockProductMostHelpfulRating = blockService.GetById(GetBlockId());
			if (blockProductMostHelpfulRating == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			var rating = GetRating();
			var ratingItems = GetRatingItems();

			if (rating != null && ratingItems.Count == 0)
			{
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockProductMostHelpfulRating_SelectItems);
				return;
			}

			int templateId;
			blockProductMostHelpfulRating.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductMostHelpfulRating.Title = BlockTitleTextBox.Text;
			blockProductMostHelpfulRating.BlockRating = rating;
			blockProductMostHelpfulRating.BlockRatingItems = ratingItems;

			blockService.Save(SignInHelper.SignedInSystemUser, blockProductMostHelpfulRating);

			if (blockProductMostHelpfulRating.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockProductMostHelpfulRatingSavedSuccessfully, InfoType.Success);
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		// Ratings grid.

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteRating":
					var ratingToDelate = State.RatingList.FirstOrDefault(r => r.Id == id);
					if (ratingToDelate != null)
					{
						State.RatingList.Remove(ratingToDelate);
						PopulateRatingGrid();

						_blockRatingItems.Clear();
						State.RatingItemList.Clear();
						PopulateRatingItemsGrid();
					}
					break;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGrid.PageIndex = e.NewPageIndex;

			PopulateRatingGrid();
		}

		protected virtual void OnRatingsAdd(object sender, UserControls.Assortment.Events.RatingSelectEventArgs e)
		{
			var rating = e.Ratings[0];

			if (State.RatingList.FirstOrDefault(r => r.Id == rating.Id) == null)
			{
				State.RatingList.Clear();
				State.RatingList.Add(rating);
				PopulateRatingGrid();

				_blockRatingItems.Clear();
				State.RatingItemList.Clear();
				State.RatingItemList.AddRange(IoC.Resolve<IRatingItemSecureService>().GetAllByRating(rating.Id));
				PopulateRatingItemsGrid();
			}
		}

		protected virtual IRating PopulateRating()
		{
			if (State.RatingList == null)
			{
				State.RatingList = new List<IRating>();
			}

			var blockRatings = IoC.Resolve<IBlockRatingSecureService>().GetAllByBlock(GetBlockId());
			if (blockRatings != null && blockRatings.Count > 0)
			{
				IRating rating = IoC.Resolve<IRatingSecureService>().GetById(blockRatings[0].RatingId);
				State.RatingList.Add(rating);
			}

			PopulateRatingGrid();

			return State.RatingList.Count > 0 ? State.RatingList[0] : null;
		}

		protected virtual void PopulateRatingGrid()
		{
			RatingGrid.DataSource = State.RatingList;
			RatingGrid.DataBind();

			RatingUpdatePanel.Update();
		}

		protected virtual string GetRatingEditUrl(object ratingId)
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				int blockId = GetBlockId();

				return LekmerPathHelper.Rating.GetEditUrlForBlockProductMostHelpfulRatingEdit(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture), id.Value, blockId);
			}

			return LekmerPathHelper.Rating.GetEditUrl(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture));
		}

		protected virtual string GetRatingPath(object folderId)
		{
			if (_allRatingFolders == null)
			{
				var service = IoC.Resolve<IRatingFolderSecureService>();
				_allRatingFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allRatingFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual IBlockRating GetRating()
		{
			IBlockRating blockRating = null;

			if(State.RatingList.Count > 0)
			{
				blockRating = IoC.Resolve<IBlockRating>();
				blockRating.BlockId = GetBlockId();
				blockRating.RatingId = State.RatingList[0].Id;
			}

			return blockRating;
		}


		// Rating groups grid.

		protected virtual void OnRatingItemsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.DataRow)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				var idHiddenField = (HiddenField) row.FindControl("IdHiddenField");

				if (selectCheckBox != null && idHiddenField != null)
				{
					if (_blockRatingItems.Count > 0 
						&& _blockRatingItems.FirstOrDefault(bri => bri.RatingItemId == System.Convert.ToInt32(idHiddenField.Value)) != null)
					{
						selectCheckBox.Checked = true;
					}
				}
			}
		}

		protected virtual void OnRatingGroupsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingItemsGrid.PageIndex = e.NewPageIndex;
			PopulateRatingItemsGrid();
		}

		protected virtual void PopulateRatingItems(IRating rating)
		{
			if (State.RatingItemList == null)
			{
				State.RatingItemList = new List<IRatingItem>();
			}

			if (rating != null)
			{
				State.RatingItemList.AddRange(rating.RatingItems);
			}

			_blockRatingItems = IoC.Resolve<IBlockRatingItemSecureService>().GetAllByBlock(GetBlockId());

			PopulateRatingItemsGrid();
		}

		protected virtual void PopulateRatingItemsGrid()
		{
			RatingItemsGrid.DataSource = State.RatingItemList;
			RatingItemsGrid.DataBind();

			RatingItemsUpdatePanel.Update();
		}

		protected virtual Collection<IBlockRatingItem> GetRatingItems()
		{
			Collection<IBlockRatingItem> ratingItems = new Collection<IBlockRatingItem>();

			var blockId = GetBlockId();

			var selectedItems = RatingItemsGrid.GetSelectedIds();
			foreach (var selectedItem in selectedItems)
			{
				var blockRatingItem = IoC.Resolve<IBlockRatingItem>();
				blockRatingItem.RatingItemId = selectedItem;
				blockRatingItem.BlockId = blockId;

				ratingItems.Add(blockRatingItem);
			}

			return ratingItems;
		}

		protected virtual void InitializeState()
		{
			if (State == null)
			{
				State = new ProductMostHelpfulRatingState
				{
					RatingList = new List<IRating>(),
					RatingItemList = new List<IRatingItem>()
				};
			}
		}
	}

	[Serializable]
	public sealed class ProductMostHelpfulRatingState
	{
		public List<IRating> RatingList { get; set; }
		public List<IRatingItem> RatingItemList { get; set; }
	}
}