﻿<%@ Page Language="C#" 
	AutoEventWireup="true" 
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" 
	CodeBehind="BlockEsalesRecommendV2Edit.aspx.cs" 
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockEsalesRecommendV2Edit" 
%>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgBlockEsalesRecommendV2" />
</asp:Content>

<asp:Content ID="BlockEsalesRecommendV2" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box esales-recommend">
			<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_Title %>"/></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator 
				runat="server" 
				ID="BlockTitleValidator"
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"
				Display ="None" ValidationGroup="vgBlockEsalesRecommendV2" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" />
			<br />
			<div class="input-box full-width">
				<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_EsalesSubPanelName %>" /></span>
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="EsalesSubPanelNameValidator"
					ControlToValidate="EsalesSubPanelNameTextBox" 
					ErrorMessage="<%$ Resources:LekmerMessage,EsalesSubPanelNameEmpty %>"
					Display ="None" ValidationGroup="vgBlockEsalesRecommendV2" />
				<br />
				<asp:TextBox ID="EsalesSubPanelNameTextBox" runat="server" />
			</div>
			<div class="input-box full-width">
				<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_WindowLastEsalesValue %>" /></span>
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="WindowLastEsalesValueValidator"
					ControlToValidate="WindowLastEsalesValueTextBox" 
					ErrorMessage="<%$ Resources:LekmerMessage,WindowLastEsalesValueEmpty%>"
					Display ="None" ValidationGroup="vgBlockEsalesRecommendV2" />
				<asp:RangeValidator
					runat="server"
					ID="WindowLastEsalesValueRangeValidator"
					ControlToValidate="WindowLastEsalesValueTextBox"
					Type="Integer"
					MinimumValue="1"
					MaximumValue="10000"
					ErrorMessage="<%$ Resources:LekmerMessage,WindowLastEsalesValueRange %>"
					Display="None" ValidationGroup="vgBlockEsalesRecommendV2" />
				<br />
				<asp:TextBox ID="WindowLastEsalesValueTextBox" runat="server" />
			</div>
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
							<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate %>" /></span>
							<br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" />
						</div>
					</div>
					<br class="clear" />
					<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgBlockEsalesRecommendV2"/>
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgBlockEsalesRecommendV2" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>
