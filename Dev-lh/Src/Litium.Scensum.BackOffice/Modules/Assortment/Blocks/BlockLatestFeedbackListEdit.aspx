﻿<%@ Page
	Language="C#"
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockLatestFeedbackListEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockLatestFeedbackListEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="scensum" TagName="CategoryNodeSelector" Src="~/UserControls/Tree/CategoryNodeSelector.ascx" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" runat="server" ValidationGroup="vgBlockLatestFeedbackList" />
</asp:Content>

<asp:Content ID="EditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><%=Resources.General.Literal_Title%>&nbsp;*</span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator runat="server" 
				ID="BlockTitleValidator" 
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$Resources:GeneralMessage, TitleEmpty %>" 
				Display ="None" 
				ValidationGroup="vgBlockLatestFeedbackList" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" runat="server" />
			<br />

			<span>
				<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:RatingReview, Literal_NumberOfItems%>" />&nbsp;*
			</span>&nbsp;
			<asp:RangeValidator runat="server" ID="NumberOfItemsRangeValidator" ControlToValidate="NumberOfItemsTextBox" Type="Integer" MinimumValue="1" MaximumValue="1000"
				ErrorMessage="<%$ Resources:RatingReview, Message_NumberOfItemsGreaterZero %>" Display="None" ValidationGroup="vgBlockLatestFeedbackList" />
			<asp:RequiredFieldValidator runat="server" ID="NumberOfItemsRequiredValidator" ControlToValidate="NumberOfItemsTextBox"
				ErrorMessage="<%$ Resources:RatingReview, Message_NumberOfItemsEmpty %>" Display="None" ValidationGroup="vgBlockLatestFeedbackList" />
			<br />
			<asp:TextBox ID="NumberOfItemsTextBox" runat="server" />
			<br />

			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings %></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
							<span><%=Resources.General.Literal_ChooseTemplate%></span>
							<br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<br class="clear" />

		<div class="content-box">
			<span class="bold"><%= Resources.Product.Literal_Category %></span>
			<scensum:CategoryNodeSelector ID="CategoryNodeSelector" runat="server" Width="300" />
		</div>

		<asp:UpdatePanel ID="BrandListUpdatePanel" runat="server" UpdateMode="Conditional">                                
			<ContentTemplate>
				<div class="content-box">
					<div class="user-roles-list-box" style="width:185px;">
						<span class="bold"><%= Resources.Lekmer.Literal_AvailableBrands %></span>
						<br />
						<asp:ListBox id="AvailableBrandsListBox" DataTextField="Title" DataValueField="Id"
							Rows="10"
							Width="180px"
							SelectionMode="Multiple"
							runat="server">
						</asp:ListBox>
					</div>
					<div class="user-roles-button" style="width:38px;">
						<uc:ImageLinkButton ID="AddBrandsButton" runat="server" Text=">>" SkinID="DefaultButton" />
						<br />
						<br />
						<uc:ImageLinkButton ID="RemoveBrandsButton" runat="server" Text="<<" SkinID="DefaultButton" />
					</div>
					<div class="user-roles-list-box" style="width:220px;">
						<span class="bold"><%= Resources.Lekmer.Literal_SelectedBrands %></span>
						<br />
						<asp:ListBox id="SelectedBrandsListBox" DataTextField="Title" DataValueField="Id"
							Rows="10"
							Width="180px"
							SelectionMode="Multiple"
							runat="server">
						</asp:ListBox>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>

		<br clear="all" />

		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgBlockLatestFeedbackList" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>

	</asp:Panel>
</asp:Content>