using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using IBlockCategoryProductList = Litium.Lekmer.Product.IBlockCategoryProductList;
using IBlockCategoryProductListSecureService = Litium.Lekmer.Product.IBlockCategoryProductListSecureService;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockCategoryProductListEdit : LekmerStatePageController<BlockCategoryProductListState>, IEditor
	{
		protected override void OnInit(EventArgs e)
		{
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnInit(e);
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			OkCategoryPopupButton.Click += OnAdd;
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(State.Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!base.Page.IsValid) return;

			State.Block.Title = BlockTitleTextBox.Text;
			State.Block.TemplateId = TemplateList.SelectedValue.Length.Equals(0)
				? null : (int?)int.Parse(
				TemplateList.SelectedValue, CultureInfo.CurrentCulture);
			BlockSetting.SetSettings(State.Block.Setting);
			State.Block.ProductSortOrderId = int.Parse(ProductSortOrderIdList.SelectedValue, CultureInfo.CurrentCulture);

			foreach (GridViewRow row in CategoryGrid.Rows)
			{
				var categoryIdHiddenField = (HiddenField)row.FindControl("CategoryIdHiddenField");
				int categoryId = int.Parse(categoryIdHiddenField.Value, CultureInfo.CurrentCulture);
				var blockCategory = State.BlockCategories.FirstOrDefault(c => c.CategoryId.Equals(categoryId));
				var includeSubcategoriesCheckBox = (CheckBox) row.FindControl("IncludeSubcategoriesCheckBox");
				blockCategory.IncludeSubcategories = includeSubcategoriesCheckBox.Checked;
				var setNodeCheckBox = (CheckBox) row.FindControl("SetNodeCheckBox");
				SaveRegistryCategorySettings(blockCategory.CategoryId, setNodeCheckBox.Checked);
			}

			int blockId = SaveBlock(State.Block, State.BlockCategories);
			if (blockId == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, Translator.GetTranslations());

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockCategoryProductList);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		private void SaveRegistryCategorySettings(int categoryId, bool useCurrentContentNode)
		{
			var categoryRecord = GetCategoryRecord(categoryId);
			var registryCategory = GetRegistry(State.CurrentContentNode.SiteStructureRegistryId, categoryRecord);
			var service = IoC.Resolve<ICategorySiteStructureRegistrySecureService>();

			if (useCurrentContentNode)
			{
				if (registryCategory == null)
				{
					registryCategory = service.Create();
					registryCategory.CategoryId = categoryId;
					registryCategory.SiteStructureRegistryId = State.CurrentContentNode.SiteStructureRegistryId;
				}
				registryCategory.ProductParentContentNodeId = State.CurrentContentNode.Id;
			}
			if (registryCategory != null)
			{
				service.Save(SignInHelper.SignedInSystemUser, registryCategory);
			}
		}

		public virtual void OnAdd(object sender, EventArgs e)
		{
			var categories = CategoryTree.SelectedIds;
			if (categories.Count == 0) return;
			foreach (int categoryId in categories)
			{
				var id = categoryId;
				var category = State.BlockCategories.FirstOrDefault(c => c.CategoryId.Equals(id));
				if (category == null)
				{
					var blockCategory = IoC.Resolve<IBlockCategoryProductListCategorySecureService>().Create();
					blockCategory.BlockId = State.Block.Id;
					blockCategory.CategoryId = id;
					State.BlockCategories.Add(blockCategory);
				}
			}
			CategoryGrid.DataSource = State.BlockCategories;
			CategoryGrid.DataBind();
		}

		protected override void PopulateForm()
		{
			var currentContentNode = GetContentNode();
			var block = GetBlock();
			var blockCategories = GetBlockCategories();

			if (currentContentNode == null)
			{
				throw new BusinessObjectNotExistsException("Cannot find content node.");
			}

			if (block == null)
			{
				throw new BusinessObjectNotExistsException("Cannot find block.");
			}

			BlockTitleTextBox.Text = block.Title;

			TemplateList.DataSource = GetTemplates();
			TemplateList.DataTextField = "Title";
			TemplateList.DataValueField = "Id";
			TemplateList.DataBind();
			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			TemplateList.SelectedValue = block.TemplateId == null
				? ""
				: block.TemplateId.Value.ToString(CultureInfo.CurrentCulture);

			BlockSetting.Setting = block.Setting;
			ProductSortOrderIdList.Items.AddRange(GetProductSortOrders());
			ProductSortOrderIdList.SelectedValue = block.ProductSortOrderId.ToString(CultureInfo.CurrentCulture);

			State = new BlockCategoryProductListState(block, blockCategories, currentContentNode);

			CategoryGrid.DataSource = blockCategories;
			CategoryGrid.DataBind();

			CategoryTree.Selector = CategorySelector;
			foreach (IBlockCategoryProductListCategory value in blockCategories)
			{
				CategoryTree.SelectedIds.Add(value.CategoryId);
			}
			CategoryTree.DataBind();

			PopulateTranslation(GetBlockId());

			var master = Master as Pages;
			if (master == null) return;

			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.ProductMessage.BlockCategoryProductListEdit);
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}
		
		protected virtual int SaveBlock(IBlockCategoryProductList block, Collection<IBlockCategoryProductListCategory> blockCategories)
		{
			return IoC.Resolve<IBlockCategoryProductListSecureService>().Save(SignInHelper.SignedInSystemUser, block, blockCategories);
		}

		protected void SetNodeCheckBoxChanged(object sender, EventArgs e)
		{
			var cb = sender as CheckBox;
			if (cb == null) return;

			var parentContentNodeLiteral = (Literal)cb.Parent.FindControl("ParentContentNodeLiteral");
			if (cb.Checked)
			{
				parentContentNodeLiteral.Text = State.CurrentContentNode.Title;
			}
			else
			{
				var categoryIdHiddenField = (HiddenField)cb.Parent.FindControl("CategoryIdHiddenField");
				int categoryId = int.Parse(categoryIdHiddenField.Value, CultureInfo.CurrentCulture);
				var registryCategory = GetRegistry(State.CurrentContentNode.SiteStructureRegistryId, GetCategoryRecord(categoryId));
				int? nodeId = registryCategory != null ? registryCategory.ProductParentContentNodeId : null;
				parentContentNodeLiteral.Text = nodeId.HasValue
													? IoC.Resolve<IContentNodeSecureService>().GetById(nodeId.Value).Title
													: string.Empty;
			}
		}

		protected void CategoryGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType != DataControlRowType.DataRow) return;

			string parentContentNodeTitle = string.Empty;
			int? parentContentNodeId = null;

			var blockCategory = (IBlockCategoryProductListCategory)e.Row.DataItem;
			ICategoryRecord categoryRecord = GetCategoryRecord(blockCategory.CategoryId);
			if (categoryRecord.CategorySiteStructureRegistries.Count > 0)
			{
				var registryCategory = GetRegistry(State.CurrentContentNode.SiteStructureRegistryId, categoryRecord);
				if (registryCategory != null && registryCategory.ProductParentContentNodeId.HasValue)
				{
					parentContentNodeId = registryCategory.ProductParentContentNodeId;
					IContentNode parentContentNode = IoC.Resolve<IContentNodeSecureService>().GetById(parentContentNodeId.Value);
					parentContentNodeTitle = parentContentNode.Title;
				}
			}
			if (parentContentNodeId == null)
			{
				parentContentNodeId = State.CurrentContentNode.Id;
				parentContentNodeTitle = State.CurrentContentNode.Title;
			}

			var titleLiteral = (Literal)e.Row.FindControl("TitleLiteral");
			titleLiteral.Text = categoryRecord.Title;

			var parentContentNodeLiteral = (Literal)e.Row.FindControl("ParentContentNodeLiteral");
			parentContentNodeLiteral.Text = parentContentNodeTitle;

			var categoryIdHiddenField = (HiddenField)e.Row.FindControl("CategoryIdHiddenField");
			categoryIdHiddenField.Value = blockCategory.CategoryId.ToString(CultureInfo.CurrentCulture);

			var setNodeCheckBox = (CheckBox)e.Row.FindControl("SetNodeCheckBox");
			setNodeCheckBox.Checked = parentContentNodeId.Equals(State.CurrentContentNode.Id);

			var includeSubcategoriesCheckBox = (CheckBox)e.Row.FindControl("IncludeSubcategoriesCheckBox");
			includeSubcategoriesCheckBox.Checked = blockCategory.IncludeSubcategories;

			e.Row.Visible = blockCategory.Status != BusinessObjectStatus.Deleted;
		}

		protected void CategoryGrid_RowCommand(object sender, CommandEventArgs e)
		{
			int categoryId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
			var category = State.BlockCategories.First(c => c.CategoryId.Equals(categoryId));
			category.Status = BusinessObjectStatus.Deleted;

			CategoryGrid.DataSource = State.BlockCategories;
			CategoryGrid.DataBind();
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ICategorySiteStructureRegistry GetRegistry(int registryId, ICategoryRecord categoryRecord)
		{
			return categoryRecord.CategorySiteStructureRegistries.FirstOrDefault(r => r.SiteStructureRegistryId.Equals(registryId));
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ICategoryRecord GetCategoryRecord(int categoryId)
		{
			return IoC.Resolve<ICategorySecureService>().GetCategoryRecordById(categoryId);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetContentNodeId()
		{
			return Request.QueryString.GetInt32("Id");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IContentNode GetContentNode()
		{
			return IoC.Resolve<IContentNodeSecureService>().GetById(GetContentNodeId());
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IBlockCategoryProductList GetBlock()
		{
			return IoC.Resolve<IBlockCategoryProductListSecureService>().GetById(GetBlockId());
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IBlockCategoryProductListCategory> GetBlockCategories()
		{
			return IoC.Resolve<IBlockCategoryProductListCategorySecureService>().GetAllByBlock(GetBlockId());
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ListItem[] GetProductSortOrders()
		{
			var values = Enum.GetValues(typeof(ProductSortOrderConstant));
			var items = new ListItem[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				var value = values.GetValue(i);
				items[i] = new ListItem(value.ToString(), ((int)value).ToString(CultureInfo.CurrentCulture));
			}
			return items;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ITemplate> GetTemplates()
		{
			return IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockCategoryProductList");
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}

	[Serializable]
	public class BlockCategoryProductListState
	{
		private readonly IBlockCategoryProductList _block;
		public IBlockCategoryProductList Block
		{
			get { return _block; }
		}

		private readonly Collection<IBlockCategoryProductListCategory> _blockCategories = new Collection<IBlockCategoryProductListCategory>();
		public Collection<IBlockCategoryProductListCategory> BlockCategories
		{
			get { return _blockCategories; }
		}

		private readonly IContentNode _currentContentNode;
		public IContentNode CurrentContentNode
		{
			get { return _currentContentNode; }
		}

		public BlockCategoryProductListState(
			IBlockCategoryProductList block,
			Collection<IBlockCategoryProductListCategory> blockCategories,
			IContentNode currentContentNode)
		{
			_block = block;
			_blockCategories = blockCategories;
			_currentContentNode = currentContentNode;
		}
	}
}
