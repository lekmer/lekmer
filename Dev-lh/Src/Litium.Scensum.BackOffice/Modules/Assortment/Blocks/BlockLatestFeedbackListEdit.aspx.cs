﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockLatestFeedbackListEdit : LekmerStatePageController<BrandsState>, IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.RatingReview.Literal_BlockLatestFeedbackListEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			CategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
			AddBrandsButton.Click += AddBrandsClick;
			RemoveBrandsButton.Click += RemoveBrandsClick;
		}

		protected override void PopulateForm()
		{
			int blockId = GetBlockId();
			var block = IoC.Resolve<IBlockLatestFeedbackListSecureService>().GetById(blockId);

			InitializeState(block);

			BlockTitleTextBox.Text = block.Title;
			NumberOfItemsTextBox.Text = block.NumberOfItems.ToString(CultureInfo.InvariantCulture);
			PopulateCategory(block.CategoryId);
			PopulateBrands(State.SelectedBrands);
			PopulateTemplateList(block);
			PopulateTranslation(blockId);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var block = IoC.Resolve<IBlockLatestFeedbackListSecureService>().GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockLatestFeedbackListSecureService>();

			int blockId = GetBlockId();
			var block = blockService.GetById(blockId);
			if (block == null)
			{
				throw new BusinessObjectNotExistsException(blockId);
			}

			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			block.Title = BlockTitleTextBox.Text;
			block.NumberOfItems = int.Parse(NumberOfItemsTextBox.Text, CultureInfo.CurrentCulture);
			block.CategoryId = CategoryNodeSelector.SelectedNodeId;
			block.BlockLatestFeedbackListBrands = GetSelectedBrands(blockId);

			blockService.Save(SignInHelper.SignedInSystemUser, block);
			if (block.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockLatestFeedbackListSavedSuccessfully, InfoType.Success);
			}
		}

		protected virtual void OnCategoryNodeCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);

			CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(nodeId);
			CategoryNodeSelector.DataBind();
		}

		protected virtual void AddBrandsClick(object sender, EventArgs e)
		{
			Collection<IBrand> brandsList = State.SelectedBrands;

			foreach (ListItem item in AvailableBrandsListBox.Items)
			{
				if (item.Selected)
				{
					var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					var brand = State.AllBrands.FirstOrDefault(b => b.Id == brandId);
					if (brand != null)
					{
						brandsList.Add(brand);
					}
				}
			}

			State.SelectedBrands = brandsList;

			PopulateBrands(brandsList);
		}

		protected virtual void RemoveBrandsClick(object sender, EventArgs e)
		{
			Collection<IBrand> brandsList = State.SelectedBrands;

			foreach (ListItem item in SelectedBrandsListBox.Items)
			{
				if (item.Selected)
				{
					var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					var brand = brandsList.FirstOrDefault(b => b.Id == brandId);
					if (brand != null)
					{
						brandsList.Remove(brand);
					}
				}
			}

			State.SelectedBrands = brandsList;

			PopulateBrands(brandsList);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void InitializeState(IBlockLatestFeedbackList block)
		{
			if (State == null)
			{
				State = new BrandsState
				{
					AllBrands = new Collection<IBrand>(),
					SelectedBrands = new Collection<IBrand>()
				};
			}

			State.AllBrands = IoC.Resolve<IBrandSecureService>().GetAll();
			foreach (var brand in block.BlockLatestFeedbackListBrands.Select(b => b.Brand))
			{
				State.SelectedBrands.Add(brand);
			}
		}

		protected virtual void PopulateCategory(int? id)
		{
			if (id != null)
			{
				CategoryNodeSelector.SelectedNodeId = id;
			}

			CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(id);
			CategoryNodeSelector.DataBind();
			CategoryNodeSelector.PopulatePath(id);
		}

		protected virtual void PopulateBrands(Collection<IBrand> stateSelectedBrands)
		{
			var allBrands = State.AllBrands;

			var availableBrands = new Collection<IBrand>();
			foreach (var brand in allBrands)
			{
				if (stateSelectedBrands.FirstOrDefault(b => b.Id == brand.Id) == null)
				{
					availableBrands.Add(brand);
				}
			}

			AvailableBrandsListBox.DataSource = availableBrands;
			AvailableBrandsListBox.DataBind();

			SelectedBrandsListBox.DataSource = stateSelectedBrands;
			SelectedBrandsListBox.DataBind();

			BrandListUpdatePanel.Update();
		}

		protected virtual void PopulateTemplateList(IBlockLatestFeedbackList block)
		{
			TemplateList.DataSource = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockLatestFeedbackList");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");

			TemplateList.Items.Insert(0, listItem);

			var item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected virtual Collection<IBlockLatestFeedbackListBrand> GetSelectedBrands(int blockId)
		{
			var blockLatestFeedbackListBrand = new Collection<IBlockLatestFeedbackListBrand>();

			foreach (var selectedBrand in State.SelectedBrands)
			{
				var latestFeedbackListBrand = IoC.Resolve<IBlockLatestFeedbackListBrand>();
				latestFeedbackListBrand.Brand = selectedBrand;
				latestFeedbackListBrand.BlockId = blockId;

				blockLatestFeedbackListBrand.Add(latestFeedbackListBrand);
			}

			return blockLatestFeedbackListBrand;
		}
	}

	[Serializable]
	public sealed class BrandsState
	{
		public Collection<IBrand> AllBrands { get; set; }
		public Collection<IBrand> SelectedBrands { get; set; }
	}
}