using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.PriceLists
{
	public class PriceListItemDataSource
	{
		private int _rowCount;

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "channelId"), 
        SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), 
        SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "priceListId"), 
        SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int priceListId, int maximumRows, int startRowIndex)
		{
			return _rowCount;
		}

		public Collection<IPriceListItem> SelectMethod(int priceListId, int maximumRows, int startRowIndex)
		{
			IPriceListItemSecureService priceListItemSecureService = IoC.Resolve<IPriceListItemSecureService>();
			var items = priceListItemSecureService.GetAllByPriceList(priceListId, startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = items.TotalCount;
			return items;
		}
	}
}
