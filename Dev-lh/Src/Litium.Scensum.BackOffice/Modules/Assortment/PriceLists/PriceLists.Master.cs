using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.PriceLists
{
	public partial class PriceLists : MasterPage
	{
		private const string SelectedNode = "SelectedPriceListFolderId";

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session[SelectedNode];
			}
			set
			{
				Session[SelectedNode] = value;
			}
		}

		public virtual bool DenySelection
		{
			get
			{
				return PriceListTree.DenySelection;
			}
			set
			{
				PriceListTree.DenySelection = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return PriceListTree.RootNodeId;
			}
			set
			{
				PriceListTree.RootNodeId = value;
			}
		}

		public virtual bool ShowFolderContent { get; set; }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			SearchButton.Click += SearchButton_Click;
			PriceListTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

            if (SelectedFolderId == null)
			{
				SelectedFolderId = PriceListTree.RootNodeId;
			}

			if (!IsPostBack)
			{
				PopulateTree(SelectedFolderId);
				RestoreSearchFields();
			}

			if (SelectedFolderId == PriceListTree.RootNodeId || PriceListTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}

			ScriptManager.RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", PriceListTree.ClientID), true);
        }

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			ShowFolderContent = true;
			PopulateTree(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					DenySelection = true;
					break;
				case "Navigate":
					SelectedFolderId = e.Id;
					Response.Redirect(PathHelper.Assortment.Pricelist.GetDefaultUrlWithShowFolderContent());
					break;
			}
		}

		protected virtual void LbtnCreateFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = PriceListTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Assortment.Pricelist.GetFolderCreateUrl(PriceListTree.MenuLastClickedNodeId.Value));
		}

		protected virtual void LbtnEditFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = PriceListTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Assortment.Pricelist.GetFolderEditUrl(PriceListTree.MenuLastClickedNodeId.Value));
		}

		protected virtual void LbtnDelete_Click(object sender, EventArgs e)
		{
        	var service = IoC.Resolve<IPriceListFolderSecureService>();
			int? parentId = service.GetById(PriceListTree.MenuLastClickedNodeId.Value).ParentPriceListFolderId;
			int result = service.Delete(SignInHelper.SignedInSystemUser, PriceListTree.MenuLastClickedNodeId.Value);
			if (result == -1)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.NotDeleteFolderWithItems);
				SelectedFolderId = PriceListTree.SelectedNodeId = PriceListTree.MenuLastClickedNodeId;
			}
			else
			{
				SystemMessageContainer.MessageType = InfoType.Success;
                SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);
				SelectedFolderId = parentId;
				PopulateTree(null);
				if (SelectedFolderId == null)
				{
					Response.Redirect(PathHelper.Assortment.Pricelist.GetDefaultUrl());
				}
				else
				{
					Response.Redirect(PathHelper.Assortment.Pricelist.GetDefaultUrlWithShowFolderContent());
				}
			}
		}
		protected virtual void SearchButton_Click(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			SearchCriteriaState<string>.Instance.Save(PathHelper.Assortment.Pricelist.GetSearchResultUrl(), SearchTextBox.Text);	
			Response.Redirect(PathHelper.Assortment.Pricelist.GetSearchResultUrl());
		}

		public void PopulateTree(int? id)
		{
			PriceListTree.DataSource = GetNodes(id);
			PriceListTree.RootNodeTitle = Resources.Product.Literal_PriceLists;
			PriceListTree.DataBind();
			LeftUpdatePanel.Update();
			PriceListTree.SelectedNodeId = id ?? SelectedFolderId;
		}

		public virtual Collection<INode> GetNodes(int? nodeId)
		{
			var definedId = nodeId ?? SelectedFolderId;
			var actualId = definedId != PriceListTree.RootNodeId ? definedId : null;
			return IoC.Resolve<IPriceListFolderSecureService>().GetTree(actualId);
		}

		public void UpdateSelection(int nodeId)
		{
			PriceListTree.SelectedNodeId = SelectedFolderId = nodeId;
			LeftUpdatePanel.Update();
		}

		protected void RestoreSearchFields()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Assortment.Pricelist.GetSearchResultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}
	}
}
