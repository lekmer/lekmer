﻿using System;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Suppliers
{
	public partial class SupplierEdit : LekmerPageController
	{
		private int _id;
		protected int Id
		{
			get { return _id > 0 ? _id : (_id = GetId()); }
		}

		private ISupplierSecureService _supplierService;
		protected ISupplierSecureService SupplierService
		{
			get { return _supplierService ?? (_supplierService = IoC.Resolve<ISupplierSecureService>()); }
		}

		private ISupplier _supplier;
		protected ISupplier Supplier
		{
			get { return _supplier ?? (_supplier = SupplierService.GetById(Id)); }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			SupplierHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", Resources.Lekmer.Literal_EditSupplier, Supplier.Name);
			SupplierNoTextBox.Text = Supplier.SupplierNo;
			ActivePassiveCodeTextBox.Text = Supplier.ActivePassiveCode;
			VatTextBox.Text = (int.Parse(Supplier.VatRate) / 100).ToString(CultureInfo.InvariantCulture);
			Phone1TextBox.Text = Supplier.Phone1;
			Phone2TextBox.Text = Supplier.Phone2;
			EmailTextBox.Text = Supplier.Email;
			CityTextBox.Text = Supplier.City;
			AddressTextBox.Text = Supplier.Address;
			ZipTextBox.Text = Supplier.ZipArea;
			CountryTextBox.Text = Supplier.CountryIso;
			LanguageTextBox.Text = Supplier.LanguageIso;
			DropShipEmailTextBox.Text = Supplier.DropShipEmail;
			IsDropShipCheckBox.Checked = Supplier.IsDropShip;
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			Supplier.DropShipEmail = DropShipEmailTextBox.Text.Trim();
			Supplier.IsDropShip = IsDropShipCheckBox.Checked;

			SupplierService.Save(SignInHelper.SignedInSystemUser, Supplier);

			Messager.Add(GeneralMessage.SaveSuccessful, InfoType.Success);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer == null)
			{
				Response.Redirect(LekmerPathHelper.Suppliers.GetDefaultUrl());
			}
			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}
	}
}