﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.Foundation;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Suppliers
{
	public partial class Default : LekmerPageController
	{
		private ISupplierSecureService _supplierService;
		protected ISupplierSecureService SupplierService
		{
			get { return _supplierService ?? (_supplierService = IoC.Resolve<ISupplierSecureService>()); }
		}

		protected virtual bool IsSearchResult
		{
			get { return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false; }
		}

		protected override void SetEventHandlers()
		{
			SearchButton.Click += OnSearch;
			SuppliersGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack)
			{
				RestoreSearchField();
				PopulateForm();
			}
		}

		protected virtual void RestoreSearchField()
		{
			if (!IsSearchResult) return;

			string searchName = SearchCriteriaState<string>.Instance.Get(LekmerPathHelper.Suppliers.GetSearchResultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		protected override void PopulateForm()
		{
			PopulateSuppliers();
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			var searchUrl = LekmerPathHelper.Suppliers.GetSearchResultUrl();
			SearchCriteriaState<string>.Instance.Save(searchUrl, SearchTextBox.Text);

			Response.Redirect(searchUrl);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SuppliersGrid.PageIndex = e.NewPageIndex;
			PopulateSuppliers();
		}

		protected virtual void PopulateSuppliers()
		{
			SuppliersGrid.DataSource = IsSearchResult && SearchTextBox.Text != string.Empty ? SearchSuppliers() : SupplierService.GetAll();
			SuppliersGrid.DataBind();
		}

		protected virtual string GetEditUrl(object supplierId)
		{
			return IsSearchResult
				? LekmerPathHelper.Suppliers.GetEditUrlFromSearch(Convert.ToInt32(supplierId, CultureInfo.CurrentCulture))
				: LekmerPathHelper.Suppliers.GetEditUrl(Convert.ToInt32(supplierId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<ISupplier> SearchSuppliers()
		{
			var searchUrl = LekmerPathHelper.Suppliers.GetSearchResultUrl();
			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);
			if (string.IsNullOrEmpty(searchCriteria))
			{
				return new Collection<ISupplier>();
			}

			return SupplierService.Search(searchCriteria);
		}
	}
}