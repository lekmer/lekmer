﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.Foundation;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Tags
{
	public partial class TagGroupEdit : LekmerPageController
	{
		private int _id;
		protected int Id
		{
			get { return _id > 0 ? _id : (_id = GetId()); }
		}

		private ITagGroupSecureService _tagGroupService;
		protected ITagGroupSecureService TagGroupService
		{
			get { return _tagGroupService ?? (_tagGroupService = IoC.Resolve<ITagGroupSecureService>()); }
		}

		private ITagSecureService _tagService;
		protected ITagSecureService TagService
		{
			get { return _tagService ?? (_tagService = IoC.Resolve<ITagSecureService>()); }
		}

		private IFlagSecureService _flagService;
		protected IFlagSecureService FlgService
		{
			get { return _flagService ?? (_flagService = IoC.Resolve<IFlagSecureService>()); }
		}

		private ITagGroup _tagGroup;
		protected ITagGroup TagGroup
		{
			get { return _tagGroup ?? (_tagGroup = TagGroupService.GetById(Id)); }
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			TagsGrid.RowDataBound += OnRowDataBound;
			TagsGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			TagGroupHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", Resources.General.Literal_EditTagGroup, TagGroup.Title);
			PopulateTags();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer == null)
			{
				Response.Redirect(LekmerPathHelper.Tags.GetDefaultUrl());
			}
			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var tag = (ITag) row.DataItem;
			if (tag.FlagId.HasValue)
			{
				var flagLabel = (Label) e.Row.FindControl("FlagLabel");
				flagLabel.Text = GetFlag(tag.FlagId.Value);
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			TagsGrid.PageIndex = e.NewPageIndex;
			PopulateTags();
		}

		protected virtual void PopulateTags()
		{
			TagsGrid.DataSource = TagService.GetAllByTagGroup(Id);
			TagsGrid.DataBind();
		}

		protected virtual string GetEditUrl(object tagId)
		{
			return LekmerPathHelper.Tags.GetTagEditUrl(Convert.ToInt32(tagId, CultureInfo.CurrentCulture), Id);
		}
		protected virtual string GetFlag(int flagId)
		{
			var flag = FlgService.GetById(flagId);
			return flag != null ? flag.Title : string.Empty;
		}
	}
}