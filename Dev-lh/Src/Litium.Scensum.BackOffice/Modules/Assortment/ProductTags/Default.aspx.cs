﻿using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.Foundation;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Tags
{
	public partial class Default : LekmerPageController
	{
		private ITagGroupSecureService _tagGroupService;
		protected ITagGroupSecureService TagGroupService
		{
			get { return _tagGroupService ?? (_tagGroupService = IoC.Resolve<ITagGroupSecureService>()); }
		}

		protected override void SetEventHandlers()
		{
			TagGroupsGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateTagGroups();
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			TagGroupsGrid.PageIndex = e.NewPageIndex;
			PopulateTagGroups();
		}

		protected virtual void PopulateTagGroups()
		{
			TagGroupsGrid.DataSource = TagGroupService.GetAllWithoutTags();
			TagGroupsGrid.DataBind();
		}

		protected virtual string GetEditUrl(object tagGroupId)
		{
			return LekmerPathHelper.Tags.GetTagGroupEditUrl(Convert.ToInt32(tagGroupId, CultureInfo.CurrentCulture));
		}
	}
}