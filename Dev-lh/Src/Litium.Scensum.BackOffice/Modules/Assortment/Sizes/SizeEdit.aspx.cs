﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Sizes
{
	public partial class SizeEdit : LekmerStatePageController<SizeEditState>, IEditor
	{
		private List<string> _validationMessage = new List<string>();
		private ISizeSecureService _sizeSecureService;
		private ISizeSecureService _SizeSecureService
		{
			get { return _sizeSecureService ?? IoC.Resolve<ISizeSecureService>(); }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var item = _SizeSecureService.GetById(id.Value);
				if (null != item)
				{
					ErpIdTextBox.Text = item.ErpId;
					ErpTitleTextBox.Text = item.EuTitle;
					EuTextBox.Value = item.Eu.ToString(CultureInfo.CurrentCulture);
					EuTitleTextBox.Text = item.EuTitle;
				}

			}
			
		}

		public void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}
				SystemMessageContainer.MessageType = InfoType.Warning;
				return;
			}

			int? id = GetIdOrNull();
			var item = id.HasValue ? _SizeSecureService.GetById(id.Value) : new Size();

			if (null != item)
			{
				item.ErpId = ErpIdTextBox.Text.Trim();
				item.ErpTitle = ErpTitleTextBox.Text.Trim();
				item.EuTitle = EuTitleTextBox.Text.Trim();
				item.Eu = decimal.Parse(EuTextBox.Value);
			}

			var result = _SizeSecureService.Save(SignInHelper.SignedInSystemUser, item);
			if(result<0)
			{
				SystemMessageContainer.Add(string.Format("{0} Id={1}.", Resources.LekmerMessage.Size_CannotSave, id));
			}
		}
		protected virtual void RedirectBack()
		{
			Response.Redirect(LekmerPathHelper.Sizes.GetEditUrl());
		}
		protected virtual SizeTablesMaster MasterPage
		{
			get { return (SizeTablesMaster)Master; }
		}
		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidStringValue(ErpIdTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.Size_Empty);
				isValid = false;
			}

			if (!IsValidStringValue(ErpIdTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.Size_Empty);
				isValid = false;
			}

			if (!IsValidStringValue(EuTitleTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.Size_Empty);
				isValid = false;
			}
			decimal val;
			if (!decimal.TryParse(EuTextBox.Value.Trim(), out val))
			{
				_validationMessage.Add(Resources.LekmerMessage.Size_EUIncorrect);
				isValid = false;
			}
			else
			{
				char separator = System.Convert.ToChar(CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
				var values = EuTextBox.Value.Trim().Split(separator);
				bool valid = values[0].Length < 4;
				if (values.Length == 2)
				{
					valid &= values[1].Length < 3;
				}
				if (!valid)
				{
					_validationMessage.Add(Resources.LekmerMessage.Size_EUIncorrect);
					isValid = false;
				}
			}


			return isValid;
		}
		protected virtual bool IsValidStringValue(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return false;
			}
			return value.Length <= 50;
		}

	}


	[Serializable]
	public class SizeEditState
	{
		public int? Id { get; set; }
		public ISize Size { get; set; }

	}
}

