﻿<%@ Page 
	Language="C#" 
	MasterPageFile="~/Modules/Assortment/Sizes/SizeTables.Master" 
	CodeBehind="SizeSorting.aspx.cs" 
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Sizes.SizeSorting" 
	MaintainScrollPositionOnPostback="true"
%>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="ErrorMessager"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:MessageContainer ID="SuccessMessager"  MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
</asp:Content>

<asp:Content ID="SizeSortingContent" ContentPlaceHolderID="SizeTablePlaceHolder" runat="server">

	<asp:Panel ID="SortPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div id="sort-content">
				<asp:GridView 
					ID="SizesGrid"
					SkinID="grid"
					runat="server" 
					AutoGenerateColumns="false"
					Width="100%">
					<Columns>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_ErpId %>">
							<ItemTemplate>
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
								<uc:LiteralEncoded runat="server" Text='<%# Eval("ErpId") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_ErpTitle %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" Text='<%# Eval("ErpTitle") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Eu %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" Text='<%# FormatSize(Eval("Eu"))%>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_EuTitle %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" Text='<%# Eval("EuTitle") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
							<HeaderTemplate>
								<div class="inline">
									<%= Resources.General.Literal_SortOrder %>
									<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" CommandArgument="0" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
								</div>
							</HeaderTemplate>
							<ItemTemplate>
								<div class="inline">
									<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
									<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
									<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%#Eval("Ordinal") %>' />
									<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
									<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
								</div>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
		</div>
		<br /><br />
		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgOrdinals" SkinID="DefaultButton"/>
			<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>" CausesValidation="false" SkinID="DefaultButton" />
		</div>
	</asp:Panel>
</asp:Content>
