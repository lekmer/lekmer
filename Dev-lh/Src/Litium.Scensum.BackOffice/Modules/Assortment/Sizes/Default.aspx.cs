﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Sizes
{
	public partial class Default : LekmerPageController
	{
		private Collection<ISizeTableFolder> _allFolders;

		public virtual SizeTablesMaster MasterPage
		{
			get
			{
				return (SizeTablesMaster) Master;
			}
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected override void SetEventHandlers()
		{
			SizeTablesGrid.RowCommand += OnRowCommand;
			SizeTablesGrid.RowDataBound += OnRowDataBound;
			SizeTablesGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateSizeTables();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteSizeTable":
					DeleteSizeTable(id);
					SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
					break;
			}

			PopulateSizeTables();
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SizeTablesGrid.PageIndex = e.NewPageIndex;

			PopulateSizeTables();
		}

		protected virtual void DeleteSizeTable(int sizeTableId)
		{
			IoC.Resolve<ISizeTableSecureService>().Delete(SignInHelper.SignedInSystemUser, sizeTableId);
		}

		protected virtual void PopulateSizeTables()
		{
			if (IsSearchResult)
			{
				SizeTablesGrid.DataSource = SearchSizeTables();
				SizeTablesGrid.Columns[3].Visible = true;
			}
			else
			{
				SizeTablesGrid.DataSource = RetrieveSizeTables();
				SizeTablesGrid.Columns[3].Visible = false;
			}

			SizeTablesGrid.DataBind();

			SizeTablesGrid.Visible = SizeTablesGrid.DataSource != null;
		}

		protected virtual string GetEditUrl(object sizeTableId)
		{
			return LekmerPathHelper.SizeTable.GetEditUrl(System.Convert.ToInt32(sizeTableId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<ISizeTable> RetrieveSizeTables()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return IoC.Resolve<ISizeTableSecureService>().GetAllByFolder(folderId.Value);
			}

			return null;
		}

		protected virtual Collection<ISizeTable> SearchSizeTables()
		{
			var searchUrl = LekmerPathHelper.SizeTable.GetSearchResultUrl();

			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);

			if (string.IsNullOrEmpty(searchCriteria))
			{
				return new Collection<ISizeTable>();
			}

			return IoC.Resolve<ISizeTableSecureService>().Search(searchCriteria);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<ISizeTableFolderSecureService>();
				_allFolders = service.GetAll();
			}

			var pathHelper = new FolderPathHelper();
			return pathHelper.GetPath(_allFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");

				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");

				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');");
			}
		}
	}
}