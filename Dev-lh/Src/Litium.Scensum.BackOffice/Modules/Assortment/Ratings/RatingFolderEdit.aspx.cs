﻿using System;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Ratings
{
	public partial class RatingFolderEdit : LekmerPageController, IEditor
	{
		protected virtual RatingsMaster MasterPage
		{
			get
			{
				return (RatingsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();
			if (id.HasValue)
			{
				var folder = IoC.Resolve<IRatingFolderSecureService>().GetById(id.Value);
				RatingFolderTitleTextBox.Text = folder.Title;

				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_EditRatingFolder);

				PopulateTreeView(null);
			}
			else
			{
				MasterPage.BreadcrumbAppend.Add(Resources.RatingReview.Literal_CreateRatingFolder);

				DestinationDiv.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.RatingReview.RatingFolderSaveSuccess);
				}
			}

			System.Web.UI.ScriptManager.RegisterStartupScript(
				this,
				GetType(),
				string.Format(CultureInfo.CurrentCulture, "root menu {0}", FolderTree.ClientID),
				string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", FolderTree.ClientID),
				true);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			int? id = GetIdOrNull();
			int? parentId = Request.QueryString.GetInt32OrNull("parentFolderId");

			var ratingFolderSecureService = IoC.Resolve<IRatingFolderSecureService>();

			var ratingFolder = id.HasValue ? ratingFolderSecureService.GetById(id.Value) : IoC.Resolve<IRatingFolder>();
			ratingFolder.Title = RatingFolderTitleTextBox.Text;

			if (id == null)
			{
				ratingFolder.ParentRatingFolderId = parentId != MasterPage.RootNodeId ? parentId : null;
			}
			else if (FolderTree.SelectedNodeId.HasValue)
			{
				ratingFolder.ParentRatingFolderId = FolderTree.SelectedNodeId.Value == FolderTree.RootNodeId ? null : FolderTree.SelectedNodeId;
			}

			ratingFolder = ratingFolderSecureService.Save(SignInHelper.SignedInSystemUser, ratingFolder);

			if (ratingFolder.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.FolderTitleExist);
			}
			else
			{
				MasterPage.PopulateTree(null);
				MasterPage.SelectedFolderId = ratingFolder.Id;

				Response.Redirect(LekmerPathHelper.RatingFolder.GetEditUrlWithMessage(ratingFolder.Id));
			}
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}

		protected void PopulateTreeView(int? folderId)
		{
			var ratingFolders = MasterPage.GetNodes(folderId ?? MasterPage.SelectedFolderId);

			if (ratingFolders == null || ratingFolders.Count <= 0)
			{
				return;
			}

			var editedRatingFolder = ratingFolders.FirstOrDefault(f => f.Id == MasterPage.SelectedFolderId);

			FolderTree.DataSource = editedRatingFolder != null
				? FolderTree.GetFilteredSource(ratingFolders, editedRatingFolder.Id)
				: ratingFolders;
			FolderTree.RootNodeTitle = Resources.RatingReview.Literal_RatingFolders;
			FolderTree.DataBind();

			FolderTree.SelectedNodeId = folderId ?? (editedRatingFolder != null ? editedRatingFolder.ParentId : FolderTree.RootNodeId) ?? FolderTree.RootNodeId;
		}
	}
}