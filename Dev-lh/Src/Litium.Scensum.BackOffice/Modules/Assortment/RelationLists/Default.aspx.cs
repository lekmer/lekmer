using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using RelationListTypeConstant = Litium.Lekmer.Product.RelationListType;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RelationLists
{
	public partial class Default : LekmerPageController
	{
        protected override void SetEventHandlers()
        {
            RelationListGrid.RowCommand += RelationListGridRowCommand;
            RelationListGrid.PageIndexChanging += RelationListGridPageIndexChanging;
            SearchButton.Click += SearchButtonClick;
        }

        protected override void PopulateForm()
        {
            var relationListTypeSecureService = IoC.Resolve<IRelationListTypeSecureService>();
			RelationListTypeList.DataSource = relationListTypeSecureService.GetAll().Where(t => t.CommonName != RelationListTypeConstant.ColorVariant.ToString());
			RelationListTypeList.DataBind();
			RelationListTypeList.Items.Insert(0, new ListItem(string.Empty, string.Empty)); 
            
            RelationListGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
            RestoreSearchFields();
        }
	    protected virtual void RelationListGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteRelationList")
			{
				DeleteRelationList(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RelationListGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RelationListGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void SearchButtonClick(object sender, EventArgs e)
		{
			var searchCriteria = IoC.Resolve<IRelationListSearchCriteria>();
			searchCriteria.Title = RelationListTitleTextBox.Text;
			searchCriteria.TypeId = RelationListTypeList.SelectedValue;
			RelationListGrid.PageIndex = 0;
            PopulateRelationListGrid(searchCriteria);
			SearchCriteriaState<IRelationListSearchCriteria>.Instance.Save(searchCriteria);
		}

		protected void DeleteRelationList(int rlId)
		{
            var relationListSecureService = IoC.Resolve<IRelationListSecureService>();

            relationListSecureService.Delete(SignInHelper.SignedInSystemUser, rlId);
			if (RelationListGrid.Rows.Count == 1 && RelationListGrid.PageIndex > 0)
				RelationListGrid.PageIndex -=1;
			RelationListGrid.DataBind();
		}

		protected void RestoreSearchFields()
		{
			var searchCriteria = SearchCriteriaState<IRelationListSearchCriteria>.Instance.Get();

            if (searchCriteria == null)
			{
				searchCriteria = IoC.Resolve<IRelationListSearchCriteria>();
				searchCriteria.Title = RelationListTitleTextBox.Text;
				searchCriteria.TypeId = RelationListTypeList.SelectedValue;
            }
			RelationListTitleTextBox.Text = searchCriteria.Title;
			RelationListTypeList.SelectedValue = searchCriteria.TypeId;
			PopulateRelationListGrid(searchCriteria);
		}

		protected void PopulateRelationListGrid(IRelationListSearchCriteria searchCriteria)
		{
			RelationListsObjectDataSource.SelectParameters.Clear();
			RelationListsObjectDataSource.SelectParameters.Add("maximumRows", RelationListGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			RelationListsObjectDataSource.SelectParameters.Add("startRowIndex", (RelationListGrid.PageIndex * RelationListGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			RelationListsObjectDataSource.SelectParameters.Add("title", searchCriteria.Title);
			RelationListsObjectDataSource.SelectParameters.Add("typeId", searchCriteria.TypeId);
		}
	}
}