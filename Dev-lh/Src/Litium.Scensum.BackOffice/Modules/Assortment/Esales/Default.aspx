<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Esales.Default" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="EsalesImportTab" ContentPlaceHolderID="body" runat="server">
	<Scensum:ToolBoxPanel ID="ProductHeaderToolBoxPanel" Text="<%$ Resources:Lekmer, Literal_Esales_Import %>" runat="server" />

	<div style="width:978px;">
		<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Warning" HideMessagesControlId="ImportButton" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgEsalesImport" />
	</div>

	<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="ImportButton">
		<div id="articles-search" class="left">
			<asp:FileUpload ID="EsalesFileUpload" runat="server"></asp:FileUpload>
			<div class="articles-search-block">
				<div class="buttons left">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ImportButton" Text="<%$ Resources:Lekmer, Button_Upload %>" ValidationGroup="vgEsalesImport" runat="server" SkinID="DefaultButton" style="padding-right: 0px;" />
				</div>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
