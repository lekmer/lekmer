using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Categories
{
	public class CategoryProductsDataSource
	{
		private int _rowCount;

		public int SelectCount()
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), 
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "categoryId")]
		public int SelectCount(int maximumRows, int startRowIndex, string categoryId)
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), 
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public static Collection<IProduct> SelectMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public Collection<IProduct> SelectMethod(int maximumRows, int startRowIndex, string categoryId)
		{
			var productSecureService = IoC.Resolve<IProductSecureService>();
			var products = productSecureService.GetAllByCategory(ChannelHelper.CurrentChannel.Id, int.Parse(categoryId, CultureInfo.CurrentCulture), startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = products.TotalCount;
			return products;
		}
	}
}
