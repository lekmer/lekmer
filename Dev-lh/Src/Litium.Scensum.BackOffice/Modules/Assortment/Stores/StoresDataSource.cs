﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Stores
{
	public class StoresDataSource
	{
		private int _rowCount;

		public int SelectCount()
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), 
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex)
		{
			return _rowCount;
		}

		public virtual StoreCollection SelectMethod(int maximumRows, int startRowIndex)
		{
			var service = IoC.Resolve<IStoreSecureService>();
			var stores = service.GetAll(startRowIndex / maximumRows + 1, maximumRows, null, null);
			_rowCount = stores.TotalCount;
			return stores;
		}
	}
}
