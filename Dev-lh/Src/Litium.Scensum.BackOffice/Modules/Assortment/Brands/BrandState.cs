using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
	[Serializable]
	public class BrandState
	{
		public bool SetNewImage { get; set; }
		public int? MediaId { get; set; }
		public Collection<IProductRegistryRestrictionItem> RegistryRestrictionBrands { get; set; }
		public Collection<IBrandSiteStructureProductRegistryWrapper> BrandSiteStructureProductRegistryWrappers { get; set; }
	}
}