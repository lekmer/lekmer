﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls
{
	[Serializable]
	public class ProductImageGroupsState
	{
		public int ProductId
		{
			get;
			set;
		}
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IProductImageGroupFull> ProductImageGroups
		{
			get; 
			set;
		}

        public ILekmerImage Movie
        {
            get;
            set;
        }
	}
}
