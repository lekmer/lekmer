﻿using System.ComponentModel;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Reviews
{
	public enum FeedbackSpreadsheetColumns
	{
		[Description("Feedback Id")]
		FeedbackId,
		[Description("HYID")]
		HYID,
		[Description("Customer Name")]
		CustomerName,
		[Description("Title")]
		Title,
		[Description("Review")]
		Review,
		[Description("Date")]
		Date,
		[Description("Channel")]
		Channel,
		[Description("Pending|Approved (0|1)")]
		PendingApproved,
		[Description("IsNeedDelete")]
		IsNeedDelete
	}
}