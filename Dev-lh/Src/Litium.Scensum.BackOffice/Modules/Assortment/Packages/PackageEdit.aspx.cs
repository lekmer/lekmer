﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.Template;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Assortment;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Packages
{
	public partial class PackageEdit : LekmerStatePageController<PackageState>
	{
		private const string DescriptionSeperator = "<br />";
		private const string DefaultAliasCommonName = "Package_{0}";
		private const int DefaultLanguageId = -1;
		private const string DefaultLanguageTitle = "Default";


		private Collection<IPriceListStatus> _statuses;
		private Dictionary<int, int> _productRegistryRestrictions = new Dictionary<int, int>();


		private int? _packageId;
		protected int? PackageId
		{
			get { return _packageId ?? (_packageId = GetIdOrNull()); }
		}

		private Collection<ILanguage> _languages;
		public Collection<ILanguage> Languages
		{
			get { return _languages ?? (_languages = LanguageService.GetAll()); }
		}

		private Collection<ILanguage> _descTranslationLanguages;
		public Collection<ILanguage> DescTranslationLanguages
		{
			get
			{
				if (_descTranslationLanguages != null)
				{
					return _descTranslationLanguages;
				}
				
				_descTranslationLanguages = new Collection<ILanguage>();
				_descTranslationLanguages.AddRange(Languages);

				var language = IoC.Resolve<ILanguage>();
				language.Id = DefaultLanguageId;
				language.Iso = DefaultLanguageTitle;
				_descTranslationLanguages.Insert(0, language);

				return _descTranslationLanguages;
			}
		} 

		private IPackageSecureService _packageService;
		protected IPackageSecureService PackageService
		{
			get { return _packageService ?? (_packageService = IoC.Resolve<IPackageSecureService>()); }
		}

		private ICategorySecureService _categoryService;
		protected ICategorySecureService CategoryService
		{
			get { return _categoryService ?? (_categoryService = IoC.Resolve<ICategorySecureService>()); }
		}

		private IProductSizeSecureService _productSizeService;
		protected IProductSizeSecureService ProductSizeService
		{
			get { return _productSizeService ?? (_productSizeService = IoC.Resolve<IProductSizeSecureService>()); }
		}

		private IPriceListItemSecureService _priceListItemService;
		protected IPriceListItemSecureService PriceListItemService
		{
			get { return _priceListItemService ?? (_priceListItemService = IoC.Resolve<IPriceListItemSecureService>()); }
		}

		private IPriceListSecureService _priceListService;
		protected IPriceListSecureService PriceListService
		{
			get { return _priceListService ?? (_priceListService = IoC.Resolve<IPriceListSecureService>()); }
		}

		private IProductRegistryProductSecureService _productRegistryProductService;
		protected IProductRegistryProductSecureService ProductRegistryProductService
		{
			get { return _productRegistryProductService ?? (_productRegistryProductService = IoC.Resolve<IProductRegistryProductSecureService>()); }
		}

		private IProductSecureService _productService;
		protected IProductSecureService ProductService
		{
			get { return _productService ?? (_productService = IoC.Resolve<IProductSecureService>()); }
		}

		private IProductUrlSecureService _productUrlService;
		protected IProductUrlSecureService ProductUrlService
		{
			get { return _productUrlService ?? (_productUrlService = IoC.Resolve<IProductUrlSecureService>()); }
		}

		private ILanguageSecureService _languageService;
		protected ILanguageSecureService LanguageService
		{
			get { return _languageService ?? (_languageService = IoC.Resolve<ILanguageSecureService>()); }
		}

		private IAliasSecureService _aliasService;
		protected IAliasSecureService AliasService
		{
			get { return _aliasService ?? (_aliasService = IoC.Resolve<IAliasSecureService>()); }
		}

		private IAliasTranslationSecureService _aliasTranslationService;
		protected IAliasTranslationSecureService AliasTranslationService
		{
			get { return _aliasTranslationService ?? (_aliasTranslationService = IoC.Resolve<IAliasTranslationSecureService>()); }
		}

		private ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> _descriptionTranslationService;
		protected ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> DescriptionTranslationService
		{
			get { return _descriptionTranslationService ?? (_descriptionTranslationService = IoC.Resolve<ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository>>()); }
		}

		private ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> _webShopTitleTranslationService;
		protected ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> WebShopTitleTranslationService
		{
			get { return _webShopTitleTranslationService ?? (_webShopTitleTranslationService = IoC.Resolve<ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository>>()); }
		}

		private ProductTranslationGenericSecureService<ProductTitleTranslationRepository> _titleTranslationService;
		protected ProductTranslationGenericSecureService<ProductTitleTranslationRepository> TitleTranslationService
		{
			get { return _titleTranslationService ?? (_titleTranslationService = IoC.Resolve<ProductTranslationGenericSecureService<ProductTitleTranslationRepository>>()); }
		}


		protected override void SetEventHandlers()
		{
			ProductDescriptionTranslator.TriggerImageButton = DescriptionTranslateButton;
			DefaultAliasTranslator.TriggerImageButton = DefaultAliasTranslateButton;
			GeneralInfoTranslator.TriggerImageButton = GeneralInfoTranslateButton;
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			DeleteButton.Click += OnDelete;
			generateDescriptionBtn.Click += OnGenerateDescription;
			RefreshPricesButton.Click += OnRefreshPrices;
			PackageCategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
			PackageProducts.ChangeProductListEvent += OnChangeProductListEvent;
			ProductRegistryGrid.RowDataBound += OnProductRegistryGridRowDataBound;
		}

		protected override void PopulateForm()
		{
			// If redirected after successful saving new package -> display success message
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
			{
				Messager.Add(Resources.GeneralMessage.SaveSuccessPackage, InfoType.Success);
			}

			EnsureState();

			PopulateStatusList();
			PopulateStockStatusList();
			BindPackageInfo();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			WebShopTitleValidator.Enabled = UseWebShopTitleCheckBox.Checked;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (Page.IsPostBack)
			{
				ShowTitle();
				ShowGeneralPackageInfo();
			}
		}

		protected virtual void OnCategoryNodeCommand(object sender, CommandEventArgs e)
		{
			var categoryId = Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			PackageCategoryNodeSelector.DataSource = CategoryService.GetTree(categoryId);
			PackageCategoryNodeSelector.DataBind();
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(LekmerPathHelper.Package.GetDefaultUrl());
		}

		protected virtual void OnDelete(object sender, EventArgs e)
		{
			if (!PackageId.HasValue) return;

			PackageService.Delete(SignInHelper.SignedInSystemUser, State.Package);

			var alias = ((ILekmerAliasSecureService) AliasService).GetByCommonName(string.Format(DefaultAliasCommonName, State.Package.PackageId));
			if (alias != null)
			{
				AliasService.Delete(SignInHelper.SignedInSystemUser, alias.Id);
			}

			Response.Redirect(LekmerPathHelper.Package.GetDefaultUrl());
		}

		protected virtual void OnSave(object sender, EventArgs e)
		{
			bool isPackageExists = PackageId.HasValue;

			var package = isPackageExists ? PackageService.GetByIdSecure(ChannelHelper.CurrentChannel.Id, PackageId.Value) : PackageService.Create();
			if (isPackageExists && package == null)
			{
				throw new BusinessObjectNotExistsException(PackageId.Value);
			}

			var webShopTitleTranslations = WebShopTitleTranslator.GetTranslations();
			var productUrls = PrepareProductUrls(package.MasterProductId, webShopTitleTranslations);

			bool isValid = ValidateInfo(package.MasterProductId, productUrls);
			if (!isValid) return;

			var registryRestrictions = GetRegistryRestrictions();

			FillInPrices(PackageProducts.Products);

			package.MasterProduct.ErpId = !ErpIdBox.Text.IsNullOrEmpty() ? ErpIdBox.Text : null;
			package.MasterProduct.ProductStatusId = Convert.ToInt32(StatusList.SelectedValue, CultureInfo.CurrentCulture);
			((ILekmerProduct) package.MasterProduct).StockStatusId = Convert.ToInt32(StockStatusList.SelectedValue, CultureInfo.CurrentCulture);
			package.MasterProduct.Title = TitleBox.Text;
			package.MasterProduct.WebShopTitle = UseWebShopTitleCheckBox.Checked ? WebShopTitleBox.Text : null;
			package.MasterProduct.NumberInStock = !NumberInStockBox.Text.IsNullOrEmpty() ? Convert.ToInt32(NumberInStockBox.Text, CultureInfo.CurrentCulture) : 0;
			package.MasterProduct.CategoryId = Convert.ToInt32(PackageCategoryNodeSelector.SelectedNodeId, CultureInfo.CurrentCulture);
			((IProductRecord)package.MasterProduct).Description = LongArticleDescriptionEditor.GetValue();
			var generalInfoTranslations = GetGeneralInfoTranslations(package);

			package = PackageService.Save(SignInHelper.SignedInSystemUser,
				package,
				State.PriceListItems,
				WebShopTitleTranslator.GetTranslations(),
				ProductDescriptionTranslator.GetTranslations(),
				generalInfoTranslations,
				registryRestrictions);
			if (package.PackageId <= 0)
			{
				Messager.Add(Resources.GeneralMessage.SaveFailedPackage, InfoType.Failure);
			}
			else
			{
				PackageService.SavePackageProducts(SignInHelper.SignedInSystemUser, package.PackageId, PackageProducts.Products.Select(p => p.ProductItem.Id));
				SaveAlias(package.PackageId);
				ManageRestrictionProduct(registryRestrictions, package.MasterProductId);

				// New package was saved successfully
				if (!isPackageExists)
				{
					SaveProductUrls(package.MasterProductId, productUrls);
					var editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", LekmerPathHelper.Package.GetEditUrl(package.PackageId));
					Response.Redirect(editUrl);
				}
				// Package was updated successfully
				else
				{
					BindLanguages();
					MasterProductLink.Text = package.MasterProduct.DisplayTitle;
					Messager.Add(Resources.GeneralMessage.SaveSuccessPackage, InfoType.Success);
				}
			}
		}

		protected virtual void SaveAlias(int packageId)
		{
			var alias = ((ILekmerAliasSecureService) AliasService).GetByCommonName(string.Format(DefaultAliasCommonName, packageId));
			if (alias == null)
			{
				alias = AliasService.Create();
				alias.AliasFolderId = PackageSetting.Instance.PackageAliasFolderId;
				alias.CommonName = string.Format(DefaultAliasCommonName, packageId);
				alias.AliasTypeId = PackageSetting.Instance.PackageAliasTypeId;
			}
			alias.Value = DefaultAliasEditor.GetValue() ?? string.Empty;

			var translations = new Collection<IAliasTranslation>();
			foreach (var aliasTranslation in DefaultAliasTranslator.GetTranslations())
			{
				IAliasTranslation translation = AliasTranslationService.Create();
				translation.Language = Languages.FirstOrDefault(language => language.Id == aliasTranslation.LanguageId);
				translation.Value = string.IsNullOrEmpty(aliasTranslation.Value) ? null : aliasTranslation.Value;

				translations.Add(translation);
			}
			
			if (alias.Value.IsNullOrEmpty() && translations.All(t => t.Value.IsNullOrEmpty()))
			{
				if (alias.Id > 0)
				{
					AliasService.Delete(SignInHelper.SignedInSystemUser, alias.Id);
				}
			}
			else
			{
				alias.Id = AliasService.Save(SignInHelper.SignedInSystemUser, alias);
				foreach (var translation in translations)
				{
					translation.AliasId = alias.Id;
				}
				AliasTranslationService.SaveAll(SignInHelper.SignedInSystemUser, translations);
			}
		}

		protected virtual void OnChangeProductListEvent(object sender, UserControls.Assortment.Events.PackageProductSearchEventArgs e)
		{
			FillInStatus(e.Products);
			FillInNumberInStock(e.Products);
			FillInPrices(e.Products);
			BindProductRegistries(e.Products);

			upPackageInfo.Update();
		}

		protected virtual void OnGenerateDescription(object sender, EventArgs e)
		{
			// Generate description and translations.
			string commonDescription;
			Collection<ITranslationGeneric> translations;
			GenerateDescription(out commonDescription, out translations);

			// Manage generated descriptions.
			var oldCommonDescription = LongArticleDescriptionEditor.GetValue();
			var oldTranslations = ProductDescriptionTranslator.GetTranslations();
			foreach (RepeaterItem item in LanguagesRepeater.Items)
			{
				var hfLangId = (HiddenField) item.FindControl("hfLangId");
				var cbLang = (CheckBox) item.FindControl("cbLang");
				int langId;
				if (hfLangId != null && cbLang != null && !cbLang.Checked && int.TryParse(hfLangId.Value, out langId))
				{
					if (langId == DefaultLanguageId)
					{
						commonDescription = oldCommonDescription;
						continue;
					}

					var newTranslation = translations.FirstOrDefault(t => t.LanguageId == langId);
					var oldTranslation = oldTranslations.FirstOrDefault(t => t.LanguageId == langId);
					if (newTranslation != null && oldTranslation != null)
					{
						newTranslation.Value = oldTranslation.Value;
					}
				}
			}

			var productId = PackageId.HasValue ? State.Package.MasterProductId : 0;

			LongArticleDescriptionEditor.SetValue(commonDescription);
			BindTranslations(productId, translations, null, null);
			ProductDescriptionTranslator.RebuildTranslations();

			BindLanguages();
		}

		protected virtual void GenerateDescription(out string commonDescription, out Collection<ITranslationGeneric> translations)
		{
			var productId = PackageId.HasValue ? State.Package.MasterProductId : 0;
			commonDescription = DefaultAliasEditor.GetValue().IsNullOrTrimmedEmpty() ? string.Empty : DefaultAliasEditor.GetValue() + DescriptionSeperator;
			translations = new Collection<ITranslationGeneric>();
			foreach (var aliasTranslation in DefaultAliasTranslator.GetTranslations())
			{
				translations.Add(new TranslationGeneric
				{
					LanguageId = aliasTranslation.LanguageId,
					Value = aliasTranslation.Value.IsNullOrTrimmedEmpty() ? string.Empty : aliasTranslation.Value + DescriptionSeperator
				});
			}

			var products = PackageProducts.Products;
			if (products != null && products.Count > 0)
			{
				// Build description and translations.
				foreach (var product in products)
				{
					var record = ProductService.GetRecordById(ChannelHelper.CurrentChannel.Id, product.ProductItem.Id);
					commonDescription += record.Description.IsNullOrEmpty() ? string.Empty : record.DisplayTitle + DescriptionSeperator + record.Description + DescriptionSeperator;

					var webShopTitleTranslations = WebShopTitleTranslationService.GetAllByProduct(product.ProductItem.Id);
					var titleTranslations = TitleTranslationService.GetAllByProduct(product.ProductItem.Id);
					var descriptionTranslations = DescriptionTranslationService.GetAllByProduct(product.ProductItem.Id);
					foreach (var translation in descriptionTranslations)
					{
						var webShopTitleTranslation = webShopTitleTranslations.FirstOrDefault(t => t.LanguageId == translation.LanguageId);
						string webShopTitle = webShopTitleTranslation != null ? webShopTitleTranslation.Value.Trim() : string.Empty;

						var titleTranslation = titleTranslations.FirstOrDefault(t => t.LanguageId == translation.LanguageId);
						string title = titleTranslation != null ? titleTranslation.Value.Trim() : string.Empty;

						string displayTitle = webShopTitle.IsNullOrTrimmedEmpty() ? title : webShopTitle;

						string value = translation.Value.IsNullOrTrimmedEmpty() ? string.Empty : displayTitle + DescriptionSeperator + translation.Value + DescriptionSeperator;

						var langTranslation = translations.FirstOrDefault(t => t.LanguageId == translation.LanguageId);
						if (langTranslation == null)
						{
							translations.Add(new TranslationGeneric { LanguageId = translation.LanguageId, Value = value });
						}
						else
						{
							langTranslation.Value += value;
						}
					}
				}
			}

			// Remove last seperator.
			commonDescription = RemoveLastSeperator(commonDescription);
			foreach (var translation in translations)
			{
				translation.Id = productId;
				translation.Value = RemoveLastSeperator(translation.Value);
			}
		}

		protected virtual void OnRefreshPrices(object sender, EventArgs e)
		{
			FillInPrices(PackageProducts.Products);
		}

		protected virtual void OnProductRegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var productRegistryIdHidden = (HiddenField) row.FindControl("ProductRegistryIdHidden");
			if (_productRegistryRestrictions.ContainsKey(Int32.Parse(productRegistryIdHidden.Value)))
			{
				var restrictionCheckBox = (CheckBox)row.FindControl("RestrictionCheckBox");
				restrictionCheckBox.Checked = true;
			}
		}


		protected virtual void EnsureState()
		{
			if (State == null)
			{
				State = new PackageState
					{
						PriceLists = new Dictionary<int, IPriceList>(),
						PriceListItems = new Collection<IPriceListItem>()
					};
			}
		}

		protected virtual void PopulateStatusList()
		{
			StatusList.DataSource = IoC.Resolve<IProductStatusSecureService>().GetAll();
			StatusList.DataBind();
		}

		protected virtual void PopulateStockStatusList()
		{
			var values = Enum.GetValues(typeof (StockStatus));
			var items = new ListItem[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				var value = values.GetValue(i);
				items[i] = new ListItem(value.ToString(), ((int) value).ToString(CultureInfo.CurrentCulture));
			}
			StockStatusList.Items.AddRange(items);
			StockStatusList.DataBind();
		}

		protected virtual void BindPackageInfo()
		{
			// Create mode.
			if (!PackageId.HasValue)
			{
				PackageHeader.Text = Resources.Lekmer.Literal_CreatePackage;
				NumberInStockBox.Text = "0";
				MasterProductLink.Text = "-";
				DeleteButton.Visible = false;
				PackageProducts.Products = new Collection<ProductStateItem>();

				BindTranslations(0, new Collection<ITranslationGeneric>(), new Collection<ITranslationGeneric>(), new Collection<ITranslationGeneric>());
				BindAliasTranslations(0, AliasTranslationService.GetAllByAlias(0));
				BindCategories(null);
				BindProductRegistries(null);

				State.Package = null;
			}
			// Edit mode.
			else
			{
				if (!(Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false))
				{
					RefreshDiv.Visible = true;
				}

				var package = PackageService.GetByIdSecure(ChannelHelper.CurrentChannel.Id, PackageId.Value);
				State.Package = package;
				var masterProduct = package.MasterProduct;

				PackageHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", Resources.Lekmer.Literal_EditPackage, masterProduct.DisplayTitle);
				TitleBox.Text = masterProduct.Title;
				WebShopTitleBox.Text = masterProduct.WebShopTitle;
				UseWebShopTitleCheckBox.Checked = !string.IsNullOrEmpty(masterProduct.WebShopTitle);
				OverrideGeneralInfo.Checked = package.GeneralInfo != null;
				BindCategories(masterProduct.CategoryId);
				ErpIdBox.Text = masterProduct.ErpId;
				NumberInStockBox.Text = masterProduct.NumberInStock.ToString(CultureInfo.InvariantCulture);
				MasterProductLink.Text = masterProduct.DisplayTitle;
				MasterProductLink.NavigateUrl = PathHelper.Assortment.Product.GetEditUrlForDefault(masterProduct.Id);
				LongArticleDescriptionEditor.SetValue(((IProductRecord) masterProduct).Description);
				GeneralInfoEditor.SetValue(package.GeneralInfo);

				var alias = ((ILekmerAliasSecureService) AliasService).GetByCommonName(string.Format(DefaultAliasCommonName, package.PackageId));
				if (alias != null)
				{
					DefaultAliasEditor.SetValue(alias.Value);
					BindAliasTranslations(alias.Id, AliasTranslationService.GetAllByAlias(alias.Id));
				}
				else
				{
					BindAliasTranslations(0, AliasTranslationService.GetAllByAlias(0));
				}

				var status = StatusList.Items.FindByValue(masterProduct.ProductStatusId.ToString(CultureInfo.CurrentCulture));
				if (status != null)
				{
					status.Selected = true;
					CurrentStatus.Text = status.Text;
				}

				var stockStatus = StockStatusList.Items.FindByValue(((ILekmerProduct)masterProduct).StockStatusId.ToString(CultureInfo.CurrentCulture));
				if (stockStatus != null)
				{
					stockStatus.Selected = true;
				}

				BindTranslations(masterProduct.Id,
					DescriptionTranslationService.GetAllByProduct(masterProduct.Id),
					WebShopTitleTranslationService.GetAllByProduct(masterProduct.Id),
					PackageService.GetAllGeneralInfoTranslationsByPackage(PackageId.Value));

				// Package products.
				var productStateItems = new Collection<ProductStateItem>();
				var productSecureService = (LekmerProductSecureService)IoC.Resolve<IProductSecureService>();
				var productCollection = productSecureService.GetAllByPackage(ChannelHelper.CurrentChannel.Id, PackageId.Value);
				foreach (var item in productCollection)
				{
					productStateItems.Add(new ProductStateItem{Id = Guid.NewGuid().ToString(), ProductItem = item});
				}
				PackageProducts.Products = productStateItems;

				// Price.
				State.PriceListItems = PriceListItemService.GetAllByProduct(masterProduct.Id);

				// Restrictions.
				BindProductRegistries(PackageProducts.Products);

				FillInStatus(PackageProducts.Products);
				FillInNumberInStock(PackageProducts.Products);
			}

			ShowTitle();
			ShowGeneralPackageInfo();
			BindPriceListItems();
			BindLanguages();

			UseWebShopTitleCheckBox.Attributes.Add("onClick", "ShowHide('"
											+ UseWebShopTitleCheckBox.ClientID + "','"
											+ WebShopTitleBox.ClientID + "','"
											+ WebShopTitleTranslator.TriggerImageButton.ClientID + "','"
											+ TitleBox.ClientID + "');");
			OverrideGeneralInfo.Attributes.Add("onClick", "OverrideGeneralInfo('" + OverrideGeneralInfo.ClientID + "','" + generalPackageInfo.ClientID + "');");
		}

		protected virtual void BindLanguages()
		{
			LanguagesRepeater.DataSource = DescTranslationLanguages;
			LanguagesRepeater.DataBind();
		}

		protected virtual void BindTranslations(int productId, Collection<ITranslationGeneric> descriptionTranslations, Collection<ITranslationGeneric> webShopTitleTranslations, Collection<ITranslationGeneric> generalInfoTranslations)
		{
			ProductDescriptionTranslator.DefaultValueControlClientId = LongArticleDescriptionEditor.ClientID;
			ProductDescriptionTranslator.BusinessObjectId = productId;
			ProductDescriptionTranslator.DataSource = descriptionTranslations;
			ProductDescriptionTranslator.DataBind();

			if (webShopTitleTranslations != null)
			{
				WebShopTitleTranslator.DefaultValueControlClientId = WebShopTitleBox.ClientID;
				WebShopTitleTranslator.BusinessObjectId = productId;
				WebShopTitleTranslator.DataSource = webShopTitleTranslations;
				WebShopTitleTranslator.DataBind();
			}

			if (generalInfoTranslations != null)
			{
				GeneralInfoTranslator.DefaultValueControlClientId = GeneralInfoEditor.ClientID;
				GeneralInfoTranslator.BusinessObjectId = productId;
				GeneralInfoTranslator.DataSource = generalInfoTranslations;
				GeneralInfoTranslator.DataBind();
			}
		}

		protected virtual void BindAliasTranslations(int aliasId, Collection<IAliasTranslation> aliasTranslations)
		{
			var translations = new Collection<ITranslationGeneric>();
			foreach (var aliasTranslation in aliasTranslations)
			{
				var translationGeneric = IoC.Resolve<ITranslationGeneric>();
				translationGeneric.Id = aliasTranslation.AliasId;
				translationGeneric.LanguageId = aliasTranslation.Language.Id;
				translationGeneric.Value = aliasTranslation.Value;

				translations.Add(translationGeneric);
			}

			DefaultAliasTranslator.DefaultValueControlClientId = DefaultAliasEditor.ClientID;
			DefaultAliasTranslator.BusinessObjectId = aliasId;
			DefaultAliasTranslator.DataSource = translations;
			DefaultAliasTranslator.DataBind();
		}

		protected virtual void BindCategories(int? categoryId)
		{
			if (categoryId != null)
			{
				PackageCategoryNodeSelector.SelectedNodeId = categoryId;
			}

			PackageCategoryNodeSelector.DataSource = CategoryService.GetTree(categoryId);
			PackageCategoryNodeSelector.DataBind();
			PackageCategoryNodeSelector.PopulatePath(categoryId);
		}

		protected virtual void BindPriceListItems()
		{
			PopulatePriceLists();

			PriceListsGrid.DataSource = State.PriceListItems;
			PriceListsGrid.DataBind();

			upPrices.Update();
			upRestrictions.Update();
		}

		protected virtual void PopulatePriceLists()
		{
			foreach (var item in State.PriceListItems)
			{
				if (State.PriceLists.ContainsKey(item.PriceListId)) continue;

				var priceList = PriceListService.GetById(item.PriceListId);
				if (priceList == null) continue;

				State.PriceLists.Add(item.PriceListId, priceList);
			}
		}

		protected virtual void FillInStatus(Collection<ProductStateItem> products)
		{
			var offlineProduct = products.FirstOrDefault(p => p.ProductItem.ProductStatusId == (int)Lekmer.Product.ProductStatus.Offline);
			if (offlineProduct != null)
			{
				StatusList.Enabled = false;
				StatusList.SelectedValue = ((int)Lekmer.Product.ProductStatus.Offline).ToString(CultureInfo.InvariantCulture);
				return;
			}

			var readyToTranslateProduct = PackageProducts.Products.FirstOrDefault(p => p.ProductItem.ProductStatusId == (int)Lekmer.Product.ProductStatus.ReadyToTranslate);
			if (readyToTranslateProduct != null)
			{
				StatusList.Enabled = false;
				StatusList.SelectedValue = ((int)Lekmer.Product.ProductStatus.ReadyToTranslate).ToString(CultureInfo.InvariantCulture);
				return;
			}

			StatusList.Enabled = true;
		}

		protected virtual void FillInNumberInStock(Collection<ProductStateItem> products)
		{
			int? min = null;

			IEnumerable<IGrouping<int, ProductStateItem>> groupedProducts = products.GroupBy(p => p.ProductItem.Id);
			foreach (IGrouping<int, ProductStateItem> groupedProductStateItems in groupedProducts)
			{
				int numberInStock = CalculateNumberInStock(groupedProductStateItems.First().ProductItem) / groupedProductStateItems.Count();
				if (!min.HasValue || numberInStock < min.Value)
				{
					min = numberInStock;
				}
			}

			NumberInStockBox.Text = !min.HasValue ? "0" : min.Value.ToString(CultureInfo.InvariantCulture);
		}

		protected virtual void FillInPrices(Collection<ProductStateItem> products)
		{
			RefreshDiv.Visible = false;

			State.PriceListItems.Clear();

			if (products == null || products.Count <= 0)
			{
				BindPriceListItems();
				return;
			}

			// set default price list items
			var priceListItems = PriceListItemService.GetAllByProduct(products[0].ProductItem.Id);
			if (priceListItems != null)
			{
				// Add new price list items
				foreach (var item in priceListItems)
				{
					if (State.PriceListItems.FirstOrDefault(pli => pli.PriceListId == item.PriceListId) == null)
					{
						State.PriceListItems.Add(new PriceListItem {PriceListId = item.PriceListId, PriceExcludingVat = 0, PriceIncludingVat = 0, VatPercentage = 0});
					}
				}
			}
			else
			{
				BindPriceListItems();
				return;
			}

			var priceListIdsToRemove = new Dictionary<int,int>();

			foreach (var product in products)
			{
				var productPriceListItems = PriceListItemService.GetAllByProduct(product.ProductItem.Id);
				if (productPriceListItems != null)
				{
					// Calculate prices
					foreach (var item in State.PriceListItems)
					{
						if (priceListIdsToRemove.ContainsKey(item.PriceListId)) continue;

						var productPriceListItem = productPriceListItems.FirstOrDefault(pli => pli.PriceListId == item.PriceListId);
						if (productPriceListItem != null)
						{
							item.PriceIncludingVat += productPriceListItem.PriceIncludingVat;
							item.PriceExcludingVat += productPriceListItem.PriceExcludingVat;
						}
						else
						{
							priceListIdsToRemove.Add(item.PriceListId, item.PriceListId);
						}
					}
				}
				else
				{
					State.PriceListItems.Clear();
					BindPriceListItems();
					return;
				}
			}

			// remove price lists which not exists in products
			foreach (var priceListItem in priceListIdsToRemove)
			{
				State.PriceListItems.Remove(State.PriceListItems.FirstOrDefault(pli => pli.PriceListId == priceListItem.Key));
			}

			// set vat percentage for price list items
			foreach (var priceListItem in State.PriceListItems)
			{
				priceListItem.VatPercentage = PriceCalc.VatPercentage2(priceListItem.PriceIncludingVat, priceListItem.PriceExcludingVat) * 100;
			}

			BindPriceListItems();
		}

		protected virtual void BindProductRegistries(Collection<ProductStateItem> products)
		{
			if (products != null)
			{
				List<int> productIds = products.Select(p => p.ProductItem.Id).ToList();
				if (PackageId != null && PackageId > 0)
				{
					productIds.Insert(0, State.Package.MasterProduct.Id);
				}

				foreach (var productsId in productIds)
				{
					foreach (var registryId in ProductRegistryProductService.GetRestrictionsByProductId(productsId))
					{
						if (!_productRegistryRestrictions.ContainsKey(registryId))
						{
							_productRegistryRestrictions.Add(registryId, registryId);
						}
					}
				}
			}

			if (State.ProductRegistries == null)
			{
				State.ProductRegistries = IoC.Resolve<IProductRegistrySecureService>().GetAll();
			}
			ProductRegistryGrid.DataSource = State.ProductRegistries;
			ProductRegistryGrid.DataBind();

			upPrices.Update();
			upRestrictions.Update();
		}

		protected virtual Collection<ITranslationGeneric> GetGeneralInfoTranslations(IPackage package)
		{
			var generalInfoTranslations = GeneralInfoTranslator.GetTranslations();

			if (OverrideGeneralInfo.Checked)
			{
				package.GeneralInfo = GeneralInfoEditor.GetValue() ?? string.Empty;
			}
			else
			{
				package.GeneralInfo = null;
				foreach (var generalInfoTranslation in generalInfoTranslations)
				{
					generalInfoTranslation.Value = null;
				}
			}

			return generalInfoTranslations;
		}

		private void ManageRestrictionProduct(IEnumerable<IProductRegistryRestrictionItem> productRegistryRestrictions, int productId)
		{
			var oldRestrictions = _productRegistryRestrictions;
			var newRestrictions = productRegistryRestrictions.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);

			UpdateProductRegistryProduct(oldRestrictions, newRestrictions, productId);
		}

		private void UpdateProductRegistryProduct(Dictionary<int, int> oldRestrictions, Dictionary<int, int> newRestrictions, int productId)
		{
			var toRemoveRestrictions = oldRestrictions.Where(oldRestriction => !newRestrictions.ContainsKey(oldRestriction.Key)).ToDictionary(oldRestriction => oldRestriction.Key, oldRestriction => oldRestriction.Key);
			var toAddRestrictions = newRestrictions.Where(newRestriction => !oldRestrictions.ContainsKey(newRestriction.Key)).ToDictionary(newRestriction => newRestriction.Key, newRestriction => newRestriction.Key);

			var productRegistryProductSecureService = IoC.Resolve<IProductRegistryProductSecureService>();

			if (toAddRestrictions.Count > 0)
			{
				productRegistryProductSecureService.DeleteByProduct(SignInHelper.SignedInSystemUser, productId, toAddRestrictions.Keys.ToList());
			}

			if (toRemoveRestrictions.Count > 0)
			{
				productRegistryProductSecureService.Insert(SignInHelper.SignedInSystemUser, productId, null, null, toRemoveRestrictions.Keys.ToList());
			}
		}

		protected virtual int CalculateNumberInStock(IProduct product)
		{
			var numberInStock = 0;

			var lekmerProduct = (ILekmerProduct) product;
			if (lekmerProduct.HasSizes)
			{
				var productSizes = ProductSizeService.GetAllByProduct(lekmerProduct.Id);
				if (productSizes != null && productSizes.Count > 0)
				{
					numberInStock = productSizes.Where(s => s.NumberInStock > 0).Sum(s => s.NumberInStock);
				}
			}
			else
			{
				numberInStock = lekmerProduct.NumberInStock;
			}

			return numberInStock;
		}

		protected virtual bool ValidateInfo(int masterProductId, IEnumerable<IProductUrl> productUrls)
		{
			bool isValid = true;

			var title = UseWebShopTitleCheckBox.Checked ? WebShopTitleBox.Text : TitleBox.Text;

			if (title.IsNullOrEmpty())
			{
				isValid = false;
				Messager.Add(Resources.GeneralMessage.TitleEmpty);
			}

			if (!PackageCategoryNodeSelector.SelectedNodeId.HasValue || PackageCategoryNodeSelector.CategoryPath.IsNullOrEmpty())
			{
				isValid = false;
				Messager.Add(Resources.GeneralMessage.CategoryEmpty);
			}

			if (PackageProducts.Products.Count > 10)
			{
				isValid = false;
				Messager.Add(Resources.LekmerMessage.Package_ProductsLimitError);
			}

			if (PackageProducts.Products.FirstOrDefault(p => p.ProductItem.IsPackage()) != null)
			{
				isValid = false;
				Messager.Add(Resources.LekmerMessage.Package_ProductsContainsPackage);
			}

			foreach (var productUrl in productUrls)
			{
				if (!ProductUrlService.Check(productUrl))
				{
					isValid = false;
					Messager.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.ProductEdit_UrlTitleExists, Languages.First(i => i.Id == productUrl.LanguageId).Title));
				}
			}

			return isValid;
		}

		private Collection<IProductRegistryRestrictionItem> GetRegistryRestrictions()
		{
			var productRegistryRestrictionProductList = new Collection<IProductRegistryRestrictionItem>();
			var productRegistryRestrictionProductSecureService = IoC.Resolve<IProductRegistryRestrictionProductSecureService>();

			foreach (GridViewRow row in ProductRegistryGrid.Rows)
			{
				var restrictionCheckBox = (CheckBox)row.FindControl("RestrictionCheckBox");
				if (restrictionCheckBox != null && restrictionCheckBox.Checked)
				{
					var productRegistryIdHidden = (HiddenField)row.FindControl("ProductRegistryIdHidden");
					if (productRegistryIdHidden != null && !string.IsNullOrEmpty(productRegistryIdHidden.Value))
					{
						int productRegistryId = Convert.ToInt32(productRegistryIdHidden.Value);
						if (productRegistryRestrictionProductList.FirstOrDefault(r => r.ProductRegistryId == productRegistryId) == null)
						{
							var productRegistryRestrictionProduct = productRegistryRestrictionProductSecureService.Create();
							productRegistryRestrictionProduct.ProductRegistryId = productRegistryId;
							productRegistryRestrictionProduct.RestrictionReason = "One or more include products restricted in this channel";
							productRegistryRestrictionProduct.UserId = SignInHelper.SignedInSystemUser.Id;

							productRegistryRestrictionProductList.Add(productRegistryRestrictionProduct);
						}
					}
				}
			}

			return productRegistryRestrictionProductList;
		}

		protected virtual IPriceList GetPriceList(int id)
		{
			var priceList = State.PriceLists[id];
			return priceList ?? PriceListService.GetById(id);
		}

		protected virtual string GetPriceListStatus(int priceListStatusId)
		{
			if (_statuses == null)
			{
				_statuses = IoC.Resolve<IPriceListStatusSecureService>().GetAll();
			}

			var priceListStatus = _statuses.FirstOrDefault(item => item.Id == priceListStatusId);
			return priceListStatus != null ? priceListStatus.Title : string.Empty;
		}

		protected virtual string GetPrice(int priceListId, decimal price)
		{
			var currencyId = GetPriceList(priceListId).CurrencyId;
			var currency = IoC.Resolve<ICurrencySecureService>().GetById(currencyId);
			return IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, currency, price);
		}

		protected virtual string RemoveLastSeperator(string commonDescription)
		{
			var lastSeperator = commonDescription.LastIndexOf(DescriptionSeperator, StringComparison.Ordinal);
			if (lastSeperator > 0)
			{
				commonDescription = commonDescription.Substring(0, lastSeperator);
			}
			return commonDescription;
		}

		protected virtual void ShowTitle()
		{
			if (UseWebShopTitleCheckBox.Checked)
			{
				TitleBox.Attributes.Add("style", "display : none;");
				WebShopTitleBox.Attributes.Add("style", "display : block;");
				WebShopTitleTranslator.TriggerImageButton.Attributes.Add("style", "display : block;");
			}
			else
			{
				TitleBox.Attributes.Add("style", "display : block;");
				WebShopTitleBox.Attributes.Add("style", "display : none;");
				WebShopTitleTranslator.TriggerImageButton.Attributes.Add("style", "display : none;");
			}
		}

		protected virtual void ShowGeneralPackageInfo()
		{
			if (!OverrideGeneralInfo.Checked)
			{
				generalPackageInfo.Style["display"] = "none";
			}
			else
			{
				generalPackageInfo.Style["display"] = "block";
			}
		}

		protected virtual Collection<IProductUrl> PrepareProductUrls(int productId, IEnumerable<ITranslationGeneric> webShopTitleTranslations)
		{
			var productUrls = new Collection<IProductUrl>();

			if (!UseWebShopTitleCheckBox.Checked)
			{
				if (!TitleBox.Text.IsNullOrEmpty())
				{
					var urlTitle = UrlCleaner.CleanUpProductUrlTitle(TitleBox.Text);
					foreach (var language in Languages)
					{
						var productUrl = ProductUrlService.Create(productId, language.Id, urlTitle);
						productUrls.Add(productUrl);
					}
				}
			}
			else
			{
				if (!WebShopTitleBox.Text.IsNullOrEmpty())
				{
					var defaultUrlTitle = UrlCleaner.CleanUpProductUrlTitle(WebShopTitleBox.Text);
					foreach (var title in webShopTitleTranslations)
					{
						var urlTitle = title.Value.IsNullOrEmpty() ? defaultUrlTitle : UrlCleaner.CleanUpProductUrlTitle(title.Value);
						var productUrl = ProductUrlService.Create(productId, title.LanguageId, urlTitle);
						productUrls.Add(productUrl);
					}
				}
			}

			return productUrls;
		}

		protected virtual void SaveProductUrls(int productId, Collection<IProductUrl> productUrls)
		{
			foreach (var productUrl in productUrls)
			{
				productUrl.ProductId = productId;
			}

			ProductUrlService.Save(SignInHelper.SignedInSystemUser, productId, productUrls);
		}
	}

	[Serializable]
	public class PackageState
	{
		public Collection<IProductRegistry> ProductRegistries { get; set; }
		public IPackage Package { get; set; }
		public Dictionary<int, IPriceList> PriceLists { get; set; }
		public Collection<IPriceListItem> PriceListItems { get; set; }
	}
}