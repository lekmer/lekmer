﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Messages
{
	public partial class Default : LekmerPageController
	{
		private Collection<IContentMessageFolder> _allFolders;

		public virtual ContentMessageMaster MasterPage
		{
			get { return (ContentMessageMaster) Master; }
		}

		protected virtual bool IsSearchResult
		{
			get { return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false; }
		}

		protected override void SetEventHandlers()
		{
			ContentMessagesGrid.RowCommand += OnRowCommand;
			ContentMessagesGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateContentMessages();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteContentMessage":
					DeleteContentMessage(id);
					SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
					break;
			}

			PopulateContentMessages();
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ContentMessagesGrid.PageIndex = e.NewPageIndex;

			PopulateContentMessages();
		}

		protected virtual void DeleteContentMessage(int contentMessageId)
		{
			IoC.Resolve<IContentMessageSecureService>().Delete(SignInHelper.SignedInSystemUser, contentMessageId);
		}

		protected virtual void PopulateContentMessages()
		{
			if (IsSearchResult)
			{
				ContentMessagesGrid.DataSource = SearchContentMessages();
				ContentMessagesGrid.Columns[1].Visible = true;
			}
			else
			{
				ContentMessagesGrid.DataSource = RetrieveContentMessages();
				ContentMessagesGrid.Columns[1].Visible = false;
			}

			ContentMessagesGrid.DataBind();

			ContentMessagesGrid.Visible = ContentMessagesGrid.DataSource != null;
		}

		protected virtual string GetEditUrl(object contentMessageId)
		{
			return LekmerPathHelper.ContentMessage.GetEditUrl(System.Convert.ToInt32(contentMessageId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<IContentMessage> RetrieveContentMessages()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return IoC.Resolve<IContentMessageSecureService>().GetAllByFolder(folderId.Value);
			}

			return null;
		}

		protected virtual Collection<IContentMessage> SearchContentMessages()
		{
			var searchUrl = LekmerPathHelper.ContentMessage.GetSearchResultUrl();

			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);

			if (string.IsNullOrEmpty(searchCriteria))
			{
				return new Collection<IContentMessage>();
			}

			return IoC.Resolve<IContentMessageSecureService>().Search(searchCriteria);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<IContentMessageFolderSecureService>();
				_allFolders = service.GetAll();
			}

			var pathHelper = new FolderPathHelper();
			return pathHelper.GetPath(_allFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}
	}
}