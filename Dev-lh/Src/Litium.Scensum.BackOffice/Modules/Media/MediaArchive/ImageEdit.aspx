<%@ Page Language="C#" AutoEventWireup="false" MasterPageFile ="~/Modules/Media/MediaArchive/Media.Master"  CodeBehind="ImageEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.MediaArchive.ImageEdit" %>
<%@ MasterType VirtualPath="~/Modules/Media/MediaArchive/Media.Master" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
				<uc:MessageContainer ID="messager" MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
				<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="Image"  />
    </asp:Content>
<asp:Content ID="MediaTabContent" ContentPlaceHolderID="MediaContent"  runat="server">
    <script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>
    
    <script type="text/javascript">
        $(function() {
            media_url = '<%=ResolveUrl("~/Media") %>';
            $('div.image-show-lightbox a').lightBox();
        });
    </script>
    
    <div class="media-item-main">
        <div class="media-item-image-wrapper">
            <div id="ImageDiv" runat="server" class="media-type-image">
                <a id="ZoomImageAnchor" runat="server" target="_blank">
                    <img id="MediaItemImage" class="media-item-image" runat="server" />                
                    <img id="ZoomImage" class="zoom-image"  runat="server" />
                </a>
            </div>
            <div id="DownloadDocumentDiv" runat="server" class="media-download-document">
				<asp:HyperLink ID="DocumentLink" runat="server" Text="Download document" Font-Underline="true" ForeColor="#004CD5"></asp:HyperLink>
            </div>
        </div>

		<div class="media-item-information-edit-block">
			<span><asp:Label ID="ImageTitleCaptionLabel" runat="server" Text="<%$ Resources:General, Literal_Title %>"/> * </span>
            <asp:RequiredFieldValidator ID="ImageItemTitleValidator" ControlToValidate="ImageTitleTextBox" Display="None" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty%>" ValidationGroup="Image" runat="server" ></asp:RequiredFieldValidator> <br />
            <asp:TextBox ID="ImageTitleTextBox" runat="server" CssClass="media-item-information-edit-value" />
            <br />
            <div id="ImageAlternateTextDiv" runat="server">
				<asp:Label ID="ImageAlternateTitleLabel" runat="server" Text="<%$ Resources:Media, Label_ImageAlternativeText%>" /><br />
				<asp:TextBox ID="AlternateTitleTextBox" runat="server" CssClass="media-item-information-edit-value" />
				<br />
			</div>
            <span><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:General, Literal_PlaceInFolder%>" /></span><br />
			<span class="media-item-information-value">
				<uc:NodeSelect ID="NodeSelectorControl" runat="server" />
			</span>
		</div>
		   
        <div id="image-details-wrapper" class="media-item-block">
            <div class="treeview-header"><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Details%>" /></div>
            <div class="media-item-information">
				<div class="media-item-information-block">
                    <asp:Label ID="FileNameCaptionLabel" runat="server" Text="<%$ Resources:Media, Literal_FileName%>" Font-Italic="true" /><br />
                    <asp:Label ID="FileNameLabel" runat="server" CssClass="media-item-information-value" />
                </div>
            
                <div class="media-item-information-block">
                    <asp:Label ID="ImageTypeCaptionLabel" runat="server" Text="<%$ Resources:Media, Literal_Type%>" Font-Italic="true" /><br />
                    <asp:Label ID="ImageTypeLabel" runat="server" CssClass="media-item-information-value" />
                </div>
                
                <div id="ImageSizeDiv" runat="server" class="media-item-information-block">
                    <asp:Label ID="ImageDimensionLabel" runat="server" Text="<%$ Resources:Media, Literal_Size%>" Font-Italic="true" /><br />
                    <asp:Label ID="ImageSizeLabel" runat="server" CssClass="media-item-information-value" />
                </div>
                
                <div class="media-item-information-block">
                    <asp:Label ID="MediaSize" runat="server" Text="<%$ Resources:Media, Literal_FileSize%>" Font-Italic="true" /><br />
                    <asp:Label ID="MediaFileSize" runat="server" CssClass="media-item-information-value" />
                </div>
            </div>
        </div>
    </div>
    <br/>
    <div class="button-left">
		<uc:ImageLinkButton ID="DeleteButton" runat="server" SkinID="DefaultButton" Text="Delete" />
    </div>
    <div class="media-item-button-wrapper">
        <uc:ImageLinkButton ID="SaveButton" runat="server" ValidationGroup="Image" SkinID="DefaultButton" Text="<%$ Resources:General, Button_Save%>" />
        <uc:ImageLinkButton ID="CancelButton" runat="server" SkinID="DefaultButton" Text="<%$ Resources:General, Button_Cancel%>" />
    </div>   
</asp:Content>