<%@ page title="" language="C#" masterpagefile="~/Modules/Media/MediaArchive/Media.Master" autoeventwireup="false" codebehind="Upload.aspx.cs" inherits="Litium.Scensum.BackOffice.Modules.Media.MediaArchive.Upload" %>

<%@ register tagprefix="ajaxToolkit" namespace="AjaxControlToolkit" assembly="AjaxControlToolkit" %>
<%@ register assembly="Litium.Scensum.Web.Controls" namespace="Litium.Scensum.Web.Controls.Tree"
	tagprefix="CustomControls" %>

<%@ register src="~/UserControls/Media/FileDrop/FileDrop.ascx" tagname="FileDrop" tagprefix="uc" %>

<asp:content id="Content1" contentplaceholderid="MessageContainer" runat="server">	
		<script src="<%=ResolveUrl("~/Media/Scripts/dropzone.js") %>" type="text/javascript"></script>
		
		<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="UploadButton" runat="server" />
</asp:content>
<asp:content id="UploadContent" contentplaceholderid="MediaContent" runat="server">
	<asp:Panel ID="UploadPanel" runat="server">
		<br /><br />
		<div class="column">
			<br />
			<uc:FileDrop ID="FileDropControl" runat="server" UseRootNodeInPath="true" />

		</div>
	</asp:Panel>
</asp:content>
