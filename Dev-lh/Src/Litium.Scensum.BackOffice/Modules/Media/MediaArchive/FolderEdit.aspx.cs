using System;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class FolderEdit : LekmerPageController,IEditor 
	{
		protected virtual bool? IsSuccessMessage()
		{
			return Request.QueryString.GetBooleanOrNull("HasMessage");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			Media.RegisterStartupScript(this, GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", PutMediaTree.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", PutMediaTree.ClientID), true);
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			PutMediaTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateForm()
		{
			var mediaFolderSecureService = IoC.Resolve<IMediaFolderSecureService>();
			if (((Media)Master).SelectedFolderId!=null)
			{
				IMediaFolder mediaFolder = mediaFolderSecureService.GetById(((Media)Master).SelectedFolderId.Value);
				TitleTextBox.Text = mediaFolder.Title;
			}		
			PopulateTreeView(null);

			if (IsSuccessMessage() == null || !((bool)IsSuccessMessage())) return;
			ShowSuccessMessage();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Media.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var mediaFolderSecureService = IoC.Resolve<IMediaFolderSecureService>();

			IMediaFolder mediaFolder = mediaFolderSecureService.GetById((int)((Media)Page.Master).SelectedFolderId);
			if (mediaFolder == null)
			{
				throw new BusinessObjectNotExistsException((int)((Media)Page.Master).SelectedFolderId);
			}
			mediaFolder.Title = TitleTextBox.Text.Trim();
			if (PutMediaTree.SelectedNodeId.HasValue)
			{
				mediaFolder.MediaFolderParentId = PutMediaTree.SelectedNodeId.Value == PutMediaTree.RootNodeId ? null : PutMediaTree.SelectedNodeId;
			}
			try
			{
				mediaFolderSecureService.Save(SignInHelper.SignedInSystemUser, mediaFolder);
			}
			catch (ArgumentException exception)
			{
                messager.Add(exception.Message);
				return;
			}

			((Media)Page.Master).PopulateTreeView(mediaFolder.Id);
			messager.Add(Resources.GeneralMessage.SaveSuccessMediaFolder);
             messager.MessageType = InfoType.Success;
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var master = (Media) Page.Master;
			var folders = master.GetMediaFolders(folderId ?? master.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;
			var editedFolder = folders.FirstOrDefault(f => f.Id == master.SelectedFolderId);

			PutMediaTree.DataSource = editedFolder != null
				? PutMediaTree.GetFilteredSource(folders, editedFolder.Id)
				: folders;
			PutMediaTree.RootNodeTitle = Resources.Media.Literal_Media;
			PutMediaTree.DataBind();
			PutMediaTree.SelectedNodeId = folderId ?? (editedFolder != null ? editedFolder.ParentId : PutMediaTree.RootNodeId) ?? PutMediaTree.RootNodeId;
		}

		private void ShowSuccessMessage()
		{
			messager.Add(Resources.GeneralMessage.SaveSuccessMediaFolder);
			messager.MessageType = InfoType.Success;
		}
	}
}
