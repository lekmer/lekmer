using System.Globalization;
using System.IO;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Media
{
	public class MediaOriginalLoader : MediaLoaderBase
	{
		protected override Stream GetMedia(string mediaId, string extension, out string mime)
		{
			int id;
			if (!int.TryParse(mediaId, out id))
			{
				mime = string.Empty;
				return null;
			}
			return IsImage(extension) 
				? IoC.Resolve<IImageSecureService>().LoadImage(id, extension, out mime)
				: IoC.Resolve<IMediaItemSecureService>().LoadMediaItem(id, extension, out mime);
		}

		protected override string GetETag(string mediaId)
		{
			return mediaId.ToString(CultureInfo.CurrentCulture);
		}
	}
}
