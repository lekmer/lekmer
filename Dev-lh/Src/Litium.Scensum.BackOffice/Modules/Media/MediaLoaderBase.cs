using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Scensum.BackOffice.Modules.Media
{
	//[DebuggerStepThrough]
	public abstract class MediaLoaderBase : IHttpHandler
	{
		private const int BufferSizeBytes = 8*1024;

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var mediaId = context.Request.QueryString["MediaId"];
			var extension = context.Request.QueryString["Extension"];

			LoadMedia(context, mediaId, extension);
		}

		private void LoadMedia(HttpContext context, string mediaId, string extension)
		{
			if (string.IsNullOrEmpty(extension) && !string.IsNullOrEmpty(mediaId)) throw new ArgumentNullException("extension");

			string etag = GetETag(mediaId);
			var lastModified = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			if (HasMatchingETag(context, etag))
			{
				SendNotModifiedHeader(context);
				return;
			}

			if (HasMatchingLastModified(context, lastModified))
			{
				SendNotModifiedHeader(context);
				return;
			}

			string mime;
			using (Stream mediaStream = GetMedia(mediaId, extension, out mime))
			{
				if (mediaStream == null)
				{
					throw new HttpException(404, "Page not found");
				}

				context.Response.Cache.SetLastModified(lastModified);
				context.Response.Cache.SetETag(etag);
				context.Response.ContentType = mime;
				context.Response.AppendHeader("Content-Disposition", "attachment; filename=" + mediaId + "." + extension);

				StreamAppender.AppendToStream(mediaStream, context.Response.OutputStream, BufferSizeBytes);
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
		protected abstract Stream GetMedia(string mediaId, string extension, out string mime);

		protected abstract string GetETag(string mediaId);

		private static bool HasMatchingETag(HttpContext context, string etag)
		{
			string ifNoneMatchHeader = context.Request.Headers["If-None-Match"];
			return ifNoneMatchHeader != null && ifNoneMatchHeader.Equals(etag);
		}

		private static bool HasMatchingLastModified(HttpContext context, DateTime lastModified)
		{
			string lastModifiedHeader = context.Request.Headers["If-Modified-Since"];
			if (lastModifiedHeader == null) return false;

			DateTime clientLastModified = DateTime.Parse(lastModifiedHeader, CultureInfo.CurrentCulture);
			return clientLastModified.ToUniversalTime() == lastModified;
		}

		private static void SendNotModifiedHeader(HttpContext context)
		{
			context.Response.StatusCode = 304;
			context.Response.StatusDescription = "Not modified";
		}

		protected virtual bool IsImage(string extension)
		{
			var imageFormats = new List<string> { "jpg", "jpeg", "jpe", "jfif", "gif", "png", "bmp", "dib", "tif", "tiff" };
			return imageFormats.Contains(extension);
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}