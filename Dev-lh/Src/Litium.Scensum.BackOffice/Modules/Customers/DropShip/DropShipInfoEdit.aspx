﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="DropShipInfoEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.DropShip.DropShipInfoEdit" %>

<%@ Import Namespace="Litium.Lekmer.DropShip" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="DropShipInfoEditTab" ContentPlaceHolderID="body" runat="server">
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SaveButton" runat="server" />
	<sc:ToolBoxPanel ID="DropShipInfoToolBoxPanel" runat="server" Text="DropShip information" />

	<div class="stores-place-holder">
		<div class="column" style="width: 50%;">
			<div class="main-caption">
				<span><%=DropShipInfoCaptions.Ordernummer%></span>
				<uc:HyperLinkEncoded ID="OrdernummerLink" runat="server" class="main-caption" style="text-decoration: underline;" />
				<br />
				<span><%=Resources.Lekmer.Literal_SupplierName%></span>:
				<asp:Literal ID="SupplierNameLiteral" runat="server" />
			</div>
		</div>
		<div class="column" style="width: 50%;">
			<div class="column" style="width: 30%;">
				<span><%=DropShipInfoCaptions.Registerad%></span><br />
				<span><%=DropShipInfoCaptions.Kundnummer%></span><br />
				<span><%=DropShipInfoCaptions.Aviseringsnummer%></span><br />
				<span><%=DropShipInfoCaptions.Betalningsmetod%></span><br />
				<span><%=DropShipInfoCaptions.Leveranssätt%></span>
			</div>
			<div class="column">
				<asp:Literal ID="RegisteradLiteral" runat="server" /><br />
				<asp:Literal ID="KundnummerLiteral" runat="server" /><br />
				<asp:Literal ID="AviseringsnummerLiteral" runat="server" /><br />
				<asp:Literal ID="BetalningsmetodLiteral" runat="server" /><br />
				<span><%=DropShipInfoCaptions.Dropship%></span>
			</div>
		</div>

		<br clear="all"/>
		<br clear="all"/>

		<div class="column" style="width: 50%;">
			<div class="main-caption">
				<span><%=DropShipInfoCaptions.Faktureringsadress%></span>
			</div>
			<div class="column" style="width: 30%;">
				<span><%=DropShipInfoCaptions.FirstName%></span><br />
				<span><%=DropShipInfoCaptions.LastName%></span><br />
				<span><%=DropShipInfoCaptions.Company%></span><br />
				<span><%=DropShipInfoCaptions.Co%></span><br />
				<span><%=DropShipInfoCaptions.Street%></span><br />
				<span><%=DropShipInfoCaptions.StreetNr%></span><br />
				<span><%=DropShipInfoCaptions.Entrance%></span><br />
				<span><%=DropShipInfoCaptions.Floor%></span><br />
				<span><%=DropShipInfoCaptions.ApartmentNo%></span><br />
				<span><%=DropShipInfoCaptions.ZipCode%></span><br />
				<span><%=DropShipInfoCaptions.City%></span><br />
				<span><%=DropShipInfoCaptions.Country%></span><br />
				<span><%=DropShipInfoCaptions.Cellphone%></span><br />
				<span><%=DropShipInfoCaptions.Email%></span>
			</div>
			<div class="column">
				<asp:Literal ID="BillingFirstNameLiteral" runat="server" /><br />
				<asp:Literal ID="BillingLastNameLiteral" runat="server" /><br />
				<asp:Literal ID="BillingCompanyLiteral" runat="server" /><br />
				<asp:Literal ID="BillingCoLiteral" runat="server" /><br />
				<asp:Literal ID="BillingStreetLiteral" runat="server" /><br />
				<asp:Literal ID="BillingStreetNrLiteral" runat="server" /><br />
				<asp:Literal ID="BillingEntranceLiteral" runat="server" /><br />
				<asp:Literal ID="BillingFloorLiteral" runat="server" /><br />
				<asp:Literal ID="BillingApartmentNoLiteral" runat="server" /><br />
				<asp:Literal ID="BillingZipCodeLiteral" runat="server" /><br />
				<asp:Literal ID="BillingCityLiteral" runat="server" /><br />
				<asp:Literal ID="BillingCountryLiteral" runat="server" /><br />
				<asp:Literal ID="BillingCellphoneLiteral" runat="server" /><br />
				<asp:Literal ID="BillingEmailLiteral" runat="server" />
			</div>
		</div>
		<div class="column" style="width: 50%;">
			<div class="main-caption">
				<span><%=DropShipInfoCaptions.Leveransadress%></span>
			</div>
			<div class="column" style="width: 30%;">
				<span><%=DropShipInfoCaptions.FirstName%></span><br />
				<span><%=DropShipInfoCaptions.LastName%></span><br />
				<span><%=DropShipInfoCaptions.Company%></span><br />
				<span><%=DropShipInfoCaptions.Co%></span><br />
				<span><%=DropShipInfoCaptions.Street%></span><br />
				<span><%=DropShipInfoCaptions.StreetNr%></span><br />
				<span><%=DropShipInfoCaptions.Entrance%></span><br />
				<span><%=DropShipInfoCaptions.Floor%></span><br />
				<span><%=DropShipInfoCaptions.ApartmentNo%></span><br />
				<span><%=DropShipInfoCaptions.ZipCode%></span><br />
				<span><%=DropShipInfoCaptions.City%></span><br />
				<span><%=DropShipInfoCaptions.Country%></span><br />
				<span><%=DropShipInfoCaptions.Cellphone%></span><br />
				<span><%=DropShipInfoCaptions.Email%></span>
			</div>
			<div class="column">
				<asp:Literal ID="DeliveryFirstNameLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryLastNameLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryCompanyLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryCoLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryStreetLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryStreetNrLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryEntranceLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryFloorLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryApartmentNoLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryZipCodeLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryCityLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryCountryLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryCellphoneLiteral" runat="server" /><br />
				<asp:Literal ID="DeliveryEmailLiteral" runat="server" />
			</div>
		</div>

		<br clear="all"/>
		<br clear="all"/>

		<sc:GridViewWithCustomPager
			ID="DropShipOrderInfoGrid"
			SkinID="grid"
			AutoGenerateColumns="false"
			Width="100%"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			PagerSettings-Mode="NumericFirstLast"
			runat="server">
			<Columns>
				<asp:BoundField DataField="OfferId" ItemStyle-Width="20%" />
				<asp:BoundField DataField="ProductArticleNumber" ItemStyle-Width="11%" />
				<asp:BoundField DataField="ProductTitle" ItemStyle-Width="36%" />
				<asp:BoundField DataField="Quantity" ItemStyle-Width="5%" />
				<asp:BoundField DataField="Price" ItemStyle-Width="8%" />
				<asp:BoundField DataField="ProductDiscount" ItemStyle-Width="10%" />
				<asp:BoundField DataField="ProductSoldSum" ItemStyle-Width="10%" />
			</Columns>
		</sc:GridViewWithCustomPager>
		<sc:GridViewWithCustomPager
			ID="DropShipOrderInfoSummaryGrid"
			SkinID="grid"
			AutoGenerateColumns="false"
			Width="20%"
			HorizontalAlign="Right"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			PagerSettings-Mode="NumericFirstLast"
			runat="server">
			<Columns>
				<asp:BoundField DataField="TotalDiscount" ItemStyle-Width="25%" />
				<asp:BoundField DataField="TotalSum" ItemStyle-Width="25%" />
			</Columns>
		</sc:GridViewWithCustomPager>

		<br class="clear">
		<br class="clear">

		<div class="buttons left">
			<uc:ImageLinkButton ID="SendFilesButton" runat="server" Text="<%$ Resources:Lekmer,Button_SendInfo%>" SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="ViewPdfButton" runat="server" Text="<%$ Resources:Lekmer,Button_GetFile%>" SkinID="DefaultButton" />
		</div>

		<div class="buttons right">
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
		</div>
	</div>
</asp:Content>