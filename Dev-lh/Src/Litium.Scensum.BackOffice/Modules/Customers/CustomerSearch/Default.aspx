<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>
<%@ Page Language="C#" MasterPageFile="~/Modules/Customers/CustomerSearch/Customers.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch.Default" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="CustomerSearch" Src="~/UserControls/Customer/CustomerSearchCriteria.ascx" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="CustomerSearchTab" ContentPlaceHolderID="CustomerPlaceHolder" runat="server">
<asp:UpdatePanel ID="SearchUpdatePanel" runat="server">
	<ContentTemplate>
	<uc:CustomerSearch ID="CustomerSearchCriteriaControl" runat="server" />
	<div id="customer-search-result" >
		<Scensum:Grid runat="server" ID="CustomersGrid" FixedColumns="First name" >
			<sc:GridViewWithCustomPager 
				ID="CustomersSearchGrid" 
				SkinID="grid" 
				runat="server" 
				DataSourceID="CustomerObjectDataSource" 
				Width="100%"				
				AutoGenerateColumns="false"
				AllowPaging="true"
				PageSize="<%$AppSettings:DefaultGridPageSize%>">
				<Columns>
					<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_ErpId %>" DataField="ErpId" ItemStyle-Width="13%" />
					<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Customer_FirstOrCompanyName %>" ItemStyle-Width="15%">
						<ItemTemplate>
							<uc:HyperLinkEncoded runat="server" ID="EditLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Customer.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' Text='<%# Eval("CustomerInformation.FirstName")%>' />
						</ItemTemplate >
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Customer_LastOrFullName %>" ItemStyle-Width="15%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="LastNameLiteral" runat="server" Text='<%# Eval("CustomerInformation.LastName")%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Phone %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="PhoneLiteral" runat="server" Text='<%# Eval("CustomerInformation.PhoneNumber")%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_MobilePhone %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="CellPhoneNumberLiteral" runat="server" Text='<%# Eval("CustomerInformation.CellPhoneNumber")%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_CivicNumber %>" ItemStyle-Width="9%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="CivicNumberLiteral" runat="server" Text='<%# Eval("CustomerInformation.CivicNumber")%>' />
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" ItemStyle-Width="11%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="CreatedDateLiteral" runat="server" Text='<%# Convert.ToDateTime(Eval("CustomerInformation.CreatedDate")).ToShortDateString() %>' /> 
					</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" ItemStyle-Width="15%">
					<ItemTemplate>
						<uc:LiteralEncoded ID="EmailLiteral" runat="server" Text='<%# Eval("CustomerInformation.Email")%>'/>
					</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</sc:GridViewWithCustomPager>
		</Scensum:Grid>
		<asp:ObjectDataSource ID="CustomerObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch.CustomerDataSource" />
	</div>
	</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
