<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.Orders.Default" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="OrdersTabContent" ContentPlaceHolderID="body" runat="server">
	<Scensum:ToolBoxPanel ID="VariationPanelToolBoxPanel" runat="server" Text="<%$ Resources:Customer, Literal_OrderToolBoxPanel %>" ShowSeparator="false">
	</Scensum:ToolBoxPanel>

	<div style="float: left; width:980px; margin-bottom:-20px">
		<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" DisplayMode="BulletList" HeaderText="<%$ Resources:CustomerMessage, OrderSearchErrors %>" ValidationGroup="vgSearchOrder" />
	</div>

	<div id="search-block" style="float: left ">
		<span class="order-header"><%= Resources.General.Literal_Search %></span>
	</div>

	<asp:Panel ID="OrderSearchPanel" runat="server" DefaultButton="SearchButtonOrder">
		<asp:UpdatePanel ID="SearchUpdatePanelOrder" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div id="orderid-search-content" class="customer-content-box customer-caption">
					<label><%= Resources.Order.Literal_OrderInformation %></label>
					<br />
					<div class="left">
						<div class="column">
							<div class="input-box">
								<span><%= Resources.Order.Literal_OrderId %></span>
								<br />
								<asp:TextBox ID="OrderIdTextBox" runat="server" />
								<asp:RangeValidator runat="server" ID="OrderIdValidator" ControlToValidate="OrderIdTextBox"
									ErrorMessage="<%$ Resources:OrderMessage, OrderIdNumberOutOfRange %>" Text="<%$ Resources:OrderMessage, OrderIdNumberOutOfRange %>"
									MinimumValue="1" MaximumValue="2147483647" Type="Integer" ValidationGroup="vgSearchOrder" />
							</div>
						</div>
					</div>
					<div class="right">
						<br />
						<div id="SearchButtonDiv" class="buttons right" style="margin-right: 7px; margin-top: 5px;">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SearchButtonOrder" Text="<%$ Resources:General, Button_Search %>" runat="server" ValidationGroup="vgSearchOrder" SkinID="DefaultButton" />
						</div>
					</div>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="SearchButtonOrder" EventName="Click" />
			</Triggers>
		</asp:UpdatePanel>
	</asp:Panel>

	<asp:Panel ID="CustomerSearchPanel" runat="server" DefaultButton="SearchButton">
		<asp:UpdatePanel ID="SearchUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div id="order-search-content" class="customer-content-box customer-caption">
					<label><%= Resources.Customer.Literal_CustomerInformation %></label>
					<br />
					<div class="left">
						<div class="column">
							<div class="input-box">
								<span><%= Resources.Lekmer.Customer_FirstOrCompanyName %></span><br />
								<asp:TextBox ID="CustomerFirstNameTextBox" runat="server" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span><%= Resources.Lekmer.Customer_LastOrFullName %></span><br />
								<asp:TextBox ID="CustomerLastNameTextBox" runat="server" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span><%= Resources.Lekmer.Customer_CivicOrOrganizationName %></span>
								<asp:RegularExpressionValidator runat="server" ID="CustomerCivicNumberValidator" ControlToValidate="CustomerCivicNumberTextBox"
									ErrorMessage="<%$ Resources:CustomerMessage, CivicNumberRequirement %>" Text=" *"
									ValidationExpression="\d{6}-\d{4}" ValidationGroup="vgSearchOrder" />
								<br />
								<asp:TextBox ID="CustomerCivicNumberTextBox" runat="server" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span><%= Resources.Customer.Literal_Email %></span>
								<asp:RegularExpressionValidator runat="server" ID="EmailValidator" ControlToValidate="EmailTextBox" ErrorMessage="<%$ Resources:CustomerMessage, EmailRequirement %>"
									Text=" *" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="vgSearchOrder" />
								<br />
								<asp:TextBox ID="EmailTextBox" runat="server" />
							</div>
						</div>
					</div>
					<div class="right">
						<br />
						<div id="order-search-button" class="buttons right">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SearchButton" Text="<%$ Resources:General, Button_Search %>" runat="server" ValidationGroup="vgSearchOrder" SkinID="DefaultButton" />
						</div>
					</div>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="SearchButton" EventName="Click" />
			</Triggers>
		</asp:UpdatePanel>
	</asp:Panel>

	<br class="clear" />

	<div class="order-horizontal-separator"></div>

	<label class="order-header" style="padding: 0px 20px 10px 20px; float: left; width: 96%;">
		<%= Resources.General.Literal_SearchResults %>
	</label>

	<div id = "order-search-result">
		<asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div id="ResultsDiv" runat="server" visible="false">
					<Scensum:Grid runat="server" ID="ArticlesGrid" FixedColumns="Order number|Customer">
						<sc:GridViewWithCustomPager ID="SearchResultsGrid" SkinID="grid" runat="server" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>"
							AutoGenerateColumns="false" DataSourceID="OrderObjectDataSource" Width="100%">
							<Columns>
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_OrderNumber %>" ItemStyle-Width="20%">
									<ItemTemplate>
										<uc:HyperLinkEncoded runat="server" ID="OrderNumberLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Order.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' Text='<%# Eval("Number")%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" DataField="Email" ItemStyle-Width="40%" />
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" DataField="CreatedDate" ItemStyle-Width="20%" />
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="20%">
									<ItemTemplate>
										<%# Eval("OrderStatus.Title")%>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
					</Scensum:Grid>
					<asp:ObjectDataSource ID="OrderObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Customers.Orders.OrderDataSource" />
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="SearchButtonOrder" EventName="Click" />
				<asp:AsyncPostBackTrigger ControlID="SearchButton" EventName="Click" />
			</Triggers>
		</asp:UpdatePanel>
	</div>
</asp:Content>