using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Order;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Customers.Orders
{
	public partial class Default : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			SearchButton.Click += SearchByCustomerInformation;
			SearchButtonOrder.Click += SearchByOrderInformation;
			SearchResultsGrid.PageIndexChanging += OrderGridPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			RestoreSearchCriteria();

			DoSearch(null);
		}

		protected virtual void OrderGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SearchResultsGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void SearchByCustomerInformation(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			ClearSearchCriteria(true);

			DoSearch(false);
		}

		protected virtual void SearchByOrderInformation(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			ClearSearchCriteria(false);

			DoSearch(true);
		}

		protected virtual void DoSearch(object isOrderSearchCriteriaType)
		{
			SearchResultsGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;

			PopulateSearch(isOrderSearchCriteriaType);

			ResultsDiv.Visible = true;
		}

		protected void RestoreSearchCriteria()
		{
			ILekmerOrderSearchCriteria lekmerOrderSearchCriteria = SearchCriteriaState<ILekmerOrderSearchCriteria>.Instance.Get(PathHelper.Order.GetDefaultUrl()) ?? IoC.Resolve<ILekmerOrderSearchCriteria>();

			OrderIdTextBox.Text = lekmerOrderSearchCriteria.OrderId != 0 ? lekmerOrderSearchCriteria.OrderId.ToString(CultureInfo.CurrentCulture) : string.Empty;
			CustomerFirstNameTextBox.Text = lekmerOrderSearchCriteria.FirstName;
			CustomerLastNameTextBox.Text = lekmerOrderSearchCriteria.LastName;
			CustomerCivicNumberTextBox.Text = lekmerOrderSearchCriteria.Number;
			EmailTextBox.Text = lekmerOrderSearchCriteria.Email;
		}

		protected void PopulateSearch(object isOrderSearchCriteriaType)
		{
			OrderObjectDataSource.SelectParameters.Clear();

			OrderObjectDataSource.SelectParameters.Add("maximumRows", SearchResultsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			OrderObjectDataSource.SelectParameters.Add("startRowIndex", (SearchResultsGrid.PageIndex * SearchResultsGrid.PageSize).ToString(CultureInfo.CurrentCulture));

			if (isOrderSearchCriteriaType == null)
			{
				OrderObjectDataSource.SelectParameters.Add("orderId", OrderIdTextBox.Text.Length != 0 ? OrderIdTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("customerFirstName", CustomerFirstNameTextBox.Text.Length != 0 ? CustomerFirstNameTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("customerLastName", CustomerLastNameTextBox.Text.Length != 0 ? CustomerLastNameTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("customerEmail", EmailTextBox.Text.Length != 0 ? EmailTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("socialSecurityNumber", CustomerCivicNumberTextBox.Text.Length != 0 ? CustomerCivicNumberTextBox.Text : null);
			}
			else
			{
				bool isOrderSearchCriteria;
				bool.TryParse(isOrderSearchCriteriaType.ToString(), out isOrderSearchCriteria);

				OrderObjectDataSource.SelectParameters.Add("orderId", isOrderSearchCriteria && OrderIdTextBox.Text.Length != 0 ? OrderIdTextBox.Text : null);

				OrderObjectDataSource.SelectParameters.Add("customerFirstName", !isOrderSearchCriteria && CustomerFirstNameTextBox.Text.Length != 0 ? CustomerFirstNameTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("customerLastName", !isOrderSearchCriteria && CustomerLastNameTextBox.Text.Length != 0 ? CustomerLastNameTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("customerEmail", !isOrderSearchCriteria && EmailTextBox.Text.Length != 0 ? EmailTextBox.Text : null);
				OrderObjectDataSource.SelectParameters.Add("socialSecurityNumber", !isOrderSearchCriteria && CustomerCivicNumberTextBox.Text.Length != 0 ? CustomerCivicNumberTextBox.Text : null);
			}

			SearchResultsGrid.DataBind();
		}

		private void ClearSearchCriteria(bool clearOrderInformation)
		{
			if (clearOrderInformation)
			{
				OrderIdTextBox.Text = string.Empty;

				SearchUpdatePanelOrder.Update();
			}
			else
			{
				CustomerFirstNameTextBox.Text = string.Empty;
				CustomerLastNameTextBox.Text = string.Empty;
				EmailTextBox.Text = string.Empty;
				CustomerCivicNumberTextBox.Text = string.Empty;

				SearchUpdatePanel.Update();
			}
		}
	}
}