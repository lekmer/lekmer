<%@ Page Language="C#" MasterPageFile="~/Master/Start.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Start.Default" %>
<%@ Import Namespace="System.Globalization"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="RssToolkit" Namespace="RssToolkit.Web.WebControls" Assembly="RssToolkit" %>
<%@ Register TagPrefix="sc" Assembly="Litium.Scensum.BackOffice" Namespace="Litium.Scensum.BackOffice.UserControls.ContextMenu"%>

<asp:Content ID="StartTab" ContentPlaceHolderID="StartBodyContentPlaceHolder" runat="server">
	<link href="../../Media/Css/ContextMenu3.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function() {
			$('div.rss-description > a').removeAttr('href');
		})
	</script>
	
	<div id="content-wrapper">
		<div class="startpage-wrapper">
			<div class="startpage-left-column">
				<div class="startpage-bestsellers-container">
					<div class="left">
						<Scensum:ToolBoxPanel ID="BestsellersPanel" runat="server">
							<div class="startpage-selector">
								<sc:ContextMenu3 ID="BestsellersViewModeList" runat="server" ImageExpandSrc="~/Media/Images/Common/context-menu.png" ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png"></sc:ContextMenu3>
							</div>
						</Scensum:ToolBoxPanel>
					</div>
					<br />
					<div class="left">
						<asp:GridView ID="BestsellersGrid" runat="server" AutoGenerateColumns="false" GridLines="None" 
							HeaderStyle-BorderStyle="None" RowStyle-BorderColor="#C6C6C6" RowStyle-BorderStyle="Solid" 
							RowStyle-BorderWidth="1px" HeaderStyle-ForeColor="#A0A0A0" Width="340px" 
							EmptyDataText='<%$ Resources:GeneralMessage, GridNoItems%>'>
							<Columns>
								<asp:TemplateField HeaderText='<%$ Resources:Start, Literal_Product%>' ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
									<ItemTemplate>
										<asp:Image ID="ProductImage" runat="server" ImageUrl='<%# GetImageUrl(Eval("Product.Image")) %>' Visible='<%# Eval("Product.Image") != null %>'  />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField ItemStyle-HorizontalAlign="Left" ItemStyle-Width="45%">
									<ItemTemplate>
										<div class="startpage-grid-item">
											<uc:HyperLinkEncoded ID="ProductLink" runat="server" Font-Bold="true" Font-Underline="true" Text='<%# Eval("Product.DisplayTitle") %>' NavigateUrl='<%# GetEditUrl(Eval("Product.Id")) %>'></uc:HyperLinkEncoded>
										</div>
										<div class="startpage-grid-item">
											<uc:LiteralEncoded ID="ProductErpLiteral" runat="server" Text='<%# GetErp(Eval("Product.ErpId")) %>'></uc:LiteralEncoded>
										</div>
										<div class="startpage-grid-item">
											<asp:Label ID="ProductPriceLabel" runat="server" Font-Bold="true" Text='<%# GetPrice(Eval("Product.Price.PriceIncludingVat")) %>'></asp:Label>
										</div>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText='<%$ Resources:Start, Literal_QuantityOrdered%>' ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" ItemStyle-Width="35%">
									<ItemTemplate>
										<asp:Label ID="OrderQuantity" runat="server" Font-Bold="true" Text='<%# Eval("Quantity") %>'></asp:Label>
										<br /><br />
										<asp:ImageButton ID="ImageButton" runat="server" ImageUrl="~/Media/Images/Start/bestsellers-button.png" PostBackUrl='<%# GetEditUrl(Eval("Product.Id")) %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</div>
				</div>
				<br />
				<div class="startpage-search-container">
					<Scensum:ToolBoxPanel ID="SearchPanel" runat="server">
						<div class="startpage-selector">
							<sc:ContextMenu3 ID="SearchViewModeList" runat="server" ImageExpandSrc="~/Media/Images/Common/context-menu.png" ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png"></sc:ContextMenu3>
						</div>
					</Scensum:ToolBoxPanel>
					<br />
					<div class="left">
						<asp:GridView ID="SearchPhrasesGrid" runat="server" AutoGenerateColumns="false" GridLines="None" 
							HeaderStyle-BorderStyle="None" RowStyle-BorderColor="#C6C6C6" RowStyle-BorderStyle="Solid" 
							RowStyle-BorderWidth="1px" HeaderStyle-ForeColor="#A0A0A0" Width="340px" 
							EmptyDataText='<%$ Resources:GeneralMessage, GridNoItems%>'>
							<Columns>
								<asp:TemplateField>
									<HeaderTemplate>
										<asp:Label runat="server" ID="HeaderLabel1" Text='<%#  Resources.Start.Literal_Phrase%>' CssClass="left"></asp:Label>
										<asp:Label runat="server" ID="HeaderLabel2" Text='<%# Resources.Start.Literal_QuantitySearched%>' CssClass="right"></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<span class="left bold">
											<uc:LiteralEncoded ID="PhraseLiteral" runat="server" Text='<%# Eval("Query").ToString().Substring(0, Math.Min(Eval("Query").ToString().Length, 30)) %>'></uc:LiteralEncoded>
											&nbsp;
										</span>
										<asp:Label ID="HitsLabel" runat="server" Text='<%# string.Format(CultureInfo.CurrentCulture, " ({0} {1})", Eval("HitCount"), Resources.Start.Literal_hits) %>' CssClass="left"></asp:Label>
										<asp:Label ID="SearchedLabel" runat="server" Text='<%# Eval("SearchCount") %>' Font-Bold="true" CssClass="right"></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</div>
					<br />
					<div class="left">
						<asp:GridView ID="ZeroHitsPhrasesGrid" runat="server" AutoGenerateColumns="false" GridLines="None" 
							HeaderStyle-BorderStyle="None" RowStyle-BorderColor="#C6C6C6" RowStyle-BorderStyle="Solid" 
							RowStyle-BorderWidth="1px" HeaderStyle-ForeColor="#A0A0A0" Font-Bold="true" Width="340px" 
							EmptyDataText='<%$ Resources:GeneralMessage, GridNoItems%>'>
							<Columns>
								<asp:TemplateField>
									<HeaderTemplate>
										<asp:Label runat="server" ID="HeaderLabel1" Text='<%# Resources.Start.Literal_ZeroHitsPhrases%>' CssClass="left"></asp:Label>
										<asp:Label runat="server" ID="HeaderLabel2" Text='<%# Resources.Start.Literal_QuantitySearched%>' CssClass="right"></asp:Label>
									</HeaderTemplate>
									<ItemTemplate>
										<span class="left">
											<uc:LiteralEncoded ID="PhraseLiteral" runat="server" Text='<%# Eval("Query").ToString().Substring(0, Math.Min(Eval("Query").ToString().Length, 30)) %>'></uc:LiteralEncoded>
										</span>
										<asp:Label ID="SearchedLabel" runat="server" Text='<%# Eval("SearchCount") %>' CssClass="right"></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</div>
				</div>
			</div>
			
			<div class="startpage-center-column">
				<Scensum:ToolBoxPanel ID="PerformancePanel" runat="server">
					<div class="startpage-selector">
						 <sc:ContextMenu3 ID="PerformanceViewModeList" runat="server" ImageExpandSrc="~/Media/Images/Common/context-menu.png" ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png"></sc:ContextMenu3>
					</div>
				</Scensum:ToolBoxPanel>
				<br />
				<div class="startpage-performance-container">
					<asp:GridView ID="PerformanceGrid" runat="server" AutoGenerateColumns="false" GridLines="None" 
						HeaderStyle-BorderStyle="None" RowStyle-BorderColor="#C6C6C6" RowStyle-BorderStyle="Solid" 
						RowStyle-BorderWidth="1px" HeaderStyle-ForeColor="#A0A0A0" Font-Bold="true" Width="340px" 
						EmptyDataText='<%$ Resources:GeneralMessage, GridNoItems%>'>
						<Columns>
							<asp:TemplateField HeaderText='<%$ Resources:Start, Literal_KeyPerformanceIndex%>' ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%">
								<ItemTemplate>
									<uc:LiteralEncoded ID="PerformanceTitle" runat="server" Text='<%# Eval("Key") %>'></uc:LiteralEncoded>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField ItemStyle-HorizontalAlign="Right" ItemStyle-Width="50%">
								<ItemTemplate>
									<asp:Label ID="PerformanceValue" runat="server" Text='<%# Eval("Value") %>'></asp:Label>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</div>
				<br />
				<div class="startpage-header">
					<span><%=Resources.Start.Literal_OrderStatistics%></span>
					<br />
					<asp:Chart ID="OrderStatisticsChart" runat="server" EnableViewState="true"
						ImageLocation="~/ChartTypes/LineCharts/Gallery/2DLine.png" Height="130px" Width="340px">
						<Series>
							<asp:Series Name="Series1" XValueMember="Key" YValueMembers="Value" BorderColor="#DDC63C" BorderWidth="6" ChartType="Area" Color="#444444" BackSecondaryColor="#222222" BackGradientStyle="TopBottom">
							</asp:Series>
						</Series>
						<ChartAreas>
							<asp:ChartArea Name="ChartArea1" BackColor="#E6E6E6" BorderColor="#C6C6C6">
								<AxisY IsMarginVisible="False" LineColor="#FFFFFF" LabelAutoFitStyle="WordWrap">
									<MajorGrid LineColor="#FFFFFF" />
									<MajorTickMark Enabled="False" />
									<LabelStyle ForeColor="#666666" Font="Microsoft Sans Serif, 6pt"/>
								</AxisY>
								<AxisX IsMarginVisible="False" LineColor="#FFFFFF" IsStartedFromZero="true" LabelAutoFitStyle="WordWrap">
									<MajorGrid LineColor="#FFFFFF" />
									<MajorTickMark Enabled="False" />
									<LabelStyle ForeColor="#666666" Font="Microsoft Sans Serif, 6pt"/>
								</AxisX>
								<Position Height="85" Width="100" X="2" Y="8" />
								<InnerPlotPosition Height="95" Y="0" />
							</asp:ChartArea>
						</ChartAreas>
					</asp:Chart>
				</div>
				<br class="clear" />
				<div class="startpage-header">
					<span><%=Resources.Start.Literal_OrderValueStatistics%></span>
					<br />
					<asp:Chart ID="SaleStatisticsChart" runat="server" EnableViewState="true" 
						ImageLocation="~/ChartTypes/LineCharts/Gallery/2DLine.png" Height="130px" Width="340px">
						<Series>
							<asp:Series Name="Series1" XValueMember="Key" YValueMembers="Value" BorderColor="#DDC63C" BorderWidth="6" ChartType="Area" Color="#444444" BackSecondaryColor="#222222" BackGradientStyle="TopBottom">
							</asp:Series>
						</Series>
						<ChartAreas>
							<asp:ChartArea Name="ChartArea1" BackColor="#E6E6E6" BorderColor="#C6C6C6">
								<AxisY IsMarginVisible="False" LineColor="#FFFFFF" LabelAutoFitStyle="WordWrap">
									<MajorGrid LineColor="#FFFFFF" />
									<MajorTickMark Enabled="False" />
									<LabelStyle ForeColor="#666666" Font="Microsoft Sans Serif, 6pt"/>
								</AxisY>
								<AxisX IsMarginVisible="False" LineColor="#FFFFFF" IsStartedFromZero="true" LabelAutoFitStyle="WordWrap">
									<MajorGrid LineColor="#FFFFFF" />
									<MajorTickMark Enabled="False" />
									<LabelStyle ForeColor="#666666" Font="Microsoft Sans Serif, 6pt"/>
								</AxisX>
								<Position Height="85" Width="100" X="2" Y="8" />
								<InnerPlotPosition Height="95" Y="0" />
							</asp:ChartArea>
						</ChartAreas>
					</asp:Chart>
				</div>
				<br class="clear" />
				<div class="startpage-header">
					<span><%=Resources.Start.Literal_VisitorStatistics%></span>
					<br />
					<asp:Chart ID="VisitorStatisticsChart" runat="server" EnableViewState="true" 
						ImageLocation="~/ChartTypes/LineCharts/Gallery/2DLine.png" Height="130px" Width="340px">
						<Series>
							<asp:Series Name="Series1" XValueMember="Key" YValueMembers="Value" BorderColor="#DDC63C" BorderWidth="6" ChartType="Area" Color="#444444" BackSecondaryColor="#222222" BackGradientStyle="TopBottom">
							</asp:Series>
						</Series>
						<ChartAreas>
							<asp:ChartArea Name="ChartArea1" BackColor="#E6E6E6" BorderColor="#C6C6C6">
								<AxisY IsMarginVisible="False" LineColor="#FFFFFF" LabelAutoFitStyle="WordWrap">
									<MajorGrid LineColor="#FFFFFF" />
									<MajorTickMark Enabled="False" />
									<LabelStyle ForeColor="#666666" Font="Microsoft Sans Serif, 6pt"/>
								</AxisY>
								<AxisX IsMarginVisible="False" LineColor="#FFFFFF" IsStartedFromZero="true" LabelAutoFitStyle="WordWrap">
									<MajorGrid LineColor="#FFFFFF" />
									<MajorTickMark Enabled="False" />
									<LabelStyle ForeColor="#666666" Font="Microsoft Sans Serif, 6pt"/>
								</AxisX>
								<Position Height="85" Width="100" X="2" Y="8" />
								<InnerPlotPosition Height="95" Y="0" />
							</asp:ChartArea>
						</ChartAreas>
					</asp:Chart>
				</div>
			</div>
			
			<div class="startpage-right-column">
				<Scensum:ToolBoxPanel ID="InformationPanel" runat="server"></Scensum:ToolBoxPanel>
				<br />
				<div class="startpage-rss">
					<asp:DataList ID="RssList" runat="server" DataSourceID="RssDataSource">
						<ItemTemplate>
							<div class="startpage-grid-item">
								<asp:Label ID="DateLabel" runat="server" Text='<%# RssHasPubDate ? GetRssDate(Eval("pubDate")) : string.Empty %>' ForeColor="#B7A42C" Font-Bold="true"></asp:Label>
							</div>
							<div class="startpage-grid-item">
								<asp:HyperLink ID="TitleLink" runat="server" Target="_blank" NavigateUrl='<%# RssHasLink ? Eval("link") : string.Empty %>' Text='<%# RssHasTitle ? Eval("title") : string.Empty %>' Font-Bold="true"></asp:HyperLink>
							</div>
							<div class="startpage-grid-item rss-description">
								<asp:Literal ID="DesrciptionLiteral" runat="server" Text='<%# RssHasDescription ? Eval("description") : string.Empty %>'></asp:Literal>
							</div>
						</ItemTemplate>
					</asp:DataList>
					<span id="NoRssFeedMessage" runat="server" visible="false">&nbsp; <%=Resources.Start.Literal_NoRssFeed%></span>
				</div>
				<RssToolkit:rssdatasource ID="RssDataSource" runat="server" Url="<%$AppSettings:RssFeed%>">
				</RssToolkit:rssdatasource>
			</div>
		</div>
    </div>
</asp:Content>