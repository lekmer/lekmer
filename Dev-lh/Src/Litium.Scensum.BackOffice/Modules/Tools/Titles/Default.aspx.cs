﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Resources;
using log4net;

namespace Litium.Scensum.BackOffice.Modules.Tools.Titles
{
	public partial class Default : LekmerPageController
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private static readonly int _minColumnCount = 3;
		private readonly string[] _validExtensions = new[]{".csv", ".txt"};
		private string _rowErrorMessage = Resources.Tools.Error_FileRow;
		private string _rowTagErrorMessage = Resources.Tools.Error_FileRowTag;

		protected override void SetEventHandlers()
		{
			UploadProductInfoButton.Click += UploadProductInfo;
		}

		protected override void PopulateForm()
		{
			CurrentChannelLabel.Text = string.Format(CurrentChannelLabel.Text, ChannelHelper.CurrentChannel.CommonName);
		}

		protected virtual void UploadProductInfo(object sender, EventArgs e)
		{
			ResultListSuccess.Text = string.Empty;
			ResultListErrors.Text = string.Empty;
			InformationLabel.Text = string.Empty;

			try
			{
				if (!ValidateFileToUpload()) return;

				SaveFile();
				ProcessFile();

				Messager.Add(Resources.Tools.Message_FileProcessed, InfoType.Success);
			}
			catch (Exception exception)
			{
				Messager.Add(string.Format("ERROR: {0}", exception.Message), InfoType.Warning);
				_log.Error(exception);
			}
		}

		protected virtual bool ValidateFileToUpload()
		{
			bool isValid = true;

			if (!FileUploadControl.HasFile)
			{
				Messager.Add(Resources.Tools.Error_TxtOrCsvFileEmpty, InfoType.Warning);
				isValid = false;
			}

			int maxFileSize = HttpRuntimeSetting.MaxRequestLength;
			if (maxFileSize <= 0)
			{
				Messager.Add(MediaMessage.NoConfirmMaxFileSize, InfoType.Warning);
				isValid = false;
			}

			HttpPostedFile file = FileUploadControl.PostedFile;
			if (file.ContentLength / 1024 > maxFileSize)
			{
				Messager.Add(Resources.Tools.Error_TxtOrCsvFileLarge, InfoType.Warning);
				isValid = false;
			}

			var extension = Path.GetExtension(file.FileName);
			int pos = Array.IndexOf(_validExtensions, extension);
			if (string.IsNullOrEmpty(extension) || pos < 0)
			{
				Messager.Add(MediaMessage.IncorrectFileFormat, InfoType.Warning);
				isValid = false;
			}

			return isValid;
		}

		protected virtual void SaveFile()
		{
			var file = FileUploadControl.PostedFile;
			var filePath = GetImportFilePath(file.FileName);
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}
			using (var fileStream = File.Create(filePath))
			{
				StreamAppender.AppendToStream(file.InputStream, fileStream, 8 * 1024);
			}
			file.InputStream.Seek(0, SeekOrigin.Begin);
		}
		protected virtual string GetImportFilePath(string fileName)
		{
			return string.Format(BackOfficeSetting.Instance.TitlesFilePath, SignInHelper.SignedInSystemUser.Id, DateTime.Now.ToString("yyyyMMdd-HHmmss"), fileName);
		}

		protected virtual void ProcessFile()
		{
			var products = GetProducts();
			if (products != null)
			{
				UpdateProducts(products);
			}
		}
		protected virtual List<IImportProductInfo> GetProducts()
		{
			var fileContent = FileUploadControl.FileContent;
			if (fileContent != null)
			{
				StreamReader file = null;
				try
				{
					var products = new List<IImportProductInfo>();

					var tagIds = IoC.Resolve<ITagSecureService>().GetAll().ToDictionary(t => t.Id, t => t.Id);

					char[] delimiterChars = { '\t' };
					file = new StreamReader(fileContent, Encoding.GetEncoding(BackOfficeSetting.Instance.EncodingType));

					string line;
					int lineNumber = 0;
					while ((line = file.ReadLine()) != null)
					{
						lineNumber++;

						// continue if line is empty.
						if (string.IsNullOrEmpty(line)) continue;

						string[] info = line.Split(delimiterChars);

						if (info.Length < _minColumnCount)
						{
							BuildErrorMessage(info[0], lineNumber);
							continue;
						}

						var tmpProduct = IoC.Resolve<IImportProductInfo>();
						tmpProduct.HYErpId = info[0];
						tmpProduct.Title = info[1];
						tmpProduct.Description = info[2];
						tmpProduct.TagIdCollection = new List<int>();

						// Set tags.
						if (info.Length > _minColumnCount)
						{
							IList<string> incorectTags = new List<string>();
							for (int i = _minColumnCount; i < info.Length; i++)
							{
								var tagData = info[i];
								if (string.IsNullOrEmpty(tagData)) continue;

								int tagId;
								if (!int.TryParse(info[i], out tagId)
									|| tagId <= 0
									|| !tagIds.ContainsKey(tagId))
								{
									incorectTags.Add(info[i]);
									continue;
								}

								tmpProduct.TagIdCollection.Add(tagId);
							}

							if (incorectTags.Count > 0)
							{
								string incorrectTagIds = string.Join(", ", incorectTags.ToArray());
								BuildErrorMessage(info[0], incorrectTagIds, lineNumber);
							}
						}

						products.Add(tmpProduct);
					}

					return products;
				}
				catch (Exception exception)
				{
					if (file != null)
					{
						file.Close();
					}
					throw new Exception(exception.Message);
				}
				finally
				{
					if (file != null)
					{
						file.Close();
					}
				}
			}

			return null;
		}
		protected virtual void BuildErrorMessage(string row, int lineNumber)
		{
			ResultListErrors.Text += "\n" + string.Format(_rowErrorMessage, row, lineNumber);
		}
		protected virtual void BuildErrorMessage(string row, string id, int lineNumber)
		{
			ResultListErrors.Text += "\n" + string.Format(_rowTagErrorMessage, row, id, lineNumber);
		}
		protected virtual void UpdateProducts(List<IImportProductInfo> products)
		{
			var productSecureService = (ILekmerProductSecureService)IoC.Resolve<IProductSecureService>();

			int channelId = ChannelHelper.CurrentChannel.Id;
			int languageId = ChannelHelper.CurrentChannel.Language.Id;

			foreach (var product in products)
			{
				string status = productSecureService.UpdateProductInfo(product, channelId, languageId, SignInHelper.SignedInSystemUser.UserName);
				ResultListSuccess.Text += "\n" + string.Format("{0,0} {2,30} {1,30}", product.HYErpId, status, DateTime.Now);
			}

			// Remove old logs.
			productSecureService.RemoveOldProductInfoImportLogs(BackOfficeSetting.Instance.LogExpirationTime);

			InformationLabel.Text = string.Format(Resources.Tools.Message_ProcessingData, products.Count);
		}
	}
}