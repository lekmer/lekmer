﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Tools.Titles.Default" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="TitlesTab" ContentPlaceHolderID="body" runat="server">
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="UploadProductInfoButton" runat="server" />
	<uc:ScensumValidationSummary ID="FileUploadValidationSummary" CssClass="advance-validation-summary" ValidationGroup="FileUpload" runat="server" />

	<Scensum:ToolBoxPanel ID="TitlesToolBoxPanel" runat="server" Text="Product info import" />

	<div class="column">
		<div class="input-box">
			<span class="text-bold"><%= Resources.Tools.Literal_UploadTxtOrCsvFile %></span>
			<asp:RequiredFieldValidator ID="FileNameValidator" runat="server" ControlToValidate="FileUploadControl" ErrorMessage="<%$ Resources:Tools, Error_TxtOrCsvFileEmpty %>" Text="*" ValidationGroup="FileUpload" />
			<asp:FileUpload ID="FileUploadControl" runat="server" CssClass="file-upload" ContentEditable="false" />
		</div>
	</div>

	<br class="clear" />
	<hr />

	<div class="column">
		<div class="input-box main-caption" style="color: darkgreen;">
			<asp:Label ID="CurrentChannelLabel" runat="server" Text="<%$ Resources:Tools, Literal_CurrentChannel %>" />
			<span><%= Resources.Tools.Literal_SelectChannelWarning %></span>
		</div>
	</div>
	<div class="right">
		<div class="input-box">
			<uc:ImageLinkButton ID="UploadProductInfoButton" Text="<%$ Resources:Interface,Button_Upload %>" ValidationGroup="FileUpload" SkinID="DefaultButton" UseSubmitBehaviour="true" runat="server" />
		</div>
	</div>
	<br class="clear" />
	<div>
		<span class="right" style="color: red"><%= Resources.Tools.Literal_UploadTakeTimeWarning %></span>
	</div>

	<br class="clear" />
	<hr />

	<div>
		<span class="main-caption">Imported</span>
		<asp:TextBox ID="ResultListSuccess" runat="server" Width="100%" Height="250px" TextMode="MultiLine" ReadOnly="True" />
	</div>

	<p>
		<asp:Label ID="InformationLabel" runat="server" CssClass="main-caption" style="color: darkgreen" />
	</p>

	<div>
		<span class="main-caption">Not Imported</span>
		<asp:TextBox ID="ResultListErrors" runat="server" Width="100%" Height="250px" TextMode="MultiLine" ReadOnly="True" />
	</div>
</asp:Content>