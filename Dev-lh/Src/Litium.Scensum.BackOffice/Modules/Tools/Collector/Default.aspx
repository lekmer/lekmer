﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Tools.Collector.Default" %>

<%@ Import Namespace="Litium.Lekmer.Payment.Collector" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="CollectorTab" ContentPlaceHolderID="body" runat="server">
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="UpdateCollectorPaymentsButton" runat="server" />

	<Scensum:ToolBoxPanel ID="CollectorToolBoxPanel" runat="server" Text="Collector payments" />

	<div class="column">
		<div class="input-box main-caption" style="color: darkgreen;">
			<asp:Label ID="CurrentChannelLabel" runat="server" Text="<%$ Resources:Tools, Literal_CurrentChannel %>" />
		</div>
	</div>

	<br class="clear" />
	<hr />

	<div class="right">
		<div class="input-box">
			<uc:ImageLinkButton ID="RefreshCurrentCollectorPaymentsButton" Text="Refresh" SkinID="DefaultButton" UseSubmitBehaviour="true" runat="server" />
		</div>
	</div>

	<br class="clear" />

	<div>
		<span class="main-caption">Payment types in use</span>

		<asp:GridView ID="CollectorPaymentsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
			<Columns>
				<asp:TemplateField HeaderText="Code">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).Code %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Description">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).Description %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Start Fee">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).StartFee %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Invoice Fee">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).InvoiceFee %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Monthly Interest Rate">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).MonthlyInterestRate %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="NoOfMonths">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).NoOfMonths %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="MinPurchaseAmount">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).MinPurchaseAmount %>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Type">
					<ItemTemplate>
						<%# ((ICollectorPaymentType)Container.DataItem).PartPaymentType %>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<EmptyDataTemplate>
				No payment types were found!
			</EmptyDataTemplate>
		</asp:GridView>

	</div>

	<br class="clear" />

</asp:Content>