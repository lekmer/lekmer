using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.BackOffice.UserControls.Tree;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.Modules.Tools.Cdon
{
	public class CdonExportConfiguratorCategory
	{
		private Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private CdonExportConfiguratorHelper _cdonExportConfiguratorHelper;
		private CdonExportConfiguratorHelper CdonExportConfiguratorHelper
		{
			get { return _cdonExportConfiguratorHelper ?? (_cdonExportConfiguratorHelper = new CdonExportConfiguratorHelper()); }
		}

		public GridView Grid { get; set; }
		public ImageLinkButton OkButton { get; set; }
		public ImageLinkButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public SelectTreeView PopupCategoryTree { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public Collection<ICdonExportRestrictionItem> Categories { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;

			OkButton.Click += OkButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(Collection<ICdonExportRestrictionItem> categories)
		{
			PopupCategoryTree.Selector = CategorySelector;

			foreach (int value in ConvertToCategoryDictionary(categories).Keys)
			{
				PopupCategoryTree.SelectedIds.Add(value);
			}

			PopupCategoryTree.DataBind();

			DataBindGrid(CdonExportConfiguratorHelper.ResolveCategories(ConvertToCategoryDictionary(categories)));
		}

		public virtual void DataBindGrid(Collection<ICategory> categories)
		{
			Grid.DataSource = categories;
			Grid.DataBind();

			GridUpdatePanel.Update();
		}

		public string GetCategoryPath(object cat)
		{
			var category = cat as ICategory;
			if (category == null) return string.Empty;

			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = RetrieveCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CdonExportConfiguratorHelper.SetSelectionFunction((GridView)sender, e.Row, ApplyToAllDiv, false);
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				CdonExportConfiguratorHelper.PopulateChannels(e.Row, ((ICategory)e.Row.DataItem).Id);
			}
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				Update();
				RemoveCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			Update();

			foreach (int categoryId in PopupCategoryTree.SelectedIds)
			{
				if (Categories.FirstOrDefault(c => c.ItemId == categoryId) == null)
				{
					var cdonExportCategory = IoC.Resolve<ICdonExportRestrictionItem>();
					cdonExportCategory.ItemId = categoryId;

					Categories.Add(cdonExportCategory);
				}
			}

			DataBindGrid(CdonExportConfiguratorHelper.ResolveCategories(ConvertToCategoryDictionary(Categories)));

			if (IncludeAllCheckbox != null && Categories.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			Update();

			foreach (var categoryId in GetSelectedFromGrid())
			{
				var cdonExportRestrictionCategory = Categories.FirstOrDefault(c => c.ItemId == categoryId);
				if (cdonExportRestrictionCategory != null)
				{
					Categories.Remove(cdonExportRestrictionCategory);
				}
			}

			DataBindGrid(CdonExportConfiguratorHelper.ResolveCategories(ConvertToCategoryDictionary(Categories)));
		}

		protected virtual void RemoveCategory(int categoryId)
		{
			var cdonExportRestrictionCategory = Categories.FirstOrDefault(c => c.ItemId == categoryId);
			if (cdonExportRestrictionCategory != null)
			{
				Categories.Remove(cdonExportRestrictionCategory);
			}

			DataBindGrid(CdonExportConfiguratorHelper.ResolveCategories(ConvertToCategoryDictionary(Categories)));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CdonExportConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		protected virtual ICategory RetrieveCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}

		private Dictionary<int, int> ConvertToCategoryDictionary(Collection<ICdonExportRestrictionItem> categories)
		{
			return categories == null || categories.Count == 0 ? new Dictionary<int, int>() : categories.ToDictionary(c => c.ItemId, c => c.ItemId);
		}

		public bool Update()
		{
			foreach (GridViewRow row in Grid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				var id = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var reason = ((TextBox)row.FindControl("reasonTextBox")).Text;

				var category = Categories.FirstOrDefault(c => c.ItemId == id);
				if (category != null)
				{
					category.Reason = reason;

					// Channels.
					var channelsRepeater = (Repeater)row.FindControl("ChannelsRepeater");
					if (channelsRepeater != null)
					{
						if (category.Channels == null)
						{
							category.Channels = new Dictionary<int, int>();
						}
						else
						{
							category.Channels.Clear();
						}

						foreach (RepeaterItem item in channelsRepeater.Items)
						{
							var hfChannelId = (HiddenField)item.FindControl("hfChannelId");
							var cbChannel = (CheckBox)item.FindControl("cbChannel");
							int channelId;
							if (hfChannelId != null && cbChannel != null && int.TryParse(hfChannelId.Value, out channelId))
							{
								if (cbChannel.Checked && !category.Channels.ContainsKey(channelId))
								{
									category.Channels.Add(channelId, channelId);
								}
							}
						}
					}
				}
			}

			return true;
		}
	}
}