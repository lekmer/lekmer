﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Tools.Cdon
{
	public partial class Default : LekmerStatePageController<ICdonExportRestriction>
	{
		private ICdonExportRestrictionSecureService _cdonExportRestrictionSecureService;
		private ICdonExportRestrictionSecureService CdonExportRestrictionSecureService
		{
			get { return _cdonExportRestrictionSecureService ?? (_cdonExportRestrictionSecureService = IoC.Resolve<ICdonExportRestrictionSecureService>()); }
		}

		private CdonExportConfiguratorHelper _cdonExportConfiguratorHelper;
		protected CdonExportConfiguratorHelper CdonExportConfiguratorHelper
		{
			get { return _cdonExportConfiguratorHelper ?? (_cdonExportConfiguratorHelper = new CdonExportConfiguratorHelper()); }
		}

		private CdonExportConfiguratorProduct _cdonExportConfiguratorIncludeProduct;
		private CdonExportConfiguratorProduct CdonExportConfiguratorIncludeProduct
		{
			get { return _cdonExportConfiguratorIncludeProduct ?? (_cdonExportConfiguratorIncludeProduct = new CdonExportConfiguratorProduct()); }
		}

		private CdonExportConfiguratorProduct _cdonExportConfiguratorExcludeProduct;
		private CdonExportConfiguratorProduct CdonExportConfiguratorExcludeProduct
		{
			get { return _cdonExportConfiguratorExcludeProduct ?? (_cdonExportConfiguratorExcludeProduct = new CdonExportConfiguratorProduct()); }
		}

		private CdonExportConfiguratorCategory _cdonExportConfiguratorIncludeCategory;
		private CdonExportConfiguratorCategory CdonExportConfiguratorIncludeCategory
		{
			get { return _cdonExportConfiguratorIncludeCategory ?? (_cdonExportConfiguratorIncludeCategory = new CdonExportConfiguratorCategory()); }
		}

		private CdonExportConfiguratorCategory _cdonExportConfiguratorExcludeCategory;
		private CdonExportConfiguratorCategory CdonExportConfiguratorExcludeCategory
		{
			get { return _cdonExportConfiguratorExcludeCategory ?? (_cdonExportConfiguratorExcludeCategory = new CdonExportConfiguratorCategory()); }
		}

		private CdonExportConfiguratorBrand _cdonExportConfiguratorIncludeBrand;
		private CdonExportConfiguratorBrand CdonExportConfiguratorIncludeBrand
		{
			get { return _cdonExportConfiguratorIncludeBrand ?? (_cdonExportConfiguratorIncludeBrand = new CdonExportConfiguratorBrand()); }
		}

		private CdonExportConfiguratorBrand _cdonExportConfiguratorExcludeBrand;
		private CdonExportConfiguratorBrand CdonExportConfiguratorExcludeBrand
		{
			get { return _cdonExportConfiguratorExcludeBrand ?? (_cdonExportConfiguratorExcludeBrand = new CdonExportConfiguratorBrand()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (State != null)
			{
				CdonExportConfiguratorIncludeProduct.Products = State.IncludeProducts;
				CdonExportConfiguratorExcludeProduct.Products = State.ExcludeProducts;

				CdonExportConfiguratorIncludeCategory.Categories = State.IncludeCategories;
				CdonExportConfiguratorExcludeCategory.Categories = State.ExcludeCategories;

				CdonExportConfiguratorIncludeBrand.Brands = State.IncludeBrands;
				CdonExportConfiguratorExcludeBrand.Brands = State.ExcludeBrands;
			}
		}

		protected override void SetEventHandlers()
		{
			InitializeConfiguratorIncludeProducts();
			InitializeConfiguratorExcludeProducts();

			InitializeConfiguratorIncludeCategories();
			InitializeConfiguratorExcludeCategories();

			InitializeConfiguratorIncludeBrands();
			InitializeConfiguratorExcludeBrands();

			CdonExportConfiguratorIncludeProduct.SetEventHandlers();
			CdonExportConfiguratorExcludeProduct.SetEventHandlers();

			CdonExportConfiguratorIncludeCategory.SetEventHandlers();
			CdonExportConfiguratorExcludeCategory.SetEventHandlers();

			CdonExportConfiguratorIncludeBrand.SetEventHandlers();
			CdonExportConfiguratorExcludeBrand.SetEventHandlers();

			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;

			OpenIncludeProductSearchPopupButton.Click += OnOpenIncludeProductSearchPopupButton;
			OpenIncludeCategorySearchPopupButton.Click += OnOpenIncludeCategorySearchPopupButton;
			OpenIncludeBrandSearchPopupButton.Click += OnOpenIncludeBrandSearchPopupButton;
			OpenExcludeProductSearchPopupButton.Click += OnOpenExcludeProductSearchPopupButton;
			OpenExcludeCategorySearchPopupButton.Click += OnOpenExcludeCategorySearchPopupButton;
			OpenExcludeBrandSearchPopupButton.Click += OnOpenExcludeBrandSearchPopupButton;

			btnSave.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			var cdonExportRestriction = CdonExportRestrictionSecureService.GetAll();
			cdonExportRestriction = PrepareSource(cdonExportRestriction);

			State = cdonExportRestriction;

			CdonExportConfiguratorIncludeProduct.DataBind(State.IncludeProducts);
			CdonExportConfiguratorExcludeProduct.DataBind(State.ExcludeProducts);

			CdonExportConfiguratorIncludeCategory.DataBind(State.IncludeCategories);
			CdonExportConfiguratorExcludeCategory.DataBind(State.ExcludeCategories);

			CdonExportConfiguratorIncludeBrand.DataBind(State.IncludeBrands);
			CdonExportConfiguratorExcludeBrand.DataBind(State.ExcludeBrands);

			State = cdonExportRestriction;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();

			Dictionary<int, int> restrictedProductsCount = CdonExportRestrictionSecureService.GetRestrictedProductsCount();
			RestrictedProductsCountGrid.DataSource = restrictedProductsCount;
			RestrictedProductsCountGrid.DataBind();
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			CdonExportConfiguratorIncludeProduct.Update();
			ProductIncludeGrid.PageIndex = 0;
			CdonExportConfiguratorIncludeProduct.DataBindGrid(State.IncludeProducts);

			CdonExportConfiguratorExcludeProduct.Update();
			ProductExcludeGrid.PageIndex = 0;
			CdonExportConfiguratorExcludeProduct.DataBindGrid(State.ExcludeProducts);
		}

		protected void OnOpenIncludeProductSearchPopupButton(object sender, EventArgs e)
		{
			ProductIncludeSearchPopup.Show();
		}
		protected void OnOpenIncludeCategorySearchPopupButton(object sender, EventArgs e)
		{
			CategoryIncludeSearchPopup.Show();
		}
		protected void OnOpenIncludeBrandSearchPopupButton(object sender, EventArgs e)
		{
			BrandIncludeSearchPopup.Show();
		}
		protected void OnOpenExcludeProductSearchPopupButton(object sender, EventArgs e)
		{
			ProductExcludeSearchPopup.Show();
		}
		protected void OnOpenExcludeCategorySearchPopupButton(object sender, EventArgs e)
		{
			CategoryExcludeSearchPopup.Show();
		}
		protected void OnOpenExcludeBrandSearchPopupButton(object sender, EventArgs e)
		{
			BrandExcludeSearchPopup.Show();
		}

		protected void OnSave(object sender, EventArgs e)
		{
			var isValidIncludeProduct = CdonExportConfiguratorIncludeProduct.Update();
			var isValidExcludeProduct = CdonExportConfiguratorExcludeProduct.Update();
			var isValidIncludeCategory = CdonExportConfiguratorIncludeCategory.Update();
			var isValidExcludeCategory = CdonExportConfiguratorExcludeCategory.Update();
			var isValidIncludeBrand = CdonExportConfiguratorIncludeBrand.Update();
			var isValidExcludeBrand = CdonExportConfiguratorExcludeBrand.Update();

			if (isValidIncludeProduct && isValidExcludeProduct
				&& isValidIncludeCategory && isValidExcludeCategory
				&& isValidIncludeBrand && isValidExcludeBrand)
			{
				var currentDate = DateTime.Now;

				foreach (var item in State.IncludeProducts)
				{
					PopulateItem(item, currentDate);
				}
				foreach (var item in State.ExcludeProducts)
				{
					PopulateItem(item, currentDate);
				}

				foreach (var item in State.IncludeCategories)
				{
					PopulateItem(item, currentDate);
				}
				foreach (var item in State.ExcludeCategories)
				{
					PopulateItem(item, currentDate);
				}

				foreach (var item in State.IncludeBrands)
				{
					PopulateItem(item, currentDate);
				}
				foreach (var item in State.ExcludeBrands)
				{
					PopulateItem(item, currentDate);
				}

				CdonExportRestrictionSecureService.SaveAll(
					State.IncludeProducts, State.ExcludeProducts,
					State.IncludeCategories, State.ExcludeCategories,
					State.IncludeBrands, State.ExcludeBrands
					);

				Messager.Add(GeneralMessage.SaveSuccessCdonRestrictions, InfoType.Success);
			}
			else
			{
				Messager.Add("Please check Reason and Channels fields in tables");
			}
		}
		private List<int> GetItemIds(IEnumerable<ICdonExportRestrictionItem> items)
		{
			var itemIds = new Dictionary<int, int>();

			foreach (var item in items)
			{
				if (!itemIds.ContainsKey(item.ItemId))
				{
					itemIds.Add(item.ItemId, item.ItemId);
				}
			}

			return itemIds.Keys.ToList();
		}
		private ICdonExportRestrictionItem PopulateItem(ICdonExportRestrictionItem item, DateTime currentDate)
		{
			item.UserId = SignInHelper.SignedInSystemUser.Id;
			item.CreatedDate = currentDate;

			return item;
		}

		protected virtual void RegisterClientScript()
		{
			System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);
			System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmBrandRemove", "function ConfirmBrandRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteBrand + "');}", true);

			const string script = "$(function() {" +
								"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('ProductExcludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.55, 0.09);" +
								"ResizePopup('CategoryExcludePopupDiv', 0.4, 0.55, 0.09);" +
								"ResizePopup('BrandIncludePopupDiv', 0.4, 0.55, 0.09);" +
								"ResizePopup('BrandExcludePopupDiv', 0.4, 0.55, 0.09);" +
								"});";
			System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CdonExportConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CdonExportConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CdonExportConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CdonExportConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IChannel> _channels;
		protected virtual string GetChannel(object channelId)
		{
			if (_channels == null)
				_channels = IoC.Resolve<IChannelSecureService>().GetAll();

			int id = System.Convert.ToInt32(channelId, CultureInfo.CurrentCulture);
			var channel = _channels.FirstOrDefault(s => s.Id == id);
			return channel != null ? channel.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		protected virtual string GetCategoryPathIncludeGrid(object cat)
		{
			return CdonExportConfiguratorIncludeCategory.GetCategoryPath(cat);
		}
		protected virtual string GetCategoryPathExcludeGrid(object cat)
		{
			return CdonExportConfiguratorExcludeCategory.GetCategoryPath(cat);
		}

		protected string GetProductReasonIncludeGrid(object product)
		{
			return GetReason(State.IncludeProducts, ((IProduct) product).Id);
		}
		protected string GetProductReasonExcludeGrid(object product)
		{
			return GetReason(State.ExcludeProducts, ((IProduct)product).Id);
		}
		protected string GetCategoryReasonIncludeGrid(object cat)
		{
			return GetReason(State.IncludeCategories, ((ICategory)cat).Id);
		}
		protected string GetCategoryReasonExcludeGrid(object cat)
		{
			return GetReason(State.ExcludeCategories, ((ICategory)cat).Id);
		}
		protected string GetBrandReasonIncludeGrid(object brand)
		{
			return GetReason(State.IncludeBrands, ((IBrand)brand).Id);
		}
		protected string GetBrandReasonExcludeGrid(object brand)
		{
			return GetReason(State.ExcludeBrands, ((IBrand)brand).Id);
		}
		private string GetReason(IEnumerable<ICdonExportRestrictionItem> items, int itemId)
		{
			var reason = string.Empty;

			var item = items.FirstOrDefault(i => i.ItemId == itemId);
			if (item != null)
			{
				reason = item.Reason;
			}

			return reason;
		}

		protected bool IsCategoryChannelInclude(object channel)
		{
			var customChannel = (CustomChannel)channel;
			return IsChannelRestricted(State.IncludeCategories, customChannel);
		}
		protected bool IsCategoryChannelExclude(object channel)
		{
			var customChannel = (CustomChannel)channel;
			return IsChannelRestricted(State.ExcludeCategories, customChannel);
		}
		protected bool IsProductChannelInclude(object channel)
		{
			var customChannel = (CustomChannel)channel;
			return IsChannelRestricted(State.IncludeProducts, customChannel);
		}
		protected bool IsProductChannelExclude(object channel)
		{
			var customChannel = (CustomChannel)channel;
			return IsChannelRestricted(State.ExcludeProducts, customChannel);
		}
		protected bool IsBrandChannelInclude(object channel)
		{
			var customChannel = (CustomChannel)channel;
			return IsChannelRestricted(State.IncludeBrands, customChannel);
		}
		protected bool IsBrandChannelExclude(object channel)
		{
			var customChannel = (CustomChannel)channel;
			return IsChannelRestricted(State.ExcludeBrands, customChannel);
		}
		private bool IsChannelRestricted(IEnumerable<ICdonExportRestrictionItem> items, CustomChannel customChannel)
		{
			var isChannelRestricted = false;

			var item = items.FirstOrDefault(i => i.ItemId == customChannel.ItemId);
			if (item != null && item.Channels != null)
			{
				isChannelRestricted = item.Channels.ContainsKey(customChannel.Id);
			}

			return isChannelRestricted;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CdonExportConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CdonExportConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			BrandIncludeApplyToAllSelectedDiv.Style["display"] = CdonExportConfiguratorHelper.HasAnySelectedItem(BrandIncludeGrid) ? "block" : "none";

			ProductExcludeApplyToAllSelectedDiv.Style["display"] = CdonExportConfiguratorHelper.HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = CdonExportConfiguratorHelper.HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			BrandExcludeApplyToAllSelectedDiv.Style["display"] = CdonExportConfiguratorHelper.HasAnySelectedItem(BrandExcludeGrid) ? "block" : "none";

			IncludePanelDiv.Style["display"] = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0 || BrandIncludeGrid.Rows.Count > 0 ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 || BrandExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}

		private void InitializeConfiguratorIncludeProducts()
		{
			CdonExportConfiguratorIncludeProduct.Grid = ProductIncludeGrid;
			CdonExportConfiguratorIncludeProduct.DataSource = IncludeProductDataSource;
			CdonExportConfiguratorIncludeProduct.GridPageSizeSelect = ProductGridPageSizeSelect;
			CdonExportConfiguratorIncludeProduct.SearchResultControl = ProductIncludeSearchResultControl;
			CdonExportConfiguratorIncludeProduct.SearchFormControl = ProductIncludeSearchFormControl;
			CdonExportConfiguratorIncludeProduct.SearchButton = ProductIncludePopupSearchButton;
			CdonExportConfiguratorIncludeProduct.OkButton = ProductIncludePopupOkButton;
			CdonExportConfiguratorIncludeProduct.AddAllButton = ProductIncludePopupAddAllButton;
			CdonExportConfiguratorIncludeProduct.RemoveSelectionGridButton = RemoveSelectionFromProductIncludeGridButton;
			CdonExportConfiguratorIncludeProduct.GridUpdatePanel = ProductIncludeGridUpdatePanel;
			CdonExportConfiguratorIncludeProduct.ApplyToAllDiv = ProductIncludeApplyToAllSelectedDiv;
		}
		private void InitializeConfiguratorExcludeProducts()
		{
			CdonExportConfiguratorExcludeProduct.Grid = ProductExcludeGrid;
			CdonExportConfiguratorExcludeProduct.DataSource = ExcludeProductDataSource;
			CdonExportConfiguratorExcludeProduct.GridPageSizeSelect = ProductGridPageSizeSelect;
			CdonExportConfiguratorExcludeProduct.SearchResultControl = ProductExcludeSearchResultControl;
			CdonExportConfiguratorExcludeProduct.SearchFormControl = ProductExcludeSearchFormControl;
			CdonExportConfiguratorExcludeProduct.SearchButton = ProductExcludePopupSearchButton;
			CdonExportConfiguratorExcludeProduct.OkButton = ProductExcludePopupOkButton;
			CdonExportConfiguratorExcludeProduct.AddAllButton = ProductExcludePopupAddAllButton;
			CdonExportConfiguratorExcludeProduct.RemoveSelectionGridButton = RemoveSelectionFromProductExcludeGridButton;
			CdonExportConfiguratorExcludeProduct.GridUpdatePanel = ProductExcludeGridUpdatePanel;
			CdonExportConfiguratorExcludeProduct.ApplyToAllDiv = ProductExcludeApplyToAllSelectedDiv;
		}

		private void InitializeConfiguratorIncludeCategories()
		{
			CdonExportConfiguratorIncludeCategory.Grid = CategoryIncludeGrid;
			CdonExportConfiguratorIncludeCategory.OkButton = CategoryIncludePopupOkButton;
			CdonExportConfiguratorIncludeCategory.RemoveSelectionGridButton = RemoveSelectionFromCategoryIncludeGridButton;
			CdonExportConfiguratorIncludeCategory.GridUpdatePanel = CategoryIncludeGridUpdatePanel;
			CdonExportConfiguratorIncludeCategory.PopupCategoryTree = CategoryIncludePopupCategoryTree;
			CdonExportConfiguratorIncludeCategory.ApplyToAllDiv = CategoryIncludeApplyToAllSelectedDiv;
		}
		private void InitializeConfiguratorExcludeCategories()
		{
			CdonExportConfiguratorExcludeCategory.Grid = CategoryExcludeGrid;
			CdonExportConfiguratorExcludeCategory.OkButton = CategoryExcludePopupOkButton;
			CdonExportConfiguratorExcludeCategory.RemoveSelectionGridButton = RemoveSelectionFromCategoryExcludeGridButton;
			CdonExportConfiguratorExcludeCategory.GridUpdatePanel = CategoryExcludeGridUpdatePanel;
			CdonExportConfiguratorExcludeCategory.PopupCategoryTree = CategoryExcludePopupCategoryTree;
			CdonExportConfiguratorExcludeCategory.ApplyToAllDiv = CategoryExcludeApplyToAllSelectedDiv;
		}

		private void InitializeConfiguratorIncludeBrands()
		{
			CdonExportConfiguratorIncludeBrand.Grid = BrandIncludeGrid;
			CdonExportConfiguratorIncludeBrand.SearchGrid = BrandIncludeSearchGrid;
			CdonExportConfiguratorIncludeBrand.OkButton = BrandIncludePopupOkButton;
			CdonExportConfiguratorIncludeBrand.RemoveSelectionGridButton = RemoveSelectionFromBrandIncludeGridButton;
			CdonExportConfiguratorIncludeBrand.GridUpdatePanel = BrandIncludeGridUpdatePanel;
			CdonExportConfiguratorIncludeBrand.PopupUpdatePanel = BrandIncludePopupUpdatePanel;
			CdonExportConfiguratorIncludeBrand.ApplyToAllDiv = BrandIncludeApplyToAllSelectedDiv;
		}
		private void InitializeConfiguratorExcludeBrands()
		{
			CdonExportConfiguratorExcludeBrand.Grid = BrandExcludeGrid;
			CdonExportConfiguratorExcludeBrand.SearchGrid = BrandExcludeSearchGrid;
			CdonExportConfiguratorExcludeBrand.OkButton = BrandExcludePopupOkButton;
			CdonExportConfiguratorExcludeBrand.RemoveSelectionGridButton = RemoveSelectionFromBrandExcludeGridButton;
			CdonExportConfiguratorExcludeBrand.GridUpdatePanel = BrandExcludeGridUpdatePanel;
			CdonExportConfiguratorExcludeBrand.PopupUpdatePanel = BrandExcludePopupUpdatePanel;
			CdonExportConfiguratorExcludeBrand.ApplyToAllDiv = BrandExcludeApplyToAllSelectedDiv;
		}

		private ICdonExportRestriction PrepareSource(ICdonExportRestriction cdonExportRestriction)
		{
			cdonExportRestriction.IncludeProducts = BuildRstrictionChannels(cdonExportRestriction.IncludeProducts);
			cdonExportRestriction.IncludeCategories = BuildRstrictionChannels(cdonExportRestriction.IncludeCategories);
			cdonExportRestriction.IncludeBrands = BuildRstrictionChannels(cdonExportRestriction.IncludeBrands);
			cdonExportRestriction.ExcludeProducts = BuildRstrictionChannels(cdonExportRestriction.ExcludeProducts);
			cdonExportRestriction.ExcludeCategories = BuildRstrictionChannels(cdonExportRestriction.ExcludeCategories);
			cdonExportRestriction.ExcludeBrands = BuildRstrictionChannels(cdonExportRestriction.ExcludeBrands);

			return cdonExportRestriction;
		}

		private Collection<ICdonExportRestrictionItem> BuildRstrictionChannels(Collection<ICdonExportRestrictionItem> cdonExportRestrictionItems)
		{
			var restrictionItems = new Collection<ICdonExportRestrictionItem>();

			foreach (var restrictionItem in cdonExportRestrictionItems)
			{
				if (restrictionItems.FirstOrDefault(b => b.ItemId == restrictionItem.ItemId) == null)
				{
					restrictionItem.Channels = new Dictionary<int, int>();
					var items = cdonExportRestrictionItems.Where(b => b.ItemId == restrictionItem.ItemId);
					foreach (var item in items)
					{
						if (!restrictionItem.Channels.ContainsKey(item.ChannelId))
						{
							restrictionItem.Channels.Add(item.ChannelId, item.ChannelId);
						}
					}
					restrictionItems.Add(restrictionItem);
				}
			}

			return restrictionItems;
		}
	}
}