﻿function __scmAttachEvents(containerSelector, imageContextMenu, imageContextMenuMouseOver) {

    $(document).click(function(event) { __scmHideMenu(containerSelector, event); });
    $(containerSelector).find("div.scm-selector").find("img:first").click(function(event) { __scmShowHideMenu(containerSelector); });
    $(containerSelector).find("div.scm-menu-item").click(function(event) { __scmMenuClick(this, containerSelector); });
    $(containerSelector).find("div.scm-menu-item").mouseover(function(event) { __scmMouseoverEffect(this); });
    $(containerSelector).find("div.scm-menu-item").mouseout(function(event) { __scmMouseoutEffect(this); });

    $(containerSelector).find("div.scm-selector").find("img:first").mouseover(
        function(event) { $(event.target).attr("src", imageContextMenuMouseOver); });
    $(containerSelector).find("div.scm-selector").find("img:first").mouseout(
        function(event) { $(event.target).attr("src", imageContextMenu); });    
}

function __scmShowHideMenu(containerSelector) {
    var menu = $(containerSelector).find("div.scm-menu");
    
    var menuShadow = $(containerSelector).find("div.scm-menu-shadow");
    if ($(menu).is(":visible")) {
        $(menu).hide();
        $(menuShadow).hide();
    }
    else {
        $(menu).show();
        $(menuShadow).show();
    }
}

function __scmHideMenu(containerSelector, event) {

    if (null != event) {
        if ($(event.target).parents(containerSelector).length != 0) {
            event.stopPropagation();
            return;
        }

    }
    var menu = $(containerSelector).find("div.scm-menu");
    var menuShadow = $(containerSelector).find("div.scm-menu-shadow");

    $(menu).hide();
    $(menuShadow).hide();

}

function __scmMenuClick(source, containerSelector) {
    $(containerSelector).find("div.scm-selected").find("input:hidden").val($(source).find("input[type='hidden']").val());
    $(containerSelector).find("div.scm-selected").find("input:hidden").change();

    $(containerSelector).find("div.scm-selected").find("span:first").text($(source).text());
    $(containerSelector).find("div.scm-selected").find("img:first").attr('src', $(source).find("img:first").attr('src'));
    __scmShowHideMenu(containerSelector);
}

function __scmMouseoverEffect(item) {
    $(item).attr("class", "scm-menu-item-highlighted");
}

function __scmMouseoutEffect(item) {
    $(item).attr("class", "scm-menu-item");
}