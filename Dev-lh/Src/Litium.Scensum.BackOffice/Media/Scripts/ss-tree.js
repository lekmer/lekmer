﻿//$(document).ready(function() {
//    HideChildNodes();
//    UnmarkAllSelected();
//    HideMenu();
//    AddClickEvents();
//    SetTreeNodeExpandImage();
//});

function Startup(treeId) {
    HideChildNodes(treeId);
    UnmarkAllSelected(treeId);
    HideMenu();
    AddClickEvents(treeId);
    SetTreeNodeExpandImage(treeId);
}

function AddClickEvents(treeId) {
    $('div#'+treeId).find('a.node-expand').click(function() { Node_Action(this); });
    $('a#menu-close').click(function() { $("div#node-menu").hide(0); })
}

function HideChildNodes(treeId) {
    $('div#' + treeId).find("div.tree-item-childs").hide();
}

function HideMenu() {
    $('div#node-menu').hide(0);
}

function SetTreeNodeExpandImage(treeId) {
    $('div#' + treeId).find('img.tree-icon').attr({ src: treeiconscollapsedurl });
}

function SetSelected(obj) {
    var id = $(obj).attr('accesskey');
    var idContainer = document.getElementById(objIdClientId_select);
    if (null != idContainer) idContainer.value = id;

    UnmarkAllSelected(obj);
    $(obj).attr({ 'class': 'node-expanded' });

//    var hidden = document.getElementById(hiddenId);
//    if (null != hidden) hidden.value = id;
}

function UnmarkAllSelected(obj) {

    if (null != obj) $(obj).parents("div.treeview").find('a.node-expanded').attr({ 'class': 'node' });
    else {
//        if (trees != null) {
//            for (var i = 0; i < trees.length; i++) {
//                $(trees[i]).find('a.node-expanded').attr({ 'class': 'node' });
//            }
//      }
        //$("div.treeview").find('a.node-expanded').attr({ 'class': 'node' });
    }
}

function AddTree(id)
{
    trees.push(id);
} 

function Node_Action(obj) {
    if (obj.parentNode.parentNode.parentNode.id == 'node-expanded')//collapse
    {
        Collapse(obj.parentNode.parentNode.parentNode);
        obj.parentNode.parentNode.parentNode.id = '';
    }
    else//expand
    {
        Expand(obj.parentNode.parentNode.parentNode);
        obj.parentNode.parentNode.parentNode.id = 'node-expanded';
    }
}

function ExpandNode(key, oneLevel, treeId) {

    var nodes = $('div#' + treeId).find("a.node");
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].accessKey == key) {
            Expand(GetParentNode(nodes[i]), true);
            if (oneLevel) $(nodes[i]).attr({ 'class': 'node-expanded' });
        }
    }
}

function GetParentNode(node) {
    return node.parentNode.parentNode.parentNode;
}

function GetChildNodes(node) {
    var childNodes = new Array();
    var previousId = node.id;
    node.id = 'node-temp';

    var children = $("#node-temp").children();
    for (var i = 0; i < children.length; i++) {
        if (children[i].className == 'tree-item-childs') {
            childNodes.push(children[i]);
        }
    }
    node.id = previousId;

    return childNodes;
}

function Expand(node, afterPostback) {
    $(node).find('div.tree-item-parent:first').find('div.tree-item-cell-expand:first').find('a.node-expand:first').find('img.tree-icon:first').attr({ src: treeiconsexpandedurl });
    var childNodes = GetChildNodes(node);
    for (var i = 0; i < childNodes.length; i++) {
        $(childNodes[i]).show('fast');
    }
    
    var parentElem;
    //if (afterPostback)
        parentElem = $(node).parent('div').parent('div').get(0);
//    else
//        parentElem = $(node).parent('div').get(0);

    if (null != parentElem && parentElem.tagName == 'DIV' && parentElem.className == 'tree-item-row') Expand(parentElem)
}

function Collapse(parent) {
//find('div.tree-item-parent').find('div.tree-item-cell-expand').find('a.node-expand').
    $(parent).find('img.tree-icon').attr({ src: treeiconscollapsedurl });
    var previousId = parent.id;
    parent.id = 'node-temp';
    $('#node-temp').find("div.tree-item-childs").hide('fast');
    parent.id = previousId;
}

function HideNode(key, treeId) {
    var nodes = $('#' + treeId).find('a.node');
    for (var i = 0; i < nodes.length; i++) {
        if (nodes[i].accessKey == key) {
            $(GetParentNode(nodes[i])).hide();
            break;
        }
    }
}

function DoPostback(sender) {
    var key = sender.accessKey;
    var hidden = document.getElementById(hiddenId);
    if (hidden.value != key) {
        hidden.value = key;
        
        if (hidden != null) {
            __doPostBack(hiddenId, '_inpTVArg_ServerChange');
            return false;
        }
    }
}

function ShowMenu(obj) {
    SetMenuObjName(obj);
    CreateMenuLinks(obj);
    HideUnusefulMenuChoices(obj);
    ChangeMenuLinksPicturesAccordingToNodeType(obj);
    SetMenuPosition(obj);
}

function SetMenuPosition(obj) {
    //var position = obj.getBoundingClientRect();

    var pos = findPos(obj);
    $("div#node-menu").css({ top: pos[1] + 12, left: pos[0] + 12 });
    $("div#node-menu").show('fast');
}

function findPos(obj) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        }
        while (obj = obj.offsetParent);
    }
    return [curleft, curtop];
}

function ChangeMenuLinksPicturesAccordingToNodeType(obj) {
    var type = $(obj.parentNode).attr('node_type');
    switch (type) {
        case 'ShortcutLink':
            $('div#to-hidden').find('img').attr('src', img_short_hid);
            $('div#to-offline').find('img').attr('src', img_short_off);
            $('div#to-online').find('img').attr('src', img_short_on);
            ShowHidePageSubMenu(false);
            break;
        case 'UrlLink':
            $('div#to-online').find('img').attr('src', img_link_on);
            $('div#to-offline').find('img').attr('src', img_link_off);
            $('div#to-hidden').find('img').attr('src', img_link_hid);
            ShowHidePageSubMenu(false);
            break;
        case 'Page':
            $('div#to-online').find('img').attr('src', img_page_on);
            $('div#to-offline').find('img').attr('src', img_page_off);
            $('div#to-hidden').find('img').attr('src', img_page_hid);
            ShowHidePageSubMenu(true);
            CreatePageSpecificMenuLinks(obj);
            break;
        case 'Folder':
            $('div#to-online').find('img').attr('src', img_folder_on);
            $('div#to-offline').find('img').attr('src', img_folder_off);
            $('div#to-hidden').find('img').attr('src', img_folder_hid);
            ShowHidePageSubMenu(false);
            break;
    }
}

function ShowHidePageSubMenu(visible) {
    if (visible) {
        $('div#menu-pages-container').css({ visibility: 'visible' });
        $('div#menu-container').attr('class', 'menu-container-typeof-page');
        $('div#menu-shadow').attr('class', 'menu-shadow-typeof-page');
    }
    else {
        $('div#menu-pages-container').css({ visibility: 'hidden' });
        $('div#menu-container').attr('class', 'menu-container');
        $('div#menu-shadow').attr('class', 'menu-shadow');
    }
}

function HideUnusefulMenuChoices(obj) {
    $('div#to-hidden').show();
    $('div#to-offline').show();
    $('div#to-online').show();
    var status = $(obj.parentNode).attr('status');
    switch (status) {
        case 'Hidden':
            $('div#to-hidden').hide();
            break;
        case 'Offline':
            $('div#to-offline').hide();
            break;
        case 'Online':
            $('div#to-online').hide();
            break;
    }
}

function SetMenuObjName(obj) {
    var anchor = $(obj.parentNode.parentNode.parentNode).find('a.node');
    if (anchor.length == 0) anchor = $(obj.parentNode.parentNode.parentNode).find('a.node-expanded');
    var objText = $(anchor.get(0)).text();
    $('div#menu-row-close').find('span').text(objText);
}

function CreateMenuLinks(obj) {

    var id = $(obj.parentNode).attr('accesskey');
    document.getElementById(objIdClientId_edit).value = id;

    // prepare Page href
    var purl = page_create_url.concat('?pId={0}').replace('{0}', id);
    $("div#node-menu").find('a#page').attr('href', purl);

    //prepare Folder href
    var furl = folder_create_url.concat('?pId={0}').replace('{0}', id);
    $("div#node-menu").find('a#folder').attr('href', furl);

    //prepare Link href
    var lurl = link_edit_url.concat('?pId={0}').replace('{0}', id);
    $("div#node-menu").find('a#link').attr('href', lurl);

    //prepare Shortcut href
    var surl = shortcut_edit_url.concat('?pId={0}').replace('{0}', id);
    $("div#node-menu").find('a#shortcut').attr('href', surl);
}

function CreatePageSpecificMenuLinks(obj) {
    var id = $(obj.parentNode).attr('accesskey');
    
    var seourl = page_seo_url.concat('?Id={0}').replace('{0}', id);
    $("div#node-menu").find('a#seo').attr('href', seourl);

    var settingsurl = page_settings_url.concat('?Id={0}').replace('{0}', id);
    $("div#node-menu").find('a#settings').attr('href', settingsurl);
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Simple MENU

function ShowSimpleMenu(obj) {
	SetMenuObjName(obj);
	CreateSimpleMenuLinks(obj);
	SetMenuPosition(obj);
}

function CreateSimpleMenuLinks(obj) {
	var id = $(obj.parentNode).attr('accesskey');
	document.getElementById(objIdClientId_edit).value = id;

	var curl = category_create_url.concat('?pId={0}').replace('{0}', id);
	$("div#node-menu").find('a#category').attr('href', curl);

	var eurl = category_create_url.concat('?Id={0}').replace('{0}', id);
	$("div#node-menu").find('a#editCategory').attr('href', eurl);
}