﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ToolTip.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Tooltip.ToolTip" %>

<a href="#" class="tooltip" runat="server" id="TooltipTag">
	<img class="InformationIcon" alt="Information" src="../../Media/Images/Common/help.png" />
	<span class="classic"><asp:Literal ID="c_toolTip" runat="server" /></span>
</a>