﻿using System;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.UserControls.User.Events
{
	public class UserSearchCriteriaEventArgs : EventArgs
	{
		public IUserSearchCriteria SearchCriteria
		{
			get;
			set;
		}
		public int? PageIndex
		{
			get;
			set;
		}
	}
}