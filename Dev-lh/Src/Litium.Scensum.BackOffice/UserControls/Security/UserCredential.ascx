﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UserCredential.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Security.UserCredential" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>     
<script type="text/javascript">
	$(function() {
		var user = $("input[name*='UserNameTextBox']");
		var clear = $("input[name*='ClearHiddenField']");
		if (user!= null && clear != null && clear.val() == "true") {
			user.val("");
		}
	})
</script>

<div id="user-credential">
	<label><h5>User credential</h5></label>
	<div class="input-box">
		<span>Username *</span><br />
		<asp:TextBox ID="UserNameTextBox" runat="server" />
		<asp:RequiredFieldValidator runat="server" ID="UserNameRequiredFieldValidator" ControlToValidate="UserNameTextBox" ErrorMessage="User name should not be empty and require email template"  Display="None" ValidationGroup="vgUserEdit" />
		<asp:RegularExpressionValidator ID="UserNameRegularExpressionValidator" runat="server" ControlToValidate="UserNameTextBox"
			ErrorMessage="User name should require email template" Text=" *" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
			ValidationGroup="vgUserEdit" />
		<asp:HiddenField ID="ClearHiddenField" runat="server"/>
		<script type="text/javascript">
			{
				var user = $("input[name*='UserNameTextBox']");
				var clear = $("input[name*='ClearHiddenField']");
				if (clear != null && user != null && user.val() == "") {
					user.val(" "); 
					clear.val("true");
				} 
			}
        </script>
	</div>
	<div id="PasswordOptionsDiv" runat="server" class="input-box-date">
		<span>Password options *</span><br />
		<asp:RadioButton ID="AutoPasswordRadioButton" runat="server" GroupName="Password"  AutoPostBack="true" Checked="true" />Auto generate
		<asp:RadioButton ID="ManualPasswordRadioButton" runat="server" AutoPostBack="true" GroupName="Password" />Enter manually
	</div>
	<br/>
	<br/>
	<br/>
	<div id="PasswordDiv" runat="server">
		<div class="input-box">
			<span>Enter password *</span><br />
			<asp:TextBox ID="PasswordTextBox"  Enabled="false" runat="server" TextMode="Password" />
			<asp:RegularExpressionValidator ID="PasswordRegularExpressionValidator" runat="server" ControlToValidate="PasswordTextBox"
				ErrorMessage="Not correct password requirements (8 digit random text consisting of the letters a-z, A-Z or the numbers 0-9)." Text=" *" ValidationExpression="(\w(\s)?){8,}$"
				ValidationGroup="vgUserEdit" />
			<asp:HiddenField ID="PasswordHidden" runat="server" />
		</div>
		<div id="ConfirmPasswordDiv" class="input-box" Visible="false" runat="server">
			<span>Confirm password *</span><br />
			<asp:TextBox ID="ConfirmTextBox" TextMode="Password" runat="server"/>&nbsp;
			<asp:CompareValidator Display ="None"  id="ConfirmPasswordCompareValidator" runat="server" ErrorMessage="Passwords do not match!" Text=" *" ControlToValidate="PasswordTextBox" 
				ControlToCompare="ConfirmTextBox" ValidationGroup="vgUserEdit" />
				<asp:RequiredFieldValidator runat="server" ID="ConfirmPasswordRequiredFieldValidator" ControlToValidate="PasswordTextBox" ErrorMessage="Password should not be empty"  Display="None" ValidationGroup="vgUserEdit" />
			<asp:HiddenField ID="ConfirmHidden" runat="server" />
		</div>
	</div>

 </div>