﻿using System;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class ProductSearch : ProductSearchCore
	{
		protected override void OnSearch(ProductSearchEventArgs e)
		{
			base.OnSearch(e);
			mpeSearchProducts.Hide();
		}

		protected override void SetEventHandlers()
		{
			base.SetEventHandlers();
			btnAddAll.Click += BtnAddAllClick;
		}

		protected override void ProductSearchEvent(object sender, ProductSearchCriteriaEventArgs e)
		{
			base.ProductSearchEvent(sender, e);
			btnAddAll.Visible = true;
		}

		/// <summary>
		/// Performs DataBind method for category tree.
		/// </summary>
		public virtual void RestoreCategoryTree()
		{
			search.RestoreCategoryTree();
		}

		private void BtnAddAllClick(object sender, EventArgs e)
		{
			if (GVSearch.Rows.Count <= 0) return;

			var criterias = search.CreateSearchCriteria();

			var searchCriteria = (ILekmerProductSearchCriteria)IoC.Resolve<IProductSearchCriteria>();
			searchCriteria.CategoryId = criterias.CategoryId;
			searchCriteria.Title = criterias.Title;
			searchCriteria.PriceFrom = criterias.PriceFrom;
			searchCriteria.PriceTo = criterias.PriceTo;
			searchCriteria.ErpId = criterias.ErpId;
			searchCriteria.StatusId = criterias.StatusId;
			searchCriteria.EanCode = criterias.EanCode;
			searchCriteria.BrandId = criterias.BrandId;
			searchCriteria.SupplierArticleNumber = criterias.SupplierArticleNumber;

			var productSecureService = IoC.Resolve<IProductSecureService>();
			var products = productSecureService.Search(
				ChannelHelper.CurrentChannel.Id,
				searchCriteria,
				0,
				int.MaxValue);

			if (products.Count > 0)
			{
				btnAddAll.Visible = false;
				productDataSource.SelectParameters.Clear();
				OnSearch(new ProductSearchEventArgs { Products = products });
			}
		}
	}
}