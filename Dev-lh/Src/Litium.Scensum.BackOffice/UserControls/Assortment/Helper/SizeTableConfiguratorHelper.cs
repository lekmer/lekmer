﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Helper
{
	public class SizeTableConfiguratorHelper
	{
		public readonly string DOWN_IMG = "~/Media/Images/Common/down.gif";
		public readonly string UP_IMG = "~/Media/Images/Common/up.gif";

		public Collection<int> GetSelectedIdsFromGrid(System.Web.UI.WebControls.GridView gridView, string idHiddenFieldName)
		{
			var ids = new Collection<int>();

			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					int id = Convert.ToInt32(((HiddenField)row.FindControl(idHiddenFieldName)).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}

			return ids;
		}

		public Collection<string> GetSelectedStringIdsFromGrid(System.Web.UI.WebControls.GridView gridView, string idHiddenFieldName)
		{
			var ids = new Collection<string>();

			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					ids.Add(((HiddenField) row.FindControl(idHiddenFieldName)).Value);
				}
			}

			return ids;
		}

		public bool HasAnySelectedItem(System.Web.UI.WebControls.GridView grid)
		{
			foreach (GridViewRow row in grid.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					return true;
				}
			}

			return false;
		}

		public void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row, HtmlGenericControl applyAllDiv)
		{
			string scriptShowBlockOption = applyAllDiv != null ? @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');" : string.Empty;

			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", @"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');" + scriptShowBlockOption);
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", @"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');" + scriptShowBlockOption);
			}
		}
	}
}