﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearchResult.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ProductSearchResult" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<sc:GridViewWithCustomPager
	ID="ProductGrid" 
	SkinID="grid" 
	runat="server"
	AllowPaging="true" 
	PageSize="<%$AppSettings:DefaultGridPageSize%>" 
	AutoGenerateColumns="false" 
	DataSourceID="productDataSource" 
	Width="100%">
	<Columns>
		<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
			<HeaderTemplate>
				<asp:CheckBox id="chkAllSearched" runat="server" />
			</HeaderTemplate>
			<ItemTemplate>
				<asp:CheckBox ID="cbSelect" runat="server" />
				<asp:HiddenField ID="hfId" Value='<%#Eval("Id") %>' runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
		
		<asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="10%" />
		<asp:BoundField HeaderText="<%$ Resources:General, Literal_Title %>" DataField="DisplayTitle" ItemStyle-Width="60%" />
		<asp:BoundField HeaderText="<%$ Resources:Product, Literal_Ean %>" DataField="EanCode" ItemStyle-Width="10%" />
		<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
			<ItemTemplate>
				<asp:Label id="lblProductStatus" runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
			<ItemTemplate>
				<asp:Label ID="lblType" runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" ItemStyle-Width="7%" ItemStyle-HorizontalAlign="Right">
			<ItemTemplate>
				<asp:Label id="lblPrice" runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</sc:GridViewWithCustomPager>
<asp:ObjectDataSource ID="productDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />
