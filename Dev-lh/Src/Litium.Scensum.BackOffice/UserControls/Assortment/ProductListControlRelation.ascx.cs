﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using ProductTypeConstant = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class ProductListControlRelation : StateUserControlController<ProductListControlRelationState>
	{
		private Collection<IProductStatus> _productStatuses;
		private Collection<IProductType> _productTypes;

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IProduct> Products
		{
			get
			{
				return State.Products;
			}
			set
			{
				InitializeState();
				State.Products = value;
			}
		}
		public Collection<IProduct> DeniedProducts
		{
			get
			{
				return State.DeniedProducts;
			}
			set
			{
				InitializeState();
				State.DeniedProducts = value;
			}
		}

		public event EventHandler<ProductSearchEventArgs> ChangeProductListEvent;
		protected virtual void OnChangeProductListEvent(ProductSearchEventArgs e)
		{
			if (ChangeProductListEvent != null)
			{
				ChangeProductListEvent(this, e);
			}
		}

		protected virtual void InitializeState()
		{
			if (State == null)
			{
				State = new ProductListControlRelationState
				{
					Products = new Collection<IProduct>(),
					DeniedProducts = new Collection<IProduct>()
				};
			}
		}

		protected override void SetEventHandlers()
		{
			GVSave.RowDataBound += ProductGridRowDataBound;
			GVSave.RowCommand += ProductGridRowCommand;
			GVSave.PageIndexChanging += ProductGridPageIndexChanging;
			productSearch.SearchEvent += ProductSearchSearchEvent;
			RemoveSelectedButton.Click += RemoveSelectedClick;
		}

		protected override void PopulateControl() { GridDataBind(); }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			GridDataBind();
		}

		private void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GVSave.PageIndex = e.NewPageIndex;
			GridDataBind();
		}

		private void ProductSearchSearchEvent(object sender, ProductSearchEventArgs e)
		{
			foreach (IProduct product in e.Products)
			{
				int productId = product.Id;
				if (Products.FirstOrDefault(item => item.Id == productId) == null
					&& DeniedProducts.FirstOrDefault(item => item.Id == productId) == null)
				{
					Products.Add(product);
				}
			}

			GridDataBind();
			upProductList.Update();

			OnChangeProductListEvent(new ProductSearchEventArgs { Products = Products });
		}

		private void GridDataBind()
		{
			GVSave.DataSource = Products;
			GVSave.DataBind();
			SetClientFunction();
		}

		private void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var product = (IProduct)row.DataItem;
			var lekmerProduct = (ILekmerProduct)product;

			var lblPrice = (Label)e.Row.FindControl("lblPrice");
			if (product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}

			var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
			lblProductStatus.Text = ResolveStatusTitle(product.ProductStatusId);

			var lblType = (Label)e.Row.FindControl("lblType");
			lblType.Text = ResolveTypeTitle(lekmerProduct.ProductTypeId);

			var lblSuplierArtNo = (Label)e.Row.FindControl("lblSuplierArtNo");
			lblSuplierArtNo.Text = lekmerProduct.SupplierArticleNumber;
		}

		private void ProductGridRowCommand(object sender, CommandEventArgs e)
		{
			int productId;

			if (!int.TryParse(e.CommandArgument.ToString(), out productId))
			{
				productId = 0;
			}

			switch (e.CommandName)
			{
				case "DeleteProduct":
					IProduct product = Products.First(item => item.Id == productId);
					Products.Remove(product);
					break;
			}
			GridDataBind();
			OnChangeProductListEvent(new ProductSearchEventArgs { Products = Products });
		}

		protected string GetCategory(int categoryId)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			var category = categorySecureService.GetById(categoryId);
			return category != null ? category.Title : string.Empty;
		}

		protected virtual void RemoveSelectedClick(object sender, EventArgs e)
		{
			var ids = GVSave.GetSelectedIds();
			if (ids.Count() == 0) return;

			foreach (var id in ids)
			{
				var productId = id;
				var product = Products.First(item => item.Id == productId);
				if (product == null) continue;

				Products.Remove(product);
			}
			GridDataBind();
			OnChangeProductListEvent(new ProductSearchEventArgs { Products = Products });
		}

		protected virtual void SetClientFunction()
		{
			if (GVSave.HeaderRow == null) return;
			var selectAllCheckBox = (CheckBox)GVSave.HeaderRow.FindControl("SelectAllCheckBox");
			selectAllCheckBox.Checked = false;
			selectAllCheckBox.Attributes.Add("onclick",
				"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + GVSave.ClientID + @"'); ShowBulkUpdatePanel('"
				+ selectAllCheckBox.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			foreach (GridViewRow row in GVSave.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('"
					+ cbSelect.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual string ResolveStatusTitle(int statusId)
		{
			if (_productStatuses == null)
			{
				var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
				_productStatuses = productStatusSecureService.GetAll();
			}
			if (_productStatuses == null)
			{
				_productStatuses = new Collection<IProductStatus>();
			}

			var productStatus = _productStatuses.First(item => item.Id == statusId).Title;

			return productStatus;
		}

		protected virtual string ResolveTypeTitle(int typeId)
		{
			if (_productTypes == null)
			{
				var productTypeSecureService = IoC.Resolve<IProductTypeSecureService>();
				_productTypes = productTypeSecureService.GetAll();
			}
			if (_productTypes == null)
			{
				_productTypes = new Collection<IProductType>();
			}

			var productType = _productTypes.First(item => item.Id == typeId).Title;

			return productType;
		}
	}

	[Serializable]
	public sealed class ProductListControlRelationState
	{
		public Collection<IProduct> Products { get; set; }
		public Collection<IProduct> DeniedProducts { get; set; }
	}
}