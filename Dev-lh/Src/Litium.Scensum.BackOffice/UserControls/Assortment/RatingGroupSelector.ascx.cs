using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.UI.WebControls;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class RatingGroupSelector : StateUserControlController<RatingGroupSelectorState>
	{
		private IEnumerable<INode> _nodes;

		public event EventHandler<RatingGroupSelectEventArgs> SelectEvent;

		protected override void SetEventHandlers()
		{
			GroupTree.NodeCommand += OnNodeCommand;
			GroupGrid.PageIndexChanging += OnPageIndexChanging;
			GroupGrid.RowDataBound += OnRowDataBound;
			GroupGrid.DataBound += OnDataBound;
			AddButton.Click += OnAdd;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (!IsPostBack)
			{
				State = new RatingGroupSelectorState();
			}

			if (State.DenySelection.HasValue)
			{
				GroupTree.DenySelection = State.DenySelection.Value;
			}
		}

		protected virtual void OnAdd(object sender, EventArgs e)
		{
			var ids = GroupGrid.GetSelectedIds();
			var ratingGroups = new Collection<IRatingGroup>();
			var service = IoC.Resolve<IRatingGroupSecureService>();
			foreach (var id in ids)
			{
				ratingGroups.Add(service.GetById(id));
			}

			if (ratingGroups.Count > 0 && SelectEvent != null)
			{
				SelectEvent(this, new RatingGroupSelectEventArgs { RatingGroups = ratingGroups });
				GroupSelectPopup.Hide();
			}
		}

		protected virtual void OnDataBound(object sender, EventArgs e)
		{
			AddButton.Enabled = GroupGrid.Rows.Count > 0;
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GroupGrid.PageIndex = e.NewPageIndex;
			PopulateGrid(State.SelectedNodeId);
		}

		protected override void PopulateControl()
		{
			PopulateTree(null);
			PopulateGrid(null);
		}

		protected virtual void PopulateTree(int? folderId)
		{
			GroupTree.DataSource = GetFolders(folderId);
			GroupTree.RootNodeTitle = Resources.RatingReview.Label_RatingGroups;
			GroupTree.DataBind();
			GroupTree.SelectedNodeId = folderId ?? GroupTree.RootNodeId;
		}

		protected virtual void PopulateGrid(int? folderId)
		{
			int? id = folderId != GroupTree.RootNodeId
				? folderId
				: -1;

			GroupGrid.DataSource = id.HasValue ? IoC.Resolve<IRatingGroupSecureService>().GetAllByFolder(id.Value) : new Collection<IRatingGroup>();

			GroupGrid.DataBind();
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					SetSelection(false);
					break;
				case "Navigate":
					PopulateGrid(e.Id);
					SetSelection(true);
					State.SelectedNodeId = e.Id;
					break;
			}
		}

		protected virtual IEnumerable<INode> GetFolders(int? nodeId)
		{
			return _nodes ?? (_nodes = IoC.Resolve<IRatingGroupFolderSecureService>().GetTree(nodeId));
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"')");
			}
		}

		protected virtual void SetSelection(bool isSelected)
		{
			State.DenySelection = GroupTree.DenySelection = !isSelected;
		}
	}

	[Serializable]
	public sealed class RatingGroupSelectorState
	{
		public int? SelectedNodeId { get; set; }
		public bool? DenySelection { get; set; }
	}
}