﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TagList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.TagList" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<script type="text/javascript">
	function CollapseExpand(targetId, stateHolderId, buttonId, collapseImg, expandImg) {
		var stateHolder = document.getElementById(stateHolderId);
		var button = document.getElementById(buttonId);
		var target = document.getElementById(targetId);

		if (!stateHolder.checked) {
			stateHolder.checked = true;
			button.src = collapseImg;
			target.style.display = "block";
		}
		else {
			stateHolder.checked = false;
			button.src = expandImg;
			target.style.display = "none";
		}
	}
</script>

<asp:UpdatePanel ID="TagGroupUpdatePanel" runat="server" >
	<ContentTemplate>
		<div class="tags-list-container" style="width:660px;">
			<div class="left">
				<div style="display: none">
					<asp:CheckBox ID="ShowTagGroup" runat="server" Checked="true" />
				</div>
				<asp:ImageButton ID="CollapseButton" runat="server" ImageUrl="~/Media/Images/Tree/tree-collapse.png" BorderStyle="Solid" BorderWidth="1px" BorderColor="#F2F2F2" />
				<asp:LinkButton ID="TagGroupLink" runat="server" ForeColor="Blue" Font-Underline="true"></asp:LinkButton>
			</div>
			<br clear="all" />
			<div class="data-row" id="TagsDiv" runat="server" style="padding-left: 25px;">
				<div class="data-column-tags">
					<asp:ListBox Id="AvailableTagsList" runat="server" DataValueField="Id" DataTextField="Value" Rows="10" CssClass="list-box-tags" SelectionMode="Multiple"></asp:ListBox>
				</div>
				<div class="data-column-tags-center">
					<uc:ImageLinkButton ID="MoveToSelectedButton" runat="server" Text=">>" SkinID="DefaultButton" />
					<br style="clear: both;"/>
					<uc:ImageLinkButton ID="MoveToAvailableButton" runat="server" Text="<<" SkinID="DefaultButton" />
				</div>
				<div class="data-column-tags">
					<asp:ListBox Id="SelectedTagsList" runat="server" DataValueField="Id" DataTextField="Value" Rows="10" CssClass="list-box-tags" SelectionMode="Multiple"></asp:ListBox>
				</div>
			</div>
		</div>

		<div id="TagEditForm" runat="server" class="popup-tags-container" style="z-index: 10010; display: none;">
			<div id="popup-tags-header">
				<div id="popup-tags-header-left">
				</div>
				<div id="popup-tags-header-center">
					<span><asp:Literal ID="TagGroupTitleLiteral" runat="server"></asp:Literal></span>
					<input type="button" id="TagPopupCloseButton" runat="server" value="x"/>
				</div>
				<div id="popup-tags-header-right">
				</div>
			</div>
			<div id="popup-tags-content">
				<div id="messages">
					<uc:MessageContainer runat="server" ID="TagPopupMessenger" MessageType="Failure" HideMessagesControlId="NewTagButton" />
				</div>
				<div class="left">
					<div class="left">
						<div class="left" style="width: 75px; padding-top: 5px;"><span><%= Resources.General.Literal_Value %></span></div>
						<div class="left"><asp:TextBox ID="NewTagBox" runat="server" Width="210px" /></div>
					</div>
					<div class="left">
						<div class="left" style="width: 75px; padding-top: 5px;"><span><%= Resources.General.Literal_CommonName %></span></div>
						<div class="left"><asp:TextBox ID="NewTagCommonNameBox" runat="server" Width="210px" /></div>
					</div>
				</div>
				<br class="clear-all" />
				<div class="right" style="padding-right: 10px;">
					<uc:ImageLinkButton ID="NewTagButton" runat="server" Text="<%$ Resources:General, Button_Add %>" SkinID="DefaultButton"/>
				</div>
				<br style="clear:both;"/>
				<div class="left" style="width: 100%; max-height: 305px; overflow-y: scroll; overflow-x: hidden; margin-top: 5px;">
					<asp:GridView
						ID="TagGrid" 
						runat="server" 
						SkinID="GridWithoutPaging" 
						AutoGenerateColumns="false"
						Width="95%">
						<Columns>
							<asp:TemplateField ItemStyle-Width="56%" HeaderText="<%$ Resources:General,Literal_Value %>">
								<ItemTemplate>
									<div id="tag-grid-item">
										<asp:TextBox ID="TagValueBox" runat="server" Text='<%# Eval("Value") %>' BorderStyle="None" Width="77%" onfocus="$(this).next().focus();"></asp:TextBox>
										&nbsp;
										<uc:GenericTranslator ID="TagValueTranslator" runat="server" UseManualDataBinding="true" />
									</div>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField ItemStyle-Width="40%" HeaderText="<%$ Resources:General,Literal_CommonName %>">
								<ItemTemplate>
									<asp:TextBox ID="TagCommonNameBox" runat="server" Text='<%# Eval("CommonName") %>' BorderStyle="None" ReadOnly="True" Width="77%"/>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteTag" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" OnClientClick="return DeleteTagConfirmation();" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</div>
				<br style="clear:both;"/>
				<div style="float:right; padding: 10px; margin-bottom: -15px;">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="TagPopupOkButton" runat="server" Text="<%$ Resources:General, Button_Ok %>" SkinID="DefaultButton" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="TagPopupCancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
				</div>
			 </div>
		</div>
		<asp:HiddenField runat="server" ID="FakePopupTarget" />

		<ajaxToolkit:ModalPopupExtender 
			ID="TagPopupExtender" 
			runat="server"
			TargetControlID="FakePopupTarget"
			PopupControlID="TagEditForm"
			CancelControlID="TagPopupCloseButton"
			BackgroundCssClass="popup-background"/>
	</ContentTemplate>
</asp:UpdatePanel>