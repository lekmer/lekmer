﻿<%@ Control
	Language="C#"
	CodeBehind="RegistryRestriction.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.RegistryRestriction" %>

	<script language="javascript" type="text/javascript">
		function Enable(checkBox) {
			var tableRow = checkBox.parentNode.parentNode;
			if (checkBox.checked) {
				tableRow.cells[2].childNodes[1].checked = true;
			}
		}
	</script>

<span class="bold">Registry Restriction</span>
<asp:GridView ID="ProductRegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" >
	<Columns>
		<asp:TemplateField HeaderText="Registry" ItemStyle-Width="19%">
			<ItemTemplate>
				<asp:Label ID="ProductRegistryTitleLabel" runat="server" Text='<%# Eval("Title") %>' />
				<asp:HiddenField ID="ProductRegistryIdHidden" runat="server" Value='<%# Eval("ProductRegistryId").ToString() %>' />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Restriction" ItemStyle-Width="10%">
			<ItemStyle HorizontalAlign="Center" />
			<ItemTemplate>
				<asp:CheckBox ID="RestrictionCheckBox" runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Page Offline" ItemStyle-Width="10%">
			<ItemStyle HorizontalAlign="Center" />
			<ItemTemplate>
				<asp:CheckBox ID="OfflineCheckBox" runat="server" />
				<asp:HiddenField ID="ContentNodeIdHidden" runat="server" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText="Restriction Reason">
			<ItemTemplate>
				<asp:TextBox ID="RestrictionReasonText" runat="server" Width="99%" />
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
</asp:GridView>