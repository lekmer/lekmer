using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls;
using TemplateField=System.Web.UI.WebControls.TemplateField;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class VariantsList : UserControlController
    {
        #region Members

        private const string ProductsKey = "ProductsKey";
        private const string VariationsKey = "VariationsKey";
    	private const string DdlVariationValuesId = "ddlVariationValues";
        private const string SeparatorVariationValues = "_";
        private const string ResetTag = "<Reset>";
        private const string HiddenProductId = "hdnProductId";
        
        public int VariationGroupId;

        private readonly IProductVariationSecureService _productVariationService = 
            IoC.Resolve<IProductVariationSecureService>();
    	private readonly IProductSecureService _productSecureService = 
            IoC.Resolve<IProductSecureService>();

        #endregion

        #region Properties

		public bool HasDefaultVariant { get; set; }
		public int? DefaultProductId
		{
			get
			{
				int defaultProductId;
				if (int.TryParse(hfDefaultProduct.Value, out defaultProductId))
				{
					return defaultProductId;
				}
				return null;
			}
			set
			{
				hfDefaultProduct.Value = value.ToString();
			}
		}

    	private Collection<IProduct> _products;
		public Collection<IProduct> Products
		{
			get
			{
				if (_products == null)
				{
					_products = new Collection<IProduct>();
				}
				return _products;
			}
		}

		private Collection<VariationTypeWrapper> _variationTypes;
		public Collection<VariationTypeWrapper> VariationTypes
    	{
			get
			{
				if (_variationTypes == null)
				{
					_variationTypes = new Collection<VariationTypeWrapper>();
				}
				return _variationTypes;
			}
    	}

		// This is list of affecting product variations.
		// Note : You should first access either getter or setter of ProductVariations 
		// to have _productVariationsDictionary filled in correctly.
		// This is due to saving state between postbacks and updating it after user input.
		private Dictionary<int, Collection<ProductVariationWrapper>> _productVariationsDictionary;

    	public Collection<IProductVariation> ProductVariations
    	{
    		get
    		{
    			var productVariations = new Collection<IProductVariation>();
    			foreach (ProductVariationWrapper productVariationWrapper in ProductVariationWrappers)
    			{
    				productVariations.Add(productVariationWrapper.ProductVariation);
    			}

    			return productVariations;
    		}
    	}

		public Collection<ProductVariationWrapper> ProductVariationWrappers
        {
            get
            {
				var variations = new Collection<ProductVariationWrapper>();
				_productVariationsDictionary = new Dictionary<int, Collection<ProductVariationWrapper>>();
                foreach (GridViewRow row in GV.Rows)
                {
                    foreach (Control cell in row.Controls)
                    {
						foreach (Control control in cell.Controls)
						{
							var ddlVariations = control as DropDownList;
							if (ddlVariations == null
							    || !ddlVariations.ID.Contains(VariationTemplate.DdlVariationsId)
							    || string.IsNullOrEmpty(ddlVariations.SelectedValue))
							{
								continue;
							}

							// Get product id.
							HtmlInputHidden hdnProductId = null;
							try
							{
								hdnProductId = row.FindControl(HiddenProductId) as HtmlInputHidden;
							}
							catch (HttpException)
							{
							}

							if (hdnProductId == null)
								continue;

							// Get variation type name.
							// The first element should always be ddlVariations.
							// And there should always be more than 1 element.
							string[] strarr = ddlVariations.ID.Split(new[] {VariationTemplate.Separator},
							                                         StringSplitOptions.None);
							if (strarr.Length < 2 || string.IsNullOrEmpty(strarr[1]))
								continue;

							var productVariationWrapper = new ProductVariationWrapper
							                                                  	{
							                                                  		ProductVariation = _productVariationService.Create(),
							                                                  		VariationGuid = new Guid(ddlVariations.SelectedItem.Value),
							                                                  		VariationTypeGuid = new Guid(strarr[1])
							                                                  	};

							int productId;
							if(!int.TryParse(hdnProductId.Value, out productId))
							{
								productId = 0;
							}
							productVariationWrapper.ProductVariation.ProductId = productId;
							productVariationWrapper.ProductVariation.Id = -1;
							productVariationWrapper.ProductVariation.VariationValue = ddlVariations.SelectedItem.Text;
							productVariationWrapper.ProductVariation.VariationGroupId = VariationGroupId;

							VariationTypeWrapper sameVariationType =
								VariationTypes.FirstOrDefault(vt => vt.Guid == productVariationWrapper.VariationTypeGuid);
							if (sameVariationType == null)
							{
								continue;
							}

							productVariationWrapper.ProductVariation.VariationTypeName = sameVariationType.VariationType.Title;

							// productVariation completely filled in with data except Id.
							variations.Add(productVariationWrapper);

							// Update page's state responsible for selected product variations in the drop downs.
							if (!_productVariationsDictionary.ContainsKey(productId) || _productVariationsDictionary[productId] == null)
							{
								_productVariationsDictionary.Add(productId, new Collection<ProductVariationWrapper>());
							}
							_productVariationsDictionary[productId].Add(productVariationWrapper);
						}
                    }
                }

                return variations;
            }

			set
			{
				if (value == null)
					return;

				_productVariationsDictionary = new Dictionary<int, Collection<ProductVariationWrapper>>();
				foreach (GridViewRow row in GV.Rows)
				{
					HtmlInputHidden hdnProductId = null;
					try
					{
						hdnProductId = row.FindControl(HiddenProductId) as HtmlInputHidden;
					}
					catch (HttpException)
					{
					}

					if (hdnProductId == null)
						continue;

					int productId;
					if(!int.TryParse(hdnProductId.Value, out productId))
					{
						productId = 0;
					}
					if (productId < 1)
						continue;

					List<ProductVariationWrapper> productVariationsList =
						value.ToList().FindAll(prodVariation => prodVariation.ProductVariation.ProductId == productId);
					foreach (ProductVariationWrapper productVariationWrapper in productVariationsList)
					{
						var ddlVariations =
							row.FindControl(string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}",
							                              VariationTemplate.DdlVariationsId,
							                              VariationTemplate.Separator,
							                              productVariationWrapper.VariationTypeGuid)) as DropDownList;

						if (ddlVariations != null
						    && ddlVariations.Items.FindByValue(productVariationWrapper.VariationGuid.ToString()) != null)
						{
							ddlVariations.SelectedValue = productVariationWrapper.VariationGuid.ToString();

							// Update page's state responsible for selected product variations in the drop downs.
							if (!_productVariationsDictionary.ContainsKey(productId) || _productVariationsDictionary[productId] == null)
							{
								_productVariationsDictionary.Add(productId, new Collection<ProductVariationWrapper>());
							}
							_productVariationsDictionary[productId].Add(productVariationWrapper);
						}
					}
				}
			}
        }

        #endregion

        #region Initialization
		protected override void SetEventHandlers()
		{
			GV.RowDataBound += VariantsGridRowDataBound;
			GV.RowCommand += VariantsGridDeleteCommand;
			GV.DataBound += VariantsGridDataBound;


			productSearch.SearchEvent += ProductSearchEvent;
			btnSet.Click += BtnSet_OnClick;
			btnRemove.Click += BtnRemove_OnClick;
		}

		protected override void PopulateControl(){}

		private void ProductSearchEvent(object sender, ProductSearchEventArgs e)
		{
			foreach (IProduct product in e.Products)
			{
			    int productId = product.Id;
				if (Products.FirstOrDefault(item => item.Id == productId) == null)
				{
					Products.Add(product);
				}
			}

			GV.DataSource = Products;
			GV.DataBind();

			upVariationProducts.Update();
		}
        
        #endregion

        #region Data bind

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
			btnSet.Visible = false;

            // Bind dynamic columns with product variations
            if (VariationTypes != null)
            {
                foreach (VariationTypeWrapper variationTypeWrapper in VariationTypes)
                {
                    bool isDuplicateColumn = false;
                    foreach (DataControlField columnPossiblyDuplicate in GV.Columns)
                    {
						if (columnPossiblyDuplicate.HeaderText == variationTypeWrapper.VariationType.Title)
                        {
                            isDuplicateColumn = true;
                            break;
                        }
                    }

                    // If there is already column with the same name, just skip this column.
                    // The page will be swtched to tab Criterias now 
                    // and rebound correctly on the next valid postback.
                    if (isDuplicateColumn)
                    {
                        continue;
                    }

					AddVariationTypeColumnToGrid(variationTypeWrapper);
					AddColumnsToMultipleActionsRegion(variationTypeWrapper);
                }
            }

			PopulateVariantsGrid();

            // Renew selected values.
			ProductVariationWrappers = ViewState[VariationsKey] as Collection<ProductVariationWrapper>;
			
        }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (HasDefaultVariant || GV.Rows.Count <= 0) return;

			var hdnProductId = (HtmlInputHidden)GV.Rows[0].FindControl("hdnProductId");
			if (hdnProductId == null || string.IsNullOrEmpty(hdnProductId.Value)) return;

			int defaultProductId;
			if (!int.TryParse(hdnProductId.Value, out defaultProductId)) return;

			DefaultProductId = defaultProductId;
			var rbtnDefault = (RadioButton)GV.Rows[0].FindControl("rbtnDefault");
			rbtnDefault.Checked = true;
		}

		private void PopulateVariantsGrid()
		{
			// First load. We can't use !IsPostBack because tab switching is a server click.
			if (Products.Count == 0)
			{
				// Get products and product variations.
				Products.AddRange(_productSecureService.GetAllByVariationGroup(ChannelHelper.CurrentChannel.Id, VariationGroupId));

				_productVariationsDictionary = new Dictionary<int, Collection<ProductVariationWrapper>>();
				foreach (IProduct product in Products)
				{
					var productVariationWrappers = new Collection<ProductVariationWrapper>();
					Collection<IProductVariation> productVariations =
						_productVariationService.GetAllByProductAndVariationGroup(product.Id, VariationGroupId);

					foreach (IProductVariation productVariation in productVariations)
					{
						IProductVariation productVariation1 = productVariation;
						VariationTypeWrapper variationTypeWrapper =
							VariationTypes.FirstOrDefault(vt => vt.VariationType.Title == productVariation1.VariationTypeName);
						int index =
							variationTypeWrapper.VariationType.Variations.IndexOf(
								variationTypeWrapper.VariationType.Variations.FirstOrDefault(v => v.Value == productVariation1.VariationValue));
						productVariationWrappers.Add(new ProductVariationWrapper
						                             	{
						                             		ProductVariation = productVariation,
						                             		VariationGuid = variationTypeWrapper.VariationGuids[index],
						                             		VariationTypeGuid = variationTypeWrapper.Guid
						                             	});
					}

					_productVariationsDictionary.Add(product.Id, productVariationWrappers);
				}
			}
			
			SortProductVariationsByProductTitle();
			GV.DataSource = Products;
			GV.DataBind();
		}

		// Sort Products by title.
		private void SortProductVariationsByProductTitle()
		{
			var productsSortedByTitle = new Collection<IProduct>();
			if (Products != null)
			{
				while (Products.Count > 0)
				{
					IProduct productFirst = null;
					foreach (IProduct product in Products)
					{
						if (product != null)
						{
							if (productFirst == null
								|| string.Compare(product.DisplayTitle, productFirst.DisplayTitle, StringComparison.OrdinalIgnoreCase) < 0
								|| (string.Compare(product.DisplayTitle, productFirst.DisplayTitle, StringComparison.OrdinalIgnoreCase) == 0
									&& string.Compare(product.ErpId, productFirst.ErpId, StringComparison.OrdinalIgnoreCase) < 0))
							{
								productFirst = product;
								continue;
							}
						}
					}

					if (productFirst != null)
					{
						productsSortedByTitle.Add(productFirst);
						Products.Remove(productFirst);
					}
				}

				Products.Clear();
				Products.AddRange(productsSortedByTitle);
			}
		}

		private void AddColumnsToMultipleActionsRegion(VariationTypeWrapper variationTypeWrapper)
		{
			if (variationTypeWrapper.VariationType.Variations != null)
			{
				var ddlVariationValues = new DropDownList
				{
					ID = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", DdlVariationValuesId,
										  SeparatorVariationValues,
										  variationTypeWrapper.Guid)
				};

				ddlVariationValues.Items.Add(new ListItem(string.Format(CultureInfo.CurrentCulture, "{0}...", variationTypeWrapper.VariationType.Title), string.Empty));

				foreach (IVariation variation in variationTypeWrapper.VariationType.Variations)
				{
					ddlVariationValues.Items.Add(new ListItem(variation.Value, variation.Value));
				}

				ddlVariationValues.Items.Add(new ListItem(ResetTag, ResetTag));

				cntVariationTypes.Controls.Add(ddlVariationValues);
				btnSet.Visible = true;
			}
		}

		private void AddVariationTypeColumnToGrid(VariationTypeWrapper variationTypeWrapper)
		{
			var column = new TemplateField
           		{
           			HeaderText = Encoder.EncodeHtml(variationTypeWrapper.VariationType.Title),
           			ItemTemplate =
           				new VariationTemplate(ListItemType.Item, variationTypeWrapper.VariationType.Variations, 
													variationTypeWrapper.VariationGuids, variationTypeWrapper.Guid),
           			Visible = true
           		};

			GV.Columns.Insert(GV.Columns.Count - 2, column);

			MakeColumnVisible(column.HeaderText);
		}

		/// <summary>
		/// Making column visible automatically, 
		/// without necessity to check appropriate checkbox in Grid settings.
		/// </summary>
		/// <param name="columnHeaderText"></param>
		private void MakeColumnVisible(string columnHeaderText)
		{
			if (gvVariants.VisibleColumnsName == null)
			{
				gvVariants.VisibleColumnsName = "Art. nbr,Title";
			}

			if (gvVariants.FixedColumns == null)
			{
				gvVariants.FixedColumns = string.Empty;
			}

			if (!gvVariants.VisibleColumnsNameAsList.Contains(columnHeaderText))
			{
				gvVariants.VisibleColumnsName =
					string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", gvVariants.VisibleColumnsName, Grid.GridViewVisibleColumnsSeparator,
								  columnHeaderText).Trim(Grid.GridViewVisibleColumnsSeparator);
			}

			if (!gvVariants.FixedColumnsAsList.Contains(columnHeaderText))
			{
				gvVariants.FixedColumns =
					string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", gvVariants.FixedColumns, Grid.FixedColumnsSeparator,
								  columnHeaderText).Trim(Grid.FixedColumnsSeparator);
			}
		}

        private void VariantsGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                var cbSelectAll = (CheckBox)e.Row.FindControl("cbSelectAll");
                cbSelectAll.Checked = false;
				cbSelectAll.Attributes.Add("onclick", "SelectAll1('" + cbSelectAll.ClientID + @"','" + GV.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelectAll.ID + "', '" +
														 GV.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
            }

			var product = e.Row.DataItem as IProduct;

			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var rbtnDefault = (RadioButton) e.Row.FindControl("rbtnDefault");
				if (product != null)
				{
					rbtnDefault.Attributes.Add("onClick", "SingleRadioBtn('" + rbtnDefault.ClientID + "','" + GV.ClientID + "','" + hfDefaultProduct.ClientID + "','" + product.Id + "');");
					if (product.Id == DefaultProductId)
					{
						rbtnDefault.Checked = true;
						HasDefaultVariant = true;
					}
				}
				if (Products.Count == 1)
				{
					var btnDelete = e.Row.FindControl("btnDelete") as ImageButton;
					if (btnDelete != null)
					{
						btnDelete.Enabled = false;
						btnDelete.ImageUrl = "~/Media/Images/Common/delete-disabled.gif";
					}
				}

				var selectAllCheckBox = (CheckBox)GV.HeaderRow.FindControl("cbSelectAll");
				var cbSelect = (CheckBox)e.Row.FindControl("cbIsSelected");
				cbSelect.Attributes.Add("onclick",
										"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
										cbSelect.ID + "', '" + GV.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
            
			if (product != null)
			{
				if (_productVariationsDictionary != null && _productVariationsDictionary.ContainsKey(product.Id))
				{
					foreach (ProductVariationWrapper productVariationWrapper in _productVariationsDictionary[product.Id])
					{
						SelectVariationValue(productVariationWrapper);
					}
				}
			}
        }

		private void SelectVariationValue(ProductVariationWrapper productVariationWrapper)
		{
			foreach (DataControlField column in GV.Columns)
			{
				var templatedColumn = column as TemplateField;
				if (templatedColumn != null)
				{
					var template = templatedColumn.ItemTemplate as VariationTemplate;
					if (template != null && template.VariationTypeGuid == productVariationWrapper.VariationTypeGuid)
					{
						template.SelectedProductVariationValue = productVariationWrapper.VariationGuid.ToString();
					}
				}
			}
		}

		private void VariantsGridDataBound(object sender, EventArgs e)
		{
			cntMultipleActions.Visible = GV.Rows.Count > 0;
		}

        #endregion

		#region Events

		private void DeleteProductVariations(int productId)
        {
			if (Products.Count > 1)
			{
				Products.Remove(Products.FirstOrDefault(prod => prod.Id == productId));
				if (productId == DefaultProductId)
				{
					DefaultProductId = Products[0].Id;
				}
			}
			else
			{
				errors.Add(Resources.ProductMessage.NotDeleteDefaultProduct);
			}
        	ViewState[VariationsKey] = ProductVariationWrappers;
            GV.DataBind();
			ProductVariationWrappers = ViewState[VariationsKey] as Collection<ProductVariationWrapper>;
        }

		private void VariantsGridDeleteCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "DeleteProductVariation")
            {
				int productId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
                DeleteProductVariations(productId);
				
            }
        }

		/*private void BtnCancel_Click(object sender, EventArgs e)
        {
            divMessages.Visible = false;
            productDataSource.SelectParameters.Clear();
        }*/

		private void BtnSet_OnClick(object sender, EventArgs e)
        {
            var setVariationValues = new Dictionary<string, int>();
			foreach (DropDownList ddl in cntVariationTypes.Controls)
			{
				if (ddl.ID == null || !ddl.ID.Contains(DdlVariationValuesId))
					continue;

				// Get variation type name.
				// The first element should always be ddlVariations.
				// And there should always be more than 1 element.
				string[] strarr = ddl.ID.Split(new[] {SeparatorVariationValues}, StringSplitOptions.None);
				if (strarr.Length > 1 && !string.IsNullOrEmpty(strarr[1]))
				{
					setVariationValues.Add(strarr[1], ddl.SelectedIndex);
				}

				ddl.SelectedIndex = 0;
			}

			foreach (GridViewRow row in GV.Rows)
			{
				// Get if the product is selected.
				CheckBox cbIsSelected = null;
				try
				{
					cbIsSelected = row.FindControl("cbIsSelected") as CheckBox;
				}
				catch (HttpException)
				{
				}

				if (cbIsSelected == null || !cbIsSelected.Checked)
					continue;

				foreach (KeyValuePair<string, int> keyValuePair in setVariationValues)
				{
					var ddlVariations =
						row.FindControl(string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}",
						                              VariationTemplate.DdlVariationsId,
						                              VariationTemplate.Separator,
						                              keyValuePair.Key)) as DropDownList;
					if (ddlVariations != null)
					{
						if (keyValuePair.Value != 0)
						{
							ddlVariations.SelectedIndex = keyValuePair.Value == ddlVariations.Items.Count
							                              	? 0
							                              	: keyValuePair.Value;
						}
					}
				}
			}
			upVariationProducts.Update();
        }

		private void BtnRemove_OnClick(object sender, EventArgs e)
        {
			foreach (GridViewRow row in GV.Rows)
			{
				var cbIsSelected = row.FindControl("cbIsSelected") as CheckBox;
				if (cbIsSelected != null && cbIsSelected.Checked)
				{
					// Get product id.
					HtmlInputHidden hdnProductId = null;
					try
					{
						hdnProductId = row.FindControl(HiddenProductId) as HtmlInputHidden;
					}
					catch (HttpException)
					{
					}

					if (hdnProductId != null)
					{
						int productId;
						if(!int.TryParse(hdnProductId.Value, out productId))
						{
							productId = 0;
						}
						DeleteProductVariations(productId);
					}
				}
			}
			upVariationProducts.Update();
        }

        #endregion

        #region ViewState

        protected override object SaveViewState()
        {
            ViewState[ProductsKey] = Products;
			ViewState[VariationsKey] = ProductVariationWrappers;
            return base.SaveViewState();
        }

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
			Products.Clear();
			Products.AddRange(ViewState[ProductsKey] as Collection<IProduct>);
        }

        #endregion

		/// <summary>
		/// Performs DataBind method for category tree in search criteria.
		/// </summary>
		public virtual void RestoreProductSearchCriteria()
		{
			productSearch.RestoreCategoryTree();
		}

    }

	[Serializable]
	public class ProductVariationWrapper
	{
		public IProductVariation ProductVariation { get; set; }
		public Guid VariationTypeGuid { get; set; }
		public Guid VariationGuid { get; set; }

		public ProductVariationWrapper()
		{
			VariationTypeGuid = Guid.NewGuid();
			VariationGuid = Guid.NewGuid();
		}
	}
}
