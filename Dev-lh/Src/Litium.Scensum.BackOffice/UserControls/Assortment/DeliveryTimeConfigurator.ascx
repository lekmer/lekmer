﻿<%@ Control Language="C#" CodeBehind="DeliveryTimeConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.DeliveryTimeConfigurator" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:ImageButton runat="server" ID="ManageButton" ImageUrl="~/Media/Images/SiteStructure/settings.png" AlternateText="Set delivery ranges" />
<ajaxToolkit:ModalPopupExtender ID="DeliveryTimePopup" runat="server" PopupControlID="DeliveryTimeDiv" BackgroundCssClass="popup-background" Y="100" TargetControlID="ManageButton" />
<div id="DeliveryTimeDiv" runat="server" class="translation-popup-container" style="z-index: 10010; display: none;">
	<div id="translation-popup-header">
		<div id="translation-popup-header-left">
		</div>
		<div id="translation-popup-header-center">
			<span>Set delivery ranges</span>
			<asp:Button ID="CloseDeliveryTimeButton" runat="server" Text="x" />
		</div>
		<div id="translation-popup-header-right">
		</div>
	</div>
	<div id="translation-popup-content">
		<asp:Label ID="ErorLabel" runat="server" style="display: none; color: red;" />
		<div id="SimpleDiv" runat="server">
			<div runat="server" id="DeliveryTimeListDiv">
				<asp:Repeater ID="SimpleDeliveryTimeRepeater" runat="server">
					<HeaderTemplate>
						<div id="value-translate" class="translation-item">
							<asp:Label ID="RegistryHeader" runat="server" Width="100px" />
							<asp:Label ID="FromHeader" runat="server" Text= "<%$ Resources:Lekmer,Literal_DeliveryTimeFrom %>" Width="134px" />
							<asp:Label ID="ToHeader" runat="server" Text= "<%$ Resources:Lekmer,Literal_DeliveryTimeTo %>" Width="134px" />
							<asp:Label ID="FormatHeader" runat="server" Text= "<%$ Resources:Lekmer,Literal_DeliveryTimeFormat %>" Width="130px" />
						</div>
					</HeaderTemplate>
					<ItemTemplate>
						<div id="value-translate" class="translation-item">
							<asp:Label ID="ProductRegistryLabel" runat="server" Text='<%# GetProductRegistryTitle(Eval("ProductRegistryid")) %>' Width="100px" />
							<asp:HiddenField ID="ProductRegistryHidden" runat="server" Value='<%# Eval("ProductRegistryid").ToString() %>' />
							<asp:TextBox ID="DeliveryTimeFromValue" runat="server" Text='<%# Eval("FromDays") %>' Width="130px" class="fromValue" />
							<asp:HiddenField ID="DeliveryTimeFromHidden" runat="server" Value='<%# Eval("FromDays") %>' />
							<asp:TextBox ID="DeliveryTimeToValue" runat="server" Text='<%# Eval("ToDays") %>' Width="130px" class="toValue" />
							<asp:HiddenField ID="DeliveryTimeToHidden" runat="server" Value='<%# Eval("ToDays") %>' />
							<asp:TextBox ID="DeliveryTimeFormatValue" runat="server" Text='<%# Eval("Format") %>' Width="130px" class="formatValue" MaxLength="25" />
							<asp:HiddenField ID="DeliveryTimeFormatHidden" runat="server" Value='<%# Eval("Format") %>' />
						</div>
					</ItemTemplate>
				</asp:Repeater>
			</div>
		</div>
		<div class="translation-popup-acton-button">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" UseSubmitBehaviour="true" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="Cancel" SkinID="DefaultButton" />
		</div>
	</div>
</div>