<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VariationTypeList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.VariationTypeList" %>
<%@ Register TagPrefix="uc"   TagName="VariationType" Src= "~/UserControls/Assortment/VariationTypeItem.ascx"%>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:UpdatePanel ID="upVariationCriteria" runat="server" UpdateMode="Always">
	<ContentTemplate>
		<div class="variation-type">
			<asp:Repeater ID="rptCriteria" runat="server">
				<ItemTemplate>						
						<uc:VariationType  runat="server" OnVariationTypeAction="VariationTypeAction" onVariationValueChanged="VariationValueChanged" OnVariationTitleChanged="VariationTitleChanged" ID="VariationType" />
				</ItemTemplate>
			</asp:Repeater>
			<div id="divSave" runat="server" class="mainDiv" style="display: none;">
				<div id="divHeader" class="headerDiv">
					<asp:Label ID="Label1" runat="server" Text="<%$ Resources:Product, Literal_RenameValue %>" />
					<asp:Button ID="btnCancelEditVariation" runat="server" Text="X" />
				</div>
				<div id="variation-type-item-subMainDiv">
					<div runat="server" id="divErrorSameName"  style="clear:both">	
					  <uc:MessageContainer ID="errorSameName"  MessageType="Failure"  runat="server" />	
					</div>	
					<div class="column">
						<div class="input-box">
							<span><%= Resources.Product.Literal_Value %>&nbsp; *</span>&nbsp;
							<asp:RequiredFieldValidator EnableClientScript="false" runat="server" ID="v1" ControlToValidate="tbVariationValue" 
							        ErrorMessage="<%$ Resources:ProductMessage, ValueEmpty %>"  Display ="None" ValidationGroup="vgVariationValue" />
							<br />
							<asp:TextBox ID="tbVariationValue" runat="server" />
						</div>
					</div>		<br style="clear:both" />												
					<div class="buttons right">
						<asp:HiddenField ID="hfItemId" runat="server" />
						<asp:HiddenField ID="hfOrdinal" runat="server" />
						<asp:HiddenField ID="hvVariationOrdinal" runat="server" />
						<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnSaveVariation" runat="server" ValidationGroup="vgVariationValue" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" />
					</div>									
				</div>
			</div>
				<asp:HiddenField ID="hf" runat="server" />
	<ajaxToolkit:ModalPopupExtender 
		ID="MPE" 		
		runat="server"
		TargetControlID="hf"
		PopupControlID="divSave"
		CancelControlID="btnCancelEditVariation"
		BackgroundCssClass="popup-background"/>
			<div style="padding-left:5px;">
			    <uc:MessageContainer ID="errorsVTList" MessageType="Failure" HideMessagesControlId="btnSave" runat="server" />
			</div>
						
		</div>
		<br class="clear" />
		<div id="variation-type-add" class="variation-item-list-footer">
				<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnVariationTypeAdd" class="variation-item-list-footer-button" runat="server" Text="<%$ Resources:General, Button_Add %>" ValidationGroup="vgVariationType" SkinID="DefaultButton" />
			</div>
	</ContentTemplate>
</asp:UpdatePanel>
