﻿<%@ Control Language="C#" CodeBehind="GenericWysiwygTranslator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Translation.GenericWysiwygTranslator" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<ajaxToolkit:ModalPopupExtender ID="TranslatePopup" runat="server" BackgroundCssClass="popup-background" Y="100"/>
<div id="WysiwygTranslateDiv" runat="server" class="translation-popup-container" style="z-index: 10010; display: none;">
	<div id="translation-popup-header">
		<div id="translation-popup-header-left">
		</div>
		<div id="translation-popup-header-center">
			<span>Translate</span>
			<asp:Button ID="CloseTranslationButton" runat="server" Text="x" />
		</div>
		<div id="translation-popup-header-right">
		</div>
	</div>
	<div id="translation-popup-content">
		<div id="WysiwygDiv" runat="server">
			<span>Default</span>
			<div class="translation-wysiwyg">
				<uc:LekmerTinyMceEditor ID="ValueTextBox" runat="server" SkinID="tinyMCE" Width="200px" />
				<asp:HiddenField ID="ValueWysiwygHiddenField" runat="server" />
			</div>
			<div runat="server" id="WysiwygTabsDiv">
				<ul>
					<asp:Repeater ID="WysiwygLanguageRepeater" runat="server">
						<ItemTemplate>
							<li>
								<a id="tabLnkWysiwyg" href='<%# string.Concat("#wysiwyg_", ID, Eval("Language.Id")) %>'>
									<asp:Label ID="lblLanguageTitleWysiwyg" runat="server" Text='<%# Eval("Language.Title") %>' />
									<asp:Image ID="imgExclamationWysiwyg" runat="server" ImageUrl="~/Media/Images/Interface/exclamation.gif" ImageAlign="AbsMiddle" style='<%# GetExclamationVisibility((string)Eval("Value")) %>' />
								</a>
							</li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<br clear="all" />
				<div runat="server" id="WysiwygTranslatinListDiv" class="translation-popup-tabs-container">
					<asp:Repeater ID="WysiwygTranslationRepeater" runat="server">
						<ItemTemplate>
							<div id='<%# string.Concat("wysiwyg_", ID, Eval("Language.Id")) %>'>
								<uc:LekmerTinyMceEditor ID="tmTranslateValueWysiwyg" runat="server" SkinID="tinyMCETranslate" Width="200px" />
								<asp:HiddenField ID="hfTranslateWysiwyg" runat="server" Value='<%# Eval("Value") %>' />
								<asp:HiddenField ID="hfLangIdWysiwyg" runat="server" Value='<%# Eval("Language.Id").ToString() %>' />
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
		</div>
		<br class="clear"/>
		<div class="translation-popup-acton-button">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="Cancel" SkinID="DefaultButton" />
		</div>
	</div>
</div>