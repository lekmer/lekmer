﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlockSetting.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.BlockSetting" %>

<div class="block-setting">
	<div class="column">
		<div class="input-box">
			<span><asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:General,Label_NumberOfColumns%>" />&nbsp;*</span>&nbsp;
			<asp:RangeValidator runat="server" ID="ColumnsNumberRangeValidator" ControlToValidate="NumberOfColumnsTextBox"
				Type="Integer" MinimumValue="1" ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>"
				Display="None"></asp:RangeValidator>
			<asp:RequiredFieldValidator runat="server" ID="ColumnsRequiredValidator" ControlToValidate="NumberOfColumnsTextBox"
				ErrorMessage="<%$ Resources:GeneralMessage, ColumnsNumberEmpty %>" Display="None" />
			<br />
			<asp:TextBox ID="NumberOfColumnsTextBox" runat="server" /><br />
		</div>
	</div>
	<div class="column">
		<div class="input-box">
			<span>
				<asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:General,Label_NumberOfRows%>" />&nbsp;*</span>&nbsp;
			<asp:RangeValidator runat="server" ID="RowsNumberRangeValidator" ControlToValidate="NumberOfRowsTextBox"
				Type="Integer" MinimumValue="1" ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberGreaterZero %>"
				Display="None"></asp:RangeValidator>
			<asp:RequiredFieldValidator runat="server" ID="RowsRequiredValidator" ControlToValidate="NumberOfRowsTextBox"
				ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberEmpty %>" Display="None" />
			<br />
			<asp:TextBox ID="NumberOfRowsTextBox" runat="server" /><br />
		</div>
	</div>
	<div class="column">
		<div class="input-box">
			<span>
				<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:TopList,Literal_TotalItemCount%>" />&nbsp;*</span>&nbsp;
			<asp:RangeValidator runat="server" ID="TotalProductsRangeValidator" ControlToValidate="NumberOfProductsTextBox"
				Type="Integer" MinimumValue="0" ErrorMessage="<%$ Resources:TopList, Message_TotalItemCountNotNegative %>"
				Display="None"></asp:RangeValidator>
			<asp:RequiredFieldValidator runat="server" ID="TotalProductsRequiredFieldValidator"
				ControlToValidate="NumberOfProductsTextBox" ErrorMessage="<%$ Resources:GeneralMessage, Message_TotalItemCountNotNegative %>"
				Display="None" />
			<br />
			<asp:TextBox ID="NumberOfProductsTextBox" runat="server" /><br />
		</div>
	</div>

	<br class="clear" />

	<div class="column">
		<div class="input-box">
			<span><%= Resources.General.Label_NumberOfColumnsMobile%></span>&nbsp;
			<asp:RangeValidator runat="server" ID="ColumnsNumberMobileRangeValidator" ControlToValidate="NumberOfColumnsMobileTextBox"
				Type="Integer" MinimumValue="1" ErrorMessage="<%$ Resources:GeneralMessage,ColumnsMobileNumberGreaterZero %>" 
				Display="None" />
			<br />
			<asp:TextBox ID="NumberOfColumnsMobileTextBox" runat="server" /><br />
		</div>
	</div>
	<div class="column">
		<div class="input-box">
			<span><%= Resources.General.Label_NumberOfRowsMobile%></span>&nbsp;
			<asp:RangeValidator runat="server" ID="RowsNumberMobileRangeValidator" ControlToValidate="NumberOfRowsMobileTextBox"
				Type="Integer" MinimumValue="1" ErrorMessage="<%$ Resources:GeneralMessage, RowsMobileNumberGreaterZero %>"
				Display="None" />
			<br />
			<asp:TextBox ID="NumberOfRowsMobileTextBox" runat="server" /><br />
		</div>
	</div>
	<div class="column">
		<div class="input-box">
			<span><%= Resources.General.Label_TotalNumberOfItemsMobile%></span>&nbsp;
			<asp:RangeValidator runat="server" ID="TotalProductsMobileRangeValidator" ControlToValidate="NumberOfProductsMobileTextBox"
				Type="Integer" MinimumValue="0" ErrorMessage="<%$ Resources:GeneralMessage, Message_TotalItemCountMobileNotNegative %>"
				Display="None" />
			<br />
			<asp:TextBox ID="NumberOfProductsMobileTextBox" runat="server" /><br />
		</div>
	</div>
</div>