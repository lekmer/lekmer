﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Group.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.Group" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="obout" Namespace="OboutInc.ColorPicker" Assembly="obout_ColorPicker" %>
<%@ Register TagPrefix="uc" TagName="CollapsiblePanel" Src="~/UserControls/Common/CollapsiblePanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="SiteStructureNodeSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>

<link href="<%=ResolveUrl("~/Media/Css/dropzone.css") %>" rel="stylesheet" type="text/css" />
<script src="<%=ResolveUrl("~/Media/Scripts/dropzone.js") %>" type="text/javascript"></script>
<script src="<%=ResolveUrl("~/Media/Scripts/draggable.js") %>" type="text/javascript"></script>

<script type="text/javascript">
	function colorChanged(sender) {
		var control = sender.TargetClient;
		control.style.backgroundColor = control.value;
	}
</script>

<script type="text/javascript">
	$(document).ready(function () {
		$(".dropzone").dropzone({
			url: '<%= ResolveUrl("~/UserControls/Common/MediaHandlers/GroupAddMediaHandler.ashx") %>',
			addRemoveLinks: true,
			maxFiles: 1,
			uploadMultiple: false,
			acceptedFiles: 'image/*',
			sending: function (file, xhr, formData) {
				var dropZone = this;
				formData.append("blockId", '<%= BlockId %>');
				formData.append("userName", '<%= UserName %>');
				var className = dropZone.previewsContainer.parentElement.className;
				formData.append("imageType", className);
				var groupIdHiddenField = dropZone.previewsContainer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.previousSibling.previousSibling;
				var groupId = groupIdHiddenField.value;
				formData.append("groupId", groupId);
			},
			success: function (response) {
				if (response != null && response.status == 'success') {
					document.getElementById('<%= RefreshButton.ClientID %>').click();
			}
			},
			init: function () {
				this.on('removedfile', function (file) {
					var dropZone = this;
					var groupIdHiddenField = dropZone.previewsContainer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.previousSibling.previousSibling;
					var groupId = groupIdHiddenField.value;
					var blockId = '<%= BlockId %>';
					var userName = '<%= UserName %>';
					var imageType = dropZone.previewsContainer.parentElement.className;
					$.ajax({
						type: "POST",
						url: '<%= ResolveUrl("~/UserControls/Common/MediaHandlers/GroupDeleteMediaHandler.ashx") %>',
						data: { groupId: groupId, blockId: blockId, imageType: imageType, userName: userName },
						dataType: "json",
						success: function (response) {
							if (response != null && response == 'success') {
								document.getElementById('<%= RefreshButton.ClientID %>').click();
							}
						}
					});
				});
			}
		});

		loadImages();
	});

	function loadImages() {
		var blockId = '<%= BlockId %>';
		$.ajax({
			type: "POST",
			url: '<%= ResolveUrl("~/UserControls/Common/MediaHandlers/GroupLoadMediaHandler.ashx") %>',
			data: { blockId: blockId },
			dataType: "json",
			success: function (response) {
				if (response != null) {
					var files = response;
					for (var i = 0; i < files.length; i++) {
						if (files[i].Url != "") {
							Dropzone.instances.forEach(function (dropZone, e) {
								var imageType = dropZone.previewsContainer.parentElement.className;
								var groupIdHiddenField = dropZone.previewsContainer.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.previousSibling.previousSibling;
								var groupId = groupIdHiddenField.value;
								if ((groupId == files[i].GroupId) && (files[i].Type == imageType)) {
									var mockFile = { imageType: files[i].Type, groupId: files[i].GroupId, url: files[i].Url };
									dropZone.emit("addedfile", mockFile);
									dropZone.emit("thumbnail", mockFile, files[i].Url);
								}
							});
						}
					}
				}
			}
		});
	}
</script>

<asp:Repeater ID="GroupRepeater" runat="server">
	<ItemTemplate>
		<uc:CollapsiblePanel
			ID="CollapsiblePanel"
			runat="server"
			HasAction="True"
			CommandArgument='<%# Eval("SliderGroupId") %>'
			ActionButtonCaption="Remove Group"
			ActionButtonCommandName="RemoveGroup"
			ActionButtonCommandArgument='<%# Eval("SliderGroupId") %>'
			Title='<%#"Group " + (((int) DataBinder.Eval(Container, "ItemIndex")) + 1) %>' />

		<asp:HiddenField ID="SliderGroupIdHiddenField" runat="server" Value='<%# Eval("SliderGroupId") %>' />
		<div id="SlideFieldsDiv" runat="server" class="area-block-items left" style="table-layout: fixed;">
			<table style="border-collapse: collapse; width: 100%;">
				<tr style="color: white; background-color: #999999;">
					<th class="grid-header">Field</th>
					<th class="grid-header">Properties</th>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Main img</td>
					<td style="width: 80%;" class="grid-row">
						<div class="main1" style="width: 100%;">
							<div class="dropzone"></div>
						</div>
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Thumb img</td>
					<td style="width: 80%;" class="grid-row">
						<div class="thumbnail1" style="width: 100%;">
							<div class="dropzone"></div>
						</div>
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Link</td>
					<td style="width: 80%;" class="grid-row">
						<uc:SiteStructureNodeSelector ID="InternalLinkContentNode1" runat="server" NotShowDefaultText="true" AllowClearSelection="true" />
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">External link</td>
					<td style="width: 80%;" class="grid-row">
						<asp:TextBox ID="ExternalLink1TB" runat="server" MaxLength="1000" Width="350px" />
						<asp:CheckBox ID="IsMovie1CB" runat="server" style="padding-left: 25px;" />
						<span>Is movie</span>
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Secondary main img</td>
					<td style="width: 80%;" class="grid-row">
						<div class="main2" style="width: 100%;">
							<div class="dropzone"></div>
						</div>
					</td>
				</tr>
				
				<tr>
					<td style="width: 20%;" class="grid-row">Secondary thumb img</td>
					<td style="width: 80%;" class="grid-row">
						<div class="thumbnail2" style="width: 100%;">
							<div class="dropzone"></div>
						</div>
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Secondary Link</td>
					<td style="width: 80%;" class="grid-row">
						<uc:SiteStructureNodeSelector ID="InternalLinkContentNode2" runat="server" NotShowDefaultText="true" AllowClearSelection="true" />
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Secondary external link</td>
					<td style="width: 80%;" class="grid-row">
						<asp:TextBox ID="ExternalLink2TB" runat="server" MaxLength="1000" Width="350px" />
						<asp:CheckBox ID="IsMovie2CB" runat="server" style="padding-left: 25px;" />
						<span>Is movie</span>
					</td>
				</tr>

				<tr>
					<td style="width: 100%;" colspan="2">
						<table style="border-collapse: collapse; width: 100%;">
							<tr style="color: white; background-color: #999999;">
								<th class="grid-header">Field</th>
								<th class="grid-header">Value</th>
								<th class="grid-header">Link</th>
								<th class="grid-header">Size</th>
								<th class="grid-header">Color</th>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Title 1</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="Title1TB" runat="server" MaxLength="100" Width="140px" />
								</td>
								<td style="width: 32%;" class="grid-row">
									<asp:TextBox ID="TitleLink1TB" runat="server" MaxLength="1000" Width="184px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="TitleSize1TB" runat="server" Width="50px" />
									<asp:CustomValidator ID="TitleSize1Validator" runat="server" ControlToValidate="TitleSize1TB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="TitleColor1TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="TitleColor1Picker" TargetId="TitleColor1TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Title 2</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="Title2TB" runat="server" MaxLength="100" Width="140px" />
								</td>
								<td style="width: 32%;" class="grid-row">
									<asp:TextBox ID="TitleLink2TB" runat="server" MaxLength="1000" Width="184px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="TitleSize2TB" runat="server" Width="50px" />
									<asp:CustomValidator ID="TitleSize2Validator" runat="server" ControlToValidate="TitleSize2TB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="TitleColor2TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="TitleColor2Picker" TargetId="TitleColor2TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Title 3</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="Title3TB" runat="server" MaxLength="100" Width="140px" />
								</td>
								<td style="width: 32%;" class="grid-row">
									<asp:TextBox ID="TitleLink3TB" runat="server" MaxLength="1000" Width="184px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="TitleSize3TB" runat="server" Width="50px" />
									<asp:CustomValidator ID="TitleSize3Validator" runat="server" ControlToValidate="TitleSize3TB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="TitleColor3TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="TitleColor3Picker" TargetId="TitleColor3TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Description</td>
								<td style="width: 57%;" class="grid-row" colspan="2">
									<asp:TextBox ID="DescriptionTB" runat="server" Width="340px" TextMode="MultiLine" Rows="3" style="resize: none;" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="DescriptionSizeTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="DescriptionSizeValidator" runat="server" ControlToValidate="DescriptionSizeTB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="DescriptionColorTB" runat="server" Width="60px" />
									<obout:ColorPicker ID="DescriptionColorPicker" TargetId="DescriptionColorTB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Disclaimer</td>
								<td style="width: 57%;" class="grid-row" colspan="2">
									<asp:TextBox ID="DisclaimerTB" runat="server" Width="340px" TextMode="MultiLine" Rows="3" style="resize: none;" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="DisclaimerSizeTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="DisclaimerSizeValidator" runat="server" ControlToValidate="DisclaimerSizeTB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="DisclaimerColorTB" runat="server" Width="60px" />
									<obout:ColorPicker ID="DisclaimerColorPicker" TargetId="DisclaimerColorTB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Button 1</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="Button1TB" runat="server" MaxLength="100" Width="140px" />
								</td>
								<td style="width: 32%;" class="grid-row">
									<asp:TextBox ID="ButtonLink1TB" runat="server" MaxLength="1000" Width="184px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="ButtonSize1TB" runat="server" Width="50px" />
									<asp:CustomValidator ID="ButtonSize1Validator" runat="server" ControlToValidate="ButtonSize1TB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="ButtonColor1TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="ButtonColor1Picker" TargetId="ButtonColor1TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Button 2</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="Button2TB" runat="server" MaxLength="100" Width="140px" />
								</td>
								<td style="width: 32%;" class="grid-row">
									<asp:TextBox ID="ButtonLink2TB" runat="server" MaxLength="1000" Width="184px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="ButtonSize2TB" runat="server" Width="50px" />
									<asp:CustomValidator ID="ButtonSize2Validator" runat="server" ControlToValidate="ButtonSize2TB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="ButtonColor2TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="ButtonColor2Picker" TargetId="ButtonColor2TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Button 3</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="Button3TB" runat="server" MaxLength="100" Width="140px" />
								</td>
								<td style="width: 32%;" class="grid-row">
									<asp:TextBox ID="ButtonLink3TB" runat="server" MaxLength="1000" Width="184px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="ButtonSize3TB" runat="server" Width="50px" />
									<asp:CustomValidator ID="ButtonSize3Validator" runat="server" ControlToValidate="ButtonSize3TB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="ButtonColor3TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="ButtonColor3Picker" TargetId="ButtonColor3TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="width: 100%;" colspan="2">
						<table style="border-collapse: collapse; width: 100%;">
							<tr style="color: white; background-color: #999999;">
								<th class="grid-header">Field</th>
								<th class="grid-header">Value</th>
								<th class="grid-header">Pos</th>
								<th class="grid-header">Size</th>
								<th class="grid-header">Color</th>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Discount</td>
								<td style="width: 44%;" class="grid-row">
									<asp:TextBox ID="DiscountTB" runat="server" MaxLength="100" Width="260px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="DiscountPosTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="DiscountPosValidator" runat="server" ControlToValidate="DiscountPosTB" Text=" *" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="DiscountSizeTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="DiscountSizeValidator" runat="server" ControlToValidate="DiscountSizeTB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="DiscountColorTB" runat="server" Width="60px" />
									<obout:ColorPicker ID="DiscountColorPicker" TargetId="DiscountColorTB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Extra</td>
								<td style="width: 44%;" class="grid-row">
									<asp:TextBox ID="ExtraTB" runat="server" MaxLength="100" Width="260px" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="ExtraPosTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="ExtraPosValidator" runat="server" ControlToValidate="ExtraPosTB" Text=" *" />
								</td>
								<td style="width: 12%;" class="grid-row">
									<asp:TextBox ID="ExtraSizeTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="ExtraSizeValidator" runat="server" ControlToValidate="ExtraSizeTB" Text=" *" />
								</td>
								<td style="width: 16%;" class="grid-row">
									<asp:TextBox ID="ExtraColorTB" runat="server" Width="60px" />
									<obout:ColorPicker ID="ExtraColorPicker" TargetId="ExtraColorTB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="width: 100%;" colspan="2">
						<table style="border-collapse: collapse; width: 100%;">
							<tr style="color: white; background-color: #999999;">
								<th class="grid-header">Field</th>
								<th class="grid-header">Color-1</th>
								<th class="grid-header">Color-2</th>
								<th class="grid-header">Gradient</th>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Background</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="BackgroundColor1TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="BackgroundColor1Picker" TargetId="BackgroundColor1TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
								<td style="width: 25%;" class="grid-row">
									<asp:TextBox ID="BackgroundColor2TB" runat="server" Width="60px" />
									<obout:ColorPicker ID="BackgroundColor2Picker" TargetId="BackgroundColor2TB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
								<td style="width: 35%;" class="grid-row">
									<asp:DropDownList ID="BackgroundGradientList" DataTextField="Value" DataValueField="Key" Width="150px" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="width: 100%;" colspan="2">
						<table style="border-collapse: collapse; width: 100%;">
							<tr style="color: white; background-color: #999999;">
								<th class="grid-header">Field</th>
								<th class="grid-header">Color</th>
								<th class="grid-header">Alpha</th>
								<th class="grid-header">Shape</th>
								<th class="grid-header">Text-align</th>
							</tr>
							<tr>
								<td style="width: 15%;" class="grid-row">Holder</td>
								<td style="width: 20%;" class="grid-row">
									<asp:TextBox ID="HolderColorTB" runat="server" Width="60px" />
									<obout:ColorPicker ID="HolderColorPicker" TargetId="HolderColorTB" OnClientPicked="colorChanged" PickerCallerImage="~/Media/Images/SiteStructure/color-picker.png" runat="server" />
								</td>
								<td style="width: 15%;" class="grid-row">
									<asp:TextBox ID="HolderAlphaTB" runat="server" Width="50px" />
									<asp:CustomValidator ID="HolderAlphaValidator" runat="server" ControlToValidate="HolderAlphaTB" Text=" *" />
								</td>
								<td style="width: 25%;" class="grid-row">
									<asp:DropDownList ID="HolderShapeList" DataTextField="Value" DataValueField="Key" Width="100px" runat="server" />
								</td>
								<td style="width: 25%;" class="grid-row">
									<asp:DropDownList ID="HolderTextAlignList" DataTextField="Value" DataValueField="Key" Width="100px" runat="server" />
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Style</td>
					<td style="width: 80%;" class="grid-row">
						<asp:TextBox ID="StyleTB" Width="350px" runat="server" />
						<asp:CustomValidator ID="StyleValidator" runat="server" ControlToValidate="StyleTB" Text=" *" />
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Ordinal</td>
					<td style="width: 80%;" class="grid-row">
						<asp:TextBox ID="OrdinalTB" Width="350px" runat="server" />
						<asp:CustomValidator ID="OrdinalValidator" runat="server" ControlToValidate="OrdinalTB" Text=" *" />
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Captions</td>
					<td style="width: 80%;" class="grid-row">
						<uc:LekmerTinyMceEditor ID="CaptionsEditor" runat="server" SkinID="tinyMCE" Width="100%" />
					</td>
				</tr>
				
				<tr>
					<td style="width: 20%;" class="grid-row">Campaign</td>
					<td style="width: 80%;" class="grid-row">
						Campaign
					</td>
				</tr>

				<tr>
					<td style="width: 20%;" class="grid-row">Timer</td>
					<td style="width: 80%;" class="grid-row">
						<div class="column">
							<span>Is active</span>
							<br />
							<asp:CheckBox ID="IsActiveCB" runat="server" />
						</div>
						<div class="column" style="padding-left: 20px;">
							<span>Use campaign start/end</span>
							<br />
							<asp:CheckBox ID="UseCampaignDatesCB" runat="server" />
						</div>
						<div class="column" style="padding-left: 0px;">
							<span><%= Resources.General.Literal_StartDate %></span>
							<asp:CustomValidator ID="StartDateValidator" runat="server" ControlToValidate="StartDateTB" Display="Static" Text=" *" />
							<br />
							<asp:TextBox ID="StartDateTB" runat="server" Width="100px" />
							<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Assortment/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
							<ajaxToolkit:CalendarExtender ID="StartDateCalendarExtender" runat="server" TargetControlID="StartDateTB" PopupButtonID="StartDateButton" />
						</div>
						<div class="column" style="padding-left: 10px;">
							<span><%= Resources.General.Literal_EndDate %></span>
							<asp:CustomValidator ID="EndDateValidator" runat="server" ControlToValidate="EndDateTB" Display="Static" Text=" *" />
							<asp:CustomValidator ID="StartEndDateLimitValidator" runat="server" ControlToValidate="EndDateTB" Display="Static" Text=" *" />
							<br />
							<asp:TextBox ID="EndDateTB" runat="server" Width="100px" />
							<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Assortment/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
							<ajaxToolkit:CalendarExtender ID="EndDateCalendarExtender" runat="server" TargetControlID="EndDateTB" PopupButtonID="EndDateButton" />
						</div>
					</td>
				</tr>
			</table>
		</div>
	</ItemTemplate>
</asp:Repeater>

<br clear="all" />
<br />

<div class="right">
	<asp:Button ID="AddGroupButton" runat="server" Text="Add Group" />
</div>


<asp:Button runat="server" ID="RefreshButton" Text="" style="display:none;" OnClick="Refresh" />
<%--<asp:HiddenField ID="DropzoneGroupIdHF" runat="server"/>
<asp:HiddenField ID="DropzoneFileIdHF" runat="server"/>
<asp:HiddenField ID="DropzoneImageTypeHF" runat="server"/>
<asp:HiddenField ID="DropzoneActionHF" runat="server"/>--%>