﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Common;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Resources;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class Group : StateUserControlController<SliderGroupState>
	{
		private const int ImageRotatorGroupMaxQuantity = 100;
		private const string GroupName = "Group";

		private readonly DateTime _minDate = Constants.MinDate;
		private readonly DateTime _maxDate = Constants.MaxDate;

		private ISliderGroupSecureService _sliderGroupService;
		private IContentNodeSecureService _contentNodeService;

		private static string ShortDateTimePattern
		{
			get { return string.Format("{0} {1}", CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern); }
		}
		private Collection<ISliderGroup> SliderGroups
		{
			get { return State.SliderGroups; }
			set
			{
				EnsureState();
				State.SliderGroups = value;
			}
		}
		public int BlockId
		{
			get { return State.BlockId; }
			set
			{
				EnsureState();
				State.BlockId = value;
			}
		}
		public string UserName
		{
			get { return SignInHelper.SignedInSystemUser.UserName; }
		}
		public ISliderGroupSecureService SliderGroupService
		{
			get { return _sliderGroupService ?? (_sliderGroupService = IoC.Resolve<ISliderGroupSecureService>()); }
		}
		public IContentNodeSecureService ContentNodeService
		{
			get { return _contentNodeService ?? (_contentNodeService = IoC.Resolve<IContentNodeSecureService>()); }
		}


		protected override void SetEventHandlers()
		{
			AddGroupButton.Click += AddGroup;

			GroupRepeater.ItemDataBound += GroupItemDataBound;
			GroupRepeater.ItemCommand += GroupItemCommand;
		}

		protected override void PopulateControl()
		{
			SliderGroups = SliderGroupService.GetByBlockId(BlockId);
			BindSliderGroups();
		}

		protected void AddGroup(object sender, EventArgs e)
		{
			SliderGroups = GetSliderGroups();
			ISliderGroup sliderGroup = SliderGroupService.Create(BlockId, SliderGroups.Count - ImageRotatorGroupMaxQuantity);
			//SliderGroupService.Save(SignInHelper.SignedInSystemUser, sliderGroup);
			SliderGroups.Add(sliderGroup);

			BindSliderGroups();
		}

		protected void GroupItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var item = e.Item;
			var sliderGroup = (ISliderGroup)item.DataItem;

			// Bind group collapsible panel
			var collapsiblePanel = (CollapsiblePanel) item.FindControl("CollapsiblePanel");
			collapsiblePanel.IsOpened = sliderGroup.IsOpened.ToString();
			collapsiblePanel.ControlToExpand = item.FindControl("SlideFieldsDiv").ClientID;
			collapsiblePanel.ActionButtonClientClick = "return DeleteConfirmation(\"" + "group" + "\");";

			// Bind group properties
			PopulateInternalLinkContentNode("InternalLinkContentNode1", sliderGroup.InternalLinkContentNodeId1, item);
			PopulateInternalLinkContentNode("InternalLinkContentNode2", sliderGroup.InternalLinkContentNodeId2, item);
			PopulateTextBox("ExternalLink1TB", sliderGroup.ExternalLink1, item);
			PopulateTextBox("ExternalLink2TB", sliderGroup.ExternalLink2, item);
			PopulateCheckBox("IsMovie1CB", sliderGroup.IsExternalLinkMovie1, item);
			PopulateCheckBox("IsMovie2CB", sliderGroup.IsExternalLinkMovie2, item);
			PopulateTextBox("Title1TB", sliderGroup.Title1, item);
			PopulateTextBox("TitleSize1TB", sliderGroup.Title1Size, item);
			PopulateTextBox("TitleLink1TB", sliderGroup.Title1Link, item);
			PopulateColorTextBox("TitleColor1TB", sliderGroup.Title1Color, item);
			PopulateTextBox("Title2TB", sliderGroup.Title2, item);
			PopulateTextBox("TitleSize2TB", sliderGroup.Title2Size, item);
			PopulateTextBox("TitleLink2TB", sliderGroup.Title2Link, item);
			PopulateColorTextBox("TitleColor2TB", sliderGroup.Title2Color, item);
			PopulateTextBox("Title3TB", sliderGroup.Title3, item);
			PopulateTextBox("TitleSize3TB", sliderGroup.Title3Size, item);
			PopulateTextBox("TitleLink3TB", sliderGroup.Title3Link, item);
			PopulateColorTextBox("TitleColor3TB", sliderGroup.Title3Color, item);
			PopulateTextBox("DescriptionTB", sliderGroup.Description, item);
			PopulateTextBox("DescriptionSizeTB", sliderGroup.DescriptionSize, item);
			PopulateColorTextBox("DescriptionColorTB", sliderGroup.DescriptionColor, item);
			PopulateTextBox("DisclaimerTB", sliderGroup.Disclaimer, item);
			PopulateTextBox("DisclaimerSizeTB", sliderGroup.DisclaimerSize, item);
			PopulateColorTextBox("DisclaimerColorTB", sliderGroup.DisclaimerColor, item);
			PopulateTextBox("Button1TB", sliderGroup.Button1Text, item);
			PopulateTextBox("ButtonSize1TB", sliderGroup.Button1Size, item);
			PopulateTextBox("ButtonLink1TB", sliderGroup.Button1Link, item);
			PopulateColorTextBox("ButtonColor1TB", sliderGroup.Button1Color, item);
			PopulateTextBox("Button2TB", sliderGroup.Button2Text, item);
			PopulateTextBox("ButtonSize2TB", sliderGroup.Button2Size, item);
			PopulateTextBox("ButtonLink2TB", sliderGroup.Button2Link, item);
			PopulateColorTextBox("ButtonColor2TB", sliderGroup.Button2Color, item);
			PopulateTextBox("Button3TB", sliderGroup.Button3Text, item);
			PopulateTextBox("ButtonSize3TB", sliderGroup.Button3Size, item);
			PopulateTextBox("ButtonLink3TB", sliderGroup.Button3Link, item);
			PopulateColorTextBox("ButtonColor3TB", sliderGroup.Button3Color, item);
			PopulateTextBox("DiscountTB", sliderGroup.DiscountText, item);
			PopulateTextBox("DiscountSizeTB", sliderGroup.DiscountSize, item);
			PopulateTextBox("DiscountPosTB", sliderGroup.DiscountPosition, item);
			PopulateColorTextBox("DiscountColorTB", sliderGroup.DiscountColor, item);
			PopulateTextBox("ExtraTB", sliderGroup.ExtraText, item);
			PopulateTextBox("ExtraSizeTB", sliderGroup.ExtraSize, item);
			PopulateTextBox("ExtraPosTB", sliderGroup.ExtraPosition, item);
			PopulateColorTextBox("ExtraColorTB", sliderGroup.ExtraColor, item);
			PopulateColorTextBox("BackgroundColor1TB", sliderGroup.BackgroundColor1, item);
			PopulateColorTextBox("BackgroundColor2TB", sliderGroup.BackgroundColor2, item);
			var gradientDataSource = Enum.GetValues(typeof (GradientEnum)).Cast<GradientEnum>().ToDictionary(type => (int) type, type => type.GetEnumDescription());
			PopulateDropDownList("BackgroundGradientList", gradientDataSource, sliderGroup.BackgroundGradient, true, item);
			PopulateColorTextBox("HolderColorTB", sliderGroup.HolderColor, item);
			var shapeDataSource = Enum.GetValues(typeof(ShapeEnum)).Cast<ShapeEnum>().ToDictionary(type => (int)type, type => type.GetEnumDescription());
			PopulateDropDownList("HolderShapeList", shapeDataSource, sliderGroup.HolderShape, true, item);
			var textAlignDataSource = Enum.GetValues(typeof(TextAlignEnum)).Cast<TextAlignEnum>().ToDictionary(type => (int)type, type => type.GetEnumDescription());
			PopulateDropDownList("HolderTextAlignList", textAlignDataSource, sliderGroup.HolderTextAlign, false, item);
			PopulateTextBox("HolderAlphaTB", sliderGroup.HolderAlpha, item);
			PopulateTextBox("StyleTB", sliderGroup.Style, item);
			PopulateTextBox("OrdinalTB", sliderGroup.Ordinal, item);
			PopulateCheckBox("IsActiveCB", sliderGroup.IsActive, item);
			PopulateCheckBox("UseCampaignDatesCB", sliderGroup.UseCampaignDates, item);
			PopulateDate("Start", sliderGroup.StartDateTime, item);
			PopulateDate("End", sliderGroup.EndDateTime, item);
			var captions = (WebControls.LekmerTinyMceEditor) item.FindControl("CaptionsEditor");
			captions.SetValue(sliderGroup.Captions ?? string.Empty);
		}

		protected void GroupItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "RemoveGroup")
			{
				int groupId;
				int.TryParse(e.CommandArgument.ToString(), out groupId);

				SliderGroups = GetSliderGroups();
				SliderGroups.Remove(SliderGroups.FirstOrDefault(item => item.SliderGroupId == groupId));
				BindSliderGroups();
			}
		}

		private void EnsureState()
		{
			if (State == null)
			{
				State = new SliderGroupState();
			}
		}

		private void BindSliderGroups()
		{
			GroupRepeater.DataSource = SliderGroups;
			GroupRepeater.DataBind();

			AddGroupButton.Visible = SliderGroups.Count < ImageRotatorGroupMaxQuantity;
		}

		private void PopulateInternalLinkContentNode(string controlId, int? selectedId, RepeaterItem item)
		{
			var nodeId = selectedId ?? Request.QueryString.GetInt32OrNull("id");
			IContentNode node = ContentNodeService.GetById(nodeId.Value);

			var internalLinkContentNode = (LekmerContentPageSelector) item.FindControl(controlId);
			internalLinkContentNode.SiteStructureRegistryId = node.SiteStructureRegistryId;
			internalLinkContentNode.SelectedNodeId = selectedId;
			internalLinkContentNode.SetContentPageTitle();
		}
		private void PopulateTextBox(string controlId, string value, RepeaterItem item)
		{
			var control = (TextBox) item.FindControl(controlId);
			control.Text = !string.IsNullOrEmpty(value) ? value : string.Empty;
		}
		private void PopulateTextBox(string controlId, int? value, RepeaterItem item)
		{
			var control = (TextBox)item.FindControl(controlId);
			control.Text = value.HasValue ? value.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;
		}
		private void PopulateColorTextBox(string controlId, string value, RepeaterItem item)
		{
			var control = (TextBox)item.FindControl(controlId);
			if (!string.IsNullOrEmpty(value))
			{
				control.Text = value;
				control.BackColor = Color.FromName(value);
			}
			else
			{
				control.Text = string.Empty;
			}
		}
		private void PopulateCheckBox(string controlId, bool value, RepeaterItem item)
		{
			var control = (CheckBox) item.FindControl(controlId);
			control.Checked = value;
		}
		private void PopulateDate(string controlId, DateTime? date, RepeaterItem item)
		{
			var dateTextBox = (TextBox)item.FindControl(controlId + "DateTB");
			dateTextBox.Text = date.HasValue ? date.Value.ToString(ShortDateTimePattern, CultureInfo.CurrentCulture) : string.Empty;
		}
		private void PopulateDropDownList(string controlId, Dictionary<int, string> dataSource, int? selectedValue, bool isNeedEmptyItem, RepeaterItem item)
		{
			var dropDownList = (DropDownList) item.FindControl(controlId);
			dropDownList.DataSource = dataSource;
			dropDownList.DataBind();

			if (isNeedEmptyItem)
			{
				var defaultValue = new ListItem(string.Empty, string.Empty);
				dropDownList.Items.Insert(0, defaultValue);
			}

			if (selectedValue.HasValue)
			{
				var value = dropDownList.Items.FindByValue(selectedValue.Value.ToString(CultureInfo.InvariantCulture));
				if (value != null)
				{
					value.Selected = true;
				}
			}
		}

		public Collection<ISliderGroup> GetSliderGroups()
		{
			foreach (RepeaterItem repeaterItem in GroupRepeater.Items)
			{
				var sliderGroupIdHiddenField = (HiddenField) repeaterItem.FindControl("SliderGroupIdHiddenField");
				int groupId = int.Parse(sliderGroupIdHiddenField.Value, CultureInfo.CurrentCulture);

				ISliderGroup sliderGroup = SliderGroups.FirstOrDefault(item => item.SliderGroupId == groupId);
				if (sliderGroup == null) continue;

				sliderGroup.InternalLinkContentNodeId1 = GetContentNodeId("InternalLinkContentNode1", repeaterItem);
				sliderGroup.ExternalLink1 = GetTextBoxValue("ExternalLink1TB", repeaterItem);
				sliderGroup.IsExternalLinkMovie1 = GetCheckBoxValue("IsMovie1CB", repeaterItem);
				sliderGroup.InternalLinkContentNodeId2 = GetContentNodeId("InternalLinkContentNode2", repeaterItem);
				sliderGroup.ExternalLink2 = GetTextBoxValue("ExternalLink2TB", repeaterItem);
				sliderGroup.IsExternalLinkMovie2 = GetCheckBoxValue("IsMovie2CB", repeaterItem);
				sliderGroup.Title1 = GetTextBoxValue("Title1TB", repeaterItem);
				sliderGroup.Title1Size = GetTextBoxIntValue("TitleSize1TB", repeaterItem);
				sliderGroup.Title1Link = GetTextBoxValue("TitleLink1TB", repeaterItem);
				sliderGroup.Title1Color = GetTextBoxValue("TitleColor1TB", repeaterItem);
				sliderGroup.Title2 = GetTextBoxValue("Title2TB", repeaterItem);
				sliderGroup.Title2Size = GetTextBoxIntValue("TitleSize2TB", repeaterItem);
				sliderGroup.Title2Link = GetTextBoxValue("TitleLink2TB", repeaterItem);
				sliderGroup.Title2Color = GetTextBoxValue("TitleColor2TB", repeaterItem);
				sliderGroup.Title3 = GetTextBoxValue("Title3TB", repeaterItem);
				sliderGroup.Title3Size = GetTextBoxIntValue("TitleSize3TB", repeaterItem);
				sliderGroup.Title3Link = GetTextBoxValue("TitleLink3TB", repeaterItem);
				sliderGroup.Title3Color = GetTextBoxValue("TitleColor3TB", repeaterItem);
				sliderGroup.Description = GetTextBoxValue("DescriptionTB", repeaterItem);
				sliderGroup.DescriptionSize = GetTextBoxIntValue("DescriptionSizeTB", repeaterItem);
				sliderGroup.DescriptionColor = GetTextBoxValue("DescriptionColorTB", repeaterItem);
				sliderGroup.Disclaimer = GetTextBoxValue("DisclaimerTB", repeaterItem);
				sliderGroup.DisclaimerSize = GetTextBoxIntValue("DisclaimerSizeTB", repeaterItem);
				sliderGroup.DisclaimerColor = GetTextBoxValue("DisclaimerColorTB", repeaterItem);
				sliderGroup.Button1Text = GetTextBoxValue("Button1TB", repeaterItem);
				sliderGroup.Button1Size = GetTextBoxIntValue("ButtonSize1TB", repeaterItem);
				sliderGroup.Button1Link = GetTextBoxValue("ButtonLink1TB", repeaterItem);
				sliderGroup.Button1Color = GetTextBoxValue("ButtonColor1TB", repeaterItem);
				sliderGroup.Button2Text = GetTextBoxValue("Button2TB", repeaterItem);
				sliderGroup.Button2Size = GetTextBoxIntValue("ButtonSize2TB", repeaterItem);
				sliderGroup.Button2Link = GetTextBoxValue("ButtonLink2TB", repeaterItem);
				sliderGroup.Button2Color = GetTextBoxValue("ButtonColor2TB", repeaterItem);
				sliderGroup.Button3Text = GetTextBoxValue("Button3TB", repeaterItem);
				sliderGroup.Button3Size = GetTextBoxIntValue("ButtonSize3TB", repeaterItem);
				sliderGroup.Button3Link = GetTextBoxValue("ButtonLink3TB", repeaterItem);
				sliderGroup.Button3Color = GetTextBoxValue("ButtonColor3TB", repeaterItem);
				sliderGroup.DiscountText = GetTextBoxValue("DiscountTB", repeaterItem);
				sliderGroup.DiscountSize = GetTextBoxIntValue("DiscountSizeTB", repeaterItem);
				sliderGroup.DiscountPosition = GetTextBoxIntValue("DiscountPosTB", repeaterItem);
				sliderGroup.DiscountColor = GetTextBoxValue("DiscountColorTB", repeaterItem);
				sliderGroup.ExtraText = GetTextBoxValue("ExtraTB", repeaterItem);
				sliderGroup.ExtraSize = GetTextBoxIntValue("ExtraSizeTB", repeaterItem);
				sliderGroup.ExtraPosition = GetTextBoxIntValue("ExtraPosTB", repeaterItem);
				sliderGroup.ExtraColor = GetTextBoxValue("ExtraColorTB", repeaterItem);
				sliderGroup.BackgroundColor1 = GetTextBoxValue("BackgroundColor1TB", repeaterItem);
				sliderGroup.BackgroundColor2 = GetTextBoxValue("BackgroundColor2TB", repeaterItem);
				sliderGroup.BackgroundGradient = GetDropDownListValue("BackgroundGradientList", repeaterItem);
				sliderGroup.HolderColor = GetTextBoxValue("HolderColorTB", repeaterItem);
				sliderGroup.HolderAlpha = GetTextBoxIntValue("HolderAlphaTB", repeaterItem);
				sliderGroup.HolderShape = GetDropDownListValue("HolderShapeList", repeaterItem);
				sliderGroup.HolderTextAlign = GetDropDownListValue("HolderTextAlignList", repeaterItem);
				sliderGroup.Style = GetTextBoxIntValue("StyleTB", repeaterItem);
				sliderGroup.Ordinal = GetTextBoxIntValue("OrdinalTB", repeaterItem).Value;
				sliderGroup.IsActive = GetCheckBoxValue("IsActiveCB", repeaterItem);
				sliderGroup.UseCampaignDates = GetCheckBoxValue("UseCampaignDatesCB", repeaterItem);

				if (!sliderGroup.UseCampaignDates)
				{
					string startDate = ((TextBox)repeaterItem.FindControl("StartDateTB")).Text;
					sliderGroup.StartDateTime = string.IsNullOrEmpty(startDate) ? null : (DateTime?)DateTime.Parse(startDate, CultureInfo.CurrentCulture);
					string endDate = ((TextBox)repeaterItem.FindControl("EndDateTB")).Text;
					sliderGroup.EndDateTime = string.IsNullOrEmpty(endDate) ? null : (DateTime?)DateTime.Parse(endDate, CultureInfo.CurrentCulture);
				}
				else
				{
					sliderGroup.StartDateTime = null;
					sliderGroup.EndDateTime = null;
				}

				var captionsEditor = (WebControls.LekmerTinyMceEditor) repeaterItem.FindControl("CaptionsEditor");
				sliderGroup.Captions = captionsEditor.GetValue();

				var areaCollapsiblePanel = (CollapsiblePanel) repeaterItem.FindControl("CollapsiblePanel");
				var isGroupOpened = (HiddenField) areaCollapsiblePanel.FindControl("IsOpenedHidden");
				sliderGroup.IsOpened = bool.Parse(isGroupOpened.Value);
			}

			return SliderGroups;
		}
		private int? GetContentNodeId(string controlId, RepeaterItem item)
		{
			var control = (LekmerContentPageSelector) item.FindControl(controlId);
			return control.SelectedNodeId;
		}
		private bool GetCheckBoxValue(string controlId, RepeaterItem item)
		{
			var control = (CheckBox) item.FindControl(controlId);
			return control.Checked;
		}
		private string GetTextBoxValue(string controlId, RepeaterItem item)
		{
			var control = (TextBox)item.FindControl(controlId);
			var value = control.Text.Trim();
			return !string.IsNullOrEmpty(value) ? value : null;
		}
		private int? GetTextBoxIntValue(string controlId, RepeaterItem item)
		{
			var control = (TextBox) item.FindControl(controlId);
			var value = control.Text.Trim();
			if (!string.IsNullOrEmpty(value))
			{
				int valueInt;
				int.TryParse(value, out valueInt);
				return valueInt;
			}
			return null;
		}
		private int? GetDropDownListValue(string controlId, RepeaterItem item)
		{
			var control = (DropDownList) item.FindControl(controlId);
			int selectedValue;
			if (int.TryParse(control.SelectedValue, out selectedValue))
			{
				return selectedValue > 0 ? selectedValue: (int?) null;
			}
			return null;
		}

		public Collection<string> Validate()
		{
			var errors = new Collection<string>();

			int groupCount = 0;
			foreach (RepeaterItem item in GroupRepeater.Items)
			{
				groupCount++;

				ValidateTextBoxIntValue("TitleSize1TB", "TitleSize1Validator", groupCount, item, "Title 1 - size", errors);
				ValidateTextBoxIntValue("TitleSize2TB", "TitleSize2Validator", groupCount, item, "Title 2 - size", errors);
				ValidateTextBoxIntValue("TitleSize3TB", "TitleSize3Validator", groupCount, item, "Title 3 - size", errors);
				ValidateTextBoxIntValue("DescriptionSizeTB", "DescriptionSizeValidator", groupCount, item, "Description - size", errors);
				ValidateTextBoxIntValue("DisclaimerSizeTB", "DisclaimerSizeValidator", groupCount, item, "Disclaimer - size", errors);
				ValidateTextBoxIntValue("ButtonSize1TB", "ButtonSize1Validator", groupCount, item, "Button 1 - size", errors);
				ValidateTextBoxIntValue("ButtonSize2TB", "ButtonSize2Validator", groupCount, item, "Button 2 - size", errors);
				ValidateTextBoxIntValue("ButtonSize3TB", "ButtonSize3Validator", groupCount, item, "Button 3 - size", errors);
				ValidateTextBoxIntValue("DiscountPosTB", "DiscountPosValidator", groupCount, item, "Discount - pos", errors);
				ValidateTextBoxIntValue("DiscountSizeTB", "DiscountSizeValidator", groupCount, item, "Discount - size", errors);
				ValidateTextBoxIntValue("ExtraPosTB", "ExtraPosValidator", groupCount, item, "Extra - pos", errors);
				ValidateTextBoxIntValue("ExtraSizeTB", "ExtraSizeValidator", groupCount, item, "Extra - size", errors);
				ValidateTextBoxIntValue("HolderAlphaTB", "HolderAlphaValidator", groupCount, item, "Holder - alpha", errors);
				ValidateTextBoxIntValue("StyleTB", "StyleValidator", groupCount, item, "Style", errors);
				ValidateTextBoxIntValue("OrdinalTB", "OrdinalValidator", groupCount, item, "Ordinal", errors);

				var useCampaignDates = GetCheckBoxValue("UseCampaignDatesCB", item);
				if (!useCampaignDates)
				{
					string startDate = ((TextBox)item.FindControl("StartDateTB")).Text.Trim();
					string endDate = ((TextBox)item.FindControl("EndDateTB")).Text.Trim();
					if (string.IsNullOrEmpty(startDate) && string.IsNullOrEmpty(endDate))
					{
						continue;
					}

					var startDateValidator = (CustomValidator)item.FindControl("StartDateValidator");
					var endDateValidator = (CustomValidator)item.FindControl("EndDateValidator");

					if ((string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate)) || (!string.IsNullOrEmpty(startDate) && string.IsNullOrEmpty(endDate)))
					{
						errors.Add(GroupName + " " + groupCount + " - " + GeneralMessage.YouCanNotEnteringJustEndDateOrStartDate);
						if (string.IsNullOrEmpty(startDate))
						{
							startDateValidator.IsValid = false;
						}
						else
						{
							endDateValidator.IsValid = false;
						}
						continue;
					}

					DateTime startDateTime;
					DateTime endDateTime;
					var validFormats = new[] { CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, ShortDateTimePattern };

					if (!DateTime.TryParseExact(startDate, validFormats, CultureInfo.CurrentCulture, DateTimeStyles.None, out startDateTime)
						|| (startDateTime < _minDate || startDateTime > _maxDate))
					{
						startDateValidator.IsValid = false;
					}

					if (!DateTime.TryParseExact(endDate, validFormats, CultureInfo.CurrentCulture, DateTimeStyles.None, out endDateTime)
						|| (endDateTime < _minDate || endDateTime > _maxDate))
					{
						endDateValidator.IsValid = false;
					}

					if (!startDateValidator.IsValid)
					{
						errors.Add(GroupName + " " + groupCount + " - " + GetIncorrectDateTimeMessage(GeneralMessage.StartDateTimeIncorrect));
					}

					if (!endDateValidator.IsValid)
					{
						errors.Add(GroupName + " " + groupCount + " - " + GetIncorrectDateTimeMessage(GeneralMessage.EndDateTimeIncorrect));
					}

					if (startDateValidator.IsValid && endDateValidator.IsValid && startDateTime > endDateTime)
					{
						var startEndDateLimitValidator = (CustomValidator)item.FindControl("StartEndDateLimitValidator");
						startEndDateLimitValidator.IsValid = false;
						errors.Add(GroupName + " " + groupCount + " - " + GeneralMessage.EndDateGreaterStartDate);
					}
				}
			}
			
			return errors;
		}
		private void ValidateTextBoxIntValue(string controlId, string validatorId, int groupCount, RepeaterItem item, string message, Collection<string> errors)
		{
			var control = (TextBox) item.FindControl(controlId);
			if (!string.IsNullOrEmpty(control.Text.Trim()))
			{
				int value;
				if (!int.TryParse(control.Text, out value))
				{
					var validator = (CustomValidator) item.FindControl(validatorId);
					validator.IsValid = false;
					errors.Add(GroupName + " " + groupCount + " - " + message + GeneralMessage.NumberGreaterZero);
				}
			}
		}
		private string GetIncorrectDateTimeMessage(string message)
		{
			return string.Format(CultureInfo.CurrentCulture, message, CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern, ShortDateTimePattern);
		}

		protected void Refresh(object sender, EventArgs e)
		{
			//SliderGroups = GetSliderGroups();

			//int groupId;
			//if (!int.TryParse(DropzoneGroupIdHF.Value, out groupId)) return;

			//var group = SliderGroups.FirstOrDefault(item => item.SliderGroupId == groupId);
			//if (group == null) return;

			//int fileId = 0;
			//var imageType = DropzoneImageTypeHF.Value;
			//var dropzoneAction = DropzoneActionHF.Value;
			//if (dropzoneAction == "remove")
			//{
			//	fileId = -1;
			//}
			//else if (dropzoneAction == "upload")
			//{
			//	if (!int.TryParse(DropzoneFileIdHF.Value, out fileId)) return;
			//}

			//int? imageId = fileId < 0 ? (int?) null : fileId;

			//switch (imageType)
			//{
			//	case "main1":
			//		group.ImageId1 = imageId;
			//		break;
			//	case "main2":
			//		group.ImageId2 = imageId;
			//		break;
			//	case "thumbnail1":
			//		group.ThumbnailImageId1 = imageId;
			//		break;
			//	case "thumbnail2":
			//		group.ThumbnailImageId2 = imageId;
			//		break;
			//}

			//BindSliderGroups();

			Refresh();
		}
		public void Refresh()
		{
			PopulateControl();
		}
	}

	[Serializable]
	public class SliderGroupState
	{
		public int BlockId
		{
			get;
			set;
		}
		public Collection<ISliderGroup> SliderGroups
		{
			get;
			set;
		}
	}
}