﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.BackOffice.UserControls.Common;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class ProductDiscountActionConfigurator : ProductActionControl<IProductDiscountAction>
	{
		protected Collection<IChannel> Channels { get; set; }
		protected Collection<ICountry> Countries { get; set; }
		protected Collection<ICurrency> Currencies { get; set; }
		protected Dictionary<int, string> ChannelsCountryIso { get; set; }

		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get
			{
				if (_campaignConfiguratorHelper == null)
				{
					_campaignConfiguratorHelper = new CampaignConfiguratorHelper();
				}

				return _campaignConfiguratorHelper;
			}
		}

		protected override void SetEventHandlers()
		{
			ProductIncludePopupSearchButton.Click += OnProductIncludeSearch;
			ProductIncludeGrid.PageIndexChanging += ProductIncludeGridPageIndexChanging;
			ProductIncludeGrid.RowDataBound += OnProductIncludeGridRowDataBound;
			ProductIncludeGrid.RowCommand += OnProductIncludeGridRowCommand;
			ProductIncludePopupOkButton.Click += OnIncludeProducts;
			ProductIncludePopupAddAllButton.Click += OnIncludeAllProducts;
			RemoveSelectionFromProductIncludeGridButton.Click += OnRemoveSelectionFromProductIncludeGrid;
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		private void OnIncludeAllProducts(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				UpdateDiscounts();

				var productSecureService = IoC.Resolve<IProductSecureService>();

				var criteriaList = new List<IProductSearchCriteria>();
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				criteriaList.Add(criteria);
				var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
				var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

				var products = productSecureService.AdvancedSearch(
					ChannelHelper.CurrentChannel.Id,
					searchCriteriaInclusive,
					searchCriteriaExclusive,
					1,
					int.MaxValue);

				var orderedProducts = products.OrderBy(i => i.Id);

				foreach (var product in orderedProducts)
				{
					if (State.ProductDiscountPrices.ContainsKey(product.Id)) continue;

					State.ProductDiscountPrices.Add(product.Id, new LekmerCurrencyValueDictionary());
				}
				DataBindProductIncludeGrid(State.ProductDiscountPrices);
			}
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			DataBindProductIncludeGrid(State.ProductDiscountPrices);
		}

		protected override void PopulateControl()
		{
		}

		public override void DataBind(IProductAction dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var action = (IProductDiscountAction)dataSource;
			State = action;
			DataBindProductIncludeGrid(action.ProductDiscountPrices);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();
			SetVisibility();
		}

		public override bool TryGet(out IProductAction entity)
		{
			var isValid = UpdateDiscounts();
			if (isValid)
			{
				entity = State;
				return true;
			}
			ValidationMessages.Add(Resources.LekmerMessage.Campaign_DiscountPriceIncorrect);
			entity = null;
			return false;
		}

		protected void OnProductIncludeSearch(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				ProductIncludeSearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void ProductIncludeGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			UpdateDiscounts();
			ProductIncludeGrid.PageIndex = e.NewPageIndex;
			DataBindProductIncludeGrid(State.ProductDiscountPrices);
		}

		protected void OnProductIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				var product = (IProduct)e.Row.DataItem;
				var amountsManager = (CurrencyValueInlineManager)e.Row.FindControl("AmountsManager");

				amountsManager.DataRequired += CurrencyValueInlineManagerDataRequired;

				amountsManager.DataSource = State.ProductDiscountPrices[product.Id];
				amountsManager.DataBind();
			}

			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ProductIncludeApplyToAllSelectedDiv, false);
		}

		protected void CurrencyValueInlineManagerDataRequired(object sender, EventArgs e)
		{
			if (Channels == null || Countries == null || Currencies == null || ChannelsCountryIso == null)
			{
				Channels = IoC.Resolve<ILekmerChannelSecureService>().GetAll(BackOfficeSetting.Instance.ChannelCommonNamesToIgnore);
				Countries = IoC.Resolve<ILekmerCountrySecureService>().GetAll(BackOfficeSetting.Instance.CountryIdsToIgnore);
				Currencies = IoC.Resolve<ILekmerCurrencySecureService>().GetAll(BackOfficeSetting.Instance.CurrencyIdsToIgnore);

				ChannelsCountryIso = new Dictionary<int, string>();
				var campaignCurrencyValueHelper = new CampaignCurrencyValueHelper();

				foreach (ICurrency currency in Currencies)
				{
					ChannelsCountryIso[currency.Id] = campaignCurrencyValueHelper.GetChannelsCountryIso(Channels, Countries, currency.Id);
				}
			}

			var amountsManager = (CurrencyValueInlineManager)sender;
			amountsManager.ProvideRequiredData(Currencies, ChannelsCountryIso);
		}

		protected void OnProductIncludeGridRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				UpdateDiscounts();
				var id = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
				State.ProductDiscountPrices.Remove(id);
				DataBindProductIncludeGrid(State.ProductDiscountPrices);
			}
		}

		protected void OnIncludeProducts(object sender, EventArgs e)
		{
			UpdateDiscounts();
			var products = ProductIncludeSearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (State.ProductDiscountPrices.ContainsKey(product.Id)) continue;

				State.ProductDiscountPrices.Add(product.Id, new LekmerCurrencyValueDictionary());
			}
			DataBindProductIncludeGrid(State.ProductDiscountPrices);
		}

		protected void OnRemoveSelectionFromProductIncludeGrid(object sender, EventArgs e)
		{
			UpdateDiscounts();
			foreach (var productId in ProductIncludeGrid.GetSelectedIds())
			{
				State.ProductDiscountPrices.Remove(productId);
			}
			DataBindProductIncludeGrid(State.ProductDiscountPrices);
		}

		private void DataBindProductIncludeGrid(Dictionary<int, LekmerCurrencyValueDictionary> data)
		{
			ProductIncludeGrid.PageSize = Convert.ToInt32(ProductGridPageSizeSelect.SelectedValue);

			IncludeProductDataSource.SelectParameters.Clear();
			IncludeProductDataSource.SelectParameters.Add("maximumRows", ProductGridPageSizeSelect.SelectedValue.ToString(CultureInfo.CurrentCulture));
			IncludeProductDataSource.SelectParameters.Add("startRowIndex", ProductIncludeGrid.PageIndex.ToString(CultureInfo.CurrentCulture));
			IncludeProductDataSource.SelectParameters.Add("productIdsString", Foundation.Convert.ToStringIdentifierList(data.Keys.ToList()));

			ProductIncludeGrid.DataBind();
			ProductIncludeGridUpdatePanel.Update();
		}

		private bool UpdateDiscounts()
		{
			foreach (GridViewRow row in ProductIncludeGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				var id = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var productDiscountPrice = State.ProductDiscountPrices.First(i => i.Key == id);

				var amountsManager = (CurrencyValueInlineManager) row.FindControl("AmountsManager");
				var currencyValues = amountsManager.CollectCurrencyValues();
					State.ProductDiscountPrices.Remove(productDiscountPrice.Key);
				State.ProductDiscountPrices.Add(productDiscountPrice.Key, currencyValues);
				}
			return true;
				}

		private readonly Collection<ICategory> _categories = new Collection<ICategory>();
		protected string GetCategory(int categoryId)
		{
			var category = _categories.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				category = categorySecureService.GetById(categoryId);
				_categories.Add(category);
			}
			return category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(int statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			var status = _productStatuses.FirstOrDefault(s => s.Id == statusId);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		protected virtual string GetProductPrice(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (lekmerProduct.Price == null)
			{
				return string.Empty;
			}

			return lekmerProduct.Price.PriceIncludingVat.ToString("C", CurrencyCulture);
		}

		private CultureInfo _currencyCulture;
		protected CultureInfo CurrencyCulture
		{
			get
			{
				if (_currencyCulture == null)
				{
					_currencyCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
					_currencyCulture.NumberFormat.CurrencySymbol = string.Empty;
					_currencyCulture.NumberFormat.NumberGroupSizes = _currencyCulture.NumberFormat.CurrencyGroupSizes;
					_currencyCulture.NumberFormat.NumberGroupSeparator = _currencyCulture.NumberFormat.CurrencyGroupSeparator;
					_currencyCulture.NumberFormat.NumberDecimalSeparator = _currencyCulture.NumberFormat.CurrencyDecimalSeparator;
					_currencyCulture.NumberFormat.NumberDecimalDigits = _currencyCulture.NumberFormat.CurrencyDecimalDigits;
				}
				return _currencyCulture;
			}
		}

		private void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_pdac_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);

			const string script = "$(function() {" +
								  "ResizePopup('ActionPopupDiv', 0.8, 0.8,  0.06);" +
								  "ResizePopup('ProductIncludePopupDiv', 0.8, 0.6, 0.07);" +
								  "});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_pdac_popupResize", script, true);
		}

		private void RegisterCollapsiblePanelScripts()
		{
			var includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
		}

		private void SetVisibility()
		{
			if (ProductIncludeGrid.Rows.Count > 0)
			{
				IncludePanelDiv.Style.Remove("display");
			}
			else
			{
				IncludePanelDiv.Style.Add("display", "none");
			}

			ProductIncludeApplyToAllSelectedDiv.Style["display"] = ProductIncludeGrid.GetSelectedIds().Count() > 0 ? "block" : "none";
		}
	}
}