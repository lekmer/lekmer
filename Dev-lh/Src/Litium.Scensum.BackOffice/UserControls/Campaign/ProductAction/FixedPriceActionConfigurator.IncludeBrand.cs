using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class FixedPriceActionConfigurator
	{
		private void SetIncludeBrandEventHandlers()
		{
			BrandIncludeGrid.RowDataBound += OnBrandIncludeGridRowDataBound;
			BrandIncludeGrid.RowCommand += OnBrandIncludeGridRowCommand;

			BrandIncludeSearchGrid.RowDataBound += OnBrandIncludeSearchGridRowDataBound;

			BrandIncludePopupOkButton.Click += OnIncludeBrands;
			RemoveSelectionFromBrandIncludeGridButton.Click += OnRemoveSelectionFromBrandIncludeGrid;
		}

		private void DataBindIncludeBrands(IFixedPriceAction action)
		{
			DataBindBrandIncludeSearchGrid();
			DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(action.IncludeBrands));
		}

		protected virtual void OnBrandIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, BrandIncludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnBrandIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveBrand"))
			{
				RemoveIncludedBrand(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OnBrandIncludeSearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, null, false);
		}

		protected virtual void RemoveIncludedBrand(int brandId)
		{
			State.IncludeBrands.Remove(brandId);

			DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.IncludeBrands));
		}

		protected virtual void OnIncludeBrands(object sender, EventArgs e)
		{
			var brandIds = CampaignConfiguratorHelper.GetSelectedIdsFromGrid(BrandIncludeSearchGrid, "IdHiddenField");

			foreach (var brandId in brandIds)
			{
				if (!State.IncludeBrands.ContainsKey(brandId))
				{
					State.IncludeBrands.Add(brandId);
				}
			}

			DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.IncludeBrands));

			if (State.IncludeBrands.Count > 0)
			{
				IncludeAllProductsCheckbox.Checked = false;
			}

			CampaignConfiguratorHelper.ClearSelectedIdsFromGrid(BrandIncludeSearchGrid);
		}

		protected virtual void OnRemoveSelectionFromBrandIncludeGrid(object sender, EventArgs e)
		{
			foreach (var brandId in GetSelectedBrandsFromIncludeGrid())
			{
				State.IncludeBrands.Remove(brandId);
			}

			DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.IncludeBrands));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedBrandsFromIncludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(BrandIncludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindBrandIncludeGrid(Collection<IBrand> brands)
		{
			BrandIncludeGrid.DataSource = brands;
			BrandIncludeGrid.DataBind();
			BrandIncludeGridUpdatePanel.Update();
		}

		protected virtual void DataBindBrandIncludeSearchGrid()
		{
			BrandIncludeSearchGrid.DataSource = BrandCollection;
			BrandIncludeSearchGrid.DataBind();
			BrandIncludePopupUpdatePanel.Update();
		}
	}
}