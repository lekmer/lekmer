﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductDiscountActionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction.ProductDiscountActionConfigurator" %>
<%@ Import Namespace="Litium.Scensum.Product" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueInlineManager.ascx" %>

<script type="text/javascript">	
	function confirmDeleteProducts() {
		return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
	}
</script>

<div class="campaign-full-left">
	<div class="input-box campaign-action-value">
		<asp:Label ID="ProductGridPageSize" Text="<%$Resources:Campaign, Literal_ProductGridPageSize%>" runat="server" />
		<br />
		<asp:DropDownList ID="ProductGridPageSizeSelect" runat="server" AutoPostBack="True">
			<asp:ListItem Value="10">10</asp:ListItem>
			<asp:ListItem Value="100">100</asp:ListItem>
			<asp:ListItem Selected="True" Value="250">250</asp:ListItem>
			<asp:ListItem Value="500">500</asp:ListItem>
		</asp:DropDownList>
	</div>
</div>
<br class="clear" />
<br />

<div class="campaign-full-left">
	<div id="includeToolbar">	
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenIncludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content">
			<asp:Panel ID="IncludePanel" runat="server">
				<asp:Label ID="LabelProducts" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="ProductIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<sc:GridViewWithCustomPager
							ID="ProductIncludeGrid"
							SkinID="grid"
							runat="server"
							AllowPaging="true"
							AutoGenerateColumns="false"
							Width="100%"
							DataSourceID="IncludeProductDataSource">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%# ((IProduct)Container.DataItem).Id %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# ((IProduct)Container.DataItem).ErpId %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="22%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# ((IProduct)Container.DataItem).DisplayTitle %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Category %>" ItemStyle-Width="12%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory( ((IProduct)Container.DataItem).CategoryId )%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus( ((IProduct)Container.DataItem).ProductStatusId )%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TypeLiteral" Text='<%# GetProductType(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price  %>"
									ItemStyle-HorizontalAlign="Right" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="PriceLiteral" Text='<%# GetProductPrice(Container.DataItem) %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_DiscountPrice  %>"
									ItemStyle-HorizontalAlign="Center" ItemStyle-Width="35%">
									<ItemTemplate>
										<scensum:CurrencyValueManager ID="AmountsManager" runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# ((IProduct)Container.DataItem).Id %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmProductRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
						<asp:ObjectDataSource ID="IncludeProductDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethodWithSortingByErpId" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />

						<div runat="server" id="ProductIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>
</div>

<div id="ProductIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="ProductIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="ProductIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="campaign-product-search">
					<div class="content-box">
						<asp:Panel ID="SearchPanel" DefaultButton="ProductIncludePopupSearchButton" runat="server">
						<uc:ProductSearchForm ID="ProductIncludeSearchFormControl" runat="server" />
						<br style="clear:both;" />
						<br />
						<div class="campaign-popup-buttons no-padding">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
						</div>
						</asp:Panel>
						<br class="clear"/>
						<br />
						<uc:ProductSearchResult ID="ProductIncludeSearchResultControl" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
		
<ajaxToolkit:ModalPopupExtender 
	ID="ProductIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeProductSearchPopupButton"
	PopupControlID="ProductIncludePopupDiv" 
	CancelControlID="ProductIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />