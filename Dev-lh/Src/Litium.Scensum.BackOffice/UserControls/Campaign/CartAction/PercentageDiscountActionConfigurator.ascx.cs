﻿using System;
using System.Globalization;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class PercentageDiscountActionConfigurator : CartActionControl<IPercentageDiscountCartAction>
	{
		public override void DataBind(ICartAction dataSource)
		{
			var percentageDiscountCartAction = (IPercentageDiscountCartAction)dataSource;
			State = percentageDiscountCartAction;
			DiscountValueTextBox.Text = State.DiscountAmount.ToString(CultureInfo.CurrentCulture);
		}

		public override bool TryGet(out ICartAction entity)
		{
			decimal discountAmount;

			entity = State;

			string discountValue = DiscountValueTextBox.Text.Trim();

			if (discountValue.Length == 0 || !decimal.TryParse(discountValue, out discountAmount))
			{
				ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.CampaignMessage.DiscountInvalid));
				return false;
			}

			if (discountAmount < 0 || discountAmount > 100)
			{
				ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.CampaignMessage.DiscountInvalidRange));
				return false;
			}

			State.DiscountAmount = discountAmount;
			entity.Status = BusinessObjectStatus.Edited;

			return true;
		}

		protected override void PopulateControl()
		{
		}

		protected override void SetEventHandlers()
		{
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			const string script = "$(function() {" +
				"var popupDiv = $(\"div[id$='ActionPopupDiv']\");" +
				"popupDiv.attr('class','campaign-popup-container small-campaign-popup-container');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-header-center']:first\").attr('class','campaign-popup-header-center small-campaign-popup-header-center');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-content']:first\").attr('class','campaign-popup-content small-campaign-popup-content');" +
				"});";

			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_fvac_smallPopup", script, true);
		}
	}
}