﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="GiftCardViaMailCartActionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.GiftCardViaMailCartActionConfigurator" %>

<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>

<div class="campaign-full-left">
	<div class="input-box campaign-action-type">
		<asp:Label ID="DiscountTypeLabel" Text="<%$Resources:Campaign, Literal_GiftCardViaEmailType%>" runat="server" />
		<br />
		<asp:TextBox ID="DiscountTypeTextBox" runat="server" ReadOnly="true" />
		<br />
		<br />
		<asp:Label ID="SendingIntervalLabel" Text="<%$Resources:Campaign, Literal_GiftCardViaEmailSendingInterval%>" runat="server" />
		<br />
		<asp:TextBox ID="SendingIntervalTextBox" runat="server" />
		<br />
		<br />
		<asp:Label ID="ChooseTemplateLabel" Text="<%$ Resources:General,Literal_ChooseTemplate%>" runat="server" />
		<br />
		<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="203px" />
	</div>

	<div class="input-box campaign-action-value">
		<scensum:CurrencyValueManager runat="server" id="CurrencyValueManager" />
	</div>
</div>