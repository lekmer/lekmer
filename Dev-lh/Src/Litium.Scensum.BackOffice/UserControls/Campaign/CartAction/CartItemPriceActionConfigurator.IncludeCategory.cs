﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.CartItemPriceAction;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemPriceActionConfigurator
	{
		private Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private void SetIncludeCategoryEventHandlers()
		{
			CategoryIncludeGrid.RowDataBound += OnCategoryIncludeGridRowDataBound;
			CategoryIncludeGrid.RowCommand += OnCategoryIncludeGridRowCommand;
			CategoryIncludePopupOkButton.Click += OnIncludeCategories;
			RemoveSelectionFromCategoryIncludeGridButton.Click += OnRemoveSelectionFromCategoryIncludeGrid;
		}

		private void DataBindIncludeCategories(ICartItemPriceAction action)
		{
			CategoryIncludePopupCategoryTree.Selector = CategorySelector;
			CategoryIncludePopupCategoryTree.DataBind();
			DataBindCategoryIncludeGrid(ResolveCategories(action.IncludeCategories));
		}

		protected virtual void OnCategoryIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CategoryIncludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnCategoryIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				RemoveIncludedCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveIncludedCategory(int productId)
		{
			State.IncludeCategories.Remove(productId);
			DataBindCategoryIncludeGrid(ResolveCategories(State.IncludeCategories));
		}

		protected virtual void OnIncludeCategories(object sender, EventArgs e)
		{
			foreach (int categoryId in CategoryIncludePopupCategoryTree.SelectedIds)
			{
				if (!State.IncludeCategories.ContainsKey(categoryId))
				{
					State.IncludeCategories.Add(categoryId);
				}
			}
			DataBindCategoryIncludeGrid(ResolveCategories(State.IncludeCategories));
			if (State.IncludeCategories.Count > 0)
			{
				IncludeAllProductsCheckbox.Checked = false;
			}
			CategoryIncludePopupCategoryTree.SelectedIds.Clear();
		}

		protected virtual void OnRemoveSelectionFromCategoryIncludeGrid(object sender, EventArgs e)
		{
			foreach (var categoryId in GetSelectedCategoriesFromIncludeGrid())
			{
				State.IncludeCategories.Remove(categoryId);
			}
			DataBindCategoryIncludeGrid(ResolveCategories(State.IncludeCategories));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCategoriesFromIncludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CategoryIncludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindCategoryIncludeGrid(Collection<ICategory> categories)
		{
			CategoryIncludeGrid.DataSource = categories;
			CategoryIncludeGrid.DataBind();
			CategoryIncludeGridUpdatePanel.Update();
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		protected virtual string GetCategoryPath(object cat)
		{
			var category = cat as ICategory;
			if (category == null) return string.Empty;

			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = RetrieveCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}

		protected virtual ICategory RetrieveCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}
	}
}
