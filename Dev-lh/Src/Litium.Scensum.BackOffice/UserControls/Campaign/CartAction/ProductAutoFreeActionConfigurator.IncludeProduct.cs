﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class ProductAutoFreeActionConfigurator
	{
		private void SetIncludeProductEventHandlers()
		{
			ProductIncludePopupSearchButton.Click += OnProductIncludeSearch;
			ProductIncludeGrid.PageIndexChanging += ProductIncludeGridPageIndexChanging;
			ProductIncludeGrid.RowDataBound += OnProductIncludeGridRowDataBound;
			ProductIncludeGrid.RowCommand += OnProductIncludeGridRowCommand;
			ProductIncludePopupOkButton.Click += OnIncludeProducts;
			RemoveSelectionFromProductIncludeGridButton.Click += OnRemoveSelectionFromProductIncludeGrid;
		}

		private void DataBindIncludeProducts(IProductAutoFreeCartAction action)
		{
			DataBindProductIncludeGrid(action.ProductId);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "criteria")]
		protected virtual void OnProductIncludeSearch(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				ProductIncludeSearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void ProductIncludeGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductIncludeGrid.PageIndex = e.NewPageIndex;
			DataBindProductIncludeGrid(State.ProductId);
		}

		protected virtual void OnProductIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ProductIncludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnProductIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				int productId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
				if (State.ProductId == productId)
				{
					RemoveIncludedProduct();
				}
			}
		}

		protected virtual void OnIncludeProducts(object sender, EventArgs e)
		{
			var products = ProductIncludeSearchResultControl.GetSelectedProducts();
			if (products.Count > 1)
			{
				ValidationMessages.Add(Resources.CampaignMessage.IncorrectCountOfSelectedProduct);
				ProductIncludeSearchPopup.Show();
				errorDiv.Visible = true;

				return;
			}

			foreach (var product in products)
			{
				if (State != null && State.ProductId != product.Id)
				{
					State.ProductId = product.Id;
				}
			}

			errorDiv.Visible = false;

			DataBindProductIncludeGrid(State.ProductId);
		}

		protected virtual void OnRemoveSelectionFromProductIncludeGrid(object sender, EventArgs e)
		{
			RemoveIncludedProduct();
		}

		protected virtual void RemoveIncludedProduct()
		{
			State.ProductId = 0;
			DataBindProductIncludeGrid(State.ProductId);
		}

		protected virtual void DataBindProductIncludeGrid(int productId)
		{
			IncludeProductDataSource.SelectParameters.Clear();

			if (productId > 0)
			{
				IncludeProductDataSource.SelectParameters.Add("maximumRows", ProductIncludeGrid.PageSize.ToString(CultureInfo.CurrentCulture));
				IncludeProductDataSource.SelectParameters.Add("startRowIndex", ProductIncludeGrid.PageIndex.ToString(CultureInfo.CurrentCulture));
				IncludeProductDataSource.SelectParameters.Add("productIdsString", productId.ToString(CultureInfo.InvariantCulture));
			}

			ProductIncludeGrid.DataBind();
			ProductIncludeGridUpdatePanel.Update();
		}
	}
}