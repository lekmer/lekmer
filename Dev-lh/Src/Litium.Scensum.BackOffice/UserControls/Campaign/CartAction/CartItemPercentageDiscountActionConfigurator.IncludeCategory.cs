﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemPercentageDiscountActionConfigurator
	{
		private void SetIncludeCategoryEventHandlers()
		{
			CategoryIncludeGrid.RowDataBound += OnCategoryIncludeGridRowDataBound;
			CategoryIncludeGrid.RowCommand += OnCategoryIncludeGridRowCommand;
			CategoryIncludePopupOkButton.Click += OnIncludeCategories;
			RemoveSelectionFromCategoryIncludeGridButton.Click += OnRemoveSelectionFromCategoryIncludeGrid;
		}

		private void DataBindIncludeCategories(ICartItemPercentageDiscountAction action)
		{
			CategoryIncludePopupCategoryTree.Selector = CategorySelector;
			CategoryIncludePopupCategoryTree.DataBind();
			DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(action.IncludeCategories));
		}

		protected virtual void OnCategoryIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CategoryIncludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnCategoryIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				RemoveIncludedCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveIncludedCategory(int productId)
		{
			State.IncludeCategories.Remove(productId);
			DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
		}

		protected virtual void OnIncludeCategories(object sender, EventArgs e)
		{
			foreach (int categoryId in CategoryIncludePopupCategoryTree.SelectedIds)
			{
				if (!State.IncludeCategories.ContainsKey(categoryId))
				{
					State.IncludeCategories.Add(categoryId, false);
				}
			}
			DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
			if (State.IncludeCategories.Count > 0)
			{
				IncludeAllProductsCheckbox.Checked = false;
			}
			CategoryIncludePopupCategoryTree.SelectedIds.Clear();
			CategoryIncludePopupCategoryTree.DataBind();
		}

		protected virtual void OnRemoveSelectionFromCategoryIncludeGrid(object sender, EventArgs e)
		{
			foreach (var categoryId in GetSelectedCategoriesFromIncludeGrid())
			{
				State.IncludeCategories.Remove(categoryId);
			}
			DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCategoriesFromIncludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CategoryIncludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindCategoryIncludeGrid(Collection<ICategory> categories)
		{
			CategoryIncludeGrid.DataSource = categories;
			CategoryIncludeGrid.DataBind();
			CategoryIncludeGridUpdatePanel.Update();
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		protected virtual bool GetIncludeSubcategories(object id)
		{
			var categoryId = System.Convert.ToInt32(id, CultureInfo.CurrentCulture);
			bool include;
			return State.IncludeCategories.TryGetValue(categoryId, out include) && include;
		}

		protected virtual void OnIncludeSubcategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox)sender;
			var row = (GridViewRow)checkBox.Parent.Parent;
			var idHidden = (HiddenField)row.FindControl("IdHiddenField");
			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);
			bool include;
			if (!State.IncludeCategories.TryGetValue(categoryId, out include)) return;
			State.IncludeCategories[categoryId] = checkBox.Checked;
			DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
		}
	}
}