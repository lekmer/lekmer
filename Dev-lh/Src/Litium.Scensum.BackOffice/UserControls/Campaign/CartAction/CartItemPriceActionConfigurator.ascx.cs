﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartItemPriceAction;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemPriceActionConfigurator : CartActionControl<ICartItemPriceAction>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get
			{
				if (_campaignConfiguratorHelper == null)
				{
					_campaignConfiguratorHelper = new CampaignConfiguratorHelper();
				}

				return _campaignConfiguratorHelper;
			}
		}

		protected override void SetEventHandlers()
		{
			SetIncludeProductEventHandlers();
			SetExcludeProductEventHandlers();
			SetIncludeCategoryEventHandlers();
			SetExcludeCategoryEventHandlers();

			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		protected override void PopulateControl() { }

		public override void DataBind(ICartAction dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var action = (ICartItemPriceAction)dataSource;
			State = action;

			MaxQuantityTextBox.Text = action.MaxQuantity.ToString(CultureInfo.CurrentCulture);

			AmountsManager.DataSource = action.Amounts;
			AmountsManager.DataBind();

			IncludeAllProductsCheckbox.Checked = action.IncludeAllProducts;

			DataBindIncludeProducts(action);
			DataBindExcludeProducts(action);

			DataBindIncludeCategories(action);
			DataBindExcludeCategories(action);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			IncludePanel.Visible = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0;
			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				DataBindProductIncludeGrid(new ProductIdDictionary());
				DataBindCategoryIncludeGrid(new Collection<ICategory>());
			}
			else
			{
				DataBindProductIncludeGrid(State.IncludeProducts);
				DataBindCategoryIncludeGrid(ResolveCategories(State.IncludeCategories));
			}
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			DataBindProductIncludeGrid(State.IncludeProducts);

			ProductExcludeGrid.PageIndex = 0;
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		public override bool TryGet(out ICartAction entity)
		{
			if (IsValid())
			{
				if (IncludeAllProductsCheckbox.Checked)
				{
					State.IncludeProducts.Clear();
					State.IncludeCategories.Clear();
				}

				var action = State;
				action.MaxQuantity = int.Parse(MaxQuantityTextBox.Text, CultureInfo.CurrentCulture);
				action.Amounts = AmountsManager.CollectCurrencyValues();
				action.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;
				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			int maxQuantity;
			if (!int.TryParse(MaxQuantityTextBox.Text, out maxQuantity) || maxQuantity < 0)
			{
				isValid = false;
				ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.CartItemPriceAction.Message_MaxQuantityInvalid, 0, int.MaxValue));
			}

			Collection<string> amountsValidationMessages;
			if (!AmountsManager.IsValid(out amountsValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in amountsValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);

			const string script = "$(function() {" +
								  "ResizePopup('ActionPopupDiv', 0.75, 0.7,  0.065);" +
								  "ResizePopup('ProductIncludePopupDiv', 0.8, 0.6, 0.07);" +
								  "ResizePopup('ProductExcludePopupDiv', 0.8, 0.6, 0.07);" +
								  "ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
								  "ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
								  "});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);
		}

		protected virtual Collection<ICategory> ResolveCategories(CategoryIdDictionary categoryDictionary)
		{
			var categories = new Collection<ICategory>();
			foreach (var id in categoryDictionary)
			{
				var category = IoC.Resolve<ICategorySecureService>().GetById(id.Key);
				if (category == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Category with ID {0} could not be found.",
						id));
				}
				categories.Add(category);
			}
			return categories;
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
			{
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();
			}

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			ProductExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}
	}
}