﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="VoucherConditionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.Condition.VoucherConditionConfigurator" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<script type="text/javascript">
    $(function() {
        $("div[id$='ConditionPopupDiv']").attr('class', 'campaign-popup-container');
        ResizePopup('ConditionPopupDiv', 0.55, 0.7, 0.065);
        $("div[class^='campaign-popup-content']").css('overflow-y', 'scroll');
    });

    function CloseBatchIncludeSelect() {
        $('.BatchIncludeCancelButton').click();
    }

    function CloseBatchExcludeSelect() {
        $('.BatchExcludeCancelButton').click();
    }

    function confirmDeleteBatchIds() {
        return DeleteConfirmation("<%= Resources.Lekmer.Literal_DeleteBatchIds  %>");
    }
</script>

<div class="campaign-full-left">

	<!-- Include Toolbar Start -->
	<div id="includeToolbar">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
					&nbsp;
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenBatchIncludeSearchPopupButton" Text="<%$Resources:Lekmer, Literal_BatchId%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content">
			<br />
			<asp:Label ID="IncludeAllBatchesLabel" Text="<%$Resources:Campaign, Literal_IncludeAllBatches%>" runat="server" CssClass="text-bold"></asp:Label>
			<br />
			<asp:CheckBox ID="IncludeAllBatchesCheckbox" AutoPostBack="true" runat="server" Text="<%$Resources:Campaign, Literal_AllBatchesInRange%>" />
			<br />
			<br />
			<asp:Panel ID="IncludePanel" runat="server">
				<asp:UpdatePanel ID="BatchIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="BatchIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField runat="server" id="BatchIdHiddenField" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_BatchId %>" ItemStyle-Width="70%">
									<ItemTemplate>
										<asp:Label runat="server" ID="BatchIdLiteral" ></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>
									<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveBatchInclude" CommandArgument='<%# ((int)Container.DataItem).ToString() %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Lekmer.Literal_DeleteBatchId + "\");" %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="BatchIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromBatchIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteBatchIds();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>
	<!-- Include  Toolbar End -->

	<br class="clear"/>
	<br />
	
	<!-- Exclude Toolbar Start -->
	<div id="excludeToolbar">
		<div class="collapsible-items left">
			<div id="ExcludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="ExcludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/exclude.gif" />
					&nbsp;
					<span><%=Resources.Campaign.Literal_Exclude%></span>
				</div>
				<div id="ExcludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenBatchExcludeSearchPopupButton" Text="<%$Resources:Lekmer, Literal_BatchId%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ExcludePanelStateHidden" runat="server" Value="true" />
					<asp:Image ID="ExcludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="ExcludePanelDiv" runat="server" class="campaign-include-content">
			<br />
			<asp:Panel ID="ExcludePanel" runat="server">
				<asp:UpdatePanel ID="BatchExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="BatchExcludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField runat="server" id="BatchIdHiddenField" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_BatchId %>" ItemStyle-Width="70%">
									<ItemTemplate>
										<asp:Label runat="server" ID="BatchIdLiteral" ></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>
									<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveBatchExclude" CommandArgument='<%# ((int)Container.DataItem).ToString() %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Lekmer.Literal_DeleteBatchId + "\");" %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="BatchExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromBatchExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteBatchIds();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>
	<!-- Exclude  Toolbar End -->

</div>

<br class="clear"/>
<br />

<!-- Batch Popup Start -->
	<!-- Include Start -->

<asp:HiddenField runat="server" ID="FakeControlInclude" />

<div id="BatchIncludeViewDiv" runat="server" class="customer-comment-mainDiv" style="z-index: 10; display: none;">
	<div id="customer-comment-header">
		<div id="customer-comment-header-left"></div>
		<div id="customer-comment-header-center">
			<span><%= Resources.Lekmer.Label_AddBatch%></span>
			<input runat="server" type="button" id="BatchIncludeCancelButton" class="BatchIncludeCancelButton" value="x" />
		</div>
		<div id="customer-comment-header-right"></div>
	</div>

	<asp:UpdatePanel ID="BatchIncludeUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="customer-freight-subMainDiv">
				<asp:Panel runat="server" DefaultButton="BatchIncludeSaveButton">
					<uc:MessageContainer runat="server" ID="BatchIncludeMessageContainer" MessageType="Warning" HideMessagesControlId="BatchIncludeSaveButton" />
					<uc:ScensumValidationSummary ID="ValidationSummaryInclude" ForeColor="Black" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="BatchIncludeValidationGroup" />
					<div class="column1">
						<div class="input-box">
							<span><%= Resources.Lekmer.Literal_BatchId%> *</span>
							<asp:RequiredFieldValidator ID="BatchIncludeValidator" runat="server" ControlToValidate="BatchIdIncludeTextBox"
								ErrorMessage="<%$ Resources:LekmerMessage,EmptyBatchId%>" Display="None" ValidationGroup="BatchIncludeValidationGroup" />
							<asp:RegularExpressionValidator ID="ValueValidatorInclude" runat="server" ValidationGroup="BatchIncludeValidationGroup" ControlToValidate="BatchIdIncludeTextBox" 
								ValidationExpression="\d*" Text="<%$ Resources:InterfaceMessage, OnlyNumericData %>"/>
							<br />
							<asp:TextBox ID="BatchIdIncludeTextBox" runat="server" />
						</div>
						<br clear="all" />
					</div>
					<br />
					<div class="customer-edit-action-buttons">
						<uc:ImageLinkButton UseSubmitBehaviour="false" ID="BatchIncludeSaveButton" runat="server" Text="<%$ Resources:General, Button_Next %>"
							SkinID="DefaultButton" ValidationGroup="BatchIncludeValidationGroup" />
						<div class="button-container" onclick="CloseBatchIncludeSelect();" style="cursor: pointer">
							<span class="button-left-border"></span><span class="button-middle"><asp:Literal runat="server" Text="<%$ Resources:General, Button_Cancel%>"></asp:Literal></span>
							<span class="button-right-border"></span>
						</div>
					</div>
				</asp:Panel>
			</div>
		</ContentTemplate> 
	</asp:UpdatePanel>
</div>

<ajaxToolkit:ModalPopupExtender ID="BatchIncludePopup" runat="server" TargetControlID="FakeControlInclude" PopupControlID="BatchIncludeViewDiv" CancelControlID="BatchIncludeCancelButton" />

	<!-- Include End -->

	<!-- Exclude Start -->

<asp:HiddenField runat="server" ID="FakeControlExclude" />

<div id="BatchExcludeViewDiv" runat="server" class="customer-comment-mainDiv" style="z-index: 10; display: none;">
	<div id="customer-comment-header">
		<div id="customer-comment-header-left"></div>
		<div id="customer-comment-header-center">
			<span><%= Resources.Lekmer.Label_AddBatch%></span>
			<input runat="server" type="button" id="BatchExcludeCancelButton" class="BatchExcludeCancelButton" value="x" />
		</div>
		<div id="customer-comment-header-right"></div>
	</div>

	<asp:UpdatePanel ID="BatchExcludeUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="customer-freight-subMainDiv">
				<asp:Panel runat="server" DefaultButton="BatchExcludeSaveButton">
					<uc:MessageContainer runat="server" ID="BatchExcludeMessageContainer" MessageType="Warning" HideMessagesControlId="BatchExcludeSaveButton" />
					<uc:ScensumValidationSummary ID="ValidationSummaryExclude" ForeColor="Black" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="BatchExcludeValidationGroup" />
					<div class="column1">
						<div class="input-box">
							<span><%= Resources.Lekmer.Literal_BatchId%> *</span>
							<asp:RequiredFieldValidator ID="BatchExcludeValidator" runat="server" ControlToValidate="BatchIdExcludeTextBox"
								ErrorMessage="<%$ Resources:LekmerMessage,EmptyBatchId%>" Display="None" ValidationGroup="BatchExcludeValidationGroup" />
							<asp:RegularExpressionValidator ID="ValueValidatorExclude" runat="server" ValidationGroup="BatchExcludeValidationGroup" ControlToValidate="BatchIdExcludeTextBox" 
								ValidationExpression="\d*" Text="<%$ Resources:InterfaceMessage, OnlyNumericData %>"/>
							<br />
							<asp:TextBox ID="BatchIdExcludeTextBox" runat="server" />
						</div>
						<br clear="all" />
					</div>
					<br />
					<div class="customer-edit-action-buttons">
						<uc:ImageLinkButton UseSubmitBehaviour="false" ID="BatchExcludeSaveButton" runat="server" Text="<%$ Resources:General, Button_Next %>"
							SkinID="DefaultButton" ValidationGroup="BatchExcludeValidationGroup" />
						<div class="button-container" onclick="CloseBatchExcludeSelect();" style="cursor: pointer">
							<span class="button-left-border"></span><span class="button-middle"><asp:Literal runat="server" Text="<%$ Resources:General, Button_Cancel%>"></asp:Literal></span>
							<span class="button-right-border"></span>
						</div>
					</div>
				</asp:Panel>
			</div>
		</ContentTemplate> 
	</asp:UpdatePanel>
</div>

<ajaxToolkit:ModalPopupExtender ID="BatchExcludePopup" runat="server" TargetControlID="FakeControlExclude" PopupControlID="BatchExcludeViewDiv" CancelControlID="BatchExcludeCancelButton" />

	<!-- Exclude End -->
<!-- Batch Popup End -->