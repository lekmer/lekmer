﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartValueConditionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.Condition.CartValueConditionConfigurator" %>
<%@ Import Namespace="System.Globalization"%>
<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>

<br style="clear:both;" />
<scensum:CurrencyValueManager runat="server" id="CartValuesManager" />