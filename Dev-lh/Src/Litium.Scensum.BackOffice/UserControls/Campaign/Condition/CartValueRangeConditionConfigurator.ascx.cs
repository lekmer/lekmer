﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.UserControls.Common;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Exceptions;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CartValueRangeConditionConfigurator : ConditionControl<ICartValueRangeCondition>
	{
		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
		}

		public override void DataBind(ICondition dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var condition = (ICartValueRangeCondition) dataSource;
			if (condition.CartValueRangeList == null)
			{
				throw new ControlIntegrationException("CartValueRangeList shoule be initialized.");
			}

			base.DataBind();

			CurrencyValuesRepeater.DataSource = PopulateCurrencyValues(condition);
			CurrencyValuesRepeater.DataBind();

			State = condition;
		}

		public override bool TryGet(out ICondition entity)
		{
			if (IsValid())
			{
				var condition = State;
				condition.CartValueRangeList = CollectCurrencyValues();

				entity = condition;

				return true;
			}

			entity = null;

			return false;
		}

		protected virtual bool IsValid()
		{
			var isValid = true;
			var noneValueDefined = true;
			var validationMessages = new Collection<string>();

			foreach (RepeaterItem repeaterItem in CurrencyValuesRepeater.Items)
			{
				var monetaryValueFromText = ((TextBox)repeaterItem.FindControl("ValueFromTextBox")).Text;
				var monetaryValueToText = ((TextBox)repeaterItem.FindControl("ValueToTextBox")).Text;
				if (monetaryValueFromText.Trim().Length == 0 || monetaryValueToText.Trim().Length == 0)
				{
					continue;
				}

				decimal monetaryValueFrom;
				decimal monetaryValueTo;
				if (!decimal.TryParse(monetaryValueFromText, out monetaryValueFrom)
					|| !decimal.TryParse(monetaryValueToText, out monetaryValueTo))
				{
					isValid = false;
					var currencyIsoText = ((Literal) repeaterItem.FindControl("CurrencyIsoLiteral")).Text;
					validationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyFormat, currencyIsoText));
				}
				else if (monetaryValueFrom < 0 || monetaryValueTo < 0)
				{
					isValid = false;
					var currencyIsoText = ((Literal) repeaterItem.FindControl("CurrencyIsoLiteral")).Text;
					validationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueNegative, currencyIsoText));
				}
				else if (monetaryValueFrom > monetaryValueTo)
				{
					isValid = false;
					var currencyIsoText = ((Literal) repeaterItem.FindControl("CurrencyIsoLiteral")).Text;
					validationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueIncorrectRange, currencyIsoText));
				}

				noneValueDefined = false;
			}

			if (noneValueDefined)
			{
				isValid = false;
				validationMessages.Add(Resources.CampaignMessage.NoCurrencyValueSet);
			}

			if (!isValid)
			{
				foreach (string validationMessage in validationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		private Collection<CurrencyValueRange> CollectCurrencyValues()
		{
			var currencyValueRanges = new Collection<CurrencyValueRange>();

			foreach (RepeaterItem repeaterItem in CurrencyValuesRepeater.Items)
			{
				var monetaryValueFromText = ((TextBox)repeaterItem.FindControl("ValueFromTextBox")).Text;
				var monetaryValueToText = ((TextBox)repeaterItem.FindControl("ValueToTextBox")).Text;
				if (monetaryValueFromText.IsNullOrTrimmedEmpty() || monetaryValueToText.IsNullOrTrimmedEmpty())
				{
					continue;
				}

				decimal monetaryValueFrom = decimal.Parse(monetaryValueFromText, CultureInfo.CurrentCulture);
				decimal monetaryValueTo = decimal.Parse(monetaryValueToText, CultureInfo.CurrentCulture);

				var currencyIdText = ((HiddenField)repeaterItem.FindControl("CurrencyIdHiddenField")).Value;
				int currencyId = int.Parse(currencyIdText, CultureInfo.CurrentCulture);

				var currencyValueRange = new CurrencyValueRange
					{
						CurrencyId = currencyId,
						MonetaryValueFrom = monetaryValueFrom,
						MonetaryValueTo = monetaryValueTo
					};

				currencyValueRanges.Add(currencyValueRange);
			}

			return currencyValueRanges;
		}

		private Collection<CurrencyRepeaterDataSourceItem> PopulateCurrencyValues(ICartValueRangeCondition condition)
		{
			var currencyValues = new Collection<CurrencyRepeaterDataSourceItem>();

			Collection<IChannel> channels = IoC.Resolve<IChannelSecureService>().GetAll();
			Collection<ICountry> countries = IoC.Resolve<ICountrySecureService>().GetAll();
			Collection<ICurrency> currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
			foreach (ICurrency currency in currencies)
			{
				CurrencyRepeaterDataSourceItem item;
				CurrencyValueRange currencyValueRange = condition.CartValueRangeList.FirstOrDefault(cvr => cvr.CurrencyId == currency.Id);
				if (currencyValueRange == null)
				{
					item = new CurrencyRepeaterDataSourceItem
						{
							Currency = currency,
							MonetaryValueFrom = string.Empty,
							MonetaryValueTo = string.Empty
						};
				}
				else
				{
					item = new CurrencyRepeaterDataSourceItem
						{
							Currency = currency,
							MonetaryValueFrom = currencyValueRange.MonetaryValueFrom.ToString("N", CultureInfo.CurrentCulture),
							MonetaryValueTo = currencyValueRange.MonetaryValueTo.ToString("N", CultureInfo.CurrentCulture)
						};
				}

				var campaignCurrencyValueHelper = new CampaignCurrencyValueHelper();
				item.CurrencyIsoWithChannels = item.Currency.Iso + campaignCurrencyValueHelper.GetChannelsCountryIso(channels, countries, item.Currency.Id);

				currencyValues.Add(item);
			}

			return currencyValues;
		}

		protected class CurrencyRepeaterDataSourceItem
		{
			public ICurrency Currency { get; set; }
			public string CurrencyIsoWithChannels { get; set; }
			public string MonetaryValueFrom { get; set; }
			public string MonetaryValueTo { get; set; }
		}
	}
}