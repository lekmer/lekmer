﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Customer;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CustomerGroupConditionConfigurator : ConditionControl<ICustomerGroupCondition>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get
			{
				if (_campaignConfiguratorHelper == null)
				{
					_campaignConfiguratorHelper = new CampaignConfiguratorHelper();
				}

				return _campaignConfiguratorHelper;
			}
		}

		private Collection<ICustomerGroup> _customerGroupCollection;
		public Collection<ICustomerGroup> CustomerGroupCollection
		{
			get
			{
				if (_customerGroupCollection == null)
				{
					_customerGroupCollection = ((ILekmerCustomerGroupSecureService) IoC.Resolve<ICustomerGroupSecureService>()).GetAll();
				}

				return _customerGroupCollection;
			}
		}

		protected override void SetEventHandlers()
		{
			SetIncludeCustomerGroupEventHandlers();
			SetExcludeCustomerGroupEventHandlers();
		}

		protected override void PopulateControl() { }

		public override void DataBind(ICondition dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var condition = (ICustomerGroupCondition) dataSource;
			State = condition;

			DataBindIncludeCustomerGroups(condition);
			DataBindExcludeCustomerGroups(condition);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		public override bool TryGet(out ICondition entity)
		{
			entity = State;
			return true;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCustomerGroupRemove", "function ConfirmCustomerGroupRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteCustomerGroup + "');}", true);

			const string script = "$(function() {" +
									"ResizePopup('CustomerGroupIncludePopupDiv', 0.4, 0.45, 0.09);" +
									"ResizePopup('CustomerGroupExcludePopupDiv', 0.4, 0.45, 0.09);" +
									"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);
		}

		protected virtual Collection<ICustomerGroup> ResolveCustomerGroups(CustomerGroupIdDictionary customerGroupIds)
		{
			var customerGroups = new Collection<ICustomerGroup>();

			foreach (var id in customerGroupIds)
			{
				var customerGroup = IoC.Resolve<ICustomerGroupSecureService>().GetById(id.Value);
				if (customerGroup == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"CustomerGroup with ID {0} could not be found.",
						id.Value));
				}
				customerGroups.Add(customerGroup);
			}

			return customerGroups;
		}

		private void SetVisibility()
		{
			CustomerGroupIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CustomerGroupIncludeGrid) ? "block" : "none";

			CustomerGroupExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CustomerGroupExcludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = CustomerGroupIncludeGrid.Rows.Count > 0 ? "block" : "none";
			ExcludePanelDiv.Style["display"] = CustomerGroupExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}
	}
}