using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CustomerGroupConditionConfigurator
	{
		private void SetExcludeCustomerGroupEventHandlers()
		{
			CustomerGroupExcludeGrid.RowDataBound += OnCustomerGroupExcludeGridRowDataBound;
			CustomerGroupExcludeGrid.RowCommand += OnCustomerGroupExcludeGridRowCommand;

			CustomerGroupExcludeSearchGrid.RowDataBound += OnCustomerGroupExcludeSearchGridRowDataBound;

			CustomerGroupExcludePopupOkButton.Click += OnExcludeCustomerGroups;
			RemoveSelectionFromCustomerGroupExcludeGridButton.Click += OnRemoveSelectionFromCustomerGroupExcludeGrid;
		}

		private void DataBindExcludeCustomerGroups(ICustomerGroupCondition condition)
		{
			DataBindCustomerGroupExcludeSearchGrid();
			DataBindCustomerGroupExcludeGrid(ResolveCustomerGroups(condition.ExcludeCustomerGroups));
		}

		protected virtual void OnCustomerGroupExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CustomerGroupExcludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnCustomerGroupExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCustomerGroup"))
			{
				RemoveExcludedCustomerGroup(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OnCustomerGroupExcludeSearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, null, false);
		}

		protected virtual void RemoveExcludedCustomerGroup(int customerGroupId)
		{
			State.ExcludeCustomerGroups.Remove(customerGroupId);

			DataBindCustomerGroupExcludeGrid(ResolveCustomerGroups(State.ExcludeCustomerGroups));
		}

		protected virtual void OnExcludeCustomerGroups(object sender, EventArgs e)
		{
			var customerGroupIds = CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CustomerGroupExcludeSearchGrid, "IdHiddenField");

			foreach (var customerGroupId in customerGroupIds)
			{
				if (!State.ExcludeCustomerGroups.ContainsKey(customerGroupId))
				{
					State.ExcludeCustomerGroups.Add(customerGroupId);
				}
			}

			DataBindCustomerGroupExcludeGrid(ResolveCustomerGroups(State.ExcludeCustomerGroups));

			CampaignConfiguratorHelper.ClearSelectedIdsFromGrid(CustomerGroupExcludeSearchGrid);
		}

		protected virtual void OnRemoveSelectionFromCustomerGroupExcludeGrid(object sender, EventArgs e)
		{
			foreach (var customerGroupId in GetSelectedCustomerGroupsFromExcludeGrid())
			{
				State.ExcludeCustomerGroups.Remove(customerGroupId);
			}

			DataBindCustomerGroupExcludeGrid(ResolveCustomerGroups(State.ExcludeCustomerGroups));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCustomerGroupsFromExcludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(CustomerGroupExcludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindCustomerGroupExcludeGrid(Collection<ICustomerGroup> customerGroups)
		{
			CustomerGroupExcludeGrid.DataSource = customerGroups;
			CustomerGroupExcludeGrid.DataBind();

			CustomerGroupExcludeGridUpdatePanel.Update();
		}

		protected virtual void DataBindCustomerGroupExcludeSearchGrid()
		{
			CustomerGroupExcludeSearchGrid.DataSource = CustomerGroupCollection;
			CustomerGroupExcludeSearchGrid.DataBind();

			CustomerGroupExcludePopupUpdatePanel.Update();
		}
	}
}