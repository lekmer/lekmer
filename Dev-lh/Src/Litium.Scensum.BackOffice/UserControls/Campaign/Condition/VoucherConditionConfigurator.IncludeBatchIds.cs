using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Resources;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class VoucherConditionConfigurator
	{
		protected virtual void SetEventHandlersInclude()
		{
			BatchIncludeGrid.RowDataBound += OnBatchIncludeGridRowDataBound;
			BatchIncludeGrid.RowCommand += OnBatchIncludeGridRowCommand;
			OpenBatchIncludeSearchPopupButton.Click += OpenBatchIncludeSearchPopupButtonOnClick;

			RemoveSelectionFromBatchIncludeGridButton.Click += OnRemoveSelectionFromBatchIncludeGrid;
			BatchIncludeSaveButton.Click += BatchIncludeSaveButtonOnClick;

			IncludeAllBatchesCheckbox.CheckedChanged += OnIncludeAllBatchesCheckboxChanged;
		}


		protected virtual void BatchIncludeSaveButtonOnClick(object sender, EventArgs e)
		{
			int batchId = int.Parse(BatchIdIncludeTextBox.Text.Trim(), CultureInfo.CurrentCulture);
			foreach (int id in State.BatchIdsInclude)
			{
				if (id == batchId)
				{
					BatchIncludeMessageContainer.Add(LekmerMessage.ExistBatchId);
					BatchIncludePopup.Show();
					return;
				}
			}
			BatchIdIncludeTextBox.Text = string.Empty;
			State.BatchIdsInclude.Add(batchId);
			DataBindBatchIncludeGrid(State.BatchIdsInclude);
			BatchIncludePopup.Hide();
		}

		protected virtual void OpenBatchIncludeSearchPopupButtonOnClick(object sender, EventArgs e)
		{
			BatchIncludeUpdatePanel.Update();
			BatchIncludePopup.Show();
		}

		protected virtual void OnRemoveSelectionFromBatchIncludeGrid(object sender, EventArgs e)
		{
			foreach (var batchId in GetSelectedBatchsFromIncludeGrid())
			{
				State.BatchIdsInclude.Remove(batchId);
			}

			DataBindBatchIncludeGrid(State.BatchIdsInclude);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
		protected virtual void OnBatchIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveBatchInclude"))
			{
				RemoveBatchInclude(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OnBatchIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, BatchIncludeApplyToAllSelectedDiv, true);
		}

		protected virtual void OnIncludeAllBatchesCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllBatchesCheckbox.Checked)
			{
				DataBindBatchIncludeGrid(new Collection<int>());
			}
			else
			{
				DataBindBatchIncludeGrid(State.BatchIdsInclude);
			}
		}


		protected virtual void RemoveBatchInclude(int batchId)
		{
			State.BatchIdsInclude.Remove(batchId);
			DataBindBatchIncludeGrid(State.BatchIdsInclude);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedBatchsFromIncludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(BatchIncludeGrid, "BatchIdHiddenField");
		}

		protected virtual void DataBindBatchIncludeGrid(Collection<int> batchIds)
		{
			BatchIncludeGrid.DataSource = batchIds;
			BatchIncludeGrid.DataBind();
			BatchIncludeGridUpdatePanel.Update();
		}

		protected virtual void SetVisibilityInclude()
		{
			BatchIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BatchIncludeGrid) ? "block" : "none";
		}

		protected virtual void RegisterCollapsibleIncludePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
		}
	}
}