using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.UserControls.Tree;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using CustomButton = Litium.Scensum.Web.Controls.Button.ImageLinkButton;
using GridControl = System.Web.UI.WebControls.GridView;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Helper
{
	public class CampaignConfiguratorCategory
	{
		private Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		private CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		public GridControl Grid { get; set; }
		public CustomButton OkButton { get; set; }
		public CustomButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public SelectTreeView PopupCategoryTree { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public CampaignCategoryDictionary Categories { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;

			OkButton.Click += OkButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(CampaignCategoryDictionary categories)
		{
			PopupCategoryTree.Selector = CategorySelector;

			foreach (int value in categories.Keys)
			{
				PopupCategoryTree.SelectedIds.Add(value);
			}

			PopupCategoryTree.DataBind();

			DataBindGrid(CampaignConfiguratorHelper.ResolveCategories(categories));
		}

		public virtual void DataBindGrid(Collection<ICategory> categories)
		{
			Grid.DataSource = categories;
			Grid.DataBind();

			GridUpdatePanel.Update();
		}

		public void OnSubCategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox)sender;
			var row = (GridViewRow)checkBox.Parent.Parent;
			var idHidden = (HiddenField)row.FindControl("IdHiddenField");

			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);

			bool include;
			if (!Categories.TryGetValue(categoryId, out include)) return;

			Categories[categoryId] = checkBox.Checked;
			DataBindGrid(CampaignConfiguratorHelper.ResolveCategories(Categories));
		}

		public string GetCategoryPath(object cat)
		{
			var category = cat as ICategory;
			if (category == null) return string.Empty;

			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = RetrieveCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ApplyToAllDiv, false);
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				RemoveCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			foreach (int categoryId in PopupCategoryTree.SelectedIds)
			{
				if (!Categories.ContainsKey(categoryId))
				{
					Categories.Add(categoryId);
				}
			}

			DataBindGrid(CampaignConfiguratorHelper.ResolveCategories(Categories));

			if (IncludeAllCheckbox != null && Categories.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			foreach (var categoryId in GetSelectedFromGrid())
			{
				Categories.Remove(categoryId);
			}

			DataBindGrid(CampaignConfiguratorHelper.ResolveCategories(Categories));
		}

		protected virtual void RemoveCategory(int categoryId)
		{
			Categories.Remove(categoryId);
			DataBindGrid(CampaignConfiguratorHelper.ResolveCategories(Categories));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		protected virtual ICategory RetrieveCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}
	}
}