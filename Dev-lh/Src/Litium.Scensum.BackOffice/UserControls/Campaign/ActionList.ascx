﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActionList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.ActionList" %>
<script type="text/javascript">
    function confirmActionDelete() {
    	return DeleteConfirmation("<%= Resources.Campaign.Literal_action %>");
    }
</script>
<div class="campaign-action-grid">
	<asp:GridView ID="ActionsGrid" SkinID="campaign-actions-grid" AutoGenerateColumns="false" runat="server" Width="100%" CellPadding="0" BorderWidth="0">
		<Columns>
			<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>">
				<ItemTemplate>
					<asp:LinkButton ID="EditButton" CommandName="Configure" runat="server"></asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Description %>">
				<ItemTemplate>
					<asp:Literal ID="DescriptionLiteral" runat="server"></asp:Literal>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px">
				<HeaderTemplate>
					<div class="inline">
						<%= Resources.General.Literal_SortOrder %>
						<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
					</div>
				</HeaderTemplate>
				<ItemTemplate>
					<div class="inline">
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Guid") %>' runat="server" />
						<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
						<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
						<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%#Eval("Ordinal") %>' Width="50%" />
						<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Guid") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
						<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Guid") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
					</div>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-HorizontalAlign="Right" ItemStyle-Width="15px" >
				<ItemTemplate>
					<asp:ImageButton ID="RemoveButton" runat="server" CommandName="Remove" ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmActionDelete();" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
</div>