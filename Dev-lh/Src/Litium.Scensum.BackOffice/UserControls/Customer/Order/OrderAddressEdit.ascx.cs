﻿using System;
using Litium.Lekmer.Order;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using System.Globalization;
using Convert=System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Order
{
	public partial class OrderAddressEdit : StateUserControlController<IOrderAddress>
	{
		public OrderAddressEdit()
		{
			State = IoC.Resolve<IOrderAddressSecureService>().Create();
		}

		public bool EnableEditing { get; set; }

		public IOrderAddress OrderAddress
		{
			get
			{
				return State;
			}
			set
			{
				State = value;
			}
		}

		private IOrderFull _orderFull;
		public IOrderFull OrderFull
		{
			get
			{
				return _orderFull;
			}
			set
			{
				_orderFull = value;
			}
		}

		public void REBind()
		{
			PopulateControl();
		}

		public event EventHandler EditCompleted;

		protected override void SetEventHandlers()
		{
			EditAddressLink.Click += EditAddressLink_Click;
			OkButton.Click += OkButton_Click;
			CancelButton.Click += CancelButton_Click;
		}

		protected void EditAddressLink_Click(object sender, EventArgs e)
		{
			PopulatePopup();
			AddressPopup.Show();
		}

		protected void CancelButton_Click(object sender, EventArgs e)
		{
			if (null != EditCompleted)
			{
				EditCompleted(this, new EventArgs());
			}
		}

		protected void OkButton_Click(object sender, EventArgs e)
		{
			if (IoC.Resolve<IOrderAddressSecureService>().CanEdit(OrderFull, State))
			{
				SaveToState();
				if (null != EditCompleted)
				{
					EditCompleted(this, new EventArgs());
				}
			}
			else
			{
				SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditAddress);
				AddressPopup.Show();
			}
		}

		private void SaveToState()
		{
			AddresseeLabel.Text = State.Addressee = AddresseeEditTextBox.Text;
			StreetAddressLiteral.Text = State.StreetAddress = AddressTextBox.Text;
			PostalCodeLabel.Text = State.PostalCode = ZipteTextBox.Text;
			CityLabel.Text = State.City = CityTextBox.Text;
			PhoneLabel.Text = State.PhoneNumber = PhoneNumberTextBox.Text;
			State.StreetAddress2 = Address2TextBox.Text;
			State.CountryId = Convert.ToInt32(Country.SelectedValue, CultureInfo.InvariantCulture);
			CountryLabel.Text = Country.SelectedItem.Text;

			var lekmerState = State as ILekmerOrderAddress;
			if (lekmerState != null)
			{
				HouseNumberLabel.Text = lekmerState.HouseNumber = HouseNumberTextBox.Text.NullWhenEmpty();
				HouseExtensionLabel.Text = lekmerState.HouseExtension = HouseExtensionTextBox.Text.NullWhenEmpty();
				ReferenceLabel.Text = lekmerState.Reference = ReferenceTextBox.Text.NullWhenEmpty();
				DoorCodeTextBox.Text = lekmerState.DoorCode = DoorCodeTextBox.Text.NullWhenEmpty();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.String.Format(System.String,System.Object)")]
		protected override void PopulateControl()
		{
			if (State == null) return;

			PopulatePopup();

			AddresseeLabel.Text = State.Addressee;
			StreetAddressLiteral.Text = State.StreetAddress;
			CityLabel.Text = State.City;
			PostalCodeLabel.Text = State.PostalCode;
			PhoneLabel.Text = State.PhoneNumber;
			if (State.CountryId > 0)
			{
				CountryLabel.Text = IoC.Resolve<ICountrySecureService>().GetById(State.CountryId).Title;
			}

			var lekmerState = State as ILekmerOrderAddress;
			if (lekmerState != null)
			{
				HouseNumberLabel.Text = lekmerState.HouseNumber;
				HouseExtensionLabel.Text = lekmerState.HouseExtension;
				ReferenceLabel.Text = lekmerState.Reference;
			}

			EditAddressLink.Visible = EnableEditing;
			ValidationSummary.ValidationGroup =
				AddresseeValidator.ValidationGroup =
				AddressValidator.ValidationGroup =
				ZipValidator.ValidationGroup =
				CityValidator.ValidationGroup =
				OkButton.ValidationGroup =
				string.Format(CultureInfo.CurrentCulture, "{0}_ValidationGroup", ClientID);
		}

		private void PopulatePopup()
		{
			Country.DataSource = IoC.Resolve<ICountrySecureService>().GetAll();
			Country.DataBind();

			AddresseeEditTextBox.Text = State.Addressee;
			AddressTextBox.Text = State.StreetAddress;
			ZipteTextBox.Text = State.PostalCode;
			CityTextBox.Text = State.City;
			PhoneNumberTextBox.Text = State.PhoneNumber;
			Address2TextBox.Text = State.StreetAddress2;
			if (State.CountryId > 0)
			{
				Country.SelectedValue = State.CountryId.ToString(CultureInfo.InvariantCulture);
			}

			var lekmerState = State as ILekmerOrderAddress;
			if (lekmerState != null)
			{
				HouseNumberTextBox.Text = lekmerState.HouseNumber;
				HouseExtensionTextBox.Text = lekmerState.HouseExtension;
				ReferenceTextBox.Text = lekmerState.Reference;
				DoorCodeTextBox.Text = lekmerState.DoorCode;
			}
		}
	}
}