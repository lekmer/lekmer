﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Customer;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	[SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
	public partial class CustomerAddresses : StateUserControlController<Collection<IAddress>>
	{
		private const string EditAddresssCommand = "EditAddress";
		private const string CreateAddresssCommand = "CreateAddress";
		private const string DeleteAddresssCommand = "DeleteAddress";

		public event EventHandler Changed;
		public event EventHandler<CustomerCannotDoEventArgs> CannotDo;

		private ICustomer _customer;
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly")]
		public ICustomer Customer
		{
			set
			{
				ViewState["Customer"] = _customer = value;
			}
			get
			{
				if (null != _customer)
				{
					return _customer;
				}
				_customer = (ICustomer)ViewState["Customer"];
				return _customer;
			}
		}

		protected override void SetEventHandlers()
		{
			AddressGrid.RowDataBound += AddressGridRowDataBound;
			AddressGrid.RowCommand += AddressGridRowCommand;

			SaveButton.Click += SaveButton_Click;
			CancelButton.Click += CancelButton_Click;

			AddNewAdressButon.Click += AddNewAddressButton_Click;
			DeleteSelectedButton.Click += DeleteSelectedButton_Click;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public Collection<IAddress> GetAddresses()
		{
			return State;
		}

		protected void DeleteSelectedButton_Click(object sender, EventArgs e)
		{
			var idsToDelete = AddressGrid.GetSelectedIds();

			foreach (var id in idsToDelete)//Check if we can delete all selected addresses
			{
				IAddress address = State.Single(item => item.Id == id);
				if (!IoC.Resolve<IAddressSecureService>().CanDelete(Customer, address))
				{
					if (null != CannotDo)
					{
						CannotDo(this, new CustomerCannotDoEventArgs(CustomerCannotDoWhat.Delete));
					}
					return;
				}
			}

			DeleteSelectedAddresses(idsToDelete);

			AddressGrid.DataSource = State.Where(item => !item.IsDeleted);
			AddressGrid.DataBind();

			if (null != Changed)
				Changed(this, new EventArgs());
		}

		protected void AddNewAddressButton_Click(object sender, EventArgs e)
		{
			if (IoC.Resolve<IAddressSecureService>().CanAdd(Customer))
			{
				OpenAddressEdit(null);
			}
			else
			{
				if (null != CannotDo)
				{
					CannotDo(this, new CustomerCannotDoEventArgs(CustomerCannotDoWhat.Add));
				}
			}
		}

		protected void CancelButton_Click(object sender, EventArgs e)
		{
			AddressPopup.Hide();
		}

		protected void SaveButton_Click(object sender, EventArgs e)
		{
			var saveButton = ((ImageLinkButton) sender);

			if (saveButton.CommandName == EditAddresssCommand)
			{
				IAddress address =
					State.Where(
						item => string.Equals(item.Id.ToString(CultureInfo.InvariantCulture), saveButton.CommandArgument))
						.SingleOrDefault();

				if (IoC.Resolve<IAddressSecureService>().CanEdit(Customer, address))
				{
					FillAddress(address);
				}
				else
				{
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditAddress);
					AddressPopup.Show();
					//if (null != CannotDo)
					//{
					//    CannotDo(this, new CustomerCannotDoEventArgs(CustomerCannotDoWhat.Edit));
					//}
					return;
				}

			}
			else if (saveButton.CommandName == CreateAddresssCommand)
			{
				IAddress address = IoC.Resolve<IAddressSecureService>().Create();

				if (State.Where(item => item.Id <= 0).Count() > 0) //there already are new addresses
				{
					address.Id = State.Min(item => item.Id) - 1;
				}

				FillAddress(address);

				State.Add(address);
			}

			AddressGrid.DataSource = State.Where(item => !item.IsDeleted);
			AddressGrid.DataBind();

			if (null != Changed)
				Changed(this, new EventArgs());

			AddressPopup.Hide();
		}

		private void FillAddress(IAddress address)
		{
			address.Addressee = AddresseeEditTextBox.Text;
			address.StreetAddress = AddressTextBox.Text;
			address.PostalCode = ZipteTextBox.Text;
			address.City = CityTextBox.Text;
			address.StreetAddress2 = Address2TextBox.Text;
			address.PhoneNumber = PhoneNumberTextBox.Text;
			address.AddressTypeId = IoC.Resolve<IAddressTypeSecureService>().GetByCommonName("DeliveryAddress").Id;
			address.CountryId = int.Parse(Country.SelectedValue, CultureInfo.InvariantCulture);

			var lekmerAddress = address as ILekmerAddress;
			if (lekmerAddress != null)
			{
				lekmerAddress.HouseNumber = HouseNumberTextBox.Text.NullWhenEmpty();
				lekmerAddress.HouseExtension = HouseExtensionTextBox.Text.NullWhenEmpty();
				lekmerAddress.Reference = ReferenceTextBox.Text.NullWhenEmpty();
				lekmerAddress.DoorCode = DoorCodeTextBox.Text.NullWhenEmpty();
			}
		}

		protected override void PopulateControl()
		{
			BindControlData();
		}

		public void BindControlData()
		{
			AddressGrid.DataSource = State = IoC.Resolve<IAddressSecureService>().GetAllByCustomer(Customer.Id);
			AddressGrid.DataBind();

			Country.DataSource = IoC.Resolve<ICountrySecureService>().GetAll();
			Country.DataBind();
		}

		protected virtual void AddressGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row);

			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var address = (IAddress)row.DataItem;

			var addressTypeLink = (LinkButton)e.Row.FindControl("AddressViewButton");
			if (addressTypeLink != null)
			{
				addressTypeLink.Text = IoC.Resolve<IAddressTypeSecureService>().GetById(address.AddressTypeId).Title;
			}

			var labelCountry = (Label)e.Row.FindControl("CountryLabel");
			if (labelCountry == null) return;
			labelCountry.Text = IoC.Resolve<ICountrySecureService>().GetById(address.CountryId).Title;
		}

		protected virtual void AddressGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			switch (e.CommandName)
			{
				case CreateAddresssCommand:
					OpenAddressEdit(null);
					break;

				case EditAddresssCommand:
					OpenAddressEdit(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
					break;

				case DeleteAddresssCommand:
					DeleteAddress(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
					break;
			}
		}

		private void DeleteAddress(int addressId)
		{
			//Check if we can delete all selected addresses
			IAddress address = State.Single(item => item.Id == addressId);
			if (!IoC.Resolve<IAddressSecureService>().CanDelete(Customer, address))
			{
				if (null != CannotDo)
				{
					CannotDo(this, new CustomerCannotDoEventArgs(CustomerCannotDoWhat.Delete));
				}
				return;
			}

			address.Status = BusinessObjectStatus.Deleted;

			AddressGrid.DataSource = State.Where(item => !item.IsDeleted);
			AddressGrid.DataBind();

			if (null != Changed)
				Changed(this, new EventArgs());
		}

		private void DeleteSelectedAddresses(IEnumerable<int> selectedIds)
		{
			foreach (var idToDelete in selectedIds)
			{
				State.Single(item => item.Id == idToDelete).Status = BusinessObjectStatus.Deleted;
			}
		}

		protected void OpenAddressEdit(int? addressId)
		{
			if (addressId.HasValue)
			{
				IAddress address = State.FirstOrDefault(a => a.Id == addressId);
				AddresseeEditTextBox.Text = address.Addressee;
				AddressTextBox.Text = address.StreetAddress;
				ZipteTextBox.Text = address.PostalCode;
				CityTextBox.Text = address.City;
				Address2TextBox.Text = address.StreetAddress2;
				PhoneNumberTextBox.Text = address.PhoneNumber;
				Country.SelectedValue = address.CountryId.ToString(CultureInfo.InvariantCulture);

				var lekmerAddress = address as ILekmerAddress;
				if (lekmerAddress != null)
				{
					HouseNumberTextBox.Text = lekmerAddress.HouseNumber;
					HouseExtensionTextBox.Text = lekmerAddress.HouseExtension;
					ReferenceTextBox.Text = lekmerAddress.Reference;
					DoorCodeTextBox.Text = lekmerAddress.DoorCode;
				}

				SaveButton.CommandName = EditAddresssCommand;
				SaveButton.CommandArgument = address.Id.ToString(CultureInfo.InvariantCulture);
			}
			else
			{
				AddresseeEditTextBox.Text = string.Empty;
				AddressTextBox.Text = string.Empty;
				ZipteTextBox.Text = string.Empty;
				CityTextBox.Text = string.Empty;
				Address2TextBox.Text = string.Empty;
				PhoneNumberTextBox.Text = string.Empty;
				HouseNumberTextBox.Text = string.Empty;
				HouseExtensionTextBox.Text = string.Empty;
				ReferenceTextBox.Text = string.Empty;
				DoorCodeTextBox.Text = string.Empty;
				SaveButton.CommandName = CreateAddresssCommand;
			}
			AddressPopup.Show();
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}
	}

	public class CustomerCannotDoEventArgs : EventArgs
	{
		public CustomerCannotDoEventArgs(CustomerCannotDoWhat cannotDo)
		{
			CustomerCannot = cannotDo;
		}

		public CustomerCannotDoWhat CustomerCannot { get; set; }
	}

	public enum CustomerCannotDoWhat
	{
		Add,
		Edit,
		Delete
	}
}