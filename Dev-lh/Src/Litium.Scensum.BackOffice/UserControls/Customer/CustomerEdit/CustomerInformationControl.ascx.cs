﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Common;
using Litium.Lekmer.Customer;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	public partial class CustomerInformationControl : StateUserControlController<ILekmerCustomerInformation>
	{
		public string ValidationGroup;
		private ICustomer _customer;

		public ICustomer Customer
		{
			set
			{
				ViewState["Customer"] = _customer = value;
			}
			get
			{
				if (_customer != null)
				{
					return _customer;
				}
				return ViewState["Customer"] as ICustomer;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			rfvFirstName.ValidationGroup = ValidationGroup;
		}

		protected override void SetEventHandlers()
		{
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.DateTime.ToString(System.String)")]
		protected override void PopulateControl()
		{
			if (Customer != null)
			{
				var customer = (ILekmerCustomerInformation)Customer.CustomerInformation;
				State = customer;
				FirstNameLiteral.Text = customer.IsCompany
					? Resources.Lekmer.Customer_CompanyName
					: Resources.Customer.Literal_FirstName;
				FirstNameTextBox.Text = Customer.CustomerInformation.FirstName;
				LastNameLiteral.Text = customer.IsCompany
					? Resources.Lekmer.Customer_CompanyFullName
					: Resources.Customer.Literal_LastName;
				LastNameTextBox.Text = Customer.CustomerInformation.LastName;
				CivicNumberLiteral.Text = customer.IsCompany 
					? Resources.Lekmer.Customer_OrganizationNumber 
					: Resources.Customer.Literal_CivicNumber;
				CivicNumberTextBox.Text = Customer.CustomerInformation.CivicNumber;
				EmailTextBox.Text = Customer.CustomerInformation.Email;
				PhoneHomeTextBox.Text = Customer.CustomerInformation.PhoneNumber;
				PhoneMobileTextBox.Text = Customer.CustomerInformation.CellPhoneNumber;
				DateCreatedLabel.Text = Customer.CustomerInformation.CreatedDate.ToString("yyyy-MM-dd HH:mm", CultureInfo.CurrentCulture);
				ICollection<IAddress> addressList = IoC.Resolve<IAddressSecureService>().GetAllByCustomer(Customer.Id);
				BindAddresses(Customer, addressList);
				BindGenderTypes(Customer.CustomerInformation);
			}
			else
			{
				State = (ILekmerCustomerInformation)IoC.Resolve<ICustomerInformationSecureService>().Create();
			}
		}

		public void BindAddresses(ICustomer customer, IEnumerable<IAddress> addressList)
		{
			Collection<IAddress> addresses = new Collection<IAddress>();
			foreach (IAddress address in addressList)
			{
				string streetAddress = address.StreetAddress + (string.IsNullOrEmpty(address.StreetAddress2) ? String.Empty : ", " + address.StreetAddress2);
				addresses.Add(new Address { Id = address.Id, StreetAddress = streetAddress });
			}
			ListItem listItem = new ListItem(Resources.Customer.Literal_ChooseAddress, string.Empty);
			DeliveryAddressList.DataSource = addresses;
			DeliveryAddressList.DataBind();
			DeliveryAddressList.Items.Insert(0, listItem);
			if (customer != null && customer.CustomerInformation.DefaultDeliveryAddressId.HasValue)
			{
				ListItem selectedItem = DeliveryAddressList.Items.FindByValue(
					customer.CustomerInformation.DefaultDeliveryAddressId.Value.ToString(CultureInfo.CurrentCulture));
				if (null != selectedItem)
				{
					selectedItem.Selected = true;
				}
			}
			BillingAddressList.DataSource = addresses;
			BillingAddressList.DataBind();
			BillingAddressList.Items.Insert(0, listItem);
			if (customer != null && customer.CustomerInformation.DefaultBillingAddressId.HasValue)
			{
				ListItem selectedItem = BillingAddressList.Items.FindByValue(
					customer.CustomerInformation.DefaultBillingAddressId.Value.ToString(CultureInfo.CurrentCulture));
				if (null != selectedItem)
				{
					selectedItem.Selected = true;
				}
			}
			ILekmerCustomerInformation info = customer != null
			                                  	? customer.CustomerInformation as ILekmerCustomerInformation
			                                  	: null;
			AlternateAddressList.DataSource = addresses;
			AlternateAddressList.DataBind();
			AlternateAddressList.Items.Insert(0, listItem);
			if (info != null && info.AlternateAddressId.HasValue)
			{
				ListItem selectedItem = AlternateAddressList.Items.FindByValue(info.AlternateAddressId.Value.ToString(CultureInfo.CurrentCulture));
				if (null != selectedItem)
				{
					selectedItem.Selected = true;
				}
			}
		}

		public void BindGenderTypes(ICustomerInformation customerInformation)
		{
			GenderList.DataSource = IoC.Resolve<IGenderTypeSecureService>().GetAll();
			GenderList.DataBind();

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				ListItem selectedItem = GenderList.Items.FindByValue(lekmerCustomerInformation.GenderTypeId.ToString(CultureInfo.CurrentCulture));
				if (selectedItem != null)
				{
					selectedItem.Selected = true;
				}
			}
		}

		public ICustomerInformation GetCustomerInformation()
		{
			State.FirstName = FirstNameTextBox.Text.Trim();
			State.LastName = LastNameTextBox.Text.Trim();
			State.CivicNumber = CivicNumberTextBox.Text.Trim();
			State.Email = EmailTextBox.Text.Trim();
			State.PhoneNumber = PhoneHomeTextBox.Text.Trim();
			State.CellPhoneNumber = PhoneMobileTextBox.Text.Trim();

			int addressId;
			State.DefaultBillingAddressId = int.TryParse(BillingAddressList.SelectedValue, out addressId)
											? (int?)addressId
											: null;
			State.DefaultDeliveryAddressId = int.TryParse(DeliveryAddressList.SelectedValue, out addressId)
											? (int?)addressId
											: null;
			State.AlternateAddressId = int.TryParse(AlternateAddressList.SelectedValue, out addressId)
											? (int?)addressId
											: null;

			int genderTypeId;
			State.GenderTypeId = int.TryParse(GenderList.SelectedValue, out genderTypeId)
				? genderTypeId
				: (int)GenderTypeInfo.Unknown;

			return State;
		}
	}
}
