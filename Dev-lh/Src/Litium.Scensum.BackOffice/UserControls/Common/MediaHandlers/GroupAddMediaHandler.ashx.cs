﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Media;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Modules.Media.MediaFolder;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Common.MediaHandlers
{
	public class GroupAddMediaHandler : IHttpHandler
	{
		private ISystemUserSecureService _systemUserService;
		public ISystemUserSecureService SystemUserService
		{
			get { return _systemUserService ?? (_systemUserService = IoC.Resolve<ISystemUserSecureService>()); }
		}

		private IMediaItemSecureService _mediaItemService;
		public IMediaItemSecureService MediaItemService
		{
			get { return _mediaItemService ?? (_mediaItemService = IoC.Resolve<IMediaItemSecureService>()); }
		}

		private IMediaFormatSecureService _mediaFormatService;
		public IMediaFormatSecureService MediaFormatService
		{
			get { return _mediaFormatService ?? (_mediaFormatService = IoC.Resolve<IMediaFormatSecureService>()); }
		}

		private ILekmerMediaFolderSecureService _mediaFolderService;
		public ILekmerMediaFolderSecureService MediaFolderService
		{
			get { return _mediaFolderService ?? (_mediaFolderService = IoC.Resolve<ILekmerMediaFolderSecureService>()); }
		}

		private ISliderGroupSecureService _sliderGroupService;
		public ISliderGroupSecureService SliderGroupService
		{
			get { return _sliderGroupService ?? (_sliderGroupService = IoC.Resolve<ISliderGroupSecureService>()); }
		}

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			if (context.Request.Files.Count > 0)
			{
				var blockId = Int32.Parse(context.Request["blockId"]);
				var groupId = Int32.Parse(context.Request["groupId"]);
				var imageType = context.Request["imageType"];
				var userName = context.Request["userName"];
				var file = context.Request.Files[0];

				var extension = Path.GetExtension(file.FileName).TrimStart(new[] { '.' });
				if (string.IsNullOrEmpty(extension))
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}

				IMediaFormat mediaFormat = MediaFormatService.GetByExtension(extension);
				if (mediaFormat == null)
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}

				var folder = MediaFolderHepler.GetFolderByType(MediaFolderService, MediaFolderType.Banners);
				if (null != folder)
				{
					var systemUser = SystemUserService.GetFullByUserName(userName);
					var fileId = MediaItemService.Insert(systemUser, file.InputStream, folder.Id, file.FileName);
					file.InputStream.Close();

					var sliderGroup = SliderGroupService.GetByBlockId(blockId).FirstOrDefault(t => t.SliderGroupId == groupId);
					if (sliderGroup != null)
					{
						if (imageType == "main1")
						{
							sliderGroup.ImageId1 = fileId;
						}
						else if (imageType == "main2")
						{
							sliderGroup.ImageId2 = fileId;
						}
						else if (imageType == "thumbnail1")
						{
							sliderGroup.ThumbnailImageId1 = fileId;
						}
						else if (imageType == "thumbnail2")
						{
							sliderGroup.ThumbnailImageId2 = fileId;
						}

						SliderGroupService.Save(systemUser, sliderGroup);
					}
				}
			}

			context.Response.ContentType = "application/json";
			context.Response.Write(new JavaScriptSerializer().Serialize("success"));
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}