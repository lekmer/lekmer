﻿using System;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Common.MediaHandlers
{
	public class GroupLoadMediaHandler : IHttpHandler
	{
		private ISystemUserSecureService _systemUserService;
		public ISystemUserSecureService SystemUserService
		{
			get { return _systemUserService ?? (_systemUserService = IoC.Resolve<ISystemUserSecureService>()); }
		}

		private IImageSecureService _imageService;
		public IImageSecureService ImageService
		{
			get { return _imageService ?? (_imageService = IoC.Resolve<IImageSecureService>()); }
		}

		private ISliderGroupSecureService _sliderGroupService;
		public ISliderGroupSecureService SliderGroupService
		{
			get { return _sliderGroupService ?? (_sliderGroupService = IoC.Resolve<ISliderGroupSecureService>()); }
		}

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var blockId = Int32.Parse(context.Request["blockId"]);
			var sliderGroups = SliderGroupService.GetByBlockId(blockId);

			var result = new Collection<SliderGroupImage>();
			foreach (var group in sliderGroups)
			{
				var image1Id = group.ImageId1;
				result.Add(new SliderGroupImage
				{
					Type = "main1",
					GroupId = group.SliderGroupId,
					Url = image1Id.HasValue ? ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image1Id.Value, ImageService.GetById(image1Id.Value).FormatExtension)) : string.Empty
				});

				var thumb1Id = group.ThumbnailImageId1;
				result.Add(new SliderGroupImage
				{
					Type = "thumbnail1",
					GroupId = group.SliderGroupId,
					Url = thumb1Id.HasValue ? ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(thumb1Id.Value, ImageService.GetById(thumb1Id.Value).FormatExtension)) : string.Empty
				});

				var image2Id = group.ImageId2;
				result.Add(new SliderGroupImage
				{
					Type = "main2",
					GroupId = group.SliderGroupId,
					Url = image2Id.HasValue ? ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image2Id.Value, ImageService.GetById(image2Id.Value).FormatExtension)) : string.Empty
				});

				var thumb2Id = group.ThumbnailImageId2;
				result.Add(new SliderGroupImage
				{
					Type = "thumbnail2",
					GroupId = group.SliderGroupId,
					Url = thumb2Id.HasValue ? ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(thumb2Id.Value, ImageService.GetById(thumb2Id.Value).FormatExtension)) : string.Empty
				});
			}
		
			context.Response.ContentType = "application/json";
			context.Response.Write(new JavaScriptSerializer().Serialize(result));
		}

		public static string ResolveUrl(string originalUrl)
		{
			if (originalUrl == null)
				return null;

			if (originalUrl.IndexOf("://") != -1)
				return originalUrl;

			if (originalUrl.StartsWith("~"))
			{
				if (HttpContext.Current != null)
				{
					return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host + originalUrl.Substring(1).Replace("\\", "/");
				}
				throw new ArgumentException("Invalid URL: Relative URL not allowed.");
			}

			return originalUrl;
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}