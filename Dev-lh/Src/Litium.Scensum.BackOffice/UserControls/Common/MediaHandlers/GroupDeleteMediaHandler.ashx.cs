﻿using System;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Common.MediaHandlers
{
	public class GroupDeleteMediaHandler : IHttpHandler
	{
		private ISystemUserSecureService _systemUserService;
		public ISystemUserSecureService SystemUserService
		{
			get { return _systemUserService ?? (_systemUserService = IoC.Resolve<ISystemUserSecureService>()); }
		}

		private ISliderGroupSecureService _sliderGroupService;
		public ISliderGroupSecureService SliderGroupService
		{
			get { return _sliderGroupService ?? (_sliderGroupService = IoC.Resolve<ISliderGroupSecureService>()); }
		}

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var blockId = Int32.Parse(context.Request["blockId"]);
			var groupId = Int32.Parse(context.Request["groupId"]);
			var imageType = context.Request["imageType"];
			var userName = context.Request["userName"];

			var systemUser = SystemUserService.GetFullByUserName(userName);

			var sliderGroup = SliderGroupService.GetByBlockId(blockId).FirstOrDefault(t => t.SliderGroupId == groupId);
			if (sliderGroup != null)
			{
				if (imageType == "main1")
				{
					sliderGroup.ImageId1 = null;
				}
				else if (imageType == "main2")
				{
					sliderGroup.ImageId2 = null;
				}
				else if (imageType == "thumbnail1")
				{
					sliderGroup.ThumbnailImageId1 = null;
				}
				else if (imageType == "thumbnail2")
				{
					sliderGroup.ThumbnailImageId2 = null;
				}

				SliderGroupService.Save(systemUser, sliderGroup);
			}

			context.Response.ContentType = "application/json";
			context.Response.Write(new JavaScriptSerializer().Serialize("success"));
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}