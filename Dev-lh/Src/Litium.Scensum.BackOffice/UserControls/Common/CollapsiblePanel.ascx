﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CollapsiblePanel.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Common.CollapsiblePanel" %>

<div class="collapsible-items left">
	<div id="CollapsibleItemDiv" runat="server" class="collapsible-item left">
		<div class="collapsible-item-header left">
			<uc:LiteralEncoded ID="TitleLiteral" runat="server" />
		</div>
		<div id="ActionBlockDiv" class="action-block left" runat="server">
			<div class="left">
				<span class="collapsible-caption"><asp:Literal runat="server" ID="ActionCaptionLiteral" /></span>
			</div>
			<div class="link-button">
				<asp:LinkButton ID="ActionButton" runat="server">
					<asp:Image runat="server" ID="ActionButtonImage" AlternateText="" />
					<asp:Literal runat="server" ID="ActionButtonCaptionLiteral" />
				</asp:LinkButton>
			</div>
		</div>
		<div class="image-block right">
			<asp:HiddenField ID="CommandArgumentHidden" runat="server" />
			<asp:HiddenField ID="IsOpenedHidden" runat="server" Value="true" />
			<asp:Image ID="IndicatorImage" runat="server" CssClass="right" />
		</div>
	</div>
</div>