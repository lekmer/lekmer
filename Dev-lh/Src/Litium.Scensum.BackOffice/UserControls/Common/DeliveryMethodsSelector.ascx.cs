﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Order;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Web.Controls.Exceptions;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Common
{
	public partial class DeliveryMethodsSelector : UserControl
	{
		protected class RepeaterDataSourceItem
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public bool IsSelected { get; set; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual IdDictionary DataSource { get; set; }

		public override void DataBind()
		{
			if (DataSource == null)
			{
				throw new ControlIntegrationException("DataSource shoule be initialized before calling DataBind().");
			}

			base.DataBind();

			var sourceItems = new Collection<RepeaterDataSourceItem>();

			var uniqueDeliveryMethods = new IdDictionary();
			Collection<IDeliveryMethod> deliveryMethods = IoC.Resolve<ILekmerDeliveryMethodSecureService>().GetAll();

			foreach (var deliveryMethod in deliveryMethods)
			{
				if (uniqueDeliveryMethods.ContainsKey(deliveryMethod.Id))
				{
					continue;
				}

				uniqueDeliveryMethods.Add(deliveryMethod.Id);

				var item = new RepeaterDataSourceItem
				{
					Id = deliveryMethod.Id,
					Title = deliveryMethod.Title,
					IsSelected = DataSource.ContainsKey(deliveryMethod.Id)
				};

				sourceItems.Add(item);
			}

			DeliveryMethodsRepeater.DataSource = sourceItems;
			DeliveryMethodsRepeater.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "0#")]
		public virtual bool IsValid(out Collection<string> validationMessages)
		{
			validationMessages = new Collection<string>();

			var selectedValues = CollectSelectedValues();

			if (selectedValues.Count == 0)
			{
				validationMessages.Add(Resources.CampaignMessage.DeliveryMethodEmptySelection);
				return false;
			}

			var uniqueDeliveryMethods = new IdDictionary();
			Collection<IDeliveryMethod> deliveryMethods = IoC.Resolve<ILekmerDeliveryMethodSecureService>().GetAll();

			foreach (var deliveryMethod in deliveryMethods)
			{
				if (uniqueDeliveryMethods.ContainsKey(deliveryMethod.Id) == false)
				{
					uniqueDeliveryMethods.Add(deliveryMethod.Id);
				}
			}

			if (selectedValues.Any(selectedValue => uniqueDeliveryMethods.ContainsKey(selectedValue.Key) == false))
			{
				validationMessages.Add(Resources.CampaignMessage.DeliveryMethodCorruptedSelection);
				return false;
			}

			return true;
		}

		public virtual IdDictionary CollectSelectedValues()
		{
			var ids = new IdDictionary();

			foreach (RepeaterItem repeaterItem in DeliveryMethodsRepeater.Items)
			{
				var deliveryMethodCheckbox = (CheckBox)repeaterItem.FindControl("DeliveryMethodCheckbox");
				if (deliveryMethodCheckbox.Checked)
				{
					int id = Convert.ToInt32(((HiddenField)repeaterItem.FindControl("DeliveryMethodIdHidden")).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}

			return ids;
		}
	}
}