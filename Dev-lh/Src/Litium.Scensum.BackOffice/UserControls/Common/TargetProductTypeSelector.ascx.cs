﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Exceptions;
using Convert = System.Convert;
using ProductTypeConstant = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Scensum.BackOffice.UserControls.Common
{
	public partial class TargetProductTypeSelector : UserControl
	{
		protected class RepeaterDataSourceItem
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public bool Selected { get; set; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual IdDictionary DataSource { get; set; }

		public override void DataBind()
		{
			if (DataSource == null)
			{
				throw new ControlIntegrationException("DataSource shoule be initialized before calling DataBind().");
			}

			base.DataBind();

			var sourceItems = GetProductTypes();

			foreach (var sourceItem in sourceItems)
			{
				sourceItem.Selected = DataSource.ContainsKey(sourceItem.Id);
			}

			ProductTypeRepeater.DataSource = sourceItems;
			ProductTypeRepeater.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "0#")]
		public virtual bool IsValid(out Collection<string> validationMessages)
		{
			validationMessages = new Collection<string>();

			var selectedValues = CollectSelectedValues();

			if (selectedValues.Count == 0)
			{
				validationMessages.Add(Resources.LekmerMessage.TargetProductTypeEmptySelection);
				return false;
			}

			var productTypes = GetProductTypes();

			if (selectedValues.Any(selectedValue => productTypes.Any(pt => pt.Id == selectedValue.Key) == false))
			{
				validationMessages.Add(Resources.LekmerMessage.TargetProductTypeCorruptedSelection);
				return false;
			}

			return true;
		}

		public virtual IdDictionary CollectSelectedValues()
		{
			var ids = new IdDictionary();

			foreach (RepeaterItem repeaterItem in ProductTypeRepeater.Items)
			{
				var productTypeCheckbox = (CheckBox)repeaterItem.FindControl("ProductTypeCheckbox");
				if (productTypeCheckbox.Checked)
				{
					int id = Convert.ToInt32(((HiddenField)repeaterItem.FindControl("ProductTypeIdHidden")).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}

			return ids;
		}

		protected Collection<RepeaterDataSourceItem> GetProductTypes()
		{
			var productTypes = new Collection<RepeaterDataSourceItem>();

			foreach (var productType in IoC.Resolve<IProductTypeSecureService>().GetAll())
			{
				productTypes.Add(new RepeaterDataSourceItem { Id = productType.Id, Title = productType.Title });
			}
			
			return productTypes;
		}
	}
}