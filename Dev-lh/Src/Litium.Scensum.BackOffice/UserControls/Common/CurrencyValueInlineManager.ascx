﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrencyValueInlineManager.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Common.CurrencyValueInlineManager" %>

<asp:Repeater runat="server" ID="CurrencyValueRepeater" OnItemDataBound="OnCurrencyValueRepeaterDataBound">
	<ItemTemplate>
		<div class="left" style="font-size: 10px; padding-right: 5px;">
			<asp:Literal runat="server" ID="CurrencyIsoLiteral" Text='<%# ((CurrencyRepeaterDataSourceItem)Container.DataItem).CurrencyIsoWithChannels %>'/>
			<asp:RangeValidator ID="CurrencyValueValidator" runat="server" ControlToValidate="ValueTextBox" Type="Currency" ErrorMessage="*" EnableClientScript="true" ValidationGroup="CurrencyValueValidationGroup"></asp:RangeValidator>
			<br/>
			<asp:TextBox runat="server" ID="ValueTextBox" Text='<%# ((CurrencyRepeaterDataSourceItem)Container.DataItem).MonetaryValue %>' Width="40px" />
			<asp:HiddenField runat="server" ID="CurrencyIdHiddenField" Value='<%# ((CurrencyRepeaterDataSourceItem)Container.DataItem).Currency.Id %>' />
		</div>
	</ItemTemplate>
</asp:Repeater>