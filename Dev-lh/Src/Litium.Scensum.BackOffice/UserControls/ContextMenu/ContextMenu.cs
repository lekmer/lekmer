﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Globalization;
namespace Litium.Scensum.BackOffice.UserControls.ContextMenu
{
    public class ContextMenu2 : Repeater
    {
        private const string ContainerId = "cm-main-container";

        private HtmlGenericControl _mainContainer;

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            _mainContainer = new HtmlGenericControl("div") {ID = ContainerId};
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
			if (Controls.Count == 0) return;

            if (Page.IsPostBack)
            {
                SelectedValue = ((HtmlInputControl) (Controls[0].FindControl("value"))).Value;
                SelectedDisplayValue = ((HtmlInputControl) (Controls[0].FindControl("title"))).Value;
            }
            else
            {
                ((HtmlInputControl)(Controls[0].FindControl("value"))).Value = SelectedValue;
                ((HtmlInputControl)(Controls[0].FindControl("title"))).Value = SelectedDisplayValue;
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Control[] controls = new Control[Controls.Count];
            Controls.CopyTo(controls, 0);
            Controls.Clear();
            foreach (var control in controls)
            {
                _mainContainer.Controls.Add(control);
            }
            Controls.Add(_mainContainer);
            AppentScripts();
            base.Render(writer);
            ScriptManager.RegisterStartupScript(this, GetType(), "set-selected" + ClientID,
                                                string.Format(CultureInfo.InvariantCulture,
                                                              "$('div#{0}').find('.{1}').text('{2}');$('div#{0}').find('.{3}').val('{4}');",
                                                              _mainContainer.ClientID,
                                                              MenuDisplaySelectedDisplayValueControlClass,
                                                              SelectedDisplayValue, MenuSelectedValueControlClass,
                                                              SelectedValue), true);
        }
        
        private void AppentScripts()
        {
            if (string.IsNullOrEmpty(MenuCallerControlClass) || string.IsNullOrEmpty(MenuItemContainerClass) ||
                string.IsNullOrEmpty(MenuItemValueControlClass) ||
                string.IsNullOrEmpty(MenuItemDisplayValueControlClass) ||
                string.IsNullOrEmpty(MenuSelectedValueControlClass) ||
                string.IsNullOrEmpty(MenuSelectedDisplayValueControlClass) || string.IsNullOrEmpty(MenuContainerControlClass))
            {
                throw new ArgumentException("Not all required properties are filled in.");
            }

            string startupScript = string.Format(CultureInfo.InvariantCulture,
                                                 @"        $(document).ready(function() {0}
        __scmAppendEvents('{2}');
        __scmInitialize('{2}');
        {1});",
                                                 "{", "}", _mainContainer.ClientID);
            ScriptManager.RegisterStartupScript(this, GetType(), "startupScript" + ClientID, startupScript, true);

            string appendEventsScript = string.Format(CultureInfo.InvariantCulture,
                                                      @"        function __scmAppendEvents(containerId) {0}

    $(document).click(function(e) {0} __scmChangeMenuVisibilityPrivate(false, containerId, e);  {1});


            $('div#' + containerId).find('.{2}').click(function(event) {0}
                __scmChangeMenuVisibility(containerId);
           {1} );
            
            $('div#' + containerId).find('div.{3}').children().find('*').click(function(event) {0}
                $(event.target).parents('div.{3}').click();
           {1} );

            $('div#' + containerId).find('div.{3}').click(function(event) {0}
                var title = $(event.target).find('.{5}').length != 0
                            ? $(event.target).find('.{5}').text()
                            : $(event.target).parents('div.{3}:first').find('.{5}').text();
                $('div#' + containerId).find('.{8}').text(title);
                $('div#' + containerId).find('.{7}').val(title);
                var value = $(event.target).find('.{4}').length != 0
                            ? $(event.target).find('.{4}').val()
                            : $(event.target).parents('div.{3}:first').find('.{4}').val();
                $('div#' + containerId).find('.{6}').val(value);

                __scmChangeMenuVisibilityPrivate(false, containerId);
           {1} );
       {1} ",
                                                      "{", "}", MenuCallerControlClass, MenuItemContainerClass,
                                                      MenuItemValueControlClass, MenuItemDisplayValueControlClass,
                                                      MenuSelectedValueControlClass,
                                                      MenuSelectedDisplayValueControlClass, MenuDisplaySelectedDisplayValueControlClass);
            ScriptManager.RegisterStartupScript(this, GetType(), "cm-appendEventsScript", appendEventsScript, true);

            string scriptChangeMenuVisibility = string.Format(CultureInfo.InvariantCulture,
                                                              @"
        function __scmInitialize(containerId) {0}
            __scmChangeMenuVisibilityPrivate(false, containerId);

       $('div#' + containerId).find('div.{3}').css('height', $('div#' + containerId).find('div.{2}').height());
       $('div#' + containerId).find('div.{3}').css('width', $('div#' + containerId).find('div.{2}').width());
        {1}

        function __scmChangeMenuVisibility(containerId) {0}
            if ($('div#' + containerId).find('div.{2}:visible').length == 0) {0}
                __scmChangeMenuVisibilityPrivate(true, containerId);
            {1}
            else {0}
                __scmChangeMenuVisibilityPrivate(false, containerId);
            {1}
        {1}

        function __scmChangeMenuVisibilityPrivate(visible, containerId, event) {0}
        if (null != event) {0}
            if ($(event.target).parents('div#' + containerId).length != 0) {0}
                event.stopPropagation();
                return;
            {1}
        {1}
            if (visible) {0}
                $('div#' + containerId).find('div.{2}').show();
                $('div#' + containerId).find('div.{3}').show();
            {1}
            else {0}
               $('div#' + containerId).find('div.{2}').hide();
                $('div#' + containerId).find('div.{3}').hide();
            {1}
        {1}
",
                                                              "{", "}", MenuContainerControlClass, MenuContainerShadowControlClass);

            ScriptManager.RegisterStartupScript(this, GetType(), "cm-visibility", scriptChangeMenuVisibility, true);
        }
        
        public string SelectedValue
        {
            get
            {
                return ViewState["sv"] != null ? (string)ViewState["sv"] : null;
            }
            set { ViewState["sv"] = value; }
        }

        public string SelectedDisplayValue
        {
            get
            {
                return ViewState["sdv"] != null ? (string)ViewState["sdv"] : null;
            }
            set { ViewState["sdv"] = value; }
        }

        public virtual string MenuCallerControlClass { get; set; }

        public virtual string MenuItemContainerClass { get; set; }

        public virtual string MenuItemValueControlClass { get; set; }

        public virtual string MenuItemDisplayValueControlClass { get; set; }

        public virtual string MenuSelectedValueControlClass { get; set; }

        public virtual string MenuSelectedDisplayValueControlClass { get; set; }

        public virtual string MenuDisplaySelectedDisplayValueControlClass { get; set; }

        public virtual string MenuContainerControlClass { get; set; }

        public virtual string MenuContainerShadowControlClass { get; set; }
    }
}
