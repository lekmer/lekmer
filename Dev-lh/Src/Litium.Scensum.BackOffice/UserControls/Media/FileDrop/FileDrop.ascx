﻿<%@ control language="C#" autoeventwireup="true" codebehind="FileDrop.ascx.cs" inherits="Litium.Scensum.BackOffice.UserControls.Media.DrungAndDrop.FileDrop" %>

<script src="<%=ResolveUrl("~/Media/Scripts/dropzone.js") %>" type="text/javascript"></script>
<link href="<%=ResolveUrl("~/Media/Css/dropzone.css") %>" rel="stylesheet" type="text/css" />

<script>
	function guid() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
			  .toString(16)
			  .substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
		  s4() + '-' + s4() + s4() + s4();
	}

	$(document).ready(function () {
		var previewNode = document.querySelector("#template");
		previewNode.id = "";
		var previewTemplate = previewNode.parentNode.innerHTML;
		previewNode.parentNode.removeChild(previewNode);

		var myDropzone = new Dropzone("div#actions1", { // Make the whole body a dropzone
			url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileDropHandler.ashx") %>', // Set the url
			thumbnailWidth: 80,
			thumbnailHeight: 80,
			addRemoveLink :true,
			parallelUploads: 20,
			previewTemplate: previewTemplate,
		
			autoQueue: false, // Make sure the files aren't queued until manually added
			previewsContainer: "#previews", // Define the container to display the previews
			acceptedFiles: '<%= AcceptedTypes %>',
			maxFilesize: '<%= AcceptedFileSize %>',
			autoDiscover: false

		});

		myDropzone.on("addedfile", function (file) {
			// Hookup the start button
			//file.previewElement.querySelector(".save").onclick = function () { myDropzone.enqueueFile(file); };
			file.previewElement.querySelector(".name").id = guid();
			file.previewElement.querySelector(".name").value = file.name;
			var dropzone = this;
			var removeButton = Dropzone.createElement("<div class='list-item-button1'><a class='movedownbutton'><img src='/Media/Images/Common/delete.gif' onclick='deleteImage(this)'></a></div>");

			removeButton.addEventListener("click", function (e) {
				e.preventDefault();
				e.stopPropagation();

				dropzone.removeFile(file);
			});
			file.previewElement.appendChild(removeButton);


		});

		// Update the total progress bar
		myDropzone.on("totaluploadprogress", function (progress) {
			document.querySelector("#progressBar").value = progress;
		});

		myDropzone.on("sending", function (file, xhr, formData) {
			// Show the total progress bar when upload starts
			//document.querySelector("#total-progress").style.opacity = "1";
			// And disable the start button
			var title = file.previewElement.querySelector(".name").value;
			formData.append("title", title);
			formData.append("folderId", '<%= FolderId %>');
			formData.append("userName", '<%= UserName %>');
			document.querySelector("#save").disable = true;
			document.querySelector("#delete").disable = true;
			document.querySelector("#progressBar").style.display = 'block';
		});

		//// Hide the total progress bar when nothing's uploading anymore
		myDropzone.on("queuecomplete", function (progress) {
			document.querySelector("#progressBar").value = "0";
			document.querySelector("#progressBar").style.display = 'none';
		});
		myDropzone.on("complete", function (progress) {
			//document.querySelector("#total-progress").style.opacity = "0";
			myDropzone.removeAllFiles(true);
		});

		// Setup the buttons for all transfers
		// The "add files" button doesn't need to be setup because the config
		// `clickable` has already been specified.
		document.querySelector("#save").onclick = function () {
			myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
		};
		document.querySelector("#delete").onclick = function () {
			myDropzone.removeAllFiles(true);
		};


	});

	function edit(e) {

		e.style.display = 'none';
		var inputcontrol = e.parentNode.querySelector('.name_entry');
		inputcontrol.style.display = 'block';
		inputcontrol.value = e.innerText;
		inputcontrol.focus();
	};

	function save(e) {
		var control = e.parentNode.querySelector('.name');
		control.style.display = 'block';
		e.style.display = 'none';
		control.innerText = e.value;

	};



</script>

<div class="dropcontainer">
	<div id="actions1" class="row">

		<div>
			 <asp:Literal runat="server" Text="<%$ Resources:Media, Literal_DropMediaFiles%>" />
			<div>
				<progress id="progressBar" max="100" value="0" style="display: none">0% complete</progress>
			</div>
		</div>



	</div>
	<div class="row">
		<div class="buttons">
			<div class="button-container" id="save" style="cursor: pointer">
				<span class="button-left-border"></span>
				<span class="button-middle"><asp:Literal runat="server" Text="<%$ Resources:Media, Literal_UploadAndSave%>" /></span>
				<span class="button-right-border"></span>
			</div>
			<div class="button-container" id="delete" style="cursor: pointer">
				<span class="button-left-border"></span>
				<span class="button-middle"><asp:Literal runat="server" Text="<%$ Resources:Media, Literal_DeleteAll%>" /></span>
				<span class="button-right-border"></span>
			</div>
		</div>
	</div>


	<div class="table table-striped" class="files" id="previews">

		<div id="template" class="DragBox">
			<!-- This is used as the file preview template -->
			<div>
				<span class="preview">
					<img data-dz-thumbnail /></span>
			</div>
			<input type="hidden" class="fileguid"></input>
			<div>

				<p class="name" data-dz-name onclick="edit(this)"></p>
				<input class="name_entry" name="amount" style="display: none;" onblur="save(this)"></input>

			</div>
			<div>
				<p data-dz-size></p>

				<strong data-dz-errormessage></strong>
			</div>

			<div>
			</div>
		</div>

	</div>
</div>

