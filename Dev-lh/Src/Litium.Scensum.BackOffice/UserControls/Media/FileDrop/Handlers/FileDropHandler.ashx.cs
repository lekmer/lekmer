﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Litium.Lekmer.Media;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Media.MediaFolder;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	/// <summary>
	/// Summary description for FileDropHandler
	/// </summary>
	public class FileDropHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{

			if (context == null) throw new ArgumentNullException("context");

			if (context.Request.Files.Count > 0)
			{
				
				int folder;
				if (Int32.TryParse(context.Request["folderId"], out folder))
				{

					var file = context.Request.Files[0];
					var userName = context.Request["userName"];
					var service = IoC.Resolve<IMediaItemSecureService>();
					var extension = Path.GetExtension(file.FileName).TrimStart(new[] { '.' });
					if (string.IsNullOrEmpty(extension))
					{
						throw new UploadException(UploadExceptionReason.IncorrectFormat);
					}
					IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension);
					if (mediaFormat == null)
					{
						throw new UploadException(UploadExceptionReason.IncorrectFormat);
					}
					var mediaFolderService = IoC.Resolve<ILekmerMediaFolderSecureService>();
					
					IMediaFolder mediaFolder = mediaFolderService.GetById(folder);
					
					var mediaFolderType = MediaFolderHepler.GetMediaFolderType(mediaFolder);
					if (mediaFormat.Mime.Contains("image"))
					{
						if ((mediaFolderType == MediaFolderType.Products) || (mediaFolderType == MediaFolderType.Brands) ||
						    (mediaFolderType == MediaFolderType.Banners)||(mediaFolderType == MediaFolderType.Icons))
						{
							var childs = mediaFolderService.GetAllByParentId(mediaFolder.Id);
							var imageFolder =
								childs.SingleOrDefault(item => MediaFolderHepler.GetMediaFolderType(item) == MediaFolderType.Images);
							if (imageFolder != null)
							{
								folder = imageFolder.Id;
							}
						}
						else if ((mediaFolderType == MediaFolderType.Pdf) && (mediaFolder.MediaFolderParentId.HasValue))
						{
							var parent = mediaFolderService.GetById(mediaFolder.MediaFolderParentId.Value);
							if (parent != null)
							{
								var childs = mediaFolderService.GetAllByParentId(mediaFolder.Id);
								var imageFolder =
									childs.SingleOrDefault(item => MediaFolderHepler.GetMediaFolderType(item) == MediaFolderType.Images);
								if (imageFolder != null)
								{
									folder = imageFolder.Id;
								}
							}

						}
					}
					else if (mediaFormat.Mime.Contains("pdf"))
					{
						if (mediaFolderType == MediaFolderType.Products) 
						{
							var childs = mediaFolderService.GetAllByParentId(mediaFolder.Id);
							var imageFolder =
								childs.SingleOrDefault(item => MediaFolderHepler.GetMediaFolderType(item) == MediaFolderType.Pdf);
							if (imageFolder != null)
							{
								folder = imageFolder.Id;
							}
						}
						else if (mediaFolder.MediaFolderParentId.HasValue)
						{
							var parent = mediaFolderService.GetById(mediaFolder.MediaFolderParentId.Value);
							if (parent != null)
							{
								var childs = mediaFolderService.GetAllByParentId(mediaFolder.Id);
								var imageFolder =
									childs.SingleOrDefault(item => MediaFolderHepler.GetMediaFolderType(item) == MediaFolderType.Pdf);
								if (imageFolder != null)
								{
									folder = imageFolder.Id;
								}
							}

						}
						else
						{
							folder = -1;
						}
					}


					if (folder > 0)
					{
						var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
						var userFull = systemUserSecureService.GetFullByUserName(userName);
						service.Insert(userFull, file.InputStream, folder, context.Request["title"] ?? file.FileName);

					}
					file.InputStream.Close();
				}
				context.Response.Redirect(PathHelper.Media.GetMediaListUrl(folder));
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}