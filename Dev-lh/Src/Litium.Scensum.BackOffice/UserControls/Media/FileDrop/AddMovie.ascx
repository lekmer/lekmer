﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddMovie.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Media.FileDrop.AddMovie" %>

<script src="<%=ResolveUrl("~/Media/Scripts/dropzone.js") %>" type="text/javascript"></script>
<link href="<%=ResolveUrl("~/Media/Css/dropzone.css") %>" rel="stylesheet" type="text/css" />
<%--//<link href='<%=ResolveUrl("~/Media/Css/filedrop.css") %>' rel="stylesheet" type="text/css" />--%>
<script src="<%=ResolveUrl("~/Media/Scripts/draggable.js") %>" type="text/javascript"></script>

<script type="text/javascript">
	var myDropzone2;
	$(document).ready(function () {
	
		 myDropzone2 = new Dropzone("#Movie", {
			url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileDropMovieUploadHandler.ashx") %>',
		 	autoQueue: false, 
		 	autoDiscover: false,
			addRemoveLinks: true,
			maxFiles: 1,
			uploadMultiple: false,
			acceptedFiles: 'image/*'
		});

		myDropzone2.on("addedfile", function (file) {
			if (this.files[1] != null) {
				this.removeFile(this.files[0]);
			}
		});
		myDropzone2.on("sending", function (file, xhr, formData) {
			var link = document.getElementById("LinkTextBox").value;
			var parameters = document.getElementById("ParameterTextBox").value;
			var productId = document.getElementById("product_id").value;
			formData.append("folderId", '<%= FolderId %>');
			formData.append("userName", '<%= UserName %>');
			formData.append("link", link);
			formData.append("parameters", parameters);
			formData.append("productId", productId);
		});
		myDropzone2.on("success", function (file, response) {
			document.getElementById("LinkTextBox").value = '';
			document.getElementById("ParameterTextBox").value = '';
			myDropzone2.removeAllFiles();
		});

		document.querySelector("#addmovie").onclick = function () {

			var link = document.getElementById("LinkTextBox").value;

			if (link == '') {
				alert("Please add movie link");
			} else {
				if (myDropzone2.files.length === 0) {
					
					var parameters = document.getElementById("ParameterTextBox").value;
					var productId = document.getElementById("product_id").value;
					var userName = '<%= UserName %>';
					var folderId = '<%= FolderId %>';
					document.getElementById("LinkTextBox").value = '';
					document.getElementById("ParameterTextBox").value = '';
					
					$.ajax({
						type: "POST",
						url: '<%= ResolveUrl("~/UserControls/Media/FileDrop/Handlers/FileDropMovieUploadHandler.ashx") %>',
						data: { productId: productId, link:link, userName:userName, folderId:folderId, parameters:parameters },
						dataType: "json"
					});

				} else {
					var t = myDropzone2.getFilesWithStatus(Dropzone.ADDED);
					myDropzone2.enqueueFiles(t);
				}
			}

		};
	});



	



</script>
<div id ="Movie" class="dropzone">
	<span class="textspan">
	
	</span>
	<input type="hidden" id="moviemediaid"></input>
	</div>
<div class="parametercontainer">
	<div>
	   <span AssociatedControlId="LinkTextBox">
	   Link:
   </span>
		<br>
	<input id="LinkTextBox"/>
	   </div>
   <div>
	   <br>
	   <br>
		   <span  AssociatedControlId="ParameterTextBox">
	   Parameter:
   </span>
	   <br>
	<input id="ParameterTextBox"/>
	   <br>
	   <br>
   </div>
			<div class="button-container" id="addmovie" style="cursor: pointer">
				<span class="button-left-border"></span>
				<span class="button-middle"><asp:Literal runat="server" Text="Add movie" /></span>
				<span class="button-right-border"></span>
			</div>
	</div>
<asp:updatepanel id="MovieUpdatePanel" runat="server" updatemode="Conditional">	
	<ContentTemplate>
		

	</ContentTemplate> 
</asp:updatepanel>
