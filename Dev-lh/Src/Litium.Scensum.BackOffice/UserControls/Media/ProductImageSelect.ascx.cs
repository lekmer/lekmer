﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;
using Resources;
using Image=System.Web.UI.WebControls.Image;
using System.Web.UI;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	public partial class ProductImageSelect : StateUserControlController<string>
	{
		public event EventHandler<ImageSelectEventArgs> Selected;
		public event EventHandler FileUploaded;
		public event EventHandler Canceled;

		[SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider",
			MessageId = "System.String.Format(System.String,System.Object)"),
		 SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public string GetSetTabScript()
		{
			return string.Format("$(\'#{0}\').tabs();", divTabsAddImages.ClientID);
		}

		protected override void SetEventHandlers()
		{
			
			mediaTree.NodeCommand += OnMediaTreeNodeCommand;
			gvImages.RowDataBound += GVImagesRowDataBound;

			btnSaveNewImage.Click += BtnSaveNewImageClick;
			btnCancelSaveNewImage.Click += BtnCancelSaveImageClick;

			btnUpload.Click += BtnUploadClick;
			btnSaveArchiveImage.Click += IlbSaveArchiveImageClick;
			//btnCancelArchiveImage.Click += BtnCancelSaveImageClick;

			ucNodeSelector.NodeCommand += OnNodeSelectorCommand;
		}

		public void PreSelectFolder(int mediaFolderId)
		{
			ucNodeSelector.SelectedNodeId = mediaFolderId;
			liArchiveImageTabImageTab.Visible = false;
			liUploadImageTab.Visible = false;
			
			mediaTree.SelectedNodeId = mediaFolderId;
			mediaTree.Visible = false;
		}

		protected virtual void GVImagesRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;
			var image = (IImage) row.DataItem;
			var imgArchiveImage = (Image) row.FindControl("imgArchiveImage");
			imgArchiveImage.ImageUrl =
				ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
			var lblArchiveImageFileType = (Label) row.FindControl("lblArchiveImageFileType");
			lblArchiveImageFileType.Text = image.FormatExtension;
			var lblArchiveImageDimensions = (Label) row.FindControl("lblArchiveImageDimensions");
			lblArchiveImageDimensions.Text = image.Width + " x " + image.Height;
			var rbSelect = (HtmlInputRadioButton) row.FindControl("rbSelect");
			rbSelect.Attributes.Add("onclick", string.Format(CultureInfo.CurrentCulture, "{0}UnCheckOther(this);", ClientID));
		}

		protected override void PopulateControl()
		{
			int? mediaTreeSelectedNode = mediaTree.SelectedNodeId;
			PopulateImageFolderTree(null);
			PopulateNodeSelectTree(null);
			if (mediaTreeSelectedNode.HasValue)
			{
				mediaTree.SelectedNodeId = mediaTreeSelectedNode;
				PopulateArchiveImages(mediaTreeSelectedNode.Value);
				gvImages.Visible = true;
			}
			else
			{
				gvImages.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CheckPrivileges();
		}

		protected void PopulateImageFolderTree(int? folderId)
		{
			mediaTree.DataSource = GetMediaTreeDataSource(folderId);
			mediaTree.DataBind();
			mediaTree.SelectedNodeId = folderId;
		}

		protected void PopulateNodeSelectTree(int? nodeId)
		{
			ucNodeSelector.DataSource = GetMediaTreeDataSource(nodeId);
			ucNodeSelector.DataBind();
		}

		protected virtual void OnMediaTreeNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateImageFolderTree(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					mediaTree.DenySelection = true;
					break;
				case "Navigate":
					PopulateArchiveImages(e.Id);
					gvImages.PageIndex = 0;
					gvImages.Visible = true;
					break;
			}
		}

		protected virtual void OnNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			PopulateNodeSelectTree(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
		}

		protected void PopulateArchiveImages(int folderId)
		{
			ids.SelectParameters.Clear();
			ids.SelectParameters.Add("id", folderId.ToString(CultureInfo.CurrentCulture));
		}

		protected virtual IEnumerable<INode> GetMediaTreeDataSource(int? folderId)
		{
			return IoC.Resolve<IMediaFolderSecureService>().GetTree(folderId);
		}

		protected virtual void BtnUploadClick(object sender, EventArgs e)
		{
			HttpPostedFile file = fileUpload.PostedFile;
			if (file.ContentLength == 0)
			{
				ShowErrorMessage(MediaMessage.FileIsEmpty);
				RegisterScriptOpenFirstTab();
				RaiseUploadEvent();
				return;
			}

			if (!IsImage(file.ContentType))
			{
				ShowErrorMessage(MediaMessage.IncorrectFileFormat);
				RegisterScriptOpenFirstTab();
				RaiseUploadEvent();
				return;
			}

			string extension = Path.GetExtension(file.FileName);
			IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension.TrimStart(new[] {'.'}));
			if (mediaFormat == null)
			{
				ShowErrorMessage(MediaMessage.IncorrectFileFormat);
				RegisterScriptOpenFirstTab();
				RaiseUploadEvent();
				return;
			}

			var service = IoC.Resolve<IMediaItemSecureService>();
			Guid mediaId = service.InsertTemporary(SignInHelper.SignedInSystemUser, file.InputStream, mediaFormat.Extension);
			State = string.Format(CultureInfo.CurrentCulture, "{0}.{1}", mediaId, mediaFormat.Extension);
			tbNewImageAlternativeText.Text = string.Empty;
			tbNewImageTitle.Text = Path.GetFileNameWithoutExtension(file.FileName);
			lblNewImageFileType.Text = mediaFormat.Extension;
			imgNew.ImageUrl = ResolveUrl(PathHelper.Media.GetImageTempLoaderUrl(mediaId, mediaFormat.Extension));
			file.InputStream.Close();
			divUploadImage.Visible = true;
			PopulateNodeSelectTree(null);

			RaiseUploadEvent();

			RegisterScriptOpenFirstTab();
		}


		private void RegisterScriptOpenFirstTab()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), "open first tab" + ClientID, string.Format(CultureInfo.InvariantCulture, "{0}UploadImageTabClick();", ClientID), true);
		}

		private void RaiseUploadEvent()
		{
			if (FileUploaded != null)
			{
				FileUploaded(this, EventArgs.Empty);
			}
		}

		private void ShowErrorMessage(string error)
		{
			divUploadImage.Visible = false;
			messager.Add(error);
			messagerPanel.Update();
		}

		protected virtual void BtnCancelSaveImageClick(object sender, EventArgs e)
		{
			if (Canceled != null)
			{
				Canceled(this, EventArgs.Empty);
			}
		}

		protected virtual void IlbSaveArchiveImageClick(object sender, EventArgs e)
		{
			foreach (GridViewRow row in gvImages.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var rbSelect = (HtmlInputRadioButton) row.FindControl("rbSelect");
				if (rbSelect.Checked)
				{
					var hfId = (HiddenField) row.FindControl("hfId");
					int imageId = int.Parse(hfId.Value, CultureInfo.CurrentCulture);
					RaiseImageSelectedEvent(imageId);
					return;
				}
			}
		}


		[SuppressMessage("Microsoft.Design", "CA1030:UseEventsWhereAppropriate")]
		protected void RaiseImageSelectedEvent(int? imageId)
		{
			if (imageId.HasValue)
			{
				if (Selected != null)
				{
					var imageService = IoC.Resolve<IImageSecureService>();
					IImage image = imageService.GetById((int) imageId);
					Selected(this, new ImageSelectEventArgs {Image = image});
				}
			}
		}

		protected virtual void BtnSaveNewImageClick(object sender, EventArgs e)
		{
			if (!ucNodeSelector.SelectedNodeId.HasValue)
			{
				divUploadImage.Visible = true;
				messager.Add(MediaMessage.MediaFolderIsNotSelected);
				messagerPanel.Update();
				RaiseUploadEvent();
				return;
			}
			var service = IoC.Resolve<IImageSecureService>();
			IImage image = service.Create();
			image.FileName = image.Title = tbNewImageTitle.Text;
			image.AlternativeText = tbNewImageAlternativeText.Text;
			image.MediaFolderId = ucNodeSelector.SelectedNodeId.Value;
			image.FormatExtension = lblNewImageFileType.Text;
			int mediaId;
			try
			{
				mediaId = service.Insert(SignInHelper.SignedInSystemUser, image, State);
			}
			catch (UploadException ex)
			{
				if (ex.Reason == UploadExceptionReason.IncorrectFormat)
				{
					messager.Add(string.Format(CultureInfo.CurrentCulture, MediaMessage.IncorrectFileFormat));
				}
				else
				{
					messager.Add(string.Format(CultureInfo.CurrentCulture, "{0} {1} {2}", MediaMessage.UploadFailed, ex.Message,
					                           ex.InnerException.Message));
				}
				return;
			}
			IoC.Resolve<IMediaItemSecureService>().DeleteTemporary(image.FileName);
			RaiseImageSelectedEvent(mediaId);
			divUploadImage.Visible = false;
		}

		protected virtual bool IsImage(string contentType)
		{
			var pos = contentType.IndexOf('/');
			if (pos < 0) return false;
			var type = contentType.Substring(0, pos);
			return type == "image";
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual void CheckPrivileges()
		{
			var accessValidator = IoC.Resolve<IAccessValidator>();
			liUploadImageTab.Visible = accessValidator.HasAccess(SignInHelper.SignedInSystemUser, PrivilegeConstant.Media);
		}
	}
}