﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Litium.Lekmer.Core;

namespace Litium.Scensum.BackOffice.Base
{
	public class FolderPathHelper
	{
		public virtual string GetPath<T>(Collection<T> allFolders, int folderId) where T: class, IFolder
		{
			var folders = allFolders;
			var folder = folders.FirstOrDefault(f => f.Id == folderId);
			if (folder == null)
			{
				return string.Empty;
			}

			var stack = new Stack<string>();
			stack.Push(folder.Title);

			while (folder.ParentFolderId.HasValue)
			{
				var child = folder;
				folder = folders.FirstOrDefault(f => f.Id == child.ParentFolderId.Value);
				stack.Push(folder.Title);
			}

			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString().TrimEnd(new[] { '\\' });
		}
	}
}