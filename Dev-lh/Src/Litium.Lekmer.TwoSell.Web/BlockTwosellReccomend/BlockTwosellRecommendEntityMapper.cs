using System.Globalization;
using Litium.Lekmer.SiteStructure;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.TwoSell.Web
{
	public class BlockTwosellRecommendEntityMapper : LekmerBlockEntityMapper<IBlockList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}