﻿namespace Litium.Lekmer.MediaArchive.Cleanup.Console
{
	public enum MediaArchiveCleanupType
	{
		NOT_EXIST = 1,
		NOT_USED = 2,
		USED = 3,
		UNKNOWN = 4
	}
}