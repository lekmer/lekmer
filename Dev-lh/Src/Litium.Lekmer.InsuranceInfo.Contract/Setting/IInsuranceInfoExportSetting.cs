﻿using System.Collections.Generic;

namespace Litium.Lekmer.InsuranceInfo.Contract
{
	public interface IInsuranceInfoExportSetting
	{
		bool UseWeeklyExecution { get; }
		int WeeklyExecutionHour { get; }
		int WeeklyExecutionMinute { get; }
		bool WeeklyExecutionMonday { get; }
		bool WeeklyExecutionTuesday { get; }
		bool WeeklyExecutionWednesday { get; }
		bool WeeklyExecutionThursday { get; }
		bool WeeklyExecutionFriday { get; }
		bool WeeklyExecutionSaturday { get; }
		bool WeeklyExecutionSunday { get; }

		bool UseDailyExecution { get; }
		int DailyExecutionHour { get; }
		int DailyExecutionMinute { get; }



		string ApplicationName { get; }

		// For File.
		string DestinationDirectory { get; }
		string FileName { get; }
		string FileDelimiter { get; }

		// For Product.
		string CategoriesErpId { get; }
		string MaxPrice { get; }
		string MinPrice { get; }
		
		// For Order.
		int MaxDaysAfterPurchase { get; }
		int MinDaysAfterPurchase { get; }

		string ReplaceChannelNames { get; }

		string Encoding { get; }
		string FtpUrl { get; }
		string Username { get; }
		string Password { get; }

		string GetChannelNameReplacement(string channelName);
		bool GetChannelNameReplacementExists(string channelName);
		string GetChannelMinPriceReplacement(string channelName);
		string GetChannelMaxPriceReplacement(string channelName);
		string GetChannelUserNameReplacement(string channelName);
		string GetChannelPasswordReplacement(string channelName);
		ICollection<string> GetAllowedCategories();
	}
}