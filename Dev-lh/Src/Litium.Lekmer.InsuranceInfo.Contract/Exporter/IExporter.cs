﻿namespace Litium.Lekmer.InsuranceInfo.Contract
{
	public interface IExporter
	{
		void Execute();
	}
}