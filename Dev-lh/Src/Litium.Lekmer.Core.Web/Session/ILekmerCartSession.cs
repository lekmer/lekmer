using System;
using Litium.Scensum.Core.Web;

namespace Litium.Lekmer.Core.Web
{
	/// <summary>
	/// Session information regarding cart.
	/// </summary>
	public interface ILekmerCartSession : ICartSession
	{
		Guid CartVersionToken { get; set; }
		void Clean();
	}
}