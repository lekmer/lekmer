using Litium.Lekmer.Customer;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Core.Web
{
	/// <summary>
	/// Session information regarding customer.
	/// </summary>
	public interface ILekmerCustomerSession : ICustomerSession
	{
		void SignIn(ICustomer customer);
		void SignIn(ICustomer customer, CustomerLoginType loginType);
	}
}