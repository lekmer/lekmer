﻿using System;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Core.Web
{
	public static class LekmerCartHelper
	{
		public static void RefreshSessionCart()
		{
			if (CartRefreshedAlready)
			{
				return;
			}

			RefreshSessionCartHard();
		}

		public static void RefreshSessionCartHard()
		{
			HandleEmptyCart();

			var session = IoC.Resolve<ICartSession>();

			if (session.Cart == null) return;
			if (session.Cart.Id == 0) throw new InvalidOperationException("The session cart has not yet been persisted and can therefore not be refreshed.");

			var cartService = IoC.Resolve<ICartService>();
			session.Cart = cartService.GetById(UserContext.Current, session.Cart.Id);

			CartRefreshedAlready = true;
		}

		private static bool CartRefreshedAlready
		{
			get { return (bool)(HttpContext.Current.Items["CartRefreshed"] ?? false); }
			set { HttpContext.Current.Items["CartRefreshed"] = value; }
		}

		private static void HandleEmptyCart()
		{
			var session = IoC.Resolve<ICartSession>();

			if (session.Cart == null || session.Cart.Id != 0)
			{
				return;
			}

			//session.Cart.Id == 0

			if (session.Cart.GetCartItems().Count > 0)
			{
				var cartService = IoC.Resolve<ICartService>();
				session.Cart.Id = cartService.Save(UserContext.Current, session.Cart);
			}
			else
			{
				IoC.Resolve<ICartSession>().Cart = null;
			}
		}
	}
}
