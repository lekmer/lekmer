using System.Collections.Generic;

namespace Litium.Lekmer.Core.Web
{
	public interface IPagingCalculator
	{
		SortedDictionary<int, int> CalculateVisiblePageLinks(int pageCount, int selectedPage, int adjacentPages, int sidePages);
		int CalculatePageCount(int totalCount, int pageSize);
	}
}