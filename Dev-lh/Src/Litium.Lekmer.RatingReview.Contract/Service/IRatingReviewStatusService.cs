﻿namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewStatusService
	{
		IRatingReviewStatus GetByCommonName(string commonName);
	}
}