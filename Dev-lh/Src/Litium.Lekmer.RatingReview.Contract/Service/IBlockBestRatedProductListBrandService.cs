﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockBestRatedProductListBrandService
	{
		Collection<IBlockBestRatedProductListBrand> GetAllByBlock(IUserContext context, int blockId);
	}
}