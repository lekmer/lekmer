﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewFeedbackService
	{
		IRatingReviewFeedbackRecord CreateRecord(IUserContext context);

		IRatingReviewFeedbackRecord Insert(IUserContext context, IRatingReviewFeedbackRecord feedbackRecord);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flag")]
		void InsertInappropriateFlag(int ratingReviewFeedbackId);

		IRatingReviewFeedback GetMostHelpful(IUserContext context, int productId, int ratingId, Collection<int> ratingItemIds, int ratingReviewStatusId, bool isInappropriate);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		IRatingReviewFeedback GetByProductAndOrder(IUserContext context, int productId, int orderId);

		Collection<IRatingReviewFeedback> GetLatestByBlock(IUserContext context, IBlockLatestFeedbackList blockLatestFeedbackList);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		Collection<IRatingReviewFeedback> GetLatestByProduct(IUserContext context, int productId, int numberOfItems, int ratingReviewStatusId, bool isInappropriate);
	}
}