﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductScoreService
	{
		IProductRatingSummaryInfo GetProductRatingSummary(IUserContext context, int ratingId, int productId);
		ProductIdCollection GetBestRatedProductsByRating(IUserContext context, int ratingId, int? categoryId, Collection<int> brandIds, int numberOfItems);
		void Delete(int ratingReviewFeedbackId);
	}
}