using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockLatestFeedbackListBrandSecureService
	{
		void Save(int blockId, Collection<IBlockLatestFeedbackListBrand> blockBrands);

		Collection<IBlockLatestFeedbackListBrand> GetAllByBlock(int blockId);
	}
}