﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductScoreSecureService
	{
		IRatingItemProductScore Create(int channelId, int productId, int ratingId, int ratingItemId);
		IRatingItemProductScore Insert(ISystemUserFull systemUserFull, IRatingItemProductScore ratingItemProductScore);
		void Delete(ISystemUserFull systemUserFull, int ratingReviewFeedbackId);
	}
}