using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockLatestFeedbackListSecureService
	{
		IBlockLatestFeedbackList Create();

		IBlockLatestFeedbackList GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockLatestFeedbackList block);
	}
}