using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockBestRatedProductListBrandSecureService
	{
		void Save(int blockId, Collection<IBlockBestRatedProductListBrand> blockBrands);

		Collection<IBlockBestRatedProductListBrand> GetAllByBlock(int blockId);
	}
}