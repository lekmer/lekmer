﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductVoteSecureService
	{
		Collection<IRatingItemProductVote> GetAllByFeedback(int ratingReviewFeedbackId);
	}
}