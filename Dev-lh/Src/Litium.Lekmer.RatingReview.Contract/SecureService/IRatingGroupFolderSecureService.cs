﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupFolderSecureService
	{
		IRatingGroupFolder Create();

		IRatingGroupFolder Save(ISystemUserFull systemUserFull, IRatingGroupFolder ratingGroupFolder);

		bool TryDelete(ISystemUserFull systemUserFull, int ratingGroupFolderId);

		IRatingGroupFolder GetById(int ratingGroupFolderId);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IRatingGroupFolder> GetAll();

		Collection<IRatingGroupFolder> GetAllByParent(int parentRatingFolderId);

		Collection<INode> GetTree(int? selectedId);
	}
}