using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductMostHelpfulRatingSecureService
	{
		IBlockProductMostHelpfulRating Create();

		IBlockProductMostHelpfulRating GetById(int id);

		int Save(ISystemUserFull systemUserFull, IBlockProductMostHelpfulRating block);
	}
}