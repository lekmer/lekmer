﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewStatusSecureService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IRatingReviewStatus> GetAll();
	}
}