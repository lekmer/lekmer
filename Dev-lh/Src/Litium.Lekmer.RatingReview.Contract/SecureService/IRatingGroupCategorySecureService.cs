﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupCategorySecureService
	{
		void Save(ISystemUserFull systemUserFull, IRatingGroupCategory ratingGroupCategory);

		void SaveAll(ISystemUserFull systemUserFull, int categoryId, Collection<IRatingGroupCategory> ratingGroupCategories);

		void Delete(ISystemUserFull systemUserFull, int ratingGroupId, int categoryId);

		Collection<IRatingGroupCategory> GetAllByGroup(int ratingGroupId);

		Collection<IRatingGroupCategory> GetAllByCategory(int categoryId);
	}
}