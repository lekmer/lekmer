﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IProductRatingSummaryProcessor
	{
		IProductRatingSummaryInfo CreateSummary(IUserContext context, int productId, int ratingId, Collection<IRatingItemProductScoreSummary> ratingItemSummaries);
	}
}