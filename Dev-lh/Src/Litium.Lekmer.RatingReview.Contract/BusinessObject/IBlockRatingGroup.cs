﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingGroup : IBusinessObjectBase
	{
		int BlockId { get; set; }
		int RatingGroupId { get; set; }
	}
}