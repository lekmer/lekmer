﻿using Litium.Lekmer.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingFolder : IBusinessObjectBase, IFolder
	{
		//int Id { get; set; }
		int? ParentRatingFolderId { get; set; }
		//string Title { get; set; }
	}
}