﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductScoreSummary : IBusinessObjectBase
	{
		int RatingId { get; set; }
		int RatingItemId { get; set; }
		int RatingItemScore { get; set; }
		int TotalHitCount { get; set; }
	}
}