﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewFeedbackLike : IBusinessObjectBase
	{
		int Id { get; set; }
		int RatingReviewFeedbackId { get; set; }
		string IPAddress { get; set; }
		DateTime CreatedDate { get; set; }
		int RatingReviewUserId { get; set; }
	}
}