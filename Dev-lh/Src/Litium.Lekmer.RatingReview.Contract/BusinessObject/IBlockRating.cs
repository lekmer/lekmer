﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRating : IBusinessObjectBase
	{
		int BlockId { get; set; }
		int RatingId { get; set; }
	}
}