﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductScore : IBusinessObjectBase
	{
		int ChannelId { get; set; }
		int ProductId { get; set; }
		int RatingId { get; set; }
		int RatingItemId { get; set; }
		int HitCount { get; set; }
	}
}