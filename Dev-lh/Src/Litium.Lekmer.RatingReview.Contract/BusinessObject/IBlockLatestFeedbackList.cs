﻿using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockLatestFeedbackList : IBlock
	{
		int? CategoryId { get; set; }
		int NumberOfItems { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<IBlockLatestFeedbackListBrand> BlockLatestFeedbackListBrands { get; set; }
	}
}