﻿using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductMostHelpfulRating : IBlock
	{
		IBlockRating BlockRating { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<IBlockRatingItem> BlockRatingItems { get; set; }
	}
}