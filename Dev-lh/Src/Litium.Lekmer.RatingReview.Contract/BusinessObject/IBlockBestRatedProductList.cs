﻿using System.Collections.ObjectModel;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockBestRatedProductList : IBlock
	{
		int? CategoryId { get; set; }
		int? RatingId { get; set; }
		IBlockSetting Setting { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<IBlockBestRatedProductListBrand> BlockBestRatedProductListBrands { get; set; }
	}
}