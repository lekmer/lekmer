﻿namespace Litium.Lekmer.RatingReview
{
	public interface IRatingRegistry
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
	}
}