﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingItem : IBusinessObjectBase
	{
		int BlockId { get; set; }
		int RatingItemId { get; set; }
	}
}