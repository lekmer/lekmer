﻿namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemSummary
	{
		int RatingItemId { get; set; }
		int TotalHitCount { get; set; }
		decimal PercentageHitValue { get; set; }
	}
}