﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IReview : IBusinessObjectBase
	{
		int Id { get; set; }
		int RatingReviewFeedbackId { get; set; }
		string AuthorName { get; set; }
		string Title { get; set; }
		string Message { get; set; }
		string Email { get; set; }
	}
}