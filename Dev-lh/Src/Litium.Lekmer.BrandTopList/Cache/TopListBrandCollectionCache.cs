using Litium.Framework.Cache;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.BrandTopList
{
	public sealed class TopListBrandCollectionCache : ScensumCacheBase<TopListBrandCollectionKey, BrandIdCollection>
	{
		private static readonly TopListBrandCollectionCache _instance = new TopListBrandCollectionCache();

		private TopListBrandCollectionCache()
		{
		}

		public static TopListBrandCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class TopListBrandCollectionKey : ICacheKey
	{
		public TopListBrandCollectionKey(int channelId, int blockId, int pageNumber, int pageSize)
		{
			ChannelId = channelId;
			BlockId = blockId;
			PageNumber = pageNumber;
			PageSize = pageSize;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }
		public int PageNumber { get; set; }
		public int PageSize { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId + "-" + PageNumber + "-" + PageSize; }
		}
	}
}