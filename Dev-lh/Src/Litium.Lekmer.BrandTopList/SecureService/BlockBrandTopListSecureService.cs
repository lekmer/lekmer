using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListSecureService : IBlockBrandTopListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockBrandTopListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IBlockBrandTopListBrandSecureService BlockBrandTopListBrandSecureService { get; private set; }
		protected IBlockBrandTopListCategorySecureService BlockBrandTopListCategorySecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockBrandTopListSecureService(
			IAccessValidator accessValidator,
			BlockBrandTopListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IBlockBrandTopListCategorySecureService blockBrandTopListCategorySecureService,
			IBlockBrandTopListBrandSecureService blockBrandTopListBrandSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			BlockBrandTopListCategorySecureService = blockBrandTopListCategorySecureService;
			BlockBrandTopListBrandSecureService = blockBrandTopListBrandSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockBrandTopList Create()
		{
			if (AccessSecureService == null)
			{
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			}

			var blockBrandTopList = IoC.Resolve<IBlockBrandTopList>();

			blockBrandTopList.BrandTopListBrands = new Collection<IBlockBrandTopListBrand>();
			blockBrandTopList.BlockBrandTopListCategory = new Collection<IBlockBrandTopListCategory>();

			blockBrandTopList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockBrandTopList.Setting = BlockSettingSecureService.Create();
			blockBrandTopList.Status = BusinessObjectStatus.New;

			return blockBrandTopList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IChannel channel, IBlockBrandTopList block)
		{
			if (BlockBrandTopListCategorySecureService == null)
			{
				throw new InvalidOperationException("BlockBrandTopListCategorySecureService must be set before calling Save.");
			}

			if (BlockBrandTopListBrandSecureService == null)
			{
				throw new InvalidOperationException("BlockBrandTopListBrandSecureService must be set before calling Save.");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				Save(systemUserFull, block);

				if (block.Id == -1)
				{
					return block.Id;
				}

				BlockBrandTopListCategorySecureService.Save(block.Id, block.BlockBrandTopListCategory);
				BlockBrandTopListBrandSecureService.Save(block.Id, block.BrandTopListBrands);

				transactedOperation.Complete();
			}

			ClearCache(block);

			return block.Id;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockBrandTopList blockBrandTopList = Create();

			blockBrandTopList.ContentNodeId = contentNodeId;
			blockBrandTopList.ContentAreaId = contentAreaId;
			blockBrandTopList.BlockTypeId = blockTypeId;
			blockBrandTopList.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockBrandTopList.IncludeAllCategories = true;
			blockBrandTopList.OrderStatisticsDayCount = 30;
			blockBrandTopList.Title = title;
			blockBrandTopList.TemplateId = null;

			blockBrandTopList.Id = Save(systemUserFull, blockBrandTopList);

			return blockBrandTopList;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}

			BlockBrandTopListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
			//TopListBrandCollectionCache.Instance.Flush(); //NOTE: Not necessary !!!
		}

		public virtual IBlockBrandTopList GetById(int id)
		{
			IBlockBrandTopList blockBrandTopList = Repository.GetByIdSecure(id);

			blockBrandTopList.BrandTopListBrands = BlockBrandTopListBrandSecureService.GetAllByBlock(id);
			blockBrandTopList.BlockBrandTopListCategory = BlockBrandTopListCategorySecureService.GetAllByBlock(id);

			blockBrandTopList.SetUntouched();

			return blockBrandTopList;
		}

		protected virtual int Save(ISystemUserFull systemUserFull, IBlockBrandTopList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (BlockSecureService == null)
			{
				throw new InvalidOperationException("BlockSecureService must be set before calling Save.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);

				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);
				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			return block.Id;
		}

		protected virtual void ClearCache(IBlockBrandTopList block)
		{
			BlockBrandTopListCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());
			TopListBrandCollectionCache.Instance.Flush();
		}
	}
}