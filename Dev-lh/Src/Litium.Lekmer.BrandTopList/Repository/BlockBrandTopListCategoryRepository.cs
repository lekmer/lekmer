﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListCategoryRepository
	{
		protected virtual DataMapperBase<IBlockBrandTopListCategory> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBrandTopListCategory>(dataReader);
		}

		public virtual void Save(IBlockBrandTopListCategory blockCategory)
		{
			if (blockCategory == null)
			{
				throw new ArgumentNullException("blockCategory");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockCategory.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", blockCategory.Category.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", blockCategory.IncludeSubcategories, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListCategoryRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandTopListCategorySave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListCategoryRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandTopListCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockBrandTopListCategory> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListCategoryRepository.GetAllByBlockSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandTopListCategoryGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockBrandTopListCategory> GetAllByBlock(int languageId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListCategoryRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandTopListCategoryGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}