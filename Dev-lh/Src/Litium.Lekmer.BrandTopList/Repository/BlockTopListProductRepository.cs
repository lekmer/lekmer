﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList.Repository
{
	public class BlockTopListProductRepository
	{
		protected virtual DataMapperBase<IBlockTopListProduct> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockTopListProduct>(dataReader);
		}

		protected virtual DataMapperBase<IProduct> CreateProductDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProduct>(dataReader);
		}

		public virtual void Save(IBlockTopListProduct blockProduct)
		{
			if (blockProduct == null) throw new ArgumentNullException("blockProduct");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockProduct.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", blockProduct.Product.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Position", blockProduct.Position, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListProductRepository.Save");
			new DataHandler().ExecuteCommand("[addon].[pBlockTopListProductSave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListProductRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pBlockTopListProductDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockTopListProduct> GetAllByBlockSecure(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListProductRepository.GetAllByBlock");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[addon].[pBlockTopListProductGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockTopListProduct> GetAllByBlock(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListProductRepository.GetAllByBlock");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pBlockTopListProductGetAllByBlock]", parameters,
				                                                         dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}