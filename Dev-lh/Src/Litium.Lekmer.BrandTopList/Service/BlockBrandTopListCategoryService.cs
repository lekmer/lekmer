using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListCategoryService : IBlockBrandTopListCategoryService
	{
		protected BlockBrandTopListCategoryRepository Repository { get; private set; }
		protected ICategoryService CategoryService { get; private set; }

		public BlockBrandTopListCategoryService(BlockBrandTopListCategoryRepository repository, ICategoryService categoryService)
		{
			Repository = repository;
			CategoryService = categoryService;
		}


		public virtual Collection<IBlockBrandTopListCategory> GetAllByBlock(IUserContext context, IBlockBrandTopList block)
		{
			return Repository.GetAllByBlock(context.Channel.Language.Id, block.Id);
		}

		public virtual Collection<ICategoryView> GetViewAllByBlockWithChildren(IUserContext context, IBlockBrandTopList block)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (CategoryService == null)
			{
				throw new InvalidOperationException("CategoryService must be set before calling GetViewAllByBlock.");
			}

			ICategoryTree categoryTree = CategoryService.GetAllAsTree(context);

			Collection<IBlockBrandTopListCategory> blockBrandTopListCategories = block.BlockBrandTopListCategory;

			var resolvedCategories = new Collection<ICategoryView>();

			foreach (IBlockBrandTopListCategory blockBrandTopListCategory in blockBrandTopListCategories)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, categoryTree, blockBrandTopListCategory);
			}

			return resolvedCategories;
		}

		public virtual Collection<ICategoryView> GetViewAllByBlockWithoutChildren(IUserContext context, IBlockBrandTopList block)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (CategoryService == null)
			{
				throw new InvalidOperationException("CategoryService must be set before calling GetViewAllByBlock.");
			}

			ICategoryTree categoryTree = CategoryService.GetAllAsTree(context);

			Collection<IBlockBrandTopListCategory> blockBrandTopListCategories = block.BlockBrandTopListCategory;

			var resolvedCategories = new Collection<ICategoryView>();

			foreach (IBlockBrandTopListCategory blockBrandTopListCategory in blockBrandTopListCategories)
			{
				ResolveBlockCategoryWithoutChildren(resolvedCategories, categoryTree, blockBrandTopListCategory);
			}

			return resolvedCategories;
		}


		protected virtual void ResolveBlockCategoryWithoutChildren(Collection<ICategoryView> resolvedCategories, ICategoryTree categoryTree, IBlockBrandTopListCategory blockBrandTopListCategory)
		{
			if (resolvedCategories == null)
			{
				throw new ArgumentNullException("resolvedCategories");
			}

			if (categoryTree == null)
			{
				throw new ArgumentNullException("categoryTree");
			}

			ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(blockBrandTopListCategory.Category.Id);
			if (categoryTreeItem == null)
			{
				return;
			}

			resolvedCategories.Add(categoryTreeItem.Category);
		}

		protected virtual void ResolveBlockCategoryWithChildren(Collection<ICategoryView> resolvedCategories, ICategoryTree categoryTree, IBlockBrandTopListCategory blockBrandTopListCategory)
		{
			if (resolvedCategories == null)
			{
				throw new ArgumentNullException("resolvedCategories");
			}

			if (categoryTree == null)
			{
				throw new ArgumentNullException("categoryTree");
			}

			ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(blockBrandTopListCategory.Category.Id);
			if (categoryTreeItem == null)
			{
				return;
			}

			if (blockBrandTopListCategory.IncludeSubcategories)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, categoryTreeItem);
			}
			else
			{
				resolvedCategories.Add(categoryTreeItem.Category);
			}
		}

		protected virtual void ResolveBlockCategoryWithChildren(Collection<ICategoryView> resolvedCategories, ICategoryTreeItem categoryTreeItem)
		{
			if (resolvedCategories == null)
			{
				throw new ArgumentNullException("resolvedCategories");
			}

			if (categoryTreeItem == null)
			{
				throw new ArgumentNullException("categoryTreeItem");
			}

			resolvedCategories.Add(categoryTreeItem.Category);

			foreach (ICategoryTreeItem child in categoryTreeItem.Children)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, child);
			}
		}
	}
}