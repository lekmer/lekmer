﻿using System;
using Litium.Framework.Messaging;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Common.Web.BlockContactUs
{
	public class ContactUsMessager : ScensumMessengerBase<ContactUsForm>
    {
		protected override Message CreateMessage(ContactUsForm messageArgs)
        {
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");

            EmailMessage message = new EmailMessage
									{
									  Body = messageArgs.Message,
									  FromEmail = messageArgs.SenderEmail,
                                      FromName = messageArgs.SenderName,
									  Subject = messageArgs.Subject,
									  ProviderName = "email",
									  Type = MessageType.Single
									};
			
			message.Recipients.Add(new EmailRecipient
			                       	{
			                       		Name = messageArgs.ReceiverEmail,
										Address = messageArgs.ReceiverEmail
			                       	});
            return message;
        }

        protected override string MessengerName
        {
            get { return "BlockContactUsControl"; }
        }
    }
}
