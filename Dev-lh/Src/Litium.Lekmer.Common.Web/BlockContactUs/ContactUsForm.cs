﻿using System.Collections.Specialized;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web.BlockContactUs
{
	public class ContactUsForm : ControlBase
	{
		private const int _maxMessageLength = 500;

		private readonly string _postUrl;
		private readonly string _receiverEmail;

		public ContactUsForm(string contactUsPostUrl, string receiverEmail)
		{
			_postUrl = contactUsPostUrl;
			_receiverEmail = receiverEmail;
		}

		public string SenderNameFormName
		{
			get { return "contactus-sendername"; }
		}

		public string SenderEmailFormName
		{
			get { return "contactus-senderemail"; }
		}

		public string SubjectFormName
		{
			get { return "contactus-subject"; }
		}

		public string MessageFormName
		{
			get { return "contactus-message"; }
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "contactus"; }
		}

		public string ReceiverEmail
		{
			get { return _receiverEmail; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string SenderName { get; set; }

		public string SenderEmail { get; set; }

		public string Subject { get; set; }
		
		public string Message { get; set; }


		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			SenderName = form[SenderNameFormName];
			SenderEmail = form[SenderEmailFormName];
			Subject = form[SubjectFormName];
			Message = form[MessageFormName];
		}

		public void ClearFrom()
		{
			SenderName = string.Empty;
			SenderEmail = string.Empty;
			Subject = string.Empty;
			Message = string.Empty;
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.SenderName.Name", SenderNameFormName);
			fragment.AddVariable("Form.SenderEmail.Name", SenderEmailFormName);
			fragment.AddVariable("Form.Subject.Name", SubjectFormName);
			fragment.AddVariable("Form.Message.Name", MessageFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.SenderName.Value", SenderName);
			fragment.AddVariable("Form.SenderEmail.Value", SenderEmail);
			fragment.AddVariable("Form.Subject.Value", Subject);
			fragment.AddVariable("Form.Message.Value", Message);
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();       
			ValidateSenderName(validationResult);
			ValidateSenderEmail(validationResult);
			ValidateSubject(validationResult);
			ValidateMessage(validationResult);
			return validationResult;
		}

		private void ValidateSenderName(ValidationResult validationResult)
		{
			if (SenderName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.ContactUs.Validation.SenderNameEmpty"));
			}
		}

		private void ValidateSenderEmail(ValidationResult validationResult)
		{
			if (SenderEmail.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.ContactUs.Validation.SenderEmailEmpty"));
			}
			else if (!ValidationUtil.IsValidEmail(SenderEmail))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.ContactUs.Validation.SenderEmailIncorrect"));
			}
		}

		private void ValidateSubject(ValidationResult validationResult)
		{
			if (Subject.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.ContactUs.Validation.SubjectEmpty"));
			}
		}

		private void ValidateMessage(ValidationResult validationResult)
		{
			if (Message.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.ContactUs.Validation.MessageEmpty"));
			}
			else if (Message.Length > _maxMessageLength)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.ContactUs.Validation.MessageToLong"));
			}
		}
	}
}