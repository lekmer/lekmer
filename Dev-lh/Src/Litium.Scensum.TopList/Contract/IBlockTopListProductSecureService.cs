using System.Collections.ObjectModel;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListProductSecureService
	{
		Collection<IBlockTopListProduct> GetAllByBlock(int channelId, int blockId);
		void Save(int blockId, Collection<IBlockTopListProduct> blockProducts);
	}
}