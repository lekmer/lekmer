using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class BlockTopListCategoryService : IBlockTopListCategoryService
	{
		private readonly ICategoryService _categoryService;
		protected BlockTopListCategoryRepository Repository { get; private set; }

		public BlockTopListCategoryService(BlockTopListCategoryRepository repository, ICategoryService categoryService)
		{
			Repository = repository;
			_categoryService = categoryService;
		}

		public virtual Collection<ICategoryView> GetViewAllByBlock(IUserContext context, IBlockTopList block)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");
			if (_categoryService == null)
				throw new InvalidOperationException(
					"_categoryService must be set before calling GetViewAllByBlock.");

			ICategoryTree categoryTree = _categoryService.GetAllAsTree(context);
			Collection<IBlockTopListCategory> blockTopListCategories = GetAllByBlock(context, block.Id);
			var resolvedCategories = new Collection<ICategoryView>();
			foreach (IBlockTopListCategory blockTopListCategory in blockTopListCategories)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, categoryTree, blockTopListCategory);
			}
			return resolvedCategories;
		}

		private Collection<IBlockTopListCategory> GetAllByBlock(IUserContext context, int blockId)
		{
			return Repository.GetAllByBlock(context.Channel.Language.Id, blockId);
		}

		private static void ResolveBlockCategoryWithChildren(Collection<ICategoryView> resolvedCategories,
		                                                     ICategoryTree categoryTree,
		                                                     IBlockTopListCategory blockTopListCategory)
		{
			if (resolvedCategories == null) throw new ArgumentNullException("resolvedCategories");
			if (categoryTree == null) throw new ArgumentNullException("categoryTree");

			ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(blockTopListCategory.Category.Id);
			if (categoryTreeItem == null) return;

			if (blockTopListCategory.IncludeSubcategories)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, categoryTreeItem);
			}
			else
			{
				resolvedCategories.Add(categoryTreeItem.Category);
			}
		}

		private static void ResolveBlockCategoryWithChildren(Collection<ICategoryView> resolvedCategories,
		                                                     ICategoryTreeItem categoryTreeItem)
		{
			if (resolvedCategories == null) throw new ArgumentNullException("resolvedCategories");
			if (categoryTreeItem == null) throw new ArgumentNullException("categoryTreeItem");

			resolvedCategories.Add(categoryTreeItem.Category);

			foreach (ICategoryTreeItem child in categoryTreeItem.Children)
			{
				ResolveBlockCategoryWithChildren(resolvedCategories, child);
			}
		}
	}
}