using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	public sealed class TopListProductCollectionCache :
		ScensumCacheBase<TopListProductCollectionKey, ProductIdCollection>
	{
		#region Singleton

		private static readonly TopListProductCollectionCache _instance =
			new TopListProductCollectionCache();

		private TopListProductCollectionCache()
		{
		}

		public static TopListProductCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class TopListProductCollectionKey : ICacheKey
	{
		public TopListProductCollectionKey(int channelId, int blockId, int pageNumber, int pageSize)
		{
			ChannelId = channelId;
			BlockId = blockId;
			PageNumber = pageNumber;
			PageSize = pageSize;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }
		public int PageNumber { get; set; }
		public int PageSize { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId + "-" + PageNumber + "-" + PageSize; }
		}
	}
}