using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class BlockTopListSecureService : IBlockTopListSecureService, IBlockCreateSecureService,
	                                         IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockTopListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IBlockTopListCategorySecureService BlockTopListCategorySecureService { get; private set; }
		protected IBlockTopListProductSecureService BlockTopListProductSecureService { get; private set; }


		public BlockTopListSecureService(
			IAccessValidator accessValidator,
			BlockTopListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IBlockTopListCategorySecureService blockTopListCategorySecureService,
			IBlockTopListProductSecureService blockTopListProductSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			BlockTopListCategorySecureService = blockTopListCategorySecureService;
			BlockTopListProductSecureService = blockTopListProductSecureService;
		}

		public virtual IBlockTopList Create()
		{
			if (AccessSecureService == null)
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockTopList = IoC.Resolve<IBlockTopList>();
			blockTopList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockTopList.Setting = BlockSettingSecureService.Create();
			blockTopList.Status = BusinessObjectStatus.New;
			return blockTopList;
		}

		public virtual IBlockTopList GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockTopList = Create();
			blockTopList.ContentNodeId = contentNodeId;
			blockTopList.ContentAreaId = contentAreaId;
			blockTopList.BlockTypeId = blockTypeId;
			blockTopList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockTopList.IncludeAllCategories = true;
			blockTopList.OrderStatisticsDayCount = 30;
			blockTopList.Title = title;
			blockTopList.TemplateId = null;
			blockTopList.Id = Save(systemUserFull, blockTopList);
			return blockTopList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockTopList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null)
				throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			BlockTopListCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IChannel channel, IBlockTopList block,
		                        Collection<IBlockTopListCategory> blockCategories,
		                        Collection<IBlockTopListProduct> blockProducts)
		{
			if (BlockTopListCategorySecureService == null)
				throw new InvalidOperationException("BlockTopListCategorySecureService must be set before calling Save.");
			if (BlockTopListProductSecureService == null)
				throw new InvalidOperationException("BlockTopListProductSecureService must be set before calling Save.");

			using (var transactedOperation = new TransactedOperation())
			{
				Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				BlockTopListCategorySecureService.Save(block.Id, blockCategories);
				BlockTopListProductSecureService.Save(block.Id, blockProducts);

				transactedOperation.Complete();
			}
			BlockTopListCache.Instance.Remove(block.Id);
			TopListProductCollectionCache.Instance.Flush();
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}
			BlockTopListCache.Instance.Remove(blockId);
		}
	}
}