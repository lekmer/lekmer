﻿using Litium.Scensum.Core;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class BlockTopListService : IBlockTopListService
	{
		protected BlockTopListRepository Repository { get; private set; }

		public BlockTopListService(BlockTopListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockTopList GetById(IUserContext context, int id)
		{
			return BlockTopListCache.Instance.TryGetItem(
				new BlockTopListKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockTopList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}