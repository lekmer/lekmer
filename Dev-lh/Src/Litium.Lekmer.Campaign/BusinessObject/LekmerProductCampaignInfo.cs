using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerProductCampaignInfo : ProductCampaignInfo, ILekmerProductCampaignInfo
	{
		private Collection<IProductActionAppliedItem> _campaignActionsApplied;
		public new Collection<IProductActionAppliedItem> CampaignActionsApplied
		{
			get { return _campaignActionsApplied ?? (_campaignActionsApplied = new Collection<IProductActionAppliedItem>()); }
		}

		Collection<IProductAction> IProductCampaignInfo.CampaignActionsApplied
		{
			get { throw new InvalidOperationException("IProductCampaignInfo.CampaignActionsApplied is not supported"); }
		}

		/// <summary>
		/// Action that affect price to set the minimum price of product.
		/// </summary>
		public IProductActionAppliedItem AffectedPriceAction { get; set; }

		/// <summary>
		/// Set new AffectedPriceAction and remove old one from applied action list.
		/// </summary>
		/// <param name="newAffectedPriceAction">New action that affect price to set the minimum price of product.</param>
		public virtual void UpdateAppliedActions(IProductAction newAffectedPriceAction)
		{
			if (AffectedPriceAction != null)
			{
				CampaignActionsApplied.Remove(AffectedPriceAction);
			}

			var productActionService = (ILekmerProductActionService) IoC.Resolve<IProductActionService>();
			AffectedPriceAction = productActionService.CreateProductActionAppliedItem().Cast(newAffectedPriceAction);
		}
	}
}