﻿using System;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class FreeFreightFlagAction : LekmerProductAction, IFreeFreightFlagAction
	{
		private LekmerCurrencyValueDictionary _productPrices;

		public LekmerCurrencyValueDictionary ProductPrices
		{
			get { return _productPrices; }
			set
			{
				CheckChanged(_productPrices, value);
				_productPrices = value;
			}
		}

		public override bool Apply(IUserContext context, IProduct product)
		{
			if (!Verify(product))
			{
				return false;
			}

			decimal price;
			if (!ProductPrices.TryGetValue(context.Channel.Currency.Id, out price))
			{
				return false;
			}

			if (product.CampaignInfo.Price.IncludingVat < price)
			{
				return false;
			}

			return true;
		}

		public override bool Verify(IProduct product)
		{
			if (product == null) throw new ArgumentNullException("product");
			if (ProductPrices == null) throw new InvalidOperationException("The ProductPrices property must be set.");

			return base.Verify(product);
		}

		public override object[] GetInfoArguments()
		{
			return new object[] {};
		}


		protected override bool IsTargetType(IProduct product)
		{
			return true; //Do not need separate by product type, cart action dont do this as well.
		}
	}
}