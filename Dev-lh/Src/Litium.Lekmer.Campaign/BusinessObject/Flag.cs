﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class Flag : BusinessObjectBase, IFlag
	{
		private int _id;
		private string _title;
		private string _class;
		private int _ordinal;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Class
		{
			get { return _class; }
			set
			{
				CheckChanged(_class, value);
				_class = value;
			}
		}

		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
	}
}