using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class VoucherDiscountAction : LekmerCartAction, IVoucherDiscountAction
	{
		public decimal? DiscountValue { get; set; }
		public bool FixedDiscount { get; set; }
		public bool PercentageDiscount { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary Amounts { get; set; }
		public bool AllowSpecialOffer { get; set; }

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			var lekmerUserContext = (ILekmerUserContext)context;
			var voucher = lekmerUserContext.Voucher;
			if (voucher != null && voucher.IsValid)
			{
				//decimal discountValue;
				//if (PercentageDiscount)
				//{
				//    if (DiscountValue.HasValue)
				//    {
				//        discountValue = DiscountValue.Value;
				//    }
				//    else
				//    {
				//        return false;
				//    }
				//}
				//else
				//{
				//    CurrencyValue currencyValue;
				//    if (Amounts.TryGetValue(context.Channel.Currency.Id, out currencyValue))
				//    {
				//        discountValue = currencyValue.MonetaryValue;
				//    }
				//    else
				//    {
				//        return false;
				//    }
				//}

				if (voucher.ValueType == VoucherValueType.Code)
				{
					return false; // Ignore "Code" vouchers
				}

				if (voucher.SpecialOffer && AllowSpecialOffer == false)
				{
					return false; // Ignore "SpecialOffer" vouchers
				}

				var lekmerCart = (ILekmerCartFull)cart;

				decimal discountValue = voucher.ValueType == VoucherValueType.GiftCard ? voucher.AmountLeft : voucher.DiscountValue;

				var cartVoucherInfo = lekmerCart.VoucherInfo;
				cartVoucherInfo.VoucherCode = voucher.VoucherCode;
				cartVoucherInfo.DiscountValue = discountValue;
				cartVoucherInfo.DiscountType = voucher.ValueType;
				return true;
			}
			return false;
		}

		public override object[] GetInfoArguments()
		{
			string discountType = FixedDiscount ? "fixed" : "percentage";
			decimal? discountAmount = PercentageDiscount ? DiscountValue : Amounts.Any() ? Amounts.FirstOrDefault().Value : (decimal?) null;
			return new object[] { discountType, discountAmount };
		}
	}
}