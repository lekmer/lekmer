﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignRegistryCampaignLandingPage : BusinessObjectBase, ICampaignRegistryCampaignLandingPage
	{
		private int _campaignLandingPageId;
		public int CampaignLandingPageId
		{
			get { return _campaignLandingPageId; }
			set
			{
				CheckChanged(_campaignLandingPageId, value);
				_campaignLandingPageId = value;
			}
		}

		private int _campaignRegistryId;
		public int CampaignRegistryId
		{
			get { return _campaignRegistryId; }
			set
			{
				CheckChanged(_campaignRegistryId, value);
				_campaignRegistryId = value;
			}
		}

		private int? _iconMediaId;
		public int? IconMediaId
		{
			get { return _iconMediaId; }
			set
			{
				CheckChanged(_iconMediaId, value);
				_iconMediaId = value;
			}
		}

		private int? _imageMediaId;
		public int? ImageMediaId
		{
			get { return _imageMediaId; }
			set
			{
				CheckChanged(_imageMediaId, value);
				_imageMediaId = value;
			}
		}

		private int _contentNodeId;
		public int ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}
	}
}