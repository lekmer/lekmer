﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class VoucherCondition : Condition, IVoucherCondition
	{
		private bool _includeAllBatchIds;
		private Collection<int> _batchIdsInclude;
		private Collection<int> _batchIdsExclude;

		public bool IncludeAllBatchIds
		{
			get { return _includeAllBatchIds; }
			set
			{
				CheckChanged(_includeAllBatchIds, value);
				_includeAllBatchIds = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<int> BatchIdsInclude
		{
			get { return _batchIdsInclude; }
			set
			{
				CheckChanged(_batchIdsInclude, value);
				_batchIdsInclude = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<int> BatchIdsExclude
		{
			get { return _batchIdsExclude; }
			set
			{
				CheckChanged(_batchIdsExclude, value);
				_batchIdsExclude = value;
			}
		}

		public override bool Fulfilled(IUserContext context)
		{
			IVoucherCheckResult voucher = ResolveVoucherSession().Voucher;

			if (voucher == null || !voucher.IsValid || !voucher.VoucherbatchId.HasValue)
			{
				return false;
			}

			if (BatchIdsExclude.Contains(voucher.VoucherbatchId.Value))
			{
				return false;
			}

			if (IncludeAllBatchIds)
			{
				return true;
			}

			if (BatchIdsInclude.Contains(voucher.VoucherbatchId.Value))
			{
				return true;
			}

			return false;
		}

		public override object[] GetInfoArguments()
		{
			return new object[] { string.Empty };
		}

		protected virtual IVoucherSession ResolveVoucherSession()
		{
			return IoC.Resolve<IVoucherSession>();
		}
	}
}
