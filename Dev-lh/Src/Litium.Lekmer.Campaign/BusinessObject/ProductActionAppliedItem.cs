﻿using System;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class ProductActionAppliedItem : BusinessObjectBase, IProductActionAppliedItem
	{
		private int _id;
		private int _productCampaignId;
		private string _actionCommonName;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}
		public int ProductCampaignId
		{
			get { return _productCampaignId; }
			set
			{
				CheckChanged(_productCampaignId, value);
				_productCampaignId = value;
			}
		}
		public string ActionCommonName
		{
			get { return _actionCommonName; }
			set
			{
				CheckChanged(_actionCommonName, value);
				_actionCommonName = value;
			}
		}

		public IProductActionAppliedItem Cast(IProductAction productAction)
		{
			Id = productAction.Id;
			ProductCampaignId = productAction.ProductCampaignId;
			ActionCommonName = productAction.ActionType.CommonName;

			return this;
		}
	}
}