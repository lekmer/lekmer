﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignLevel : BusinessObjectBase, ICampaignLevel
	{
		private int _id;
		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		private string _title;
		public string Title
		{
			get { return _title; }
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				CheckChanged(_title, value);
				_title = value;
			}
		}

		private int _priority;
		public int Priority
		{
			get { return _priority; }
			set
			{
				CheckChanged(_priority, value);
				_priority = value;
			}
		}
	}
}