﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignPriceType : BusinessObjectBase, ICampaignPriceType
	{
		private int _id;
		private string _commonName;
		private string _title;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				CheckChanged(_title, value);
				_title = value;
			}
		}
	}
}