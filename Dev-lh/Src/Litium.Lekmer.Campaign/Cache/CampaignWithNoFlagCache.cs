﻿using System;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Campaign.Cache
{
	public sealed class CampaignWithNoFlagCache : ScensumCacheBase<CampaignWithNoFlagKey, CampaignWithNoFlag>
	{
		private static readonly CampaignWithNoFlagCache _instance = new CampaignWithNoFlagCache();

		private CampaignWithNoFlagCache()
		{
		}

		public static CampaignWithNoFlagCache Instance
		{
			get { return _instance; }
		}
	}

	public class CampaignWithNoFlagKey : ICacheKey
	{
		public CampaignWithNoFlagKey(int campaignId)
		{
			CampaignId = campaignId;
		}

		public int CampaignId { get; set; }

		public string Key
		{
			get { return string.Concat("id_", CampaignId); }
		}
	}

	[Serializable]
	public class CampaignWithNoFlag {}
}
