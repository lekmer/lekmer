﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Campaign.Cache
{
	public sealed class CampaignFlagCache : ScensumCacheBase<CampaignFlagKey, IFlag>
	{
		private static readonly CampaignFlagCache _instance = new CampaignFlagCache();

		private CampaignFlagCache()
		{
		}

		public static CampaignFlagCache Instance
		{
			get { return _instance; }
		}

		public override void Remove(CampaignFlagKey key)
		{
			base.Remove(key);

			CampaignWithNoFlagCache.Instance.Remove(new CampaignWithNoFlagKey(key.CampaignId));
		}

		public override void Flush()
		{
			base.Flush();

			CampaignWithNoFlagCache.Instance.Flush();
		}
	}

	public class CampaignFlagKey : ICacheKey
	{
		public CampaignFlagKey(int languageId, int campaignId)
		{
			LanguageId = languageId;
			CampaignId = campaignId;
		}

		public int LanguageId { get; set; }

		public int CampaignId { get; set; }

		public string Key
		{
			get { return string.Format(CultureInfo.InvariantCulture, "l_{0}-id_{1}", LanguageId, CampaignId); }
		}
	}
}
