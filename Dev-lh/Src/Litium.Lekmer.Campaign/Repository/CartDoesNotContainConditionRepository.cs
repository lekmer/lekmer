﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartDoesNotContainConditionRepository
	{
		protected virtual DataMapperBase<ICartDoesNotContainCondition> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartDoesNotContainCondition>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(ICartDoesNotContainCondition condition)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", condition.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ConfigId", condition.CampaignConfigId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartDoesNotContainConditionRepository.Save");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartDoesNotContainConditionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartDoesNotContainConditionRepository.Delete");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartDoesNotContainConditionDelete]", parameters, dbSettings);
		}

		public virtual ICartDoesNotContainCondition GetById(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartDoesNotContainConditionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartDoesNotContainConditionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}