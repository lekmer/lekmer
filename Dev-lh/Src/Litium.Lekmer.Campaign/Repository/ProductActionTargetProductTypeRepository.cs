﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class ProductActionTargetProductTypeRepository
	{
		protected virtual DataMapperBase<IProductActionTargetProductType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductActionTargetProductType>(dataReader);
		}

		public virtual Collection<IProductActionTargetProductType> GetAllByCampaign(int campaignId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductActionTargetProductTypeRepository.GetAllByCampaign");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pProductActionTargetProductTypeGetAllByCampaign]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Save(int productActionId, string targetProductTypeIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TargetProductTypeIds", targetProductTypeIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductActionTargetProductTypeRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pProductActionTargetProductTypeSave]", parameters, dbSettings);
		}

		public virtual void Delete(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductActionTargetProductTypeRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pProductActionTargetProductTypeDelete]", parameters, dbSettings);
		}
	}
}