﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartItemsGroupValueConditionRepository
	{
		protected virtual DataMapperBase<ICartItemsGroupValueCondition> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemsGroupValueCondition>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(ICartItemsGroupValueCondition condition)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", condition.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ConfigId", condition.CampaignConfigId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemsGroupValueConditionRepository.Save");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemsGroupValueConditionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemsGroupValueConditionRepository.Delete");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemsGroupValueConditionDelete]", parameters, dbSettings);
		}

		public virtual ICartItemsGroupValueCondition GetById(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemsGroupValueConditionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemsGroupValueConditionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// CurrencyValue.
		public virtual void SaveCurrencyValue(int conditionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", currencyValue.Value, SqlDbType.Decimal)
				};
			var dbSettings = new DatabaseSetting("CartItemsGroupValueConditionRepository.SaveCurrencyValue");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemsGroupValueConditionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemsGroupValueConditionRepository.DeleteAllCurrencyValue");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByCondition(int conditionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemsGroupValueConditionRepository.GetCurrencyValuesByCondition");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemsGroupValueConditionCurrencyGetByCondition]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}
	}
}