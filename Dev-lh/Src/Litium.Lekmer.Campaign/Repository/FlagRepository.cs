﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class FlagRepository
	{
		protected virtual DataMapperBase<IFlag> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IFlag>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual IFlag GetByIdSecure(int flagId)
		{
			var dbSettings = new DatabaseSetting("FlagRepository.GetByIdSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FlagId", flagId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IFlag GetByCampaignSecure(int campaignId)
		{
			var dbSettings = new DatabaseSetting("FlagRepository.GetByCampaignSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CampaignId", campaignId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagGetByCampaignSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IFlag GetByCampaignFolderSecure(int folderId)
		{
			var dbSettings = new DatabaseSetting("FlagRepository.GetByCampaignFolderSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FolderId", folderId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagGetByCampaignFolderSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IFlag> GetAllSecure()
		{
			var dbSettings = new DatabaseSetting("FlagRepository.GetAllSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagGetAllSecure]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual IFlag GetByCampaign(int languageId, int campaignId)
		{
			var dbSettings = new DatabaseSetting("FlagRepository.GetByCampaign");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CampaignId", campaignId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagGetByCampaign]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IFlag> GetByProduct(int languageId, int productId)
		{
			var dbSettings = new DatabaseSetting("FlagRepository.GetByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagGetByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(IFlag flag)
		{
			if (flag == null) throw new ArgumentNullException("flag");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FlagId", flag.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title", flag.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Class", flag.Class, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Ordinal", flag.Ordinal, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FlagRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pFlagSave]", parameters, dbSettings);
		}

		public virtual void SetOrder(IEnumerable<IFlag> flags)
		{
			const char separator = ',';
			string idList = string.Join(separator.ToString(CultureInfo.InvariantCulture), flags.Select(x => x.Id.ToString(CultureInfo.InvariantCulture)).ToArray());
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FlagIdList", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Separator", separator, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("FlagRepository.SetOrder");
			new DataHandler().ExecuteCommand("[lekmer].[pFlagSetOrdinal]", parameters, dbSettings);
		}

		public virtual void SaveCampaignFlag(int campaignId, int? flagId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CampaignId", campaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@FlagId", flagId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FlagRepository.SaveCampaignFlag");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCampaignFlagSave]", parameters, dbSettings);
		}

		public virtual void SaveCampaignFolderFlag(int campaignFolderId, int? flagId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FolderId", campaignFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@FlagId", flagId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FlagRepository.SaveCampaignFolderFlag");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCampaignFolderFlagSave]", parameters, dbSettings);
		}

		public virtual void Delete(int flagId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FlagId", flagId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FlagRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pFlagDelete]", parameters, dbSettings);
		}

		public virtual Collection<ITranslationGeneric> GetAllTranslationsByFlag(int flagId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FlagId", flagId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FlagRepository.GetAllTranslationsByFlag");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFlagTranslationGetAllByFlag]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void SaveTranslation(int flagId, int languageId, string title)
		{
			var dbSettings = new DatabaseSetting("FlagRepository.SaveTranslation");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@FlagId", flagId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title", title, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pFlagTranslationSave]", parameters, dbSettings);
		}
	}
}
