using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CampaignRegistryCampaignRepository
	{
		protected virtual DataMapperBase<ICampaignRegistryCampaign> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignRegistryCampaign>(dataReader);
		}

		public virtual Collection<ICampaignRegistryCampaign> GetRegistriesByCampaignId(int campaignId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CampaignRegistryCampaignRepository.GetRegistriesByCampaignId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignRegistryCampaignGetByCampaignId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Save(int campaignId, string campaignRegistryIdList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CampaignRegistryIds", campaignRegistryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("CampaignRegistryCampaignRepository.Save");
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignRegistryCampaignSave]", parameters, dbSettings);
		}

		public virtual void Delete(int campaignId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CampaignRegistryCampaignRepository.Delete");
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignRegistryCampaignDelete]", parameters, dbSettings);
		}
	}
}