using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class FixedDiscountCartActionRepository
	{
		protected virtual DataMapperBase<IFixedDiscountCartAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IFixedDiscountCartAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(IFixedDiscountCartAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountCartActionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountCartActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountCartActionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountCartActionDelete]", parameters, dbSettings);
		}

		public virtual IFixedDiscountCartAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountCartActionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountCartActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int cartActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("MonetaryValue", currencyValue.Value, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountCartActionRepository.SaveCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountCartActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountCartActionRepository.DeleteAllCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountCartActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int cartActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountCartActionRepository.GetCurrencyValuesByAction");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountCartActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}
	}
}