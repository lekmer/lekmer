﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class LekmerCartValueConditionRepository : CartValueConditionRepository
	{
		/// <summary>
		/// Gets currency value dictionary by condition Id.
		/// </summary>
		/// <param name="conditionId">Condition Id</param>
		/// <returns>The <see cref="LekmerCurrencyValueDictionary"/> object</returns>
		public new virtual LekmerCurrencyValueDictionary GetCartValuesByCondition(int conditionId)
		{
			IEnumerable<CurrencyValue> currencyValues;
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartValueConditionRepository.GetCurrencyValuesByCondition");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaign].[pCartValuesGetByConditionId]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}
			var dictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValue in currencyValues)
			{
				dictionary.Add(currencyValue.Currency.Id, currencyValue.MonetaryValue);
			}
			return dictionary;
		}

		/// <summary>
		/// Saves cart value
		/// </summary>
		public virtual void SaveCartValue(int conditionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CurrencyId", currencyValue.Key, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Value", currencyValue.Value, SqlDbType.Decimal)
			};
			var dbSettings = new DatabaseSetting("CartValueConditionRepository.SaveCartValue");
			new DataHandler().ExecuteCommand("[campaign].[pCartValueSave]", parameters, dbSettings);
		}
	}
}