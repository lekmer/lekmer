﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class FreeFreightFlagActionRepository
	{
		protected DataMapperBase<IFreeFreightFlagAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IFreeFreightFlagAction>(dataReader);
		}

		protected DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}

		public IFreeFreightFlagAction GetById(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Id", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FreeFreightFlagActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFreeFreightFlagActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public LekmerCurrencyValueDictionary GetProductPricesByAction(int actionId)
		{
			IEnumerable<CurrencyValue> currencyValues;
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FreeFreightFlagActionRepository.GetProductPricesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFreeFreightFlagActionCurrencyGetAllByActionId]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}
			var dictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValue in currencyValues)
			{
				dictionary.Add(currencyValue.Currency.Id, currencyValue.MonetaryValue);
			}
			return dictionary;
		}

		public void Save(IFreeFreightFlagAction action)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", action.Id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FreeFreightFlagActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pFreeFreightFlagActionSave]", parameters, dbSettings);
		}

		public virtual void SaveProductPrice(int actionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CurrencyId", currencyValue.Key, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Value", currencyValue.Value, SqlDbType.Decimal)
			};
			var dbSettings = new DatabaseSetting("FreeFreightFlagActionRepository.SaveProductPrice");
			new DataHandler().ExecuteCommand("[lekmer].[pFreeFreightFlagActionCurrencySave]", parameters, dbSettings);
		}

		public void Delete(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FreeFreightFlagActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pFreeFreightFlagActionDelete]", parameters, dbSettings);
		}

		public void DeleteAllProductPrices(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FreeFreightFlagActionRepository.DeleteAllProductPrices");
			new DataHandler().ExecuteCommand("[lekmer].[pFreeFreightFlagActionCurrencyDeleteAll]", parameters, dbSettings);
		}
	}
}
