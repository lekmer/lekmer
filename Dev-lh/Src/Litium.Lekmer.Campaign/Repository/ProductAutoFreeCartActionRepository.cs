using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class ProductAutoFreeCartActionRepository
	{
		protected virtual DataMapperBase<IProductAutoFreeCartAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductAutoFreeCartAction>(dataReader);
		}

		public virtual void Save(IProductAutoFreeCartAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", action.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Quantity", action.Quantity, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductAutoFreeCartActionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pProductAutoFreeCartActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductAutoFreeCartActionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pProductAutoFreeCartActionDelete]", parameters, dbSettings);
		}

		public virtual IProductAutoFreeCartAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductAutoFreeCartActionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pProductAutoFreeCartActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}