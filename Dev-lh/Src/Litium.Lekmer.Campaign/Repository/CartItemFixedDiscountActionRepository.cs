﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartItemFixedDiscountActionRepository
	{
		protected DataMapperBase<ICartItemFixedDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemFixedDiscountAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}

		public virtual void Save(ICartItemFixedDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllProducts", action.IncludeAllProducts, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionDelete]", parameters, dbSettings);
		}

		public virtual ICartItemFixedDiscountAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int cartActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("MonetaryValue", currencyValue.Value, SqlDbType.Decimal)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.SaveCurrencyValue");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteAllCurrencyValue");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int cartActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetCurrencyValuesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}


		// Products

		public virtual void InsertIncludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemFixedDiscountActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemFixedDiscountActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetIncludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetExcludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Categories

		public virtual void InsertIncludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemFixedDiscountActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemFixedDiscountActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CampaignCategoryDictionary GetIncludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetIncludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetIncludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetIncludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetExcludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetExcludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionExcludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}


		// Brands

		public virtual void InsertIncludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.InsertIncludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemFixedDiscountActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.InsertExcludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemFixedDiscountActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteIncludeBrands");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.DeleteExcludeBrands");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemFixedDiscountActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetIncludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemFixedDiscountActionRepository.GetExcludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemFixedDiscountActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}
