﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignRegistryCampaignLandingPageDataMapper : DataMapperBase<ICampaignRegistryCampaignLandingPage>
	{
		public CampaignRegistryCampaignLandingPageDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignRegistryCampaignLandingPage Create()
		{
			var campaignRegistryCampaignLandingPage = IoC.Resolve<ICampaignRegistryCampaignLandingPage>();

			campaignRegistryCampaignLandingPage.CampaignLandingPageId = MapValue<int>("CampaignRegistryCampaignLandingPage.CampaignLandingPageId");
			campaignRegistryCampaignLandingPage.CampaignRegistryId = MapValue<int>("CampaignRegistryCampaignLandingPage.CampaignRegistryId");
			campaignRegistryCampaignLandingPage.IconMediaId = MapNullableValue<int?>("CampaignRegistryCampaignLandingPage.IconMediaId");
			campaignRegistryCampaignLandingPage.ImageMediaId = MapNullableValue<int?>("CampaignRegistryCampaignLandingPage.ImageMediaId");
			campaignRegistryCampaignLandingPage.ContentNodeId = MapValue<int>("CampaignRegistryCampaignLandingPage.ContentNodeId");

			return campaignRegistryCampaignLandingPage;
		}
	}
}