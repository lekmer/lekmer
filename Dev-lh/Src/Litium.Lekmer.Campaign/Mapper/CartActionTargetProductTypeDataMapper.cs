﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartActionTargetProductTypeDataMapper : DataMapperBase<ICartActionTargetProductType>
	{
		public CartActionTargetProductTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICartActionTargetProductType Create()
		{
			var targetProductType = IoC.Resolve<ICartActionTargetProductType>();

			targetProductType.CartActionId = MapValue<int>("CartActionTargetProductType.CartActionId");
			targetProductType.ProductTypeId = MapValue<int>("CartActionTargetProductType.ProductTypeId");

			targetProductType.SetUntouched();

			return targetProductType;
		}
	}
}