﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class ProductActionTargetProductTypeDataMapper : DataMapperBase<IProductActionTargetProductType>
	{
		public ProductActionTargetProductTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductActionTargetProductType Create()
		{
			var targetProductType = IoC.Resolve<IProductActionTargetProductType>();

			targetProductType.ProductActionId = MapValue<int>("ProductActionTargetProductType.ProductActionId");
			targetProductType.ProductTypeId = MapValue<int>("ProductActionTargetProductType.ProductTypeId");

			targetProductType.SetUntouched();

			return targetProductType;
		}
	}
}