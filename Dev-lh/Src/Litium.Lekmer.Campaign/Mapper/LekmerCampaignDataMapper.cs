﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class LekmerCampaignDataMapper<T> : DataMapperBase<T>
		where T : class, ICampaign
	{
		private int _id;
		private int _title;
		private int _folderId;
		private int _startDate;
		private int _endDate;

		private DataMapperBase<ICampaignStatus> _campaignStatusDataMapper;

		public LekmerCampaignDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_campaignStatusDataMapper = DataMapperResolver.Resolve<ICampaignStatus>(DataReader);

			_id = OrdinalOf("Campaign.Id");
			_folderId = OrdinalOf("Campaign.FolderId");
			_title = OrdinalOf("Campaign.Title");
			_startDate = OrdinalOf("Campaign.StartDate");
			_endDate = OrdinalOf("Campaign.EndDate");
		}

		protected override T Create()
		{
			var campaign = IoC.Resolve<T>();
			campaign.Id = MapValue<int>(_id);
			campaign.FolderId = MapValue<int>(_folderId);
			campaign.CampaignStatus = _campaignStatusDataMapper.MapRow();
			campaign.Title = MapValue<string>(_title);
			campaign.StartDate = MapNullableValue<DateTime?>(_startDate);
			campaign.EndDate = MapNullableValue<DateTime?>(_endDate);
			campaign.Priority = MapValue<int>("Campaign.Priority");
			campaign.Exclusive = MapValue<bool>("Campaign.Exclusive");
			campaign.Status = BusinessObjectStatus.Untouched;

			var lekmerCampaign = campaign as ILekmerCampaign;
			if (lekmerCampaign != null)
			{
				lekmerCampaign.LevelId = MapValue<int>("Campaign.LevelId");
				lekmerCampaign.PriceTypeId = MapNullableValue<int?>("Campaign.PriceTypeId");
				lekmerCampaign.TagId = MapNullableValue<int?>("Campaign.TagId");
				/* AUTO-CAMPAIGN */
				lekmerCampaign.UseLandingPage = MapValue<bool>("Campaign.UseLandingPage");
			}

			return campaign;
		}
	}
}