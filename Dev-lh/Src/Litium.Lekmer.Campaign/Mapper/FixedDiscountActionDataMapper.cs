﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class FixedDiscountActionDataMapper : ProductActionDataMapper<IFixedDiscountAction>
	{
		public FixedDiscountActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IFixedDiscountAction Create()
		{
			var fixedDiscountAction = base.Create();

			fixedDiscountAction.IncludeAllProducts = MapValue<bool>("FixedDiscountAction.IncludeAllProducts");

			return fixedDiscountAction;
		}
	}
}