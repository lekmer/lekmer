﻿using System;
using Litium.Lekmer.Order;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCartCampaignProcessor : CartCampaignProcessor
	{
		public LekmerCartCampaignProcessor(ICartCampaignInfoFactory cartCampaignInfoFactory, ICartCampaignService cartCampaignService)
			: base(cartCampaignInfoFactory, cartCampaignService)
		{
		}

		public override void Process(IUserContext context, ICartFull cart)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (cart == null)
			{
				throw new ArgumentNullException("cart");
			}

			var lekmerCartFull = (ILekmerCartFull)cart;

			lekmerCartFull.DeleteItemsAffectedByCampaign();

			// Initialize cart voucher info
			lekmerCartFull.VoucherInfo = IoC.Resolve<ICartVoucherInfo>();

			base.Process(context, cart);
		}
	}
}