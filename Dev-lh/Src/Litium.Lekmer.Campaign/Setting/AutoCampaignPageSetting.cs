﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Campaign.Setting
{
	public sealed class AutoCampaignPageSetting : SettingBase
	{
		private static readonly AutoCampaignPageSetting _instance = new AutoCampaignPageSetting();

		private AutoCampaignPageSetting()
		{
		}

		public static AutoCampaignPageSetting Instance
		{
			get { return _instance; }
		}

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "LekmerAutoCampaign"; }
		}

		public int CampaignTagGroupId
		{
			get { return GetInt32(_groupName, "CampaignTagGroupId"); }
		}

		public int UseMasterPageId
		{
			get { return GetInt32(_groupName, "UseMasterPageId"); }
		}

		public int ProductFilterPrimaryTemplateId
		{
			get { return GetInt32(_groupName, "ProductFilterPrimaryTemplateId"); }
		}

		public int ProductFilterSecondaryTemplateId
		{
			get { return GetInt32(_groupName, "ProductFilterSecondaryTemplateId"); }
		}

		public int AutoCampaignParentPage(int channelId)
		{
			return GetInt32(_groupName, "AutoCampaignParentPage_" + channelId);
		}

		public int MaximumAttemptsAtCommonName
		{
			get { return GetInt32(_groupName, "MaximumAttemptsAtCommonName"); }
		}
	}
}