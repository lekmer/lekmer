﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Campaign.Setting
{
	public sealed class FlagSetting : SettingBase
	{
		private static readonly FlagSetting _instance = new FlagSetting();

		private FlagSetting()
		{
		}

		public static FlagSetting Instance
		{
			get { return _instance; }
		}

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "LekmerCampaignFlag"; }
		}

		public int MaxFlagsToShow
		{
			get { return GetInt32(_groupName, "MaxFlagsToShow", 3); }
		}
	}
}
