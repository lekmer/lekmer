﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CartActionTargetProductTypeSecureService : ICartActionTargetProductTypeSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartActionTargetProductTypeRepository Repository { get; private set; }

		public CartActionTargetProductTypeSecureService(
			IAccessValidator accessValidator,
			CartActionTargetProductTypeRepository cartActionTargetProductTypeRepository)
		{
			AccessValidator = accessValidator;
			Repository = cartActionTargetProductTypeRepository;
		}

		public virtual Collection<ICartActionTargetProductType> GetAllByCampaign(int campaignId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCampaign(campaignId);
		}

		public virtual void Save(ISystemUserFull systemUserFull, ICartAction cartAction)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			var lekmerCartAction = (ILekmerCartAction) cartAction;

			string targetProductTypeIds = Convert.ToStringIdentifierList(lekmerCartAction.TargetProductTypes.Keys);

			Repository.Save(cartAction.Id, targetProductTypeIds, Convert.DefaultListSeparator);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int cartActionId)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			Repository.Delete(cartActionId);
		}
	}
}
