﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class LekmerFreightValueActionSecureService : FreightValueActionSecureService
	{
		protected LekmerFreightValueActionRepository LekmerRepository { get; private set; }

		public LekmerFreightValueActionSecureService(
			IAccessValidator accessValidator,
			LekmerFreightValueActionRepository repository,
			ICartActionTypeSecureService cartActionTypeSecureService)
			: base(accessValidator, repository, cartActionTypeSecureService)
		{
			LekmerRepository = repository;
		}

		/// <summary>
		/// Creates new <see cref="IFreightValueAction"/> object.
		/// </summary>
		/// <returns>The <see cref="IFreightValueAction"/> object</returns>
		public override ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");

			var action = IoC.Resolve<IFreightValueAction>() as ILekmerFreightValueAction;
			if (action != null)
			{
				action.ActionType = CartActionTypeSecureService.GetByCommonName("FreightValue");
				action.Amounts = new LekmerCurrencyValueDictionary();
				action.DeliveryMethodIds = new IdDictionary();
				action.Status = BusinessObjectStatus.New;
			}

			return action;
		}

		/// <summary>
		/// Gets <see cref="IFreightValueAction"/> object by id.
		/// </summary>
		/// <param name="id">Action id</param>
		/// <returns>The <see cref="IFreightValueAction"/> object</returns>
		public override ICartAction GetById(int id)
		{
			if (LekmerRepository == null)
			{
				throw new InvalidOperationException("LekmerRepository cannot be null.");
			}

			var action = Repository.GetById(id);
			if (action != null)
			{
				var lekmerAction = (ILekmerFreightValueAction) action;
				lekmerAction.Amounts = LekmerRepository.GetCartValuesByAction(id);
				lekmerAction.DeliveryMethodIds = LekmerRepository.GetDeliveryMethods(id);
				lekmerAction.Status = BusinessObjectStatus.Untouched;
			}

			return action;
		}

		/// <summary>
		/// Saves <see cref="IFreightValueAction"/> object.
		/// </summary>
		/// <param name="user">Logged in system user</param>
		/// <param name="action">Cart action</param>
		public override void Save(ISystemUserFull user, ICartAction action)
		{
			if (LekmerRepository == null)
			{
				throw new InvalidOperationException("LekmerRepository cannot be null.");
			}

			var freightAction = action as ILekmerFreightValueAction;
			if (freightAction == null)
			{
				throw new InvalidOperationException("action is not ILekmerFreightValueAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(freightAction);
				foreach (var amount in freightAction.Amounts)
				{
					LekmerRepository.SaveFreightValue(freightAction.Id, amount);
				}
				SaveDeliveryMethods(user, freightAction);
				transactedOperation.Complete();
			}
		}

		/// <summary>
		/// Deletes <see cref="IFreightValueAction"/> object.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="id">Action identifier</param>
		public override void Delete(ISystemUserFull systemUserFull, int id)
		{
			if (LekmerRepository == null)
			{
				throw new InvalidOperationException("LekmerRepository cannot be null.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				LekmerRepository.DeleteDeliveryMethods(id);

				base.Delete(systemUserFull, id);

				transactedOperation.Complete();
			}
		}


		protected virtual void SaveDeliveryMethods(ISystemUserFull user, ILekmerFreightValueAction freightValueAction)
		{
			LekmerRepository.DeleteDeliveryMethods(freightValueAction.Id);

			IdDictionary deliveryMethodIds = freightValueAction.DeliveryMethodIds;
			if (deliveryMethodIds == null || deliveryMethodIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> deliveryMethodId in deliveryMethodIds)
			{
				LekmerRepository.InsertDeliveryMethod(freightValueAction.Id, deliveryMethodId.Key);
			}
		}
	}
}