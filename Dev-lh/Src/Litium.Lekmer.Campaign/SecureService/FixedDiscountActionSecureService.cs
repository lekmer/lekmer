﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class FixedDiscountActionSecureService : IProductActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected FixedDiscountActionRepository Repository { get; private set; }
		protected IProductActionTypeSecureService ProductActionTypeSecureService { get; set; }

		public FixedDiscountActionSecureService(
			IAccessValidator accessValidator,
			FixedDiscountActionRepository fixedDiscountActionRepository,
			IProductActionTypeSecureService productActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = fixedDiscountActionRepository;
			ProductActionTypeSecureService = productActionTypeSecureService;
		}

		public IProductAction Create()
		{
			if (ProductActionTypeSecureService == null)
			{
				throw new InvalidOperationException("ProductActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IFixedDiscountAction>();

			action.ActionType = ProductActionTypeSecureService.GetByCommonName("FixedDiscount");
			action.Amounts = new LekmerCurrencyValueDictionary();
			action.IncludeProducts = new ProductIdDictionary();
			action.ExcludeProducts = new ProductIdDictionary();
			action.IncludeCategories = new CategoryIdDictionary();
			action.ExcludeCategories = new CategoryIdDictionary();
			action.IncludeBrands = new BrandIdDictionary();
			action.ExcludeBrands = new BrandIdDictionary();
			action.Status = BusinessObjectStatus.New;

			return action;
		}

		public void Save(ISystemUserFull user, IProductAction action)
		{
			Repository.EnsureNotNull();

			var fixedDiscountAction = action as IFixedDiscountAction;
			if (fixedDiscountAction == null)
			{
				throw new InvalidOperationException("action is not IFixedDiscountAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(fixedDiscountAction);

				SaveAmounts(fixedDiscountAction);

				SaveIncludeProducts(fixedDiscountAction);
				SaveExcludeProducts(fixedDiscountAction);
				SaveIncludeCategories(fixedDiscountAction);
				SaveExcludeCategories(fixedDiscountAction);
				SaveIncludeBrands(fixedDiscountAction);
				SaveExcludeBrands(fixedDiscountAction);

				transactedOperation.Complete();
			}
		}

		public void Delete(ISystemUserFull user, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public IProductAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);

			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(id);
			action.IncludeProducts = Repository.GetIncludeProducts(id);
			action.ExcludeProducts = Repository.GetExcludeProducts(id);
			action.IncludeCategories = Repository.GetIncludeCategories(id);
			action.ExcludeCategories = Repository.GetExcludeCategories(id);
			action.IncludeBrands = Repository.GetIncludeBrands(id);
			action.ExcludeBrands = Repository.GetExcludeBrands(id);

			return action;
		}


		protected virtual void SaveAmounts(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteAllCurrencyValue(fixedDiscountAction.Id);

			foreach (var amount in fixedDiscountAction.Amounts)
			{
				Repository.SaveCurrencyValue(fixedDiscountAction.Id, amount);
			}
		}


		protected virtual void SaveIncludeProducts(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteIncludeProducts(fixedDiscountAction.Id);

			if (fixedDiscountAction.IncludeAllProducts)
			{
				return;
			}

			ProductIdDictionary productIdDictionary = fixedDiscountAction.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertIncludeProduct(fixedDiscountAction.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveExcludeProducts(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteExcludeProducts(fixedDiscountAction.Id);

			ProductIdDictionary productIdDictionary = fixedDiscountAction.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertExcludeProduct(fixedDiscountAction.Id, productIdPair.Key);
			}
		}


		protected virtual void SaveIncludeCategories(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteIncludeCategories(fixedDiscountAction.Id);

			if (fixedDiscountAction.IncludeAllProducts)
			{
				return;
			}

			CategoryIdDictionary categoryIds = fixedDiscountAction.IncludeCategories;
			if (categoryIds == null || categoryIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> categoryId in categoryIds)
			{
				Repository.InsertIncludeCategory(fixedDiscountAction.Id, categoryId.Key);
			}
		}

		protected virtual void SaveExcludeCategories(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteExcludeCategories(fixedDiscountAction.Id);

			CategoryIdDictionary categoryIds = fixedDiscountAction.ExcludeCategories;
			if (categoryIds == null || categoryIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> categoryId in categoryIds)
			{
				Repository.InsertExcludeCategory(fixedDiscountAction.Id, categoryId.Key);
			}
		}


		protected virtual void SaveIncludeBrands(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteIncludeBrands(fixedDiscountAction.Id);

			if (fixedDiscountAction.IncludeAllProducts)
			{
				return;
			}

			BrandIdDictionary brandIds = fixedDiscountAction.IncludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertIncludeBrand(fixedDiscountAction.Id, brandId.Key);
			}
		}

		protected virtual void SaveExcludeBrands(IFixedDiscountAction fixedDiscountAction)
		{
			Repository.DeleteExcludeBrands(fixedDiscountAction.Id);

			BrandIdDictionary brandIds = fixedDiscountAction.ExcludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertExcludeBrand(fixedDiscountAction.Id, brandId.Key);
			}
		}
	}
}