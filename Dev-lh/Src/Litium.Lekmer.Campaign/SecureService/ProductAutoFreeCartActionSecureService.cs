﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class ProductAutoFreeCartActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected ProductAutoFreeCartActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; private set; }

		public ProductAutoFreeCartActionSecureService(
			IAccessValidator accessValidator,
			ProductAutoFreeCartActionRepository productAutoFreeCartActionRepository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = productAutoFreeCartActionRepository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
			{
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IProductAutoFreeCartAction>();

			action.ActionType = CartActionTypeSecureService.GetByCommonName("ProductAutoFree");
			action.Quantity = 0;
			action.Status = BusinessObjectStatus.New;

			return action;
		}

		public void Save(ISystemUserFull user, ICartAction action)
		{
			Repository.EnsureNotNull();

			var productAutoFreeCartAction = action as IProductAutoFreeCartAction;
			if (productAutoFreeCartAction == null)
			{
				throw new InvalidOperationException("action is not IProductAutoFreeCartAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(productAutoFreeCartAction);
				transactedOperation.Complete();
			}
		}

		public void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public ICartAction GetById(int id)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(id);
		}
	}
}