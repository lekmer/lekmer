﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class GiftCardViaMailInfoSecureService : IGiftCardViaMailInfoSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected GiftCardViaMailInfoRepository Repository { get; private set; }

		public GiftCardViaMailInfoSecureService(IAccessValidator accessValidator, GiftCardViaMailInfoRepository giftCardViaMailInfoRepository)
		{
			AccessValidator = accessValidator;
			Repository = giftCardViaMailInfoRepository;
		}

		public Collection<IGiftCardViaMailInfo> GetAllByOrder(int orderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByOrder(orderId);
		}

		public void DeleteByOrder(ISystemUserFull systemUserFull, int orderId)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			Repository.DeleteByOrder(orderId);
		}
	}
}