﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CartValueRangeConditionSecureService : IConditionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartValueRangeConditionRepository Repository { get; private set; }
		protected IConditionTypeSecureService ConditionTypeSecureService { get; private set; }

		public CartValueRangeConditionSecureService(
			IAccessValidator accessValidator,
			CartValueRangeConditionRepository repository,
			IConditionTypeSecureService conditionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			ConditionTypeSecureService = conditionTypeSecureService;
		}

		public virtual ICondition Create()
		{
			if (ConditionTypeSecureService == null)
			{
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");
			}

			var condition = IoC.Resolve<ICartValueRangeCondition>();

			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("CartValueRange");
			condition.CartValueRangeList = new Collection<CurrencyValueRange>();
			condition.Status = BusinessObjectStatus.New;

			return condition;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1725:ParameterNamesShouldMatchBaseDeclaration", MessageId = "0#")]
		public virtual void Save(ISystemUserFull systemUserFull, ICondition condition)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var cartValueRangeCondition = condition as ICartValueRangeCondition;
			if (cartValueRangeCondition == null)
			{
				throw new InvalidOperationException("condition is not CartValueRange type");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(cartValueRangeCondition);

				Repository.DeleteAllCartValueRange(cartValueRangeCondition.Id);
				foreach (var cartValueRange in cartValueRangeCondition.CartValueRangeList)
				{
					Repository.SaveCartValueRange(cartValueRangeCondition.Id, cartValueRange);
				}

				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public virtual ICondition GetById(int id)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var condition = Repository.GetById(id);
			if (condition == null)
			{
				return null;
			}

			condition.CartValueRangeList = Repository.GetCartValueRangeByCondition(id);

			return condition;
		}
	}
}