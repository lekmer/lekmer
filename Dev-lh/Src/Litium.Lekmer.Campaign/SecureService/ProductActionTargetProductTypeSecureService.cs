﻿using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class ProductActionTargetProductTypeSecureService : IProductActionTargetProductTypeSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected ProductActionTargetProductTypeRepository Repository { get; private set; }

		public ProductActionTargetProductTypeSecureService(
			IAccessValidator accessValidator,
			ProductActionTargetProductTypeRepository productActionTargetProductTypeRepository)
		{
			AccessValidator = accessValidator;
			Repository = productActionTargetProductTypeRepository;
		}

		public virtual Collection<IProductActionTargetProductType> GetAllByCampaign(int campaignId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCampaign(campaignId);
		}

		public virtual void Save(ISystemUserFull systemUserFull, IProductAction productAction)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			var lekmerProductAction = (ILekmerProductAction) productAction;

			string targetProductTypeIds = Convert.ToStringIdentifierList(lekmerProductAction.TargetProductTypes.Keys);

			Repository.Save(productAction.Id, targetProductTypeIds, Convert.DefaultListSeparator);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int productActionId)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			Repository.Delete(productActionId);
		}
	}
}
