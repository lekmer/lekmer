﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CartDoesNotContainConditionSecureService : IConditionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartDoesNotContainConditionRepository Repository { get; private set; }
		protected IConditionTypeSecureService ConditionTypeSecureService { get; private set; }
		protected ICampaignConfigSecureService CampaignConfigSecureService { get; private set; }

		public CartDoesNotContainConditionSecureService(
			IAccessValidator accessValidator,
			CartDoesNotContainConditionRepository cartDoesNotContainConditionRepository,
			IConditionTypeSecureService conditionTypeSecureService,
			ICampaignConfigSecureService campaignConfigSecureService)
		{
			AccessValidator = accessValidator;
			Repository = cartDoesNotContainConditionRepository;
			ConditionTypeSecureService = conditionTypeSecureService;
			CampaignConfigSecureService = campaignConfigSecureService;
		}

		public virtual ICondition Create()
		{
			if (ConditionTypeSecureService == null)
			{
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");
			}

			var condition = IoC.Resolve<ICartDoesNotContainCondition>();
			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("CartDoesNotContain");
			condition.CampaignConfig = CampaignConfigSecureService.Create();
			condition.Status = BusinessObjectStatus.New;
			return condition;
		}

		public virtual void Save(ISystemUserFull user, ICondition condition)
		{
			Repository.EnsureNotNull();

			var cartDoesNotContainCondition = condition as ICartDoesNotContainCondition;
			if (cartDoesNotContainCondition == null)
			{
				throw new InvalidOperationException("condition is not ICartDoesNotContainCondition type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				cartDoesNotContainCondition.CampaignConfigId = CampaignConfigSecureService.Save(cartDoesNotContainCondition.CampaignConfig);
				Repository.Save(cartDoesNotContainCondition);
				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public virtual ICondition GetById(int id)
		{
			Repository.EnsureNotNull();

			var condition = Repository.GetById(id);
			if (condition == null)
			{
				return null;
			}

			condition.CampaignConfig = CampaignConfigSecureService.GetById(condition.CampaignConfigId);
			return condition;
		}
	}
}