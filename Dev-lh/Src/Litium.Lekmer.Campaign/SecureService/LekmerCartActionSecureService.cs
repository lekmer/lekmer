﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product.Constant;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCartActionSecureService : CartActionSecureService
	{
		protected ICartActionTargetProductTypeSecureService CartActionTargetProductTypeSecureService { get; private set; }

		public LekmerCartActionSecureService(
			IAccessValidator accessValidator,
			CartActionRepository cartActionRepository,
			CartActionSecureServiceLocator secureServiceLocator,
			ICartActionTargetProductTypeSecureService cartActionTargetProductTypeSecureService)
			: base(accessValidator, cartActionRepository, secureServiceLocator)
		{
			CartActionTargetProductTypeSecureService = cartActionTargetProductTypeSecureService;
		}

		/// <summary>
		/// Creates a new <see cref="ICartAction"/> object.
		/// </summary>
		/// <param name="typeCommonName">Cart action type common name</param>
		/// <returns>The <see cref="ICartAction"/> object.</returns>
		public override ICartAction Create(string typeCommonName)
		{
			var cartAction = (ILekmerCartAction)base.Create(typeCommonName);

			cartAction.TargetProductTypes = new IdDictionary { (int)ProductType.Product }; // Target = Product by default

			return cartAction;
		}

		/// <summary>
		/// Gets a collection of <see cref="ICartAction"/> objects assigned to proper campaign.
		/// </summary>
		/// <param name="campaignId">Campaign Id.</param>
		/// <returns>The collection of <see cref="ICartAction"/> objects.</returns>
		public override Collection<ICartAction> GetAllByCampaign(int campaignId)
		{
			Collection<ICartAction> cartActions = base.GetAllByCampaign(campaignId);

			var actionTargetProductTypes = CartActionTargetProductTypeSecureService.GetAllByCampaign(campaignId);

			foreach (ILekmerCartAction cartAction in cartActions)
			{
				int cartActionId = cartAction.Id;
				IEnumerable<int> productTypes = actionTargetProductTypes.Where(t => t.CartActionId == cartActionId).Select(t => t.ProductTypeId);

				cartAction.TargetProductTypes = new IdDictionary(productTypes);
			}

			return cartActions;
		}

		/// <summary>
		/// Saves <see cref="ICartAction"/> object.
		/// </summary>
		/// <param name="user">Logged in system user</param>
		/// <param name="cartAction">Cart action to save</param>
		/// <returns>The <see cref="ICartAction"/> object identifier</returns>
		public override int Save(ISystemUserFull user, ICartAction cartAction)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				base.Save(user, cartAction);

				CartActionTargetProductTypeSecureService.Save(user, cartAction);

				transactedOperation.Complete();
			}

			return cartAction.Id;
		}

		/// <summary>
		/// Deletes cart action.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="id">Cart action identifier</param>
		/// <param name="typeCommonName">Cart action type common name</param>
		public override void Delete(ISystemUserFull systemUserFull, int id, string typeCommonName)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				CartActionTargetProductTypeSecureService.Delete(systemUserFull, id);

				base.Delete(systemUserFull, id, typeCommonName);

				transactedOperation.Complete();
			}
		}
	}
}
