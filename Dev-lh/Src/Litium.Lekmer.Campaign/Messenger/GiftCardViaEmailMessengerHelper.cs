﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.RatingReview
{
	public class GiftCardViaEmailMessengerHelper
	{
		private readonly ILog _log;

		private int _maxVoucherQuantity;
		private int _validInDays;
		private int _numberOfTokens;
		private Collection<IChannel> _channels;

		private IGiftCardViaMailInfoService _giftCardViaMailInfoService;
		private ILekmerChannelService _channelService;
		private IVoucherService _voucherService;
		private ICustomerService _customerService;
		private IOrderService _orderService;
		private ICartCampaignService _cartCampaignService;
		private IProductCampaignService _productCampaignService;

		private IUserContext _context;

		public GiftCardViaEmailMessengerHelper(int maxVoucherQuantity, int validInDays, int numberOfTokens, ILog log)
		{
			_maxVoucherQuantity = maxVoucherQuantity;
			_validInDays = validInDays;
			_numberOfTokens = numberOfTokens;
			_log = log;

			_channelService = (ILekmerChannelService)IoC.Resolve<IChannelService>();
			_giftCardViaMailInfoService = IoC.Resolve<IGiftCardViaMailInfoService>();
			_voucherService = IoC.Resolve<IVoucherService>();
			_customerService = IoC.Resolve<ICustomerService>();
			_orderService = IoC.Resolve<IOrderService>();

			_cartCampaignService = IoC.Resolve<ICartCampaignService>();
			_productCampaignService = IoC.Resolve<IProductCampaignService>();

			_context = IoC.Resolve<IUserContext>();

			_channels = _channelService.GetAll();
		}

		public void ProcessMessages()
		{
			Collection<IGiftCardViaMailInfo>  giftCardViaMailInfoCollection = _giftCardViaMailInfoService.GetAllToSend();
			foreach (var giftCardViaMailInfo in giftCardViaMailInfoCollection)
			{
				var channel = _channels.FirstOrDefault(c => c.Id == giftCardViaMailInfo.ChannelId);
				if (channel == null)
				{
					_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not find channel. GiftCardViaMailInfo.Id = {0}", giftCardViaMailInfo.Id));
					continue;
				}

				string createdBy = channel.Id + "_" + giftCardViaMailInfo.CampaignId + "_" + giftCardViaMailInfo.DiscountValue;
				int allowedVouchersQuantity;

				int voucherInfoId = _voucherService.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue(
					giftCardViaMailInfo.DiscountValue,
					channel.ApplicationName,
					createdBy,
					out allowedVouchersQuantity);

				if (voucherInfoId <= 0)
				{
					voucherInfoId = _voucherService.VoucherBatchCreate(
						DateTime.Now, "", "", 
						_maxVoucherQuantity,
						giftCardViaMailInfo.DiscountValue, 
						channel.ApplicationName, 
						_numberOfTokens, 
						createdBy);

					allowedVouchersQuantity = _maxVoucherQuantity;
				}

				giftCardViaMailInfo.VoucherInfoId = voucherInfoId;

				int vouchersQuantity = _voucherService.VoucherCountGetByVoucherInfo(voucherInfoId);
				if (vouchersQuantity >= allowedVouchersQuantity)
				{
					_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not create voucher for VoucherInfoId = {0} because exceeded allowed vouchers quantity. GiftCardViaMailInfo.Id = {1}", voucherInfoId, giftCardViaMailInfo.Id));

					giftCardViaMailInfo.StatusId = (int)GiftCardViaMailInfoStatus.ExceedsQuantity;
					_giftCardViaMailInfoService.Update(giftCardViaMailInfo);

					continue;
				}

				string voucherCode = _voucherService.VoucherGenerateCodes(voucherInfoId, 1, DateTime.Now.AddDays(_validInDays));

				if (voucherCode.HasValue())
				{
					giftCardViaMailInfo.VoucherCode = voucherCode;
					giftCardViaMailInfo.StatusId = (int)GiftCardViaMailInfoStatus.Sent;

					_context.Channel = channel;
					var order = _orderService.GetFullById(_context, giftCardViaMailInfo.OrderId);

					SendMessage(giftCardViaMailInfo, channel, (ILekmerOrderFull) order, voucherCode);

					_giftCardViaMailInfoService.Update(giftCardViaMailInfo);
				}
				else
				{
					_log.Error(string.Format(CultureInfo.InvariantCulture, "Generated voucher code is empty. VoucherInfoId = {0}; GiftCardViaMailInfo.Id = {1}", voucherInfoId, giftCardViaMailInfo.Id));

					giftCardViaMailInfo.StatusId = (int)GiftCardViaMailInfoStatus.EmptyVoucherCode;
					_giftCardViaMailInfoService.Update(giftCardViaMailInfo);
				}
			}
		}

		protected void SendMessage(IGiftCardViaMailInfo giftCardViaMailInfo, IChannel channel, ILekmerOrderFull order, string voucherCode)
		{
			_context.Channel = channel;
			order.Customer = _customerService.GetById(_context, order.CustomerId);

			int? templateId = RetrieveTemplateId(giftCardViaMailInfo);

			var messageArgs = new GiftCardViaEmailMessageArgs(channel, order, giftCardViaMailInfo.CampaignId, templateId, giftCardViaMailInfo.DiscountValue, voucherCode);
			var messenger = new GiftCardViaEmailMessenger();
			messenger.Send(messageArgs);
		}

		protected int? RetrieveTemplateId(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			var cartCampaign = _cartCampaignService.GetById(_context, giftCardViaMailInfo.CampaignId);
			if (cartCampaign != null)
			{
				foreach (var cartAction in cartCampaign.Actions)
				{
					if (cartAction.Id == giftCardViaMailInfo.ActionId)
					{
						var giftCardViaMailCartAction = (IGiftCardViaMailCartAction)cartAction;
						return giftCardViaMailCartAction.TemplateId;
					}
				}
			}

			var productCampaign = _productCampaignService.GetById(_context, giftCardViaMailInfo.CampaignId);
			if (productCampaign != null)
			{
				foreach (var productAction in productCampaign.Actions)
				{
					if (productAction.Id == giftCardViaMailInfo.ActionId)
					{
						var giftCardViaMailProductAction = (IGiftCardViaMailProductAction)productAction;
						return giftCardViaMailProductAction.TemplateId;
					}
				}
			}

			return null;
		}
	}
}