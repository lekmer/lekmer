﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class FreeFreightFlagActionService : IProductActionPluginService
	{
		protected FreeFreightFlagActionRepository Repository { get; private set; }

		public FreeFreightFlagActionService(FreeFreightFlagActionRepository repository)
		{
			Repository = repository;
		}

		public IProductAction GetById(IUserContext context, int productActionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var action = Repository.GetById(productActionId);
			if (action == null)
			{
				return null;
			}
			action.ProductPrices = Repository.GetProductPricesByAction(productActionId);
			return action;
		}
	}
}