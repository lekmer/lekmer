﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerConditionService : ConditionService
	{
		protected IConditionTargetProductTypeService ConditionTargetProductTypeService { get; private set; }

		public LekmerConditionService(
			ConditionRepository conditionRepository,
			ConditionServiceLocator serviceLocator,
			IConditionTargetProductTypeService conditionTargetProductTypeService)
			: base(conditionRepository, serviceLocator)
		{
			ConditionTargetProductTypeService = conditionTargetProductTypeService;
		}

		public override Collection<ICondition> GetAllByCampaign(IUserContext userContext, int campaignId)
		{
			Collection<ICondition> conditions = base.GetAllByCampaign(userContext, campaignId);

			var actionTargetProductTypes = ConditionTargetProductTypeService.GetAllByCampaign(campaignId);

			foreach (ICondition condition in conditions)
			{
				var lekmerCondition = condition as ILekmerCondition;

				if (lekmerCondition != null)
				{
					int conditionId = condition.Id;
					IEnumerable<int> productTypes = actionTargetProductTypes.Where(t => t.ConditionId == conditionId).Select(t => t.ProductTypeId);

					lekmerCondition.TargetProductTypes = new IdDictionary(productTypes);
				}
			}

			return conditions;
		}
	}
}