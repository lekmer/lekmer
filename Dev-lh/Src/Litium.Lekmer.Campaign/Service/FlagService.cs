﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class FlagService : IFlagService
	{
		protected FlagRepository Repository { get; private set; }

		public FlagService(FlagRepository flagRepository)
		{
			Repository = flagRepository;
		}

		public virtual IFlag GetByCampaign(IUserContext context, int campaignId)
		{
			var noFlag = CampaignWithNoFlagCache.Instance.GetData(new CampaignWithNoFlagKey(campaignId));

			if (noFlag != null)
			{
				return null;
			}

			var flag = CampaignFlagCache.Instance.TryGetItem(new CampaignFlagKey(context.Channel.Language.Id, campaignId), () => GetByCampaignCore(context, campaignId));

			if (flag == null)
			{
				CampaignWithNoFlagCache.Instance.Add(new CampaignWithNoFlagKey(campaignId), new CampaignWithNoFlag());
			}

			return flag;
		}
		protected virtual IFlag GetByCampaignCore(IUserContext context, int campaignId)
		{
			return Repository.GetByCampaign(context.Channel.Language.Id, campaignId);
		}

		public virtual Collection<IFlag> GetByProduct(IUserContext context, int productId)
		{
			return ProductFlagListCache.Instance.TryGetItem(new ProductFlagListKey(context.Channel.Language.Id, productId), () => GetByProductCore(context, productId));
		}
		protected virtual Collection<IFlag> GetByProductCore(IUserContext context, int productId)
		{
			return Repository.GetByProduct(context.Channel.Language.Id, productId);
		}
	}
}