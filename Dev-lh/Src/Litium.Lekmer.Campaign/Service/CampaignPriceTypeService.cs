﻿using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	public class CampaignPriceTypeService : ICampaignPriceTypeService
	{
		protected CampaignPriceTypeRepository Repository { get; private set; }

		public CampaignPriceTypeService(CampaignPriceTypeRepository repository)
		{
			Repository = repository;
		}

		public virtual ICampaignPriceType GetById(int id)
		{
			Repository.EnsureNotNull();

			return CampaignPriceTypeCache.Instance.TryGetItem(
				new CampaignPriceTypeKey(id),
				delegate { return GetByIdCore(id); });
		}

		protected virtual ICampaignPriceType GetByIdCore(int id)
		{
			return Repository.GetById(id);
		}
	}
}