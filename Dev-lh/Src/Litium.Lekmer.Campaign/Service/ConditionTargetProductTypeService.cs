﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	public class ConditionTargetProductTypeService : IConditionTargetProductTypeService
	{
		protected ConditionTargetProductTypeRepository Repository { get; private set; }

		public ConditionTargetProductTypeService(ConditionTargetProductTypeRepository conditionTargetProductTypeRepository)
		{
			Repository = conditionTargetProductTypeRepository;
		}

		public virtual Collection<IConditionTargetProductType> GetAllByCampaign(int campaignId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCampaign(campaignId);
		}
	}
}
