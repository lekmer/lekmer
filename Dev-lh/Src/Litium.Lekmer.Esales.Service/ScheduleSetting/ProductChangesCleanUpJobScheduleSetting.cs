﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Esales.Service
{
	public class ProductChangesCleanUpJobScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "Esales"; }
		}

		protected override string GroupName
		{
			get { return "ProductChangesCleanUpJob"; }
		}
	}
}