﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DropShip.Service
{
	public class DropShipImportJob : BaseJob
	{
		public override string Name
		{
			get { return "DropShipImportJob"; }
		}

		protected override void ExecuteAction()
		{
			var dropShipImport = IoC.Resolve<ITask>("DropShipImport");
			dropShipImport.Execute();
		}
	}
}