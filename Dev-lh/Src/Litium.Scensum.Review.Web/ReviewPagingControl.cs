using System;
using System.Globalization;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core.Web;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Scensum.Review.Web
{
	public class ReviewPagingControl : LekmerPagingControl, IReviewPagingControl
	{
		public ReviewPagingControl(
			ITemplateFactory templateFactory,
			IPagingCalculator pagingCalculator)
			: base(templateFactory, pagingCalculator)
		{
		}

		public string ReviewAddedQueryStringParameterName { get; set; }

		public override void Initialize()
		{
			if (ReviewAddedQueryStringParameterName.IsEmpty())
			{
				throw new InvalidOperationException("ReviewAddedQueryStringParameterName must be set.");
			}

			base.Initialize();
		}

		protected override string GetPageUrl(int page)
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			if (page != 1)
			{
				queryBuilder.ReplaceOrAdd(PageQueryStringParameterName, page.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				queryBuilder.Remove(PageQueryStringParameterName);
			}

			queryBuilder.Remove(ReviewAddedQueryStringParameterName);

			return PageBaseUrl + queryBuilder;
		}
	}
}