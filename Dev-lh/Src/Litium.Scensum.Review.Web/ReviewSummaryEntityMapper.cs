using System;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Review.Web
{
	public class ReviewSummaryEntityMapper : EntityMapper<IReviewSummary>
	{
		private readonly IFormatter _formatter;

		public ReviewSummaryEntityMapper(IFormatter formatter)
		{
			_formatter = formatter;
		}

		public override void AddEntityVariables(Fragment fragment, IReviewSummary item)
		{
			fragment.AddCondition("ReviewSummary.HasReviews", item.AverageRating.HasValue);

			fragment.AddVariable("ReviewSummary.ReviewCount", _formatter.FormatNumber(Channel.Current, item.ReviewCount));

			fragment.AddVariable("ReviewSummary.AverageRating",
			                     item.AverageRating.HasValue
			                     	? Math.Round(item.AverageRating.Value, 0).ToString(Channel.Current.CultureInfo)
			                     	: string.Empty);

			fragment.AddVariable("ReviewSummary.AverageRatingAsDecimal",
			                     item.AverageRating.HasValue
			                     	? Math.Round(item.AverageRating.Value, 1).ToString(Channel.Current.CultureInfo)
			                     	: string.Empty);
		}
	}
}