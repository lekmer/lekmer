﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Repository
{
	public class NewsletterSubscriberRepository
	{
		protected virtual DataMapperBase<INewsletterSubscriber> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INewsletterSubscriber>(dataReader);
		}

		public virtual int Save(INewsletterSubscriber subscriber)
		{
			if (subscriber == null)
			{
				throw new ArgumentNullException("subscriber");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", subscriber.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", subscriber.Email, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@SubscriberTypeId", subscriber.SubscriberTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CreatedDate", subscriber.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@UpdateDate", subscriber.UpdatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterSubscriberSave]", parameters, dbSettings);
		}

		public virtual int Delete(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.Delete");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterSubscriberDelete]", parameters, dbSettings);
		}

		public virtual int DeleteByEmail(int channelId, string email)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Email", email, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.DeleteByEmail");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pNewsletterSubscriberDeleteByEmail]", parameters, dbSettings);
		}

		public virtual void UpdateFromOrders()
		{
			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.UpdateFromOrders");

			new DataHandler().ExecuteCommand("[lekmer].[pNewsletterSubscriberUpdateFromOrders]", new IDataParameter[0], dbSettings);
		}

		public virtual void SetSentStatus(int subscriberId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SubscriberId", subscriberId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.SetSentStatus");

			new DataHandler().ExecuteCommand("[lekmer].[pNewsletterSubscriberSetSentStatus]", parameters, dbSettings);
		}

		public virtual Collection<INewsletterSubscriber> GetAllWithNotSentStatus()
		{
			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.GetAllWithNotSentStatus");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterSubscriberGetAllWithNotSentStatus]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual INewsletterSubscriber Get(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.Get");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterSubscriberGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<INewsletterSubscriber> SearchByEmail(int channelId, string email, int page, int pageSize, out int items)
		{
			var subscribers = new Collection<INewsletterSubscriber>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Email", email, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("PageSize", pageSize, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.SearchByEmail");

			items = 0;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pNewsletterSubscriberSearchByEmail]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					items = dataReader.GetInt32(0);
				}

				if (dataReader.NextResult())
				{
					subscribers = CreateDataMapper(dataReader).ReadMultipleRows();
				}

				return subscribers;
			}
		}
	}
}