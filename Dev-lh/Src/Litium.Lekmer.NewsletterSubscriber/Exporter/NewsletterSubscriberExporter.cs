﻿using System;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterSubscriberExporter : INewsletterSubscriberExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly NewsletterSubscriberSetting _setting = new NewsletterSubscriberSetting();

		protected INewsletterSubscriberService NewsletterSubscriberService { get; private set; }
		protected IInterspireConnector InterspireConnector { get; private set; }

		public NewsletterSubscriberExporter(INewsletterSubscriberService newsletterSubscriberService, IInterspireConnector interspireConnector)
		{
			NewsletterSubscriberService = newsletterSubscriberService;
			InterspireConnector = interspireConnector;
		}

		public virtual void Execute()
		{
			if (_setting.SubscribeFromOrders)
			{
				_log.InfoFormat("Subscribe from orders.");
				NewsletterSubscriberService.UpdateFromOrders();
			}

			_log.InfoFormat("Get new subscribers.");
			var newsletterSubscribers = NewsletterSubscriberService.GetAllWithNotSentStatus();
			_log.InfoFormat("Count of new subscribers - {0}", newsletterSubscribers.Count);

			int index = 1;
			foreach (var subscriber in newsletterSubscribers)
			{
				RemoveFromBanList(subscriber.Email);

				bool statusSuccess = true;
				foreach (var newsLetterList in _setting.GetMailingList(subscriber.ChannelId))
				{
					try
					{
						_log.InfoFormat("Sending: {0}, list: '{1}', index: {2}", subscriber.Email, newsLetterList, index);

						var responseAddSubscriber = InterspireConnector.AddSubscriberToList(subscriber.Email, newsLetterList);

						if (responseAddSubscriber.Status != (int) InterspireResponseStatus.Success)
						{
							_log.WarnFormat("Interspire response fail: {0}, list: '{1}', status: {2}, message: {3}", subscriber.Email, newsLetterList, responseAddSubscriber.Status, responseAddSubscriber.ErrorMessage);
						}

						if (responseAddSubscriber.Status == (int) InterspireResponseStatus.BadWebResponse)
						{
							statusSuccess = false;
						}
					}
					catch (Exception ex)
					{
						statusSuccess = false;
						_log.ErrorFormat("Interspire error: {0}, error message: {1}", subscriber.Email, ex);
					}

					index = index + 1;
				}

				// if status = SUCCESS write to DB and change status on subscriber
				if (statusSuccess)
				{
					NewsletterSubscriberService.SetSentStatus(subscriber.Id);
				}
			}
		}

		protected virtual void RemoveFromBanList(string email)
		{
			var interspireResponseFetchBannedSubscriber = InterspireConnector.FetchBannedSubscriber(email);
			if (interspireResponseFetchBannedSubscriber.BanList != null && interspireResponseFetchBannedSubscriber.BanList.Email == email)
			{
				_log.InfoFormat("Remove subscriber '{0}' from interspire ban list.", email);
				var response = InterspireConnector.RemoveBannedSubscriber(interspireResponseFetchBannedSubscriber.BanList.BanId);
				if (response.Status != (int)InterspireResponseStatus.Success)
				{
					_log.WarnFormat("Interspire response: {0}, status: {1}, message: {2}", email, response.Status, response.ErrorMessage);
				}
			}
		}
	}
}