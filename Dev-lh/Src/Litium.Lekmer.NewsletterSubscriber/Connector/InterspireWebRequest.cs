﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class InterspireWebRequest : IInterspireWebRequest
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public virtual IInterspireWebResponse PostData(string postUrl, string postData)
		{
			try
			{
				// Create a request using a URL that can receive a post.
				WebRequest request = WebRequest.Create(postUrl);

				// Set the Method property of the request to POST.
				request.Method = "POST";

				// Convert POST data to a byte array.
				byte[] byteArray = Encoding.UTF8.GetBytes(postData);

				// Set the ContentType property of the WebRequest.
				request.ContentType = "application/x-www-form-urlencoded";

				// Set the ContentLength property of the WebRequest.
				request.ContentLength = byteArray.Length;

				// Get the request stream.
				Stream dataStream = request.GetRequestStream();

				// Write the data to the request stream.
				dataStream.Write(byteArray, 0, byteArray.Length);

				// Close the Stream object.
				dataStream.Close();

				// Get the response.
				var response = (HttpWebResponse)request.GetResponse();

				var interspireWebResponse = IoC.Resolve<IInterspireWebResponse>();

				interspireWebResponse.StatusCode = (int)response.StatusCode;
				interspireWebResponse.StatusDescription = response.StatusDescription;

				var readStream = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

				interspireWebResponse.Response = readStream.ReadToEnd();

				readStream.Close();
				response.Close();

				return interspireWebResponse;
			}
			catch (Exception exception)
			{
				_log.WarnFormat("The process failed: {0}", exception.StackTrace);

				var interspireWebResponse = IoC.Resolve<IInterspireWebResponse>();
				interspireWebResponse.StatusCode = 999;
				interspireWebResponse.StatusDescription = exception.Message;

				return interspireWebResponse;
			}
		}
	}
}