﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class InterspireConnector : IInterspireConnector
	{
		protected INewsletterSubscriberSetting NewsletterSubscriberSetting { get; set; }
		protected IInterspireXmlBilder InterspireXmlBilder { get; set; }
		protected IInterspireXmlParser InterspireXmlParser { get; set; }
		protected IInterspireWebRequest InterspireWebRequest { get; set; }

		public InterspireConnector(
			INewsletterSubscriberSetting newsletterSubscriberSetting, 
			IInterspireXmlBilder interspireXmlBilder, 
			IInterspireXmlParser interspireXmlParser, 
			IInterspireWebRequest interspireWebRequest)
		{
			NewsletterSubscriberSetting = newsletterSubscriberSetting;
			InterspireXmlBilder = interspireXmlBilder;
			InterspireXmlParser = interspireXmlParser;
			InterspireWebRequest = interspireWebRequest;
		}

		public virtual IInterspireResponseAddSubscriber AddSubscriberToList(string emailAddress, string mailingListId)
		{
			var xmlRequest = InterspireXmlBilder.GetAddSubscriberXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken, emailAddress, mailingListId);

			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);

			IInterspireResponseAddSubscriber interspireResponseAddSubscriber;

			if (webResponse.StatusCode == 200)
			{
				interspireResponseAddSubscriber = InterspireXmlParser.ParseAddSubscriberResponse(webResponse.Response);
			}
			else
			{
				interspireResponseAddSubscriber = HandleBadWebResponse<IInterspireResponseAddSubscriber>(webResponse);
			}

			return interspireResponseAddSubscriber;
		}

		public virtual IInterspireResponseDeleteSubscriber DeleteSubscriber(string emailAddress, string mailingListId)
		{
			var xmlRequest = InterspireXmlBilder.GetDeleteSubscriberXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken, emailAddress, mailingListId);

			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);

			IInterspireResponseDeleteSubscriber interspireResponseDeleteSubscriber;

			if (webResponse.StatusCode == 200)
			{
				interspireResponseDeleteSubscriber = InterspireXmlParser.ParseDeleteSubscriberResponse(webResponse.Response);
			}
			else
			{
				interspireResponseDeleteSubscriber = HandleBadWebResponse<IInterspireResponseDeleteSubscriber>(webResponse);
			}

			return interspireResponseDeleteSubscriber;
		}

		public virtual IInterspireResponseUnsubscribeSubscriber UnsubscribeSubscriber(string emailAddress, string mailingListId)
		{
			var xmlRequest = InterspireXmlBilder.GetUnsubscribeSubscriberXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken, emailAddress, mailingListId);

			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);

			IInterspireResponseUnsubscribeSubscriber interspireResponseUnsubscribeSubscriber;

			if (webResponse.StatusCode == 200)
			{
				interspireResponseUnsubscribeSubscriber = InterspireXmlParser.ParseUnsubscribeSubscriberResponse(webResponse.Response);
			}
			else
			{
				interspireResponseUnsubscribeSubscriber = HandleBadWebResponse<IInterspireResponseUnsubscribeSubscriber>(webResponse);
			}

			return interspireResponseUnsubscribeSubscriber;
		}

		public virtual IInterspireResponseGetLists GetLists()
		{
			var xmlRequest = InterspireXmlBilder.GetGetListsXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken);

			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);

			IInterspireResponseGetLists interspireResponseGetLists;

			if (webResponse.StatusCode == 200)
			{
				interspireResponseGetLists = InterspireXmlParser.ParseGetListsResponse(webResponse.Response);
			}
			else
			{
				interspireResponseGetLists = HandleBadWebResponse<IInterspireResponseGetLists>(webResponse);
			}

			return interspireResponseGetLists;
		}

		public virtual IInterspireResponseIsContactOnList IsContactOnList(string emailAddress, string mailingListId)
		{
			var xmlRequest = InterspireXmlBilder.GetIsContactOnListXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken, emailAddress, mailingListId);

			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);

			IInterspireResponseIsContactOnList interspireResponseIsContactOnList;

			if (webResponse.StatusCode == 200)
			{
				interspireResponseIsContactOnList = InterspireXmlParser.ParseIsContactOnListResponse(webResponse.Response);
			}
			else
			{
				interspireResponseIsContactOnList = HandleBadWebResponse<IInterspireResponseIsContactOnList>(webResponse);
			}

			return interspireResponseIsContactOnList;
		}

		// Ban List
		public virtual IInterspireResponse AddBannedSubscriber(string emailAddress)
		{
			IInterspireResponse interspireResponse;

			var xmlRequest = InterspireXmlBilder.GetAddBannedSubscriberXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken, emailAddress);
			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);
			if (webResponse.StatusCode == 200)
			{
				interspireResponse = InterspireXmlParser.ParseAddBannedSubscriberResponse(webResponse.Response);
			}
			else
			{
				interspireResponse = HandleBadWebResponse<IInterspireResponse>(webResponse);
			}

			return interspireResponse;
		}

		public virtual IInterspireResponseFetchBannedSubscriber FetchBannedSubscriber(string emailAddress)
		{
			IInterspireResponseFetchBannedSubscriber interspireResponseFetchBannedSubscriber;

			var xmlRequest = InterspireXmlBilder.GetFetchBannedSubscriberXml(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken);
			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);
			if (webResponse.StatusCode == 200)
			{
				interspireResponseFetchBannedSubscriber = InterspireXmlParser.ParseFetchBannedSubscriberResponse(webResponse.Response, emailAddress);
			}
			else
			{
				interspireResponseFetchBannedSubscriber = HandleBadWebResponse<IInterspireResponseFetchBannedSubscriber>(webResponse);
			}

			return interspireResponseFetchBannedSubscriber;
		}

		public virtual IInterspireResponse RemoveBannedSubscriber(int banId)
		{
			IInterspireResponse interspireResponse;

			var xmlRequest = InterspireXmlBilder.RemoveBannedSubscriber(NewsletterSubscriberSetting.UserName, NewsletterSubscriberSetting.UserToken, banId);
			IInterspireWebResponse webResponse = InterspireWebRequest.PostData(NewsletterSubscriberSetting.PostUrl, xmlRequest);
			if (webResponse.StatusCode == 200)
			{
				interspireResponse = InterspireXmlParser.ParseRemoveBannedSubscriberResponse(webResponse.Response);
			}
			else
			{
				interspireResponse = HandleBadWebResponse<IInterspireResponse>(webResponse);
			}

			return interspireResponse;
		}

		protected virtual T HandleBadWebResponse<T>(IInterspireWebResponse webResponse) where T : IInterspireResponse
		{
			var response = IoC.Resolve<T>();

			response.Status = (int)InterspireResponseStatus.BadWebResponse;
			response.ErrorMessage = webResponse.StatusDescription;

			return response;
		}
	}
}