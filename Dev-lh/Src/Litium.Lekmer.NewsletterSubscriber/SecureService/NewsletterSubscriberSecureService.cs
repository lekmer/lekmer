﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterSubscriberSecureService : INewsletterSubscriberSecureService
	{
		protected NewsletterSubscriberRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public NewsletterSubscriberSecureService(NewsletterSubscriberRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual INewsletterSubscriber Create(int channelId)
		{
			var subscriber = IoC.Resolve<INewsletterSubscriber>();

			subscriber.ChannelId = channelId;
			subscriber.CreatedDate = DateTime.Now;

			subscriber.Status = BusinessObjectStatus.New;

			return subscriber;
		}

		public virtual int Save(ISystemUserFull systemUserFull, INewsletterSubscriber subscriber)
		{
			if (subscriber == null) throw new ArgumentNullException("subscriber");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			using (var transactedOperation = new TransactedOperation())
			{
				subscriber.UpdatedDate = DateTime.Now;
				subscriber.Id = Repository.Save(subscriber);
				transactedOperation.Complete();
			}

			subscriber.SetUntouched();

			return subscriber.Id;
		}

		public virtual bool Delete(ISystemUserFull systemUserFull, int id)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			int result;
			using (var transactedOperation = new TransactedOperation())
			{
				result = Repository.Delete(id);
				transactedOperation.Complete();
			}
			return result > 0;
		}

		public virtual INewsletterSubscriber Get(int id)
		{
			return Repository.Get(id);
		}

		public Collection<INewsletterSubscriber> SearchByEmail(int channelId, string email, int page, int pageSize, out int itemsCount)
		{
			return Repository.SearchByEmail(channelId, email, page, pageSize, out itemsCount);
		}
	}
}