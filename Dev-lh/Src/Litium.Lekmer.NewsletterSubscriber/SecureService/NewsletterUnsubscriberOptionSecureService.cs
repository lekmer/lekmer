﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterUnsubscriberOptionSecureService : INewsletterUnsubscriberOptionSecureService
	{
		protected NewsletterUnsubscriberOptionRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public NewsletterUnsubscriberOptionSecureService(NewsletterUnsubscriberOptionRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual INewsletterUnsubscriberOption Create()
		{
			var unsubscriberOption = IoC.Resolve<INewsletterUnsubscriberOption>();
			unsubscriberOption.CreatedDate = DateTime.Now;
			unsubscriberOption.Status = BusinessObjectStatus.New;
			return unsubscriberOption;
		}

		public virtual int Save(ISystemUserFull systemUserFull, INewsletterUnsubscriberOption unsubscriberOption)
		{
			if (unsubscriberOption == null) throw new ArgumentNullException("unsubscriberOption");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			using (var transactedOperation = new TransactedOperation())
			{
				unsubscriberOption.Id = Repository.Save(unsubscriberOption);
				transactedOperation.Complete();
			}

			unsubscriberOption.SetUntouched();

			return unsubscriberOption.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int unsubscriberOptionId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(unsubscriberOptionId);
				transactedOperation.Complete();
			}
		}

		public virtual Collection<INewsletterUnsubscriberOption> GetByUnsubscriber(int unsubscriberId)
		{
			return Repository.GetByUnsubscriber(unsubscriberId);
		}
	}
}