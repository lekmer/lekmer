﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterUnsubscriberOptionService : INewsletterUnsubscriberOptionService
	{
		protected NewsletterUnsubscriberOptionRepository Repository { get; private set; }

		public NewsletterUnsubscriberOptionService(NewsletterUnsubscriberOptionRepository repository)
		{
			Repository = repository;
		}

		public virtual INewsletterUnsubscriberOption Create()
		{
			var unsubscriberOption = IoC.Resolve<INewsletterUnsubscriberOption>();

			unsubscriberOption.CreatedDate = DateTime.Now;

			unsubscriberOption.Status = BusinessObjectStatus.New;

			return unsubscriberOption;
		}

		public virtual Collection<INewsletterUnsubscriberOption> GetByUnsubscriber(int unsubscriberId)
		{
			return Repository.GetByUnsubscriber(unsubscriberId);
		}

		public virtual int Save(INewsletterUnsubscriberOption unsubscriberOption)
		{
			if (unsubscriberOption == null) throw new ArgumentNullException("unsubscriberOption");

			using (var transactedOperation = new TransactedOperation())
			{
				unsubscriberOption.Id = Repository.Save(unsubscriberOption);
				transactedOperation.Complete();
			}

			unsubscriberOption.SetUntouched();

			return unsubscriberOption.Id;
		}

		public virtual void Delete(int unsubscriberOptionId)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(unsubscriberOptionId);
				transactedOperation.Complete();
			}
		}
	}
}
