﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Mapper
{
	public class NewsletterUnsubscriberDataMapper : DataMapperBase<INewsletterUnsubscriber>
	{
		public NewsletterUnsubscriberDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override INewsletterUnsubscriber Create()
		{
			var newsletterSubscriber = IoC.Resolve<INewsletterUnsubscriber>();

			newsletterSubscriber.Id = MapValue<int>("NewsletterUnsubscriber.UnsubscriberId");
			newsletterSubscriber.ChannelId = MapValue<int>("NewsletterUnsubscriber.ChannelId");
			newsletterSubscriber.Email = MapValue<string>("NewsletterUnsubscriber.Email");
			newsletterSubscriber.CreatedDate = MapValue<DateTime>("NewsletterUnsubscriber.CreatedDate");
			newsletterSubscriber.UpdatedDate = MapValue<DateTime>("NewsletterUnsubscriber.UpdatedDate");

			newsletterSubscriber.SetUntouched();

			return newsletterSubscriber;
		}
	}
}