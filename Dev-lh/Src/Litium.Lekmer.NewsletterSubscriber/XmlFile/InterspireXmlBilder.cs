﻿using System.Xml.Linq;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class InterspireXmlBilder : IInterspireXmlBilder
	{
		public virtual string GetAddSubscriberXml(string userName, string userToken, string emailAddress, string mailingListId)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueAddSubscriberToList), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.EmailAddressNode, emailAddress), // emailaddress
						new XElement(NewsletterSubscriberConstants.MailingListNode, mailingListId), // mailinglist
						new XElement(NewsletterSubscriberConstants.FormatNode, NewsletterSubscriberConstants.FormatNodeValueHtml), // format
						new XElement(NewsletterSubscriberConstants.ConfirmedNode, NewsletterSubscriberConstants.ConfirmedNodeValueYes) // confirmed
					)
				);

			return xmlRequest.ToString();
		}

		public virtual string GetDeleteSubscriberXml(string userName, string userToken, string emailAddress, string mailingListId)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueDeleteSubscriber), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.EmailAddressNode, emailAddress), // emailaddress
						new XElement(NewsletterSubscriberConstants.ListNode, mailingListId) // list
					)
				);

			return xmlRequest.ToString();
		}

		public virtual string GetUnsubscribeSubscriberXml(string userName, string userToken, string emailAddress, string mailingListId)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueUnsubscribeSubscriber), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.EmailAddressNode, emailAddress), // emailaddress
						new XElement(NewsletterSubscriberConstants.ListIdNode, mailingListId) // listid
					)
				);

			return xmlRequest.ToString();
		}

		public virtual string GetAddBannedSubscriberXml(string userName, string userToken, string emailAddress)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueAddBannedSubscriber), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.EmailAddressNode, emailAddress), // emailaddress
						new XElement(NewsletterSubscriberConstants.ListIdNode, NewsletterSubscriberConstants.BanListId) // listid
					)
				);

			return xmlRequest.ToString();
		}

		public virtual string GetFetchBannedSubscriberXml(string userName, string userToken)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueFetchBannedSubscribers), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.PageIdNode, 1), // pageid
						new XElement(NewsletterSubscriberConstants.PerpageNode, NewsletterSubscriberConstants.AllValue), // perpage
						new XElement(NewsletterSubscriberConstants.SearchInfoNode, // searchinfo
							new XElement(NewsletterSubscriberConstants.ListUpperNode, NewsletterSubscriberConstants.BanListId) // List
						)
					)
				);

			return xmlRequest.ToString();
		}

		public virtual string RemoveBannedSubscriber(string userName, string userToken, int banId)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueRemoveBannedSubscriber), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.BanIdNode, banId), // banid
						new XElement(NewsletterSubscriberConstants.ListIdNode, NewsletterSubscriberConstants.BanListId) //listid
					)
				);

			return xmlRequest.ToString();
		}

		public virtual string GetGetListsXml(string userName, string userToken)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueUser), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueGetLists), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, " ") // details //NOTE: Doesn't work with empty element
				);

			return xmlRequest.ToString();
		}

		public virtual string GetIsContactOnListXml(string userName, string userToken, string emailAddress, string mailingListId)
		{
			var xmlRequest =
				new XElement(NewsletterSubscriberConstants.XmlRequestNode, // xmlrequest
					new XElement(NewsletterSubscriberConstants.UserNameNode, userName), // username
					new XElement(NewsletterSubscriberConstants.UserTokenNode, userToken), // usertoken
					new XElement(NewsletterSubscriberConstants.RequestTypeNode, NewsletterSubscriberConstants.RequestTypeNodeValueSubscribers), // requesttype
					new XElement(NewsletterSubscriberConstants.RequestMethodNode, NewsletterSubscriberConstants.RequestMethodNodeValueIsSubscriberOnList), // requestmethod
					new XElement(NewsletterSubscriberConstants.DetailsNode, // details
						new XElement(NewsletterSubscriberConstants.EmailNode, emailAddress), // email
						new XElement(NewsletterSubscriberConstants.ListNode, mailingListId) // list
					)
				);

			return xmlRequest.ToString();
		}
	}
}
