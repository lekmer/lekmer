﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class InterspireXmlParser : IInterspireXmlParser
	{
		public virtual IInterspireResponseAddSubscriber ParseAddSubscriberResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var responseAddSubscriber = IoC.Resolve<IInterspireResponseAddSubscriber>();

			ParseStatus(xmlDocument, responseAddSubscriber);

			if (responseAddSubscriber.Status == (int)InterspireResponseStatus.Success)
			{
				responseAddSubscriber.ContactsIdNumber = xmlDocument.Root.ElementOrDefault("data").ValueOrDefault();
			}

			return responseAddSubscriber;
		}

		public virtual IInterspireResponseDeleteSubscriber ParseDeleteSubscriberResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var responseDeleteSubscriber = IoC.Resolve<IInterspireResponseDeleteSubscriber>();

			ParseStatus(xmlDocument, responseDeleteSubscriber);

			if (responseDeleteSubscriber.Status == (int)InterspireResponseStatus.Success)
			{
				int removedSubscribers;
				if (int.TryParse(xmlDocument.Root.ElementOrDefault("data").ElementOrDefault("item").ValueOrDefault(), out removedSubscribers))
				{
					responseDeleteSubscriber.RemovedSubscribers = removedSubscribers;
				}
			}

			return responseDeleteSubscriber;
		}

		public virtual IInterspireResponseUnsubscribeSubscriber ParseUnsubscribeSubscriberResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var responseDeleteSubscriber = IoC.Resolve<IInterspireResponseUnsubscribeSubscriber>();

			ParseStatus(xmlDocument, responseDeleteSubscriber);

			return responseDeleteSubscriber;
		}

		public virtual IInterspireResponseGetLists ParseGetListsResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var responseGetLists = IoC.Resolve<IInterspireResponseGetLists>();

			ParseStatus(xmlDocument, responseGetLists);

			if (responseGetLists.Status == (int)InterspireResponseStatus.Success)
			{
				IEnumerable<XElement> items = xmlDocument.Root.ElementOrDefault("data").ElementsOrEmpty("item");
				responseGetLists.ContactLists = ParseContactLists(items);
			}

			return responseGetLists;
		}

		public virtual IInterspireResponseIsContactOnList ParseIsContactOnListResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);


			var responseIsContactOnList = IoC.Resolve<IInterspireResponseIsContactOnList>();

			ParseStatus(xmlDocument, responseIsContactOnList);

			if (responseIsContactOnList.Status == (int)InterspireResponseStatus.Success)
			{
				responseIsContactOnList.ContactIsLocated = xmlDocument.Root.ElementOrDefault("data").ValueOrDefault() == "1";
			}

			return responseIsContactOnList;
		}

		public virtual IInterspireResponse ParseAddBannedSubscriberResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var addBannedSubscriber = IoC.Resolve<IInterspireResponse>();

			ParseStatus(xmlDocument, addBannedSubscriber);

			return addBannedSubscriber;
		}

		public virtual IInterspireResponseFetchBannedSubscriber ParseFetchBannedSubscriberResponse(string response, string emailAddress)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var fetchBannedSubscribers = IoC.Resolve<IInterspireResponseFetchBannedSubscriber>();

			ParseStatus(xmlDocument, fetchBannedSubscribers);
			if (fetchBannedSubscribers.Status == (int)InterspireResponseStatus.Success)
			{
				IEnumerable<XElement> items = xmlDocument.Root.ElementOrDefault("data").ElementOrDefault("subscriberlist").ElementsOrEmpty("item");
				var interspireBanLists = ParseBanLists(items);
				var list = interspireBanLists.FirstOrDefault(l => l.Email == emailAddress);
				if (list != null)
				{
					fetchBannedSubscribers.BanList = list;
				}
			}

			return fetchBannedSubscribers;
		}

		public virtual IInterspireResponse ParseRemoveBannedSubscriberResponse(string response)
		{
			XDocument xmlDocument = XDocument.Parse(response);

			var removeBannedSubscriber = IoC.Resolve<IInterspireResponse>();

			ParseStatus(xmlDocument, removeBannedSubscriber);

			return removeBannedSubscriber;
		}


		protected virtual Collection<IInterspireContactList> ParseContactLists(IEnumerable<XElement> items)
		{
			var contactLists = new Collection<IInterspireContactList>();

			foreach (XElement item in items)
			{
				var contactList = IoC.Resolve<IInterspireContactList>();

				int listId;
				if (int.TryParse(item.ElementOrDefault("listid").ValueOrDefault(), out listId))
				{
					contactList.ListId = listId;
				}

				contactList.Name = item.ElementOrDefault("name").ValueOrDefault();

				int subscribeCount;
				if (int.TryParse(item.ElementOrDefault("subscribecount").ValueOrDefault(), out subscribeCount))
				{
					contactList.SubscribeCount = subscribeCount;
				}

				int unsubscribeCount;
				if (int.TryParse(item.ElementOrDefault("unsubscribecount").ValueOrDefault(), out unsubscribeCount))
				{
					contactList.UnsubscribeCount = unsubscribeCount;
				}

				int autoResponderCount;
				if (int.TryParse(item.ElementOrDefault("autorespondercount").ValueOrDefault(), out autoResponderCount))
				{
					contactList.AutoResponderCount = autoResponderCount;
				}

				contactLists.Add(contactList);
			}

			return contactLists;
		}

		protected virtual Collection<IInterspireBanList> ParseBanLists(IEnumerable<XElement> items)
		{
			var banList = new Collection<IInterspireBanList>();

			foreach (XElement item in items)
			{
				var interspireBanList = IoC.Resolve<IInterspireBanList>();

				int banId;
				if (int.TryParse(item.ElementOrDefault("banid").ValueOrDefault(), out banId))
				{
					interspireBanList.BanId = banId;
				}
				interspireBanList.Email = item.ElementOrDefault("emailaddress").ValueOrDefault();
				interspireBanList.List = item.ElementOrDefault("list").ValueOrDefault();

				banList.Add(interspireBanList);
			}

			return banList;
		}

		protected virtual void ParseStatus(XDocument xmlDocument, IInterspireResponse interspireResponse)
		{
			string status = xmlDocument.Root.ElementOrDefault("status").ValueOrDefault();

			if (status == "SUCCESS")
			{
				interspireResponse.Status = (int)InterspireResponseStatus.Success;
			}
			else if (status == "ERROR")
			{
				interspireResponse.Status = (int)InterspireResponseStatus.Error;
				interspireResponse.ErrorMessage = xmlDocument.Root.ElementOrDefault("errormessage").ValueOrDefault();
			}
			else if (status == "FAILED")
			{
				interspireResponse.Status = (int)InterspireResponseStatus.Failed;
				interspireResponse.ErrorMessage = xmlDocument.Root.ElementOrDefault("errormessage").ValueOrDefault();
			}
			else
			{
				interspireResponse.Status = (int)InterspireResponseStatus.Unknown;
			}
		}
	}
}