﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class NewsletterUnsubscriberOption : BusinessObjectBase, INewsletterUnsubscriberOption
	{
		private int _id;
		private int _unsubscriberId;
		private int _newsletterTypeId;
		private DateTime _createdDate;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int UnsubscriberId
		{
			get { return _unsubscriberId; }
			set
			{
				CheckChanged(_unsubscriberId, value);
				_unsubscriberId = value;
			}
		}

		public int NewsletterTypeId
		{
			get { return _newsletterTypeId; }
			set
			{
				CheckChanged(_newsletterTypeId, value);
				_newsletterTypeId = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}
	}
}