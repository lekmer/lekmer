﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterSubscriberSetting : SettingBase, INewsletterSubscriberSetting
	{
		public string ApplicationName
		{
			get { return GetString(GroupName, "ApplicationName"); }
		}

		public string PostUrl
		{
			get { return GetString(GroupName, "posturl"); }
		}

		public string UserName
		{
			get { return GetString(GroupName, "username"); }
		}

		public string UserToken
		{
			get { return GetString(GroupName, "usertoken"); }
		}

		public string[] GetMailingList(int channelId)
		{
			var emailLists = GetString(GroupName, "mailinglist" + channelId).Split(',');
			return emailLists;
		}

		public bool SubscribeFromOrders
		{
			get { return GetBoolean(GroupName, "SubscribeFromOrders", false); }
		}

		protected virtual string GroupName
		{
			get { return "NewsletterSubscriberSetting"; }
		}

		protected override string StorageName
		{
			get { return "NewsletterSubscriber"; }
		}
	}
}