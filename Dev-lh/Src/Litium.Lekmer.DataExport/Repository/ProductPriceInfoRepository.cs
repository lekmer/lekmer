using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DataExport.Repository
{
	public class ProductPriceInfoRepository
	{
		protected virtual DataMapperBase<IProductPriceInfo> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductPriceInfo>(dataReader);
		}

		public virtual Collection<IProductPriceInfo> GetAll(int channelId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("ProductPriceInfoRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pProductPriceGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Save(IProductPriceInfo productPriceInfo)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ChannelId", productPriceInfo.ChannelId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductId", productPriceInfo.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@PriceIncludingVat", productPriceInfo.PriceIncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@PriceExcludingVat", productPriceInfo.PriceExcludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@DiscountPriceIncludingVat", productPriceInfo.DiscountPriceIncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@DiscountPriceExcludingVat", productPriceInfo.DiscountPriceExcludingVat, SqlDbType.Decimal)
			};

			var dbSettings = new DatabaseSetting("ProductPriceInfoRepository.Save");

			new DataHandler().ExecuteCommand("[export].[pProductPriceSave]", parameters, dbSettings);
		}

		public virtual void Delete(int channelId, string productIdList, char delimiter)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductIds", productIdList, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
			};

			var dbSettings = new DatabaseSetting("ProductPriceInfoRepository.Delete");

			new DataHandler().ExecuteCommand("[export].[pProductPriceDeleteByIdList]", parameters, dbSettings);
		}
	}
}