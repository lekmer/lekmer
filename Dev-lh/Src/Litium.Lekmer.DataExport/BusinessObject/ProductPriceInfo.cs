using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DataExport
{
	[Serializable]
	public class ProductPriceInfo : BusinessObjectBase, IProductPriceInfo
	{
		private int _channelId;
		private int _productId;
		private decimal _priceIncludingVat;
		private decimal _priceExcludingVat;
		private decimal? _discountPriceIncludingVat;
		private decimal? _discountPriceExcludingVat;

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public decimal PriceIncludingVat
		{
			get { return _priceIncludingVat; }
			set
			{
				CheckChanged(_priceIncludingVat, value);
				_priceIncludingVat = value;
			}
		}

		public decimal PriceExcludingVat
		{
			get { return _priceExcludingVat; }
			set
			{
				CheckChanged(_priceExcludingVat, value);
				_priceExcludingVat = value;
			}
		}

		public decimal? DiscountPriceIncludingVat
		{
			get { return _discountPriceIncludingVat; }
			set
			{
				CheckChanged(_discountPriceIncludingVat, value);
				_discountPriceIncludingVat = value;
			}
		}

		public decimal? DiscountPriceExcludingVat
		{
			get { return _discountPriceExcludingVat; }
			set
			{
				CheckChanged(_discountPriceExcludingVat, value);
				_discountPriceExcludingVat = value;
			}
		}
	}
}