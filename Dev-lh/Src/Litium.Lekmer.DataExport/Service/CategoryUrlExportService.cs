﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using log4net;

namespace Litium.Lekmer.DataExport
{
	public class CategoryUrlExportService : ICategoryUrlExportService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IEnumerable<IUserContext> _userContexts;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected IContentNodeService ContentNodeService { get; private set; }

		public CategoryUrlExportService(ILekmerChannelService lekmerChannelService, ICategoryService categoryService, IContentNodeService contentNodeService)
		{
			LekmerChannelService = lekmerChannelService;
			CategoryService = categoryService;
			ContentNodeService = contentNodeService;
		}

		public virtual void ExportCategoryUrl()
		{
			_log.Info("ExportCategoryUrl execution is started.");

			_userContexts = GetUserContextForAllChannels();
			foreach (IUserContext context in _userContexts)
			{
				_log.InfoFormat("Reading categories is started. //{0}", context.Channel.CommonName);

				var categoryTrees = CategoryService.GetAllAsTree(context);

				string csvFilePath = string.Format("CategoryUrls_{0}", context.Channel.CommonName);
				_log.InfoFormat("File saving... //{0}", csvFilePath);

				if (File.Exists(csvFilePath))
				{
					File.Delete(csvFilePath);
				}
				using (Stream stream = File.OpenWrite(csvFilePath))
				{
					var writer = new StreamWriter(stream);

					GetUrl(context, categoryTrees.Root.Children, writer, string.Empty);

					writer.Flush();
					writer.Close();
				}

				_log.InfoFormat("File is saved //{0}", csvFilePath);
			}

			_log.Info("ExportCategoryUrl is executed.");
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}

		protected virtual void GetUrl(IUserContext context, Collection<ICategoryTreeItem> items, StreamWriter writer, string previousUrl)
		{
			foreach (var item in items)
			{
				var url = GetCategoryUrl(context, item, previousUrl);
				writer.WriteLine(url + "\t" + item.Category.ErpId + "\t" + item.Category.Title + "\t" + item.Level);
				GetUrl(context, item.Children, writer, url);
			}
		}

		protected virtual string GetCategoryUrl(IUserContext context, ICategoryTreeItem categoryTreeItem, string previousObjectUrl)
		{
			int? contentNodeId = GetCategoryContentNodeId(context, categoryTreeItem);

			if (contentNodeId.HasValue)
			{
				string categoryUrl = GetContentNodeUrl(context, contentNodeId.Value);
				if (categoryUrl != null)
				{
					return ResolveUrlHttp(context, categoryUrl);
				}
			}

			if (string.IsNullOrEmpty(previousObjectUrl))
			{
				return string.Empty;
			}

			previousObjectUrl = RemoveLastSlash(previousObjectUrl);
			string appUrl = RemoveLastSlash(GetApplicationUrl(context));

			if (previousObjectUrl == appUrl)
			{
				return string.Empty;
			}

			string url = string.Empty;

			int position = previousObjectUrl.LastIndexOf('/');
			if (position > 0)
			{
				url = previousObjectUrl.Substring(0, position + 1);
			}

			return url;
		}

		protected virtual int? GetCategoryContentNodeId(IUserContext context, ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null || categoryTreeItem.Category == null)
			{
				return null;
			}

			if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
			{
				return categoryTreeItem.Category.ProductParentContentNodeId.Value;
			}

			foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
			{
				if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
				{
					return parentItem.Category.ProductParentContentNodeId.Value;
				}
			}

			return null;
		}

		protected virtual string GetContentNodeUrl(IUserContext context, int contentNodeId)
		{
			var treeItem = ContentNodeService.GetTreeItemById(context, contentNodeId);
			if (treeItem == null || treeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return treeItem.Url;
		}

		protected virtual string ResolveUrlHttp(IUserContext context, string relativeUrl)
		{
			if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

			if (!relativeUrl.StartsWith("~/", StringComparison.Ordinal))
			{
				return relativeUrl;
			}

			return GetApplicationUrl(context) + relativeUrl.Substring(1);
		}

		protected virtual string GetApplicationUrl(IUserContext context)
		{
			return "http://" + context.Channel.ApplicationName;
		}

		protected virtual string RemoveLastSlash(string url)
		{
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.Substring(0, url.Length - 1);
			}

			return url;
		}
	}
}