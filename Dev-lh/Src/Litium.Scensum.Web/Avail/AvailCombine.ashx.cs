﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web;
using Litium.Lekmer.Avail.Web.AvailCombine;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Avail
{
	public class AvailCombine : HandlerBase
	{
		private const int DEFAULT_COLUMNS_COUNT = 4;
		private const int DEFAULT_ROWS_COUNT = 2;

		private Template.Engine.Template _template;
		private Collection<IProduct> _products = new Collection<IProduct>();
		private IPagingControl _pagingControl;
		private int _numberOfColumns;
		private int _numberOfRows;
		private string _availTitle;
		private string _trackingCode;
		private int? _contentNodeId;
		private IEnumerable<IProduct> _filteredProducts;
		private string _addedtobasket;

		protected override void ProcessRequest()
		{
			var query = Request.QueryString["ids"];
			if (query.IsEmpty())
			{
				return;
			}

			var templateIdParameter = Request.QueryString["templateid"];
			if (templateIdParameter.IsEmpty())
			{
				return;
			}
			var templateId = Request.QueryString.GetInt32("templateid");
			
			var nodeIdParameter = Request.QueryString["nodeId"];
			_contentNodeId = nodeIdParameter.IsEmpty() ? null : Request.QueryString.GetInt32OrNull("nodeId");

			var numberOfColumnsParameter = Request.QueryString["columns"];
			_numberOfColumns = numberOfColumnsParameter.IsEmpty() ? DEFAULT_COLUMNS_COUNT : Request.QueryString.GetInt32("columns");

			var numberOfRowsParameter = Request.QueryString["rows"];
			_numberOfRows = numberOfRowsParameter.IsEmpty() ? DEFAULT_ROWS_COUNT : Request.QueryString.GetInt32("rows");

			_availTitle = Request.QueryString["availtitle"];
			_trackingCode = Request.QueryString["trackingcode"];
			_addedtobasket = Request.QueryString["addedtobasket"];

			string[] productIdsString = query.Split(',');

			List<int> productIdsInt = new List<int>();

			for (int i = 0; i < productIdsString.Length; i++)
			{
				int productrId;
				if (int.TryParse(productIdsString[i], out productrId))
				{
					productIdsInt.Add(productrId);
				}
			}

			// Remove duplicates.
			var ids = new List<int>();
			foreach (var productId in productIdsInt.Where(productId => !ids.Contains(productId)))
			{
				ids.Add(productId);
			}
			var productIds = new ProductIdCollection(ids);

			var productService = IoC.Resolve<ILekmerProductService>();
			var products = productService.PopulateViewProducts(UserContext.Current, productIds);
			
			_products = new Collection<IProduct>(products.Where(product => product.NumberInStock > 0 && !((LekmerProduct)product).HasSizes).ToList());
			_products.AddRange(productService.SupplementProductsWithSizes(products.Cast<ILekmerProductView>()));
		 
			_template = GetTemplate(templateId);

			Response.Write(RenderProductList());
		}

		private void Initialize()
		{
			_pagingControl = CreatePagingControl();
			_filteredProducts = _products.Skip((_pagingControl.SelectedPage - 1) * _pagingControl.PageSize).Take(_pagingControl.PageSize);
		}

		protected AvailPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<AvailPagingControl>();

			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = "page";
			pagingControl.PageSize = _numberOfColumns * _numberOfRows;
			pagingControl.Initialize();

			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Product url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		private string RenderProductList()
		{
			Initialize();

			if (_products.Count == 0)
			{
				return null;
			}

			_pagingControl.TotalCount = _products.Count;
			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentProductList = _template.GetFragment("AvailContent");

			fragmentProductList.AddVariable("AvailTitle", _availTitle, VariableEncoding.None);
			fragmentProductList.AddVariable("ProductList", RenderProducts(), VariableEncoding.None);
			fragmentProductList.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);

			return fragmentProductList.Render();
		}

		private string RenderProducts()
		{
			return CreateGridControl().Render();
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual AvailGridControl<IProduct> CreateGridControl()
		{
			return new AvailGridControl<IProduct>
			{
				Items = new Collection<IProduct>(_filteredProducts.ToList()),
				ColumnCount = _numberOfColumns,
				RowCount = _numberOfRows,
				Template = _template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace",
				WasSearched = WasSearched,
				TrackingCode = _trackingCode,
				RelativeUrl = RelativeUrl(Request.UrlReferrer),
				ContentNodeId = _contentNodeId,
				Addedtobasket = _addedtobasket,
				IsSecureConnection = Request.IsSecureConnection()
			};
		}

		private static Template.Engine.Template GetTemplate(int templateId)
		{
			return IoC.Resolve<ITemplateFactory>().Create(templateId);
		}

		private static string RelativeUrl(Uri uri)
		{
			if (uri == null)
			{
				return string.Empty;
			}

			// Get full url.
			string url = uri.AbsolutePath;

			// Take url and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath ?? string.Empty;
			string relativeUrl = url.Substring(appPath.Length);
			if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
			{
				relativeUrl = "/" + relativeUrl;
			}

			// Return relative url.
			return relativeUrl;
		}

		private bool WasSearched
		{
			get
			{
				return Request.UrlReferrer == null ? false : Request.UrlReferrer.Query.Contains("q");
			}
		}
	}
}