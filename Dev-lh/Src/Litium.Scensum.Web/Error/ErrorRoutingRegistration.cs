using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Error
{
	public static class ErrorRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");
			routes.Add(new Route(@"exceptiontest", new RouteHandler("~/Error/ExceptionTest.ashx")));
			routes.Add(new Route(@"{*url}", new RouteHandler("~/Error/PageNotFound.ashx")));
		}
	}
}