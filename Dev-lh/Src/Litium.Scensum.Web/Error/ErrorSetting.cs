using System;
using Litium.Framework.Setting;

namespace Litium.Scensum.Web.Error
{
	public sealed class ErrorSetting : SettingBase
	{
		#region Singleton

		private static readonly ErrorSetting _instance = new ErrorSetting();

		private ErrorSetting()
		{
		}

		public static ErrorSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "Error"; }
		}

		public bool ShowFriendlyError
		{
			get
			{
				return GetBoolean(_groupName, "ShowFriendlyError");
			}
		}

		public bool Send500StatusCode
		{
			get
			{
				return GetBoolean(_groupName, "Send500StatusCode", false);
			}
		}

		public bool Send404StatusCode
		{
			get
			{
				return GetBoolean(_groupName, "Send404StatusCode", false);
			}
		}
	}
}