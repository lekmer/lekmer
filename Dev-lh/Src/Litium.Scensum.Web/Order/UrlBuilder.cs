//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Text;
//using System.Web;

//namespace Litium.Scensum.Web.Order
//{
//    internal class UrlBuilder
//    {
//        private readonly Collection<KeyValuePair<string, object>> _parameters = new Collection<KeyValuePair<string, object>>();

//        public UrlBuilder()
//        {
//        }

//        public UrlBuilder(string baseUrl)
//        {
//            if (baseUrl == null) throw new ArgumentNullException("baseUrl");

//            BaseUrl = baseUrl;
//        }

//        public string BaseUrl { get; set; }

//        public void AddParameter(string name, object value)
//        {
//            if (name == null) throw new ArgumentNullException("name");

//            _parameters.Add(new KeyValuePair<string, object>(name, value));
//        }

//        public override string ToString()
//        {
//            var url = new StringBuilder();

//            if (!string.IsNullOrEmpty(BaseUrl))
//            {
//                url.Append(BaseUrl);
//            }

//            if (!string.IsNullOrEmpty(BaseUrl) && _parameters.Count > 0)
//            {
//                url.Append("?");
//            }

//            bool first = true;
//            foreach (KeyValuePair<string, object> parameter in _parameters)
//            {
//                if (!first)
//                {
//                    url.Append("&");
//                }

//                url.Append(HttpUtility.UrlEncode(parameter.Key));
//                url.Append("=");
//                if (parameter.Value != null)
//                {
//                    url.Append(HttpUtility.UrlEncode(parameter.Value.ToString()));
//                }

//                first = false;
//            }

//            return url.ToString();
//        }
//    }
//}