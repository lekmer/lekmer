using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.Web.Lekmer
{
	public class ProductUrlHistoryRoute : Route
	{
		public ProductUrlHistoryRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public ProductUrlHistoryRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
			: base(url, defaults, routeHandler)
		{
		}

		public ProductUrlHistoryRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, IRouteHandler routeHandler)
			: base(url, defaults, constraints, routeHandler)
		{
		}

		public ProductUrlHistoryRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, RouteValueDictionary dataTokens, IRouteHandler routeHandler)
			: base(url, defaults, constraints, dataTokens, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null)
			{
				throw new ArgumentNullException("httpContext");
			}

			RouteData data = base.GetRouteData(httpContext);
			if (data == null)
			{
				return null;
			}

			string url = (data.Values["url"] ?? string.Empty).ToString();
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				return null;
			}

			string urlTitle;
			string parentPageUrl;
			int slashIndex = url.LastIndexOf('/');
			if (slashIndex != -1)
			{
				urlTitle = url.Substring(slashIndex + 1);
				parentPageUrl = url.Substring(0, slashIndex + 1);
			}
			else
			{
				urlTitle = url;
				parentPageUrl = null;
			}

			urlTitle = urlTitle.Replace('-', ' ');

			var productUrlHistoryService = IoC.Resolve<IProductUrlHistoryService>();

			Collection<IProductUrlHistory> productUrlHistoryList = productUrlHistoryService.GetAllByUrlTitle(UserContext.Current, urlTitle);

			if (productUrlHistoryList == null || productUrlHistoryList.Count == 0)
			{
				return null;
			}

			var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
			var product = productService.GetViewById(UserContext.Current, productUrlHistoryList[0].ProductId, true, false);
			if (product == null)
			{
				return null;
			}

			httpContext.Items["UrlProductComplete"] = RelativeUrl(httpContext.Request);
			httpContext.Items["UrlProductTitle"] = urlTitle;
			httpContext.Items["UrlProductId"] = productUrlHistoryList[0].ProductId;
			httpContext.Items["UrlParentPageUrl"] = parentPageUrl;

			productUrlHistoryService.Update(productUrlHistoryList[0].Id);

			return data;
		}

		public static string RelativeUrl(HttpRequestBase request)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}

			// Get full url.
			string url = request.ServerVariables["HTTP_URL"];

			// Take url and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath;
			string relativeUrl = appPath.IsEmpty() ? url : url.Substring(appPath.Length);

			if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
			{
				relativeUrl = "/" + relativeUrl;
			}

			int questionMarkIndex = relativeUrl.IndexOf('?');
			if (questionMarkIndex != -1)
			{
				relativeUrl = relativeUrl.Substring(0, questionMarkIndex);
			}

			// Return relative url.
			return relativeUrl;
		}
	}
}