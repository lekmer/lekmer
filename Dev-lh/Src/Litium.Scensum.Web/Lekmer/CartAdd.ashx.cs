using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Avail.Web.Helper;
using Litium.Lekmer.Common;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Cart;
using Litium.Scensum.Product;

namespace Litium.Scensum.Web.Lekmer
{
	public class CartAdd : CartAddHandler
	{
		private readonly ICartService _cartService = IoC.Resolve<ICartService>();
		private readonly ILekmerProductService _productService = (ILekmerProductService) IoC.Resolve<IProductService>();
		private CartAddHelper _cartAddHelper;
		private int _productId;
		private int? _sizeId;
		private IProduct _product;
		private Collection<ICartItemPackageElement> _cartItemPackageElements;

		protected override void ProcessRequest()
		{
			var crawlerDetector = IoC.Resolve<ICrawlerDetectorService>();
			if (Request.Browser.Crawler || crawlerDetector.IsMatch(Request.UserAgent))
			{
				Response.Write(@"<!-- CD -->");
				Response.DisablePageCaching();
				return;
			}

			_cartAddHelper = new CartAddHelper(Request.QueryString);
			_cartItemPackageElements = _cartAddHelper.GetCartItemPackageElements();
			ICartFull cart = GetCart();
			int? productId = GetProductId();
			if (productId.HasValue)
			{
				_product = GetProduct(productId.Value);
				if (_product == null || !ValidateSize() || !ValidatePackageProducts())
				{
					HandleEmptyCart(cart);
					return;
				}

				AddItemToCart(productId.Value, cart, GetQuantity());

				Session["CartItemAdded"] = productId.Value;

				_cartAddHelper.GetAvailSessionId();

				var availLoggingHelper = new AvailLoggingHelper();
				string trackingCode = _cartAddHelper.GetTrackingCode();
				availLoggingHelper.SendAvailLogAddedToCart(productId.Value, trackingCode);
			}

			HandleEmptyCart(cart);

			if (AjaxHelper.IsXmlHttpRequest(Request))
			{
				Response.Write(AjaxHelper.RenderCart());
			}
			else
			{
				Response.Redirect(GetRedirectUrl());
			}
		}

		protected override void AddItemToCart(int productId, ICartFull cart, int quantity)
		{
			if (cart == null) throw new ArgumentNullException("cart");

			if (_product != null)
			{
				((ILekmerCartFull)cart).ChangeItemQuantity(UserContext.Current, _product, quantity, _sizeId, _cartItemPackageElements);
				cart.Id = _cartService.Save(UserContext.Current, cart);
				RefreshCart();
			}
		}

		protected virtual bool ValidatePackageProducts()
		{
			bool isValid = true;

			if (!_product.IsPackage()) return isValid;

			var productWithSizes = new List<int>();

			var productIds = _productService.GetIdAllByPackageMasterProduct(UserContext.Current, _productId);
			var products = _productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
			foreach (ILekmerProduct product in products)
			{
				if (product.HasSizes)
				{
					productWithSizes.Add(product.Id);
				}
			}

			var cartItemPackageElements = new Collection<ICartItemPackageElement>();
			foreach (var element in _cartItemPackageElements)
			{
				var cartItemPackageElement = IoC.Resolve<ICartItemPackageElement>();
				cartItemPackageElement.ProductId = element.ProductId;
				cartItemPackageElement.SizeId = element.SizeId;
				cartItemPackageElements.Add(cartItemPackageElement);
			}

			if (productWithSizes.Count != cartItemPackageElements.Count)
			{
				return false;
			}

			foreach (var productId in productWithSizes)
			{
				var packageElement = cartItemPackageElements.FirstOrDefault(e => e.ProductId == productId);
				if (packageElement == null || packageElement.SizeId == null || packageElement.SizeId <= 0)
				{
					isValid = false;
					break;
				}
				cartItemPackageElements.Remove(packageElement);
			}

			return isValid;
		}

		protected virtual bool ValidateSize()
		{
			bool isValid = true;

			_sizeId = _cartAddHelper.GetSizeId();
			var lekmerProduct = (ILekmerProduct) _product;
			if ((lekmerProduct.HasSizes && _sizeId == null)
				|| (!lekmerProduct.HasSizes && _sizeId != null))
			{
				isValid = false;
			}

			return isValid;
		}

		protected virtual void HandleEmptyCart(ICartFull cart)
		{
			if (cart == null || cart.Id != 0) return;

			if (cart.GetCartItems().Count > 0)
			{
				cart.Id = _cartService.Save(UserContext.Current, cart);
			}
			else
			{
				IoC.Resolve<ICartSession>().Cart = null;
			}
		}

		protected override int? GetProductId()
		{
			int? productId = _cartAddHelper.GetProductId();
			if (productId != null)
			{
				_productId = productId.Value;
			}
			return productId;
		}

		protected override int GetQuantity()
		{
			return _cartAddHelper.GetQuantity();
		}

		protected override string GetRedirectUrl()
		{
			return _cartAddHelper.GetRedirectUrl(_productId);
		}

		protected override ICartFull CreateCart()
		{
			return ((ILekmerCartService) _cartService).Create(UserContext.Current, Request.UserHostAddress);
		}
	}
}