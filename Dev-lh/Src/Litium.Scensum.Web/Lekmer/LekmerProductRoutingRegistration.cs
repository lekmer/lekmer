using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Lekmer
{
	public static class LekmerProductRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			routes.Add(new LekmerProductRoute(@"{*url}", new RouteHandler("~/Product/ProductPage.ashx")));
		}
	}
}