using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Lekmer
{
	internal struct AreaBlockPair
	{
		private readonly IContentArea _contentArea;
		private readonly IBlock _block;

		public AreaBlockPair(IContentArea contentArea, IBlock block)
		{
			_contentArea = contentArea;
			_block = block;
		}

		public IContentArea ContentArea
		{
			get { return _contentArea; }
		}

		public IBlock Block
		{
			get { return _block; }
		}
	}
}