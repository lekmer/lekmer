using System;
using System.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.Web.Lekmer
{
	public static class AjaxHelper
	{
		public static bool IsXmlHttpRequest(HttpRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			string header = request.Headers["X-Requested-With"];
			if (header == null) return false;

			return header.Equals("XMLHttpRequest", StringComparison.OrdinalIgnoreCase);
		}

		public static string RenderCart()
		{
			var includeService = IoC.Resolve<IIncludeSharedService>();
			var include = includeService.GetByCommonName("MiniCart");

			// Use a master to get variables such as [Channel.MediaUrl].
			var master = new EmptyMaster();
			master.Page.Content = include.Content;
			return master.Render();
		}

		public static string RenderWishList()
		{
			var includeService = IoC.Resolve<IIncludeSharedService>();
			var include = includeService.GetByCommonName("WishList");

			// Use a master to get variables such as [Channel.MediaUrl].
			var master = new EmptyMaster();
			master.Page.Content = include.Content;
			return master.Render();
		}
	}
}