using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Litium.Scensum.Web.Lekmer.OldUrl
{
	public static class OldUrlService
	{
		public static string GetRedirectUrl(string oldUrl)
		{
			var redirect = GetAllRedirects()
				.FirstOrDefault(r => r.Source.Equals(oldUrl, StringComparison.OrdinalIgnoreCase));

			return redirect != null ? redirect.Target : null;
		}

		private static IEnumerable<Redirect> GetAllRedirects()
		{
			return RedirectListCache.Instance.TryGetItem(
				new RedirectListKey(),
				GetAllRedirectsInternal);
		}

		private static IEnumerable<Redirect> GetAllRedirectsInternal()
		{
			string fileContent = ReadRedirectFile();
			fileContent = fileContent
				.Replace(Environment.NewLine, "\n")
				.Replace('\r', '\n');

			var rows = fileContent
				.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);

			var redirects = new List<Redirect>();

			foreach (string row in rows)
			{
				var columns = row
					.Split(new[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);

				if (columns.Length < 2) continue;

				var redirect = new Redirect
				{
					Source = columns[0].Trim(),
					Target = columns[1].Trim()
				};
				redirects.Add(redirect);
			}

			return redirects;
		}

		private static string ReadRedirectFile()
		{
			string fullPath = Path.GetFullPath(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "redirect.txt"));

			if (!File.Exists(fullPath))
			{
				throw new ConfigurationErrorsException(
					string.Format(CultureInfo.InvariantCulture, "The file '{0}' does not exists.", fullPath));
			}

			return File.ReadAllText(fullPath);
		}
	}
}