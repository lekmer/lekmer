using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Lekmer.OldUrl;

namespace Litium.Scensum.Web.Lekmer
{
	public class ContentPageUrlHistoryRoute : Route
	{
		private readonly ILekmerContentNodeService _contentNodeService = IoC.Resolve<ILekmerContentNodeService>();
		private readonly IContentPageUrlHistoryService _contentPageUrlHistoryService = IoC.Resolve<IContentPageUrlHistoryService>();

		public ContentPageUrlHistoryRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null)
			{
				throw new ArgumentNullException("httpContext");
			}

			RouteData data = base.GetRouteData(httpContext);
			if (data == null)
			{
				return null;
			}

			string url = (data.Values["url"] ?? "").ToString();

			if (url.IsEmpty())
			{
				return null;
			}

			if (url.Equals("/"))
			{
				return null;
			}

			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.TrimEnd('/');
			}

			IContentNodeTree contentNodeTree = _contentNodeService.GetAllAsTree(UserContext.Current);
			var treeItemsHash = _contentNodeService.GetAllAsTreeItemsHash(UserContext.Current);
			
			IContentNodeTreeItem item = FindValidNode(httpContext, url, contentNodeTree, treeItemsHash);

			if (item == null)
			{
				return null;
			}

			var contentPage = item.ContentNode as IContentPage;
			if (contentPage == null)
			{
				return null;
			}

			httpContext.Response.DisablePageCaching();
			httpContext.Response.PermanentRedirect("~/" + item.PageUrl);

			return null;
		}

		protected virtual IContentNodeTreeItem FindValidNode(HttpContextBase httpContext, string url, IContentNodeTree contentNodeTree, ITreeItemsHash treeItemsHash)
		{
			if (url.IsEmpty())
			{
				return null;
			}

			// Find url in url hash
			string treeUrl = url;
			if (!treeUrl.EndsWith("/", StringComparison.Ordinal))
			{
				treeUrl += '/';
			}

			// Redirect from file (static)
			RedirectToNewPage(httpContext, treeUrl);

			IContentNodeTreeItem item = contentNodeTree.FindItemByUrl(treeUrl);
			if (item != null)
			{
				return item;
			}

			string pageUrlTitle;
			string parentPageUrl;
			int slashIndex = url.LastIndexOf('/');
			if (slashIndex != -1)
			{
				pageUrlTitle = url.Substring(slashIndex + 1);
				parentPageUrl = url.Substring(0, slashIndex);
			}
			else
			{
				pageUrlTitle = url;
				parentPageUrl = null;
			}

			// Redirect from file (static)
			RedirectToNewPage(httpContext, parentPageUrl);

			// Find node by existed url title
			if (treeItemsHash.ContainsKey(pageUrlTitle))
			{
				// Return first if there will more than one
				return treeItemsHash.FirstOrDefault(pageUrlTitle);
			}

			// Find node by old url title
			var contentPageUrlHistories = _contentPageUrlHistoryService.GetAllByUrlTitle(UserContext.Current, pageUrlTitle);
			if (contentPageUrlHistories != null && contentPageUrlHistories.Count > 0)
			{
				int contentPageId = contentPageUrlHistories[0].ContentPageId;
				IContentNodeTreeItem contentNodeTreeItem = contentNodeTree.FindItemById(contentPageId);
				if (contentNodeTreeItem != null)
				{
					_contentPageUrlHistoryService.Update(contentPageUrlHistories[0].Id);
					return contentNodeTreeItem;
				}
			}

			if (parentPageUrl.HasValue())
			{
				return FindValidNode(httpContext, parentPageUrl, contentNodeTree, treeItemsHash);
			}

			return null;
		}

		private static void RedirectToNewPage(HttpContextBase httpContext, string url)
		{
			if (url.IsNullOrEmpty() || url.Equals("/")) return;

			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.TrimEnd('/');
			}

			if (!url.StartsWith("/", StringComparison.Ordinal))
			{
				url = "/" + url;
			}

			var redirectUrl = OldUrlService.GetRedirectUrl(url);
			if (redirectUrl.IsNullOrEmpty()) return;

			httpContext.Response.DisablePageCaching();
			httpContext.Response.PermanentRedirect(redirectUrl);
		}
	}
}