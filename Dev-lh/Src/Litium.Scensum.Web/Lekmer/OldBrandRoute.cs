﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Web.Lekmer
{
	public class OldBrandRoute : Route
	{
		private static readonly Regex _oldUrlRegex = new Regex(@"^\/varumarke\/([^\/]+)$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		public OldBrandRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			if (data == null) return null;

			string relativeUrl = RelativeUrl(httpContext.Request);

			Match oldUrlMatch = _oldUrlRegex.Match(relativeUrl);
			if (!oldUrlMatch.Success) return null;

			string urlBrandTitle = oldUrlMatch.Groups[1].Value;

			string title = HttpUtility.UrlDecode(urlBrandTitle.Replace('_', '+'), Encoding.GetEncoding("iso-8859-1"));

			var brandService = IoC.Resolve<IBrandService>();
			int? brandId = brandService.GetIdByTitle(UserContext.Current, title);


			// if we do not get hit on brandid try with utf-8 encoding (due to redirect logic)
			if (!brandId.HasValue)
			{
				title = HttpUtility.UrlDecode(urlBrandTitle.Replace('_', '+'), Encoding.UTF8);
				brandId = brandService.GetIdByTitle(UserContext.Current, title);
			}

			if (!brandId.HasValue)
			{
				return null;
			}

			httpContext.Items["BrandId"] = brandId.Value;

			return data;
		}

		public static string RelativeUrl(HttpRequestBase request)
		{
			if (request == null) throw new ArgumentNullException("request");

			// Get full url.
			string url = request.ServerVariables["HTTP_URL"];

			// Take url and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath;
			string relativeUrl = url.Substring(appPath.Length);
			if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
			{
				relativeUrl = "/" + relativeUrl;
			}

			// Return relative url.
			return relativeUrl;
		}
	}
}