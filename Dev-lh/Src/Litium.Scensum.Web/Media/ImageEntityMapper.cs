using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Media;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Media
{
	public class ImageEntityMapper : EntityMapper<IImage>
	{
		private const string _imageUrlMatchPattern = @"\[Image.Url\(""(.*?)""\)\]";
		private const string _imageWidthMatchPattern = @"\[Image.Width\(""(.*?)""\)\]";
		private const string _imageHeightMatchPattern = @"\[Image.Height\(""(.*?)""\)\]";
		private const int _imageUrlMatchCommonNamePosition = 1;

		public override void AddEntityVariables(Fragment fragment, IImage item)
		{
			fragment.AddVariable("Image.Link", ((ILekmerImage)item).Link);
			fragment.AddVariable("Image.Parameter", ((ILekmerImage)item).Parameter);
			fragment.AddCondition("Image.HasTumbnailImageUrl", !string.IsNullOrEmpty(((ILekmerImage)item).TumbnailImageUrl));
			fragment.AddVariable("Image.TumbnailImageUrl", ((ILekmerImage)item).TumbnailImageUrl);
			fragment.AddCondition("Image.IsLink", ((ILekmerImage)item).IsLink);
			fragment.AddCondition("Image.HasImage", ((ILekmerImage)item).HasImage);

			fragment.AddVariable("Image.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Image.Title", item.Title);
			fragment.AddVariable("Image.AlternativeText", item.AlternativeText);

			fragment.AddVariable("Image.Width", item.Width.ToString(CultureInfo.CurrentCulture));
			fragment.AddVariable("Image.Height", item.Height.ToString(CultureInfo.CurrentCulture));

			string mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(Channel.Current, item);

			fragment.AddVariable("Image.Url", MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, item));

			fragment.AddRegexVariable(
				_imageUrlMatchPattern,
				delegate(Match match)
				{
					string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

					return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, item, commonName);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageWidthMatchPattern,
				delegate(Match match)
				{
					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(item, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageHeightMatchPattern,
				delegate(Match match)
				{
					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(item, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}
	}
}