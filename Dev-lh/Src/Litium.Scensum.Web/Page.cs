﻿using System.Globalization;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Scensum.Web
{
	public class Page : Core.Web.Page
	{
		public virtual int? ActiveContentNodeId { get; set; }

		protected override void AddStateVariables(Fragment fragment)
		{
			/*Base implementation*/
			string relativeUrl = Request.RelativeUrl();
			fragment.AddVariable("Page.CurrentRelativeUrl", relativeUrl, VariableEncoding.None);
			fragment.AddVariable("Page.CurrentRelativeUrl.UrlEncode", relativeUrl, VariableEncoding.UrlEncode);
			fragment.AddCondition("Page.HttpsEnabled", Request.IsSecureConnection());
			/*Base implementation*/

			fragment.AddVariable("ActiveContentNodeId", ActiveContentNodeId.HasValue ? ActiveContentNodeId.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
		}
	}
}
