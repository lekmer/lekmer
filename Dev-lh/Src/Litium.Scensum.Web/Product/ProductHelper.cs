using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Web;
using Litium.Lekmer.Core;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using Channel = Litium.Scensum.Core.Web.Channel;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Scensum.Web.Product
{
	internal class ProductHelper
	{
		private const string _imageUrlMatchPattern = @"\[Product.ImageUrl\(""(.*?)""\)\]";
		private const string _flagsMatchPattern = @"\[Product.CampaignFlags\(""(\d*?)""\)\]";
		private const string _imageWidthMatchPattern = @"\[Product.ImageWidth\(""(.*?)""\)\]";
		private const string _imageHeightMatchPattern = @"\[Product.ImageHeight\(""(.*?)""\)\]";
		private const int _imageUrlMatchCommonNamePosition = 1;

		private const string _productUrlMatchPattern = @"\[Product.Url\(""(.*?)""\)\]";
		private const int _productUrlMatchCommonNamePosition = 1;

		private static IFlagService _flagService;
		private static IFlagService FlagService
		{
			get { return _flagService ?? (_flagService = IoC.Resolve<IFlagService>()); }
		}

		internal static void AddImageVariables(Fragment fragment, IImage image, string productTitle)
		{
			string mediaUrl = string.Empty;

			string originalSizeImageUrl = null;
			string alternativeText = null;
			if (image != null)
			{
				mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveUrl(Channel.Current, image);
				alternativeText = image.AlternativeText;
				originalSizeImageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image, productTitle);
			}

			fragment.AddCondition("Product.HasImage", image != null);
			fragment.AddVariable("Product.ImageUrl", originalSizeImageUrl);
			fragment.AddCondition("Product.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
			fragment.AddVariable("Product.ImageAlternativeText", alternativeText);
			fragment.AddVariable("Product.ImageWidth", image != null ? image.Width.ToString(CultureInfo.CurrentCulture) : null);
			fragment.AddVariable("Product.ImageHeight", image != null ? image.Height.ToString(CultureInfo.CurrentCulture) : null);

			fragment.AddRegexVariable(
				_imageUrlMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, image, productTitle, commonName);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageWidthMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetWidth(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);

			fragment.AddRegexVariable(
				_imageHeightMatchPattern,
				delegate(Match match)
				{
					if (image == null) return null;

					var commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;
					return ImageSizeUtil.GetHeight(image, commonName);
				},
				VariableEncoding.None,
				RegexOptions.Compiled);
		}

		internal static void AddCampaignAppliedConditions(Fragment fragment, IProduct item)
		{
			fragment.AddCondition("Product.HasProductDiscountAction", HasCampaignActionApplied(item, "ProductDiscount"));
			fragment.AddCondition("Product.HasPercentagePriceDiscountAction", HasCampaignActionApplied(item, "PercentagePriceDiscount"));
		}
		private static bool HasCampaignActionApplied(IProduct item, string actionCommonName)
		{
			var campaignInfo = (ILekmerProductCampaignInfo) item.CampaignInfo;
			return campaignInfo.CampaignActionsApplied.Any(action => action.ActionCommonName.Equals(actionCommonName));
		}

		internal static void AddFlags(Fragment fragment, IProduct item)
		{
			var fragmentInfo = new FragmentVariableInfo(fragment, true);
			var isFlagVariableExists = IsFlagVariableExists(fragmentInfo, "Product.HasFlags", "Product.Flags", "Product.Flags");
			if (isFlagVariableExists)
			{
				var flags = new Collection<IFlag>();
				int maxFlagsToShow = FlagSetting.Instance.MaxFlagsToShow;
				if (maxFlagsToShow > 0)
				{
					Dictionary<int, IFlag> flagsTemp = GetFlagsFromCampaigns(item);
					flagsTemp = GetFlagsFromTags(item, flagsTemp);

					var flagsToShow = flagsTemp.Values.OrderBy(x => x.Ordinal).Take(maxFlagsToShow);
					flags = new Collection<IFlag>(flagsToShow.ToArray());
				}
				AddFlagVariables(fragment, fragmentInfo, flags, "Product.HasFlags", "Product.Flags", "Product.Flags");
			}
		}
		private static bool IsFlagVariableExists(FragmentVariableInfo fragmentInfo, string condition, string variable, string regexVariable)
		{
			return fragmentInfo.HasCondition(condition) || fragmentInfo.HasVariable(variable) || fragmentInfo.HasRegexVariable(regexVariable);
		}
		private static void AddFlagVariables(Fragment fragment, FragmentVariableInfo fragmentInfo, Collection<IFlag> flags, string condition, string variable, string regexVariable)
		{
			var productFlagsControl = IoC.Resolve<IProductFlagsControl>();

			if (fragmentInfo.HasCondition(condition))
			{
				fragment.AddCondition(condition, flags.Count > 0);
			}

			if (fragmentInfo.HasVariable(variable))
			{
				fragment.AddVariable(variable, flags.Count > 0 ? productFlagsControl.RenderFromIncludes(flags) : string.Empty, VariableEncoding.None);
			}

			if (fragmentInfo.HasRegexVariable(regexVariable))
			{
				fragment.AddRegexVariable(
					_flagsMatchPattern,
					match =>
					{
						if (flags.Count > 0)
						{
							int templateId;
							if (int.TryParse(match.Groups[1].Value, out templateId))
							{
								return productFlagsControl.Render(flags, templateId);
							}

							return productFlagsControl.RenderFromIncludes(flags);
						}

						return string.Empty;
					},
					VariableEncoding.None,
					RegexOptions.Compiled);
			}
		}
		private static Dictionary<int, IFlag> GetFlagsFromCampaigns(IProduct item)
		{
			var flags = new Dictionary<int, IFlag>();
			var campaignInfo = (ILekmerProductCampaignInfo) item.CampaignInfo;
			if (campaignInfo.CampaignActionsApplied.Count == 0)
			{
				return flags;
			}

			var campaignIds = campaignInfo.CampaignActionsApplied.Select(x => x.ProductCampaignId).Distinct();
			foreach (var campaignId in campaignIds)
			{
				var flag = FlagService.GetByCampaign(UserContext.Current, campaignId);
				if (flag != null && !flags.ContainsKey(flag.Id))
				{
					flags.Add(flag.Id, flag);
				}
			}

			return flags;
		}
		private static Dictionary<int, IFlag> GetFlagsFromTags(IProduct item, Dictionary<int, IFlag> flags)
		{
			var productFlags = FlagService.GetByProduct(UserContext.Current, item.Id);
			foreach (var flag in productFlags)
			{
				if (flag != null && !flags.ContainsKey(flag.Id))
				{
					flags.Add(flag.Id, flag);
				}
			}
			return flags;
		}

		internal static void AddRecommendedPriceData(Fragment fragment, ILekmerProduct item)
		{
			var formatter = IoC.Resolve<IFormatter>();
			fragment.AddCondition("Product.HasRecommendedPrice", item.RecommendedPrice.HasValue);
			fragment.AddVariable("Product.RecommendedPrice",
				item.RecommendedPrice.HasValue
					? formatter.FormatPrice(Channel.Current, item.RecommendedPrice.Value)
					: string.Empty);

			var absoluteSave = item.AbsoluteSave;
			fragment.AddCondition("Product.HasAbsoluteSave", absoluteSave.HasValue);
			fragment.AddVariable("Product.AbsoluteSave",
				absoluteSave.HasValue
					? formatter.FormatPrice(Channel.Current, absoluteSave.Value)
					: string.Empty);

			var percentageSave = item.PercentageSave;
			fragment.AddCondition("Product.HasPercentageSave", percentageSave.HasValue);
			fragment.AddVariable("Product.PercentageSave",
				percentageSave.HasValue
					? Math.Round(percentageSave.Value, 0, MidpointRounding.AwayFromZero) + "%"
					: string.Empty);
		}

		internal static void AddStockRangeValue(Fragment fragment, ILekmerProduct item)
		{
			bool isBuyable = false;
			bool hasStockRange = false;
			string stockRangeValue = string.Empty;

			int numberInStock;

			if (!item.HasSizes)
			{
				numberInStock = item.NumberInStock;
			}
			else
			{
				var productSizeService = IoC.Resolve<IProductSizeService>();
				var productSizes = productSizeService.GetAllByProduct(item.Id);
				numberInStock = productSizes.Where(s => s.NumberInStock > 0).Sum(s => s.NumberInStock);
			}

			if (numberInStock > 0)
			{
				isBuyable = true;

				var stockRangeService = IoC.Resolve<IStockRangeService>();
				var stockRanges = stockRangeService.GetAll(UserContext.Current);
				foreach (var stockRange in stockRanges)
				{
					if ((stockRange.StartValue == null || numberInStock >= stockRange.StartValue)
						&& (stockRange.EndValue == null || numberInStock <= stockRange.EndValue))
					{
						hasStockRange = true;
						stockRangeValue = stockRange.CommonName;
					}
				}
			}

			fragment.AddCondition("Product.IsBuyable", isBuyable);
			fragment.AddCondition("Product.HasStockRange", hasStockRange);
			fragment.AddVariable("Product.StockRangeValue", stockRangeValue);
		}

		internal static void AddLekmerProductInfo(Fragment fragment, ILekmerProduct item)
		{
			var sellOnlyInPackage = item.SellOnlyInPackage;
			fragment.AddCondition("Product.IsSellOnlyInPackage", sellOnlyInPackage);

			var isPackage = item.IsPackage();

			fragment.AddCondition("ProductType.IsProduct", item.IsProduct());
			fragment.AddCondition("ProductType.IsPackage", isPackage);

			if (isPackage)
			{
				ProductCollection packageProducts;
				fragment.AddCondition("Product.IsPackageWithSizes", IsPackageWithSizes(item, out packageProducts));
				fragment.AddCondition("Product.IsAbove60L", packageProducts.Any(product => ((ILekmerProduct)product).IsAbove60L));
				fragment.AddCondition("Product.IsPassive", packageProducts.Any(product => product.IsPassive()));
			}
			else
			{
				fragment.AddCondition("Product.IsPackageWithSizes", false);
				fragment.AddCondition("Product.IsAbove60L", item.IsAbove60L);
				fragment.AddCondition("Product.IsPassive", item.IsPassive());
			}

			fragment.AddCondition("Product.IsDropShipArticle", item.IsDropShip);
		}
		private static bool IsPackageWithSizes(ILekmerProduct item, out ProductCollection packageProducts)
		{
			bool isPackageWithSizes = false;

			var productService = (ILekmerProductService)IoC.Resolve<IProductService>();

			var productIds = productService.GetIdAllByPackageMasterProduct(UserContext.Current, item.Id);
			packageProducts = productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
			if (packageProducts.Any(product => ((ILekmerProduct)product).HasSizes))
			{
				isPackageWithSizes = true;
			}

			return isPackageWithSizes;
		}

		internal static void AddCampaignPriceType (Fragment fragment, IProduct item)
		{
			bool isOutlet = false, isCampaign = false, isLowered = false, campaignHasEndDate = false;
			DateTime? campaignEndDate = null;

			var affectedPriceAction = ((ILekmerProductCampaignInfo) item.CampaignInfo).AffectedPriceAction;
			if (affectedPriceAction != null)
			{
				var campaign = (ILekmerCampaign) IoC.Resolve<IProductCampaignService>().GetById(UserContext.Current, affectedPriceAction.ProductCampaignId);
				if (campaign != null && campaign.PriceTypeId.HasValue)
				{
					if (campaign.EndDate != null)
					{
						campaignHasEndDate = true;
						campaignEndDate = campaign.EndDate.Value;
					}

					var priceType = IoC.Resolve<ICampaignPriceTypeService>().GetById(campaign.PriceTypeId.Value);
					if (priceType != null)
					{
						switch ((CampaignPriceTypeList)Enum.Parse(typeof(CampaignPriceTypeList), priceType.CommonName))
						{
							case (CampaignPriceTypeList.OutletPrice):
								isOutlet = true;
								break;
							case (CampaignPriceTypeList.CampaignPrice):
								isCampaign = true;
								break;
							case (CampaignPriceTypeList.LoweredPrice):
								isLowered = true;
								break;
						}
					}
				}
			}

			fragment.AddCondition("Product.Campaign.IsOutletPriceType", isOutlet);
			fragment.AddCondition("Product.Campaign.IsCampaignPriceType", isCampaign);
			fragment.AddCondition("Product.Campaign.IsLoweredPriceType", isLowered);
			fragment.AddCondition("Product.Campaign.HasEndDate", campaignHasEndDate);
			fragment.AddVariable("Product.Campaign.EndDate", campaignEndDate != null ? campaignEndDate.ToString() : string.Empty);
		}

		internal static void AddDeliveryTime(Fragment fragment, ILekmerProduct lekmerProduct)
		{
			var hasDeliveryTimeRange = false;
			var hasDeliveryTimeFrom = false;
			var hasDeliveryTimeTo = false;
			var hasDeliveryTimeFormat = false;
			int? deliveryTimeFrom = null;
			int? deliveryTimeTo = null;
			string deliveryTimeFormat = null;

			var deliveryTime = GetDeliveryTime(lekmerProduct);
			if (deliveryTime != null)
			{
				hasDeliveryTimeRange = deliveryTime.FromDays.HasValue && deliveryTime.ToDays.HasValue;
				hasDeliveryTimeFrom = deliveryTime.FromDays.HasValue;
				hasDeliveryTimeTo = deliveryTime.ToDays.HasValue;
				hasDeliveryTimeFormat = !string.IsNullOrEmpty(deliveryTime.Format);
				deliveryTimeFrom = deliveryTime.FromDays;
				deliveryTimeTo = deliveryTime.ToDays;
				deliveryTimeFormat = deliveryTime.Format;
			}

			fragment.AddCondition("Product.HasDeliveryTimeRange", hasDeliveryTimeRange);
			fragment.AddCondition("Product.HasDeliveryTimeFrom", hasDeliveryTimeFrom);
			fragment.AddCondition("Product.HasDeliveryTimeTo", hasDeliveryTimeTo);
			fragment.AddCondition("Product.HasDeliveryTimeFormat", hasDeliveryTimeFormat);
			fragment.AddVariable("Product.DeliveryTimeFrom", deliveryTimeFrom.HasValue ? deliveryTimeFrom.Value.ToString(CultureInfo.InvariantCulture) : null);
			fragment.AddVariable("Product.DeliveryTimeTo", deliveryTimeTo.HasValue ? deliveryTimeTo.Value.ToString(CultureInfo.InvariantCulture) : null);
			fragment.AddVariable("Product.DeliveryTimeFormat", deliveryTimeFormat);
		}
		private static IDeliveryTime GetDeliveryTime(ILekmerProduct product)
		{
			if (product.DeliveryTimeId.HasValue && (product.DeliveryTime.FromDays.HasValue || product.DeliveryTime.ToDays.HasValue))
			{
				return product.DeliveryTime;
			}

			if (product.Brand != null && product.Brand.DeliveryTimeId.HasValue && (product.Brand.DeliveryTime.FromDays.HasValue || product.Brand.DeliveryTime.ToDays.HasValue))
			{
				return product.Brand.DeliveryTime;
			}

			var categoryTree = IoC.Resolve<ICategoryService>().GetAllAsTree(UserContext.Current);
			var category = categoryTree.FindItemById(product.CategoryId);
			var deliveryTime = GetCategoryDeliveryTime(category);
			return deliveryTime;
		}
		private static IDeliveryTime GetCategoryDeliveryTime(ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null)
			{
				return null;
			}

			var category = categoryTreeItem.Category as ILekmerCategory;
			if (category != null && category.DeliveryTimeId.HasValue && (category.DeliveryTime.FromDays.HasValue || category.DeliveryTime.ToDays.HasValue))
			{
				return category.DeliveryTime;
			}

			return GetCategoryDeliveryTime(categoryTreeItem.Parent);
		}

		internal static void AddPriceVariables(Fragment fragment, IProduct product, ILekmerFormatter formatter)
		{
			AddPriceVariables(fragment, product, formatter, Channel.Current);
		}
		internal static void AddPriceVariables(Fragment fragment, IProduct product, ILekmerFormatter formatter, IChannel channel)
		{
			fragment.AddVariable("Product.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, product.Price.PriceIncludingVat));
			fragment.AddVariable("Product.Price_Formatted.Json", product.Price.PriceIncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);

			bool flag = IsPriceAffectedByCampaign(product);
			if (flag)
			{
				fragment.AddVariable("Product.CampaignInfo.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, product.CampaignInfo.Price.IncludingVat));
				fragment.AddVariable("Product.CampaignInfo.Price_Formatted.Json", product.CampaignInfo.Price.IncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);
			}
		}
		private static bool IsPriceAffectedByCampaign(IProduct product)
		{
			if (product.CampaignInfo != null)
			{
				return product.CampaignInfo.Price.IncludingVat != product.Price.PriceIncludingVat;
			}

			return false;
		}

		internal static void AddUrlVariables(Fragment fragment, string lekmerProductUrl)
		{
			// Absolute
			fragment.AddVariable("Product.Url", UrlHelper.ResolveUrlHttp("~/" + lekmerProductUrl));

			// Root relative
			fragment.AddVariable("Product.Url.RootRelative", UrlHelper.ResolveUrlHttp(lekmerProductUrl));

			fragment.AddRegexVariable(
				_productUrlMatchPattern,
				delegate(Match match)
				{
					string commonName = match.Groups[_productUrlMatchCommonNamePosition].Value;

					string productUrl = string.Format(CultureInfo.InvariantCulture, "~/{0}?page-common-name={1}", lekmerProductUrl, commonName);

					return UrlHelper.ResolveUrlHttp(productUrl);
				},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);
		}
	}
}