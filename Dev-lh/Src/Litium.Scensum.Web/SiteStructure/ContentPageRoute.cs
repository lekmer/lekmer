using System;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Lekmer.OldUrl;

namespace Litium.Scensum.Web.SiteStructure
{
	public class ContentPageRoute : Route
	{
		public ContentPageRoute(string url, IRouteHandler routeHandler) : base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null)
			{
				throw new ArgumentNullException("httpContext");
			}

			RouteData data = base.GetRouteData(httpContext);
			if (data == null)
			{
				return null;
			}

			string url = (data.Values["url"] ?? "").ToString();

			var contentNodeService = IoC.Resolve<IContentNodeService>();
			IContentNodeTree contentNodeTree = contentNodeService.GetAllAsTree(UserContext.Current);

			IContentNodeTreeItem item;
			if (url.IsEmpty())
			{
				var siteStructureRegistryService = IoC.Resolve<ISiteStructureRegistryService>();
				ISiteStructureRegistry siteStructureRegistry = siteStructureRegistryService.GetByChannel(UserContext.Current);
				if (siteStructureRegistry == null || !siteStructureRegistry.StartContentNodeId.HasValue)
				{
					return null;
				}

				item = contentNodeTree.FindItemById(siteStructureRegistry.StartContentNodeId.Value);
			}
			else
			{
				item = contentNodeTree.FindItemByUrl(url);
			}

			if (item == null)
			{
				return null;
			}

			var contentPage = item.ContentNode as IContentPage;
			if (contentPage == null)
			{
				return null;
			}

			// Instead of returning 404 error - redirect to parent page.
			if (!contentPage.ShowPage)
			{
				// Redirect from file (static)
				RedirectToNewPage(httpContext, url);

				while (item.Parent != null)
				{
					var itemPageUrl = item.Parent.PageUrl;
					if (itemPageUrl.HasValue())
					{
						httpContext.Response.DisablePageCaching();
						httpContext.Response.PermanentRedirect("~/" + itemPageUrl);
					}

					item = item.Parent;
				}
			}

			httpContext.Items["ContentPage"] = contentPage;
			httpContext.Items["ContentNodeTreeItem"] = item;

			return data;
		}

		private static void RedirectToNewPage(HttpContextBase httpContext, string url)
		{
			if (url.IsNullOrEmpty() || url.Equals("/")) return;

			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.TrimEnd('/');
			}

			if (!url.StartsWith("/", StringComparison.Ordinal))
			{
				url = "/" + url;
			}

			var redirectUrl = OldUrlService.GetRedirectUrl(url);
			if (redirectUrl.IsNullOrEmpty()) return;

			httpContext.Response.DisablePageCaching();
			httpContext.Response.PermanentRedirect(redirectUrl);
		}
	}
}