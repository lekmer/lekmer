﻿using System;

namespace Litium.Lekmer.Core
{
	[Serializable]
	public enum ContentType
	{
		None = 0,
		StaticMedia = 1,
		MediaArchiveWeb = 2,
		MediaArchiveBackOffice = 3,
		MediaArchiveExternal = 4
	}
}