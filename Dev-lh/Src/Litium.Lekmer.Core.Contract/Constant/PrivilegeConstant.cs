﻿using System;

namespace Litium.Lekmer.Core
{
	[Serializable]
	public static class LekmerPrivilegeConstant
	{
		public static string AssortmentBrand
		{
			get { return "Assortment.Brand"; }
		}

		public static string AssortmentReview
		{
			get { return "Assortment.Review"; }
		}

		public static string AssortmentRating
		{
			get { return "Assortment.Rating"; }
		}

		public static string AssortmentPackage
		{
			get { return "Assortment.Package"; }
		}

		public static string CampaignAds
		{
			get { return "Campaign.Ads"; }
		}

		public static string ToolsPayments
		{
			get { return "Tools.Payments"; }
		}
	}
}