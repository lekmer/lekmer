﻿using System;

namespace Litium.Lekmer.Core
{
	[Serializable]
	public enum UrlType
	{
		None = 0,
		Absolute = 1,
		Relative = 2
	}
}