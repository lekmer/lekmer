﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ILekmerCurrencySecureService : ICurrencySecureService
	{
		Collection<ICurrency> GetAll(string currencyIdsToIgnore);
	}
}
