namespace Litium.Lekmer.Core
{
	public interface IFolder
	{
		int Id { get; set; }
		int? ParentFolderId { get; set; }
		string Title { get; set; }
	}
}