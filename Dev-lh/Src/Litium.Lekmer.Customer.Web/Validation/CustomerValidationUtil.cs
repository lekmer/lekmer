﻿using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Web
{
	public static class CustomerValidationUtil
	{
		public static ValidationResult ValidateUserName(string userName, string formType)
		{
			var validationResult = new ValidationResult();

			if (userName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.UserNameNotProvided", formType)));
			}
			else if (userName.Length > 320)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.UserNameTooLong", formType)));
			}
			else if (!ValidationUtil.IsValidEmail(userName))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.UserNameIncorrect", formType)));
			}

			return validationResult;
		}

		public static ValidationResult ValidatePassword(string password, string passwordVerify, string formType)
		{
			var validationResult = new ValidationResult();

			if (password.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.PasswordNotProvided", formType)));
			}
			else if (password.Length > 50)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.PasswordTooLong", formType)));
			}
			else if (!password.Equals(passwordVerify))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.PasswordVerfifyDontMatch", formType)));
			}

			return validationResult;
		}
	}
}
