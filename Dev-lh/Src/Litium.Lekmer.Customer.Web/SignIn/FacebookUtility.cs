﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.Customer
{
	public class FacebookUtility
	{
		private readonly string _dataKey = "data";
		private readonly string _isSilhouette = "is_silhouette";
		private readonly string _fbUserPictureMatchPattern = @"\[User.Picture\(""(.*?),(.*?)""\)\]";
		private readonly int _fbUserPictureMatchWidthPosition = 1;
		private readonly int _fbUserPictureMatchHeightPosition = 2;

		private readonly IMyAccountSetting _setting;
		private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public FacebookUtility(IMyAccountSetting setting)
		{
			_setting = setting;
		}

		public ValidationResult ValidateCookies(out string userProfile)
		{
			string appId = _setting.FacebookAppId;
			string appSecret = _setting.FacebookAppSecret;

			userProfile = string.Empty;
			var validationResult = new ValidationResult();

			var fbCookies = GetCookie(appId);
			if (fbCookies == null)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.NoFbCookies"));
				return validationResult;
			}

			var fbCookieValue = fbCookies.Value;
			if (string.IsNullOrEmpty(fbCookieValue))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.EmptyFbCookies"));
				return validationResult;
			}

			var cookieData = GetCookieData(fbCookieValue, appSecret);
			if (cookieData == null)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.FbCookiesIncorrect"));
				return validationResult;
			}

			if (!IsCookieNew(cookieData["issued_at"]))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.FbCookiesIncorrect"));
				return validationResult;
			}

			var token = GetToken(cookieData["code"], appId, appSecret);
			if (string.IsNullOrEmpty(token))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.FbTokenNotProvided"));
				return validationResult;
			}

			userProfile = GetUserProfile(token);
			if (string.IsNullOrEmpty(userProfile))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.FbUserProfileNotProvided"));
			}
			return validationResult;
		}
		protected virtual HttpCookie GetCookie(string appId)
		{
			return HttpContext.Current.Request.Cookies[string.Format(_setting.FacebookCookieMask, appId)];
		}
		protected virtual Dictionary<string, string> GetCookieData(string value, string appSecret)
		{
			int separator = value.IndexOf(".");
			if (separator == -1) return null;

			string encodedSig = value.Substring(0, separator);
			string payload = value.Substring(separator + 1);

			string sig = Base64Decode(encodedSig);

			var crypt = new HMACSHA256(Encoding.ASCII.GetBytes(appSecret));
			crypt.ComputeHash(Encoding.UTF8.GetBytes(payload));
			string expectedSig = Encoding.UTF8.GetString(crypt.Hash);
			if (sig != expectedSig) return null;

			var serializer = new JavaScriptSerializer();
			var data = serializer.Deserialize<Dictionary<string, string>>(Base64Decode(payload));
			if (data["algorithm"].ToUpper() != _setting.FacebookAlgoritm) return null;

			return data;
		}
		protected virtual bool IsCookieNew(string issuedAt)
		{
			var dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
			dateTime = dateTime.AddSeconds(double.Parse(issuedAt)).ToLocalTime();
			return dateTime >= DateTime.Now.AddMinutes(-10).ToLocalTime();
		}
		protected virtual string GetToken(string code, string appId, string appSecret)
		{
			string accessTokenResponse = GetResponse(string.Format(_setting.FacebookTokenRequestUrl, appId, appSecret, code));
			NameValueCollection options = HttpUtility.ParseQueryString(accessTokenResponse);
			return options["access_token"];
		}
		protected virtual string GetUserProfile(string token)
		{
			string userResponse = GetResponse(string.Format(_setting.FacebookUserProfileRequestUrl, token));
			userResponse = Regex.Replace(userResponse, @"\\u([\dA-Fa-f]{4})", v => ((char)Convert.ToInt32(v.Groups[1].Value, 16)).ToString(CultureInfo.InvariantCulture));
			return userResponse;
		}

		public ValidationResult GetUserParameters(string userProfile, out string id, out string email, out string name, out string firstName, out string lastName, out string gender)
		{
			var validationResult = new ValidationResult();

			var serializer = new JavaScriptSerializer();
			var json = serializer.Deserialize<Dictionary<string, object>>(userProfile);
			id = GetValue(json, "id");
			email = GetValue(json, "email");
			name = GetValue(json, "name");
			firstName = GetValue(json, "first_name");
			lastName = GetValue(json, "last_name");
			gender = GetValue(json, "gender");

			if (string.IsNullOrEmpty(id))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.FbUserIdNotProvided"));
			}
			if (string.IsNullOrEmpty(name))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.FacebookSignIn.Validation.FbUserNameNotProvided"));
			}
			return validationResult;
		}
		protected virtual string GetValue(Dictionary<string, object> json, string key)
		{
			return json.ContainsKey(key) && json[key] != null ? json[key].ToString() : null;
		}

		public int GetGenderTypeId(string gender)
		{
			int genderTypeId;
			genderTypeId = gender == FacebookGenderType.Male.ToString().ToLower()
				? (int)GenderTypeInfo.Man
				: (gender == FacebookGenderType.Female.ToString().ToLower() ? (int)GenderTypeInfo.Woman : (int)GenderTypeInfo.Unknown);
			return genderTypeId;
		}

		public void AddUserPictureParameters(Fragment fragment, IFacebookUser facebookUser)
		{
			bool hasPicture = false;

			if (facebookUser != null)
			{
				var isPictureDefault = IsPictureDefault(facebookUser.FacebookId);
				if (!isPictureDefault)
				{
					hasPicture = true;

					fragment.AddVariable("User.Picture", string.Format(_setting.FacebookPictureRequestUrl, facebookUser.FacebookId));
					fragment.AddRegexVariable(
						_fbUserPictureMatchPattern,
						delegate(Match match)
						{
							string width = match.Groups[_fbUserPictureMatchWidthPosition].Value;
							string height = match.Groups[_fbUserPictureMatchHeightPosition].Value;
							string pictureUrl = string.Format(_setting.FacebookPictureRequestUrl + "?width={1}&height={2}", facebookUser.FacebookId, width, height);
							return pictureUrl;
						},
						VariableEncoding.HtmlEncodeLight,
						RegexOptions.Compiled);
				}
			}

			fragment.AddCondition("User.HasPicture", hasPicture);
		}
		protected virtual bool IsPictureDefault(string id)
		{
			try
			{
				string pictureData = GetResponse(string.Format(_setting.FacebookPictureDataRequestUrl, id));

				var serializer = new JavaScriptSerializer();
				var json = serializer.Deserialize<Dictionary<string, object>>(pictureData);
				if (!json.ContainsKey(_dataKey)) return true;

				var data = (Dictionary<string, object>)json[_dataKey];
				if (!data.ContainsKey(_isSilhouette)) return true;

				var isSilhouette = data[_isSilhouette];
				if (isSilhouette == null) return true;

				bool isPictureDefault;
				if (bool.TryParse(isSilhouette.ToString(), out isPictureDefault))
				{
					return isPictureDefault;
				}

				return true;
			}
			catch (Exception ex)
			{
				_log.Error(string.Format("Error while getting facebook picture for ID: {0}", id), ex);
				return true;
			}
		}

		private string Base64Decode(string input)
		{
			var encoding = new UTF8Encoding();
			string encoded = input.Replace("=", string.Empty).Replace('-', '+').Replace('_', '/');
			var decoded = Convert.FromBase64String(encoded.PadRight(encoded.Length + (4 - encoded.Length % 4) % 4, '='));
			var result = encoding.GetString(decoded);
			return result;
		}
		private string GetResponse(string url)
		{
			try
			{
				string result;
				WebRequest request = (HttpWebRequest)WebRequest.Create(url);
				WebResponse response = request.GetResponse();
				Encoding encode = Encoding.GetEncoding("utf-8");
				using (var sr = new StreamReader(response.GetResponseStream(), encode))
				{
					result = sr.ReadToEnd();
					sr.Close();
				}
				return result;
			}
			catch (Exception ex)
			{
				_log.Error(string.Format("Error with request URL: {0}", url), ex);
				return string.Empty;
			}
		}
	}

	public enum FacebookGenderType
	{
		Male,
		Female
	}
}
