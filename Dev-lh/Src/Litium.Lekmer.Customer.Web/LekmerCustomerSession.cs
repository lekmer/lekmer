﻿using System.Globalization;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Esales;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerCustomerSession : CustomerSession, ILekmerCustomerSession
	{
		protected IEsalesNotifyService EsalesNotifyService { get; private set; }

		public LekmerCustomerSession(IUserContextFactory userContextFactory, IEsalesNotifyService esalesNotifyService)
			: base(userContextFactory)
		{
			EsalesNotifyService = esalesNotifyService;
		}

		public virtual void SignIn(ICustomer customer)
		{
			var lekmerCustomer = (ILekmerCustomer) SignedInCustomer;
			SignIn(customer, lekmerCustomer != null ? lekmerCustomer.LoginType : CustomerLoginType.None);
		}
		public virtual void SignIn(ICustomer customer, CustomerLoginType loginType)
		{
			var lekmerCustomer = (ILekmerCustomer) customer;
			lekmerCustomer.LoginType = loginType;
			SignedInCustomer = lekmerCustomer;
		}

		protected override void OnCustomerUpdated()
		{
			base.OnCustomerUpdated();

			ICustomer signedInCustomer = SignedInCustomer;
			if (signedInCustomer != null)
			{
				EsalesNotifyService.NotifyCustomerKey("customer_key_" + signedInCustomer.Id.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				EsalesNotifyService.NotifySessionEnd();
			}
		}
	}
}