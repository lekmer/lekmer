using System;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Edit customer information control.
	/// </summary>
	public class BlockCustomerDetailControl : BlockControlBase
	{
		private readonly ILekmerUserService _userService;
		private readonly IFacebookUserService _facebookUserService;
		private readonly ILekmerCustomerService _customerService;
		private readonly ILekmerCustomerSession _customerSession;
		private readonly IMyAccountSetting _setting;

		private CustomerDetailForm _form;
		private CustomerDetailForm Form
		{
			get
			{
				return _form ?? (_form = new CustomerDetailForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		private FacebookUtility _facebookUtility;
		protected FacebookUtility FacebookUtility
		{
			get
			{
				return _facebookUtility ?? (_facebookUtility = new FacebookUtility(_setting));
			}
		}

		private bool WasUsernameChangedSuccessfully { get; set; }
		private bool WasPasswordChangedSuccessfully { get; set; }
		private bool WasCustomerInformationUpdatedSuccessfully { get; set; }

		public BlockCustomerDetailControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IUserService userService,
			IFacebookUserService facebookUserService,
			ICustomerService customerService,
			ICustomerSession customerSession,
			IMyAccountSetting setting)
			: base(templateFactory, blockService)
		{
			_userService = (ILekmerUserService) userService;
			_facebookUserService = facebookUserService;
			_customerService = (ILekmerCustomerService) customerService;
			_customerSession = (ILekmerCustomerSession) customerSession;
			_setting = setting;
		}

		protected override BlockContent RenderCore()
		{
			if (!_customerSession.IsSignedIn)
			{
				return new BlockContent();
			}

			var signedInCustomer = (ILekmerCustomer) _customerSession.SignedInCustomer;
			var customerId = signedInCustomer.Id;

			var validationResult = new ValidationResult();
			if (Form.IsFormPostBack)
			{
				Form.MapFromRequest();

				if (Form.IsChangeUsernameActionPost)
				{
					validationResult = Form.ValidateUserName(customerId, Form.UserName, "CustomerDetail");
					if (validationResult.IsValid)
					{
						UpdateCustomer(customerId);
						WasUsernameChangedSuccessfully = true;
					}
				}
				else if (Form.IsChangePasswordActionPost)
				{
					validationResult = Form.ValidatePassword(signedInCustomer);
					if (validationResult.IsValid)
					{
						UpdateCustomer(customerId);
						WasPasswordChangedSuccessfully = true;
					}
				}
				else if (Form.IsAddAlternateLoginActionPost)
				{
					validationResult = Form.ValidateAlternateLogin(signedInCustomer, Form.AlternateUserName, "AlternateLogin");
					if (validationResult.IsValid)
					{
						AlternateLogin(signedInCustomer, customerId);
					}
				}
				else if (Form.IsAddAlternateFacebookLoginActionPost)
				{
					FacebookAlternateLogin(signedInCustomer, out validationResult);
				}
				else if (Form.IsDeleteAlternateLoginActionPost)
				{
					DeleteAlternateLogin(signedInCustomer);
				}
				else if (Form.IsUpdateCustomerInformationActionPost)
				{
					validationResult = Form.ValidateCustomerInformation();
					if (validationResult.IsValid)
					{
						UpdateCustomer(customerId);
						WasCustomerInformationUpdatedSuccessfully = true;
					}
				}
			}

			Form.MapToForm(_customerSession.SignedInCustomer);
			return RenderCustomerDetailForm(validationResult);
		}

		private static void SendConfirmation(string userName, string name, DateTime createdDate, bool isAlternatelogin, bool isLekmerAccount)
		{
			var messageArgs = new RegisterMessageArgs(userName, name, createdDate, isAlternatelogin, isLekmerAccount);
			new RegisterMessenger().Send(messageArgs);
		}

		protected virtual BlockContent RenderCustomerDetailForm(ValidationResult validationResult)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			Form.MapFieldNamesToFragment(fragmentContent);
			Form.MapFieldValuesToFragment(fragmentContent, true);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			fragmentContent.AddVariable("AppId", _setting.FacebookAppId, VariableEncoding.None);
			RenderUpdateSucceedNotification(fragmentContent, validationResult);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}
		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			Form.MapFieldNamesToFragment(fragment);
			Form.MapFieldValuesToFragment(fragment, false);
			fragment.AddVariable("AppId", _setting.FacebookAppId, VariableEncoding.None);
			return fragment.Render();
		}
		protected virtual void RenderUpdateSucceedNotification(Fragment fragmentContent, ValidationResult validationResult)
		{
			fragmentContent.AddCondition("WasUsernameChangedSuccessfully", WasUsernameChangedSuccessfully);
			fragmentContent.AddVariable("UsernameChangedSuccessfully", WasUsernameChangedSuccessfully ? AliasHelper.GetAliasValue("Customer.CustomerDetail.UsernameChangedSuccessfully") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasPasswordChangedSuccessfully", WasPasswordChangedSuccessfully);
			fragmentContent.AddVariable("PasswordChangedSuccessfully", WasPasswordChangedSuccessfully ? AliasHelper.GetAliasValue("Customer.CustomerDetail.PasswordChangedSuccessfully") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasCustomerInformationUpdatedSuccessfully", WasCustomerInformationUpdatedSuccessfully);
			fragmentContent.AddVariable("CustomerInformationUpdatedSuccessfully", WasCustomerInformationUpdatedSuccessfully ? AliasHelper.GetAliasValue("Customer.CustomerDetail.CustomerInformationUpdatedSuccessfully") : string.Empty, VariableEncoding.None);
		}

		protected virtual void AlternateLogin(ILekmerCustomer customer, int customerId)
		{
			customer.User = _userService.Create(UserContext.Current);
			Form.MapToUser((ILekmerUser)customer.User, customerId);
			_userService.Save(UserContext.Current, customer.User);
			SendConfirmation(customer.User.UserName, null, customer.User.CreatedDate, true, true);
		}
		protected virtual void FacebookAlternateLogin(ILekmerCustomer customer, out ValidationResult validationResult)
		{
			string userProfile;
			validationResult = FacebookUtility.ValidateCookies(out userProfile);
			if (!validationResult.IsValid)
			{
				return;
			}

			string id;
			string email;
			string name;
			string firstName;
			string lastName;
			string gender;
			validationResult = FacebookUtility.GetUserParameters(userProfile, out id, out email, out name, out firstName, out lastName, out gender);
			if (!validationResult.IsValid)
			{
				return;
			}

			var facebookCustomer = (ILekmerCustomer)_customerService.GetByFacebookId(UserContext.Current, id);
			if (facebookCustomer != null && facebookCustomer.FacebookUser != null)
			{
				// Already exists
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.AlternateLogin.Validation.FacebookUserAlreadyExists"));
			}
			else
			{
				customer.FacebookUser = _facebookUserService.Create();
				customer.FacebookUser.FacebookId = id;
				customer.FacebookUser.Name = name;
				customer.FacebookUser.Email = email;
				customer.FacebookUser.CustomerId = customer.Id;
				customer.FacebookUser.CustomerRegistryId = customer.CustomerRegistryId;
				customer.FacebookUser.IsAlternateLogin = true;
				customer.CustomerStatusId = (int)CustomerStatusInfo.Online;
				_facebookUserService.Save(customer.FacebookUser);

				var fbUser = customer.FacebookUser;
				SendConfirmation(fbUser.Email, fbUser.Name, fbUser.CreatedDate, true, false);
			}
		}
		protected virtual void DeleteAlternateLogin(ILekmerCustomer customer)
		{
			switch (customer.LoginType)
			{
				case CustomerLoginType.FacebookAccount:
					var user = (ILekmerUser)customer.User;
					if (user != null && user.IsAlternateLogin)
					{
						_userService.DeleteAlternateAccount(user);
						customer.User = null;
					}
					break;
				case CustomerLoginType.LekmerAccount:
					var fbUser = customer.FacebookUser;
					if (fbUser != null && fbUser.IsAlternateLogin)
					{
						_facebookUserService.DeleteAlternateAccount(fbUser);
						customer.FacebookUser = null;
					}
					break;
			}
		}
		protected virtual void UpdateCustomer(int customerId)
		{
			var customer = _customerService.GetById(UserContext.Current, customerId);
			Form.MapToCustomer(customer);
			_customerService.Save(UserContext.Current, customer);
			_customerSession.SignIn(customer);
		}
	}
}