﻿using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class CheckoutSignUpForm : ControlBase
	{
		private readonly string _signInPostUrl;

		public virtual string PostUrl
		{
			get { return _signInPostUrl; }
		}
		public virtual string PostModeValue
		{
			get { return "signin"; }
		}
		public virtual string CheckoutSignUpActionName
		{
			get { return "checkout-signup-action"; }
		}
		public virtual string FacebookCheckoutSignUpActionName
		{
			get { return "facebook-checkout-signup-action"; }
		}
		public virtual string UserName { get; set; }
		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}
		public virtual bool CheckoutSignUpActionPost
		{
			get { return Request.Form[CheckoutSignUpActionName] != null; }
		}
		public virtual bool FacebookCheckoutSignUpActionPost
		{
			get { return Request.Form[FacebookCheckoutSignUpActionName] != null; }
		}


		public CheckoutSignUpForm(string signInPostUrl)
		{
			_signInPostUrl = signInPostUrl;
		}


		/// <summary>
		/// Adds field name variables to a certain fragment.
		/// </summary>
		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.CheckoutSignUp.Action.Name", CheckoutSignUpActionName);
			fragment.AddVariable("Form.FacebookCheckoutSignUp.Action.Name", FacebookCheckoutSignUpActionName);
		}

		/// <summary>
		/// Adds field value variables to a certain fragment.
		/// </summary>
		public virtual void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.UserName.Value", UserName);
		}

		/// <summary>
		/// Map fields from order.
		/// </summary>
		public virtual void MapFromOrder(IOrderFull order)
		{
			UserName = order.Email;
		}

		/// <summary>
		/// Validates the user name.
		/// </summary>
		public virtual ValidationResult Validate()
		{
			return CustomerValidationUtil.ValidateUserName(UserName, "CheckoutRegister");
		}
	}
}
