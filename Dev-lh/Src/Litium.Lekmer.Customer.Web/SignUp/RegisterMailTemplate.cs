﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class RegisterMailTemplate : Template
	{
		public RegisterMailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public RegisterMailTemplate(IChannel channel)
			: base("RegisterCustomerConfirmation", channel, false)
		{
		}
	}
}
