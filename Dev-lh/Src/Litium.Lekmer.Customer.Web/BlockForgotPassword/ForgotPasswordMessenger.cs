﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class ForgotPasswordMessenger : ScensumMessengerBase<ICustomer>
	{
		protected ForgotPasswordMailTemplate Template { get; set; }

		protected override Message CreateMessage(ICustomer messageArgs)
		{
			if (messageArgs == null || messageArgs.User == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			string email = messageArgs.User.UserName;
			if (email.IsNullOrTrimmedEmpty())
			{
				throw new ArgumentException("Empty Email/Username");
			}

			Template = new ForgotPasswordMailTemplate(UserContext.Current.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Address = email,
				Name = email
			});
			return message;
		}

		private string RenderMessage(ICustomer customer)
		{
			Fragment htmlFragment = Template.GetFragment("Message");
			htmlFragment.AddVariable("Customer.Username", customer.User.UserName);
			htmlFragment.AddVariable("Customer.CreatedDate", customer.User.CreatedDate.ToString("yyyy-MM-dd HH:mm"));
			htmlFragment.AddVariable("Customer.Channel", UserContext.Current.Channel.CommonName);
			htmlFragment.AddVariable("NewPassword.Url", GetNewPasswordPageUrl(customer));
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private static string GetNewPasswordPageUrl(ICustomer customer)
		{
			var guid = CreateCustomerForgotPassword(customer);
			var pageUrl = UrlHelper.ResolveUrlHttp(CustomerUrlHelper.GetPageUrl("NewPassword"));
			return string.Format(CultureInfo.InvariantCulture, "{0}?id={1}", pageUrl, guid);
		}

		private static Guid CreateCustomerForgotPassword(ICustomer customer)
		{
			var service = IoC.Resolve<ICustomerForgotPasswordService>();
			var setting = IoC.Resolve<IMyAccountSetting>();
			var customerForgotPassword = service.Create(customer.Id, setting.NewPasswordLinkLifeCycleValue);
			service.Save(customerForgotPassword);
			return customerForgotPassword.Guid;
		}

		protected override string MessengerName
		{
			get { return "ForgotPassword"; }
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}
