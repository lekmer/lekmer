﻿using System;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class BlockForgotPasswordControl : BlockControlBase
	{
		private ForgotPasswordForm _forgotPasswordForm;
		private ForgotPasswordForm ForgotPasswordForm
		{
			get { return _forgotPasswordForm ?? (_forgotPasswordForm = new ForgotPasswordForm(ResolveUrl(ContentNodeTreeItem.Url))); }
		}

		private bool WasMailSent { get; set; }


		public BlockForgotPasswordControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			if (ForgotPasswordForm.IsFormPostBack)
			{
				ForgotPasswordForm.MapFromRequest();
				ICustomer customer;
				validationResult = ForgotPasswordForm.Validate(out customer);
				if (validationResult.IsValid && customer != null)
				{
					SendConfirmation(customer);
				}
			}

			return RenderForgotPasswordForm(validationResult);
		}

		protected virtual BlockContent RenderForgotPasswordForm(ValidationResult validationResult)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			ForgotPasswordForm.MapFieldsToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", RenderValidationErrors(validationResult), VariableEncoding.None);
			fragmentContent.AddVariable("SucceedMessage", WasMailSent ? AliasHelper.GetAliasValue("Customer.ForgotPassword.SucceedMessage") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasMailSent", WasMailSent);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			ForgotPasswordForm.MapFieldsToFragment(fragment);
			return fragment.Render();
		}

		private string RenderValidationErrors(ValidationResult validationResult)
		{
			if (validationResult == null || validationResult.IsValid)
				return null;

			var validationControl = new ValidationControl(validationResult.Errors);
			return validationControl.Render();
		}

		private void SendConfirmation(ICustomer customer)
		{
			var id = new ForgotPasswordMessenger().Send(customer);
			WasMailSent = !id.Equals(Guid.Empty);
		}
	}
}