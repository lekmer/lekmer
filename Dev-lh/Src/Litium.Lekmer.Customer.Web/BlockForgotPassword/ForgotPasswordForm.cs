﻿using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class ForgotPasswordForm : ControlBase
	{
		private readonly string _forgotPasswordPostUrl;

		public string PostUrl
		{
			get { return _forgotPasswordPostUrl; }
		}
		public string PostModeValue
		{
			get { return "forgotpassword"; }
		}
		public virtual string UserNameFormName
		{
			get { return "forgotpassword-username"; }
		}
		public string UserName { get; set; }
		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}


		public ForgotPasswordForm(string forgotPasswordPostUrl)
		{
			_forgotPasswordPostUrl = forgotPasswordPostUrl;
		}


		public virtual void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.UserName.Name", UserNameFormName);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.UserName.Value", UserName);
			fragment.AddVariable("Form.PostUrl", PostUrl);
		}

		public virtual void MapFromRequest()
		{
			UserName = Request.Form[UserNameFormName];
		}

		public virtual ValidationResult Validate(out ICustomer customer)
		{
			customer = null;
			var validationResult = new ValidationResult();

			if (UserName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.ForgotPassword.Validation.UserNameNotProvided"));
			}
			else
			{
				var customerService = IoC.Resolve<ICustomerService>();
				customer = customerService.GetByUserName(UserContext.Current, UserName);
				if (customer == null || customer.User == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.ForgotPassword.Validation.CustomerNotFound"));
				}
			}

			return validationResult;
		}
	}
}
