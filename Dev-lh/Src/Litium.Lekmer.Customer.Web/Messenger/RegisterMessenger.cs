﻿using System;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class RegisterMessenger : MessengerBase<RegisterMessageArgs>
	{
		protected RegisterMailTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "Register"; }
		}

		protected override Message CreateMessage(RegisterMessageArgs messageArgs)
		{
			if (messageArgs == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			string email = messageArgs.UserName;
			if (email.IsNullOrTrimmedEmpty())
			{
				throw new ArgumentException("Empty Email/Username");
			}

			Template = new RegisterMailTemplate(UserContext.Current.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Address = email,
				Name = email
			});
			return message;
		}

		private string RenderMessage(RegisterMessageArgs messageArgs)
		{
			var isFacebookUser = !messageArgs.IsLekmerAccount;

			Fragment htmlFragment = Template.GetFragment("Content");
			htmlFragment.AddVariable("Customer.Username", messageArgs.UserName);
			htmlFragment.AddVariable("Customer.CreatedDate", messageArgs.CreatedDate.ToString("yyyy-MM-dd HH:mm"));
			htmlFragment.AddCondition("Customer.IsAlternateLogin", messageArgs.IsAlternateLogin);
			htmlFragment.AddCondition("Customer.IsFacebookUser", isFacebookUser);
			if (isFacebookUser)
			{
				htmlFragment.AddVariable("Customer.FacebookUsername", messageArgs.Name);
			}

			htmlFragment.AddVariable("Customer.Channel", UserContext.Current.Channel.CommonName);
			htmlFragment.AddVariable("Customer.SignIn.Url", UrlHelper.ResolveUrlHttp(CustomerUrlHelper.GetPageUrl("SignIn")));
			htmlFragment.AddVariable("Customer.ForgotPassword.Url", UrlHelper.ResolveUrlHttp(CustomerUrlHelper.GetPageUrl("ForgotPassword")));
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}
