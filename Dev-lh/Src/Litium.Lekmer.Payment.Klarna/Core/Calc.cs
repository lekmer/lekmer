﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Klarna.Core
{
    public class Calc
    {

        /// <summary>
        /// This constant tells the IRR function when to stop.
        /// If the calculation error is lower than this the calculation is done.
        /// </summary>
        protected static double accuracy = 0.01;
        private StackTrace strace = new StackTrace();

        /// <summary>
        /// Calculates the MidPoint between two points. Used by divide and conquer.
        /// </summary>
        /// <param name="a">Point one</param>
        /// <param name="b">Point two</param>
        /// <returns>double MidPoint</returns>
        private double MidPoint(double a, double b)
        {
            return ((a + b) / 2);
        }

        /// <summary>
        /// Calculates the difference between the initial loan to the customer
        /// and the individual payments adjusted for the inverse of the interest
        /// rate. The variable we are searching for is rate and if pval,
        /// payarray and rate is perfectly balanced this function returns 0.0.
        /// </summary>
        /// <param name="pval">Initial loan to customer (in any currency)</param>
        /// <param name="payarray">Array of monthly payments from the customer</param>
        /// <param name="rate">Interest rate per year in %</param>
        /// <param name="fromdayone">Do we count interest from the first day yes(1)/no(0).</param>
        /// <returns>Net Present Value</returns>
        private double NPV(double pval, List<double> payarray, double rate, int fromdayone)
        {
            int month = fromdayone;
            foreach (double payment in payarray)
            {
                pval -= payment / Math.Pow(1 + rate / (12 * 100.0), month++);
            }

            return (pval);
        }

        /// <summary>
        /// This function uses divide and conquer to numerically find the IRR,
        /// Internal Rate of Return. It starts of by trying a low of 0% and a
        /// high of 100%. If this isn"t enough it will double the interval up
        /// to 1000000%. Note that this is insanely high, and if you try to convert
        /// an IRR that high to an APR you will get even more insane values,
        /// so feed this function good data.
        ///
        /// Return values: double IRR  if it was possible to find a rate that gets
        ///                          NPV closer to 0 than accuracy.
        ///               int -1     The sum of the payarray is less than the lent
        ///                          amount, pval. Hellooooooo. Impossible.
        ///               int -2     the IRR is way to high, giving up.
        ///
        /// This algorithm works in logarithmic time no matter what inputs you give
        /// and it will come to a good answer within ~30 steps.
        /// </summary>
        /// <param name="pval">Initial loan to customer (in any currency)</param>
        /// <param name="payarray">Array of monthly payments from the customer</param>
        /// <param name="fromdayone">Do we count interest from the first day yes(1)/no(0).</param>
        /// <returns>Internal Rate of Return, expressed yearly, in %</returns>
        private double IRR(double pval, List<double> payarray, int fromdayone)
        {
            double low = 0.0;
            double high = 100.0;
            double mid = 0.0;
            double midval = 0.0;
            double lowval = NPV(pval, payarray, low, fromdayone);
            double highval = NPV(pval, payarray, high, fromdayone);

            // The sum of payarray is smaller than pval, impossible!
            if (lowval > 0.0)
            {
                return (-1);
            }

            // Standard divide and conquer.
            do
            {
                mid = MidPoint(low, high);
                midval = NPV(pval, payarray, mid, fromdayone);
                if (Math.Abs(midval) < accuracy)
                {
                    //we are close enough
                    return (mid);
                }

                if (highval < 0.0)
                {
                    // we are not in range, so double it
                    low = high;
                    lowval = highval;
                    high *= 2;
                    highval = NPV(pval, payarray, high, fromdayone);
                }
                else if (midval >= 0.0)
                {
                    // IRR is between low and mid
                    high = mid;
                    highval = midval;
                }
                else
                {
                    // IRR is between mid and high
                    low = mid;
                    lowval = midval;
                }
            } while (high < 1000000);
            // bad input, insanely high interest. APR will be INSANER!
            return (-2);
        }

        /// <summary>
        /// IRR is not the same thing as APR, Annual Percentage Rate. The
        /// IRR is per time period, i.e. 1 month, and the APR is per year,
        /// and note that that you need to raise to the power of 12, not
        /// mutliply by 12.
        ///
        /// This function turns an IRR into an APR.
        ///
        /// If you feed it a value of 100%, yes the APR will be millions!
        /// If you feed it a value of   9%, it will be 9.3806%.
        /// That is the nature of this math and you can check the wiki
        /// page for APR for more info.
        /// </summary>
        /// <param name="irr">Internal Rate of Return, expressed yearly, in %</param>
        /// <returns>Annual Percentage Rate, in %</returns>
        private double IRR2APR(double irr)
        {
            return (100 * (Math.Pow(1 + irr / (12 * 100.0), 12) - 1));
        }

        /// <summary>
        /// This is a simplified model of how our paccengine works if
        /// a client always pays their bills. It adds interest and fees
        /// and checks minimum payments. It will run until the value
        /// of the account reaches 0, and return an array of all the
        /// individual payments. Months is the amount of months to run
        /// the simulation. Important! Don"t feed it too few months or
        /// the whole loan won"t be paid off, but the other functions
        /// should handle this correctly.
        ///
        /// Giving it too many months has no bad effects, or negative
        /// amount of months which means run forever, but it will stop
        /// as soon as the account is paid in full.
        ///
        /// Depending if the account is a base account or not, the
        /// payment has to be 1/24 of the capital amount.
        ///
        /// The payment has to be at least minpay, unless the capital
        /// amount + interest + fee is less than minpay; in that case
        /// that amount is paid and the function returns since the client
        /// no longer owes any money.
        /// </summary>
        /// <param name="pval">Initial loan to customer (in any currency)</param>
        /// <param name="rate">Interest rate per year in %</param>
        /// <param name="fee">Monthly invoice fee</param>
        /// <param name="minpay">Minimum monthly payment allowed for this country.</param>
        /// <param name="payment">Payment the client to pay each month</param>
        /// <param name="months">Amount of months to run (-1 => infinity)</param>
        /// <param name="baseAccount">Is it a base account?</param>
        /// <returns>An array of monthly payments for the customer.</returns>
        private List<double> fulpacc(double pval, double rate, double fee, double minpay, double payment, int months, bool baseAccount)
        {
            double bal = pval;
            List<double> payarray = new List<double>();
            double interest = 0.0;
            double newbal = 0.0;
            double newpay = 0.0;

            while ((months != 0) && (bal > accuracy))
            {
                interest = bal * rate / (100.0 * 12);
                newbal = bal + interest + fee;

                if (minpay >= newbal || payment >= newbal)
                {
                    payarray.Add(newbal);
                    return payarray;
                }

                newpay = Math.Max(payment, minpay);
                if (baseAccount)
                {
                    newpay = Math.Max(newpay, bal / 24.0 + fee + interest);
                }

                bal = newbal - newpay;
                payarray.Add(newpay);
                months -= 1;
            }

            return payarray;
        }
        /// <summary>
        /// Calculates how much you have to pay each month if you want to
        /// pay exactly the same amount each month. The interesting input
        /// is the amount of months.
        ///
        /// It does not include the fee so add that later.
        /// </summary>
        /// <param name="pval">Principal value - Initial loan to customer (in any currency)</param>
        /// <param name="months">months to pay of in</param>
        /// <param name="rate">interest rate in % as before</param>
        /// <returns>Monthly payment</returns>
        private double Annuity(double pval, int months, double rate)
        {
            double p = 0.0;
            if (months == 0)
            {
                return pval;
            }

            if (rate == 0)
            {
                return pval / months;
            }

            p = rate / (100.0 * 12);
            return pval * p / (1 - Math.Pow((1 + p), -months));
        }

        /// <summary>
        /// How many months does it take to pay off a loan if I pay
        /// exactly monthly each month? It might actually go faster
        /// if you hit the minimum payments, but this function returns
        /// the longest amount of months.
        ///
        /// This function _does_ not include the fee, so remove the fee
        /// from the monthly before sending it into this function.
        /// Return values: double months
        ///                int   -1      you are not paying more than
        ///                              the interest. infinity
        ///                int   -2      fromdayone has to be 0 or 1
        /// fromdayone should be 0 for pay_in_X_months since the interest
        /// won't be applied on the first invoice. In all other cases use 1.
        /// </summary>
        /// <param name="pval">Principal value</param>
        /// <param name="monthly">Payment/month (-fee)</param>
        /// <param name="rate">Interest rate in %</param>
        /// <param name="fromdayone">Do we count interest from day one? [0, 1]</param>
        /// <returns>Months it takes (round it up)</returns>
        private double FixedPrice(double pval, double monthly, double rate, int fromdayone)
        {
            double p = rate / (100.0 * 12);
            double f = 1 + p;
            if (fromdayone == 0)
            {
                if (f < (pval * p / monthly))
                {
                    return -1;
                }
                // this might be wrong. check it.
                // it seems to give the right output.
                // If f is 1 this will give a NaN value ( division by 0 )
                return 1 - Math.Log(f - pval * p / monthly) / Math.Log(f);
            }
            else if (fromdayone == 1)
            {
                if (1.0 < pval * p / monthly)
                {
                    return -1;
                }
                return -Math.Log(1.0 - pval * p / monthly) / Math.Log(f);
            }
            else
            {
                return -2;
            }
        }

        /// <summary>
        /// Calculate the APR for an Annuity given the following inputs.
        ///
        /// If you give it bad inputs, it will return negative values.
        /// </summary>
        /// <param name="pval">Principal value</param>
        /// <param name="months">Months to pay off in</param>
        /// <param name="rate">Interest rate in % as before</param>
        /// <param name="fee">Monthly fee</param>
        /// <param name="minpay">Minimum payment per month</param>
        /// <returns>APR in %</returns>
        private double APR_Annuity(double pval, int months, double rate, double fee, double minpay)
        {
            double payment = Annuity(pval, months, rate) + fee;
            if (payment < 0)
            {
                return payment;
            }
            List<double> payarray = fulpacc(pval, rate, fee, minpay, payment, months, false);
            double apr = IRR2APR(IRR(pval, payarray, 1));

            return apr;
        }

        /// <summary>
        /// Calculate the APR given a fixed payment each month.
        ///
        /// If you give it bad inputs, it will return negative values.
        /// </summary>
        /// <param name="pval">Principal value</param>
        /// <param name="payment">Monthly payment for client</param>
        /// <param name="rate">Interest rate in % as before</param>
        /// <param name="fee">Monthly fee</param>
        /// <param name="minpay">Minimum payment per month</param>
        /// <returns>APR in %</returns>
        private double APR_FixedPrice(double pval, double payment, double rate, double fee, double minpay)
        {
            double months = FixedPrice(pval, payment - fee, rate, 1);
            if (months < 0)
            {
                return months;
            }
            months = Math.Ceiling(months);
            List<double> payarray = fulpacc(pval, rate, fee, minpay, payment, (int)months, false);
            double apr = IRR2APR(IRR(pval, payarray, 1));

            return apr;
        }

        /// <summary>
        /// This tries to pay the absolute minimum each month.
        /// Give the absolute worst APR.
        /// Don't export, only here for reference.
        /// </summary>
        /// <param name="pval">Principal value</param>
        /// <param name="rate">Interest rate in % as before</param>
        /// <param name="fee">Monthly fee</param>
        /// <param name="minpay">Minimum payment per month</param>
        /// <returns>APR in %</returns>
        private double APR_Min(double pval, double rate, double fee, double minpay)
        {
            List<double> payarray = fulpacc(pval, rate, fee, minpay, 0.0, -1, true);
            double apr = IRR2APR(IRR(pval, payarray, 1));

            return apr;
        }

        /// <summary>
        /// Calculates APR for a campaign where you give free months to
        /// the client and there is no interest on the first invoice.
        /// The only new input is free, and if you give "Pay in Jan"
        /// in November, then free = 2.
        ///
        /// The more free months you give, the lower the APR so it does
        /// matter.
        ///
        /// This function basically pads the payarray with zeros in the
        /// beginning (but there is some more magic as well).
        /// </summary>
        /// <param name="pval">Principal value</param>
        /// <param name="payment">Monthly payment for client</param>
        /// <param name="rate">Interest rate in % as before</param>
        /// <param name="fee">Monthly fee</param>
        /// <param name="minpay">Free months</param>
        /// <param name="free">Minimum payment per month</param>
        /// <returns>APR in %</returns>
        private double APR_PayInXMonths(double pval, double payment, double rate, double fee, double minpay, int free)
        {
            double firstpay = payment; //this used to be buggy. use this line.
            double months = FixedPrice(pval, payment - fee, rate, 0);
            if (months < 0)
            {
                return months;
            }

            months = Math.Ceiling(months);
            double newpval = pval - firstpay;
            List<double> farray = new List<double>();
            while (free-- >= 0)
            {
                farray.Add(0.0);
            }
            pval += fee;

            farray.Add(firstpay);
            pval -= firstpay;
            List<double> payarray = fulpacc(pval, rate, fee, minpay, payment, (int)months, false);
            farray.AddRange(payarray);

            return IRR2APR(IRR(pval, farray, 1));
        }

        /// <summary>
        /// Grabs the array of all monthly payments for specified PClass.
        /// Flag can be either:
        /// {@link API.CalculateOn.CheckoutPage}
        /// {@link API.CalculateOn.ProductPage}
        /// </summary>
        /// <param name="sum">The sum for the order/product.</param>
        /// <param name="pclass">{@link KlarnaPClass PClass} used to calculate the APR.</param>
        /// <param name="flags">Indicates if it is the checkout or a product page.</param>
        /// <returns>An array of monthly payments</returns>
        private List<double> GetPayArr(double sum, PClass pclass, API.CalculateOn flags)
        {
            double monthsfee = ((flags == API.CalculateOn.CheckoutPage) ? pclass.InvoiceFee : 0);
            double startfee = ((flags == API.CalculateOn.CheckoutPage) ? pclass.StartFee : 0);
            double minpay = 0.0;
            //Include start fee in sum
            sum += startfee;

            bool baseAccount = (pclass.Type == PClass.PClassType.Account);

            double lowest = GetLowestPaymentForAccount(pclass.Country);
            if (flags == API.CalculateOn.CheckoutPage)
            {
                minpay = (pclass.Type == PClass.PClassType.Account) ? lowest : 0;
            }
            else
            {
                minpay = 0;
            }

            double payment = Annuity(sum, pclass.Months, pclass.InterestRate);

            //Add monthly fee
            payment += monthsfee;

            return fulpacc(sum, pclass.InterestRate, monthsfee, minpay, payment, pclass.Months, baseAccount);
        }

        /// <summary>
        /// Calculates APR for the specified values.
        /// Result is rounded with two decimals.
        /// Flag can be either
        /// {@link API.CalculateOn.CheckoutPage}<br>
        /// {@link API.CalculateOn.ProductPage}<br>
        /// </summary>
        /// <param name="sum">The sum for the order/product.</param>
        /// <param name="pclass">{@link KlarnaPClass PClass} used to calculate the APR.</param>
        /// <param name="flags">Indicates if it is the checkout or a product page.</param>
        /// <param name="free">Number of free months @TODO This should be included in the pclass as months!</param>
        /// <returns>APR in %</returns>
        public double CalcAPR(double sum, PClass pclass, API.CalculateOn flags, int free)
        {

            double monthsfee = ((flags == API.CalculateOn.CheckoutPage) ? pclass.InvoiceFee : 0);
            double startfee = ((flags == API.CalculateOn.CheckoutPage) ? pclass.StartFee : 0);
            double minpay = 0;
            //Include start fee in sum
            sum += startfee;

            double lowest = GetLowestPaymentForAccount(pclass.Country);
            if (flags == API.CalculateOn.CheckoutPage)
            {
                minpay = (pclass.Type == PClass.PClassType.Account) ? lowest : 0;
            }
            else
            {
                minpay = 0;
            }

            //add monthly fee
            double payment = Annuity(sum, pclass.Months, pclass.InterestRate) + monthsfee;
            double apr = 0;

            switch (pclass.Type)
            {
                case PClass.PClassType.Campaign:
                case PClass.PClassType.Account:
                    apr = APR_Annuity(sum, pclass.Months, pclass.InterestRate, pclass.InvoiceFee, minpay);
                    break;
                case PClass.PClassType.SpecialCampaign:
                    apr = APR_PayInXMonths(sum, payment, pclass.InterestRate, pclass.InvoiceFee, minpay, free); //@TODO free = pclass.getMonths() ?
                    break;
                case PClass.PClassType.FixedPrice:
                    apr = APR_FixedPrice(sum, payment, pclass.InterestRate, pclass.InvoiceFee, minpay);
                    break;
                default:
                    throw new KlarnaException(-999, "Error in " + strace.GetFrame(0).GetMethod() + ": Unknown PClass type! (" + pclass.Type + ")");
            }

            return Math.Round(apr, 2);
        }

        /// <summary>
        /// Calculates the total credit purchase cost.
        /// Flag can be either
        /// <see cref="API.CalculateOn.CheckoutPage"/>
        /// <see cref="API.CalculateOn.ProductPage"/>
        /// </summary>
        /// <param name="sum">The sum for the order/product.</param>
        /// <param name="pclass">PClass used to calculate total credit cost.<see cref="PClass"/></param>
        /// <param name="flags">Indicates if it is the checkout or a product page.</param>
        /// <returns>Credit purchase cost.</returns>
        public double TotalCreditPurchaseCost(double sum, PClass pclass, API.CalculateOn flags)
        {
            List<double> payarr = GetPayArr(sum, pclass, flags);

            double credit_cost = 0;
            foreach (double pay in payarr)
            {
                credit_cost += pay;
            }
            return (flags == API.CalculateOn.CheckoutPage) ? Math.Round((credit_cost), 2) : PRound((credit_cost), pclass.Country);
        }

        /// <summary>
        /// Calculates the monthly cost for the specified pclass.
        /// The result is rounded up to the correct value depending on the pclass country.
        /// <example>
        ///     In product view, round monthly cost with max 0.5 or 0.1 depending on currency.
        ///         10.50 SEK rounds to 11 SEK
        ///         10.49 SEK rounds to 10 SEK
        ///         8.55 EUR rounds to 8.6 EUR
        ///         8.54 EUR rounds to 8.5 EUR
        ///     In checkout, round the monthly cost to have 2 decimals.
        ///     For example “10.57 SEK/per månad”
        /// </example>
        /// Flag can be either
        /// <see cref="API.CalculateOn.CheckoutPage"/>
        /// <see cref="API.CalculateOn.ProductPage"/>
        /// </summary>
        /// <param name="sum">The sum for the order/product.</param>
        /// <param name="pclass">PClass used to calculate total credit cost.<see cref="PClass"/></param>
        /// <param name="flags">Indicates if it is the checkout or a product page.</param>
        /// <returns>The monthly cost</returns>
        public double CalcMonthlyCost(double sum, PClass pclass, API.CalculateOn flags)
        {
            List<double> payarr = GetPayArr(sum, pclass, flags);
            double value = payarr.Count > 0 ? (payarr[0]) : 0;
            return (flags == API.CalculateOn.CheckoutPage) ? Math.Round(value, 2) : PRound(value, pclass.Country);
        }

        /// <summary>
        /// Returns the lowest monthly payment for Klarna Account.
        /// </summary>
        /// <param name="country">Country <see cref="API.Country"/></param>
        /// <returns>Lowest monthly payment.</returns>
        public double GetLowestPaymentForAccount(API.Country country)
        {

            double lowest_monthly_payment = 0;

            switch (country)
            {
                case API.Country.Sweden:
                    lowest_monthly_payment = 50.0;
                    break;
                case API.Country.Norway:
                    lowest_monthly_payment = 95.0;
                    break;
                case API.Country.Finland:
                    lowest_monthly_payment = 8.95;
                    break;
                case API.Country.Denmark:
                    lowest_monthly_payment = 89.0;
                    break;
                case API.Country.Germany:
                case API.Country.Netherlands:
                case API.Country.Austria:
                    lowest_monthly_payment = 6.95;
                    break;
                default:
                    throw new KlarnaException(-999, "Error in " + strace.GetFrame(0).GetMethod() + ": Not allowed for this country!");
            }

            return lowest_monthly_payment;
        }

        /// <summary>
        /// Rounds a value depending on the specified country.
        /// </summary>
        /// <param name="value">The value to be rounded.</param>
        /// <param name="country">Country <see cref="API.Country"/></param>
        /// <returns>double|int</returns>
        public double PRound(double value, API.Country country)
        {
            int multiply = 1; //Round to closest integer
            switch (country)
            {
                case API.Country.Finland:
                case API.Country.Germany:
                case API.Country.Netherlands:
                case API.Country.Austria:
                    multiply = 10; //Round to closest decimal
                    break;
            }
            return Math.Floor((value * multiply) + 0.5) / multiply;
        }

    }
}
