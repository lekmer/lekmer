﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using System.Collections;

using Klarna.Core.Structs;

namespace Klarna.Core
{
    public partial class API
    {
        /// <summary>
        /// Performs a partial refund on an invoice, part payment or mobile purchase.
        /// </summary>
        /// <param name="invoiceNumber">Invoice to partially refund</param>
        /// <param name="creditNumber">Credit number</param>
        /// <returns>Invoice number</returns>
        public string CreditPart(string invoiceNumber, string creditNumber)
        {
            string digest = this.eid + ":" + invoiceNumber + ":";

            if (this.artnocoll.Count() > 0)
            {
                foreach (ArticleNo art in this.artnocoll.GetArticleNumbers())
                {
                    digest += art.ArticleNumber + ":" + art.Quantity + ":";
                }
            }

            digest = this.CreateDigest(digest + this.secret);

            string response = "";

            if (this.goodslist.Count() > 0)
            {
                response = this.CreditPartWithGoodslist(invoiceNumber, creditNumber, digest);
            }
            else
            {
                response = (string)CallKlarna("credit_part", this.eid, invoiceNumber, this.artnocoll.GetArticleNumbers(), creditNumber, digest);
            }

            if (!this.keepData)
            {
                this.artnocoll.Clear();
            }

            return response;
        }


        /// <summary>
        /// Protected function for handling new functionality of having goodslist as a new parameter
        /// </summary>
        /// <param name="invoiceNumber">Invoice to partially refund</param>
        /// <param name="creditNumber">Credit number</param>
        /// <param name="digest">Digest</param>
        /// <returns>Invoice number</returns>
        protected string CreditPartWithGoodslist(string invoiceNumber, string creditNumber, string digest)
        {
            if (this.artnocoll.Count() > 0)
            {
                return (string)CallKlarna("credit_part", this.eid, invoiceNumber, this.artnocoll.GetArticleNumbers(), creditNumber, digest, 0, this.goodslist.GetGoodsList());
            }

            return (string)CallKlarna("credit_part", this.eid, invoiceNumber, new ArticleNo[] { }, creditNumber, digest, 0, this.goodslist.GetGoodsList());
        }
    }
}
