﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Klarna.Core
{
    public class KlarnaConfig
    {
        private int eid;
        private string secret;
        private API.Country country;
        private API.Language language;
        private API.Currency currency;
        private API.Encoding encoding;
        private API.KlarnaServer mode;
        private string pcstorage;
        private string pcuri;
        private Dictionary<string, string> values = new Dictionary<string, string> ();

        public int EID {
            set { this.eid = value;
                this.values ["eid"] = ""; }
            get { return this.eid; }
        }

        public string Secret {
            set { this.secret = value;
                this.values ["secret"] = ""; }
            get { return this.secret; }
        }

        public API.Country Country {
            set { this.country = value;
                this.values ["country"] = ""; }
            get { return this.country; }
        }

        public API.Language Language {
            set { this.language = value;
                this.values ["language"] = ""; }
            get { return this.language; }
        }

        public API.Currency Currency {
            set { this.currency = value;
                this.values ["currency"] = ""; }
            get { return this.currency; }
        }

        public API.Encoding Encoding {
            set { this.encoding = value;
                this.values ["encoding"] = ""; }
            get { return this.encoding; }
        }

        public API.KlarnaServer Mode {
            set { this.mode = value;
                this.values ["mode"] = ""; }
            get { return this.mode; }
        }

        public string PCStorage {
            set { this.pcstorage = value;
                this.values ["pcstorage"] = ""; }
            get { return this.pcstorage; }
        }

        public string PCURI {
            set { this.pcuri = value;
                this.values ["pcuri"] = ""; }
            get { return this.pcuri; }
        }

        public Uri Server { set; get; }

        public KlarnaConfig ()
        {
            //In order to take no params and set the config values later by hand.
        }

        public KlarnaConfig (int eid, string secret, API.Country country, API.Language language, API.Currency currency, API.Encoding encoding, API.KlarnaServer mode, string pcstorage, string pcuri)
        {
            this.eid = eid;
            this.values ["eid"] = "";
            this.secret = secret;
            this.values ["secret"] = "";
            this.country = country;
            this.values ["country"] = "";
            this.language = language;
            this.values ["language"] = "";
            this.currency = currency;
            this.values ["currency"] = "";
            this.encoding = encoding;
            this.values ["encoding"] = "";
            this.mode = mode;
            this.values ["mode"] = "";
            this.pcstorage = pcstorage;
            this.values ["pcstorage"] = "";
            this.pcuri = pcuri;
            this.values ["pcuri"] = "";
        }

        public bool EmptyFields (string s)
        {
            if (s.Equals ("eid") && !this.values.ContainsKey ("eid")) {
                return true;
            }
            if (s.Equals ("secret") && !this.values.ContainsKey ("secret")) {
                return true;
            }
            if (s.Equals ("country") && !this.values.ContainsKey ("country")) {
                return true;
            }
            if (s.Equals ("language") && !this.values.ContainsKey ("language")) {
                return true;
            }
            if (s.Equals ("currency") && !this.values.ContainsKey ("currency")) {
                return true;
            }
            if (s.Equals ("encoding") && !this.values.ContainsKey ("encoding")) {
                return true;
            }
            if (s.Equals ("mode") && !this.values.ContainsKey ("mode")) {
                return true;
            }
            if (s.Equals ("pcstorage") && !this.values.ContainsKey ("pcstorage")) {
                return true;
            }
            if (s.Equals ("pcuri") && !this.values.ContainsKey ("pcuri")) {
                return true;
            } else {
                return false;
            }
        }
    }
}
