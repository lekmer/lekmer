﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections.Generic;
using System.Text;

namespace Klarna.Core
{
	public class PClass
	{
		private int eid;
		private int pClassID;
		private string description;
		private int months;
		private double startFee;
		private double invoiceFee;
		private double interestRate;
		private double minAmountForPClass;
		private API.Country country;
		private PClassType type;
		private DateTime expire;

		public int EID { get { return this.eid; } }
		public int PClassID { get { return this.pClassID; } }
		public string Description { get { return this.description; } }
		public int Months { get { return this.months; } }
		public double StartFee { get { return this.startFee; } }
		public double InvoiceFee { get { return this.invoiceFee; } }
		public double InterestRate { get { return this.interestRate; } }
		public double MinAmountForPClass { get { return this.minAmountForPClass; } }
		public API.Country Country { get { return this.country; } }
		public PClassType Type { get { return this.type; } }
		public DateTime ExpireDate { get { return this.expire; } }

		public PClass(int EID, int pClassID, string description, int months, double startFee, double invoiceFee, double interestRate,
			double minAmountForPClass, API.Country country, PClassType type, DateTime expire)
		{
			this.eid = EID;
			this.pClassID = pClassID;
			this.description = description;
			this.months = months;
			this.startFee = startFee;
			this.invoiceFee = invoiceFee;
			this.interestRate = interestRate;
			this.minAmountForPClass = minAmountForPClass;
			this.country = country;
			this.type = type;
			this.expire = expire;
		}

		/// <summary>
		/// Creates a array/dictionary representation of the PClass.
		/// </summary>
		/// <returns>
		/// A <see cref="Dictionary<System.String, System.Object>"/>
		/// </returns>
		public Dictionary<string, object> toArray()
		{
			Dictionary<string, object> dict = new Dictionary<string, object>();

			dict.Add("eid", eid);
			dict.Add("id", pClassID);
			dict.Add("description", description);
			dict.Add("months", months);
			dict.Add("startfee", startFee);
			dict.Add("invoicefee", invoiceFee);
			dict.Add("interestrate", interestRate);
			dict.Add("minamount", minAmountForPClass);
			dict.Add("country", country);
			dict.Add("type", type);
			dict.Add("expire", expire.ToString("yyyy-MM-dd"));

			return dict;
		}

		/// <summary>
		/// Compares the dictionaries in the PClasses.
		/// </summary>
		/// <param name="p">
		/// A <see cref="PClass"/>
		/// </param>
		/// <returns>
		/// A <see cref="System.Boolean"/>
		/// </returns>
		public bool Equals(PClass p)
		{
			Dictionary<string, object> dict = p.toArray();
			foreach (KeyValuePair<string, object> kv in (Dictionary<string, object>)toArray())
			{
				if (!dict[kv.Key].Equals(kv.Value))
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Specifies the different kinds of campaigns that are available <br />
		/// 0 = Regular part payment
		/// 1 = Klarna Account
		/// 2 = Special Campaign, Christmas/Spring campaign etc.
		/// 3 = A fixed price to be paid every months. Number of months vary by
		/// total purchase price
		/// </summary>
		public enum PClassType : int
		{
			Campaign = 0,
			Account = 1,
			SpecialCampaign = 2,
			FixedPrice = 3,
			Delay = 4,
			Mobile = 5,
			NULL = 999
		};

	}
}