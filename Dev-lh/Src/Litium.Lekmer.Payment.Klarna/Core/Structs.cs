﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using System.Text;

namespace Klarna.Core.Structs
{

    /// <summary>
    /// Representing the Goodslist
    /// </summary>
    public struct GoodsList
    {
        private Goods goods;
        private Article article;
        private List<Goods> goodslist;

        /// <summary>
        /// Adds an article to the Goodslist
        /// </summary>
        /// <param name="Quantity">Quantity of this item</param>
        /// <param name="ArticleNumber">Article number</param>
        /// <param name="title">The name of the article</param>
        /// <param name="price">The price of the article in crowns/euro</param>
        /// <param name="vat">Article VAT</param>
        /// <param name="discount">Discount in percent on this article</param>
        /// <param name="flags">Flag</param>
        public void AddArticle (int qty, string artno, string title, double price, double vat, double discount, API.GoodsIs flags)
        {
            goods = new Goods ();

            article = new Article (artno, title, price, vat, discount, (int)flags);


            if (goodslist == null) {
                goodslist = new List<Goods> ();
            }

            goods.Article = article;
            goods.Quantity = qty;

            goodslist.Add (goods);
        }

        /// <summary>
        /// Returns an array with Goods objects
        /// </summary>
        /// <returns>Array with Goods objects</returns>
        public Goods[] GetGoodsList ()
        {

            if (this.goodslist == null)
                throw new KlarnaException (-999, "No goods list supplied");

            return this.goodslist.ToArray ();
        }

        /// <summary>
        /// Returns number of goods on the goodslist
        /// </summary>
        /// <returns>Integer number of goods on the goodslist</returns>
        public int Count()
        {

            if (this.goodslist != null)
            {
                return this.goodslist.Count;
            }

            return 0;
        }

        /// <summary>
        /// Clears the goodslist
        /// </summary>
        public void Clear ()
        {
            this.goodslist = null;
        }
    }

    public struct Goods
    {
        /// <summary>
        /// An article
        /// </summary>
        [XmlRpcMember("goods")]
        public Article Article;
        /// <summary>
        /// Quantity of the article
        /// </summary>
        [XmlRpcMember("qty")]
        public int Quantity;

    }

    public struct Article
    {
        [XmlRpcMember("artno")]
        public readonly string ArticleNumber;
        [XmlRpcMember("title")]
        public readonly string Title;
        [XmlRpcMember("price")]
        public readonly int Price;
        [XmlRpcMember("vat")]
        public readonly double VAT;
        [XmlRpcMember("discount")]
        public readonly double Discount;
        [XmlRpcMember("flags")]
        public readonly int Flags;

        /// <summary>
        /// Constructor for Article struct
        /// </summary>
        /// <param name="ArticleNumber">Article number</param>
        /// <param name="title">Name of the article</param>
        /// <param name="price">Price of the article</param>
        /// <param name="vat">VAT on the article</param>
        /// <param name="discount">Percental discount</param>
        /// <param name="flags">Flag</param>
        public Article (string artno, string title, double price, double vat, double discount, int flags)
        {
            this.ArticleNumber = artno;
            this.Title = title;
            this.Price = (int)Math.Round (price * 100);
            this.VAT = vat;
            this.Discount = discount;
            this.Flags = flags;
        }
    }

    public struct ArticleNumberCollection
    {
        private ArticleNo article;
        private List<ArticleNo> articlelist;

        /// <summary>
        /// Adds an article to the internal article list
        /// </summary>
        /// <param name="Quantity">Quantity of this article</param>
        /// <param name="ArticleNumber">Article number for this article</param>
        public void AddArticleNumber (int qty, string artno)
        {

            article = new ArticleNo (qty, artno);

            if (articlelist == null) {
                articlelist = new List<ArticleNo> ();
            }

            articlelist.Add (article);
        }

        /// <summary>
        /// Returns an array containing the article number objects
        /// </summary>
        /// <returns>An array of ArticleNo objects</returns>
        public ArticleNo[] GetArticleNumbers ()
        {
            if (this.articlelist == null) {
                throw new KlarnaException (-999, "No article list supplied");
            }
            return articlelist.ToArray ();
        }

        /// <summary>
        /// Returns number of articles on the articlelist
        /// </summary>
        /// <returns>An integer of number of articles </returns>
        public int Count()
        {
            if (this.articlelist == null)
            {
                return 0;
            }

            return articlelist.Count;
        }

        public void Clear ()
        {
            this.articlelist = null;
        }
    }

    public struct ArticleNo
    {
        [XmlRpcMember("qty")]
        public int Quantity;
        [XmlRpcMember("artno")]
        public string ArticleNumber;

        public ArticleNo (int qty, string artno)
        {
            this.Quantity = qty;
            this.ArticleNumber = artno;
        }
    }

    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public struct AddressStruct
    {
        [XmlRpcMember("email")]
        private string emailp;
        [XmlRpcMember("telno")]
        private string telnop;
        [XmlRpcMember("cellno")]
        private string cellnop;
        [XmlRpcMember("fname")]
        private string fnamep;
        [XmlRpcMember("lname")]
        private string lnamep;
        [XmlRpcMember("street")]
        private string streetp;
        [XmlRpcMember("careof")]
        private string careofp;
        [XmlRpcMember("zip")]
        private string zipp;
        [XmlRpcMember("city")]
        private string cityp;
        [XmlRpcMember("country")]
        private int? countryp;
        [XmlRpcMember("house_number")]
        private string houseNumberp;
        [XmlRpcMember("house_extension")]
        private string houseExtensionp;
        [XmlRpcMember("company")]
        private string companyp;

        public string email { get { return this.emailp; } set { this.emailp = value; } }

        public string telno { get { return this.telnop; } set { this.telnop = value; } }

        public string cellno { get { return this.cellnop; } set { this.cellnop = value; } }

        public string fname { get { return this.fnamep; } set { this.fnamep = value; } }

        public string lname { get { return this.lnamep; } set { this.lnamep = value; } }

        public string street { get { return this.streetp; } set { this.streetp = value; } }

        public string careof { get { return this.careofp; } set { this.careofp = value; } }

        public string zip { get { return this.zipp; } set { this.zipp = value; } }

        public string city { get { return this.cityp; } set { this.cityp = value; } }

        public int? country { get { return this.countryp; } set { this.countryp = value; } }

        public string house_number { get { return this.houseNumberp; } set { this.houseNumberp = value; } }

        public string house_extension { get { return this.houseExtensionp; } set { this.houseExtensionp = value; } }

        public string company { get { return this.companyp; } set { this.companyp = value; } }

        /// <summary>
        /// Main constructor of klarna address
        /// </summary>
        /// <param name="email">Email address</param>
        /// <param name="telno">Tell number</param>
        /// <param name="cellno">Cellphone number</param>
        /// <param name="fname">First name</param>
        /// <param name="lname">Last name</param>
        /// <param name="careof">Careof</param>
        /// <param name="street">Street</param>
        /// <param name="zip">Zipcode</param>
        /// <param name="city">City</param>
        /// <param name="country">Country</param>
        /// <param name="houseNumber">House number</param>
        /// <param name="houseExtension">House extension</param>
        /// <param name="company">Company name</param>
        public AddressStruct (
            string email, string telno, string cellno, string fname, string lname,
            string careof, string street, string zip, string city, API.Country country,
            string houseNumber, string houseExtension, string company)
        {
            this.emailp = email;
            this.telnop = telno;
            this.cellnop = cellno;
            this.fnamep = fname;
            this.lnamep = lname;
            this.careofp = careof;
            this.streetp = street;
            this.zipp = zip;
            this.cityp = city;
            this.countryp = (int)country;
            this.houseNumberp = houseNumber;
            this.houseExtensionp = houseExtension;
            this.companyp = company;
        }

        /// <summary>
        /// Overloaded constructor for converting ShippingAddress to AddressStruct
        /// </summary>
        /// <param name="shipping">Shipping address</param>
        public AddressStruct (ShippingAddress shipping)
        {
            this.emailp = shipping.Email;
            this.telnop = shipping.PhoneNumber;
            this.cellnop = shipping.CellPhoneNumber;
            this.fnamep = shipping.Firstname;
            this.lnamep = shipping.Lastname;
            this.careofp = shipping.CareOf;
            this.streetp = shipping.Street;
            this.zipp = shipping.ZipCode;
            this.cityp = shipping.City;
            this.countryp = (int)shipping.Country;
            this.houseNumberp = shipping.HouseNumber;
            this.houseExtensionp = shipping.HouseExtension;
            this.companyp = shipping.CompanyName;
        }

        /// <summary>
        /// Overloaded constructor for converting BillingAddress to AddressStruct
        /// </summary>
        /// <param name="billing">Billing address</param>
        public AddressStruct (BillingAddress billing)
        {
            this.emailp = billing.Email;
            this.telnop = billing.PhoneNumber;
            this.cellnop = billing.CellPhoneNumber;
            this.fnamep = billing.Firstname;
            this.lnamep = billing.Lastname;
            this.careofp = billing.CareOf;
            this.streetp = billing.Street;
            this.zipp = billing.ZipCode;
            this.cityp = billing.City;
            this.countryp = (int)billing.Country;
            this.houseNumberp = billing.HouseNumber;
            this.houseExtensionp = billing.HouseExtension;
            this.companyp = billing.CompanyName;
        }

        /// <summary>
        /// Makes an empty instance of the class
        /// </summary>
        public void Clear ()
        {
            this = new AddressStruct();
        }
    }
}
