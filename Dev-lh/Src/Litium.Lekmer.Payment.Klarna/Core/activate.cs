/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using System.Collections;

using Klarna.Core.Structs;

namespace Klarna.Core
{
    public partial class API
    {
        #region Activate Info member
        public ActivateInfo ActivateInfo { get; set; }
        #endregion

        #region Overloads
        /// <summary>
        /// Activate the reservation matching the given reservation number with
        /// the specified OCR number.
        /// The OCR number will overvrite any OCR number previosly set in
        /// ActivateInfo
        /// </summary>
        /// <param name='rno'>
        /// Reservation number
        /// </param>
        /// <param name='ocr'>
        /// OCR Number
        /// </param>
        public string[] Activate (string rno, string ocr)
        {
            this.ActivateInfo.ocr = ocr;
            return Activate (rno);
        }

        /// <summary>
        /// Activate the reservation matching the given reservation number with
        /// flags affecting the behavior.
        /// Flags will overvrite any flags previosly set in ActivateInfo
        /// </summary>
        /// <param name='rno'>
        /// Reservation number
        /// </param>
        /// <param name='flags'>
        /// Flags that affect the behavior
        /// </param>
        public string[] Activate (string rno, API.Flag? flags)
        {
            this.ActivateInfo.flags = (int?) flags;
            return Activate (rno);
        }

        /// <summary>
        /// Activate the reservation matching the given reservation number with
        /// the specified OCR number and flags affecting the behavior.
        /// Flags and OCR will overvrite any such info previosly set in ActivateInfo
        /// </summary>
        /// <param name='rno'>
        /// Reservation number
        /// </param>
        /// <param name='ocr'>
        /// OCR Number
        /// </param>
        /// <param name='flags'>
        /// Flags that affect the behavior
        /// </param>
        public string[] Activate (string rno, string ocr, API.Flag? flags)
        {
            this.ActivateInfo.ocr = ocr;
            this.ActivateInfo.flags = (int?) flags;
            return Activate (rno);
        }
        #endregion

        /// <summary>
        /// Activate the reservation matching the given reservation number.
        /// Optional information should be set in ActivateInfo
        /// </summary>
        /// <param name='rno'>
        /// Reservation number
        /// </param>
        /// <see cref="Klarna.Core.Structs.ActivateInfo"/>
        /// <exception cref='KlarnaException'>
        /// Is thrown when the RNO is not specified, or if an error is recieved
        /// by Klarna Online
        /// </exception>
        /// <returns>
        /// A string array with risk status and reservation number.
        /// </returns>
        public string[] Activate (string rno)
        {
            // RNO in the only required field and not allowed to be null.
            // No other validation to be done.
            if (rno == null) {
                throw new KlarnaException (
                    "Error in Activate: RNO not specified"
                );
            }

            // Set the shipping info on the ActivateInfo
            this.ActivateInfo.shipment_info = this.ShipmentInfo;

            // Build the digest for the activate call. Null objects and empty
            // strings will be ignored.
            // The Proto VSN for the digest has to have it's . replaced by :
            // according to the specifications.
            try {
                this.ActivateInfo.article_numbers = this.artnocoll.GetArticleNumbers();
            } catch (KlarnaException e) {
                this.ActivateInfo.article_numbers = new ArticleNo[]{};
            }

            string digest = this.ActivateDigest(rno);

            this.ActivateInfo.SetShipmentInfo();
            // Make the call to KO
            string[] result = (string[])CallKlarna (
                "activate", this.eid, digest, rno, ToXmlRpcStruct(ActivateInfo)
            );

            // If keepData is set to false, clear the data.
            if (!this.keepData) {
                this.ClearData ();
            }

            return result;
        }

        /// <summary>
        /// All info that is sent in is part of the digest secret, in this order:
        /// [
        ///      proto_vsn, client_vsn, eid, rno, bclass, cust_no, flags,
        ///      orderid1, orderid2, reference, reference_code, delay_adjust,
        ///      artno, qty
        /// ].
        /// For every article, artno and qty are added to the digest.
        /// If no value is sent in for an optional field, there
        /// is no entry for this field in the digest secret. Shared secret
        /// is added at the end of the digest secret.
        /// </summary>
        /// <param name="rno">The rno</param>
        /// <returns>Resulting digest</returns>
        private string ActivateDigest(string rno)
        {
            List<object> args = new List<object>();
            args.Add(API.KLARNA_PROTOCOL.Replace(".",":"));
            args.Add(API.API_VERSION);
            args.Add(this.eid);
            args.Add(rno);
            args.Add(this.ActivateInfo.bclass);
            args.Add(this.ActivateInfo.cust_no);
            args.Add(this.ActivateInfo.flags);
            args.Add(this.ActivateInfo.ocr);
            args.Add(this.ActivateInfo.orderid1);
            args.Add(this.ActivateInfo.orderid2);
            args.Add(this.ActivateInfo.reference);
            args.Add(this.ActivateInfo.reference_code);
            args.Add(this.ActivateInfo.shipment_info.shipmentType);

            foreach (ArticleNo artno in this.ActivateInfo.article_numbers) {
                args.Add(artno.ArticleNumber);
                args.Add(artno.Quantity);
            }

            args.Add(this.secret);

            return this.buildDigest(args.ToArray());
        }
    }
}
