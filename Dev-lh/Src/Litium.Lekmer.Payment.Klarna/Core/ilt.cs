/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using System.Collections;

namespace Klarna.Core
{
    partial class API
    {
        /// <summary>
        /// The purpose of this method is to check if the customer needs to answer the ILT questions.
        ///
        /// You need to render the questions and then send the identifier
        /// with the user supplied answer in the IncomeExpense struct.
        ///
        /// Note:
        /// You need to set the shipping address before calling this method.
        /// </summary>
        /// <param name="amount">Amount including VAT.</param>
        /// <param name="pno">The SSN/PNO of the customer</param>
        /// <param name="gender">Customers gender</param>
        /// <param name="enc">PNO encoding</param>
        /// <returns>A list of questions</returns>
        public ILTInfo CheckILT (string pno, API.Gender? gender, double amount, int pclass, Encoding enc)
        {
            return CheckILT (pno, gender, (int)Math.Round (amount * 100), pclass, enc);
        }

        public ILTInfo CheckILT (string pno, API.Gender? gender, double amount, int pclass)
        {
            return CheckILT (pno, gender, amount, pclass, this.PNOEncoding);
        }

        private ILTInfo CheckILT (string pno, API.Gender? gender, int amount, int pclass)
        {
            return CheckILT (pno, gender, amount, pclass, this.PNOEncoding);
        }

        private ILTInfo CheckILT (string pno, API.Gender? gender, int amount, int pclass, Encoding enc)
        {
            if (this.shippingaddr == null) {
                throw new Exception ("No shipping address set");
            }

            string digest = CreateDigest (this.eid + ":" + pno + (int?)gender + ":" +
                pclass + ":" + amount + ":" + this.secret);
            XmlRpcStruct result = (XmlRpcStruct)CallKlarna ("check_ilt", pno,
                (gender != null ? (object)(int)gender : (string)""),
                this.shippingaddr, (int)this.currency,
                (int)this.country, (int)this.language, this.eid, digest,
                (int)enc, pclass, amount);

            ILTInfo kilt = new ILTInfo ();
            kilt.url = (string)result ["ilt_question_url"];
            foreach (string key in (Array) result ["ilt_question_ids"]) {
                kilt.AddQuestion (key);
            }

            return kilt;
        }
    }
}
