﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaCheckoutSetting : KlarnaSetting, IKlarnaCheckoutSetting
	{
		private const string _storageName = "KlarnaCheckout";

		protected override string StorageName
		{
			get { return _storageName; }
		}

		public KlarnaCheckoutSetting(IKlarnaSettingParser klarnaSettingParser)
			: base(klarnaSettingParser)
		{
		}

		public string ContentType
		{
			get { return GetString(GroupName, "ContentType"); }
		}

		public string CheckoutUrl
		{
			get { return GetString(GroupName, "CheckoutUrl"); }
		}

		public string ConfirmationUrl
		{
			get { return GetString(GroupName, "ConfirmationUrl"); }
		}

		public string PushUrl
		{
			get { return GetString(GroupName, "PushUrl"); }
		}

		public string TermsUrl
		{
			get { return GetString(GroupName, "TermsUrl"); }
		}
	}
}