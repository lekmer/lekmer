﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaPendingIntervalSetting : SettingBase, IKlarnaPendingIntervalSetting
	{
		protected override string StorageName
		{
			get { return "Klarna"; }
		}

		protected virtual string GroupName
		{
			get { return "KlarnaPendingIntervals"; }
		}

		public virtual int OrderStatusCheckInterval
		{
			get { return GetInt32(GroupName, "OrderStatusCheckInterval", 6); }
		}

		public virtual int DecisionGuaranteedInterval
		{
			get { return GetInt32(GroupName, "DecisionGuaranteedInterval", 24); }
		}
	}
}