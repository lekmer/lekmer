﻿namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaReservationResult : KlarnaResult, IKlarnaReservationResult
	{
		public string ReservationId { get; set; }
	}
}
