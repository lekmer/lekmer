﻿using System;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class LekmerCustomer : Scensum.Customer.Customer, ILekmerCustomer
	{
		private IFacebookUser _facebookUser;
		public IFacebookUser FacebookUser
		{
			get { return _facebookUser; }
			set
			{
				CheckChanged(_facebookUser, value);
				_facebookUser = value;
			}
		}

		public CustomerLoginType LoginType { get; set; }
	}
}