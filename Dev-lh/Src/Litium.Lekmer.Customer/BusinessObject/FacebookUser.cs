﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class FacebookUser : BusinessObjectBase, IFacebookUser
	{
		private int _customerId;
		public int CustomerId
		{
			get { return _customerId; }
			set
			{
				CheckChanged(_customerId, value);
				_customerId = value;
			}
		}

		private int _customerRegistryId;
		public int CustomerRegistryId
		{
			get { return _customerRegistryId; }
			set
			{
				CheckChanged(_customerRegistryId, value);
				_customerRegistryId = value;
			}
		}

		private string _facebookId;
		public string FacebookId
		{
			get { return _facebookId; }
			set
			{
				CheckChanged(_facebookId, value);
				_facebookId = value;
			}
		}

		private string _name;
		public string Name
		{
			get { return _name; }
			set
			{
				CheckChanged(_name, value);
				_name = value;
			}
		}

		private string _email;
		public string Email
		{
			get
			{ return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}

		private DateTime _createdDate;
		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		private bool _isAlternateLogin;
		public bool IsAlternateLogin
		{
			get
			{ return _isAlternateLogin; }
			set
			{
				CheckChanged(_isAlternateLogin, value);
				_isAlternateLogin = value;
			}
		}
	}
}
