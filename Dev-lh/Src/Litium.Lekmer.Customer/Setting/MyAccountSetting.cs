﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Customer
{
	public class MyAccountSetting : SettingBase, IMyAccountSetting
	{
		private const string _groupName = "MyAccount";

		protected override string StorageName
		{
			get { return "MyAccount"; }
		}

		public virtual int NewPasswordLinkLifeCycleValue
		{
			get { return GetInt32(_groupName, "NewPasswordLinkLifeCycleValue", 60); }
		}

		public string FacebookAppId
		{
			get { return GetString(_groupName, "fb_AppId"); }
		}

		public string FacebookAppSecret
		{
			get { return GetString(_groupName, "fb_AppSecret"); }
		}

		public string FacebookCookieMask
		{
			get { return GetString(_groupName, "fb_CookieMask"); }
		}

		public string FacebookAlgoritm
		{
			get { return GetString(_groupName, "fb_Algoritm"); }
		}

		public string FacebookTokenRequestUrl
		{
			get { return GetString(_groupName, "fb_TokenRequestUrl"); }
		}

		public string FacebookUserProfileRequestUrl
		{
			get { return GetString(_groupName, "fb_UserProfileRequestUrl"); }
		}

		public string FacebookPictureDataRequestUrl
		{
			get { return GetString(_groupName, "fb_PictureDataRequestUrl"); }
		}

		public string FacebookPictureRequestUrl
		{
			get { return GetString(_groupName, "fb_PictureRequestUrl"); }
		}
	}
}