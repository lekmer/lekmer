﻿using System.Data;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Mapper;

namespace Litium.Lekmer.Customer
{
	public class LekmerUserDataMapper : UserDataMapper
	{
		public LekmerUserDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IUser Create()
		{
			var user = base.Create();
			var lekmerUser = user as ILekmerUser;

			if (lekmerUser != null)
			{
				lekmerUser.IsAlternateLogin = MapValue<bool>("CustomerUser.IsAlternateLogin");
			}

			user.SetUntouched();
			return user;
		}
	}
}