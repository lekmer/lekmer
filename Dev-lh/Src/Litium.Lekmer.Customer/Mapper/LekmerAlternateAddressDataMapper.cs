﻿using System.Data;

namespace Litium.Lekmer.Customer.Mapper
{
	public class LekmerAlternateAddressDataMapper : LekmerAddressDataMapper<ILekmerAlternateAddress>
	{
		public LekmerAlternateAddressDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override string Prefix
		{
			get { return "Alternate"; }
		}
	}
}