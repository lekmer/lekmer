﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class LekmerCustomerRepository : CustomerRepository
	{
		public virtual void Delete(int customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerCustomerRepository.Delete");
			new DataHandler().ExecuteCommand("[customerlek].[pCustomerDelete]", parameters, dbSettings);
		}

		public virtual ICustomer GetByFacebookId(int channelId, string facebookId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FacebookId", facebookId, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("LekmerCustomerRepository.GetByFacebookId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[customerlek].[pCustomerGetByFacebookId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}