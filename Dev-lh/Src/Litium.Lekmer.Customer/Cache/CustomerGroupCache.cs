﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Customer
{
	public sealed class CustomerGroupCollectionCache : ScensumCacheBase<CustomerGroupCollectionCacheKey, Collection<ICustomerGroup>>
	{
		private static readonly CustomerGroupCollectionCache _instance = new CustomerGroupCollectionCache();

		private CustomerGroupCollectionCache()
		{
		}

		public static CustomerGroupCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class CustomerGroupCollectionCacheKey : ICacheKey
	{
		public CustomerGroupCollectionCacheKey(int customerId)
		{
			CustomerId = customerId;
		}

		public int CustomerId { get; set; }

		public string Key
		{
			get
			{
				return CustomerId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
