﻿using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerInformationSecureService : CustomerInformationSecureService, ILekmerCustomerInformationSecureService
	{
		protected LekmerCustomerInformationRepository LekmerRepository { get; private set; }

		public LekmerCustomerInformationSecureService(CustomerInformationRepository repository)
			: base(repository)
		{
			LekmerRepository = (LekmerCustomerInformationRepository) repository;
		}

		public override ICustomerInformation Create()
		{
			ICustomerInformation customerInformation = base.Create();

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				// Set default gender type
				lekmerCustomerInformation.GenderTypeId = (int)GenderTypeInfo.Unknown;
			}

			customerInformation.Status = BusinessObjectStatus.New;
			return customerInformation;
		}

		public void DeleteByCustomer(ISystemUserFull systemUserFull, int customerId)
		{
			LekmerRepository.EnsureNotNull();
			LekmerRepository.DeleteByCustomer(customerId);
		}
	}
}