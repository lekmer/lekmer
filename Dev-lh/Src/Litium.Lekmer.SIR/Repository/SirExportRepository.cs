﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SIR.Repository
{
	public class SirExportRepository
	{
		protected virtual DataMapperBase<IProductInfo> CreateProductInfoDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductInfo>(dataReader);
		}

		protected virtual DataMapperBase<ISirBatch> CreateSirBatchDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISirBatch>(dataReader);
		}

		public virtual void RefreshProducts()
		{
			var dbSettings = new DatabaseSetting("SirExportRepository.RefreshProducts");

			IDataParameter[] parameters ={};

			new DataHandler().ExecuteCommand("[integration].[pSirProductsRefresh]", parameters, dbSettings);
		}

		public virtual Collection<IProductInfo> GetAll()
		{
			var dbSettings = new DatabaseSetting("SirExportRepository.GetAll");

			Collection<IProductInfo> productInfoCollection;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pSirProductsGetAll]", dbSettings))
			{
				productInfoCollection = CreateProductInfoDataMapper(dataReader).ReadMultipleRows();
			}

			return productInfoCollection;
		}
		
		public virtual Collection<IProductInfo> GetChangesFromDate(DateTime? fromDate)
		{
			var dbSettings = new DatabaseSetting("SirExportRepository.GetChangesFromDate");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("FromDate", fromDate, SqlDbType.DateTime)
			};

			Collection<IProductInfo> productInfoCollection;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pSirProductsGetChangesFromDate]", parameters, dbSettings))
			{
				productInfoCollection = CreateProductInfoDataMapper(dataReader).ReadMultipleRows();
			}

			return productInfoCollection;
		}

		public virtual ISirBatch StartNewBatch()
		{
			var dbSettings = new DatabaseSetting("SirExportRepository.StartNewBatch");

			ISirBatch batch;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pSirBatchGetNew]", dbSettings))
			{
				batch = CreateSirBatchDataMapper(dataReader).ReadRow();
			}

			return batch;
		}

		public virtual ISirBatch CompleteBatch(int batchId)
		{
			var dbSettings = new DatabaseSetting("SirExportRepository.CompleteBatch");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BatchId", batchId, SqlDbType.Int)
			};

			ISirBatch batch;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pSirBatchComplete]", parameters, dbSettings))
			{
				batch = CreateSirBatchDataMapper(dataReader).ReadRow();
			}

			return batch;
		}
	}
}
