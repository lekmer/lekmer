﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using Litium.Lekmer.Common.Job;
using log4net;

namespace Litium.Lekmer.TwosellExport.Service
{
	public partial class TwosellExporterServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private object _syncToken = new object();
		private Collection<IJob> _jobs = new Collection<IJob>();

		public TwosellExporterServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			try
			{
				foreach (IJob job in _jobs)
				{
					job.StopJob();
				}

				_log.Info("Service stopped successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Error occurred while stopping service.", ex);
			}
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var orderInfoJob = new OrderInfoExportJob { ScheduleSetting = new OrderInfoExportScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(orderInfoJob);

			var productInfoJob = new ProductInfoExportJob { ScheduleSetting = new ProductInfoExportScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(productInfoJob);
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}
