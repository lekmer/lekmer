﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwosellExport.Service
{
	public class OrderInfoExportJob : BaseJob
	{
		public override string Group
		{
			get { return "TwosellExport"; }
		}

		public override string Name
		{
			get { return "OrderInfoExportJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<IExporter>("OrderInfoExporter").Execute();
		}
	}
}
