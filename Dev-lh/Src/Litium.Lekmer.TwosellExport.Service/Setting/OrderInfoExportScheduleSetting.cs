﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.TwosellExport.Service
{
	public class OrderInfoExportScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "TwosellExport"; }
		}

		protected override string GroupName
		{
			get { return "OrderInfoExportJob"; }
		}
	}
}