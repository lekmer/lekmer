﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;

namespace Litium.Lekmer.BrandTopList.Web
{
	public class BrandGridControl : GridControl<IBrand>
	{
		public IBlockBrandTopList Block { get; set; }
		public IContentNodeService ContentNodeService { get; set; }
		public IBlockBrandTopListCategoryService BlockBrandTopListCategoryService { get; set; }

		private string _filterUrl;

		public override string Render()
		{
			_filterUrl = GetFilterUrl();

			return base.Render();
		}

		protected override string RenderItem(int columnIndex, IBrand brand)
		{
			if (brand == null)
			{
				throw new ArgumentNullException("brand");
			}

			Fragment fragment = Template.GetFragment(ItemFragmentName);
			AddPositionCondition(fragment, columnIndex, ColumnCount);
			fragment.AddEntity(brand);

			fragment.AddVariable("Brand.FilterUrl", GetBrandUrl(brand));

			return fragment.Render();
		}

		protected virtual string GetFilterUrl()
		{
			if (Block.LinkContentNodeId.HasValue)
			{
				return GetContentNodeUrl(Block.LinkContentNodeId.Value);
			}

			Collection<ICategoryView> categories = BlockBrandTopListCategoryService.GetViewAllByBlockWithoutChildren(UserContext.Current, Block);
			if (categories != null && categories.Count == 1)
			{
				if (categories[0].ProductParentContentNodeId.HasValue)
				{
					return GetContentNodeUrl(categories[0].ProductParentContentNodeId.Value);
				}
			}

			return string.Empty;
		}

		protected virtual string GetBrandUrl(IBrand brand)
		{
			if (!_filterUrl.IsNullOrEmpty())
			{
				return _filterUrl + GetFilterQueryString(brand);
			}

			if (brand.ContentNodeId.HasValue)
			{
				return GetContentNodeUrl(brand.ContentNodeId.Value);
			}

			return string.Empty;
		}

		protected virtual string GetContentNodeUrl(int contentNodeId)
		{
			var item = ContentNodeService.GetTreeItemById(UserContext.Current, contentNodeId);
			if (item == null || item.Url.IsNullOrEmpty())
			{
				return null;
			}

			return UrlHelper.ResolveUrlHttp(item.Url);
		}

		protected virtual string GetFilterQueryString(IBrand brand)
		{
			var query = new QueryBuilder();
			query.Add("mode", "filter");
			query.Add("brand-id", brand.Id.ToString(CultureInfo.InvariantCulture));
			return query.ToString();
		}
	}
}