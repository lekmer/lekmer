﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Klarna.Core;
using Litium.Lekmer.Payment.Klarna;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	/// <summary>
	/// Payment control - Klarna logic
	/// </summary>
	public partial class Payment
	{
		// Properties.
		private IKlarnaClient _klarnaClient;
		protected IKlarnaClient KlarnaClient
		{
			get { return _klarnaClient ?? (_klarnaClient = KlarnaFactory.CreateKlarnaClient(UserContext.Current.Channel.CommonName)); }
		}

		private IKlarnaClient _klarnaClientAdvanced;
		protected IKlarnaClient KlarnaClientAdvanced
		{
			get { return _klarnaClientAdvanced ?? (_klarnaClientAdvanced = KlarnaFactory.CreateKlarnaAdvancedClient(UserContext.Current.Channel.CommonName)); }
		}

		protected int ProfileKlarnaPartPaymentId { get; set; }

		public int ActiveKlarnaPartPaymentId { get; private set; }

		public bool IsKlarnaPaymentActive
		{
			get
			{
				var paymentMethodType = (PaymentMethodType)ActivePaymentTypeId;

				switch (paymentMethodType)
				{
					case PaymentMethodType.KlarnaInvoice:
					case PaymentMethodType.KlarnaPartPayment:
					case PaymentMethodType.KlarnaSpecialPartPayment:
					case PaymentMethodType.KlarnaAdvanced:
					case PaymentMethodType.KlarnaAdvancedPartPayment:
					case PaymentMethodType.KlarnaAdvancedSpecialPartPayment:
						return true;
				}

				return false;
			}
		}


		// Methods
		protected virtual void InitializeActiveKlarnaPartPayment()
		{
			if (PaymentTypeHelper.IsKlarnaPartPayment(ActivePaymentTypeId) == false) // just for storing selected option in profile
			{
				if (PaymentForm.KlarnaPartPaymentId > 0)
				{
					ActiveKlarnaPartPaymentId = PaymentForm.KlarnaPartPaymentId;
				}
				return;
			}

			int klarnaPartPaymentId = PaymentForm.KlarnaPartPaymentId;
			bool paymentTypeIdIsValid = IsValidKlarnaPartPaymentType(ActivePaymentTypeId, klarnaPartPaymentId);

			if (paymentTypeIdIsValid == false)
			{
				klarnaPartPaymentId = ProfileKlarnaPartPaymentId;
				paymentTypeIdIsValid = IsValidKlarnaPartPaymentType(ActivePaymentTypeId, klarnaPartPaymentId);
			}

			if (paymentTypeIdIsValid) // == true
			{
				ActiveKlarnaPartPaymentId = klarnaPartPaymentId;
			}
		}


		// Render
		protected virtual void RenderPaymentTypeExtensionForKlarnaAll(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isKlarnaPayment = PaymentTypeHelper.IsKlarnaPayment(paymentTypeId);
			bool isKlarnaPaymentAndSelected = paymentTypeIsSelected && isKlarnaPayment;

			fragment.AddCondition("isKlarnaPayment", isKlarnaPayment);
			fragment.AddCondition("isKlarnaPaymentAndSelected", isKlarnaPaymentAndSelected);

			if (isKlarnaPayment)
			{
				decimal lowestMonthlyFee = (decimal)GetLowestMonthlyFee(paymentTypeId);
				fragment.AddVariable("KlarnaInvoiceCharge", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, lowestMonthlyFee));

				var klarnaSetting = GetKlarnaClient(paymentTypeId).KlarnaSetting;
				fragment.AddVariable("EID", klarnaSetting.EID.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);
			}
		}

		protected virtual void RenderPaymentTypeExtensionForKlarnaInvoice(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isKlarnaInvoice = PaymentTypeHelper.IsKlarnaInvoicePayment(paymentTypeId);
			bool isKlarnaInvoiceAndSelected = paymentTypeIsSelected && isKlarnaInvoice;

			fragment.AddCondition("isKlarnaInvoice", isKlarnaInvoice);
			fragment.AddCondition("isKlarnaInvoiceAndSelected", isKlarnaInvoiceAndSelected);
		}

		protected virtual void RenderPaymentTypeExtensionForKlarnaPartPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isKlarnaPartPayment = PaymentTypeHelper.IsKlarnaPartPayment(paymentTypeId);
			bool isKlarnaPartPaymentAndSelected = paymentTypeIsSelected && isKlarnaPartPayment;

			string klarnaPartPaymentContent = string.Empty;
			if (isKlarnaPartPaymentAndSelected)
			{
				klarnaPartPaymentContent = RenderKlarnaPartPaymentAlternativ(paymentTypeId);
			}

			fragment.AddCondition("isKlarnaPartPayment", isKlarnaPartPayment);
			fragment.AddCondition("isKlarnaPartPaymentAndSelected", isKlarnaPartPaymentAndSelected);

			fragment.AddVariable("Iterate:PartPayment", klarnaPartPaymentContent, VariableEncoding.None);
		}

		protected virtual void RenderPaymentTypeExtensionForKlarnaSpecialPartPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isKlarnaSpecialPartPayment = PaymentTypeHelper.IsKlarnaSpecialPartPayment(paymentTypeId);
			bool isKlarnaSpecialPartPaymentAndSelected = paymentTypeIsSelected && isKlarnaSpecialPartPayment;

			fragment.AddCondition("isKlarnaSpecialPartPayment", isKlarnaSpecialPartPayment);
			fragment.AddCondition("isKlarnaSpecialPartPaymentAndSelected", isKlarnaSpecialPartPaymentAndSelected);

			string pClassId = "none";
			if (isKlarnaSpecialPartPayment)
			{
				int klarnaPClass = GetKlarnaSpecialPartPaymentId(paymentTypeId, OrderTotalSum);
				if (klarnaPClass > 0)
					pClassId = klarnaPClass.ToString(CultureInfo.InvariantCulture);
			}

			fragment.AddVariable("SpecialPartPayment.Id", pClassId);
		}

		protected virtual void RenderPaymentTypeExtensionForKlarnaCheckout(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isKlarnaCheckout = PaymentTypeHelper.IsKlarnaCheckoutPayment(paymentTypeId);
			bool isKlarnaCheckoutAndSelected = paymentTypeIsSelected && isKlarnaCheckout;

			fragment.AddCondition("isKlarnaCheckout", isKlarnaCheckout);
			fragment.AddCondition("isKlarnaCheckoutAndSelected", isKlarnaCheckoutAndSelected);
		}


		/// <summary>
		/// List the part payment alternatives. 
		/// </summary>
		/// <param name="paymentTypeId"> </param>
		protected virtual string RenderKlarnaPartPaymentAlternativ(int paymentTypeId)
		{
			double totalOrderCost = OrderTotalSum;
			List<PClass> pClasses = GetKlarnaClient(paymentTypeId).GetPClasses().Cast<PClass>().ToList();
			var partPaymentBuilder = new StringBuilder();

			foreach (PClass pClass in pClasses)
			{
				if (totalOrderCost >= pClass.MinAmountForPClass)
				{
					string description;
					decimal monthlyCost = (decimal)GetKlarnaClient(paymentTypeId).CalculateMonthlyCost(totalOrderCost, pClass, true);
					var pClassType = pClass.Type;

					if (pClassType == PClass.PClassType.Account/*1*/)
					{
						description = AliasHelper.GetAliasValue("Order.Checkout.Payment.KlarnaPartPaymentAccount").Replace("x", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyCost));
					}
					else if (pClassType == PClass.PClassType.SpecialCampaign/*2*/)
					{
						continue;
					}
					else
					{
						description = AliasHelper.GetAliasValue("Order.Checkout.Payment.KlarnaPartPayment").Replace("x", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyCost));
						description = description.Replace("y", pClass.Months.ToString(CultureInfo.InvariantCulture));
					}

					partPaymentBuilder.Append(RenderKlarnaPartPaymentSubType(description, pClass.PClassID));
				}
			}

			return partPaymentBuilder.ToString();
		}

		protected virtual string RenderKlarnaPartPaymentSubType(string description, int pClassId)
		{
			bool selected = ActiveKlarnaPartPaymentId == pClassId;

			Fragment fragmentPartPayment = _template.GetFragment("PartPayment");

			fragmentPartPayment.AddCondition("IsSelected", selected);
			fragmentPartPayment.AddVariable("PartPayment.Title", description, VariableEncoding.None);
			fragmentPartPayment.AddVariable("PartPayment.Id", pClassId.ToString(CultureInfo.InvariantCulture));

			return fragmentPartPayment.Render();
		}

		/// <summary>
		/// Gets the klarna special part payment id (pclass).
		/// </summary>
		protected virtual int GetKlarnaSpecialPartPaymentId(int paymentTypeId, double totalOrderCost)
		{
			if (totalOrderCost <= 0)
			{
				totalOrderCost = 1;
			}

			List<PClass> pClasses = GetKlarnaClient(paymentTypeId).GetPClasses().Cast<PClass>().ToList();

			PClass klarnaPClass = pClasses
				.Where(pc => pc.MinAmountForPClass <= totalOrderCost)
				.Where(pc => pc.Type == PClass.PClassType.SpecialCampaign /*2*/)
				.Where(pc => pc.ExpireDate > DateTime.Now.Date)
				.OrderByDescending(pc => pc.ExpireDate)
				.FirstOrDefault();

			int pClassId = 0;
			if (klarnaPClass != null)
			{
				pClassId = klarnaPClass.PClassID;
			}

			return pClassId;
		}


		// Validation
		protected virtual bool IsValidKlarnaPartPaymentType(int paymentTypeId, int partPaymentTypeId)
		{
			bool isPaymentValid = false;

			if (partPaymentTypeId > 0)
			{
				isPaymentValid = IsValidKlarnaPclass(paymentTypeId, partPaymentTypeId);
			}

			return isPaymentValid;
		}

		protected virtual bool IsValidKlarnaPclass(int paymentTypeId, int klarnaPclass)
		{
			return GetKlarnaClient(paymentTypeId).GetPClasses().Cast<PClass>().Any(p => p.PClassID == klarnaPclass);
		}


		// Payment type checks
		protected virtual bool IsKlarnaSpecialPartPaymentAvailable()
		{
			return AvailablePaymentTypes.Any(pt => PaymentTypeHelper.IsKlarnaSpecialPartPayment(pt.Id));
		}

		protected virtual IKlarnaClient GetKlarnaClient(int paymentTypeId)
		{
			if (PaymentTypeHelper.IsKlarnaSimplePayment(paymentTypeId))
			{
				return KlarnaClient;
			}
			if (PaymentTypeHelper.IsKlarnaAdvancedPayment(paymentTypeId))
			{
				return KlarnaClientAdvanced;
			}
			return null;
		}
	}
}