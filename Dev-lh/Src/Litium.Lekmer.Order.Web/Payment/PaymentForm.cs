﻿using System.Collections.Specialized;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class PaymentForm : ControlBase, IPaymentForm
	{
		protected string PaymentTypeIdFormName
		{
			get { return "paymentType"; }
		}
		protected string KlarnaPartPaymentIdFormName
		{
			get { return "Partpaymentlist"; }
		}
		protected string KlarnaSpecialPartPaymentIdFormName
		{
			get { return "specialPartPayment"; }
		}
		protected string MaksuturvaSubPaymentIdFormName
		{
			get { return "MaksuturvaSubPayment"; }
		}
		protected string QliroPartPaymentIdFormName
		{
			get { return "QliroPartPaymentId"; }
		}
		protected string CollectorPartPaymentIdFormName
		{
			get { return "CollectorPartPaymentId"; }
		}

		public virtual int PaymentTypeId { get; set; }
		public virtual int KlarnaPartPaymentId { get; set; }
		public virtual string MaksuturvaSubPaymentId { get; set; }
		public string QliroPartPaymentId { get; set; }
		public string CollectorPartPaymentId { get; set; }

		public virtual void MapFromRequest()
		{
			NameValueCollection form = Request.Form;

			//PaymentTypeId
			string paymentTypeIdFromRequest = form[PaymentTypeIdFormName];
			int paymentTypeId;
			if (int.TryParse(paymentTypeIdFromRequest, out paymentTypeId))
			{
				PaymentTypeId = paymentTypeId;
			}

			//KlarnaPartPaymentId
			string klarnaPartPaymentIdFromRequest = form[KlarnaPartPaymentIdFormName];
			int klarnaPartPaymentId;
			if (int.TryParse(klarnaPartPaymentIdFromRequest, out klarnaPartPaymentId))
			{
				KlarnaPartPaymentId = klarnaPartPaymentId;
			}

			MaksuturvaSubPaymentId = form[MaksuturvaSubPaymentIdFormName];
			QliroPartPaymentId = form[QliroPartPaymentIdFormName];
			CollectorPartPaymentId = form[CollectorPartPaymentIdFormName];
		}

		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PaymentTypeId.Name", PaymentTypeIdFormName);
			fragment.AddVariable("Form.KlarnaPartPaymentId.Name", KlarnaPartPaymentIdFormName);
			fragment.AddVariable("Form.KlarnaSpecialPartPaymentId.Name", KlarnaSpecialPartPaymentIdFormName);
			fragment.AddVariable("Form.MaksuturvaSubPaymentId.Name", MaksuturvaSubPaymentIdFormName);
			fragment.AddVariable("Form.QliroPartPaymentId.Name", QliroPartPaymentIdFormName);
			fragment.AddVariable("Form.CollectorPartPaymentId.Name", CollectorPartPaymentIdFormName);
		}
	}
}