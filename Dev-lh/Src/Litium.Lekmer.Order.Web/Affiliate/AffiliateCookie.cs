using System;
using System.Web;

namespace Litium.Lekmer.Order.Web
{
	public static class AffiliateCookie
	{
		public static string TradeDoublerIdKey = "TradeDoublerId";

		public static string GetTradeDoublerId()
		{
			HttpCookie cookie = HttpContext.Current.Request.Cookies[TradeDoublerIdKey];
			if (cookie == null) return null;
			return cookie.Value;
		}

		public static void SetTradeDoublerId(string tradeDoublerId)
		{
			if (!string.IsNullOrEmpty(tradeDoublerId))
			{
				DateTime dtNow = DateTime.Now;
				TimeSpan tsMinute = new TimeSpan(354,50, 0, 0, 0);
				HttpCookie cookie = HttpContext.Current.Request.Cookies[TradeDoublerIdKey];
				if (cookie == null)
				{
					cookie = new HttpCookie(TradeDoublerIdKey);
				}
				cookie.Value = tradeDoublerId;
				cookie.Expires = dtNow + tsMinute;
				HttpContext.Current.Response.Cookies.Add(cookie);
			}
		}
	}
}