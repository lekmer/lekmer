﻿using System;
using System.Web;
using System.Web.SessionState;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Order.Web.Cart;

namespace Litium.Lekmer.Order.Web.Cart
{
	public class LekmerCartSession : CartSession, ILekmerCartSession
	{
		public LekmerCartSession(IUserContextFactory userContextFactory)
			: base(userContextFactory)
		{
		}

		private static HttpSessionState Session
		{
			get { return HttpContext.Current.Session; }
		}

		/// <summary>
		/// The current cart version token.
		/// </summary>
		public Guid CartVersionToken
		{
			get
			{
				if (Session == null)
				{
					return Guid.Empty;
				}

				return (Guid)(Session["CartVersionToken"] ?? Guid.Empty);
			}
			set
			{
				if (Session == null)
				{
					return;
				}

				if (value == Guid.Empty)
				{
					Session.Remove("CartVersionToken");
				}
				else
				{
					Session["CartVersionToken"] = value;
				}
			}
		}

		public virtual void Clean()
		{
			CartVersionToken = Guid.Empty;
			Cart = null;
		}
	}
}
