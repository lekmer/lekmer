using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	/// <summary>
	/// Extended mini cart component.
	/// </summary>
	public class ExtendedMiniCart : ComponentControl
	{
		protected ILekmerCartSession CartSession { get; private set; }
		protected IVoucherSession VoucherSession { get; private set; }
		protected IAvailSession AvailSession { get; private set; }
		protected ILekmerFormatter Formatter { get; private set; }
		protected ICartService CartService { get; private set; }
		protected LekmerOrderService LekmerOrderService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }

		protected int? CartItemAdded
		{
			get { return Session["CartItemAdded"] == null ? null : Session["CartItemAdded"] as int?; }
		}

		private readonly Dictionary<LekmerCartItemKey, bool> _updatedItems = new Dictionary<LekmerCartItemKey, bool>();
		private int _maxQuantity;
		private bool _allowPostback;
		private CartHelper _cartHelper;

		private ExtendedMiniCartForm _extendedMiniCartForm;
		public ExtendedMiniCartForm ExtendedMiniCartForm
		{
			get
			{
				if (_extendedMiniCartForm == null)
				{
					_extendedMiniCartForm = new ExtendedMiniCartForm(string.Empty);
				}

				return _extendedMiniCartForm;
			}
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "ExtendedMiniCart"; }
		}

		public ExtendedMiniCart(ITemplateFactory templateFactory, ILekmerCartSession cartSession, IVoucherSession voucherSession, IAvailSession availSession, IFormatter formatter, ICartService cartService, IOrderService lekmerOrderService, IProductSizeService productSizeService, ITagGroupService tagGroupService)
			: base(templateFactory)
		{
			CartSession = cartSession;
			VoucherSession = voucherSession;
			AvailSession = availSession;
			Formatter = (ILekmerFormatter)formatter;
			CartService = cartService;
			LekmerOrderService = (LekmerOrderService) lekmerOrderService;
			ProductSizeService = productSizeService;
			TagGroupService = tagGroupService;
		}

		/// <summary>
		/// Render the component's body and head.
		/// </summary>
		public override ComponentContent Render()
		{
			Initialize();

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			string body = RenderCart() ?? RenderEmptyCart();
			return new ComponentContent(head, body, footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		/// <summary>
		/// Renders the cart when there's no items added to it.
		/// </summary>
		/// <returns>Rendered empty cart.</returns>
		protected virtual string RenderEmptyCart()
		{
			return Template.GetFragment("Empty").Render();
		}

		/// <summary>
		/// Renders a cart with items.
		/// </summary>
		/// <returns>Rendered cart. Null if no items is added to the cart.</returns>
		protected virtual string RenderCart()
		{
			ILekmerCartFull cart;
			Collection<ILekmerCartItem> cartItems;
			if (IsCartNullOrEmpty(out cart, out cartItems))
			{
				return null;
			}

			// Change Quantity.
			if (IsPostBack && ExtendedMiniCartForm.IsChangeQuantityPost() && _allowPostback)
			{
				ChangeQuantityOfItems(cart);

				if (IsCartNullOrEmpty(out cart, out cartItems))
				{
					return null;
				}
			}

			Fragment fragmentContent = Template.GetFragment("Content");

			RenderTrackingInfo(fragmentContent);

			fragmentContent.AddVariable("Iterate:CartItem", RenderCartItems(cartItems), VariableEncoding.None);

			// Cart info
			_cartHelper.RenderCartInfo(fragmentContent, cart, Formatter);

			// Cart price
			_cartHelper.RenderCartPrices(fragmentContent, cart, Formatter);

			ExtendedMiniCartForm.MapFieldNamesToFragment(fragmentContent);
			ExtendedMiniCartForm.MapFieldValuesToFragment(fragmentContent);

			return fragmentContent.Render();
		}

		/// <summary>
		/// Render avail info.
		/// </summary>
		protected virtual void RenderTrackingInfo(Fragment fragmentContent)
		{
			int? productId = AvailSession.AvailProductId;
			string trackingCode = AvailSession.TrackingCode;
			bool isRecommendedProduct = !string.IsNullOrEmpty(trackingCode);

			fragmentContent.AddCondition("AddToAvail", productId.HasValue);
			fragmentContent.AddCondition("Product.IsRecommended", isRecommendedProduct);

			if (productId.HasValue)
			{
				fragmentContent.AddVariable("Product.Id", productId.Value.ToString(CultureInfo.InvariantCulture));
				AvailSession.AvailProductId = null;
			}

			if (isRecommendedProduct)
			{
				fragmentContent.AddVariable("TrackingCode", trackingCode);
				AvailSession.TrackingCode = null;
			}
		}

		/// <summary>
		/// Renders the items in a cart.
		/// </summary>
		/// <param name="lekmerCartItems">The items to render.</param>
		/// <returns>Rendered items.</returns>
		protected virtual string RenderCartItems(Collection<ILekmerCartItem> lekmerCartItems)
		{
			var itemBuilder = new StringBuilder();

			foreach (ILekmerCartItem cartItem in lekmerCartItems)
			{
				itemBuilder.Append(RenderCartItem(cartItem, GroupByProductAndSizeAndHashCode(lekmerCartItems)));
			}

			return itemBuilder.ToString();
		}

		/// <summary>
		/// Renders a cart item.
		/// </summary>
		/// <param name="cartItem">The cart item to render.</param>
		/// <param name="groupedCartItems">Cart items grouped by pair: productId, sizeId.</param>
		/// <returns>Rendered item.</returns>
		protected virtual string RenderCartItem(ILekmerCartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems)
		{
			Fragment fragment = Template.GetFragment("CartItem");

			// Product.
			fragment.AddEntity(cartItem.Product);

			// Size.
			fragment.AddCondition("CartItem.HasSize", cartItem.SizeId.HasValue);
			fragment.AddEntity(GetSize(cartItem));

			fragment.AddVariable("Product.CampaignInfoPriceHidden", cartItem.Product.CampaignInfo.Price.IncludingVat.ToString());
			fragment.AddVariable("CartItem.Quantity", Formatter.FormatNumber(Channel.Current, cartItem.Quantity));

			// Price
			_cartHelper.RenderCartItemPrices(fragment, cartItem, Formatter);

			// Stock.
			bool isInStock = LekmerOrderService.CheckNumberInStock(UserContext.Current, cartItem, groupedCartItems);
			int quantityLimit = ObtainQuantityLimit(cartItem);
			fragment.AddCondition("Product.IsInStock", isInStock);
			fragment.AddCondition("IsNumberInStockTooSmall", cartItem.Quantity > quantityLimit);
			fragment.AddVariable("Product.NumberInStockForSize", quantityLimit.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);

			bool itemWasAdded = CartItemAdded.HasValue && CartItemAdded.Value == cartItem.Product.Id;
			fragment.AddCondition("ItemWasAdded", itemWasAdded);

			fragment.AddVariable("Iterate:QuantityOption", RenderQuantityOptions(cartItem.Quantity, quantityLimit), VariableEncoding.None);

			bool isPackageWithSizes = _cartHelper.IsPackageWithSizes(cartItem);
			fragment.AddCondition("CartItem.IsPackageWithSizes", isPackageWithSizes);
			if (isPackageWithSizes)
			{
				fragment.AddVariable("CartItem.PackageElementsHashCode", cartItem.PackageElements.GetPackageElementsHashCode().ToString());
			}

			fragment.AddRegexVariable(
				@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;
					return RenderTagGroup(tagGroupCommonName, cartItem);
				},
				VariableEncoding.None);

			var cartItemKey = new LekmerCartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
			bool itemWasUpdated = _updatedItems.ContainsKey(cartItemKey);
			fragment.AddCondition("ItemWasUpdated", itemWasUpdated);

			return fragment.Render();
		}

		/// <summary>
		/// Render quantity select.
		/// </summary>
		/// <param name="quantity">Quantity to buy.</param>
		/// <param name="quantitiesToDisplay">Quantity limit.</param>
		protected virtual string RenderQuantityOptions(int quantity, int quantitiesToDisplay)
		{
			var optionBuilder = new StringBuilder();

			for (int i = 0; i <= Math.Max(quantitiesToDisplay, quantity); i++)
			{
				Fragment fragmentOption = Template.GetFragment("QuantityOption");
				fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddCondition("IsSelected", i == quantity);
				optionBuilder.AppendLine(fragmentOption.Render());
			}

			return optionBuilder.ToString();
		}

		/// <summary>
		/// Renders the tags in a tag group.
		/// </summary>
		/// <param name="commonName">The common name of tag group.</param>
		/// <param name="cartItem">Rendered cart item.</param>
		/// <returns>Rendered tags.</returns>
		protected virtual string RenderTagGroup(string commonName, ILekmerCartItem cartItem)
		{
			if (commonName == null)
			{
				throw new ArgumentNullException("commonName");
			}

			Collection<ITagGroup> tagGroups = TagGroupService.GetAllByProduct(UserContext.Current, cartItem.Product.Id);
			ITagGroup tagGroup = tagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}

			Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("TagGroup.Title", AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName), VariableEncoding.None);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);

			return fragmentTagGroup.Render();
		}

		/// <summary>
		/// Renders a tag.
		/// </summary>
		/// <param name="tagGroup">The tag group.</param>
		/// <returns>Rendered tag.</returns>
		protected virtual string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();

			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				Fragment fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}

			return tagBuilder.ToString();
		}

		/// <summary>
		/// Add tag position conditions.
		/// </summary>
		protected virtual void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}


		/// <summary>
		/// Initialize parameters.
		/// </summary>
		protected virtual void Initialize()
		{
			if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
			{
				_maxQuantity = 10;
			}

			if (!bool.TryParse(Template.GetSettingOrNull("AllowPostback"), out _allowPostback))
			{
				_allowPostback = false;
			}

			_cartHelper = new CartHelper();
		}

		/// <summary>
		/// Check if cart is null or empty.
		/// </summary>
		protected virtual bool IsCartNullOrEmpty(out ILekmerCartFull cart, out Collection<ILekmerCartItem> cartItems)
		{
			cartItems = null;

			LekmerCartHelper.RefreshSessionCart();

			cart = CartSession.Cart as ILekmerCartFull;
			if (cart == null)
			{
				return true;
			}

			cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (cartItems.Count == 0)
			{
				return true;
			}

			return false;
		}

		/// <summary>
		/// Change quantity of cart items.
		/// </summary>
		protected virtual void ChangeQuantityOfItems(ILekmerCartFull cart)
		{
			foreach (ILekmerCartItem cartItem in cart.GetGroupedCartItemsByProductAndPriceAndSize())
			{
				string key = GetQuantityFormKey(cartItem);

				int? newQuantity = Request.Form.GetInt32OrNull(key);
				if (!newQuantity.HasValue)
				{
					continue;
				}

				if (newQuantity.Value <= 0)
				{
					//Delete item by productId and price
					cart.DeleteItem(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
				}
				else if (newQuantity.Value != cartItem.Quantity)
				{
					//Get actual quantity which need add or remove
					int quantity = newQuantity.Value - cartItem.Quantity;

					cart.ChangeItemQuantityForBlockExtendedCartControl(
						cartItem.Product,
						quantity,
						cartItem.SizeId,
						cartItem.IsAffectedByCampaign,
						cartItem.PackageElements);

					var cartItemKey = new LekmerCartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
					_updatedItems[cartItemKey] = true;
				}
			}

			CartService.Save(UserContext.Current, cart);
			LekmerCartHelper.RefreshSessionCartHard();
		}

		/// <summary>
		/// Get form key for cart item.
		/// </summary>
		protected virtual string GetQuantityFormKey(ILekmerCartItem cartItem)
		{
			string key = "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat;

			if (cartItem.SizeId.HasValue)
			{
				key = key + "-" + cartItem.SizeId;
			}

			if (cartItem.PackageElements != null && cartItem.PackageElements.Count > 0)
			{
				key = key + "-" + cartItem.PackageElements.GetPackageElementsHashCode();
			}

			return key;
		}

		/// <summary>
		/// Group cart items by product and size and package items hash code.
		/// </summary>
		protected virtual IEnumerable<IGrouping<string, ICartItem>> GroupByProductAndSizeAndHashCode(Collection<ILekmerCartItem> lekmerCartItems)
		{
			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var cartItems = new Collection<ICartItem>(lekmerCartItems.Cast<ICartItem>().ToArray());
			return cartItems.GroupBy(item => string.Format("{0}-{1}-{2}", item.Product.Id, ((ILekmerCartItem)item).SizeId, ((ILekmerCartItem)item).PackageElements.GetPackageElementsHashCode()));
		}

		/// <summary>
		/// Get cart item size.
		/// </summary>
		/// <param name="item">Cart item to get the size.</param>
		/// <returns>Cart item size.</returns>
		protected virtual IProductSize GetSize(ILekmerCartItem item)
		{
			return item.SizeId.HasValue
				? ProductSizeService.GetById(item.Product.Id, item.SizeId.Value)
				: IoC.Resolve<IProductSize>();
		}

		/// <summary>
		/// Get the quantity limit for cart item.
		/// </summary>
		/// <param name="lekmerCartItem">The cart item.</param>
		protected virtual int ObtainQuantityLimit(ILekmerCartItem lekmerCartItem)
		{
			var itemWithoutSizes = new Collection<int>();
			var itemWithSizes = new Collection<IPackageElement>();
			lekmerCartItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);
			int numberInStock = LekmerOrderService.GetAvaliableNumberInStock(UserContext.Current, lekmerCartItem.Product.Id, lekmerCartItem.SizeId, itemWithoutSizes, itemWithSizes);

			int quantityLimit = 0;
			if (numberInStock > 0) //buy
			{
				quantityLimit = Math.Min(_maxQuantity, numberInStock);
			}
			else if (numberInStock == 0 && ((ILekmerProduct)lekmerCartItem.Product).IsBookable) //book
			{
				quantityLimit = _maxQuantity;
			}

			return quantityLimit;
		}
	}
}