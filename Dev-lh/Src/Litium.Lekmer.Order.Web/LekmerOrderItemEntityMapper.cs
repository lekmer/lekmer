﻿using System.Globalization;
using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerOrderItemEntityMapper : OrderItemEntityMapper
	{
		public LekmerOrderItemEntityMapper(IFormatter formatter) : base(formatter)
		{
		}

		public override void AddEntityVariables(Fragment fragment, IOrderItem item)
		{
			//standard order item variables
			fragment.AddVariable("OrderItem.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("OrderItem.Title", item.Title);
			fragment.AddVariable("OrderItem.Quantity", Formatter.FormatNumber(Channel.Current, item.Quantity));

			//Lekmer specefic order item variables
			fragment.AddVariable("OrderItem.ErpId", item.ErpId);
			fragment.AddVariable("OrderItem.Status", AliasHelper.GetAliasValue("Order.Item.Status." + item.OrderItemStatus.CommonName), VariableEncoding.None);

			var lekmerItem = (ILekmerOrderItem) item;
			fragment.AddCondition("OrderItem.HasSize", lekmerItem.HasSize);
			if (lekmerItem.HasSize)
			{
				fragment.AddEntity(lekmerItem.Size);
			}
			fragment.AddCondition("OrderItem.HasBrand", lekmerItem.HasBrand);
			if (lekmerItem.HasBrand)
			{
				fragment.AddEntity(lekmerItem.Brand);
			}
			fragment.AddCondition("OrderItem.HasImage", lekmerItem.HasImage);
			if (lekmerItem.HasImage)
			{
				fragment.AddEntity(lekmerItem.Image);
			}
			fragment.AddCondition("OrderItem.IsDropShip", lekmerItem.IsDropShip);

			var orderItemHelper = new OrderItemHelper();
			orderItemHelper.AddProductAndWarehouseInfo(fragment, item.ProductId);
			orderItemHelper.RenderOrderItemPrices(fragment, item, (ILekmerFormatter) Formatter, Channel.Current);
		}
	}
}