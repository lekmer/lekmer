using System.Collections.Specialized;
using System.Globalization;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class ExtendedCartFreeProductForm : ControlBase
	{
		private readonly string _postUrl;

		public ExtendedCartFreeProductForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		protected NameValueCollection RequestForm
		{
			get { return Request.Form; }
		}

		protected override string PostModeName
		{
			get { return "free-product-mode"; }
		}

		protected virtual string PostModeValue
		{
			get { return "free-product-add-block-{0}"; }
		}

		public virtual string SubmitButtonName
		{
			get { return "free-product-add-submit"; }
		}

		public virtual bool IsFormPostBack(int blockId)
		{
			string postModeName = RequestForm[PostModeName];
			return IsPostBack && postModeName != null && postModeName.Equals(string.Format(CultureInfo.InvariantCulture, PostModeValue, blockId));
		}

		public virtual string GetCartItemOptionId()
		{
			return RequestForm[SubmitButtonName];
		}

		public virtual void MapToFreeProductItem(Fragment fragment, int cartItemOptionId, int blockId)
		{
			fragment.AddVariable("Form.Submit.Name", SubmitButtonName);
			fragment.AddVariable("Form.Submit.Value", cartItemOptionId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Form.Free-Product.PostUrl", _postUrl);
			fragment.AddVariable("Form.Free-Product.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Free-Product.PostMode.Value", string.Format(CultureInfo.InvariantCulture, PostModeValue, blockId));
		}
	}
}