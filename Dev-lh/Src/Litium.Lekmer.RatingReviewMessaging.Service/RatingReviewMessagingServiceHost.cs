﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using System.Timers;
using Litium.Lekmer.RatingReview;
using log4net;
using log4net.Config;
using Timer = System.Timers.Timer;

namespace Litium.Lekmer.RatingReviewMessaging.Service
{
	public partial class RatingReviewMessagingServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private Timer _timer;

		public RatingReviewMessagingServiceHost()
		{
			InitializeComponent();

			XmlConfigurator.Configure();
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			Exception serviceError = null;
			try
			{
				_log.Info("Starting Lekmer RatingReview Messaging Service.");

				RatingReviewMessengerSetting setting = new RatingReviewMessengerSetting();

				int interval = setting.TimerInterval();

				_timer = new Timer
				{
					Interval = interval * 1000 * 60 * 60,
					Enabled = true,
					AutoReset = true
				};

				_timer.Elapsed += OnTimerElapsed;
				_timer.Start();

				ProcessOrders();

				_log.Info("Lekmer RatingReview Messaging started successfuly.");
			}
			catch (Exception exception)
			{
				serviceError = new Exception("Unable to start service. See inner exception for details.", exception);
			}
			finally
			{
				if (serviceError != null)
				{
					try
					{
						_log.Error("Failed to start Lekmer RatingReview Messaging Service.", serviceError);
					}
					catch (Exception exception)
					{

						Trace.WriteLine("Failed to start service. An error occurred:");
						Trace.WriteLine(serviceError.ToString());
						Trace.WriteLine("Failed to log error:");
						Trace.WriteLine(exception.ToString());
					}
					finally
					{
						Stop();
					}
				}
			}
		}

		protected override void OnStop()
		{
			try
			{
				_timer.Stop();

				_log.Info("Lekmer RatingReview Messaging Service stopped successfuly.");

				base.OnStop();
			}
			catch (Exception exception)
			{
				_log.Error("Error occurred while stopping Lekmer RatingReview Messaging Service.", exception);
			}
		}

		private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			try
			{
				_timer.Stop();

				_log.Error("ERROR occurred during Lekmer RatingReview Messaging Service execution. Stopping service", e.ExceptionObject is Exception ? e.ExceptionObject as Exception : null);
			}
			finally
			{
				Stop();
			}
		}

		private void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			ProcessOrders();
		}

		private void ProcessOrders()
		{
			try
			{
				var setting = new RatingReviewMessengerSetting();

				var orderedProductMessengerHelper = new ReviewOrderedProductMessengerHelper(setting.DaysAfterPurchase(), setting.ReviewOrderedProductUrl());
				orderedProductMessengerHelper.ProcessFeedbackMessages();
			}
			catch (Exception exception)
			{
				_log.Error("Failed to send email to rate ordered products.", exception);
			}
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}
