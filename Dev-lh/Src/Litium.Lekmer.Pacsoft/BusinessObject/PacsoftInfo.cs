using System;
using Litium.Lekmer.Pacsoft.Contract;

namespace Litium.Lekmer.Pacsoft
{
	[Serializable]
	public class PacsoftInfo : IPacsoftInfo
	{
		public int OrderId { get; set; }
		public int QuickId { get; set; }
		public string Name { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string Email { get; set; }
		public string Phone { get; set; }
		public string Sms { get; set; }
		public int ItemsCount { get; set; }
	}
}