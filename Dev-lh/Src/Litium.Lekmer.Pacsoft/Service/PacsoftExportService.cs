﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.DropShip;
using Litium.Lekmer.Order;
using Litium.Lekmer.Pacsoft.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Pacsoft
{
	public class PacsoftExportService : IPacsoftExportService
	{
		protected PacsoftExportRepository Repository { get; private set; }
		protected IPacsoftExportSetting Setting { get; private set; }
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ICountryService CountryService { get; private set; }
		protected ILekmerOrderService OrderService { get; private set; }
		protected IDropShipService DropShipService { get; private set; }

		private Collection<ICountry> _countries;
		private IEnumerable<ICountry> Countries
		{
			get { return _countries ?? (_countries = CountryService.GetAll()); }
		}

		public PacsoftExportService(
			PacsoftExportRepository repository,
			IPacsoftExportSetting pacsoftExportSetting,
			ILekmerChannelService channelService,
			ICountryService countryService,
			IOrderService orderService,
			IDropShipService dropShipService)
		{
			Repository = repository;
			Setting = pacsoftExportSetting;
			ChannelService = channelService;
			CountryService = countryService;
			OrderService = (ILekmerOrderService)orderService;
			DropShipService = dropShipService;
		}

		public IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			var channels = ChannelService.GetAll().Where(c => Setting.GetChannelNameReplacementExists(c.CommonName));

			return channels.Select(channel =>
			{
				var context = IoC.Resolve<IUserContext>();
				context.Channel = channel;
				return context;
			}).ToList();
		}

		public string GetFilePath(string channelCommonName, int orderid)
		{
			string dirr = Setting.DestinationDirectory;
			string channelName = Setting.GetChannelNameReplacement(channelCommonName);

			string fileName = string.Format(Setting.FileName, orderid, DateTime.Now.Date.ToString("yyyyMMdd"));
			fileName = fileName.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, fileName);
		}

		public Collection<IPacsoftInfo> GetPacsoftInfoCollectionToExport(IUserContext context)
		{
			PopulatePacsoftOrders(context, Setting.ProductErpId, Setting.SupplierId, Setting.DaysAfterPurchase);

			var pacsoftInfoCollection = new Collection<IPacsoftInfo>();
			var orders = OrderService.GetToSendPacsoftInfo(context);
			foreach (IOrderFull order in orders)
			{
				if (order == null) continue;

				var dropShipInfo = GetDropShipInfo(order.Id);
				if (dropShipInfo == null) continue;

				var pacsoftInfo = CreatePacsoftInfo(order, dropShipInfo);
				if (pacsoftInfo != null)
				{
					pacsoftInfoCollection.Add(pacsoftInfo);
				}
			}
			return pacsoftInfoCollection;
		}

		public virtual void PopulatePacsoftOrders(IUserContext context, string productHyId, string supplierId, int daysAfterPurchase)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productHyId == null) throw new ArgumentNullException("productHyId");

			Repository.PopulatePacsoftOrders(context.Channel.Id, productHyId, supplierId, daysAfterPurchase);
		}

		public void UpdatePacsoftOrder(int orderId)
		{
			Repository.Update(orderId);
		}

		protected virtual IDropShipInfo GetDropShipInfo(int orderId)
		{
			var dropShipInfoCollection = DropShipService.GetAllByOrderAndSupplier(orderId, Setting.SupplierId);
			return dropShipInfoCollection != null && dropShipInfoCollection.Count > 0 ? dropShipInfoCollection[0] : null;
		}

		protected virtual IPacsoftInfo CreatePacsoftInfo(IOrderFull order, IDropShipInfo dropShipInfo)
		{
			IPacsoftInfo pacsoftInfo = CreatePacsoftInfoDefault(order);
			bool isOrderDataExists = order.Customer != null
									&& order.Customer.CustomerInformation != null
									&& order.Customer.CustomerInformation.DefaultDeliveryAddress != null;
			if (isOrderDataExists)
			{
				pacsoftInfo = PopulateWithOrderData(pacsoftInfo, order);
			}

			if (dropShipInfo != null)
			{
				if (!string.IsNullOrEmpty(dropShipInfo.DeliveryFirstName) || !string.IsNullOrEmpty(dropShipInfo.DeliveryLastName))
				{
					pacsoftInfo.Name = dropShipInfo.DeliveryFirstName + " " + dropShipInfo.DeliveryLastName;
				}
				else if (!string.IsNullOrEmpty(dropShipInfo.BillingFirstName) || !string.IsNullOrEmpty(dropShipInfo.BillingLastName))
				{
					pacsoftInfo.Name = dropShipInfo.BillingFirstName + " " + dropShipInfo.BillingLastName;
				}
				pacsoftInfo.Name = pacsoftInfo.Name.Trim();
				pacsoftInfo.Address1 = SetField(pacsoftInfo.Address1, dropShipInfo.DeliveryStreet, dropShipInfo.BillingStreet);
				pacsoftInfo.PostalCode = SetField(pacsoftInfo.PostalCode, dropShipInfo.DeliveryZip, dropShipInfo.BillingZip);
				pacsoftInfo.City = SetField(pacsoftInfo.City, dropShipInfo.DeliveryCity, dropShipInfo.BillingCity);
				pacsoftInfo.Country = SetField(pacsoftInfo.Country, dropShipInfo.DeliveryCountry, dropShipInfo.BillingCountry);
				pacsoftInfo.Email = SetField(pacsoftInfo.Email, dropShipInfo.Email, null);
				pacsoftInfo.Phone = SetField(pacsoftInfo.Phone, dropShipInfo.Phone, null);
				pacsoftInfo.Sms = pacsoftInfo.Phone;
			}

			return pacsoftInfo;
		}

		protected virtual IPacsoftInfo CreatePacsoftInfoDefault(IOrderFull order)
		{
			var pacsoftInfo = IoC.Resolve<IPacsoftInfo>();
			pacsoftInfo.OrderId = order.Id;
			pacsoftInfo.QuickId = order.CustomerId;
			pacsoftInfo.ItemsCount = GetItemsCount(order);
			return pacsoftInfo;
		}
		protected virtual IPacsoftInfo PopulateWithOrderData(IPacsoftInfo pacsoftInfo, IOrderFull order)
		{
			var customer = order.Customer.CustomerInformation;
			var address = order.Customer.CustomerInformation.DefaultDeliveryAddress;
			var country = Countries.FirstOrDefault(c => c.Id == address.CountryId);

			pacsoftInfo.Name = ((ILekmerCustomerInformation)customer).IsCompany ? customer.FirstName : customer.FirstName + " " + customer.LastName;
			pacsoftInfo.Address1 = address.StreetAddress;
			pacsoftInfo.Address2 = address.StreetAddress2;
			pacsoftInfo.PostalCode = address.PostalCode;
			pacsoftInfo.City = address.City;
			pacsoftInfo.Country = country != null ? country.Iso : string.Empty;
			pacsoftInfo.Email = order.Email;
			pacsoftInfo.Phone = customer.CellPhoneNumber;
			pacsoftInfo.Sms = customer.CellPhoneNumber;
			return pacsoftInfo;
		}

		protected virtual string SetField(string field, string value1, string value2)
		{
			if (!string.IsNullOrEmpty(value1))
			{
				field = value1;
			}
			else if (value2 != null && !string.IsNullOrEmpty(value2))
			{
				field = value2;
			}
			return field;
		}

		protected virtual int GetItemsCount(IOrderFull order)
		{
			var orderItems = order.GetOrderItems();
			var itemsCount = orderItems.Where(i => i.ErpId == Setting.ProductErpId).Sum(i => i.Quantity);
			foreach (var orderItem in orderItems)
			{
				var lekmerOrderItem = (ILekmerOrderItem)orderItem;
				if (ProductExtensions.IsProduct(lekmerOrderItem.ProductTypeId)) continue;

				itemsCount += lekmerOrderItem.PackageOrderItems.Where(i => i.ErpId == Setting.ProductErpId).Sum(i => i.Quantity);
			}

			return itemsCount;
		}
	}
}