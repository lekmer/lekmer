﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Cache.Cleaner.Service
{
	public class CacheCleanerScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "Cache"; }
		}

		protected override string GroupName
		{
			get { return "CacheCleanerJob"; }
		}
	}
}