﻿using System.Globalization;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.SiteStructure.Web
{
	public class LekmerNavigationControl : NavigationControl
	{
		private int _maxLinkTextLength;

		protected virtual int? MaxLinkTextLength
		{
			get { return Parameters.GetInt32OrNull("MaxLinkTextLength"); }
		}

		public LekmerNavigationControl(ITemplateFactory templateFactory, IContentNodeService contentNodeService, ICustomerSession customerSession)
			: base(templateFactory, contentNodeService, customerSession)
		{
		}

		public override ComponentContent Render()
		{
			Initialize();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Setting.MaxLinkTextLength", _maxLinkTextLength.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("ContentNodeList", RenderNodeList(), VariableEncoding.None);

			return new ComponentContent(
				Template.GetFragment("Head").Render(),
				fragmentContent.Render());
		}

		protected override string RenderNode(IContentNodeTreeItem treeItem)
		{
			Fragment fragmentNode = Template.GetFragment("ContentNode");

			string url = treeItem.Url;
			if (!string.IsNullOrEmpty(url))
			{
				url = UrlHelper.ResolveUrl(treeItem.Url);
			}
			IContentNode contentNode = treeItem.ContentNode;
			var title = _maxLinkTextLength > 0 && contentNode.Title.Length > _maxLinkTextLength ? contentNode.Title.Substring(0, _maxLinkTextLength) + "... " : contentNode.Title;

			fragmentNode.AddVariable("ContentNode.Url", url);
			fragmentNode.AddVariable("ContentNode.Id", contentNode.Id.ToString(CultureInfo.InvariantCulture));
			fragmentNode.AddVariable("ContentNode.Title", title);
			fragmentNode.AddVariable("ContentNode.CommonName", contentNode.CommonName);
			fragmentNode.AddCondition("IsActive", IsActive(contentNode.Id));
			fragmentNode.AddCondition("IsChildActive", IsChildActive(contentNode.Id));

			return fragmentNode.Render();
		}

		protected virtual void Initialize()
		{
			if (MaxLinkTextLength.HasValue)
			{
				_maxLinkTextLength = MaxLinkTextLength.Value;
			}
			else if (!int.TryParse(Template.GetSettingOrNull("MaxLinkTextLength"), out _maxLinkTextLength))
			{
				_maxLinkTextLength = 0;
			}
		}
	}
}