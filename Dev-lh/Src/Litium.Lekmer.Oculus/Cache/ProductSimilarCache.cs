﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Oculus
{
	public sealed class ProductSimilarCache : ScensumCacheBase<ProductSimilarKey, IProductSimilar>
	{
		private static readonly ProductSimilarCache _instance = new ProductSimilarCache();

		private ProductSimilarCache()
		{
		}

		public static ProductSimilarCache Instance
		{
			get { return _instance; }
		}
	}

	public class ProductSimilarKey : ICacheKey
	{
		public ProductSimilarKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}