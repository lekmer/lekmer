﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Oculus
{
	[Serializable]
	public class ProductSimilar : IProductSimilar
	{
		private Collection<ISimilarProductItem> _similarProductCollection;

		public int ProductId { get; set; }

		public string ProductErpId { get; set; }

		public Collection<ISimilarProductItem> SimilarProductCollection
		{
			get { return _similarProductCollection ?? (_similarProductCollection = new Collection<ISimilarProductItem>()); }
			set { _similarProductCollection = value; }
		}
	}
}