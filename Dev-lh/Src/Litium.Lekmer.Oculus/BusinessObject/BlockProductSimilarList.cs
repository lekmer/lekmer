﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Oculus
{
	[Serializable]
	public class BlockProductSimilarList : BlockBase, IBlockProductSimilarList
	{
		private int _priceRangeType;
		private IBlockSetting _setting; 

		public int PriceRangeType
		{
			get { return _priceRangeType; }
			set
			{
				CheckChanged(_priceRangeType, value);
				_priceRangeType = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}