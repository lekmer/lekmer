﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Oculus.Repository
{
	public class ProductSimilarRepository
	{
		private static readonly char DEFAULT_SEPARATOR = ';';

		protected virtual DataMapperBase<ISimilarProductItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISimilarProductItem>(dataReader);
		}

		public virtual IProductSimilar GetAllByProductId(int productId)
		{
			Collection<ISimilarProductItem> similarProductItems;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			DatabaseSetting dbSettings = new DatabaseSetting("ProductSimilarRepository.GetAllByProductId");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSimilarGetAllByProductId]", parameters, dbSettings))
			{
				similarProductItems = CreateDataMapper(dataReader).ReadMultipleRows();
			}

			IProductSimilar productSimilar = IoC.Resolve<IProductSimilar>();
			productSimilar.ProductId = productId;
			productSimilar.SimilarProductCollection = similarProductItems;

			return productSimilar;
		}

		public virtual IProductSimilar Save(IProductSimilar productSimilar)
		{
			if (productSimilar == null) throw new ArgumentNullException("productSimilar");

			string productSimilarIdList = Convert.ToStringIdentifierList(productSimilar.SimilarProductCollection.Select(obj => obj.ProductErpId), DEFAULT_SEPARATOR);
			string scoreList = Convert.ToStringIdentifierList(productSimilar.SimilarProductCollection.Select(obj => obj.Score), DEFAULT_SEPARATOR);

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("HYProductErpId", productSimilar.ProductErpId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ProductSimilarIdList", productSimilarIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ScoreList", scoreList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Separator", DEFAULT_SEPARATOR, SqlDbType.Char),
				};

			DatabaseSetting dbSettings = new DatabaseSetting("ProductSimilarRepository.Save");
			productSimilar.ProductId = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pProductSimilarSave]", parameters, dbSettings);

			return productSimilar;
		}
	}
}