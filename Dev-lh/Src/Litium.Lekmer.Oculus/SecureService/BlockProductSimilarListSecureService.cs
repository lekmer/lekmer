using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Oculus.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Oculus
{
	public class BlockProductSimilarListSecureService : IBlockProductSimilarListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductSimilarListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockProductSimilarListSecureService(
			IAccessValidator accessValidator,
			BlockProductSimilarListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockProductSimilarList Create()
		{
			if (AccessSecureService == null)
			{
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			}

			IBlockProductSimilarList blockProductSimilarList = IoC.Resolve<IBlockProductSimilarList>();

			blockProductSimilarList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductSimilarList.Setting = BlockSettingSecureService.Create();
			blockProductSimilarList.Status = BusinessObjectStatus.New;

			return blockProductSimilarList;
		}

		public virtual IBlockProductSimilarList GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockProductSimilarList blockTopList = Create();

			blockTopList.ContentNodeId = contentNodeId;
			blockTopList.ContentAreaId = contentAreaId;
			blockTopList.BlockTypeId = blockTypeId;
			blockTopList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockTopList.Title = title;
			blockTopList.TemplateId = null;
			blockTopList.Id = Save(systemUserFull, blockTopList);

			return blockTopList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductSimilarList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			BlockProductSimilarListCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());
			
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}

			BlockProductSimilarListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}