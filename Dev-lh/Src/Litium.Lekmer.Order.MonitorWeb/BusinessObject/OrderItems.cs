﻿using System;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Litium.Lekmer.Order.MonitorWeb.BusinessObject
{
    public class OrderItems
    {
        public int OrderId { get; set; }
        public DateTime Time { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        [ScriptIgnore]
        public int ChannelId { get; set; }
        public string ApplicationName { get; set; }
        public decimal Total { get; set; }
	    public List<OrderItem> Items { get; set; }
    }

    public class OrderItem
    {
	    public int Id { get; set; }
	    public string Name { get; set; }
	    public decimal Price { get; set; }
	    public string ImageUrl { get; set; }
		public string ProductUrl { get; set; }
	    public int Quantity { get; set; }


    }

}