﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Order.MonitorWeb
{
	public class OrderMonitorSetting : SettingBase
	{
		protected override string StorageName
		{
			get { return "OrderMonitor"; }
		}

		protected virtual string GroupName
		{
			get { return "OrderMonitorSettings"; }
		}


		// Properties

		public int MinutesAfterPurchase
		{
			get { return GetInt32(GroupName, "MinutesAfterPurchase", 1); }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}

		public string OrderFileName
		{
			get { return GetString(GroupName, "OrderFileName"); }
		}

		public string TotalFileName
		{
			get { return GetString(GroupName, "TotalFileName"); }
		}
	}
}