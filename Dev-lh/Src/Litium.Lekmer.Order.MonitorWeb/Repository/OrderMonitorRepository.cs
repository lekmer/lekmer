using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Lekmer.Order.MonitorConsole;
using Litium.Lekmer.Order.MonitorWeb.BusinessObject;

namespace Litium.Lekmer.Order.MonitorWeb
{
	public class OrderMonitorRepository
	{
		public Collection<OrderStatistic> GetOrderStatistics(DateTime startDate, DateTime endDate)
		{
			var statistics = new Collection<OrderStatistic>();

			IDataParameter[] parameters = new IDataParameter[]
            {
                ParameterHelper.CreateParameter("DateFrom", startDate, SqlDbType.DateTime),
				ParameterHelper.CreateParameter("DateTo", endDate, SqlDbType.DateTime)
            };
			var dbSettings = new DatabaseSetting("OrderMonitorRepository.GetOrderStatistics");
			IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pReportLekmerDailyOrdersResultset]", parameters, dbSettings);
			while (dataReader.Read())
			{
				var orderStatistic = new OrderStatistic
					{
						Date = DateTime.Parse(dataReader["Date"].ToString()),
						NumberOfOrders = (int)dataReader["NumberOfOrders"],
						AverageOrderPrice = (decimal)dataReader["AverageOrderPrice"],
						TotalOrderValue = (decimal)dataReader["TotalOrderValue"],
						Channel = dataReader["Channel"].ToString()
					};

				statistics.Add(orderStatistic);
			}
			dataReader.Dispose();

			return statistics;
		}

		public Collection<OrderItems> GetOrders(DateTime date, int minutesAfterPurchase, bool withImages)
		{

			var result = new Collection<OrderItems>();
			IDataParameter[] parameters = new[]
                {
                    ParameterHelper.CreateParameter("Date", date, SqlDbType.DateTime),
                    ParameterHelper.CreateParameter("MinAfterPurchase", minutesAfterPurchase, SqlDbType.Int),
                    ParameterHelper.CreateParameter("GetImages", withImages, SqlDbType.Bit)
                };

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetOrders");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[lekmer].[pLekmerOrderMonitorNew]", parameters, dbSettings))
			{

				while (reader.Read())
				{
					var oredrItem = new OrderItems()
					{
						OrderId = (int)reader["OrderId"],
						Time = DateTime.Parse(reader["Time"].ToString()),
						City = reader["City"].ToString(),
						ApplicationName = reader["ApplicationName"].ToString(),
						Country = reader["Country"].ToString(),
						ChannelId = (int)reader["ChannelId"],
						Total = (decimal)reader["Total"],
					};
					result.Add(oredrItem);
				}
			}

			return result;
		}
	}
}