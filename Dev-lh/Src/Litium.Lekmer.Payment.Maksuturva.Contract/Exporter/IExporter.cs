﻿namespace Litium.Lekmer.Payment.Maksuturva.Contract
{
	public interface IExporter
	{
		void Execute();
	}
}