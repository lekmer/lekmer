﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public interface IMaksuturvaPendingOrderService
	{
		IMaksuturvaPendingOrder Create();
		int Insert(IMaksuturvaPendingOrder maksuturvaPendingOrder);
		void Update(IMaksuturvaPendingOrder maksuturvaPendingOrder);
		void Delete(int maksuturvaPendingOrderId);
		IMaksuturvaPendingOrder Get(int orderId);
		Collection<IMaksuturvaPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkFromDate);
	}
}