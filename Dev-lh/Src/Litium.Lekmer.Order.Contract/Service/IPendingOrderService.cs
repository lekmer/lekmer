﻿namespace Litium.Lekmer.Order
{
	public interface IPendingOrderService
	{
		void CheckKlarnaOrders();
		void CheckMaksuturvaOrders();
		void CheckQliroOrders();
	}
}