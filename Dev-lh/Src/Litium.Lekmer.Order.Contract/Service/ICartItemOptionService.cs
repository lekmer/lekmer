﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Contract;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface ICartItemOptionService
	{
		ICartItemOption Create();
		void Save(ICartItemOption cartItem);
		void Delete(int cartItemOptionId);
		Collection<ICartItemOption> GetAllByCart(IUserContext context, int cartId);
	}
}