using System.Collections.ObjectModel;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderItemService : IOrderItemService
	{
		Collection<IOrderItem> GetAllByOrderWithoutBrandAndTags(int orderId);
	}
}