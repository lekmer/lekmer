﻿namespace Litium.Lekmer.Order
{
	public interface IOrderVoucherInfoService
	{
		IOrderVoucherInfo Create();
		IOrderVoucherInfo GetByOrderId(int orderId);
		void Save(int orderId, IOrderVoucherInfo orderVoucherInfo);
		void UpdateStatus(int orderId, IOrderVoucherInfo orderVoucherInfo);
	}
}