﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface ITradeDoubleProductGroupService
	{
		ITradeDoubleProductGroup GetProductGroupMapping(IChannel channel, int productId);
	}
}
