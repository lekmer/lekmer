using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderService : IOrderService
	{
		int SaveFullExtended(IUserContext context, IOrderFull order);

		void UpdateNumberInStock(IUserContext context, IOrderFull order);
		bool CheckNumberInStock(IUserContext context, IOrderFull order);
		bool CheckNumberInStock(IUserContext context, ICartFull cart);
		bool CheckNumberInStock(IUserContext context, ICartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems);
		int GetAvaliableNumberInStock(IUserContext context, int productId, int? sizeId, Collection<int> itemWithoutSizes, Collection<IPackageElement> itemWithSizes);
		int GetAvaliableNumberInStock(ILekmerProduct product, int? sizeId, Collection<int> itemWithoutSizes, Collection<IPackageElement> itemWithSizes);

		void SetFeedbackToken(int orderId, Guid feedbackToken);
		IOrderFull GetByFeedbackToken(IUserContext context, Guid feedbackToken);
		IOrderFull GetByKcoId(IUserContext context, string kcoId);
		IOrderFull GetFullByKcoId(IUserContext context, string kcoId);
		Collection<IOrderFull> GetLatest(DateTime date);
		Collection<IOrder> GetToSendInsuranceInfo(IUserContext context, int maxDaysAfterPurchase, int minDaysAfterPurchase);
		Collection<IOrder> GetToSendGfkStatistics(IUserContext context, string startDate, string endDate);
		Collection<IOrder> GetToSendPacsoftInfo(IUserContext context);
		Collection<IOrder> GetOrders(IUserContext context, DateTime date, int minutesAfterPurchase);
		Collection<IOrderFull> GetOrdersByEmailAndDate(IUserContext context, string email, DateTime date);
		Collection<IOrderFull> GetOrdersByEmail(IUserContext context, string email);
		void UpdateSendInsuranceInfoFlag(List<int> orderIds);
		void SendRejectMail(IUserContext context, IOrderFull order);
		void UpdateOrderStatus(int orderId, int orderStatusId);

		void UpdateVoucherStatus(IOrderFull order);
		void OrderVoucherRelease(IUserContext context, ILekmerOrderFull order);
		void OrderVoucherConsume(IUserContext context, ILekmerOrderFull order);
	}
}