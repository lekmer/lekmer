﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface IDeliveryMethodWeightPriceService
	{
		Collection<IDeliveryMethodWeightPrice> GetAllByDeliveryMethod(IUserContext context, int deliveryMethodId);
	}
}