using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public interface IOrderVoucherInfo : IBusinessObjectBase
	{
		string VoucherCode { get; set; }
		int? DiscountType { get; set; }
		/// <summary>
		/// Original discount value that comes from Voucher. Be aware, it could be amount or percentage value, or something else....
		/// </summary>
		decimal? DiscountValue { get; set; }
		/// <summary>
		/// Discount amount, which was applied in cart.
		/// </summary>
		decimal? AmountUsed { get; set; }
		/// <summary>
		/// Discount amount, with is still available after voucher was used in cart.
		/// </summary>
		decimal? AmountLeft { get; set; }
		/// <summary>
		/// Specifies if voucher was consumed.
		/// </summary>
		int VoucherStatus { get; set; }
	}
}