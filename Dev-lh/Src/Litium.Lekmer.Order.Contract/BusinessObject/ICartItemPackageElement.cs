using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public interface ICartItemPackageElement : IBusinessObjectBase
	{
		int Id { get; set; }
		int CartItemId { get; set; }
		int ProductId { get; set; }
		int? SizeId { get; set; }
	}
}