﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public interface ISubPaymentType : IBusinessObjectBase
	{
		int Id { get; set; }
		int PaymentId { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		string Code { get; set; }
	}
}