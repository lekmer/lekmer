﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderSecureService : IOrderSecureService
	{
		bool CanChangePaymentCost(IOrderFull order, decimal previousPaymentCost);

		Collection<IOrder> Search(ILekmerOrderSearchCriteria searchCriteria, int page, int pageSize, out int itemsCount);

		int Save(
			ISystemUserFull systemUserFull,
			ILekmerOrderFull order,
			Collection<IOrderComment> orderComments,
			IOrderAddress billingAddress,
			IOrderAddress deliveryAddress,
			IOrderAddress alternateAddress,
			Collection<IOrderAudit> orderAudits,
			Collection<IOrderPayment> orderPayments);

		void SaveSplit(
			ISystemUserFull systemUserFull,
			ILekmerOrderFull order,
			Collection<IOrderComment> orderComments,
			IOrderAddress billingAddress,
			IOrderAddress deliveryAddress,
			IOrderAddress alternateAddress,
			Collection<IOrderAudit> orderAudits,
			Collection<IOrderPayment> orderPayments,
			ILekmerOrderFull splitOrder,
			Collection<IOrderComment> splitOrderComments,
			IOrderAddress splitOrderBillingAddress,
			IOrderAddress splitOrderDeliveryAddress,
			IOrderAddress splitOrderAlternateAddress,
			Collection<IOrderAudit> splitOrderAudits,
			Collection<IOrderPayment> splitOrderPayments);

		Collection<IOrderFull> GetKlarnaCheckoutOrders(int days, string orderStatus);

		void Delete(ISystemUserFull systemUserFull, IOrderFull order);
	}
}