﻿using System;

namespace Litium.Lekmer.Order
{
	[Flags]
	public enum OrderItemsType
	{
		None = 0x0,
		Regular = 0x1,
		Diapers = 0x2
	}
}
