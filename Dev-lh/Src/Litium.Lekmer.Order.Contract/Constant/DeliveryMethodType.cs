﻿namespace Litium.Lekmer.Order
{
	public enum DeliveryMethodType
	{
		/// <summary>
		/// Posten
		/// </summary>
		Post = 1,
		/// <summary>
		/// Schenker
		/// </summary>
		Schenker = 1000001,
		/// <summary>
		/// Posten Företagspaket
		/// </summary>
		BusinessParcel = 1000002,
		/// <summary>
		/// PostpaketFöretag
		/// </summary>
		PostCompany = 1000003,
		/// <summary>
		/// Doorstep delivery
		/// </summary>
		DoorstepDelivery = 1000004,
		/// <summary>
		/// Doorstep diapers delivery
		/// </summary>
		DoorstepDiapers = 1000005
	}
}
