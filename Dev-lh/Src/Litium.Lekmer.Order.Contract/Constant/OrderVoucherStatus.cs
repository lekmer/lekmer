using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public enum OrderVoucherStatus
	{
		Unknown = 0,
		Reserved = 1,
		Released = 2,
		Consumed = 3
	}
}