﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroGetPaymentTypesResponse : IQliroResult
	{
		paymentType[] PaymentTypes { get; set; }
	}
}
