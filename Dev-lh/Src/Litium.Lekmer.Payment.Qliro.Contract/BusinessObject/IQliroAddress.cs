using Litium.Scensum.Customer;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroAddress : IAddress
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		bool IsCompany { get; set; }
		string CompanyName { get; set; }
	}
}