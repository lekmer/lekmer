﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroPaymentTypeService
	{
		Collection<IQliroPaymentType> GetAllByChannel(IChannel channel);
	}
}
