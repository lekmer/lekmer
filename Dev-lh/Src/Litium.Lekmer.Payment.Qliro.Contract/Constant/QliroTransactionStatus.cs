﻿namespace Litium.Lekmer.Payment.Qliro
{
	public enum QliroTransactionStatus
	{
		Unknown = 0,
		Ok = 1,
		TimeoutResponse = 2,
		UnspecifiedError = 3
	}
}
