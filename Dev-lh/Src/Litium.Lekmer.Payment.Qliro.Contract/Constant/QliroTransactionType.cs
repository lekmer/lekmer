﻿namespace Litium.Lekmer.Payment.Qliro
{
	public enum QliroTransactionType
	{
		None = 0,
		GetAddress = 1,
		ReserveAmount = 2,
		ReserveAmountCompany = 3,
		CancelReservation = 4,
		CheckOrderStatus = 5,
		GetPaymentTypes = 6,
		ReserveAmountTimeout = 7
	}
}
