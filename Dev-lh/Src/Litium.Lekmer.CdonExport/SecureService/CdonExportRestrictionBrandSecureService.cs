﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionBrandSecureService : ICdonExportRestrictionBrandSecureService
	{
		private const char DELIMITER = ',';

		protected CdonExportRestrictionBrandRepository Repository { get; private set; }

		public CdonExportRestrictionBrandSecureService(CdonExportRestrictionBrandRepository repository)
		{
			Repository = repository;
		}

		public void InsertIncludeBrand(ICdonExportRestrictionItem brand, List<int> productChannelIds)
		{
			Repository.InsertIncludeBrand(brand, Scensum.Foundation.Convert.ToStringIdentifierList(productChannelIds), DELIMITER);
		}

		public void InsertRestrictionBrand(ICdonExportRestrictionItem brand, List<int> productChannelIds)
		{
			Repository.InsertRestrictionBrand(brand, Scensum.Foundation.Convert.ToStringIdentifierList(productChannelIds), DELIMITER);
		}

		public void DeleteIncludeBrands(List<int> brandIds)
		{
			Repository.DeleteIncludeBrands(Scensum.Foundation.Convert.ToStringIdentifierList(brandIds), DELIMITER);
		}

		public void DeleteRestrictionBrands(List<int> brandIds)
		{
			Repository.DeleteRestrictionBrands(Scensum.Foundation.Convert.ToStringIdentifierList(brandIds), DELIMITER);
		}

		public Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			return Repository.GetIncludeAll();
		}

		public Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			return Repository.GetRestrictionAll();
		}
	}
}