﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionSecureService : ICdonExportRestrictionSecureService
	{
		protected CdonExportRestrictionRepository CdonExportRestrictionRepository { get; private set; }
		protected ICdonExportRestrictionBrandSecureService CdonExportRestrictionBrandSecureService { get; private set; }
		protected ICdonExportRestrictionCategorySecureService CdonExportRestrictionCategorySecureService { get; private set; }
		protected ICdonExportRestrictionProductSecureService CdonExportRestrictionProductSecureService { get; private set; }

		public CdonExportRestrictionSecureService(
			CdonExportRestrictionRepository cdonExportRestrictionRepository,
			ICdonExportRestrictionBrandSecureService cdonExportRestrictionBrandSecureService,
			ICdonExportRestrictionCategorySecureService cdonExportRestrictionCategorySecureService,
			ICdonExportRestrictionProductSecureService cdonExportRestrictionProductSecureService)
		{
			CdonExportRestrictionRepository = cdonExportRestrictionRepository;
			CdonExportRestrictionBrandSecureService = cdonExportRestrictionBrandSecureService;
			CdonExportRestrictionCategorySecureService = cdonExportRestrictionCategorySecureService;
			CdonExportRestrictionProductSecureService = cdonExportRestrictionProductSecureService;
		}

		public virtual ICdonExportRestriction GetAll()
		{
			var cdonExportRestriction = IoC.Resolve<ICdonExportRestriction>();

			cdonExportRestriction.ExcludeBrands = CdonExportRestrictionBrandSecureService.GetRestrictionAll();
			cdonExportRestriction.IncludeBrands = CdonExportRestrictionBrandSecureService.GetIncludeAll();
			cdonExportRestriction.ExcludeCategories = CdonExportRestrictionCategorySecureService.GetRestrictionAll();
			cdonExportRestriction.IncludeCategories = CdonExportRestrictionCategorySecureService.GetIncludeAll();
			cdonExportRestriction.ExcludeProducts = CdonExportRestrictionProductSecureService.GetRestrictionAll();
			cdonExportRestriction.IncludeProducts = CdonExportRestrictionProductSecureService.GetIncludeAll();

			return cdonExportRestriction;
		}

		public virtual void DeleteAll()
		{
			CdonExportRestrictionRepository.DeleteAll();
		}

		public virtual Dictionary<int, int> GetRestrictedProductsCount()
		{
			return CdonExportRestrictionRepository.GetRestrictedProductsCount();
		}

		public virtual void SaveAll(
			Collection<ICdonExportRestrictionItem> includeProducts, Collection<ICdonExportRestrictionItem> restrictionProducts,
			Collection<ICdonExportRestrictionItem> includeCategories, Collection<ICdonExportRestrictionItem> restrictionCategories,
			Collection<ICdonExportRestrictionItem> includeBrands, Collection<ICdonExportRestrictionItem> restrictionBrands)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				// DELETE.
				DeleteAll();

				// Insert Products.
				foreach (var item in includeProducts)
				{
					CdonExportRestrictionProductSecureService.InsertIncludeProduct(item, item.Channels.Keys.ToList());
				}
				foreach (var item in restrictionProducts)
				{
					CdonExportRestrictionProductSecureService.InsertRestrictionProduct(item, item.Channels.Keys.ToList());
				}

				// Insert Categories.
				foreach (var item in includeCategories)
				{
					CdonExportRestrictionCategorySecureService.InsertIncludeCategory(item, item.Channels.Keys.ToList());
				}
				foreach (var item in restrictionCategories)
				{
					CdonExportRestrictionCategorySecureService.InsertRestrictionCategory(item, item.Channels.Keys.ToList());
				}

				// Insert Brands.
				foreach (var item in includeBrands)
				{
					CdonExportRestrictionBrandSecureService.InsertIncludeBrand(item, item.Channels.Keys.ToList());
				}
				foreach (var item in restrictionBrands)
				{
					CdonExportRestrictionBrandSecureService.InsertRestrictionBrand(item, item.Channels.Keys.ToList());
				}

				transactedOperation.Complete();
			}
		}
	}
}