﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Mapper
{
	public class CdonExportRestrictionItemDataMapper : DataMapperBase<ICdonExportRestrictionItem>
	{
		public CdonExportRestrictionItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICdonExportRestrictionItem Create()
		{
			var cdonExportRestrictionBrand = IoC.Resolve<ICdonExportRestrictionItem>();

			cdonExportRestrictionBrand.ProductRegistryId = MapValue<int>("ProductRegistryId");
			cdonExportRestrictionBrand.ItemId = MapValue<int>("ItemId");
			cdonExportRestrictionBrand.Reason = MapNullableValue<string>("Reason");
			cdonExportRestrictionBrand.UserId = MapNullableValue<int?>("UserId");
			cdonExportRestrictionBrand.CreatedDate = MapValue<DateTime>("CreatedDate");
			cdonExportRestrictionBrand.ChannelId = MapValue<int>("ChannelId");

			return cdonExportRestrictionBrand;
		}
	}
}