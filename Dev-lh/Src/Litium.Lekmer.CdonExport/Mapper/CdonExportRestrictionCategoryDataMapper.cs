﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Mapper
{
	public class CdonExportRestrictionCategoryDataMapper : DataMapperBase<ICdonExportRestrictionCategory>
	{
		public CdonExportRestrictionCategoryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICdonExportRestrictionCategory Create()
		{
			var cdonExportRestrictionCategory = IoC.Resolve<ICdonExportRestrictionCategory>();

			cdonExportRestrictionCategory.ProductRegistryId = MapValue<int>("ProductRegistryId");
			cdonExportRestrictionCategory.CategoryId = MapValue<int>("CategoryId");
			cdonExportRestrictionCategory.RestrictionReason = MapNullableValue<string>("RestrictionReason");
			cdonExportRestrictionCategory.UserId = MapNullableValue<int?>("UserId");
			cdonExportRestrictionCategory.CreatedDate = MapValue<DateTime>("CreatedDate");
			cdonExportRestrictionCategory.ChannelId = MapValue<int>("ChannelId");

			return cdonExportRestrictionCategory;
		}
	}
}