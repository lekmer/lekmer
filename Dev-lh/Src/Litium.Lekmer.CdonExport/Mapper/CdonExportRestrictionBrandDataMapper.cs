﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Mapper
{
	public class CdonExportRestrictionBrandDataMapper : DataMapperBase<ICdonExportRestrictionBrand>
	{
		public CdonExportRestrictionBrandDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICdonExportRestrictionBrand Create()
		{
			var cdonExportRestrictionBrand = IoC.Resolve<ICdonExportRestrictionBrand>();

			cdonExportRestrictionBrand.ProductRegistryId = MapValue<int>("ProductRegistryId");
			cdonExportRestrictionBrand.BrandId = MapValue<int>("BrandId");
			cdonExportRestrictionBrand.RestrictionReason = MapNullableValue<string>("RestrictionReason");
			cdonExportRestrictionBrand.UserId = MapNullableValue<int?>("UserId");
			cdonExportRestrictionBrand.CreatedDate = MapValue<DateTime>("CreatedDate");
			cdonExportRestrictionBrand.ChannelId = MapValue<int>("ChannelId");

			return cdonExportRestrictionBrand;
		}
	}
}