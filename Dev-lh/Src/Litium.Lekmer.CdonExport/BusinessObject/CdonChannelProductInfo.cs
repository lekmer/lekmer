using System;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonChannelProductInfo : ICdonChannelProductInfo
	{
		public IChannel Channel { get; set; }
		public ILekmerProductView Product { get; set; }
		public IColor Color { get; set; }
	}
}