using System;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonTag : ICdonTag
	{
		public int Id { get; set; }
		public int CdonTagId { get; set; }
		public string Value { get; set; }
	}
}