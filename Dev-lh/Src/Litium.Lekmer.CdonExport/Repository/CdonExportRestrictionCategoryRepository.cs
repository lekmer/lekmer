﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionCategoryRepository
	{
		protected virtual DataMapperBase<ICdonExportRestrictionItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICdonExportRestrictionItem>(dataReader);
		}

		// Categories.

		public virtual void InsertIncludeCategory(ICdonExportRestrictionItem category, string productChannelIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductChannelIds", productChannelIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CategoryId", category.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Reason", category.Reason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", category.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", category.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.InsertIncludeCategory");

			new DataHandler().ExecuteCommandWithReturnValue("[export].[pCdonExportIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertRestrictionCategory(ICdonExportRestrictionItem category, string productChannelIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductChannelIds", productChannelIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CategoryId", category.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Reason", category.Reason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", category.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", category.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.InsertRestrictionCategory");

			new DataHandler().ExecuteCommandWithReturnValue("[export].[pCdonExportRestrictionCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(string categoryIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryIds", categoryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.DeleteIncludeCategories");

			new DataHandler().ExecuteCommand("[export].[pCdonExportIncludeCategoryDeleteAllByCategory]", parameters, dbSettings);
		}

		public virtual void DeleteRestrictionCategories(string categoryIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryIds", categoryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.DeleteRestrictionCategories");

			new DataHandler().ExecuteCommand("[export].[pCdonExportRestrictionCategoryDeleteAllByCategory]", parameters, dbSettings);
		}

		public virtual Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.GetIncludeAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportIncludeCategoryGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.GetRestrictionAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportRestrictionCategoryGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ICdonExportRestrictionItem> GetIncludeAllSecure()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.GetIncludeAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportIncludeCategoryGetAllSecure]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ICdonExportRestrictionItem> GetRestrictionAllSecure()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionCategoryRepository.GetRestrictionAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportRestrictionCategoryGetAllSecure]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}