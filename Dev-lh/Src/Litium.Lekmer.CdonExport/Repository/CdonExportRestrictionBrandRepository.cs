﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionBrandRepository
	{
		protected virtual DataMapperBase<ICdonExportRestrictionItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICdonExportRestrictionItem>(dataReader);
		}

		// Brands.

		public virtual void InsertIncludeBrand(ICdonExportRestrictionItem brand, string productChannelIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductChannelIds", productChannelIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandId", brand.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Reason", brand.Reason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", brand.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", brand.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionBrandRepository.InsertIncludeBrand");

			new DataHandler().ExecuteCommandWithReturnValue("[export].[pCdonExportIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertRestrictionBrand(ICdonExportRestrictionItem brand, string productChannelIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductChannelIds", productChannelIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandId", brand.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Reason", brand.Reason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", brand.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", brand.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionBrandRepository.InsertRestrictionBrand");

			new DataHandler().ExecuteCommandWithReturnValue("[export].[pCdonExportRestrictionBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(string brandIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandIds", brandIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionBrandRepository.DeleteIncludeBrands");

			new DataHandler().ExecuteCommand("[export].[pCdonExportIncludeBrandDeleteAllByBrand]", parameters, dbSettings);
		}

		public virtual void DeleteRestrictionBrands(string brandIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandIds", brandIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CdonExportRestrictionBrandRepository.DeleteRestrictionBrands");

			new DataHandler().ExecuteCommand("[export].[pCdonExportRestrictionBrandDeleteAllByBrand]", parameters, dbSettings);
		}

		public virtual Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionBrandRepository.GetIncludeAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportIncludeBrandGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			var dbSettings = new DatabaseSetting("CdonExportRestrictionBrandRepository.GetRestrictionAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportRestrictionBrandGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}