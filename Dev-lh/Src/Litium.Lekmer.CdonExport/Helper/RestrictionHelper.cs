﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Product;

namespace Litium.Lekmer.CdonExport
{
	public class RestrictionHelper
	{
		private const string CategoryMissing = "***Missing***";


		private ICdonExportSetting _cdonExportSetting;
		private ICdonExportService _cdonExportService;

		private readonly Dictionary<int, int> _incorrectCategories = new Dictionary<int, int>(); // Dictionary<categoryId, categoryId>


		private Dictionary<int, Collection<int>> _cdonRestrictedBrands; // Dictionary<brandId, Collection<channelId>>
		protected Dictionary<int, Collection<int>> CdonRestrictedBrands
		{
			get { return _cdonRestrictedBrands ?? (_cdonRestrictedBrands = _cdonExportService.GetRestrictedBrands()); }
		}

		private Dictionary<int, Collection<int>> _cdonRestrictedCategories; // Dictionary<categoryId, Collection<channelId>>
		protected Dictionary<int, Collection<int>> CdonRestrictedCategories
		{
			get { return _cdonRestrictedCategories ?? (_cdonRestrictedCategories = _cdonExportService.GetRestrictedCategories()); }
		}

		private Dictionary<int, Collection<int>> _cdonRestrictedProducts; // Dictionary<productId, Collection<channelId>>
		protected Dictionary<int, Collection<int>> CdonRestrictedProducts
		{
			get { return _cdonRestrictedProducts ?? (_cdonRestrictedProducts = _cdonExportService.GetRestrictedProducts()); }
		}

		private Dictionary<int, Collection<int>> _cdonIncludeBrands; // Dictionary<brandId, Collection<channelId>>
		protected Dictionary<int, Collection<int>> CdonIncludeBrands
		{
			get { return _cdonIncludeBrands ?? (_cdonIncludeBrands = _cdonExportService.GetIncludeBrands()); }
		}

		private Dictionary<int, Collection<int>> _cdonIncludeCategories; // Dictionary<categoryId, Collection<channelId>>
		protected Dictionary<int, Collection<int>> CdonIncludeCategories
		{
			get { return _cdonIncludeCategories ?? (_cdonIncludeCategories = _cdonExportService.GetIncludeCategories()); }
		}

		private Dictionary<int, Collection<int>> _cdonIncludeProducts; // Dictionary<productId, Collection<channelId>>
		protected Dictionary<int, Collection<int>> CdonIncludeProducts
		{
			get { return _cdonIncludeProducts ?? (_cdonIncludeProducts = _cdonExportService.GetIncludeProducts()); }
		}

		private Dictionary<int, Collection<IProductRegistryRestrictionItem>> _heppoRestrictedBrands;
		protected Dictionary<int, Collection<IProductRegistryRestrictionItem>> HeppoRestrictedBrands
		{
			get { return _heppoRestrictedBrands ?? (_heppoRestrictedBrands = _cdonExportService.GetHeppoRestrictedBrands()); }
		}

		private Dictionary<int, Collection<IProductRegistryRestrictionItem>> _heppoRestrictedCategories;
		protected Dictionary<int, Collection<IProductRegistryRestrictionItem>> HeppoRestrictedCategories
		{
			get { return _heppoRestrictedCategories ?? (_heppoRestrictedCategories = _cdonExportService.GetHeppoRestrictedCategories()); }
		}

		private Dictionary<int, Collection<IProductRegistryRestrictionItem>> _heppoRestrictedProducts;
		protected Dictionary<int, Collection<IProductRegistryRestrictionItem>> HeppoRestrictedProducts
		{
			get { return _heppoRestrictedProducts ?? (_heppoRestrictedProducts = _cdonExportService.GetHeppoRestrictedProducts()); }
		}

		private Collection<string> _notShoesCategories;
		protected Collection<string> NotShoesCategories
		{
			get { return _notShoesCategories ?? (_notShoesCategories = new Collection<string>(_cdonExportSetting.NotShoesCategoryIds.Split(','))); }
		}

		protected bool IsHeppo
		{
			get { return _cdonExportSetting.ApplicationName == CdonExportXmlNodes.HeppoApplication; }
		}


		public RestrictionHelper(ICdonExportSetting cdonExportSetting)
		{
			_cdonExportSetting = cdonExportSetting;
		}

		public RestrictionHelper(ICdonExportSetting cdonExportSetting, ICdonExportService cdonExportService)
		{
			_cdonExportSetting = cdonExportSetting;
			_cdonExportService = cdonExportService;
		}


		// 1. (Not DELETED and ONLINE) OR (not DELETED and OFFLINE but purchased in last N month)
		public virtual int GetMonthPurchasedAgoValue()
		{
			return _cdonExportSetting.MonthPurchasedAgo;
		}

		// 2. Without sizes or with EU sizes
		public virtual Collection<IProductSize> GetProductSizesAllByProduct(Collection<IProductSize> sizes, out bool needToRemove)
		{
			needToRemove = true;
			var productSizes = new Dictionary<int, IProductSize>();

			if (sizes.Count <= 0)
			{
				needToRemove = false;
				return new Collection<IProductSize>(productSizes.Values.ToArray());
			}

			foreach (var size in sizes)
			{
				var erpTitle = size.SizeInfo.ErpTitle;
				if (erpTitle != null && !productSizes.ContainsKey(size.SizeInfo.Id))
				{
					if (!IsHeppo)
					{
						productSizes.Add(size.SizeInfo.Id, size);
						needToRemove = false;
					}
					else
					{
						if (erpTitle.StartsWith("UKM-") || erpTitle.StartsWith("UKW-") || erpTitle.StartsWith("USM-") || erpTitle.StartsWith("USW-")) continue;

						productSizes.Add(size.SizeInfo.Id, size);
						needToRemove = false;
					}
				}
			}

			return new Collection<IProductSize>(productSizes.Values.ToArray());
		}

		// 3. Not restricted in CDON
		protected virtual bool IsCdonRestricted(int productId, int categoryId, int? brandId, int channelId)
		{
			bool isRestricted = false;

			if (CdonRestrictedProducts.ContainsKey(productId) && CdonRestrictedProducts[productId].Contains(channelId))
			{
				isRestricted = true;
			}

			if (brandId.HasValue && CdonRestrictedBrands.ContainsKey(brandId.Value) && CdonRestrictedBrands[brandId.Value].Contains(channelId))
			{
				isRestricted = true;
			}

			if (CdonRestrictedCategories.ContainsKey(categoryId) && CdonRestrictedCategories[categoryId].Contains(channelId))
			{
				isRestricted = true;
			}

			if (CdonIncludeProducts.ContainsKey(productId) && CdonIncludeProducts[productId].Contains(channelId))
			{
				isRestricted = false;
			}

			if (brandId.HasValue && CdonIncludeBrands.ContainsKey(brandId.Value) && CdonIncludeBrands[brandId.Value].Contains(channelId))
			{
				isRestricted = false;
			}

			if (CdonIncludeCategories.ContainsKey(categoryId) && CdonIncludeCategories[categoryId].Contains(channelId))
			{
				isRestricted = false;
			}

			return isRestricted;
		}

		// 4. Filter if shoes or not shoes but with one size.
		protected virtual bool IsAllowed(int categoryId, Collection<IProductSize> productSizes, ICategoryTree categoryTree)
		{
			bool allowed = true;

			if (categoryTree != null)
			{
				ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(categoryId);
				while (categoryTreeItem != null && categoryTreeItem.Category != null)
				{
					if (!NotShoesCategories.Contains(categoryTreeItem.Category.Id.ToString(CultureInfo.InvariantCulture)))
					{
						categoryTreeItem = categoryTreeItem.Parent;
						continue;
					}

					if (productSizes != null && productSizes.Count != 0)
					{
						allowed = false;
					}

					break;
				}
			}

			return allowed;
		}

		// 3 AND 4 restrictons
		public virtual Collection<ICdonProductInfo> FilterProducts(Collection<ICdonProductInfo> products, ICategoryTree categoryTree)
		{
			var filteredProducts = new Collection<ICdonProductInfo>();

			foreach (var product in products)
			{
				if (IsDropShip(product.DefaultProductInfo)) continue;

				foreach (var channelProduct in product.ChannelProductInfoCollection)
				{
					bool isCdonRestricted = IsCdonRestricted(channelProduct.Product.Id, channelProduct.Product.CategoryId, channelProduct.Product.BrandId, channelProduct.Channel.Id);
					if (isCdonRestricted) continue;

					if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.HeppoApplication)
					{
						//bool isAllowed = IsAllowed(product.DefaultProductInfo.Product.CategoryId, product.ProductSizesCollection, categoryTree);
						//if (isAllowed)
						//{
							filteredProducts.Add(product);
							break;
						//}
					}
					else if (_cdonExportSetting.ApplicationName == CdonExportXmlNodes.LekmerApplication)
					{
						filteredProducts.Add(product);
						break;
					}
				}
			}

			return filteredProducts;
		}

		// 5. Not 'MISSING' category
		public virtual bool PopulateIncorrectCategory(ICategoryTreeItem treeItem)
		{
			if (treeItem.Category.Title == CategoryMissing)
			{
				AddToIncorrectCategories(treeItem.Category.Id);

				AddCategoryChildsToIncorrectCategories(treeItem);

				return false;
			}

			return true;
		}
		protected virtual void AddCategoryChildsToIncorrectCategories(ICategoryTreeItem treeItem)
		{
			foreach (var childCategory in treeItem.Children)
			{
				AddToIncorrectCategories(childCategory.Id);

				AddCategoryChildsToIncorrectCategories(childCategory);
			}
		}
		protected virtual void AddToIncorrectCategories(int categoryId)
		{
			if (!_incorrectCategories.ContainsKey(categoryId))
			{
				_incorrectCategories.Add(categoryId, categoryId);
			}
		}
		public virtual bool IsCorrectCategory(int categoryId)
		{
			return _incorrectCategories.ContainsKey(categoryId);
		}

		// 7. Not restricted in HEPPO
		public virtual bool IsHeppoRestricted(int productId, int categoryId, int? brandId, int channelId)
		{
			if (HeppoRestrictedProducts.ContainsKey(productId)
				&& HeppoRestrictedProducts[productId].FirstOrDefault(p => p.ItemId == productId && p.ChannelId == channelId) != null)
			{
				return true;
			}

			if (brandId.HasValue
				&& HeppoRestrictedBrands.ContainsKey(brandId.Value)
				&& HeppoRestrictedBrands[brandId.Value].FirstOrDefault(b => b.ItemId == brandId.Value && b.ChannelId == channelId) != null)
			{
				return true;
			}

			if (HeppoRestrictedCategories.ContainsKey(categoryId)
				&& HeppoRestrictedCategories[categoryId].FirstOrDefault(c => c.ItemId == categoryId && c.ChannelId == channelId) != null)
			{
				return true;
			}

			return false;
		}

		// 8. Not drop ship
		protected virtual bool IsDropShip(ICdonChannelProductInfo productInfo)
		{
			return productInfo.Product.IsDropShip;
		}
	}
}