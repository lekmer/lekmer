﻿using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionBrandService : ICdonExportRestrictionBrandService
	{
		protected CdonExportRestrictionBrandRepository Repository { get; private set; }

		public CdonExportRestrictionBrandService(CdonExportRestrictionBrandRepository repository)
		{
			Repository = repository;
		}

		public Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			return Repository.GetIncludeAll();
		}

		public Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			return Repository.GetRestrictionAll();
		}
	}
}