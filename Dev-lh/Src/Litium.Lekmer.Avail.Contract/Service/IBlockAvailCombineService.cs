﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Avail
{
	public interface IBlockAvailCombineService
	{
		IBlockAvailCombine GetById(IUserContext context, int id);
	}
}
