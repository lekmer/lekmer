﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core
{
	public interface IChannelFormatInfo : IBusinessObjectBase
	{
		int ChannelId { get; set; }
		string TimeFormat { get; set; }
		string WeekDayFormat { get; set; }
		string DayFormat { get; set; }
		string DateTimeFormat { get; set; }
		int TimeZoneDiff { get; set; }
	}
}
