﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomer : ICustomer
	{
		IFacebookUser FacebookUser { get; set; }
		CustomerLoginType LoginType { get; set; }
	}
}