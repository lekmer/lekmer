﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface IProductActionAppliedItem : IBusinessObjectBase
	{
		int Id { get; set; }
		int ProductCampaignId { get; set; }
		string ActionCommonName { get; set; }

		IProductActionAppliedItem Cast(IProductAction productAction);
	}
}