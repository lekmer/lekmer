﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerUser : IUser
	{
		bool IsAlternateLogin { get; set; }
	}
}