﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Contract
{
	[Serializable]
	public class IdDictionary : Dictionary<int, int>
	{
		public IdDictionary()
		{
		}

		public IdDictionary(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		public IdDictionary(IEnumerable<int> ids)
		{
			foreach (int id in ids)
			{
				Add(id);
			}
		}

		public void Add(int id)
		{
			Add(id, id);
		}
	}
}