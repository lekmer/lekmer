﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public interface IFacebookUser : IBusinessObjectBase
	{
		int CustomerId { get; set; }
		int CustomerRegistryId { get; set; }
		string FacebookId { get; set; }
		string Name { get; set; }
		string Email { get; set; }
		DateTime CreatedDate { get; set; }
		bool IsAlternateLogin { get; set; }
	}
}