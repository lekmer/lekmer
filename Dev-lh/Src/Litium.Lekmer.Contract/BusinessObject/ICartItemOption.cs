using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Contract
{
	public interface ICartItemOption : IBusinessObjectBase
	{
		int Id { get; set; }
		int CartId { get; set; }
		IProduct Product { get; set; }
		int? SizeId { get; set; }
		int Quantity { get; set; }
		DateTime CreatedDate { get; set; }

		Price GetActualPrice();
		Price GetActualPriceSummary();
	}
}