﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerAddress : IAddress
	{
		string HouseNumber { get; set; }
		string HouseExtension { get; set; }
		string Reference { get; set; }
		string DoorCode { get; set; }
	}
}