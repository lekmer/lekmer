﻿using System;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using Incordia.Enterprise.CustomerService.WebService;
using Litium.Lekmer.ServiceCenter.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.ServiceCenter.Web
{
	public class BlockViewCaseControl:BlockControlBase<IBlock>
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IBlockService _blockService;
		private readonly IServiceCenterService _serviceCenterService;
		private readonly IFormatter _formatter;
		private Fragment _fragmentContent;

		public BlockViewCaseControl(ITemplateFactory factory, IBlockService blockService, IServiceCenterService serviceCenterService, IFormatter formatter)
			: base(factory)
		{
			_blockService = blockService;
			_serviceCenterService = serviceCenterService;
			_formatter = formatter;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			_fragmentContent = Template.GetFragment("Content");

			var ticket = Request.QueryString["ticket"];
			if (ticket == null)
			{
				throw new HttpException(404, "Not found");
			}

			try
			{
				//Get case.
				var customerCase = _serviceCenterService.GetCaseByGuid(ticket, Channel.Current.ApplicationName);
				if (customerCase == null)
				{
					throw new HttpException(404, "Not found");
				}

				ValidationResult validationResult = null;
				var queryBuilder = new QueryBuilder(Request.QueryString);

				if (IsPostBack && PostMode.Equals("customercase-reply"))
				{
					string replyMessage = Request.Form["Form.Message"];

					if (!string.IsNullOrEmpty(replyMessage))
					{
						var shortTicket = customerCase.Ticket;
						_serviceCenterService.AddMessageToCase(shortTicket, replyMessage);

						queryBuilder.ReplaceOrAdd("ticket", ticket);
						Response.Redirect("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);
					}
					else
					{
						validationResult = new ValidationResult();
						validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.ReplyMessageNotProvided"));
					}
				}

				string validationError = null;
				if (validationResult != null && !validationResult.IsValid)
				{
					var validationControl = new ValidationControl(validationResult.Errors);
					validationError = validationControl.Render();
				}

				string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);

				_fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
				_fragmentContent.AddVariable("Case.MessageList", RenderMessageList(customerCase), VariableEncoding.None);
				_fragmentContent.AddVariable("Form.PostUrl", postUrl);
				_fragmentContent.AddVariable("Case.Ticket", customerCase.Ticket);
				_fragmentContent.AddVariable("Case.CreationDate", _formatter.FormatDateTime(Channel.Current, customerCase.CreationDate));
				_fragmentContent.AddVariable("Case.Status", customerCase.Status);
				_fragmentContent.AddCondition("is_error", false);
			}
			catch (ThreadAbortException)
			{
				throw;
			}
			catch (Exception exception)
			{
				_log.Error("Error Occured while block render.", exception);
				RenderErrorFragment();
			}
			
			_fragmentContent.AddEntity(Block);

			string content = _fragmentContent.Render();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, content, footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private void RenderErrorFragment()
		{
			_fragmentContent.AddCondition("is_error", true);

			Fragment fragmentError = Template.GetFragment("Error");
			_fragmentContent.AddVariable("Error", fragmentError.Render());
		}

		private string RenderMessageList(CSMCustomerCaseDetails customerCase)
		{
			StringBuilder messageBuilder = new StringBuilder();

			foreach (CSMCaseMessage message in customerCase.Messages)
			{
				Fragment fragmentMessage = Template.GetFragment("Message");
				fragmentMessage.AddVariable("Case.Message.CreationDate", _formatter.FormatDateTime(Channel.Current, message.CreationDate));
				fragmentMessage.AddVariable("Case.ReplyMessage.Text", HttpUtility.HtmlEncode(message.Message).Replace("\n", "<br />").Replace("\r", ""),VariableEncoding.None);
				fragmentMessage.AddVariable("Case.ReplyMessage.Type", message.Type);
				fragmentMessage.AddCondition("Case.ReplyMessage.IsQuestion", message.IsQuestion);

				messageBuilder.AppendLine(fragmentMessage.Render());
			}

			return messageBuilder.ToString();
		}
	}
}