﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using Incordia.Enterprise.CustomerService.WebService;
using Litium.Lekmer.ServiceCenter.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Lekmer.ServiceCenter.Web
{
	public class BlockAddCaseControl : BlockControlBase<IBlock>
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IBlockService _blockService;
		private readonly IServiceCenterService _serviceCenterService;
		private List<CSMCategory> _categoryList;
		private CaseForm _caseForm;
		private Fragment _fragmentContent;
		private bool _isError;

		private const int cDISPLAY_PRIVATE = 2;
		private const int cDISPLAY_PUBLIC = 4;

		public BlockAddCaseControl(ITemplateFactory factory, IBlockService blockService, IServiceCenterService serviceCenterService)
			: base(factory)
		{
			_blockService = blockService;
			_serviceCenterService = serviceCenterService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			_fragmentContent = Template.GetFragment("Content");

			ValidationResult validationResult = null;

			if (Request.QueryString["ticket"] != null)
			{
				_fragmentContent.AddCondition("is_confirmed", true);
			}
			else if (CaseForm.IsFormPostBack)
			{
				CaseForm.MapFromRequest();
				validationResult = CaseForm.Validate();

				var categoryId = ConvertToCategoryId(CaseForm.CategoryId);

				if (validationResult.IsValid && categoryId != null)
				{
					bool isSaved = false;
					string code = string.Empty;

					try
					{
						code = _serviceCenterService.SaveCase(
							null,
							(int)categoryId,
							CaseForm.FirstName,
							CaseForm.LastName,
							CaseForm.Email,
							CaseForm.CustomerId,
							CaseForm.OrderNumber,
							string.Empty,
							string.Empty,
							CaseForm.Message,
							Channel.Current.Id,
							Channel.Current.ApplicationName
						);

						isSaved = true;
					}
					catch (Exception ex)
					{
						_log.Error("Error occurred while saving case. ", ex);
						_isError = true;
					}

					if (isSaved)
					{
						RenderCaseSavedResponse(code);
					}
				}
				_fragmentContent.AddCondition("is_confirmed", false);
			}
			else
			{
				_fragmentContent.AddCondition("is_confirmed", false);
			}

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, RenderCaseForm(validationResult), footer);
		}

		private List<CSMCategory> CategoryList
		{
			get
			{
				try
				{
					if (_categoryList == null)
					{
						const int display = cDISPLAY_PUBLIC + cDISPLAY_PRIVATE;
						_categoryList = _serviceCenterService.GetCSMCategories(display, Channel.Current.ApplicationName);
					}
				}
				catch (Exception exception)
				{
					_log.Error(exception.Message);
					_categoryList = new List<CSMCategory>();

					_isError = true;
				}

				return _categoryList;
			}
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private void RenderCaseSavedResponse(string code)
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			queryBuilder.ReplaceOrAdd("ticket", code);

			Response.Redirect("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);
		}

		private string RenderCaseForm(ValidationResult validationResult)
		{
			string validationError = null;
			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			CaseForm.MapToFragment(_fragmentContent);
			_fragmentContent.AddVariable("Form.CategoryOptionsList", RenderOptionList(ConvertToCategoryId(CaseForm.CategoryId)), VariableEncoding.None);
			_fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);

			if (_isError)
			{
				_fragmentContent.AddCondition("is_error", true);
				Fragment fragmentError = Template.GetFragment("Error");
				_fragmentContent.AddVariable("Error", fragmentError.Render());
			}
			else
			{
				_fragmentContent.AddCondition("is_error", false);
			}

			_fragmentContent.AddEntity(Block);

			return _fragmentContent.Render();
		}

		private string RenderOptionList(int? selectedCategoryId)
		{
			StringBuilder rows = new StringBuilder();

			foreach (CSMCategory category in CategoryList)
			{
				Fragment fragmentOption = Template.GetFragment("Option");
				fragmentOption.AddVariable("value", category.CategoryID.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddVariable("text", category.Caption);

				if (selectedCategoryId.HasValue && selectedCategoryId.Value == category.CategoryID)
				{
					fragmentOption.AddCondition("is_selected", true);
				}
				else
				{
					fragmentOption.AddCondition("is_selected", false);
				}

				rows.AppendLine(fragmentOption.Render());
			}

			return rows.ToString();
		}

		private int? ConvertToCategoryId(string input)
		{
			int intCategoryId;
			if (!int.TryParse(input, out intCategoryId))
			{
				return null;
			}

			bool categoryExists = CategoryList.Exists(c => c.CategoryID == intCategoryId);
			if (!categoryExists)
			{
				return null;
			}

			return intCategoryId;
		}

		private CaseForm CaseForm
		{
			get
			{
				if (_caseForm == null)
				{
					var queryBuilder = new QueryBuilder(Request.QueryString);

					string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);
					_caseForm = new CaseForm(postUrl);
				}
				return _caseForm;
			}
		}
	}
}