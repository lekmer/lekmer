﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Litium.Scensum.CartContainsCondition
{
	[Serializable]
	public class CategoryDictionary : Dictionary<int, bool>
	{
		public CategoryDictionary() { }

        protected CategoryDictionary(SerializationInfo info, StreamingContext context) : base(info, context) {}

		public CategoryDictionary(IEnumerable<int> categoryIds)
		{
			foreach (int categoryId in categoryIds)
			{
				Add(categoryId, false);
			}
		}
	}
}
