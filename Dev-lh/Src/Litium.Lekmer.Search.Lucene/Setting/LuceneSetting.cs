using Litium.Framework.Setting;

namespace Litium.Lekmer.Search.Lucene.Setting
{
	public class LuceneSetting : SettingBase
	{
		#region Singleton

		private static readonly LuceneSetting _instance = new LuceneSetting();

		private LuceneSetting()
		{
		}

		public static LuceneSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "LuceneSetting";

		protected override string StorageName
		{
			get { return "LuceneSetting"; }
		}

		public float MinimumSimilarity
		{
			get { return GetSingle(_groupName, "MinimumSimilarity", 0.5f); }
		}
		public int MinimumPrefixLength
		{
			get { return GetInt32(_groupName, "MinimumPrefixLength", 2); }
		}
	}
}