﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign.Web
{
	/// <summary>
	/// Control for editing a customer.
	/// </summary>
	public class LekmerCartCampaignControl : CartCampaignControl
	{
		protected ILekmerProductCampaignService LekmerProductCampaignService { get; private set; }

		/// <summary>
		/// Initializes the <see cref="LekmerCartCampaignControl"/>.
		/// </summary>
		public LekmerCartCampaignControl(
			ITemplateFactory templateFactory,
			ICartSession cartSession,
			ICartCampaignService cartCampaignService,
			IProductCampaignService productCampaignService,
			IFormatter formatter)
			: base(templateFactory, cartSession, cartCampaignService, productCampaignService, formatter)
		{
			LekmerProductCampaignService = (ILekmerProductCampaignService) productCampaignService;
		}

		protected override Collection<IProductCampaign> GetAppliedProductCampaigns(ICartFull cart)
		{
			if (cart == null) return new Collection<IProductCampaign>();

			return LekmerProductCampaignService.GetDistinctCampaigns(UserContext.Current, GetAppliedProductCampaignActions(cart));
		}
		protected new virtual Collection<IProductActionAppliedItem> GetAppliedProductCampaignActions(ICartFull cart)
		{
			if (cart == null) throw new ArgumentNullException("cart");

			var actions = new Collection<IProductActionAppliedItem>();

			foreach (ICartItem cartItem in cart.GetCartItems())
			{
				var productCampaignInfo = (ILekmerProductCampaignInfo) cartItem.Product.CampaignInfo;
				if (productCampaignInfo == null)
				{
					continue;
				}

				Collection<IProductActionAppliedItem> productActions = productCampaignInfo.CampaignActionsApplied;
				if (productActions == null || productActions.Count == 0)
				{
					continue;
				}

				foreach (var action in productActions)
				{
					if (actions.FirstOrDefault(a => a.Id == action.Id) != null)
					{
						continue;
					}
					actions.Add(action);
				}
			}

			return actions;
		}
	}
}