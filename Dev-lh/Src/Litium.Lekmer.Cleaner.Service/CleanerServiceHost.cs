﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using Litium.Lekmer.Common.Job;
using log4net;

namespace Litium.Lekmer.Cleaner.Service
{
	public partial class CleanerServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private object _syncToken = new object();
		private Collection<IJob> _jobs = new Collection<IJob>();

		public CleanerServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			try
			{
				foreach (IJob job in _jobs)
				{
					job.StopJob();
				}

				_log.Info("Service stopped successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Error occurred while stopping service.", ex);
			}
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var cartCleanerJob = new CartCleanerJob { ScheduleSetting = new CartCleanerScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(cartCleanerJob);

			var urlHistoryCleanerJob = new UrlHistoryCleanerJob { ScheduleSetting = new UrlHistoryScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(urlHistoryCleanerJob);
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}
