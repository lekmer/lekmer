﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Cleaner.Service
{
	public abstract class BaseCleanerScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "Cleaner"; }
		}
	}
}