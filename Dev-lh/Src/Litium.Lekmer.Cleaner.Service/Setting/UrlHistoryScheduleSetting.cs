﻿namespace Litium.Lekmer.Cleaner.Service
{
	public class UrlHistoryScheduleSetting : BaseCleanerScheduleSetting
	{
		protected override string GroupName
		{
			get { return "UrlHistoryCleanerJob"; }
		}

		public int UrlHistoryMaxNumber
		{
			get { return GetInt32(GroupName, "UrlHistoryMaxNumber", 5); }
		}
	}
}