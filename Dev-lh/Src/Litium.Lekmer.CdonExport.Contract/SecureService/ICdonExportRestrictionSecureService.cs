﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionSecureService
	{
		ICdonExportRestriction GetAll();
		void DeleteAll();
		Dictionary<int, int> GetRestrictedProductsCount();

		void SaveAll(
				Collection<ICdonExportRestrictionItem> includeProducts, Collection<ICdonExportRestrictionItem> restrictionProducts,
				Collection<ICdonExportRestrictionItem> includeCategories, Collection<ICdonExportRestrictionItem> restrictionCategories,
				Collection<ICdonExportRestrictionItem> includeBrands, Collection<ICdonExportRestrictionItem> restrictionBrands
			);
	}
}