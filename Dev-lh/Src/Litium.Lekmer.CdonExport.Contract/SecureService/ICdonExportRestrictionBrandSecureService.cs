﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionBrandSecureService
	{
		void InsertIncludeBrand(ICdonExportRestrictionItem brand, List<int> productChannelIds);

		void InsertRestrictionBrand(ICdonExportRestrictionItem brand, List<int> productChannelIds);

		void DeleteIncludeBrands(List<int> brandIds);

		void DeleteRestrictionBrands(List<int> brandIds);

		Collection<ICdonExportRestrictionItem> GetIncludeAll();

		Collection<ICdonExportRestrictionItem> GetRestrictionAll();
	}
}