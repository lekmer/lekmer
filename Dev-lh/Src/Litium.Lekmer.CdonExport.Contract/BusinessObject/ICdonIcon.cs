namespace Litium.Lekmer.CdonExport
{
	public interface ICdonIcon
	{
		int Id { get; set; }
		int CdonIconId { get; set; }
		string Title { get; set; }
	}
}