using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.CdonExport
{
	public interface ICdonProductInfo
	{
		int ProductId { get; set; }
		string VariantErpId { get; set; }
		bool Available { get; }

		ICdonChannelProductInfo DefaultProductInfo { get; }

		Collection<ICdonChannelProductInfo> ChannelProductInfoCollection { get; }

		Collection<IProductImageGroupFull> ProductImagesCollection { get; }

		Collection<ICdonIcon> ProductIconsCollection { get; }

		Collection<IProductSize> ProductSizesCollection { get; }

		Dictionary<int, ICdonProductInfo> ProductVariantsCollection { get; }

		void Add(IChannel channel, ILekmerProductView product);
	}
}