﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.DropShip
{
	public interface IDropShipSecureService
	{
		Collection<IDropShipInfo> GetAllGroupByOrder();
		Collection<IDropShipInfo> GetAllByOrder(int orderId);
	}
}