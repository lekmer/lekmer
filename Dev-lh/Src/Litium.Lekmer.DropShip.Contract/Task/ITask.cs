﻿namespace Litium.Lekmer.DropShip
{
	public interface ITask
	{
		void Execute();
	}
}