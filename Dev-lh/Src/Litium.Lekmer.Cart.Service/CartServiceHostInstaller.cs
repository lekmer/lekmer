﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Litium.Lekmer.Cart.Service
{
	[RunInstaller(true)]
	public partial class CartServiceHostInstaller : Installer
	{
		public CartServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}