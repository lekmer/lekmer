﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Payment.Klarna
{
	[Serializable]
	// Important: This attribute is NOT inherited from Exception, and MUST be specified 
	// otherwise serialization will fail with a SerializationException stating that
	// "Type X in Assembly Y is not marked as serializable."
	public class KlarnaUnspecifiedException : KlarnaExceptionCustom
	{
		public KlarnaUnspecifiedException()
		{
		}

		public KlarnaUnspecifiedException(string message)
			: base(message)
		{
		}

		public KlarnaUnspecifiedException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		// Without this constructor, deserialization will fail
		protected KlarnaUnspecifiedException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
