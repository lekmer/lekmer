﻿namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaResult
	{
		int KlarnaTransactionId { get; set; }
		int ResponseCode { get; set; }
		int FaultCode { get; set; }
		string FaultReason { get; set; }
		long Duration { get; set; }
		string ResponseContent { get; set; }
	}
}
