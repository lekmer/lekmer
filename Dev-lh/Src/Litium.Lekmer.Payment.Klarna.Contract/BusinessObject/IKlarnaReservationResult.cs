﻿namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaReservationResult : IKlarnaResult
	{
		string ReservationId { get; set; }
	}
}
