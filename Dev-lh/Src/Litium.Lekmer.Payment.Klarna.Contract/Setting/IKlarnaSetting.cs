﻿using System.Collections.Generic;

namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public interface IKlarnaSetting
	{
		string SecretKey { get; }
		string Url { get; }
		int Mode { get; }
		int Timeout { get; }
		int EID { get; }
		bool HandleTimeoutResponse { get; }
		string PCStorage { get; }
		string PCURI { get; }
		IEnumerable<ITimeOutData> GetTimeoutSteps();
		void Initialize(string channelCommonName);
	}
}