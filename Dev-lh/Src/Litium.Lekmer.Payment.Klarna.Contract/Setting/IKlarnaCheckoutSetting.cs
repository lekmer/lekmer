﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public interface IKlarnaCheckoutSetting : IKlarnaSetting
	{
		string ContentType { get; }
		string CheckoutUrl { get; }
		string ConfirmationUrl { get; }
		string PushUrl { get; }
		string TermsUrl { get; }
	}
}