﻿namespace Litium.Lekmer.Payment.Klarna
{
	public enum KlarnaTransactionType
	{
		None = 0,
		GetAddress = 1,
		ReserveAmount = 2,
		ReserveAmountCompany = 3,
		CancelReservation = 4,
		CheckOrderStatus = 5
	}
}
