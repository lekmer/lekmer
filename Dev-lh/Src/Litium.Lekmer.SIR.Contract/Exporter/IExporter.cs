﻿namespace Litium.Lekmer.SIR
{
	public interface IExporter
	{
		void Execute();
	}
}