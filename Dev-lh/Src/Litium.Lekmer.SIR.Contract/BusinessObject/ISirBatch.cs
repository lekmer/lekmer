using System;

namespace Litium.Lekmer.SIR
{
	public interface ISirBatch
	{
		int Id { get; set; }
		DateTime? ChangesFromDate { get; set; }
		DateTime StartedDate { get; set; }
		DateTime? CompletedDate { get; set; }
	}
}