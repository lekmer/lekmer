namespace Litium.Lekmer.SIR
{
	public interface IProductStructure
	{
		string Trade { get; set; }
		string TradeDescription { get; set; }
		string UpperProductGroup { get; set; }
		string UpperProductGroupDescription { get; set; }
		string ProductGroup { get; set; }
		string ProductGroupDescription { get; set; }
	}
}