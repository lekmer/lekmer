using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewUserService : IRatingReviewUserService
	{
		protected RatingReviewUserRepository Repository { get; private set; }

		public RatingReviewUserService(RatingReviewUserRepository repository)
		{
			Repository = repository;
		}

		public virtual IRatingReviewUser Create(IUserContext context)
		{
			var reviewUser = IoC.Resolve<IRatingReviewUser>();

			reviewUser.CreatedDate = DateTime.Now;
			reviewUser.CustomerId = context.Customer != null ? context.Customer.Id : (int?)null;
			reviewUser.Token = Guid.NewGuid().ToString();

			reviewUser.SetUntouched();
			return reviewUser;
		}

		public virtual IRatingReviewUser Save(IRatingReviewUser ratingReviewUser)
		{
			if (ratingReviewUser == null)
			{
				throw new ArgumentNullException("ratingReviewUser");
			}

			Repository.EnsureNotNull();

			using (var transaction = new TransactedOperation())
			{
				ratingReviewUser.Id = Repository.Save(ratingReviewUser);

				transaction.Complete();
			}

			RatingReviewUserCache.Instance.Remove(new RatingReviewUserIdKey(ratingReviewUser.Id));
			RatingReviewUserCache.Instance.Remove(new RatingReviewUserTokenKey(ratingReviewUser.Token));
			
			if (ratingReviewUser.CustomerId.HasValue)
			{
				RatingReviewUserCache.Instance.Remove(new RatingReviewUserCustomerIdKey(ratingReviewUser.CustomerId.Value));
			}

			ratingReviewUser.SetUntouched();

			return ratingReviewUser;
		}

		public virtual IRatingReviewUser GetById(int ratingReviewUserId)
		{
			Repository.EnsureNotNull();

			return RatingReviewUserCache.Instance.TryGetItem(
				new RatingReviewUserIdKey(ratingReviewUserId),
				() => Repository.GetById(ratingReviewUserId));
		}

		public virtual IRatingReviewUser GetByCustomer(int customerId)
		{
			Repository.EnsureNotNull();

			return RatingReviewUserCache.Instance.TryGetItem(
				new RatingReviewUserCustomerIdKey(customerId),
				() => Repository.GetByCustomer(customerId));
		}

		public virtual IRatingReviewUser GetByToken(string token)
		{
			Repository.EnsureNotNull();

			return RatingReviewUserCache.Instance.TryGetItem(
				new RatingReviewUserTokenKey(token),
				() => Repository.GetByToken(token));
		}
	}
}