﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class BlockRatingService : IBlockRatingService
	{
		protected BlockRatingRepository Repository { get; private set; }

		public BlockRatingService(BlockRatingRepository blockRatingRepository)
		{
			Repository = blockRatingRepository;
		}

		public virtual Collection<IBlockRating> GetAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByBlock(blockId);
		}
	}
}