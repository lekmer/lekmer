using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductRatingSummaryService : IBlockProductRatingSummaryService
	{
		protected IBlockService BlockService { get; private set; }
		protected IBlockRatingService BlockRatingService { get; private set; }
		protected IBlockRatingGroupService BlockRatingGroupService { get; private set; }

		public BlockProductRatingSummaryService(IBlockService blockService, IBlockRatingService blockRatingService, IBlockRatingGroupService blockRatingGroupService)
		{
			BlockService = blockService;
			BlockRatingService = blockRatingService;
			BlockRatingGroupService = blockRatingGroupService;
		}

		public virtual IBlockProductRatingSummary GetById(IUserContext context, int id)
		{
			return BlockProductRatingSummaryCache.Instance.TryGetItem(
				new BlockProductRatingSummaryKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockProductRatingSummary GetByIdCore(IUserContext context, int id)
		{
			IBlock block = BlockService.GetById(context, id);

			var blockProductRatingSummary = IoC.Resolve<IBlockProductRatingSummary>();

			block.ConvertTo(blockProductRatingSummary);

			blockProductRatingSummary.BlockRatings = BlockRatingService.GetAllByBlock(id);
			blockProductRatingSummary.BlockRatingGroups = BlockRatingGroupService.GetAllByBlock(id);

			return blockProductRatingSummary;
		}
	}
}