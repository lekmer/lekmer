﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public class ProductRatingSummaryProcessor : IProductRatingSummaryProcessor
	{
		public virtual IProductRatingSummaryInfo CreateSummary(IUserContext context, int productId, int ratingId, Collection<IRatingItemProductScoreSummary> ratingItemSummaries)
		{
			var productRatingSummary = IoC.Resolve<IProductRatingSummaryInfo>();

			productRatingSummary.ChannelId = context.Channel.Id;
			productRatingSummary.ProductId = productId;
			productRatingSummary.RatingId = ratingId;

			SetTotalNumberOfVotes(productRatingSummary, ratingItemSummaries);
			SetAverageRatingScore(productRatingSummary, ratingItemSummaries);
			SetAverageRatingItem(productRatingSummary, ratingItemSummaries);
			SetRatingItemSummaryCollection(productRatingSummary, ratingItemSummaries);

			return productRatingSummary;
		}

		protected virtual void SetTotalNumberOfVotes(IProductRatingSummaryInfo productRatingSummaryInfo, Collection<IRatingItemProductScoreSummary> ratingItemSummaries)
		{
			foreach (var itemSummary in ratingItemSummaries)
			{
				productRatingSummaryInfo.TotalNumberOfVotes += itemSummary.TotalHitCount;
			}
		}

		protected virtual void SetAverageRatingScore(IProductRatingSummaryInfo productRatingSummaryInfo, Collection<IRatingItemProductScoreSummary> ratingItemSummaries)
		{
			if (productRatingSummaryInfo.TotalNumberOfVotes > 0)
			{
				decimal itemsTotalScore = ratingItemSummaries.Sum(ratingItemSummary => ratingItemSummary.RatingItemScore * ratingItemSummary.TotalHitCount);

				productRatingSummaryInfo.AverageRatingScore = itemsTotalScore / productRatingSummaryInfo.TotalNumberOfVotes;
			}
		}

		protected virtual void SetAverageRatingItem(IProductRatingSummaryInfo productRatingSummaryInfo, Collection<IRatingItemProductScoreSummary> ratingItemSummaries)
		{
			decimal min = ratingItemSummaries.Min(ris => Math.Abs(ris.RatingItemScore - productRatingSummaryInfo.AverageRatingScore));
			var avarageRatingItem = ratingItemSummaries.FirstOrDefault(ri => min == Math.Abs(ri.RatingItemScore - productRatingSummaryInfo.AverageRatingScore));
			if (avarageRatingItem != null)
			{
				productRatingSummaryInfo.AverageRatingItemId = avarageRatingItem.RatingItemId;
			}
		}

		protected virtual void SetRatingItemSummaryCollection(IProductRatingSummaryInfo productRatingSummaryInfo, Collection<IRatingItemProductScoreSummary> ratingItemSummaries)
		{
			productRatingSummaryInfo.RatingItemSummaryCollection = new Collection<IRatingItemSummary>();

			foreach (var ratingItemSummary in ratingItemSummaries)
			{
				var itemSummary = IoC.Resolve<IRatingItemSummary>();

				itemSummary.RatingItemId = ratingItemSummary.RatingItemId;
				itemSummary.TotalHitCount = ratingItemSummary.TotalHitCount;
				if (productRatingSummaryInfo.TotalNumberOfVotes > 0)
				{
					itemSummary.PercentageHitValue = (decimal)ratingItemSummary.TotalHitCount / productRatingSummaryInfo.TotalNumberOfVotes * 100;
				}

				productRatingSummaryInfo.RatingItemSummaryCollection.Add(itemSummary);
			}
		}
	}
}