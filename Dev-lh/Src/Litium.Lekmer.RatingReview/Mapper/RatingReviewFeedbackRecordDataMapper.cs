using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingReviewFeedbackRecordDataMapper : RatingReviewFeedbackDataMapper<IRatingReviewFeedbackRecord>
	{
		private DataMapperBase<IReview> _review;

		public RatingReviewFeedbackRecordDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_review = DataMapperResolver.Resolve<IReview>(DataReader);
		}

		protected override IRatingReviewFeedbackRecord Create()
		{
			var reviewRecord = base.Create();

			reviewRecord.ProductTitle = MapValue<string>("ReviewRecord.Product.Title");
			reviewRecord.ProductErpId = MapValue<string>("ReviewRecord.Product.ErpId");
			reviewRecord.UserName = MapNullableValue<string>("ReviewRecord.Customer.UserName");
			reviewRecord.Review = _review.MapRow();

			return reviewRecord;
		}
	}
}