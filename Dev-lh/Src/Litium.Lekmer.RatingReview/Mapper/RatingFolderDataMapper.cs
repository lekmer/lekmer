﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingFolderDataMapper : DataMapperBase<IRatingFolder>
	{
		public RatingFolderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingFolder Create()
		{
			var ratingFolder = IoC.Resolve<IRatingFolder>();

			ratingFolder.Id = MapValue<int>("RatingFolder.RatingFolderId");
			ratingFolder.ParentRatingFolderId = MapNullableValue<int?>("RatingFolder.ParentRatingFolderId");
			ratingFolder.Title = MapValue<string>("RatingFolder.Title");

			ratingFolder.SetUntouched();

			return ratingFolder;
		}
	}
}