﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingReviewFeedbackLikeDataMapper : DataMapperBase<IRatingReviewFeedbackLike>
	{
		public RatingReviewFeedbackLikeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingReviewFeedbackLike Create()
		{
			var ratingReviewFeedbackLike = IoC.Resolve<IRatingReviewFeedbackLike>();

			ratingReviewFeedbackLike.Id = MapValue<int>("RatingReviewFeedbackLike.RatingReviewFeedbackLikeId");
			ratingReviewFeedbackLike.RatingReviewFeedbackId = MapValue<int>("RatingReviewFeedbackLike.RatingReviewFeedbackId");
			ratingReviewFeedbackLike.IPAddress = MapValue<string>("RatingReviewFeedbackLike.IpAddress");
			ratingReviewFeedbackLike.CreatedDate = MapValue<DateTime>("RatingReviewFeedbackLike.CreatedDate");
			ratingReviewFeedbackLike.RatingReviewUserId = MapValue<int>("RatingReviewFeedbackLike.RatingReviewUserId");

			ratingReviewFeedbackLike.SetUntouched();

			return ratingReviewFeedbackLike;
		}
	}
}