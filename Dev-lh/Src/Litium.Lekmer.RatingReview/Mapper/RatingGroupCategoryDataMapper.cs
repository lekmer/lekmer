﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingGroupCategoryDataMapper : DataMapperBase<IRatingGroupCategory>
	{
		public RatingGroupCategoryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingGroupCategory Create()
		{
			var ratingGroupCategory = IoC.Resolve<IRatingGroupCategory>();

			ratingGroupCategory.RatingGroupId = MapValue<int>("RatingGroupCategory.RatingGroupId");
			ratingGroupCategory.CategoryId = MapValue<int>("RatingGroupCategory.CategoryId");
			ratingGroupCategory.IncludeSubcategories = MapValue<bool>("RatingGroupCategory.IncludeSubCategories");

			ratingGroupCategory.SetUntouched();

			return ratingGroupCategory;
		}
	}
}