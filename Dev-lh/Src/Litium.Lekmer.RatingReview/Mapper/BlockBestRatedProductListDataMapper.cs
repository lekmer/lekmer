﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockBestRatedProductListDataMapper : DataMapperBase<IBlockBestRatedProductList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockBestRatedProductListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockBestRatedProductList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockBestRatedProductList = IoC.Resolve<IBlockBestRatedProductList>();

			block.ConvertTo(blockBestRatedProductList);

			blockBestRatedProductList.CategoryId = MapNullableValue<int?>("BlockBestRatedProductList.CategoryId");
			blockBestRatedProductList.RatingId = MapNullableValue<int?>("BlockBestRatedProductList.RatingId");
			blockBestRatedProductList.Setting = _blockSettingDataMapper.MapRow();

			blockBestRatedProductList.SetUntouched();

			return blockBestRatedProductList;
		}
	}
}