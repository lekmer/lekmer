﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class BlockProductMostHelpfulRatingCache : ScensumCacheBase<BlockProductMostHelpfulRatingKey, IBlockProductMostHelpfulRating>
	{
		private static readonly BlockProductMostHelpfulRatingCache _instance = new BlockProductMostHelpfulRatingCache();

		private BlockProductMostHelpfulRatingCache()
		{
		}

		public static BlockProductMostHelpfulRatingCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockProductMostHelpfulRatingKey(channel.Id, blockId));
			}
		}
	}

	public class BlockProductMostHelpfulRatingKey : ICacheKey
	{
		public BlockProductMostHelpfulRatingKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}