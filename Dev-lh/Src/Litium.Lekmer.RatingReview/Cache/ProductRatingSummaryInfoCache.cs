﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class ProductRatingSummaryInfoCache : ScensumCacheBase<ProductRatingSummaryInfoCacheKey, IProductRatingSummaryInfo>
	{
		private static readonly ProductRatingSummaryInfoCache _instance = new ProductRatingSummaryInfoCache();

		private ProductRatingSummaryInfoCache()
		{
		}

		public static ProductRatingSummaryInfoCache Instance
		{
			get { return _instance; }
		}
	}

	public class ProductRatingSummaryInfoCacheKey : ICacheKey
	{
		public ProductRatingSummaryInfoCacheKey(int channelId, int ratingId, int productId)
		{
			ChannelId = channelId;
			RatingId = ratingId;
			ProductId = productId;
		}

		public int ChannelId { get; set; }
		public int RatingId { get; set; }
		public int ProductId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + RatingId + "-" + ProductId; }
		}
	}
}