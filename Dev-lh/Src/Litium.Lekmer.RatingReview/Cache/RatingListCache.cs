﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingListCache : ScensumCacheBase<RatingListKey, Collection<IRating>>
	{
		private static readonly RatingListCache _instance = new RatingListCache();

		private RatingListCache()
		{
		}

		public static RatingListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int groupId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new RatingListKey(channel.Id, groupId));
			}
		}
	}

	public class RatingListKey : ICacheKey
	{
		public RatingListKey(int channelId, int groupId)
		{
			ChannelId = channelId;
			GroupId = groupId;
		}

		public int ChannelId { get; set; }
		public int GroupId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + GroupId; }
		}
	}
}