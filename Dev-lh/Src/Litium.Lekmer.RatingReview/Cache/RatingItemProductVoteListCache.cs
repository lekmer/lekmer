﻿using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingItemProductVoteListCache : ScensumCacheBase<RatingItemProductVoteListKey, Collection<IRatingItemProductVote>>
	{
		private static readonly RatingItemProductVoteListCache _instance = new RatingItemProductVoteListCache();

		private RatingItemProductVoteListCache()
		{
		}

		public static RatingItemProductVoteListCache Instance
		{
			get { return _instance; }
		}
	}

	public class RatingItemProductVoteListKey : ICacheKey
	{
		public RatingItemProductVoteListKey(int ratingReviewFeedbackId)
		{
			RatingReviewFeedbackId = ratingReviewFeedbackId;
		}

		public int RatingReviewFeedbackId { get; set; }

		public string Key
		{
			get { return "RatingItemProductVote_RatingReviewFeedbackId_" + RatingReviewFeedbackId; }
		}
	}
}