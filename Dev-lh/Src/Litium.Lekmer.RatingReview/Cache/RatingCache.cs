﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingCache : ScensumCacheBase<RatingCacheKey, IRating>
	{
		private static readonly RatingCache _instance = new RatingCache();

		private RatingCache()
		{
		}

		public static RatingCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int ratingId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new RatingCacheKey(channel.Id, ratingId));
			}
		}
	}

	public class RatingCacheKey : ICacheKey
	{
		public RatingCacheKey(int channelId, int ratingId)
		{
			ChannelId = channelId;
			RatingId = ratingId;
		}

		public int ChannelId { get; set; }
		public int RatingId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + RatingId; }
		}
	}
}