﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockRatingItem : BusinessObjectBase, IBlockRatingItem
	{
		private int _blockId;
		private int _ratingItemId;

		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public int RatingItemId
		{
			get { return _ratingItemId; }
			set
			{
				CheckChanged(_ratingItemId, value);
				_ratingItemId = value;
			}
		}
	}
}