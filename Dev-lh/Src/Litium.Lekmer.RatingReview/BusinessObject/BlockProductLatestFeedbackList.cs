﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockProductLatestFeedbackList : BlockBase, IBlockProductLatestFeedbackList
	{
		private int _numberOfItems;

		public int NumberOfItems
		{
			get
			{
				return _numberOfItems;
			}
			set
			{
				CheckChanged(_numberOfItems, value);
				_numberOfItems = value;
			}
		}
	}
}