﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockRatingGroup : BusinessObjectBase, IBlockRatingGroup
	{
		private int _blockId;
		private int _ratingGroupId;

		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public int RatingGroupId
		{
			get { return _ratingGroupId; }
			set
			{
				CheckChanged(_ratingGroupId, value);
				_ratingGroupId = value;
			}
		}
	}
}