﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingGroupFolder : BusinessObjectBase, IRatingGroupFolder
	{
		private int _id;
		private int? _parentRatingGroupFolderId;
		private string _title;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int? ParentFolderId
		{
			get { return ParentRatingGroupFolderId; }
			set { ParentRatingGroupFolderId = value; }
		}

		public int? ParentRatingGroupFolderId
		{
			get { return _parentRatingGroupFolderId; }
			set
			{
				CheckChanged(_parentRatingGroupFolderId, value);
				_parentRatingGroupFolderId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
	}
}