﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class ProductRatingSummaryInfo : IProductRatingSummaryInfo
	{
		public int ChannelId { get; set; }
		public int ProductId { get; set; }
		public int RatingId { get; set; }
		public int TotalNumberOfVotes { get; set; }
		public int AverageRatingItemId { get; set; }
		public decimal AverageRatingScore { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IRatingItemSummary> RatingItemSummaryCollection { get; set; }
	}
}