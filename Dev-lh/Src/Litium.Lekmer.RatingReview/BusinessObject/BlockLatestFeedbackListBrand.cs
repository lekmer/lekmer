﻿using System;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockLatestFeedbackListBrand : BusinessObjectBase, IBlockLatestFeedbackListBrand
	{
		private int _blockId;
		private IBrand _brand;

		public int BlockId
		{
			get
			{
				return _blockId;
			}
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public IBrand Brand
		{
			get
			{
				return _brand;
			}
			set
			{
				CheckChanged(_brand, value);
				_brand = value;
			}
		}
	}
}