﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview
{
	public class ReviewOrderedProductEmailTemplate : Template
	{
		public ReviewOrderedProductEmailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public ReviewOrderedProductEmailTemplate(IChannel channel)
			: base("ReviewOrderedProductEmail", channel, false)
		{
		}
	}
}