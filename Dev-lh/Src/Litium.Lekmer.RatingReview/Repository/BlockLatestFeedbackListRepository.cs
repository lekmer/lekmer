﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockLatestFeedbackListRepository
	{
		protected virtual DataMapperBase<IBlockLatestFeedbackList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockLatestFeedbackList>(dataReader);
		}

		public virtual void Save(IBlockLatestFeedbackList blockLatestFeedbackList)
		{
			if (blockLatestFeedbackList == null)
			{
				throw new ArgumentNullException("blockLatestFeedbackList");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockLatestFeedbackList.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", blockLatestFeedbackList.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("NumberOfItems", blockLatestFeedbackList.NumberOfItems, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockLatestFeedbackListRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockLatestFeedbackListSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockLatestFeedbackListRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockLatestFeedbackListDelete]", parameters, dbSettings);
		}

		public virtual IBlockLatestFeedbackList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockLatestFeedbackListRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockLatestFeedbackListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockLatestFeedbackList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockLatestFeedbackListRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockLatestFeedbackListGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}