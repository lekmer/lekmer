﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingReviewFeedbackLikeRepository
	{
		protected virtual DataMapperBase<IRatingReviewFeedbackLike> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingReviewFeedbackLike>(dataReader);
		}

		public virtual int Insert(IRatingReviewFeedbackLike ratingReviewFeedbackLike)
		{
			if (ratingReviewFeedbackLike == null)
			{
				throw new ArgumentNullException("ratingReviewFeedbackLike");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackLike.RatingReviewFeedbackId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IPAddress", ratingReviewFeedbackLike.IPAddress, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", ratingReviewFeedbackLike.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("RatingReviewUserId", ratingReviewFeedbackLike.RatingReviewUserId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewFeedbackLikeRepository.Insert");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingReviewFeedbackLikeInsert]", parameters, dbSettings);
		}
	}
}