﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingReviewUserRepository
	{
		protected virtual DataMapperBase<IRatingReviewUser> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingReviewUser>(dataReader);
		}

		public virtual int Save(IRatingReviewUser ratingReviewUser)
		{
			if (ratingReviewUser == null)
			{
				throw new ArgumentNullException("ratingReviewUser");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewUserId", ratingReviewUser.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", ratingReviewUser.CustomerId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Token", ratingReviewUser.Token, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", ratingReviewUser.CreatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("RatingReviewUserRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingReviewUserSave]", parameters, dbSettings);
		}

		public virtual IRatingReviewUser GetById(int ratingReviewUserId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewUserId", ratingReviewUserId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewUserRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewUserGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IRatingReviewUser GetByCustomer(int customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingReviewUserRepository.GetByCustomer");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewUserGetByCustomer]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IRatingReviewUser GetByToken(string token)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Token", token, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("RatingReviewUserRepository.GetByToken");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewUserGetByToken]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}