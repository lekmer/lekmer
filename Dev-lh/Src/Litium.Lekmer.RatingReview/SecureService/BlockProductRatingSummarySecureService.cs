using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductRatingSummarySecureService : IBlockProductRatingSummarySecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockRatingSecureService BlockRatingSecureService { get; private set; }
		protected IBlockRatingGroupSecureService BlockRatingGroupSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockProductRatingSummarySecureService(
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockRatingSecureService blockRatingSecureService,
			IBlockRatingGroupSecureService blockRatingGroupSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockRatingSecureService = blockRatingSecureService;
			BlockRatingGroupSecureService = blockRatingGroupSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockProductRatingSummary Create()
		{
			AccessSecureService.EnsureNotNull();

			var block = IoC.Resolve<IBlockProductRatingSummary>();

			block.AccessId = AccessSecureService.GetByCommonName("All").Id;

			block.BlockRatings = new Collection<IBlockRating>();
			block.BlockRatingGroups = new Collection<IBlockRatingGroup>();
			
			block.Status = BusinessObjectStatus.New;

			return block;
		}

		public virtual IBlockProductRatingSummary GetById(int id)
		{
			IBlock block = BlockSecureService.GetById(id);

			var blockProductRatingSummary = IoC.Resolve<IBlockProductRatingSummary>();

			block.ConvertTo(blockProductRatingSummary);

			blockProductRatingSummary.BlockRatings = BlockRatingSecureService.GetAllByBlock(id);
			blockProductRatingSummary.BlockRatingGroups = BlockRatingGroupSecureService.GetAllByBlock(id);

			return blockProductRatingSummary;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockProductRatingSummary blockProductRatingSummary = Create();

			blockProductRatingSummary.ContentNodeId = contentNodeId;
			blockProductRatingSummary.ContentAreaId = contentAreaId;
			blockProductRatingSummary.BlockTypeId = blockTypeId;
			blockProductRatingSummary.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockProductRatingSummary.Title = title;
			blockProductRatingSummary.TemplateId = null;
			blockProductRatingSummary.Id = Save(systemUserFull, blockProductRatingSummary);

			return blockProductRatingSummary;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductRatingSummary block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			
			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				BlockRatingSecureService.SaveAll(systemUserFull, block.Id, block.BlockRatings);
				BlockRatingGroupSecureService.SaveAll(systemUserFull, block.Id, block.BlockRatingGroups);

				transactedOperation.Complete();
			}

			BlockProductRatingSummaryCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				BlockRatingSecureService.Delete(systemUserFull, blockId);
				BlockRatingGroupSecureService.Delete(systemUserFull, blockId);

				transactedOperation.Complete();
			}

			BlockProductRatingSummaryCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}