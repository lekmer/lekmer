using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductFeedbackSecureService : IBlockProductFeedbackSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductFeedbackRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockProductFeedbackSecureService(
			IAccessValidator accessValidator,
			BlockProductFeedbackRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockProductFeedback Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockProductFeedback = IoC.Resolve<IBlockProductFeedback>();

			blockProductFeedback.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductFeedback.Status = BusinessObjectStatus.New;

			return blockProductFeedback;
		}

		public virtual IBlockProductFeedback GetById(int id)
		{
			Repository.EnsureNotNull();

			return Repository.GetByIdSecure(id);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockProductFeedback blockProductFeedback = Create();

			blockProductFeedback.ContentNodeId = contentNodeId;
			blockProductFeedback.ContentAreaId = contentAreaId;
			blockProductFeedback.BlockTypeId = blockTypeId;
			blockProductFeedback.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockProductFeedback.AllowReview = true;
			blockProductFeedback.Title = title;
			blockProductFeedback.TemplateId = null;
			blockProductFeedback.Id = Save(systemUserFull, blockProductFeedback);

			return blockProductFeedback;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductFeedback block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			Repository.EnsureNotNull();
			
			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				transactedOperation.Complete();
			}

			BlockProductFeedbackCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockProductFeedbackCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}