﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingSecureService : IRatingSecureService
	{
		protected RatingRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IRatingItemSecureService RatingItemSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public RatingSecureService(RatingRepository ratingRepository, IAccessValidator accessValidator, IRatingItemSecureService ratingItemSecureService, IChannelSecureService channelSecureService)
		{
			Repository = ratingRepository;
			AccessValidator = accessValidator;
			RatingItemSecureService = ratingItemSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IRating Save(ISystemUserFull systemUserFull, IRating rating, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			if (rating == null)
			{
				throw new ArgumentNullException("rating");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			RatingItemSecureService.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				// Save rating.
				int ratingId = Repository.Save(rating);
				rating.Id = ratingId;

				if (ratingId > 0)
				{
					// Save rating translations.
					SaveTranslations(systemUserFull, ratingId, titleTranslations, descriptionTranslations);

					// Delete rating items.
					Collection<IRatingItem> ratingItems = RatingItemSecureService.GetAllByRating(ratingId);
					foreach (var ratingItem in ratingItems)
					{
						if (rating.RatingItems.FirstOrDefault(ri => ri.Id == ratingItem.Id) == null)
						{
							RatingItemSecureService.Delete(systemUserFull, ratingItem.Id);
						}
					}

					// Save rating items.
					foreach (var ratingItem in rating.RatingItems)
					{
						ratingItem.RatingId = ratingId;

						// Save rating item.
						RatingItemSecureService.Save(systemUserFull, ratingItem);

						// Save rating item translations.
						SaveRatingItemTranslations(systemUserFull, ratingItem.Id, ratingItem.TitleTranslations, ratingItem.DescriptionTranslations);
					}
				}

				transaction.Complete();
			}

			var allChannels = ChannelSecureService.GetAll();

			RatingCache.Instance.Remove(rating.Id, allChannels);
			RatingListCache.Instance.Flush();
			RatingItemListCache.Instance.Remove(rating.Id, allChannels);
			ProductRatingSummaryInfoCache.Instance.Flush();

			return rating;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int ratingId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(ratingId);

				transaction.Complete();
			}

			var allChannels = ChannelSecureService.GetAll();

			RatingCache.Instance.Remove(ratingId, allChannels);
			RatingListCache.Instance.Flush();
			RatingItemListCache.Instance.Remove(ratingId, allChannels);
			ProductRatingSummaryInfoCache.Instance.Flush();
		}

		public virtual IRating GetById(int ratingId)
		{
			Repository.EnsureNotNull();

			var rating = Repository.GetByIdSecure(ratingId);

			PopulateRatingItems(rating);

			return rating;
		}

		public Collection<IRating> GetAllByGroup(int groupId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByGroupSecure(groupId);
		}

		public virtual Collection<IRating> GetAllByFolder(int folderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByFolder(folderId);
		}

		public virtual Collection<IRating> Search(string searchCriteria)
		{
			Repository.EnsureNotNull();

			return Repository.Search(searchCriteria);
		}

		protected virtual void SaveTranslations(ISystemUserFull systemUserFull, int ratingId, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			for (int index = 0; index < titleTranslations.Count; index++)
			{
				var titleTranslation = titleTranslations[index];
				var descriptionTranslation = descriptionTranslations[index];
				SaveTranslations(systemUserFull, ratingId, titleTranslation.LanguageId, titleTranslation.Value, descriptionTranslation.Value);
			}
		}

		protected virtual void SaveRatingItemTranslations(ISystemUserFull systemUserFull, int ratingItemId, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations)
		{
			for (int index = 0; index < titleTranslations.Count; index++)
			{
				var titleTranslation = titleTranslations[index];
				var descriptionTranslation = descriptionTranslations[index];
				RatingItemSecureService.SaveTranslations(systemUserFull, ratingItemId, titleTranslation.LanguageId, titleTranslation.Value, descriptionTranslation.Value);
			}
		}

		// Translations.

		public virtual void SaveTranslations(ISystemUserFull systemUserFull, int ratingId, int languageId, string title, string description)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.SaveTranslations(ratingId, languageId, title, description);

				transaction.Complete();
			}
		}

		public virtual Collection<ITranslationGeneric> GetAllTitleTranslations(int ratingId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllTitleTranslations(ratingId);
		}

		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int ratingId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllDescriptionTranslations(ratingId);
		}


		protected virtual void PopulateRatingItems(IRating rating)
		{
			if (rating == null)
			{
				return;
			}

			rating.RatingItems.Clear();
			rating.RatingItems.AddRange(RatingItemSecureService.GetAllByRating(rating.Id));
		}
	}
}