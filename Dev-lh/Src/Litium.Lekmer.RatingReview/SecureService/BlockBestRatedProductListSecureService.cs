using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockBestRatedProductListSecureService : IBlockBestRatedProductListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockBestRatedProductListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }
		protected IBlockBestRatedProductListBrandSecureService BlockBestRatedProductListBrandSecureService { get; private set; }

		public BlockBestRatedProductListSecureService(
			IAccessValidator accessValidator,
			BlockBestRatedProductListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IChannelSecureService channelSecureService,
			IBlockBestRatedProductListBrandSecureService blockBestRatedProductListBrandSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			ChannelSecureService = channelSecureService;
			BlockBestRatedProductListBrandSecureService = blockBestRatedProductListBrandSecureService;
		}

		public virtual IBlockBestRatedProductList Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockBestRatedProductList = IoC.Resolve<IBlockBestRatedProductList>();

			blockBestRatedProductList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockBestRatedProductList.Setting = BlockSettingSecureService.Create();
			blockBestRatedProductList.BlockBestRatedProductListBrands = new Collection<IBlockBestRatedProductListBrand>();

			blockBestRatedProductList.Status = BusinessObjectStatus.New;

			return blockBestRatedProductList;
		}

		public virtual IBlockBestRatedProductList GetById(int blockId)
		{
			Repository.EnsureNotNull();

			var blockBestRatedProductList = Repository.GetByIdSecure(blockId);

			blockBestRatedProductList.BlockBestRatedProductListBrands = BlockBestRatedProductListBrandSecureService.GetAllByBlock(blockId);

			return blockBestRatedProductList;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockBestRatedProductList blockBestRatedProductList = Create();

			blockBestRatedProductList.ContentNodeId = contentNodeId;
			blockBestRatedProductList.ContentAreaId = contentAreaId;
			blockBestRatedProductList.BlockTypeId = blockTypeId;
			blockBestRatedProductList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockBestRatedProductList.CategoryId = null;
			blockBestRatedProductList.RatingId = null;
			blockBestRatedProductList.Title = title;
			blockBestRatedProductList.TemplateId = null;
			blockBestRatedProductList.Id = Save(systemUserFull, blockBestRatedProductList);

			return blockBestRatedProductList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockBestRatedProductList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();
			
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				BlockBestRatedProductListBrandSecureService.Save(block.Id, block.BlockBestRatedProductListBrands);

				transactedOperation.Complete();
			}

			BlockBestRatedProductListCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockBestRatedProductListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}