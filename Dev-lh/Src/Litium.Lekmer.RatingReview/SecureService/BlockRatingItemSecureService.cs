﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockRatingItemSecureService : IBlockRatingItemSecureService
	{
		protected BlockRatingItemRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public BlockRatingItemSecureService(BlockRatingItemRepository blockRatingItemRepository, IAccessValidator accessValidator)
		{
			Repository = blockRatingItemRepository;
			AccessValidator = accessValidator;
		}

		public virtual void SaveAll(ISystemUserFull systemUserFull, int blockId, Collection<IBlockRatingItem> blockRatingItems)
		{
			if (blockRatingItems == null)
			{
				throw new ArgumentNullException("blockRatingItems");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(blockId);

				foreach (var blockRatingItem in blockRatingItems)
				{
					Repository.Save(blockId, blockRatingItem.RatingItemId);
				}

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(blockId);

				transaction.Complete();
			}
		}

		public virtual Collection<IBlockRatingItem> GetAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByBlock(blockId);
		}
	}
}