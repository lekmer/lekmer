﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingRegistrySecureService : IRatingRegistrySecureService
	{
		protected RatingRegistryRepository Repository { get; private set; }

		public RatingRegistrySecureService(RatingRegistryRepository ratingRegistryRepository)
		{
			Repository = ratingRegistryRepository;
		}

		public virtual Collection<IRatingRegistry> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}
	}
}