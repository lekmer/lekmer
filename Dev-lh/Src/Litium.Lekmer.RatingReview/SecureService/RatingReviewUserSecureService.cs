using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewUserSecureService : IRatingReviewUserSecureService
	{
		protected RatingReviewUserRepository Repository { get; private set; }

		public RatingReviewUserSecureService(RatingReviewUserRepository repository)
		{
			Repository = repository;
		}

		public IRatingReviewUser GetById(int ratingReviewUserId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(ratingReviewUserId);
		}
	}
}