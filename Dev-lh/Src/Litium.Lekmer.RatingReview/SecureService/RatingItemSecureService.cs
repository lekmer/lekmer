﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingItemSecureService : IRatingItemSecureService
	{
		protected RatingItemRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public RatingItemSecureService(RatingItemRepository ratingItemRepository, IAccessValidator accessValidator)
		{
			Repository = ratingItemRepository;
			AccessValidator = accessValidator;
		}

		public virtual IRatingItem Save(ISystemUserFull systemUserFull, IRatingItem ratingItem)
		{
			if (ratingItem == null)
			{
				throw new ArgumentNullException("ratingItem");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				ratingItem.Id = Repository.Save(ratingItem);

				transaction.Complete();
			}

			return ratingItem;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int ratingItemId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(ratingItemId);

				transaction.Complete();
			}
		}

		public virtual void DeleteAll(ISystemUserFull systemUserFull, int ratingId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAll(ratingId);

				transaction.Complete();
			}
		}

		public virtual Collection<IRatingItem> GetAllByRating(int ratingId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByRatingSecure(ratingId);
		}

		// Translations.

		public virtual void SaveTranslations(ISystemUserFull systemUserFull, int ratingItemId, int languageId, string title, string description)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.SaveTranslations(ratingItemId, languageId, title, description);

				transaction.Complete();
			}
		}

		public virtual Collection<ITranslationGeneric> GetAllTitleTranslations(int ratingItemId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllTitleTranslations(ratingItemId);
		}

		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int ratingItemId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllDescriptionTranslations(ratingItemId);
		}
	}
}