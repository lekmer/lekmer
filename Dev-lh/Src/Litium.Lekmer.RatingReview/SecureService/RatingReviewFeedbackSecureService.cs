using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewFeedbackSecureService : IRatingReviewFeedbackSecureService
	{
		private const string PENDING_STATUS = "Pending";
		private const string APPROVED_STATUS = "Approved";

		private Collection<IRatingReviewStatus> _statuses;
		protected Collection<IRatingReviewStatus> Statuses
		{
			get { return _statuses ?? (_statuses = RatingReviewStatusSecureService.GetAll()); }
		}

		protected RatingReviewFeedbackRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IRatingItemProductVoteSecureService RatingItemProductVoteSecureService { get; private set; }
		protected ILekmerProductSecureService LekmerProductSecureService { get; private set; }
		protected IRatingSecureService RatingSecureService { get; private set; }
		protected IRatingItemProductScoreSecureService RatingItemProductScoreSecureService { get; private set; }
		protected IRatingReviewStatusSecureService RatingReviewStatusSecureService { get; private set; }
		protected IReviewSecureService ReviewSecureService { get; private set; }
		
		public RatingReviewFeedbackSecureService(
			RatingReviewFeedbackRepository repository,
			IAccessValidator accessValidator,
			IRatingItemProductVoteSecureService ratingItemProductVoteSecureService,
			IProductSecureService lekmerProductSecureService,
			IRatingSecureService ratingSecureService,
			IRatingItemProductScoreSecureService ratingItemProductScoreSecureService,
			IRatingReviewStatusSecureService ratingReviewStatusSecureService,
			IReviewSecureService reviewSecureService)
		{
			Repository = repository;
			AccessValidator = accessValidator;
			RatingItemProductVoteSecureService = ratingItemProductVoteSecureService;
			LekmerProductSecureService = (ILekmerProductSecureService) lekmerProductSecureService;
			RatingSecureService = ratingSecureService;
			RatingItemProductScoreSecureService = ratingItemProductScoreSecureService;
			RatingReviewStatusSecureService = ratingReviewStatusSecureService;
			ReviewSecureService = reviewSecureService;
		}

		public virtual bool Save(ISystemUserFull systemUserFull, int id, string title, string message, int statusId, bool isInappropriate)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			var newStatus = Statuses.FirstOrDefault(s => s.Id == statusId);
			if (newStatus == null) return false;

			bool isNeedInsertScore;
			bool isNeedDeleteScore;

			var feedback = GetRecordById(id);
			bool isStatusChanged = feedback.RatingReviewStatusId != statusId;
			bool isInappropriateChanged = feedback.Inappropriate != isInappropriate;
			GetActivityWithScore(isStatusChanged, isInappropriateChanged, newStatus, isInappropriate, out isNeedInsertScore, out isNeedDeleteScore);

			using (var transactedOperation = new TransactedOperation())
			{
				if (feedback.Review.Title != title || feedback.Review.Message != message)
				{
					ReviewSecureService.ReviewHistoryInsert(systemUserFull, feedback.Review.Id);
				}
				feedback.Review.Title = title;
				feedback.Review.Message = message;
				ReviewSecureService.Save(systemUserFull, feedback.Review);

				if (isStatusChanged || isInappropriateChanged)
				{
					feedback.RatingReviewStatusId = statusId;
					feedback.Inappropriate = isInappropriate;
					Repository.Save(feedback);
				}

				if (isNeedInsertScore)
				{
					InsertRatingItemProductScore(systemUserFull, feedback);
				}

				if (isNeedDeleteScore)
				{
					DeleteRatingItemProductScore(systemUserFull, feedback.Id);
				}

				transactedOperation.Complete();
			}

			ReviewCache.Instance.Remove(new ReviewKey(feedback.Id));
			RatingItemProductVoteListCache.Instance.Remove(new RatingItemProductVoteListKey(feedback.Id));
			LatestRatingReviewFeedbackListCache.Instance.Flush();

			return true;
		}

		public virtual void SetStatus(ISystemUserFull systemUserFull, IEnumerable<int> ratingReviewFeedbackIds, int statusId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			if (ratingReviewFeedbackIds == null)
			{
				throw new ArgumentNullException("ratingReviewFeedbackIds");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var id in ratingReviewFeedbackIds)
				{
					SetStatusCore(systemUserFull, id, statusId);
				}
				transactedOperation.Complete();
			}

			LatestRatingReviewFeedbackListCache.Instance.Flush();
		}

		public virtual void Delete(ISystemUserFull systemUserFull, IEnumerable<int> ratingReviewFeedbackIds)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			if (ratingReviewFeedbackIds == null)
			{
				throw new ArgumentNullException("ratingReviewFeedbackIds");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var id in ratingReviewFeedbackIds)
				{
					var feedback = GetById(id);
					if (feedback != null)
					{
						var status = Statuses.FirstOrDefault(s => s.Id == feedback.RatingReviewStatusId);
						if (status != null && status.CommonName == "Approved" && !feedback.Inappropriate)
						{
							DeleteRatingItemProductScore(systemUserFull, id);
						}
					}
					Repository.Delete(id);

					ReviewCache.Instance.Remove(new ReviewKey(id));
					RatingItemProductVoteListCache.Instance.Remove(new RatingItemProductVoteListKey(id));
				}
				transactedOperation.Complete();
			}

			LatestRatingReviewFeedbackListCache.Instance.Flush();
		}

		public virtual void RemoveInappropriateFlag(ISystemUserFull systemUserFull, IEnumerable<int> ratingReviewFeedbackIds)
		{
			if (ratingReviewFeedbackIds == null)
			{
				throw new ArgumentNullException("ratingReviewFeedbackIds");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var ratingReviewFeedbackId in ratingReviewFeedbackIds)
				{
					var feedback = GetById(ratingReviewFeedbackId);
					var status = Statuses.FirstOrDefault(s => s.Id == feedback.RatingReviewStatusId);
					if (feedback.Inappropriate && status != null && status.CommonName == "Approved")
					{
						InsertRatingItemProductScore(systemUserFull, feedback);
					}

					Repository.UpdateInappropriateFlag(ratingReviewFeedbackId, false);

					ReviewCache.Instance.Remove(new ReviewKey(ratingReviewFeedbackId));
					RatingItemProductVoteListCache.Instance.Remove(new RatingItemProductVoteListKey(ratingReviewFeedbackId));
				}

				transactedOperation.Complete();
			}

			LatestRatingReviewFeedbackListCache.Instance.Flush();
		}

		public virtual RatingReviewFeedbackRecordCollection Search(IReviewSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null)
			{
				throw new ArgumentNullException("searchCriteria");
			}

			Repository.EnsureNotNull();
			RatingItemProductVoteSecureService.EnsureNotNull();

			RatingReviewFeedbackRecordCollection ratingReviewFeedbackRecordCollection = Repository.Search(searchCriteria, page, pageSize);
			foreach (var ratingReviewFeedbackRecord in ratingReviewFeedbackRecordCollection)
			{
				ratingReviewFeedbackRecord.RatingItemProductVotes = RatingItemProductVoteSecureService.GetAllByFeedback(ratingReviewFeedbackRecord.Id);
			}

			return ratingReviewFeedbackRecordCollection;
		}

		public virtual IRatingReviewFeedback GetById(int ratingReviewFeedbackId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(ratingReviewFeedbackId);
		}

		public virtual IRatingReviewFeedbackRecord GetRecordById(int ratingReviewFeedbackId)
		{
			Repository.EnsureNotNull();

			var ratingReviewFeedbackRecord = Repository.GetRecordById(ratingReviewFeedbackId);
			ratingReviewFeedbackRecord.RatingItemProductVotes = RatingItemProductVoteSecureService.GetAllByFeedback(ratingReviewFeedbackId);

			return ratingReviewFeedbackRecord;
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "4#"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "5#")]
		protected virtual void GetActivityWithScore(
			bool isStatusChanged,
			bool isInappropriateChanged,
			IRatingReviewStatus status,
			bool isInappropriate,
			out bool isInsert,
			out bool isDelete)
		{
			isInsert = false;
			isDelete = false;

			if (isStatusChanged && isInappropriateChanged)
			{
				if (isInappropriate && status.CommonName == PENDING_STATUS)
				{
					isDelete = true;
				}
				else if (!isInappropriate && status.CommonName == APPROVED_STATUS)
				{
					isInsert = true;
				}
			}
			else if (isInappropriateChanged && status.CommonName == APPROVED_STATUS)
			{
				if (isInappropriate)
				{
					isDelete = true;
				}
				else
				{
					isInsert = true;
				}
			}
			else if (isStatusChanged && !isInappropriate)
			{
				if (status.CommonName == APPROVED_STATUS)
				{
					isInsert = true;
				}
				else if (status.CommonName == PENDING_STATUS)
				{
					isDelete = true;
				}
			}
		}

		protected virtual void SetStatusCore(ISystemUserFull systemUserFull, int ratingReviewFeedbackId, int statusId)
		{
			using (var transaction = new TransactedOperation())
			{
				var feedback = GetById(ratingReviewFeedbackId);
				if (feedback != null && feedback.RatingReviewStatusId != statusId)
				{
					Repository.SetStatus(ratingReviewFeedbackId, statusId);

					var status = Statuses.FirstOrDefault(s => s.Id == statusId);
					if (status == null) return;

					if (status.CommonName == PENDING_STATUS && !feedback.Inappropriate)
					{
						DeleteRatingItemProductScore(systemUserFull, feedback.Id);
					}
					else if (status.CommonName == APPROVED_STATUS && !feedback.Inappropriate)
					{
						InsertRatingItemProductScore(systemUserFull, feedback);
					}
				}

				transaction.Complete();
			}
		}

		protected virtual void InsertRatingItemProductScore(ISystemUserFull systemUserFull, IRatingReviewFeedback feedback)
		{
			var ratingItemProductVotes = RatingItemProductVoteSecureService.GetAllByFeedback(feedback.Id);
			if (ratingItemProductVotes == null) return;

			foreach (var productVote in ratingItemProductVotes)
			{
				InsertRatingItemProductScore(systemUserFull, feedback.ChannelId, feedback.ProductId, productVote.RatingId, productVote.RatingItemId);
			}

			// Update score for all product variants
			ProductIdCollection productIdCollection = LekmerProductSecureService.GetVariantIdAllByProduct(feedback.ProductId);
			foreach (var productVote in ratingItemProductVotes)
			{
				var rating = RatingSecureService.GetById(productVote.RatingId);
				if (rating.CommonForVariants)
				{
					foreach (int productId in productIdCollection)
					{
						InsertRatingItemProductScore(systemUserFull, feedback.ChannelId, productId, productVote.RatingId, productVote.RatingItemId);
					}
				}
			}
		}

		protected virtual void InsertRatingItemProductScore(ISystemUserFull systemUserFull, int channelId, int productId, int ratingId, int ratingItemId)
		{
			var ratingItemProductScore = RatingItemProductScoreSecureService.Create(channelId, productId, ratingId, ratingItemId);
			RatingItemProductScoreSecureService.Insert(systemUserFull, ratingItemProductScore);
		}

		protected virtual void DeleteRatingItemProductScore(ISystemUserFull systemUserFull, int feedbackId)
		{
			RatingItemProductScoreSecureService.Delete(systemUserFull, feedbackId);
		}
	}
}