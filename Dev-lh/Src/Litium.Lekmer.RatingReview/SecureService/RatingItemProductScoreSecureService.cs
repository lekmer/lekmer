﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public class RatingItemProductScoreSecureService : IRatingItemProductScoreSecureService
	{
		protected RatingItemProductScoreRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public RatingItemProductScoreSecureService(RatingItemProductScoreRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual IRatingItemProductScore Create(int channelId, int productId, int ratingId, int ratingItemId)
		{
			var ratingItemProductScore = IoC.Resolve<IRatingItemProductScore>();

			ratingItemProductScore.ChannelId = channelId;
			ratingItemProductScore.ProductId = productId;
			ratingItemProductScore.RatingId = ratingId;
			ratingItemProductScore.RatingItemId = ratingItemId;

			ratingItemProductScore.HitCount = 1;

			ratingItemProductScore.Status = BusinessObjectStatus.New;
			return ratingItemProductScore;
		}

		public virtual IRatingItemProductScore Insert(ISystemUserFull systemUserFull, IRatingItemProductScore ratingItemProductScore)
		{
			if (ratingItemProductScore == null)
			{
				throw new ArgumentNullException("ratingItemProductScore");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);
			Repository.Insert(ratingItemProductScore);

			ProductRatingSummaryInfoCache.Instance.Remove(new ProductRatingSummaryInfoCacheKey(ratingItemProductScore.ChannelId, ratingItemProductScore.RatingId, ratingItemProductScore.ProductId));

			return ratingItemProductScore;
		}

		public void Delete(ISystemUserFull systemUserFull, int ratingReviewFeedbackId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);
			Repository.Delete(ratingReviewFeedbackId);

			ProductRatingSummaryInfoCache.Instance.Flush();
		}
	}
}