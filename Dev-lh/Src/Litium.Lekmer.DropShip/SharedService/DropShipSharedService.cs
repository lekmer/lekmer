﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Litium.Framework.Messaging.SmtpEmail;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.DropShip
{
	public class DropShipSharedService : IDropShipSharedService
	{
		protected SupplierRepository Repository { get; private set; }
		protected IDropShipSetting Setting { get; private set; }
		protected SmtpEmailSetting SmtpEmailSetting { get; private set; }

		public DropShipSharedService(SupplierRepository repository, IDropShipSetting setting)
		{
			Repository = repository;
			Setting = setting;
			SmtpEmailSetting = new SmtpEmailSetting();
		}

		public virtual string SaveCsvFile(KeyValuePair<string, Collection<IDropShipInfo>> dropShipInfo)
		{
			var csvFilePath = GetFilePath(Setting.CsvFileName, dropShipInfo.Key);
			using (Stream stream = File.OpenWrite(csvFilePath))
			{
				var csvDropShipFile = new CsvDropShipFile();
				csvDropShipFile.Initialize(Setting, dropShipInfo.Value.OrderBy(ds => ds.OrderId));
				csvDropShipFile.Save(stream);
			}

			return csvFilePath;
		}

		public virtual string SavePdfFile(KeyValuePair<string, Collection<IDropShipInfo>> dropShipInfo)
		{
			var pdfFilePath = GetFilePath(Setting.PdfFileName, dropShipInfo.Key);
			var pdfDropShipFile = new PdfDropShipFile();
			pdfDropShipFile.Initialize(pdfFilePath, Setting.LogoPath, dropShipInfo.Value);
			pdfDropShipFile.Save();

			return pdfFilePath;
		}

		public virtual void SendMessage(string supplierNumber, string csvFilePath, string pdfFilePath, bool isFileSent)
		{
			var supplier = Repository.GetByNo(supplierNumber);

			var supplierName = supplier.Name;
			var supplierEmail = supplier.DropShipEmail;
			if (string.IsNullOrEmpty(supplierEmail))
			{
				throw new Exception(string.Format("Supplier '{0}' had no e-mail address.", supplierNumber));
			}

			MailAddress to;
			try
			{
				to = new MailAddress(supplierEmail, supplierName);
			}
			catch (Exception ex)
			{
				throw new Exception(string.Format(CultureInfo.InvariantCulture, "Supplier '{0}' had an invalid e-mail address ('{1}').", supplierNumber, supplierEmail), ex);
			}

			var from = new MailAddress(Setting.FromEmail, Setting.FromName);

			var smtpClient = new SmtpClient(SmtpEmailSetting.Host, SmtpEmailSetting.Port); //("smtp.gmail.com", 587) for test
			smtpClient.EnableSsl = Setting.EnableSsl;

			string userName = SmtpEmailSetting.NetworkCredentialUserName; //"lh.robot.service@gmail.com" for test
			if (!string.IsNullOrEmpty(userName))
			{
				smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
				smtpClient.UseDefaultCredentials = false;
				smtpClient.Credentials = new NetworkCredential(
					userName,
					SmtpEmailSetting.NetworkCredentialPassword ?? string.Empty, //"C9rmHAimBH3F" for test
					SmtpEmailSetting.NetworkCredentialDomain ?? string.Empty);
			}

			int? timeout = SmtpEmailSetting.Timeout;
			if (timeout.HasValue)
			{
				smtpClient.Timeout = timeout.Value;
			}

			var mailMessage = new MailMessage(from, to)
			{
				Subject = isFileSent ? Setting.MailSubjectSecond : Setting.MailSubject,
				Body = isFileSent ? Setting.MailBodySecond: Setting.MailBody,
				IsBodyHtml = true
			};

			string[] bccEmails = Setting.BccEmail.Split(';');
			foreach (string bcc in bccEmails)
			{
				mailMessage.Bcc.Add(new MailAddress(bcc));
			}

			mailMessage.Attachments.Add(new Attachment(csvFilePath));
			mailMessage.Attachments.Add(new Attachment(pdfFilePath));

			smtpClient.Send(mailMessage);
		}

		protected virtual string GetFilePath(string fileNameTemplate, string supplierNo)
		{
			string dirr = Setting.DestinationDirectory;
			string fileName = string.Format(fileNameTemplate, supplierNo, DateTime.Now.ToString("yyyyMMdd-HHmmss"));
			var filePath = Path.Combine(dirr, fileName);

			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			return filePath;
		}
	}
}