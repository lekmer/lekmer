﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.DropShip
{
	public class DropShipSecureService : IDropShipSecureService
	{
		protected DropShipRepository Repository { get; private set; }

		public DropShipSecureService(DropShipRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IDropShipInfo> GetAllGroupByOrder()
		{
			return Repository.GetAllGroupByOrder();
		}

		public virtual Collection<IDropShipInfo> GetAllByOrder(int orderId)
		{
			return Repository.GetAllByOrder(orderId);
		}
	}
}