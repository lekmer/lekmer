﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Pdf;
using Convert = System.Convert;

namespace Litium.Lekmer.DropShip
{
	public class PdfDropShipFile
	{
		private static XPdfFontOptions _options = new XPdfFontOptions(PdfFontEncoding.Unicode, PdfFontEmbedding.Always);
		private XFont HeaderFont
		{
			get { return BuildFont(17, XFontStyle.Regular); }
		}
		private XFont OrderNoFont
		{
			get { return BuildFont(24, XFontStyle.Bold); }
		}
		private XFont AddressHeaderFont
		{
			get { return BuildFont(12, XFontStyle.Bold); }
		}
		private XFont LabelFont
		{
			get { return BuildFont(10, XFontStyle.Bold); }
		}
		private XFont DataFont
		{
			get { return BuildFont(10, XFontStyle.Regular); }
		}
		private XFont ArticleLabelFont
		{
			get { return BuildFont(7, XFontStyle.Bold); }
		}
		private XFont ArticleDataFont
		{
			get { return BuildFont(7, XFontStyle.Regular); }
		}

		private bool _isInitialized;
		private string _filePath;
		private string _logoPath;
		private Collection<IDropShipInfo> _dsInfoCollection;

		public void Initialize(string filePath, string logoPath, Collection<IDropShipInfo> dropShipInfoCollection)
		{
			_filePath = filePath;
			_logoPath = logoPath;
			_dsInfoCollection = dropShipInfoCollection;

			_isInitialized = true;
		}

		public void Save()
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) should be called first.");
			}

			var document = new PdfDocument();

			Dictionary<string, Collection<IDropShipInfo>> dsInfoGroupedByOrder = _dsInfoCollection.GroupBy(ds => ds.OrderId).ToDictionary(ds => ds.Key, ds => new Collection<IDropShipInfo>(ds.ToArray()));

			int pageNo = 1;
			int pageCount = dsInfoGroupedByOrder.Values.Count;
			foreach (var dropShipInfoCollection in dsInfoGroupedByOrder.Values)
			{
				IDropShipInfo generalInfo = dropShipInfoCollection[0];
				IDropShipInfoFormatter generalInfoFormatter = new DropShipInfoFormatter();
				generalInfoFormatter.DropShipInfoSource = generalInfo;

				PdfPage page = document.AddPage();
				XGraphics graphics = XGraphics.FromPdfPage(page);
				var formater = new XTextFormatter(graphics);

				// Head
				graphics.DrawRectangle(BuildSolidBrush(141, 216, 248), BuildRectangle(0, 0, page.Width.Value, 57));
				graphics.DrawRectangle(BuildSolidBrush(242, 243, 245), BuildRectangle(35, 395, page.Width - 70, 20));
				graphics.DrawImage(XImage.FromFile(_logoPath), BuildRectangle(35, 0, 108, 57));
				formater.DrawString(DropShipInfoCaptions.Orderinformation, HeaderFont, XBrushes.White, BuildRectangle(page.Width - 270, 20, 100, 30));

				// Page
				formater.DrawString(string.Format("Page {0} of {1}", pageNo, pageCount), DataFont, XBrushes.Black, BuildRectangle(page.Width - 70, 68, 80, 10));

				// Order No
				formater.DrawString(DropShipInfoCaptions.Ordernummer, LabelFont, XBrushes.Black, BuildRectangle(35, 92, 100, 10));
				formater.DrawString(generalInfoFormatter.OrderId, OrderNoFont, XBrushes.Black, BuildRectangle(35, 104, 100, 10));

				// Order info
				formater.DrawString(DropShipInfoCaptions.Registerad, LabelFont, XBrushes.Black, BuildRectangle(page.Width - 270, 92, 100, 10));
				formater.DrawString(generalInfoFormatter.OrderDate, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, 92, 150, 10));
				formater.DrawString(DropShipInfoCaptions.Kundnummer, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, 114, 100, 10));
				formater.DrawString(generalInfoFormatter.CustomerNumber, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, 114, 150, 10));
				formater.DrawString(DropShipInfoCaptions.Aviseringsnummer, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, 126, 100, 10));
				formater.DrawString(generalInfoFormatter.NotificationNumber, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, 126, 150, 10));
				formater.DrawString(DropShipInfoCaptions.Betalningsmetod, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, 138, 100, 10));
				formater.DrawString(generalInfoFormatter.PaymentMethod, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, 138, 150, 10));
				formater.DrawString(DropShipInfoCaptions.Leveranssätt, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, 150, 100, 10));
				formater.DrawString(DropShipInfoCaptions.Dropship, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, 150, 150, 10));

				// Billing address
				var startY = 180;
				formater.DrawString(DropShipInfoCaptions.Faktureringsadress, AddressHeaderFont, XBrushes.Black, BuildRectangle(35, startY, 100, 10));
				formater.DrawString(DropShipInfoCaptions.FirstName, DataFont, XBrushes.Black, BuildRectangle(35, startY += 30, 80, 10));
				formater.DrawString(DropShipInfoCaptions.LastName, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Company, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Co, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Street, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.StreetNr, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Entrance, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Floor, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.ApartmentNo, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.ZipCode, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.City, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Country, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Cellphone, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Email, DataFont, XBrushes.Black, BuildRectangle(35, startY += 11, 80, 10));

				startY = 210;
				formater.DrawString(generalInfoFormatter.BillingFirstName, DataFont, XBrushes.Black, BuildRectangle(120, startY, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingLastName, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingCompany, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingCo, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingStreet, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingStreetNr, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingEntrance, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingFloor, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingApartmentNumber, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingZip, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingCity, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.BillingCountry, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.Phone, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.Email, DataFont, XBrushes.Black, BuildRectangle(120, startY += 11, 150, 10));

				// Delivery address
				startY = 180;
				formater.DrawString(DropShipInfoCaptions.Leveransadress, AddressHeaderFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY, 100, 10));
				formater.DrawString(DropShipInfoCaptions.FirstName, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 30, 80, 10));
				formater.DrawString(DropShipInfoCaptions.LastName, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Company, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Co, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Street, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.StreetNr, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Entrance, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Floor, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.ApartmentNo, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.ZipCode, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.City, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Country, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Cellphone, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));
				formater.DrawString(DropShipInfoCaptions.Email, DataFont, XBrushes.Black, BuildRectangle(page.Width - 270, startY += 11, 80, 10));

				generalInfo.PopulateDeliveryInfo();
				IDropShipInfoFormatter deliveryAddressFormatter = new DropShipInfoFormatter();
				deliveryAddressFormatter.DropShipInfoSource = generalInfo;

				startY = 210;
				formater.DrawString(deliveryAddressFormatter.DeliveryFirstName, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryLastName, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryCompany, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryCo, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryStreet, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryStreetNr, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryEntrance, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryFloor, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryApartmentNumber, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryZip, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryCity, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(deliveryAddressFormatter.DeliveryCountry, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.Phone, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));
				formater.DrawString(generalInfoFormatter.Email, DataFont, XBrushes.Black, BuildRectangle(page.Width - 165, startY += 11, 150, 10));

				// Articles
				formater.DrawString(DropShipInfoCaptions.SupplierArtNo, ArticleLabelFont, XBrushes.Black, BuildRectangle(37, 401, 101, 10));
				formater.DrawString(DropShipInfoCaptions.LekmerArtNo, ArticleLabelFont, XBrushes.Black, BuildRectangle(140, 401, 68, 10));
				formater.DrawString(DropShipInfoCaptions.Produkt, ArticleLabelFont, XBrushes.Black, BuildRectangle(210, 401, 138, 10));
				formater.DrawString(DropShipInfoCaptions.Antal, ArticleLabelFont, XBrushes.Black, BuildRectangle(350, 401, 33, 10));
				formater.DrawString(DropShipInfoCaptions.PrisSt, ArticleLabelFont, XBrushes.Black, BuildRectangle(385, 401, 43, 10));
				formater.DrawString(DropShipInfoCaptions.Rabbat, ArticleLabelFont, XBrushes.Black, BuildRectangle(430, 401, 58, 10));
				formater.DrawString(DropShipInfoCaptions.Summa, ArticleLabelFont, XBrushes.Black, BuildRectangle(490, 401, 70, 10));

				var seCultureInfo = new CultureInfo("sv-SE");
				IDropShipInfoFormatter dropShipInfoFormatter = new DropShipInfoFormatter();
				startY = 415;
				decimal sum = 0;
				foreach (var dropShipInfo in dropShipInfoCollection)
				{
					dropShipInfoFormatter.DropShipInfoSource = dropShipInfo;

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(35, startY, 103, 20));
					formater.DrawString(dropShipInfoFormatter.OfferId, ArticleDataFont, XBrushes.Black, BuildRectangle(37, startY + 6, 101, 10));

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(138, startY, 70, 20));
					formater.DrawString(dropShipInfoFormatter.ProductArticleNumber, ArticleDataFont, XBrushes.Black, BuildRectangle(140, startY + 6, 68, 10));

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(208, startY, 140, 20));
					formater.DrawString(dropShipInfoFormatter.ProductTitle, ArticleDataFont, XBrushes.Black, BuildRectangle(210, startY + 6, 138, 10));

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(348, startY, 35, 20));
					formater.DrawString(dropShipInfoFormatter.Quantity, ArticleDataFont, XBrushes.Black, BuildRectangle(350, startY + 6, 33, 10));

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(383, startY, 45, 20));
					formater.DrawString(dropShipInfoFormatter.Price, ArticleDataFont, XBrushes.Black, BuildRectangle(385, startY + 6, 43, 10));

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(428, startY, 60, 20));
					formater.DrawString(dropShipInfoFormatter.ProductDiscount, ArticleDataFont, XBrushes.Black, BuildRectangle(430, startY + 6, 58, 10));

					graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(488, startY, 72, 20));
					formater.DrawString(dropShipInfoFormatter.ProductSoldSum, ArticleDataFont, XBrushes.Black, BuildRectangle(490, startY + 6, 70, 10));

					startY += 20;
					sum += Convert.ToDecimal(dropShipInfo.ProductSoldSum, seCultureInfo);
				}

				// Totals
				formater.Alignment = XParagraphAlignment.Right;

				graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(428, startY, 60, 20));
				formater.DrawString(DropShipInfoCaptions.TotalRabatt, ArticleLabelFont, XBrushes.Black, BuildRectangle(428, startY + 6, 58, 10));

				graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(488, startY, 72, 20));
				formater.DrawString(DropShipInfoCaptions.TotalSumma, LabelFont, XBrushes.Black, BuildRectangle(488, startY + 3, 70, 10));

				startY += 20;

				graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(428, startY, 60, 20));
				formater.DrawString(generalInfoFormatter.Discount, ArticleDataFont, XBrushes.Black, BuildRectangle(428, startY + 6, 58, 10));

				graphics.DrawRectangle(BuildPen(242, 243, 245), BuildRectangle(488, startY, 72, 20));
				formater.DrawString(sum.ToString(seCultureInfo), LabelFont, XBrushes.Black, BuildRectangle(488, startY + 3, 70, 10));

				pageNo++;
			}

			document.Save(_filePath);
		}

		private XFont BuildFont(int size, XFontStyle style)
		{
			return new XFont("Helvetica", size, style, _options);
		}
		private XRect BuildRectangle(double x, double y, double width, double height)
		{
			return new XRect(x, y, width, height);
		}
		private XSolidBrush BuildSolidBrush(byte r, byte g, byte b)
		{
			return new XSolidBrush(new XColor {R = r, G = g, B = b});
		}
		private XPen BuildPen(byte r, byte g, byte b)
		{
			return new XPen(new XColor { R = r, G = g, B = b });
		}
	}
}