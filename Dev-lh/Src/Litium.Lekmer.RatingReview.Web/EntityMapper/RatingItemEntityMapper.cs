using System.Globalization;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class RatingItemEntityMapper : EntityMapper<IRatingItem>
	{
		public override void AddEntityVariables(Fragment fragment, IRatingItem item)
		{
			fragment.AddVariable("RatingItem.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItem.RatingId", item.RatingId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItem.Title", item.Title);
			fragment.AddVariable("RatingItem.Description", item.Description);
			fragment.AddVariable("RatingItem.Score", item.Score.ToString(CultureInfo.InvariantCulture));
		}
	}
}