using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using System.Globalization;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockProductLatestFeedbackListControl : BlockProductPageControlBase<IBlockProductLatestFeedbackList>
	{
		private readonly IBlockProductLatestFeedbackListService _blockProductLatestFeedbackListService;
		private readonly IRatingReviewStatusService _ratingReviewStatusService;
		private readonly IRatingReviewFeedbackService _ratingReviewFeedbackService;
		private readonly IReviewService _reviewService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingService _ratingService;
		private readonly IRatingItemProductVoteService _ratingItemProductVoteService;
		private readonly IProductService _productService;
		private readonly IRatingReviewFeedbackLikeService _ratingReviewFeedbackLikeService;
		private readonly IRatingReviewUserLocatorService _ratingReviewUserLocatorService;

		private LikeFeedbackForm _likeFeedbackForm;
		private InappropriateFeedbackForm _inappropriateFeedbackForm;

		private Collection<IRatingReviewFeedback> _feedbackList;
		private Dictionary<int, IReview> _feedbackReviews;
		private Dictionary<int, IProduct> _feedbackProducts;
		private Dictionary<int, Collection<IRatingGroup>> _feedbackRatingGroups;
		private Dictionary<int, Collection<IRatingItemProductVote>> _feedbackVotes;
		private Dictionary<int, Dictionary<int, Collection<IRating>>> _feedbackRatingGroupRatings;

		public BlockProductLatestFeedbackListControl(
			ITemplateFactory templateFactory,
			IBlockProductLatestFeedbackListService blockProductLatestFeedbackListService,
			IRatingReviewStatusService ratingReviewStatusService,
			IRatingReviewFeedbackService ratingReviewFeedbackService,
			IReviewService reviewService,
			IRatingGroupService ratingGroupService,
			IRatingService ratingService,
			IRatingItemProductVoteService ratingItemProductVoteService,
			IProductService productService,
			IRatingReviewFeedbackLikeService ratingReviewFeedbackLikeService,
			IRatingReviewUserLocatorService ratingReviewUserLocatorService)
			: base(templateFactory)
		{
			_blockProductLatestFeedbackListService = blockProductLatestFeedbackListService;
			_ratingReviewStatusService = ratingReviewStatusService;
			_ratingReviewFeedbackService = ratingReviewFeedbackService;
			_reviewService = reviewService;
			_ratingGroupService = ratingGroupService;
			_ratingService = ratingService;
			_ratingItemProductVoteService = ratingItemProductVoteService;
			_productService = productService;
			_ratingReviewFeedbackLikeService = ratingReviewFeedbackLikeService;
			_ratingReviewUserLocatorService = ratingReviewUserLocatorService;
		}

		protected override IBlockProductLatestFeedbackList GetBlockById(int blockId)
		{
			return _blockProductLatestFeedbackListService.GetById(UserContext.Current, blockId);
		}


		protected override BlockContent RenderCore()
		{
			if (Block == null || Product == null)
			{
				return new BlockContent(null, "[ Can't render product latest feedback list control on this page ]");
			}

			Initialize();

			if (_feedbackList == null ||_feedbackList.Count == 0
				|| _feedbackReviews == null || _feedbackReviews.Count == 0)
			{
				return new BlockContent();
			}

			var content = RenderContent();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, content.Render(), footer);
		}


		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);

			fragment.AddEntity(Block);

			return fragment.Render();
		}

		protected virtual Fragment RenderContent()
		{
			Fragment fragment = Template.GetFragment("Content");

			fragment.AddEntity(Block);

			fragment.AddVariable("FeedbackList", RenderFeedbackList().Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual Fragment RenderFeedbackList()
		{
			Fragment fragment = Template.GetFragment("FeedbackList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingReviewFeedback feedback in _feedbackList)
			{
				stringBuilder.AppendLine(RenderFeedback(feedback).Render());
			}

			fragment.AddVariable("Iterate:Feedback", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderFeedback(IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("Feedback");

			if (_feedbackProducts.ContainsKey(feedback.Id))
			{
				fragment.AddEntity(_feedbackProducts[feedback.Id]);
			}

			fragment.AddVariable("LikeForm", ProcessLikeForm(feedback), VariableEncoding.None);
			fragment.AddVariable("InappropriateForm", ProcessInappropriateForm(feedback), VariableEncoding.None);

			fragment.AddEntity(feedback);

			if (_feedbackReviews.ContainsKey(feedback.Id))
			{
				fragment.AddEntity(_feedbackReviews[feedback.Id]);
			}

			fragment.AddVariable("Review", RenderReview(feedback).Render(), VariableEncoding.None);
			fragment.AddVariable("RatingGroupList", RenderRatingGroupList(feedback).Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual string ProcessLikeForm(IRatingReviewFeedback feedback)
		{
			_likeFeedbackForm.MapFromRequest(Block.Id, feedback.Id);

			bool userCanLike = true;

			if (_likeFeedbackForm.IsFormPostBack && feedback.Id == _likeFeedbackForm.PostBackFeedbackId)
			{
				IRatingReviewFeedbackLike feedbackLike = _ratingReviewFeedbackLikeService.Create(UserContext.Current);
				feedbackLike.RatingReviewFeedbackId = feedback.Id;
				feedbackLike.RatingReviewUserId = _ratingReviewUserLocatorService.GetRatingReviewUser(UserContext.Current).Id;
				feedbackLike.IPAddress = Request.UserHostAddress;

				int feedbackLikeId = _ratingReviewFeedbackLikeService.Insert(UserContext.Current, feedbackLike);
				userCanLike = feedbackLikeId >= 0;

				if (userCanLike)
				{
					feedback.LikeHit = feedback.LikeHit + 1;
				}
			}

			return RenderLikeForm(feedback.Id, userCanLike);
		}

		protected virtual string RenderLikeForm(int feedbackId, bool userCanLike)
		{
			Fragment fragment = Template.GetFragment("LikeForm");

			_likeFeedbackForm.MapToForm(Block.Id, feedbackId, userCanLike, fragment);

			return fragment.Render();
		}


		protected virtual string ProcessInappropriateForm(IRatingReviewFeedback feedback)
		{
			_inappropriateFeedbackForm.MapFromRequest(Block.Id, feedback.Id);

			if (_inappropriateFeedbackForm.IsFormPostBack && feedback.Id == _inappropriateFeedbackForm.PostBackFeedbackId)
			{
				_ratingReviewFeedbackService.InsertInappropriateFlag(feedback.Id);
			}

			return RenderInappropriateForm(feedback);
		}

		protected virtual string RenderInappropriateForm(IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("InappropriateForm");

			_inappropriateFeedbackForm.MapToForm(Block.Id, feedback.Id, fragment);

			return fragment.Render();
		}


		protected virtual Fragment RenderReview(IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("Review");

			if (_feedbackProducts.ContainsKey(feedback.Id))
			{
				fragment.AddEntity(_feedbackProducts[feedback.Id]);
			}

			fragment.AddEntity(feedback);

			if (_feedbackReviews.ContainsKey(feedback.Id))
			{
				fragment.AddEntity(_feedbackReviews[feedback.Id]);
			}

			return fragment;
		}


		protected virtual Fragment RenderRatingGroupList(IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("RatingGroupList");

			var stringBuilder = new StringBuilder();
			if (_feedbackRatingGroups.ContainsKey(feedback.Id) && _feedbackRatingGroupRatings.ContainsKey(feedback.Id))
			{
				var ratingGroupRatings = _feedbackRatingGroupRatings[feedback.Id];
				foreach (IRatingGroup ratingGroup in _feedbackRatingGroups[feedback.Id])
				{
					if (ratingGroupRatings.ContainsKey(ratingGroup.Id) && ratingGroupRatings[ratingGroup.Id].Count > 0)
					{
						stringBuilder.AppendLine(RenderRatingGroup(ratingGroup, feedback).Render());
					}
				}

				fragment.AddVariable("Iterate:RatingGroup", stringBuilder.ToString(), VariableEncoding.None);
			}

			return fragment;
		}

		protected virtual Fragment RenderRatingGroup(IRatingGroup ratingGroup, IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("RatingGroup");

			fragment.AddEntity(ratingGroup);
			fragment.AddVariable("RatingList", RenderRatingList(ratingGroup, feedback).Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual Fragment RenderRatingList(IRatingGroup ratingGroup, IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("RatingList");

			var ratingGroupRatings = _feedbackRatingGroupRatings[feedback.Id];

			var stringBuilder = new StringBuilder();
			foreach (IRating rating in ratingGroupRatings[ratingGroup.Id])
			{
				stringBuilder.AppendLine(RenderRating(rating, ratingGroup, feedback).Render());
			}

			fragment.AddVariable("Iterate:Rating", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRating(IRating rating, IRatingGroup ratingGroup, IRatingReviewFeedback feedback)
		{
			Fragment fragment = Template.GetFragment("Rating");

			if (ratingGroup != null)
			{
				fragment.AddEntity(ratingGroup);
			}

			fragment.AddEntity(rating);

			int selectedRatingItemId = FindSelectedRatingItemId(feedback.Id, rating.Id);

			fragment.AddVariable("Rating.SelectedRatingItemId", selectedRatingItemId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItemList", RenderRatingItemList(rating, feedback, selectedRatingItemId).Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual Fragment RenderRatingItemList(IRating rating, IRatingReviewFeedback feedback, int selectedRatingItemId)
		{
			Fragment fragment = Template.GetFragment("RatingItemList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingItem ratingItem in rating.RatingItems)
			{
				stringBuilder.AppendLine(RenderRatingItem(rating, ratingItem, feedback, selectedRatingItemId).Render());
			}

			fragment.AddVariable("Iterate:RatingItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItem(IRating rating, IRatingItem ratingItem, IRatingReviewFeedback feedback, int selectedRatingItemId)
		{
			Fragment fragment = Template.GetFragment("RatingItem");

			fragment.AddEntity(ratingItem);
			fragment.AddEntity(Block);
			fragment.AddEntity(feedback);

			fragment.AddCondition("RatingItem.Selected", ratingItem.Id == selectedRatingItemId);

			return fragment;
		}


		protected virtual void Initialize()
		{
			// Feedback list.
			PopulateFeedbackList();

			// Products and Reviews and Rating Groups and Rating Group Ratings.
			_feedbackProducts = new Dictionary<int, IProduct>();
			_feedbackReviews = new Dictionary<int, IReview>();
			_feedbackRatingGroups = new Dictionary<int, Collection<IRatingGroup>>();
			_feedbackRatingGroupRatings = new Dictionary<int, Dictionary<int, Collection<IRating>>>();
			_feedbackVotes = new Dictionary<int, Collection<IRatingItemProductVote>>();

			foreach (var feedback in _feedbackList)
			{
				// Review.
				PopulateFeedbackReviewList(feedback.Id);

				// Votes
				PopulateFeedbackVoteList(feedback.Id);

				// Rating Groups.
				var product = _productService.GetById(UserContext.Current, feedback.ProductId);
				if (product != null)
				{
					if (!_feedbackProducts.ContainsKey(feedback.Id))
					{
						_feedbackProducts[feedback.Id] = product;
					}

					var ratingGroups = _ratingGroupService.GetAllByProduct(UserContext.Current, product.Id, product.CategoryId);
					if (ratingGroups != null && ratingGroups.Count > 0)
					{
						if (!_feedbackRatingGroups.ContainsKey(feedback.Id))
						{
							_feedbackRatingGroups[feedback.Id] = ratingGroups;
						}

						// Rating Group Ratings.
						PopulateFeedbackRatingGroupRatings(feedback.Id, ratingGroups);
					}
				}
			}
		}

		protected virtual void PopulateFeedbackList()
		{
			var ratingReviewStatus = _ratingReviewStatusService.GetByCommonName(RatingReviewStatusType.Approved.ToString());
			if (ratingReviewStatus == null)
			{
				return;
			}

			_feedbackList = _ratingReviewFeedbackService.GetLatestByProduct(UserContext.Current, Product.Id, Block.NumberOfItems, ratingReviewStatus.Id, false);

			RemoveDuplicateReviewWithMostHelpfulRatingBlock();

			var feedbacks = new Dictionary<int, int>();
			if (_feedbackList != null && _feedbackList.Count > 0)
			{
				feedbacks = _feedbackList.ToDictionary(f => f.Id, f => f.Id);
			}

			CreateForms(feedbacks);
		}

		protected virtual void PopulateFeedbackReviewList(int feedbackId)
		{
			var review = _reviewService.GetByFeedback(feedbackId);
			if (review == null)
			{
				return;
			}

			if (!_feedbackReviews.ContainsKey(feedbackId))
			{
				_feedbackReviews[feedbackId] = review;
			}
		}

		protected virtual void PopulateFeedbackVoteList(int feedbackId)
		{
			var votes = _ratingItemProductVoteService.GetAllByFeedback(feedbackId);

			if (votes == null)
			{
				return;
			}

			if (!_feedbackVotes.ContainsKey(feedbackId))
			{
				_feedbackVotes[feedbackId] = votes;
			}
		}

		protected virtual void PopulateFeedbackRatingGroupRatings(int feedbackId, Collection<IRatingGroup> ratingGroups)
		{
			var ratingGroupRatings = new Dictionary<int, Collection<IRating>>();
			var ratings = new Dictionary<int, IRating>();

			foreach (IRatingGroup ratingGroup in ratingGroups)
			{
				ratingGroupRatings[ratingGroup.Id] = new Collection<IRating>();

				foreach (IRating rating in _ratingService.GetAllByGroup(UserContext.Current, ratingGroup.Id))
				{
					if (!ratings.ContainsKey(rating.Id))
					{
						ratings[rating.Id] = rating;
						ratingGroupRatings[ratingGroup.Id].Add(rating);
					}
				}
			}

			if (!_feedbackRatingGroupRatings.ContainsKey(feedbackId))
			{
				_feedbackRatingGroupRatings[feedbackId] = ratingGroupRatings;
			}
		}

		protected virtual int FindSelectedRatingItemId(int feedbackId, int ratingId)
		{
			int selectedRatingItemId = -1;

			if (_feedbackVotes.ContainsKey(feedbackId))
			{
				var votes = _feedbackVotes[feedbackId];
				var selectedRatingItem = votes.FirstOrDefault(v => v.RatingId == ratingId);

				if (selectedRatingItem != null)
				{
					selectedRatingItemId = selectedRatingItem.RatingItemId;
				}
			}

			return selectedRatingItemId;
		}


		protected virtual void CreateForms(Dictionary<int, int> feedbacks)
		{
			string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrl());

			_likeFeedbackForm = CreateLikeFeedbackForm(postUrl);
			_likeFeedbackForm.Feedbacks = feedbacks;

			_inappropriateFeedbackForm = CreateInappropriateFeedbackForm(postUrl);
			_inappropriateFeedbackForm.Feedbacks = feedbacks;
		}

		protected virtual LikeFeedbackForm CreateLikeFeedbackForm(string postUrl)
		{
			return new LikeFeedbackForm(postUrl, "product-latest-feedback-list-like-" + Block.Id);
		}

		protected virtual InappropriateFeedbackForm CreateInappropriateFeedbackForm(string postUrl)
		{
			return new InappropriateFeedbackForm(postUrl, "product-latest-feedback-list-inappropriate-" + Block.Id);
		}

		/// <summary>
		/// Skip the review shown by the BlockProductMostHelpfulRatingControl
		/// </summary>
		protected virtual void RemoveDuplicateReviewWithMostHelpfulRatingBlock()
		{
			const string mostHelpfulRatingFeedbackIdKey = "MostHelpfulRating.FeedbackId";

			if (!HttpContext.Current.Items.Contains(mostHelpfulRatingFeedbackIdKey))
				return;

			var feedbackId = (int)HttpContext.Current.Items[mostHelpfulRatingFeedbackIdKey];
			_feedbackList.Remove(_feedbackList.FirstOrDefault(x => x.Id == feedbackId));
		}
	}
}