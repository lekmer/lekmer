using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockProductLatestFeedbackListEntityMapper : LekmerBlockEntityMapper<IBlockProductLatestFeedbackList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductLatestFeedbackList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.NumberOfItems", item.NumberOfItems.ToString(CultureInfo.InvariantCulture));
		}
	}
}