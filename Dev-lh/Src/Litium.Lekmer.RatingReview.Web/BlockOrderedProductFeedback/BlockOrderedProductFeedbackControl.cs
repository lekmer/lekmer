using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockOrderedProductFeedbackControl : BlockControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		private readonly ILekmerOrderService _orderService;
		private readonly IProductService _productService;
		private readonly IRatingReviewUserLocatorService _ratingReviewUserLocatorService;
		private readonly IRatingReviewFeedbackService _ratingReviewFeedbackService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingService _ratingService;

		private OrderedProductFeedbackForm _form;
		private IOrderFull _order;
		private Dictionary<int, IProduct> _orderedProducts;
		private Dictionary<int, Collection<IRatingGroup>> _productRatingGroups;
		private Dictionary<int, Dictionary<int, Collection<IRating>>> _productRatingGroupRatings;

		private bool _isAllProductsRated;

		public BlockOrderedProductFeedbackControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IOrderService orderService,
			IProductService productService,
			IRatingReviewUserLocatorService ratingReviewUserLocatorService,
			IRatingReviewFeedbackService ratingReviewFeedbackService,
			IRatingGroupService ratingGroupService,
			IRatingService ratingService)
			: base(templateFactory)
		{
			_blockService = blockService;
			_orderService = (ILekmerOrderService)orderService;
			_productService = productService;
			_ratingReviewUserLocatorService = ratingReviewUserLocatorService;
			_ratingReviewFeedbackService = ratingReviewFeedbackService;
			_ratingGroupService = ratingGroupService;
			_ratingService = ratingService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}


		protected override BlockContent RenderCore()
		{
			Initialize();

			if (_order == null || _orderedProducts == null)
			{
				return new BlockContent();
			}

			var content = RenderContent();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, content.Render(), footer);
		}


		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);

			fragment.AddEntity(Block);

			return fragment.Render();
		}

		protected virtual Fragment RenderContent()
		{
			Fragment fragment = Template.GetFragment("Content");

			fragment.AddEntity(Block);
			fragment.AddVariable("Block.Title.Raw", Block.Title, VariableEncoding.None);

			fragment.AddVariable("OrderedProductFeedbackForm", ProcessFeedbackFormList(), VariableEncoding.None);

			fragment.AddCondition("IsAllProductsRated", _isAllProductsRated);

			return fragment;
		}

		protected virtual string ProcessFeedbackFormList()
		{
			Fragment fragment = Template.GetFragment("OrderedProductFeedbackForm");

			var stringBuilder = new StringBuilder();
			foreach (var feedback in _form.Feedbacks.Values)
			{
				stringBuilder.AppendLine(ProcessFeedbackForm(feedback.ProductId));
			}

			fragment.AddVariable("OrderedProductFeedbackList", stringBuilder.ToString(), VariableEncoding.None);
			
			_form.MapToForm(fragment);

			return fragment.Render();
		}

		protected virtual string ProcessFeedbackForm(int productId)
		{
			bool isFeedbackSended = false;

			var customer = _order.Customer;
			if (!_form.IsFormPostBack)
			{
				_form.SetDefaultValues(productId, customer);
			}

			_form.MapFromRequest(productId);

			ValidationResult validationResult = null;
			if (_form.IsFormPostBack && productId == _form.PostBackProductId)
			{
				validationResult = _form.Validate(_form.PostBackProductId);
				if (validationResult.IsValid)
				{
					var feedback = CreateFeedback();

					_ratingReviewFeedbackService.Insert(UserContext.Current, feedback);

					isFeedbackSended = true;

					_isAllProductsRated = _orderedProducts.Count == 1;
				}
			}

			if (!isFeedbackSended)
			{
				return RenderFeedbackForm(productId, validationResult);
			}
			
			return null;
		}

		protected virtual string RenderFeedbackForm(int productId, ValidationResult validationResult)
		{
			string validationError = null;
			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragment = Template.GetFragment("OrderedProductFeedbackList");

			fragment.AddEntity(_orderedProducts[productId]);

			fragment.AddVariable("Review", RenderReview(productId).Render(), VariableEncoding.None);
			fragment.AddVariable("RatingGroupList", RenderRatingGroupList(productId).Render(), VariableEncoding.None);

			fragment.AddVariable("ValidationError", validationError, VariableEncoding.None);
			_form.MapToFragmentFeedbackForm(productId, fragment);

			return fragment.Render();
		}

		protected virtual Fragment RenderReview(int productId)
		{
			Fragment fragment = Template.GetFragment("Review");

			fragment.AddVariable("Review.MessageMaxLength", "3000");

			_form.MapToFragmentReview(productId, fragment);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroupList(int productId)
		{
			Fragment fragment = Template.GetFragment("RatingGroupList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingGroup ratingGroup in _productRatingGroups[productId])
			{
				stringBuilder.AppendLine(RenderRatingGroup(ratingGroup, productId).Render());
			}

			fragment.AddVariable("Iterate:RatingGroup", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroup(IRatingGroup ratingGroup, int productId)
		{
			Fragment fragment = Template.GetFragment("RatingGroup");

			fragment.AddEntity(ratingGroup);
			fragment.AddVariable("RatingList", RenderRatingList(ratingGroup, productId).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingList(IRatingGroup ratingGroup, int productId)
		{
			Fragment fragment = Template.GetFragment("RatingList");

			var stringBuilder = new StringBuilder();

			foreach (IRating rating in _productRatingGroupRatings[productId][ratingGroup.Id])
			{
				stringBuilder.AppendLine(RenderRating(ratingGroup, rating, productId).Render());
			}

			fragment.AddVariable("Iterate:Rating", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRating(IRatingGroup ratingGroup, IRating rating, int productId)
		{
			Fragment fragment = Template.GetFragment("Rating");

			fragment.AddEntity(ratingGroup);
			fragment.AddEntity(rating);
			fragment.AddVariable("RatingItemList", RenderRatingItemList(rating, productId).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItemList(IRating rating, int productId)
		{
			Fragment fragment = Template.GetFragment("RatingItemList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingItem ratingItem in rating.RatingItems)
			{
				stringBuilder.AppendLine(RenderRatingItem(rating, ratingItem, productId).Render());
			}

			fragment.AddVariable("Iterate:RatingItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItem(IRating rating, IRatingItem ratingItem, int productId)
		{
			Fragment fragment = Template.GetFragment("RatingItem");

			fragment.AddEntity(rating);
			fragment.AddEntity(ratingItem);

			_form.MapToFragmentRatingItem(productId, fragment, ratingItem);

			return fragment;
		 }


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
		protected virtual void Initialize()
		{
			_orderedProducts = new Dictionary<int, IProduct>();
			_productRatingGroups = new Dictionary<int, Collection<IRatingGroup>>();
			_productRatingGroupRatings = new Dictionary<int, Dictionary<int, Collection<IRating>>>();

			// Get token from query.
			string tokenValue = Request.QueryString["token"];
			if (string.IsNullOrEmpty(tokenValue))
			{
				return;
			}

			// Try to conver token to Guid.
			Guid token;
			try
			{
				token = new Guid(tokenValue);
			}
			catch
			{
				return;
			}

			// Get order by token.
			_order = _orderService.GetByFeedbackToken(UserContext.Current, token);
			if (_order == null)
			{
				return;
			}

			// Get ordered products.
			foreach (var orderItem in _order.GetOrderItems())
			{
				if (orderItem.IsDeleted) continue;

				if (_orderedProducts.ContainsKey(orderItem.ProductId)) continue;

				var product = _productService.GetById(UserContext.Current, orderItem.ProductId);
				if (product == null)
				{
					product = ((ILekmerProductService)_productService).GetByIdWithoutStatusFilter(UserContext.Current, orderItem.ProductId, true);
					if (product == null) continue;
				}

				var productFeedback = _ratingReviewFeedbackService.GetByProductAndOrder(UserContext.Current, product.Id, _order.Id);
				if (productFeedback != null) continue;

				var ratingGroups = _ratingGroupService.GetAllByProduct(UserContext.Current, product.Id, product.CategoryId);
				if (ratingGroups == null || ratingGroups.Count <= 0) continue;

				bool hasRatings = false;
				foreach (var ratingGroup in ratingGroups)
				{
					var productRatings = _ratingService.GetAllByGroup(UserContext.Current, ratingGroup.Id);
					if (productRatings == null || productRatings.Count <= 0) continue;

					hasRatings = true;
					break;
				}

				if (hasRatings)
				{
					_productRatingGroups.Add(product.Id, ratingGroups);
					_orderedProducts.Add(product.Id, product);
				}
			}

			// Feedback added.
			_isAllProductsRated = _orderedProducts.Count <= 0;

			PopulateFeedbackForms();
		}

		protected virtual void PopulateFeedbackForms()
		{
			Dictionary<int, OrderedProductFeedback> feedbacks = new Dictionary<int, OrderedProductFeedback>();

			foreach (var orderedProduct in _orderedProducts)
			{
				Dictionary<int, IRating> ratings = new Dictionary<int, IRating>();

				foreach (var productRatingGroup in _productRatingGroups[orderedProduct.Key])
				{
					if (!_productRatingGroupRatings.ContainsKey(orderedProduct.Key))
					{
						_productRatingGroupRatings[orderedProduct.Key] = new Dictionary<int, Collection<IRating>>();
					}

					var productRatingGroupRatings = _productRatingGroupRatings[orderedProduct.Key];

					foreach (IRating rating in _ratingService.GetAllByGroup(UserContext.Current, productRatingGroup.Id))
					{
						if (!ratings.ContainsKey(rating.Id))
						{
							ratings[rating.Id] = rating;
							if (!productRatingGroupRatings.ContainsKey(productRatingGroup.Id))
							{
								productRatingGroupRatings[productRatingGroup.Id] = new Collection<IRating>();
							}
							productRatingGroupRatings[productRatingGroup.Id].Add(rating);
						}
					}
				}

				feedbacks.Add(orderedProduct.Key, new OrderedProductFeedback(orderedProduct.Key, "", "", "", "", "", ratings, ratings.Values.SelectMany(r => r.RatingItems).ToDictionary(ri => ri.Id)));
			}
			_form = CreateFeedbackForm();
			_form.Feedbacks = feedbacks;
		}

		protected virtual OrderedProductFeedbackForm CreateFeedbackForm()
		{
			string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrl());
			return new OrderedProductFeedbackForm(postUrl);
		}

		private IRatingReviewFeedbackRecord CreateFeedback()
		{
			IRatingReviewFeedbackRecord feedback = _ratingReviewFeedbackService.CreateRecord(UserContext.Current);
			_form.MapToFeedback(_form.PostBackProductId, feedback);
			feedback.ProductId = _form.PostBackProductId;
			feedback.OrderId = _order.Id;
			feedback.Review.Email = _order.Email;
			feedback.RatingReviewUserId = _ratingReviewUserLocatorService.GetRatingReviewUser(UserContext.Current, _order.CustomerId).Id;
			feedback.IPAddress = Request.UserHostAddress;

			return feedback;
		}
	}
}