using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockProductRatingSummaryControl : BlockProductPageControlBase<IBlockProductRatingSummary>
	{
		private readonly IBlockProductRatingSummaryService _blockProductRatingSummaryService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingService _ratingService;
		private readonly IRatingItemProductScoreService _ratingItemProductScoreService;

		private Collection<IRatingGroup> _ratingGroups;
		private Collection<IRating> _ratings;
		private Dictionary<int, Collection<IRating>> _ratingsByGroupDictionary;
		private Dictionary<int, IProductRatingSummaryInfo> _productRatingSummaries;

		public BlockProductRatingSummaryControl(
			ITemplateFactory templateFactory,
			IBlockProductRatingSummaryService blockProductRatingSummaryService,
			IRatingGroupService ratingGroupService,
			IRatingService ratingService,
			IRatingItemProductScoreService ratingItemProductScoreService)
			: base(templateFactory)
		{
			_blockProductRatingSummaryService = blockProductRatingSummaryService;
			_ratingService = ratingService;
			_ratingGroupService = ratingGroupService;
			_ratingItemProductScoreService = ratingItemProductScoreService;
		}

		protected override IBlockProductRatingSummary GetBlockById(int blockId)
		{
			return _blockProductRatingSummaryService.GetById(UserContext.Current, blockId);
		}


		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render product rating summary control on this page ]");
			}

			Initialize();

			var content = RenderContent();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, content.Render(), footer);
		}


		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);

			fragment.AddEntity(Block);

			return fragment.Render();
		}

		protected virtual Fragment RenderContent()
		{
			Fragment fragment = Template.GetFragment("Content");

			fragment.AddEntity(Block);

			fragment.AddVariable("RatingList", RenderRatingList().Render(), VariableEncoding.None);
			fragment.AddVariable("RatingGroupList", RenderRatingGroupList().Render(), VariableEncoding.None);

			return fragment;
		}


		protected virtual Fragment RenderRatingList()
		{
			Fragment fragment = Template.GetFragment("RatingList");

			var stringBuilder = new StringBuilder();
			foreach (IRating rating in _ratings)
			{
				stringBuilder.AppendLine(RenderRating(rating).Render());
			}

			fragment.AddVariable("Iterate:Rating", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroupList()
		{
			Fragment fragment = Template.GetFragment("RatingGroupList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingGroup ratingGroup in _ratingGroups)
			{
				if (_ratingsByGroupDictionary.ContainsKey(ratingGroup.Id))
				{
					stringBuilder.AppendLine(RenderRatingGroup(ratingGroup).Render());
				}
			}

			fragment.AddVariable("Iterate:RatingGroup", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroup(IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("RatingGroup");

			fragment.AddEntity(ratingGroup);
			fragment.AddVariable("RatingList", RenderRatingList(ratingGroup).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingList(IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("RatingList");

			var stringBuilder = new StringBuilder();
			foreach (IRating rating in _ratingsByGroupDictionary[ratingGroup.Id])
			{
				stringBuilder.AppendLine(RenderRating(rating, ratingGroup).Render());
			}

			fragment.AddVariable("Iterate:Rating", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRating(IRating rating)
		{
			return RenderRating(rating, null);
		}

		protected virtual Fragment RenderRating(IRating rating, IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("Rating");

			if (ratingGroup != null)
			{
				fragment.AddEntity(ratingGroup);
			}

			fragment.AddEntity(rating);

			IProductRatingSummaryInfo productRatingSummaryInfo = GetProductRatingSummary(rating.Id);

			fragment.AddVariable("Rating.AverageRatingScoreRaw", productRatingSummaryInfo.AverageRatingScore.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.AverageRatingScore", Math.Round(productRatingSummaryInfo.AverageRatingScore, 1).ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.AverageRatingItemId", productRatingSummaryInfo.AverageRatingItemId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.TotalNumberOfVotes", productRatingSummaryInfo.TotalNumberOfVotes.ToString(CultureInfo.InvariantCulture));

			fragment.AddVariable("RatingItemList", RenderRatingItemList(rating).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItemList(IRating rating)
		{
			Fragment fragment = Template.GetFragment("RatingItemList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingItem ratingItem in rating.RatingItems)
			{
				stringBuilder.AppendLine(RenderRatingItem(rating, ratingItem).Render());
			}

			fragment.AddVariable("Iterate:RatingItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItem(IRating rating, IRatingItem ratingItem)
		{
			Fragment fragment = Template.GetFragment("RatingItem");

			fragment.AddEntity(rating);
			fragment.AddEntity(ratingItem);

			IProductRatingSummaryInfo productRatingSummaryInfo = GetProductRatingSummary(rating.Id);
			IRatingItemSummary ratingItemSummary = GetRatingItemSummary(rating.Id, ratingItem.Id);

			fragment.AddCondition("RatingItem.IsAverage", productRatingSummaryInfo.AverageRatingItemId == ratingItem.Id);
			fragment.AddVariable("RatingItem.TotalNumberOfVotes", ratingItemSummary.TotalHitCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItem.PersentageNumberOfVotesRaw", ratingItemSummary.PercentageHitValue.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingItem.PersentageNumberOfVotes", Math.Round(ratingItemSummary.PercentageHitValue, 0).ToString(CultureInfo.InvariantCulture));

			return fragment;
		}


		protected virtual void Initialize()
		{
			_productRatingSummaries = new Dictionary<int, IProductRatingSummaryInfo>();

			_ratings = new Collection<IRating>();
			_ratingGroups = new Collection<IRatingGroup>();
			_ratingsByGroupDictionary = new Dictionary<int, Collection<IRating>>();

			var ratings = new Collection<IRating>();
			var ratingGroups = new Collection<IRatingGroup>();
			var ratingDictionaty = new Dictionary<int, IRating>();

			// Get ratings and groups linked to Block
			Collection<IBlockRatingGroup> blockRatingGroups = Block.BlockRatingGroups;
			Collection<IBlockRating> blockRatings = Block.BlockRatings;

			foreach (var blockRatingGroup in blockRatingGroups)
			{
				ratingGroups.Add(_ratingGroupService.GetById(blockRatingGroup.RatingGroupId));
			}

			foreach (var blockRating in blockRatings)
			{
				ratings.Add(_ratingService.GetById(UserContext.Current, blockRating.RatingId));
			}


			// Get rating groups linked to Product
			var productRatingGroups = _ratingGroupService.GetAllByProduct(UserContext.Current, Product.Id, Product.CategoryId);
			var productRatingDictionaty = new Dictionary<int, IRating>();
			var productRatingGroupDictionaty = productRatingGroups.ToDictionary(rg => rg.Id);

			foreach (IRatingGroup productRatingGroup in productRatingGroups)
			{
				foreach (IRating productRating in _ratingService.GetAllByGroup(UserContext.Current, productRatingGroup.Id))
				{
					productRatingDictionaty[productRating.Id] = productRating;
				}
			}

			// Filter
			foreach (var rating in ratings)
			{
				if (productRatingDictionaty.ContainsKey(rating.Id))
				{
					_ratings.Add(rating);
				}
			}

			foreach (var ratingGroup in ratingGroups)
			{
				if (productRatingGroupDictionaty.ContainsKey(ratingGroup.Id))
				{
					_ratingGroups.Add(ratingGroup);
				}
			}

			// Exclude duplicated ratings
			foreach (IRatingGroup ratingGroup in _ratingGroups)
			{
				_ratingsByGroupDictionary[ratingGroup.Id] = new Collection<IRating>();

				foreach (IRating rating in _ratingService.GetAllByGroup(UserContext.Current, ratingGroup.Id))
				{
					if (!ratingDictionaty.ContainsKey(rating.Id))
					{
						ratingDictionaty[rating.Id] = rating;
						_ratingsByGroupDictionary[ratingGroup.Id].Add(rating);
					}
				}
			}
		}

		protected virtual IProductRatingSummaryInfo GetProductRatingSummary(int ratingId)
		{
			if (_productRatingSummaries.ContainsKey(ratingId))
			{
				return _productRatingSummaries[ratingId];
			}

			IProductRatingSummaryInfo productRatingSummaryInfo = _ratingItemProductScoreService.GetProductRatingSummary(UserContext.Current, ratingId, Product.Id);

			_productRatingSummaries[ratingId] = productRatingSummaryInfo;

			return productRatingSummaryInfo;
		}

		protected virtual IRatingItemSummary GetRatingItemSummary(int ratingId, int ratingItemId)
		{
			return GetProductRatingSummary(ratingId).RatingItemSummaryCollection.FirstOrDefault(ri => ri.RatingItemId == ratingItemId);
		}
	}
}