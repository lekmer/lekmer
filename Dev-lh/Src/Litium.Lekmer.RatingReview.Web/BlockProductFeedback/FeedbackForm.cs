using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class FeedbackForm : ControlBase
	{
		private readonly string _postUrl;

		public FeedbackForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		// Properties

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, IRating> Ratings { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, IRatingItem> RatingItems { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public int ReviewMessageMaxLength
		{
			get { return 3000; }
		}

		public string ReviewAuthorName { get; set; }

		public string ReviewTitle { get; set; }

		public string ReviewMessage { get; set; }

		public string ReviewEmail { get; set; }

		public string ReviewCaptcha { get; set; }

		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals("review"); }
		}

		public virtual string PostUrl
		{
			get { return _postUrl; }
		}

		public virtual NameValueCollection RequestForm
		{
			get { return Request.Form; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string ReviewAuthorFormName
		{
			get { return "review-author"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string ReviewTitleFormName
		{
			get { return "review-title"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string ReviewMessageFormName
		{
			get { return "review-message"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string ReviewEmailFormName
		{
			get { return "review-email"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string ReviewCaptchaFormName
		{
			get { return "review-captcha"; }
		}

		protected Dictionary<int, int> SelectedRatingItems { get; private set; }

		// Methods

		public virtual ValidationResult Validate()
		{
			var validationResult = new ValidationResult();

			ValidateReview(validationResult);
			ValidateRatings(validationResult);

			return validationResult;
		}

		public virtual void SetDefaultValues(ICustomer customer)
		{
			if (customer != null && customer.CustomerInformation != null)
			{
				ReviewAuthorName = customer.CustomerInformation.FirstName + " " + customer.CustomerInformation.LastName;
			}
		}

		public virtual void MapFromRequest()
		{
			NameValueCollection form = RequestForm;

			ReviewAuthorName = form[ReviewAuthorFormName];
			ReviewTitle = form[ReviewTitleFormName];
			ReviewMessage = form[ReviewMessageFormName];
			ReviewEmail = form[ReviewEmailFormName];
			ReviewCaptcha = form[ReviewCaptchaFormName];

			// map ratings
			SelectedRatingItems = new Dictionary<int, int>();
			foreach (IRating rating in Ratings.Values)
			{
				var ratingFormName = GetRatingFormName(rating.Id);

				int? ratingItemId = form.GetInt32OrNull(ratingFormName);

				if (ratingItemId.HasValue)
				{
					SelectedRatingItems[rating.Id] = ratingItemId.Value;
				}
			}
		}

		public virtual void MapToFeedback(IRatingReviewFeedbackRecord ratingReviewFeedbackRecord)
		{
			if (!Validate().IsValid)
			{
				throw new ValidationException("Couldn't validate the review.");
			}

			var review = IoC.Resolve<IReview>();
			review.AuthorName = ReviewAuthorName;
			review.Title = ReviewTitle;
			review.Message = ReviewMessage;
			review.Email = ReviewEmail;

			ratingReviewFeedbackRecord.Review = review;

			ratingReviewFeedbackRecord.RatingItemProductVotes = new Collection<IRatingItemProductVote>();

			foreach (var selectedRatingItem in SelectedRatingItems)
			{
				var productVote = IoC.Resolve<IRatingItemProductVote>();
				productVote.RatingId = selectedRatingItem.Key;
				productVote.RatingItemId = selectedRatingItem.Value;

				ratingReviewFeedbackRecord.RatingItemProductVotes.Add(productVote);
			}
		}

		public virtual void MapToFragmentForm(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
		}

		public virtual void MapToFragmentReview(Fragment fragment)
		{
			fragment.AddVariable("Form.Review.Author.Name", ReviewAuthorFormName);
			fragment.AddVariable("Form.Review.Title.Name", ReviewTitleFormName);
			fragment.AddVariable("Form.Review.Message.Name", ReviewMessageFormName);
			fragment.AddVariable("Form.Review.Email.Name", ReviewEmailFormName);
			fragment.AddVariable("Form.Review.Captcha.Name", ReviewCaptchaFormName);           

			fragment.AddVariable("Form.Review.Author.Value", ReviewAuthorName);
			fragment.AddVariable("Form.Review.Title.Value", ReviewTitle);
			fragment.AddVariable("Form.Review.Message.Value", ReviewMessage);
			fragment.AddVariable("Form.Review.Email.Value", ReviewEmail);
			fragment.AddVariable("Form.Review.Captcha.Value", ReviewCaptcha);            

		}

		public virtual void MapToFragmentRatingItem(Fragment fragment, IRatingItem ratingItem)
		{
			bool itemSelected = false;

			if (SelectedRatingItems != null && SelectedRatingItems.ContainsKey(ratingItem.RatingId))
			{
				itemSelected = SelectedRatingItems[ratingItem.RatingId] == ratingItem.Id;
			}

			fragment.AddCondition("RatingItem.Selected", itemSelected);

			fragment.AddVariable("Form.RatingItem.Name", GetRatingFormName(ratingItem.RatingId));
			fragment.AddVariable("Form.RatingItem.Value", GetRatingItemFormValue(ratingItem.Id));
		}


		protected virtual string GetRatingFormName(int ratingId)
		{
			return "rating-" + ratingId.ToString(CultureInfo.InvariantCulture);
		}

		protected virtual string GetRatingItemFormValue(int ratingItemId)
		{
			return ratingItemId.ToString(CultureInfo.InvariantCulture);
		}

		protected virtual void ValidateReview(ValidationResult validationResult)
		{
			if (ReviewAuthorName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.AuthorNotProvided"));
			}

			if (ReviewTitle.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.TitleNotProvided"));
			}

			if (ReviewMessage.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.MessageNotProvided"));
			}

			if (ReviewMessage != null && ReviewMessage.Length > ReviewMessageMaxLength)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.MessageTooLong"));
			}

			if (!ReviewEmail.IsNullOrTrimmedEmpty())
			{
				if (!ValidationUtil.IsValidEmail(ReviewEmail))
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.EmailIncorrect"));
				}
			}
			if (!ReviewCaptcha.IsNullOrEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.CaptchaFailed"));
			}

		}

		protected virtual void ValidateRatings(ValidationResult validationResult)
		{
			bool ratingsWithoutSelection = false;
			bool ratingsWithInvalidSelection = false;

			foreach (IRating rating in Ratings.Values)
			{
				if (SelectedRatingItems.ContainsKey(rating.Id))
				{
					int ratingItemId = SelectedRatingItems[rating.Id];

					IRatingItem ratingItem = rating.RatingItems.FirstOrDefault(ri => ri.Id == ratingItemId);

					if (ratingItem == null)
					{
						ratingsWithInvalidSelection = true;
						break;
					}
				}
				else
				{
					ratingsWithoutSelection = true;
					break;
				}
			}

			if (ratingsWithoutSelection || ratingsWithInvalidSelection)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.RatingNotProvided"));
			}
		}
	}
}