﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.ProductFilter.Cache
{
	public sealed class BlockEsalesProductFilterCache : ScensumCacheBase<BlockEsalesProductFilterKey, IBlockEsalesProductFilter>
	{
		private static readonly BlockEsalesProductFilterCache _instance = new BlockEsalesProductFilterCache();

		private BlockEsalesProductFilterCache()
		{
		}

		public static BlockEsalesProductFilterCache Instance
		{
			get { return _instance; }
		}
	}

	public class BlockEsalesProductFilterKey : ICacheKey
	{
		public BlockEsalesProductFilterKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}