using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.ProductFilter.Cache
{
	public sealed class FilterProductCollectionCache : ScensumCacheBase<FilterProductKey, Collection<FilterProduct>>
	{
		// Singleton

		private static readonly FilterProductCollectionCache _instance = new FilterProductCollectionCache();

		private FilterProductCollectionCache()
		{
		}

		public static FilterProductCollectionCache Instance
		{
			get { return _instance; }
		}

		// End of singleton
	}

	public class FilterProductKey : ICacheKey
	{
		public FilterProductKey(int channelId)
		{
			ChannelId = channelId;
			Key = "c_" + channelId.ToString(CultureInfo.InvariantCulture);
		}

		public string Key { get; protected set; }

		public int ChannelId { get; protected set; }
	}
}