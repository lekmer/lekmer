using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter
{
	public class EsalesProductFilterService : IEsalesProductFilterService
	{
		private const string KeyPanelName = "panel-name";
		private const string DefaultPanelName = "Products";

		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ISizeService SizeService { get; private set; }

		public EsalesProductFilterService(
			IEsalesProductKeyService esalesProductKeyService,
			IProductService productService,
			ICategoryService categoryService,
			IBrandService brandService,
			ISizeService sizeService)
		{
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = (ILekmerProductService)productService;
			CategoryService = categoryService;
			BrandService = brandService;
			SizeService = sizeService;
		}

		public virtual ProductCollection GetProducts(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent)
		{
			string productPanelName = esalesModelComponent.TryGetParameterValue(KeyPanelName, DefaultPanelName);
			IPanelContent productPanelContent = esalesResponse.FindPanel(productPanelName);

			return FindProducts(context, productPanelContent);
		}

		public virtual IEsalesProductFilterFacet GetBlockFilterFacet(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent)
		{
			string productPanelName = esalesModelComponent.TryGetParameterValue(KeyPanelName, DefaultPanelName);
			IPanelContent productPanelContent = esalesResponse.FindPanel(productPanelName);

			IEsalesProductFilterFacet blockFilterFacet = GetFilterFacet(context, productPanelContent, "Facets (Block)");

			IPanelContent blockFacetPanelContent = esalesResponse.FindPanel("Facets (Block)");
			if (blockFacetPanelContent != null)
			{
				ICategoryTree categoryTree = CategoryService.GetAllAsTree(context);

				blockFilterFacet.MainCategories = FindCategories(blockFacetPanelContent, "main_category_id", categoryTree);
				blockFilterFacet.ParentCategories = FindCategories(blockFacetPanelContent, "parent_category_id", categoryTree);
				blockFilterFacet.Categories = FindCategories(blockFacetPanelContent, "category_id", categoryTree);

				blockFilterFacet.Brands = FindBrands(context, blockFacetPanelContent, "brand_id");

				blockFilterFacet.Sizes = FindSizes(context, blockFacetPanelContent, "size_id");
			}

			return blockFilterFacet;
		}

		public virtual IEsalesProductFilterFacet GetUserFilterFacet(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent)
		{
			string productPanelName = esalesModelComponent.TryGetParameterValue(KeyPanelName, DefaultPanelName);
			IPanelContent productPanelContent = esalesResponse.FindPanel(productPanelName);

			IEsalesProductFilterFacet userFilterFacet = GetFilterFacet(context, productPanelContent, "Facets (User)");

			return userFilterFacet;
		}


		protected virtual ProductCollection FindProducts(IUserContext context, IPanelContent productPanelContent)
		{
			if (productPanelContent != null)
			{
				var productsResult = productPanelContent.FindResult(EsalesResultType.Products) as IProductHits;
				var productsCountResult = productPanelContent.FindResult(EsalesResultType.Count) as ICountHits;

				if (productsResult != null)
				{
					var productIds = new ProductIdCollection(productsResult.ProductsInfo.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
					productIds.TotalCount = productsResult.ProductsInfo.Count;
					if (productsCountResult != null)
					{
						productIds.TotalCount = productsCountResult.TotalHitCount;
					}

					ProductCollection products = ProductService.PopulateViewProducts(context, productIds);

					Dictionary<int, string> tickets = productsResult.ProductsInfo.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

					foreach (IProduct product in products)
					{
						var lekmerProduct = (ILekmerProduct)product;
						lekmerProduct.EsalesTicket = tickets[product.Id];
					}

					return products;
				}
			}

			return new ProductCollection();
		}

		protected virtual CategoryCollection FindCategories(IPanelContent facetPanelContent, string categoryFacetPanelName, ICategoryTree categoryTree)
		{
			var categories = new CategoryCollection();

			IdCollection categoryIds = FindIds(facetPanelContent, categoryFacetPanelName);
			FillUpCategoriesByIds(categoryTree, categoryIds, categories);

			return categories;
		}

		protected virtual BrandCollection FindBrands(IUserContext context, IPanelContent facetPanelContent, string brandFacetPanelName)
		{
			IdCollection ids = FindIds(facetPanelContent, brandFacetPanelName);

			if (ids != null)
			{
				var brandIds = new BrandIdCollection(ids);
				return BrandService.PopulateBrands(context, brandIds);
			}

			return new BrandCollection();
		}

		protected virtual Collection<ISize> FindSizes(IUserContext context, IPanelContent facetPanelContent, string sizeFacetPanelName)
		{
			var sizes = new Collection<ISize>();
			IdCollection ids = FindIds(facetPanelContent, sizeFacetPanelName);
			if (ids != null)
			{
				var allSzes = SizeService.GetAll(context);
				if (allSzes == null) return sizes;

				Dictionary<int, int> idDict = ids.ToDictionary(id => id);
				foreach (var size in allSzes)
				{
					if (idDict.ContainsKey(size.Id))
					{
						sizes.Add(size);
					}
				}
			}
			return sizes;
		}


		protected virtual IEsalesProductFilterFacet GetFilterFacet(IUserContext context, IPanelContent productPanelContent, string facetPanelName)
		{
			var blockFilterFacet = IoC.Resolve<IEsalesProductFilterFacet>();

			if (productPanelContent == null)
			{
				return blockFilterFacet;
			}

			IPanelContent blockFacetPanelContent = productPanelContent.FindPanel(facetPanelName);
			if (blockFacetPanelContent != null)
			{
				FindAgeInterval(blockFilterFacet, blockFacetPanelContent);
				FindPriceInterval(blockFilterFacet, blockFacetPanelContent);

				blockFilterFacet.MainCategoryStats = FindIntegerValues(blockFacetPanelContent, "main_category_id");
				blockFilterFacet.ParentCategoryStats = FindIntegerValues(blockFacetPanelContent, "parent_category_id");
				blockFilterFacet.CategoryStats = FindIntegerValues(blockFacetPanelContent, "category_id");

				blockFilterFacet.BrandStats = FindIntegerValues(blockFacetPanelContent, "brand_id");

				blockFilterFacet.SizeStats = FindIntegerValues(blockFacetPanelContent, "size_id");
				blockFilterFacet.TagStats = FindIntegerValues(blockFacetPanelContent, "tag_id");
			}

			return blockFilterFacet;
		}


		protected virtual void FindAgeInterval(IEsalesProductFilterFacet filterFacet, IPanelContent esalesResponse)
		{
			Dictionary<int, int> ageFromMonths = FindIntegerValues(esalesResponse, "Facet Age From");
			Dictionary<int, int> ageToMonths = FindIntegerValues(esalesResponse, "Facet Age To");
			
			int? minAge = null;
			int? maxAge = null;

			if (ageFromMonths != null && ageFromMonths.Keys.Count > 0)
			{
				minAge = ageFromMonths.Keys.Min();
			}

			if (ageToMonths != null && ageToMonths.Keys.Count > 0)
			{
				maxAge = ageToMonths.Keys.Max();
			}

			filterFacet.AgeInterval.FromMonth = minAge;
			filterFacet.AgeInterval.ToMonth = maxAge;
		}

		protected virtual void FindPriceInterval(IEsalesProductFilterFacet filterFacet, IPanelContent esalesResponse)
		{
			Dictionary<decimal, int> minPrices = FindDecimalValues(esalesResponse, "Facet Price Min");
			Dictionary<decimal, int> maxPrices = FindDecimalValues(esalesResponse, "Facet Price Max");

			decimal? minPrice = null;
			decimal? maxPrice = null;

			if (minPrices != null && minPrices.Keys.Count > 0)
			{
				minPrice = minPrices.Keys.Min();
			}

			if (maxPrices != null && maxPrices.Keys.Count > 0)
			{
				maxPrice = maxPrices.Keys.Max();
			}

			filterFacet.PriceInterval.From = minPrice;
			filterFacet.PriceInterval.To = maxPrice;
		}


		protected virtual IdCollection FindIds(IPanelContent facetPanelContent, string panelName)
		{
			IPanelContent panelContent = facetPanelContent.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					var ids = new IdCollection(result.ValuesInfo.Select(c => int.Parse(c.Text, CultureInfo.InvariantCulture)));
					ids.TotalCount = result.ValuesInfo.Count;

					var countResult = panelContent.FindResult(EsalesResultType.Count) as ICountHits;
					if (countResult != null)
					{
						ids.TotalCount = countResult.TotalHitCount;
					}
					return ids;
				}
			}

			return null;
		}

		protected virtual Dictionary<int, int> FindIntegerValues(IPanelContent facetPanelContent, string panelName)
		{
			IPanelContent panelContent = facetPanelContent.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					return result.ValuesInfo.ToDictionary(c => int.Parse(c.Text, CultureInfo.InvariantCulture), c => c.Count);
				}
			}

			return new Dictionary<int, int>();
		}

		protected virtual Dictionary<decimal, int> FindDecimalValues(IPanelContent facetPanelContent, string panelName)
		{
			IPanelContent panelContent = facetPanelContent.FindPanel(panelName);
			if (panelContent != null)
			{
				var result = panelContent.FindResult(EsalesResultType.Values) as IValueHits;
				if (result != null)
				{
					return result.ValuesInfo.ToDictionary(c => decimal.Parse(c.Text.Replace('.', ','), CultureInfo.CurrentCulture), c => c.Count);
				}
			}

			return new Dictionary<decimal, int>();
		}


		protected virtual void FillUpCategoriesByIds(ICategoryTree categoryTree, IdCollection categoryIds, CategoryCollection categories)
		{
			if (categoryIds == null)
			{
				return;
			}

			foreach (int categoryId in categoryIds)
			{
				var categoryItem = categoryTree.FindItemById(categoryId);
				if (categoryItem != null && categoryItem.Category != null)
				{
					categories.Add(categoryItem.Category);
				}
			}
		}
	}
}