using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter.Mapper
{
	public class BlockEsalesProductFilterDataMapper : DataMapperBase<IBlockEsalesProductFilter>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockEsalesSetting> _blockEsalesSettingDataMapper;

		public BlockEsalesProductFilterDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockEsalesSettingDataMapper = DataMapperResolver.Resolve<IBlockEsalesSetting>(DataReader);
		}

		protected override IBlockEsalesProductFilter Create()
		{
			var block = _blockDataMapper.MapRow();

			var blockProductFilter = IoC.Resolve<IBlockEsalesProductFilter>();

			block.ConvertTo(blockProductFilter);

			blockProductFilter.DefaultCategoryId = MapNullableValue<int?>("BlockProductFilter.DefaultCategoryId");
			blockProductFilter.SecondaryTemplateId = MapNullableValue<int?>("BlockProductFilter.SecondaryTemplateId"); 
			blockProductFilter.DefaultPriceIntervalId = MapNullableValue<int?>("BlockProductFilter.DefaultPriceIntervalId");
			blockProductFilter.DefaultAgeIntervalId = MapNullableValue<int?>("BlockProductFilter.DefaultAgeIntervalId");
			blockProductFilter.DefaultSizeId = MapNullableValue<int?>("BlockProductFilter.DefaultSizeId");
			blockProductFilter.ProductListCookie = MapNullableValue<string>("BlockProductFilter.ProductListCookie");
			blockProductFilter.DefaultSort = MapNullableValue<string>("BlockProductFilter.DefaultSort");

			blockProductFilter.EsalesSetting = _blockEsalesSettingDataMapper.MapRow();

			blockProductFilter.Status = BusinessObjectStatus.Untouched;

			return blockProductFilter;
		}
	}
}