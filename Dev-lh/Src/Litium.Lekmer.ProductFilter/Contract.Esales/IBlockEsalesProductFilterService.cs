using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public interface IBlockEsalesProductFilterService
	{
		IBlockEsalesProductFilter GetById(IUserContext context, int blockId);
	}
}