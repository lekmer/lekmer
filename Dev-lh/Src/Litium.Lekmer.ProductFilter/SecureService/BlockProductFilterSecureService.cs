﻿using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Core;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Scensum.Foundation;
using Litium.Framework.Transaction;
using System;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;
using PrivilegeConstant = Litium.Scensum.Core.PrivilegeConstant;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.ProductFilter
{
	public class BlockProductFilterSecureService : IBlockProductFilterSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected BlockProductFilterRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockRatingSecureService BlockRatingSecureService { get; private set; }
		protected IProductTypeSecureService ProductTypeSecureService { get; private set; }

		public BlockProductFilterSecureService(
			BlockProductFilterRepository repository,
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockRatingSecureService blockRatingSecureService,
			IProductTypeSecureService productTypeSecureService)
		{
			Repository = repository;
			AccessValidator = accessValidator;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockRatingSecureService = blockRatingSecureService;
			ProductTypeSecureService = productTypeSecureService;
		}

		public virtual IBlockProductFilter Create()
		{
			var blockProductFilter = IoC.Resolve<IBlockProductFilter>();
			blockProductFilter.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductFilter.DefaultBrandIds = new Collection<int>();
			blockProductFilter.DefaultTagIds = new Collection<int>();
			blockProductFilter.TargetProductTypeIds = new Collection<int> { (int)ProductTypeEnum.Product, (int)ProductTypeEnum.Package }; // Target = Both by default

			blockProductFilter.Status = BusinessObjectStatus.New;

			return blockProductFilter;
		}

		public virtual IBlockProductFilter GetByIdSecure(int blockId)
		{
			var blockProductFilter = Repository.GetByIdSecure(blockId);
			blockProductFilter.DefaultBrandIds = Repository.GetAllBrandsByBlock(blockId);
			blockProductFilter.DefaultTagIds = Repository.GetAllTagsByBlock(blockId);
			blockProductFilter.TargetProductTypeIds = ProductTypeSecureService.GetIdAllByBlock(blockId);

			var ratings = BlockRatingSecureService.GetAllByBlock(blockId);
			if (ratings != null && ratings.Count > 0)
			{
				blockProductFilter.BlockRating = ratings[0];
			}

			return blockProductFilter;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductFilter blockProductFilter)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");
			if (blockProductFilter == null) throw new ArgumentNullException("blockProductFilter");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				blockProductFilter.Id = BlockSecureService.Save(systemUserFull, blockProductFilter);
				if (blockProductFilter.Id == -1)
				{
					return blockProductFilter.Id;
				}

				Repository.Save(blockProductFilter);

				Repository.DeleteAllBrands(blockProductFilter.Id);
				foreach (int brandId in blockProductFilter.DefaultBrandIds)
				{
					Repository.SaveBrand(blockProductFilter.Id, brandId);
				}

				Repository.DeleteAllTags(blockProductFilter.Id);
				foreach (int tagId in blockProductFilter.DefaultTagIds)
				{
					Repository.SaveTag(blockProductFilter.Id, tagId);
				}

				ProductTypeSecureService.SaveByBlock(systemUserFull, blockProductFilter.Id, blockProductFilter.TargetProductTypeIds);

				var ratings = new Collection<IBlockRating>();
				if (blockProductFilter.BlockRating != null)
				{
					ratings.Add(blockProductFilter.BlockRating);
				}
				BlockRatingSecureService.SaveAll(systemUserFull, blockProductFilter.Id, ratings);

				transaction.Complete();
			}

			BlockProductFilterCache.Instance.Flush();

			return blockProductFilter.Id;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockProductFilter = Create();
			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = contentAreaId;
			blockProductFilter.BlockTypeId = blockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockProductFilter.Title = title;
			blockProductFilter.TemplateId = null;
			blockProductFilter.Id = Save(systemUserFull, blockProductFilter);
			return blockProductFilter;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				ProductTypeSecureService.DeleteByBlock(systemUserFull, blockId);
				Repository.DeleteAllBrands(blockId);
				Repository.DeleteAllTags(blockId);
				BlockRatingSecureService.Delete(systemUserFull, blockId);
				Repository.Delete(blockId);
				transaction.Complete();
			}

			BlockProductFilterCache.Instance.Flush();
		}

		public virtual void DeleteTag(ISystemUserFull systemUserFull, int tagId)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			Repository.DeleteTag(tagId);

			BlockProductFilterCache.Instance.Flush();
		}
	}
}
