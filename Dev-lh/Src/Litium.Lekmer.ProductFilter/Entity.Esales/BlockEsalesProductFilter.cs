﻿using System;
using Litium.Lekmer.Esales;

namespace Litium.Lekmer.ProductFilter
{
	[Serializable]
	public class BlockEsalesProductFilter : BlockProductFilter, IBlockEsalesProductFilter
	{
		private IBlockEsalesSetting _esalesSetting;
		public IBlockEsalesSetting EsalesSetting
		{
			get { return _esalesSetting; }
			set
			{
				CheckChanged(_esalesSetting, value);
				_esalesSetting = value;
			}
		}
	}
}