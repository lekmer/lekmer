using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.ProductFilter.Mapper;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter.Repository
{
	public class FilterProductRepository
	{
		protected virtual DataMapperBase<FilterProduct> CreateDataMapper(IDataReader dataReader)
		{
			return new FilterProductDataMapper(dataReader);
		}

		public Collection<FilterProduct> GetAll(int channelId, ICustomer customer)
		{
			var dbSettings = new DatabaseSetting("FilterProductRepository.GetAll");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?) customer.Id : null, SqlDbType.Int)
				};

			Collection<FilterProduct> filterProducts;
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pFilterProductGetAll]", parameters, dbSettings))
			{
				filterProducts = CreateDataMapper(dataReader).ReadMultipleRows();
			}
			return filterProducts;
		}
	}
}