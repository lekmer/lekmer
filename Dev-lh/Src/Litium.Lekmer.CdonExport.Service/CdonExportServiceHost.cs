﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using System.Timers;
using Litium.Scensum.Foundation;
using log4net;
using log4net.Config;

namespace Litium.Lekmer.CdonExport.Service
{
	public partial class CdonExportServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private ICdonExportSetting _cdonExportSetting;
		private Timer _timerFullExport;
		private Timer _timerIncrementalExport;

		protected ICdonExportSetting CdonExportSetting
		{
			get { return _cdonExportSetting ?? (_cdonExportSetting = IoC.Resolve<ICdonExportSetting>()); }
		}

		public CdonExportServiceHost()
		{
			InitializeComponent();

			XmlConfigurator.Configure();
			AppDomain.CurrentDomain.UnhandledException += UnhandledException;
		}


		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			Exception serviceError = null;
			try
			{
				_log.Info("Starting Lekmer CdonExport Service.");

				ScheduleFullExport();
				ScheduleIncrementalExport();

				_log.Info("Lekmer CdonExport Service started successfuly.");
			}
			catch (Exception exception)
			{
				serviceError = new Exception("Unable to start service. See inner exception for details.", exception);
			}
			finally
			{
				if (serviceError != null)
				{
					try
					{
						_log.Error("Failed to start Lekmer CdonExport Service.", serviceError);
					}
					catch (Exception exception)
					{

						Trace.WriteLine("Failed to start service. An error occurred:");
						Trace.WriteLine(serviceError.ToString());
						Trace.WriteLine("Failed to log error:");
						Trace.WriteLine(exception.ToString());
					}
					finally
					{
						Stop();
					}
				}
			}
		}

		protected override void OnStop()
		{
			try
			{
				_timerFullExport.Stop();

				_log.Info("Lekmer CdonExport Service stopped successfuly.");

				base.OnStop();
			}
			catch (Exception exception)
			{
				_log.Error("Error occurred while stopping Lekmer CdonExport Service.", exception);
			}
		}

		protected virtual void UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			try
			{
				_timerFullExport.Stop();

				_log.Error("ERROR occurred during Lekmer CdonExport Service execution. Stopping service", e.ExceptionObject is Exception ? e.ExceptionObject as Exception : null);
			}
			finally
			{
				Stop();
			}
		}


		protected virtual void ScheduleFullExport()
		{
			_timerFullExport = new Timer();

			_timerFullExport.Elapsed += OnTimerFullExportElapsed;
			_timerFullExport.Interval = CalculateIntervalToNextFullExportExecution(true);
			_timerFullExport.AutoReset = false;

			_timerFullExport.Start();
		}
		protected virtual int CalculateIntervalToNextFullExportExecution(bool isFirst)
		{
			DateTime now = DateTime.Now;

			if (CdonExportSetting.ExportInterval > 0)
			{
				var interval = new TimeSpan(0, Math.Abs(CdonExportSetting.ExportInterval), 0);
				if (interval.TotalMinutes < 1)
				{
					interval = TimeSpan.FromMinutes(1);
				}

				if (!isFirst)
				{
					return (int)(interval).TotalMilliseconds;
				}

				DateTime startTime = now.Date
					.AddHours(Math.Abs(CdonExportSetting.ExecutionHour))
					.AddMinutes(Math.Abs(CdonExportSetting.ExecutionMinute));

				if (startTime > now)
				{
					return (int)(startTime - now).TotalMilliseconds;
				}

				double minutes = (now - startTime).TotalMinutes % interval.TotalMinutes;

				return (int)(interval - TimeSpan.FromMinutes(minutes)).TotalMilliseconds;
			}

			DateTime timeX = DateTime.Today
				.AddHours(CdonExportSetting.ExecutionHour)
				.AddMinutes(CdonExportSetting.ExecutionMinute);

			// If it's already past, wait until tomorrow.
			if (now > timeX)
			{
				timeX = timeX.AddDays(1);
			}

			int msToTimeX = (int)((timeX - now).TotalMilliseconds);

			return msToTimeX;
		}
		protected virtual void OnTimerFullExportElapsed(object sender, ElapsedEventArgs e)
		{
			ExecuteFullExport();

			_timerFullExport.Interval = CalculateIntervalToNextFullExportExecution(false);
			_timerFullExport.Start();
		}
		protected virtual void ExecuteFullExport()
		{
			try
			{
				var exporter = IoC.Resolve<IExporter>("CdonFullExporter");
				exporter.Execute();

				_log.Info("Lekmer CdonExport FULL finished successfuly.");
			}
			catch (Exception exception)
			{
				_log.Error("Lekmer CdonExport FULL failed.", exception);
			}
		}


		protected virtual void ScheduleIncrementalExport()
		{
			_timerIncrementalExport = new Timer();

			_timerIncrementalExport.Elapsed += OnTimerIncrementalExportElapsed;
			_timerIncrementalExport.Interval = CalculateIntervalToNextIncrementalExportExecution(true);
			_timerIncrementalExport.AutoReset = false;

			_timerIncrementalExport.Start();
		}
		protected virtual int CalculateIntervalToNextIncrementalExportExecution(bool isFirst)
		{
			var interval = new TimeSpan(0, Math.Abs(CdonExportSetting.IncrementalExportInterval), 0);

			if (interval.TotalMinutes < 1)
			{
				interval = TimeSpan.FromMinutes(1);
			}

			if (!isFirst)
			{
				return (int)(interval).TotalMilliseconds;
			}

			DateTime now = DateTime.Now;
			DateTime startTime = now.Date
				.AddHours(Math.Abs(CdonExportSetting.IncrementalExportExecutionHour))
				.AddMinutes(Math.Abs(CdonExportSetting.IncrementalExportExecutionMinute));

			if (startTime > now)
			{
				return (int)(startTime - now).TotalMilliseconds;
			}

			double minutes = (now - startTime).TotalMinutes % interval.TotalMinutes;

			return (int) (interval - TimeSpan.FromMinutes(minutes)).TotalMilliseconds;
		}
		protected virtual void OnTimerIncrementalExportElapsed(object sender, ElapsedEventArgs e)
		{
			ExecuteIncrementalExport();

			_timerIncrementalExport.Interval = CalculateIntervalToNextIncrementalExportExecution(false);
			_timerIncrementalExport.Start();
		}
		protected virtual void ExecuteIncrementalExport()
		{
			try
			{
				var exporter = IoC.Resolve<IExporter>("CdonIncrementalExporter");
				exporter.Execute();

				_log.Info("Lekmer CdonExport INCREMENTAL finished successfuly.");
			}
			catch (Exception exception)
			{
				_log.Error("Lekmer CdonExport INCREMENTAL failed.", exception);
			}
		}


		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}