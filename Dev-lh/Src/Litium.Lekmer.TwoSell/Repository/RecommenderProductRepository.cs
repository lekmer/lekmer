﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.TwoSell.Repository
{
	public class RecommenderProductRepository
	{
		public virtual ProductIdCollection GetRecommendationProductIdBySessionAndProduct(string sessionId, int productId, int itemsToReturn)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SessionId", sessionId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ItemsToReturn", itemsToReturn, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RecommenderProductRepository.GetRecommendationProductIdBySessionAndProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pRecommendationProductGetIdBySessionAndProduct]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}

				if (productIds.Count > 0)
				{
					return productIds;
				}

				while (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(0));
					}

					if (productIds.Count > 0)
					{
						return productIds;
					}
				}
			}

			return productIds;
		}
	}
}
