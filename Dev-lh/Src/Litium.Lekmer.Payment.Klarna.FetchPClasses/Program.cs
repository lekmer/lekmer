﻿using System;
using System.IO;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna
{
	public class Program
	{
		static void Main()
		{
			Console.WriteLine("Get channels");
			var channelService = (ILekmerChannelService)IoC.Resolve<IChannelService>();
			var channels = channelService.GetAll();
			foreach (var channel in channels)
			{
				if (channel.CommonName == "Europe") continue;

				Console.WriteLine("Process channel - " + channel.CommonName);

				var setting = CreateKlarnaSetting(channel.CommonName);
				var date = DateTime.Now.ToShortDateString().Replace("/", "");

				var pcuri = setting.PCURI;
				var pcBackupUri = pcuri + "_" + date + ".bak";

				if (File.Exists(pcuri))
				{
					File.Copy(pcuri, pcBackupUri, true);
				}
				else
				{
					using (File.Create(pcuri)) { }
				}

				try
				{
					IKlarnaClient klarnaClient = KlarnaFactory.CreateKlarnaClient(channel.CommonName);

					klarnaClient.FetchPClasses();

					//IKlarnaClient klarnaClientAdv = KlarnaFactory.CreateKlarnaAdvancedClient(channel.CommonName);

					//klarnaClientAdv.FetchPClasses();

					Console.WriteLine("Finished channel - " + channel.CommonName);
				}
				catch (Exception e)
				{
					if (File.Exists(pcBackupUri))
					{
						File.Copy(pcBackupUri, pcuri, true);
					}

					Console.WriteLine("ERROR " + e.StackTrace);
					Console.WriteLine("Failed to update klarna - " + e.Message);
					Console.WriteLine("Press key to exit");
					Console.Read();
				}
			}

			Console.WriteLine("Press key to exit");
			Console.Read();

		}

		public static IKlarnaSetting CreateKlarnaSetting(string channelCommonName)
		{
			var klarnaSetting = IoC.Resolve<IKlarnaSetting>();
			klarnaSetting.Initialize(channelCommonName);
			return klarnaSetting;
		}
	}
}