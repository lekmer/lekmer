﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector.Repository
{
	public class CollectorTransactionRepository
	{
		public virtual int CreateGetAddress(int storeId, int transactionType, string civicNumber, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("CollectorTransactionRepository.CreateGetAddress");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("StoreId", storeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CivicNumber", civicNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pCollectorTransactionCreateGetAddress]", parameters, dbSettings);
		}

		public virtual int CreateAddInvoice(int storeId, int transactionType, string civicNumber, int orderId, string productCode, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("CollectorTransactionRepository.CreateAddInvoice");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("StoreId", storeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CivicNumber", civicNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductCode", productCode, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pCollectorTransactionCreateAddInvoice]", parameters, dbSettings);
		}

		public virtual int CreateInvoiceNotification(int storeId, int transactionType, string queryString, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("CollectorTransactionRepository.CreateInvoiceNotification");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("StoreId", storeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("ResponseContent", queryString, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pCollectorTransactionCreateInvoiceNotification]", parameters, dbSettings);
		}

		public virtual void SaveResult(ICollectorResult collectorResult)
		{
			var dbSettings = new DatabaseSetting("CollectorTransactionRepository.SaveResult");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", collectorResult.TransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusCode", collectorResult.StatusCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("ErrorMessage", collectorResult.ErrorMessage, SqlDbType.Int),
					ParameterHelper.CreateParameter("FaultCode", collectorResult.FaultCode, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("FaultMessage", collectorResult.FaultMessage, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", collectorResult.Duration, SqlDbType.BigInt),
					ParameterHelper.CreateParameter("ResponseContent", collectorResult.ResponseContent, SqlDbType.NVarChar)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorTransactionSaveResult]", parameters, dbSettings);
		}

		public virtual void SaveAddInvoiceResponse(ICollectorAddInvoiceResult collectorAddInvoiceResult)
		{
			var dbSettings = new DatabaseSetting("CollectorTransactionRepository.SaveAddInvoiceResponse");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", collectorAddInvoiceResult.TransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusCode", collectorAddInvoiceResult.StatusCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("AvailableReservationAmount", collectorAddInvoiceResult.AvailableReservationAmount, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("InvoiceNo", collectorAddInvoiceResult.InvoiceNo, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("InvoiceStatus", collectorAddInvoiceResult.InvoiceStatus, SqlDbType.Int),
					ParameterHelper.CreateParameter("InvoiceUrl", collectorAddInvoiceResult.InvoiceUrl, SqlDbType.Int),
					ParameterHelper.CreateParameter("ErrorMessage", collectorAddInvoiceResult.ErrorMessage, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("FaultCode", collectorAddInvoiceResult.FaultCode, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("FaultMessage", collectorAddInvoiceResult.FaultMessage, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", collectorAddInvoiceResult.Duration, SqlDbType.BigInt),
					ParameterHelper.CreateParameter("ResponseContent", collectorAddInvoiceResult.ResponseContent, SqlDbType.NVarChar)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorTransactionSaveAddInvoiceResponse]", parameters, dbSettings);
		}

		public virtual void SaveInvoiceNotificationResult(ICollectorNotificationResult collectorNotificationResult)
		{
			var dbSettings = new DatabaseSetting("CollectorTransactionRepository.SaveInvoiceNotificationResult");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", collectorNotificationResult.TransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("InvoiceNo", collectorNotificationResult.InvoiceNo, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("OrderId", collectorNotificationResult.OrderNo, SqlDbType.Int),
					ParameterHelper.CreateParameter("InvoiceStatus", collectorNotificationResult.InvoiceStatus, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorTransactionSaveInvoiceNotificationResult]", parameters, dbSettings);
		}
	}
}
