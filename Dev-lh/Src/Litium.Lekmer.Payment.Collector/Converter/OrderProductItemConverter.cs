﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Collector
{
	public class OrderProductItemConverter : IOrderItemConverter
	{
		public virtual Collection<InvoiceRow> Convert(IOrderItem orderItem, bool taxFreeZone)
		{
			var lekmerOrderItem = (ILekmerOrderItem)orderItem;

			string rowDescription = ParameterConverter.FormatParameter(lekmerOrderItem.Title, 50);
			decimal unitPrice = ParameterConverter.FormatParameter(lekmerOrderItem.ActualPrice.IncludingVat, 2);
			decimal vatPercent = ParameterConverter.FormatParameter(lekmerOrderItem.GetOriginalVatPercent(), 0);

			if (taxFreeZone) // ExclVat
			{
				unitPrice = ParameterConverter.FormatParameter(lekmerOrderItem.ActualPrice.ExcludingVat, 2);
				vatPercent = 0m;
			}

			string erpId = lekmerOrderItem.OrderItemSize.SizeId.HasValue ? lekmerOrderItem.OrderItemSize.ErpId : lekmerOrderItem.ErpId + "-000";

			var invoiceItem = new InvoiceRow
			{
				ArticleId = erpId,
				Description = rowDescription,
				Quantity = lekmerOrderItem.Quantity,
				UnitPrice = unitPrice, // Price per unit incl. VAT.
				VAT = vatPercent, //VAT in percent.
			};

			return new Collection<InvoiceRow> { invoiceItem };
		}
	}
}
