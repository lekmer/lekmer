﻿using System;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Payment.Collector
{
	public static class ParameterConverter
	{
		public static string FormatParameter(string value, int maxLength)
		{
			if (value.HasValue() && value.Length > maxLength)
			{
				return value.Substring(0, maxLength);
			}

			return value;
		}

		public static decimal FormatParameter(decimal value, int scale)
		{
			return Math.Round(value, scale, MidpointRounding.AwayFromZero);
		}
	}
}
