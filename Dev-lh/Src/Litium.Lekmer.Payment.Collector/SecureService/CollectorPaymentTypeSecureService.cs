﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Collector.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorPaymentTypeSecureService : ICollectorPaymentTypeSecureService
	{
		protected CollectorPaymentTypeRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public CollectorPaymentTypeSecureService(
			CollectorPaymentTypeRepository repository,
			IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public Collection<ICollectorPaymentType> GetAllByChannel(IChannel channel)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByChannel(channel.Id);
		}

		public void Delete(ISystemUserFull systemUserFull, int collectorPaymentTypeId)
		{
			Repository.EnsureNotNull();

			//AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.ToolsCollector);

			Repository.Delete(collectorPaymentTypeId);
		}

		public int Save(ISystemUserFull systemUserFull, ICollectorPaymentType collectorPaymentType)
		{
			Repository.EnsureNotNull();

			//AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.ToolsCollector);

			return Repository.Save(collectorPaymentType);
		}
	}
}
