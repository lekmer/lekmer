using Litium.Scensum.Customer;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorAddress : Address, ICollectorAddress
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public bool IsCompany { get; set; }
		public string CompanyName { get; set; }
	}
}