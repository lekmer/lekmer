﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Payment.Collector.Cache
{
	public sealed class CollectorPaymentTypesCache : ScensumCacheBase<CollectorPaymentTypeKey, Collection<ICollectorPaymentType>>
	{
		#region Singleton

		private static readonly CollectorPaymentTypesCache _instance = new CollectorPaymentTypesCache();

		private CollectorPaymentTypesCache()
		{
		}

		public static CollectorPaymentTypesCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class CollectorPaymentTypeKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public CollectorPaymentTypeKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}