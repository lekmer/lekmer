﻿using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Lekmer.Payment.Collector.Setting;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorAddInvoiceRequest
	{
		public IInvoiceServiceV31 InvoiceServiceClient { get; set; }
		public ICollectorSetting CollectorSetting { get; set; }
		public int OrderId { get; set; }
		public string ProductCode { get; set; }
		public AddInvoiceRequest Request { get; set; }
	}
}
