using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DataExport
{
	public interface IProductPriceInfo : IBusinessObjectBase
	{
		int ChannelId { get; set; }
		int ProductId { get; set; }
		decimal PriceIncludingVat { get; set; }
		decimal PriceExcludingVat { get; set; }
		decimal? DiscountPriceIncludingVat { get; set; }
		decimal? DiscountPriceExcludingVat { get; set; }
	}
}