using System.Collections.ObjectModel;
using Litium.Lekmer.Core.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public class SubDomainSecureService : ISubDomainSecureService
	{
		protected virtual SubDomainRepository Repository { get; private set; }

		public SubDomainSecureService(SubDomainRepository subDomainRepository)
		{
			Repository = subDomainRepository;
		}

		public virtual Collection<ISubDomain> GetAllByContentType(int contentTypeId)
		{
			return Repository.GetAllByContentType(contentTypeId);
		}

		public virtual Collection<ISubDomain> GetAllByContentType(IChannel channel, int contentTypeId)
		{
			return Repository.GetAllByChannelAndContentType(channel, contentTypeId);
		}
	}
}