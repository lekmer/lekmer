﻿using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Scensum.Review
{
	public sealed class BlockProductReviewCache : ScensumCacheBase<BlockProductReviewKey, IBlockProductReview>
	{
		#region Singleton

		private static readonly BlockProductReviewCache _instance = new BlockProductReviewCache();

		private BlockProductReviewCache()
		{
		}

		public static BlockProductReviewCache Instance
		{
			get { return _instance; }
		}

		#endregion

		public void Remove(int blockId)
		{
			foreach (var channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockProductReviewKey(channel.Id, blockId));
			}
		}
	}

	public class BlockProductReviewKey : ICacheKey
	{
		public BlockProductReviewKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}