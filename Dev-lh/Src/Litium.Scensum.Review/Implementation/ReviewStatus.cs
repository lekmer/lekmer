using System;

namespace Litium.Scensum.Review
{
	[Serializable]
	public class ReviewStatus : IReviewStatus
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string CommonName { get; set; }
	}
}