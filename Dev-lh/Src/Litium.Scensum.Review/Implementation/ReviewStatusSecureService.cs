using System.Collections.ObjectModel;
using Litium.Scensum.Review.Repository;

namespace Litium.Scensum.Review
{
	public class ReviewStatusSecureService : IReviewStatusSecureService
	{
		protected ReviewStatusRepository Repository { get; private set; }

		public ReviewStatusSecureService(ReviewStatusRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IReviewStatus> GetAll()
		{
			return Repository.GetAll();
		}
	}
}