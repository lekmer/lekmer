﻿namespace Litium.Scensum.Review
{
	public interface IReviewSummary
	{
		decimal? AverageRating { get; set; }

		int ReviewCount { get; set; }
	}
}