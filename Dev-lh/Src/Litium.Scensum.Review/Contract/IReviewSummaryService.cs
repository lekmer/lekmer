﻿using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core;

namespace Litium.Scensum.Review
{
	public interface IReviewSummaryService
	{
		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		IReviewSummary GetByProduct(IUserContext context, int productId);
	}
}