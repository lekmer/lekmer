using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Scensum.Review
{
	public class ReviewCollection : Collection<IReview>
	{
		public ReviewCollection()
		{
		}

		public ReviewCollection(IList<IReview> list) : base(list)
		{
		}

		public ReviewCollection(IEnumerable<IReview> list) : base(new List<IReview>(list))
		{
		}

		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		public int TotalCount { get; set; }
	}
}