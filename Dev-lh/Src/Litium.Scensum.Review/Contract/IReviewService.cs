using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core;

namespace Litium.Scensum.Review
{
	public interface IReviewService
	{
		IReview Create(IUserContext context);

		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		ReviewCollection GetAllByProduct(IUserContext context, int productId, int pageNumber, int pageSize);

		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		ReviewCollection GetAllByProduct(IUserContext context, int productId, int pageNumber, int pageSize, bool cacheEnabled);

		int Save(IUserContext context, IReview review);
	}
}