namespace Litium.Lekmer.Common
{
	public interface ICommonSession
	{
		bool IsDesktop { get; }
		bool IsMobile { get; }
		bool IsViewportDesktop { get; }
		bool IsViewportMobile { get; }

		void ResetDetectedDevice();
		void SetDevice(string deviceType);
	}
}