﻿namespace Litium.Lekmer.Common
{
	public enum BlockTypeInfo
	{
		None = 0,
		//TODO: Add all other block types if necessary
		BrandList,
		HelloWorld,
		ProductTopList,
		ProductFilter,
		RichText,
		ProductRelationList,
		EsalesRecommendV2,
		EsalesTopSellersV2
	}
}
