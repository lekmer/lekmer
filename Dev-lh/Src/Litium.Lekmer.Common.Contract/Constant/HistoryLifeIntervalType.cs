﻿namespace Litium.Lekmer.Common
{
	public enum HistoryLifeIntervalType
	{
		None = 0,
		Immortal = 1,
		Week = 2,
		Month = 3,
		Year = 4,
		Expired = 5
	}
}