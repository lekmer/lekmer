﻿using log4net.Appender;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Litium.Lekmer.Utils.log4net
{
    public class ExtendedSmtpAppender : SmtpPickupDirAppender
    {
        protected override void SendBuffer(LoggingEvent[] events)
        {
            PrepareSubject(events); 

            base.SendBuffer(events);
        }
        protected virtual void PrepareSubject(ICollection<LoggingEvent> events)
        {
            if (string.IsNullOrEmpty(Subject) || (!Subject.Contains(Environment.MachineName)))
            {
                Subject = string.Format("{0} {1}", Environment.MachineName, Subject);
            }
        }
    }
}
