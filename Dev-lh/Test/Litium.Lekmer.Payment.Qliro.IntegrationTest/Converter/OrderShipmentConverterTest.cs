﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class OrderShipmentConverterTest
	{
		[Test]
		[Category("IoC")]
		public void OrderShipmentConverter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderShipmentConverter>();

			Assert.IsInstanceOf<IOrderShipmentConverter>(service);
			Assert.IsInstanceOf<OrderShipmentConverter>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderShipmentConverter_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderShipmentConverter>();
			var service2 = IoC.Resolve<IOrderShipmentConverter>();

			Assert.AreEqual(service1, service2);
		}
	}
}