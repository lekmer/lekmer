﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class OrderItemsConverterTest
	{
		[Test]
		[Category("IoC")]
		public void OrderItemsConverter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderItemsConverter>();

			Assert.IsInstanceOf<IOrderItemsConverter>(service);
			Assert.IsInstanceOf<OrderItemsConverter>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderItemsConverter_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderItemsConverter>();
			var service2 = IoC.Resolve<IOrderItemsConverter>();

			Assert.AreEqual(service1, service2);
		}
	}
}