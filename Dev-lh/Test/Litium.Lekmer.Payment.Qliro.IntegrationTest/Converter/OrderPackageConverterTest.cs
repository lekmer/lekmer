﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class OrderPackageConverterTest
	{
		[Test]
		[Category("IoC")]
		public void OrderPackageConverter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderPackageConverter>();

			Assert.IsInstanceOf<IOrderPackageConverter>(service);
			Assert.IsInstanceOf<OrderPackageConverter>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderPackageConverter_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderPackageConverter>();
			var service2 = IoC.Resolve<IOrderPackageConverter>();

			Assert.AreEqual(service1, service2);
		}
	}
}