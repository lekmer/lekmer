﻿using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Setting
{
	[TestFixture]
	public class QliroPendingIntervalSettingTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPendingIntervalSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroPendingIntervalSetting>();

			Assert.IsInstanceOf<IQliroPendingIntervalSetting>(service);
			Assert.IsInstanceOf<IQliroPendingIntervalSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroPendingIntervalSetting_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IQliroPendingIntervalSetting>();
			var service2 = IoC.Resolve<IQliroPendingIntervalSetting>();

			Assert.AreEqual(service1, service2);
		}
	}
}