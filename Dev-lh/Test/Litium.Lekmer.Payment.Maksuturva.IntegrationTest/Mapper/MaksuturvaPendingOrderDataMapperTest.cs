﻿using System.Data;
using Litium.Lekmer.Payment.Maksuturva.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Mapper
{
	[TestFixture]
	public class MaksuturvaPendingOrderDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrderDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IMaksuturvaPendingOrder>(_dataReader);

			Assert.IsInstanceOf<MaksuturvaPendingOrderDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrderDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IMaksuturvaPendingOrder>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IMaksuturvaPendingOrder>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}