﻿using System;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Setting
{
	[TestFixture]
	public class MaksuturvaSettingTest
	{
		private static IMaksuturvaSetting _maksuturvaSetting;
 
		[SetUp]
		public void SetUp()
		{
			_maksuturvaSetting = IoC.Resolve<IMaksuturvaSetting>();
			_maksuturvaSetting.Initialize("Finland");
		}

		[Test]
		public void GetOrderCheckDate_PendingStatus_GetDate()
		{
			//<Setting Name="PendingOrderCheckInterval">120:5;120:60;-1:1440</Setting>
			/*
			var currentTime = DateTime.Now;
			DateTime nextCheckDate;
			DateTime firstCheck;

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("PaymentPending", null, currentTime);
			Assert.AreEqual(currentTime.AddMinutes(5), nextCheckDate);

			firstCheck = nextCheckDate;

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("PaymentPending", firstCheck, currentTime.AddMinutes(126));
			Assert.AreEqual(currentTime.AddMinutes(186), nextCheckDate);

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("PaymentPending", firstCheck, currentTime.AddMinutes(253));
			Assert.AreEqual(currentTime.AddMinutes(1693), nextCheckDate);
			*/
		}

		[Test]
		public void GetOrderCheckDate_RejectedStatus_GetDate()
		{
			//<Setting Name="RejectedOrderCheckInterval">120:60;-1:1440</Setting>
			/*
			var currentTime = DateTime.Now;
			DateTime nextCheckDate;
			DateTime firstCheck;

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("RejectedByPaymentProvider", null, currentTime);
			Assert.AreEqual(currentTime.AddMinutes(60), nextCheckDate);

			firstCheck = nextCheckDate;

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("RejectedByPaymentProvider", firstCheck, currentTime.AddMinutes(181));
			Assert.AreEqual(currentTime.AddMinutes(1621), nextCheckDate);
			*/
		}

		[Test]
		public void GetOrderCheckDate_OnHoldStatus_GetDate()
		{
			//<Setting Name="OnHoldOrderCheckInterval">-1:1440</Setting>
			/*
			var currentTime = DateTime.Now;
			DateTime nextCheckDate;
			DateTime firstCheck;

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("PaymentOnHold", null, currentTime);
			Assert.AreEqual(currentTime.AddMinutes(1440), nextCheckDate);

			firstCheck = nextCheckDate;

			nextCheckDate = _maksuturvaSetting.GetOrderCheckDate("PaymentOnHold", firstCheck, currentTime.AddMinutes(2881));
			Assert.AreEqual(currentTime.AddMinutes(1440 + 2881), nextCheckDate);
			*/
		}
	}
}