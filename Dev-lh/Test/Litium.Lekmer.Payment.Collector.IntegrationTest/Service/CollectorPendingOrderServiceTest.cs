﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Service
{
	[TestFixture]
	public class CollectorPendingOrderServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorPendingOrderService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICollectorPendingOrderService>();

			Assert.IsInstanceOf<ICollectorPendingOrderService>(service);
			Assert.IsInstanceOf<CollectorPendingOrderService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorPendingOrderService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICollectorPendingOrderService>();
			var service2 = IoC.Resolve<ICollectorPendingOrderService>();

			Assert.AreEqual(service1, service2);
		}
	}
}