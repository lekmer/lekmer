﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.SecureService
{
	[TestFixture]
	public class CollectorPaymentTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorPaymentTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICollectorPaymentTypeSecureService>();

			Assert.IsInstanceOf<ICollectorPaymentTypeSecureService>(service);
			Assert.IsInstanceOf<CollectorPaymentTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorPaymentTypeSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICollectorPaymentTypeSecureService>();
			var service2 = IoC.Resolve<ICollectorPaymentTypeSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}