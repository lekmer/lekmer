﻿using Litium.Lekmer.Pacsoft.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Pacsoft.IntegrationTest.Service
{
	[TestFixture]
	public class PacsoftExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PacsoftExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPacsoftExportService>();

			Assert.IsInstanceOf<IPacsoftExportService>(service);
			Assert.IsInstanceOf<PacsoftExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PacsoftExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IPacsoftExportService>();
			var service2 = IoC.Resolve<IPacsoftExportService>();
			Assert.AreEqual(service1, service2);
		}
	}
}