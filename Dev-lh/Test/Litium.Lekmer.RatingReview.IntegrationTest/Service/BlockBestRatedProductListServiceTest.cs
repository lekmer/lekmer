﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockBestRatedProductListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBestRatedProductListService>();

			Assert.IsInstanceOf<IBlockBestRatedProductListService>(service);
			Assert.IsInstanceOf<BlockBestRatedProductListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBestRatedProductListService>();
			var instance2 = IoC.Resolve<IBlockBestRatedProductListService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}