﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingReviewUserServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewUserService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewUserService>();

			Assert.IsInstanceOf<IRatingReviewUserService>(service);
			Assert.IsInstanceOf<RatingReviewUserService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewUserService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewUserService>();
			var instance2 = IoC.Resolve<IRatingReviewUserService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}