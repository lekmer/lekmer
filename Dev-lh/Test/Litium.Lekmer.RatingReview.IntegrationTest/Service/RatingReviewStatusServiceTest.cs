﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingReviewStatusServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewStatusService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewStatusService>();

			Assert.IsInstanceOf<IRatingReviewStatusService>(service);
			Assert.IsInstanceOf<RatingReviewStatusService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewStatusService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewStatusService>();
			var instance2 = IoC.Resolve<IRatingReviewStatusService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}