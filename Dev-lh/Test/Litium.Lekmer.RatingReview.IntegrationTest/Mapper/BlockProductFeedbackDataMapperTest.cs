﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockProductFeedbackDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockProductFeedback> blockProductFeedbackDataMapper = DataMapperResolver.Resolve<IBlockProductFeedback>(_dataReader);

			Assert.IsInstanceOf<BlockProductFeedbackDataMapper>(blockProductFeedbackDataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockProductFeedback> blockProductFeedbackDataMapper1 = DataMapperResolver.Resolve<IBlockProductFeedback>(_dataReader);
			DataMapperBase<IBlockProductFeedback> blockProductFeedbackDataMapper2 = DataMapperResolver.Resolve<IBlockProductFeedback>(_dataReader);

			Assert.AreNotEqual(blockProductFeedbackDataMapper1, blockProductFeedbackDataMapper2);
		}
	}
}