﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockBestRatedProductListBrandDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockBestRatedProductListBrand> dataMapper = DataMapperResolver.Resolve<IBlockBestRatedProductListBrand>(_dataReader);

			Assert.IsInstanceOf<BlockBestRatedProductListBrandDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockBestRatedProductListBrand> dataMapper1 = DataMapperResolver.Resolve<IBlockBestRatedProductListBrand>(_dataReader);
			DataMapperBase<IBlockBestRatedProductListBrand> dataMapper2 = DataMapperResolver.Resolve<IBlockBestRatedProductListBrand>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}