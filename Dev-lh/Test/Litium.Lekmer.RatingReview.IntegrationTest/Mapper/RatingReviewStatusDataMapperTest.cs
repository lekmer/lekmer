﻿using System.Data;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class RatingReviewStatusDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewStatusDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IRatingReviewStatus>(_dataReader);

			Assert.IsInstanceOf<RatingReviewStatusDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewStatusDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IRatingReviewStatus>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IRatingReviewStatus>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}