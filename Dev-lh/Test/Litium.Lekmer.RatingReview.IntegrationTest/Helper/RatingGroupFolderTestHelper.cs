﻿using System.Collections.Generic;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Helper
{
	public class RatingGroupFolderTestHelper
	{
		public ISystemUserFull SystemUserFull { get; private set; }
		public IRatingGroupFolderSecureService RatingGroupFolderSecureService { get; private set; }

		public RatingGroupFolderTestHelper(IRatingGroupFolderSecureService ratingGroupFolderSecureService, ISystemUserFull systemUserFull)
		{
			RatingGroupFolderSecureService = ratingGroupFolderSecureService;
			SystemUserFull = systemUserFull;
		}


		public IRatingGroupFolder Create(string title)
		{
			var ratingGroupFolder = RatingGroupFolderSecureService.Create();
			ratingGroupFolder.Title = title;
			return ratingGroupFolder;
		}

		public IRatingGroupFolder Create(string title, int parentId)
		{
			var ratingGroupFolder = RatingGroupFolderSecureService.Create();
			ratingGroupFolder.ParentRatingGroupFolderId = parentId;
			ratingGroupFolder.Title = title;
			return ratingGroupFolder;
		}

		public IRatingGroupFolder Create(string title, IRatingGroupFolder parent)
		{
			return Create(title, parent.Id);
		}

		public IRatingGroupFolder CreateAndSave(string title)
		{
			var ratingGroupFolder = Create(title);
			return RatingGroupFolderSecureService.Save(SystemUserFull, ratingGroupFolder);
		}

		public IRatingGroupFolder CreateAndSave(string title, int parentId)
		{
			var ratingGroupFolder = Create(title, parentId);
			return RatingGroupFolderSecureService.Save(SystemUserFull, ratingGroupFolder);
		}

		public IRatingGroupFolder CreateAndSave(string title, IRatingGroupFolder parent)
		{
			return CreateAndSave(title, parent.Id);
		}


		// Asserts
		public void AssertContains(IRatingGroupFolder expected, IEnumerable<IRatingGroupFolder> actual)
		{
			Assert.IsNotNull(expected, "RatingGroupFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<RatingGroupFolder> actual");

			var actualRatingGroupFolder = actual.FirstOrDefault(rf => rf.Id == expected.Id);

			Assert.IsNotNull(actualRatingGroupFolder, "RatingGroupFolder actual");

			AssertAreEquel(expected, actualRatingGroupFolder);
		}

		public void AssertContains(IRatingGroupFolder expected, IEnumerable<INode> actual)
		{
			Assert.IsNotNull(expected, "RatingGroupFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<RatingGroupFolder> actual");

			var actualNode = actual.FirstOrDefault(n => n.Id == expected.Id);

			Assert.IsNotNull(actualNode, "RatingGroupFolder actual");

			AssertAreEquel(expected, actualNode);
		}

		public void AssertDoesNotContain(IRatingGroupFolder expected, IEnumerable<IRatingGroupFolder> actual)
		{
			Assert.IsNotNull(expected, "RatingGroupFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<RatingGroupFolder> actual");

			var actualRatingGroupFolder = actual.FirstOrDefault(rf => rf.Id == expected.Id);

			Assert.IsNull(actualRatingGroupFolder, "RatingGroupFolder actual");
		}

		public void AssertDoesNotContain(IRatingGroupFolder expected, IEnumerable<INode> actual)
		{
			Assert.IsNotNull(expected, "RatingGroupFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<INode> actual");

			var actualNode = actual.FirstOrDefault(n => n.Id == expected.Id);

			Assert.IsNull(actualNode, "RatingGroupFolder actual");
		}

		public void AssertAreEquel(IRatingGroupFolder expected, IRatingGroupFolder actual)
		{
			Assert.IsNotNull(expected, "RatingGroupFolder expected");
			Assert.IsNotNull(actual, "RatingGroupFolder actual");

			Assert.AreEqual(expected.Id, actual.Id, "RatingGroupFolder.Id");
			Assert.AreEqual(expected.ParentRatingGroupFolderId, actual.ParentRatingGroupFolderId, "RatingGroupFolder.ParentRatingGroupFolderId");
			Assert.AreEqual(expected.Title, actual.Title, "RatingGroupFolder.Title");
		}

		public void AssertAreEquel(IRatingGroupFolder expected, INode actual)
		{
			Assert.IsNotNull(expected, "RatingGroupFolder expected");
			Assert.IsNotNull(actual, "RatingGroupFolder actual");

			Assert.AreEqual(expected.Id, actual.Id, "RatingGroupFolder.Id");
			Assert.AreEqual(expected.ParentRatingGroupFolderId, actual.ParentId, "RatingGroupFolder.ParentRatingGroupFolderId");
			Assert.AreEqual(expected.Title, actual.Title, "RatingGroupFolder.Title");
		}
	}
}