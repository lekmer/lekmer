﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockLatestFeedbackListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackList_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockLatestFeedbackList>();

			Assert.IsInstanceOf<IBlockLatestFeedbackList>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackList_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockLatestFeedbackList>();
			var block2 = IoC.Resolve<IBlockLatestFeedbackList>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}