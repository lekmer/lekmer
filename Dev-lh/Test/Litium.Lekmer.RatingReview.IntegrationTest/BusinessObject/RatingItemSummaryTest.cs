﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingItemSummaryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemSummary_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingItemSummary>();

			Assert.IsInstanceOf<IRatingItemSummary>(instance);
			Assert.IsInstanceOf<IRatingItemSummary>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemSummary_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemSummary>();
			var instance2 = IoC.Resolve<IRatingItemSummary>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}