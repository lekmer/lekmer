﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductFeedbackTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductFeedback_Resolve_Resolved()
		{
			IBlockProductFeedback blockProductFeedback = IoC.Resolve<IBlockProductFeedback>();

			Assert.IsInstanceOf<IBlockProductFeedback>(blockProductFeedback);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductFeedback_ResolveTwice_DifferentObjects()
		{
			IBlockProductFeedback blockProductFeedback1 = IoC.Resolve<IBlockProductFeedback>();
			IBlockProductFeedback blockProductFeedback2 = IoC.Resolve<IBlockProductFeedback>();

			Assert.AreNotEqual(blockProductFeedback1, blockProductFeedback2);
		}
	}
}