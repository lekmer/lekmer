﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductRatingSummaryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRatingSummary_Resolve_Resolved()
		{
			IBlockProductRatingSummary blockProductRatingSummary = IoC.Resolve<IBlockProductRatingSummary>();

			Assert.IsInstanceOf<IBlockProductRatingSummary>(blockProductRatingSummary);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRatingSummary_ResolveTwice_DifferentObjects()
		{
			IBlockProductRatingSummary blockProductRatingSummary1 = IoC.Resolve<IBlockProductRatingSummary>();
			IBlockProductRatingSummary blockProductRatingSummary2 = IoC.Resolve<IBlockProductRatingSummary>();

			Assert.AreNotEqual(blockProductRatingSummary1, blockProductRatingSummary2);
		}
	}
}