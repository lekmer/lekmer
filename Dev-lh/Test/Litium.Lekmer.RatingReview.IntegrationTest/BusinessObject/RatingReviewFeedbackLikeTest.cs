﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingReviewFeedbackLikeTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackLike_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingReviewFeedbackLike>();

			Assert.IsInstanceOf<IRatingReviewFeedbackLike>(instance);
			Assert.IsInstanceOf<IRatingReviewFeedbackLike>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackLike_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewFeedbackLike>();
			var instance2 = IoC.Resolve<IRatingReviewFeedbackLike>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}