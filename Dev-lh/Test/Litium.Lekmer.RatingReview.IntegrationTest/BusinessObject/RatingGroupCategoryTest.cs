﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingGroupCategoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupCategory_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingGroupCategory>();

			Assert.IsInstanceOf<IRatingGroupCategory>(instance);
			Assert.IsInstanceOf<IRatingGroupCategory>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupCategory_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupCategory>();
			var instance2 = IoC.Resolve<IRatingGroupCategory>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}