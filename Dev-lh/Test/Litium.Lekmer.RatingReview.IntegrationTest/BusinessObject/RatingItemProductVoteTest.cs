﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingItemProductVoteTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductVote_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingItemProductVote>();

			Assert.IsInstanceOf<IRatingItemProductVote>(instance);
			Assert.IsInstanceOf<IRatingItemProductVote>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductVote_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductVote>();
			var instance2 = IoC.Resolve<IRatingItemProductVote>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}