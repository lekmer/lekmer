﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockBestRatedProductListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListSecureService_Resolve_Resolved()
		{
			IBlockBestRatedProductListSecureService service = IoC.Resolve<IBlockBestRatedProductListSecureService>();

			Assert.IsInstanceOf<IBlockBestRatedProductListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListSecureService_ResolveTwice_SameObjects()
		{
			IBlockBestRatedProductListSecureService service1 = IoC.Resolve<IBlockBestRatedProductListSecureService>();
			IBlockBestRatedProductListSecureService service2 = IoC.Resolve<IBlockBestRatedProductListSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}