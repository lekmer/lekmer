﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockLatestFeedbackListBrandSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandSecureService_Resolve_Resolved()
		{
			IBlockLatestFeedbackListBrandSecureService service = IoC.Resolve<IBlockLatestFeedbackListBrandSecureService>();

			Assert.IsInstanceOf<IBlockLatestFeedbackListBrandSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandSecureService_ResolveTwice_SameObjects()
		{
			IBlockLatestFeedbackListBrandSecureService service1 = IoC.Resolve<IBlockLatestFeedbackListBrandSecureService>();
			IBlockLatestFeedbackListBrandSecureService service2 = IoC.Resolve<IBlockLatestFeedbackListBrandSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}