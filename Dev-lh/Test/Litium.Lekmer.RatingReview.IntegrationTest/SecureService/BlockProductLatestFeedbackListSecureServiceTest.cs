﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductLatestFeedbackListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListSecureService_Resolve_Resolved()
		{
			IBlockProductLatestFeedbackListSecureService service = IoC.Resolve<IBlockProductLatestFeedbackListSecureService>();

			Assert.IsInstanceOf<IBlockProductLatestFeedbackListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListSecureService_ResolveTwice_SameObjects()
		{
			IBlockProductLatestFeedbackListSecureService service1 = IoC.Resolve<IBlockProductLatestFeedbackListSecureService>();
			IBlockProductLatestFeedbackListSecureService service2 = IoC.Resolve<IBlockProductLatestFeedbackListSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}