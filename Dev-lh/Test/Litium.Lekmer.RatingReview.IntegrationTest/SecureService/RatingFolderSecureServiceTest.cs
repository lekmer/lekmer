﻿using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.RatingReview.IntegrationTest.Helper;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingFolderSecureServiceTest
	{
		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private IRatingFolderSecureService _ratingFolderSecureService;
		private IRatingSecureService _ratingSecureService;
		private RatingFolderTestHelper _ratingFolderTestHelper;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();

			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();

			_ratingSecureService = _mocker.Stub<IRatingSecureService>();
			_ratingFolderSecureService = new RatingFolderSecureService(new RatingFolderRepository(), _accessValidator, _ratingSecureService);
			_ratingFolderTestHelper = new RatingFolderTestHelper(_ratingFolderSecureService, _systemUserFull);
		}

		[Test]
		[Category("IoC")]
		public void RatingFolderSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingFolderSecureService>();

			Assert.IsInstanceOf<IRatingFolderSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingFolderSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingFolderSecureService>();
			var instance2 = IoC.Resolve<IRatingFolderSecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveNewRatingFolder_FolderSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				var ratingFolder = _ratingFolderTestHelper.Create("Test rating folder");

				// Save
				var actualFolder = _ratingFolderSecureService.Save(_systemUserFull, ratingFolder);

				Assert.IsTrue(actualFolder.Id > 0);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveRatingFolderWithSameTitleOnRootLevel_FolderNotSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder1 = _ratingFolderTestHelper.Create("Test rating folder same title");
				var ratingFolder2 = _ratingFolderTestHelper.Create("Test rating folder same title");

				// Save
				var actualFolder1 = _ratingFolderSecureService.Save(_systemUserFull, ratingFolder1);
				var actualFolder2 = _ratingFolderSecureService.Save(_systemUserFull, ratingFolder2);

				// Check if it works
				Assert.Greater(actualFolder1.Id, 0, "RatingFolder.Id");
				Assert.AreEqual(-1, actualFolder2.Id, "RatingFolder.Id with same title");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_SaveRatingFolderWithSameTitleOnSubLevel_FolderNotSaved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var rootFolder = _ratingFolderTestHelper.CreateAndSave("Test root rating folder");

				var ratingFolder1 = _ratingFolderTestHelper.CreateAndSave("Test rating folder same title", rootFolder);
				var ratingFolder2 = _ratingFolderTestHelper.CreateAndSave("Test rating folder same title", rootFolder);

				// Check if it works
				Assert.Greater(rootFolder.Id, 0, "RatingFolder.Id");
				Assert.Greater(ratingFolder1.Id, 0, "RatingFolder.Id");
				Assert.AreEqual(-1, ratingFolder2.Id, "RatingFolder.Id with same title");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Save")]
		public void Save_UpdateRatingFolder_FolderUpdated()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var rootFolder1 = _ratingFolderTestHelper.CreateAndSave("Test root rating folder 1");
				var rootFolder2 = _ratingFolderTestHelper.CreateAndSave("Test root rating folder 2");

				// Create folder
				var ratingFolder = _ratingFolderTestHelper.CreateAndSave("Test rating folder", rootFolder1);

				// Update
				ratingFolder.ParentRatingFolderId = rootFolder2.Id;
				ratingFolder.Title = "Test rating folder updated";
				
				// Update in db
				var actualFolder = _ratingFolderSecureService.Save(_systemUserFull, ratingFolder);

				// Check if it works
				Assert.AreEqual(ratingFolder.Id, actualFolder.Id, "RatingFolder.Id");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetById_GetFolderById_CorrectFolderReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder = _ratingFolderTestHelper.CreateAndSave("Test rating folder 1");

				// Get all
				var actualRatingFolder = _ratingFolderSecureService.GetById(ratingFolder.Id);

				_ratingFolderTestHelper.AssertAreEquel(ratingFolder, actualRatingFolder);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllFolders_AllFoldersReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder1 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 1");
				var ratingFolder2 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 2");
				var ratingFolder3 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 3");

				// Get all
				var ratingFolders = _ratingFolderSecureService.GetAll();
				
				_ratingFolderTestHelper.AssertContains(ratingFolder1, ratingFolders);
				_ratingFolderTestHelper.AssertContains(ratingFolder2, ratingFolders);
				_ratingFolderTestHelper.AssertContains(ratingFolder3, ratingFolders);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllByParent_GetAllFoldersBySomeParent_AllSubFoldersReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder1 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 1");
				var ratingFolder2 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 2", ratingFolder1);
				var ratingFolder3 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 3", ratingFolder1);

				// Get by parent
				var ratingFolders = _ratingFolderSecureService.GetAllByParent(ratingFolder1.Id);

				_ratingFolderTestHelper.AssertDoesNotContain(ratingFolder1, ratingFolders);
				_ratingFolderTestHelper.AssertContains(ratingFolder2, ratingFolders);
				_ratingFolderTestHelper.AssertContains(ratingFolder3, ratingFolders);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetTree_SelectedIdNull_CorrectNodesReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder1 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 1");
				var ratingFolder2 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 2", ratingFolder1);
				var ratingFolder3 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 3", ratingFolder2);
				var ratingFolder4 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 4");

				// Get by parent
				Collection<INode> ratingFolderNodes = _ratingFolderSecureService.GetTree(null);

				_ratingFolderTestHelper.AssertContains(ratingFolder1, ratingFolderNodes);
				_ratingFolderTestHelper.AssertDoesNotContain(ratingFolder2, ratingFolderNodes);
				_ratingFolderTestHelper.AssertDoesNotContain(ratingFolder3, ratingFolderNodes);
				_ratingFolderTestHelper.AssertContains(ratingFolder4, ratingFolderNodes);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Get")]
		public void GetTree_SecondLevelSelected_CorrectNodesReturned()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder1 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 1");
				var ratingFolder2 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 2", ratingFolder1);
				var ratingFolder3 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 3", ratingFolder2);
				var ratingFolder4 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 4", ratingFolder3);
				var ratingFolder5 = _ratingFolderTestHelper.CreateAndSave("Test rating folder 5");

				// Get by parent
				Collection<INode> ratingFolderNodes = _ratingFolderSecureService.GetTree(ratingFolder2.Id);

				_ratingFolderTestHelper.AssertContains(ratingFolder1, ratingFolderNodes);
				_ratingFolderTestHelper.AssertContains(ratingFolder2, ratingFolderNodes);
				_ratingFolderTestHelper.AssertContains(ratingFolder3, ratingFolderNodes);
				// Node 4 is not included in output
				_ratingFolderTestHelper.AssertDoesNotContain(ratingFolder4, ratingFolderNodes);
				_ratingFolderTestHelper.AssertContains(ratingFolder5, ratingFolderNodes);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void TryDelete_DeleteSingleFolder_FolderRemoved()
		{
			 Expect.Call(_ratingSecureService.GetAllByFolder(0)).Return(new Collection<IRating>() ).IgnoreArguments();

			_mocker.ReplayAll();

			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder = _ratingFolderTestHelper.CreateAndSave("Test rating folder");

				// Get by parent
				bool isDeleted = _ratingFolderSecureService.TryDelete(_systemUserFull, ratingFolder.Id);
				var actualRatingFolder = _ratingFolderSecureService.GetById(ratingFolder.Id);

				Assert.IsTrue(isDeleted, "RatingFolder deleted");
				Assert.IsNull(actualRatingFolder, "RatingFolder deleted");

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void TryDelete_DeleteFolderWithSubFoldersAndRatings_FolderNotRemoved()
		{
			using (var transactionScope = new TransactionScope())
			{
				// Create new folders
				var ratingFolder1 = _ratingFolderTestHelper.CreateAndSave("Test rating folder");
				var ratingFolder2 = _ratingFolderTestHelper.CreateAndSave("Test rating folder", ratingFolder1);

				Expect
					.Call(_ratingSecureService.GetAllByFolder(ratingFolder1.Id))
					.Return(new Collection<IRating>());

				Expect
					.Call(_ratingSecureService.GetAllByFolder(ratingFolder2.Id))
					.Return(new Collection<IRating> { new Rating() });

				_mocker.ReplayAll();
				
				// Get by parent
				bool isDeleted = _ratingFolderSecureService.TryDelete(_systemUserFull, ratingFolder1.Id);

				Assert.IsFalse(isDeleted, "RatingFolder deleted");

				transactionScope.Dispose();
			}
		}
	}
}