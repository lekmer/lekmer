﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingGroupProductSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupProductSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupProductSecureService>();

			Assert.IsInstanceOf<IRatingGroupProductSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupProductSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupProductSecureService>();
			var instance2 = IoC.Resolve<IRatingGroupProductSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}