﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingGroupRatingSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupRatingSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupRatingSecureService>();

			Assert.IsInstanceOf<IRatingGroupRatingSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupRatingSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupRatingSecureService>();
			var instance2 = IoC.Resolve<IRatingGroupRatingSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}