﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class ReviewRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ReviewRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ReviewRepository>();

			Assert.IsInstanceOf<ReviewRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ReviewRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ReviewRepository>();
			var instance2 = IoC.Resolve<ReviewRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}