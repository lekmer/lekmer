﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockLatestFeedbackListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListRepository_Resolve_Resolved()
		{
			BlockLatestFeedbackListRepository repository = IoC.Resolve<BlockLatestFeedbackListRepository>();

			Assert.IsInstanceOf<BlockLatestFeedbackListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListRepository_ResolveTwice_SameObjects()
		{
			BlockLatestFeedbackListRepository repository1 = IoC.Resolve<BlockLatestFeedbackListRepository>();
			BlockLatestFeedbackListRepository repository2 = IoC.Resolve<BlockLatestFeedbackListRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}