﻿using Litium.Lekmer.SiteStructure.Cache;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.UnitTest.Cache
{
	[TestFixture]
	public class BlockSliderCacheTest
	{
		private static MockRepository _mocker;
		private static IBlockSlider _blockSlider;
		private static BlockSliderKey _blockSliderKey;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_blockSlider = _mocker.Stub<IBlockSlider>();
			_blockSliderKey = new BlockSliderKey(1, 1);
		}

		[Test]
		public void BlockSliderCache_CacheAdd_Exists()
		{
			BlockSliderCache.Instance.Add(_blockSliderKey, _blockSlider);
			Assert.IsTrue(BlockSliderCache.Instance.Contains(_blockSliderKey));
			Assert.IsNotNull(BlockSliderCache.Instance.GetData(_blockSliderKey));
			BlockSliderCache.Instance.Flush();
		}

		[Test]
		public void BlockSliderCache_CacheTryGetItem_AddAndExists()
		{
			BlockSliderCache.Instance.TryGetItem(_blockSliderKey, () => _blockSlider);
			Assert.IsTrue(BlockSliderCache.Instance.Contains(_blockSliderKey));
			Assert.IsNotNull(BlockSliderCache.Instance.GetData(_blockSliderKey));
			BlockSliderCache.Instance.Flush();
		}

		[Test]
		public void BlockSliderCache_CacheRemove_Removed()
		{
			BlockSliderCache.Instance.Add(_blockSliderKey, _blockSlider);
			BlockSliderCache.Instance.Remove(_blockSliderKey);
			Assert.IsFalse(BlockSliderCache.Instance.Contains(_blockSliderKey));
			Assert.IsNull(BlockSliderCache.Instance.GetData(_blockSliderKey));
		}

		public void BlockSliderCache_CacheFlush_Flushed()
		{
			BlockSliderCache.Instance.Add(_blockSliderKey, _blockSlider);
			BlockSliderCache.Instance.Flush();
			Assert.IsFalse(BlockSliderCache.Instance.Contains(_blockSliderKey));
			Assert.IsNull(BlockSliderCache.Instance.GetData(_blockSliderKey));
		}
	}
}