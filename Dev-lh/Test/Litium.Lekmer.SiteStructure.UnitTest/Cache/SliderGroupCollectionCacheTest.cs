﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.SiteStructure.Cache;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.UnitTest.Cache
{
	[TestFixture]
	public class SliderGroupCollectionCacheTest
	{
		private static MockRepository _mocker;
		private static Collection<ISliderGroup> _sliderGroupCollection;
		private static SliderGroupCollectionKey _sliderGroupCollectionKey;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_sliderGroupCollection = _mocker.Stub<Collection<ISliderGroup>>();
			_sliderGroupCollectionKey = new SliderGroupCollectionKey(1, 1);
		}

		[Test]
		public void SliderGroupCollectionCache_CacheAdd_Exists()
		{
			SliderGroupCollectionCache.Instance.Add(_sliderGroupCollectionKey, _sliderGroupCollection);
			Assert.IsTrue(SliderGroupCollectionCache.Instance.Contains(_sliderGroupCollectionKey));
			Assert.IsNotNull(SliderGroupCollectionCache.Instance.GetData(_sliderGroupCollectionKey));
			BlockSliderCache.Instance.Flush();
		}

		[Test]
		public void SliderGroupCollectionCache_CacheTryGetItem_AddAndExists()
		{
			SliderGroupCollectionCache.Instance.TryGetItem(_sliderGroupCollectionKey, () => _sliderGroupCollection);
			Assert.IsTrue(SliderGroupCollectionCache.Instance.Contains(_sliderGroupCollectionKey));
			Assert.IsNotNull(SliderGroupCollectionCache.Instance.GetData(_sliderGroupCollectionKey));
			BlockSliderCache.Instance.Flush();
		}

		public void SliderGroupCollectionCache_CacheFlush_Flushed()
		{
			SliderGroupCollectionCache.Instance.Add(_sliderGroupCollectionKey, _sliderGroupCollection);
			SliderGroupCollectionCache.Instance.Flush();
			Assert.IsFalse(SliderGroupCollectionCache.Instance.Contains(_sliderGroupCollectionKey));
			Assert.IsNull(SliderGroupCollectionCache.Instance.GetData(_sliderGroupCollectionKey));
		}
	}
}