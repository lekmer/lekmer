﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SliderGroupTest
	{
		[Test]
		[Category("IoC")]
		public void SliderGroup_Resolve_Resolved()
		{
			var obj = IoC.Resolve<ISliderGroup>();
			Assert.IsInstanceOf<ISliderGroup>(obj);
		}

		[Test]
		[Category("IoC")]
		public void SliderGroup_ResolveTwice_DifferentObjects()
		{
			var obj1 = IoC.Resolve<ISliderGroup>();
			var obj2 = IoC.Resolve<ISliderGroup>();
			Assert.AreNotEqual(obj1, obj2);
		}
	}
}