﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockSliderTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSlider_Resolve_Resolved()
		{
			var obj = IoC.Resolve<IBlockSlider>();
			Assert.IsInstanceOf<IBlockSlider>(obj);
		}

		[Test]
		[Category("IoC")]
		public void BlockSlider_ResolveTwice_DifferentObjects()
		{
			var obj1 = IoC.Resolve<IBlockSlider>();
			var obj2 = IoC.Resolve<IBlockSlider>();
			Assert.AreNotEqual(obj1, obj2);
		}
	}
}