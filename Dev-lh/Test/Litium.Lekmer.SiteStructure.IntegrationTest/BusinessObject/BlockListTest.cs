﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockList_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockList>();
			Assert.IsInstanceOf<IBlockList>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockList_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockList>();
			var block2 = IoC.Resolve<IBlockList>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}
