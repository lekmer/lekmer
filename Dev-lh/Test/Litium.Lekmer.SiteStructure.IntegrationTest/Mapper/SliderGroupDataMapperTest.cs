﻿using System.Data;
using Litium.Lekmer.SiteStructure.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Mapper
{
	[TestFixture]
	public class SliderGroupDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SliderGroup_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ISliderGroup>(_dataReader);
			Assert.IsInstanceOf<SliderGroupDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void SliderGroup_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ISliderGroup>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ISliderGroup>(_dataReader);
			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}