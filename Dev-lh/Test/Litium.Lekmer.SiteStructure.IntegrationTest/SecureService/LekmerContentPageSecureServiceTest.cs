﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerContentPageSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerContentPageSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IContentPageSecureService>();

			Assert.IsInstanceOf<IContentPageSecureService>(secureService);
			Assert.IsInstanceOf<LekmerContentPageSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentPageSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IContentPageSecureService>();
			var secureService2 = IoC.Resolve<IContentPageSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}