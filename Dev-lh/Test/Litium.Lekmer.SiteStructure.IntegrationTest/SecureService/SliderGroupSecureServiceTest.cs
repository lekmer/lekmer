﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class SliderGroupSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SliderGroupSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<ISliderGroupSecureService>();
			Assert.IsInstanceOf<ISliderGroupSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void SliderGroupSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<ISliderGroupSecureService>();
			var secureService2 = IoC.Resolve<ISliderGroupSecureService>();
			Assert.AreEqual(secureService1, secureService2);
		}
	}
}