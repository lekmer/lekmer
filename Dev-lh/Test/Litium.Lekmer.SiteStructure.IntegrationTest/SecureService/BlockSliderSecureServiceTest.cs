﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockSliderSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSliderSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IBlockSliderSecureService>();
			Assert.IsInstanceOf<IBlockSliderSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void BlockSliderSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IBlockSliderSecureService>();
			var secureService2 = IoC.Resolve<IBlockSliderSecureService>();
			Assert.AreEqual(secureService1, secureService2);
		}
	}
}