﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Helper
{
	public class ContentPageUrlHistoryHelper
	{
		public IEnumerable<IContentPage> GetContentNodes(IUserContext userContext)
		{
			IContentPageService contentPageService = IoC.Resolve<IContentPageService>();

			Collection<IContentPage> contentPages = contentPageService.GetAll(userContext);

			return contentPages;
		}
	}
}