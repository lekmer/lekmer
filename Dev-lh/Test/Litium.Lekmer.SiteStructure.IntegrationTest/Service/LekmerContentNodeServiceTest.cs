﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerContentNodeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerContentNodeService_ResolveByBaseInterface_Resolved()
		{
			var service = IoC.Resolve<IContentNodeService>();

			Assert.IsInstanceOf<IContentNodeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentNodeService_ResolveByDescendantInterface_Resolved()
		{
			var service = IoC.Resolve<ILekmerContentNodeService>();

			Assert.IsInstanceOf<ILekmerContentNodeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentNodeService_ResolveByBaseInterface_LekmerDescendantResolved()
		{
			var service = IoC.Resolve<IContentNodeService>();

			Assert.IsInstanceOf<LekmerContentNodeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentNodeService_ResolveByDescendantInterface_LekmerDescendantResolved()
		{
			var service = IoC.Resolve<ILekmerContentNodeService>();

			Assert.IsInstanceOf<LekmerContentNodeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerContentNodeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IContentNodeService>();
			var service2 = IoC.Resolve<IContentNodeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}