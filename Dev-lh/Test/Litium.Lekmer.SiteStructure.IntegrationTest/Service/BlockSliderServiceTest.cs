﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class BlockSliderServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSliderService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockSliderService>();

			Assert.IsInstanceOf<IBlockSliderService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockSliderService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockSliderService>();
			var service2 = IoC.Resolve<IBlockSliderService>();

			Assert.AreEqual(service1, service2);
		}
	}
}