﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class SliderGroupServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SliderGroupService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISliderGroupService>();

			Assert.IsInstanceOf<ISliderGroupService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SliderGroupService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISliderGroupService>();
			var service2 = IoC.Resolve<ISliderGroupService>();

			Assert.AreEqual(service1, service2);
		}
	}
}