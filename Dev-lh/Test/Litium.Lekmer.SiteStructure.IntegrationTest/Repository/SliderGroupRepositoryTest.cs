﻿using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class SliderGroupRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SliderGroupRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SliderGroupRepository>();
			Assert.IsInstanceOf<SliderGroupRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SliderGroupRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<SliderGroupRepository>();
			var repository2 = IoC.Resolve<SliderGroupRepository>();
			Assert.AreEqual(repository1, repository2);
		}
	}
}