﻿using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockSettingRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSettingRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockSettingRepository>();
			Assert.IsInstanceOf<BlockSettingRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockSettingRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockSettingRepository>();
			var repository2 = IoC.Resolve<BlockSettingRepository>();
			Assert.AreEqual(repository1, repository2);
		}
	}
}