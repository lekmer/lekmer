﻿using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockSliderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSliderRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockSliderRepository>();
			Assert.IsInstanceOf<BlockSliderRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockSliderRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockSliderRepository>();
			var repository2 = IoC.Resolve<BlockSliderRepository>();
			Assert.AreEqual(repository1, repository2);
		}
	}
}