﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Service
{
	[TestFixture]
	public class NewsletterUnsubscriberServiceTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberService_Resolve_Resolved()
		{
			var service = IoC.Resolve<INewsletterUnsubscriberService>();

			Assert.IsInstanceOf<INewsletterUnsubscriberService>(service);
			Assert.IsInstanceOf<NewsletterUnsubscriberService>(service);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<INewsletterUnsubscriberService>();
			var service2 = IoC.Resolve<INewsletterUnsubscriberService>();

			Assert.AreEqual(service1, service2);
		}
	}
}