﻿using System.Data;
using Litium.Lekmer.NewsletterSubscriber.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Mapper
{
	[TestFixture]
	public class NewsletterSubscriberDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriber_Resolve_Resolved()
		{
			var instance = DataMapperResolver.Resolve<INewsletterSubscriber>(_dataReader);

			Assert.IsInstanceOf<NewsletterSubscriberDataMapper>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriber_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<INewsletterSubscriber>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<INewsletterSubscriber>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}