﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class NewsletterSubscriberTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterSubscriber_Resolve_Resolved()
		{
			var instance = IoC.Resolve<INewsletterSubscriber>();

			Assert.IsInstanceOf<INewsletterSubscriber>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriber_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<INewsletterSubscriber>();
			var instance2 = IoC.Resolve<INewsletterSubscriber>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}