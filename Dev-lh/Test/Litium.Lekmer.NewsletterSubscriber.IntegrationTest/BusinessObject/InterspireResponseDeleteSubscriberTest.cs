﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class InterspireResponseDeleteSubscriberTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireResponseDeleteSubscriber_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireResponseDeleteSubscriber>();

			Assert.IsInstanceOf<IInterspireResponseDeleteSubscriber>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireResponseDeleteSubscriber_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IInterspireResponseDeleteSubscriber>();
			var instance2 = IoC.Resolve<IInterspireResponseDeleteSubscriber>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}