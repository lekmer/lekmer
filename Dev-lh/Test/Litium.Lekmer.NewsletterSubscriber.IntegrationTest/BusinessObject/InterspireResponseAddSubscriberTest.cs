﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class InterspireResponseAddSubscriberTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireResponseAddSubscriber_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireResponseAddSubscriber>();

			Assert.IsInstanceOf<IInterspireResponseAddSubscriber>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireResponseAddSubscriber_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IInterspireResponseAddSubscriber>();
			var instance2 = IoC.Resolve<IInterspireResponseAddSubscriber>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}