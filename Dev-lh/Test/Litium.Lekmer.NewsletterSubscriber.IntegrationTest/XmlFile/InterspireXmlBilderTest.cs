﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.XmlFile
{
	[TestFixture]
	public class InterspireXmlBilderTest
	{
		private static string _xmlGetAddSubscriber =
@"<xmlrequest>
  <username>test-user</username>
  <usertoken>test-token</usertoken>
  <requesttype>subscribers</requesttype>
  <requestmethod>AddSubscriberToList</requestmethod>
  <details>
    <emailaddress>test-email-address</emailaddress>
    <mailinglist>test-mailing-list-id</mailinglist>
    <format>html</format>
    <confirmed>yes</confirmed>
  </details>
</xmlrequest>";

		private static string _xmlGetDeleteSubscriber =
@"<xmlrequest>
  <username>test-user</username>
  <usertoken>test-token</usertoken>
  <requesttype>subscribers</requesttype>
  <requestmethod>DeleteSubscriber</requestmethod>
  <details>
    <emailaddress>test-email-address</emailaddress>
    <list>test-mailing-list-id</list>
  </details>
</xmlrequest>";

		private static string _xmlGetGetLists =
@"<xmlrequest>
  <username>test-user</username>
  <usertoken>test-token</usertoken>
  <requesttype>user</requesttype>
  <requestmethod>GetLists</requestmethod>
  <details> </details>
</xmlrequest>";

		private static string _xmlGetIsContactOnList =
@"<xmlrequest>
  <username>test-user</username>
  <usertoken>test-token</usertoken>
  <requesttype>subscribers</requesttype>
  <requestmethod>IsSubscriberOnList</requestmethod>
  <details>
    <email>test-email-address</email>
    <list>test-mailing-list-id</list>
  </details>
</xmlrequest>";

		[Test]
		[Category("IoC")]
		public void InterspireXmlBilder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireXmlBilder>();

			Assert.IsInstanceOf<IInterspireXmlBilder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireXmlBilder_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IInterspireXmlBilder>();
			var instance2 = IoC.Resolve<IInterspireXmlBilder>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		public void GetAddSubscriberXml_Execute_CorrectXmlGenerated()
		{
			var xmlBuilder = IoC.Resolve<IInterspireXmlBilder>();

			string xmlActual = xmlBuilder.GetAddSubscriberXml("test-user", "test-token", "test-email-address", "test-mailing-list-id");

			Assert.AreEqual(_xmlGetAddSubscriber, xmlActual);
		}

		[Test]
		public void GetDeleteSubscriberXml_Execute_CorrectXmlGenerated()
		{
			var xmlBuilder = IoC.Resolve<IInterspireXmlBilder>();

			string xmlActual = xmlBuilder.GetDeleteSubscriberXml("test-user", "test-token", "test-email-address", "test-mailing-list-id");

			Assert.AreEqual(_xmlGetDeleteSubscriber, xmlActual);
		}

		[Test]
		public void GetGetListsXml_Execute_CorrectXmlGenerated()
		{
			var xmlBuilder = IoC.Resolve<IInterspireXmlBilder>();

			string xmlActual = xmlBuilder.GetGetListsXml("test-user", "test-token");

			Assert.AreEqual(_xmlGetGetLists, xmlActual);
		}

		[Test]
		public void GetIsContactOnListXml_Execute_CorrectXmlGenerated()
		{
			var xmlBuilder = IoC.Resolve<IInterspireXmlBilder>();

			string xmlActual = xmlBuilder.GetIsContactOnListXml("test-user", "test-token", "test-email-address", "test-mailing-list-id"); ;

			Assert.AreEqual(_xmlGetIsContactOnList, xmlActual);
		}
	}
}