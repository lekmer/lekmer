﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.XmlFile
{
	[TestFixture]
	public class InterspireXmlParserTest
	{
		private static string _xmlFailedWithMessage =
@"<response>
<status>FAILED</status>
<errormessage>The XML format you have sent is invalid.</errormessage>
</response>
";

		private static string _xmlErrorWithMessage =
@"<response>
<status>ERROR</status>
<errormessage>The XML format you have sent is invalid.</errormessage>
</response>
";

		[Test]
		[Category("IoC")]
		public void InterspireXmlParser_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireXmlParser>();

			Assert.IsInstanceOf<IInterspireXmlParser>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireXmlParser_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IInterspireXmlParser>();
			var instance2 = IoC.Resolve<IInterspireXmlParser>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		public void ParseAddSubscriberResponse_ExecuteWithFailedXml_XmlCorrectlyParsed()
		{
			var xmlParser = IoC.Resolve<IInterspireXmlParser>();

			IInterspireResponse interspireResponse = xmlParser.ParseAddSubscriberResponse(_xmlFailedWithMessage);

			Assert.AreEqual((int)InterspireResponseStatus.Failed, interspireResponse.Status);
			Assert.AreEqual("The XML format you have sent is invalid.", interspireResponse.ErrorMessage);
		}

		[Test]
		public void ParseAddSubscriberResponse_ExecuteWithErrorXml_XmlCorrectlyParsed()
		{
			var xmlParser = IoC.Resolve<IInterspireXmlParser>();

			IInterspireResponse interspireResponse = xmlParser.ParseAddSubscriberResponse(_xmlErrorWithMessage);

			Assert.AreEqual((int)InterspireResponseStatus.Error, interspireResponse.Status);
			Assert.AreEqual("The XML format you have sent is invalid.", interspireResponse.ErrorMessage);
		}
	}
}