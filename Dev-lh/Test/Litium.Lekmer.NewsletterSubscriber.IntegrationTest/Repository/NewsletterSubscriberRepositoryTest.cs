﻿using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Repository
{
	[TestFixture]
	public class NewsletterSubscriberRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberRepository_Resolve_Resolved()
		{
			var instance = IoC.Resolve<NewsletterSubscriberRepository>();

			Assert.IsInstanceOf<NewsletterSubscriberRepository>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<NewsletterSubscriberRepository>();
			var instance2 = IoC.Resolve<NewsletterSubscriberRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}