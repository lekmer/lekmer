﻿using Litium.Lekmer.Order.Cache;
using Litium.Lekmer.Order.SecureService;
using Litium.Lekmer.Order.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.UnitTest.Service
{
	[TestFixture]
	public class LekmerBlockCheckoutServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int BLOCK_ID_1 = 510;
		private static readonly int BLOCK_ID_2 = 514;
		private static readonly int BLOCK_ID_3 = 515;
		private static readonly int BLOCK_ID_4 = 516;

		private static MockRepository _mocker;
		private static IUserContext _userContext;
		private static IAccessValidator _accessValidator;
		private static ISystemUserFull _systemUser;
		private static BlockCheckoutRepository _blockCheckoutRepository;
		private static LekmerBlockCheckoutService _lekmerBlockCheckoutService;
		private static LekmerBlockCheckoutSecureService _lekmerBlockCheckoutSecureService;
		private static IChannel _channel;
		private static ILanguage _language;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_userContext = _mocker.Stub<IUserContext>();
			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUser = _mocker.Stub<ISystemUserFull>();
			_blockCheckoutRepository = _mocker.DynamicMock<BlockCheckoutRepository>();
			_lekmerBlockCheckoutService = new LekmerBlockCheckoutService(_blockCheckoutRepository);
			_lekmerBlockCheckoutSecureService = new LekmerBlockCheckoutSecureService(
				_accessValidator, 
				_blockCheckoutRepository, 
				_mocker.Stub<IAccessSecureService>(), 
				_mocker.Stub<IBlockSecureService>());

			_language = _mocker.Stub<ILanguage>();
			_language.Id = CHANNEL_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			BlockCheckoutCache.Instance.Flush();
		}

		[Test]
		public void GetById_SameId_CacheUsed()
		{
			Expect.Call(_blockCheckoutRepository.GetById(_channel, BLOCK_ID_1)).Return(new BlockCheckout {Id = BLOCK_ID_1});

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_1, _lekmerBlockCheckoutService.GetById(_userContext, BLOCK_ID_1).Id);
			Assert.AreEqual(BLOCK_ID_1, _lekmerBlockCheckoutService.GetById(_userContext, BLOCK_ID_1).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_blockCheckoutRepository.GetById(_channel, BLOCK_ID_2)).Return(new BlockCheckout {Id = BLOCK_ID_2});
			Expect.Call(_blockCheckoutRepository.GetById(_channel, BLOCK_ID_3)).Return(new BlockCheckout { Id = BLOCK_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_2, _lekmerBlockCheckoutService.GetById(_userContext, BLOCK_ID_2).Id);
			Assert.AreEqual(BLOCK_ID_3, _lekmerBlockCheckoutService.GetById(_userContext, BLOCK_ID_3).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_BlockDeleted_CacheRemoved()
		{
			IBlockCheckout blockCheckout = new BlockCheckout {Id = BLOCK_ID_4};

			Expect.Call(() => _blockCheckoutRepository.Delete(BLOCK_ID_4));
			Expect.Call(_blockCheckoutRepository.GetById(_channel, BLOCK_ID_4)).Return(blockCheckout).Repeat.Twice();

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_4, _lekmerBlockCheckoutService.GetById(_userContext, BLOCK_ID_4).Id);
			_lekmerBlockCheckoutSecureService.Delete(_systemUser, BLOCK_ID_4);
			Assert.AreEqual(BLOCK_ID_4, _lekmerBlockCheckoutService.GetById(_userContext, BLOCK_ID_4).Id);

			_mocker.VerifyAll();
		}
	}
}