﻿using System.Transactions;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.Service
{
	[TestFixture]
	public class ProductSimilarServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductSimilarService_Resolve_Resolved()
		{
			IProductSimilarService productSimilarService = IoC.Resolve<IProductSimilarService>();

			Assert.IsInstanceOf<IProductSimilarService>(productSimilarService);
		}

		[Test]
		[Category("IoC")]
		public void ProductSimilarService_ResolveTwice_SameObjects()
		{
			IProductSimilarService productSimilarService1 = IoC.Resolve<IProductSimilarService>();
			IProductSimilarService productSimilarService2 = IoC.Resolve<IProductSimilarService>();

			Assert.AreEqual(productSimilarService1, productSimilarService2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAllByProductId_GetSimilarProduct_GetFromDB()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				ProductHelper productHelper = new ProductHelper();

				// Get product collection.
				ProductCollection productCollection = productHelper.GetProducts();

				// Save similar products.
				productHelper.SaveSimilarProduct(productCollection);

				int productId = productCollection[0].Id;

				IProductSimilarService productSimilarService = IoC.Resolve<IProductSimilarService>();
				IProductSimilar productSimilar = productSimilarService.GetAllByProductId(productId);

				Assert.AreEqual(productSimilar.ProductId, productId);
				Assert.AreEqual(productSimilar.SimilarProductCollection.Count, ProductHelper.SIMILAR_PRODUCT_COUNT);

				for (int i = 1; i < productCollection.Count; i++)
				{
					Assert.AreEqual(productSimilar.SimilarProductCollection[i - 1].ProductId, productCollection[i].Id);
					Assert.AreEqual(productSimilar.SimilarProductCollection[i - 1].Score, i);
				}

				transactionScope.Dispose();
			}
		}
	}
}