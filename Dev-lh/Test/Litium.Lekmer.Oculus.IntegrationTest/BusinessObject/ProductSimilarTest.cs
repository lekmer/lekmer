﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductSimilarTest
	{
		[Test]
		[Category("IoC")]
		public void ProductSimilar_Resolve_Resolved()
		{
			IProductSimilar productSimilar = IoC.Resolve<IProductSimilar>();

			Assert.IsInstanceOf<IProductSimilar>(productSimilar);
		}

		[Test]
		[Category("IoC")]
		public void ProductSimilar_ResolveTwice_DifferentObjects()
		{
			IProductSimilar productSimilar1 = IoC.Resolve<IProductSimilar>();
			IProductSimilar productSimilar2 = IoC.Resolve<IProductSimilar>();

			Assert.AreNotEqual(productSimilar1, productSimilar2);
		}
	}
}