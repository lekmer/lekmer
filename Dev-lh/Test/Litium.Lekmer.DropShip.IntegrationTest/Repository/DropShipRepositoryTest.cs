﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Repository
{
	[TestFixture]
	public class DropShipRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<DropShipRepository>();
			Assert.IsInstanceOf<DropShipRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void DropShipRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<DropShipRepository>();
			var service2 = IoC.Resolve<DropShipRepository>();
			Assert.AreEqual(service1, service2);
		}
	}
}