﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Task
{
	[TestFixture]
	public class DropShipExportTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipExport_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<ITask>("DropShipExport");

			Assert.IsInstanceOf<ITask>(exporter);
			Assert.IsInstanceOf<DropShipExport>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void DropShipExport_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<ITask>("DropShipExport");
			var exporter2 = IoC.Resolve<ITask>("DropShipExport");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}