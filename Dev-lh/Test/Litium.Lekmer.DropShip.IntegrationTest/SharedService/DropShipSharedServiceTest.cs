﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Service
{
	[TestFixture]
	public class DropShipSharedServiceTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipSharedService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IDropShipSharedService>();

			Assert.IsInstanceOf<IDropShipSharedService>(service);
			Assert.IsInstanceOf<DropShipSharedService>(service);
		}

		[Test]
		[Category("IoC")]
		public void DropShipSharedService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IDropShipSharedService>();
			var service2 = IoC.Resolve<IDropShipSharedService>();
			Assert.AreEqual(service1, service2);
		}
	}
}