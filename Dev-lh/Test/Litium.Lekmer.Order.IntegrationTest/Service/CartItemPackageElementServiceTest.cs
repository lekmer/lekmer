﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class CartItemPackageElementServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";

		private static readonly int PRODUCT_ID = 109547;
		private static readonly int PACKAGE_PRODUCT_ID = 1029167;
		private static readonly int PACKAGE_PRODUCT_ITEM_ID = 1025482;
		private static readonly int PACKAGE_PRODUCT_ITEM_SIZE_ID = 91;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;
		private ICurrency _currency;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_currency = _mocker.Stub<ICurrency>();
			_currency.Id = CURRENCY_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;
			_channel.Currency = _currency;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void CartItemPackageElementService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartItemPackageElementService>();

			Assert.IsInstanceOf<ICartItemPackageElementService>(service);
			Assert.IsInstanceOf<CartItemPackageElementService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartItemPackageElementService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartItemPackageElementService>();
			var instance2 = IoC.Resolve<ICartItemPackageElementService>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		[Category("DB")]
		public void Save_ExecuteCartSaveAndRetrieveBack_ElementsAreSavedAndRetrieved()
		{
			var cartService = CreateCartService();
			var cartItemService = IoC.Resolve<ICartItemService>();

			_mocker.ReplayAll();

			var cartFull = CreateCart(cartService, cartItemService);

			using (var transactionScope = new TransactionScope())
			{
				int id = cartService.Save(_userContext, cartFull);

				var cartFull2 = cartService.GetById(_userContext, id);

				var packageCartItem = (ILekmerCartItem)cartFull2.GetCartItems().FirstOrDefault(ci => ci.Product.IsPackage());

				Assert.IsNotNull(packageCartItem);
				Assert.AreEqual(PACKAGE_PRODUCT_ID, packageCartItem.Product.Id);
				Assert.IsNotNull(packageCartItem.PackageElements);
				Assert.AreEqual(1, packageCartItem.PackageElements.Count);
				Assert.AreEqual(PACKAGE_PRODUCT_ITEM_ID, packageCartItem.PackageElements[0].ProductId);
				Assert.AreEqual(PACKAGE_PRODUCT_ITEM_SIZE_ID, packageCartItem.PackageElements[0].SizeId);

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("DB")]
		public void Delete_DeletePackageItem_ElementsAreSavedAndRetrieved()
		{
			var cartService = CreateCartService();
			var cartItemService = IoC.Resolve<ICartItemService>();

			_mocker.ReplayAll();

			var cartFull = CreateCart(cartService, cartItemService);

			using (var transactionScope = new TransactionScope())
			{
				int id = cartService.Save(_userContext, cartFull);

				var cartFull2 = (ILekmerCartFull)cartService.GetById(_userContext, id);
				
				var packageCartItem = (ILekmerCartItem)cartFull2.GetCartItems().FirstOrDefault(ci => ci.Product.IsPackage());

				Assert.IsNotNull(packageCartItem);
				Assert.AreEqual(PACKAGE_PRODUCT_ID, packageCartItem.Product.Id);
				Assert.AreEqual(1, packageCartItem.PackageElements.Count);

				cartFull2.DeleteItem(PACKAGE_PRODUCT_ID, packageCartItem.Product.CampaignInfo.Price.IncludingVat, 99, null, packageCartItem.PackageElements.GetPackageElementsHashCode());

				cartService.Save(_userContext, cartFull2);

				var cartItemPackageElementService = IoC.Resolve<ICartItemPackageElementService>();

				Collection<ICartItemPackageElement> cartItemPackageElements = cartItemPackageElementService.GetAllByCart(_userContext, id);

				Assert.AreEqual(0, cartItemPackageElements.Count);

				transactionScope.Dispose();
			}
		}

		private ILekmerCartService CreateCartService()
		{
			var lekmerCartSession = _mocker.Stub<ILekmerCartSession>(); //Require HttpContext
			var cartCampaignProcessor = _mocker.Stub<ICartCampaignProcessor>(); // We wan't test cart campaigns here, also it has voucher session dependency from web.ioc
			var cartCookieService = _mocker.Stub<ICartCookieService>(); //Require HttpContext
			var cartSetting = _mocker.Stub<ILekmerCartSetting>();

			var cartService = _mocker.PartialMock<LekmerCartService>(
				IoC.Resolve<CartRepository>(),
				IoC.Resolve<ICartItemService>(),
				cartCampaignProcessor,
				IoC.Resolve<IProductService>(),
				lekmerCartSession,
				cartCookieService,
				cartSetting
			);

			//Expect.Call(() => cartService.ApplyCartCampaigns(null, null)).IgnoreArguments().Repeat.Any();

			return cartService;
		}

		private ICartFull CreateCart(ILekmerCartService cartService, ICartItemService cartItemService)
		{
			var cartFull = cartService.Create(_userContext);
			var cartItem1 = (ILekmerCartItem)cartItemService.Create();
			var cartItem2 = (ILekmerCartItem)cartItemService.Create();

			cartItem1.Id = 1;
			cartItem2.Id = 2;
			cartItem1.Quantity = 1;
			cartItem2.Quantity = 1;
			cartItem1.Product.Id = PRODUCT_ID;
			cartItem2.Product.Id = PACKAGE_PRODUCT_ID;
			cartItem1.Product.CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100m, 80m) };
			cartItem2.Product.CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(100m, 80m) };
			((ILekmerProduct)cartItem1.Product).ProductTypeId = (int)ProductTypeEnum.Product;
			((ILekmerProduct)cartItem2.Product).ProductTypeId = (int)ProductTypeEnum.Package;

			cartItem2.PackageElements = new Collection<ICartItemPackageElement>
				{
					new CartItemPackageElement
						{
							ProductId = PACKAGE_PRODUCT_ITEM_ID,
							SizeId = PACKAGE_PRODUCT_ITEM_SIZE_ID
						}
				};

			cartFull.AddItem(cartItem1);
			cartFull.AddItem(cartItem2);

			return cartFull;
		}
	}
}