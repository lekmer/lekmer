﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class PendingOrderServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PendingOrderService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPendingOrderService>();

			Assert.IsInstanceOf<IPendingOrderService>(service);
			Assert.IsInstanceOf<PendingOrderService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PendingOrderService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IPendingOrderService>();
			var instance2 = IoC.Resolve<IPendingOrderService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}