﻿using Litium.Lekmer.Order.Cookie;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Cookie
{
	[TestFixture]
	public class CartCookieServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartCookieService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartCookieService>();

			Assert.IsInstanceOf<ICartCookieService>(service);
			Assert.IsInstanceOf<CartCookieService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartCookieService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartCookieService>();
			var instance2 = IoC.Resolve<ICartCookieService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}