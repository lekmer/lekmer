﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderItemRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderItemRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderItemRepository>();

			Assert.IsInstanceOf<OrderItemRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderItemRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderItemRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<OrderItemRepository>();
			var repository2 = IoC.Resolve<OrderItemRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}