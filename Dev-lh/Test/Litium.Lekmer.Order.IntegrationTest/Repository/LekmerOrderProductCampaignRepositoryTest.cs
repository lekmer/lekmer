﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository.Campaign;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderProductCampaignRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderProductCampaignRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderProductCampaignRepository>();

			Assert.IsInstanceOf<OrderProductCampaignRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderProductCampaignRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderProductCampaignRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<OrderProductCampaignRepository>();
			var instance2 = IoC.Resolve<OrderProductCampaignRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}