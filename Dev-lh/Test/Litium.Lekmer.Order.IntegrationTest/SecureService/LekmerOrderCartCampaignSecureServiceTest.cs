﻿using Litium.Lekmer.Order.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Campaign;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerOrderCartCampaignSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderCartCampaignSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderCartCampaignSecureService>();

			Assert.IsInstanceOf<IOrderCartCampaignSecureService>(service);
			Assert.IsInstanceOf<OrderCartCampaignSecureService>(service);
			Assert.IsInstanceOf<LekmerOrderCartCampaignSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderCartCampaignSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderCartCampaignSecureService>();
			var service2 = IoC.Resolve<IOrderCartCampaignSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}