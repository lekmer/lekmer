﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerOrderPaymentSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderPaymentSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderPaymentSecureService>();

			Assert.IsInstanceOf<IOrderPaymentSecureService>(service);
			Assert.IsInstanceOf<OrderPaymentSecureService>(service);
			Assert.IsInstanceOf<LekmerOrderPaymentSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderPaymentSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderPaymentSecureService>();
			var service2 = IoC.Resolve<IOrderPaymentSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}