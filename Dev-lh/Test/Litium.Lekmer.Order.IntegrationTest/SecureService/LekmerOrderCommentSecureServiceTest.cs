﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerOrderCommentSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderCommentSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderCommentSecureService>();

			Assert.IsInstanceOf<IOrderCommentSecureService>(service);
			Assert.IsInstanceOf<OrderCommentSecureService>(service);
			Assert.IsInstanceOf<LekmerOrderCommentSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderCommentSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderCommentSecureService>();
			var service2 = IoC.Resolve<IOrderCommentSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}