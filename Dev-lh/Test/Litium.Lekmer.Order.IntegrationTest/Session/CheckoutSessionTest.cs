﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Session
{
	[TestFixture]
	public class CheckoutSessionTest
	{
		[Test]
		[Category("IoC")]
		public void CheckoutSession_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICheckoutSession>();

			Assert.IsInstanceOf<ICheckoutSession>(service);
			Assert.IsInstanceOf<CheckoutSession>(service);
		}

		[Test]
		[Category("IoC")]
		public void CheckoutSession_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICheckoutSession>();
			var instance2 = IoC.Resolve<ICheckoutSession>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}