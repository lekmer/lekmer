﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PackageOrderItemTest
	{
		[Test]
		[Category("IoC")]
		public void PackageOrderItem_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IPackageOrderItem>();

			Assert.IsInstanceOf<IPackageOrderItem>(instance);
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItem_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IPackageOrderItem>();
			var instance2 = IoC.Resolve<IPackageOrderItem>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}