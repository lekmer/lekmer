﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartItemOptionTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemOption_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartItemOption>();

			Assert.IsInstanceOf<ICartItemOption>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartItemOption_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartItemOption>();
			var instance2 = IoC.Resolve<ICartItemOption>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}