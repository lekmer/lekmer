﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartItemPackageElementTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemPackageElement_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartItemPackageElement>();

			Assert.IsInstanceOf<ICartItemPackageElement>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartItemPackageElement_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartItemPackageElement>();
			var instance2 = IoC.Resolve<ICartItemPackageElement>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}