﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerOrderAddressTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderAddress_Resolve_Resolved()
		{
			var address = IoC.Resolve<IOrderAddress>();

			Assert.IsInstanceOf<IOrderAddress>(address);
			Assert.IsInstanceOf<ILekmerOrderAddress>(address);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderAddress_ResolveTwice_DifferentObjects()
		{
			var address1 = IoC.Resolve<IOrderAddress>();
			var address2 = IoC.Resolve<IOrderAddress>();

			Assert.AreNotEqual(address1, address2);
		}
	}
}
