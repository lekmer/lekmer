﻿using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Setting
{
	[TestFixture]
	public class TrustPilotSettingTest
	{
		[Test]
		[Category("IoC")]
		public void TrustPilotSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITrustPilotSetting>();

			Assert.IsInstanceOf<ITrustPilotSetting>(service);
			Assert.IsInstanceOf<TrustPilotSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void TrustPilotSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<ITrustPilotSetting>();
			var setting2 = IoC.Resolve<ITrustPilotSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}