﻿using System.Data;
using Litium.Lekmer.Order.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Mapper
{
	[TestFixture]
	public class PackageOrderItemDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItemDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IPackageOrderItem>(_dataReader);

			Assert.IsInstanceOf<PackageOrderItemDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItemDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IPackageOrderItem>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IPackageOrderItem>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}