using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PagingTest
	{
		[Test]
		public void Paging_CreateP1S10_FirstNadLastNumbersAreFine()
		{
			IPaging paging = new Paging(1, 10);

			Assert.AreEqual(1, paging.FirstItemNumber);
			Assert.AreEqual(10, paging.LastItemNumber);
		}

		[Test]
		public void Paging_CreateP5S10_FirstNadLastNumbersAreFine()
		{
			IPaging paging = new Paging(5, 10);

			Assert.AreEqual(41, paging.FirstItemNumber);
			Assert.AreEqual(50, paging.LastItemNumber);
		}
	}
}