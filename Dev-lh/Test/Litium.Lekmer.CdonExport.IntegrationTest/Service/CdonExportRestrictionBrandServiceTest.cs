﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Service
{
	[TestFixture]
	public class CdonExportRestrictionBrandServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionBrandService>();

			Assert.IsInstanceOf<ICdonExportRestrictionBrandService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionBrandService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionBrandService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionBrandService>();
			Assert.AreEqual(service1, service2);
		}
	}
}