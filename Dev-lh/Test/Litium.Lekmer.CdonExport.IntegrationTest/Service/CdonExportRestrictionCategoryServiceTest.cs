﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Service
{
	[TestFixture]
	public class CdonExportRestrictionCategoryServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategoryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionCategoryService>();

			Assert.IsInstanceOf<ICdonExportRestrictionCategoryService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionCategoryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategoryService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionCategoryService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionCategoryService>();
			Assert.AreEqual(service1, service2);
		}
	}
}