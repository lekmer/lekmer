﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Repository
{
	[TestFixture]
	public class CdonExportRestrictionProductRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProductRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CdonExportRestrictionProductRepository>();
			Assert.IsInstanceOf<CdonExportRestrictionProductRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProductRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CdonExportRestrictionProductRepository>();
			var repository2 = IoC.Resolve<CdonExportRestrictionProductRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}