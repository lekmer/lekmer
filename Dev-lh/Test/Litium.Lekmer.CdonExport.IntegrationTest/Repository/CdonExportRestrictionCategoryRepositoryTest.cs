﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Repository
{
	[TestFixture]
	public class CdonExportRestrictionCategoryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategoryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CdonExportRestrictionCategoryRepository>();
			Assert.IsInstanceOf<CdonExportRestrictionCategoryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategoryRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CdonExportRestrictionCategoryRepository>();
			var repository2 = IoC.Resolve<CdonExportRestrictionCategoryRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}