﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.SecureService
{
	[TestFixture]
	public class CdonExportRestrictionProductSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProductSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionProductSecureService>();

			Assert.IsInstanceOf<ICdonExportRestrictionProductSecureService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionProductSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProductSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionProductSecureService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionProductSecureService>();
			Assert.AreEqual(service1, service2);
		}
	}
}