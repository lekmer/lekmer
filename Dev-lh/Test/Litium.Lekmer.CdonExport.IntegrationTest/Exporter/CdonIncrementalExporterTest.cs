﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Exporter
{
	[TestFixture]
	public class CdonIncrementalExporterTest
	{
		[Test]
		[Category("IoC")]
		public void CdonIncrementalExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("CdonIncrementalExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<CdonIncrementalExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void CdonIncrementalExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("CdonIncrementalExporter");
			var exporter2 = IoC.Resolve<IExporter>("CdonIncrementalExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}