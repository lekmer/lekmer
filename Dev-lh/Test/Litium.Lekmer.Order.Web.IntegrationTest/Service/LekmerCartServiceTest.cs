﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCartServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartService>();

			Assert.IsInstanceOf<ICartService>(service);
			Assert.IsInstanceOf<CartService>(service);
			Assert.IsInstanceOf<LekmerCartService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartService_LekmerResolve_Resolved()
		{
			var service = IoC.Resolve<ILekmerCartService>();

			Assert.IsInstanceOf<ICartService>(service);
			Assert.IsInstanceOf<CartService>(service);
			Assert.IsInstanceOf<LekmerCartService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICartService>();
			var service2 = IoC.Resolve<ICartService>();

			Assert.AreEqual(service1, service2);
		}
	}
}