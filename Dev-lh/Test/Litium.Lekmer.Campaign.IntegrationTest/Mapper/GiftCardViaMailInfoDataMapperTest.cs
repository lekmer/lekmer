﻿using System.Data;
using Litium.Lekmer.Campaign.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.IntegrationTest.Mapper
{
	[TestFixture]
	public class GiftCardViaMailInfoDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IGiftCardViaMailInfo>(_dataReader);

			Assert.IsInstanceOf<GiftCardViaMailInfoDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IGiftCardViaMailInfo>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IGiftCardViaMailInfo>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}