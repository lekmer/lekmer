﻿using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCartCampaignProcessorTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartCampaignProcessor_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartCampaignProcessor>();

			Assert.IsInstanceOf<ICartCampaignProcessor>(service);
			Assert.IsInstanceOf<CartCampaignProcessor>(service);
			Assert.IsInstanceOf<LekmerCartCampaignProcessor>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartCampaignProcessor_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICartCampaignProcessor>();
			var service2 = IoC.Resolve<ICartCampaignProcessor>();

			Assert.AreEqual(service1, service2);
		}
	}
}