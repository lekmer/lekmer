﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerCampaignRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCampaignRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignRepository>();

			Assert.IsInstanceOf<CampaignRepository>(repository);
			Assert.IsInstanceOf<LekmerCampaignRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaignRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CampaignRepository>();
			var repository2 = IoC.Resolve<CampaignRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}