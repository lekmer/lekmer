﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductDiscountActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductDiscountActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductDiscountActionRepository>();

			Assert.IsInstanceOf<ProductDiscountActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductDiscountActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ProductDiscountActionRepository>();
			var instance2 = IoC.Resolve<ProductDiscountActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}