﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CampaignPriceTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignPriceTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignPriceTypeRepository>();

			Assert.IsInstanceOf<CampaignPriceTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CampaignPriceTypeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CampaignPriceTypeRepository>();
			var instance2 = IoC.Resolve<CampaignPriceTypeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}