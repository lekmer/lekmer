﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class VoucherDiscountActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void VoucherDiscountActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<VoucherDiscountActionRepository>();

			Assert.IsInstanceOf<VoucherDiscountActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void VoucherDiscountActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<VoucherDiscountActionRepository>();
			var instance2 = IoC.Resolve<VoucherDiscountActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}