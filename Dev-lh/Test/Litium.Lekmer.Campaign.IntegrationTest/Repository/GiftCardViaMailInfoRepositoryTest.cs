﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class GiftCardViaMailInfoRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<GiftCardViaMailInfoRepository>();

			Assert.IsInstanceOf<GiftCardViaMailInfoRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<GiftCardViaMailInfoRepository>();
			var instance2 = IoC.Resolve<GiftCardViaMailInfoRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}