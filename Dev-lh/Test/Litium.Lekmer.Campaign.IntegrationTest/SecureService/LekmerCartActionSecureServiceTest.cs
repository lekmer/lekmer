﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCartActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionSecureService>();

			Assert.IsInstanceOf<ICartActionSecureService>(service);
			Assert.IsInstanceOf<LekmerCartActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionSecureService>();
			var instance2 = IoC.Resolve<ICartActionSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}