﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CartItemsGroupValueConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginSecureService>("secure_CartItemsGroupValue");

			Assert.IsInstanceOf<IConditionPluginSecureService>(service);
			Assert.IsInstanceOf<CartItemsGroupValueConditionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueConditionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginSecureService>("secure_CartItemsGroupValue");
			var instance2 = IoC.Resolve<IConditionPluginSecureService>("secure_CartItemsGroupValue");

			Assert.AreEqual(instance1, instance2);
		}
	}
}