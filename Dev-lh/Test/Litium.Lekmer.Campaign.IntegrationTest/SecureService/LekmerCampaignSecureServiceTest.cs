﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCampaignSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCampaignSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignSecureService>();

			Assert.IsInstanceOf<ICampaignSecureService>(service);
			Assert.IsInstanceOf<ICampaignSecureService>(service);
			Assert.IsInstanceOf<LekmerCampaignSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaignSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICampaignSecureService>();
			var service2 = IoC.Resolve<ICampaignSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}