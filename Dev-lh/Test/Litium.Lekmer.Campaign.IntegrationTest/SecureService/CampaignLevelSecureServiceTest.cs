﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CampaignLevelSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLevelSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignLevelSecureService>();

			Assert.IsInstanceOf<ICampaignLevelSecureService>(service);
			Assert.IsInstanceOf<CampaignLevelSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLevelSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignLevelSecureService>();
			var instance2 = IoC.Resolve<ICampaignLevelSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}