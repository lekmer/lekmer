﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CartItemDiscountCartActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginSecureService>("secure_CartItemDiscount");

			Assert.IsInstanceOf<ICartActionPluginSecureService>(service);
			Assert.IsInstanceOf<CartItemDiscountCartActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginSecureService>("secure_CartItemDiscount");
			var instance2 = IoC.Resolve<ICartActionPluginSecureService>("secure_CartItemDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}