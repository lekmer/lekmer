﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerPercentagePriceDiscountActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginSecureService>("secure_PercentagePriceDiscount");

			Assert.IsInstanceOf<IProductActionPluginSecureService>(service);
			Assert.IsInstanceOf<LekmerPercentagePriceDiscountActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginSecureService>("secure_PercentagePriceDiscount");
			var instance2 = IoC.Resolve<IProductActionPluginSecureService>("secure_PercentagePriceDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}