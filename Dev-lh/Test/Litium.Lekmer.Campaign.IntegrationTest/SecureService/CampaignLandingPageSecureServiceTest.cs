﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CampaignLandingPageSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLandingPageSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignLandingPageSecureService>();

			Assert.IsInstanceOf<ICampaignLandingPageSecureService>(service);
			Assert.IsInstanceOf<CampaignLandingPageSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLandingPageSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignLandingPageSecureService>();
			var instance2 = IoC.Resolve<ICampaignLandingPageSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}