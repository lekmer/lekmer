﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductAutoFreeCartActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginSecureService>("secure_ProductAutoFree");

			Assert.IsInstanceOf<ICartActionPluginSecureService>(service);
			Assert.IsInstanceOf<ProductAutoFreeCartActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginSecureService>("secure_ProductAutoFree");
			var instance2 = IoC.Resolve<ICartActionPluginSecureService>("secure_ProductAutoFree");

			Assert.AreEqual(instance1, instance2);
		}
	}
}