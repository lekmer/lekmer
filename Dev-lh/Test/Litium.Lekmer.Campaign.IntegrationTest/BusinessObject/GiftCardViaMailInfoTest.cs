﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class GiftCardViaMailInfoTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfo_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IGiftCardViaMailInfo>();

			Assert.IsInstanceOf<IGiftCardViaMailInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfo_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IGiftCardViaMailInfo>();
			var instance2 = IoC.Resolve<IGiftCardViaMailInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}