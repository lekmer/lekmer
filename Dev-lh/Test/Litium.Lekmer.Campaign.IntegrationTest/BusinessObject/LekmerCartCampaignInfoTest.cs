﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCartCampaignInfoTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartCampaignInfo_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartCampaignInfo>();

			Assert.IsInstanceOf<ICartCampaignInfo>(instance);
			Assert.IsInstanceOf<ILekmerCartCampaignInfo>(instance);
			Assert.IsInstanceOf<LekmerCartCampaignInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartCampaignInfo_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartCampaignInfo>();
			var instance2 = IoC.Resolve<ICartCampaignInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}