﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignLevelTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLevel_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignLevel>();
			Assert.IsInstanceOf<ICampaignLevel>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLevel_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignLevel>();
			var instance2 = IoC.Resolve<ICampaignLevel>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}