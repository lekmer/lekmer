﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class GiftCardViaMailProductActionTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IGiftCardViaMailProductAction>();

			Assert.IsInstanceOf<IGiftCardViaMailProductAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IGiftCardViaMailProductAction>();
			var instance2 = IoC.Resolve<IGiftCardViaMailProductAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}