﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerConditionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCondition_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICondition>();

			Assert.IsInstanceOf<ICondition>(instance);
			Assert.IsInstanceOf<ILekmerCondition>(instance);
			Assert.IsInstanceOf<LekmerCondition>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCondition_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICondition>();
			var instance2 = IoC.Resolve<ICondition>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}