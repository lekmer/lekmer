﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CustomerGroupConditionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginService>("CustomerGroupUser");

			Assert.IsInstanceOf<IConditionPluginService>(service);
			Assert.IsInstanceOf<CustomerGroupConditionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginService>("CustomerGroupUser");
			var instance2 = IoC.Resolve<IConditionPluginService>("CustomerGroupUser");

			Assert.AreEqual(instance1, instance2);
		}
	}
}