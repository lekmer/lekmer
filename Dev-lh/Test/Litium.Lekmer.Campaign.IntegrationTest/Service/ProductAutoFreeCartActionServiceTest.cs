﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class ProductAutoFreeCartActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginService>("ProductAutoFree");

			Assert.IsInstanceOf<ICartActionPluginService>(service);
			Assert.IsInstanceOf<ProductAutoFreeCartActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginService>("ProductAutoFree");
			var instance2 = IoC.Resolve<ICartActionPluginService>("ProductAutoFree");

			Assert.AreEqual(instance1, instance2);
		}
	}
}