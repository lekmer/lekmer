﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class ProductActionTargetProductTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionTargetProductTypeService>();

			Assert.IsInstanceOf<IProductActionTargetProductTypeService>(service);
			Assert.IsInstanceOf<ProductActionTargetProductTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductTypeService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionTargetProductTypeService>();
			var instance2 = IoC.Resolve<IProductActionTargetProductTypeService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}