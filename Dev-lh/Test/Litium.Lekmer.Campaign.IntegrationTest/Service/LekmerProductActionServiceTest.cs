﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerProductActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerProductActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionService>();

			Assert.IsInstanceOf<IProductActionService>(service);
			Assert.IsInstanceOf<LekmerProductActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionService>();
			var instance2 = IoC.Resolve<IProductActionService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}