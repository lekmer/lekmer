﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartValueRangeConditionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartValueRangeConditionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginService>("CartValueRange");

			Assert.IsInstanceOf<IConditionPluginService>(service);
			Assert.IsInstanceOf<CartValueRangeConditionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartValueRangeConditionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginService>("CartValueRange");
			var instance2 = IoC.Resolve<IConditionPluginService>("CartValueRange");

			Assert.AreEqual(instance1, instance2);
		}
	}
}