﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CampaignConfigServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignConfigService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignConfigService>();

			Assert.IsInstanceOf<ICampaignConfigService>(service);
			Assert.IsInstanceOf<CampaignConfigService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignConfigService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignConfigService>();
			var instance2 = IoC.Resolve<ICampaignConfigService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}