﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerFreightValueActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginService>("FreightValue");

			Assert.IsInstanceOf<ICartActionPluginService>(service);
			Assert.IsInstanceOf<LekmerFreightValueActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginService>("FreightValue");
			var instance2 = IoC.Resolve<ICartActionPluginService>("FreightValue");

			Assert.AreEqual(instance1, instance2);
		}
	}
}