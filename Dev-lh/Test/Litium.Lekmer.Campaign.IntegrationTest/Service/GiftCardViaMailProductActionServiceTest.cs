﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class GiftCardViaMailProductActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginService>("GiftCardViaMailProduct");

			Assert.IsInstanceOf<IProductActionPluginService>(service);
			Assert.IsInstanceOf<GiftCardViaMailProductActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginService>("GiftCardViaMailProduct");
			var instance2 = IoC.Resolve<IProductActionPluginService>("GiftCardViaMailProduct");

			Assert.AreEqual(instance1, instance2);
		}
	}
}