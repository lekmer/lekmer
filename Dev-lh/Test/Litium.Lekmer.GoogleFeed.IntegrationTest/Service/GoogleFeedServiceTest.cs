﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GoogleFeed.IntegrationTest.Service
{
	[TestFixture]
	public class GoogleFeedServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GoogleFeedService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IGoogleFeedService>();

			Assert.IsInstanceOf<IGoogleFeedService>(service);
			Assert.IsInstanceOf<GoogleFeedService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GoogleFeedService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IGoogleFeedService>();
			var service2 = IoC.Resolve<IGoogleFeedService>();
			Assert.AreEqual(service1, service2);
		}
	}
}