﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GoogleFeed.IntegrationTest.Exporter
{
	[TestFixture]
	public class GoogleFeedExporterTest
	{
		[Test]
		[Category("IoC")]
		public void GoogleFeedExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("GoogleFeedExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<GoogleFeedExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void GoogleFeedExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("GoogleFeedExporter");
			var exporter2 = IoC.Resolve<IExporter>("GoogleFeedExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}