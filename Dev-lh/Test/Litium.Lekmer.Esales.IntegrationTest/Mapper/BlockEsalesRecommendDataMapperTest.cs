﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockEsalesRecommendDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockEsalesRecommend> dataMapper = DataMapperResolver.Resolve<IBlockEsalesRecommend>(_dataReader);

			Assert.IsInstanceOf<BlockEsalesRecommendDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockEsalesRecommend> dataMapper1 = DataMapperResolver.Resolve<IBlockEsalesRecommend>(_dataReader);
			DataMapperBase<IBlockEsalesRecommend> dataMapper2 = DataMapperResolver.Resolve<IBlockEsalesRecommend>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}