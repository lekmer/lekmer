﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockEsalesTopSellersProductDataMapperTestV2
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductDataMapperV2_Resolve_Resolved()
		{
			DataMapperBase<IBlockEsalesTopSellersProductV2> dataMapper = DataMapperResolver.Resolve<IBlockEsalesTopSellersProductV2>(_dataReader);

			Assert.IsInstanceOf<BlockEsalesTopSellersProductDataMapperV2>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductDataMapperV2_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockEsalesTopSellersProductV2> dataMapper1 = DataMapperResolver.Resolve<IBlockEsalesTopSellersProductV2>(_dataReader);
			DataMapperBase<IBlockEsalesTopSellersProductV2> dataMapper2 = DataMapperResolver.Resolve<IBlockEsalesTopSellersProductV2>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}