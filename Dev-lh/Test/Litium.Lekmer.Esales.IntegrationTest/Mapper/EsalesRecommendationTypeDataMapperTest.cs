﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class EsalesRecommendationTypeDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void EsalesRecommendationTypeDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IEsalesRecommendationType> dataMapper = DataMapperResolver.Resolve<IEsalesRecommendationType>(_dataReader);

			Assert.IsInstanceOf<EsalesRecommendationTypeDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRecommendationTypeDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IEsalesRecommendationType> dataMapper1 = DataMapperResolver.Resolve<IEsalesRecommendationType>(_dataReader);
			DataMapperBase<IEsalesRecommendationType> dataMapper2 = DataMapperResolver.Resolve<IEsalesRecommendationType>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}