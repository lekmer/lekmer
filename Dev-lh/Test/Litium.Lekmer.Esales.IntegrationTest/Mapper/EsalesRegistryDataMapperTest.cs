﻿using System.Data;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class EsalesRegistryDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void EsalesRegistryDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IEsalesRegistry>(_dataReader);

			Assert.IsInstanceOf<EsalesRegistryDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRegistryDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IEsalesRegistry>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IEsalesRegistry>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}