﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesRecommendServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesRecommendService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesRecommendService>();

			Assert.IsInstanceOf<IEsalesRecommendService>(service);
			Assert.IsInstanceOf<EsalesRecommendService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRecommendService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesRecommendService>();
			var service2 = IoC.Resolve<IEsalesRecommendService>();

			Assert.AreEqual(service1, service2);
		}
	}
}