﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class BlockEsalesTopSellersCategoryServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesTopSellersCategoryServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersCategoryServiceV2>(service);
			Assert.IsInstanceOf<BlockEsalesTopSellersCategoryServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryServiceV2_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockEsalesTopSellersCategoryServiceV2>();
			var instance2 = IoC.Resolve<IBlockEsalesTopSellersCategoryServiceV2>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}