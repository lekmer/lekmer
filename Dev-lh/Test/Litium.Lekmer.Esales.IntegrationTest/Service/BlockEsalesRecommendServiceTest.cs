﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class BlockEsalesRecommendServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesRecommendService>();

			Assert.IsInstanceOf<IBlockEsalesRecommendService>(service);
			Assert.IsInstanceOf<BlockEsalesRecommendService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockEsalesRecommendService>();
			var instance2 = IoC.Resolve<IBlockEsalesRecommendService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}