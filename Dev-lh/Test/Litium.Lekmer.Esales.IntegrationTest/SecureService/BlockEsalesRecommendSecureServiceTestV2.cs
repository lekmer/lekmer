﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockEsalesRecommendSecureServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendSecureServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesRecommendSecureServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesRecommendSecureServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendSecureServiceV2_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockEsalesRecommendSecureServiceV2>();
			var service2 = IoC.Resolve<IBlockEsalesRecommendSecureServiceV2>();

			Assert.AreEqual(service1, service2);
		}
	}
}