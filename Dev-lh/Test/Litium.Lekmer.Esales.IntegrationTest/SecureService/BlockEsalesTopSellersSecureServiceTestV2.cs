﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockEsalesTopSellersSecureServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersSecureServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesTopSellersSecureServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersSecureServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersSecureServiceV2_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockEsalesTopSellersSecureServiceV2>();
			var service2 = IoC.Resolve<IBlockEsalesTopSellersSecureServiceV2>();

			Assert.AreEqual(service1, service2);
		}
	}
}