﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockEsalesAdsSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesAdsSecureService>();

			Assert.IsInstanceOf<IBlockEsalesAdsSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockEsalesAdsSecureService>();
			var service2 = IoC.Resolve<IBlockEsalesAdsSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}