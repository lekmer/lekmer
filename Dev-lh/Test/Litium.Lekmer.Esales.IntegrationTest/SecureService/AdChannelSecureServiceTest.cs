﻿using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.Esales.IntegrationTest.Helper;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class AdChannelSecureServiceTest
	{
		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private IAdChannelSecureService _AdChannelSecureService;
		//private IAdSecureService _adSecureService;
		//private AdChannelTestHelper _AdChannelTestHelper;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();

			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();

			//_adSecureService = _mocker.Stub<IAdSecureService>();
			_AdChannelSecureService = new AdChannelSecureService(new AdChannelRepository(), _accessValidator/*, _adSecureService*/);
			//_AdChannelTestHelper = new AdChannelTestHelper(_AdChannelSecureService, _systemUserFull);
		}

		[Test]
		[Category("IoC")]
		public void AdChannelSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IAdChannelSecureService>();

			Assert.IsInstanceOf<IAdChannelSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void AdChannelSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IAdChannelSecureService>();
			var instance2 = IoC.Resolve<IAdChannelSecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveNewAdChannel_FolderSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		var AdChannel = _AdChannelTestHelper.Create("Test Ad folder");

		//		// Save
		//		var actualFolder = _AdChannelSecureService.Save(_systemUserFull, AdChannel);

		//		Assert.IsTrue(actualFolder.Id > 0);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveAdChannelWithSameTitleOnRootLevel_FolderNotSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel1 = _AdChannelTestHelper.Create("Test Ad folder same title");
		//		var AdChannel2 = _AdChannelTestHelper.Create("Test Ad folder same title");

		//		// Save
		//		var actualFolder1 = _AdChannelSecureService.Save(_systemUserFull, AdChannel1);
		//		var actualFolder2 = _AdChannelSecureService.Save(_systemUserFull, AdChannel2);

		//		// Check if it works
		//		Assert.Greater(actualFolder1.Id, 0, "AdChannel.Id");
		//		Assert.AreEqual(-1, actualFolder2.Id, "AdChannel.Id with same title");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveAdChannelWithSameTitleOnSubLevel_FolderNotSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var rootFolder = _AdChannelTestHelper.CreateAndSave("Test root Ad folder");

		//		var AdChannel1 = _AdChannelTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);
		//		var AdChannel2 = _AdChannelTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);

		//		// Check if it works
		//		Assert.Greater(rootFolder.Id, 0, "AdChannel.Id");
		//		Assert.Greater(AdChannel1.Id, 0, "AdChannel.Id");
		//		Assert.AreEqual(-1, AdChannel2.Id, "AdChannel.Id with same title");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_UpdateAdChannel_FolderUpdated()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var rootFolder1 = _AdChannelTestHelper.CreateAndSave("Test root Ad folder 1");
		//		var rootFolder2 = _AdChannelTestHelper.CreateAndSave("Test root Ad folder 2");

		//		// Create folder
		//		var AdChannel = _AdChannelTestHelper.CreateAndSave("Test Ad folder", rootFolder1);

		//		// Update
		//		AdChannel.ParentFolderId = rootFolder2.Id;
		//		AdChannel.Title = "Test Ad folder updated";
				
		//		// Update in db
		//		var actualFolder = _AdChannelSecureService.Save(_systemUserFull, AdChannel);

		//		// Check if it works
		//		Assert.AreEqual(AdChannel.Id, actualFolder.Id, "AdChannel.Id");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetById_GetFolderById_CorrectFolderReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel = _AdChannelTestHelper.CreateAndSave("Test Ad folder 1");

		//		// Get all
		//		var actualAdChannel = _AdChannelSecureService.GetById(AdChannel.Id);

		//		_AdChannelTestHelper.AssertAreEquel(AdChannel, actualAdChannel);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetAll_GetAllFolders_AllFoldersReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel1 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 1");
		//		var AdChannel2 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 2");
		//		var AdChannel3 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 3");

		//		// Get all
		//		var AdChannels = _AdChannelSecureService.GetAll();
				
		//		_AdChannelTestHelper.AssertContains(AdChannel1, AdChannels);
		//		_AdChannelTestHelper.AssertContains(AdChannel2, AdChannels);
		//		_AdChannelTestHelper.AssertContains(AdChannel3, AdChannels);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetAllByParent_GetAllFoldersBySomeParent_AllSubFoldersReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel1 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 1");
		//		var AdChannel2 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 2", AdChannel1);
		//		var AdChannel3 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 3", AdChannel1);

		//		// Get by parent
		//		var AdChannels = _AdChannelSecureService.GetAllByParent(AdChannel1.Id);

		//		_AdChannelTestHelper.AssertDoesNotContain(AdChannel1, AdChannels);
		//		_AdChannelTestHelper.AssertContains(AdChannel2, AdChannels);
		//		_AdChannelTestHelper.AssertContains(AdChannel3, AdChannels);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetTree_SelectedIdNull_CorrectNodesReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel1 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 1");
		//		var AdChannel2 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 2", AdChannel1);
		//		var AdChannel3 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 3", AdChannel2);
		//		var AdChannel4 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 4");

		//		// Get by parent
		//		Collection<INode> AdChannelNodes = _AdChannelSecureService.GetTree(null);

		//		_AdChannelTestHelper.AssertContains(AdChannel1, AdChannelNodes);
		//		_AdChannelTestHelper.AssertDoesNotContain(AdChannel2, AdChannelNodes);
		//		_AdChannelTestHelper.AssertDoesNotContain(AdChannel3, AdChannelNodes);
		//		_AdChannelTestHelper.AssertContains(AdChannel4, AdChannelNodes);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetTree_SecondLevelSelected_CorrectNodesReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel1 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 1");
		//		var AdChannel2 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 2", AdChannel1);
		//		var AdChannel3 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 3", AdChannel2);
		//		var AdChannel4 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 4", AdChannel3);
		//		var AdChannel5 = _AdChannelTestHelper.CreateAndSave("Test Ad folder 5");

		//		// Get by parent
		//		Collection<INode> AdChannelNodes = _AdChannelSecureService.GetTree(AdChannel2.Id);

		//		_AdChannelTestHelper.AssertContains(AdChannel1, AdChannelNodes);
		//		_AdChannelTestHelper.AssertContains(AdChannel2, AdChannelNodes);
		//		_AdChannelTestHelper.AssertContains(AdChannel3, AdChannelNodes);
		//		// Node 4 is not included in output
		//		_AdChannelTestHelper.AssertDoesNotContain(AdChannel4, AdChannelNodes);
		//		_AdChannelTestHelper.AssertContains(AdChannel5, AdChannelNodes);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Delete")]
		//public void TryDelete_DeleteSingleFolder_FolderRemoved()
		//{
		//	 //Expect.Call(_adSecureService.GetAllByFolder(0)).Return(new Collection<IAd>() ).IgnoreArguments();

		//	_mocker.ReplayAll();

		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel = _AdChannelTestHelper.CreateAndSave("Test Ad folder");

		//		// Get by parent
		//		bool isDeleted = _AdChannelSecureService.TryDelete(_systemUserFull, AdChannel.Id);
		//		var actualAdChannel = _AdChannelSecureService.GetById(AdChannel.Id);

		//		Assert.IsTrue(isDeleted, "AdChannel deleted");
		//		Assert.IsNull(actualAdChannel, "AdChannel deleted");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Delete")]
		//public void TryDelete_DeleteFolderWithSubFoldersAndAds_FolderNotRemoved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var AdChannel1 = _AdChannelTestHelper.CreateAndSave("Test Ad folder");
		//		var AdChannel2 = _AdChannelTestHelper.CreateAndSave("Test Ad folder", AdChannel1);

		//		//Expect
		//		//	.Call(_adSecureService.GetAllByFolder(AdChannel1.Id))
		//		//	.Return(new Collection<IAd>());

		//		//Expect
		//		//	.Call(_adSecureService.GetAllByFolder(AdChannel2.Id))
		//		//	.Return(new Collection<IAd> { new Ad() });

		//		_mocker.ReplayAll();
				
		//		// Get by parent
		//		bool isDeleted = _AdChannelSecureService.TryDelete(_systemUserFull, AdChannel1.Id);

		//		Assert.IsFalse(isDeleted, "AdChannel deleted");

		//		transactionScope.Dispose();
		//	}
		//}
	}
}