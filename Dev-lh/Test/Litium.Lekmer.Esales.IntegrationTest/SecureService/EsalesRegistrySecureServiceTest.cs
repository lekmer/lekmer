﻿using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.Esales.IntegrationTest.Helper;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class EsalesRegistrySecureServiceTest
	{
		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private IEsalesRegistrySecureService _EsalesRegistrySecureService;
		//private IAdSecureService _adSecureService;
		//private EsalesRegistryTestHelper _EsalesRegistryTestHelper;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();

			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();

			//_adSecureService = _mocker.Stub<IAdSecureService>();
			_EsalesRegistrySecureService = new EsalesRegistrySecureService(new EsalesRegistryRepository(), _accessValidator/*, _adSecureService*/);
			//_EsalesRegistryTestHelper = new EsalesRegistryTestHelper(_EsalesRegistrySecureService, _systemUserFull);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRegistrySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesRegistrySecureService>();

			Assert.IsInstanceOf<IEsalesRegistrySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRegistrySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IEsalesRegistrySecureService>();
			var instance2 = IoC.Resolve<IEsalesRegistrySecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveNewEsalesRegistry_FolderSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		var EsalesRegistry = _EsalesRegistryTestHelper.Create("Test Ad folder");

		//		// Save
		//		var actualFolder = _EsalesRegistrySecureService.Save(_systemUserFull, EsalesRegistry);

		//		Assert.IsTrue(actualFolder.Id > 0);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveEsalesRegistryWithSameTitleOnRootLevel_FolderNotSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.Create("Test Ad folder same title");
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.Create("Test Ad folder same title");

		//		// Save
		//		var actualFolder1 = _EsalesRegistrySecureService.Save(_systemUserFull, EsalesRegistry1);
		//		var actualFolder2 = _EsalesRegistrySecureService.Save(_systemUserFull, EsalesRegistry2);

		//		// Check if it works
		//		Assert.Greater(actualFolder1.Id, 0, "EsalesRegistry.Id");
		//		Assert.AreEqual(-1, actualFolder2.Id, "EsalesRegistry.Id with same title");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_SaveEsalesRegistryWithSameTitleOnSubLevel_FolderNotSaved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var rootFolder = _EsalesRegistryTestHelper.CreateAndSave("Test root Ad folder");

		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder same title", rootFolder);

		//		// Check if it works
		//		Assert.Greater(rootFolder.Id, 0, "EsalesRegistry.Id");
		//		Assert.Greater(EsalesRegistry1.Id, 0, "EsalesRegistry.Id");
		//		Assert.AreEqual(-1, EsalesRegistry2.Id, "EsalesRegistry.Id with same title");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Save")]
		//public void Save_UpdateEsalesRegistry_FolderUpdated()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var rootFolder1 = _EsalesRegistryTestHelper.CreateAndSave("Test root Ad folder 1");
		//		var rootFolder2 = _EsalesRegistryTestHelper.CreateAndSave("Test root Ad folder 2");

		//		// Create folder
		//		var EsalesRegistry = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder", rootFolder1);

		//		// Update
		//		EsalesRegistry.ParentFolderId = rootFolder2.Id;
		//		EsalesRegistry.Title = "Test Ad folder updated";
				
		//		// Update in db
		//		var actualFolder = _EsalesRegistrySecureService.Save(_systemUserFull, EsalesRegistry);

		//		// Check if it works
		//		Assert.AreEqual(EsalesRegistry.Id, actualFolder.Id, "EsalesRegistry.Id");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetById_GetFolderById_CorrectFolderReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 1");

		//		// Get all
		//		var actualEsalesRegistry = _EsalesRegistrySecureService.GetById(EsalesRegistry.Id);

		//		_EsalesRegistryTestHelper.AssertAreEquel(EsalesRegistry, actualEsalesRegistry);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetAll_GetAllFolders_AllFoldersReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 1");
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 2");
		//		var EsalesRegistry3 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 3");

		//		// Get all
		//		var EsalesRegistrys = _EsalesRegistrySecureService.GetAll();
				
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry1, EsalesRegistrys);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry2, EsalesRegistrys);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry3, EsalesRegistrys);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetAllByParent_GetAllFoldersBySomeParent_AllSubFoldersReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 1");
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 2", EsalesRegistry1);
		//		var EsalesRegistry3 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 3", EsalesRegistry1);

		//		// Get by parent
		//		var EsalesRegistrys = _EsalesRegistrySecureService.GetAllByParent(EsalesRegistry1.Id);

		//		_EsalesRegistryTestHelper.AssertDoesNotContain(EsalesRegistry1, EsalesRegistrys);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry2, EsalesRegistrys);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry3, EsalesRegistrys);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetTree_SelectedIdNull_CorrectNodesReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 1");
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 2", EsalesRegistry1);
		//		var EsalesRegistry3 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 3", EsalesRegistry2);
		//		var EsalesRegistry4 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 4");

		//		// Get by parent
		//		Collection<INode> EsalesRegistryNodes = _EsalesRegistrySecureService.GetTree(null);

		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry1, EsalesRegistryNodes);
		//		_EsalesRegistryTestHelper.AssertDoesNotContain(EsalesRegistry2, EsalesRegistryNodes);
		//		_EsalesRegistryTestHelper.AssertDoesNotContain(EsalesRegistry3, EsalesRegistryNodes);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry4, EsalesRegistryNodes);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Get")]
		//public void GetTree_SecondLevelSelected_CorrectNodesReturned()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 1");
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 2", EsalesRegistry1);
		//		var EsalesRegistry3 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 3", EsalesRegistry2);
		//		var EsalesRegistry4 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 4", EsalesRegistry3);
		//		var EsalesRegistry5 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder 5");

		//		// Get by parent
		//		Collection<INode> EsalesRegistryNodes = _EsalesRegistrySecureService.GetTree(EsalesRegistry2.Id);

		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry1, EsalesRegistryNodes);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry2, EsalesRegistryNodes);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry3, EsalesRegistryNodes);
		//		// Node 4 is not included in output
		//		_EsalesRegistryTestHelper.AssertDoesNotContain(EsalesRegistry4, EsalesRegistryNodes);
		//		_EsalesRegistryTestHelper.AssertContains(EsalesRegistry5, EsalesRegistryNodes);

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Delete")]
		//public void TryDelete_DeleteSingleFolder_FolderRemoved()
		//{
		//	 //Expect.Call(_adSecureService.GetAllByFolder(0)).Return(new Collection<IAd>() ).IgnoreArguments();

		//	_mocker.ReplayAll();

		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder");

		//		// Get by parent
		//		bool isDeleted = _EsalesRegistrySecureService.TryDelete(_systemUserFull, EsalesRegistry.Id);
		//		var actualEsalesRegistry = _EsalesRegistrySecureService.GetById(EsalesRegistry.Id);

		//		Assert.IsTrue(isDeleted, "EsalesRegistry deleted");
		//		Assert.IsNull(actualEsalesRegistry, "EsalesRegistry deleted");

		//		transactionScope.Dispose();
		//	}
		//}

		//[Test]
		//[Category("Database.Delete")]
		//public void TryDelete_DeleteFolderWithSubFoldersAndAds_FolderNotRemoved()
		//{
		//	using (var transactionScope = new TransactionScope())
		//	{
		//		// Create new folders
		//		var EsalesRegistry1 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder");
		//		var EsalesRegistry2 = _EsalesRegistryTestHelper.CreateAndSave("Test Ad folder", EsalesRegistry1);

		//		//Expect
		//		//	.Call(_adSecureService.GetAllByFolder(EsalesRegistry1.Id))
		//		//	.Return(new Collection<IAd>());

		//		//Expect
		//		//	.Call(_adSecureService.GetAllByFolder(EsalesRegistry2.Id))
		//		//	.Return(new Collection<IAd> { new Ad() });

		//		_mocker.ReplayAll();
				
		//		// Get by parent
		//		bool isDeleted = _EsalesRegistrySecureService.TryDelete(_systemUserFull, EsalesRegistry1.Id);

		//		Assert.IsFalse(isDeleted, "EsalesRegistry deleted");

		//		transactionScope.Dispose();
		//	}
		//}
	}
}