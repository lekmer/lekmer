﻿using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Setting
{
	[TestFixture]
	public class EsalesSettingTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesSetting>();

			Assert.IsInstanceOf<IEsalesSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesSetting_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesSetting>();
			var service2 = IoC.Resolve<IEsalesSetting>();

			Assert.AreEqual(service1, service2);
		}
	}
}