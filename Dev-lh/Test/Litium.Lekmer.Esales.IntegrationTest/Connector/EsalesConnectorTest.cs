﻿using Litium.Lekmer.Esales.Connector;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Connector
{
	[TestFixture]
	public class EsalesConnectorTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesConnector_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesConnector>();

			Assert.IsInstanceOf<IEsalesConnector>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesConnector_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesConnector>();
			var service2 = IoC.Resolve<IEsalesConnector>();

			Assert.AreEqual(service1, service2);
		}
	}
}