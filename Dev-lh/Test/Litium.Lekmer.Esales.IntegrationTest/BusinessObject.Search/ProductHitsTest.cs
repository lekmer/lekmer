using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class ProductHitsTest
	{
		[Test]
		[Category("IoC")]
		public void ProductHits_Resolve_Resolved()
		{
			var info = IoC.Resolve<IProductHits>();

			Assert.IsInstanceOf<IProductHits>(info);
			Assert.IsInstanceOf<ProductHits>(info);
		}

		[Test]
		[Category("IoC")]
		public void ProductHits_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IProductHits>();
			var esalesInfo2 = IoC.Resolve<IProductHits>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}