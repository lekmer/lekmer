using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class AdInfoTest
	{
		[Test]
		[Category("IoC")]
		public void AdInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IAdInfo>();

			Assert.IsInstanceOf<IAdInfo>(info);
			Assert.IsInstanceOf<AdInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void AdInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IAdInfo>();
			var esalesInfo2 = IoC.Resolve<IAdInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}