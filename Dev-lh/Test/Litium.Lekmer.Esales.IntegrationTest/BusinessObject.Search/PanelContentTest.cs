using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class PanelContentTest
	{
		[Test]
		[Category("IoC")]
		public void PanelContent_Resolve_Resolved()
		{
			var info = IoC.Resolve<IPanelContent>();

			Assert.IsInstanceOf<IPanelContent>(info);
			Assert.IsInstanceOf<PanelContent>(info);
		}

		[Test]
		[Category("IoC")]
		public void PanelContent_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IPanelContent>();
			var esalesInfo2 = IoC.Resolve<IPanelContent>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}