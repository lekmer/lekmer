using System.Collections.Generic;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class RecommendEsalesRequestTest
	{
		[Test]
		[Category("IoC")]
		public void RecommendEsalesRequest_Resolve_Resolved()
		{
			var info = IoC.Resolve<IRecommendEsalesRequest>();

			Assert.IsInstanceOf<IRecommendEsalesRequest>(info);
			Assert.IsInstanceOf<RecommendEsalesRequest>(info);
		}

		[Test]
		[Category("IoC")]
		public void RecommendEsalesRequest_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IRecommendEsalesRequest>();
			var esalesInfo2 = IoC.Resolve<IRecommendEsalesRequest>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}

		[Test]
		public void RecommendEsalesRequest_SetChannel_CorrectFilter()
		{
			var esalesRequest = IoC.Resolve<IRecommendEsalesRequest>();
			esalesRequest.ChannelId = "1";

			Dictionary<string, string> arguments = esalesRequest.ComposeArguments();

			Assert.AreEqual("available_in_channels:'1'", arguments["filter"]);
		}

		[Test]
		public void RecommendEsalesRequest_ExcludeProductIds_CorrectFilter()
		{
			var esalesRequest = IoC.Resolve<IRecommendEsalesRequest>();
			esalesRequest.ChannelId = "1";

			esalesRequest.ExcludeProductIds = new List<int>{1, 2, 3};

			Dictionary<string, string> arguments = esalesRequest.ComposeArguments();

			Assert.AreEqual("available_in_channels:'1' AND NOT (product_key:'1' OR product_key:'2' OR product_key:'3')", arguments["filter"]);
		}

		[Test]
		public void RecommendEsalesRequest_IncludeCategoryIds_CorrectFilter()
		{
			var esalesRequest = IoC.Resolve<IRecommendEsalesRequest>();
			esalesRequest.ChannelId = "1";

			esalesRequest.IncludeCategoryIds = new List<int> { 1 };
			esalesRequest.IncludeParentCategoryIds = new List<int> { 1, 2 };
			esalesRequest.IncludeMainCategoryIds = new List<int> { 1, 2, 3 };

			Dictionary<string, string> arguments = esalesRequest.ComposeArguments();

			Assert.AreEqual("available_in_channels:'1' AND (category_id:'1') AND (parent_category_id:'1' OR parent_category_id:'2') AND (main_category_id:'1' OR main_category_id:'2' OR main_category_id:'3')", arguments["filter"]);
		}
	}
}