﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class EsalesRecommendationTypeTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesRecommendationType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IEsalesRecommendationType>();

			Assert.IsInstanceOf<IEsalesRecommendationType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRecommendationType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IEsalesRecommendationType>();
			var instance2 = IoC.Resolve<IEsalesRecommendationType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}