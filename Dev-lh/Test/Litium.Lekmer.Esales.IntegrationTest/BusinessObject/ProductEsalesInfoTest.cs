using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductEsalesInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ProductEsalesInfo_Resolve_Resolved()
		{
			var productEsalesInfo = IoC.Resolve<IEsalesProductInfo>();

			Assert.IsInstanceOf<IEsalesProductInfo>(productEsalesInfo);
		}

		[Test]
		[Category("IoC")]
		public void ProductEsalesInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IEsalesProductInfo>();
			var esalesInfo2 = IoC.Resolve<IEsalesProductInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}