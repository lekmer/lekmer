﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockEsalesTopSellersTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersV2_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockEsalesTopSellersV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersV2>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersV2_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockEsalesTopSellersV2>();
			var block2 = IoC.Resolve<IBlockEsalesTopSellersV2>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}