﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockEsalesAdsRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockEsalesAdsRepository>();

			Assert.IsInstanceOf<BlockEsalesAdsRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockEsalesAdsRepository>();
			var repository2 = IoC.Resolve<BlockEsalesAdsRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}