﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class AdChannelRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void AdChannelRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<AdChannelRepository>();

			Assert.IsInstanceOf<AdChannelRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void AdChannelRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<AdChannelRepository>();
			var instance2 = IoC.Resolve<AdChannelRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}