using System;
using System.Globalization;
using Litium.Lekmer.Common.Job;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Common.UnitTest.Job
{
	[TestFixture]
	public class IntervalCalculatorTest
	{
		private MockRepository _mocker;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void FirstRepeatExecution_StartTimeBeforeNow_CorrectInterval()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			scheduleSetting.Expect(s => s.RepeatExecutionStartHour).Return(5);
			scheduleSetting.Expect(s => s.RepeatExecutionStartMinute).Return(45);

			scheduleSetting.Expect(s => s.RepeatExecutionIntervalHours).Return(0);
			scheduleSetting.Expect(s => s.RepeatExecutionIntervalMinutes).Return(30);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture);

			TimeSpan interval = calculator.FirstRepeatExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(20, interval.TotalMinutes);
		}

		[Test]
		public void FirstRepeatExecution_StartTimeAfterNow_IntervalToStartTime()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			scheduleSetting.Expect(s => s.RepeatExecutionStartHour).Return(16);
			scheduleSetting.Expect(s => s.RepeatExecutionStartMinute).Return(45);

			scheduleSetting.Expect(s => s.RepeatExecutionIntervalHours).Return(0);
			scheduleSetting.Expect(s => s.RepeatExecutionIntervalMinutes).Return(30);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture);

			TimeSpan interval = calculator.FirstRepeatExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(50, interval.TotalMinutes);
		}

		[Test]
		public void NextRepeatExecution_IntervalSpecified_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			scheduleSetting.Expect(s => s.RepeatExecutionIntervalHours).Return(0);
			scheduleSetting.Expect(s => s.RepeatExecutionIntervalMinutes).Return(30);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture);

			TimeSpan interval = calculator.NextRepeatExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(30, interval.TotalMinutes);
		}

		[Test]
		public void NextRepeatExecution_NegativeIntervalSpecified_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			scheduleSetting.Expect(s => s.RepeatExecutionIntervalHours).Return(0);
			scheduleSetting.Expect(s => s.RepeatExecutionIntervalMinutes).Return(-30);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture);

			TimeSpan interval = calculator.NextRepeatExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(30, interval.TotalMinutes);
		}

		[Test]
		public void NextRepeatExecution_ZeroIntervalSpecified_MinuteIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			scheduleSetting.Expect(s => s.RepeatExecutionIntervalHours).Return(0);
			scheduleSetting.Expect(s => s.RepeatExecutionIntervalMinutes).Return(0);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture);

			TimeSpan interval = calculator.NextRepeatExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(1, interval.TotalMinutes);
		}

		[Test]
		public void NextWeeklyExecution_AllWeekDaysSpecifiedNotToday_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			SetupScheduleSetting(12, 30, true, true, true, true, true, true, true, scheduleSetting);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture); //Wednesday

			TimeSpan interval = calculator.NextWeeklyExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(20 * 60 + 35, interval.TotalMinutes);
		}

		[Test]
		public void NextWeeklyExecution_AllWeekDaysSpecifiedToday_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			SetupScheduleSetting(12, 30, true, true, true, true, true, true, true, scheduleSetting);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 12:25", CultureInfo.InvariantCulture); //Wednesday

			TimeSpan interval = calculator.NextWeeklyExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(5, interval.TotalMinutes);
		}

		[Test]
		public void NextWeeklyExecution_NoWeekDaysSpecifiedToday_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			SetupScheduleSetting(12, 30, false, false, false, false, false, false, false, scheduleSetting);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 12:25", CultureInfo.InvariantCulture); //Wednesday

			TimeSpan interval = calculator.NextWeeklyExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(5, interval.TotalMinutes);
		}

		[Test]
		public void NextWeeklyExecution_NoWeekDaysSpecifiedNotToday_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			SetupScheduleSetting(12, 30, false, false, false, false, false, false, false, scheduleSetting);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture); //Wednesday

			TimeSpan interval = calculator.NextWeeklyExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(20 * 60 + 35, interval.TotalMinutes);
		}

		[Test]
		public void NextWeeklyExecution_OneWeekDaysSpecifiedBeforeToday_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			SetupScheduleSetting(12, 30, true, false, false, false, false, false, false, scheduleSetting);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture); //Wednesday

			TimeSpan interval = calculator.NextWeeklyExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(4 * 24 * 60 + 20 * 60 + 35, interval.TotalMinutes);
		}

		[Test]
		public void NextWeeklyExecution_OneWeekDaysSpecifiedAfterToday_CorrectIntervalReturned()
		{
			var scheduleSetting = _mocker.Stub<IScheduleSetting>();

			SetupScheduleSetting(12, 30, false, false, false, false, false, false, true, scheduleSetting);

			_mocker.ReplayAll();

			IIntervalCalculator calculator = new IntervalCalculator();

			DateTime timeFrom = DateTime.Parse("2012-07-18 15:55", CultureInfo.InvariantCulture); //Wednesday

			TimeSpan interval = calculator.NextWeeklyExecution(timeFrom, scheduleSetting);

			Assert.AreEqual(3 * 24 * 60 + 20 * 60 + 35, interval.TotalMinutes);
		}


		private void SetupScheduleSetting(int hour, int minute, bool monday, bool tuesday, bool wednesday, bool thursday, bool friday, bool saturday, bool sunday, IScheduleSetting scheduleSetting)
		{
			scheduleSetting.Expect(s => s.WeeklyExecutionHour).Return(hour);
			scheduleSetting.Expect(s => s.WeeklyExecutionMinute).Return(minute);

			scheduleSetting.Expect(s => s.WeeklyExecutionMonday).Return(monday).Repeat.Any();
			scheduleSetting.Expect(s => s.WeeklyExecutionTuesday).Return(tuesday).Repeat.Any();
			scheduleSetting.Expect(s => s.WeeklyExecutionWednesday).Return(wednesday).Repeat.Any();
			scheduleSetting.Expect(s => s.WeeklyExecutionThursday).Return(thursday).Repeat.Any();
			scheduleSetting.Expect(s => s.WeeklyExecutionFriday).Return(friday).Repeat.Any();
			scheduleSetting.Expect(s => s.WeeklyExecutionSaturday).Return(saturday).Repeat.Any();
			scheduleSetting.Expect(s => s.WeeklyExecutionSunday).Return(sunday).Repeat.Any();
		}
	}
}