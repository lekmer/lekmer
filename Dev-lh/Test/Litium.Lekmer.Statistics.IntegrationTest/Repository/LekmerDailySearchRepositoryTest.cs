﻿using Litium.Lekmer.Statistics.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Statistics;
using Litium.Scensum.Statistics.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerDailySearchRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerDailySearchRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<DailySearchRepository>();

			Assert.IsInstanceOf<DailySearchRepository>(repository);
			Assert.IsInstanceOf<LekmerDailySearchRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDailySearchRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<DailySearchRepository>();
			var repository2 = IoC.Resolve<DailySearchRepository>();

			Assert.AreEqual(repository1, repository2);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDailySearchRepository_Save_NotSaved()
		{
			var query = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
			query += "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
			query += "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
			query += "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567END";
			query += "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
			//query = "Тест методу 1";

			var repository = (LekmerDailySearchRepository) IoC.Resolve<DailySearchRepository>();

			var dailySearch = IoC.Resolve<IDailySearch>();
			dailySearch.ChannelId = 1;
			dailySearch.HitCount = 10;
			dailySearch.SearchCount = 10;
			dailySearch.Month = 10;
			dailySearch.Day = 10;
			dailySearch.Year = 2013;
			dailySearch.Query = query;

			repository.Save(dailySearch);
		}
	}
}