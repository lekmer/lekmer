﻿using Litium.Framework.Cache.UpdateFeed;
using Litium.Lekmer.Cache.UpdateFeed;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Cache.IntegrationTest.UpdateFeed
{
	[TestFixture]
	public class LekmerCacheUpdateFeedTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCacheUpdateFeed_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICacheUpdateFeed>();

			Assert.IsInstanceOf<ICacheUpdateFeed>(service);
			Assert.IsInstanceOf<LekmerCacheUpdateFeed>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCacheUpdateFeed_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICacheUpdateFeed>();
			var service2 = IoC.Resolve<ICacheUpdateFeed>();

			Assert.AreEqual(service1, service2);
		}
	}
}