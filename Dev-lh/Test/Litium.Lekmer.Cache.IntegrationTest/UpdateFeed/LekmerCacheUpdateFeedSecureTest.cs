﻿using Litium.Lekmer.Cache.UpdateFeed;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Cache.IntegrationTest.UpdateFeed
{
	[TestFixture]
	public class LekmerCacheUpdateFeedSecureTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCacheUpdateFeedSecure_Resolve_Resolved()
		{
			var service = IoC.Resolve<ILekmerCacheUpdateFeedSecure>();

			Assert.IsInstanceOf<ILekmerCacheUpdateFeedSecure>(service);
			Assert.IsInstanceOf<LekmerCacheUpdateFeedSecure>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCacheUpdateFeedSecure_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ILekmerCacheUpdateFeedSecure>();
			var service2 = IoC.Resolve<ILekmerCacheUpdateFeedSecure>();

			Assert.AreEqual(service1, service2);
		}
	}
}