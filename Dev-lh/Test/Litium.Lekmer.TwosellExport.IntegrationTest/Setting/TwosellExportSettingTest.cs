﻿using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.TwosellExport.IntegrationTest.Setting
{
	[TestFixture]
	public class TwosellExportSettingTest
	{
		[Test]
		[Category("IoC")]
		public void TwosellExportSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<ITwosellExportSetting>();
			Assert.IsInstanceOf<ITwosellExportSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void TwosellExportSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<ITwosellExportSetting>();
			var setting2 = IoC.Resolve<ITwosellExportSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}