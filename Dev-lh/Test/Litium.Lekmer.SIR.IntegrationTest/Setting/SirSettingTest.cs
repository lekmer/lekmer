﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.Setting
{
	[TestFixture]
	public class SirSettingTest
	{
		[Test]
		[Category("IoC")]
		public void SirSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<ISirSetting>();
			Assert.IsInstanceOf<ISirSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void SirSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<ISirSetting>();
			var setting2 = IoC.Resolve<ISirSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}