﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Service
{
	[TestFixture]
	public class KlarnaPendingOrderServiceTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrderService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaPendingOrderService>();

			Assert.IsInstanceOf<IKlarnaPendingOrderService>(service);
			Assert.IsInstanceOf<KlarnaPendingOrderService>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrderService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IKlarnaPendingOrderService>();
			var service2 = IoC.Resolve<IKlarnaPendingOrderService>();

			Assert.AreEqual(service1, service2);
		}
	}
}