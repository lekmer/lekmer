﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class KlarnaPendingOrderTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IKlarnaPendingOrder>();

			Assert.IsInstanceOf<IKlarnaPendingOrder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IKlarnaPendingOrder>();
			var instance2 = IoC.Resolve<IKlarnaPendingOrder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}