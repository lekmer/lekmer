﻿using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.API
{
	[TestFixture]
	public class KlarnaClientTestInAction
	{
		private MockRepository _mocker;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void GetAddress_ValidCivicNumber_AddressReturned()
		{
			//Test persons: https://integration.klarna.com/en/testing/test-persons

			IChannel channel = CreateChannel();
			IKlarnaClient klarnaClient = CreateKlarnaClient();

			_mocker.ReplayAll();

			klarnaClient.Initialize(channel.CommonName);

			IKlarnaGetAddressResult klarnaGetAddressResult = klarnaClient.GetAddress(channel, "410321-9202", "192.0.2.9");

			_mocker.VerifyAll();

			Assert.IsNotNull(klarnaGetAddressResult);

			Assert.AreEqual((int)KlarnaResponseCode.Ok, klarnaGetAddressResult.ResponseCode);
			Assert.AreEqual(0, klarnaGetAddressResult.FaultCode);

			Assert.IsNotNull(klarnaGetAddressResult.KlarnaAddresses);
			Assert.IsTrue(klarnaGetAddressResult.KlarnaAddresses.Count > 0);

			Assert.AreEqual("Testperson-se", klarnaGetAddressResult.KlarnaAddresses[0].FirstName);
			Assert.AreEqual("Approved", klarnaGetAddressResult.KlarnaAddresses[0].LastName);
			Assert.AreEqual("Stårgatan 1", klarnaGetAddressResult.KlarnaAddresses[0].StreetAddress);
			Assert.AreEqual("12345", klarnaGetAddressResult.KlarnaAddresses[0].PostalCode);
			Assert.AreEqual("Ankeborg", klarnaGetAddressResult.KlarnaAddresses[0].City);
		}

		[Test]
		public void GetAddress_InvalidCivicNumber_ErrorCodeReturned()
		{
			//Test persons: https://integration.klarna.com/en/testing/test-persons

			IChannel channel = CreateChannel();
			IKlarnaClient klarnaClient = CreateKlarnaClient();

			_mocker.ReplayAll();

			klarnaClient.Initialize(channel.CommonName);

			IKlarnaGetAddressResult klarnaGetAddressResult = klarnaClient.GetAddress(channel, "123456-9999", "192.0.2.9");

			_mocker.VerifyAll();

			Assert.IsNotNull(klarnaGetAddressResult);

			Assert.AreEqual((int)KlarnaResponseCode.SpecifiedError, klarnaGetAddressResult.ResponseCode);
			Assert.AreEqual(2201, klarnaGetAddressResult.FaultCode);

			Assert.IsTrue(klarnaGetAddressResult.KlarnaAddresses.Count == 0);
		}

		[Test]
		public void GetAddress_ValidCompanyNumber_AddressReturned()
		{
			//Test persons: https://integration.klarna.com/en/testing/test-persons

			IChannel channel = CreateChannel();
			IKlarnaClient klarnaClient = CreateKlarnaClient();

			_mocker.ReplayAll();

			klarnaClient.Initialize(channel.CommonName);

			IKlarnaGetAddressResult klarnaGetAddressResult = klarnaClient.GetAddress(channel, "002031-0132", "192.0.2.9");

			_mocker.VerifyAll();

			Assert.IsNotNull(klarnaGetAddressResult);

			Assert.AreEqual((int)KlarnaResponseCode.Ok, klarnaGetAddressResult.ResponseCode);
			Assert.AreEqual(0, klarnaGetAddressResult.FaultCode);

			Assert.IsNotNull(klarnaGetAddressResult.KlarnaAddresses);
			Assert.IsTrue(klarnaGetAddressResult.KlarnaAddresses.Count > 0);

			Assert.AreEqual(true, klarnaGetAddressResult.KlarnaAddresses[0].IsCompany);
			Assert.AreEqual("Testcompany-se", klarnaGetAddressResult.KlarnaAddresses[0].CompanyName);
			Assert.AreEqual(string.Empty, klarnaGetAddressResult.KlarnaAddresses[0].FirstName);
			Assert.AreEqual(string.Empty, klarnaGetAddressResult.KlarnaAddresses[0].LastName);
			Assert.AreEqual("Stårgatan 1", klarnaGetAddressResult.KlarnaAddresses[0].StreetAddress);
			Assert.AreEqual("12345", klarnaGetAddressResult.KlarnaAddresses[0].PostalCode);
			Assert.AreEqual("Ankeborg", klarnaGetAddressResult.KlarnaAddresses[0].City);
		}

		private IKlarnaClient CreateKlarnaClient()
		{
			var klarnaClient = new KlarnaClient(CreateKlarnaApi());
			klarnaClient.KlarnaSetting = CreateKlarnaSetting();
			return klarnaClient;
		}

		private IKlarnaApi CreateKlarnaApi()
		{
			var klarnaApi = new KlarnaApi(CreateKlarnaTransactionService());
			klarnaApi.KlarnaSetting = CreateKlarnaSetting();
			return klarnaApi;
		}

		private IKlarnaSetting CreateKlarnaSetting()
		{
			var klarnaSetting = _mocker.Stub<IKlarnaSetting>();

			klarnaSetting.Expect(ks => ks.Url).Return("https://payment.klarna.com:443").Repeat.Any();
			klarnaSetting.Expect(ks => ks.Timeout).Return(3000).Repeat.Any();
			klarnaSetting.Expect(ks => ks.EID).Return(5857).Repeat.Any();
			klarnaSetting.Expect(ks => ks.SecretKey).Return("YocvXm6JQV48dyf").Repeat.Any();
			klarnaSetting.Expect(ks => ks.Mode).Return(0).Repeat.Any();
			klarnaSetting.Expect(ks => ks.PCStorage).Return(string.Empty).Repeat.Any();
			klarnaSetting.Expect(ks => ks.PCURI).Return(string.Empty).Repeat.Any();

			Expect.Call(() => klarnaSetting.Initialize(null)).IgnoreArguments().Repeat.Any();

			return klarnaSetting;
		}

		private IKlarnaTransactionService CreateKlarnaTransactionService()
		{
			return _mocker.DynamicMock<IKlarnaTransactionService>();
		}

		private IChannel CreateChannel()
		{
			var channel = _mocker.Stub<IChannel>();
			channel.CommonName = "Sweden";

			channel.Country = _mocker.Stub<ICountry>();
			channel.Country.Iso = "SE";

			return channel;
		}
	}
}