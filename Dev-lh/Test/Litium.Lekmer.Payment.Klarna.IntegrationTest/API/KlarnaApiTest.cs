﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.API
{
	[TestFixture]
	public class KlarnaApiTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaApi_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaApi>();

			Assert.IsInstanceOf<IKlarnaApi>(service);
			Assert.IsInstanceOf<KlarnaApi>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaApi_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IKlarnaApi>();
			var service2 = IoC.Resolve<IKlarnaApi>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}