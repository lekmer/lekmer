﻿using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Setting
{
	[TestFixture]
	public class TimeOutDataTest
	{
		[Test]
		[Category("IoC")]
		public void TimeOutData_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITimeOutData>();

			Assert.IsInstanceOf<ITimeOutData>(service);
			Assert.IsInstanceOf<ITimeOutData>(service);
		}

		[Test]
		[Category("IoC")]
		public void TimeOutData_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<ITimeOutData>();
			var service2 = IoC.Resolve<ITimeOutData>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}