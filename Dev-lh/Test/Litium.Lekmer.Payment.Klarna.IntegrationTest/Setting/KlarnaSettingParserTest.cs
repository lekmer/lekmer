﻿using System.Linq;
using Litium.Lekmer.Payment.Klarna.Setting;

using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Setting
{
	[TestFixture]
	public class KlarnaSettingParserTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaSettingParser_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaSettingParser>();

			Assert.IsInstanceOf<IKlarnaSettingParser>(service);
			Assert.IsInstanceOf<IKlarnaSettingParser>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaSettingParser_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IKlarnaSettingParser>();
			var service2 = IoC.Resolve<IKlarnaSettingParser>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void ParseTimeoutSteps_SettingsValueWithoutTimeOut_ParsedCorrectly()
		{
			IKlarnaSettingParser klarnaSettingParser = new KlarnaSettingParser();

			var timeoutSteps = klarnaSettingParser.ParseTimeoutSteps("").ToArray();

			Assert.IsNotNull(timeoutSteps);
			Assert.AreEqual(0, timeoutSteps.Length);
		}

		[Test]
		public void ParseTimeoutSteps_SettingsValueWithOneTimeOut_ParsedCorrectly()
		{
			IKlarnaSettingParser klarnaSettingParser = new KlarnaSettingParser();

			var timeoutSteps = klarnaSettingParser.ParseTimeoutSteps("3000:7000").ToArray();

			Assert.IsNotNull(timeoutSteps);
			Assert.AreEqual(1, timeoutSteps.Length);
			Assert.AreEqual(3000, timeoutSteps[0].OrderValue);
			Assert.AreEqual(7000, timeoutSteps[0].TimeOutValue);
		}

		[Test]
		public void ParseTimeoutSteps_SettingsValueWithFewTimeOuts_ParsedCorrectly()
		{
			IKlarnaSettingParser klarnaSettingParser = new KlarnaSettingParser();

			var timeoutSteps = klarnaSettingParser.ParseTimeoutSteps("1000:3000;3000:7000;5000:10000").ToArray();

			Assert.IsNotNull(timeoutSteps);
			Assert.AreEqual(3, timeoutSteps.Length);

			Assert.AreEqual(1000, timeoutSteps[0].OrderValue);
			Assert.AreEqual(3000, timeoutSteps[0].TimeOutValue);

			Assert.AreEqual(3000, timeoutSteps[1].OrderValue);
			Assert.AreEqual(7000, timeoutSteps[1].TimeOutValue);

			Assert.AreEqual(5000, timeoutSteps[2].OrderValue);
			Assert.AreEqual(10000, timeoutSteps[2].TimeOutValue);
		}

		[Test]
		public void ParseTimeoutSteps_SettingsValueWithWrongFormat_ParsedCorrectly()
		{
			IKlarnaSettingParser klarnaSettingParser = new KlarnaSettingParser();

			var timeoutSteps = klarnaSettingParser.ParseTimeoutSteps(";;1000:3000;;3000:7000;;").ToArray();

			Assert.IsNotNull(timeoutSteps);
			Assert.AreEqual(2, timeoutSteps.Length);

			Assert.AreEqual(1000, timeoutSteps[0].OrderValue);
			Assert.AreEqual(3000, timeoutSteps[0].TimeOutValue);

			Assert.AreEqual(3000, timeoutSteps[1].OrderValue);
			Assert.AreEqual(7000, timeoutSteps[1].TimeOutValue);
		}
	}
}