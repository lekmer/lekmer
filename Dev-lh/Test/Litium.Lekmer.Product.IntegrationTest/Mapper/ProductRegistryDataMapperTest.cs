﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class ProductRegistryDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistry_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IProductRegistry>(_dataReader);

			Assert.IsInstanceOf<ProductRegistryDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistry_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IProductRegistry>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IProductRegistry>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}