﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class SizeTableRowDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRowDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ISizeTableRow>(_dataReader);

			Assert.IsInstanceOf<SizeTableRowDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRowDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ISizeTableRow>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ISizeTableRow>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}