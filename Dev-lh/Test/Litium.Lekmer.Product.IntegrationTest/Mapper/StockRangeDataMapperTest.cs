﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class StockRangeDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void StockRange_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IStockRange>(_dataReader);
			Assert.IsInstanceOf<StockRangeDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void StockRange_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IStockRange>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IStockRange>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}