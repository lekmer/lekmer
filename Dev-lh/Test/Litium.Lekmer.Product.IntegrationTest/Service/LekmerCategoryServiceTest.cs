﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCategoryServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";
		private static readonly int CATEGORY_ID = 1000469; // C_10-118-1081

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;
		private ICurrency _currency;

		private ILekmerCategoryService _lekmerCategoryService;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_currency = _mocker.Stub<ICurrency>();
			_currency.Id = CURRENCY_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;
			_channel.Currency = _currency;

			_userContext.Channel = _channel;

			_lekmerCategoryService = (ILekmerCategoryService)IoC.Resolve<ICategoryService>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerCategoryService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICategoryService>();

			Assert.IsInstanceOf<ICategoryService>(service);
			Assert.IsInstanceOf<ILekmerCategoryService>(service);
			Assert.IsInstanceOf<CategoryService>(service);
			Assert.IsInstanceOf<LekmerCategoryService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCategoryService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICategoryService>();
			var service2 = IoC.Resolve<ICategoryService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetViewByIdAll_ExecuteWithId_OneElementReturned()
		{
			Collection<ICategoryView> categoryViews = _lekmerCategoryService.GetViewByIdAll(_userContext, CATEGORY_ID);

			Assert.AreEqual(1, categoryViews.Count, "Count");
		}
	}
}