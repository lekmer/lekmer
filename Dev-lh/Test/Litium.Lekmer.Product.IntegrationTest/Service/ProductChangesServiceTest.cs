﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductChangesServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductChangesService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductChangesService>();

			Assert.IsInstanceOf<IProductChangesService>(service);
			Assert.IsInstanceOf<ProductChangesService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductChangesService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductChangesService>();
			var service2 = IoC.Resolve<IProductChangesService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Save")]
		public void Insert_CreateNewItemAndSave_ItemSaved()
		{
			var service = IoC.Resolve<IProductChangesService>();
			var productChangeEvent = service.Create(123456, "Testing");

			using (var transactionScope = new TransactionScope())
			{
				productChangeEvent.Id = service.Insert(productChangeEvent);
				transactionScope.Dispose();
			}

			Assert.IsTrue(productChangeEvent.Id > 0);
		}

		[Test]
		[Category("Database.Save")]
		public void SetStatus_Execute_ItemsUpdated()
		{
			var service = IoC.Resolve<IProductChangesService>();

			using (var transactionScope = new TransactionScope())
			{
				var productChangeEvents = new Collection<IProductChangeEvent>();
				for (int i = 0; i < 10; i++)
				{
					IProductChangeEvent productChangeEvent = service.Create(1000 + i, "Testing");
					productChangeEvents.Add(productChangeEvent);
					service.Insert(productChangeEvent);
				}

				service.SetStatus(productChangeEvents, ProductChangeEventStatus.InProgress);
				Collection<IProductChangeEvent> eventsInProgress = service.GetAll(ProductChangeEventStatus.InProgress);
				foreach (IProductChangeEvent productChangeEvent in productChangeEvents)
				{
					IProductChangeEvent eventInProgress = eventsInProgress.FirstOrDefault(e => e.Id == productChangeEvent.Id);
					Assert.NotNull(eventInProgress);
					Assert.AreEqual(eventInProgress.EventStatusId, (int)ProductChangeEventStatus.InProgress);
				}

				service.SetStatus(productChangeEvents, ProductChangeEventStatus.ActionApplied);
				Collection<IProductChangeEvent> eventsActionApplied = service.GetAll(ProductChangeEventStatus.ActionApplied);
				foreach (IProductChangeEvent productChangeEvent in productChangeEvents)
				{
					IProductChangeEvent eventActionApplied = eventsActionApplied.FirstOrDefault(e => e.Id == productChangeEvent.Id);
					Assert.NotNull(eventActionApplied);
					Assert.AreEqual(eventActionApplied.EventStatusId, (int)ProductChangeEventStatus.ActionApplied);
				}

				Collection<IProductChangeEvent> eventsInQueue = service.GetAll(ProductChangeEventStatus.InQueue);
				foreach (IProductChangeEvent productChangeEvent in productChangeEvents)
				{
					IProductChangeEvent eventInQueue = eventsInQueue.FirstOrDefault(e => e.Id == productChangeEvent.Id);
					Assert.Null(eventInQueue);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		[Category("Database.Delete")]
		public void DeleteExpiredItems_Execute_Executed()
		{
			var service = IoC.Resolve<IProductChangesService>();

			using (var transactionScope = new TransactionScope())
			{
				service.DeleteExpiredItems();

				transactionScope.Dispose();
			}
		}
	}
}