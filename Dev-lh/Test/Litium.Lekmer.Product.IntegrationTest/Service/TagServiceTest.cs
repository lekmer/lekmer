﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class TagServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void TagService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITagService>();

			Assert.IsInstanceOf<ITagService>(service);
			Assert.IsInstanceOf<TagService>(service);
		}

		[Test]
		[Category("IoC")]
		public void TagService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ITagService>();
			var service2 = IoC.Resolve<ITagService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void GetAll_ExecuteGetAllByContext_ReturnedList()
		{
			var service = IoC.Resolve<ITagService>();
			Collection<ITag> tags = service.GetAll(_userContext);

			Assert.IsNotNull(tags, "Tags collection is null");
			Assert.IsTrue(tags.Count > 0, "Tags collection is empty");
		}

		[Test]
		public void GetAllIdsByCategory_Execute_ReturnedList()
		{
			var service = IoC.Resolve<ITagService>();

			ICategoryTree tree = IoC.Resolve<ICategoryService>().GetAllAsTree(_userContext);

			var categories = new Collection<ICategory>();

			FindEndLevelCategories(tree.Root, categories);

			IdCollection tagIds = null;

			foreach (ICategory category in categories)
			{
				tagIds = service.GetAllIdsByCategory(category.Id);

				if (tagIds.Count > 0)
				{
					break;
				}
			}

			Assert.IsNotNull(tagIds, "TagIds collection is null");
			Assert.IsTrue(tagIds.Count > 0, "TagIds collection is empty");
		}


		protected void FindEndLevelCategories(ICategoryTreeItem treeItem, Collection<ICategory> categories)
		{
			if (treeItem.Children == null || treeItem.Children.Count == 0)
			{
				categories.Add(treeItem.Category);
				return;
			}

			foreach (ICategoryTreeItem child in treeItem.Children)
			{
				FindEndLevelCategories(child, categories);
			}
		}
	}
}