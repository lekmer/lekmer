﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ColorServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ColorService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IColorService>();

			Assert.IsInstanceOf<IColorService>(service);
			Assert.IsInstanceOf<ColorService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ColorService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IColorService>();
			var service2 = IoC.Resolve<IColorService>();

			Assert.AreEqual(service1, service2);
		}
	}
}