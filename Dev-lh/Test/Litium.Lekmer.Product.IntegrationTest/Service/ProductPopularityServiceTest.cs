﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductPopularityServiceTest
	{
		private static readonly int CHANNEL_ID = 1;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void ProductPopularityService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductPopularityService>();

			Assert.IsInstanceOf<IProductPopularityService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductPopularityService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductPopularityService>();
			var service2 = IoC.Resolve<IProductPopularityService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllByChannel_ReturnedList()
		{
			var productPopularityService = IoC.Resolve<IProductPopularityService>();

			Collection<IProductPopularity> productPopularities = productPopularityService.GetAll(_userContext);

			Assert.IsNotNull(productPopularities);
		}
	}
}