﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductImageGroupServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductImageGroupService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductImageGroupService>();

			Assert.IsInstanceOf<IProductImageGroupService>(service);
			Assert.IsInstanceOf<ProductImageGroupService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductImageGroupService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductImageGroupService>();
			var service2 = IoC.Resolve<IProductImageGroupService>();

			Assert.AreEqual(service1, service2);
		}
	}
}