﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class PackageServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PackageService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPackageService>();

			Assert.IsInstanceOf<IPackageService>(service);
			Assert.IsInstanceOf<PackageService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PackageService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IPackageService>();
			var service2 = IoC.Resolve<IPackageService>();

			Assert.AreEqual(service1, service2);
		}
	}
}