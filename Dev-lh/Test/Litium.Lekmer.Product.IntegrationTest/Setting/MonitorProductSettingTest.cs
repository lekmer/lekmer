﻿using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Setting
{
	[TestFixture]
	public class MonitorProductSettingTest
	{
		[Test]
		[Category("IoC")]
		public void MonitorProductSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMonitorProductSetting>();

			Assert.IsInstanceOf<IMonitorProductSetting>(service);
			Assert.IsInstanceOf<MonitorProductSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void MonitorProductSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<IMonitorProductSetting>();
			var setting2 = IoC.Resolve<IMonitorProductSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}