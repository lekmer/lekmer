﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service.Search
{
	[TestFixture]
	public class EsalesProductTicketServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductTicketService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesProductTicketService>();

			Assert.IsInstanceOf<IEsalesProductTicketService>(service);
			Assert.IsInstanceOf<EsalesProductTicketService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductTicketService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesProductTicketService>();
			var service2 = IoC.Resolve<IEsalesProductTicketService>();

			Assert.AreEqual(service1, service2);
		}
	}
}