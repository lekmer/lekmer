﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductPopularityTest
	{
		[Test]
		[Category("IoC")]
		public void ProductPopularity_Resolve_Resolved()
		{
			var productPopularity = IoC.Resolve<IProductPopularity>();

			Assert.IsInstanceOf<IProductPopularity>(productPopularity);
		}

		[Test]
		[Category("IoC")]
		public void ProductPopularity_ResolveTwice_DifferentObjects()
		{
			var productPopularity1 = IoC.Resolve<IProductPopularity>();
			var productPopularity2 = IoC.Resolve<IProductPopularity>();

			Assert.AreNotEqual(productPopularity1, productPopularity2);
		}
	}
}