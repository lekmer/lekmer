﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableFolderTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableFolder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableFolder>();

			Assert.IsInstanceOf<ISizeTableFolder>(instance);
			Assert.IsInstanceOf<ISizeTableFolder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableFolder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableFolder>();
			var instance2 = IoC.Resolve<ISizeTableFolder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}