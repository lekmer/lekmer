﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTable_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTable>();
			Assert.IsInstanceOf<ISizeTable>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTable_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTable>();
			var instance2 = IoC.Resolve<ISizeTable>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}