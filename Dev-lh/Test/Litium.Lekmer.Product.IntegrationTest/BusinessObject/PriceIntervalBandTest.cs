﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PriceIntervalBandTest
	{
		[Test]
		[Category("IoC")]
		public void PriceIntervalBand_Resolve_Resolved()
		{
			var PriceIntervalBand = IoC.Resolve<IPriceIntervalBand>();

			Assert.IsInstanceOf<IPriceIntervalBand>(PriceIntervalBand);
		}

		[Test]
		[Category("IoC")]
		public void PriceIntervalBand_ResolveTwice_DifferentObjects()
		{
			var PriceIntervalBand1 = IoC.Resolve<IPriceIntervalBand>();
			var PriceIntervalBand2 = IoC.Resolve<IPriceIntervalBand>();

			Assert.AreNotEqual(PriceIntervalBand1, PriceIntervalBand2);
		}
	}
}