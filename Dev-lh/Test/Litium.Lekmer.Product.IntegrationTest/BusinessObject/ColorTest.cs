﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ColorTest
	{
		[Test]
		[Category("IoC")]
		public void Color_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IColor>();

			Assert.IsInstanceOf<IColor>(instance);
		}

		[Test]
		[Category("IoC")]
		public void Color_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IColor>();
			var instance2 = IoC.Resolve<IColor>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}