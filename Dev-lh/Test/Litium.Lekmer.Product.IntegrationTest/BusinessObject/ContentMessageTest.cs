﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ContentMessageTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessage_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IContentMessage>();

			Assert.IsInstanceOf<IContentMessage>(instance);
			Assert.IsInstanceOf<IContentMessage>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessage_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IContentMessage>();
			var instance2 = IoC.Resolve<IContentMessage>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}