﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductSizeRelationTest
	{
		[Test]
		[Category("IoC")]
		public void ProductSizeRelation_Resolve_Resolved()
		{
			var productSizeRelation = IoC.Resolve<IProductSizeRelation>();

			Assert.IsInstanceOf<IProductSizeRelation>(productSizeRelation);
		}

		[Test]
		[Category("IoC")]
		public void ProductSizeRelation_ResolveTwice_DifferentObjects()
		{
			var productSizeRelation1 = IoC.Resolve<IProductSizeRelation>();
			var productSizeRelation2 = IoC.Resolve<IProductSizeRelation>();

			Assert.AreNotEqual(productSizeRelation1, productSizeRelation2);
		}
	}
}