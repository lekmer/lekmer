﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ImportProductInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ImportProductInfo_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IImportProductInfo>();

			Assert.IsInstanceOf<IImportProductInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ImportProductInfo_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IImportProductInfo>();
			var instance2 = IoC.Resolve<IImportProductInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}