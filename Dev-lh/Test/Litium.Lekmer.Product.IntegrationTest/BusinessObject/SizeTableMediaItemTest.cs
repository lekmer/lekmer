﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableMediaItemTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaItem_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableMediaItem>();

			Assert.IsInstanceOf<ISizeTableMediaItem>(instance);
			Assert.IsInstanceOf<ISizeTableMediaItem>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaItem_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableMediaItem>();
			var instance2 = IoC.Resolve<ISizeTableMediaItem>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}