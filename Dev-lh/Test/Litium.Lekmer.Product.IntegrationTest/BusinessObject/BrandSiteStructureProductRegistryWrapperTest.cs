﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BrandSiteStructureProductRegistryWrapperTest
	{
		[Test]
		[Category("IoC")]
		public void BrandSiteStructureProductRegistryWrapper_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBrandSiteStructureProductRegistryWrapper>();

			Assert.IsInstanceOf<IBrandSiteStructureProductRegistryWrapper>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BrandSiteStructureProductRegistryWrapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBrandSiteStructureProductRegistryWrapper>();
			var instance2 = IoC.Resolve<IBrandSiteStructureProductRegistryWrapper>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}