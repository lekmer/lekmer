﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PriceIntervalTest
	{
		[Test]
		[Category("IoC")]
		public void PriceInterval_Resolve_Resolved()
		{
			var PriceInterval = IoC.Resolve<IPriceInterval>();

			Assert.IsInstanceOf<IPriceInterval>(PriceInterval);
		}

		[Test]
		[Category("IoC")]
		public void PriceInterval_ResolveTwice_DifferentObjects()
		{
			var PriceInterval1 = IoC.Resolve<IPriceInterval>();
			var PriceInterval2 = IoC.Resolve<IPriceInterval>();

			Assert.AreNotEqual(PriceInterval1, PriceInterval2);
		}
	}
}