﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Transactions;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product.IntegrationTest.Helper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductUrlHistorySecureServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";
		private static readonly string TEST_URL_TITLE_1 = "Test URL Title1_";
		private static readonly string TEST_URL_TITLE_2 = "Test URL Title2_";
		private static readonly string TEST_URL_TITLE_3 = "Test URL Title3_";
		private static readonly string TEST_URL_TITLE_4 = "Test URL Title4_";

		private MockRepository _mocker;
		private IUserContext _userContext;
		private ISystemUserFull _systemUserFull;
		private IPrivilege _privilege;
		private IChannel _channel;
		private ILanguage _language;

		private IProductUrlHistorySecureService _productUrlHistorySecureService;
		private IProductUrlHistoryService _productUrlHistoryService;

		private ProductUrlHistoryHelper _productUrlHistoryHelper;
		private ProductCollection _productCollection;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_privilege = _mocker.Stub<IPrivilege>();
			_privilege.CommonName = PrivilegeConstant.AssortmentProduct;

			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_systemUserFull.Privileges = new Collection<IPrivilege> { _privilege };

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			_productUrlHistorySecureService = IoC.Resolve<IProductUrlHistorySecureService>();
			_productUrlHistoryService = IoC.Resolve<IProductUrlHistoryService>();

			_productUrlHistoryHelper = new ProductUrlHistoryHelper();
			_productCollection = _productUrlHistoryHelper.GetProducts();
		}

		[Test]
		[Category("IoC")]
		public void ProductUrlHistorySecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IProductUrlHistorySecureService>();

			Assert.IsInstanceOf<IProductUrlHistorySecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void ProductUrlHistorySecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IProductUrlHistorySecureService>();
			var secureService2 = IoC.Resolve<IProductUrlHistorySecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}

		[Test]
		public void ProductUrlHistoryService_GetAllByProduct_ReturnedList()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				int productId = _productCollection[0].Id;

				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						productId,
						LANGUAGE_ID,
						TEST_URL_TITLE_1 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Get ProductUrlHistory by Product.
				IEnumerable<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistorySecureService.GetAllByProduct(productId)
					.Where(h => h.LanguageId == LANGUAGE_ID);

				for (int i = 0; i <= 10; i++)
				{
					IProductUrlHistory productUrlHistory = productUrlHistoryCollection.FirstOrDefault(h => h.UrlTitleOld == TEST_URL_TITLE_1 + i);

					Assert.AreEqual(productId, productUrlHistory.ProductId);
					Assert.AreEqual(LANGUAGE_ID, productUrlHistory.LanguageId);
					Assert.AreEqual((int)HistoryLifeIntervalType.Immortal, productUrlHistory.TypeId);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_DeleteById_Deleted()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						_productCollection[i].Id,
						LANGUAGE_ID,
						TEST_URL_TITLE_2 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				List<int> productUrlHistoryIds = _productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				foreach (int productUrlHistoryId in productUrlHistoryIds)
				{
					_productUrlHistorySecureService.DeleteById(_systemUserFull, productUrlHistoryId);
				}

				// Get ProductUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_2 + i);

					Assert.AreEqual(0, productUrlHistoryCollection.Count);
				}

				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_DeleteAllByProduct_Deleted()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				int productId = _productCollection[0].Id;

				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						productId,
						LANGUAGE_ID,
						TEST_URL_TITLE_3 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Delete ProductUrlHistory collection by Product.
				_productUrlHistorySecureService.DeleteAllByProduct(_systemUserFull, productId);

				// Get ProductUrlHistory by Product.
				Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistorySecureService.GetAllByProduct(productId);
				Assert.AreEqual(0, productUrlHistoryCollection.Count);
				
				transactionScope.Dispose();
			}
		}

		[Test]
		public void ProductUrlHistoryService_DeleteAllByProductAndUrlTitle_Deleted()
		{
			using (TransactionScope transactionScope = new TransactionScope())
			{
				Collection<IProductUrlHistory> productUrlHistoryCollectionToSave = new Collection<IProductUrlHistory>();

				// Populate collection of ProductUrlHistory objects.
				for (int i = 0; i <= 10; i++)
				{
					productUrlHistoryCollectionToSave.Add(_productUrlHistorySecureService.Create(
						_productCollection[i].Id,
						LANGUAGE_ID,
						TEST_URL_TITLE_4 + i,
						(int)HistoryLifeIntervalType.Immortal));
				}

				// Save ProductUrlHistory collection.
				_productUrlHistorySecureService.Save(_systemUserFull, productUrlHistoryCollectionToSave);

				// Delete ProductUrlHistory collection.
				for (int i = 0; i <= 10; i++)
				{
					_productUrlHistorySecureService.DeleteAllByProductAndUrlTitle(_systemUserFull, _productCollection[i].Id, LANGUAGE_ID, TEST_URL_TITLE_4 + i);
				}

				// Get ProductUrlHistory by URL.
				for (int i = 0; i <= 10; i++)
				{
					Collection<IProductUrlHistory> productUrlHistoryCollection = _productUrlHistoryService.GetAllByUrlTitle(_userContext, TEST_URL_TITLE_4 + i);

					Assert.AreEqual(0, productUrlHistoryCollection.Count);
				}

				transactionScope.Dispose();
			}
		}
	}
}