﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class SizeTableMediaSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISizeTableMediaSecureService>();

			Assert.IsInstanceOf<ISizeTableMediaSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableMediaSecureService>();
			var instance2 = IoC.Resolve<ISizeTableMediaSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}