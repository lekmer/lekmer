﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductRegistrySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistrySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductRegistrySecureService>();

			Assert.IsInstanceOf<IProductRegistrySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistrySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistrySecureService>();
			var instance2 = IoC.Resolve<IProductRegistrySecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}