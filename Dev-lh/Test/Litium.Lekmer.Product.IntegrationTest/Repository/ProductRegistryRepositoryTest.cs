﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductRegistryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductRegistryRepository>();

			Assert.IsInstanceOf<ProductRegistryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductRegistryRepository>();
			var repository2 = IoC.Resolve<ProductRegistryRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}