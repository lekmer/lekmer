﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockPackageProductListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockPackageProductListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockPackageProductListRepository>();

			Assert.IsInstanceOf<BlockPackageProductListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockPackageProductListRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockPackageProductListRepository>();
			var repository2 = IoC.Resolve<BlockPackageProductListRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}