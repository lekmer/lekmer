﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BrandSiteStructureProductRegistryWrapperRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BrandSiteStructureProductRegistryWrapperRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BrandSiteStructureProductRegistryWrapperRepository>();

			Assert.IsInstanceOf<BrandSiteStructureProductRegistryWrapperRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BrandSiteStructureProductRegistryWrapperRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BrandSiteStructureProductRegistryWrapperRepository>();
			var repository2 = IoC.Resolve<BrandSiteStructureProductRegistryWrapperRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}