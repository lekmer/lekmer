﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class SupplierRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SupplierRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SupplierRepository>();

			Assert.IsInstanceOf<SupplierRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SupplierRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<SupplierRepository>();
			var repository2 = IoC.Resolve<SupplierRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}