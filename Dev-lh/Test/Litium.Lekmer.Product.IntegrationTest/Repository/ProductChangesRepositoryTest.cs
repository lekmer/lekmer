﻿using System;
using System.Collections.ObjectModel;
using System.Transactions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductChangesRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductChangesRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductChangesRepository>();

			Assert.IsInstanceOf<ProductChangesRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductChangesRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductChangesRepository>();
			var repository2 = IoC.Resolve<ProductChangesRepository>();

			Assert.AreEqual(repository1, repository2);
		}

		[Test]
		[Category("Database.Save")]
		public void Insert_SaveNewEvent_ItemSaved()
		{
			var productChangeEvent = CreateProductChangeEvent();

			var repository = IoC.Resolve<ProductChangesRepository>();
			int productChangeEventId;

			using (var transactionScope = new TransactionScope())
			{
				productChangeEventId = repository.Insert(productChangeEvent);
				transactionScope.Dispose();
			}

			Assert.IsTrue(productChangeEventId > 0);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllItems_ItemsReturned()
		{
			var repository = IoC.Resolve<ProductChangesRepository>();

			Collection<IProductChangeEvent> productChangeEvents = repository.GetAll(100, ProductChangeEventStatus.InQueue);

			Assert.NotNull(productChangeEvents);
		}

		private IProductChangeEvent CreateProductChangeEvent()
		{
			var productChangeEvent = IoC.Resolve<IProductChangeEvent>();

			productChangeEvent.ProductId = 123456;
			productChangeEvent.EventStatusId = (int)ProductChangeEventStatus.InQueue;
			productChangeEvent.CreatedDate = DateTime.Now;
			productChangeEvent.Reference = "Testing";

			return productChangeEvent;
		}
	}
}