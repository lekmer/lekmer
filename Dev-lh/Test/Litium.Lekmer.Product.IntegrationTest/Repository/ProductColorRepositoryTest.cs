﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductColorRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductColorRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductColorRepository>();

			Assert.IsInstanceOf<ProductColorRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductColorRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductColorRepository>();
			var repository2 = IoC.Resolve<ProductColorRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}