﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using NUnit.Framework;

namespace Litium.Lekmer.Template.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerIncludeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerIncludeSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IIncludeSecureService>();

			Assert.IsInstanceOf<IIncludeSecureService>(secureService);
			Assert.IsInstanceOf<LekmerIncludeSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerIncludeSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IIncludeSecureService>();
			var secureService2 = IoC.Resolve<IIncludeSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}