﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using NUnit.Framework;

namespace Litium.Lekmer.Template.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerIncludeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerIncludeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IIncludeService>();

			Assert.IsInstanceOf<IIncludeService>(service);
			Assert.IsInstanceOf<LekmerIncludeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerIncludeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IIncludeService>();
			var service2 = IoC.Resolve<IIncludeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}