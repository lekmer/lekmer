﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerImageSharedServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerImageSharedService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IImageSharedService>();

			Assert.IsInstanceOf<IImageSharedService>(service);
			Assert.IsInstanceOf<LekmerImageSharedService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerImageSharedService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IImageSharedService>();
			var service2 = IoC.Resolve<IImageSharedService>();

			Assert.AreEqual(service1, service2);
		}
	}
}