﻿using System.Data;
using Litium.Lekmer.Customer.Mapper;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.IntegrationTest.Mapper
{
	[TestFixture]
	public class LekmerCustomerInformationDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ICustomerInformation>(_dataReader);

			Assert.IsInstanceOf<LekmerCustomerInformationDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ICustomerInformation>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ICustomerInformation>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}