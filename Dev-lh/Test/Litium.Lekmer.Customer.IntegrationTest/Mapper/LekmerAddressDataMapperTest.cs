﻿using System.Data;
using Litium.Lekmer.Customer.Mapper;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.IntegrationTest.Mapper
{
	[TestFixture]
	public class LekmerAddressDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IAddress>(_dataReader);

			Assert.IsInstanceOf<LekmerAddressDataMapper<IAddress>>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IAddress>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IAddress>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressDataMapper_ResolveForBillingAddress_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IBillingAddress>(_dataReader);

			Assert.IsInstanceOf<LekmerBillingAddressDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressDataMapper_ResolveForDeliveryAddress_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IDeliveryAddress>(_dataReader);

			Assert.IsInstanceOf<LekmerDeliveryAddressDataMapper>(dataMapper);
		}
	}
}