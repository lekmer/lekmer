﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerUserTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerUser_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IUser>();

			Assert.IsInstanceOf<IUser>(instance);
			Assert.IsInstanceOf<ILekmerUser>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerUser_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IUser>();
			var instance2 = IoC.Resolve<IUser>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}
