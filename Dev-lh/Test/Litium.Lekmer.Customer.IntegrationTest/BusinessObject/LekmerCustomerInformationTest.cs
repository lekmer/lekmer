﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCustomerInformationTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformation_Resolve_Resolved()
		{
			var customerInformation = IoC.Resolve<ICustomerInformation>();

			Assert.IsInstanceOf<ICustomerInformation>(customerInformation);
			Assert.IsInstanceOf<ILekmerCustomerInformation>(customerInformation);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformation_ResolveTwice_DifferentObjects()
		{
			var customerInformation1 = IoC.Resolve<ICustomerInformation>();
			var customerInformation2 = IoC.Resolve<ICustomerInformation>();

			Assert.AreNotEqual(customerInformation1, customerInformation2);
		}
	}
}
