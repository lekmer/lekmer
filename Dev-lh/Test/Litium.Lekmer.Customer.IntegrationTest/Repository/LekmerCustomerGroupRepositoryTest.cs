﻿using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerCustomerGroupRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerGroupRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CustomerGroupRepository>();

			Assert.IsInstanceOf<CustomerGroupRepository>(repository);
			Assert.IsInstanceOf<LekmerCustomerGroupRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerGroupRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CustomerGroupRepository>();
			var repository2 = IoC.Resolve<CustomerGroupRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}