﻿using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerUserRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerUserRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<UserRepository>();

			Assert.IsInstanceOf<UserRepository>(repository);
			Assert.IsInstanceOf<LekmerUserRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerUserRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<UserRepository>();
			var repository2 = IoC.Resolve<UserRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}