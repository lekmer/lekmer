﻿using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerCustomerInformationRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CustomerInformationRepository>();

			Assert.IsInstanceOf<CustomerInformationRepository>(repository);
			Assert.IsInstanceOf<LekmerCustomerInformationRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CustomerInformationRepository>();
			var repository2 = IoC.Resolve<CustomerInformationRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}