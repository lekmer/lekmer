﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class FacebookUserRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void FacebookUserRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<FacebookUserRepository>();

			Assert.IsInstanceOf<FacebookUserRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void FacebookUserRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<FacebookUserRepository>();
			var repository2 = IoC.Resolve<FacebookUserRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}