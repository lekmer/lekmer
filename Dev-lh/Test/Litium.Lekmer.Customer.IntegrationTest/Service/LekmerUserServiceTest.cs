﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerUserServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerUserService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IUserService>();

			Assert.IsInstanceOf<IUserService>(service);
			Assert.IsInstanceOf<UserService>(service);
			Assert.IsInstanceOf<LekmerUserService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerUserService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IUserService>();
			var service2 = IoC.Resolve<IUserService>();

			Assert.AreEqual(service1, service2);
		}
	}
}