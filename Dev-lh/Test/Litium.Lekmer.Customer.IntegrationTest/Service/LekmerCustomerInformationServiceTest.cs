﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCustomerInformationServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICustomerInformationService>();

			Assert.IsInstanceOf<ICustomerInformationService>(service);
			Assert.IsInstanceOf<CustomerInformationService>(service);
			Assert.IsInstanceOf<LekmerCustomerInformationService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICustomerInformationService>();
			var service2 = IoC.Resolve<ICustomerInformationService>();

			Assert.AreEqual(service1, service2);
		}
	}
}