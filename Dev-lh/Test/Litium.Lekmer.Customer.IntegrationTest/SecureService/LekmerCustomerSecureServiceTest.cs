﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCustomerSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICustomerSecureService>();

			Assert.IsInstanceOf<ICustomerSecureService>(service);
			Assert.IsInstanceOf<CustomerSecureService>(service);
			Assert.IsInstanceOf<LekmerCustomerSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICustomerSecureService>();
			var service2 = IoC.Resolve<ICustomerSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}