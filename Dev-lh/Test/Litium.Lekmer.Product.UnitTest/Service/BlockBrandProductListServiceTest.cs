﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.UnitTest.Service
{
	[TestFixture]
	public class BlockBrandProductListServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int BLOCK_ID_1 = 510;
		private static readonly int BLOCK_ID_2 = 514;
		private static readonly int BLOCK_ID_3 = 515;
		private static readonly int BLOCK_ID_4 = 516;

		private static MockRepository _mocker;
		private static IUserContext _userContext;
		private static BlockBrandProductListRepository _blockBrandProductListRepository;
		private static BlockBrandProductListService _blockBrandProductListService;
		private static IChannel _channel;
		private static ILanguage _language;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_userContext = _mocker.Stub<IUserContext>();
			_blockBrandProductListRepository = _mocker.DynamicMock<BlockBrandProductListRepository>();
			_blockBrandProductListService = new BlockBrandProductListService(_blockBrandProductListRepository);

			_language = _mocker.Stub<ILanguage>();
			_language.Id = CHANNEL_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			BlockBrandProductListCache.Instance.Flush();
		}

		[Test]
		public void GetById_SameId_CacheUsed()
		{
			Expect.Call(_blockBrandProductListRepository.GetById(_channel, BLOCK_ID_1)).Return(new BlockBrandProductList { Id = BLOCK_ID_1 });

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_1, _blockBrandProductListService.GetById(_userContext, BLOCK_ID_1).Id);
			Assert.AreEqual(BLOCK_ID_1, _blockBrandProductListService.GetById(_userContext, BLOCK_ID_1).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_blockBrandProductListRepository.GetById(_channel, BLOCK_ID_2)).Return(new BlockBrandProductList { Id = BLOCK_ID_2 });
			Expect.Call(_blockBrandProductListRepository.GetById(_channel, BLOCK_ID_3)).Return(new BlockBrandProductList { Id = BLOCK_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_2, _blockBrandProductListService.GetById(_userContext, BLOCK_ID_2).Id);
			Assert.AreEqual(BLOCK_ID_3, _blockBrandProductListService.GetById(_userContext, BLOCK_ID_3).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_BlockRemoved_CacheRemoved()
		{
			IBlockBrandProductList blockBrandProductList = new BlockBrandProductList { Id = BLOCK_ID_4 };

			Expect.Call(_blockBrandProductListRepository.GetById(_channel, BLOCK_ID_4)).Return(blockBrandProductList).Repeat.Twice();

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_4, _blockBrandProductListService.GetById(_userContext, BLOCK_ID_4).Id);
			BlockBrandProductListCache.Instance.Remove(BLOCK_ID_4);
			Assert.AreEqual(BLOCK_ID_4, _blockBrandProductListService.GetById(_userContext, BLOCK_ID_4).Id);

			_mocker.VerifyAll();
		}
	}}
