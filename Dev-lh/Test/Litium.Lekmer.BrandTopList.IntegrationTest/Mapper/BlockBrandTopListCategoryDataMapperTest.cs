﻿using System.Data;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockBrandTopListCategoryDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategoryDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IBlockBrandTopListCategory>(_dataReader);

			Assert.IsInstanceOf<BlockBrandTopListCategoryDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListCategoryDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IBlockBrandTopListCategory>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IBlockBrandTopListCategory>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}