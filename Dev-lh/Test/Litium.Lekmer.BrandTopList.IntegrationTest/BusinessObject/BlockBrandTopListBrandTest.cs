﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockBrandTopListBrandTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrand_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockBrandTopListBrand>();

			Assert.IsInstanceOf<IBlockBrandTopListBrand>(instance);
			Assert.IsInstanceOf<BlockBrandTopListBrand>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrand_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListBrand>();
			var instance2 = IoC.Resolve<IBlockBrandTopListBrand>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}