﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ChannelFormatInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ChannelFormatInfo_Resolve_Resolved()
		{
			var channelFormatInfo = IoC.Resolve<IChannelFormatInfo>();

			Assert.IsInstanceOf<IChannelFormatInfo>(channelFormatInfo);
		}

		[Test]
		[Category("IoC")]
		public void ChannelFormatInfo_ResolveTwice_DifferentObjects()
		{
			var channelFormatInfo1 = IoC.Resolve<IChannelFormatInfo>();
			var channelFormatInfo2 = IoC.Resolve<IChannelFormatInfo>();

			Assert.AreNotEqual(channelFormatInfo1, channelFormatInfo2);
		}
	}
}