﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SubDomainTest
	{
		[Test]
		[Category("IoC")]
		public void SubDomain_Resolve_Resolved()
		{
			var subDomain = IoC.Resolve<ISubDomain>();

			Assert.IsInstanceOf<ISubDomain>(subDomain);
		}

		[Test]
		[Category("IoC")]
		public void SubDomain_ResolveTwice_DifferentObjects()
		{
			var subDomain1 = IoC.Resolve<ISubDomain>();
			var subDomain2 = IoC.Resolve<ISubDomain>();

			Assert.AreNotEqual(subDomain1, subDomain2);
		}
	}
}