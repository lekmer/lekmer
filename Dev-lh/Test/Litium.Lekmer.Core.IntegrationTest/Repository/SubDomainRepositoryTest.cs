﻿using Litium.Lekmer.Core.Repository;
using Litium.Scensum.Core.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SubDomainRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SubDomainRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SubDomainRepository>();

			Assert.IsInstanceOf<SubDomainRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SubDomainRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<SubDomainRepository>();
			var repository2 = IoC.Resolve<SubDomainRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}