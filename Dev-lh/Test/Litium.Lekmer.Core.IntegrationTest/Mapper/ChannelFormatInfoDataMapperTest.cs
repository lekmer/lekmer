﻿using System.Data;
using Litium.Lekmer.Core.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ChannelFormatInfoDataMapperTest
	{
		private static MockRepository _mocker;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		[Category("IoC")]
		public void ChannelFormatInfoDataMapper_Resolve_Resolved()
		{
			var dataReader = _mocker.Stub<IDataReader>();

			var mapper = DataMapperResolver.Resolve<IChannelFormatInfo>(dataReader);

			Assert.IsInstanceOf<ChannelFormatInfoDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void ChannelFormatInfoDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataReader = _mocker.Stub<IDataReader>();

			var mapper1 = DataMapperResolver.Resolve<IChannelFormatInfo>(dataReader);
			var mapper2 = DataMapperResolver.Resolve<IChannelFormatInfo>(dataReader);

			Assert.AreNotEqual(mapper1, mapper2);
		}
	}
}