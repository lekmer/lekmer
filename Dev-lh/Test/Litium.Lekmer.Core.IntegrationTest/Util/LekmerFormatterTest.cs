﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.Util
{
	[TestFixture]
	public class LekmerFormatterTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerFormatter_ResolveService_ServiceResolved()
		{
			var service = IoC.Resolve<IFormatter>();

			Assert.IsInstanceOf<IFormatter>(service);
			Assert.IsInstanceOf<ILekmerFormatter>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerFormatter_ResolveServiceTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IFormatter>();
			var service2 = IoC.Resolve<IFormatter>();

			Assert.AreEqual(service1, service2);
		}
	}
}