﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Core.IntegrationTest.Service
{
	[TestFixture]
	public class SubDomainServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;
		private ICurrency _currency;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_currency = _mocker.Stub<ICurrency>();
			_currency.Id = CURRENCY_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;
			_channel.Currency = _currency;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void SubDomainService_ResolveService_ServiceResolved()
		{
			var service = IoC.Resolve<ISubDomainService>();

			Assert.IsInstanceOf<ISubDomainService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SubDomainService_ResolveServiceTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISubDomainService>();
			var service2 = IoC.Resolve<ISubDomainService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("IoC")]
		public void GetAllByContentType_ExecuteWithDifferentContentType_CorrectResults()
		{
			var service = IoC.Resolve<ISubDomainService>();

			Collection<ISubDomain> subDomainNone = service.GetAllByContentType(_channel, (int)ContentType.None);
			Collection<ISubDomain> subDomainStatic = service.GetAllByContentType(_channel, (int)ContentType.StaticMedia);
			Collection<ISubDomain> subDomainMedia = service.GetAllByContentType(_channel, (int)ContentType.MediaArchiveWeb);

			Assert.IsTrue(subDomainNone.Count == 0, "None: count != 0");
			Assert.IsTrue(subDomainStatic.Count > 0, "Static: count <= 0");
			Assert.IsTrue(subDomainMedia.Count > 0, "Media: count <= 0");
		}
	}
}