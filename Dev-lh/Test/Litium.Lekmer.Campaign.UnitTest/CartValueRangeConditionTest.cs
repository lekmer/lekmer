﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class CartValueRangeConditionTest
	{
		[Test, Sequential]
		public void FulfilledWorksCorrectly(
			[Values(1, 1797, 1798, 1, 1, 1000000, 1, 1797, 179)] int monetaryValueFrom,
			[Values(2000, 1798, 2000, 1797, 1796, 1100000, 10, 17970, 1790)] int monetaryValueTo,
			[Values(true, true, true, true, true, true, false, false, false)] bool swedishCurrency,
			[Values(true, true, false, true, false, false, false, false, false)] bool expectedResult)
		{
			var cart = TestHelper.GetCartTemplate();

			cart.SetItems(new Collection<ICartItem> {
				new CartItem
				{
					CartId = cart.Id,
					Quantity = 1,
					Product = TestHelper.GetProductFifa(),
					CreatedDate = DateTime.Now
				},
				new CartItem
				{
					CartId = cart.Id,
					Quantity = 2,
					Product = TestHelper.GetProductNhl(),
					CreatedDate = DateTime.Now
				}
			});

			var userContext = TestHelper.GetSwedishUserContext();
			userContext.Cart = cart;

			if (!swedishCurrency)
			{
				userContext.Channel.Currency = TestHelper.GetBolivianBolivianosCurrency();
			}

			var cartValueRangeCondition = new CartValueRangeCondition
			{
				CartValueRangeList = new Collection<CurrencyValueRange>
					{
						new CurrencyValueRange
							{
								CurrencyId = TestHelper.GetSwedishKronorCurrency().Id,
								MonetaryValueFrom = monetaryValueFrom,
								MonetaryValueTo = monetaryValueTo,
							}
					}
			};

			Assert.AreEqual(expectedResult, cartValueRangeCondition.Fulfilled(userContext));
		}
	}
}