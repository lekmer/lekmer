﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using NUnit.Framework;

namespace Litium.Lekmer.Template.IntegrationTest.SharedService
{
	[TestFixture]
	public class LekmerIncludeSharedServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerIncludeSharedService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IIncludeSharedService>();

			Assert.IsInstanceOf<IIncludeSharedService>(service);
			Assert.IsInstanceOf<LekmerIncludeSharedService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerIncludeSharedService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IIncludeSharedService>();
			var service2 = IoC.Resolve<IIncludeSharedService>();

			Assert.AreEqual(service1, service2);
		}
	}
}