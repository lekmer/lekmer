﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Cache;
using Litium.Scensum.Template.Repository;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Template.IntegrationTest.SharedService
{
	[TestFixture]
	public class LekmerAliasSharedServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly string COMMON_NAME_1 = "Order.Checkout.Voucher.UseVoucher.Button.Value_1";
		private static readonly string COMMON_NAME_2 = "Order.Checkout.Voucher.UseVoucher.Button.Value_2";
		private static readonly string COMMON_NAME_3 = "Order.Checkout.Voucher.UseVoucher.Button.Value_3";
		private static readonly string COMMON_NAME_4 = "Order.Checkout.Voucher.UseVoucher.Button.Value_4";

		private static MockRepository _mocker;
		private static IAccessValidator _accessValidator;
		private static ISystemUserFull _systemUser;
		private static AliasRepository _aliasRepository;
		private static LekmerAliasSharedService _lekmerAliasSharedService;
		private static LekmerAliasSecureService _lekmerAliasSecureService;
		
		private static IChannel _channel;
		private static ILanguage _language;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUser = _mocker.Stub<ISystemUserFull>();
			_aliasRepository = _mocker.DynamicMock<AliasRepository>();
			_lekmerAliasSharedService = new LekmerAliasSharedService(_aliasRepository);
			_lekmerAliasSecureService = new LekmerAliasSecureService(_accessValidator, _aliasRepository);

			_language = _mocker.Stub<ILanguage>();
			_language.Id = CHANNEL_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			AliasCache.Instance.Flush();
		}

		[Test]
		public void GetByCommonName_SameCommonName_CacheUsed()
		{
			Expect.Call(_aliasRepository.GetByCommonName(CHANNEL_ID, COMMON_NAME_1)).Return(null);

			_mocker.ReplayAll();

			Assert.IsNull(_lekmerAliasSharedService.GetByCommonName(_channel, COMMON_NAME_1));
			Assert.IsNull(_lekmerAliasSharedService.GetByCommonName(_channel, COMMON_NAME_1));

			_mocker.VerifyAll();
		}

		[Test]
		public void GetByCommonName_TwoCommonName_CacheIsNotUsed()
		{
			Expect.Call(_aliasRepository.GetByCommonName(CHANNEL_ID, COMMON_NAME_2)).Return(null);
			Expect.Call(_aliasRepository.GetByCommonName(CHANNEL_ID, COMMON_NAME_3)).Return(null);

			_mocker.ReplayAll();

			Assert.IsNull(_lekmerAliasSharedService.GetByCommonName(_channel, COMMON_NAME_2));
			Assert.IsNull(_lekmerAliasSharedService.GetByCommonName(_channel, COMMON_NAME_3));

			_mocker.VerifyAll();
		}

		[Test]
		public void GetByCommonName_AliasUpdated_CacheRemoved()
		{
			IAlias alias = new Alias {CommonName = COMMON_NAME_4};

			Expect.Call(_aliasRepository.Save(alias)).Return(-1).IgnoreArguments();
			Expect.Call(_aliasRepository.GetByCommonName(CHANNEL_ID, COMMON_NAME_4)).Return(null).Repeat.Twice();
			
			_mocker.ReplayAll();
			
			Assert.IsNull(_lekmerAliasSharedService.GetByCommonName(_channel, COMMON_NAME_4));
			_lekmerAliasSecureService.Save(_systemUser, alias);
			Assert.IsNull(_lekmerAliasSharedService.GetByCommonName(_channel, COMMON_NAME_4));

			_mocker.VerifyAll();
		}

		[Test]
		[Category("IoC")]
		public void LekmerAliasSharedService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IAliasSharedService>();

			Assert.IsInstanceOf<IAliasSharedService>(service);
			Assert.IsInstanceOf<LekmerAliasSharedService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAliasSharedService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IAliasSharedService>();
			var service2 = IoC.Resolve<IAliasSharedService>();

			Assert.AreEqual(service1, service2);
		}
	}
}