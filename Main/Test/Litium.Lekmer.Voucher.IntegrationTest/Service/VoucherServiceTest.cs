﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Voucher.IntegrationTest.Service
{
	[TestFixture]
	public class VoucherServiceTest
	{
		[Test]
		[Category("IoC")]
		public void VoucherService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IVoucherService>();

			Assert.IsInstanceOf<IVoucherService>(service);
			Assert.IsInstanceOf<VoucherService>(service);
		}

		[Test]
		[Category("IoC")]
		public void VoucherService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IVoucherService>();
			var instance2 = IoC.Resolve<IVoucherService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}