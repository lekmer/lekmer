﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.DataExport.IntegrationTest.Service
{
	[TestFixture]
	public class ProductPriceInfoServiceTest
	{
		private static readonly int CHANNEL_ID = 1;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfoService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductPriceInfoService>();

			Assert.IsInstanceOf<IProductPriceInfoService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfoService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductPriceInfoService>();
			var service2 = IoC.Resolve<IProductPriceInfoService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAll_ReturnedList()
		{
			var productPriceInfoService = IoC.Resolve<IProductPriceInfoService>();

			Collection<IProductPriceInfo> productPriceInfos = productPriceInfoService.GetAll(_userContext);

			Assert.IsNotNull(productPriceInfos);
		}
	}
}