﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.Service
{
	[TestFixture]
	public class MediaUrlServiceTest
	{
		[Test]
		[Category("IoC")]
		public void MediaUrlService_ResolveService_ServiceResolved()
		{
			var service = IoC.Resolve<IMediaUrlService>();

			Assert.IsInstanceOf<IMediaUrlService>(service);
		}

		[Test]
		[Category("IoC")]
		public void MediaUrlService_ResolveServiceTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMediaUrlService>();
			var service2 = IoC.Resolve<IMediaUrlService>();

			Assert.AreEqual(service1, service2);
		}
	}
}