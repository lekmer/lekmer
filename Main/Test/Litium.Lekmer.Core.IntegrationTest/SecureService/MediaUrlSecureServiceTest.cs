﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.SecureService
{
	[TestFixture]
	public class MediaUrlSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void MediaUrlSecureService_ResolveService_ServiceResolved()
		{
			var service = IoC.Resolve<IMediaUrlSecureService>();

			Assert.IsInstanceOf<IMediaUrlSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void MediaUrlSecureService_ResolveServiceTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMediaUrlSecureService>();
			var service2 = IoC.Resolve<IMediaUrlSecureService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("IoC")]
		public void GetBackOfficeMediaUrl_ExecuteMethod_CorrectResults()
		{
			var service = IoC.Resolve<IMediaUrlSecureService>();

			string backOfficeMediaUrl = service.GetBackOfficeMediaUrl();

			//Assert.AreEqual("http://media.lekmer.com", backOfficeMediaUrl);
		}
	}
}