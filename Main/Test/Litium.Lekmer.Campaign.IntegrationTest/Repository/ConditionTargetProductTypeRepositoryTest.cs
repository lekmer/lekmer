﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class ConditionTargetProductTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ConditionTargetProductTypeRepository>();

			Assert.IsInstanceOf<ConditionTargetProductTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ConditionTargetProductTypeRepository>();
			var instance2 = IoC.Resolve<ConditionTargetProductTypeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}