﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CartValueRangeConditionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CartValueRangeConditionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CartValueRangeConditionRepository>();

			Assert.IsInstanceOf<CartValueRangeConditionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CartValueRangeConditionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CartValueRangeConditionRepository>();
			var instance2 = IoC.Resolve<CartValueRangeConditionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}