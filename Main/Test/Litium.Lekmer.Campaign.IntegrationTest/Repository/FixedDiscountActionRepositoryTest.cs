﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class FixedDiscountActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<FixedDiscountActionRepository>();

			Assert.IsInstanceOf<FixedDiscountActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<FixedDiscountActionRepository>();
			var instance2 = IoC.Resolve<FixedDiscountActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}