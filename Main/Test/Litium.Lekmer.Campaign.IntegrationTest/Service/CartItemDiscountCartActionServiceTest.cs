﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartItemDiscountCartActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginService>("CartItemDiscount");

			Assert.IsInstanceOf<ICartActionPluginService>(service);
			Assert.IsInstanceOf<CartItemDiscountCartActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginService>("CartItemDiscount");
			var instance2 = IoC.Resolve<ICartActionPluginService>("CartItemDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}