﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerConditionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerConditionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionService>();

			Assert.IsInstanceOf<IConditionService>(service);
			Assert.IsInstanceOf<LekmerConditionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerConditionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionService>();
			var instance2 = IoC.Resolve<IConditionService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}