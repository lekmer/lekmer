﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class GiftCardViaMailInfoServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IGiftCardViaMailInfoService>();

			Assert.IsInstanceOf<IGiftCardViaMailInfoService>(service);
			Assert.IsInstanceOf<GiftCardViaMailInfoService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailInfoService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IGiftCardViaMailInfoService>();
			var instance2 = IoC.Resolve<IGiftCardViaMailInfoService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}