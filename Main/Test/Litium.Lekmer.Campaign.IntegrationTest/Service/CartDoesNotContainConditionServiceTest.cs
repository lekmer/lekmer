﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartDoesNotContainConditionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartDoesNotContainConditionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginService>("CartDoesNotContain");

			Assert.IsInstanceOf<IConditionPluginService>(service);
			Assert.IsInstanceOf<CartDoesNotContainConditionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartDoesNotContainConditionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginService>("CartDoesNotContain");
			var instance2 = IoC.Resolve<IConditionPluginService>("CartDoesNotContain");

			Assert.AreEqual(instance1, instance2);
		}
	}
}