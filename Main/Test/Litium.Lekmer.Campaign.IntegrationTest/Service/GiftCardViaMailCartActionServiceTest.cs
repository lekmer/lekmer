﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class GiftCardViaMailCartActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginService>("GiftCardViaMailCart");

			Assert.IsInstanceOf<ICartActionPluginService>(service);
			Assert.IsInstanceOf<GiftCardViaMailCartActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginService>("GiftCardViaMailCart");
			var instance2 = IoC.Resolve<ICartActionPluginService>("GiftCardViaMailCart");

			Assert.AreEqual(instance1, instance2);
		}
	}
}