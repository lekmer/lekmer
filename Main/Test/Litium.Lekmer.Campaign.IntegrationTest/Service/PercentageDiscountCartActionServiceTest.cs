﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class PercentageDiscountCartActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginService>("PercentageDiscount");

			Assert.IsInstanceOf<ICartActionPluginService>(service);
			Assert.IsInstanceOf<PercentageDiscountCartActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginService>("PercentageDiscount");
			var instance2 = IoC.Resolve<ICartActionPluginService>("PercentageDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}