﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCartActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionService>();

			Assert.IsInstanceOf<ICartActionService>(service);
			Assert.IsInstanceOf<LekmerCartActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionService>();
			var instance2 = IoC.Resolve<ICartActionService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}