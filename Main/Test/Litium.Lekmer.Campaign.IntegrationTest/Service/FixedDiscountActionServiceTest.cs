﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class FixedDiscountActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginService>("FixedDiscount");

			Assert.IsInstanceOf<IProductActionPluginService>(service);
			Assert.IsInstanceOf<FixedDiscountActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginService>("FixedDiscount");
			var instance2 = IoC.Resolve<IProductActionPluginService>("FixedDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}