﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class FixedPriceActionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void FixedPriceActionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginService>("FixedPrice");

			Assert.IsInstanceOf<IProductActionPluginService>(service);
			Assert.IsInstanceOf<FixedPriceActionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void FixedPriceActionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginService>("FixedPrice");
			var instance2 = IoC.Resolve<IProductActionPluginService>("FixedPrice");

			Assert.AreEqual(instance1, instance2);
		}
	}
}