﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartCampaignSharedSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartCampaignSharedSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartCampaignSharedService>("CartCampaignSharedSecureService");

			Assert.IsInstanceOf<ICartCampaignSharedService>(service);
			Assert.IsInstanceOf<CartCampaignSharedSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartCampaignSharedSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartCampaignSharedService>("CartCampaignSharedSecureService");
			var instance2 = IoC.Resolve<ICartCampaignSharedService>("CartCampaignSharedSecureService");

			Assert.AreEqual(instance1, instance2);
		}
	}
}