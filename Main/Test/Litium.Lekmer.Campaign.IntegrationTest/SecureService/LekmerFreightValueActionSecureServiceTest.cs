﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerFreightValueActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginSecureService>("secure_FreightValue");

			Assert.IsInstanceOf<ICartActionPluginSecureService>(service);
			Assert.IsInstanceOf<LekmerFreightValueActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginSecureService>("secure_FreightValue");
			var instance2 = IoC.Resolve<ICartActionPluginSecureService>("secure_FreightValue");

			Assert.AreEqual(instance1, instance2);
		}
	}
}