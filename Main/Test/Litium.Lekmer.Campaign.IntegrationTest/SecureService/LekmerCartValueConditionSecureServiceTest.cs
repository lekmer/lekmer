﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCartValueConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartValueConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginSecureService>("secure_CartValue");

			Assert.IsInstanceOf<IConditionPluginSecureService>(service);
			Assert.IsInstanceOf<LekmerCartValueConditionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartValueConditionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginSecureService>("secure_CartValue");
			var instance2 = IoC.Resolve<IConditionPluginSecureService>("secure_CartValue");

			Assert.AreEqual(instance1, instance2);
		}
	}
}