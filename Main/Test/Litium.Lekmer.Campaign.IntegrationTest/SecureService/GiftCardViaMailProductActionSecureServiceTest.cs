﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class GiftCardViaMailProductActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginSecureService>("secure_GiftCardViaMailProduct");

			Assert.IsInstanceOf<IProductActionPluginSecureService>(service);
			Assert.IsInstanceOf<GiftCardViaMailProductActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginSecureService>("secure_GiftCardViaMailProduct");
			var instance2 = IoC.Resolve<IProductActionPluginSecureService>("secure_GiftCardViaMailProduct");

			Assert.AreEqual(instance1, instance2);
		}
	}
}