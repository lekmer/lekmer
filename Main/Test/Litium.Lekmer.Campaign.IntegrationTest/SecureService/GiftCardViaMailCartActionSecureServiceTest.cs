﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class GiftCardViaMailCartActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailCartActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginSecureService>("secure_GiftCardViaMailCart");

			Assert.IsInstanceOf<ICartActionPluginSecureService>(service);
			Assert.IsInstanceOf<GiftCardViaMailCartActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailCartActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginSecureService>("secure_GiftCardViaMailCart");
			var instance2 = IoC.Resolve<ICartActionPluginSecureService>("secure_GiftCardViaMailCart");

			Assert.AreEqual(instance1, instance2);
		}
	}
}