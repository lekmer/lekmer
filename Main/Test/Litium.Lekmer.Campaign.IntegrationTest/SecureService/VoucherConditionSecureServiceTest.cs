﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class VoucherConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void VoucherConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginSecureService>("secure_Voucher");

			Assert.IsInstanceOf<IConditionPluginSecureService>(service);
			Assert.IsInstanceOf<VoucherConditionSecureService>(service);
		}
	}
}