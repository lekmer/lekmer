﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCampaignTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCampaign_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaign>();

			Assert.IsInstanceOf<ICampaign>(instance);
			Assert.IsInstanceOf<ILekmerCampaign>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaign_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaign>();
			var instance2 = IoC.Resolve<ICampaign>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}