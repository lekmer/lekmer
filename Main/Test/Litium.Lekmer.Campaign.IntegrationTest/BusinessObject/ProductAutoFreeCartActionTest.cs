﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductAutoFreeCartActionTest
	{
		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductAutoFreeCartAction>();

			Assert.IsInstanceOf<IProductAutoFreeCartAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductAutoFreeCartAction>();
			var instance2 = IoC.Resolve<IProductAutoFreeCartAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}