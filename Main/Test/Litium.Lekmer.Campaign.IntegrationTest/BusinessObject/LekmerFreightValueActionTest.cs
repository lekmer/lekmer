﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerFreightValueActionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerFreightValueAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IFreightValueAction>();

			Assert.IsInstanceOf<IFreightValueAction>(instance);
			Assert.IsInstanceOf<ILekmerFreightValueAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerFreightValueAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IFreightValueAction>();
			var instance2 = IoC.Resolve<IFreightValueAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}