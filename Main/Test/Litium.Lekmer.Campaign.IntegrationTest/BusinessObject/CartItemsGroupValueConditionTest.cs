﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartItemsGroupValueConditionTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueCondition_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartItemsGroupValueCondition>();

			Assert.IsInstanceOf<ICartItemsGroupValueCondition>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartItemsGroupValueCondition_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartItemsGroupValueCondition>();
			var instance2 = IoC.Resolve<ICartItemsGroupValueCondition>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}