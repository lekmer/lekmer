﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class GiftCardViaMailCartActionTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailCartAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IGiftCardViaMailCartAction>();

			Assert.IsInstanceOf<IGiftCardViaMailCartAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailCartAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IGiftCardViaMailCartAction>();
			var instance2 = IoC.Resolve<IGiftCardViaMailCartAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}