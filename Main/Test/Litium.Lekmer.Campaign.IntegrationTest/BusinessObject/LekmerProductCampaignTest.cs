﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerProductCampaignTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerProductCampaign_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductCampaign>();

			Assert.IsInstanceOf<IProductCampaign>(instance);
			Assert.IsInstanceOf<ILekmerProductCampaign>(instance);
			Assert.IsInstanceOf<LekmerProductCampaign>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductCampaign_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductCampaign>();
			var instance2 = IoC.Resolve<IProductCampaign>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}