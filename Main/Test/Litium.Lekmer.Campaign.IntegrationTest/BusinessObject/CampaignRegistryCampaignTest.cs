﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CampaignRegistryCampaignTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaign_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICampaignRegistryCampaign>();
			Assert.IsInstanceOf<ICampaignRegistryCampaign>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaign_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICampaignRegistryCampaign>();
			var instance2 = IoC.Resolve<ICampaignRegistryCampaign>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}