﻿using System.Data;
using Litium.Lekmer.Campaign.Mapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.IntegrationTest.Mapper
{
	[TestFixture]
	public class LekmerCampaignDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaignDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ICampaign>(_dataReader);

			Assert.IsInstanceOf<LekmerCampaignDataMapper<ICampaign>>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaignDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ICampaign>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ICampaign>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaignDataMapper_ResolveForCartCampaign_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ICartCampaign>(_dataReader);

			Assert.IsInstanceOf<LekmerCartCampaignDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCampaignDataMapper_ResolveForProductCampaign_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IProductCampaign>(_dataReader);

			Assert.IsInstanceOf<LekmerProductCampaignDataMapper>(dataMapper);
		}
	}
}