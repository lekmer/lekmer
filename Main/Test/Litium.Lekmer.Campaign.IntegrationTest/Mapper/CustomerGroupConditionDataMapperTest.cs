﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.IntegrationTest.Mapper
{
	[TestFixture]
	public class CustomerGroupConditionDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ICustomerGroupCondition>(_dataReader);

			Assert.IsInstanceOf<ConditionDataMapper<ICustomerGroupCondition>>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ICustomerGroupCondition>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ICustomerGroupCondition>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}