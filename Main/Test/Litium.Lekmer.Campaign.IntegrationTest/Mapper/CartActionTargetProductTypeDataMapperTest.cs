﻿using System.Data;
using Litium.Lekmer.Campaign.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.IntegrationTest.Mapper
{
	[TestFixture]
	public class CartActionTargetProductTypeDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ICartActionTargetProductType>(_dataReader);
			Assert.IsInstanceOf<CartActionTargetProductTypeDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void CartActionTargetProductTypeDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ICartActionTargetProductType>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ICartActionTargetProductType>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}