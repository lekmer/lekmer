﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductImageServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductImageService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductImageService>();

			Assert.IsInstanceOf<IProductImageService>(service);
			Assert.IsInstanceOf<ProductImageService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductImageService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductImageService>();
			var service2 = IoC.Resolve<IProductImageService>();

			Assert.AreEqual(service1, service2);
		}
	}
}