﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ProductTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductTypeService>();

			Assert.IsInstanceOf<IProductTypeService>(service);
			Assert.IsInstanceOf<ProductTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductTypeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductTypeService>();
			var service2 = IoC.Resolve<IProductTypeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}