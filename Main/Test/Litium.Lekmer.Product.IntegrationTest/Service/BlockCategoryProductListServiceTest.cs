﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class BlockCategoryProductListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockCategoryProductListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockCategoryProductListService>();

			Assert.IsInstanceOf<IBlockCategoryProductListService>(service);
			Assert.IsInstanceOf<BlockCategoryProductListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockCategoryProductListService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockCategoryProductListService>();
			var service2 = IoC.Resolve<IBlockCategoryProductListService>();

			Assert.AreEqual(service1, service2);
		}
	}
}