﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class WishListPackageItemServiceTest
	{
		[Test]
		[Category("IoC")]
		public void WishListPackageItemService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IWishListPackageItemService>();

			Assert.IsInstanceOf<IWishListPackageItemService>(service);
			Assert.IsInstanceOf<WishListPackageItemService>(service);
		}

		[Test]
		[Category("IoC")]
		public void WishListPackageItemService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IWishListPackageItemService>();
			var service2 = IoC.Resolve<IWishListPackageItemService>();

			Assert.AreEqual(service1, service2);
		}
	}
}