﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductListService>();

			Assert.IsInstanceOf<IBlockProductListService>(service);
			Assert.IsInstanceOf<BlockProductListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductListService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockProductListService>();
			var service2 = IoC.Resolve<IBlockProductListService>();

			Assert.AreEqual(service1, service2);
		}
	}
}