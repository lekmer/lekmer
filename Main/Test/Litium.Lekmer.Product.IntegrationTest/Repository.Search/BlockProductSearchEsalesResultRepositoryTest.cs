﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductSearchEsalesResultRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResultRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockProductSearchEsalesResultRepository>();

			Assert.IsInstanceOf<BlockProductSearchEsalesResultRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResultRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockProductSearchEsalesResultRepository>();
			var repository2 = IoC.Resolve<BlockProductSearchEsalesResultRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}