﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class DeliveryTimeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void DeliveryTimeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<DeliveryTimeRepository>();

			Assert.IsInstanceOf<DeliveryTimeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void DeliveryTimeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<DeliveryTimeRepository>();
			var repository2 = IoC.Resolve<DeliveryTimeRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}