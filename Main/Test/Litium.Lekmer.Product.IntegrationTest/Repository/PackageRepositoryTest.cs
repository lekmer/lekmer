﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class PackageRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void PackageRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<PackageRepository>();

			Assert.IsInstanceOf<PackageRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void PackageRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<PackageRepository>();
			var repository2 = IoC.Resolve<PackageRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}