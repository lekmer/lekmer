﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class StockRangeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void StockRangeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<StockRangeRepository>();

			Assert.IsInstanceOf<StockRangeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void StockRangeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<StockRangeRepository>();
			var repository2 = IoC.Resolve<StockRangeRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}