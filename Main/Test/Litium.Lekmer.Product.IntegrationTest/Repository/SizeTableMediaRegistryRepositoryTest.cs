﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class SizeTableMediaRegistryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SizeTableMediaRegistryRepository>();

			Assert.IsInstanceOf<SizeTableMediaRegistryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistryRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SizeTableMediaRegistryRepository>();
			var instance2 = IoC.Resolve<SizeTableMediaRegistryRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}