﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductImageListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductImageListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockProductImageListRepository>();

			Assert.IsInstanceOf<BlockProductImageListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductImageListRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockProductImageListRepository>();
			var instance2 = IoC.Resolve<BlockProductImageListRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}