﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockCategoryProductListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockCategoryProductListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockCategoryProductListRepository>();

			Assert.IsInstanceOf<BlockCategoryProductListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockCategoryProductListRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockCategoryProductListRepository>();
			var instance2 = IoC.Resolve<BlockCategoryProductListRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}