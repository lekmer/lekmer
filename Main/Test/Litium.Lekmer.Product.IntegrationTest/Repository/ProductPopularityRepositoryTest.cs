﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductPopularityRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductPopularityRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductPopularityRepository>();

			Assert.IsInstanceOf<ProductPopularityRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductPopularityRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductPopularityRepository>();
			var repository2 = IoC.Resolve<ProductPopularityRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}