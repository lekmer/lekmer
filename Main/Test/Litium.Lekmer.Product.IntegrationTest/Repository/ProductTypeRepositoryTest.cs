﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductTypeRepository>();

			Assert.IsInstanceOf<ProductTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductTypeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductTypeRepository>();
			var repository2 = IoC.Resolve<ProductTypeRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}