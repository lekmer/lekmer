﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductImageListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductImageList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockProductImageList>();
			Assert.IsInstanceOf<IBlockProductImageList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductImageList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductImageList>();
			var instance2 = IoC.Resolve<IBlockProductImageList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}