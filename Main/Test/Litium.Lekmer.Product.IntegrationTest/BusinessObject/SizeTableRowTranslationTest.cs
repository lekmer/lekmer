﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableRowTranslationTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableRowTranslation_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableRowTranslation>();

			Assert.IsInstanceOf<ISizeTableRowTranslation>(instance);
			Assert.IsInstanceOf<ISizeTableRowTranslation>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRowTranslation_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableRowTranslation>();
			var instance2 = IoC.Resolve<ISizeTableRowTranslation>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}