﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SupplierTest
	{
		[Test]
		[Category("IoC")]
		public void Supplier_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISupplier>();

			Assert.IsInstanceOf<ISupplier>(instance);
		}

		[Test]
		[Category("IoC")]
		public void Supplier_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISupplier>();
			var instance2 = IoC.Resolve<ISupplier>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}