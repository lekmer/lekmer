﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductRegistryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistry_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductRegistry>();

			Assert.IsInstanceOf<IProductRegistry>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistry_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistry>();
			var instance2 = IoC.Resolve<IProductRegistry>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}