﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PackageTest
	{
		[Test]
		[Category("IoC")]
		public void Package_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IPackage>();

			Assert.IsInstanceOf<IPackage>(instance);
		}

		[Test]
		[Category("IoC")]
		public void Package_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IPackage>();
			var instance2 = IoC.Resolve<IPackage>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}