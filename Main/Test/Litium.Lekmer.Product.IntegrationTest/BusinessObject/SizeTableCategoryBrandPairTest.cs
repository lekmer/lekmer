﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableCategoryBrandPairTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableCategoryBrandPair_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableCategoryBrandPair>();

			Assert.IsInstanceOf<ISizeTableCategoryBrandPair>(instance);
			Assert.IsInstanceOf<ISizeTableCategoryBrandPair>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableCategoryBrandPair_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableCategoryBrandPair>();
			var instance2 = IoC.Resolve<ISizeTableCategoryBrandPair>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}