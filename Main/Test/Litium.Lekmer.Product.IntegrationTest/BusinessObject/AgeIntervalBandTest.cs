﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class AgeIntervalBandTest
	{
		[Test]
		[Category("IoC")]
		public void AgeIntervalBand_Resolve_Resolved()
		{
			var ageIntervalBand = IoC.Resolve<IAgeIntervalBand>();

			Assert.IsInstanceOf<IAgeIntervalBand>(ageIntervalBand);
		}

		[Test]
		[Category("IoC")]
		public void AgeIntervalBand_ResolveTwice_DifferentObjects()
		{
			var ageIntervalBand1 = IoC.Resolve<IAgeIntervalBand>();
			var ageIntervalBand2 = IoC.Resolve<IAgeIntervalBand>();

			Assert.AreNotEqual(ageIntervalBand1, ageIntervalBand2);
		}
	}
}