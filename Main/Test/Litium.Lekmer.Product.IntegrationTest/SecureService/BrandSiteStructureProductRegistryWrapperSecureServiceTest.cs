﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BrandSiteStructureProductRegistryWrapperSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BrandSiteStructureProductRegistryWrapperSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBrandSiteStructureProductRegistryWrapperSecureService>();

			Assert.IsInstanceOf<IBrandSiteStructureProductRegistryWrapperSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BrandSiteStructureProductRegistryWrapperSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBrandSiteStructureProductRegistryWrapperSecureService>();
			var instance2 = IoC.Resolve<IBrandSiteStructureProductRegistryWrapperSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}