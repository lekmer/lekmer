﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class SizeTableRowSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableRowSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISizeTableRowSecureService>();

			Assert.IsInstanceOf<ISizeTableRowSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRowSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableRowSecureService>();
			var instance2 = IoC.Resolve<ISizeTableRowSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}