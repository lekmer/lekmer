﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductImageListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductImageListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductImageListSecureService>();

			Assert.IsInstanceOf<IBlockProductImageListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductImageListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductImageListSecureService>();
			var instance2 = IoC.Resolve<IBlockProductImageListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}