﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerRelationListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerRelationListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRelationListSecureService>();

			Assert.IsInstanceOf<IRelationListSecureService>(service);
			Assert.IsInstanceOf<ILekmerRelationListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerRelationListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRelationListSecureService>();
			var instance2 = IoC.Resolve<IRelationListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}