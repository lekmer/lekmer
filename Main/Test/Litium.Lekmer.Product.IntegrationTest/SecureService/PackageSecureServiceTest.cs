﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class PackageSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PackageSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPackageSecureService>();

			Assert.IsInstanceOf<IPackageSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PackageSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IPackageSecureService>();
			var instance2 = IoC.Resolve<IPackageSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}