﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductListSecureService>();

			Assert.IsInstanceOf<IBlockProductListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductListSecureService>();
			var instance2 = IoC.Resolve<IBlockProductListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}