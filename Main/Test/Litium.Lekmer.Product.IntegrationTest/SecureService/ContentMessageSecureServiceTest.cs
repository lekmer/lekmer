﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ContentMessageSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IContentMessageSecureService>();

			Assert.IsInstanceOf<IContentMessageSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IContentMessageSecureService>();
			var instance2 = IoC.Resolve<IContentMessageSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}