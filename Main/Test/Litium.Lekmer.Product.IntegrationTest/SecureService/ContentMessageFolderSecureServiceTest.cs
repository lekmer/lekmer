﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ContentMessageFolderSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageFolderSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IContentMessageFolderSecureService>();

			Assert.IsInstanceOf<IContentMessageFolderSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageFolderSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IContentMessageFolderSecureService>();
			var instance2 = IoC.Resolve<IContentMessageFolderSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}