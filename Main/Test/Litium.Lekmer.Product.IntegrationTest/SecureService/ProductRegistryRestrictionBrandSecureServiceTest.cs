﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductRegistryRestrictionBrandSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionBrandSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductRegistryRestrictionBrandSecureService>();

			Assert.IsInstanceOf<IProductRegistryRestrictionBrandSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionBrandSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistryRestrictionBrandSecureService>();
			var instance2 = IoC.Resolve<IProductRegistryRestrictionBrandSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}