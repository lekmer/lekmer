﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class SizeTableMediaRegistrySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistrySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISizeTableMediaRegistrySecureService>();

			Assert.IsInstanceOf<ISizeTableMediaRegistrySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistrySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableMediaRegistrySecureService>();
			var instance2 = IoC.Resolve<ISizeTableMediaRegistrySecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}