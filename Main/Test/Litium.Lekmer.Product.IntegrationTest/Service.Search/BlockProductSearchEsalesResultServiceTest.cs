﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductSearchEsalesResultServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResultService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductSearchEsalesResultService>();

			Assert.IsInstanceOf<IBlockProductSearchEsalesResultService>(service);
			Assert.IsInstanceOf<BlockProductSearchEsalesResultService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResultService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductSearchEsalesResultService>();
			var instance2 = IoC.Resolve<IBlockProductSearchEsalesResultService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}