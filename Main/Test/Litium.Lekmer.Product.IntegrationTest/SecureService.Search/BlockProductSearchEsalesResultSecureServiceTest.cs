﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductSearchEsalesResultSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResultSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductSearchEsalesResultSecureService>();

			Assert.IsInstanceOf<IBlockProductSearchEsalesResultSecureService>(service);
			Assert.IsInstanceOf<BlockProductSearchEsalesResultSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResultSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductSearchEsalesResultSecureService>();
			var instance2 = IoC.Resolve<IBlockProductSearchEsalesResultSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}