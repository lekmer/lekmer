﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class CategoryTagGroupDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CategoryTagGroup_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ICategoryTagGroup>(_dataReader);

			Assert.IsInstanceOf<CategoryTagGroupDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void CategoryTagGroup_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ICategoryTagGroup>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ICategoryTagGroup>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}