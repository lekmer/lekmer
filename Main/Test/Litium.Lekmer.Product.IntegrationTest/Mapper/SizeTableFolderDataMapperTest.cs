﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class SizeTableFolderDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SizeTableFolderDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ISizeTableFolder>(_dataReader);

			Assert.IsInstanceOf<SizeTableFolderDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableFolderDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ISizeTableFolder>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ISizeTableFolder>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}