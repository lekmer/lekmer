﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Media.Cache;
using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Media.UnitTest.Service
{
	[TestFixture]
	public class LekmerImageServiceTest
	{
		private static MockRepository _mocker;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			LekmerImageCache.Instance.Flush();
		}

		[Test]
		public void GetById_SameId_CacheUsed()
		{
			var imageRepository = _mocker.DynamicMock<LekmerImageRepository>();
			Expect.Call(imageRepository.GetById(1, 11)).Return(new Image { Id = 11 });

			var userContext = _mocker.Stub<IUserContext>();
			userContext.Channel = _mocker.Stub<IChannel>();
			userContext.Channel.Id = 1;

			_mocker.ReplayAll();

			var service = new LekmerImageService(imageRepository, null, null, null, null);
			Assert.AreEqual(11, service.GetById(userContext, 11).Id);
			Assert.AreEqual(11, service.GetById(userContext, 11).Id);
			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_TwoId_CacheIsNotUsed()
		{
			var imageRepository = _mocker.DynamicMock<LekmerImageRepository>();

			Expect.Call(imageRepository.GetById(1, 22)).Return(new Image { Id = 22 });
			Expect.Call(imageRepository.GetById(1, 33)).Return(new Image { Id = 33 });

			var userContext = _mocker.Stub<IUserContext>();
			userContext.Channel = _mocker.Stub<IChannel>();
			userContext.Channel.Id = 1;

			_mocker.ReplayAll();

			var service = new LekmerImageService(imageRepository, null, null, null, null);
			Assert.AreEqual(22, service.GetById(userContext, 22).Id);
			Assert.AreEqual(33, service.GetById(userContext, 33).Id);
			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_ImageUpdated_CacheRemoved()
		{
			var imageRepository = _mocker.DynamicMock<LekmerImageRepository>();

			var image = new Image {Id = 44};

			Expect.Call(imageRepository.GetById(1, 44)).Return(image).Repeat.Twice();

			var stubAccessValidator = _mocker.Stub<IAccessValidator>();
			var stubMediaItemSecureService = _mocker.Stub<IMediaItemSecureService>();
			SetupResult.For(stubMediaItemSecureService.Update(null, null)).Return(44).IgnoreArguments().Repeat.Once();
			var stubChannelSecureService = _mocker.Stub<IChannelSecureService>();
			SetupResult.For(stubChannelSecureService.GetAll()).Return(new Collection<IChannel> { new Channel { Id = 1 } }).Repeat.Once();

			var stubUserContext = _mocker.Stub<IUserContext>();
			stubUserContext.Channel = _mocker.Stub<IChannel>();
			stubUserContext.Channel.Id = 1;

			var stubSystemUser = _mocker.Stub<ISystemUserFull>();

			_mocker.ReplayAll();

			var service = new LekmerImageService(imageRepository, null, null, null, null);
			var secureService = new LekmerImageSecureService(stubAccessValidator, imageRepository, stubMediaItemSecureService, null, null, null, null, null, stubChannelSecureService);

			// Read from repository and add to cache.
			Assert.AreEqual(44, service.GetById(stubUserContext, 44).Id);
			// Update image, it should be removed from cache.
			secureService.Update(stubSystemUser, image);
			// Again read from repository and add to cache.
			Assert.AreEqual(44, service.GetById(stubUserContext, 44).Id);
			
			_mocker.VerifyAll();
		}
	}
}