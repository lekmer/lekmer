using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class VoucherDiscountActionTest
	{
		private const int _swedishKronorCurrencyId = 1;

		private MockRepository _mocker;
		
		[Test]
		public void Invalid_discount_is_not_applied()
		{
			_mocker = new MockRepository();

			var cartItemServiceMock = _mocker.StrictMock<ICartItemService>();
			Expect.Call(cartItemServiceMock.Create()).Repeat.Any().Return(MockCartItem());

			var productService = _mocker.DynamicMock<ILekmerProductService>();

			var cart = _mocker.PartialMock<LekmerCartFull>(cartItemServiceMock, productService);
			Expect.Call(cart.GetCurrency()).IgnoreArguments().Return(new Currency { Id = _swedishKronorCurrencyId });

			_mocker.ReplayAll();

			cart.AddItem((IProduct) GetProduct(110, 100), 1, null, false, 1, new Collection<ICartItemPackageElement>());

			// can not add second product to cart becaseu CartItemService.Create is mocked and thus one and the same product is always referenced.
			cart.CampaignInfo = new LekmerCartCampaignInfo();
			cart.VoucherInfo = new CartVoucherInfo();

			var swedishContext = new LekmerUserContext
			{
				Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } },
				Voucher = new VoucherCheckResult { DiscountValue = 10, IsValid = false, ValueType = VoucherValueType.Price },
				Cart = cart
			};

			var voucherDiscountAction = new VoucherDiscountAction();
			voucherDiscountAction.Apply(swedishContext, cart);

			Assert.AreEqual(110, cart.GetActualPriceSummary().IncludingVat);
			_mocker.VerifyAll();
		}

		[Test]
		public void Price_discount_is_applied()
		{
			_mocker = new MockRepository();

			var cartItemServiceMock = _mocker.StrictMock<ICartItemService>();
			Expect.Call(cartItemServiceMock.Create()).Repeat.Any().Return(MockCartItem());

			var productService = _mocker.DynamicMock<ILekmerProductService>();

			var cart = _mocker.PartialMock<LekmerCartFull>(cartItemServiceMock, productService);
			Expect.Call(cart.GetCurrency()).IgnoreArguments().Return(new Currency { Id = _swedishKronorCurrencyId });

			_mocker.ReplayAll();

			cart.AddItem(GetProduct(110, 100), 1, null, false, 1, new Collection<ICartItemPackageElement>());

			// can not add second product to cart becaseu CartItemService.Create is mocked and thus one and the same product is always referenced.
			cart.CampaignInfo = new LekmerCartCampaignInfo();
			cart.VoucherInfo = new CartVoucherInfo();

			var swedishContext = new LekmerUserContext
			{
				Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } },
				Voucher = new VoucherCheckResult { DiscountValue = 10, IsValid = true, ValueType = VoucherValueType.Price },
				Cart = cart
			};

			var voucherDiscountAction = new VoucherDiscountAction();
			voucherDiscountAction.Apply(swedishContext, cart);

			Assert.AreEqual(100, cart.GetActualPriceSummary().IncludingVat);
			
			_mocker.VerifyAll();
		}

		[Test]
		public void Percentage_discount_is_applied()
		{
			_mocker = new MockRepository();

			var cartItemServiceMock = _mocker.StrictMock<ICartItemService>();
			Expect.Call(cartItemServiceMock.Create()).Repeat.Any().Return(MockCartItem());

			var productService = _mocker.DynamicMock<ILekmerProductService>();

			var cart = _mocker.PartialMock<LekmerCartFull>(cartItemServiceMock, productService);
			Expect.Call(cart.GetCurrency()).IgnoreArguments().Return(new Currency { Id = _swedishKronorCurrencyId });

			_mocker.ReplayAll();

			cart.AddItem(GetProduct(110, 100), 1, null, false, 1, new Collection<ICartItemPackageElement>());

			// can not add second product to cart becaseu CartItemService.Create is mocked and thus one and the same product is always referenced.
			cart.CampaignInfo = new LekmerCartCampaignInfo();
			cart.VoucherInfo = new CartVoucherInfo();

			var swedishContext = new LekmerUserContext
			{
				Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } },
				Voucher = new VoucherCheckResult { DiscountValue = 10, IsValid = true, ValueType = VoucherValueType.Percentage },
				Cart = cart
			};

			var voucherDiscountAction = new VoucherDiscountAction();
			voucherDiscountAction.Apply(swedishContext, cart);

			Assert.AreEqual(99, cart.GetActualPriceSummary().IncludingVat);

			_mocker.VerifyAll();
		}


		private ICartItem MockCartItem()
		{
			var priceListItem = new LekmerPriceListItem();
			var campaignInfo = new LekmerProductCampaignInfo { Price = new Price() };

			var product = new LekmerProduct
				{
					Price = priceListItem,
					CampaignInfo = campaignInfo
				};

			ICartItem cartItem = new LekmerCartItem();
			cartItem.Status = BusinessObjectStatus.New;
			cartItem.Product = product;
			cartItem.CreatedDate = DateTime.Now;
			return cartItem;
		}

		private static LekmerProduct GetProduct(decimal priceIncludingVat, decimal priceExcludingVat)
		{
			var priceListItem = new PriceListItem
				{
					PriceIncludingVat = priceIncludingVat,
					PriceExcludingVat = priceExcludingVat,
					VatPercentage = .1m
				};

			var campaignInfo = new LekmerProductCampaignInfo
				{
					Price = new Price(priceIncludingVat, priceExcludingVat)
				};

			var product = new LekmerProduct
				{
					Id = -1,
					Price = priceListItem,
					CampaignInfo = campaignInfo
				};

			return product;
		}
	}
}