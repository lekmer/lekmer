﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class FixedDiscountCartActionTest
	{
		[Test, Sequential]
		public void ApplyDiscount_CheckForNegativePrice_WorksCorrectly(
			[Values( 600,  600, -100,  600,   600   )] int priceIncludingVat,
			[Values( 500,  500,  100,  500,   500   )] int priceExcludingVat,
			[Values( 480,  777,  777,  480,   777   )] int fixedDiscount,
			[Values( true, true, true, false, false )] bool swedishDiscount,
			[Values( 120,  0,   -100,  600,   600   )] int expectedPriceIncludingVat,
			[Values( 100,  0,    100,  500,   500   )] int expectedPriceExcludingVat)
		{
			Currency currency = swedishDiscount ? TestHelper.GetSwedishKronorCurrency() : TestHelper.GetBolivianBolivianosCurrency();

			var fixedDiscountCartAction = new FixedDiscountCartAction
			{
				Amounts = new LekmerCurrencyValueDictionary { new CurrencyValue { Currency = TestHelper.GetSwedishKronorCurrency(), MonetaryValue = fixedDiscount } }
			};

			var priceSummary = new Price(priceIncludingVat, priceExcludingVat);

			Price priceDiscounted = fixedDiscountCartAction.ApplyDiscount(priceSummary, currency);

			Assert.AreEqual(expectedPriceIncludingVat, priceDiscounted.IncludingVat);
			Assert.AreEqual(expectedPriceExcludingVat, priceDiscounted.ExcludingVat);
		}
	}
}