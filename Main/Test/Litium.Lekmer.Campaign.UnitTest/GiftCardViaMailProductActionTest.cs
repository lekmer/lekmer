﻿using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class GiftCardViaMailProductActionTest
	{
		private const int _swedishKronorCurrencyId = 1;

		[Test]
		public void Apply_ProductAndTargetProduct_ActionApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = CreateAction();
			action.TargetProductTypes.Add((int)ProductTypeEnum.Product);

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(true, isApplied);
		}

		[Test]
		public void Apply_PackageAndTargetProduct_ActionNotApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Package
			};

			var action = CreateAction();
			action.TargetProductTypes.Add((int)ProductTypeEnum.Product);

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(false, isApplied);
		}

		[Test]
		public void Apply_ProductAndTargetPackage_ActionNotApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = CreateAction();
			action.TargetProductTypes.Add((int)ProductTypeEnum.Package);

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(false, isApplied);
		}


		private GiftCardViaMailProductAction CreateAction()
		{
			var campaignConfig = new CampaignConfig
				{
					IncludeAllProducts = true,
					IncludeProducts = new ProductIdDictionary(),
					ExcludeProducts = new ProductIdDictionary(),
					IncludeCategories = new CampaignCategoryDictionary(),
					ExcludeCategories = new CampaignCategoryDictionary(),
					IncludeBrands = new BrandIdDictionary(),
					ExcludeBrands = new BrandIdDictionary()
				};

			var action = new GiftCardViaMailProductAction
			{
				Amounts = new LekmerCurrencyValueDictionary { new CurrencyValue { Currency = TestHelper.GetSwedishKronorCurrency(), MonetaryValue = 100m } },
				CampaignConfig = campaignConfig,
				TargetProductTypes = new IdDictionary()
			};

			return action;
		}
	}
}