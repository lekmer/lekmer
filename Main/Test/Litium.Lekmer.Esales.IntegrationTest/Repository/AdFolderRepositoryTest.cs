﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class AdFolderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void AdFolderRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<AdFolderRepository>();

			Assert.IsInstanceOf<AdFolderRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void AdFolderRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<AdFolderRepository>();
			var instance2 = IoC.Resolve<AdFolderRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}