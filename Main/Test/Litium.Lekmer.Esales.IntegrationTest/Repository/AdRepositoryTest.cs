﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class AdRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void AdRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<AdRepository>();

			Assert.IsInstanceOf<AdRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void AdRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<AdRepository>();
			var instance2 = IoC.Resolve<AdRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}