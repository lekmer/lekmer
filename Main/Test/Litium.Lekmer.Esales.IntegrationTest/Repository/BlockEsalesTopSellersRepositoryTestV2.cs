﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockEsalesTopSellersRepositoryTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersRepositoryV2_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockEsalesTopSellersRepositoryV2>();

			Assert.IsInstanceOf<BlockEsalesTopSellersRepositoryV2>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersRepositoryV2_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockEsalesTopSellersRepositoryV2>();
			var repository2 = IoC.Resolve<BlockEsalesTopSellersRepositoryV2>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}