using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class CompletionHitsTest
	{
		[Test]
		[Category("IoC")]
		public void CompletionHits_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICompletionHits>();

			Assert.IsInstanceOf<ICompletionHits>(info);
			Assert.IsInstanceOf<CompletionHits>(info);
		}

		[Test]
		[Category("IoC")]
		public void CompletionHits_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ICompletionHits>();
			var esalesInfo2 = IoC.Resolve<ICompletionHits>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}