using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class ProductHitInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ProductHitInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IProductHitInfo>();

			Assert.IsInstanceOf<IProductHitInfo>(info);
			Assert.IsInstanceOf<ProductHitInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void ProductHitInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IProductHitInfo>();
			var esalesInfo2 = IoC.Resolve<IProductHitInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}