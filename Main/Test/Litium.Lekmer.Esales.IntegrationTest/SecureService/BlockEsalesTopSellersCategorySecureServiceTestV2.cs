﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockEsalesTopSellersCategorySecureServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategorySecureServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesTopSellersCategorySecureServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersCategorySecureServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategorySecureServiceV2_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockEsalesTopSellersCategorySecureServiceV2>();
			var service2 = IoC.Resolve<IBlockEsalesTopSellersCategorySecureServiceV2>();

			Assert.AreEqual(service1, service2);
		}
	}
}