﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class EsalesRecommendationTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesRecommendationTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesRecommendationTypeSecureService>();

			Assert.IsInstanceOf<IEsalesRecommendationTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRecommendationTypeSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesRecommendationTypeSecureService>();
			var service2 = IoC.Resolve<IEsalesRecommendationTypeSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}