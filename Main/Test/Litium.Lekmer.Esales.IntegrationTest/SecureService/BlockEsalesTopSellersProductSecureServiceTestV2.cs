﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockEsalesTopSellersProductSecureServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductSecureServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesTopSellersProductSecureServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersProductSecureServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductSecureServiceV2_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockEsalesTopSellersProductSecureServiceV2>();
			var service2 = IoC.Resolve<IBlockEsalesTopSellersProductSecureServiceV2>();

			Assert.AreEqual(service1, service2);
		}
	}
}