﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockEsalesTopSellersCategoryDataMapperTestV2
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryDataMapperV2_Resolve_Resolved()
		{
			DataMapperBase<IBlockEsalesTopSellersCategoryV2> dataMapper = DataMapperResolver.Resolve<IBlockEsalesTopSellersCategoryV2>(_dataReader);

			Assert.IsInstanceOf<BlockEsalesTopSellersCategoryDataMapperV2>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryDataMapperV2_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockEsalesTopSellersCategoryV2> dataMapper1 = DataMapperResolver.Resolve<IBlockEsalesTopSellersCategoryV2>(_dataReader);
			DataMapperBase<IBlockEsalesTopSellersCategoryV2> dataMapper2 = DataMapperResolver.Resolve<IBlockEsalesTopSellersCategoryV2>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}