using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class EsalesChannelProductInfoTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesChannelProductInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IEsalesChannelProductInfo>();

			Assert.IsInstanceOf<IEsalesChannelProductInfo>(info);
			Assert.IsInstanceOf<EsalesChannelProductInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void EsalesChannelProductInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IEsalesChannelProductInfo>();
			var esalesInfo2 = IoC.Resolve<IEsalesChannelProductInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}