using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class EsalesProductInfoTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IEsalesProductInfo>();

			Assert.IsInstanceOf<IEsalesProductInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IEsalesProductInfo>();
			var esalesInfo2 = IoC.Resolve<IEsalesProductInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}