﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockEsalesRecommendTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendV2_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockEsalesRecommendV2>();

			Assert.IsInstanceOf<IBlockEsalesRecommendV2>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesRecommendV2_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockEsalesRecommendV2>();
			var block2 = IoC.Resolve<IBlockEsalesRecommendV2>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}