﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockEsalesTopSellersCategoryTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryV2_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockEsalesTopSellersCategoryV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersCategoryV2>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersCategoryV2_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockEsalesTopSellersCategoryV2>();
			var block2 = IoC.Resolve<IBlockEsalesTopSellersCategoryV2>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}