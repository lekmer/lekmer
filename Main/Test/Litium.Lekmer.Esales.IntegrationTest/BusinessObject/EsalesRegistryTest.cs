﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class EsalesRegistryTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesRegistry_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IEsalesRegistry>();

			Assert.IsInstanceOf<IEsalesRegistry>(instance);
			Assert.IsInstanceOf<IEsalesRegistry>(instance);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRegistry_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IEsalesRegistry>();
			var instance2 = IoC.Resolve<IEsalesRegistry>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}