﻿using Litium.Lekmer.Esales.Exporter;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Exporter
{
	[TestFixture]
	public class EsalesProductInfoIncrementalExporterTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductInfoIncrementalExporter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IExporter>("EsalesProductInfoIncrementalExporter");

			Assert.IsInstanceOf<IExporter>(service);
			Assert.IsInstanceOf<EsalesProductInfoIncrementalExporter>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductInfoIncrementalExporter_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IExporter>("EsalesProductInfoIncrementalExporter");
			var service2 = IoC.Resolve<IExporter>("EsalesProductInfoIncrementalExporter");

			Assert.AreNotEqual(service1, service2);
		}
	}
}