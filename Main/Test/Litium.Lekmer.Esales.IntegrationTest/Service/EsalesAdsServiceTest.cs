﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesAdsServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesAdsService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesAdsService>();

			Assert.IsInstanceOf<IEsalesAdsService>(service);
			Assert.IsInstanceOf<EsalesAdsService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesAdsService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesAdsService>();
			var service2 = IoC.Resolve<IEsalesAdsService>();

			Assert.AreEqual(service1, service2);
		}
	}
}