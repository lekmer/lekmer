﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesProductServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesProductService>();

			Assert.IsInstanceOf<IEsalesProductService>(service);
			Assert.IsInstanceOf<EsalesProductService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductService_ResolveTwice_NotSameObjects()
		{
			var service1 = IoC.Resolve<IEsalesProductService>();
			var service2 = IoC.Resolve<IEsalesProductService>();
			//Transient due to internal implementation details
			Assert.AreNotEqual(service1, service2);
		}
	}
}