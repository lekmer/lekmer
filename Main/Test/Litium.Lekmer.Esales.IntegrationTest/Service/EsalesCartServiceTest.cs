﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesCartServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesCartService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesCartService>();

			Assert.IsInstanceOf<IEsalesCartService>(service);
			Assert.IsInstanceOf<EsalesCartService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesCartService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesCartService>();
			var service2 = IoC.Resolve<IEsalesCartService>();

			Assert.AreEqual(service1, service2);
		}
	}
}
