﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class BlockEsalesTopSellersServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesTopSellersServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersServiceV2>(service);
			Assert.IsInstanceOf<BlockEsalesTopSellersServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersServiceV2_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockEsalesTopSellersServiceV2>();
			var instance2 = IoC.Resolve<IBlockEsalesTopSellersServiceV2>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}