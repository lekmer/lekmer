using Litium.Lekmer.Pacsoft.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Pacsoft.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PacsoftInfoTest
	{
		[Test]
		[Category("IoC")]
		public void PacsoftInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IPacsoftInfo>();
			Assert.IsInstanceOf<IPacsoftInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void PacsoftInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IPacsoftInfo>();
			var info2 = IoC.Resolve<IPacsoftInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}