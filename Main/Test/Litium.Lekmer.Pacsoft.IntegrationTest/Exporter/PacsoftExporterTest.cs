﻿using Litium.Lekmer.Pacsoft.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Pacsoft.IntegrationTest.Exporter
{
	[TestFixture]
	public class PacsoftExporterTest
	{
		[Test]
		[Category("IoC")]
		public void PacsoftExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("PacsoftExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<PacsoftExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void PacsoftExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("PacsoftExporter");
			var exporter2 = IoC.Resolve<IExporter>("PacsoftExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}