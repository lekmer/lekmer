﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.SecureService
{
	[TestFixture]
	public class CdonExportRestrictionBrandSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionBrandSecureService>();

			Assert.IsInstanceOf<ICdonExportRestrictionBrandSecureService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionBrandSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrandSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionBrandSecureService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionBrandSecureService>();
			Assert.AreEqual(service1, service2);
		}
	}
}