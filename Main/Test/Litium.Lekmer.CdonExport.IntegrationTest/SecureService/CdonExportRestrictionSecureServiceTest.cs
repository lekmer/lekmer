﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.SecureService
{
	[TestFixture]
	public class CdonExportRestrictionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionSecureService>();

			Assert.IsInstanceOf<ICdonExportRestrictionSecureService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionSecureService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionSecureService>();
			Assert.AreEqual(service1, service2);
		}
	}
}