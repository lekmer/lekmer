﻿using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.SecureService
{
	[TestFixture]
	public class CdonExportRestrictionCategorySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategorySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportRestrictionCategorySecureService>();

			Assert.IsInstanceOf<ICdonExportRestrictionCategorySecureService>(service);
			Assert.IsInstanceOf<CdonExportRestrictionCategorySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategorySecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportRestrictionCategorySecureService>();
			var service2 = IoC.Resolve<ICdonExportRestrictionCategorySecureService>();
			Assert.AreEqual(service1, service2);
		}
	}
}