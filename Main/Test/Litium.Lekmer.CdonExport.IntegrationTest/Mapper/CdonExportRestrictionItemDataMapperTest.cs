﻿using System.Data;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Lekmer.CdonExport.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Mapper
{
	[TestFixture]
	public class CdonExportRestrictionItemDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProduct_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ICdonExportRestrictionItem>(_dataReader);

			Assert.IsInstanceOf<CdonExportRestrictionItemDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProduct_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ICdonExportRestrictionItem>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ICdonExportRestrictionItem>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}