﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Exporter
{
	[TestFixture]
	public class CdonFullExporterTest
	{
		[Test]
		[Category("IoC")]
		public void CdonFullExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("CdonFullExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<CdonFullExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void CdonFullExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("CdonFullExporter");
			var exporter2 = IoC.Resolve<IExporter>("CdonFullExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}