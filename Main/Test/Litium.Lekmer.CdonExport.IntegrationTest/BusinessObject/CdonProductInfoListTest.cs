using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonProductInfoListTest
	{
		[Test]
		[Category("IoC")]
		public void CdonProductInfoList_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonProductInfoList>();
			Assert.IsInstanceOf<ICdonProductInfoList>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonProductInfoList_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonProductInfoList>();
			var info2 = IoC.Resolve<ICdonProductInfoList>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}