﻿using System.Collections.Generic;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Validation
{
	[TestFixture]
	public class ValidationUtilTest
	{
		[Test, Sequential]
		public void IsEmailCorrect(
			[Values(
				"niceandsimple@example.com",
				"very.common@example.com",
				"a.little.lengthy.but.fine@dept.example.com",
				"disposable.style.email.with+symbol@example.com",
				"other.email-with-dash@example.com",
				//"user@[IPv6:2001:db8:1ff::a0b:dbd0]",
				//"\"much.more unusual\"@example.com",
				//"\"very.unusual.@.unusual.com\"@example.com",
				//"\"very.(),:;<>[]\".VERY.\"very@\\ \"very\".unusual\"@strange.example.com",
				//"postbox@com", //(top-level domains are valid hostnames)
				//"admin@mailserver1", //(local domain name with no TLD)
				"!#$%&'*+-/=?^_`{}|~@example.org",
				//"\"()<>[]:,;@\\\"!#$%&'*+-/=?^_`{}| ~.a\"@example.org",
				//"\" \"@example.org", //(space between the quotes)
				//"üñîçøðé@example.com", //(Unicode characters in local part)
				"test_ok@gmail.com"
			)] string email
			)
		{
			bool result = ValidationUtil.IsValidEmail(email);
			Assert.AreEqual(true, result);
		}

		[Test, Sequential]
		public void IsEmailNotCorrect(
			[Values(
				"Abc.example.com",
				"A@b@c@example.com",
				"just\"not\"right@example.com",
				"this is\"not\allowed@example.com",
				"knappen@-live.se",
				"knappen@live-.se",
				"knappen@me.li--ve.se"
			)] string email)
		{
			bool result = ValidationUtil.IsValidEmail(email);
			Assert.AreEqual(false, result);
		}

		[Test, Sequential]
		public void IsEmailCorrectSimple(
			[Values("!#$%&'*+-/=?^_`{}|~unit.test.9@example.org", "name+suername@gmail.com")] string email,
			[Values(true, true)] bool validationResult)
		{
			bool result = ValidationUtil.IsValidEmail(email);
			Assert.AreEqual(validationResult, result);
		}


		[Test, Sequential]
		public void IsPostalCodeCorrect(
			[Values(
				"1234AB",
				"1234ab",
				"1234aB",
				"1234Ab",
				"1234-AB",
				"1234 AB",
				"1000AB"
			)] string postalCode)
		{
			bool result = ValidationUtil.IsValidPostalCode("NL" ,postalCode);
			Assert.AreEqual(true, result);
		}

		[Test, Sequential]
		public void IsPostalCodeNotCorrect(
			[Values(
				"1234A",
				"1234ABC",
				"1234	AB",
				"1234_AB",
				"1234  AB",
				"1234--AB",
				"1234 -AB",
				"1234	-AB",
				"1234	AB",
				"0234AB",
				"123AB"
			)] string postalCode)
		{
			bool result = ValidationUtil.IsValidPostalCode("NL", postalCode);
			Assert.AreEqual(false, result);
		}

		[Test, Sequential]
		public void IsDoorCodeCorrect(
			[Values("SE", "NO", "DK", "NL", "SE", "FI", "FI", "FI", "FI", "FI")] string countryIso,
			[Values("1", "12", "123", "1234", "12345", "*", "*123", "*123*", "**", "12*34")] string doorCode)
		{
			Assert.IsTrue(ValidationUtil.IsValidDoorCode(countryIso, doorCode, new List<string> {"SE", "FI"}));
		}

		[Test, Sequential]
		public void IsDoorCodeNotCorrect(
			[Values("SE", "NO", "DK", "NL", "SE", "FI")] string countryIso,
			[Values("1*", "*", "123a", "21.", "123e", "test")] string doorCode)
		{
			Assert.IsFalse(ValidationUtil.IsValidDoorCode(countryIso, doorCode, new List<string> {"FI"}));
		}
	}
}