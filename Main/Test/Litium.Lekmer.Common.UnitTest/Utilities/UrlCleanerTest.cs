﻿using System;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Utilities
{
	[TestFixture]
	public class UrlCleanerTest
	{
		[Test]
		public void CleanUp_crashes_when_passing_null_value()
		{
			Assert.Throws<ArgumentNullException>(
				delegate { UrlCleaner.CleanUp(null); });
		}

		[Test, Sequential]
		public void CleanUp_cleans_string_and_returns_expected_value(
			[Values(
				"abc 123",
				" abc 123 ",
				"abc  123",
				"åäö üû ñ",
				@"æø€£$&*\/ +=!?:;,._1",
				"a@#{}",
				"-a-")]
			string value,
			[Values(
				"abc-123",
				"abc-123",
				"abc-123",
				"aao-uu-n",
				"aoelsox-1",
				"a",
				"a")]
			string expected)
		{
			string result = UrlCleaner.CleanUp(value);

			Assert.AreEqual(expected, result);
		}

		[Test, Sequential]
		public void CleanUp_cleans_string_and_returns_expected_value_more(
			[Values(
				"børneværelse"
			)]
			string value,
			[Values(
				"bornevarelse"
			)]
			string expected)
		{
			string result = UrlCleaner.CleanUp(value);

			Assert.AreEqual(expected, result);
		}
	}
}