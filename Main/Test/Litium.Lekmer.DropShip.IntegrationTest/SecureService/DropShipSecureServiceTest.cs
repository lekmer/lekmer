﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Service
{
	[TestFixture]
	public class DropShipSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IDropShipSecureService>();

			Assert.IsInstanceOf<IDropShipSecureService>(service);
			Assert.IsInstanceOf<DropShipSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void DropShipSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IDropShipSecureService>();
			var service2 = IoC.Resolve<IDropShipSecureService>();
			Assert.AreEqual(service1, service2);
		}
	}
}