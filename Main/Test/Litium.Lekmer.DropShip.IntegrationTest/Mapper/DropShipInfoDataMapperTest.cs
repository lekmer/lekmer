﻿using System.Data;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.DropShip.IntegrationTest.Mapper
{
	[TestFixture]
	public class DropShipInfoDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void DropShipInfo_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IDropShipInfo>(_dataReader);
			Assert.IsInstanceOf<DropShipInfoDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void DropShipInfo_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IDropShipInfo>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IDropShipInfo>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}