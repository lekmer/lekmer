﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Task
{
	[TestFixture]
	public class DropShipImportTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipImport_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<ITask>("DropShipImport");

			Assert.IsInstanceOf<ITask>(exporter);
			Assert.IsInstanceOf<DropShipImport>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void DropShipImport_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<ITask>("DropShipImport");
			var exporter2 = IoC.Resolve<ITask>("DropShipImport");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}