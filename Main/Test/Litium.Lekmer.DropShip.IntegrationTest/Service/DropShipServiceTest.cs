﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DropShip.IntegrationTest.Service
{
	[TestFixture]
	public class DropShipServiceTest
	{
		[Test]
		[Category("IoC")]
		public void DropShipService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IDropShipService>();

			Assert.IsInstanceOf<IDropShipService>(service);
			Assert.IsInstanceOf<DropShipService>(service);
		}

		[Test]
		[Category("IoC")]
		public void DropShipService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IDropShipService>();
			var service2 = IoC.Resolve<IDropShipService>();
			Assert.AreEqual(service1, service2);
		}
	}
}