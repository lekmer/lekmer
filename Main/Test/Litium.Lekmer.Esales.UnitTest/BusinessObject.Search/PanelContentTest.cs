using System.Collections.ObjectModel;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.UnitTest.BusinessObject.Search
{
	[TestFixture]
	public class PanelContentTest
	{
		[Test]
		public void FindPanel_PanelL3N1_PanelReturned_()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent actualPanel = panel.FindPanel("PanelL3N1");

			Assert.IsNotNull(actualPanel);
			Assert.AreEqual("PanelL3N1", actualPanel.Name);
		}

		[Test]
		public void FindPanel_PanelL3N4_PanelReturned_()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent actualPanel = panel.FindPanel("PanelL3N4");

			Assert.IsNotNull(actualPanel);
			Assert.AreEqual("PanelL3N4", actualPanel.Name);
		}

		[Test]
		public void FindPanel_PanelL2N1_PanelReturned_()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent actualPanel = panel.FindPanel("PanelL2N1");

			Assert.IsNotNull(actualPanel);
			Assert.AreEqual("PanelL2N1", actualPanel.Name);
		}

		[Test]
		public void FindPanel_ZoneL2N2_ZoneReturned_()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent actualPanel = panel.FindPanel("ZoneL2N2");

			Assert.IsNotNull(actualPanel);
			Assert.AreEqual("ZoneL2N2", actualPanel.Name);
		}

		[Test]
		public void FindPanel_PanelNotExist_NullReturned_()
		{
			IPanelContent panel = CreateDummyPanelTree();

			Assert.IsNull(panel.FindPanel("PanelNotExist"));
		}

		[Test]
		public void FindPanel_EmptyPanelName_NullReturned_()
		{
			IPanelContent panel = CreateDummyPanelTree();

			Assert.IsNull(panel.FindPanel(string.Empty));
			Assert.IsNull(panel.FindPanel(null));
		}

		[Test]
		public void FindResult_UseZone_FirstPanelResultReturned()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent zone = panel.FindPanel("ZoneL2N2");

			var actualResult = zone.FindResult(EsalesResultType.Products) as IProductHits;

			Assert.IsNotNull(actualResult);
			Assert.AreSame(zone.SubPanels[0].Result, actualResult);
		}

		[Test]
		public void FindResult_UsePanel_PanelResultReturned()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent subPanel = panel.FindPanel("PanelL3N4");

			var actualResult = subPanel.FindResult(EsalesResultType.Products) as IProductHits;

			Assert.IsNotNull(actualResult);
			Assert.AreSame(subPanel.Result, actualResult);
		}

		[Test]
		public void FindResults_UseTree_AllResultsReturned()
		{
			IPanelContent panel = CreateDummyPanelTree();

			var actualResults = panel.FindResults(EsalesResultType.Products);

			Assert.IsNotNull(actualResults);
			Assert.AreEqual(5, actualResults.Count);
		}

		[Test]
		public void FindResults_UseZone_AllZoneResultsReturned()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent zone = panel.FindPanel("ZoneL2N2");

			var actualResults = zone.FindResults(EsalesResultType.Products);

			Assert.IsNotNull(actualResults);
			Assert.AreEqual(2, actualResults.Count);
		}

		[Test]
		public void FindResults_UsePanel_AllPanelResultsReturned()
		{
			IPanelContent panel = CreateDummyPanelTree();

			IPanelContent subPanel = panel.FindPanel("PanelL2N1");

			var actualResults = subPanel.FindResults(EsalesResultType.Products);

			Assert.IsNotNull(actualResults);
			Assert.AreEqual(1, actualResults.Count);
		}

		// Helpers

		private IPanelContent CreateDummyPanelTree()
		{
			IPanelContent zoneRoot = DummyZone("ZoneRoot");
			IPanelContent zoneL2N1 = DummyZone("ZoneL2N1");
			IPanelContent zoneL2N2 = DummyZone("ZoneL2N2");

			IPanelContent panelL3N1 = DummyPanel("PanelL3N1", DummyResult(EsalesResultType.Products));
			IPanelContent panelL3N2 = DummyPanel("PanelL3N2", DummyResult(EsalesResultType.Products));
			IPanelContent panelL3N3 = DummyPanel("PanelL3N3", DummyResult(EsalesResultType.Products));
			IPanelContent panelL3N4 = DummyPanel("PanelL3N4", DummyResult(EsalesResultType.Products));
			IPanelContent panelL2N1 = DummyPanel("PanelL2N1", DummyResult(EsalesResultType.Products));

			zoneRoot.SubPanels.Add(zoneL2N1);
			zoneRoot.SubPanels.Add(zoneL2N2);
			zoneRoot.SubPanels.Add(panelL2N1);

			zoneL2N1.SubPanels.Add(panelL3N1);
			zoneL2N1.SubPanels.Add(panelL3N2);
			zoneL2N2.SubPanels.Add(panelL3N3);
			zoneL2N2.SubPanels.Add(panelL3N4);

			return zoneRoot;
		}

		private IPanelContent DummyZone(string name)
		{
			return new PanelContent { Name = name, IsZone = true, SubPanels = new Collection<IPanelContent>() };
		}

		private IPanelContent DummyPanel(string name, IResult result)
		{
			return new PanelContent { Name = name, HasRezult = true, Result = result, SubPanels = new Collection<IPanelContent>() };
		}

		private IResult DummyResult(EsalesResultType resultType)
		{
			if (resultType == EsalesResultType.Products)
			{
				return new ProductHits {ProductsInfo = new Collection<IProductHitInfo> {new ProductHitInfo {Rank = 1, Key = "123456", Ticket = "ticket"}}};
			}

			return null;
		}
	}
}