﻿using Litium.Lekmer.Payment.Collector.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Setting
{
	[TestFixture]
	public class CollectorSettingTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICollectorSetting>();

			Assert.IsInstanceOf<ICollectorSetting>(service);
			Assert.IsInstanceOf<ICollectorSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorSetting_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<ICollectorSetting>();
			var service2 = IoC.Resolve<ICollectorSetting>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}