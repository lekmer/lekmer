﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Service
{
	[TestFixture]
	public class CollectorPaymentTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorPaymentTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICollectorPaymentTypeService>();

			Assert.IsInstanceOf<ICollectorPaymentTypeService>(service);
			Assert.IsInstanceOf<CollectorPaymentTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorPaymentTypeService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICollectorPaymentTypeService>();
			var service2 = IoC.Resolve<ICollectorPaymentTypeService>();

			Assert.AreEqual(service1, service2);
		}
	}
}