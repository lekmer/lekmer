﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerMediaItemServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerMediaItemService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMediaItemService>();

			Assert.IsInstanceOf<IMediaItemService>(service);
			Assert.IsInstanceOf<ILekmerMediaItemService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerMediaItemService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IMediaItemService>();
			var service2 = IoC.Resolve<IMediaItemService>();

			Assert.AreEqual(service1, service2);
		}
	}
}