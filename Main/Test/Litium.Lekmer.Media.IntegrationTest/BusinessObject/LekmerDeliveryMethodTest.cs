﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using NUnit.Framework;

namespace Litium.Lekmer.Media.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerImageSizeTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerImageSize_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IImageSize>();

			Assert.IsInstanceOf<IImageSize>(instance);
			Assert.IsInstanceOf<ILekmerImageSize>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerImageSize_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IImageSize>();
			var instance2 = IoC.Resolve<IImageSize>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}
