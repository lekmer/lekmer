﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerDeliveryMethodRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<DeliveryMethodRepository>();

			Assert.IsInstanceOf<DeliveryMethodRepository>(repository);
			Assert.IsInstanceOf<LekmerDeliveryMethodRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodRepository_ResolveLekmer_Resolved()
		{
			var repository = IoC.Resolve<LekmerDeliveryMethodRepository>();

			Assert.IsInstanceOf<DeliveryMethodRepository>(repository);
			Assert.IsInstanceOf<LekmerDeliveryMethodRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<DeliveryMethodRepository>();
			var repository2 = IoC.Resolve<DeliveryMethodRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}