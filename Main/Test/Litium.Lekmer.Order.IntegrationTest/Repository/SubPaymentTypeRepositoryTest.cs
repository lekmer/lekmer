﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class SubPaymentTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SubPaymentTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SubPaymentTypeRepository>();
			Assert.IsInstanceOf<SubPaymentTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SubPaymentTypeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SubPaymentTypeRepository>();
			var instance2 = IoC.Resolve<SubPaymentTypeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}