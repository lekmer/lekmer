﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderAddressRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderAddressRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderAddressRepository>();

			Assert.IsInstanceOf<OrderAddressRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderAddressRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderAddressRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<OrderAddressRepository>();
			var repository2 = IoC.Resolve<OrderAddressRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}