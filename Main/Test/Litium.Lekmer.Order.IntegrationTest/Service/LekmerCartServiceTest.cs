﻿using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class LekmerCartServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;
		private static readonly int CURRENCY_ID = 1;
		private static readonly string CHANNEL_CommonName = "Sweden";

		private static readonly int CART_ID_INEXISTED = 999999;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;
		private ICurrency _currency;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_currency = _mocker.Stub<ICurrency>();
			_currency.Id = CURRENCY_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.CommonName = CHANNEL_CommonName;
			_channel.Language = _language;
			_channel.Currency = _currency;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartService>();

			Assert.IsInstanceOf<ICartService>(service);
			Assert.IsInstanceOf<CartService>(service);
			Assert.IsInstanceOf<LekmerCartService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartService_LekmerResolve_Resolved()
		{
			var service = IoC.Resolve<ILekmerCartService>();

			Assert.IsInstanceOf<ICartService>(service);
			Assert.IsInstanceOf<CartService>(service);
			Assert.IsInstanceOf<LekmerCartService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICartService>();
			var service2 = IoC.Resolve<ICartService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("DB")]
		public void GetById_ExecuteWithInexistedId_NullIsReturned()
		{
			var cartService = CreateCartService();

			_mocker.ReplayAll();

			var cart = cartService.GetById(_userContext, CART_ID_INEXISTED);

			Assert.IsNull(cart);
		}

		private ILekmerCartService CreateCartService()
		{
			var lekmerCartSession = _mocker.Stub<ILekmerCartSession>(); //Require HttpContext
			var cartCampaignProcessor = _mocker.Stub<ICartCampaignProcessor>(); // We wan't test cart campaigns here, also it has voucher session dependency from web.ioc
			var cartCookieService = _mocker.Stub<ICartCookieService>(); //Require HttpContext
			var cartSetting = _mocker.Stub<ILekmerCartSetting>();

			var cartService = _mocker.PartialMock<LekmerCartService>(
				IoC.Resolve<CartRepository>(),
				IoC.Resolve<ICartItemService>(),
				cartCampaignProcessor,
				IoC.Resolve<IProductService>(),
				lekmerCartSession,
				cartCookieService,
				cartSetting
			);

			return cartService;
		}
	}
}