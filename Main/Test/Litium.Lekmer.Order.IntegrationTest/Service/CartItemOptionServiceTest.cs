﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class CartItemOptionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemOptionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartItemOptionService>();

			Assert.IsInstanceOf<ICartItemOptionService>(service);
			Assert.IsInstanceOf<CartItemOptionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartItemOptionService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartItemOptionService>();
			var instance2 = IoC.Resolve<ICartItemOptionService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}