﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class TrustPilotServiceTest
	{
		[Test]
		[Category("IoC")]
		public void TrustPilotService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITrustPilotService>();

			Assert.IsInstanceOf<ITrustPilotService>(service);
			Assert.IsInstanceOf<TrustPilotService>(service);
		}

		[Test]
		[Category("IoC")]
		public void TrustPilotService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ITrustPilotService>();
			var service2 = IoC.Resolve<ITrustPilotService>();

			Assert.AreEqual(service1, service2);
		}
	}
}