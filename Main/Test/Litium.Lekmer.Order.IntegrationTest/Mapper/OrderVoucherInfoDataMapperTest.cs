﻿using System.Data;
using Litium.Lekmer.Order.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Mapper
{
	[TestFixture]
	public class OrderVoucherInfoDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IOrderVoucherInfo>(_dataReader);

			Assert.IsInstanceOf<OrderVoucherInfoDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IOrderVoucherInfo>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IOrderVoucherInfo>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}