﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerOrderAuditSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderAuditSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderAuditSecureService>();

			Assert.IsInstanceOf<IOrderAuditSecureService>(service);
			Assert.IsInstanceOf<OrderAuditSecureService>(service);
			Assert.IsInstanceOf<LekmerOrderAuditSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderAuditSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderAuditSecureService>();
			var service2 = IoC.Resolve<IOrderAuditSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}