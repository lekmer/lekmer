﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerPaymentTypeSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerPaymentTypeSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPaymentTypeSecureService>();

			Assert.IsInstanceOf<IPaymentTypeSecureService>(service);
			Assert.IsInstanceOf<PaymentTypeSecureService>(service);
			Assert.IsInstanceOf<LekmerPaymentTypeSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerPaymentTypeSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IPaymentTypeSecureService>();
			var service2 = IoC.Resolve<IPaymentTypeSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}