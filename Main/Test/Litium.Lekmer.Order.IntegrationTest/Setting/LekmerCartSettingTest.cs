﻿using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Setting
{
	[TestFixture]
	public class LekmerCartSettingTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<ILekmerCartSetting>();

			Assert.IsInstanceOf<ILekmerCartSetting>(service);
			Assert.IsInstanceOf<LekmerCartSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<ILekmerCartSetting>();
			var setting2 = IoC.Resolve<ILekmerCartSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}