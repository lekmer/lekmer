﻿using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.UnitTest.Extensions
{
	[TestFixture]
	public class ProductUrlExtensionsTest
	{
		private static MockRepository _mocker;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void IsSameAs_SameProductURLs_ComparedFine()
		{
			var productUrl1 = _mocker.Stub<IProductUrl>();
			var productUrl2 = _mocker.Stub<IProductUrl>();

			productUrl1.LanguageId = 1;
			productUrl2.LanguageId = 1;

			productUrl1.UrlTitle = "The same URL";
			productUrl2.UrlTitle = "The same URL";

			Assert.IsTrue(productUrl1.IsSameAs(productUrl2), "IsSameAs");
		}

		[Test]
		public void IsSameAs_DifferentLanguageId_ComparedFine()
		{
			var productUrl1 = _mocker.Stub<IProductUrl>();
			var productUrl2 = _mocker.Stub<IProductUrl>();

			productUrl1.LanguageId = 1;
			productUrl2.LanguageId = 2;

			productUrl1.UrlTitle = "The same URL";
			productUrl2.UrlTitle = "The same URL";

			Assert.IsFalse(productUrl1.IsSameAs(productUrl2), "IsSameAs");
		}

		[Test]
		public void IsSameAs_DifferentUrlTitle_ComparedFine()
		{
			var productUrl1 = _mocker.Stub<IProductUrl>();
			var productUrl2 = _mocker.Stub<IProductUrl>();

			productUrl1.LanguageId = 1;
			productUrl2.LanguageId = 1;

			productUrl1.UrlTitle = "The URL 1";
			productUrl2.UrlTitle = "The URL 2";

			Assert.IsFalse(productUrl1.IsSameAs(productUrl2), "IsSameAs");
		}

		[Test]
		public void IsSameAs_SameProductURLandHistory_ComparedFine()
		{
			var productUrl1 = _mocker.Stub<IProductUrl>();
			var productUrl2 = _mocker.Stub<IProductUrlHistory>();

			productUrl1.LanguageId = 1;
			productUrl2.LanguageId = 1;

			productUrl1.UrlTitle = "The same URL";
			productUrl2.UrlTitleOld = "The same URL";

			Assert.IsTrue(productUrl1.IsSameAs(productUrl2), "IsSameAs");
		}

		[Test]
		public void IsSameAs_DifferentHistoryLanguageId_ComparedFine()
		{
			var productUrl1 = _mocker.Stub<IProductUrl>();
			var productUrl2 = _mocker.Stub<IProductUrlHistory>();

			productUrl1.LanguageId = 1;
			productUrl2.LanguageId = 2;

			productUrl1.UrlTitle = "The same URL";
			productUrl2.UrlTitleOld = "The same URL";

			Assert.IsFalse(productUrl1.IsSameAs(productUrl2), "IsSameAs");
		}

		[Test]
		public void IsSameAs_DifferentHistoryUrlTitle_ComparedFine()
		{
			var productUrl1 = _mocker.Stub<IProductUrl>();
			var productUrl2 = _mocker.Stub<IProductUrlHistory>();

			productUrl1.LanguageId = 1;
			productUrl2.LanguageId = 1;

			productUrl1.UrlTitle = "The URL 1";
			productUrl2.UrlTitleOld = "The URL 2";

			Assert.IsFalse(productUrl1.IsSameAs(productUrl2), "IsSameAs");
		}
	}
}