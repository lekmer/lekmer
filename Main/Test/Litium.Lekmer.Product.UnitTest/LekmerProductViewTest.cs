﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.UnitTest
{
	[TestFixture]
	public class LekmerProductViewTest
	{
		[Test]
		public void LekmerProductView_TotalNumberInStock_WithoutSizes()
		{
			const int numberInStock = 10;

			ILekmerProductView lekmerProductView = new LekmerProductView();

			lekmerProductView.HasSizes = false;
			lekmerProductView.NumberInStock = numberInStock;

			Assert.IsTrue(numberInStock == lekmerProductView.TotalNumberInStock);
		}

		[Test]
		public void LekmerProductView_TotalNumberInStock_WithSizes()
		{
			const int numberInStock = 10;
			int totalNumberInStock = 0;

			ILekmerProductView lekmerProductView = new LekmerProductView();
			lekmerProductView.HasSizes = true;
			lekmerProductView.ProductSizes = new Collection<IProductSize>();

			for (int index = 1; index < 10; index++ )
			{
				totalNumberInStock += index;

				var productSize = IoC.Resolve<IProductSize>();
				productSize.NumberInStock = index;
				lekmerProductView.ProductSizes.Add(productSize);
			}

			lekmerProductView.NumberInStock = numberInStock;

			Assert.IsFalse(numberInStock == lekmerProductView.TotalNumberInStock);
			Assert.IsTrue(totalNumberInStock == lekmerProductView.TotalNumberInStock);
		}
	}
}