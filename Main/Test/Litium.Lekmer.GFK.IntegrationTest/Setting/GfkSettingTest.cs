﻿using Litium.Lekmer.GFK.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GFK.IntegrationTest.Setting
{
	[TestFixture]
	public class GfkSettingTest
	{
		[Test]
		[Category("IoC")]
		public void GfkSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<IGfkSetting>();
			Assert.IsInstanceOf<IGfkSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void GfkSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<IGfkSetting>();
			var setting2 = IoC.Resolve<IGfkSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}