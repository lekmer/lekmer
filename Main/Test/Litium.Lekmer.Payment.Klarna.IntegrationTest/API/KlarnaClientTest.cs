﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.API
{
	[TestFixture]
	public class KlarnaClientTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaClient_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaClient>();

			Assert.IsInstanceOf<IKlarnaClient>(service);
			Assert.IsInstanceOf<KlarnaClient>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaClient_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IKlarnaClient>();
			var service2 = IoC.Resolve<IKlarnaClient>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}