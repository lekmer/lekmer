﻿using Litium.Lekmer.Payment.Klarna.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Repository
{
	[TestFixture]
	public class KlarnaTransactionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaTransactionRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<KlarnaTransactionRepository>();

			Assert.IsInstanceOf<KlarnaTransactionRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaTransactionRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<KlarnaTransactionRepository>();
			var service2 = IoC.Resolve<KlarnaTransactionRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}