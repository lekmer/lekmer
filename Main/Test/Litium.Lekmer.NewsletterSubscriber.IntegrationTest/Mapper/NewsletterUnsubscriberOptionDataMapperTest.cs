﻿using System.Data;
using Litium.Lekmer.NewsletterSubscriber.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Mapper
{
	[TestFixture]
	public class NewsletterUnsubscriberOptionDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOption_Resolve_Resolved()
		{
			var instance = DataMapperResolver.Resolve<INewsletterUnsubscriberOption>(_dataReader);

			Assert.IsInstanceOf<NewsletterUnsubscriberOptionDataMapper>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOption_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<INewsletterUnsubscriberOption>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<INewsletterUnsubscriberOption>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}