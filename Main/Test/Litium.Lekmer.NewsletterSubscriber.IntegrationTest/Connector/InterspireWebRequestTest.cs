﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Connector
{
	[TestFixture]
	public class InterspireWebRequestTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireWebRequest_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireWebRequest>();

			Assert.IsInstanceOf<IInterspireWebRequest>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireWebRequest_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IInterspireWebRequest>();
			var instance2 = IoC.Resolve<IInterspireWebRequest>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}