﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class NewsletterUnsubscriberOptionTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOption_Resolve_Resolved()
		{
			var instance = IoC.Resolve<INewsletterUnsubscriberOption>();

			Assert.IsInstanceOf<INewsletterUnsubscriberOption>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOption_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<INewsletterUnsubscriberOption>();
			var instance2 = IoC.Resolve<INewsletterUnsubscriberOption>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}