﻿using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.InsuranceInfo.Integrationtest.Exporter
{
	[TestFixture]
	public class InsuranceInfoExporterTest
	{
		[Test]
		[Category("IoC")]
		public void InsuranceInfoExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("InsuranceInfoExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<InsuranceInfoExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void InsuranceInfoExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("InsuranceInfoExporter");
			var exporter2 = IoC.Resolve<IExporter>("InsuranceInfoExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}