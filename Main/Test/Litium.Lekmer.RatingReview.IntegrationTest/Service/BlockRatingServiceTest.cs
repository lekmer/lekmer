﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockRatingServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockRatingService>();

			Assert.IsInstanceOf<IBlockRatingService>(service);
			Assert.IsInstanceOf<BlockRatingService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockRatingService>();
			var instance2 = IoC.Resolve<IBlockRatingService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}