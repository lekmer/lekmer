﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingItemProductScoreServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingItemProductScoreService>();

			Assert.IsInstanceOf<IRatingItemProductScoreService>(service);
			Assert.IsInstanceOf<RatingItemProductScoreService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductScoreService>();
			var instance2 = IoC.Resolve<IRatingItemProductScoreService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}