﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockLatestFeedbackListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockLatestFeedbackListService>();

			Assert.IsInstanceOf<IBlockLatestFeedbackListService>(service);
			Assert.IsInstanceOf<BlockLatestFeedbackListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockLatestFeedbackListService>();
			var instance2 = IoC.Resolve<IBlockLatestFeedbackListService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}