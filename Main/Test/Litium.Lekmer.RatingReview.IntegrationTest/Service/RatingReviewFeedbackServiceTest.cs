﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingReviewFeedbackServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewFeedbackService>();

			Assert.IsInstanceOf<IRatingReviewFeedbackService>(service);
			Assert.IsInstanceOf<RatingReviewFeedbackService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewFeedbackService>();
			var instance2 = IoC.Resolve<IRatingReviewFeedbackService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}