﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockLatestFeedbackListBrandServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockLatestFeedbackListBrandService>();

			Assert.IsInstanceOf<IBlockLatestFeedbackListBrandService>(service);
			Assert.IsInstanceOf<BlockLatestFeedbackListBrandService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockLatestFeedbackListBrandService>();
			var instance2 = IoC.Resolve<IBlockLatestFeedbackListBrandService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}