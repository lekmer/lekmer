﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductRatingSummaryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRatingSummary_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductRatingSummaryInfo>();

			Assert.IsInstanceOf<IProductRatingSummaryInfo>(instance);
			Assert.IsInstanceOf<IProductRatingSummaryInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductRatingSummary_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductRatingSummaryInfo>();
			var instance2 = IoC.Resolve<IProductRatingSummaryInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}