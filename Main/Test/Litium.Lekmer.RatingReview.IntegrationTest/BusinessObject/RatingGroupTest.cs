﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingGroupTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroup_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingGroup>();

			Assert.IsInstanceOf<IRatingGroup>(instance);
			Assert.IsInstanceOf<IRatingGroup>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroup_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroup>();
			var instance2 = IoC.Resolve<IRatingGroup>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}