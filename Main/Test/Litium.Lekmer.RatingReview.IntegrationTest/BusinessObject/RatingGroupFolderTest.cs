﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingGroupFolderTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupFolder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingGroupFolder>();

			Assert.IsInstanceOf<IRatingGroupFolder>(instance);
			Assert.IsInstanceOf<IRatingGroupFolder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupFolder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupFolder>();
			var instance2 = IoC.Resolve<IRatingGroupFolder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}