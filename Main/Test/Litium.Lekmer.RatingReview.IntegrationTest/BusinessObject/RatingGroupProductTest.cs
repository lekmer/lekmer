﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingGroupProductTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupProduct_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingGroupProduct>();

			Assert.IsInstanceOf<IRatingGroupProduct>(instance);
			Assert.IsInstanceOf<IRatingGroupProduct>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupProduct_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupProduct>();
			var instance2 = IoC.Resolve<IRatingGroupProduct>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}