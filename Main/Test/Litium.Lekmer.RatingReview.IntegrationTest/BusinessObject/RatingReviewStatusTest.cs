﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingReviewStatusTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewStatus_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingReviewStatus>();

			Assert.IsInstanceOf<IRatingReviewStatus>(instance);
			Assert.IsInstanceOf<IRatingReviewStatus>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewStatus_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewStatus>();
			var instance2 = IoC.Resolve<IRatingReviewStatus>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}