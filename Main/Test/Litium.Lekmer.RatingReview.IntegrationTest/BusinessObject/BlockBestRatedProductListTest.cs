﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockBestRatedProductListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductList_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockBestRatedProductList>();

			Assert.IsInstanceOf<IBlockBestRatedProductList>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductList_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockBestRatedProductList>();
			var block2 = IoC.Resolve<IBlockBestRatedProductList>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}