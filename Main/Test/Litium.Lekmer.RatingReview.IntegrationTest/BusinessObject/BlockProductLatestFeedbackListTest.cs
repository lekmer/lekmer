﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductLatestFeedbackListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackList_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockProductLatestFeedbackList>();

			Assert.IsInstanceOf<IBlockProductLatestFeedbackList>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackList_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockProductLatestFeedbackList>();
			var block2 = IoC.Resolve<IBlockProductLatestFeedbackList>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}