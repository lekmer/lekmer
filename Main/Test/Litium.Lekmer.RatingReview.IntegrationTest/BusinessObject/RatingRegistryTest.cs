﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingRegistryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingRegistry_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingRegistry>();

			Assert.IsInstanceOf<IRatingRegistry>(instance);
			Assert.IsInstanceOf<RatingRegistry>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingRegistry_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingRegistry>();
			var instance2 = IoC.Resolve<IRatingRegistry>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}