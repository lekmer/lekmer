﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingReviewUserSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewUserSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewUserSecureService>();

			Assert.IsInstanceOf<IRatingReviewUserSecureService>(service);
			Assert.IsInstanceOf<RatingReviewUserSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewUserSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewUserSecureService>();
			var instance2 = IoC.Resolve<IRatingReviewUserSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}