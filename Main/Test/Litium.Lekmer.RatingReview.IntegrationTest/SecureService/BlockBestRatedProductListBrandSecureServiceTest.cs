﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockBestRatedProductListBrandSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandSecureService_Resolve_Resolved()
		{
			IBlockBestRatedProductListBrandSecureService service = IoC.Resolve<IBlockBestRatedProductListBrandSecureService>();

			Assert.IsInstanceOf<IBlockBestRatedProductListBrandSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandSecureService_ResolveTwice_SameObjects()
		{
			IBlockBestRatedProductListBrandSecureService service1 = IoC.Resolve<IBlockBestRatedProductListBrandSecureService>();
			IBlockBestRatedProductListBrandSecureService service2 = IoC.Resolve<IBlockBestRatedProductListBrandSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}