﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockRatingItemSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingItemSecureService_Resolve_Resolved()
		{
			IBlockRatingItemSecureService service = IoC.Resolve<IBlockRatingItemSecureService>();

			Assert.IsInstanceOf<IBlockRatingItemSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingItemSecureService_ResolveTwice_SameObjects()
		{
			IBlockRatingItemSecureService service1 = IoC.Resolve<IBlockRatingItemSecureService>();
			IBlockRatingItemSecureService service2 = IoC.Resolve<IBlockRatingItemSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}