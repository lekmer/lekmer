﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductFeedbackSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackSecureService_Resolve_Resolved()
		{
			IBlockProductFeedbackSecureService service = IoC.Resolve<IBlockProductFeedbackSecureService>();

			Assert.IsInstanceOf<IBlockProductFeedbackSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackSecureService_ResolveTwice_SameObjects()
		{
			IBlockProductFeedbackSecureService service1 = IoC.Resolve<IBlockProductFeedbackSecureService>();
			IBlockProductFeedbackSecureService service2 = IoC.Resolve<IBlockProductFeedbackSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}