﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingReviewStatusSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewStatusSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewStatusSecureService>();

			Assert.IsInstanceOf<IRatingReviewStatusSecureService>(service);
			Assert.IsInstanceOf<RatingReviewStatusSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewStatusSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewStatusSecureService>();
			var instance2 = IoC.Resolve<IRatingReviewStatusSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}