﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockRatingGroupSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingGroupSecureService_Resolve_Resolved()
		{
			IBlockRatingGroupSecureService service = IoC.Resolve<IBlockRatingGroupSecureService>();

			Assert.IsInstanceOf<IBlockRatingGroupSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingGroupSecureService_ResolveTwice_SameObjects()
		{
			IBlockRatingGroupSecureService service1 = IoC.Resolve<IBlockRatingGroupSecureService>();
			IBlockRatingGroupSecureService service2 = IoC.Resolve<IBlockRatingGroupSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}