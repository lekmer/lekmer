﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockProductRatingSummarySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRatingSummarySecureService_Resolve_Resolved()
		{
			IBlockProductRatingSummarySecureService service = IoC.Resolve<IBlockProductRatingSummarySecureService>();

			Assert.IsInstanceOf<IBlockProductRatingSummarySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRatingSummarySecureService_ResolveTwice_SameObjects()
		{
			IBlockProductRatingSummarySecureService service1 = IoC.Resolve<IBlockProductRatingSummarySecureService>();
			IBlockProductRatingSummarySecureService service2 = IoC.Resolve<IBlockProductRatingSummarySecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}