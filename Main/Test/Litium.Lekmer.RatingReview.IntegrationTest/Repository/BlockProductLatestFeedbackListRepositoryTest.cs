﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductLatestFeedbackListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListRepository_Resolve_Resolved()
		{
			BlockProductLatestFeedbackListRepository repository = IoC.Resolve<BlockProductLatestFeedbackListRepository>();

			Assert.IsInstanceOf<BlockProductLatestFeedbackListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListRepository_ResolveTwice_SameObjects()
		{
			BlockProductLatestFeedbackListRepository repository1 = IoC.Resolve<BlockProductLatestFeedbackListRepository>();
			BlockProductLatestFeedbackListRepository repository2 = IoC.Resolve<BlockProductLatestFeedbackListRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}