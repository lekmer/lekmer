﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingReviewFeedbackLikeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackLikeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingReviewFeedbackLikeRepository>();

			Assert.IsInstanceOf<RatingReviewFeedbackLikeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackLikeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingReviewFeedbackLikeRepository>();
			var instance2 = IoC.Resolve<RatingReviewFeedbackLikeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}