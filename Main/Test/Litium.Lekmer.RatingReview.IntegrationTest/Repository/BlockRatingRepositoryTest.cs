﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockRatingRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingRepository_Resolve_Resolved()
		{
			BlockRatingRepository blockRatingRepository = IoC.Resolve<BlockRatingRepository>();

			Assert.IsInstanceOf<BlockRatingRepository>(blockRatingRepository);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingRepository_ResolveTwice_SameObjects()
		{
			BlockRatingRepository blockRatingRepository1 = IoC.Resolve<BlockRatingRepository>();
			BlockRatingRepository blockRatingRepository2 = IoC.Resolve<BlockRatingRepository>();

			Assert.AreEqual(blockRatingRepository1, blockRatingRepository2);
		}
	}
}