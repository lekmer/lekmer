﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockLatestFeedbackListBrandRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandRepository_Resolve_Resolved()
		{
			BlockLatestFeedbackListBrandRepository repository = IoC.Resolve<BlockLatestFeedbackListBrandRepository>();

			Assert.IsInstanceOf<BlockLatestFeedbackListBrandRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandRepository_ResolveTwice_SameObjects()
		{
			BlockLatestFeedbackListBrandRepository repository1 = IoC.Resolve<BlockLatestFeedbackListBrandRepository>();
			BlockLatestFeedbackListBrandRepository repository2 = IoC.Resolve<BlockLatestFeedbackListBrandRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}