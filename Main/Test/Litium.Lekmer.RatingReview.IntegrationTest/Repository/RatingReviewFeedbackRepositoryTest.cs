﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingReviewFeedbackRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingReviewFeedbackRepository>();

			Assert.IsInstanceOf<RatingReviewFeedbackRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingReviewFeedbackRepository>();
			var instance2 = IoC.Resolve<RatingReviewFeedbackRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}