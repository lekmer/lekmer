﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingReviewStatusRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewStatusRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingReviewStatusRepository>();

			Assert.IsInstanceOf<RatingReviewStatusRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewStatusRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingReviewStatusRepository>();
			var instance2 = IoC.Resolve<RatingReviewStatusRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}