﻿using System.Data;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class RatingItemDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void RatingItemDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IRatingItem>(_dataReader);

			Assert.IsInstanceOf<RatingItemDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IRatingItem>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IRatingItem>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}