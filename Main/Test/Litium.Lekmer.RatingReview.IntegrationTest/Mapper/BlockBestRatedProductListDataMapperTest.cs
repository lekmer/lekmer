﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockBestRatedProductListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockBestRatedProductList> dataMapper = DataMapperResolver.Resolve<IBlockBestRatedProductList>(_dataReader);

			Assert.IsInstanceOf<BlockBestRatedProductListDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockBestRatedProductList> dataMapper1 = DataMapperResolver.Resolve<IBlockBestRatedProductList>(_dataReader);
			DataMapperBase<IBlockBestRatedProductList> dataMapper2 = DataMapperResolver.Resolve<IBlockBestRatedProductList>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}