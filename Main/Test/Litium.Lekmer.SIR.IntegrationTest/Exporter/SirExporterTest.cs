﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.Exporter
{
	[TestFixture]
	public class SirExporterTest
	{
		[Test]
		[Category("IoC")]
		public void SirExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("SirExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<SirExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void SirExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("SirExporter");
			var exporter2 = IoC.Resolve<IExporter>("SirExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}