﻿using System.Data;
using Litium.Lekmer.SIR.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SIR.IntegrationTest.Mapper
{
	[TestFixture]
	public class SirBatchDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SirBatch_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ISirBatch>(_dataReader);

			Assert.IsInstanceOf<SirBatchDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void SirBatchr_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ISirBatch>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ISirBatch>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}