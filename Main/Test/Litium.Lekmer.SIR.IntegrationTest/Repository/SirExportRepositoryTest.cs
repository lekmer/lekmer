﻿using Litium.Lekmer.SIR.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.Repository
{
	[TestFixture]
	public class SirExportRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SirExportRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SirExportRepository>();

			Assert.IsInstanceOf<SirExportRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SirExportRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<SirExportRepository>();
			var repository2 = IoC.Resolve<SirExportRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}