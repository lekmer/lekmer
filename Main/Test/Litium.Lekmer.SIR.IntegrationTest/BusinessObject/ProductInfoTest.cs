using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductInfoTest
	{
		[Test]
		[Category("IoC")]
		public void ProductInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IProductInfo>();
			Assert.IsInstanceOf<IProductInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void ProductInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IProductInfo>();
			var info2 = IoC.Resolve<IProductInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}