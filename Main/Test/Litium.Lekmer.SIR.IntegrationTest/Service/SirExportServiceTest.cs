﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.Service
{
	[TestFixture]
	public class SirExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SirExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISirExportService>();

			Assert.IsInstanceOf<ISirExportService>(service);
			Assert.IsInstanceOf<SirExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SirExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISirExportService>();
			var service2 = IoC.Resolve<ISirExportService>();
			Assert.AreEqual(service1, service2);
		}
	}
}