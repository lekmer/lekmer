﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.Web.IntegrationTest
{
	[TestFixture]
	public class BlockBrandTopListEntityMapperTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListEntityMapper_Resolve_Resolved()
		{
			var fragment = new Fragment("FragmentContent");

			var block = IoC.Resolve<IBlockBrandTopList>();
			block.Setting = IoC.Resolve<IBlockSetting>();

			// Here we testing if BlockBrandTopListEntityMapper is resolved correctly.
			fragment.AddEntity(block);

			fragment.Render();
		}
	}
}