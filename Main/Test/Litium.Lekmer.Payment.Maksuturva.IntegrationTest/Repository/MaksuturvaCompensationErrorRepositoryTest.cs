﻿using Litium.Lekmer.Payment.Maksuturva.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Repository
{
	[TestFixture]
	public class MaksuturvaCompensationErrorRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationErrorRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<MaksuturvaCompensationErrorRepository>();

			Assert.IsInstanceOf<MaksuturvaCompensationErrorRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationErrorRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<MaksuturvaCompensationErrorRepository>();
			var service2 = IoC.Resolve<MaksuturvaCompensationErrorRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}