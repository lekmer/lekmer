﻿using Litium.Lekmer.Payment.Maksuturva.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.Repository
{
	[TestFixture]
	public class MaksuturvaPendingOrderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrderRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<MaksuturvaPendingOrderRepository>();

			Assert.IsInstanceOf<MaksuturvaPendingOrderRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrderRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<MaksuturvaPendingOrderRepository>();
			var service2 = IoC.Resolve<MaksuturvaPendingOrderRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}