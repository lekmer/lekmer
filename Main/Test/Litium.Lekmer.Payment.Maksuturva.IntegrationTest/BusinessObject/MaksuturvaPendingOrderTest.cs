﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class MaksuturvaPendingOrderTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrder_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IMaksuturvaPendingOrder>();

			Assert.IsInstanceOf<IMaksuturvaPendingOrder>(instance);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaPendingOrder_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IMaksuturvaPendingOrder>();
			var instance2 = IoC.Resolve<IMaksuturvaPendingOrder>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}