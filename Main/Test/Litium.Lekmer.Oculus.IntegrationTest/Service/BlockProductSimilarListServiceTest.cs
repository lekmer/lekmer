﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductSimilarListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSimilarListService_Resolve_Resolved()
		{
			IBlockProductSimilarListService blockProductSimilarListService = IoC.Resolve<IBlockProductSimilarListService>();

			Assert.IsInstanceOf<IBlockProductSimilarListService>(blockProductSimilarListService);
		}

		[Test]
		[Category("IoC")]
		public void ProductSimilarService_ResolveTwice_SameObjects()
		{
			IBlockProductSimilarListService blockProductSimilarListService1 = IoC.Resolve<IBlockProductSimilarListService>();
			IBlockProductSimilarListService blockProductSimilarListService2 = IoC.Resolve<IBlockProductSimilarListService>();

			Assert.AreEqual(blockProductSimilarListService1, blockProductSimilarListService2);
		}
	}
}