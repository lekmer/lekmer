﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Oculus.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Oculus.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockProductSimilarListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSimilarListDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockProductSimilarList> blockProductSimilarListDataMapper = DataMapperResolver.Resolve<IBlockProductSimilarList>(_dataReader);

			Assert.IsInstanceOf<BlockProductSimilarListDataMapper>(blockProductSimilarListDataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSimilarListDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockProductSimilarList> blockProductSimilarListDataMapper1 = DataMapperResolver.Resolve<IBlockProductSimilarList>(_dataReader);
			DataMapperBase<IBlockProductSimilarList> blockProductSimilarListDataMapper2 = DataMapperResolver.Resolve<IBlockProductSimilarList>(_dataReader);

			Assert.AreNotEqual(blockProductSimilarListDataMapper1, blockProductSimilarListDataMapper2);
		}
	}
}