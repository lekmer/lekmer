﻿using System.Globalization;
using Litium.Scensum.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Core.UnitTest.Util
{
	[TestFixture]
	public class LekmerFormatterTest
	{
		private MockRepository _mocker;
		private IChannel _channel;
		private ICurrency _currency;
		private CultureInfo _cultureInfo;
		private ILekmerFormatter _formatter;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_currency = _mocker.Stub<ICurrency>();
			_currency.PriceFormat = "{0} kr";
			_currency.NumberOfDecimals = 1;

			_cultureInfo = new CultureInfo("sv-SE");

			_channel = _mocker.Stub<IChannel>();
			_channel.Currency = _currency;
			_channel.Expect(c => c.CultureInfo).Repeat.Any().Return(_cultureInfo);

			_formatter = new LekmerFormatter();

			_mocker.ReplayAll();
		}


		[Test]
		public void FormatPriceTwoOrLessDecimals_IntegerPrice_CorectlyFormatted()
		{
			string formattedPrice = _formatter.FormatPriceTwoOrLessDecimals(_channel, 123m);

			Assert.AreEqual("123 kr", formattedPrice);
		}

		[Test]
		public void FormatPriceTwoOrLessDecimals_DecimalPrice_CorectlyFormatted()
		{
			string formattedPrice = _formatter.FormatPriceTwoOrLessDecimals(_channel, 123.45m);

			Assert.AreEqual("123,45 kr", formattedPrice);
		}

		[Test]
		public void FormatPriceTwoOrLessDecimals_MoreDecimalPrice_CorectlyFormattedAndRounded()
		{
			string formattedPrice = _formatter.FormatPriceTwoOrLessDecimals(_channel, 123.4567m);

			Assert.AreEqual("123,46 kr", formattedPrice);
		}

		[Test]
		public void FormatPriceTwoOrLessDecimals_BigPrice_CorectlyFormatted()
		{
			string formattedPrice = _formatter.FormatPriceTwoOrLessDecimals(_channel, int.MaxValue + 1m);

			Assert.AreEqual("2 147 483 648 kr", formattedPrice);
		}


		[Test]
		public void FormatPriceChannelOrLessDecimals_IntegerPrice_CorectlyFormatted()
		{
			string formattedPrice = _formatter.FormatPriceChannelOrLessDecimals(_channel, 123m);

			Assert.AreEqual("123 kr", formattedPrice);
		}

		[Test]
		public void FormatPriceChannelOrLessDecimals_DecimalPrice_CorectlyFormatted()
		{
			string formattedPrice = _formatter.FormatPriceChannelOrLessDecimals(_channel, 123.44m);

			Assert.AreEqual("123,4 kr", formattedPrice);
		}

		[Test]
		public void FormatPriceChannelOrLessDecimals_MoreDecimalPrice_CorectlyFormattedAndRounded()
		{
			string formattedPrice = _formatter.FormatPriceChannelOrLessDecimals(_channel, 123.4321m);

			Assert.AreEqual("123,4 kr", formattedPrice);
		}

		[Test]
		public void FormatPriceChannelOrLessDecimals_BigPrice_CorectlyFormatted()
		{
			string formattedPrice = _formatter.FormatPriceChannelOrLessDecimals(_channel, int.MaxValue + 1m);

			Assert.AreEqual("2 147 483 648 kr", formattedPrice);
		}
	}
}