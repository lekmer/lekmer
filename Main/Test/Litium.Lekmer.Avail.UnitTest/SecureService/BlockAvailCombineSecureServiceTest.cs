﻿using Litium.Lekmer.Avail.Cache;
using Litium.Lekmer.Avail.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Avail.UnitTest.SecureService
{
	[TestFixture]
	public class BlockAvailCombineSecureServiceTest
	{
		private static readonly int TEST_BLOCK_ID_1 = 1000742;
		private static readonly int TEST_BLOCK_ID_2 = 1000743;
		private static readonly int TEST_BLOCK_ID_3 = 1000744;
		private static readonly int TEST_BLOCK_ID_4 = 1000745;

		private MockRepository _mocker;
		private IAccessValidator _accessValidator;
		private ISystemUserFull _systemUserFull;
		private BlockAvailCombineRepository _blockAvailCombineRepository;
		private IBlockSecureService _blockSecureService;
		private BlockAvailCombineSecureService _blockAvailCombineSecureService;
		private IBlockSettingSecureService _blockSettingSecureService;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_blockAvailCombineRepository = _mocker.DynamicMock<BlockAvailCombineRepository>();
			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_blockSecureService = _mocker.Stub<IBlockSecureService>();
			_blockSettingSecureService = _mocker.Stub<IBlockSettingSecureService>();
			_blockAvailCombineSecureService = new BlockAvailCombineSecureService(_accessValidator, _blockAvailCombineRepository, null, _blockSecureService, _blockSettingSecureService);

			BlockAvailCombineCache.Instance.Flush();
		}

		[Test]
		public void GetById_SameId_CacheUsed()
		{
			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_1)).Return(new BlockAvailCombine { Id = TEST_BLOCK_ID_1 });

			_mocker.ReplayAll();

			Assert.AreEqual(TEST_BLOCK_ID_1, _blockAvailCombineSecureService.GetById(TEST_BLOCK_ID_1).Id);
			Assert.AreEqual(TEST_BLOCK_ID_1, _blockAvailCombineSecureService.GetById(TEST_BLOCK_ID_1).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_2)).Return(new BlockAvailCombine { Id = TEST_BLOCK_ID_2 });
			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_3)).Return(new BlockAvailCombine { Id = TEST_BLOCK_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(TEST_BLOCK_ID_2, _blockAvailCombineSecureService.GetById(TEST_BLOCK_ID_2).Id);
			Assert.AreEqual(TEST_BLOCK_ID_3, _blockAvailCombineSecureService.GetById(TEST_BLOCK_ID_3).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_BlockAvailUpdated_CacheRemoved()
		{
			IBlockAvailCombine blockAvailCombine = new BlockAvailCombine { Id = TEST_BLOCK_ID_4, Setting = new BlockSetting()};

			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_4)).Return(blockAvailCombine).Repeat.Twice();
			Expect.Call(_blockSecureService.Save(_systemUserFull, blockAvailCombine)).Return(TEST_BLOCK_ID_4).IgnoreArguments().Repeat.Once();

			_mocker.ReplayAll();

			Assert.AreEqual(TEST_BLOCK_ID_4, _blockAvailCombineSecureService.GetById(TEST_BLOCK_ID_4).Id);
			Assert.AreEqual(TEST_BLOCK_ID_4, _blockAvailCombineSecureService.Save(_systemUserFull, blockAvailCombine));
			Assert.AreEqual(TEST_BLOCK_ID_4, _blockAvailCombineSecureService.GetById(TEST_BLOCK_ID_4).Id);

			_mocker.VerifyAll();
		}
	}
}