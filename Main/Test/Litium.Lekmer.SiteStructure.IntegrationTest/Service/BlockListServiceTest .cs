﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class BlockListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockListService>();

			Assert.IsInstanceOf<IBlockListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockListService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockListService>();
			var service2 = IoC.Resolve<IBlockListService>();

			Assert.AreEqual(service1, service2);
		}
	}
}