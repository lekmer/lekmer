﻿using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Repository
{
	[TestFixture]
	public class ContentPageUrlHistoryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistoryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ContentPageUrlHistoryRepository>();

			Assert.IsInstanceOf<ContentPageUrlHistoryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistoryRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ContentPageUrlHistoryRepository>();
			var repository2 = IoC.Resolve<ContentPageUrlHistoryRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}