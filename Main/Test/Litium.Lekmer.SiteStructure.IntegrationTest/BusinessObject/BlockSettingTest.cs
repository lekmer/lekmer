﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockSettingTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSetting_Resolve_Resolved()
		{
			var obj = IoC.Resolve<IBlockSetting>();
			Assert.IsInstanceOf<IBlockSetting>(obj);
		}

		[Test]
		[Category("IoC")]
		public void BlockSetting_ResolveTwice_DifferentObjects()
		{
			var obj1 = IoC.Resolve<IBlockSetting>();
			var obj2 = IoC.Resolve<IBlockSetting>();
			Assert.AreNotEqual(obj1, obj2);
		}
	}
}