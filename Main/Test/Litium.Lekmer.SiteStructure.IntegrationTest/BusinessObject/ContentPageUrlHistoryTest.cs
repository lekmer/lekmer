﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ContentPageUrlHistoryTest
	{
		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistory_Resolve_Resolved()
		{
			var contentPageUrlHistory = IoC.Resolve<IContentPageUrlHistory>();

			Assert.IsInstanceOf<IContentPageUrlHistory>(contentPageUrlHistory);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageUrlHistory_ResolveTwice_DifferentObjects()
		{
			var contentPageUrlHistory1 = IoC.Resolve<IContentPageUrlHistory>();
			var contentPageUrlHistory2 = IoC.Resolve<IContentPageUrlHistory>();

			Assert.AreNotEqual(contentPageUrlHistory1, contentPageUrlHistory2);
		}
	}
}