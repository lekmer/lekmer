﻿using System.Data;
using Litium.Lekmer.SiteStructure.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockSettingDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockSetting_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IBlockSetting>(_dataReader);
			Assert.IsInstanceOf<BlockSettingDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockSetting_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IBlockSetting>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IBlockSetting>(_dataReader);
			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}