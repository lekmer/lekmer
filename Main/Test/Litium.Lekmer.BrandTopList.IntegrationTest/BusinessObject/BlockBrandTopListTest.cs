﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockBrandTopListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockBrandTopList>();

			Assert.IsInstanceOf<IBlockBrandTopList>(instance);
			Assert.IsInstanceOf<BlockBrandTopList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopList>();
			var instance2 = IoC.Resolve<IBlockBrandTopList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}