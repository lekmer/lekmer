﻿using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class GenderTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void GenderTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<GenderTypeRepository>();

			Assert.IsInstanceOf<GenderTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void GenderTypeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<GenderTypeRepository>();
			var repository2 = IoC.Resolve<GenderTypeRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}