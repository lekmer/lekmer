﻿using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerCustomerRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CustomerRepository>();

			Assert.IsInstanceOf<CustomerRepository>(repository);
			Assert.IsInstanceOf<LekmerCustomerRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CustomerRepository>();
			var repository2 = IoC.Resolve<CustomerRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}