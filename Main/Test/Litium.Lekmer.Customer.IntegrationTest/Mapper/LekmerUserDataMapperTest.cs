﻿using System.Data;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.IntegrationTest.Mapper
{
	[TestFixture]
	public class LekmerUserDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void LekmerUserDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IUser>(_dataReader);

			Assert.IsInstanceOf<LekmerUserDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void LekmerUserDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IUser>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IUser>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}