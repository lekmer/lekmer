﻿using System.Data;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.IntegrationTest.Mapper
{
	[TestFixture]
	public class FacebookUserDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void FacebookUserDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IFacebookUser>(_dataReader);

			Assert.IsInstanceOf<FacebookUserDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void FacebookUserDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IFacebookUser>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IFacebookUser>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}