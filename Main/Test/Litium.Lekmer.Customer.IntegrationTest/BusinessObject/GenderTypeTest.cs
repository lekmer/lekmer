﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class GenderTypeTest
	{
		[Test]
		[Category("IoC")]
		public void GenderType_Resolve_Resolved()
		{
			var genderType = IoC.Resolve<IGenderType>();

			Assert.IsInstanceOf<IGenderType>(genderType);
			Assert.IsInstanceOf<GenderType>(genderType);
		}

		[Test]
		[Category("IoC")]
		public void GenderType_ResolveTwice_DifferentObjects()
		{
			var genderType1 = IoC.Resolve<IGenderType>();
			var genderType2 = IoC.Resolve<IGenderType>();

			Assert.AreNotEqual(genderType1, genderType2);
		}
	}
}
