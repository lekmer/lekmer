﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class FacebookUserTest
	{
		[Test]
		[Category("IoC")]
		public void FacebookUser_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IFacebookUser>();
			Assert.IsInstanceOf<IFacebookUser>(instance);
		}

		[Test]
		[Category("IoC")]
		public void FacebookUser_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IFacebookUser>();
			var instance2 = IoC.Resolve<IFacebookUser>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}