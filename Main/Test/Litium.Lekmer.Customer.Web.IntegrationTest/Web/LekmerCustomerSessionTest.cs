﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.Web.IntegrationTest
{
	[TestFixture]
	public class LekmerCustomerSessionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerSession_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICustomerSession>();

			Assert.IsInstanceOf<ICustomerSession>(service);
			Assert.IsInstanceOf<LekmerCustomerSession>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerSession_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICustomerSession>();
			var service2 = IoC.Resolve<ICustomerSession>();

			Assert.AreEqual(service1, service2);
		}
	}
}