﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.Web.IntegrationTest
{
	[TestFixture]
	public class LekmerCustomerEntityMapperTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerEntityMapper_Resolve_Resolved()
		{
			string content = "FragmentContent [CustomerInformation.DeliveryAddress.HouseNumber] [CustomerInformation.BillingAddress.HouseNumber]";
			content = content + "[CustomerInformation.DeliveryAddress.HouseExtension] [CustomerInformation.BillingAddress.HouseExtension]";

			var fragment = new Fragment(content);

			var customer = IoC.Resolve<ICustomer>();
			//customer.CustomerInformation = IoC.Resolve<ICustomerInformation>();
			//customer.CustomerInformation.DefaultBillingAddress = IoC.Resolve<IAddress>();
			//customer.CustomerInformation.DefaultDeliveryAddress = IoC.Resolve<IAddress>();

			// Here we testing if LekmerCustomerEntityMapper is resolved correctly.
			fragment.AddEntity(customer);

			fragment.Render();

			// Customer entity mapper uses Channel.Current, and that cause test failing....
			//Assert.IsFalse(renderedContent.Contains("[CustomerInformation.DeliveryAddress.HouseNumber]"));
			//Assert.IsFalse(renderedContent.Contains("[CustomerInformation.DeliveryAddress.HouseExtension]"));
			//Assert.IsFalse(renderedContent.Contains("[CustomerInformation.BillingAddress.HouseNumber]"));
			//Assert.IsFalse(renderedContent.Contains("[CustomerInformation.BillingAddress.HouseExtension]"));
		}
	}
}