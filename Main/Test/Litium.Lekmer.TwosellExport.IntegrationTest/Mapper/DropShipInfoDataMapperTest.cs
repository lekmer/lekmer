﻿using System.Data;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.TwosellExport.IntegrationTest.Mapper
{
	[TestFixture]
	public class OrderInfoDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void OrderInfo_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IOrderInfo>(_dataReader);
			Assert.IsInstanceOf<OrderInfoDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void OrderInfo_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IOrderInfo>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IOrderInfo>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}