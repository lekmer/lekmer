﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Service
{
	[TestFixture]
	public class QliroTransactionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void QliroTransactionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroTransactionService>();

			Assert.IsInstanceOf<IQliroTransactionService>(service);
			Assert.IsInstanceOf<QliroTransactionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroTransactionService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IQliroTransactionService>();
			var service2 = IoC.Resolve<IQliroTransactionService>();

			Assert.AreEqual(service1, service2);
		}
	}
}