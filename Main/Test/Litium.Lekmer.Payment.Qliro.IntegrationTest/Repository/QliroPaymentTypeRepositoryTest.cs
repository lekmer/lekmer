﻿using Litium.Lekmer.Payment.Qliro.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Repository
{
	[TestFixture]
	public class QliroPaymentTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void QliroPaymentTypeRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<QliroPaymentTypeRepository>();

			Assert.IsInstanceOf<QliroPaymentTypeRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroPaymentTypeRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<QliroPaymentTypeRepository>();
			var service2 = IoC.Resolve<QliroPaymentTypeRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}