SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductActionTypeGetById]
	@Id int
as
begin
	select
		*
	from
		campaign.vCustomProductActionType
	where
		[ProductActionType.Id] = @Id
end

GO
