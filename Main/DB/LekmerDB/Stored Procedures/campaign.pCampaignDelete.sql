
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCampaignDelete]
@CampaignId		int
AS
BEGIN
	EXEC [campaignlek].[pCampaignRegistryCampaignDelete] @CampaignId	

	DELETE FROM campaign.tCampaign
	WHERE CampaignId = @CampaignId
END
GO
