
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tGiftCardViaMailProductAction] WHERE ProductActionId = @ProductActionId)

	EXEC [campaignlek].[pGiftCardViaMailProductActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tGiftCardViaMailProductAction]
	WHERE ProductActionId = @ProductActionId
	
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
