SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListDelete]
	@BlockId INT
AS
BEGIN
	EXEC [review].[pBlockBestRatedProductListBrandDeleteAll] @BlockId

	DELETE [review].[tBlockBestRatedProductList]
	WHERE BlockId = @BlockId
END
GO
