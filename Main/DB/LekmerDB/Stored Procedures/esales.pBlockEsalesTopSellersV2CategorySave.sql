SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2CategorySave]
	@BlockId				INT,
	@CategoryId				INT,
	@IncludeSubcategories	BIT
AS
BEGIN
	SET NOCOUNT ON

	INSERT [esales].[tBlockEsalesTopSellersV2Category] (
		[BlockId],
		[CategoryId],
		[IncludeSubcategories]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@IncludeSubcategories
	)
END
GO
