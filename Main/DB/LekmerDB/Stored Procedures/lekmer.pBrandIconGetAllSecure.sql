SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandIconGetAllSecure]
	@BrandId	INT
AS 
BEGIN 
	SELECT 
		i.*
	FROM 
		[lekmer].[vIconSecure] i
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE
		[bi].[BrandId] = @BrandId
END
GO
