
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pBlockTopListSave]
	@BlockId					INT,
	@IncludeAllCategories		BIT,
	@OrderStatisticsDayCount	INT,
	@LinkContentNodeId			INT = NULL,
	@CustomUrl					NVARCHAR(MAX) = NULL
AS
BEGIN
	UPDATE [addon].[tBlockTopList]
	SET
		[IncludeAllCategories]		= @IncludeAllCategories,
		[OrderStatisticsDayCount]	= @OrderStatisticsDayCount,
		[LinkContentNodeId]			= @LinkContentNodeId,
		[CustomUrl]					= @CustomUrl
	WHERE
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [addon].[tBlockTopList] (
		[BlockId],
		[IncludeAllCategories],
		[OrderStatisticsDayCount],
		[LinkContentNodeId],
		[CustomUrl]
	)
	VALUES (
		@BlockId,
		@IncludeAllCategories,
		@OrderStatisticsDayCount,
		@LinkContentNodeId,
		@CustomUrl
	)
END
GO
