SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryGetAllByLanguageId]
	@LanguageId	INT
AS 
BEGIN 
	SELECT 
		CPUH.*
	FROM 
		[lekmer].[vCustomProductUrlHistory] CPUH
	WHERE
		CPUH.[ProductUrlHistory.LanguageId] = @LanguageId
END
GO
