
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionSaveAddInvoiceResponse]
	@TransactionId INT,
	@StatusCode INT,
	@AvailableReservationAmount DECIMAL(16,2),
	@InvoiceNo VARCHAR(50),
	@InvoiceStatus INT,
	@InvoiceUrl VARCHAR(200),
	@FaultCode NVARCHAR(100),
	@FaultMessage NVARCHAR(MAX),
	@ErrorMessage NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tCollectorTransaction]
	SET
		[StatusCode] = @StatusCode,
		[AvailableReservationAmount] = @AvailableReservationAmount,
		[InvoiceNo] = @InvoiceNo,
		[InvoiceStatus] = @InvoiceStatus,
		[InvoiceUrl] = @InvoiceUrl,
		[FaultCode] = @FaultCode,
		[FaultMessage] = @FaultMessage,
		[ErrorMessage] = @ErrorMessage,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[TransactionId] = @TransactionId
END
GO
