SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRating_Save]
	@BlockId INT,
	@RatingId INT
AS
BEGIN
	INSERT [review].[tBlockRating] (
		[BlockId],
		[RatingId]
	)
	VALUES (
		@BlockId,
		@RatingId
	)
END
GO
