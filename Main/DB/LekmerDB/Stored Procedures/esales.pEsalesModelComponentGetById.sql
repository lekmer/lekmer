SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pEsalesModelComponentGetById]
	@ModelComponentId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		[em].*,
		[ec].*
	FROM
		[esales].[vEsalesModel] em
		INNER JOIN [esales].[vEsalesModelComponent] ec ON [ec].[EsalesModelComponent.ModelId] = [em].[EsalesModel.ModelId]
	WHERE
		[ec].[EsalesModelComponent.ModelComponentId] = @ModelComponentId
END
GO
