SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pPercentageDiscountCartActionSave]
	@CartActionId INT,
	@DiscountAmount DECIMAL(16,2)
AS
BEGIN
	UPDATE
		[campaignlek].[tPercentageDiscountCartAction]
	SET
		DiscountAmount = @DiscountAmount
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tPercentageDiscountCartAction] (
			CartActionId,
			DiscountAmount
		)
		VALUES (
			@CartActionId,
			@DiscountAmount
		)
	END
END
GO
