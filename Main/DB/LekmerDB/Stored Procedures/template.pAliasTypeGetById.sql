SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 22.12.2008		Time: 10:40
Description:
			Created 
*/

CREATE PROCEDURE [template].[pAliasTypeGetById]
@AliasTypeId int
AS
BEGIN
	SELECT
		*
	FROM
		[template].[vCustomAliasType]
	where
		@AliasTypeId = [AliasType.Id]
	ORDER BY [AliasType.Ordinal]
END

GO
