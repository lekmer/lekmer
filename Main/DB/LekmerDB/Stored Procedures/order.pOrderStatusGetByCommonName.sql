SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderStatusGetByCommonName]
	@CommonName	varchar(50)
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM
		[order].[vCustomOrderStatus]
	WHERE
		[OrderStatus.CommonName] = @CommonName
END

GO
