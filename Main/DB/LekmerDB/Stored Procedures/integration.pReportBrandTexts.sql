
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportBrandTexts]

AS
BEGIN
	SET NOCOUNT ON

	SELECT
		--b.BrandId,
		b.ErpId,
		b.Title AS BrandTitle,
		b.[Description] AS SE,
		nob.[Description] AS 'NO',
		dkb.[Description] AS 'DK',
		fib.[Description] AS 'FI',
		nlb.[Description] AS 'NL'
	FROM 
		lekmer.tBrand b WITH (NOLOCK)
		LEFT JOIN lekmer.tBrandTranslation nob WITH (NOLOCK) ON nob.LanguageId = 1000001 AND nob.BrandId = b.BrandId
		LEFT JOIN lekmer.tBrandTranslation dkb WITH (NOLOCK) ON dkb.LanguageId = 1000002 AND dkb.BrandId = b.BrandId
		LEFT JOIN lekmer.tBrandTranslation fib WITH (NOLOCK) ON fib.LanguageId = 1000003 AND fib.BrandId = b.BrandId
		LEFT JOIN lekmer.tBrandTranslation nlb WITH (NOLOCK) ON nlb.LanguageId = 1000005 AND nlb.BrandId = b.BrandId
END
GO
