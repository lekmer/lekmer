SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_Save]
	@RatingReviewFeedbackId INT = NULL,
	@ChannelId INT,
	@ProductId INT,
	@OrderId INT,
	@RatingReviewStatusId INT,
	@LikeHit INT,
	@Inappropriate BIT,
	@IPAddress VARCHAR(50),
	@CreatedDate DATETIME,
	@RatingReviewUserId INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingReviewFeedbackId IS NOT NULL)
	BEGIN
		UPDATE 
			[review].[tRatingReviewFeedback]
		SET 
			[RatingReviewStatusId] = @RatingReviewStatusId,
			[Inappropriate] = @Inappropriate
		WHERE
			[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	END
	ELSE
	BEGIN
		INSERT INTO [review].[tRatingReviewFeedback] (
			[ChannelId],
			[ProductId],
			[OrderId],
			[RatingReviewStatusId],
			[LikeHit],
			[Inappropriate],
			[IPAddress],
			[CreatedDate],
			[RatingReviewUserId]
		)
		VALUES (
			@ChannelId,
			@ProductId,
			@OrderId,
			@RatingReviewStatusId,
			@LikeHit,
			@Inappropriate,
			@IPAddress,
			@CreatedDate,
			@RatingReviewUserId
		)

		SET @RatingReviewFeedbackId = SCOPE_IDENTITY()
	END

	RETURN @RatingReviewFeedbackId
END
GO
