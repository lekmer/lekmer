SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pVariationGroupDelete]
	@VariationGroupId	int
as
begin         
	delete product.tVariationGroupTranslation where [VariationGroupId] = @VariationGroupId		
	delete product.tVariationGroup where [VariationGroupId] = @VariationGroupId		
end
GO
