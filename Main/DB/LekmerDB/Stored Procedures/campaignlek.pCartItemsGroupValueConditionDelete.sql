
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionDelete]
	@ConditionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaignlek].[tCartItemsGroupValueCondition] WHERE [ConditionId] = @ConditionId)

	EXEC [campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll] @ConditionId

	DELETE FROM [campaignlek].[tCartItemsGroupValueCondition]
	WHERE [ConditionId] = @ConditionId

	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
