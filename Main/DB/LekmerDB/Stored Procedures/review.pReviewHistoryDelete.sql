SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pReviewHistoryDelete]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE rh
	FROM [review].[tReviewHistory] rh
	INNER JOIN [review].[tReview] r ON [r].[ReviewId] = [rh].[ReviewId]
	WHERE [r].[RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
