SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2GetById]
	@BlockId	INT,
	@LanguageId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[esales].[vBlockEsalesTopSellersV2]
	WHERE
		[BlockEsalesTopSellersV2.BlockId] = @BlockId
		AND [Block.LanguageId] = @LanguageId
end
GO
