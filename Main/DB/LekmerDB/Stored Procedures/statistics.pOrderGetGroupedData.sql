SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [statistics].[pOrderGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		
	DECLARE @tOrder TABLE ([DateTime] smalldatetime)
	INSERT INTO 
		@tOrder
	(
		[DateTime]
	)
	SELECT
		[statistics].[fnGetDateTimeKeyDirectly]([CreatedDate], @Mode) AS 'DateTime'
	FROM
		[order].[tOrder] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
			
	SELECT 
		d.[DateTime], 
		isnull(count(o.DateTime), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tOrder o ON o.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
