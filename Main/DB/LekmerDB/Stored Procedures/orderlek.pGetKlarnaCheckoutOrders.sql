SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pGetKlarnaCheckoutOrders]
	@Days INT,
	@OrderStatus VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Current date.
	DECLARE @CurrentDate AS DATETIME
	SET @CurrentDate = GETDATE()

	-- Deletion date.
	DECLARE @DeletionDate AS DATETIME
	SET @DeletionDate = DATEADD(DAY, -@Days, @CurrentDate)

	-- Get old deleted orders
	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] < @DeletionDate
		AND [OrderStatus.CommonName] = @OrderStatus
END
GO
