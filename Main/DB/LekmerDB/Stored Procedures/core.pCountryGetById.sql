SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pCountryGetById]
	@CountryId	int
as
begin
	set nocount on

	select
		*
	from
		[core].[vCustomCountry]
	where
		[Country.Id] = @CountryId
end

GO
