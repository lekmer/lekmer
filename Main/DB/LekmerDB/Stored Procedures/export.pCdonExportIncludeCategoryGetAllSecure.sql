SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryGetAllSecure]
AS
BEGIN
	SELECT
		[ic].[CategoryId] 'ItemId',
		[ic].[ProductRegistryId] 'ProductRegistryId',
		[ic].[Reason] 'Reason',
		[ic].[UserId] 'UserId',
		[ic].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportIncludeCategory] ic
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [ic].[ProductRegistryId]
END
GO
