SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryDeleteAllByCategory]
	@CategoryIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE
		ic
	FROM
		[export].[tCdonExportIncludeCategory] ic
		INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) AS pl ON pl.[ID] = [ic].[CategoryId]
END
GO
