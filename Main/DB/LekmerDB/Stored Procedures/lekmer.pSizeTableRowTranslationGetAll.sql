
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowTranslationGetAll]
	@SizeTableRowId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [strt].[SizeTableRowTranslation.SizeTableRowId],
		[strt].[SizeTableRowTranslation.LanguageId],
		[strt].[SizeTableRowTranslation.Column1Value],
		[strt].[SizeTableRowTranslation.Column2Value]
	FROM
	    [lekmer].[vSizeTableRowTranslation] strt
	WHERE 
		[strt].[SizeTableRowTranslation.SizeTableRowId] = @SizeTableRowId
	ORDER BY
		[strt].[SizeTableRowTranslation.LanguageId]
END
GO
