SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroPendingOrderDelete]
	@QliroPendingOrderId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE
		[orderlek].[tQliroPendingOrder]
	WHERE
		[QliroPendingOrderId] = @QliroPendingOrderId
END
GO
