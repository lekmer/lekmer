SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlSave]
	@ProductId	int,
	@LanguageId	int,
	@UrlTitle	nvarchar(256)
AS 
BEGIN
	UPDATE
		[lekmer].[tProductUrl]
	SET
		[UrlTitle] = @UrlTitle
	WHERE
		[ProductId] = @ProductId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tProductUrl]
		(
			[ProductId],
			[LanguageId],
			[UrlTitle]
		)
		VALUES
		(
			@ProductId,
			@LanguageId,
			@UrlTitle
		)
	END
END
GO
