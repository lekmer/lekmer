
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartItemPriceActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[c].[CategoryId]
	FROM 
		[addon].[tCartItemPriceActionExcludeCategory] c
	WHERE 
		[c].[CartActionId] = @CartActionId
END
GO
