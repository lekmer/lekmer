SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-19
-- Description:	Saves a media items
-- =============================================
Create PROCEDURE [media].[pMediaItemInsert]
	@MediaId INT,
	@MediaFormatId INT,
	@MediaFolderId INT,
	@Title NVARCHAR(150),
	@FileName NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON

	    INSERT [media].[tMedia]
	      (
	        [MediaFormatId],
	        [MediaFolderId],
	        [Title],
	        [FileName]
	      )
	    VALUES
	      (
	        @MediaFormatId,
	        @MediaFolderId,
	        @Title,
	        @FileName
	      ) 
	    SET @MediaId = SCOPE_IDENTITY()
	
	RETURN @MediaId
END
GO
