SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeBrandInsert]
	@ConfigId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionExcludeBrand] (
		ConfigId,
		BrandId
	)
	VALUES (
		@ConfigId,
		@BrandId
	)
END
GO
