SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageDelete]
	@CampaignId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @CampaignLandingPageId INT
	SET @CampaignLandingPageId = (SELECT [CampaignLandingPageId] FROM [campaignlek].[tCampaignLandingPage] WHERE [CampaignId] = @CampaignId)
	
	DELETE FROM [campaignlek].[tCampaignRegistryCampaignLandingPage] 
	WHERE [CampaignLandingPageId] = @CampaignLandingPageId
	
	EXEC [campaignlek].[pCampaignLandingPageTranslationDelete] @CampaignLandingPageId
			
	DELETE FROM [campaignlek].[tCampaignLandingPage]
	WHERE CampaignId = @CampaignId
END
GO
