SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportEmptyBrandTexts]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[b].[ErpId]										AS 'ErpId',
		[b].[Title]										AS 'BrandTitle',
		ISNULL([seb].[Description], [b].[Description])	AS 'SE',
		[nob].[Description] 							AS 'NO',
		[dkb].[Description] 							AS 'DK',
		[fib].[Description] 							AS 'FI',
		[nlb].[Description] 							AS 'NL'
	FROM 
		[lekmer].[tBrand] b WITH (NOLOCK)
		LEFT JOIN [lekmer].[tBrandTranslation] seb WITH (NOLOCK) ON [seb].[LanguageId] = 1 AND [seb].[BrandId] = [b].[BrandId]
		LEFT JOIN [lekmer].[tBrandTranslation] nob WITH (NOLOCK) ON [nob].[LanguageId] = 1000001 AND [nob].[BrandId] = [b].[BrandId]
		LEFT JOIN [lekmer].[tBrandTranslation] dkb WITH (NOLOCK) ON [dkb].[LanguageId] = 1000002 AND [dkb].[BrandId] = [b].[BrandId]
		LEFT JOIN [lekmer].[tBrandTranslation] fib WITH (NOLOCK) ON [fib].[LanguageId] = 1000003 AND [fib].[BrandId] = [b].[BrandId]
		LEFT JOIN [lekmer].[tBrandTranslation] nlb WITH (NOLOCK) ON [nlb].[LanguageId] = 1000005 AND [nlb].[BrandId] = [b].[BrandId]
	WHERE	(
				([b].[Description] IS NULL OR [b].[Description] = '')
				AND ([seb].[Description] IS NULL OR [seb].[Description] = '')
			)
			OR ([nob].[Description] IS NULL OR [nob].[Description] = '')
			OR ([dkb].[Description] IS NULL OR [dkb].[Description] = '')
			OR ([fib].[Description] IS NULL OR [fib].[Description] = '')
			OR ([nlb].[Description] IS NULL OR [nlb].[Description] = '')
END
GO
