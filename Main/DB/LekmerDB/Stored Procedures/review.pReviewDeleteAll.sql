
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pReviewDeleteAll]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	EXEC [review].[pReviewHistoryDelete] @RatingReviewFeedbackId
	
	DELETE FROM	[review].[tReview]
	WHERE [RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
