SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingRegistryGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rr.*
	FROM
		[review].[vRatingRegistry] rr
END
GO
