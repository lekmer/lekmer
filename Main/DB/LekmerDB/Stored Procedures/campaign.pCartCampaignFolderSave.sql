
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCartCampaignFolderSave]
@FolderId	int,
@ParentId	int,
@Title		nvarchar(50)
AS
BEGIN
	IF EXISTS
		(
			SELECT 1 FROM campaign.tCampaignFolder
			WHERE Title = @Title 
				AND FolderId <> @FolderId
				AND	((@ParentId IS NULL AND ParentFolderId IS NULL)
					OR ParentFolderId = @ParentId)
				AND TypeId = 2
		)
		RETURN -1
		
	SET NOCOUNT ON
	
	UPDATE 
		campaign.tCampaignFolder
	SET 
		ParentFolderId = @ParentId,
		Title = @Title
	WHERE
		FolderId = @FolderId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO campaign.tCampaignFolder
		(
			ParentFolderId,
			Title,
			TypeId,
			Ordinal
		)
		VALUES
		(
			@ParentId,
			@Title,
			2,
			(SELECT MAX(Ordinal)+1 FROM campaign.tCampaignFolder)
		)
		SET @FolderId = scope_identity()
	END
	RETURN @FolderId
END
GO
