
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeBrandGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tCampaignActionIncludeBrand]
	WHERE 
		[ConfigId] = @ConfigId
END
GO
