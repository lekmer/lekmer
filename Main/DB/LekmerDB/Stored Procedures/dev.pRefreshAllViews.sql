SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dev].[pRefreshAllViews]
as
begin

	declare @RefreshScript varchar(max)
	set @RefreshScript = ''

	select @RefreshScript= @RefreshScript + '
	exec sp_refreshview N''['+table_schema+'].['+table_name+']'''
	from information_schema.tables
	where table_type = 'view'

	exec (@RefreshScript)
	exec (@RefreshScript)
	exec (@RefreshScript)
	exec (@RefreshScript)
	exec (@RefreshScript)

end


GO
