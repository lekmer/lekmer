
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerCartItemSave]
	@CartItemId 			INT,
	@ProductId				INT,
	@SizeId					INT,
	@IsAffectedByCampaign	BIT,
	@IPAddress				VARCHAR(50)
AS
BEGIN
      SET NOCOUNT ON

      UPDATE
            [lekmer].[tLekmerCartItem]
      SET
            SizeId = @SizeId,
            IsAffectedByCampaign = IsAffectedByCampaign
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [lekmer].[tLekmerCartItem]
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  IsAffectedByCampaign,
                  IPAddress
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @IsAffectedByCampaign,
                  @IPAddress
            )
      END
END
GO
