
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetViewAllByIdListWithoutAnyFilter]
	@ChannelId		INT,
	@ProductIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @CurrencyId INT
	SET @CurrencyId = (SELECT [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @PriceListRegistryId INT
	SET @PriceListRegistryId = (SELECT [PriceListRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	DECLARE @SiteStructureRegistryId INT
	SET @SiteStructureRegistryId = (SELECT [SiteStructureRegistryId] FROM [sitestructure].[tSiteStructureModuleChannel] WHERE [ChannelId] = @ChannelId)
	
	SELECT
		[p].[ProductId]						'Product.Id',
		[p].[ErpId]							'Product.ErpId',
		[p].[EanCode]						'Product.EanCode',
		[p].[CategoryId]					'Product.CategoryId',
		[p].[ProductStatusId]				'Product.ProductStatusId',
		[p].[NumberInStock]					'Product.NumberInStock',
		[p].[ItemsInPackage]				'Product.ItemsInPackage',
		[p].[MediaId]						'Product.MediaId',
		[p].[IsDeleted]						'Product.IsDeleted',
		COALESCE([pt].[Title], [p].[Title])							'Product.Title',
		COALESCE([pt].[WebShopTitle], [p].[WebShopTitle])			'Product.WebShopTitle',
		COALESCE([pt].[ShortDescription], [p].[ShortDescription])	'Product.ShortDescription',
		COALESCE([pt].[Description], [p].[Description])				'Product.Description',
		[lp].[Lekmer.BrandId]				'Lekmer.BrandId',
		[lp].[Lekmer.IsBookable]			'Lekmer.IsBookable',
		[lp].[Lekmer.IsNewFrom]				'Lekmer.IsNewFrom',
		[lp].[Lekmer.IsNewTo]				'Lekmer.IsNewTo',
		[lp].[Lekmer.CreatedDate]			'Lekmer.CreatedDate',
		[lp].[Lekmer.LekmerErpId]			'Lekmer.LekmerErpId',
		[lp].[Lekmer.HasSizes]				'Lekmer.HasSizes',
		[lp].[Lekmer.ShowVariantRelations]	'Lekmer.ShowVariantRelations',
		[lp].[Lekmer.Weight]				'Lekmer.Weight',
		[lp].[Lekmer.ProductTypeId]			'Lekmer.ProductTypeId',
		[lp].[Lekmer.IsAbove60L]			'Lekmer.IsAbove60L',
		[lp].[Lekmer.StockStatusId]			'Lekmer.StockStatusId',
		[lp].[Lekmer.MonitorThreshold]		'Lekmer.MonitorThreshold',
		[lp].[Lekmer.MaxQuantityPerOrder]	'Lekmer.MaxQuantityPerOrder',
		[lp].[Lekmer.DeliveryTimeId]		'Lekmer.DeliveryTimeId',
		[lp].[Lekmer.SellOnlyInPackage]		'Lekmer.SellOnlyInPackage',
		[lp].[Lekmer.SupplierArticleNumber]	'Lekmer.SupplierArticleNumber',
		[lp].[Lekmer.IsDropShip]			'Lekmer.IsDropShip',
		[pu].[ProductUrl.UrlTitle]			'Lekmer.UrlTitle',
		[rp].[Price]						'Lekmer.RecommendedPrice',
		[pssr].[ParentContentNodeId]		'Product.ParentContentNodeId',
		[pssr].[TemplateContentNodeId]		'Product.TemplateContentNodeId',
		[i].*,
		[dt].*,
		COALESCE ([lpt].[Product.Measurement], [lp].[Lekmer.Measurement]) 'Lekmer.Measurement',
		[lp].[Lekmer.AgeFromMonth],
		[lp].[Lekmer.AgeToMonth],
		[lp].[Lekmer.BatteryTypeId],
		[lp].[Lekmer.NumberOfBatteries],
		[lp].[Lekmer.IsBatteryIncluded],
		[lp].[Lekmer.ExpectedBackInStock],
		[lp].[Lekmer.SizeDeviationId],
		[bt].*,
		[sd].*,
		@ChannelId				'Product.ChannelId', 
		@CurrencyId				'Product.CurrencyId',
		@PriceListRegistryId	'Product.PriceListRegistryId',
		[pli].*
	FROM
		[generic].[fnConvertIDListToTableWithOrdinal] (@ProductIds, @Delimiter) AS pl
		INNER JOIN [product].[tProduct] AS p ON [p].[ProductId] = [pl].[Id]
		INNER JOIN [lekmer].[vLekmerProduct] lp ON [lp].[Lekmer.ProductId] = [p].[ProductId]
		INNER JOIN [lekmer].[vProductUrl] pu ON [pu].[ProductUrl.ProductId] = [lp].[Lekmer.ProductId] AND [pu].[ProductUrl.LanguageId] = @LanguageId
		LEFT JOIN [product].[tProductTranslation] AS pt ON [pt].[ProductId] = [p].[ProductId] AND [pt].[LanguageId] = @LanguageId
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON [rp].[ProductId] = [p].[ProductId] AND [rp].[ChannelId] = @ChannelId
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON [pssr].[ProductId] = [p].[ProductId] AND [pssr].[SiteStructureRegistryId] = @SiteStructureRegistryId
		LEFT JOIN [media].[vCustomImage] AS i ON [i].[Image.MediaId] = [p].[MediaId] AND [i].[Image.LanguageId] = @LanguageId
		LEFT JOIN [productlek].[vDeliveryTime] AS dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = @ChannelId
		LEFT JOIN [lekmer].[vLekmerProductTranslation] lpt ON [lpt].[Product.Id] = [p].[ProductId] AND [lpt].[Language.Id] = @LanguageId
		LEFT JOIN [lekmer].[vBatteryType] bt ON [bt].[BatteryType.Id] = [lp].[Lekmer.BatteryTypeId]
		LEFT JOIN [lekmer].[vSizeDeviation] sd ON [sd].[SizeDeviation.Id] = [lp].[Lekmer.SizeDeviationId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON [pli].[Price.ProductId] = [p].[ProductId]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice](
				@CurrencyId,
				[p].[ProductId],
				@PriceListRegistryId,
				NULL
			)
	WHERE
		[p].[IsDeleted] = 0
		AND [lp].[Lekmer.SellOnlyInPackage] = 0
END
GO
