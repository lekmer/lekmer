
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandProductListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [lekmer].[tBlockBrandProductList]
	WHERE [BlockId] = @BlockId
END
GO
