SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockProductFilterDelete]
	@BlockId	int
as
begin
	delete
		[lekmer].[tBlockProductFilter]
	where
		[BlockId] = @BlockId
end
GO
