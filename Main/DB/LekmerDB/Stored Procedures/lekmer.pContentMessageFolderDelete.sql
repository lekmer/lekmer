SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderDelete]
	@ContentMessageFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].[tContentMessageFolder]
	WHERE [ContentMessageFolderId] = @ContentMessageFolderId
END
GO
