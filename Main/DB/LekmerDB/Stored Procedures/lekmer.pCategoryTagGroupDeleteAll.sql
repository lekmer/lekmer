SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryTagGroupDeleteAll]
	@CategoryId	INT
AS
BEGIN 
	DELETE FROM [lekmer].[tCategoryTagGroup]
	WHERE CategoryId = @CategoryId
END 
GO
