SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2Delete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [esales].[pBlockEsalesTopSellersV2CategoryDeleteAll] @BlockId

	EXEC [esales].[pBlockEsalesTopSellersV2ProductDeleteAll] @BlockId

	EXEC [esales].[pBlockEsalesTopSellersV2TranslationDelete] @BlockId

	DELETE [esales].[tBlockEsalesTopSellersV2]
	WHERE [BlockId] = @BlockId
END
GO
