SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetAllByContentNodeSecure]
	@ContentNodeId	int
as
begin
	select 
		B.*
	from 
		[sitestructure].[vCustomBlockSecure] as B
	where
		[Block.ContentNodeId] = @ContentNodeId
	order by [Block.Ordinal]
end

GO
