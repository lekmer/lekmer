
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_ChannelRestrictionsProducts]
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRANSACTION
		--------------------------------------------------
		--- Update tProductTranslation with new products
		--------------------------------------------------
		-- NO
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000001 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000001)

		-- DK
		INSERT INTO product.tProductTranslation (ProductId, LanguageId)
		SELECT p.productid, 1000002 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000002)

		-- FI
		INSERT INTO product.tProductTranslation(ProductId, LanguageId)
		SELECT p.productid, 1000003 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000003)

		-- NL
		INSERT INTO product.tProductTranslation(ProductId, LanguageId)
		SELECT p.productid, 1000005 FROM product.tProduct p
		WHERE p.ProductId NOT IN (SELECT productid FROM product.tProductTranslation WHERE LanguageId = 1000005)


		----------------------------------------------------------------------------
		--- Remove product restriction with reason = 'No Translation'
		----------------------------------------------------------------------------
		DELETE FROM [lekmer].[tProductRegistryRestrictionProduct]
		WHERE RestrictionReason = 'No Translation'
		--AND UserId IS NULL
		

		----------------------------------------------------------------------------
		--- Insert not translated products in the tProductRegistryRestrictionProduct
		----------------------------------------------------------------------------
		-- NO
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			2,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000001 -- NO
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 2)

		-- DK
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			3,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000002 -- DK
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 3)

		-- FI
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			4,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000003 -- FI
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 4)
							
		-- NL
		INSERT INTO	[lekmer].[tProductRegistryRestrictionProduct] (
			ProductRegistryId,
			ProductId,
			RestrictionReason,
			UserId,
			CreatedDate
		)
		SELECT
			1000005,
			pt.ProductId,
			'No Translation',
			NULL,
			GETDATE()
		FROM
			[product].[tProductTranslation] pt
		WHERE
			LanguageId = 1000005 -- NL
			AND Title IS NULL
			AND WebShopTitle IS NULL
			AND NOT EXISTS (SELECT 1 FROM [lekmer].[tProductRegistryRestrictionProduct] prrp
							WHERE pt.ProductId = prrp.ProductId AND prrp.ProductRegistryId = 1000005)


		----------------------------------------------------------------------
		---- Get Product Ids that need add to product.tProductRegistryProduct.
		----------------------------------------------------------------------
		DECLARE @ProductIdsToInsert [lekmer].[ProductRegistryProducts]

		-- NO
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 2 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 2)

		-- DK
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 3 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 3)
		
		-- FI
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 4 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 4)
		
		-- NL
		INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
		SELECT p.productid, 1000005 FROM [product].[tProduct] p
		WHERE p.ProductId NOT IN (SELECT ProductId FROM [product].[tProductRegistryProduct] WHERE ProductRegistryId = 1000005)


		--------------------------------------------------------------------------------
		---- Delete items from tProductRegistryProduct and from list products to insert.
		--------------------------------------------------------------------------------
		DELETE prp
		FROM [product].[tProductRegistryProduct] prp
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = prp.ProductRegistryId 
																		 AND prrp.ProductId = prp.ProductId

		DELETE pIds
		FROM @ProductIdsToInsert pIds
		INNER JOIN [lekmer].[tProductRegistryRestrictionProduct] prrp ON prrp.ProductRegistryId = pIds.ProductRegistryId 
																		 AND prrp.ProductId = pIds.ProductId


		EXEC [lekmer].[pProductRegistryProductAdd] @ProductIdsToInsert
		
	COMMIT TRANSACTION
	END TRY

	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			'',
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)
	END CATCH
END
GO
