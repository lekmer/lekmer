SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemGroupFixedDiscountActionCurrency]
	WHERE 
		[CartItemGroupFixedDiscountActionCurrency.CartActionId] = @CartActionId
END

GO
