SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetByIdSecure]
	@BlockId int
as
begin
	select 
		B.*		
	from 
		[sitestructure].[vCustomBlockSecure] as B
	where
		[Block.BlockId] = @BlockId
end

GO
