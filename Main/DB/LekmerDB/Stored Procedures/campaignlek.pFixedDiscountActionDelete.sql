SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pFixedDiscountActionIncludeBrandDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedDiscountActionExcludeBrandDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedDiscountActionIncludeCategoryDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedDiscountActionExcludeCategoryDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedDiscountActionIncludeProductDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedDiscountActionExcludeProductDeleteAll] @ProductActionId
	
	EXEC [campaignlek].[pFixedDiscountActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tFixedDiscountAction]
	WHERE ProductActionId = @ProductActionId
END
GO
