
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [customer].[pCustomerUserIsUserNameUnique]
	@CustomerId INT,
	@UserName	NVARCHAR(320)
AS	
BEGIN
	IF EXISTS (
				SELECT 
					1 
				FROM 
					[customer].[tCustomerUser] 
				WHERE
					[UserName]= @UserName 
					AND [CustomerId] <> @CustomerId
				)
	BEGIN
		RETURN -1
	END
END
GO
