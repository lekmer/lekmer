SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemGetAllByOrder]
	@OrderId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vPackageOrderItem]
	WHERE
		[PackageOrderItem.OrderId] = @OrderId
END
GO
