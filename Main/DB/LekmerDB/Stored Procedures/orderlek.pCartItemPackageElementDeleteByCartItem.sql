SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCartItemPackageElementDeleteByCartItem]
	@CartItemId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[orderlek].[tCartItemPackageElement]
	WHERE
		[CartItemId] = @CartItemId
END
GO
