SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionSave]
	@CartActionId INT
AS 
BEGIN
	IF EXISTS (SELECT 1 FROM [campaignlek].[tFixedDiscountCartAction] WHERE CartActionId = @CartActionId)
		RETURN
	
	INSERT [campaignlek].[tFixedDiscountCartAction]
	( 
		CartActionId
	)
	VALUES 
	(
		@CartActionId
	)
END
GO
