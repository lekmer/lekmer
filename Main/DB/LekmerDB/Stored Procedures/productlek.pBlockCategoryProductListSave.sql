SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListSave]
	@BlockId			INT,
	@ProductSortOrderId	INT
AS
BEGIN
	UPDATE
		[productlek].[tBlockCategoryProductList]
	SET
		[ProductSortOrderId] = @ProductSortOrderId
	WHERE
		[BlockId] = @BlockId
			
	IF  @@ROWCOUNT = 0 
	BEGIN
		INSERT [productlek].[tBlockCategoryProductList] (
			[BlockId],
			[ProductSortOrderId]
		)
		VALUES (
			@BlockId,
			@ProductSortOrderId
		)
	END
END
GO
