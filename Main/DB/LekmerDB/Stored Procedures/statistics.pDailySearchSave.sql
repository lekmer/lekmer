
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pDailySearchSave]
	@ChannelId		INT,
	@Query			NVARCHAR(400),
	@Date			SMALLDATETIME,
	@HitCount		INT,
	@SearchCount	INT
AS
BEGIN
	UPDATE
		[statistics].[tDailySearch]
	SET
		[HitCount] = @HitCount,
		[SearchCount] = [SearchCount] + @SearchCount
	WHERE
		[ChannelId] = @ChannelId
		AND [Query] = @Query
		AND [Date] = @Date

	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [statistics].[tDailySearch] (
			[ChannelId],
			[Query],
			[Date],
			[HitCount],
			[SearchCount]
		)
		VALUES (
			@ChannelId,
			@Query,
			@Date,
			@HitCount,
			@SearchCount
		)
	END
END
GO
