
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableIncludeProductDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableIncludeCategoryBrandDelete] @SizeTableId	
	EXEC [lekmer].[pSizeTableTranslationDelete] @SizeTableId
	EXEC [lekmer].[pSizeTableRowDelete] @SizeTableId
	
	EXEC [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTable] @SizeTableId
	EXEC [lekmer].[pSizeTableMediaDeleteAllBySizeTable] @SizeTableId
		
	DELETE FROM [lekmer].[tSizeTable]
	WHERE [SizeTableId] = @SizeTableId
END
GO
