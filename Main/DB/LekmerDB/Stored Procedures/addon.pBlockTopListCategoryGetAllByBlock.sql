SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pBlockTopListCategoryGetAllByBlock]
	@LanguageId int,
	@BlockId int
as
begin
	select
		b.*
	from
		addon.vCustomBlockTopListCategorySecure b
		inner join [product].[vCustomCategory] as c on b.[BlockTopListCategory.CategoryId] = c.[Category.Id]
	where
		[BlockTopListCategory.BlockId] = @BlockId
		and c.[Category.LanguageId] = @LanguageId
end
GO
