
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_UpdateProductBrandLekmer]
AS
BEGIN

	BEGIN TRY
	BEGIN TRANSACTION

		UPDATE
			lp
		SET
			lp.BrandId = b.BrandId
		FROM
			[integration].[tempProduct] tp
			INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[HYErpId] = SUBSTRING(tp.[HYarticleId], 5, 12)
			INNER JOIN [lekmer].[tBrand] b ON b.[ErpId] = tp.[BrandId]
		WHERE 
			SUBSTRING(tp.HYarticleId, 1, 3) = 1
			AND 
			(
				lp.[BrandId] <> b.[BrandId]
				OR
				lp.[BrandId] IS NULL
			)

	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
