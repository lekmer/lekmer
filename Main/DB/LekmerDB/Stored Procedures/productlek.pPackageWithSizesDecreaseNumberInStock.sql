
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--DROP PROCEDURE [lekmer].[pPackageWithSizesDecreaseNumberInStock]

CREATE PROCEDURE [productlek].[pPackageWithSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT,
	@ItemsWithoutSizes		VARCHAR(max),
	@ItemsWithSizes			XML,
	@Delimiter				CHAR(1)
	
AS 
BEGIN
	SET NOCOUNT ON

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	-- products without sizes 
	DECLARE @tmpPackageProductWithoutSizes TABLE (ProductId INT)
	INSERT INTO @tmpPackageProductWithoutSizes (ProductId)
	SELECT [Id] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ItemsWithoutSizes, @Delimiter)
	
	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithoutSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithoutSizes)

		-- Update Package Products NumberInSock
		UPDATE p
		SET
			[p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
										ELSE [p].[NumberInStock] - @Quantity
								   END)
		FROM [product].[tProduct] p
		WHERE [p].[ProductId] = @tmpProductId
		
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId
		DELETE TOP(1) @tmpPackageProductWithoutSizes WHERE [ProductId] = @tmpProductId
	END
	
	
	-- products with sizes 
	DECLARE @tmpPackageProductWithSizes TABLE (ProductId INT, SizeId INT)
	INSERT INTO @tmpPackageProductWithSizes (ProductId, [SizeId])
	SELECT c.value('@productId[1]', 'int'), c.value('@sizeId[1]', 'int') FROM @ItemsWithSizes.nodes('/items/item') T(c)

	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpSizeId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithSizes)
		SET @tmpSizeId = (SELECT TOP 1 [SizeId] FROM @tmpPackageProductWithSizes)

		-- Update ProductSize NumberInSock
		UPDATE
			ps
		SET
			[ps].[NumberInStock] = (CASE WHEN [ps].[NumberInStock] - @Quantity < 0 THEN 0
										 ELSE [ps].[NumberInStock] - @Quantity END)
		FROM
			[lekmer].[tProductSize] ps
		WHERE [ps].[ProductId] = @tmpProductId AND [ps].[SizeId] = @tmpSizeId

		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId
		DELETE TOP(1) @tmpPackageProductWithSizes WHERE [ProductId] = @tmpProductId
	END
END
GO
