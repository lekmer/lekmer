SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderSave]
	@SizeTableFolderId INT,
	@ParentSizeTableFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tSizeTableFolder]
		WHERE [Title] = @Title
			AND [SizeTableFolderId] <> @SizeTableFolderId
			AND	((@ParentSizeTableFolderId IS NULL AND [ParentSizeTableFolderId] IS NULL)
				OR [ParentSizeTableFolderId] = @ParentSizeTableFolderId)
	)
	RETURN -1
	
	UPDATE 
		[lekmer].[tSizeTableFolder]
	SET 
		[ParentSizeTableFolderId] = @ParentSizeTableFolderId,
		[Title] = @Title
	WHERE
		[SizeTableFolderId] = @SizeTableFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTableFolder] (
			[ParentSizeTableFolderId],
			[Title]
		)
		VALUES (
			@ParentSizeTableFolderId,
			@Title
		)

		SET @SizeTableFolderId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableFolderId
END
GO
