SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportTagIds]
	
AS
BEGIN
	SET NOCOUNT ON
	
	select
		t.TagId,
		t.Value as TagTitle,
		t.CommonName,
		tg.Title as TagGroupTitle
	from 
		lekmer.tTag t inner join lekmer.tTagGroup tg on t.TagGroupId = tg.TagGroupId
    order by
		tg.Title
    
END

GO
