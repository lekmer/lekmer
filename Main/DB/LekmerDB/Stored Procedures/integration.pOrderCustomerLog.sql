SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pOrderCustomerLog]
	@CustomerId	int,
	@XmlIn		Text,
	@XmlOut		Text
AS
begin
	set nocount on
	begin try
	begin transaction
			
		insert into
			integration.tOrderLog(OrderId, OrderXmlIn, OrderXmlOut, Date, CustomerId)
		values
			(1, @XmlIn, @XmlOut, GETDATE(), @CustomerId)
		
	commit
	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('CustomerId: ' + @CustomerId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
