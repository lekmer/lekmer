SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pComponentGetTree]
AS
BEGIN	
	SELECT
		v.[Component.Id] AS 'Id',
		NULL AS 'ParentId',
		v.[Component.Title] AS 'Title',
		CAST(0 AS bit) AS 'HasChildren'
	FROM template.vCustomComponent AS v
END

GO
