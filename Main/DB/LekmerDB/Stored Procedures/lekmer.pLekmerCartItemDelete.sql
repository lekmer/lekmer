SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pLekmerCartItemDelete]
	@CartItemId int
as
begin
	delete
		lekmer.tLekmerCartItem
	where
		 CartItemId = @CartItemId
end
GO
