SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderDelete]
	@OrderId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE [orderlek].[tMaksuturvaPendingOrder]
	WHERE [OrderId] = @OrderId
END
GO
