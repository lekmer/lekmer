SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendGetById]
	@LanguageId INT,
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesRecommend] AS ber
	WHERE
		ber.[Block.BlockId] = @BlockId
		AND
		ber.[Block.LanguageId] = @LanguageId
END
GO
