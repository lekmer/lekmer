SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewUserGetByCustomer]
	@CustomerId INT
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewUser] rru
	WHERE
		rru.[RatingReviewUser.CustomerId] = @CustomerId
END
GO
