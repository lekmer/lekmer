
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRating_Save]
	@RatingId INT,
	@RatingFolderId INT,
	@RatingRegistryId INT,
	@CommonName VARCHAR(50),
	@CommonForVariants BIT,
	@Title NVARCHAR(50),
	@Description NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRating]
		WHERE [Title] = @Title
			AND [RatingId] <> @RatingId
			AND	[RatingFolderId] = @RatingFolderId
	)
	RETURN -1
	
	-- Same common names are not allowed in same registry (return -2)
	IF EXISTS
	(
		SELECT 1
		FROM [review].[tRating]
		WHERE [CommonName] = @CommonName
			AND [RatingRegistryId] = @RatingRegistryId
			AND [RatingId] <> @RatingId
	)
	RETURN -2
	
	UPDATE 
		[review].[tRating]
	SET 
		[RatingFolderId] = @RatingFolderId,
		[RatingRegistryId] = @RatingRegistryId,
		[CommonName] = @CommonName,
		[Title] = @Title,
		[Description] = @Description,
		[CommonForVariants] = @CommonForVariants
	WHERE
		[RatingId] = @RatingId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRating] (
			[RatingFolderId],
			[RatingRegistryId],
			[CommonName],
			[Title],
			[Description],
			[CommonForVariants]			
		)
		VALUES (
			@RatingFolderId,
			@RatingRegistryId,
			@CommonName,
			@Title,
			@Description,
			@CommonForVariants
		)

		SET @RatingId = SCOPE_IDENTITY()
	END

	RETURN @RatingId
END
GO
