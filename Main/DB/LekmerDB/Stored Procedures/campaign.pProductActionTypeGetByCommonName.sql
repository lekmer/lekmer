SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductActionTypeGetByCommonName]
	@ProductActionTypeCommonName	varchar(50)
AS 
BEGIN 
	SELECT 
		*
	FROM 
		campaign.vCustomProductActionType
	WHERE 
		[ProductActionType.CommonName] = @ProductActionTypeCommonName
END

GO
