SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemTranslationDelete]
	@RatingItemId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [review].[tRatingItemTranslation]
	WHERE [RatingItemId] = @RatingItemId
END
GO
