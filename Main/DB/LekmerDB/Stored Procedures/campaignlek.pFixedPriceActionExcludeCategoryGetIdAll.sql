
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[CategoryId]
	FROM 
		[campaignlek].[tFixedPriceActionExcludeCategory]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
