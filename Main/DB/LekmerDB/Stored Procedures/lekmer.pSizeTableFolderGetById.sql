SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetById]
	@SizeTableFolderId	INT
AS
BEGIN
	SELECT
		[stf].*
	FROM
		[lekmer].[vSizeTableFolder] stf
	WHERE
		[stf].[SizeTableFolder.SizeTableFolderId] = @SizeTableFolderId
END
GO
