SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryGetByUrlTitle]
	@LanguageId	INT,
	@UrlTitleOld NVARCHAR(256)
AS 
BEGIN 
	SELECT 
		CPUH.*
	FROM 
		[lekmer].[vCustomProductUrlHistory] CPUH
	WHERE
		CPUH.[ProductUrlHistory.LanguageId] = @LanguageId
		AND
		CPUH.[ProductUrlHistory.UrlTitleOld] = @UrlTitleOld
END
GO
