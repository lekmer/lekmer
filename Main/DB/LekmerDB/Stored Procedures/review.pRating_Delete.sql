
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRating_Delete]
	@RatingId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [review].[pRatingItemDeleteAll] @RatingId
	EXEC [review].[pRatingGroupRatingDelete] NULL, @RatingId
	EXEC [review].[pRatingTranslationDelete] @RatingId

	DELETE FROM [review].[tRating]
	WHERE [RatingId] = @RatingId
END
GO
