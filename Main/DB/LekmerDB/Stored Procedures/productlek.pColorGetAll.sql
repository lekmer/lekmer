SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pColorGetAll]
	@LanguageId INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		[c].*
	FROM 
		[productlek].[vColor] c
	WHERE
		[c].[Color.LanguageId] = @LanguageId
END
GO
