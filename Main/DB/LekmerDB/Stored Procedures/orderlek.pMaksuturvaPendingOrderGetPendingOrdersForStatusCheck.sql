SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderGetPendingOrdersForStatusCheck]
	@CheckToDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vMaksuturvaPendingOrder] po
	WHERE
		[po].[MaksuturvaPendingOrder.NextAttempt] <= @CheckToDate
END
GO
