SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pConditionTargetProductTypeSave]
	@ConditionId INT,
	@TargetProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@TargetProductTypeIds, @Delimiter)
	
	DELETE [campaignlek].[tConditionTargetProductType]
	WHERE
		[ConditionId] = @ConditionId
	
	INSERT [campaignlek].[tConditionTargetProductType] ( [ConditionId], [ProductTypeId] )
	SELECT
		@ConditionId,
		Id
	FROM
		@IdList
END
GO
