
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pProductImageSave]
	@ProductId int,
	@MediaId int,
	@ProductImageGroupId int,
	@Ordinal int
AS
BEGIN
	UPDATE	[product].[tProductImage] 
	SET
		ProductImageGroupId = @ProductImageGroupId,
		Ordinal = @Ordinal
	WHERE
		(ProductId = @ProductId)And(MediaId=@MediaId)
IF @@ROWCOUNT = 0
	INSERT [product].[tProductImage]
	(
		ProductId,
		MediaId,
		ProductImageGroupId,
		Ordinal
	)
	VALUES
	(
		@ProductId,
		@MediaId,
		@ProductImageGroupId,
		@Ordinal
	)
END
GO
