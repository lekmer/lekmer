SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderPaymentDeleteByOrder]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].[tOrderPayment]
	WHERE [OrderId] = @OrderId
END
GO
