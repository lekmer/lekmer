SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*
*****************  Version 1  *****************
User: Egorov V.		Date: 26.01.2009		Time: 21:11
Description:
			Created 
*/

CREATE procedure [template].[pIncludeFolderEdit]
	@IncludeFolderId		int,
	@ParentIncludeFolderId	int,
	@Title					nvarchar(50)
	
as
begin
	set nocount on
	
	--Check that parent folder has folders with same name
	if exists (select 1 from [template].[tIncludeFolder] where [Title] = @Title AND [IncludeFolderId] <> @IncludeFolderId And ((@ParentIncludeFolderId IS NULL And ParentIncludeFolderId IS NULL) OR ParentIncludeFolderId = @ParentIncludeFolderId))
		return -1
	
	update 
		template.[tIncludeFolder]
	set
		ParentIncludeFolderId = @ParentIncludeFolderId,
		[Title] = @Title
	where
		IncludeFolderId = @IncludeFolderId
	
return @IncludeFolderId
end





GO
