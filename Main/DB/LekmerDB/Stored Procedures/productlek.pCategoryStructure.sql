SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pCategoryStructure]
	@ChannelId INT
AS
BEGIN
	DECLARE @LanguageId INT
	SET @LanguageId = (SELECT [LanguageId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId)

	DECLARE @tUsedCategoryFirstLevelId TABLE (Id INT)
	DECLARE @tTree TABLE (CategoryFirstLevelTitle NVARCHAR(MAX), CategorySecondLevelTitle NVARCHAR(MAX), CategoryThirdLevelTitle NVARCHAR(MAX))

	DECLARE @CategoryFirstLevelId INT, @CategorySecondLevelId INT
	DECLARE @CategoryFirstLevelTitle NVARCHAR(50), @CategorySecondLevelTitle NVARCHAR(50), @CategoryThirdLevelTitle NVARCHAR(50)

	-- Get first level category
	SELECT TOP(1)
		@CategoryFirstLevelId = [c].[CategoryId],
		@CategoryFirstLevelTitle = COALESCE ([ct].[Title], [c].[Title])
	FROM [product].[tCategory] c
	LEFT JOIN [product].[tCategoryTranslation] ct on [ct].[CategoryId] = [c].[CategoryId] and [ct].[LanguageId] = @LanguageId
	WHERE [c].[ParentCategoryId] IS NULL
	ORDER BY COALESCE ([ct].[Title], [c].[Title])

	WHILE @CategoryFirstLevelTitle IS NOT NULL
	BEGIN
		-- Cursor by second level categories
		DECLARE category_cursor CURSOR FOR 
			SELECT
				[c].[CategoryId],
				COALESCE ([ct].[Title], [c].[Title])
			FROM [product].[tCategory] c
			LEFT JOIN [product].[tCategoryTranslation] ct on [ct].[CategoryId] = [c].[CategoryId] and [ct].[LanguageId] = @LanguageId
			WHERE [c].[ParentCategoryId] = @CategoryFirstLevelId
			ORDER BY COALESCE ([ct].[Title], [c].[Title])
				
		OPEN category_cursor
		FETCH NEXT FROM category_cursor INTO @CategorySecondLevelId, @CategorySecondLevelTitle

		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Fill category tree with titles
			INSERT INTO @tTree 
			SELECT
				@CategoryFirstLevelTitle,
				@CategorySecondLevelTitle,
				COALESCE ([ct].[Title], [c].[Title])
			FROM [product].[tCategory] c
			LEFT JOIN [product].[tCategoryTranslation] ct on [ct].[CategoryId] = [c].[CategoryId] and [ct].[LanguageId] = @LanguageId
			WHERE [c].[ParentCategoryId] = @CategorySecondLevelId
			ORDER BY COALESCE ([ct].[Title], [c].[Title])

			FETCH NEXT FROM category_cursor INTO @CategorySecondLevelId, @CategorySecondLevelTitle
		END

		CLOSE category_cursor
		DEALLOCATE category_cursor

		-- Get next first level category
		INSERT INTO @tUsedCategoryFirstLevelId ([Id]) VALUES (@CategoryFirstLevelId)
		SET @CategoryFirstLevelTitle = NULL
		SELECT TOP(1)
			@CategoryFirstLevelId = [c].[CategoryId],
			@CategoryFirstLevelTitle = COALESCE ([ct].[Title], [c].[Title])
		FROM [product].[tCategory] c
		LEFT JOIN [product].[tCategoryTranslation] ct on [ct].[CategoryId] = [c].[CategoryId] and [ct].[LanguageId] = @LanguageId
		WHERE [c].[ParentCategoryId] IS NULL AND [c].[CategoryId] NOT IN (SELECT Id FROM @tUsedCategoryFirstLevelId)
		ORDER BY COALESCE ([ct].[Title], [c].[Title])
	END

	SELECT * FROM @tTree
END
GO
