SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pSiteStructureRegistryGetAll]
as
begin
	select 
		ssr.*
	from 
		[sitestructure].[vCustomSiteStructureRegistry] ssr
	order by
		ssr.[SiteStructureRegistry.Title]
end

GO
