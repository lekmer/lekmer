
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pCartItemSave]
	@CartItemId INT,
	@CartId INT,
	@ProductId INT,
	@Quantity INT,
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[order].tCartItem
	SET	
		Quantity = @Quantity
	WHERE
		CartItemId = @CartItemId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	INTO [order].tCartItem
				( CartId,
				  ProductId,
				  Quantity,
				  CreatedDate            
           		)
		VALUES
				( @CartId,
				  @ProductId,
				  @Quantity,
				  @CreatedDate
           		)

		SET @CartItemId = SCOPE_IDENTITY()
	END
	
	RETURN @CartItemId
END
GO
