
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagSave]
	@TagId		INT,
	@TagGroupId INT,
	@Value		NVARCHAR(255),
	@CommonName VARCHAR(255)
AS
BEGIN
	IF EXISTS ( SELECT 1 
				FROM lekmer.tTag 
				WHERE 
					(Value = @Value AND TagGroupId = @TagGroupId AND TagId <> @TagId)
					OR (CommonName = @CommonName AND TagGroupId = @TagGroupId AND TagId <> @TagId)
			   )
		RETURN -1
	 
	UPDATE
		[lekmer].[tTag]
	SET
		[Value] = @Value,
		[CommonName] = @CommonName
	WHERE
		[TagId] = @TagId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tTag]
		( 
			[TagGroupId],
			[Value],
			[CommonName]
		)
		VALUES
		(
			@TagGroupId,
			@Value,
			@CommonName
		)
		
		SET @TagId = SCOPE_IDENTITY()						
	END 
	RETURN @TagId
END 
GO
