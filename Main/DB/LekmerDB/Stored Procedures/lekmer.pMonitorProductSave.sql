SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMonitorProductSave]
	@ProductId		INT,
	@ChannelId		INT,
	@Email			VARCHAR(320),
	@CreatedDate	DATETIME,
	@SizeId			INT
AS
BEGIN
	DECLARE @Id INT
	UPDATE
		[lekmer].[tMonitorProduct]
	SET
		[CreatedDate] = @CreatedDate,
		@Id = [MonitorProductId]
	WHERE
		[ProductId] = @ProductId AND
		[ChannelId] = @ChannelId AND
		[Email] = @Email AND
		((@SizeId IS NULL AND [SizeId] IS NULL) OR ([SizeId] = @SizeId))
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tMonitorProduct]
		(
			[ProductId],
			[ChannelId],
			[Email],
			[CreatedDate],
			[SizeId]
		)
		VALUES
		(
			@ProductId,
			@ChannelId,
			@Email,
			@CreatedDate,
			@SizeId
		)
		SET @Id = scope_identity()
	END
	RETURN @Id
END
GO
