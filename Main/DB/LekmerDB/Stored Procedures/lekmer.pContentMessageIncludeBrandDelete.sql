SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludeBrandDelete]
	@ContentMessageId	INT
AS
BEGIN
	SET NOCOUNT ON
		
	DELETE FROM [lekmer].[tContentMessageIncludeBrand]
	WHERE [ContentMessageId] = @ContentMessageId
END
GO
