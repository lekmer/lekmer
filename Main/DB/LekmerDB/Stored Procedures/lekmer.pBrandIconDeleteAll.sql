SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandIconDeleteAll]
	@BrandId	INT
AS 
BEGIN 
	DELETE 
		[lekmer].[tBrandIcon]
	WHERE
		[BrandId] = @BrandId
END
GO
