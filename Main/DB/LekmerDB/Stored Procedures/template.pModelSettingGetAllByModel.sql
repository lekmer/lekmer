SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSettingGetAllByModel]
    @ModelId INT
AS 
BEGIN
    SET NOCOUNT ON
    
    SELECT  *
    FROM    [template].[vCustomModelSetting]
    WHERE   [ModelSetting.ModelId] = @ModelId
END

GO
