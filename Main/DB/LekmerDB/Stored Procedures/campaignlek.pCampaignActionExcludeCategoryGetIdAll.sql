
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		[aec].[CategoryId],
		[aec].[IncludeSubCategories]
	FROM 
		[campaignlek].[tCampaignActionExcludeCategory] aec
	WHERE 
		[aec].[ConfigId] = @ConfigId
END
GO
