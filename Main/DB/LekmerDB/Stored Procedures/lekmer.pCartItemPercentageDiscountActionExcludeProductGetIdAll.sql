
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[aep].[ProductId]
	FROM
		[lekmer].[tCartItemPercentageDiscountActionExcludeProduct] aep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aep].[ProductId]
	WHERE
		[aep].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
