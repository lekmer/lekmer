SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaPendingOrderGet]
	@OrderId INT
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vMaksuturvaPendingOrder] po
	WHERE
		[po].[MaksuturvaPendingOrder.OrderId] = @OrderId
END
GO
