SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductAdd]
	@ProductRegistryProducts [lekmer].[ProductRegistryProducts] READONLY
AS
BEGIN
	DECLARE @ProductRestriction		 TABLE(ProductId INT,  ProductRegistryId INT)
	DECLARE @BrandRestriction		 TABLE(BrandId INT,	   ProductRegistryId INT)
	DECLARE @CategoryRestriction	 TABLE(CategoryId INT, ProductRegistryId INT)


	-- Get All Product Restrictions.
	INSERT INTO @ProductRestriction (ProductId, ProductRegistryId)
	EXEC [lekmer].[pProductRegistryRestrictionProductGetAll]

	-- Get All Brand Restrictions.
	INSERT INTO @BrandRestriction (BrandId, ProductRegistryId)
	EXEC [lekmer].[pProductRegistryRestrictionBrandGetAll]

	-- Get All Category Restrictions.	
	INSERT INTO @CategoryRestriction (CategoryId, ProductRegistryId)
	EXEC [lekmer].[pProductRegistryRestrictionCategoryGetAll]


	DECLARE @ProductIdsToInsert TABLE(ProductId INT, ProductRegistryId INT)
	INSERT INTO @ProductIdsToInsert (ProductId, ProductRegistryId)
	SELECT ProductId, ProductRegistryId FROM @ProductRegistryProducts


	-- Remove products that have any restrictions.
	DELETE pId FROM @ProductIdsToInsert pId
	INNER JOIN product.tProduct p ON p.ProductId = pId.ProductId
	INNER JOIN lekmer.tLekmerProduct lp ON lp.ProductId = p.ProductId
	WHERE EXISTS (SELECT 1 FROM @ProductRestriction WHERE ProductId = pId.ProductId AND ProductRegistryId = pId.ProductRegistryId)
		  OR EXISTS (SELECT 1 FROM @BrandRestriction WHERE BrandId = lp.BrandId AND ProductRegistryId = pId.ProductRegistryId)
		  OR EXISTS (SELECT 1 FROM @CategoryRestriction WHERE CategoryId = p.CategoryId AND ProductRegistryId = pId.ProductRegistryId)
		  OR EXISTS (SELECT 1 FROM product.tProductRegistryProduct WHERE ProductId = pId.ProductId AND ProductRegistryId = pId.ProductRegistryId)


	-- Add product to tProductRegistryProduct table.
	INSERT INTO product.tProductRegistryProduct (ProductId, ProductRegistryId)
	SELECT ProductId, ProductRegistryId FROM @ProductIdsToInsert
END
GO
