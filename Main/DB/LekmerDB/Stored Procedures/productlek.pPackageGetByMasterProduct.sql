SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageGetByMasterProduct]
	@MasterProductId	INT,
	@ChannelId			INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	     *
	FROM
	    [productlek].[vPackage]
	WHERE 
		[Package.MasterProductId] = @MasterProductId
		AND [ChannelId] = @ChannelId
END
GO
