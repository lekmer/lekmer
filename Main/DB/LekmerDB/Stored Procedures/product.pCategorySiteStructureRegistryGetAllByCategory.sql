SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategorySiteStructureRegistryGetAllByCategory]
	@CategoryId		int
as
begin
	select 
		C.CategoryId 'CategoryRegistry.CategoryId',
		R.SiteStructureRegistryId 'CategoryRegistry.SiteStructureRegistryId',
		vCR.[CategoryRegistry.ProductParentContentNodeId],
		vCR.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		product.tCategory C
		CROSS JOIN sitestructure.tSiteStructureRegistry R
		LEFT JOIN product.vCustomCategorySiteStructureRegistry vCR
			ON vCR.[CategoryRegistry.SiteStructureRegistryId] = R.[SiteStructureRegistryId]
			AND vCR.[CategoryRegistry.CategoryId] = C.CategoryId
	where 
		C.CategoryId = @CategoryId
	order by
		R.Title
end

GO
