SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pProductActionDelete]
	@ProductActionId int
AS 
BEGIN
	DELETE FROM campaign.tProductAction
	WHERE ProductActionId = @ProductActionId
END
GO
