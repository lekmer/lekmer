
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryGetAllByCategory]
	@CategoryId		INT
AS
BEGIN
	SELECT
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[CategoryId] 'ItemId',
		[rc].[RestrictionReason] 'RestrictionReason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM 
		[lekmer].[tProductRegistryRestrictionCategory] rc
		INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
	WHERE
		[rc].[CategoryId] = @CategoryId
END
GO
