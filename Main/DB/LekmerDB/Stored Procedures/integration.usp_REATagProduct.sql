
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_REATagProduct]
AS
BEGIN
	
	DECLARE @SalesTagId INT
		SET @SalesTagId = (SELECT TagId FROM lekmer.tTag WHERE Value = 'Sale')
	
	DECLARE @ActiveCampaigns TABLE 
	(
		CampaignId int primary key with (ignore_dup_key = on)
	)
	INSERT INTO @ActiveCampaigns (CampaignId)
	SELECT
		c.CampaignId
	FROM 
		campaign.tCampaign c
	WHERE
		c.EndDate > GETDATE()
	AND c.StartDate < GETDATE() --
	AND CampaignStatusId = 0
	-- HARD FIX TO EXCLUDE CAMPAIGNS UNDER "Product Campaigns > Leksaker > Varumärken" // PT47797233
	AND c.[FolderId] != 1000084
	
	
	-- Campaigns that are not to be tagged for one reason or another, poor solution but temporary.
	DECLARE @ExclusionCampaigns TABLE 
	(
		CampaignId int primary key with (ignore_dup_key = on)
	)
	INSERT INTO @ExclusionCampaigns (CampaignId)
	VALUES 
	(1000421),(1000418),(1000419),(1000420),
	-- 'Brands (SE)'
	(1000418),(1000490),(1000491),(1000492),(1000493),(1000494),(1000495),(1000496),(1000497),(1000498),(1000499),(1000500),
	-- 'Brands (NO)'
	(1000419),(1000501),(1000502),(1000503),(1000504),(1000505),(1000506),(1000507),(1000508),(1000509),(1000510),(1000511),
	-- 'Brands (DK)'
	(1000421),(1000523),(1000524),(1000525),(1000526),(1000527),(1000528),(1000529),(1000530),(1000531),(1000532),(1000533),
	-- 'Brands (FI)'
	(1000420),(1000512),(1000513),(1000514),(1000515),(1000516),(1000517),(1000518),(1000519),(1000520),(1000521),(1000522)
	
	-- 'Campaigns (SE)'
	--(1001486),
	-- 'Campaigns (NO)'
	--(1001487),
	-- 'Campaigns (DK)'
	--(1001488),
	-- 'Campaigns (FI)'
	--(1001489)

	
	DELETE 
		a
	FROM
		@ActiveCampaigns a INNER JOIN @ExclusionCampaigns e ON a.CampaignId = e.CampaignId
			
			
	DECLARE @ProductsToBeTaged TABLE 
	(
		ProductId int primary key with (ignore_dup_key = on)
	)
	
	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		b.ProductId
	FROM
		@ActiveCampaigns a
		INNER JOIN campaign.tProductAction c ON c.CampaignId = a.CampaignId
		INNER JOIN lekmer.tProductDiscountActionItem b ON b.ProductActionId = c.ProductActionId

	INSERT INTO @ProductsToBeTaged (ProductId)
	SELECT 
		ip.ProductId
	FROM
		@ActiveCampaigns a
		INNER JOIN campaign.tProductAction c ON c.CampaignId = a.CampaignId
		INNER JOIN [campaign].[tPercentagePriceDiscountAction] ppda ON [ppda].[ProductActionId] = [c].[ProductActionId]
		CROSS APPLY [integration].[fnGetPercentagePriceDiscountActionAffectedProducts] ([ppda].[ProductActionId]) ip

	DECLARE @ProductWithDiscountPrices TABLE
	(
		ChannelId INT,
		ProductId INT,
		SitePrice DECIMAL (16,2),
		PRIMARY KEY(ChannelId, ProductId)
	)
	
	INSERT INTO @ProductWithDiscountPrices
	EXEC [lekmer].[pAvailGetProductDiscountPriceOnSite]



	BEGIN TRY
	BEGIN TRANSACTION

		DELETE FROM lekmer.tProductTag WHERE TagId = @SalesTagId

		INSERT INTO lekmer.tProductTag(ProductId, TagId)
		SELECT
			p.ProductId,
			@SalesTagId
		FROM
			@ProductsToBeTaged p
			INNER JOIN @ProductWithDiscountPrices d
				ON p.ProductId = d.ProductId
				AND d.ChannelId = 1			
			INNER JOIN product.tPriceListItem i
				ON d.ProductId = i.ProductId
				AND i.PriceListId = 1
		WHERE
			d.SitePrice < i.PriceIncludingVat -- Only products with lower price than original price get tagged, lekmer uses campigns diffrently...
				
					
	COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
