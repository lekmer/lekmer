SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 12:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentPageSeoSettingGetByIdSecure]
@ContentNodeId	int
as
begin
	select cpss.*
	from [sitestructure].[vCustomContentPageSeoSettingSecure] cpss
	where cpss.[ContentPageSeoSetting.ContentNodeId] = @ContentNodeId
end

GO
