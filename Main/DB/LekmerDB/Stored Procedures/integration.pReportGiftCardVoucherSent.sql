
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportGiftCardVoucherSent]
	@OrderId	int
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		m.OrderId,
		m.VoucherCode,	
		m.DiscountValue,
		o.Email,
		c.Title as [Status],
		g.CreatedDate as [SendDate],
		ca.Title as CampaignTitle,
		cs.Title as CampaignStatus,
		ca.StartDate as CampaignStart,
		ca.EndDate as CampaignEnd,
		cr.CommonName as Channel
	FROM 
		[campaignlek].[tGiftCardViaMailInfo] m
		inner join [order].[tOrder] o on o.OrderId = m.OrderId
		inner join [messaging].[tRecipient] r on r.[Address] = o.Email
		inner join [messaging].[tMessage] g on g.MessageId = r.MessageId
		inner join [campaignlek].[tGiftCardViaMailInfoStatus] c on c.StatusId = m.StatusId
		inner join [campaign].[tCampaign] ca on m.CampaignId = ca.CampaignId
		inner join [campaign].[tCampaignStatus] cs on ca.CampaignStatusId = cs.CampaignStatusId
		INNER JOIN [campaign].[tCampaignModuleChannel] cmc ON [cmc].[ChannelId] = [o].[ChannelId]
		inner join [campaign].[tCampaignRegistry] cr on cr.CampaignRegistryId = [cmc].[CampaignRegistryId]
	WHERE 
		m.OrderId = @OrderId
	AND m.VoucherCode IS NOT NULL
	ORDER BY
		g.CreatedDate	

END
GO
