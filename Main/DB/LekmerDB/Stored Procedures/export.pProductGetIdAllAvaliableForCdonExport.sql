
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pProductGetIdAllAvaliableForCdonExport]
	@MonthPurchasedAgo INT
AS
BEGIN
	SELECT [p1].[ProductId]
	FROM [product].[tProduct] p1
		 INNER JOIN [lekmer].[tLekmerProduct] lp1 ON [lp1].[ProductId] = [p1].[ProductId]
	WHERE 
		[p1].[IsDeleted] = 0
		AND [lp1].[SellOnlyInPackage] = 0
		AND [p1].[ProductStatusId] = 0
		AND [lp1].[ProductTypeId] = 1
	UNION
	SELECT DISTINCT ([p2].[ProductId])
	FROM [product].[tProduct] p2
		 INNER JOIN [lekmer].[tLekmerProduct] lp2 ON [lp2].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p2].[ProductId]
		 INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderItemId] = [oip].[OrderItemId]
		 INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
	WHERE 
		[p2].[IsDeleted] = 0
		AND [lp2].[SellOnlyInPackage] = 0
		AND [p2].[ProductStatusId] = 1
		AND [o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > DATEADD(MONTH, -@MonthPurchasedAgo, GETDATE())
		AND [lp2].[ProductTypeId] = 1
END
GO
