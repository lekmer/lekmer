
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignSearchCartCampaigns]
@SearchString	nvarchar(MAX)
AS
BEGIN
	DECLARE @ProductId VARCHAR(50) = null;
	DECLARE @ProductIds VARCHAR(MAX);
	DECLARE @Title VARCHAR(MAX)
	SET @Title = [generic].[fPrepareSearchParameter](@SearchString)
	SET @ProductIds = @SearchString;

	DECLARE @IncludedCampaign TABLE 
	( 
		Id int NOT NULL IDENTITY(1,1),
		CartActionId INT,
		ErpId VARCHAR(50),
		CampaignId INT
	)

	DECLARE @ExludedCampaign TABLE 
	( 
		Id int NOT NULL IDENTITY(1,1),
		CartActionId INT,
		ErpId VARCHAR(50),
		CampaignId INT
	)
	SET @ProductIds = REPLACE(@ProductIds, ' ', '')

	WHILE LEN(@ProductIds) > 0
	BEGIN
		IF PATINDEX('%,%',@ProductIds) > 0
		BEGIN
			SET @ProductId = SUBSTRING(@ProductIds, 0, PATINDEX('%,%',@ProductIds))
			SET @ProductIds = SUBSTRING(@ProductIds, LEN(@ProductId + ',') + 1, LEN(@ProductIds))
		END
		ELSE
		BEGIN
			SET @ProductId = @ProductIds
			SET @ProductIds = NULL
		END

		DECLARE @ProductTypeId INT

		SELECT @ProductTypeId = lp.ProductTypeId
		FROM [lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p
		ON lp.ProductId = p.ProductId
		WHERE p.ErpId = @ProductId;

		DECLARE @CategoryId INT

		SELECT @CategoryId = p.CategoryId 
		FROM [product].[tProduct] p
		WHERE p.ErpId = @ProductId

		CREATE TABLE #CategoriesTemp
		(
			CategoryId INT,
			ParentCategoryId INT
		);

		WITH Categories AS
		(
			SELECT c.CategoryId, c.ParentCategoryId
			FROM [product].[tCategory] c
			WHERE c.CategoryId = @CategoryId
			UNION ALL
			SELECT c.CategoryId, c.ParentCategoryId
			FROM [product].[tCategory] c
			INNER JOIN Categories ci
			ON ci.ParentCategoryId = c.CategoryId
		)

		INSERT INTO #CategoriesTemp (CategoryId, ParentCategoryId)
		SELECT CategoryId, ParentCategoryId
		FROM Categories

		DECLARE @BrandId INT

		SELECT @BrandId = lp.BrandId
		FROM [lekmer].[tLekmerProduct] lp
		WHERE lp.[HYErpId] = @ProductId;

		--CART ITEM DISCOUNT
		--IncludedProducts
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [campaignlek].[tCartItemDiscountCartAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [campaignlek].[tCartItemDiscountCartActionIncludeProduct] cai
					INNER JOIN [product].[tProduct] p
					ON p.ProductId = cai.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
			AND
			(
				catt.ProductTypeId = @ProductTypeId
			)

		)
		--ExludedProducts
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [campaignlek].[tCartItemDiscountCartAction] cda
				WHERE cda.CartActionId IN
				(
						SELECT cai.CartActionId
						FROM [campaignlek].[tCartItemDiscountCartActionExcludeProduct] cai
						INNER JOIN [product].[tProduct] p
						ON p.ProductId = cai.ProductId
						WHERE p.ErpId = @ProductId
				)
			)
		)
		--IncludedCategories
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [campaignlek].[tCartItemDiscountCartAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cai.CartActionId
						FROM [campaignlek].[tCartItemDiscountCartActionIncludeCategory] cai
						INNER JOIN #CategoriesTemp ct
						ON ct.CategoryId = cai.CategoryId
						WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
					)
				)
			)
		)
		--ExcludedCategories
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [campaignlek].[tCartItemDiscountCartAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [campaignlek].[tCartItemDiscountCartActionExcludeCategory] cai
					INNER JOIN #CategoriesTemp ct
					ON ct.CategoryId = cai.CategoryId
					WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
				)
			)
		)
		--IncludedBrands
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [campaignlek].[tCartItemDiscountCartAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cidc.CartActionId
						FROM [campaignlek].[tCartItemDiscountCartActionIncludeBrand] cidc
						WHERE cidc.BrandId = @BrandId
					)
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign  (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [campaignlek].[tCartItemDiscountCartAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cidc.CartActionId
					FROM [campaignlek].[tCartItemDiscountCartActionExcludeBrand] cidc
					WHERE cidc.BrandId = @BrandId
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [campaignlek].[tCartItemDiscountCartAction] cda
					WHERE cda.IncludeAllProducts = 1
				)
			)
		)

		--CART ITEM PERCENTAGE DISCOUNT
		--IncludedProducts
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemPercentageDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] cai
					INNER JOIN [product].[tProduct] p
					ON p.ProductId = cai.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
			AND
			(
				catt.ProductTypeId = @ProductTypeId
			)

		)
		--ExludedProducts
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemPercentageDiscountAction] cda
				WHERE cda.CartActionId IN
				(
						SELECT cai.CartActionId
						FROM [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] cai
						INNER JOIN [product].[tProduct] p
						ON p.ProductId = cai.ProductId
						WHERE p.ErpId = @ProductId
				)
			)
		)
		--IncludedCategories
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [lekmer].[tCartItemPercentageDiscountAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cai.CartActionId
						FROM [lekmer].[tCartItemPercentageDiscountActionIncludeCategory] cai
						INNER JOIN #CategoriesTemp ct
						ON ct.CategoryId = cai.CategoryId
						WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
					)
				)
			)
		)
		--ExcludedCategories
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemPercentageDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [lekmer].[tCartItemPercentageDiscountActionExcludeCategory] cai
					INNER JOIN #CategoriesTemp ct
					ON ct.CategoryId = cai.CategoryId
					WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
				)
			)
		)
		--IncludedBrands
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [lekmer].[tCartItemPercentageDiscountAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cidc.CartActionId
						FROM [lekmer].[tCartItemPercentageDiscountActionIncludeBrand] cidc
						WHERE cidc.BrandId = @BrandId
					)
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign  (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemPercentageDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cidc.CartActionId
					FROM [lekmer].[tCartItemPercentageDiscountActionExcludeBrand] cidc
					WHERE cidc.BrandId = @BrandId
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM  [lekmer].[tCartItemPercentageDiscountAction] cda
					WHERE cda.IncludeAllProducts = 1
				)
			)
		)

		--CART ITEM GROUP FIXED DISCOUNT
		--IncludedProducts
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemGroupFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] cai
					INNER JOIN [product].[tProduct] p
					ON p.ProductId = cai.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
			AND
			(
				catt.ProductTypeId = @ProductTypeId
			)

		)
		--ExludedProducts
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemGroupFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
						SELECT cai.CartActionId
						FROM [lekmer].[tCartItemGroupFixedDiscountActionExcludeProduct] cai
						INNER JOIN [product].[tProduct] p
						ON p.ProductId = cai.ProductId
						WHERE p.ErpId = @ProductId
				)
			)
		)
		--IncludedCategories
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [lekmer].[tCartItemGroupFixedDiscountAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cai.CartActionId
						FROM [lekmer].[tCartItemGroupFixedDiscountActionIncludeCategory] cai
						INNER JOIN #CategoriesTemp ct
						ON ct.CategoryId = cai.CategoryId
						WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
					)
				)
			)
		)
		--ExcludedCategories
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemGroupFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] cai
					INNER JOIN #CategoriesTemp ct
					ON ct.CategoryId = cai.CategoryId
					WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
				)
			)
		)
		--IncludedBrands
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [lekmer].[tCartItemGroupFixedDiscountAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cidc.CartActionId
						FROM [lekmer].[tCartItemGroupFixedDiscountActionIncludeBrand] cidc
						WHERE cidc.BrandId = @BrandId
					)
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign  (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemGroupFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cidc.CartActionId
					FROM [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] cidc
					WHERE cidc.BrandId = @BrandId
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM  [lekmer].[tCartItemGroupFixedDiscountAction] cda
					WHERE cda.IncludeAllProducts = 1
				)
			)
		)

		--CART ITEM FIXED DISCOUNT
		--IncludedProducts
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [lekmer].[tCartItemFixedDiscountActionIncludeProduct] cai
					INNER JOIN [product].[tProduct] p
					ON p.ProductId = cai.ProductId
					WHERE p.ErpId = @ProductId
				)
			)
			AND
			(
				catt.ProductTypeId = @ProductTypeId
			)

		)
		--ExludedProducts
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
						SELECT cai.CartActionId
						FROM [lekmer].[tCartItemFixedDiscountActionExcludeProduct] cai
						INNER JOIN [product].[tProduct] p
						ON p.ProductId = cai.ProductId
						WHERE p.ErpId = @ProductId
				)
			)
		)
		--IncludedCategories
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [lekmer].[tCartItemFixedDiscountAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cai.CartActionId
						FROM [lekmer].[tCartItemFixedDiscountActionIncludeCategory] cai
						INNER JOIN #CategoriesTemp ct
						ON ct.CategoryId = cai.CategoryId
						WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
					)
				)
			)
		)
		--ExcludedCategories
		INSERT INTO @ExludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cai.CartActionId
					FROM [lekmer].[tCartItemFixedDiscountActionExcludeCategory] cai
					INNER JOIN #CategoriesTemp ct
					ON ct.CategoryId = cai.CategoryId
					WHERE (cai.IncludeSubCategories =1)OR(cai.CategoryId = @CategoryId)
				)
			)
		)
		--IncludedBrands
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM [lekmer].[tCartItemFixedDiscountAction] cda
					WHERE cda.CartActionId IN
					(
						SELECT cidc.CartActionId
						FROM [lekmer].[tCartItemFixedDiscountActionIncludeBrand] cidc
						WHERE cidc.BrandId = @BrandId
					)
				)
			)
		)

		--ExludeBrands
		INSERT INTO @ExludedCampaign  (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			(
				SELECT cda.CartActionId
				FROM [lekmer].[tCartItemFixedDiscountAction] cda
				WHERE cda.CartActionId IN
				(
					SELECT cidc.CartActionId
					FROM [lekmer].[tCartItemFixedDiscountActionExcludeBrand] cidc
					WHERE cidc.BrandId = @BrandId
				)
			)
		)

		--IncludeAll
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			LEFT JOIN [campaignlek].[tCartActionTargetProductType] catt
			ON ca.CartActionId = catt.CartActionId
			WHERE
			(
				catt.ProductTypeId = @ProductTypeId
			)
			AND
			( ca.CartActionId IN
				(
					SELECT cda.CartActionId
					FROM  [lekmer].[tCartItemFixedDiscountAction] cda
					WHERE cda.IncludeAllProducts = 1
				)
			)
		)

		--PRODUCT AUTO FEE
		--IncludedProducts
		INSERT INTO @IncludedCampaign (CartActionId, ErpId, CampaignId) 
		(
			SELECT ca.CartActionId, @ProductId, ca.CampaignId
			FROM [campaign].[tCartAction] ca
			WHERE ca.CartActionId IN
			( 
				SELECT paf.CartActionId
				FROM [campaignlek].[tProductAutoFreeCartAction] paf
				INNER JOIN [product].[tProduct] p
				ON p.ProductId = paf.ProductId
				WHERE p.ErpId = @ProductId
			)
		)

		IF(OBJECT_ID('tempdb..#CategoriesTemp') Is Not Null) DROP TABLE #CategoriesTemp
	END

	DECLARE @ResultTable Table
	(
		Id int NOT NULL IDENTITY(1,1),
		ProductActionId INT,
		ErpId VARCHAR(50),
		CampaignId INT
	)

	--remove dublicates
	DELETE FROM @IncludedCampaign
	WHERE Id NOT IN 
	(
		SELECT MIN(Id) _
		FROM @IncludedCampaign 
		GROUP BY CartActionId, ErpId, CampaignId
	) 

	DELETE FROM @ExludedCampaign
	WHERE Id NOT IN 
	(
		SELECT MIN(Id) _
		FROM @ExludedCampaign 
		GROUP BY CartActionId, ErpId, CampaignId
	) 

	--remove excluded
	INSERT INTO @ResultTable (ProductActionId, ErpId, CampaignId)
	(
		SELECT CartActionId, ErpId, CampaignId
		FROM @IncludedCampaign
		EXCEPT
		SELECT  CartActionId, ErpId, CampaignId
		FROM @ExludedCampaign
	)

	SELECT 
		c.*, rt.ErpId
	FROM 
		campaign.vCustomCampaign c
	INNER JOIN 
		campaign.tCartCampaign cc
	ON 
		cc.CampaignId = c.[Campaign.Id]
	INNER JOIN
	(
		SELECT  CampaignId, ErpId
		FROM @ResultTable 
		GROUP BY ErpId, CampaignId
	) rt
	ON c.[Campaign.Id] = rt.CampaignId
	UNION ALL
		SELECT 
		C.*, ''
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		ErpId ASC
END
GO
