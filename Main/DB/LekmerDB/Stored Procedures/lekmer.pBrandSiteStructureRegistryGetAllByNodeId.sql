SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureRegistryGetAllByNodeId]
	@NodeId	int
as
begin
	select
		*
	FROM
		lekmer.vBrandSiteStructureRegistry
	where 
		[BrandSiteStructureRegistry.ContentNodeId] = @NodeId
end

GO
