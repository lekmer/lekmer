
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeBrandGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionExcludeBrand]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
