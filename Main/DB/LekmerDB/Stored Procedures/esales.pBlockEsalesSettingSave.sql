SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pBlockEsalesSettingSave]
	@BlockId				INT,
	@EsalesModelComponentId	INT
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[esales].[tBlockEsalesSetting]
	SET	
		[EsalesModelComponentId] = @EsalesModelComponentId
	WHERE
		[BlockId] = @BlockId
  
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[esales].[tBlockEsalesSetting] (
			 [BlockId]
			,[EsalesModelComponentId]
		)
		VALUES (
			 @BlockId
			,@EsalesModelComponentId
		)
	END
END

GO
