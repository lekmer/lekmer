
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pOrderAddressSave]
	@OrderAddressId INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100),
	@Reference		NVARCHAR(50),
	@DoorCode		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension,
		Reference = @Reference,
		DoorCode = @DoorCode
	WHERE	
		OrderAddressId = @OrderAddressId

	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT INTO [orderlek].[tOrderAddress]
		(
			OrderAddressId,
			HouseNumber,
			HouseExtension,
			Reference,
			DoorCode
		)
		VALUES
		(
			@OrderAddressId,
			@HouseNumber,
			@HouseExtension,
			@Reference,
			@DoorCode
		)
	END
END
GO
