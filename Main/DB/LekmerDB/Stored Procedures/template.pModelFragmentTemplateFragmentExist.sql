SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pModelFragmentTemplateFragmentExist] 
	@ModelFragmentId INT
AS 
BEGIN
    SET NOCOUNT ON	

    IF EXISTS ( SELECT  *
                FROM    [template].[tTemplateFragment]
                WHERE   [ModelFragmentId] = @ModelFragmentId ) 
        RETURN 1

    RETURN 0
END
GO
