SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [security].[pUserRemoveRoles]
	@UserId int
as
begin
	delete
		security.tUserRole
	where
		SystemUserId = @UserId
end
GO
