
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_ImportUpdateProductSizesLekmer]
	@HYChannel NVARCHAR(40),
	@ProductId INT,
	@HYArticleNoColorSize NVARCHAR(50),
	@HYSizeId NVARCHAR (50),
	@HYSizeValue NVARCHAR (50),
	@NoInStock NVARCHAR(250),
	@Weight NVARCHAR (50),
	@StockStatusId INT
AS
BEGIN

	DECLARE 
		@Data NVARCHAR(4000),
		@SizeId INT,
		@NeedToInsertProductSize BIT
	
	IF @HYChannel = '001' -- Only SE channel
	IF @HYSizeId NOT IN ('000', '100', '223')
	IF NOT EXISTS(SELECT 1 FROM lekmer.tProductSize WHERE ErpId = @HYArticleNoColorSize)
	
	BEGIN TRY
		BEGIN TRANSACTION

		SET @Data = 'tProductSIZE Shoes: @HYArticleNoColorSize ' + @HYArticleNoColorSize + ' @HYChannel ' + @HYChannel + ' @HYSizeValue ' + @HYSizeValue

		SET @NeedToInsertProductSize = 0
		
		SET @SizeId = (SELECT SizeId FROM lekmer.tSize s WHERE s.ErpId = @HYSizeId)
		
		IF @SizeId IS NOT NULL
		SET @NeedToInsertProductSize = 1
		
		IF @NeedToInsertProductSize = 1
		BEGIN
			IF NOT EXISTS (SELECT 1
						   FROM [lekmer].tProductSize ps
						   WHERE ps.ProductId = @ProductId 
						   AND ps.SizeId = @SizeId)
			BEGIN
				INSERT INTO [lekmer].tProductSize (
					ProductId,
					SizeId,
					ErpId,
					NumberInStock,
					[Weight],
					[StockStatusId]
				)
				VALUES (
					@ProductId,
					@SizeId,
					@HYArticleNoColorSize,
					@NoInStock,
					CONVERT(DECIMAl(18,3), CONVERT(DECIMAl(18,3), @Weight) / 1000),
					@StockStatusId
				)
			END
		END
	
		COMMIT	
	END TRY
	
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		
		INSERT INTO [integration].[integrationLog] (
			Data,
			[Message],
			[Date],
			OcuredInProcedure
		)
		VALUES (
			@Data,
			ERROR_MESSAGE(),
			GETDATE(),
			ERROR_PROCEDURE()
		)

	END CATCH
		
END
GO
