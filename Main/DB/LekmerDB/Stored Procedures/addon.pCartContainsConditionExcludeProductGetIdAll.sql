
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeProductGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		P.ProductId
	FROM 
		[addon].[tCartContainsConditionExcludeProduct] cep
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [cep].[ProductId]
	WHERE 
		[cep].[ConditionId] = @ConditionId
		AND [p].[IsDeleted] = 0
END
GO
