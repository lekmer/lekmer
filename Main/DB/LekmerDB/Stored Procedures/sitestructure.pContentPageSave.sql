SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [sitestructure].[pContentPageSave]
	@ContentNodeId				int,
	@SiteStructureRegistryId	int,
	@TemplateId					int,
	@MasterTemplateId			int,
	@Title						nvarchar(200),
	@UrlTitle					varchar(200),
	@ContentPageTypeId			int,
	@MasterPageId				int,
	@IsMaster					bit		
as
BEGIN
	update
		[sitestructure].[tContentPage]
	set
		[TemplateId] = @TemplateId,
		[MasterTemplateId] = @MasterTemplateId,
		[Title] = @Title,
		[UrlTitle] = @UrlTitle,
		[ContentPageTypeId] = @ContentPageTypeId,
		[MasterPageId] = @MasterPageId,
		[IsMaster] = @IsMaster
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		insert
			[sitestructure].[tContentPage]
		(
			[ContentNodeId],
			[SiteStructureRegistryId],
			[TemplateId],
			[MasterTemplateId],
			[Title],
			[UrlTitle],
			[ContentPageTypeId],
			[MasterPageId],
			[IsMaster]
		)
		values
		(
			@ContentNodeId,
			@SiteStructureRegistryId,
			@TemplateId,
			@MasterTemplateId,
			@Title,
			@UrlTitle,
			@ContentPageTypeId,
			@MasterPageId,
			@IsMaster
		)
	end
end
GO
