SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pConditionGetAllByCampaignId]
	@CampaignId int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCondition
	WHERE
		[Condition.CampaignId] = @CampaignId
end

GO
