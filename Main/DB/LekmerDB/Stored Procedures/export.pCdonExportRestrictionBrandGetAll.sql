
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionBrandGetAll]
AS
BEGIN
	SELECT 
		[rb].CreatedDate 'CreatedDate',
		[rb].BrandId 'ItemId',
		[rb].ProductRegistryId 'ProductRegistryId',
		[rb].Reason 'Reason',
		[rb].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionBrand] rb
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rb].[ProductRegistryId]
END
GO
