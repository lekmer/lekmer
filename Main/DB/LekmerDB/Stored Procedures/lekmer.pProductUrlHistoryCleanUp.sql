SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryCleanUp]
	@MaxCount INT
AS
BEGIN
	DECLARE 
		@ProductId INT,
		@LanguageId INT,
		@Difference INT,
		@UrlHistoryCount INT

	DECLARE cur_urlHistory CURSOR FAST_FORWARD FOR
		SELECT
			puh.ProductId,
			puh.LanguageId
		FROM
			[lekmer].[tProductUrlHistory] puh
			GROUP BY ProductId, LanguageId

	OPEN cur_urlHistory
	FETCH NEXT FROM cur_urlHistory
		INTO
			@ProductId,
			@LanguageId
			
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @UrlHistoryCount = (SELECT COUNT(1) FROM [lekmer].[tProductUrlHistory] 
								WHERE ProductId = @ProductId
									  AND LanguageId = @LanguageId)
									  
		SET @Difference = @UrlHistoryCount - @MaxCount
		
		IF @Difference > 0
		BEGIN
			DELETE UrlHistory FROM 
			(SELECT TOP(@Difference) * 
			 FROM [lekmer].[tProductUrlHistory] 
			 WHERE ProductId = @ProductId
				   AND LanguageId = @LanguageId
			 ORDER BY LastUsageDate ASC) UrlHistory
		END
	
		FETCH NEXT FROM cur_urlHistory
		INTO
			@ProductId,
			@LanguageId
	END
	
	CLOSE cur_urlHistory
	DEALLOCATE cur_urlHistory
END
GO
