SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategoryGetAllByBlock]
	@LanguageId INT,
	@BlockId INT
AS
BEGIN
	SELECT
		b.*
	FROM
		[lekmer].[vBlockBrandTopListCategorySecure] b
		INNER JOIN [product].[vCustomCategory] AS c ON b.[BlockBrandTopListCategory.CategoryId] = c.[Category.Id]
	WHERE
		[BlockBrandTopListCategory.BlockId] = @BlockId
		AND c.[Category.LanguageId] = @LanguageId
END
GO
