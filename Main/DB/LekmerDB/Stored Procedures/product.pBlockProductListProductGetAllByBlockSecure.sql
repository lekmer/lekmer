SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pBlockProductListProductGetAllByBlockSecure]
	@ChannelId int,
	@BlockId int
as
begin
	
	DECLARE @CurrencyId int,@ProductRegistryId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	
	SELECT 
		bp.*,
		p.*,
		pli.*
	from 
		[product].[vCustomBlockProductListProduct] as bp
		inner join [product].[vCustomProductSecure] as p on bp.[BlockProductListProduct.ProductId] = p.[Product.Id] AND bp.[BlockProductListProduct.BlockId] = @BlockId
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = P.[Product.Id] AND PRP.ProductRegistryId = @ProductRegistryId
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, P.[Product.Id], PMC.PriceListRegistryId, NULL)
end

GO
