SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionDelete]
	@ConditionId INT
AS 
BEGIN
	EXEC [campaignlek].[pCartValueRangeConditionCurrencyDeleteAll] @ConditionId
	
	DELETE FROM [campaignlek].[tCartValueRangeCondition]
	WHERE ConditionId = @ConditionId
END
GO
