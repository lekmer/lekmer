SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemGroupFixedDiscountAction]
	WHERE
		[CartItemGroupFixedDiscountAction.CartActionId] = @CartActionId
END

GO
