SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionIncludeCategory]
	WHERE
		ConfigId = @ConfigId
END
GO
