SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockEsalesProductFilterGetByIdSecure]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		f.*,
		b.*,
		bes.*
	FROM
		[lekmer].[vBlockProductFilter] AS f
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON [f].[BlockProductFilter.BlockId] = [b].[Block.BlockId]
		INNER JOIN [esales].[vBlockEsalesSetting] AS bes ON [bes].[BlockEsalesSetting.BlockId] = [f].[BlockProductFilter.BlockId]
	WHERE
		f.[BlockProductFilter.BlockId] = @BlockId
END
GO
