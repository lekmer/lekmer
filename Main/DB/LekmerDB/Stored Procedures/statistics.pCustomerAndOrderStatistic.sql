SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pCustomerAndOrderStatistic]
	@ChannelId	INT,
	@DateFrom	DATETIME,
	@DateTo		DATETIME
AS
BEGIN
	DECLARE @tStatistic TABLE (
		OrdersInPeriod INT,
		UniqueEmailsInPeriod INT,
		UniqueEmailsInPeriodAndLast12Months INT,
		SalesInPeriodOfUniqueEmailsInPeriodAndLast12Months DECIMAL(16,2),

		OrdersInLast12Months INT,
		UniqueEmailsInLast12Months INT,
		UniqueEmailsAppearingOnceInLast12Months INT
	)

	DECLARE @12MonthsAgo DATETIME
	SET @12MonthsAgo = DATEADD(YEAR, -1, @DateFrom)	

	DECLARE @tTempOrderEmailForPeriod TABLE (OrderId INT,  Email VARCHAR(320))
	INSERT INTO @tTempOrderEmailForPeriod
	SELECT [o].[OrderId], LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[ChannelId] = @ChannelId
		AND [o].[CreatedDate] >= @DateFrom
		AND [o].[CreatedDate] <= @DateTo

	DECLARE @tTempEmail TABLE (Email VARCHAR(320) PRIMARY KEY WITH (IGNORE_DUP_KEY = ON))
	INSERT INTO @tTempEmail
	SELECT DISTINCT [e].[Email]
		FROM @tTempOrderEmailForPeriod e
		INNER JOIN [order].[tOrder] o ON LTRIM(RTRIM([o].[Email])) = [e].[Email]
		WHERE
			[o].[OrderStatusId] = 4
			AND [o].[ChannelId] = @ChannelId
			AND [o].[CreatedDate] >= @12MonthsAgo
			AND [o].[CreatedDate] < @DateFrom


	-- Number of orders in month X	
	INSERT INTO @tStatistic ([OrdersInPeriod])
	SELECT COUNT(1) FROM @tTempOrderEmailForPeriod

	-- Number of unique email addresses shopped in month X
	UPDATE @tStatistic
	SET [UniqueEmailsInPeriod] = (SELECT COUNT(DISTINCT [Email]) FROM @tTempOrderEmailForPeriod)

	-- Number of email adresses that is used in month X and at least one more time in the last 12 months
	UPDATE @tStatistic
	SET [UniqueEmailsInPeriodAndLast12Months] = (SELECT COUNT(1) FROM @tTempEmail)
	

	-- Sales in month X from email addresses that has shopped in month X and at least one more time in the last 12 months
	UPDATE @tStatistic
	SET [SalesInPeriodOfUniqueEmailsInPeriodAndLast12Months] = (
		SELECT SUM(ISNULL([p].[Price],0)
			+ ISNULL([o].[FreightCost],0)
			+ ISNULL([lo].[PaymentCost],0)
			+ ISNULL([lo].[OptionalFreightCost],0)
			+ ISNULL([lo].[DiapersFreightCost],0)) 
		FROM @tTempEmail e
		INNER JOIN @tTempOrderEmailForPeriod oe ON [oe].[Email] = [e].[Email]
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oe].[OrderId]
		INNER JOIN [lekmer].[tLekmerOrder] lo ON [lo].[OrderId] = [o].[OrderId]
		INNER JOIN [order].[tOrderPayment] p ON [p].[OrderId] = [o].[OrderId])

	------------------------------------------------------------------------

	SET @12MonthsAgo = DATEADD(MONTH, -12, GETDATE())
	
	INSERT INTO @tTempOrderEmailForPeriod
	SELECT [o].[OrderId], LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[ChannelId] = @ChannelId
		AND [o].[CreatedDate] > @12MonthsAgo

	-- Number of orders in the last 12 months
	UPDATE @tStatistic
	SET [OrdersInLast12Months] = (SELECT COUNT(1) FROM @tTempOrderEmailForPeriod)
	
	-- Number of unique email addresses in the last 12 months
	UPDATE @tStatistic
	SET [UniqueEmailsInLast12Months] = (SELECT COUNT(DISTINCT [Email]) FROM @tTempOrderEmailForPeriod)

	-- Number of email addresses only appearing once in the last 12 months
	;WITH TempEmails (Email) AS (
		SELECT [oe].[Email]
		FROM @tTempOrderEmailForPeriod oe
		GROUP BY [oe].[Email]
		HAVING COUNT(1) = 1
	)
	UPDATE @tStatistic
	SET UniqueEmailsAppearingOnceInLast12Months = (SELECT COUNT(1) FROM TempEmails)
	
	SELECT * FROM @tStatistic
END
GO
