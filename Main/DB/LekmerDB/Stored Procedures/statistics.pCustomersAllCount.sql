SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pCustomersAllCount]
	@ChannelId INT
AS
BEGIN
	-- Total number of unique email addresses in database
	SELECT
		COUNT(DISTINCT [o].[Email]) 'Customers'
	FROM
		[order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[ChannelId] = @ChannelId
END
GO
