SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [orderlek].[pQliroPendingOrderInsert]
	@ChannelId INT,
	@OrderId INT,
	@FirstAttempt DATETIME,
	@LastAttempt DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroPendingOrder]
			( [ChannelId],
			  [OrderId],
			  [FirstAttempt],
			  [LastAttempt]
			)
	VALUES
			( @ChannelId,
			  @OrderId,
			  @FirstAttempt,
			  @LastAttempt
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
