SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionExcludeCategory]
	WHERE
		ConfigId = @ConfigId
END
GO
