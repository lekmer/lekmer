SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderSave]
	@ContentMessageFolderId INT,
	@ParentContentMessageFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [lekmer].[tContentMessageFolder]
		WHERE [Title] = @Title
			AND [ContentMessageFolderId] <> @ContentMessageFolderId
			AND	((@ParentContentMessageFolderId IS NULL AND [ParentContentMessageFolderId] IS NULL)
				OR [ParentContentMessageFolderId] = @ParentContentMessageFolderId)
	)
	RETURN -1
	
	UPDATE 
		[lekmer].[tContentMessageFolder]
	SET 
		[ParentContentMessageFolderId] = @ParentContentMessageFolderId,
		[Title] = @Title
	WHERE
		[ContentMessageFolderId] = @ContentMessageFolderId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tContentMessageFolder] (
			[ParentContentMessageFolderId],
			[Title]
		)
		VALUES (
			@ParentContentMessageFolderId,
			@Title
		)

		SET @ContentMessageFolderId = SCOPE_IDENTITY()
	END

	RETURN @ContentMessageFolderId
END
GO
