
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignLandingPageTranslationWebTitleGetAll]
	@CampaignLandingPageId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] AS 'Id',
		[clpt].[CampaignLandingPageTranslation.LanguageId] AS 'LanguageId',
		[clpt].[CampaignLandingPageTranslation.WebTitle] AS 'Value'
	FROM
	    [campaignlek].[vCampaignLandingPageTranslation] clpt
	WHERE 
		[clpt].[CampaignLandingPageTranslation.CampaignLandingPageId] = @CampaignLandingPageId
	ORDER BY
		[clpt].[CampaignLandingPageTranslation.LanguageId]
END
GO
