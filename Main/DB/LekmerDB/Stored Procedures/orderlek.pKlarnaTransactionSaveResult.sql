
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionSaveResult]
	@KlarnaTransactionId INT,
	@ResponseCodeId INT,
	@KlarnaFaultCode INT,
	@KlarnaFaultReason NVARCHAR(MAX),
	@Duration BIGINT,
	@ResponseContent NVARCHAR(MAX)
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tKlarnaTransaction]
	SET
		[ResponseCodeId] = @ResponseCodeId,
		[KlarnaFaultCode] = @KlarnaFaultCode,
		[KlarnaFaultReason] = @KlarnaFaultReason,
		[Duration] = @Duration,
		[ResponseContent] = @ResponseContent
	WHERE
		[KlarnaTransactionId] = @KlarnaTransactionId
END
GO
