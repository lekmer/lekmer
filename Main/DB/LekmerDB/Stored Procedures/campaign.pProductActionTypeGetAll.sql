SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductActionTypeGetAll]
as
begin
	select
		*
	from
		campaign.vCustomProductActionType
end

GO
