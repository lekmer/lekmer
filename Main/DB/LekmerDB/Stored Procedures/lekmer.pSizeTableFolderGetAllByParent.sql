SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		[stf].*
	FROM
		[lekmer].[vSizeTableFolder] stf
	WHERE
		([stf].[SizeTableFolder.ParentSizeTableFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		([stf].[SizeTableFolder.ParentSizeTableFolderId] IS NULL AND @ParentId IS NULL)
END
GO
