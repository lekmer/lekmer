
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeGetAllByProduct]
	@ProductId	INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vProductSize]
	WHERE
		[ProductSize.ProductId] = @ProductId
	ORDER BY
		[Size.Ordinal]
END
GO
