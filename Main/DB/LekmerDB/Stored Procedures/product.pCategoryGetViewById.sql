
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategoryGetViewById]
	@CategoryId	INT,
	@ChannelId	INT
AS
BEGIN
	SELECT
		c.*
	FROM
		product.vCustomCategoryView AS c
	WHERE
		c.[Category.Id] = @CategoryId
		AND
		[c].[Category.ChannelId] = @ChannelId
END
GO
