
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportOnlineProducts]

AS
BEGIN
	SET NOCOUNT ON

	SELECT
		x.SizeErpId,
		x.Title,
		x.NumberInStock,
		(CASE WHEN SUM(x.[SE]) > 0 THEN 'x' ELSE '' END) AS 'SE',
		(CASE WHEN SUM(x.[NO]) > 0 THEN 'x' ELSE '' END) AS 'NO',
		(CASE WHEN SUM(x.[DK]) > 0 THEN 'x' ELSE '' END) AS 'DK',
		(CASE WHEN SUM(x.[FI]) > 0 THEN 'x' ELSE '' END) AS 'FI',
		(CASE WHEN SUM(x.[NL]) > 0 THEN 'x' ELSE '' END) AS 'NL'
	FROM
		(
		SELECT
			ISNULL(s.ErpId, l.HYErpId + '-000') AS SizeErpId,
			p.Title,
			COALESCE(s.NumberInStock, p.NumberInStock) AS NumberInStock,
			CASE WHEN r.ProductRegistryId = 1 THEN 1 END AS 'SE',
			CASE WHEN r.ProductRegistryId = 2 THEN 1 END AS 'NO',
			CASE WHEN r.ProductRegistryId = 3 THEN 1 END AS 'DK',
			CASE WHEN r.ProductRegistryId = 4 THEN 1 END AS 'FI',
			CASE WHEN r.ProductRegistryId = 1000005 THEN 1 END AS 'NL'
		FROM 
			lekmer.tLekmerProduct l WITH (NOLOCK)
			INNER JOIN product.tProduct p WITH (NOLOCK)
				ON l.ProductId = p.ProductId
			LEFT JOIN lekmer.tProductSize s WITH (NOLOCK)
				ON s.ProductId = p.ProductId
			INNER JOIN product.tProductRegistryProduct r WITH (NOLOCK)
				ON r.ProductId = p.ProductId
		WHERE
			p.ProductStatusId = 0
			AND (p.NumberInStock > 0 AND s.NumberInStock IS NULL
									OR (s.NumberInStock > 0))
		) x
	GROUP BY
	    x.SizeErpId,
		x.Title,
		x.NumberInStock
	ORDER BY
		x.Title,
	    x.SizeErpId,		
		x.NumberInStock
	
END
GO
