
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetMostHelpfulByProductAndVariants]
	@ChannelId				INT,
	@ProductIds				VARCHAR(MAX),
	@RatingId				INT,
	@RatingItemIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@RatingReviewStatusId	INT,
	@IsInappropriate		BIT,
	@CustomerId				INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		TOP(1) rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
		INNER JOIN [review].[vRatingItemProductVote] ripv 
			ON ripv.[RatingItemProductVote.RatingReviewFeedbackId] = rrf.[RatingReviewFeedback.RatingReviewFeedbackId]
		INNER JOIN [generic].[fnConvertIDListToTable](@RatingItemIds, @Delimiter) ri 
			ON ri.[ID] = ripv.[RatingItemProductVote.RatingItemId]
		INNER JOIN [product].[vCustomProduct] p 
			ON p.[Product.Id] = rrf.[RatingReviewFeedback.ProductId] 
			AND p.[Product.ChannelId] = rrf.[RatingReviewFeedback.ChannelId]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], @CustomerId)
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) pl ON pl.[ID] = p.[Product.Id]
	WHERE 
		rrf.[RatingReviewFeedback.ChannelId] = @ChannelId
		AND rrf.[RatingReviewFeedback.RatingReviewStatusId] = @RatingReviewStatusId
		AND rrf.[RatingReviewFeedback.Inappropriate] = @IsInappropriate
		AND ripv.[RatingItemProductVote.RatingId] = @RatingId
	ORDER BY
		rrf.[RatingReviewFeedback.LikeHit] DESC
END
GO
