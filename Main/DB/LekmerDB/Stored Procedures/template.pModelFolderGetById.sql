SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFolderGetById] @ModelFolderId INT

AS 
BEGIN
    SET NOCOUNT ON

    SELECT  *
    FROM    [template].[vCustomModelFolder]
    WHERE   [ModelFolder.Id] = @ModelFolderId
END

GO
