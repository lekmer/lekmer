
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pFacebookUserSave]
	@CustomerId			INT,
	@CustomerRegistryId INT,
	@FacebookId			NVARCHAR(MAX),
	@Name				NVARCHAR(MAX),
	@Email				NVARCHAR(MAX) = NULL,
	@CreatedDate		DATETIME,
	@IsAlternateLogin	BIT
AS
BEGIN
	SET NOCOUNT ON
	UPDATE
		[customerlek].[tFacebookCustomerUser]
	SET    
		[CustomerRegistryId] = @CustomerRegistryId,
		[Name] = @Name,
		[Email] = @Email
	WHERE 
		[CustomerId] = @CustomerId
		AND [FacebookId] = @FacebookId
		
	IF @@ROWCOUNT = 0	
	BEGIN
		INSERT INTO	[customerlek].[tFacebookCustomerUser] (
			[CustomerId],
			[CustomerRegistryId],
			[FacebookId],
			[Name],
			[Email],
			[CreatedDate],
			[IsAlternateLogin]
		)
		VALUES (
			@CustomerId,
			@CustomerRegistryId,
			@FacebookId,
			@Name,
			@Email,
			@CreatedDate,
			@IsAlternateLogin
		)
	END
	RETURN @CustomerId
END
GO
