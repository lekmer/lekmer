SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityDelete]
	@EntityId	int
AS
begin
	set nocount ON
	DELETE [template].[tModelFragmentEntity] WHERE EntityId = @EntityId
	DELETE [template].[tEntityFunction] WHERE EntityId = @EntityId
	DELETE [template].[tEntity] WHERE EntityId = @EntityId
end	




GO
