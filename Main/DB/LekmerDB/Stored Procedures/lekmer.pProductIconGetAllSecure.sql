SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductIconGetAllSecure]
	@ProductId	INT,
	@CategoryId	INT,
	@BrandId	INT = NULL
AS 
BEGIN 
	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i INNER JOIN 
		[lekmer].[tLekmerProductIcon] lpi ON lpi.IconId = i.[Icon.Id]
	WHERE lpi.ProductId = @ProductId

	UNION

	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i 
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE [bi].[BrandId] = @BrandId
	
	UNION
	
	SELECT i.*
	FROM 
		[lekmer].[vIconSecure] i 
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE [ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](@CategoryId))
END
GO
