SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pOrderGetToSendGfkStatistics]
	@ChannelId	INT,
	@StartDate	VARCHAR(50),
	@EndDate	VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.ChannelId] = @ChannelId
		AND [OrderStatus.CommonName] = 'OrderInHY'
		AND [Order.CreatedDate] >= @StartDate
		AND [Order.CreatedDate] < @EndDate
END
GO
