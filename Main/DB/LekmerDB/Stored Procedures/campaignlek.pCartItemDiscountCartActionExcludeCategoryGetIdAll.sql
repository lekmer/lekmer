
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[campaignlek].[tCartItemDiscountCartActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
