SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignStatusGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCampaignStatus
	WHERE
		[CampaignStatus.Id] = @Id
end

GO
