SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandTopListCategorySave]
	@BlockId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	UPDATE [lekmer].[tBlockBrandTopListCategory]
	SET
		[IncludeSubcategories] = @IncludeSubcategories
	WHERE
		[BlockId] = @BlockId
		AND [CategoryId] = @CategoryId
		
	IF @@ROWCOUNT <> 0 RETURN
		
	INSERT [lekmer].[tBlockBrandTopListCategory] (
		[BlockId],
		[CategoryId],
		[IncludeSubcategories]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@IncludeSubcategories
	)
END
GO
