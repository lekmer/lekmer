SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pCountryGetAll]
as
begin
	set nocount on

	SELECT *
	FROM core.vCustomCountry
end

GO
