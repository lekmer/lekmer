SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeProductDeleteAllByProduct]
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		ip
	FROM
		[export].[tCdonExportIncludeProduct] ip
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) AS pl ON pl.[ID] = [ip].[ProductId]
END
GO
