SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pBlockRegisterGetByIdSecure]
@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		br.*,
		b.*
	FROM
		[customer].[vCustomBlockRegister] AS br
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON br.[BlockRegister.BlockId] = b.[Block.BlockId]
	WHERE
		br.[BlockRegister.BlockId] = @BlockId
END

GO
