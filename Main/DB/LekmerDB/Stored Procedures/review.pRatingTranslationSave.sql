SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingTranslationSave]
	@RatingId		INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[review].[tRatingTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[RatingId] = @RatingId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [review].[tRatingTranslation] (
			[RatingId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES (
			@RatingId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END
GO
