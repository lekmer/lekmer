SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pSirProductsGetChangesFromDate]
	@FromDate DATETIME
AS
BEGIN
	SET NOCOUNT ON;
	
	IF @FromDate IS NOT NULL
		SELECT *
		FROM [integration].[tSirProductsFull]
		WHERE [UpdatedDate] > @FromDate
		ORDER BY [ArticleNumber]
	ELSE
		SELECT *
		FROM [integration].[tSirProductsFull]
		ORDER BY [ArticleNumber]

END
GO
