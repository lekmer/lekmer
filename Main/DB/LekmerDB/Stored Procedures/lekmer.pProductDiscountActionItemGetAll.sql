
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemGetAll]
	@ActionId	INT,
	@SortBy		VARCHAR(20) = NULL
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)
	SET @sql = 'SELECT
					[pdai].*
				FROM 
					[lekmer].[vProductDiscountActionItem] pdai
					INNER JOIN [product].[tProduct] p ON [pdai].[ProductDiscountActionItem.ProductId] = [p].[ProductId]
				WHERE
					[p].[IsDeleted] = 0
					AND [pdai].[ProductDiscountActionItem.ActionId] = ' + CAST(@ActionId AS VARCHAR(10))

	IF @SortBy IS NOT NULL 
    BEGIN
        SET @sql = @sql + ' ORDER BY ' + @SortBy
    END

	EXEC SP_EXECUTESQL @sql
END
GO
