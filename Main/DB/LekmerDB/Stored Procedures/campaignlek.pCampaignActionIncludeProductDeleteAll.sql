SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductDeleteAll]
	@ConfigId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCampaignActionIncludeProduct]
	WHERE
		ConfigId = @ConfigId
END
GO
