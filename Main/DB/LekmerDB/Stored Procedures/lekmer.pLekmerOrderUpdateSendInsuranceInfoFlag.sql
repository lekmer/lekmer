SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerOrderUpdateSendInsuranceInfoFlag]
	@OrderIds		VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	UPDATE 
		o
	SET 
		[o].[NeedSendInsuranceInfo] = 0
	FROM 
		[lekmer].[tLekmerOrder] o
		INNER JOIN [generic].[fnConvertIDListToTableWithOrdinal](@OrderIds, @Delimiter) AS ol ON [ol].[Id] = [o].[OrderId]
END
GO
