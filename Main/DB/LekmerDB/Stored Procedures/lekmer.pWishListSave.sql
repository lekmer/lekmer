
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pWishListSave]
	@Id						int,
	@Key					uniqueidentifier,
	@Title					varchar(500),
	@CreatedDate			datetime,
	@UpdatedDate			datetime,
	@IpAddress				varchar(50),
	@NumberOfRemovedItems	int
as
begin
	update [lekmer].[tWishList] 
	set
		WishListTitle			= @Title,
		CreatedDate				= @CreatedDate,
		UpdatedDate				= @UpdatedDate,
		IpAddress				= @IpAddress,
		NumberOfRemovedItems	= @NumberOfRemovedItems
	where
		WishListId				= @Id
		
	if @@rowcount = 0
	begin	
		insert into [lekmer].[tWishList] 
		(
			WishListKey,
			WishListTitle,
			CreatedDate,
			UpdatedDate,
			IpAddress,
			NumberOfRemovedItems
		)
		values
		(
			@Key,
			@Title,
			@CreatedDate,
			@UpdatedDate,
			@IpAddress,
			@NumberOfRemovedItems
		)
		set @Id = scope_identity()		
	end

	return @Id
end
GO
