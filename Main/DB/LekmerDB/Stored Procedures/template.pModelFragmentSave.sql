SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************  Version 1  **********************************************
User: Volodymdyr Y.		Date: 20/01/2009                            >Creation*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelFragmentSave]
	@ModelFragmentId INT,
	@ModelFragmentRegionId INT,
	@Title NVARCHAR(50),
	@CommonName NVARCHAR(50),
	@ModelId INT,
	@Ordinal INT,
	@Size INT
AS 
BEGIN
    SET NOCOUNT ON
    
    UPDATE  [template].[tModelFragment]
    SET     [ModelFragmentRegionId] = @ModelFragmentRegionId,
			[Title] = @Title,
			[CommonName] = @CommonName,
			[ModelId] = @ModelId,
			[Ordinal] = @Ordinal,
			[Size] = @Size
    WHERE   [ModelFragmentId] = @ModelFragmentId 
       
    IF  @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tModelFragment] (
			[ModelFragmentRegionId],
			[Title],
			[CommonName],
			[ModelId],
			[Ordinal],
			[Size]
		) VALUES ( 
			@ModelFragmentRegionId,
			@Title,
			@CommonName,
			@ModelId,
			@Ordinal,
			@Size
		)

        SET @ModelFragmentId = SCOPE_IDENTITY()
    END
       
    RETURN @ModelFragmentId
END
GO
