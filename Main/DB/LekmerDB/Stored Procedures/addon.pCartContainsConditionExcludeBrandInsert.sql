SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeBrandInsert]
	@ConditionId	INT,
	@BrandId		INT
AS
BEGIN
	INSERT [addon].[tCartContainsConditionExcludeBrand] (
		ConditionId,
		BrandId
	)
	VALUES (
		@ConditionId,
		@BrandId
	)
END
GO
