
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageGeneralInfoTranslationGetAllByPackage]
	@PackageId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [PackageTranslation.PackageId]   AS 'Id',
		[PackageTranslation.LanguageId]  AS 'LanguageId',
		[PackageTranslation.GeneralInfo] AS 'Value'
	FROM
	    [productlek].[vPackageTranslation] 
	WHERE
		[PackageTranslation.PackageId] = @PackageId
	ORDER BY
		[PackageTranslation.LanguageId]
END
GO
