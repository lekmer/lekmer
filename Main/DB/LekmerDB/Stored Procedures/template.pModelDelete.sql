SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelDelete] 
	@ModelId INT
AS 
BEGIN
    SET NOCOUNT ON	

	UPDATE [template].[tModel] SET DefaultTemplateId = NULL WHERE ModelId = @ModelId
	
    DELETE  [template].[tModel]
    WHERE   [ModelId] = @ModelId
    
    RETURN 0
END
GO
