SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountCartAction]
	WHERE
		[FixedDiscountCartAction.CartActionId] = @CartActionId
END
GO
