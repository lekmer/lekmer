SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-23
-- Description:	Gets all media folders
-- =============================================
CREATE PROCEDURE [media].[pMediaFolderGetAll]	
AS
BEGIN
	SET NOCOUNT ON
	SELECT 
		*
	FROM   [media].[vCustomMediaFolder]	
END

GO
