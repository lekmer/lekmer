SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberGetById]
	@Id	INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[lekmer].[vNewsletterSubscriber]
	WHERE
		[NewsletterSubscriber.SubscriberId] = @Id
END
GO
