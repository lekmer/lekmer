SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportCustomerSegmentListGetCategoryAllByIdList]
	@CategoryIds	VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		[c].[Category.Id],
		[c].[Category.Title],
		[c].[Category.ErpId]
	FROM [product].[vCategorySecure] c
	INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, ',') sc ON [sc].[ID] = [c].[Category.Id]
END
GO
