SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@MonetaryValue	DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountCartActionCurrency] (
		CartActionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@MonetaryValue
	)
END
GO
