SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [addon].[pCartItemPriceActionIncludeProductDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionIncludeProduct
	where CartActionId = @CartActionId
end
GO
