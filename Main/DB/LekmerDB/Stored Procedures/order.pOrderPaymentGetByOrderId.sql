SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderPaymentGetByOrderId]
	@OrderId		INT
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM
		[order].[vCustomOrderPayment]
	WHERE
		[OrderPayment.OrderId] = @OrderId
END

GO
