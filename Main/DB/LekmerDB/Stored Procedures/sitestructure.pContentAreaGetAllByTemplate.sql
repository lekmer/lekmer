SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentAreaGetAllByTemplate]
@TemplateId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentArea] ca
	where
		ca.[ContentArea.TemplateId] = @TemplateId
	order by
		ca.[ContentArea.Title]
end

GO
