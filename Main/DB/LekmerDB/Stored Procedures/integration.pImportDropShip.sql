SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pImportDropShip]
	@FilePath NVARCHAR(MAX)
AS
BEGIN
	BEGIN TRY
		TRUNCATE TABLE [integration].[tempDropShip]
		
		DECLARE @sqlBulkInsert NVARCHAR(MAX)
		SET @sqlBulkInsert = 'BULK INSERT [integration].[tempDropShip]
							  FROM ''' + @FilePath + ''' 
							  WITH (FIRSTROW = 2, FIELDTERMINATOR = '';'', ROWTERMINATOR = ''\n'', CODEPAGE = ''ACP'')'
		EXEC(@sqlBulkInsert);
		
		BEGIN TRANSACTION
		-- insert new Drop Ship data
		INSERT INTO [orderlek].[tDropShip] (
			[ExternalOrderId],
			[OfferId],
			[Email],
			[Phone],
			[SocialSecurityNumber],
			[BillingFirstName],
			[BillingLastName],
			[BillingCompany],
			[BillingCo],
			[BillingStreet],
			[BillingStreetNr],
			[BillingEntrance],
			[BillingFloor],
			[BillingApartmentNumber],
			[BillingZip],
			[BillingCity],
			[BillingCountry],
			[DeliveryFirstName],
			[DeliveryLastName],
			[DeliveryCompany],
			[DeliveryCo],
			[DeliveryStreet],
			[DeliveryStreetNr],
			[DeliveryFloor],
			[DeliveryEntrance],
			[DeliveryZip],
			[DeliveryCity],
			[DeliveryCountry],
			[Discount],
			[Comment],
			[SupplierNo],
			[OrderId],
			[OrderDate],
			[CustomerNumber],
			[NotificationNumber],
			[PaymentMethod],
			[ProductArticleNumber],
			[ProductTitle],
			[Quantity],
			[Price],
			[ProductDiscount],
			[ProductSoldSum],
			[ProductStatus],
			[InvoiceDate],
			[VATproc],
			[CreatedDate])
		SELECT
			[ds].[ExternalOrderId], 
			[ds].[OfferId],
			[ds].[Email],
			[ds].[Phone],
			[ds].[SocialSecurityNumber],
			[ds].[BillingFirstName],
			[ds].[BillingLastName],
			[ds].[BillingCompany],
			[ds].[BillingCo],
			[ds].[BillingStreet],
			[ds].[BillingStreetNr],
			[ds].[BillingEntrance],
			[ds].[BillingFloor],
			[ds].[BillingApartmentNumber],
			[ds].[BillingZip],
			[ds].[BillingCity],
			[ds].[BillingCountry],
			[ds].[DeliveryFirstName],
			[ds].[DeliveryLastName],
			[ds].[DeliveryCompany],
			[ds].[DeliveryCo],
			[ds].[DeliveryStreet],
			[ds].[DeliveryStreetNr],
			[ds].[DeliveryFloor],
			[ds].[DeliveryEntrance],
			[ds].[DeliveryZip],
			[ds].[DeliveryCity],
			[ds].[DeliveryCountry],
			[ds].[Discount],
			[ds].[Comment],
			[ds].[SupplierNo],
			[ds].[OrderId],
			[ds].[OrderDate],
			[ds].[CustomerNumber],
			[ds].[NotificationNumber],
			[ds].[PaymentMethod],
			[ds].[ProductArticleNumber],
			[ds].[ProductTitle],
			[ds].[Quantity],
			[ds].[Price],
			[ds].[ProductDiscount],
			[ds].[ProductSoldSum],
			[ds].[ProductStatus],
			[ds].[InvoiceDate],
			[ds].[VATproc],
			GETDATE()		
		FROM [integration].[tempDropShip] ds
			
		COMMIT
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0 ROLLBACK
		INSERT INTO [integration].[integrationLog] (Data, [Message], [Date], OcuredInProcedure)
		VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
