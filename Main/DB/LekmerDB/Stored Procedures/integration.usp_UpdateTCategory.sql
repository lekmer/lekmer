SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_UpdateTCategory]
	-- Add the parameters for the stored procedure here
AS
BEGIN
		-- tCategory
		declare @Data nvarchar(4000)
		
		declare @HYarticleId nvarchar(50), --needas inte
				@ArticleClassId nvarchar(5),
				@ArticleClassTitle nvarchar(40),
				@ArticleGroupId nvarchar(5),
				@ArticleGroupTitle nvarchar(40),
				@ArticleCodeId nvarchar(5),
				@ArticleCodeTitle nvarchar(40)
						
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleID,
				tp.ArticleClassId ,
				tp.ArticleClassTitle,
				tp.ArticleGroupId,
				tp.ArticleGroupTitle,
				tp.ArticleCodeId,
				tp.ArticleCodeTitle
			from
				[integration].tempProduct tp
				left join lekmer.tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,17)
				left join product.tProduct p
					on p.ProductId = lp.ProductId
				left join product.tCategory c
					on c.CategoryId = p.CategoryId
			where
				lp.HYErpId is null
				or 'C_' +
					isnull(tp.ArticleClassId, '') + '-' +
					isnull(tp.ArticleGroupId, '') + '-' +
					isnull(tp.ArticleCodeId, '') <> c.ErpId
		
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @ArticleClassId,
				 @ArticleClassTitle,
				 @ArticleGroupId,
				 @ArticleGroupTitle,
				 @ArticleCodeId,
				 @ArticleCodeTitle
		
		while @@FETCH_STATUS = 0
		begin

			begin try
				begin transaction
	
				-- Check if varuklassId exists, if not create it	
				
				/*IF not exists (SELECT  FROM t where epid='CAT_%varuklassid')
				  --- make insert
				  -- if exists get ID for category use it for parentId later
				
				IF not exists (SELECT * FROM t where epid='CAT_%varugruppid-%varuklass')
				  -- make insert
				  -- if exists get ID for category use it for parentId later
			
				IF not exists (SELECT * FROM t where epid='CAT_%varugruppid-%varuklass-%varukod')*/	
		
	
				-- varuklass
				IF not exists (SELECT 1 
					FROM product.tCategory tc
					WHERE tc.ErpId = 'C_' + @ArticleClassId)
				begin
				--SET IDENTITY_INSERT [product].tCategory ON
					INSERT INTO 
						[product].tCategory(ParentCategoryId, Title, ErpId)
					SELECT 	
						null,
						@ArticleClassTitle,		
						'C_' + @ArticleClassId
						
				--SET IDENTITY_INSERT [product].tCategory OFF
				end

				-- Varugrupp
				IF not exists (SELECT 1 
					FROM product.tCategory tc
					WHERE tc.ErpId = 'C_' + @ArticleClassId+'-' +@ArticleGroupId)
				begin
					INSERT INTO 
						[product].tCategory(ParentCategoryId, Title, ErpId)
					SELECT 	
						(SELECT CategoryId from product.tCategory
						WHERE ErpId = 'C_' + @ArticleClassId),
						@ArticleGroupTitle,		
						'C_' + @ArticleClassId+'-' +@ArticleGroupId
						
				end
				
				-- Varukod
				IF not exists (SELECT 1 
					FROM product.tCategory tc
					WHERE tc.ErpId = 'C_' + @ArticleClassId+'-' +@ArticleGroupId+'-' +@ArticleCodeId)
				begin
					INSERT INTO 
						[product].tCategory(ParentCategoryId, Title, ErpId)
					SELECT 	
						(SELECT CategoryId from product.tCategory
						WHERE ErpId = 'C_' + @ArticleClassId + '-' + @ArticleGroupId),
						@ArticleCodeTitle,		
						'C_' + @ArticleClassId+'-' +@ArticleGroupId+'-' +@ArticleCodeId

				SET @Data = @ArticleClassId+'-' +@ArticleGroupId+'-' +@ArticleCodeId
				end
			
				--print 'Success!'
			
				-- ändra referensen på categoryId om den bytat
					UPDATE 
						p
					SET 
						p.CategoryId = (select categoryId from product.tCategory where ErpId = 'C_' + @ArticleClassId+'-' +@ArticleGroupId+'-' +@ArticleCodeId)
					FROM 
						[lekmer].tLekmerProduct lp						
							inner join product.tProduct p
								on p.ProductId = lp.ProductId				
					WHERE 
						lp.HYErpId = substring(@HYArticleId, 5,17) and
						p.CategoryId <> (select categoryId from product.tCategory where ErpId = 'C_' + @ArticleClassId+'-' +@ArticleGroupId+'-' +@ArticleCodeId)
				
				commit
			end try
			
			begin catch
				--print 'ROLLBACK PÅ EN RAD'
				if @@TRANCOUNT > 0 rollback
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
				--print 'Failure!'
				close cur_product -- behövs den?
				deallocate cur_product -- behövs den?
			end catch
			
			fetch next from cur_product into 
				 @HYArticleId,
				 @ArticleClassId,
				 @ArticleClassTitle,
				 @ArticleGroupId,
				 @ArticleGroupTitle,
				 @ArticleCodeId,
				 @ArticleCodeTitle
		end
		
		close cur_product
		deallocate cur_product	
        
        -- en delete här som plockar bort nivå 3 om det inte finns produkter länkade till den
        -- plocka ut de categoryId från tProdukt som inte är länkade till nån produkt id
        -- försök deleta dem
        
			/*delete pli
			from product.tCategory pli
			where pli.CategoryId in ((select c.CategoryId
										from product.tCategory c
										where Len(ErpId) >= 12)
										EXCEPT
										(select distinct CategoryId
										from product.tProduct))*/
        
        
END
GO
