SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionExcludeGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT 
		[CustomerGroupId]
	FROM 
		[campaignlek].[tCustomerGroupConditionExclude] 
	WHERE 
		[ConditionId] = @ConditionId 
END
GO
