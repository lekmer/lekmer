SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 2  *****************
User: Dovhun.		Date: 26.11.2008		Time: 16:40
Description:
			Edited

*/
/*
*****************  Version 1  *****************
User: Victor E.		Date: 22.10.2008		Time: 16:40
Description:
			Created 
*****************  Version 3  *****************
User: Yuriy P.		Date: 22.01.2009
Description:
			added Image
*/
CREATE PROCEDURE [template].[pTemplateGetAllByModelCommonName]
	@CommonName	varchar(50)
AS
BEGIN
	SELECT
		T.*
	FROM
		[template].[vCustomTemplate] T
		inner join [template].[vCustomModel] as M	on  T.[Template.ModelId] = M.[Model.Id]
	WHERE
		M.[Model.CommonName] = @CommonName
END

GO
