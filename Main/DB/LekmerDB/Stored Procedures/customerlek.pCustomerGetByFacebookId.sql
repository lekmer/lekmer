
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pCustomerGetByFacebookId]
	@ChannelId	INT,
	@FacebookId NVARCHAR(50)	
AS	
BEGIN
	SET NOCOUNT ON

	SELECT
		*	
	FROM
		[customer].[vCustomCustomer]
	WHERE
	    [FacebookCustomerUser.FacebookId] = @FacebookId
	    AND [Customer.CustomerRegistryId] IN
	    (
			SELECT [CustomerRegistryId] FROM [customer].[tCustomerModuleChannel]
			WHERE [ChannelId] = @ChannelId
	    )
END
GO
