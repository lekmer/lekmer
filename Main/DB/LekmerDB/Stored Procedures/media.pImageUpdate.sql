SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [media].[pImageUpdate]
	@MediaId INT,
	@AlternativeText NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	    UPDATE [media].[tImage]
	    SET    [AlternativeText] = @AlternativeText
	    WHERE  [MediaId] = @MediaId	
	RETURN @MediaId
END
GO
