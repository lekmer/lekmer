SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [performance].[pCheckIndexFragmentation]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @minFragmentation FLOAT = 5.0 /* in percent, will not defrag if fragmentation less than specified */

	DECLARE @db_id SMALLINT;

	SET @db_id = DB_ID();

	IF @db_id IS NULL
	BEGIN
		PRINT N'Invalid database'
		RETURN
	END

	-- Conditionally select tables and indexes from the sys.dm_db_index_physical_stats function 
	-- and convert object and index IDs to names.
	SELECT
		ips.[index_id],
		s.[name],
		o.[name],
		i.[name],
		ips.[avg_fragmentation_in_percent],
		ips.[page_count],
		i.[allow_page_locks]
	FROM
		sys.dm_db_index_physical_stats(@db_id, NULL, NULL, NULL, 'LIMITED') ips
		JOIN sys.indexes i ON [ips].[index_id] = [i].[index_id] AND [ips].[object_id] = [i].[object_id]
		JOIN sys.objects o ON [i].[object_id] = [o].[object_id]
		JOIN sys.schemas s ON [o].[schema_id] = [s].[schema_id]
	WHERE
		avg_fragmentation_in_percent >= @minFragmentation
		AND ips.index_id > 0 -- ignore heaps
	ORDER BY
		s.[name], o.[name], i.[name]

END
GO
