SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupGetAllByCustomer]
@CustomerId	int
AS
BEGIN
	SELECT
		CG.*
	FROM
		[customer].[vCustomCustomerGroup] AS CG
		INNER JOIN [customer].[tCustomerGroupCustomer] AS CGC 
			ON CGC.CustomerGroupId = CG.[CustomerGroup.CustomerGroupId]
	WHERE
		CGC.CustomerId = @CustomerId
END

GO
