SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartValueConditionGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCartValueCondition
	WHERE
		[Condition.Id] = @Id
end

GO
