
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductGetAllWithChannel]
AS
BEGIN
	SELECT
		[rp].[ProductRegistryId] 'ProductRegistryId',
		[rp].[ProductId] 'ItemId',
		[rp].[RestrictionReason] 'RestrictionReason',
		[rp].[UserId] 'UserId',
		[rp].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [lekmer].[tProductRegistryRestrictionProduct] rp
	INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
END
GO
