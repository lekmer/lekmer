SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionBrandDeleteAllByBrand]
	@BrandIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	DELETE
		rb
	FROM
		[export].[tCdonExportRestrictionBrand] rb
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) AS pl ON pl.[ID] = [rb].[BrandId]
END
GO
