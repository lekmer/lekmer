
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pLanguageGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[core].[vCustomLanguage]
	ORDER BY
		[Language.Id]
END

GO
