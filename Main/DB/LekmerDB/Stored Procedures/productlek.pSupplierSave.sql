SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pSupplierSave]
	@SupplierId		INT,
	@IsDropShip		BIT,
	@DropShipEmail	VARCHAR(320)
AS
BEGIN
	UPDATE
		[productlek].[tSupplier]
	SET
		[IsDropShip]	= @IsDropShip,
		[DropShipEmail] = @DropShipEmail
	WHERE
		[SupplierId] = @SupplierId
END
GO
