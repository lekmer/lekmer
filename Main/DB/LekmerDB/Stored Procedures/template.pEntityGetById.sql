SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityGetById]
	@EntityId	int
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomEntity] 
	where
		[Entity.Id] = @EntityId
end

GO
