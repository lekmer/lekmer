SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pAdFolderSave]
	@AdFolderId INT,
	@ParentFolderId INT,
	@Title NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON

	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [esales].[tAdFolder]
		WHERE [Title] = @Title
			AND [AdFolderId] <> @AdFolderId
			AND	(
				(@ParentFolderId IS NULL AND [ParentFolderId] IS NULL)
				OR
				[ParentFolderId] = @ParentFolderId
				)
	)
	RETURN -1

	UPDATE
		[esales].[tAdFolder]
	SET
		[ParentFolderId] = @ParentFolderId,
		[Title] = @Title
	WHERE
		[AdFolderId] = @AdFolderId

	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [esales].[tAdFolder] (
			[ParentFolderId],
			[Title]
		)
		VALUES (
			@ParentFolderId,
			@Title
		)

		SET @AdFolderId = SCOPE_IDENTITY()
	END

	RETURN @AdFolderId
END
GO
