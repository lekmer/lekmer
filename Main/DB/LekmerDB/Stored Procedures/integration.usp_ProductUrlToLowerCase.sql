SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ProductUrlToLowerCase]
	
AS
begin
	set nocount on
		
	begin try
		begin transaction
		
		update
			lekmer.tProductUrl
		set
			UrlTitle = lower(UrlTitle)
	
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
