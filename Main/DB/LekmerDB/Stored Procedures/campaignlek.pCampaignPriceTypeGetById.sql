SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignPriceTypeGetById]
	@Id INT
AS
BEGIN
	SELECT 
		[cpt].*
	FROM 
		[campaignlek].[vCampaignPriceType] cpt
	WHERE
		[cpt].[CampaignPriceType.Id] = @Id
END
GO
