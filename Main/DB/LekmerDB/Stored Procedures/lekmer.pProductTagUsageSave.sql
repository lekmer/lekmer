SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagUsageSave]
	@ProductId INT,
	@TagId INT,
	@ObjectId INT
AS
BEGIN
	DECLARE @ProductTagId INT
	SET @ProductTagId = (SELECT [ProductTagId] FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @TagId)
	IF (@ProductTagId IS NOT NULL)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTagUsage] WHERE [ProductTagId] = @ProductTagId AND [ObjectId] = @ObjectId)
			INSERT INTO [lekmer].[tProductTagUsage] (
				[ProductTagId],
				[ObjectId]
			)
			VALUES (
				@ProductTagId,
				@ObjectId
			)
	END
END
GO
