
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionProductGetAll]
AS
BEGIN
	SELECT 
		[rp].CreatedDate 'CreatedDate',
		[rp].ProductId 'ItemId',
		[rp].ProductRegistryId 'ProductRegistryId',
		[rp].Reason 'Reason',
		[rp].UserId 'UserId',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionProduct] rp
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rp].[ProductRegistryId]
END
GO
