
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ImportUpdateProductHeppo]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategoryHeppo] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrandHeppo
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,11)	-- 5,17 innan
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			p.Title <> tp.ArticleTitle OR
			p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock
		 
		print 'End product update'
		
		---------------------------------------------
		--print 'Start lekmer product update'
		--exec integration.usp_ImportUpdateLekmerProduct
		--print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICE]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,11) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.ProductRegistryId)
			
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				SET @HYarticleSize = substring(@HYarticleId, 17,3)
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,11)
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFokNoSize)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFokNoSize, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						0 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
					print 'insert into lekmer.tlekmerProduct'		
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, ShowVariantRelations, ProductTypeId)
					values(
					@ProductId, 
					@HYarticleIdNoFokNoSize,
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					0, -- ShowVariantRelations
					1 -- ProductTypeId
					)
					print 'insert into product.tPriceListItem'
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, @ProductId, @Price, (@Price * ((100-@Vat)/100)), @Vat)
					print 'insert into product.tProductReg'		
					-- tProductRegistryProduct
					EXEC [lekmer].[pProductRegistryProductInsert]  @ProductId, NULL,NULL, @Fok, ','
					
				
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tProductRegistryProduct prp
							where lp.HYErpId = @HYarticleIdNoFokNoSize -- @HYarticleNoFok innan
							and lp.ProductId = prp.ProductId 
							and prp.ProductRegistryId = @Fok)
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize --@HYarticleIdNoFoK innan
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					print 'insert into product.tproductRegProg (in else)'	
					-- tProductRegistryProduct
					EXEC [lekmer].[pProductRegistryProductInsert]  @ProductId, NULL,NULL, @Fok, ','
					
					print 'insert into product.rPriceList (in else)'	
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize), --@HYarticleIdNoFoK innan),
					@Price, (@Price * ((100-@Vat)/100)), @Vat)
					
				end
				
				
				-- placeringen funkar inte, kommer bara gå in hit om produkten inte är
				-- i alla 4 foks...
				-- om den specifika sizen av produkten inte finns i tProductSize
				/*
				if not exists (select 1
						from lekmer.tProductSize ps
						where ps.ErpId = @HYarticleIdNoFoK)
				begin
				set @Data = 'NEW tProductSIZE: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						@ProductId,
						1, --(select SizeId from lekmer.tSize where ErpId = @HYarticleSize),
						@HYarticleIdNoFoK,
						@NoInStock
					
				end
			*/
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId
		end
		
		close cur_product
		deallocate cur_product
		

		-- hantera storlekarna på bilderna
		print '[integration].[usp_ImportUpdateProductSizesHeppo]  körs'
		exec [integration].[usp_ImportUpdateProductSizesHeppo]
				
END
GO
