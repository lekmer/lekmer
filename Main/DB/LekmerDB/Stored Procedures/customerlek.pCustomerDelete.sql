
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pCustomerDelete]
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE [customer].[tCustomerUser]
	WHERE [CustomerId] = @CustomerId

	DELETE [customerlek].[tFacebookCustomerUser]
	WHERE [CustomerId] = @CustomerId

	DELETE [customer].[tCustomerComment]
	WHERE [CustomerId] = @CustomerId

	DELETE [customer].[tCustomerGroupCustomer]
	WHERE [CustomerId] = @CustomerId

	DELETE [customer].[tCustomer]
	WHERE [CustomerId] = @CustomerId
END
GO
