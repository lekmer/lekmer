SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pSiteStructureRegistryGetById]
@SiteStructureRegistryId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomSiteStructureRegistry]
	where 
		[SiteStructureRegistry.Id] = @SiteStructureRegistryId
end

GO
