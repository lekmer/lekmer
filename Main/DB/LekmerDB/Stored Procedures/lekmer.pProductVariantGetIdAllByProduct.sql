SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductVariantGetIdAllByProduct]
	@ProductId		INT,
	@RelationListTypeId	INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		DISTINCT rlp.[ProductId]
	FROM
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.[RelationListId] = rl.[RelationListId]
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.[RelationListId] = rlp.[RelationListId]
	WHERE
		prl.[ProductId] = @ProductId
		AND rlp.[ProductId] <> @ProductId
		AND rl.[RelationListTypeId] = @RelationListTypeId
END
GO
