
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionDelete]
	@ProductActionId INT
AS 
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT ConfigId FROM [campaign].[tPercentagePriceDiscountAction] WHERE ProductActionId = @ProductActionId)

	DELETE FROM [campaign].[tPercentagePriceDiscountAction]
	WHERE ProductActionId = @ProductActionId
	
	EXEC [campaignlek].[pCampaignActionConfiguratorDeleteAll] @ConfigId
END
GO
