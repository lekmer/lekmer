SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportOrdersGetAllByDate]
	@ChannelId INT,
	@DateFrom DATETIME,
	@DateTo DATETIME
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		o.[OrderId] AS 'Order.ID',
		o.[Email] AS 'Order.Email',
		o.[CreatedDate] AS 'Order.CreatedDate',
		op.[Price] + o.[FreightCost] + ISNULL(lo.[OptionalFreightCost], 0) + lo.[PaymentCost] AS 'Order.TotalValue'
	FROM
		[order].[tOrder] o
		INNER JOIN [order].[tOrderPayment] op ON op.[OrderId] = o.[OrderId]
		INNER JOIN [lekmer].[tLekmerOrder] lo ON lo.[OrderId] = o.[OrderId]
	WHERE
		o.[ChannelId] = @ChannelId
		AND
		o.[CreatedDate] > @DateFrom
		AND
		o.[CreatedDate] < @DateTo
		AND
		o.[OrderStatusId] = 4
	ORDER BY
		o.[OrderId]

END
GO
