SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pVoucherActionValueSave]
	@CartActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN
	UPDATE
		[lekmer].[tVoucherActionCurrency]
	SET 
		[Value] = @Value
	WHERE 
		CartActionId = @CartActionId AND 
		CurrencyId = @CurrencyId
		
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tVoucherActionCurrency]
		( 
			CartActionId,
			CurrencyId,
			[Value]
		)
		VALUES 
		(
			@CartActionId,
			@CurrencyId,
			@Value
		)
	END
END
GO
