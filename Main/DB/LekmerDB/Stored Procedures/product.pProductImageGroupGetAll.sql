SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductImageGroupGetAll]
AS
BEGIN
	SELECT
		*
	FROM 
		product.vCustomProductImageGroup
END

GO
