SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionGetBatchIdsExcludeByConditionId]
	@ConditionId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		BatchId
	FROM
		lekmer.[tVoucherBatchesExclude]
	WHERE
		[ConditionId] = @ConditionId
END
GO
