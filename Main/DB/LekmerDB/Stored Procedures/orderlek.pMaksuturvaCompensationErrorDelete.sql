SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorDelete]
	@MaksuturvaCompensationErrorId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	DELETE [orderlek].[tMaksuturvaCompensationError]
	WHERE [MaksuturvaCompensationErrorId] = @MaksuturvaCompensationErrorId
END
GO
