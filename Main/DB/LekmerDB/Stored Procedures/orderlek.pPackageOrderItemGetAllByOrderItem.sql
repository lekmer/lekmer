SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemGetAllByOrderItem]
	@OrderItemId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vPackageOrderItem]
	WHERE
		[PackageOrderItem.OrderItemId] = @OrderItemId
END
GO
