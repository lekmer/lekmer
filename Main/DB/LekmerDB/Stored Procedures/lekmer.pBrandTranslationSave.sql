
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandTranslationSave]
	@BrandId		INT,
	@LanguageId		INT,
	@Title			NVARCHAR(500),
	@Description	NVARCHAR(MAX),
	@PackageInfo	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		lekmer.tBrandTranslation
	SET
		[Title] = @Title,
		[Description] = @Description,
		[PackageInfo] = @PackageInfo
	WHERE
		[BrandId] = @BrandId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tBrandTranslation
		(
			[BrandId],
			[LanguageId],
			[Title],
			[Description],
			[PackageInfo]	
		)
		VALUES
		(
			@BrandId,
			@LanguageId,
			@Title,
			@Description,
			@PackageInfo
		)
	END
END
GO
