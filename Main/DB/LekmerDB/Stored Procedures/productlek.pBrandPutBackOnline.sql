SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBrandPutBackOnline]
AS
BEGIN
	-- Put brands back online when a brand gets a product online (automatic)
	
	-- Brand is offline
	-- Product is online
	-- And product is not restricted
	-----

	/*
	SELECT
		b.[BrandId],
		p.[ProductId],
		cr3.[CategoryId],
		cr2.[CategoryId],
		cr1.[CategoryId],
		pr.[ProductId]
	*/
	UPDATE
		b
	SET
		b.[IsOffline] = 0 -- Online
	FROM
		[product].[tProduct] p
		
		INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = p.[ProductId]
		INNER JOIN [lekmer].[tBrand] b ON b.[BrandId] = lp.[BrandId]
		
		LEFT OUTER JOIN [product].[tCategory] c3 ON c3.[CategoryId] = p.[CategoryId]
		LEFT OUTER JOIN [product].[tCategory] c2 ON c2.[CategoryId] = c3.[ParentCategoryId]
		LEFT OUTER JOIN [product].[tCategory] c1 ON c1.[CategoryId] = c2.[ParentCategoryId]
		
		LEFT OUTER JOIN [lekmer].[tProductRegistryRestrictionBrand] br ON br.[BrandId] = b.[BrandId]
		
		LEFT OUTER JOIN [lekmer].[tProductRegistryRestrictionCategory] cr3 ON cr3.[CategoryId] = c3.[CategoryId]
		LEFT OUTER JOIN [lekmer].[tProductRegistryRestrictionCategory] cr2 ON cr2.[CategoryId] = c2.[CategoryId]
		LEFT OUTER JOIN [lekmer].[tProductRegistryRestrictionCategory] cr1 ON cr1.[CategoryId] = c1.[CategoryId]
		
		LEFT OUTER JOIN [lekmer].[tProductRegistryRestrictionProduct] pr ON pr.[ProductId] = p.[ProductId]
		
	WHERE
		b.[IsOffline] = 1 -- Offline
		AND
		p.[ProductStatusId] = 0 -- Online
		AND -- no restrictions
		(
			br.[BrandId] IS NULL
			AND
			cr3.[CategoryId] IS NULL
			AND
			cr2.[CategoryId] IS NULL
			AND
			cr1.[CategoryId] IS NULL
			AND
			pr.[ProductId] IS NULL
		)

END
GO
