SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockContactUsGetById]
@LanguageId INT,
@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		bc.*,
		b.*
	FROM
		[lekmer].[vBlockContactUs] AS bc
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bc.[BlockContactUs.BlockId] = b.[Block.BlockId]
	WHERE
		bc.[BlockContactUs.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId 
END

GO
