SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListSearchAdvanced]
	@Title NVARCHAR(50),
	@CurrencyId	INT,
	@StatusId	INT,
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = NULL,
	@SortDescending	BIT = NULL
AS
BEGIN
	
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY pl.[' 
			+ COALESCE(@SortBy, 'PriceList.Id')
			+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			pl.*
		FROM
			[product].[vCustomPriceList] pl
		WHERE NULL IS NULL '
          + CASE WHEN (@Title IS NOT NULL) THEN + 'AND pl.[PriceList.Title] LIKE ''%' + @Title + '%''' ELSE '' END
          + CASE WHEN (@StatusId IS NOT NULL) THEN + 'AND pl.[PriceList.StatusId] = @StatusId ' ELSE '' END
          + CASE WHEN (@CurrencyId IS NOT NULL) THEN + 'AND pl.[PriceList.CurrencyId] = @CurrencyId ' ELSE '' END
		
	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@Title		nvarchar(50),
			@CurrencyId	int,
			@StatusId	int',
			@Title,
			@CurrencyId,
			@StatusId

	EXEC sp_executesql @sql, 
		N'	@Title		nvarchar(50),
			@CurrencyId	int,
			@StatusId	int',
			@Title,
			@CurrencyId,
			@StatusId
END

GO
