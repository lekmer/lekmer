
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pQliroTimeoutOrderGetAll]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[qt].[TransactionId] 'TimeoutOrder.TransactionId',
		[qt].[StatusCode] 'TimeoutOrder.StatusCode',
		[qt].[InvoiceStatus] 'TimeoutOrder.InvoiceStatus',
		[qt].[OrderId] 'TimeoutOrder.OrderId',
		[qt].[ReservationNumber] 'TimeoutOrder.ReservationNumber',
		[o].[ChannelId] 'TimeoutOrder.ChannelId'
	FROM
		[orderlek].[tQliroTransaction] qt
		INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [qt].[OrderId]
	WHERE
		[qt].[TransactionTypeId] = 7 -- ReserveAmountTimeout
		AND
		(
			[qt].[TimeoutProcessed] IS NULL
			OR
			[qt].[TimeoutProcessed] <> 1
		)
		AND [o].[OrderStatusId] = 11 --PaymentTimeout
END
GO
