SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryGetAll]
AS
BEGIN
	SELECT
		*
	FROM 
		[product].[tProductRegistry]
END
GO
