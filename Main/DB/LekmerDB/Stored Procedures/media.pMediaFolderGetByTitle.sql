SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pMediaFolderGetByTitle]
	@Title nvarchar(50)
as
begin
	select
		*
	from
		[media].vCustomMediaFolder
	where
		[MediaFolder.Title] = @Title
end

GO
