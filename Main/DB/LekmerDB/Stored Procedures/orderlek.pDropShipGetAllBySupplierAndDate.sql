SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDropShipGetAllBySupplierAndDate]
	@SupplierNo INT = NULL,
	@DateFrom DATETIME = NULL,
	@DateTo DATETIME = NULL
AS
BEGIN
	DECLARE @sql NVARCHAR(MAX)

	SET @sql = '
		SELECT
			[ds].[DropShip.SupplierNo],
			[s].[Supplier.Name],
			[ds].[DropShip.ProductArticleNumber],
			[ds].[DropShip.OfferId],
			[ds].[DropShip.ProductTitle],
			[ds].[DropShip.Price],
			[ds].[DropShip.OrderId],
			[o].[CreatedDate]
		FROM
			[orderlek].[vDropShip] ds
			INNER JOIN [productlek].[vSupplier] s ON [s].[Supplier.SupplierNo] = [ds].[DropShip.SupplierNo]
			INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [ds].[DropShip.OrderId]
		WHERE
			[ds].[DropShip.ProductArticleNumber] IS NOT NULL'
			+ CASE WHEN (@SupplierNo IS NOT NULL) THEN + ' AND [ds].[DropShip.SupplierNo] = @SupplierNo' ELSE '' END
			+ CASE WHEN (@DateFrom IS NOT NULL) THEN + ' AND [o].[CreatedDate] >= @DateFrom' ELSE '' END
			+ CASE WHEN (@DateTo IS NOT NULL) THEN + ' AND [o].[CreatedDate] <= @DateTo' ELSE '' END
		+ ' ORDER BY
			[ds].[DropShip.OrderId]'
		
	EXEC sp_executesql @sql, 
		N'	
			@SupplierNo INT = NULL,
			@DateFrom DATETIME = NULL,
			@DateTo DATETIME = NULL',
			@SupplierNo,
			@DateFrom,
			@DateTo
END
GO
