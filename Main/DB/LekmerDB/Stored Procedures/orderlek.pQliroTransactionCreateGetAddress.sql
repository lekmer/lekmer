
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionCreateGetAddress]
	@ClientRef VARCHAR(50),
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [TransactionTypeId],
			  [CivicNumber],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @TransactionTypeId,
			  @CivicNumber,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
