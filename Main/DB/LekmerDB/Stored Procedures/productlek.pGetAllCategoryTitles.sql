SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pGetAllCategoryTitles]
AS
BEGIN
	DECLARE @tUsedCategoryFirstLevelId TABLE (Id INT)
	DECLARE @tCategoryTitles TABLE (CategoryId INT, CategoryTitle NVARCHAR(MAX), CategoryLevel INT)

	DECLARE @CategoryFirstLevelId INT, @CategorySecondLevelId INT
	DECLARE @CategoryFirstLevelTitle NVARCHAR(50), @CategorySecondLevelTitle NVARCHAR(50)
	DECLARE @CategoryFirstLevelErpId NVARCHAR(50), @CategorySecondLevelErpId NVARCHAR(50)

	-- Get first level category
	SELECT TOP(1)
		@CategoryFirstLevelId = [c].[CategoryId],
		@CategoryFirstLevelTitle = [c].[Title],
		@CategoryFirstLevelErpId = [c].[ErpId]		
	FROM [product].[tCategory] c
	WHERE [c].[ParentCategoryId] IS NULL
	ORDER BY [c].[Title]

	WHILE @CategoryFirstLevelTitle IS NOT NULL
	BEGIN
		-- Fill tCategoryTitles with first level categories
		INSERT INTO @tCategoryTitles
		VALUES (
			@CategoryFirstLevelId,
			@CategoryFirstLevelTitle + ' (' + CAST(@CategoryFirstLevelId AS NVARCHAR(50)) + ' : ' + @CategoryFirstLevelErpId + ')', 1
		)

		-- Cursor by second level categories
		DECLARE category_cursor CURSOR FOR 
			SELECT
				[c].[CategoryId],
				[c].[Title],
				[c].[ErpId]
			FROM [product].[tCategory] c
			WHERE [c].[ParentCategoryId] = @CategoryFirstLevelId
			ORDER BY [c].[Title]

		OPEN category_cursor
		FETCH NEXT FROM category_cursor INTO @CategorySecondLevelId, @CategorySecondLevelTitle,	@CategorySecondLevelErpId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Fill tCategoryTitles with second level categories
			INSERT INTO @tCategoryTitles
			VALUES (
				@CategorySecondLevelId,
				@CategoryFirstLevelTitle + ' > ' + @CategorySecondLevelTitle + ' (' + CAST(@CategorySecondLevelId AS NVARCHAR(50)) + ' : ' + @CategorySecondLevelErpId + ')', 2
			)

			-- Fill tCategoryTitles with third level categories
			INSERT INTO @tCategoryTitles
			SELECT
				[c].[CategoryId],
				@CategoryFirstLevelTitle + ' > ' + @CategorySecondLevelTitle + ' > ' + [c].[Title]  + ' (' + CAST([c].[CategoryId] AS NVARCHAR(50)) + ' : ' + [c].[ErpId] + ')',
				3
			FROM [product].[tCategory] c
			WHERE [c].[ParentCategoryId] = @CategorySecondLevelId
			ORDER BY [c].[Title]

			FETCH NEXT FROM category_cursor INTO @CategorySecondLevelId, @CategorySecondLevelTitle, @CategorySecondLevelErpId
		END

		CLOSE category_cursor
		DEALLOCATE category_cursor

		-- Get next first level category
		INSERT INTO @tUsedCategoryFirstLevelId ([Id]) VALUES (@CategoryFirstLevelId)
		SET @CategoryFirstLevelTitle = NULL
		SELECT TOP(1)
			@CategoryFirstLevelId = [c].[CategoryId],
			@CategoryFirstLevelTitle = [c].[Title],
			@CategoryFirstLevelErpId = [c].[ErpId]
		FROM [product].[tCategory] c
		WHERE [c].[ParentCategoryId] IS NULL AND [c].[CategoryId] NOT IN (SELECT Id FROM @tUsedCategoryFirstLevelId)
		ORDER BY [c].[Title]
	END

	SELECT * FROM @tCategoryTitles
	ORDER BY [CategoryLevel], [CategoryTitle]
END
GO
