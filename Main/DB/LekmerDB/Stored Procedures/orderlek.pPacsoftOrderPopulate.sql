SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPacsoftOrderPopulate]
	@ChannelId			INT,
	@HyId				VARCHAR(50),
	@SupplierId			NVARCHAR(500),
	@DaysAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentDate DATETIME, @DateFrom DATETIME

	SET @CurrentDate = CONVERT (DATE, GETDATE())
	SET @DateFrom = (SELECT DATEADD(DAY, @DaysAfterPurchase * -1, @CurrentDate))
	
	INSERT INTO [orderlek].[tPacsoftOrder] ([OrderId])
	SELECT
		[o].[Order.OrderId]
	FROM
		[order].[vCustomOrder] o
		INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderId] = [o].[Order.OrderId]
		INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[OrderItemId] = [oi].[OrderItemId]
		INNER JOIN [lekmer].[tLekmerProduct] p ON [p].[ProductId] = [oip].[ProductId]
	WHERE
		[o].[Order.ChannelId] = @ChannelId
		AND [o].[OrderStatus.Id] = 4
		AND [o].[Order.CreatedDate] >= @DateFrom
		AND [o].[Order.CreatedDate] < @CurrentDate
		AND [p].[HYErpId] = @HyId
		AND [p].[SupplierId] = @SupplierId
		AND NOT EXISTS (
			SELECT [po].[OrderId] 
			FROM [orderlek].[tPacsoftOrder] po 
			WHERE [po].[OrderId] = [o].[Order.OrderId])

	INSERT INTO [orderlek].[tPacsoftOrder] ([OrderId])
	SELECT
		[o].[Order.OrderId]
	FROM
		[order].[vCustomOrder] o
		INNER JOIN [orderlek].[tPackageOrderItem] oi ON [oi].[OrderId] = [o].[Order.OrderId]
		INNER JOIN [orderlek].[tPackageOrderItemProduct] oip ON [oip].[OrderItemId] = [oi].[OrderItemId]
		INNER JOIN [lekmer].[tLekmerProduct] p ON [p].[ProductId] = [oip].[ProductId]
	WHERE
		[o].[Order.ChannelId] = @ChannelId
		AND [o].[OrderStatus.Id] = 4
		AND [o].[Order.CreatedDate] >= @DateFrom
		AND [o].[Order.CreatedDate] < @CurrentDate
		AND [p].[HYErpId] = @HyId
		AND [p].[SupplierId] = @SupplierId
		AND NOT EXISTS (
			SELECT [po].[OrderId] 
			FROM [orderlek].[tPacsoftOrder] po 
			WHERE [po].[OrderId] = [o].[Order.OrderId])
END
GO
