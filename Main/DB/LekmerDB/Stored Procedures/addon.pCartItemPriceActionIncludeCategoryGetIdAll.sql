
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartItemPriceActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[c].[CategoryId]
	FROM 
		[addon].[tCartItemPriceActionIncludeCategory] c
	WHERE 
		[c].[CartActionId] = @CartActionId
END
GO
