
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pProductImageDelete]
@ProductId INT,
@MediaId INT
AS
BEGIN
	DELETE
		[product].[tProductImage]
	WHERE
		(ProductId = @ProductId)
	AND
		(MediaId = @MediaId)

	IF @@ROWCOUNT = 0 
	DELETE
		[product].[tProductMedia]
	WHERE
		(ProductId = @ProductId)
	AND
		(MediaId = @MediaId)
END
GO
