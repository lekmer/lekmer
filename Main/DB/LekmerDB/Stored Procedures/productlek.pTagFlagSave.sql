SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pTagFlagSave]
	@TagId	INT,
	@FlagId	INT
AS 
BEGIN 
	UPDATE [productlek].[tTagFlag]
	SET [FlagId] = @FlagId
	WHERE [TagId] = @TagId

	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [productlek].[tTagFlag] (
			[TagId],
			[FlagId]
		)
		VALUES (
			@TagId,
			@FlagId
		)
	END
END
GO
