SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pEsalesModelComponentParameterAllByModelComponent]
	@SiteStructureRegistryId INT,
	@ModelComponentId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[emcp].[Name] AS 'EsalesModelParameter.Name',
		[emcp].[Value] AS 'EsalesModelParameter.Value'
	FROM
		[esales].[tEsalesModelComponent] emc
		INNER JOIN [esales].[tEsalesModelComponentParameter] emcp ON emcp.[ModelComponentId] = emc.[ModelComponentId]
	WHERE
		emc.[ModelComponentId] = @ModelComponentId
		AND
		(
			emcp.[SiteStructureRegistryId] = @SiteStructureRegistryId
			OR
			emcp.[SiteStructureRegistryId] IS NULL
		)
END
GO
