SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pCustomerUserDeleteAlternateLogin]
	@CustomerId INT,
	@UserName NVARCHAR(320)
AS	
BEGIN
	SET NOCOUNT ON
	IF EXISTS (SELECT 1 FROM [customerlek].[tFacebookCustomerUser] WHERE [CustomerId] = @CustomerId AND [IsAlternateLogin] = 0)
	BEGIN
		DELETE	[customer].[tCustomerUser]	
		WHERE   [CustomerId] = @CustomerId
				AND [UserName] = @UserName
				AND [IsAlternateLogin] = 1
	END
END
GO
