
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pTopListProductGetIdAllByStatisticsAndCategoryIdListExcludeForced]
	@ChannelId				INT,
	@BlockId				INT,
	@IncludeAllCategories	BIT,
	@CategoryIds			VARCHAR(MAX),
	@Delimiter				CHAR(1),
	@CountOrdersFrom		DATETIME,
	@From					INT,
	@To						INT
AS
BEGIN

	DECLARE @sql AS NVARCHAR(MAX)
	DECLARE @sqlCount AS NVARCHAR(MAX)
	DECLARE @sqlFragment AS NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT
			ROW_NUMBER() OVER (ORDER BY SUM([tlp].[PurchaseSum]) DESC) AS Number,
			[tlp].[ProductId]
		FROM
			[statistics].[tTopListProduct] tlp
			INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [tlp].[ProductId]
			CROSS APPLY (
				SELECT SUM([ps].[NumberInStock]) AS ''NumberInStock'' FROM [lekmer].[tProductSize] ps WHERE [ps].[ProductId] = [p].[ProductId]
			) a1
		WHERE
		   [p].[IsDeleted] = 0
			AND [p].[ProductStatusId] = 0
			AND [tlp].[ChannelId] = @ChannelId
			AND [tlp].[Date] >= @CountOrdersFrom
			AND ISNULL([a1].[NumberInStock], [p].[NumberInStock]) > 0'
			
	IF (@IncludeAllCategories = 0 AND @CategoryIds IS NOT NULL)
	SET @sqlFragment = @sqlFragment + '
			AND ( [p].[CategoryId] IN ( SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter) ) )'

	SET @sqlFragment = @sqlFragment + '		
			AND NOT EXISTS ( SELECT 1 FROM [addon].[tBlockTopListProduct] fp WHERE [fp].[BlockId] = @BlockId AND [fp].[ProductId] = [p].[ProductId] )
		GROUP BY
			[tlp].[ProductId]
	'

	SET @sqlCount = '
		SELECT COUNT(*)
		FROM
			(' + @sqlFragment + ') AS CountResults'

	SET @sql = '
		SELECT ProductId
		FROM
			(' + @sqlFragment + ') AS SearchResult
		WHERE
			Number BETWEEN @From AND @To'

	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT,
			@BlockId				INT,
			@IncludeAllCategories	BIT,
			@CategoryIds			VARCHAR(MAX),
			@Delimiter				CHAR(1),
			@CountOrdersFrom		DATETIME,
			@From					INT,
			@To						INT',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To
END
GO
