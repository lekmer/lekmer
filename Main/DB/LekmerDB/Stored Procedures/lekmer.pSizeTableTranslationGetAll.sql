
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableTranslationGetAll]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
	    [stt].*
	FROM
	    [lekmer].[vSizeTableTranslation] stt
	WHERE 
		stt.[SizeTableTranslation.SizeTableId] = @SizeTableId
	ORDER BY
		stt.[SizeTableTranslation.LanguageId]
END
GO
