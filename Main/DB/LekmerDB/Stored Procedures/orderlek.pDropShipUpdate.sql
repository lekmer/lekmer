
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDropShipUpdate]
	@DropShipId		INT,
	@CsvFileName	NVARCHAR(100),
	@PdfFileName	NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[orderlek].[tDropShip]
	SET
		[SendDate] = GETDATE(),
		[CsvFileName] = @CsvFileName,
		[PdfFileName] = @PdfFileName
	WHERE
		[DropShipId] = @DropShipId
END
GO
