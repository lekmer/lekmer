SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 15:00
Description:	Created
*/

CREATE procedure [sitestructure].[pBlockDelete]
@BlockId	int
as
BEGIN
	DELETE 
		sitestructure.tBlockTranslation
	WHERE 
		[BlockId] = @BlockId

	DELETE
		[sitestructure].[tBlock]	
	WHERE
		[BlockId] = @BlockId
end
GO
