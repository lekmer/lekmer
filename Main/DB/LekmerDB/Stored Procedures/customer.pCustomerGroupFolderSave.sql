SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerGroupFolderSave]
@CustomerGroupFolderId		int,
@ParentId					int,
@Title						nvarchar(50)
AS
BEGIN
	IF EXISTS
		(
			SELECT 1 FROM customer.tCustomerGroupFolder 
			WHERE Title = @Title 
				AND CustomerGroupFolderId <> @CustomerGroupFolderId
				AND	((@ParentId IS NULL AND ParentId IS NULL)
					OR ParentId = @ParentId)
		)
		RETURN -1
	
	UPDATE 
		customer.tCustomerGroupFolder
	SET
		ParentId = @ParentId,
		Title = @Title
	WHERE 
		CustomerGroupFolderId = @CustomerGroupFolderId
		
	if @@ROWCOUNT = 0
		BEGIN
			INSERT INTO 
				customer.tCustomerGroupFolder 
				(
					ParentId,
					Title
				)
			VALUES
				(
					@ParentId,
					@Title
				)
			SET @CustomerGroupFolderId = SCOPE_IDENTITY()
		END
	
	RETURN @CustomerGroupFolderId
END













GO
