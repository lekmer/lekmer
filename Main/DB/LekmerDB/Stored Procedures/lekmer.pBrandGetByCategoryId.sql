SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandGetByCategoryId]
@CategoryId	int,
@ChannelId	int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    b.[Brand.BrandId], b.[Brand.Title], b.[Brand.ContentNodeId] 
	FROM
	    lekmer.vBrand b
	INNER JOIN
		[product].[vCustomProduct] cp
	ON 
		b.[Brand.BrandId] = cp.[Lekmer.BrandId]
	WHERE
		((b.[Brand.ContentNodeId] >0)AND
		(cp.[Product.NumberInStock] > 0)AND
		(cp.[Product.ChannelId] = @ChannelId)AND
		((cp.[Product.CategoryId] = @CategoryId)OR
		(cp.[Product.CategoryId] IN (SELECT CategoryId FROM [product].[tCategory] WHERE ParentCategoryId = @CategoryId))OR
		(cp.[Product.CategoryId] IN (SELECT CategoryId FROM [product].[tCategory] WHERE ParentCategoryId in (SELECT CategoryId FROM [product].[tCategory] WHERE ParentCategoryId = @CategoryId)))))
	GROUP BY b.[Brand.BrandId], b.[Brand.Title], b.[Brand.ContentNodeId]

	
END
GO
