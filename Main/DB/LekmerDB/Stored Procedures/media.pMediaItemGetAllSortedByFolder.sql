
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pMediaItemGetAllSortedByFolder]
	@MediaFolderId  INT,
	@Page			int = NULL,
	@PageSize		int,
	@SortBy			varchar(20),
	@SortDescending	bit = NULL
AS
BEGIN
	SET NOCOUNT ON; 

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

    DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)
	SET @sqlFragment = (CASE WHEN @SortBy = 'FileType' 
		THEN
			'SELECT ROW_NUMBER() OVER (ORDER BY MF.[MediaFormat.Title]'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			M.*    
   			FROM [media].[vCustomMedia] as M
   			INNER JOIN [media].[vCustomMediaFormat] as MF on M.[Media.FormatId] = MF.[MediaFormat.Id]
			WHERE M.[Media.FolderId] = @MediaFolderId' 		
		WHEN @SortBy = 'Title'
		THEN 
			'SELECT ROW_NUMBER() OVER (ORDER BY M.[Media.Title]'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			M.*    
   			FROM [media].[vCustomMedia] AS M
			WHERE M.[Media.FolderId] = @MediaFolderId' 
		WHEN @SortBy = 'Id'
		THEN
			'SELECT ROW_NUMBER() OVER (ORDER BY M.[Media.Id]'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			M.*    
   			FROM [media].[vCustomMedia] AS M
			WHERE M.[Media.FolderId] = @MediaFolderId' 
		END)

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	EXEC sp_executesql @sqlCount,
		N'	@MediaFolderId             INT',	
			@MediaFolderId
			     
	EXEC sp_executesql @sql, 
			N'	@MediaFolderId         INT',	
			@MediaFolderId
END

GO
