
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroup_Save]
	@RatingGroupId INT,
	@RatingGroupFolderId INT,
	@CommonName VARCHAR(50),
	@Title NVARCHAR(50),
	@Description NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Same titles on same level are not allowed (return -1)
	IF EXISTS
	(
		SELECT 1 FROM [review].[tRatingGroup]
		WHERE [Title] = @Title
			AND [RatingGroupId] <> @RatingGroupId
			AND [RatingGroupFolderId] = @RatingGroupFolderId
	)
	RETURN -1
	
	-- Same common names are not allowed (return -2)
	IF EXISTS
	(
		SELECT 1
		FROM [review].[tRatingGroup]
		WHERE [CommonName] = @CommonName AND [RatingGroupId] <> @RatingGroupId
	)
	RETURN -2
	
	UPDATE 
		[review].[tRatingGroup]
	SET 
		[RatingGroupFolderId] = @RatingGroupFolderId,
		[CommonName] = @CommonName,
		[Title] = @Title,
		[Description] = @Description		
	WHERE
		[RatingGroupId] = @RatingGroupId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingGroup] (
			[RatingGroupFolderId],
			[CommonName],
			[Title],
			[Description]
		)
		VALUES (
			@RatingGroupFolderId,
			@CommonName,
			@Title,
			@Description
		)

		SET @RatingGroupId = SCOPE_IDENTITY()
	END

	RETURN @RatingGroupId
END
GO
