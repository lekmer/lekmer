
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		[b].*,
		[bs].*
	FROM 
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
