
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tFixedPriceActionIncludeBrand]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
