SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaDeleteAllBySizeTable]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[lekmer].[tSizeTableMedia]
	WHERE
		[SizeTableId] = @SizeTableId
END
GO
