SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pSupplierGetAll]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[productlek].[vSupplier]
END
GO
