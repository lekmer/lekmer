
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeDecreaseNumberInStock]
	@ProductId INT,
	@SizeId INT,
	@Quantity INT
AS
BEGIN
	SET NOCOUNT ON

	-- Update ProductSize NumberInSock
	UPDATE
		[lekmer].[tProductSize]
	SET
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0 
								ELSE [NumberInStock] - @Quantity END)
	WHERE
		[ProductId] = @ProductId
		AND [SizeId] = @SizeId

	-- Update NumberInStock of Packages that contain current product	
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId
END
GO
