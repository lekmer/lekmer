SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pEsalesModelComponentGetAllByBlockTypeIdSecure]
	@BlockTypeId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		[em].*,
		[ec].*
	FROM
		[esales].[vEsalesModel] em
		INNER JOIN [esales].[vEsalesModelComponent] ec ON [ec].[EsalesModelComponent.ModelId] = [em].[EsalesModel.ModelId]
		INNER JOIN [esales].[tEsalesModelComponentBlockType] ebt ON [ebt].[ModelComponentId] = [ec].[EsalesModelComponent.ModelComponentId]
	WHERE
		[ebt].[BlockTypeId] = @BlockTypeId
	ORDER BY
		[em].[EsalesModel.Ordinal],
		[ec].[EsalesModelComponent.Ordinal]
END
GO
