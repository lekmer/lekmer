SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCollectorTransactionSaveInvoiceNotificationResult]
	@TransactionId INT,
	@InvoiceNo VARCHAR(50),
	@OrderId INT,
	@InvoiceStatus INT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tCollectorTransaction]
	SET
		[InvoiceNo] = @InvoiceNo,
		[InvoiceStatus] = @InvoiceStatus,
		[OrderId] = @OrderId
	WHERE
		[TransactionId] = @TransactionId
END
GO
