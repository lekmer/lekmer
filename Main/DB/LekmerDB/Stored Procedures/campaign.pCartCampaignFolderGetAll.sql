
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaign].[pCartCampaignFolderGetAll]
AS
BEGIN
	SELECT *
	FROM campaign.vCustomCampaignFolder
	WHERE [CampaignFolder.TypeId] = 2
	ORDER BY [CampaignFolder.Ordinal]
END

GO
