SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPaymentTypeGetByIdSecure]
	@PaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vCustomPaymentTypeSecure]
	WHERE
		[PaymentType.PaymentTypeId] = @PaymentTypeId
END
GO
