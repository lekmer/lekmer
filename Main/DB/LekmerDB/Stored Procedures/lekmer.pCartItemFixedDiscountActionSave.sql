SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionSave]
	@CartActionId			INT,
	@IncludeAllProducts		BIT
AS
BEGIN
	UPDATE
		[lekmer].[tCartItemFixedDiscountAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [lekmer].[tCartItemFixedDiscountAction] (
			CartActionId,
			IncludeAllProducts
		)
		VALUES (
			@CartActionId,
			@IncludeAllProducts
		)
	END
END

GO
