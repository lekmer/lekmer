SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pCustomersLapsingByPeriodCount]
	@ChannelId	INT
AS
BEGIN
	-- Number of lapsing customers (6-12 months)
	
	DECLARE @CurrentDate DATETIME
	DECLARE @6MonthsAgo DATETIME
	DECLARE @12MonthsAgo DATETIME
	SET @CurrentDate = GETDATE()
	SET @6MonthsAgo = DATEADD(MONTH, -6, @CurrentDate)
	SET @12MonthsAgo = DATEADD(MONTH, -12, @CurrentDate)

	DECLARE @tInPeriod TABLE (Email VARCHAR(320) primary key with (ignore_dup_key = on))
	INSERT INTO @tInPeriod
	SELECT LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > @12MonthsAgo
		AND [o].[CreatedDate] < @6MonthsAgo
		AND [o].[ChannelId] = @ChannelId
		
	DECLARE @tAfterPeriod TABLE (Email VARCHAR(320) primary key with (ignore_dup_key = on))
	INSERT INTO @tAfterPeriod
	SELECT LTRIM(RTRIM([o].[Email]))
	FROM [order].[tOrder] o
	WHERE
		[o].[OrderStatusId] = 4
		AND [o].[CreatedDate] > @6MonthsAgo
		AND [o].[ChannelId] = @ChannelId

	SELECT COUNT(DISTINCT [a].[Email]) 'Customers' FROM (
		SELECT [Email] FROM @tInPeriod
		EXCEPT
		SELECT [Email] FROM @tAfterPeriod
	) a
END
GO
