SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListSave]
	@BlockId			INT,
	@ProductSortOrderId	INT
AS
BEGIN
	UPDATE
		[productlek].[tBlockProductRelationList]
	SET
		[ProductSortOrderId] = @ProductSortOrderId
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [productlek].[tBlockProductRelationList] (
			[BlockId],
			[ProductSortOrderId]
		)
		VALUES (
			@BlockId,
			@ProductSortOrderId
		)
	END
END
GO
