
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pWishListItemDelete]
	@Id	INT
AS
BEGIN
	EXEC [lekmer].[pWishListPackageItemDelete] @id	
	DELETE FROM [lekmer].[tWishListItem] WHERE [Id] = @Id
END
GO
