SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pAddressGetById]
	@AddressId int
as	
begin
	set nocount on

	select
		*
	from
		[customer].[vCustomAddress]
	where
		[Address.AddressId] = @AddressId
end

GO
