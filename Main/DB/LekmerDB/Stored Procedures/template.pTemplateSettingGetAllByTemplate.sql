
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 5  *****************
User: Roman D.		Date: 24.12.2008		Time: 15:00
Description:
			added inner join for field TypeName
************************************************
*****************  Version 4  *****************
User: Roman D.		Date: 23.12.2008		Time: 15:10
Description:
			added ModelSettingTypeId
************************************************
*****************  Version 3  *****************
User: Roman G.		Date: 22.12.2008		Time: 12:30
Description:
			Remove TemplateSettingId
************************************************

*****************  Version 2  *****************
User: Victor E.		Date: 18.11.2008		Time: 15:40
Description:
			Add AlternateValue
************************************************

*****************  Version 1  *****************
User: Victor E.		Date: 08.10.2008		Time: 12:00
Description:
			Created 
*/

CREATE PROCEDURE [template].[pTemplateSettingGetAllByTemplate]
		@TemplateId	int
AS
begin
	set nocount on
	
	select
		*
	from
		[template].[vCustomTemplateSetting] ts
	where
		ts.[TemplateSetting.TemplateId] = @TemplateId
	order by
		ts.[ModelSetting.CommonName]
end

GO
