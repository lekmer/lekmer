SET IDENTITY_INSERT [esales].[tEsalesModelParameter] ON
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (5, 1, 1000005, N'panel-path', N'/category-page-nl', 50)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (10, 2, 1000005, N'panel-path', N'/brand-page-nl', 50)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (15, 3, 1000005, N'panel-path', N'/top-category-page-nl', 50)
SET IDENTITY_INSERT [esales].[tEsalesModelParameter] OFF
SET IDENTITY_INSERT [esales].[tEsalesModelParameter] ON
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (1, 1, 1, N'panel-path', N'/category-page-se', 10)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (2, 1, 2, N'panel-path', N'/category-page-no', 20)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (3, 1, 3, N'panel-path', N'/category-page-dk', 30)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (4, 1, 4, N'panel-path', N'/category-page-fi', 40)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (6, 2, 1, N'panel-path', N'/brand-page-se', 10)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (7, 2, 2, N'panel-path', N'/brand-page-no', 20)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (8, 2, 3, N'panel-path', N'/brand-page-dk', 30)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (9, 2, 4, N'panel-path', N'/brand-page-fi', 40)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (11, 3, 1, N'panel-path', N'/top-category-page-se', 10)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (12, 3, 2, N'panel-path', N'/top-category-page-no', 20)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (13, 3, 3, N'panel-path', N'/top-category-page-dk', 30)
INSERT INTO [esales].[tEsalesModelParameter] ([ModelParameterId], [ModelId], [SiteStructureRegistryId], [Name], [Value], [Ordinal]) VALUES (14, 3, 4, N'panel-path', N'/top-category-page-fi', 40)
SET IDENTITY_INSERT [esales].[tEsalesModelParameter] OFF
