SET IDENTITY_INSERT [campaign].[tConditionType] ON
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000005, N'Cart does not contain', N'CartDoesNotContain')
SET IDENTITY_INSERT [campaign].[tConditionType] OFF
SET IDENTITY_INSERT [campaign].[tConditionType] ON
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1, N'Cart value', N'CartValue')
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (100003, N'Cart contains', N'CartContains')
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000001, N'Voucher', N'Voucher')
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000002, N'Cart value range', N'CartValueRange')
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000003, N'Customer group', N'CustomerGroupUser')
INSERT INTO [campaign].[tConditionType] ([ConditionTypeId], [Title], [CommonName]) VALUES (1000004, N'Cart items group value', N'CartItemsGroupValue')
SET IDENTITY_INSERT [campaign].[tConditionType] OFF
