SET IDENTITY_INSERT [esales].[tEsalesModelComponent] ON
INSERT INTO [esales].[tEsalesModelComponent] ([ModelComponentId], [ModelId], [Title], [CommonName], [Ordinal]) VALUES (1, 1, N'Category Products', N'CategoryProducts', 10)
INSERT INTO [esales].[tEsalesModelComponent] ([ModelComponentId], [ModelId], [Title], [CommonName], [Ordinal]) VALUES (2, 1, N'Recommended Products', N'RecommendedProducts', 20)
INSERT INTO [esales].[tEsalesModelComponent] ([ModelComponentId], [ModelId], [Title], [CommonName], [Ordinal]) VALUES (4, 2, N'Brand Products', N'BrandProducts', 10)
INSERT INTO [esales].[tEsalesModelComponent] ([ModelComponentId], [ModelId], [Title], [CommonName], [Ordinal]) VALUES (5, 2, N'Recommended Products', N'RecommendedProducts', 20)
INSERT INTO [esales].[tEsalesModelComponent] ([ModelComponentId], [ModelId], [Title], [CommonName], [Ordinal]) VALUES (6, 3, N'Popular Products', N'PopularProducts', 10)
INSERT INTO [esales].[tEsalesModelComponent] ([ModelComponentId], [ModelId], [Title], [CommonName], [Ordinal]) VALUES (7, 3, N'New Products', N'NewProducts', 20)
SET IDENTITY_INSERT [esales].[tEsalesModelComponent] OFF
