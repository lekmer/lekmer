SET IDENTITY_INSERT [campaign].[tCartActionType] ON
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1, N'Freight value', N'FreightValue')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000001, N'Voucher discount', N'VoucherDiscountActionType')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000002, N'Percentage discount', N'PercentageDiscount')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000003, N'Cart Item discount', N'CartItemDiscount')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000004, N'Fixed discount', N'FixedCartDiscount')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000005, N'Product auto free', N'ProductAutoFree')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000006, N'Gift card via email', N'GiftCardViaMailCart')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000007, N'Cart item percentage discount', N'CartItemPercentageDiscount')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000008, N'Cart item fixed discount', N'CartItemFixedDiscount')
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (1000010, N'Cart item group fixed discount', N'CartItemGroupFixedDiscount')
SET IDENTITY_INSERT [campaign].[tCartActionType] OFF
