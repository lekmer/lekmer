SET IDENTITY_INSERT [security].[tRole] ON
INSERT INTO [security].[tRole] ([RoleId], [Title]) VALUES (400, N'Warehouse')
SET IDENTITY_INSERT [security].[tRole] OFF
SET IDENTITY_INSERT [security].[tRole] ON
INSERT INTO [security].[tRole] ([RoleId], [Title]) VALUES (1, N'Administrator')
INSERT INTO [security].[tRole] ([RoleId], [Title]) VALUES (217, N'Product Manager')
INSERT INTO [security].[tRole] ([RoleId], [Title]) VALUES (361, N'Customer Service')
INSERT INTO [security].[tRole] ([RoleId], [Title]) VALUES (362, N'Web site manager')
SET IDENTITY_INSERT [security].[tRole] OFF
