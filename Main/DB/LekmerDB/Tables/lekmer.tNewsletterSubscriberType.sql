CREATE TABLE [lekmer].[tNewsletterSubscriberType]
(
[NewsletterSubscriberTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tNewsletterSubscriberType] ADD CONSTRAINT [PK_tNewsletterSubscriberType] PRIMARY KEY CLUSTERED  ([NewsletterSubscriberTypeId]) ON [PRIMARY]
GO
