CREATE TABLE [lekmer].[tCartItemPercentageDiscountAction]
(
[CartActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountAction] ADD CONSTRAINT [PK_tCartItemPercentageDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountAction] ADD CONSTRAINT [FK_tCartItemPercentageDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
