CREATE TABLE [orderlek].[tQliroTransaction]
(
[TransactionId] [int] NOT NULL IDENTITY(1, 1),
[ClientRef] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Mode] [int] NULL,
[TransactionTypeId] [int] NOT NULL,
[TimeoutProcessed] [int] NULL,
[StatusCode] [int] NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Duration] [bigint] NULL,
[OrderId] [int] NULL,
[Amount] [decimal] (16, 2) NULL,
[CurrencyCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PaymentType] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ReservationNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[InvoiceStatus] [int] NULL,
[ReturnCodeId] [int] NULL,
[Message] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[CustomerMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [orderlek].[tQliroTransaction] ADD 
CONSTRAINT [PK_tQliroTransaction] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]


GO
