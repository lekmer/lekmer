CREATE TABLE [review].[tRatingItemTranslation]
(
[RatingItemId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingItemTranslation] ADD CONSTRAINT [PK_tRatingItemTranslation] PRIMARY KEY CLUSTERED  ([RatingItemId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingItemTranslation] ADD CONSTRAINT [FK_tRatingItemTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [review].[tRatingItemTranslation] ADD CONSTRAINT [FK_tRatingItemTranslation_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId])
GO
