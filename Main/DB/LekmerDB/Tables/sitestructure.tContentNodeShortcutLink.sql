CREATE TABLE [sitestructure].[tContentNodeShortcutLink]
(
[ContentNodeId] [int] NOT NULL,
[LinkContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeShortcutLink] ADD CONSTRAINT [CK_tContentNodeShortcutLink] CHECK (([ContentNodeId]<>[LinkContentNodeId]))
GO
ALTER TABLE [sitestructure].[tContentNodeShortcutLink] ADD CONSTRAINT [PK_tContentNodeShortcutLink] PRIMARY KEY CLUSTERED  ([ContentNodeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tContentNodeShortcutLink_LinkContentNodeId] ON [sitestructure].[tContentNodeShortcutLink] ([LinkContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeShortcutLink] ADD CONSTRAINT [FK_tContentNodeShortcutLink_tContentNode] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tContentNodeShortcutLink] ADD CONSTRAINT [FK_tContentNodeShortcutLink_tContentNode_Link] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
