CREATE TABLE [lekmer].[tTagGroupInfoPointMapping]
(
[TagGroupCommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[InfoPointCommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTagGroupInfoPointMapping] ADD CONSTRAINT [PK_tTagGroupInfoPointMapping] PRIMARY KEY CLUSTERED  ([TagGroupCommonName], [InfoPointCommonName]) ON [PRIMARY]
GO
