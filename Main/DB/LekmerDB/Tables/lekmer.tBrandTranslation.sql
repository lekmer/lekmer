CREATE TABLE [lekmer].[tBrandTranslation]
(
[BrandId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrandTranslation] ADD CONSTRAINT [PK_tBrandTranslation] PRIMARY KEY CLUSTERED  ([BrandId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrandTranslation] ADD CONSTRAINT [FK_tBrandTranslation_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [lekmer].[tBrandTranslation] ADD CONSTRAINT [FK_tBrandTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
