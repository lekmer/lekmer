CREATE TABLE [template].[tEntityFunction]
(
[FunctionId] [int] NOT NULL IDENTITY(1, 1),
[EntityId] [int] NOT NULL,
[FunctionTypeId] [int] NOT NULL,
[CommonName] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL CONSTRAINT [DF_tEntityFunction_Description] DEFAULT ('')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [PK_tEntityFunction] PRIMARY KEY CLUSTERED  ([FunctionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tEntityFunction_EntityId] ON [template].[tEntityFunction] ([EntityId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tEntityFunction_FunctionTypeId] ON [template].[tEntityFunction] ([FunctionTypeId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
GO
ALTER TABLE [template].[tEntityFunction] ADD CONSTRAINT [FK_tEntityFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
GO
