CREATE TABLE [lekmer].[tSizeTable]
(
[SizeTableId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Column1Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Column2Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTable] ADD 
CONSTRAINT [PK_tSizeTable] PRIMARY KEY CLUSTERED  ([SizeTableId]) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTable] ADD
CONSTRAINT [FK_tSizeTable_tSizeTableFolder] FOREIGN KEY ([SizeTableFolderId]) REFERENCES [lekmer].[tSizeTableFolder] ([SizeTableFolderId])
GO
