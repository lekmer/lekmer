CREATE TABLE [lekmer].[tBlockProductSimilarList]
(
[BlockId] [int] NOT NULL,
[PriceRangeType] [int] NOT NULL CONSTRAINT [DF_tBlockProductSimilarList_PriceType] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] ADD CONSTRAINT [PK_tBlockProductSimilarList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductSimilarList] ADD CONSTRAINT [FK_tBlockProductSimilarList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
