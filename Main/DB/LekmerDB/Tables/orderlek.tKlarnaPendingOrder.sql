CREATE TABLE [orderlek].[tKlarnaPendingOrder]
(
[KlarnaPendingOrderId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[FirstAttempt] [datetime] NOT NULL,
[LastAttempt] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tKlarnaPendingOrder] ADD CONSTRAINT [PK_tKlarnaPendingOrder] PRIMARY KEY CLUSTERED  ([KlarnaPendingOrderId]) ON [PRIMARY]
GO
