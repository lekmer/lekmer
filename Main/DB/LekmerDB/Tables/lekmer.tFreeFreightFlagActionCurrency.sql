CREATE TABLE [lekmer].[tFreeFreightFlagActionCurrency]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFreeFreightFlagActionCurrency] ADD CONSTRAINT [PK_tFreeFreightFlagActionCurrency] PRIMARY KEY CLUSTERED  ([ProductActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFreeFreightFlagActionCurrency] ADD CONSTRAINT [FK_tFreeFreightFlagActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tFreeFreightFlagActionCurrency] ADD CONSTRAINT [FK_tFreeFreightFlagActionCurrency_tFreeFreightFlagAction] FOREIGN KEY ([ProductActionId]) REFERENCES [lekmer].[tFreeFreightFlagAction] ([ProductActionId]) ON DELETE CASCADE
GO
