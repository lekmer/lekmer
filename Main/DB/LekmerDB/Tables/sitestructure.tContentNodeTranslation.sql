CREATE TABLE [sitestructure].[tContentNodeTranslation]
(
[ContentNodeId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeTranslation] ADD CONSTRAINT [PK_tContentNodeTranslation] PRIMARY KEY CLUSTERED  ([ContentNodeId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeTranslation] ADD CONSTRAINT [FK_tContentNodeTranslation_tContentNode] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tContentNodeTranslation] ADD CONSTRAINT [FK_tContentNodeTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
