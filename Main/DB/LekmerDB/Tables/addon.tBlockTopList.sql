CREATE TABLE [addon].[tBlockTopList]
(
[BlockId] [int] NOT NULL,
[IncludeAllCategories] [bit] NOT NULL,
[OrderStatisticsDayCount] [int] NOT NULL,
[LinkContentNodeId] [int] NULL,
[CustomUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [addon].[tBlockTopList] ADD
CONSTRAINT [FK_tBlockTopList_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
ALTER TABLE [addon].[tBlockTopList] ADD CONSTRAINT [PK_tBlockTopList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopList] ADD CONSTRAINT [FK_tBlockTopList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
