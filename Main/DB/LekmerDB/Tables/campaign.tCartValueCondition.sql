CREATE TABLE [campaign].[tCartValueCondition]
(
[ConditionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartValueCondition] ADD CONSTRAINT [PK_tCartValueCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartValueCondition] ADD CONSTRAINT [FK_tCartValueCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
