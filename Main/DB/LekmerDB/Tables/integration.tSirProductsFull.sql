CREATE TABLE [integration].[tSirProductsFull]
(
[ArticleNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ProductId] [int] NOT NULL,
[ProductGroup] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ArticleDescription] [nvarchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[OurPrice] [decimal] (16, 2) NULL,
[ListPrice] [decimal] (16, 2) NULL,
[Supplier] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Category] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[LekmerArtNo] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[SupplierArtNo] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[AveragePrice] [decimal] (16, 2) NULL,
[InsertedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tSirProductsFull] ADD CONSTRAINT [PK_tSirProductsFull] PRIMARY KEY CLUSTERED  ([ArticleNumber]) ON [PRIMARY]
GO
