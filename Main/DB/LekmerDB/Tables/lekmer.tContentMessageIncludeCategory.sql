CREATE TABLE [lekmer].[tContentMessageIncludeCategory]
(
[ContentMessageId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeCategory] ADD CONSTRAINT [PK_tContentMessageIncludeCategory] PRIMARY KEY CLUSTERED  ([ContentMessageId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeCategory] ADD CONSTRAINT [FK_tContentMessageIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [lekmer].[tContentMessageIncludeCategory] ADD CONSTRAINT [FK_tContentMessageIncludeCategory_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
GO
