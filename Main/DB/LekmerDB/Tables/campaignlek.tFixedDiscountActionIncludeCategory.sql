CREATE TABLE [campaignlek].[tFixedDiscountActionIncludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeCategory] ADD CONSTRAINT [PK_tFixedDiscountActionIncludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeCategory] ADD CONSTRAINT [FK_tFixedDiscountActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionIncludeCategory] ADD CONSTRAINT [FK_tFixedDiscountActionIncludeCategory_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
