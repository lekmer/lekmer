CREATE TABLE [esales].[tBlockEsalesSetting]
(
[BlockId] [int] NOT NULL,
[EsalesModelComponentId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesSetting] ADD CONSTRAINT [PK_tBlockEsalesSetting] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesSetting] ADD CONSTRAINT [FK_tBlockEsalesSetting_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [esales].[tBlockEsalesSetting] ADD CONSTRAINT [FK_tBlockEsalesSetting_tEsalesModelComponent] FOREIGN KEY ([EsalesModelComponentId]) REFERENCES [esales].[tEsalesModelComponent] ([ModelComponentId])
GO
