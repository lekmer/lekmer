CREATE TABLE [order].[tOrderComment]
(
[OrderCommentId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[CreationDate] [datetime] NOT NULL,
[Comment] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AuthorId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderComment] ADD CONSTRAINT [PK_tOrderComment] PRIMARY KEY CLUSTERED  ([OrderCommentId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderComment_SystemUserId] ON [order].[tOrderComment] ([AuthorId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderComment_OrderId] ON [order].[tOrderComment] ([OrderId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderComment] ADD CONSTRAINT [FK_tOrderComment_tSystemUser] FOREIGN KEY ([AuthorId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
ALTER TABLE [order].[tOrderComment] ADD CONSTRAINT [FK_tOrderComment_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
