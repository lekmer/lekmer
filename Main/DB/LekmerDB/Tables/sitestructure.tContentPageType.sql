CREATE TABLE [sitestructure].[tContentPageType]
(
[ContentPageTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageType] ADD CONSTRAINT [PK_tContentPageType] PRIMARY KEY CLUSTERED  ([ContentPageTypeId]) ON [PRIMARY]
GO
