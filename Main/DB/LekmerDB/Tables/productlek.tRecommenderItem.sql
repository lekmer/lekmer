CREATE TABLE [productlek].[tRecommenderItem]
(
[RecommenderItemId] [int] NOT NULL IDENTITY(1, 1),
[RecommenderListId] [int] NOT NULL,
[ProductId] [int] NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tRecommenderItem] ADD CONSTRAINT [PK_tRecommenderItem] PRIMARY KEY CLUSTERED  ([RecommenderItemId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRecommenderItem_RecommenderListId_ProductId] ON [productlek].[tRecommenderItem] ([RecommenderListId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tRecommenderItem] ADD CONSTRAINT [FK_tRecommenderItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [productlek].[tRecommenderItem] ADD CONSTRAINT [FK_tRecommenderItem_tRecommenderList] FOREIGN KEY ([RecommenderListId]) REFERENCES [productlek].[tRecommenderList] ([RecommenderListId])
GO
