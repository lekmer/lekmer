CREATE TABLE [esales].[tEsalesModelComponentParameter]
(
[ModelComponentParameterId] [int] NOT NULL IDENTITY(1, 1),
[ModelComponentId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NULL,
[Name] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [varchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelComponentParameter] ADD CONSTRAINT [PK_tEsalesModelComponentParameter] PRIMARY KEY CLUSTERED  ([ModelComponentParameterId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelComponentParameter] ADD CONSTRAINT [FK_tEsalesModelComponentParameter_tEsalesModelComponent] FOREIGN KEY ([ModelComponentId]) REFERENCES [esales].[tEsalesModelComponent] ([ModelComponentId])
GO
ALTER TABLE [esales].[tEsalesModelComponentParameter] ADD CONSTRAINT [FK_tEsalesModelComponentParameter_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
