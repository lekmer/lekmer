CREATE TABLE [lekmer].[tProductTag]
(
[ProductTagId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[TagId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tProductTag] ADD 
CONSTRAINT [PK_tProductTag] PRIMARY KEY CLUSTERED  ([ProductTagId]) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tProductTag_ProductId_TagId] ON [lekmer].[tProductTag] ([ProductId], [TagId]) ON [PRIMARY]

ALTER TABLE [lekmer].[tProductTag] ADD
CONSTRAINT [FK_tProductTag_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
ALTER TABLE [lekmer].[tProductTag] ADD
CONSTRAINT [FK_tProductTag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId])
GO
