CREATE TABLE [lekmer].[tSizeDeviation]
(
[SizeDeviationId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeDeviation] ADD CONSTRAINT [PK_tSizeDeviation] PRIMARY KEY CLUSTERED  ([SizeDeviationId]) ON [PRIMARY]
GO
