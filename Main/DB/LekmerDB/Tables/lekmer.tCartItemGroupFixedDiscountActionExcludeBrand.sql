CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeBrand_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
