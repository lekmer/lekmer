CREATE TABLE [integration].[tAccessoriesSize]
(
[AccessoriesSizeId] [int] NOT NULL,
[AccessoriesSize] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AccessoriesSizeErpId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tAccessoriesSize_AccessoriesSizeErpId] ON [integration].[tAccessoriesSize] ([AccessoriesSizeErpId]) ON [PRIMARY]

ALTER TABLE [integration].[tAccessoriesSize] ADD 
CONSTRAINT [PK_tAccessoriesSize] PRIMARY KEY CLUSTERED  ([AccessoriesSizeId]) ON [PRIMARY]
GO
