CREATE TABLE [product].[tProductMedia]
(
[ProductId] [int] NOT NULL,
[MediaId] [int] NOT NULL,
[ProductImageGroupId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductMedia] ADD CONSTRAINT [PK_tProductMedia] PRIMARY KEY CLUSTERED  ([ProductId], [MediaId], [ProductImageGroupId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductMedia] ADD CONSTRAINT [FK_tProductMedia_tMedia] FOREIGN KEY ([MediaId]) REFERENCES [media].[tMedia] ([MediaId]) ON DELETE CASCADE
GO
ALTER TABLE [product].[tProductMedia] ADD CONSTRAINT [FK_tProductMedia_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tProductMedia] ADD CONSTRAINT [FK_tProductMedia_tProductImageGroup] FOREIGN KEY ([ProductImageGroupId]) REFERENCES [product].[tProductImageGroup] ([ProductImageGroupId])
GO
