CREATE TABLE [campaignlek].[tCampaignActionIncludeProduct]
(
[ConfigId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeProduct] ADD CONSTRAINT [PK_tCampaignActionIncludeProduct] PRIMARY KEY CLUSTERED  ([ConfigId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeProduct] ADD CONSTRAINT [FK_tCampaignActionIncludeProduct_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeProduct] ADD CONSTRAINT [FK_tCampaignActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
