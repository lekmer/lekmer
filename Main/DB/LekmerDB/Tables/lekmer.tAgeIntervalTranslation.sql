CREATE TABLE [lekmer].[tAgeIntervalTranslation]
(
[AgeIntervalId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tAgeIntervalTranslation] ADD CONSTRAINT [PK_tAgeIntervalTranslation] PRIMARY KEY CLUSTERED  ([AgeIntervalId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tAgeIntervalTranslation] ADD CONSTRAINT [FK_tAgeIntervalTranslation_tAgeInterval] FOREIGN KEY ([AgeIntervalId]) REFERENCES [lekmer].[tAgeInterval] ([AgeIntervalId])
GO
ALTER TABLE [lekmer].[tAgeIntervalTranslation] ADD CONSTRAINT [FK_tAgeIntervalTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
