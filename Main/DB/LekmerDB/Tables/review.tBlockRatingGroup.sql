CREATE TABLE [review].[tBlockRatingGroup]
(
[BlockId] [int] NOT NULL,
[RatingGroupId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockRatingGroup] ADD CONSTRAINT [PK_tBlockRatingGroup] PRIMARY KEY CLUSTERED  ([BlockId], [RatingGroupId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockRatingGroup] ADD CONSTRAINT [FK_tBlockRatingGroup_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [review].[tBlockRatingGroup] ADD CONSTRAINT [FK_tBlockRatingGroup_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId])
GO
