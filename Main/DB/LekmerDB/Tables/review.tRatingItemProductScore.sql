CREATE TABLE [review].[tRatingItemProductScore]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[RatingId] [int] NOT NULL,
[RatingItemId] [int] NOT NULL,
[HitCount] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [review].[tRatingItemProductScore] ADD
CONSTRAINT [FK_tRatingItemProductScore_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
ALTER TABLE [review].[tRatingItemProductScore] ADD
CONSTRAINT [FK_tRatingItemProductScore_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
ALTER TABLE [review].[tRatingItemProductScore] ADD 
CONSTRAINT [PK_tRatingItemProductScore] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId], [RatingId], [RatingItemId]) ON [PRIMARY]

GO

ALTER TABLE [review].[tRatingItemProductScore] ADD CONSTRAINT [FK_tRatingItemProductScore_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [review].[tRatingItemProductScore] ADD CONSTRAINT [FK_tRatingItemProductScore_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId])
GO
