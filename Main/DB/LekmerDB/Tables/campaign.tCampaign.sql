CREATE TABLE [campaign].[tCampaign]
(
[CampaignId] [int] NOT NULL IDENTITY(1, 1),
[FolderId] [int] NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CampaignStatusId] [int] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Exclusive] [bit] NOT NULL CONSTRAINT [DF_tCampaign_Exclusive] DEFAULT ((0)),
[Priority] [int] NOT NULL,
[LevelId] [int] NOT NULL,
[UseLandingPage] [bit] NOT NULL CONSTRAINT [DF_tCampaign_UseLandingPage] DEFAULT ((0)),
[PriceTypeId] [int] NULL,
[TagId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [campaign].[tCampaign] ADD
CONSTRAINT [FK_tCampaign_tCampaignPriceType] FOREIGN KEY ([PriceTypeId]) REFERENCES [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId])
GO
ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [PK_tCampaign] PRIMARY KEY CLUSTERED  ([CampaignId]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_tCampaign_CampaignStatusId] ON [campaign].[tCampaign] ([CampaignStatusId]) ON [PRIMARY]
GO

ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [FK_tCampaign_tCampaignStatus] FOREIGN KEY ([CampaignStatusId]) REFERENCES [campaign].[tCampaignStatus] ([CampaignStatusId])
GO
ALTER TABLE [campaign].[tCampaign] ADD CONSTRAINT [FK_tCampaign_tCampaignFolder] FOREIGN KEY ([FolderId]) REFERENCES [campaign].[tCampaignFolder] ([FolderId])
GO
