CREATE TABLE [security].[tRolePrivilege]
(
[RoleId] [int] NOT NULL,
[PrivilegeId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [security].[tRolePrivilege] WITH NOCHECK ADD
CONSTRAINT [FK_tRolePrivilege_tPrivilege] FOREIGN KEY ([PrivilegeId]) REFERENCES [security].[tPrivilege] ([PrivilegeId])
GO
ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [PK_tRolePrivilege] PRIMARY KEY CLUSTERED  ([RoleId], [PrivilegeId]) ON [PRIMARY]
GO

ALTER TABLE [security].[tRolePrivilege] ADD CONSTRAINT [FK_tRolePrivilege_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])
GO
