CREATE TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionIncludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionIncludeBrand] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionIncludeBrand_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
