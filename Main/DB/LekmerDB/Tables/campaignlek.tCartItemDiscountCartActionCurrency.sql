CREATE TABLE [campaignlek].[tCartItemDiscountCartActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionCurrency] ADD CONSTRAINT [PK_tCartItemDiscountCartActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionCurrency] ADD CONSTRAINT [FK_tCartItemDiscountCartActionCurrency_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId])
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionCurrency] ADD CONSTRAINT [FK_tCartItemDiscountCartActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
