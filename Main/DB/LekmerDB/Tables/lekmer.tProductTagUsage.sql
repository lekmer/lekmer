CREATE TABLE [lekmer].[tProductTagUsage]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ProductTagId] [int] NOT NULL,
[ObjectId] [int] NOT NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tProductTagUsage_ObjectId] ON [lekmer].[tProductTagUsage] ([ObjectId]) INCLUDE ([ProductTagId]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tProductTagUsage_ProductTagId] ON [lekmer].[tProductTagUsage] ([ProductTagId]) ON [PRIMARY]

GO
ALTER TABLE [lekmer].[tProductTagUsage] ADD CONSTRAINT [PK_tProductTagUsage] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductTagUsage] ADD CONSTRAINT [FK_tProductTagUsage_tProductTag] FOREIGN KEY ([ProductTagId]) REFERENCES [lekmer].[tProductTag] ([ProductTagId])
GO
