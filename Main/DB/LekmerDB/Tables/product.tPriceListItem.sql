CREATE TABLE [product].[tPriceListItem]
(
[PriceListId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[PriceIncludingVat] [decimal] (16, 2) NOT NULL,
[PriceExcludingVat] [decimal] (16, 2) NOT NULL,
[VatPercentage] [decimal] (16, 2) NOT NULL,
[tmpIdentity] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
ALTER TABLE [product].[tPriceListItem] ADD
CONSTRAINT [CK_tPriceListItem_PriceIncludingVat] CHECK (([PriceIncludingVat]>=[PriceExcludingVat]))
ALTER TABLE [product].[tPriceListItem] ADD
CONSTRAINT [CK_tPriceListItem_VatPercentage] CHECK (([VatPercentage]>=(0)))
CREATE NONCLUSTERED INDEX [IX_tPriceListItem_ProductId] ON [product].[tPriceListItem] ([ProductId]) INCLUDE ([PriceIncludingVat], [PriceListId])

GO
ALTER TABLE [product].[tPriceListItem] ADD CONSTRAINT [CK_tPriceListItem_PriceExcludingVat] CHECK (([PriceExcludingVat]>(0)))
GO

ALTER TABLE [product].[tPriceListItem] ADD CONSTRAINT [PK_tPriceListItem] PRIMARY KEY CLUSTERED  ([PriceListId], [ProductId]) ON [PRIMARY]
GO

ALTER TABLE [product].[tPriceListItem] ADD CONSTRAINT [FK_tPriceListItem_tPriceList] FOREIGN KEY ([PriceListId]) REFERENCES [product].[tPriceList] ([PriceListId])
GO
ALTER TABLE [product].[tPriceListItem] ADD CONSTRAINT [FK_tPriceListItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
