CREATE TABLE [corelek].[tCacheUpdate]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[ManagerName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UpdateType] [int] NOT NULL,
[Key] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tCacheUpdate_CreationDate] DEFAULT (getdate()),
[InsertionDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [corelek].[tCacheUpdate] ADD CONSTRAINT [PK_tCacheUpdate] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
