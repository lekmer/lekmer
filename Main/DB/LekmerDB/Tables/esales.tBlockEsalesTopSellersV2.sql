CREATE TABLE [esales].[tBlockEsalesTopSellersV2]
(
[BlockId] [int] NOT NULL,
[PanelName] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[WindowLastEsalesValue] [int] NOT NULL,
[IncludeAllCategories] [bit] NOT NULL,
[LinkContentNodeId] [int] NULL,
[CustomUrl] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[UrlTitle] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2] ADD CONSTRAINT [PK_tBlockEsalesTopSellersV2] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
