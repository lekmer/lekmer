CREATE TABLE [campaign].[tProductAction]
(
[ProductActionId] [int] NOT NULL IDENTITY(1, 1),
[ProductActionTypeId] [int] NOT NULL,
[CampaignId] [int] NOT NULL,
[Ordinal] [int] NOT NULL CONSTRAINT [DF_tProductAction_Exclusive] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tProductAction] ADD CONSTRAINT [PK_tProductAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductAction_CampaignId] ON [campaign].[tProductAction] ([CampaignId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductAction_ProductActionTypeId] ON [campaign].[tProductAction] ([ProductActionTypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tProductAction] ADD CONSTRAINT [FK_tProductAction_tProductCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tProductCampaign] ([CampaignId])
GO
ALTER TABLE [campaign].[tProductAction] ADD CONSTRAINT [FK_tProductAction_tProductActionType] FOREIGN KEY ([ProductActionTypeId]) REFERENCES [campaign].[tProductActionType] ([ProductActionTypeId])
GO
