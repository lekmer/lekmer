CREATE TABLE [productlek].[tProductRegistryDeliveryTime]
(
[ProductRegistryId] [int] NOT NULL,
[DeliveryTimeId] [int] NOT NULL,
[From] [int] NULL,
[To] [int] NULL,
[Format] [nvarchar] (25) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tProductRegistryDeliveryTime] ADD CONSTRAINT [PK_tProductRegistryDeliveryTime] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [DeliveryTimeId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tProductRegistryDeliveryTime] ADD CONSTRAINT [FK_tProductRegistryDeliveryTime_tDeliveryTime] FOREIGN KEY ([DeliveryTimeId]) REFERENCES [productlek].[tDeliveryTime] ([DeliveryTimeId])
GO
ALTER TABLE [productlek].[tProductRegistryDeliveryTime] ADD CONSTRAINT [FK_tProductRegistryDeliveryTime_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
