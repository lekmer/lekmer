CREATE TABLE [orderlek].[tSubPaymentType]
(
[SubPaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[PaymentTypeId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Code] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tSubPaymentType] ADD CONSTRAINT [PK_tSubPaymentType] PRIMARY KEY CLUSTERED  ([SubPaymentTypeId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tSubPaymentType] ADD CONSTRAINT [UQ_tSubPaymentType_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tSubPaymentType] ADD CONSTRAINT [UQ_tSubPaymentType_Title] UNIQUE NONCLUSTERED  ([Title]) ON [PRIMARY]
GO
