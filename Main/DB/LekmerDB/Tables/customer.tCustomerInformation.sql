CREATE TABLE [customer].[tCustomerInformation]
(
[CustomerId] [int] NOT NULL,
[FirstName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[LastName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CivicNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PhoneNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CellPhoneNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DefaultBillingAddressId] [int] NULL,
[DefaultDeliveryAddressId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerInformation] ADD CONSTRAINT [PK_tCustomerInformation] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomerInformation_DefaultBillingAddressId] ON [customer].[tCustomerInformation] ([DefaultBillingAddressId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomerInformation_DefaultDeliveryAddressId] ON [customer].[tCustomerInformation] ([DefaultDeliveryAddressId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerInformation] ADD CONSTRAINT [FK_tCustomerInformation_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId]) ON DELETE CASCADE
GO
ALTER TABLE [customer].[tCustomerInformation] ADD CONSTRAINT [FK_tCustomerInformation_tAddress_Billing] FOREIGN KEY ([CustomerId], [DefaultBillingAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId])
GO
ALTER TABLE [customer].[tCustomerInformation] ADD CONSTRAINT [FK_tCustomerInformation_tAddress_Delivery] FOREIGN KEY ([CustomerId], [DefaultDeliveryAddressId]) REFERENCES [customer].[tAddress] ([CustomerId], [AddressId])
GO
CREATE FULLTEXT INDEX ON [customer].[tCustomerInformation] KEY INDEX [PK_tCustomerInformation] ON [fulltextcatalog_tcustomerinformation_default]
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ADD ([FirstName] LANGUAGE 0)
GO
ALTER FULLTEXT INDEX ON [customer].[tCustomerInformation] ADD ([LastName] LANGUAGE 0)
GO
