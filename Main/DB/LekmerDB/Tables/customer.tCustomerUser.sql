CREATE TABLE [customer].[tCustomerUser]
(
[CustomerId] [int] NOT NULL,
[CustomerRegistryId] [int] NOT NULL,
[UserName] [nvarchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Password] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[PasswordSalt] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[IsAlternateLogin] [bit] NOT NULL CONSTRAINT [DF_tCustomerUser_IsLoginOption] DEFAULT ((0))
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCustomerUser_UserName] ON [customer].[tCustomerUser] ([UserName]) ON [PRIMARY]

ALTER TABLE [customer].[tCustomerUser] ADD
CONSTRAINT [FK_tCustomer_tCustomerUser] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
ALTER TABLE [customer].[tCustomerUser] ADD CONSTRAINT [PK_tCustomerUser] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
