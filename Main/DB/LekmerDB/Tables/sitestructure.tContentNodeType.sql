CREATE TABLE [sitestructure].[tContentNodeType]
(
[ContentNodeTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeType] ADD CONSTRAINT [PK_tContentNodeType] PRIMARY KEY CLUSTERED  ([ContentNodeTypeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeType] ADD CONSTRAINT [UQ_tContentNodeType_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
