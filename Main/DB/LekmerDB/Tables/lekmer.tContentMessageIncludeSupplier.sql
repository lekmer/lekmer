CREATE TABLE [lekmer].[tContentMessageIncludeSupplier]
(
[ContentMessageId] [int] NOT NULL,
[SupplierId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeSupplier] ADD CONSTRAINT [PK_tContentMessageIncludeSupplier] PRIMARY KEY CLUSTERED  ([ContentMessageId], [SupplierId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeSupplier] ADD CONSTRAINT [FK_tContentMessageIncludeSupplier_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
GO
ALTER TABLE [lekmer].[tContentMessageIncludeSupplier] ADD CONSTRAINT [FK_tContentMessageIncludeSupplier_tSupplier] FOREIGN KEY ([SupplierId]) REFERENCES [productlek].[tSupplier] ([SupplierId])
GO
