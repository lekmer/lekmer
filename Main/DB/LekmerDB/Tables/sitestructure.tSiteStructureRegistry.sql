CREATE TABLE [sitestructure].[tSiteStructureRegistry]
(
[SiteStructureRegistryId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StartContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tSiteStructureRegistry] ADD CONSTRAINT [PK_tSiteStructureRegistry] PRIMARY KEY CLUSTERED  ([SiteStructureRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tSiteStructureRegistry_StartContentNodeId] ON [sitestructure].[tSiteStructureRegistry] ([StartContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tSiteStructureRegistry] ADD CONSTRAINT [FK_tSiteStructureRegistry_tContentNode] FOREIGN KEY ([StartContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId], [SiteStructureRegistryId])
GO
