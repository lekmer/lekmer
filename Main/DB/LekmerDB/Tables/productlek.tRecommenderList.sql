CREATE TABLE [productlek].[tRecommenderList]
(
[RecommenderListId] [int] NOT NULL IDENTITY(1, 1),
[SessionId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tRecommenderList] ADD CONSTRAINT [PK_tRecommenderList] PRIMARY KEY CLUSTERED  ([RecommenderListId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tRecommenderList_SessionId] ON [productlek].[tRecommenderList] ([SessionId]) ON [PRIMARY]
GO
