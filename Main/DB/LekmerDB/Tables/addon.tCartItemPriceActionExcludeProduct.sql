CREATE TABLE [addon].[tCartItemPriceActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemPriceActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemPriceActionExcludeProduct_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId])
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemPriceActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
