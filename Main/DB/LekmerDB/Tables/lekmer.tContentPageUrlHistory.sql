CREATE TABLE [lekmer].[tContentPageUrlHistory]
(
[ContentPageUrlHistoryId] [int] NOT NULL IDENTITY(1, 1),
[ContentPageId] [int] NOT NULL,
[UrlTitleOld] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tContentPageUrlHistory_CreationDate] DEFAULT (getdate()),
[LastUsageDate] [datetime] NOT NULL CONSTRAINT [DF_tContentPageUrlHistory_LastUsageDate] DEFAULT (getdate()),
[UsageAmount] [int] NOT NULL CONSTRAINT [DF_tContentPageUrlHistory_UsageAmount] DEFAULT ((0)),
[HistoryLifeIntervalTypeId] [int] NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [IX_tContentPageUrlHistory_ContentPageId_UrlTitleOld] ON [lekmer].[tContentPageUrlHistory] ([ContentPageId], [UrlTitleOld]) ON [PRIMARY]

GO
ALTER TABLE [lekmer].[tContentPageUrlHistory] ADD CONSTRAINT [PK_tContentPageUrlHistory] PRIMARY KEY CLUSTERED  ([ContentPageUrlHistoryId]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_tContentPageUrlHistory_UrlTitleOld] ON [lekmer].[tContentPageUrlHistory] ([UrlTitleOld]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentPageUrlHistory] ADD CONSTRAINT [FK_tContentPageUrlHistory_tContentPage] FOREIGN KEY ([ContentPageId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
ALTER TABLE [lekmer].[tContentPageUrlHistory] ADD CONSTRAINT [FK_tContentPageUrlHistory_tHistoryLifeIntervalType] FOREIGN KEY ([HistoryLifeIntervalTypeId]) REFERENCES [lekmer].[tHistoryLifeIntervalType] ([HistoryLifeIntervalTypeId])
GO
