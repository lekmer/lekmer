CREATE TABLE [lekmer].[tWishList]
(
[WishListId] [int] NOT NULL IDENTITY(1, 1),
[WishListKey] [uniqueidentifier] NOT NULL,
[WishList] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[WishListTitle] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[UpdatedDate] [datetime] NULL,
[IpAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[NumberOfRemovedItems] [int] NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tWishList_WishListKey] ON [lekmer].[tWishList] ([WishListKey])

GO
ALTER TABLE [lekmer].[tWishList] ADD CONSTRAINT [PK_tWishList] PRIMARY KEY CLUSTERED  ([WishListId]) ON [PRIMARY]
GO
