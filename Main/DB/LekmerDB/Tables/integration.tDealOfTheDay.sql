CREATE TABLE [integration].[tDealOfTheDay]
(
[BlockId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Type] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Rownumber] [nchar] (10) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tDealOfTheDay] ADD 
CONSTRAINT [PK_tDealOfTheDay] PRIMARY KEY CLUSTERED  ([BlockId], [ChannelId]) ON [PRIMARY]
GO
