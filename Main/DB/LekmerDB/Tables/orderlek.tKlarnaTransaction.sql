CREATE TABLE [orderlek].[tKlarnaTransaction]
(
[KlarnaTransactionId] [int] NOT NULL IDENTITY(1, 1),
[KlarnaShopId] [int] NOT NULL,
[KlarnaMode] [int] NOT NULL,
[KlarnaTransactionTypeId] [int] NOT NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[OrderId] [int] NULL,
[Amount] [decimal] (16, 2) NULL,
[CurrencyId] [int] NULL,
[PClassId] [int] NULL,
[YearlySalary] [int] NULL,
[Duration] [bigint] NULL,
[ReservationNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseCodeId] [int] NULL,
[KlarnaFaultCode] [int] NULL,
[KlarnaFaultReason] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tKlarnaTransaction] ADD CONSTRAINT [PK_tKlarnaTransaction] PRIMARY KEY CLUSTERED  ([KlarnaTransactionId]) ON [PRIMARY]
GO
