CREATE TABLE [product].[tVariationGroupTranslation]
(
[VariationGroupId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationGroupTranslation] ADD CONSTRAINT [PK_tVariationGroupTranslation] PRIMARY KEY CLUSTERED  ([VariationGroupId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationGroupTranslation] ADD CONSTRAINT [FK_tVariationGroupTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [product].[tVariationGroupTranslation] ADD CONSTRAINT [FK_tVariationGroupTranslation_tVariationGroup] FOREIGN KEY ([VariationGroupId]) REFERENCES [product].[tVariationGroup] ([VariationGroupId])
GO
