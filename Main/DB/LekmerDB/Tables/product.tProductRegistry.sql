CREATE TABLE [product].[tProductRegistry]
(
[ProductRegistryId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductRegistry] ADD CONSTRAINT [PK_tProductRegistry] PRIMARY KEY CLUSTERED  ([ProductRegistryId]) ON [PRIMARY]
GO
