CREATE TABLE [lekmer].[tContentMessageIncludeBrand]
(
[ContentMessageId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeBrand] ADD CONSTRAINT [PK_tContentMessageIncludeBrand] PRIMARY KEY CLUSTERED  ([ContentMessageId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tContentMessageIncludeBrand] ADD CONSTRAINT [FK_tContentMessageIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [lekmer].[tContentMessageIncludeBrand] ADD CONSTRAINT [FK_tContentMessageIncludeBrand_tContentMessage] FOREIGN KEY ([ContentMessageId]) REFERENCES [lekmer].[tContentMessage] ([ContentMessageId])
GO
