CREATE TABLE [lekmer].[tIconTranslation]
(
[IconId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [lekmer].[tIconTranslation] ADD
CONSTRAINT [FK_tIconTranslation_tIcon] FOREIGN KEY ([IconId]) REFERENCES [lekmer].[tIcon] ([IconId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tIconTranslation] ADD CONSTRAINT [PK_tIconTranslation] PRIMARY KEY CLUSTERED  ([IconId], [LanguageId]) ON [PRIMARY]
GO

ALTER TABLE [lekmer].[tIconTranslation] ADD CONSTRAINT [FK_tIconTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
