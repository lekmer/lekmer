CREATE TABLE [lekmer].[tWishListPackageItem]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[WishListItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [PK_tWishListPackageItem] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [FK_tWishListPackageItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [FK_tWishListPackageItem_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tWishListPackageItem] ADD CONSTRAINT [FK_tWishListPackageItem_tWishListItem] FOREIGN KEY ([WishListItemId]) REFERENCES [lekmer].[tWishListItem] ([Id])
GO
