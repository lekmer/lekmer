CREATE TABLE [productlek].[tBlockProductSearchEsalesResult]
(
[BlockId] [int] NOT NULL,
[SecondaryTemplateId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD
CONSTRAINT [FK_tBlockProductSearchEsalesResult_tTemplate] FOREIGN KEY ([SecondaryTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD CONSTRAINT [PK_tBlockProductSearchEsalesResult] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockProductSearchEsalesResult] ADD CONSTRAINT [FK_tBlockProductSearchEsalesResult_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
