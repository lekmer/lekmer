CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeProduct_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
