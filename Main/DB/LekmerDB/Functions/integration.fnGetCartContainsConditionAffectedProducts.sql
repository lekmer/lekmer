SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [integration].[fnGetCartContainsConditionAffectedProducts](
	@ConditionId INT
)
RETURNS @tAffectedProducts TABLE (ProductId INT)
AS 
BEGIN
	INSERT INTO @tAffectedProducts (ProductId)
	
	      -- include products from categories without subcategories
	
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartContainsConditionIncludeCategory ic ON ic.CategoryId = p.CategoryId
		WHERE
			ic.ConditionId = @ConditionId
			AND
			ic.IncludeSubcategories = 0
		
	UNION -- include products from categories with subcategories
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionIncludeCategory ic
			CROSS APPLY [product].[fnGetSubCategories] (ic.CategoryId) sc
			INNER JOIN product.tProduct p ON p.CategoryId = sc.CategoryId
		WHERE
			ic.ConditionId = @ConditionId
			AND
			ic.IncludeSubcategories = 1
		
	UNION -- include products
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionIncludeProduct ip
		WHERE
			ip.ConditionId = @ConditionId
			
	EXCEPT -- exclude products from categories without subcategories
		
		SELECT
			ProductId
		FROM
			product.tProduct p
			INNER JOIN addon.tCartContainsConditionExcludeCategory ec ON ec.CategoryId = p.CategoryId
		WHERE
			ec.ConditionId = @ConditionId
			AND
			ec.IncludeSubcategories = 0
		
	EXCEPT -- exclude products from categories with subcategories
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionExcludeCategory ec
			CROSS APPLY [product].[fnGetSubCategories] (ec.CategoryId) sc
			INNER JOIN product.tProduct p ON p.CategoryId = sc.CategoryId
		WHERE
			ec.ConditionId = @ConditionId
			AND
			ec.IncludeSubcategories = 1
		
	EXCEPT -- exclude products
	
		SELECT
			ProductId
		FROM
			addon.tCartContainsConditionExcludeProduct ep
		WHERE
			ep.ConditionId = @ConditionId

	RETURN
END
GO
