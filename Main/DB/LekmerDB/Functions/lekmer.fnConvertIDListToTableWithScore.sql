SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [lekmer].[fnConvertIDListToTableWithScore](
	@IDList VARCHAR(MAX),
	@ScoreList VARCHAR(MAX),
	@Delimiter CHAR(1) = ';'
)
RETURNS @tbl TABLE(ErpId VARCHAR(50), Score DECIMAL(18, 6))
AS
BEGIN
	IF @IDList IS NULL OR @IDList = '' OR @ScoreList IS NULL OR @ScoreList = ''
		RETURN

	IF SUBSTRING(@IDList, LEN(@IDList), 1) <> @Delimiter
		SELECT @IDList = @IDList + @Delimiter
		
	IF SUBSTRING(@ScoreList, LEN(@ScoreList), 1) <> @Delimiter
		SELECT @ScoreList = @ScoreList + @Delimiter

	DECLARE @indBegin1 INT, @ind1 INT, @indBegin2 INT, @ind2 INT
	
	SET @indBegin1 = 1
	SET @indBegin2 = 1

	WHILE 1 = 1
	BEGIN
		SELECT @ind1 = CHARINDEX(@Delimiter, @IDList, @indBegin1)
		SELECT @ind2 = CHARINDEX(@Delimiter, @ScoreList, @indBegin2)

		IF @ind1 = 0 OR @ind2 = 0
			BREAK
		
		INSERT
			@tbl
		VALUES
		(
			CAST(SUBSTRING(@IDList, @indBegin1, @ind1 - @indBegin1) AS VARCHAR),
			CAST(REPLACE(SUBSTRING(@ScoreList, @indBegin2, @ind2 - @indBegin2), ',', '.') AS DECIMAL(18, 6))
		)

		SET @indBegin1 = @ind1 + 1
		SET @indBegin2 = @ind2 + 1
	END

	RETURN
END
GO
