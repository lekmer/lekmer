SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fGetPageUrl](
	@ContentPageId INT
)
RETURNS VARCHAR(1000)
AS
BEGIN

	DECLARE @PageUrl VARCHAR(1000);

	WITH NodeList (ContentNodeId, Lvl)
	AS
	(
		SELECT
			cn.[ContentNodeId],
			0
		FROM
			[sitestructure].[tContentNode] cn
		WHERE
			cn.[ContentNodeId] = @ContentPageId

		UNION ALL

		SELECT
			cn.[ParentContentNodeId],
			Lvl+1
		FROM
			[sitestructure].[tContentNode] cn
			INNER JOIN NodeList nl ON nl.ContentNodeId = cn.[ContentNodeId]
		WHERE
			cn.[ParentContentNodeId] IS NOT NULL
	)

	SELECT @PageUrl = (
		SELECT ISNULL(cp.[UrlTitle], '') + '/'
		FROM
			NodeList nl
			LEFT OUTER JOIN [sitestructure].[tContentPage] cp ON cp.[ContentNodeId] = nl.[ContentNodeId]
		ORDER BY
			nl.lvl DESC
		FOR XML PATH (''))

	SET @PageUrl = REPLACE(@PageUrl, '//', '/')
	SET @PageUrl = REPLACE(@PageUrl, '//', '/')
	SET @PageUrl = REPLACE(@PageUrl, '//', '/')
	SET @PageUrl = REPLACE(@PageUrl, '//', '/')
	SET @PageUrl = REPLACE(@PageUrl, '//', '/')
	SET @PageUrl = REPLACE(@PageUrl, '//', '/')

	SET @PageUrl = REPLACE(@PageUrl, ' ', '')

	RETURN @PageUrl
END
GO
