SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [lekmer].[fnGetParentCategories](
	@CategoryId int
)
RETURNS @tCategoryIds TABLE (CategoryId INT)
AS 
BEGIN
	WITH CategoryIds (CategoryId, ParentCategoryId) AS 
	(
		SELECT [c1].[CategoryId], [c1].[ParentCategoryId]
		FROM [product].[tCategory] c1
		WHERE [c1].[CategoryId] = @CategoryId
		UNION ALL
		SELECT [c2].[CategoryId], [c2].[ParentCategoryId]
		FROM [product].[tCategory] c2 
		JOIN [CategoryIds] ci ON [ci].[ParentCategoryId] = [c2].[CategoryId]
	)
	
	INSERT INTO @tCategoryIds (CategoryId)
	SELECT CategoryId FROM CategoryIds
	UNION ALL
	SELECT @CategoryId
	
	RETURN
END
GO
