
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vCustomOrderItem]
AS
	SELECT
		[oi].*,
		[ois].[OrderItemId]	AS 'OrderItemSize.OrderItemId',
		[ois].[SizeId]		AS 'OrderItemSize.SizeId',
		[ois].[ErpId]		AS 'OrderItemSize.ErpId',
		[ps].*,
		[ci].*,
		[lp].[BrandId]
	FROM
		[order].[vOrderItem] oi
		LEFT JOIN [lekmer].[tOrderItemSize] ois ON [oi].[OrderItem.OrderItemId] = [ois].[OrderItemId]
		LEFT JOIN [lekmer].[vProductSize] ps ON [oi].[OrderItem.ProductId] = [ps].[ProductSize.ProductId] AND [ois].[SizeId] = [ps].[ProductSize.SizeId]
		LEFT JOIN [product].[tProduct] p ON [oi].[OrderItem.ProductId] = [p].[ProductId]
		LEFT JOIN [media].[vCustomImageSecure] ci ON [ci].[Image.MediaId] = [p].[MediaId]
		LEFT JOIN [lekmer].[tLekmerProduct] lp ON [oi].[OrderItem.ProductId] = [lp].[ProductId]
GO
