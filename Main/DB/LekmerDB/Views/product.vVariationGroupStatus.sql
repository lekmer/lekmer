SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vVariationGroupStatus]
AS
SELECT
		VariationGroupStatusId 'VariationGroupStatus.Id',
		Title 'VariationGroupStatus.Title',
		CommonName 'VariationGroupStatus.CommonName'
FROM
		[product].[tVariationGroupStatus]
GO
