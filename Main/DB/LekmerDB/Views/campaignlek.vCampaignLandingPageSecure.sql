SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vCampaignLandingPageSecure]
AS
SELECT 
	[CampaignLandingPageId] AS 'CampaignLandingPage.CampaignLandingPageId',
	[CampaignId]			AS 'CampaignLandingPage.CampaignId',
	[WebTitle]				AS 'CampaignLandingPage.WebTitle',
	[Description]			AS 'CampaignLandingPage.Description'
FROM 
	[campaignlek].[tCampaignLandingPage]

GO
