
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [productlek].[vSupplier]
AS
SELECT
	[s].[SupplierId] 'Supplier.Id',
	[s].[SupplierNo] 'Supplier.SupplierNo',
	[s].[ActivePassiveCode] 'Supplier.ActivePassiveCode',
	[s].[Name] 'Supplier.Name',
	[s].[Address] 'Supplier.Address',
	[s].[ZipArea] 'Supplier.ZipArea',
	[s].[City] 'Supplier.City',
	[s].[CountryISO] 'Supplier.CountryISO',
	[s].[LanguageISO] 'Supplier.LanguageISO',
	[s].[VATRate] 'Supplier.VATRate',
	[s].[Phone1] 'Supplier.Phone1',
	[s].[Phone2] 'Supplier.Phone2',
	[s].[Email] 'Supplier.Email',
	[s].[IsDropShip] 'Supplier.IsDropShip',
	[s].[DropShipEmail] 'Supplier.DropShipEmail'
FROM
	[productlek].[tSupplier] s
GO
