SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vSizeDeviation]
AS
SELECT     
	sd.[SizeDeviationId] AS 'SizeDeviation.Id',
	sd.[Title] AS 'SizeDeviation.Title',
	sd.[CommonName] AS 'SizeDeviation.CommonName'
FROM
	[lekmer].[tSizeDeviation] sd
GO
