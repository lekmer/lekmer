SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [template].[vAliasFolder]
AS
SELECT 
	AliasFolderId AS 'AliasFolder.Id', 
	Title AS 'AliasFolder.Title', 
	ParentAliasFolderId AS 'AliasFolder.ParentAliasFolderId'
FROM         
    template.tAliasFolder
GO
