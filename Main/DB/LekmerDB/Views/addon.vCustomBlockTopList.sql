
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [addon].[vCustomBlockTopList]
AS
	SELECT
		*
	FROM
		[addon].[vBlockTopList] btl
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [btl].[BlockTopList.BlockId]
GO
