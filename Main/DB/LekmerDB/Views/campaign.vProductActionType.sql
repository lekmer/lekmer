SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [campaign].[vProductActionType]
as
	select
		ProductActionTypeId as 'ProductActionType.Id',
		Title as 'ProductActionType.Title',
		CommonName as 'ProductActionType.CommonName'
	from
		campaign.tProductActionType

GO
