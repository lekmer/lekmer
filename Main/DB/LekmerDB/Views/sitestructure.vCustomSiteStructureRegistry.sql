SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomSiteStructureRegistry]
as
	select
		*
	from
		[sitestructure].[vSiteStructureRegistry]
GO
