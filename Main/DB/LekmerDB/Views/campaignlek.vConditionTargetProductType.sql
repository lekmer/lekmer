SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vConditionTargetProductType]
AS
	SELECT
		[ConditionId] AS 'ConditionTargetProductType.ConditionId',
		[ProductTypeId] AS 'ConditionTargetProductType.ProductTypeId'
	FROM
		[campaignlek].[tConditionTargetProductType] t

GO
