SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vContentPageType]
AS
	select 
		[ContentPageTypeId] 'ContentPageType.Id',
		[Title] 'ContentPageType.Title',
		[CommonName] 'ContentPageType.CommonName'
	from 
		[sitestructure].[tContentPageType]

GO
