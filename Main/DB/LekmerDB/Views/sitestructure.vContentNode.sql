SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vContentNode]
AS
SELECT     
	cn.ContentNodeId AS 'ContentNode.ContentNodeId', 
	COALESCE (cnt.Title, cn.Title) AS 'ContentNode.Title', 
	cn.ParentContentNodeId AS 'ContentNode.ParentContentNodeId', 
	cn.ContentNodeTypeId AS 'ContentNodeType.ContentNodeTypeId', 
	cn.ContentNodeStatusId AS 'ContentNode.ContentNodeStatusId', 
	cn.Ordinal AS 'ContentNode.Ordinal', 
	cn.CommonName AS 'ContentNode.CommonName',
	cn.AccessId AS 'ContentNode.AccessId', 
	cn.SiteStructureRegistryId AS 'ContentNode.SiteStructureRegistryId', 
	t.Title AS 'ContentNodeType.Title', 
    t.CommonName AS 'ContentNodeType.CommonName', 
    ch.ChannelId AS 'ContentNode.ChannelId'
FROM	
	sitestructure.tContentNode AS cn 
	INNER JOIN sitestructure.tContentNodeType AS t ON t.ContentNodeTypeId = cn.ContentNodeTypeId 
    INNER JOIN sitestructure.tSiteStructureModuleChannel AS mch ON mch.SiteStructureRegistryId = cn.SiteStructureRegistryId
	INNER JOIN core.tChannel AS ch ON mch.ChannelId = ch.ChannelId
    LEFT JOIN sitestructure.tContentNodeTranslation AS cnt ON ch.LanguageId = cnt.LanguageId and cn.ContentNodeId = cnt.ContentNodeId
GO
