
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vSizeTableRow]
AS
	SELECT
		[st].[SizeTableRowId]	'SizeTableRow.SizeTableRowId',
		[st].[SizeTableId]		'SizeTableRow.SizeTableId',
		[st].[SizeId]			'SizeTableRow.SizeId',
		COALESCE ([stt].[Column1Value], [st].[Column1Value]) 'SizeTableRow.Column1Value',
		COALESCE ([stt].[Column2Value], [st].[Column2Value]) 'SizeTableRow.Column2Value',
		[st].[Ordinal]			'SizeTableRow.Ordinal',
		[l].[LanguageId]		'SizeTableRow.LanguageId'
	FROM 
		[lekmer].[tSizeTableRow] st
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tSizeTableRowTranslation] AS stt ON [stt].[SizeTableRowId] = [st].[SizeTableRowId] AND [stt].[LanguageId] = l.[LanguageId]
GO
