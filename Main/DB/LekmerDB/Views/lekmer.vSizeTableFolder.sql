SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vSizeTableFolder]
AS
	SELECT
		[stf].[SizeTableFolderId]		'SizeTableFolder.SizeTableFolderId',
		[stf].[ParentSizeTableFolderId]	'SizeTableFolder.ParentSizeTableFolderId',
		[stf].[Title]					'SizeTableFolder.Title'
	FROM 
		[lekmer].[tSizeTableFolder] stf
GO
