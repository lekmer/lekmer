SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [product].[vCustomStoreProduct]
as
	select
		*
	from
		[product].[vStoreProduct]
GO
