
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [productlek].[vProductChangeEvent]
AS
SELECT     
	[pce].[ProductChangeEventId] AS 'ProductChangeEvent.ProductChangeEventId' ,
	[pce].[ProductId] AS 'ProductChangeEvent.ProductId' ,
	[pce].[EventStatusId] AS 'ProductChangeEvent.EventStatusId' ,
	[pce].[CdonExportEventStatusId] AS 'ProductChangeEvent.CdonExportEventStatusId' ,
	[pce].[CreatedDate] AS 'ProductChangeEvent.CreatedDate' ,
	[pce].[ActionAppliedDate] AS 'ProductChangeEvent.ActionAppliedDate' ,
	[pce].[Reference] AS 'ProductChangeEvent.Reference'
FROM
	[productlek].[tProductChangeEvent] pce

GO
