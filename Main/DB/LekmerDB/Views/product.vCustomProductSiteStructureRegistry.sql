SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductSiteStructureRegistry]
as
	select
		*
	from
		[product].[vProductSiteStructureRegistry]
GO
