
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [orderlek].[vCustomPaymentTypeSecure]
AS
	SELECT
		[pt].*,
		CAST(0 AS BIT)	'PaymentType.IsPersonDefault',
		CAST(0 AS BIT)	'PaymentType.IsCompanyDefault',
		0.00			'PaymentType.PaymentCost'
	FROM
		[orderlek].[vPaymentTypeSecure] pt
GO
