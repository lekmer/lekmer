SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vRatingItemTranslation]
AS
SELECT 
	[RatingItemId] AS 'RatingItemTranslation.RatingItemId',
	[LanguageId] AS 'RatingItemTranslation.LanguageId',
	[Title] AS 'RatingItemTranslation.Title',
	[Description] AS 'RatingItemTranslation.Description'
FROM 
	[review].[tRatingItemTranslation]

GO
