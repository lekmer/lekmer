SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vBlockProductLatestFeedbackList]
AS
	SELECT
		[BlockId] AS 'BlockProductLatestFeedbackList.BlockId',
		[NumberOfItems] AS 'BlockProductLatestFeedbackList.NumberOfItems'
	FROM
		[review].[tBlockProductLatestFeedbackList]
GO
