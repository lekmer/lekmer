
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vBlockBrandTopList]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopList.BlockId',
		b.[IncludeAllCategories] AS 'BlockBrandTopList.IncludeAllCategories',
		b.[OrderStatisticsDayCount] AS 'BlockBrandTopList.OrderStatisticsDayCount',
		b.[LinkContentNodeId] AS 'BlockBrandTopList.LinkContentNodeId',
		[bs].*
	FROM
		[lekmer].[tBlockBrandTopList] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[BlockId]
GO
