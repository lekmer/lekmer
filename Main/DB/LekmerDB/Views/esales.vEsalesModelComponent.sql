SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [esales].[vEsalesModelComponent]
AS
	SELECT
		[emc].[ModelComponentId] AS 'EsalesModelComponent.ModelComponentId',
		[emc].[ModelId] AS 'EsalesModelComponent.ModelId',
		[emc].[Title] AS 'EsalesModelComponent.Title',
		[emc].[CommonName] AS 'EsalesModelComponent.CommonName',
		[emc].[Ordinal] AS 'EsalesModelComponent.Ordinal'
	FROM
		[esales].[tEsalesModelComponent] emc

GO
