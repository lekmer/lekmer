
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [productlek].[vProductRegistryDeliveryTime]
AS
SELECT
	[prdt].[DeliveryTimeId] AS 'DeliveryTime.Id',
	[prdt].[From] AS 'DeliveryTime.From',
	[prdt].[To] AS 'DeliveryTime.To',
	[prdt].[Format] AS 'DeliveryTime.Format',
	[prdt].[ProductRegistryId] AS 'DeliveryTime.ProductRegistryId',
	[pmc].[ChannelId] AS 'DeliveryTime.ProductRegistryChannelId'
FROM
	[productlek].[tProductRegistryDeliveryTime] AS prdt
	INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prdt].[ProductRegistryId]




GO
