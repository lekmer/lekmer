SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vCampaignLandingPageTranslation]
AS
SELECT 
	[CampaignLandingPageId] AS 'CampaignLandingPageTranslation.CampaignLandingPageId',
	[LanguageId]			AS 'CampaignLandingPageTranslation.LanguageId',
	[WebTitle]				AS 'CampaignLandingPageTranslation.WebTitle',
	[Description]			AS 'CampaignLandingPageTranslation.Description'
FROM 
	[campaignlek].[tCampaignLandingPageTranslation]
GO
