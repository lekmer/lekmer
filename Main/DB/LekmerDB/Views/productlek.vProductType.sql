SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vProductType]
AS
SELECT     
      [ProductTypeId] 'ProductType.Id',
      [Title] 'ProductType.Title',
      [CommonName] 'ProductType.CommonName'
FROM
      [productlek].[tProductType]
GO
