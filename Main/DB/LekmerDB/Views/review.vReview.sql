
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vReview]
AS
	SELECT
		r.[ReviewId] 'Review.ReviewId',
		r.[RatingReviewFeedbackId] 'Review.RatingReviewFeedbackId',
		r.[AuthorName] 'Review.AuthorName',
		r.[Title] 'Review.Title',
		r.[Message] 'Review.Message',
		r.[Email] 'Review.Email'
	FROM 
		[review].[tReview] r

GO
