SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomOrderAuditType]
as
	select
		*
	from
		[order].[vOrderAuditType]

GO
