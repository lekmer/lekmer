
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vCategory]
AS
SELECT     
	c.CategoryId 'Category.Id',
	c.ParentCategoryId 'Category.ParentCategoryId',
	COALESCE (ct.Title, c.Title) 'Category.Title', 
	c.ErpId 'Category.ErpId',
	t.LanguageId 'Category.LanguageId',
	t.[ChannelId] 'Category.ChannelId'
FROM
	[product].[tCategory] c
	CROSS JOIN [core].[tChannel] t
	LEFT JOIN [product].[tCategoryTranslation] ct ON ct.CategoryId = c.CategoryId AND ct.LanguageId = t.LanguageId

GO
