SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vCustomProductMedia]
AS
SELECT
	p.ProductId 'ProductMedia.ProductId',
	p.MediaId 'ProductMedia.MediaId',
	p.ProductImageGroupId 'ProductMedia.GroupId',
	p.Ordinal 'ProductMedia.Ordinal',
	m.*,
	media.tMediaFormat.Extension AS 'MediaFormat.Extension',
	media.tMediaFormat.CommonName AS 'MediaFormat.CommonName'
FROM
    product.tProductMedia AS p
    INNER JOIN media.vCustomMedia m ON p.MediaId = m.[Media.Id]
	INNER JOIN media.tMediaFormat ON m.[Media.FormatId] = media.tMediaFormat.MediaFormatId

GO
