SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vFlagSecure]
AS
SELECT     
      FlagId AS 'Flag.Id',
      Title AS 'Flag.Title',
      Class AS 'Flag.Class', 
      Ordinal AS 'Flag.Ordinal'
FROM
      lekmer.tFlag
GO
