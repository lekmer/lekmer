SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create VIEW lekmer.[vProductTranslation]
AS
select
	pt.[ProductId] AS 'Product.Id',
	pt.[Title] AS 'Product.Title',
	pt.[WebShopTitle] AS 'Product.WebShopTitle',
	pt.[Description] AS 'Product.Description',
	pt.[ShortDescription] AS 'Product.ShortDescription',
	l.[LanguageId] AS 'Language.Id',
	l.[Title] AS 'Language.Title',
	l.[ISO] AS 'Language.ISO'
from
	product.[tProductTranslation] AS pt
	INNER JOIN [core].[tLanguage] l ON pt.LanguageId = l.LanguageId

GO
