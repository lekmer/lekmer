
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vContentMessage]
AS
	SELECT
		[cm].[ContentMessageId]						'ContentMessage.ContentMessageId',
		[cm].[ContentMessageFolderId]				'ContentMessage.ContentMessageFolderId',
		[cm].[CommonName]							'ContentMessage.CommonName',
		COALESCE ([cmt].[Message], [cm].[Message])	'ContentMessage.Message',
		[cm].[IsDropShip]							'ContentMessage.IsDropShip',
		[cm].[StartDate]							'EntityTimeLimit.StartDate',
		[cm].[EndDate]								'EntityTimeLimit.EndDate',
		[cm].[StartDailyIntervalMinutes]			'EntityTimeLimit.StartDailyIntervalMinutes',
		[cm].[EndDailyIntervalMinutes]				'EntityTimeLimit.EndDailyIntervalMinutes',
		[l].[LanguageId]							'ContentMessage.LanguageId'
	FROM 
		[lekmer].[tContentMessage] cm
		CROSS JOIN [core].[tLanguage] AS l
		LEFT JOIN [lekmer].[tContentMessageTranslation] AS cmt ON [cmt].[ContentMessageId] = [cm].[ContentMessageId] AND [cmt].[LanguageId] = l.[LanguageId]
GO
