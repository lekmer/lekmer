SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create VIEW [lekmer].[vProductSeoSettingTranslation]
AS
select
	pt.[ProductId] AS 'Product.Id',
	pt.[Title] AS 'ProductSeoSetting.Title',
	pt.[Keywords] AS 'ProductSeoSetting.Keywords',
	pt.[Description] AS 'ProductSeoSetting.Description',
	l.[LanguageId] AS 'Language.Id',
	l.[Title] AS 'Language.Title',
	l.[ISO] AS 'Language.ISO'
from
	product.[tProductSeoSettingTranslation] AS pt
	INNER JOIN [core].[tLanguage] l ON pt.LanguageId = l.LanguageId


GO
