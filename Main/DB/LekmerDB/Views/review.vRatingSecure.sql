
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingSecure]
AS
	SELECT
		r.[RatingId]		'Rating.RatingId',
		r.[RatingFolderId]	'Rating.RatingFolderId',
		r.[RatingRegistryId]'Rating.RatingRegistryId',
		r.[CommonName]		'Rating.CommonName',
		r.[Title]			'Rating.Title',
		r.[Description]		'Rating.Description',
		r.[CommonForVariants]	'Rating.CommonForVariants'
	FROM 
		[review].[tRating] r
GO
