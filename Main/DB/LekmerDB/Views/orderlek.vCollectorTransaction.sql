SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [orderlek].[vCollectorTransaction]
AS
SELECT
	[ct].[TransactionId],
	[ct].[StoreId],
	[ct].[TransactionTypeId],

	CASE [ct].[TransactionTypeId]
		WHEN 0 THEN 'None'
		WHEN 1 THEN 'GetAddress'
		WHEN 2 THEN 'AddInvoice'
		WHEN 3 THEN 'InvoiceNotification'
		WHEN 4 THEN 'AddInvoiceTimeout'
		ELSE CONVERT(VARCHAR(50), [ct].[TransactionTypeId])
	END AS 'TransactionType.Name',

	[ct].[TimeoutProcessed],
	[ct].[StatusCode],

	CASE [ct].[StatusCode]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Ok'
		WHEN 2 THEN 'TimeoutResponse'
		WHEN 3 THEN 'UnspecifiedError'
		WHEN 4 THEN 'SoapFault'
		ELSE CONVERT(VARCHAR(50), [ct].[StatusCode])
	END AS 'StatusCode.Name',

	[ct].[Created],
	[ct].[CivicNumber],
	[ct].[Duration],
	[ct].[OrderId],
	[ct].[ProductCode],
	[ct].[AvailableReservationAmount],
	[ct].[InvoiceNo],
	[ct].[InvoiceStatus],

	CASE [ct].[InvoiceStatus]
		WHEN 0 THEN 'OnHold'
		WHEN 1 THEN 'Pending'
		WHEN 2 THEN 'Activated'
		WHEN 5 THEN 'Rejected'
		ELSE CONVERT(VARCHAR(50), [ct].[InvoiceStatus])
	END AS 'InvoiceStatus.Name',

	[ct].[InvoiceUrl],
	[ct].[FaultCode],
	[ct].[FaultMessage],
	[ct].[ErrorMessage],
	[ct].[ResponseContent]
FROM
	[orderlek].[tCollectorTransaction] ct
GO
