
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vBlockBrandList]
AS
SELECT 
	[bbl].[BlockId] 'BlockBrandList.BlockId',
	[bbl].[IncludeAllBrands] 'BlockBrandList.IncludeAllBrands',
	[bbl].[LinkContentNodeId] 'BlockBrandList.LinkContentNodeId',
	[bs].*
FROM 
	[lekmer].[tBlockBrandList] bbl
	INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bbl].[BlockId]
GO
