SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomFreightValueActionCurrency]
as
	select
		*
	from
		[campaign].[vFreightValueActionCurrency]
GO
