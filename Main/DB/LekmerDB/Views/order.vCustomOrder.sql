
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vCustomOrder]
AS
	SELECT
		o.[Order.OrderId],
		o.[Order.Number],
		o.[Order.CustomerId],
		o.[Order.BillingAddressId],
		o.[Order.DeliveryAddressId],
		o.[Order.CreatedDate],
		o.[Order.FreightCost],
		o.[Order.Email],
		o.[Order.DeliveryMethodId],
		o.[Order.ChannelId],
		o.[Order.OrderStatusId],
		o.[Order.IP],
		o.[Order.DeliveryTrackingId],
		o.[OrderStatus.Id],
		o.[OrderStatus.Title],
		o.[OrderStatus.CommonName],
		[lo].[PaymentCost] AS [Lekmer.PaymentCost],
		[lo].[CustomerIdentificationKey] AS [Lekmer.CustomerIdentificationKey],
		[lo].[FeedbackToken] AS [Lekmer.FeedbackToken],
		[lo].[AlternateAddressId] AS [Lekmer.AlternateAddressId],
		[lo].[NeedSendInsuranceInfo] AS [Lekmer.NeedSendInsuranceInfo],
		[lo].[CivicNumber] AS [Lekmer.CivicNumber],
		[lo].[OptionalDeliveryMethodId] AS [Lekmer.OptionalDeliveryMethodId],
		[lo].[OptionalFreightCost] AS [Lekmer.OptionalFreightCost],
		[lo].[DiapersDeliveryMethodId] AS [Lekmer.DiapersDeliveryMethodId],
		[lo].[DiapersFreightCost] AS [Lekmer.DiapersFreightCost],
		[lo].[KcoId] AS [Lekmer.KcoId],
		[lo].[UserAgent] AS [Lekmer.UserAgent]
	FROM
		[order].[vOrder] AS o
		LEFT OUTER JOIN [lekmer].[tLekmerOrder] AS lo ON o.[Order.OrderId] = lo.[OrderId]
GO
