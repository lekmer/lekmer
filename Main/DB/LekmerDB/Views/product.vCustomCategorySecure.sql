
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [product].[vCustomCategorySecure]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		[l].[PackageInfo] AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder],
		[l].[DeliveryTimeId] AS [LekmerCategory.DeliveryTimeId],
		[l].[MinQuantityInStock] AS [LekmerCategory.MinQuantityInStock],
		[dt].*
	FROM
		[product].[vCategorySecure] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [l].[DeliveryTimeId]



GO
