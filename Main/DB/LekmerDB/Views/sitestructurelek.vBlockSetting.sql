
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [sitestructurelek].[vBlockSetting]
AS
SELECT   
	   [bs].[BlockId]				'BlockSetting.BlockId'
      ,[bs].[ColumnCount]			'BlockSetting.ColumnCount'
      ,[bs].[ColumnCountMobile]		'BlockSetting.ColumnCountMobile'
      ,[bs].[RowCount]				'BlockSetting.RowCount'
      ,[bs].[RowCountMobile]		'BlockSetting.RowCountMobile'
      ,[bs].[TotalItemCount]		'BlockSetting.TotalItemCount'
      ,[bs].[TotalItemCountMobile]	'BlockSetting.TotalItemCountMobile'
FROM 
	[sitestructurelek].[tBlockSetting] AS bs


GO
