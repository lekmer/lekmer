SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [template].[vCustomInclude]
as
	select
		*
	from
		[template].[vInclude]
GO
