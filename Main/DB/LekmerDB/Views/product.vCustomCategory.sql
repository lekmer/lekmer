
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [product].[vCustomCategory]
AS
	SELECT
		[c].*,
		[l].[AllowMultipleSizesPurchase] AS [LekmerCategory.AllowMultipleSizesPurchase],
		COALESCE ([lct].[PackageInfo], [l].[PackageInfo]) AS [LekmerCategory.PackageInfo],
		[l].[MonitorThreshold] AS [LekmerCategory.MonitorThreshold],
		[l].[MaxQuantityPerOrder] AS [LekmerCategory.MaxQuantityPerOrder],
		[l].[DeliveryTimeId] AS [LekmerCategory.DeliveryTimeId],
		[l].[MinQuantityInStock] AS [LekmerCategory.MinQuantityInStock],
		[dt].*
	FROM
		[product].[vCategory] c
		LEFT JOIN [lekmer].[tLekmerCategory] l ON [c].[Category.Id] = [l].[CategoryId]
		LEFT JOIN [productlek].[tLekmerCategoryTranslation] lct ON [lct].[CategoryId] = [c].[Category.Id] AND [lct].[LanguageId] = [c].[Category.LanguageId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [l].[DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Category.ChannelId]



GO
