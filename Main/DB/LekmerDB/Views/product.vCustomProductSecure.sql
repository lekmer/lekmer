
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO












CREATE  VIEW [product].[vCustomProductSecure]
AS 
	SELECT 
		p.*, 
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.IsAbove60L],
		[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[lp].[Lekmer.SupplierArticleNumber],
		[lp].[Lekmer.IsDropShip],
		'' AS 'Lekmer.UrlTitle',
		[dt].*,
		NULL AS 'Product.ParentContentNodeId',
		NULL AS 'Lekmer.RecommendedPrice'
	FROM 
		[product].[vProductSecure] AS p 
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]		
		LEFT JOIN [productlek].[vDeliveryTimeSecure] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId]











GO
