SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vProductSimilar]
AS
SELECT     
	ps.ProductId 'ProductSimilar.ProductId',
	ps.SimilarProductId 'ProductSimilar.SimilarProductId',
	ps.Score 'ProductSimilar.Score'
FROM
	[lekmer].[tProductSimilar] ps
GO
