SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vBlockProductRelationListItem]
AS
	SELECT 
		[BlockId] 'BlockProductRelationListItem.BlockId',
		[RelationListTypeId] 'BlockProductRelationListItem.RelationListTypeId'
	FROM 
		[product].[tBlockProductRelationListItem]
GO
