
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- View

CREATE VIEW [productlek].[vDeliveryTime]
AS
SELECT     
	[dt].[DeliveryTimeId] AS 'DeliveryTime.Id',
	[prdt].[From] AS 'DeliveryTime.From',
	[prdt].[To] AS 'DeliveryTime.To',
	[prdt].[Format] AS 'DeliveryTime.Format',
	[c].[ChannelId] AS 'DeliveryTime.ChannelId',
	NULL AS 'DeliveryTime.ProductRegistryId',
	NULL AS 'DeliveryTime.ProductRegistryChannelId'
FROM
	[productlek].[tDeliveryTime] dt
    CROSS JOIN core.tChannel AS c
    LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ChannelId]
    LEFT JOIN [product].[tProductRegistry] pr ON [pr].[ProductRegistryId] = [pmc].[ProductRegistryId]
    LEFT JOIN [productlek].[tProductRegistryDeliveryTime] prdt ON [prdt].[ProductRegistryId] = [pr].[ProductRegistryId]
		AND [prdt].[DeliveryTimeId] = [dt].[DeliveryTimeId]





GO
