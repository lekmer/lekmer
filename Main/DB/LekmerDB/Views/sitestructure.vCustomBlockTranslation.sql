SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlockTranslation]
as
	select
		*
	from
		[sitestructure].[vBlockTranslation]
GO
