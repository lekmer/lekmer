SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vOrderAuditType]
AS
SELECT 
	OAT.OrderAuditTypeId AS 'OrderAuditType.OrderAuditTypeId',
	OAT.Title AS 'OrderAuditType.Title',
	OAT.CommonName AS 'OrderAuditType.CommonName'
  FROM [order].[tOrderAuditType] OAT 
GO
