SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vRecommendedPrice]
AS
SELECT     lekmer.tRecommendedPrice.*
FROM         lekmer.tRecommendedPrice

GO
