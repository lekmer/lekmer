
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [media].[vCustomMedia]
AS
SELECT
	*
FROM
	[media].[vMedia]

GO
