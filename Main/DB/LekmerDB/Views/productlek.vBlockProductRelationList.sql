SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [productlek].[vBlockProductRelationList]
AS
	SELECT 
		[bprl].[BlockId] 'BlockProductRelationList.BlockId',
		[pso].*,
		[bs].*
	FROM 
		[productlek].[tBlockProductRelationList] bprl
		INNER JOIN product.vCustomProductSortOrder pso ON [pso].[ProductSortOrder.Id] = [bprl].[ProductSortOrderId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [bprl].[BlockId]
GO
