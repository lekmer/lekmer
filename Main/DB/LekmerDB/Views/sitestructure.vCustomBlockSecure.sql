
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vCustomBlockSecure]
AS
SELECT
	[b].*,
	[l].[StartDate] AS 'Block.StartDate',
	[l].[EndDate] AS 'Block.EndDate',
	[l].[StartDailyIntervalMinutes] AS 'Block.StartDailyIntervalMinutes',
	[l].[EndDailyIntervalMinutes] AS 'Block.EndDailyIntervalMinutes',
	[l].[ShowOnDesktop] AS 'Block.ShowOnDesktop',
	[l].[ShowOnMobile] AS 'Block.ShowOnMobile',
	[l].[CanBeUsedAsFallbackOption] AS 'Block.CanBeUsedAsFallbackOption'
FROM
	[sitestructure].[vBlockSecure] b
	LEFT JOIN [lekmer].[tLekmerBlock] l ON b.[Block.BlockId] = l.[BlockId]

GO
