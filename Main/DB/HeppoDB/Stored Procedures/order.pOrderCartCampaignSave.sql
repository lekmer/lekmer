SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [order].[pOrderCartCampaignSave]
	@Id int,
	@CampaignId int,
	@OrderId int,
	@Title nvarchar(500)
as
begin
	update
		[order].tOrderCartCampaign
	set
		CampaignId = @CampaignId,
		OrderId = @OrderId,
		Title = @Title
	where
		OrderCartCampaignId = @Id
	
	if @@rowcount = 0
	begin
		insert
			[order].tOrderCartCampaign
		(
			CampaignId,
			OrderId,
			Title
		)
		values
		(
			@CampaignId,
			@OrderId,
			@Title
		)
	end
end
GO
