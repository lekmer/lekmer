SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pMessageCustomFieldInsert]
@MessageId			uniqueidentifier,
@Name				nvarchar(200),
@Value				nvarchar(MAX)
as
begin

	insert into
		[messaging].[tMessageCustomField]
		(
			MessageId,
			[Name],
			[Value]
		)
	values
		(
			@MessageId,
			@Name,
			@Value
		)

end




GO
