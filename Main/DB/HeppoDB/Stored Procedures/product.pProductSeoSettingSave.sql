
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Roman G.		Date: 15.09.2008		Time: 17:00
Description:
      Save/Update SEOSettings for Product.
/***********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 14:35
Description: delete try - cath  block and error logging 
**********************************************************/
*/
CREATE PROCEDURE [product].[pProductSeoSettingSave]
	@ProductId		INT,
	@Title			NVARCHAR(500),
	@Description	NVARCHAR(500),
	@Keywords		NVARCHAR(500)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[product].[tProductSeoSetting]
	SET
		[Title] = @Title,
		[Description] = @Description,
		[Keywords] = @Keywords
	WHERE
		[ProductId] = @ProductId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT [product].[tProductSeoSetting]
		(				
			[ProductId],
			[Title],
			[Description],
			[Keywords]
		)
		VALUES
		(
			@ProductId,
			@Title,
			@Description,
			@Keywords
		)
	END
END
GO
