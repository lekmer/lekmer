SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductTypeGetIdAllByBlock]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		[bpt].[ProductTypeId]
	FROM
		[productlek].[tBlockProductType] bpt
	WHERE
		[bpt].[BlockId] = @BlockId
END
GO
