SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityFunctionGetAllByEntityCommonName]
	@EntityCommonName	varchar(50)
AS
begin
	set nocount on
	select
		F.*
	from
		[template].[vCustomEntityFunction] F
		INNER JOIN [template].[vCustomEntity] E ON E.[Entity.Id] = F.[EntityFunction.EntityId]
	where
		E.[Entity.CommonName] = @EntityCommonName
	ORDER BY F.[EntityFunction.CommonName]
end

GO
