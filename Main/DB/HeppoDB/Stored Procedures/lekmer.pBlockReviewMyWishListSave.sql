SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure  [lekmer].[pBlockReviewMyWishListSave]
	@BlockId int,
	@ColumnCount  int,
	@RowCount int
as
begin
	update [lekmer].[tBlockReviewMyWishList] 
	set
		ColumnCount = @ColumnCount,
		[RowCount] = @RowCount
	where
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert [lekmer].[tBlockReviewMyWishList] 
	(
		BlockId,
		ColumnCount,
		[RowCount]
	)
	values
	(
		@BlockId,
		@ColumnCount,
		@RowCount
	)
end
GO
