SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCheckMediaFileUsage]
	@MediaId	INT
AS	
BEGIN
	IF NOT EXISTS (SELECT 1 FROM [media].[tMedia] WHERE [MediaId] = @MediaId)
	BEGIN
		RETURN 1 -- NOT EXISTS
	END
	
	IF EXISTS (
		SELECT 1 FROM [media].[tMedia] m
		LEFT JOIN [product].[tProduct] p ON [p].[MediaId] = [m].[MediaId]
		LEFT JOIN [product].[tProductImage] ip ON [ip].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tBrand] b ON [b].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tIcon] i ON [i].[MediaId] = [m].[MediaId]
		WHERE 
			[m].[MediaId] = @MediaId
			AND [p].[MediaId] IS NULL 
			AND [ip].[MediaId] IS NULL
			AND [b].[MediaId] IS NULL 
			AND [i].[MediaId] IS NULL
	)
	BEGIN
		RETURN 2 -- NOT_USED
	END
	
	IF EXISTS (
		SELECT 1 FROM [media].[tMedia] m
		LEFT JOIN [product].[tProduct] p ON [p].[MediaId] = [m].[MediaId]
		LEFT JOIN [product].[tProductImage] ip ON [ip].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tBrand] b ON [b].[MediaId] = [m].[MediaId]
		LEFT JOIN [lekmer].[tIcon] i ON [i].[MediaId] = [m].[MediaId]
		WHERE 
			[m].[MediaId] = @MediaId
			AND (
					[p].[MediaId] IS NOT NULL 
					OR [ip].[MediaId] IS NOT NULL 
					OR [b].[MediaId] IS NOT NULL 
					OR [i].[MediaId] IS NOT NULL
				)
	)
	BEGIN
		RETURN 3 -- USED
	END
	
	RETURN 4 -- UNKNOWN
END
GO
