SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderPaymentSave]
	@OrderPaymentId INT,
	@OrderId INT,
	@PaymentTypeId INT,
	@Price DECIMAL(16,2),
	@Vat DECIMAL(16,2),
	@ReferenceId VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderPayment]
	SET
		OrderId = @OrderId,
		PaymentTypeId = @PaymentTypeId,
		Price = @Price,
		VAT = @VAT,
		ReferenceId = @ReferenceId
	WHERE
		OrderPaymentId = @OrderPaymentId

	IF  @@ROWCOUNT = 0
	begin	
		INSERT INTO [order].[tOrderPayment]
		(
			OrderId,
			PaymentTypeId,
			Price,
			VAT,
			ReferenceId
		)
		VALUES
		(
			@OrderId,
			@PaymentTypeId,
			@Price,
			@Vat,
			@ReferenceId
		)
		SET @OrderPaymentId = cast(scope_identity() as int)
	end
	
	return @OrderPaymentId
END

GO
