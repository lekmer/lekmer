SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--DROP PROCEDURE [lekmer].[pPackageWithoutSizesDecreaseNumberInStock]

CREATE PROCEDURE [productlek].[pPackageWithoutSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	DECLARE @PackageId INT
	SET @PackageId = (SELECT [PackageId] FROM [productlek].[tPackage] WHERE [MasterProductId] = @PackageMasterProductId)

	DECLARE @tmpPackageProduct TABLE (ProductId INT)
	INSERT INTO @tmpPackageProduct (ProductId)
	SELECT DISTINCT [ProductId] FROM [productlek].[tPackageProduct] WHERE [PackageId] = @PackageId

	-- Update Package Products NumberInSock		
	UPDATE
		p
	SET
		[p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
									ELSE [p].[NumberInStock] - @Quantity
							   END)
	FROM
		[product].[tProduct] p
		INNER JOIN @tmpPackageProduct pp ON [pp].[ProductId] = [p].[ProductId]
	
	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProduct) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProduct)
		SET @NewNumberInStock = (SELECT [NumberInStock] FROM [product].[tProduct] WHERE [ProductId] = @tmpProductId)
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId, @NewNumberInStock
		DELETE @tmpPackageProduct WHERE [ProductId] = @tmpProductId
	END
END
GO
