SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductGetIdAll]
	@ChannelId			INT,
	@CustomerId			INT,
	@ProductActionId	INT
AS
BEGIN
	SELECT
		p.[Product.Id]
	FROM
		[campaignlek].[tFixedDiscountActionIncludeProduct] fdaip
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = fdaip.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		fdaip.[ProductActionId] = @ProductActionId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
