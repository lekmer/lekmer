
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportHeppoFailedOrders]

AS
begin
	set nocount on
	
		
		select OrderId, CreatedDate, Email, case ChannelId when 1 then 'Sweden'
											when 4 then 'Norway'
											when 657 then 'Denmark'
											when 1000003 then 'Finland'
											when 1000006 then 'France'
											when 1000007 then 'Netherlands'
											end as Channel
		from [order].torder
		where --Createddate > DATEADD(day, -1, Convert(varchar, GETDATE(), 112))
		--and CreatedDate < GETDATE()
		--and 
			(
				OrderStatusId = 5 
				or OrderStatusId = 6
			)

end
GO
