SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman D.	Date: 21.05.2009	Time: 16:00
Description:	Created
*/

create PROCEDURE [customer].[pBlockRegisterDelete]
@BlockId	int
as
begin
	delete
		[customer].[tBlockRegister]
	where
		[BlockId] = @BlockId
end
GO
