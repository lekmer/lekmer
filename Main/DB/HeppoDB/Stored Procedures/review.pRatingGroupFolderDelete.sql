SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupFolderDelete]
	@RatingGroupFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [review].[tRatingGroupFolder]
	WHERE [RatingGroupFolderId] = @RatingGroupFolderId
END
GO
