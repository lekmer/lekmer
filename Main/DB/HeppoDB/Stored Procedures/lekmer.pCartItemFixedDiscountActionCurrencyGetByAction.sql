SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemFixedDiscountActionCurrency]
	WHERE 
		[CartItemFixedDiscountActionCurrency.CartActionId] = @CartActionId
END

GO
