SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pLekmerOrderGetByFeedbackToken]
	@FeedbackToken UNIQUEIDENTIFIER
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Lekmer.FeedbackToken] = @FeedbackToken
END

GO
