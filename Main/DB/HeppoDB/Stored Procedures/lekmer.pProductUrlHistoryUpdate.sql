SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryUpdate]
	@ProductUrlHistoryId	INT
AS 
BEGIN 
	UPDATE 
		lekmer.tProductUrlHistory
	SET 
		UsageAmount = UsageAmount + 1,
		LastUsageDate = GETDATE()
	WHERE
		ProductUrlHistoryId = @ProductUrlHistoryId
END 
GO
