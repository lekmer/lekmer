SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pComponentParameterDelete]
	@ComponentParameterId	INT
AS
BEGIN
	DELETE
		[template].[tComponentParameter]
	WHERE
		[ComponentParameterId]  = @ComponentParameterId
END	


GO
