SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 13.01.2009
Description:
			Created 
*/

CREATE PROCEDURE [core].[pCacheUpdateGetFromDate]
	@FromDate	datetime
AS
BEGIN
	SET NOCOUNT ON

	select
		ManagerName,
		UpdateType,
		[Key],
		CreationDate
	from
		[core].[tCacheUpdate]
	where
		[CreationDate] >= @FromDate
	order by
		[CreationDate] asc
END
GO
