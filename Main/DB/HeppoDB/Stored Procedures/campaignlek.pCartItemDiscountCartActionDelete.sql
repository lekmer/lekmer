SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionDelete]
	@CartActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pCartItemDiscountCartActionCurrencyDeleteAll] @CartActionId

	DELETE FROM [campaignlek].[tCartItemDiscountCartAction]
	WHERE CartActionId = @CartActionId
END
GO
