SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [core].[pChannelGet]
	@ApplicationName varchar(100)
as
begin
	set nocount on

	select
		*
	from
		[core].[vCustomChannel]
	where
		[Channel.ApplicationName] = @ApplicationName
end

GO
