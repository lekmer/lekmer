SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pImageGetById] 
	@MediaId INT,
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		i.*
	FROM   
		[media].[vCustomImage] i INNER JOIN 
		core.tChannel c ON i.[Image.LanguageId] = c.LanguageId
	WHERE  
		i.[Image.MediaId] = @MediaId AND c.ChannelId = @ChannelId
END

GO
