
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRating_Search]
	@SearchCriteria	NVARCHAR(MAX)
AS
BEGIN
	SET @SearchCriteria = [generic].[fPrepareSearchParameter](@SearchCriteria)
	
	SELECT 
		r.*
	FROM 
		[review].[vRatingSecure] r
	WHERE
		r.[Rating.Title] LIKE @SearchCriteria ESCAPE '\'
		OR r.[Rating.CommonName] LIKE @SearchCriteria ESCAPE '\'
END
GO
