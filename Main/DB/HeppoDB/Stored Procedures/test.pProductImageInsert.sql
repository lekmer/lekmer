SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure test.pProductImageInsert
	@ProductId int, 
	@ImageGroupId int,
	@MediaId int,
	@Ordinal int
as
begin
	insert product.tProductImage
	(
		ProductId, 
		ProductImageGroupId,
		MediaId,
		Ordinal
	)
	values
	(
		@ProductId, 
		@ImageGroupId,
		@MediaId,
		@Ordinal
	)
end
GO
