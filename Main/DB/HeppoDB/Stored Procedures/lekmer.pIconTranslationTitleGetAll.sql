SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconTranslationTitleGetAll]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    [it].[IconTranslation.IconId] AS 'Id',
		[it].[IconTranslation.LanguageId] AS 'LanguageId',
		[it].[IconTranslation.Title] AS 'Value'
	FROM
	    [lekmer].[vIconTranslation] it
	WHERE 
		[it].[IconTranslation.IconId] = @IconId
END
GO
