
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportHeppoDailyOrdersResultset]
	@DateFrom	DATETIME = NULL,
	@DateTo		DATETIME = NULL

AS
begin
	set nocount on

		SET @DateTo = DATEADD(day, 1, Convert(varchar, @DateTo, 112))
		
		IF @DateFrom IS NULL
		BEGIN
			SET @DateFrom = DATEADD(day, -1, Convert(varchar, GETDATE(), 112))
		END
		
		IF @DateTo IS NULL
		BEGIN
			SET @DateTo = DATEADD(day, 1, Convert(varchar, GETDATE(), 112))
		END
		
		if object_id('tempdb..#tTmpResult') is not null
		drop table #tTmpResult
		
		create table #tTmpResult
		(			
			[Date] datetime not null,
			NumberOfOrders int not null,
			AverageOrderPrice decimal(16,2) not null,
			TotalOrderValue decimal(16,2) not null,
			Channel nvarchar (50)
			--constraint PK_#tProductPriceIncludingVatLess primary key(ProductId, PricelistId)
		)
		
		insert into #tTmpResult([Date], NumberOfOrders, AverageOrderPrice, TotalOrderValue, Channel)
		select
			x.[Date] as 'Date',
			COUNT(x.OrderId) as 'NumberOfOrders',
			(SUM(x.Price)/COUNT(x.OrderId) * 0.8) as 'AvrageOrderPrice',
			(SUM(x.Price) * 0.8) as 'TotalOrderValue',
			case x.ChannelId when 1 then 'Sweden'
								when 4 then 'Norway'
								when 657 then 'Denmark'
								when 1000003 then 'Finland'
								when 1000006 then 'France'
								when 1000007 then 'Netherlands'
								end as Channel
			--ROW_NUMBER() OVER(ORDER BY channelid) AS 'RowNumber'
		from
			(
			select
				cast(year(Createddate) as varchar(4)) + '-' + cast(month(Createddate) as varchar(2)) + '-' + cast(day(Createddate) as varchar(2)) as 'Date',
				o.OrderId,
				op.Price,
				ChannelId
			from 
			   [order].tOrder o
			   inner join [order].tOrderPayment op
						  on o.OrderId = op.OrderId
						  and o.OrderStatusId = 4
			where 
				--Createddate > DATEADD(day, -@NumberOfDaysBack, Convert(varchar, GETDATE(), 112))
				Createddate > @DateFrom
				AND Createddate < @DateTo

			) x
		group by
			x.[Date], x.ChannelId					  
		order by
			ChannelId
			
		-- Hårdkoda generella valutor (FEL!!)
		update #tTmpResult
		set AverageOrderPrice = (AverageOrderPrice * 8.9),
			TotalOrderValue = (TotalOrderValue * 8.9)
		where
			Channel = 'Finland'
		
		update #tTmpResult
		set AverageOrderPrice = (AverageOrderPrice * 8.9),
			TotalOrderValue = (TotalOrderValue * 8.9)
		where
			Channel = 'France'
		
		update #tTmpResult
		set AverageOrderPrice = (AverageOrderPrice * 8.9),
			TotalOrderValue = (TotalOrderValue * 8.9)
		where
			Channel = 'Netherlands'
		
		update #tTmpResult
		set AverageOrderPrice = (AverageOrderPrice * 1.1),
			TotalOrderValue = (TotalOrderValue * 1.1)
		where
			Channel = 'Norway'
		
		update #tTmpResult
		set AverageOrderPrice = (AverageOrderPrice * 1.2),
			TotalOrderValue = (TotalOrderValue * 1.2)
		where
			Channel = 'Denmark'	
		--- RESULT
		
		select 
			convert(VARCHAR (10), [Date], 120) AS [Date],
			--CONVERT(VARCHAR(10), GETDATE(), 120) AS [YYYY-MM-DD]
			NumberOfOrders, 
			convert(decimal(16,2),AverageOrderPrice) as AverageOrderPrice,
			TotalOrderValue, Channel
		from
			(select * from #tTmpResult
			UNION		
			select [Date], SUM(NumberOfOrders) as Total, (SUM(TotalOrderValue)/SUM(NumberOfOrders)), SUM(TotalOrderValue),
			 'Total' from #tTmpResult
			group by
					[Date]) v
		order by
			Channel					
		 
end
GO
