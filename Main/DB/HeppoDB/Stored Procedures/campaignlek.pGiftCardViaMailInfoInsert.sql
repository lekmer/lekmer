
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoInsert]
	@OrderId		INT,
	@CampaignId		INT,
	@ActionId		INT,
	@DiscountValue	DECIMAL(16,2),
	@DateToSend		DATETIME,
	@StatusId		INT
AS
BEGIN
	INSERT [campaignlek].[tGiftCardViaMailInfo] (
		OrderId,
		CampaignId,
		ActionId,
		DiscountValue,
		DateToSend,
		StatusId
	)
	VALUES (
		@OrderId,
		@CampaignId,
		@ActionId,
		@DiscountValue,
		@DateToSend,
		@StatusId
	)
END
GO
