SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure campaign.pProductCampaignInsert
	@CampaignId int
as
begin
	insert
		campaign.tProductCampaign
	(
		CampaignId
	)
	values
	(
		@CampaignId
	)
end
GO
