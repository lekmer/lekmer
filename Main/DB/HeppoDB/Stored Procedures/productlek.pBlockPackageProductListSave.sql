SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT
AS
BEGIN
	UPDATE [productlek].[tBlockPackageProductList]
	SET
		[ColumnCount] = @ColumnCount,
		[RowCount] = @RowCount
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [productlek].[tBlockPackageProductList] (
		[BlockId],
		[ColumnCount],
		[RowCount]
	)
	VALUES (
		@BlockId,
		@ColumnCount,
		@RowCount
	)
END
GO
