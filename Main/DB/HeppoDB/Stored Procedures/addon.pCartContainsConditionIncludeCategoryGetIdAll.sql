SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryGetIdAll]
	@ConditionId		int
AS
BEGIN
	SELECT 
		c.CategoryId,
		A.IncludeSubcategories
	FROM 
		[addon].tCartContainsConditionIncludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.ConditionId = @ConditionId
END
GO
