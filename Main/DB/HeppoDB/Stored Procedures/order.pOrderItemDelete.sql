
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderItemDelete]
	@OrderItemId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [order].tOrderProductCampaign
	WHERE OrderItemId = @OrderItemId
	
	DELETE [order].[tOrderItemProduct]
	WHERE OrderItemId = @OrderItemId
	
	DELETE [order].[tOrderItem]
	WHERE OrderItemId = @OrderItemId
END
GO
