SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pProductSeoSettingTitleTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[ProductSeoSetting.Title] AS 'Value'
	FROM
		lekmer.[vProductSeoSettingTranslation]
	WHERE 
		[Product.Id] = @ProductId
END

GO
