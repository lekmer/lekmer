
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderItemSave]
	@OrderItemId				INT,
	@OrderId					INT,
	@ProductId					INT,
	@Quantity					INT,
	@ActualPriceIncludingVat	DECIMAL(16,2),
	@OriginalPriceIncludingVat	DECIMAL(16,2),
	@VAT						DECIMAL(16,2),
	@ErpId						VARCHAR(50),
	@EanCode					VARCHAR(20),
	@Title						NVARCHAR(50),
	@OrderItemStatusId			INT = 1,
	@ProductTypeId				INT
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderItem]
	SET
		OrderId = @OrderId,
		Quantity = @Quantity,
		ActualPriceIncludingVat = @ActualPriceIncludingVat,
		OriginalPriceIncludingVat = @OriginalPriceIncludingVat,
		VAT = @VAT,
		OrderItemStatusId = @OrderItemStatusId
	WHERE
		OrderItemId = @OrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderItem]
		(
			OrderId,
			Quantity,
			ActualPriceIncludingVat,
			OriginalPriceIncludingVat,
			VAT,
			OrderItemStatusId
		)
		VALUES
		(
			@OrderId,
			@Quantity,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @OrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [order].[tOrderItemProduct]
		(
			OrderItemId,
			ProductId,
			ErpId,
			EanCode,
			Title,
			[ProductTypeId]
		)
		VALUES
		(
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title,
			@ProductTypeId
		)
	END
	
	RETURN @OrderItemId
END


GO
