SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionSave]
	@ConditionId		INT,
	@IncludeAllProducts	BIT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartItemsGroupValueCondition]
	SET
		ConfigId = @ConfigId
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [campaignlek].[tCampaignActionConfigurator] DEFAULT VALUES
		SET @ConfigId = SCOPE_IDENTITY()
	
		INSERT [campaignlek].[tCartItemsGroupValueCondition] (
			ConditionId,
			ConfigId
		)
		VALUES (
			@ConditionId,
			@ConfigId
		)
	END
	
	UPDATE 
		[campaignlek].[tCampaignActionConfigurator]
	SET 
		IncludeAllProducts = @IncludeAllProducts
	WHERE 
		[CampaignActionConfiguratorId] = @ConfigId
	
	RETURN @ConfigId
END
GO
