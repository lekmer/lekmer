SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconTranslationDescriptionGetAll]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    it.[IconTranslation.IconId] AS 'Id',
		it.[IconTranslation.LanguageId] AS 'LanguageId',
		it.[IconTranslation.Description] AS 'Value'
	FROM
	    [lekmer].[vIconTranslation] it
	WHERE 
		it.[IconTranslation.IconId] = @IconId
END
GO
