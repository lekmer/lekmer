SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionBrandInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@BrandId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportRestrictionBrand] (
		[ProductRegistryId],
		[BrandId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@BrandId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
