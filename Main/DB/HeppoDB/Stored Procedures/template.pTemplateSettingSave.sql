SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
**********************  Version 1  ************************
User: Roman G.		Date: 12/23/2008		Time: 12:57:03
Description: Created 
*/
CREATE PROCEDURE [template].[pTemplateSettingSave]
	@TemplateId			INT,
	@ModelSettingId		INT,
	@Value				NVARCHAR(50),
	@AlternateValue		NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[template].[tTemplateSetting]			
	SET
		[Value] = @Value,
		[AlternateValue] = @AlternateValue
	WHERE 
		[TemplateId] = @TemplateId
		AND [ModelSettingId] = @ModelSettingId
		
	IF @@ROWCOUNT = 0
		BEGIN
			INSERT 
				[template].[tTemplateSetting]
			(
				[TemplateId],
				[ModelSettingId],
				[Value],
				[AlternateValue]
			)
			VALUES
			(
				@TemplateId,
				@ModelSettingId,
				@Value,
				@AlternateValue
			)
		END
END
			







GO
