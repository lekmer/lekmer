
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateProductSeoShoes]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@ChannelId			INT
AS
BEGIN
	SET NOCOUNT ON
	
	BEGIN TRY
		BEGIN TRANSACTION							
		
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		DECLARE @StartPageMenTitle NVARCHAR(50)
		DECLARE @StartPageWomenTitle NVARCHAR(50)
		DECLARE @LanguageId int
		
		SET @LanguageId = (select LanguageId from core.tChannel where ChannelId = @ChannelId)
						
		INSERT INTO product.tProductSeoSettingTranslation(ProductId, LanguageId)
		SELECT
			p.ProductId,
			@LanguageId
		FROM
			product.tProduct p
		WHERE
			not exists (SELECT 1
							FROM product.tProductSeoSettingTranslation n
							WHERE n.ProductId = p.ProductId and
							n.LanguageId = @LanguageId)
	
		INSERT INTO lekmer.tTagTranslation (TagId, LanguageId)
		SELECT
			r.TagId,
			@LanguageId
		FROM lekmer.tTagTranslation r
		WHERE
			LanguageId = @LanguageId
			and not exists (SELECT 1 FROM lekmer.tTagTranslation h
								WHERE h.LanguageId = @LanguageId and h.TagId = r.TagId)
							
	
		SET @SiteStructureRegistryId = (Select SiteStructureRegistryId
											from sitestructure.tSiteStructureModuleChannel																					
											where ChannelId = @ChannelId)
		
		
		SELECT @StartPageMenTitle = Lower(Title)
		FROM sitestructure.tContentNode
		WHERE
			CommonName = 'startPageMen'
			AND SiteStructureRegistryId = @SiteStructureRegistryId
			
		SELECT @StartPageWomenTitle = Lower(Title)
		FROM sitestructure.tContentNode
		WHERE
			CommonName = 'startPageWomen'
			AND SiteStructureRegistryId = @SiteStructureRegistryId
		
	
		UPDATE
			pss
		SET
		@NewTitle = @FormulaTitle,
		@NewTitle = REPLACE(@NewTitle, '[BrandTitle]', b.Title),
		@NewTitle = REPLACE(@NewTitle, '[ProductTitle]', p.Title),
		@NewTitle = REPLACE(@NewTitle, '[TagColor]', LOWER(coalesce(tt.Value,''))),
		@NewTitle = REPLACE(@NewTitle, '[CategoryLevelTwo]',  case c2.CategoryId 
																when 1000064 then coalesce(@StartPageWomenTitle, '')
																when 1000060 then coalesce(@StartPageMenTitle, '')
																else ''
																end ), 
		
		pss.Title = @NewTitle,

		@NewDescription	= @FormulaDescription,
		@NewDescription = REPLACE(@NewDescription, '[BrandTitle]', b.Title),
		@NewDescription = REPLACE(@NewDescription, '[ProductTitle]', p.Title),
		@NewDescription = REPLACE(@NewDescription, '[CategoryLevelOne]', coalesce(ct.Title, c.Title)),
		@NewDescription = REPLACE(@NewDescription, '[CategoryLevelTwo]',  case c2.CategoryId 
																when 1000064 then coalesce(@StartPageWomenTitle, '')
																when 1000060 then coalesce(@StartPageMenTitle, '')
																else ''
																end ), 
			
		pss.[Description] = @NewDescription
								
		FROM 
			product.tProductSeoSettingTranslation pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				inner join product.tCategoryTranslation ct
				on c.CategoryId = ct.CategoryId
				
				inner join product.tCategoryTranslation ct2
				on c2.CategoryId = ct2.CategoryId
				
				left join lekmer.tProductTag pt
					on pt.ProductId = p.ProductId
				left join lekmer.tTag t
					on t.TagId = pt.TagId
				left join lekmer.tTagTranslation tt
					on t.TagId = tt.TagId
		WHERE
			c3.CategoryId = 1000059 -- skor
			and t.TagGroupId = 30 -- färg
			and pss.LanguageId = @LanguageId
			and tt.LanguageId = @LanguageId
			and ct.LanguageId = @LanguageId
			and ct2.LanguageId = @LanguageId
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))
	
	
		-- INTE KLAR MED SE GENERAL
		--IF @LanguageId = 1
		--BEGIN
		
		--select 1
		
		--END
	
				
		--rollback
	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@trancount > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					VALUES('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
