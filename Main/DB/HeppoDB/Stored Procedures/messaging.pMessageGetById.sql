SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [messaging].[pMessageGetById]
@MessageId	uniqueidentifier
as
begin

	select
		MessageId,
		BatchId,
		MessageStatusId,
		MessageTypeId,
		ProviderName,
		Body,
		CreatedDate
	from 
		[messaging].[tMessage]
	where
		[MessageId] = @MessageId

	select 
		[MessageId],
		[Name],
		[Value]
	from 
		[messaging].[tMessageCustomField]
	where
		[MessageId] = @MessageId

end
GO
