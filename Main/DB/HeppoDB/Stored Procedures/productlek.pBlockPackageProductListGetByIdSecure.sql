SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		ppl.*,
		b.*
	FROM 
		[productlek].[vBlockPackageProductList] AS ppl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON ppl.[BlockPackageProductList.BlockId] = b.[Block.BlockId]
	WHERE
		ppl.[BlockPackageProductList.BlockId] = @BlockId
END
GO
