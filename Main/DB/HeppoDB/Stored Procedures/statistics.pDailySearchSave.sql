SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pDailySearchSave]
	@ChannelId int,
	@Query varchar(max),
	@Date smalldatetime,
	@HitCount int,
	@SearchCount int
as
begin
	update
		[statistics].[tDailySearch]
	set
		[HitCount] = @HitCount,
		[SearchCount] = [SearchCount] + @SearchCount
	where
		[ChannelId] = @ChannelId
		and [Query] = @Query
		and [Date] = @Date

	if  @@rowcount = 0
	begin
		insert
			[statistics].[tDailySearch]
		(
			[ChannelId],
			[Query],
			[Date],
			[HitCount],
			[SearchCount]
		)
		values
		(
			@ChannelId,
			@Query,
			@Date,
			@HitCount,
			@SearchCount
		)
	end
end

GO
