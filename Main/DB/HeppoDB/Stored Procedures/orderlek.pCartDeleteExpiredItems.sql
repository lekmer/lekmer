
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pCartDeleteExpiredItems]
	@LifeCycleDays INT
AS
BEGIN
	SET NOCOUNT ON;

	-- Protection of Fools
	IF @LifeCycleDays < 30
		SET @LifeCycleDays = 30
	
	DECLARE	@ExpirationDate DATETIME = DATEADD(DAY, -@LifeCycleDays, DATEDIFF(DAY, 0, GETDATE()))
	DECLARE	@BatchSize INT = 1000

	IF EXISTS ( SELECT * FROM tempdb..sysobjects WHERE id = OBJECT_ID('tempdb..#DeleteItems') ) 
		DROP TABLE #DeleteItems

	CREATE TABLE #DeleteItems
	(
	  CartId INT,
	  PRIMARY KEY ( CartId )
	)

	WHILE 1 = 1
	BEGIN

		INSERT	INTO [#DeleteItems]
				SELECT TOP ( @BatchSize ) c.[CartId]
				FROM [order].[tCart] c
				WHERE c.[CreatedDate] < @ExpirationDate
					
		IF @@ROWCOUNT = 0
			BREAK

		DELETE cipe --tCartItemPackageElement
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [orderlek].[tCartItemPackageElement] cipe ON [cipe].[CartItemId] = [ci].[CartItemId]

		DELETE lci --tLekmerCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			LEFT OUTER JOIN [lekmer].[tLekmerCartItem] lci ON [lci].[CartItemId] = [ci].[CartItemId]
			
		DELETE ci --tCartItem
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [order].[tCartItem] ci ON [ci].[CartId] = [c].[CartId]
			
		DELETE cio --CartItemOption
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			LEFT OUTER JOIN [campaignlek].[tCartItemOption] cio ON [cio].[CartId] = [di].[CartId]
			
		DELETE c --tCart
		FROM
			[#DeleteItems] di
			LEFT OUTER JOIN [order].[tCart] c ON c.[CartId] = [di].[CartId]
			
		TRUNCATE TABLE [#DeleteItems]
	END

	DROP TABLE [#DeleteItems]
END
GO
