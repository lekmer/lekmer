SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetViewById]
	@ProductId	int,
	@CustomerId int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;	
	
	SELECT *		
	FROM 
		[product].[vCustomProductView] as p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	where
		p.[Product.Id] = @ProductId
		and p.[Product.ChannelId] = @ChannelId
end

GO
