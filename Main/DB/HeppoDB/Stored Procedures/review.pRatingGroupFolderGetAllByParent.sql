SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		rgf.*
	FROM
		[review].[vRatingGroupFolder] rgf
	WHERE
		(rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		(rgf.[RatingGroupFolder.ParentRatingGroupFolderId] IS NULL AND @ParentId IS NULL)
END
GO
