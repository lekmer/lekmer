
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemInsert]
	@ActionId			int,
	@ProductId			int,
	@CurrencyId			int,
	@DiscountPrice		decimal(16,2)
AS 
BEGIN 
	INSERT
		lekmer.tProductDiscountActionItem
	(
		ProductActionId,
		ProductId,
		CurrencyId,
		DiscountPrice
	)
	VALUES
	(
		@ActionId,
		@ProductId,
		@CurrencyId,
		@DiscountPrice
	)
END 



GO
