
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pDeliveryMethodGetAllByPaymentType]
	@ChannelId INT,
	@PaymentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		dm.*
	FROM 
		[order].[vCustomDeliveryMethod] dm
		INNER JOIN [order].[tPaymentTypeDeliveryMethod] ptdm ON ptdm.DeliveryMethodId = dm.[DeliveryMethod.DeliveryMethodId]
	WHERE
		ptdm.PaymentTypeId = @PaymentTypeId
		AND dm.[DeliveryMethod.ChannelId] = @ChannelId
	ORDER BY
		dm.[DeliveryMethod.Priority]
END
GO
