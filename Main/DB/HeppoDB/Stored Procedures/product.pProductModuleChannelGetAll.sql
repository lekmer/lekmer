SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductModuleChannelGetAll]
as
begin
	select 
		*
	from 
		[product].[vCustomProductModuleChannel]
end

GO
