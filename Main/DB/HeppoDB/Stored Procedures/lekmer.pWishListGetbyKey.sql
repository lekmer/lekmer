SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure  [lekmer].[pWishListGetbyKey]
	@WishListKey uniqueidentifier
as
begin

	select *
	from
		[lekmer].[tWishList] 
	where
		WishListKey = @WishListKey		
end
GO
