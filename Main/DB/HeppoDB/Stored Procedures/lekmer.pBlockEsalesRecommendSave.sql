
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendSave]
	@BlockId INT,
	@ColumnCount INT,
	@RowCount INT,
	@RecommendationType INT,
	@PanelPath NVARCHAR(MAX),
	@FallbackPanelPath NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesRecommend]
	SET
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount,
		[EsalesRecommendationTypeId] = @RecommendationType,
		[PanelPath]		= @PanelPath,
		[FallbackPanelPath] = @FallbackPanelPath
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesRecommend]
	(
		[BlockId],
		[ColumnCount],
		[RowCount],
		[EsalesRecommendationTypeId],
		[PanelPath],
		[FallbackPanelPath]		
	)
	VALUES
	(
		@BlockId,
		@ColumnCount,
		@RowCount,
		@RecommendationType,
		@PanelPath,
		@FallbackPanelPath
	)
END
GO
