SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pBlockTopListGetByIdSecure]
	@BlockId	int
as
begin
	select 
		tl.*,
		b.*
	from 
		[addon].[vCustomBlockTopList] as tl
		inner join [sitestructure].[vCustomBlockSecure] as b on tl.[BlockTopList.BlockId] = b.[Block.BlockId]
	where
		tl.[BlockTopList.BlockId] = @BlockId
end

GO
