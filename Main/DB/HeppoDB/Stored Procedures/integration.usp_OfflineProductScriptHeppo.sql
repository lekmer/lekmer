
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_OfflineProductScriptHeppo]

AS
BEGIN	
	SET NOCOUNT ON
	
		
		declare @SizeProduct table
		(
			ProductId int primary key
		)
		insert into @SizeProduct
		select distinct ProductId
		from lekmer.tProductSize
		
		declare @SetOffline table
		(
			ProductId int primary key with (ignore_dup_key = on)
		)


		-- No Media or Description
		insert into @SetOffline (ProductId) 
		select 
			p.ProductId
		from 
			product.tProduct p
		where 
			p.MediaId is null
			--or p.[Description] = ''
			
		-- No sizes left for products with size 
		insert into @SetOffline (ProductId)	
		select 
			p.ProductId
		from 
			product.tProduct p
			inner join @SizeProduct sp 
				on sp.ProductId = p.ProductId
			left join lekmer.tProductSize ps 
				on ps.ProductId = p.ProductId
				and ps.NumberInStock > 0
		where 
			ps.ProductId is null
		
		-- Check which "no size" products to set offline
		insert into @SetOffline (ProductId)	
		select 
			p.ProductId
		from 
			product.tProduct p
			left outer join @SizeProduct sp 
				on sp.ProductId = p.ProductId
		where 
			p.NumberInStock = 0
			and sp.ProductId is null
		
		
		-- Table that will hold all products and their rightful productstatus
		declare @ChangeProductStatus table
		(
			ProductId int primary key with (ignore_dup_key = on),
			ProductStatusId int
		)
		
		insert into @ChangeProductStatus (ProductId, ProductStatusId)
		select 
			p.ProductId, 
			1
		from 
			@SetOffline sf
			inner join product.tProduct p 
				on p.ProductId = sf.ProductId
		where 
			p.ProductStatusId = 0				
		
		
		insert into @ChangeProductStatus (ProductId, ProductStatusId) 
		select 
			p.ProductId, 
			0
		from 
			product.tProduct p
			left outer join @SetOffline sf 
				on sf.ProductId = p.ProductId
		where 
			p.ProductStatusId = 1
			and sf.ProductId is null
	
		
		-- Update with Increments
		declare @ProductsToUpdate int
		declare @RowSize int
		set @ProductsToUpdate = (select COUNT(ProductId) from @ChangeProductStatus)
		set @RowSize = 200				
			
	
		while @ProductsToUpdate > 0 
		begin
			begin try
				begin transaction
												
				update top (@RowSize)
					p
				set
					p.ProductStatusId = i.ProductStatusId
				from 
					product.tproduct p
					inner join @ChangeProductStatus i
						on p.ProductId = i.ProductId	
						
				
				delete top (@RowSize) from @ChangeProductStatus 															
				set @ProductsToUpdate = @ProductsToUpdate - @RowSize
	
				commit					
			end try
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG here
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch													
		end
END

GO
