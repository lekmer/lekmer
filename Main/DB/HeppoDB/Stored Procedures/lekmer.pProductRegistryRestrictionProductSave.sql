SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionProductSave]
	@ProductRegistryId	INT,
	@ProductId			INT,
	@RestrictionReason	NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME
AS
BEGIN
	INSERT INTO [lekmer].[tProductRegistryRestrictionProduct] (
		ProductRegistryId,
		ProductId,
		RestrictionReason,
		UserId,
		CreatedDate
	)
	VALUES (
		@ProductRegistryId,
		@ProductId,
		@RestrictionReason,
		@UserId,
		@CreatedDate
	)
END
GO
