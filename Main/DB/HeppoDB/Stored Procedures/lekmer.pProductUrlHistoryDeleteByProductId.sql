SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteByProductId]
	@ProductId INT
AS
BEGIN
	DELETE [lekmer].[tProductUrlHistory]
	WHERE ProductId = @ProductId
END
GO
