SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportOrderCustomerData]

AS
begin
	set nocount on
	begin try
				
		select i.OrderId, i.Price, i.[Date], i.CivicNumber, i.Email, i.Channel,
		SUM(oi.Quantity) as Antal from integration.tOrderDataStatistics i
		inner join [order].tOrderItem oi
			on oi.OrderId = i.OrderId
		group by
			i.OrderId, i.Price, i.[Date], i.CivicNumber, i.Email, i.Channel
		order by
			Channel
				
/*
				select * from integration.tOrderDataStatistics 
				
				select * from [order].tOrder
				
				select * from customer.tCustomerInformation

				
			begin tran
			insert into integration.tOrderDataStatistics (OrderId, Price, Date, CivicNumber, Email, Channel)
			select
				z.OrderId,
				z.Price,
				z.CreatedDate,
				z.CivicNumber,
				z.Email,
				z.CommonName
			from
				(select o.OrderId, op.Price, o.CreatedDate, c.CivicNumber, o.Email, t.CommonName
				from [order].tOrder o
				inner join [order].tOrderPayment op
					on o.OrderId = op.OrderId
				inner join customer.tCustomerInformation c
					on o.CustomerId = c.CustomerId 
				inner join core.tChannel t
					on o.ChannelId = t.ChannelId
				where
					o.OrderStatusId = 4
					and o.OrderId in ()
					and 
				) z
			rollback
*/
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
