SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRatingItemDelete]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockRatingItem]
	WHERE BlockId = @BlockId
END
GO
