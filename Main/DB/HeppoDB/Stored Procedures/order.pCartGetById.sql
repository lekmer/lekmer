SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pCartGetById] 
@CartId		int
as
begin
	set nocount on
	select	
		*
	from
		[order].vCustomCart
	where
		[Cart.CartId] = @CartId
end

GO
