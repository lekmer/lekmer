SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pAddressTypeGetAll]
as	
begin
set nocount on
	select
		*
	from
		customer.vCustomAddressType
end

GO
