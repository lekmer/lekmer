SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pThemeDelete]
@ThemeId	int
AS
BEGIN
	DELETE FROM template.tTemplateModuleChannel
	WHERE ThemeId = @ThemeId
	
	DELETE FROM template.tThemeModel
	WHERE ThemeId = @ThemeId
	
	DELETE FROM template.tTheme
	WHERE ThemeId = @ThemeId
END













GO
