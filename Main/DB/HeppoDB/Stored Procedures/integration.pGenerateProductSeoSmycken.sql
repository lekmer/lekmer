SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateProductSeoSmycken]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@ChannelId			INT
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION

		------------------------------------------------------------
		-- <Smycken>
		------------------------------------------------------------
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @LanguageId int

		-- Get Language Id for channel.		
		SET @LanguageId = (SELECT LanguageId FROM core.tChannel WHERE ChannelId = @ChannelId)
		
		INSERT INTO product.tProductSeoSettingTranslation (ProductId, LanguageId)
		SELECT
			p.ProductId,
			@LanguageId
		FROM
			product.tProduct p
		WHERE
			NOT EXISTS (SELECT 1
						FROM product.tProductSeoSettingTranslation n
						WHERE n.ProductId = p.ProductId
						AND n.LanguageId = @LanguageId)
							
		INSERT INTO product.tCategoryTranslation (CategoryId, LanguageId, Title)
		SELECT
			c.CategoryId,
			@LanguageId,
			NULL
		FROM
			product.tCategory c
		WHERE
			NOT EXISTS (SELECT 1 
						FROM product.tCategoryTranslation ct 
						WHERE ct.CategoryId = c.CategoryId
						AND ct.LanguageId = @LanguageId)												

		UPDATE
			pss
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[BrandTitle]', b.Title),
			@NewTitle = REPLACE(@NewTitle, '[ProductTitle]', p.Title),
			@NewTitle = REPLACE(@NewTitle, '[CategoryLevelTwo]', COALESCE(ct.Title, c2.Title)),
			
			pss.Title = @NewTitle,
						
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[BrandTitle]', b.Title),
			@NewDescription = REPLACE(@NewDescription, '[ProductTitle]', p.Title),

			pss.[Description] = @NewDescription
		FROM 
			product.tProductSeoSettingTranslation pss
			INNER JOIN lekmer.tLekmerProduct l ON l.ProductId = pss.ProductId
			INNER JOIN product.tProduct p ON p.ProductId = l.ProductId
			INNER JOIN lekmer.tBrand b ON b.BrandId = l.BrandId
			------------------------------------------------------------------------------
			INNER JOIN product.tCategory c ON c.CategoryId = p.CategoryId
			INNER JOIN product.tCategory c2 ON c2.CategoryId = c.ParentCategoryId
			INNER JOIN product.tCategory c3 ON c3.CategoryId = c2.ParentCategoryId
			------------------------------------------------------------------------------
			INNER JOIN product.tCategoryTranslation ct ON ct.CategoryId = c2.CategoryId
		WHERE
			c3.CategoryId = 1001254 -- Smycken
			AND pss.LanguageId = @LanguageId
			AND ct.LanguageId = @LanguageId
			--AND
			--((pss.Title IS NULL OR pss.Title = '')
			--	OR (pss.[Description] IS NULL OR pss.[Description] = ''))
			
	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] (Data, [Message], [Date], OcuredInProcedure)
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
