SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingTranslationDescriptionGetAll]
	@RatingId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	    rt.[RatingTranslation.RatingId] AS 'Id',
		rt.[RatingTranslation.LanguageId] AS 'LanguageId',
		rt.[RatingTranslation.Description] AS 'Value'
	FROM
	    [review].[vRatingTranslation] rt
	WHERE 
		rt.[RatingTranslation.RatingId] = @RatingId
END
GO
