SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductPopularityGetAll]
	@ChannelId	INT
AS	
BEGIN
	SET NOCOUNT ON	
	
	SELECT
		pp.*
	FROM
		[lekmer].[vProductPopularity] pp
	WHERE
		pp.[ProductPopularity.ChannelId] = @ChannelId
END
GO
