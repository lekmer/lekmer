SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductGetPriceByChannelAndCampaign]
	@CurrencyId int,
	@Price decimal(16,2),
	@IncludeCategories as varchar(max) = '',
	@IncludeBrands as varchar(max) = '',
	@IncludeProducts as varchar(max) = '',
	@ExcludeCategories varchar(max) = '',
	@ExcludeBrands as varchar(max) = '',
	@ExcludeProducts as varchar(max) = ''
AS
BEGIN
	SET NOCOUNT ON

select pp.ErpId,  CAST([product].[fnGetPriceOfItemWithLowestPrice](@CurrencyId,pp.ProductId, null) as Decimal(16,2)) as Price from (
select Cast(SepString as INT) as ProductId from [generic].[fStringToStringTablePerformance](@IncludeProducts,',')
union
select ProductId from product.tProduct p
inner join [generic].[fStringToStringTablePerformance](@IncludeCategories,',') c on c.SepString = p.CategoryId
union
select ProductId from lekmer.tLekmerProduct p
inner join [generic].[fStringToStringTablePerformance](@IncludeBrands,',') c on c.SepString = p.BrandId
except
select Cast(SepString as INT) as ProductId from [generic].[fStringToStringTablePerformance](@ExcludeProducts,',')
union
select ProductId from product.tProduct p
inner join [generic].[fStringToStringTablePerformance](@ExcludeCategories,',') c on c.SepString = p.CategoryId
union
select ProductId from lekmer.tLekmerProduct p
inner join [generic].[fStringToStringTablePerformance](@ExcludeBrands,',') c on c.SepString = p.BrandId
) c
inner join product.tProduct pp on pp.ProductId = c.ProductId
where [product].[fnGetPriceOfItemWithLowestPrice](@CurrencyId,pp.ProductId, null) < @Price
END
GO
