SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pFreeFreightFlagActionDelete]
	@ActionId int
AS 
BEGIN
	DELETE FROM [lekmer].[tFreeFreightFlagAction]
	WHERE ProductActionId = @ActionId
END
GO
