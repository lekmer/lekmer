SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBatteryTypeGetAll]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[lekmer].[vBatteryType]
END
GO
