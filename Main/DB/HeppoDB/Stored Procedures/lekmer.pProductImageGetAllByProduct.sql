SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductImageGetAllByProduct]
	@ProductId INT
AS
BEGIN
	SELECT
		[i].*
	FROM 
		[product].[vCustomProductImage] i
	WHERE
		i.[ProductImage.ProductId] = @ProductId
	ORDER BY 
		[i].[ProductImage.GroupId],
		[i].[ProductImage.Ordinal]
END
GO
