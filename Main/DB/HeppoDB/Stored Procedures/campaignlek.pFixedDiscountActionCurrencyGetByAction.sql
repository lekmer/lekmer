SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionCurrencyGetByAction]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountActionCurrency]
	WHERE 
		[FixedDiscountActionCurrency.ProductActionId] = @ProductActionId
END
GO
