SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionDeleteAllByFragment]
	@FragmentId INT
AS
BEGIN
	SET NOCOUNT ON	
	
	DELETE [template].[tModelFragmentFunction]
	WHERE  [ModelFragmentId] = @FragmentId
	
	RETURN SCOPE_IDENTITY()
	
END	









	




GO
