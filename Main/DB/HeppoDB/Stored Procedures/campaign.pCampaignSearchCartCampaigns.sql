SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignSearchCartCampaigns]
@Title	nvarchar(MAX)
AS
BEGIN
	SET @Title = [generic].[fPrepareSearchParameter](@Title)
	
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		C.[Campaign.Priority] ASC
END

GO
