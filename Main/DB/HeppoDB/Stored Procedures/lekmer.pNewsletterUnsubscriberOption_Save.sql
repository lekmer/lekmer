SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_Save]
	@UnsubscriberId INT,
	@NewsletterTypeId INT,
	@InterspireContactList NVARCHAR(100),
	@CreatedDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [lekmer].[tNewsletterUnsubscriberOption]
	(
		[UnsubscriberId],
		[NewsletterTypeId],
		[InterspireContactList],
		[CreatedDate]
	)
	VALUES
	(
		@UnsubscriberId,
		@NewsletterTypeId,
		@InterspireContactList,
		@CreatedDate
	)

	RETURN SCOPE_IDENTITY()
END
GO
