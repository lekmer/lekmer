
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductDeletePopularity]
	@ChannelId	INT
AS	
BEGIN
	SET NOCOUNT ON	
	
	DELETE  [lekmer].[tProductPopularity]
	WHERE ChannelId = @ChannelId
END
GO
