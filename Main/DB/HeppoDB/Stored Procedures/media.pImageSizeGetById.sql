SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dovhun Roman
-- Create date: 2009-03-24
-- Description:	created
-- =============================================
CREATE PROCEDURE [media].[pImageSizeGetById]
	@ImageSizeId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomImageSize] AS vis
	WHERE  vis.[ImageSize.Id] = @ImageSizeId
END

GO
