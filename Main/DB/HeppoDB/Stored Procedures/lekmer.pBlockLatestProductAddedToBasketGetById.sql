SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBlockLatestProductAddedToBasketGetById]
	@BlockId	int,
	@LanguageId	int
as	
begin
	set nocount on
	select
		BLPATB.*,
		b.*
	from
		[lekmer].[tBlockLatestProductAddedToBasket] as BLPATB
		inner join [sitestructure].[vCustomBlock] as b on BLPATB.[BlockId] = b.[Block.BlockId]
	where
		BLPATB.[BlockId] = @BlockId
		and b.[Block.LanguageId] = @LanguageId 
end
GO
