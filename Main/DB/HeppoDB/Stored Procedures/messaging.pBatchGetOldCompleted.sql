SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pBatchGetOldCompleted]
@MaxEndDate					datetime,
@CompletedMessageStatusId	int
as
begin

	select
		BatchId,
		StartDate,
		EndDate,
		MachineName,
		ProcessId
	from 
		[messaging].[tBatch]
	where
		EndDate <= @MaxEndDate
		and not BatchId in (
			select BatchId from [messaging].[tMessage]
			where BatchId is not null and MessageStatusId <> @CompletedMessageStatusId)

end
GO
