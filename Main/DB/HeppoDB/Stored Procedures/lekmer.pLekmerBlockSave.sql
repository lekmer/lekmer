SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerBlockSave]
 @BlockId     int,
 @StartDate     datetime = NULL,
 @EndDate     datetime = NULL,
 @StartDailyIntervalMinutes int = NULL,
 @EndDailyIntervalMinutes int = NULL
as
begin
 update
  [lekmer].[tLekmerBlock]
 set
  [StartDate]      = @StartDate,
  [EndDate]      = @EndDate,
  [StartDailyIntervalMinutes]  = @StartDailyIntervalMinutes,
  [EndDailyIntervalMinutes]  = @EndDailyIntervalMinutes
 where
  [BlockId] = @BlockId
  
 if @@ROWCOUNT = 0
  begin
   insert
    [lekmer].[tLekmerBlock]
   (
    [BlockId],
    [StartDate],
    [EndDate],
    [StartDailyIntervalMinutes],
    [EndDailyIntervalMinutes]
   )
   values
   (
    @BlockId,
    @StartDate,
    @EndDate,
    @StartDailyIntervalMinutes,
    @EndDailyIntervalMinutes
   )
  end
end

GO
