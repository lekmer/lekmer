SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionGetById]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartValueRangeCondition] cvrc
	WHERE
		cvrc.[Condition.Id] = @ConditionId
END
GO
