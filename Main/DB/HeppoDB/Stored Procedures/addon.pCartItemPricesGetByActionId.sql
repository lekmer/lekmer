SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartItemPricesGetByActionId]
	@CartActionId int
as
begin
	SELECT
		*
	FROM
		addon.[vCartItemPriceActionCurrency]
	WHERE 
		[CartItemPriceAction.ActionId] = @CartActionId
end

GO
