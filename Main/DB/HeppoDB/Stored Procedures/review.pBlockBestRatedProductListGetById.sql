SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		bbrpl.*,
		b.*
	FROM 
		[review].[vBlockBestRatedProductList] AS bbrpl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bbrpl.[BlockBestRatedProductList.BlockId] = b.[Block.BlockId]
	WHERE
		bbrpl.[BlockBestRatedProductList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
