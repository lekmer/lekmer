SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBrandSiteStructureRegistryGet]
	@SiteStructureRegistry int,
	@BrandId int
as
begin
	select
		*
	FROM
		lekmer.vBrandSiteStructureRegistry
	where
		[BrandSiteStructureRegistry.SiteStructureRegistryId] = @SiteStructureRegistry
		and [BrandSiteStructureRegistry.BrandId] = @BrandId
end

GO
