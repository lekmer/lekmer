SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeBrandInsert]
	@ConditionId	INT,
	@BrandId		INT
AS
BEGIN
	INSERT [addon].[tCartContainsConditionIncludeBrand] (
		ConditionId,
		BrandId
	)
	VALUES (
		@ConditionId,
		@BrandId
	)
END
GO
