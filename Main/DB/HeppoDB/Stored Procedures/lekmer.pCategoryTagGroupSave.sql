SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryTagGroupSave]
	@CategoryId	INT,
	@TagGroupId	INT
AS
BEGIN 
	INSERT INTO [lekmer].[tCategoryTagGroup] (
		CategoryId,
		TagGroupId
	)
	VALUES (
		@CategoryId,
		@TagGroupId
	)
END 
GO
