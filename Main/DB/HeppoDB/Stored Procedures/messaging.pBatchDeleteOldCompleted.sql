SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE procedure [messaging].[pBatchDeleteOldCompleted]
@BatchId	uniqueidentifier
as
begin
	-- Messages to delete
	create table #MessageToDelete (
		MessageId uniqueidentifier not null,
		UNIQUE(MessageId)
	)
	
	insert into #MessageToDelete(MessageId)
	select MessageId from
		[messaging].[tMessage]
	where
		BatchId = @BatchId

	-- Recipients to delete
	create table #RecipientToDelete (
		RecipientId uniqueidentifier not null,
		UNIQUE(RecipientId)
	)
	
	insert into #RecipientToDelete(RecipientId)
	select RecipientId from
		[messaging].[tRecipient]
	where
		BatchId = @BatchId or
		MessageId in (select MessageId from #MessageToDelete)

	-- Delete!
	delete from
		[messaging].[tRecipientCustomField]
	where
		RecipientId in (select RecipientId from #RecipientToDelete)

	delete from
		[messaging].[tRecipient]
	where
		RecipientId in (select RecipientId from #RecipientToDelete)

	delete from
		[messaging].[tMessageCustomField]
	where
		MessageId in (select MessageId from #MessageToDelete)

	delete from
		[messaging].[tMessage]
	where
		 MessageId in (select MessageId from #MessageToDelete)

	delete from
		[messaging].[tBatch]
	where
		BatchId = @BatchId

	drop table #MessageToDelete
	drop table #RecipientToDelete

end
GO
