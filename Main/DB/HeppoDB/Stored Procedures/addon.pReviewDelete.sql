SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pReviewDelete]
         @ReviewId  int
AS    
BEGIN
         DELETE 
                   addon.tReview
         WHERE 
                   ReviewId = @ReviewId
END

GO
