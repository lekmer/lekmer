SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionExcludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END
GO
