SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignGetById]
@CampaignId	int
AS
BEGIN
	SELECT *
	FROM [campaign].[vCustomCampaign]
	WHERE [Campaign.Id] = @CampaignId
END

GO
