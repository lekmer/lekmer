SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Mykhaylo A.		Date: 26.05.2009		Time: 16:50
Description:
			Created  
*/
CREATE PROCEDURE [template].[pIncludeFolderGetAllByParentId]
	@IncludeFolderId	int
AS
begin
	set nocount on

	select
		*	
	from
		[template].[vCustomIncludeFolder]
	where
		[IncludeFolder.ParentIncludeFolderId] = @IncludeFolderId
end

GO
