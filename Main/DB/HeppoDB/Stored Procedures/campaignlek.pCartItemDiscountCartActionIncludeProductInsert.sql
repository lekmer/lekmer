SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tCartItemDiscountCartActionIncludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END

GO
