SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartItemPriceActionIncludeCategoryGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		c.CategoryId
	FROM 
		[addon].tCartItemPriceActionIncludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
