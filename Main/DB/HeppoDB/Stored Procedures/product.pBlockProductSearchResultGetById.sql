SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductSearchResultGetById]
    @LanguageId INT,
	@BlockId	INT
AS BEGIN
	SELECT
		ba.*,
		b.*
	FROM
		[product].[vCustomBlockProductSearchResult] AS ba
		inner join [sitestructure].[vCustomBlock] AS b ON ba.[BlockProductSearchResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchResult.BlockId] = @BlockId 
		AND b.[Block.LanguageId] = @LanguageId
END

GO
