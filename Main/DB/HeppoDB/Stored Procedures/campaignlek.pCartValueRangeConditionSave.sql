SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionSave]
	@ConditionId INT
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM [campaignlek].[tCartValueRangeCondition] WHERE ConditionId = @ConditionId)
			RETURN
		
		INSERT [campaignlek].[tCartValueRangeCondition] ( 
			ConditionId
		)
		VALUES (
			@ConditionId
		)
END
GO
