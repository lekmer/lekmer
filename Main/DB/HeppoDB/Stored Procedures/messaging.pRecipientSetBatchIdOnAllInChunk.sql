SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [messaging].[pRecipientSetBatchIdOnAllInChunk]
@MessageId	uniqueidentifier,
@Chunk			int,
@BatchId		uniqueidentifier
as
begin

	update
		[messaging].[tRecipient]
	set
		BatchId = @BatchId
	where
		MessageId = @MessageId and
		Chunk = @Chunk

end



GO
