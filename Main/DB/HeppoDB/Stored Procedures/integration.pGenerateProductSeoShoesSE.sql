SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateProductSeoShoesSE]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Produktnivå skor>
		-- Title>
		-- Formel: Varumärke Produkttitel skor - Heppo.se - Färg huvudkategori online
		-- Exempel: Blowfish Hamish Street Denim skor – Heppo.se – Röda damskor online

		
		-- Meta Desc>
		-- Köp Produktnamn huvudkategori från varumärke på nätet. Gratis frakt på skor, även fri retur. 
		-- 30 dagars öppet köp på underkategori och skor hos Heppo.
		------------------------------------------------------------
		
		-- DEFAULT --
		declare @LanguageId int
		set @LanguageId = 1 -- Sweden
		
		INSERT INTO product.tProductSeoSettingTranslation(ProductId, LanguageId)
		SELECT
			p.ProductId,
			@languageId
		FROM
			product.tProduct p
		WHERE
			not exists (SELECT 1
							FROM product.tProductSeoSettingTranslation n
							WHERE n.ProductId = p.ProductId and
							n.LanguageId = @LanguageId)
							


		UPDATE
			pss
		SET
			pss.Title = b.Title  + ' ' + p.Title + ' skor - Heppo.se ' + LOWER(coalesce(t.Value,'')) + 'a ' 
			+ case c2.Title when 'Kvinna' then 'damskor'
								when 'Man' then 'herrskor'
								--when 657 then 'Denmark'
								end 
			+ ' online', 
			
			pss.[Description] = 'Heppo.se din skobutik på nätet. Köp ' + b.Title + ' ' + p.Title + ' på nätet. ' + 
			'Gratis frakt på skor, även fri retur. 30 dagars öppet köp när du köper ' + c.Title + 
			' och ' + case c2.Title when 'Kvinna' then 'damskor'
								when 'Man' then 'herrskor'
								end 
			+ ' online från Heppo.'
			-- WARNING !! kan finnas mer än en tag färg !
			--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title, tt.value
		FROM 
			product.tProductSeoSettingTranslation pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				inner join product.tCategoryTranslation ct
				on c.CategoryId = ct.CategoryId
				
				inner join product.tCategoryTranslation ct2
				on c2.CategoryId = ct2.CategoryId
				
				left join lekmer.tProductTag pt
					on pt.ProductId = p.ProductId
				left join lekmer.tTag t
					on t.TagId = pt.TagId
				inner join lekmer.tTagTranslation tt
					on t.TagId = tt.TagId
		WHERE
			c3.CategoryId = 1000059 -- skor
			and t.TagGroupId = 30 -- färg
			and pss.LanguageId = @LanguageId
			and tt.LanguageId = @LanguageId
			and ct.LanguageId = @LanguageId
			and ct2.LanguageId = @LanguageId
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))


		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
