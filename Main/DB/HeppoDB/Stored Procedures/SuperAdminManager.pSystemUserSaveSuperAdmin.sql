SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE procedure [SuperAdminManager].[pSystemUserSaveSuperAdmin]
	@Username			nvarchar(100),
	@Password			nvarchar(50)
as
begin
	set nocount on

	declare @SuperUserId int
	if not exists(select 1 from [security].[tSystemUser] where [Username] = @UserName)
	begin
		insert into [security].[tSystemUser]
		(
			[Name],
			[Username],
			[Password]
		)
		values
		(
			@Username,
			@Username,
			@Password
		)

		set @SuperUserId = scope_identity()
	end
	else
	begin
		update
			[security].[tSystemUser]
		set
			[Password] = @Password,
			[ActivationDate] = getdate(),
			[ExpirationDate] = dateadd(day, 30, getdate()),
			[StatusId] = (select SystemuserStatusId from security.tSystemuserStatus where CommonName = 'Online')
		where
			[Username] = @UserName

		delete
			[security].[tUserRole]
		from
			[security].[tUserRole] r
			inner join [security].[tSystemUser] u on r.[SystemUserId] = u.[SystemUserId]
		where
			[Username] = @UserName

		select
			@SuperUserId = SystemUserId
		from
			[security].[tSystemUser]
		where
			[Username] = @UserName
	end

	insert into [security].[tUserRole]
	(
		[SystemUserId],
		[RoleId]
	)
	select
		@SuperUserId,
		RoleId
	from 
		[security].[tRole]
end






GO
