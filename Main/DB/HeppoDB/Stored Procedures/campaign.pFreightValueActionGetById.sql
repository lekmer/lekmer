SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pFreightValueActionGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomFreightValueAction
	WHERE
		[CartAction.Id] = @Id
end

GO
