SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationGroupGetById]
	@VariationGroupId	int
as
begin
	set nocount off
	SELECT 
		*
	FROM 
		[product].[vCustomVariationGroup] vg
	where
		vg.[VariationGroup.Id] = @VariationGroupId
end

GO
