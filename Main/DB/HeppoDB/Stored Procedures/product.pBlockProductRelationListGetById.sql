SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductRelationListGetById]
 @LanguageId	int,
 @BlockId		int
as
begin
	select 
		bprl.*,
		b.*
	from 
		[product].[vCustomBlockProductRelationList] as bprl
		inner join [sitestructure].[vCustomBlock] as b on bprl.[BlockProductRelationList.BlockId] = b.[Block.BlockId]
	where
		bprl.[BlockProductRelationList.BlockId] = @BlockId 
		AND b.[Block.LanguageId] = @LanguageId
end

GO
