SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteByProductIdAndUrlTitle]
	@ProductId		INT,
	@LanguageId		INT,
	@UrlTitleOld	NVARCHAR(256)
AS
BEGIN
	DELETE 
		[lekmer].[tProductUrlHistory]
	WHERE 
		ProductId = @ProductId
		AND LanguageId = @LanguageId
		AND UrlTitleOld = @UrlTitleOld
END
GO
