SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockAvailCombineGetById]
	@BlockId	int
AS 
BEGIN 
	SET nocount ON 
	SELECT 
		* 
	FROM 
		lekmer.tBlockAvailCombine as BAC
		inner join [sitestructure].[vCustomBlock] as b on BAC.[BlockId] = b.[Block.BlockId]

	WHERE 
		BAC.BlockId = @BlockId
END
GO
