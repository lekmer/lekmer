SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pConditionTypeGetAll]
as
begin
	SELECT
		*
	FROM
		campaign.vCustomConditionType
end

GO
