SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ändra sen så att den kan hantera en lista av Ids
CREATE PROCEDURE [integration].[usp_DeleteProductImageReferences]
	@HYErpId	nvarchar	(25)
AS
begin
	set nocount on
	
	if object_id('tempdb..#tProductImageMediaIdToDelete') is not null
		drop table #tProductImageMediaIdToDelete
		
	
	create table #tProductImageMediaIdToDelete
	(
		tMediaMediaId int not null,
		tImageMediaId int not null,
		tProductImageMediaId int not null
		constraint PK_#tProductImageMediaIdToDelete primary key(tMediaMediaId)
	)
	
	
	begin try
		begin transaction
		
		declare @StartMediaId int
		declare @EndMediaId int
		--set @HYErpId = '001009-0108'

			--coalesce(ois.ErpId, p.ErpId) ErpId
		
		
		-- om det är bara en bild finns den inte i product.tProductImage och får då hanteras manuellt
		insert into #tProductImageMediaIdToDelete(tMediaMediaId, tImageMediaId, tProductImageMediaId)
		select m.MediaId, im.MediaId, pii.MediaId
		from
			lekmer.tLekmerProduct l
				inner join product.tProduct p
					on l.productid = p.productid
				inner join product.tproductImage pii
					on p.productid = pii.productid
				inner join media.tMedia m
					on pii.MediaId = m.MediaId
				inner join media.tImage im
					on im.MediaId = m.MediaId
		where 
			l.HYErpId = @HYErpId
	
		--select * from #tProductImageMediaIdToDelete
		
		--select top 1 tMediaMediaId, tImageMediaId, tProductImageMediaId from #tProductImageMediaIdToDelete
		
		--select top 1 tMediaMediaId, tImageMediaId, tProductImageMediaId from #tProductImageMediaIdToDelete
		--order by tMediaMediaId desc
		
		
		set @StartMediaId = (select top 1 tMediaMediaId from #tProductImageMediaIdToDelete)
		set @EndMediaId = (select top 1 tMediaMediaId from #tProductImageMediaIdToDelete
							order by tMediaMediaId desc)
		--select @StartMediaId, @EndMediaId
		
		-- en kontroll här så mediaid är samma i alla 3 kolumner i temp taballen
		if @StartMediaId = (select top 1 tProductImageMediaId from #tProductImageMediaIdToDelete)
			AND @EndMediaId = (select top 1 tProductImageMediaId from #tProductImageMediaIdToDelete
							order by tMediaMediaId desc)
		begin
			update 
				p
			set 
				MediaId = NULL 
			from product.tProduct p inner join lekmer.tlekmerproduct l
				on p.productid  = l.ProductId
			where l.HYErpId = @HYErpId--'106009-0054'

			delete a
			from product.tProductImage as a
			where a.mediaId between @StartMediaId and @EndMediaId

			print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table product.tProductImage'
			
			delete a
			from media.tImage as a
			where a.mediaId between @StartMediaId and @EndMediaId

			print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table media.tImage' 
			
			delete a
			from media.tMedia as a
			where a.mediaId between @StartMediaId and @EndMediaId

			print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table media.tMedia' 
		end
		Else
		begin
			print 'No image references to be found...'
		end
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
