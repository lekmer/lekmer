SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman D.	Date: 21.05.2009	Time: 16:00
Description:	Created
*/

create PROCEDURE [order].[pBlockCheckoutDelete]
@BlockId	int
as
begin
	delete
		[order].[tBlockCheckout]
	where
		[BlockId] = @BlockId
end
GO
