SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pReviewStatusGetByCommonName]
@CommonName varchar(50)
AS    
BEGIN
         SELECT 
                   * 
         FROM 
                   addon.vCustomReviewStatus
         WHERE 
                   [ReviewStatus.CommonName] = @CommonName
END

GO
