
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignSave]
	@CampaignId			INT,
	@Title				VARCHAR(500),
	@CampaignStatusId	INT,
	@CampaignRegistryId INT,
	@FolderId			INT,
	@StartDate			DATETIME,
	@EndDate			DATETIME,
	@Exclusive			BIT,
	@Priority			INT,
	@LevelId			INT	,
	@WebTitle			VARCHAR(500) =null,
	@IconMediaId		INT = NULL,
	@ImageMediaId		INT = NULL,
	@Description		VARCHAR(MAX) = NULL,
	@LinkContentNodeId	INT = NULL,
	@UseLandingPage		BIT = 0
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		[Title]					= @Title,
		[CampaignStatusId]		= @CampaignStatusId,
		[CampaignRegistryId]	= @CampaignRegistryId,
		[FolderId]				= @FolderId,
		[StartDate]				= @StartDate,
		[EndDate]				= @EndDate,
		[Exclusive]				= @Exclusive,
		[Priority]				= @Priority,
		[LevelId]				= @LevelId,
		[WebTitle]				= @WebTitle,
		[IconMediaId]			= @IconMediaId,
		[ImageMediaId]			= @ImageMediaId,
		[Description]			= @Description,
		[LinkContentNodeId]		= @LinkContentNodeId,
		[UseLandingPage]		= @UseLandingPage
	WHERE
		[CampaignId] = @CampaignId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		SET @Priority = (SELECT MAX(PRIORITY) FROM campaign.tCampaign) + 1
		IF @Priority IS NULL
			BEGIN
				SET @Priority = 1
			END
			
		INSERT campaign.tCampaign (
			[Title],
			[CampaignStatusId],
			[CampaignRegistryId],
			[FolderId],
			[StartDate],
			[EndDate],
			[Exclusive],
			[Priority],
			[LevelId],
			[WebTitle],
			[IconMediaId],
			[ImageMediaId],
			[Description],
			[LinkContentNodeId],
			[UseLandingPage]
		)
		VALUES (
			@Title,
			@CampaignStatusId,
			@CampaignRegistryId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority,
			@LevelId,
			@WebTitle,
			@IconMediaId,
			@ImageMediaId,
			@Description,
			@LinkContentNodeId,
			@UseLandingPage	
		)
		
		SET @CampaignId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @CampaignId
END
GO
