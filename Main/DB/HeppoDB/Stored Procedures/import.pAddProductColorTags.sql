SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pAddProductColorTags]

AS
BEGIN

		begin
			begin try
				begin transaction
					
				insert into lekmer.tProductTag
				select distinct
					ProductId = p.ProductId,
					TagId = isnull((select TagId from lekmer.tTag where Value = tp.ColorId), 251)			 
				FROM 
				[import].tProduct tp
				inner join [lekmer].tLekmerProduct lp
						on lp.HYErpId = tp.HYErpId
				inner join product.tProduct p
						on p.ProductId = lp.ProductId

				where
					 tp.ChannelId = 1
					 and p.productid not in (select productid
													from lekmer.tproductTag pt
													inner join lekmer.tTag t
														on pt.TagId = t.TagId
													inner join lekmer.tTagGroup tg
														on t.TagGroupId = tg.TagGroupId
													where 
														tg.TagGroupId = 30)
					 
					 				
				commit
			end try		
			begin catch
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
						values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			

		end		
END
GO
