SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionExcludeCategoryDeleteAll]
	@ProductActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tFixedPriceActionExcludeCategory]
	WHERE
		ProductActionId = @ProductActionId
END

GO
