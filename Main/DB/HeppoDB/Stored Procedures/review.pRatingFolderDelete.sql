SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingFolderDelete]
	@RatingFolderId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM [review].[tRatingFolder]
	WHERE [RatingFolderId] = @RatingFolderId
END
GO
