
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCategorySave]
	@CategoryId					int,
	@Title						nvarchar(50),
	@AllowMultipleSizesPurchase	bit
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[product].[tCategory]
	SET
		[Title] = @Title
	WHERE
		[CategoryId] = @CategoryId
	
	SET NOCOUNT OFF
		
	UPDATE
		[lekmer].[tLekmerCategory]
	SET
		[AllowMultipleSizesPurchase] = @AllowMultipleSizesPurchase
	WHERE
		[CategoryId] = @CategoryId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			[lekmer].[tLekmerCategory]
		( 
			[CategoryId],
			[AllowMultipleSizesPurchase]
		)
		VALUES 
		(
			@CategoryId,
			@AllowMultipleSizesPurchase
		)
	END
END
GO
