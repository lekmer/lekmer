SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewStatusGetAll]
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewStatus]
END
GO
