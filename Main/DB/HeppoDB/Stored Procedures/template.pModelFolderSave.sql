SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFolderSave]
    @ModelFolderId INT,
    @Title NVARCHAR(50)
AS 
BEGIN
    SET NOCOUNT ON
    	
    UPDATE  
		[template].[tModelFolder]
    SET 
		[Title] = @Title
    WHERE
		[ModelFolderId] = @ModelFolderId
    
    IF  @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tModelFolder] ( [Title] )
        VALUES  ( @Title )

        SET @ModelFolderId = SCOPE_IDENTITY()
    END
    RETURN @ModelFolderId
END
GO
