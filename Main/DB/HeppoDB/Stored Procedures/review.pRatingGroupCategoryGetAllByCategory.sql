SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupCategoryGetAllByCategory]
	@CategoryId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgc.*
	FROM
		[review].[vRatingGroupCategory] rgc
	WHERE
		rgc.[RatingGroupCategory.CategoryId] = @CategoryId
END
GO
