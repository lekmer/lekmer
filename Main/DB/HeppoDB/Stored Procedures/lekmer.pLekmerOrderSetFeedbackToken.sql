SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pLekmerOrderSetFeedbackToken]
	@OrderId INT,
	@FeedbackToken UNIQUEIDENTIFIER
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		FeedbackToken = @FeedbackToken
	WHERE 
		OrderId = @OrderId
END 
GO
