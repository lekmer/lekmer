SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagSetOrdinal]
	@FlagIdList		varchar(max),
	@Separator			char(1)
AS
BEGIN
	UPDATE
		[lekmer].[tFlag]
	SET
		Ordinal = t.Ordinal
	FROM
		[lekmer].[tFlag] f
		INNER JOIN generic.fnConvertIDListToTableWithOrdinal(@FlagIdList, @Separator) t ON f.FlagId = t.Id
END

GO
