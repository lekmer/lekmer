
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductVoteInsert]
	@RatingReviewFeedbackId INT,
	@RatingId INT,
	@RatingItemId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingItemProductVote] (
		[RatingReviewFeedbackId],
		[RatingId],
		[RatingItemId]
	)
	VALUES (
		@RatingReviewFeedbackId,
		@RatingId,
		@RatingItemId
	)
END
GO
