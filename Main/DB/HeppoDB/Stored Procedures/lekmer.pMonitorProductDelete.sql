SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMonitorProductDelete]
	@MonitorProductId	INT
AS 
BEGIN 
	DELETE
		[lekmer].[tMonitorProduct]
	WHERE
		MonitorProductId = @MonitorProductId
END
GO
