SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pRecommendedPriceDeleteAllByProduct]
	@ProductId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tRecommendedPrice]
	WHERE 
		[ProductId] = @ProductId
END 
GO
