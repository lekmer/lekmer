
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customerlek].[pAddressSave]
	@AddressId		INT,
	@CustomerId		INT,
	@Addressee		NVARCHAR(100),
	@StreetAddress	NVARCHAR(200),
	@StreetAddress2 NVARCHAR(200),
	@PostalCode		NVARCHAR(10),
	@City			NVARCHAR(100),
	@CountryId		INT,
	@PhoneNumber	NVARCHAR(20),
	@AddressTypeId	INT,
	@HouseNumber	NVARCHAR(50),
	@HouseExtension	NVARCHAR(100),
	@Reference		NVARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON
	
	--Check if customer exist
	IF NOT EXISTS ( 
					SELECT 
						1 
					FROM 
						[customer].[tCustomer] 
					WHERE 
						[tCustomer].[CustomerId] = @CustomerId 
					)
		RETURN -1
	
	UPDATE  [customer].[tAddress]
	SET 
		Addressee = @Addressee,
		StreetAddress = @StreetAddress,
		StreetAddress2 = @StreetAddress2,
		PostalCode = @PostalCode,
		City = @City,
		CountryId = @CountryId,
		PhoneNumber = @PhoneNumber,
		AddressTypeId = @AddressTypeId
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customer].[tAddress]
		(
			CustomerId,
			Addressee,
			StreetAddress,
			StreetAddress2,
			PostalCode,
			City,
			CountryId,
			PhoneNumber,
			AddressTypeId
		)
		VALUES
		(
			@CustomerId,
			@Addressee,
			@StreetAddress,
			@StreetAddress2,
			@PostalCode,
			@City,
			@CountryId,
			@PhoneNumber,
			@AddressTypeId
		)
		SET	@AddressId = SCOPE_IDENTITY()
	END
	
	UPDATE  [customerlek].[tAddress]
	SET 
		HouseNumber = @HouseNumber,
		HouseExtension = @HouseExtension,
		Reference = @Reference
	WHERE
		AddressId = @AddressId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customerlek].[tAddress]
		(
			AddressId,
			CustomerId,
			HouseNumber,
			HouseExtension,
			Reference
		)
		VALUES
		(
			@AddressId,
			@CustomerId,
			@HouseNumber,
			@HouseExtension,
			@Reference
		)
	END

	RETURN @AddressId
END
GO
