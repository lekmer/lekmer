SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeCategoryGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		CategoryId
	FROM 
		[campaignlek].[tFixedDiscountActionIncludeCategory]
	WHERE 
		ProductActionId = @ProductActionId
END
GO
