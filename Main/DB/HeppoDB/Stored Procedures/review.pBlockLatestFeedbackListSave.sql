SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListSave]
	@BlockId INT,
	@CategoryId INT,
	@NumberOfItems INT
AS
BEGIN
	UPDATE [review].[tBlockLatestFeedbackList]
	SET
		[CategoryId] = @CategoryId,
		[NumberOfItems] = @NumberOfItems
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [review].[tBlockLatestFeedbackList] (
		[BlockId],
		[CategoryId],
		[NumberOfItems]
	)
	VALUES (
		@BlockId,
		@CategoryId,
		@NumberOfItems
	)
END
GO
