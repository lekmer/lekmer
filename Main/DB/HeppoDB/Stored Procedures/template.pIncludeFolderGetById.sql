SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 19.09.2008		Time: 17:00
Description:
			Created 
*****************  Version 2  *****************
User: Yuriy P.		Date: 06.10.2008		Time: 10:00
Description:
			Change logic (Modified Preorder Tree Traversal)

*****************  Version 3  *****************
User: Egorov V.		Date: 20.11.2008		Time: 10:23
Description:
			Delete ChannelId 
*****************  Version 4  *****************
User: Dovhun R..		Date: 14.01.09		Time: 12:23
Description:
			Path
*****************  Version 5  *****************
User: Yuriy P.		Date: 23/01/2009
Description: Added Path 
*/
CREATE PROCEDURE [template].[pIncludeFolderGetById]
	@IncludeFolderId	int
AS
begin
	set nocount on

	select
		*	
	from
		[template].[vCustomIncludeFolder]
	where
		[IncludeFolder.Id] = @IncludeFolderId
end

GO
