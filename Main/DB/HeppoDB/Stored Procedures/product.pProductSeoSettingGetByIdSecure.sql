SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductSeoSettingGetByIdSecure]
	@ProductId	INT
AS
BEGIN
	SELECT 
		*
	FROM 
		[product].[vCustomProductSeoSettingSecure] AS PSS
	WHERE
		PSS.[SeoSetting.ProductId] = @ProductId
END

GO
