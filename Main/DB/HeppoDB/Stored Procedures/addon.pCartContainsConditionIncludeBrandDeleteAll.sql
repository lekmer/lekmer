SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeBrandDeleteAll]
	@ConditionId INT
AS
BEGIN
	DELETE
		[addon].[tCartContainsConditionIncludeBrand]
	WHERE
		ConditionId = @ConditionId
END
GO
