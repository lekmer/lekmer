SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pFreeFreightFlagActionCurrencyDeleteAll]
	@ActionId int
AS 
BEGIN
	DELETE FROM [lekmer].[tFreeFreightFlagActionCurrency]
	WHERE ProductActionId = @ActionId
END

GO
