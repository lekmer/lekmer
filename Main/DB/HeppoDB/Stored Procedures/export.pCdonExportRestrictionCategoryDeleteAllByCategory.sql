SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryDeleteAllByCategory]
	@CategoryIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE
		rc
	FROM
		[export].[tCdonExportRestrictionCategory] rc
		INNER JOIN [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) AS pl ON pl.[ID] = [rc].[CategoryId]
END
GO
