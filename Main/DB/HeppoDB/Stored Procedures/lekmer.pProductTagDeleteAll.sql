SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagDeleteAll]
	@ProductId	int
AS
BEGIN 
	DELETE FROM lekmer.tProductTag
	WHERE ProductId = @ProductId
END 
GO
