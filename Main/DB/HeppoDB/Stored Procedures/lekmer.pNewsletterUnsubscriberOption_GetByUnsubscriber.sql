SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_GetByUnsubscriber]
	@UnsubscriberId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vNewsletterUnsubscriberOption]
	WHERE
		[NewsletterUnsubscriberOption.UnsubscriberId] = @UnsubscriberId
END

GO
