SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionCurrencyDeleteAll]
	@ConditionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemsGroupValueConditionCurrency]
	WHERE
		[ConditionId] = @ConditionId
END
GO
