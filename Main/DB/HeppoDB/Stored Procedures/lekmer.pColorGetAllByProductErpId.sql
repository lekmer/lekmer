SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pColorGetAllByProductErpId]
	@ProductErpId	VARCHAR(50)
AS 
BEGIN 
	SELECT 
		[c].[ColorId] AS 'ProductColor.ColorId',
		[lp].[ProductId] AS 'ProductColor.ProductId'
	FROM 
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [productlek].[tColor] c ON [c].[HyErpId] = SUBSTRING([lp].[HYErpId], LEN([lp].[HYErpId])-3, 4)
	WHERE
		[lp].[HYErpId] = @ProductErpId
END
GO
