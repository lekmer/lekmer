SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pOrderVoucherInfoDelete]
	@OrderId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE
		[orderlek].[tOrderVoucherInfo]
	WHERE
		[OrderId] = @OrderId
END
GO
