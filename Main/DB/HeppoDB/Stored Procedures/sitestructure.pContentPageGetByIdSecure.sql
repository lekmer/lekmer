SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetByIdSecure]
@ContentNodeId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageSecure]
	where
		[ContentNode.ContentNodeId] = @ContentNodeId
end

GO
