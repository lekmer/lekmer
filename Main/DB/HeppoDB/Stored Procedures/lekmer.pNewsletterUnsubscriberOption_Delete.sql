SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterUnsubscriberOption_Delete]
	@UnsubscriberOprionId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tNewsletterUnsubscriberOption]
	WHERE
		[UnsubscriberOprionId] = @UnsubscriberOprionId

END
GO
