SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionCurrencySave]
	@ConditionId	INT,
	@CurrencyId		INT,
	@ValueFrom		DECIMAL(16,2),
	@ValueTo		DECIMAL(16,2)
AS 
BEGIN 
	INSERT [campaignlek].[tCartValueRangeConditionCurrency] ( 
		ConditionId,
		CurrencyId,
		MonetaryValueFrom,
		MonetaryValueTo
	)
	VALUES (
		@ConditionId,
		@CurrencyId,
		@ValueFrom,
		@ValueTo
	)
END
GO
