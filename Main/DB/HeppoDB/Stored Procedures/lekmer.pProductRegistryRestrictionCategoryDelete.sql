SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionCategoryDelete]
	@CategoryId	INT
AS
BEGIN 
	DELETE 
		[lekmer].[tProductRegistryRestrictionCategory]
	WHERE 
		[CategoryId] = @CategoryId
END 
GO
