SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[pShowRunningSQL]
as
begin

	set nocount on
    set transaction isolation level read uncommitted

    select 
		session_Id as [Spid], 
		[ecid], 
		DB_NAME(sp.dbid) as [Database],
		nt_username as [User],
		er.status as [Status],
		wait_type as [Wait],
		substring (
			qt.text, 
			er.statement_start_offset/2,
			(
				case 
					when er.statement_end_offset = -1 then len(convert(nvarchar(max), qt.text)) * 2
					else er.statement_end_offset 
				end - er.statement_start_offset)/2
		) as [Individual Query],
		qt.text as [Parent Query],
		program_name as [Program],
		[Hostname],
		[nt_domain],
		[start_time]
	from 
		sys.dm_exec_requests er
		inner join sys.sysprocesses sp on er.session_id = sp.spid
		cross apply sys.dm_exec_sql_text(er.sql_handle) as qt
	where 
		session_Id > 50 -- Ignore system spids.
		and session_Id not in (@@spid) -- Ignore this current statement.
	order by 
		1, 2
end


GO
