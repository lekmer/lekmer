SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconTranslationSave]
	@IconId			INT,
	@LanguageId		INT,
	@Title			NVARCHAR(50),
	@Description	NVARCHAR(255)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tIconTranslation]
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[IconId] = @IconId
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [lekmer].[tIconTranslation] (
			[IconId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES (
			@IconId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END
GO
