SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [lekmer].[pBlockBrandProductListSave]
@BlockId			int,
@ColumnCount		int,
@RowCount			int,
@ProductSortOrderId	int
as
begin
	update
		[lekmer].[tBlockBrandProductList]
	set
		[ColumnCount]			= @ColumnCount,
		[RowCount]				= @RowCount,
		[ProductSortOrderId]	= @ProductSortOrderId
	where
		[BlockId]		= @BlockId
			
	if  @@ROWCOUNT = 0 
		begin
			insert
				[lekmer].[tBlockBrandProductList]
				(
					[BlockId],
					[ColumnCount],
					[RowCount],
					[ProductSortOrderId]
				)
			values
				(
					@BlockId,
					@ColumnCount,
					@RowCount,
					@ProductSortOrderId
				)
		end
end

GO
