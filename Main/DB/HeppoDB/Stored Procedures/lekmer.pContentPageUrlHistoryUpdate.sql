SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryUpdate]
	@ContentPageUrlHistoryId	INT
AS 
BEGIN 
	UPDATE 
		lekmer.tContentPageUrlHistory
	SET 
		UsageAmount = UsageAmount + 1,
		LastUsageDate = GETDATE()
	WHERE
		ContentPageUrlHistoryId = @ContentPageUrlHistoryId
END 
GO
