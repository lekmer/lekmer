SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [security].[pSystemUserSave]
@SystemUserId			int,
@Username				nvarchar(100),
@Name					nvarchar(50),
@Password				nvarchar(50),
@StatusId				int,
@FailedSignInCounter	int,
@CultureId				int,
@ActivationDate			smalldatetime,
@ExpirationDate			smalldatetime
as

begin
	set nocount on
	
	if exists (
			select 
				1 
			from 
				[security].[tSystemUser]
			where 
				[Username] = @Username 
				and [SystemUserId] <> @SystemUserId
			)
	return -1
	
	update 
		[security].[tSystemUser]
	SET
		[Username] = @Username,
		[Name] = @Name,
		[StatusId] = @StatusId,
		[Password] = @Password,
		[FailedSigninCounter] = @FailedSigninCounter,
		[CultureId] = @CultureId,
		[ActivationDate] = @ActivationDate,
		[ExpirationDate] = @ExpirationDate
	where
		[SystemUserId] = @SystemUserId
	if @@ROWCOUNT = 0
	begin			
		insert into [security].[tSystemUser]
		(
			[Username],
			[Name],
			[Password],
			[StatusId],
			[FailedSigninCounter],
			[CultureId],
			[ActivationDate],
			[ExpirationDate]
		)
		values
		(
			@Username,
			@Name,
			@Password,
			@StatusId,
			0,
			@CultureId,
			@ActivationDate,
			@ExpirationDate
		)
		
		Set @SystemUserId = scope_identity()
	end

	return @SystemUserId
end
	




GO
