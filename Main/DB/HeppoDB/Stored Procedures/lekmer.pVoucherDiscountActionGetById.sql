
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherDiscountActionGetById]
	@ActionId int
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		CCA.*,
		VA.*
	FROM
		campaign.vCustomCartAction as CCA
		left join lekmer.tVoucherAction VA on VA.CartActionId=CCA.[CartAction.Id]
	WHERE
		[CartAction.Id] = @ActionId
END
GO
