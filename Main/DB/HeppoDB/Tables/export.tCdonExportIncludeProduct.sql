CREATE TABLE [export].[tCdonExportIncludeProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportIncludeProduct] ADD CONSTRAINT [PK_tCdonExportIncludeProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportIncludeProduct] ADD CONSTRAINT [FK_tCdonExportIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [export].[tCdonExportIncludeProduct] ADD CONSTRAINT [FK_tCdonExportIncludeProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
