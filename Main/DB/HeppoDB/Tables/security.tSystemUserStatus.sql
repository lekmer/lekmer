CREATE TABLE [security].[tSystemUserStatus]
(
[SystemUserStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [security].[tSystemUserStatus] ADD CONSTRAINT [PK_tSystemUserStatus] PRIMARY KEY CLUSTERED  ([SystemUserStatusId]) ON [PRIMARY]
GO
