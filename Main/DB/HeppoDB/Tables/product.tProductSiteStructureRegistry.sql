CREATE TABLE [product].[tProductSiteStructureRegistry]
(
[ProductId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NOT NULL,
[ParentContentNodeId] [int] NULL,
[TemplateContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSiteStructureRegistry] ADD CONSTRAINT [PK_tProductSiteStructureRegistry] PRIMARY KEY CLUSTERED  ([ProductId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductSiteStructureRegistry] ADD CONSTRAINT [FK_tProductSiteStructureRegistry_tContentPage_Parent] FOREIGN KEY ([ParentContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId], [SiteStructureRegistryId])
GO
ALTER TABLE [product].[tProductSiteStructureRegistry] ADD CONSTRAINT [FK_tProductSiteStructureRegistry_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tProductSiteStructureRegistry] ADD CONSTRAINT [FK_tProductSiteStructureRegistry_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
ALTER TABLE [product].[tProductSiteStructureRegistry] ADD CONSTRAINT [FK_tProductSiteStructureRegistry_tContentPage_Template] FOREIGN KEY ([TemplateContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId], [SiteStructureRegistryId])
GO
