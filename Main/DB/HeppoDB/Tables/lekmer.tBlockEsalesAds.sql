CREATE TABLE [lekmer].[tBlockEsalesAds]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[PanelPath] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Format] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Gender] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductCategory] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockEsalesAds] ADD CONSTRAINT [PK_BlockEsalesAds] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockEsalesAds] ADD CONSTRAINT [FK_BlockEsalesAds_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
