CREATE TABLE [product].[tProductVariation]
(
[VariationGroupId] [int] NOT NULL,
[VariationId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[VariationTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductVariation] ADD CONSTRAINT [PK_tProductVariation] PRIMARY KEY CLUSTERED  ([VariationId], [VariationGroupId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductVariation] ADD CONSTRAINT [UQ_tProductVariation_Product_VariationType] UNIQUE NONCLUSTERED  ([ProductId], [VariationGroupId], [VariationTypeId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductVariation] ADD CONSTRAINT [FK_tProductVariation_tVariationGroupProduct] FOREIGN KEY ([VariationGroupId], [ProductId]) REFERENCES [product].[tVariationGroupProduct] ([VariationGroupId], [ProductId])
GO
ALTER TABLE [product].[tProductVariation] ADD CONSTRAINT [FK_tProductVariation_tVariation] FOREIGN KEY ([VariationGroupId], [VariationId]) REFERENCES [product].[tVariation] ([VariationGroupId], [VariationId])
GO
ALTER TABLE [product].[tProductVariation] ADD CONSTRAINT [FK_tProductVariation_tVariationType] FOREIGN KEY ([VariationGroupId], [VariationTypeId]) REFERENCES [product].[tVariationType] ([VariationGroupId], [VariationTypeId])
GO
