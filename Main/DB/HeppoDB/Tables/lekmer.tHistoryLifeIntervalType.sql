CREATE TABLE [lekmer].[tHistoryLifeIntervalType]
(
[HistoryLifeIntervalTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DaysActive] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tHistoryLifeIntervalType] ADD CONSTRAINT [PK_HistoryLifeIntervalType] PRIMARY KEY CLUSTERED  ([HistoryLifeIntervalTypeId]) ON [PRIMARY]
GO
