CREATE TABLE [review].[tRatingModuleChannel]
(
[ChannelId] [int] NOT NULL,
[RatingRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingModuleChannel] ADD CONSTRAINT [PK_tRatingModuleChannel] PRIMARY KEY CLUSTERED  ([ChannelId], [RatingRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingModuleChannel] ADD CONSTRAINT [FK_tRatingModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [review].[tRatingModuleChannel] ADD CONSTRAINT [FK_tRatingModuleChannel_tRatingRegistry] FOREIGN KEY ([RatingRegistryId]) REFERENCES [review].[tRatingRegistry] ([RatingRegistryId])
GO
