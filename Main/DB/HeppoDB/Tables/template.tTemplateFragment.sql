CREATE TABLE [template].[tTemplateFragment]
(
[TemplateId] [int] NOT NULL,
[ModelFragmentId] [int] NOT NULL,
[Content] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AlternateContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [PK_tTemplateFragment] PRIMARY KEY CLUSTERED  ([TemplateId], [ModelFragmentId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [FK_tTemplateFragment_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
GO
ALTER TABLE [template].[tTemplateFragment] ADD CONSTRAINT [FK_tTemplateFragment_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
