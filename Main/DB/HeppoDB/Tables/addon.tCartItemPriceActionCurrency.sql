CREATE TABLE [addon].[tCartItemPriceActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionCurrency] ADD CONSTRAINT [PK_tCartItemPriceActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionCurrency] ADD CONSTRAINT [FK_tCartItemPriceActionCurrency_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId])
GO
ALTER TABLE [addon].[tCartItemPriceActionCurrency] ADD CONSTRAINT [FK_tCartItemPriceActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
