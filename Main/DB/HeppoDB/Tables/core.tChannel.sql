CREATE TABLE [core].[tChannel]
(
[ChannelId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CountryId] [int] NOT NULL,
[ApplicationName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AlternateLayoutRatio] [decimal] (5, 2) NOT NULL CONSTRAINT [DF_tChannel_AlternateLayoutRatio] DEFAULT ((0)),
[LanguageId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[CultureId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [PK_tChannel] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [UQ_tChannel_ApplicationName] UNIQUE NONCLUSTERED  ([ApplicationName]) ON [PRIMARY]
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [UQ_tChannel_Title] UNIQUE NONCLUSTERED  ([Title]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tChannel_CountryId] ON [core].[tChannel] ([CountryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tChannel_CultureId] ON [core].[tChannel] ([CultureId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tChannel_CurrencyId] ON [core].[tChannel] ([CurrencyId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tChannel_LanguageId] ON [core].[tChannel] ([LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [FK_tChannel_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [FK_tChannel_tCulture] FOREIGN KEY ([CultureId]) REFERENCES [core].[tCulture] ([CultureId])
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [FK_tChannel_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [core].[tChannel] ADD CONSTRAINT [FK_tChannel_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'A channel is an entry in to the site that gives the user specific poduct categories, products and prices. The swedish site is a channel. The daish site is another. This may also be a B2B-channel.', 'SCHEMA', N'core', 'TABLE', N'tChannel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Percentage determine how many users that will see the alternate templates. Stored as a positive integer multiplied with 1000 (1% = 1000)', 'SCHEMA', N'core', 'TABLE', N'tChannel', 'COLUMN', N'AlternateLayoutRatio'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the application (domain) for this channel. Unique constraint added.', 'SCHEMA', N'core', 'TABLE', N'tChannel', 'COLUMN', N'ApplicationName'
GO
EXEC sp_addextendedproperty N'MS_Description', N'A unique ID for the channel. Created automatically by the database engine.', 'SCHEMA', N'core', 'TABLE', N'tChannel', 'COLUMN', N'ChannelId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'A description of the channel.', 'SCHEMA', N'core', 'TABLE', N'tChannel', 'COLUMN', N'Title'
GO
