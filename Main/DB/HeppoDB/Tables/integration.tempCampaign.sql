CREATE TABLE [integration].[tempCampaign]
(
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tempCampaign] ADD CONSTRAINT [PK__tempCamp__B40CC6CD438B2AEE] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
