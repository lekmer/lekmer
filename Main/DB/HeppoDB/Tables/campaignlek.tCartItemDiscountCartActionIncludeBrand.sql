CREATE TABLE [campaignlek].[tCartItemDiscountCartActionIncludeBrand]
(
[CartActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionIncludeBrand] ADD CONSTRAINT [PK_tCartItemDiscountCartActionIncludeBrand] PRIMARY KEY CLUSTERED  ([CartActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionIncludeBrand] ADD CONSTRAINT [FK_tCartItemDiscountCartActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionIncludeBrand] ADD CONSTRAINT [FK_tCartItemDiscountCartActionIncludeBrand_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId]) ON DELETE CASCADE
GO
