CREATE TABLE [campaignlek].[tCampaignActionExcludeBrand]
(
[ConfigId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeBrand] ADD CONSTRAINT [PK_tCampaignActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ConfigId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeBrand] ADD CONSTRAINT [FK_tCampaignActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [campaignlek].[tCampaignActionExcludeBrand] ADD CONSTRAINT [FK_tCampaignActionExcludeBrand_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
