CREATE TABLE [template].[tInclude]
(
[IncludeId] [int] NOT NULL IDENTITY(1, 1),
[IncludeFolderId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Content] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [template].[tInclude] ADD CONSTRAINT [PK_tInclude] PRIMARY KEY CLUSTERED  ([IncludeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tInclude_CommonName] ON [template].[tInclude] ([CommonName]) ON [PRIMARY]
GO
ALTER TABLE [template].[tInclude] ADD CONSTRAINT [FK_tInclude_tIncludeFolder] FOREIGN KEY ([IncludeFolderId]) REFERENCES [template].[tIncludeFolder] ([IncludeFolderId])
GO
