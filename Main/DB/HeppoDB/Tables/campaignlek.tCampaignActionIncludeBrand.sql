CREATE TABLE [campaignlek].[tCampaignActionIncludeBrand]
(
[ConfigId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeBrand] ADD CONSTRAINT [PK_tCampaignActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ConfigId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeBrand] ADD CONSTRAINT [FK_tCampaignActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeBrand] ADD CONSTRAINT [FK_tCampaignActionIncludeBrand_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
