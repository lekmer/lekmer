CREATE TABLE [lekmer].[tOrderItemSize]
(
[OrderItemId] [int] NOT NULL,
[SizeId] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tOrderItemSize] ADD CONSTRAINT [PK_tOrderItemSize] PRIMARY KEY CLUSTERED  ([OrderItemId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tOrderItemSize] ADD CONSTRAINT [FK_tOrderItemSize_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
GO
ALTER TABLE [lekmer].[tOrderItemSize] ADD CONSTRAINT [FK_tOrderItemSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
