CREATE TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemPercentageDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeProduct_tCartItemPercentageDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemPercentageDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemPercentageDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tCartItemPercentageDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
