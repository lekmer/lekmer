CREATE TABLE [export].[tAvailCategoryPath]
(
[CategoryId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[CategoryPath] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tAvailCategoryPath] ADD CONSTRAINT [PK_tAvailCategoryPath] PRIMARY KEY CLUSTERED  ([CategoryId], [LanguageId]) ON [PRIMARY]
GO
