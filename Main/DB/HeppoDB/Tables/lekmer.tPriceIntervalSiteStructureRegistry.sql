CREATE TABLE [lekmer].[tPriceIntervalSiteStructureRegistry]
(
[PriceIntervalId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NOT NULL,
[ContentNodeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPriceIntervalSiteStructureRegistry] ADD CONSTRAINT [PK_tPriceIntervalSiteStructureRegistry] PRIMARY KEY CLUSTERED  ([PriceIntervalId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPriceIntervalSiteStructureRegistry] ADD CONSTRAINT [FK_tPriceIntervalSiteStructureRegistry_tContentNode] FOREIGN KEY ([ContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tPriceIntervalSiteStructureRegistry] ADD CONSTRAINT [FK_tPriceIntervalSiteStructureRegistry_tPriceInterval] FOREIGN KEY ([PriceIntervalId]) REFERENCES [lekmer].[tPriceInterval] ([PriceIntervalId])
GO
ALTER TABLE [lekmer].[tPriceIntervalSiteStructureRegistry] ADD CONSTRAINT [FK_tPriceIntervalSiteStructureRegistry_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
