CREATE TABLE [campaign].[tProductActionType]
(
[ProductActionTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tProductActionType] ADD CONSTRAINT [PK_tProductActionType] PRIMARY KEY CLUSTERED  ([ProductActionTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tProductActionType_CommonName] ON [campaign].[tProductActionType] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tProductActionType_Title] ON [campaign].[tProductActionType] ([Title]) ON [PRIMARY]
GO
