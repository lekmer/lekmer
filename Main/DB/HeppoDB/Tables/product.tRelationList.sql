CREATE TABLE [product].[tRelationList]
(
[RelationListId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[RelationListTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tRelationList] ADD CONSTRAINT [PK_tRelationList] PRIMARY KEY CLUSTERED  ([RelationListId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tRelationList_RelationListTypeId] ON [product].[tRelationList] ([RelationListTypeId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tRelationList] ADD CONSTRAINT [FK_tRelationList_tRelationListType] FOREIGN KEY ([RelationListTypeId]) REFERENCES [product].[tRelationListType] ([RelationListTypeId])
GO
