CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeProduct_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
