CREATE TABLE [review].[tBlockRatingItem]
(
[BlockId] [int] NOT NULL,
[RatingItemId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockRatingItem] ADD CONSTRAINT [PK_tBlockRatingItem] PRIMARY KEY CLUSTERED  ([BlockId], [RatingItemId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockRatingItem] ADD CONSTRAINT [FK_tBlockRatingItem_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [review].[tBlockRatingItem] ADD CONSTRAINT [FK_tBlockRatingItem_tRatingItem] FOREIGN KEY ([RatingItemId]) REFERENCES [review].[tRatingItem] ([RatingItemId])
GO
