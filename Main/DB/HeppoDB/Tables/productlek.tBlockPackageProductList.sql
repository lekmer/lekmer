CREATE TABLE [productlek].[tBlockPackageProductList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockPackageProductList] ADD CONSTRAINT [PK_tBlockPackageProductList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tBlockPackageProductList] ADD CONSTRAINT [FK_tBlockPackageProductList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
