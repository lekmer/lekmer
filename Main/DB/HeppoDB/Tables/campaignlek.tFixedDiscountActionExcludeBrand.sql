CREATE TABLE [campaignlek].[tFixedDiscountActionExcludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeBrand] ADD CONSTRAINT [PK_tFixedDiscountActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tFixedDiscountActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [campaignlek].[tFixedDiscountActionExcludeBrand] ADD CONSTRAINT [FK_tFixedDiscountActionExcludeBrand_tFixedDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedDiscountAction] ([ProductActionId])
GO
