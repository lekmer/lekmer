CREATE TABLE [corelek].[tSubDomainChannel]
(
[ChannelId] [int] NOT NULL,
[SubDomainId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [corelek].[tSubDomainChannel] ADD CONSTRAINT [PK_tSubDomainChannel] PRIMARY KEY CLUSTERED  ([ChannelId], [SubDomainId]) ON [PRIMARY]
GO
ALTER TABLE [corelek].[tSubDomainChannel] ADD CONSTRAINT [FK_tSubDomainChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [corelek].[tSubDomainChannel] ADD CONSTRAINT [FK_tSubDomainChannel_tSubDomain] FOREIGN KEY ([SubDomainId]) REFERENCES [corelek].[tSubDomain] ([SubDomainId])
GO
