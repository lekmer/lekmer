CREATE TABLE [lekmer].[tBrandSiteStructureRegistry]
(
[BrandId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NOT NULL,
[ContentNodeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrandSiteStructureRegistry] ADD CONSTRAINT [PK_tBrandSiteStructureRegistry] PRIMARY KEY CLUSTERED  ([BrandId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrandSiteStructureRegistry] ADD CONSTRAINT [FK_tBrandSiteStructureRegistry_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [lekmer].[tBrandSiteStructureRegistry] ADD CONSTRAINT [FK_tBrandSiteStructureRegistry_tContentNode] FOREIGN KEY ([ContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tBrandSiteStructureRegistry] ADD CONSTRAINT [FK_tBrandSiteStructureRegistry_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
