CREATE TABLE [lekmer].[tPriceIntervalTranslation]
(
[PriceIntervalId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPriceIntervalTranslation] ADD CONSTRAINT [PK_tPriceIntervalTranslation] PRIMARY KEY CLUSTERED  ([PriceIntervalId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPriceIntervalTranslation] ADD CONSTRAINT [FK_tPriceIntervalTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [lekmer].[tPriceIntervalTranslation] ADD CONSTRAINT [FK_tPriceIntervalTranslation_tPriceInterval] FOREIGN KEY ([PriceIntervalId]) REFERENCES [lekmer].[tPriceInterval] ([PriceIntervalId])
GO
