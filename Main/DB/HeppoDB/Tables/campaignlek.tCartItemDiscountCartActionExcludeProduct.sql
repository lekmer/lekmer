CREATE TABLE [campaignlek].[tCartItemDiscountCartActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemDiscountCartActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemDiscountCartActionExcludeProduct_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemDiscountCartActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
