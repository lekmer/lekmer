CREATE TABLE [lekmer].[tCategoryTagGroup]
(
[CategoryId] [int] NOT NULL,
[TagGroupId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCategoryTagGroup] ADD CONSTRAINT [PK_tCategoryTagGroup] PRIMARY KEY CLUSTERED  ([CategoryId], [TagGroupId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCategoryTagGroup] ADD CONSTRAINT [FK_tCategoryTagGroup_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [lekmer].[tCategoryTagGroup] ADD CONSTRAINT [FK_tCategoryTagGroup_tTagGroup] FOREIGN KEY ([TagGroupId]) REFERENCES [lekmer].[tTagGroup] ([TagGroupId])
GO
