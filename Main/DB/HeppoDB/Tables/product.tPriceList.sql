CREATE TABLE [product].[tPriceList]
(
[PriceListId] [int] NOT NULL IDENTITY(1, 1),
[CurrencyId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StartDateTime] [datetime] NULL,
[EndDateTime] [datetime] NULL,
[PriceListFolderId] [int] NOT NULL,
[PriceListStatusId] [int] NOT NULL,
[PriceListRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceList] ADD CONSTRAINT [PK_tPriceList] PRIMARY KEY CLUSTERED  ([PriceListId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPriceList_CommonName] ON [product].[tPriceList] ([CommonName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPriceList_CurrencyId] ON [product].[tPriceList] ([CurrencyId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPriceList_PriceListFolderId] ON [product].[tPriceList] ([PriceListFolderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPriceList_PriceListRegistryId] ON [product].[tPriceList] ([PriceListRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPriceList_PriceListStatusId] ON [product].[tPriceList] ([PriceListStatusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPriceList_Title] ON [product].[tPriceList] ([Title]) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceList] ADD CONSTRAINT [FK_tPriceList_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [product].[tPriceList] ADD CONSTRAINT [FK_tPriceList_tPriceListFolder] FOREIGN KEY ([PriceListFolderId]) REFERENCES [product].[tPriceListFolder] ([PriceListFolderId])
GO
ALTER TABLE [product].[tPriceList] ADD CONSTRAINT [FK_tPriceList_tPriceListRegistry] FOREIGN KEY ([PriceListRegistryId]) REFERENCES [product].[tPriceListRegistry] ([PriceListRegistryId])
GO
ALTER TABLE [product].[tPriceList] ADD CONSTRAINT [FK_tPriceList_tPriceListStatus] FOREIGN KEY ([PriceListStatusId]) REFERENCES [product].[tPriceListStatus] ([PriceListStatusId])
GO
