CREATE TABLE [integration].[tempProductNumberInStockDayBeforeNews]
(
[ProductId] [int] NULL,
[NumberInStockDayBefore] [int] NULL,
[NumberInStock] [int] NULL
) ON [PRIMARY]
GO
