CREATE TABLE [lekmer].[tVoucherActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherActionCurrency] ADD CONSTRAINT [PK_tVoucherActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherActionCurrency] ADD CONSTRAINT [FK_tVoucherActionCurrency_tVoucherAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tVoucherAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tVoucherActionCurrency] ADD CONSTRAINT [FK_tVoucherActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
