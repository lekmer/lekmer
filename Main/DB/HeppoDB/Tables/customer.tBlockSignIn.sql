CREATE TABLE [customer].[tBlockSignIn]
(
[BlockId] [int] NOT NULL,
[RedirectContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tBlockSignIn] ADD CONSTRAINT [PK_tBlockSignIn] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockSignIn_RedirectContentNodeId] ON [customer].[tBlockSignIn] ([RedirectContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tBlockSignIn] ADD CONSTRAINT [FK_tBlockSignIn_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [customer].[tBlockSignIn] ADD CONSTRAINT [FK_tBlockSignIn_tContentNode] FOREIGN KEY ([RedirectContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
