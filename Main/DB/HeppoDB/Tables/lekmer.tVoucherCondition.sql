CREATE TABLE [lekmer].[tVoucherCondition]
(
[ConditionId] [int] NOT NULL,
[IncludeAllBatchIds] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherCondition] ADD CONSTRAINT [PK_tVoucherCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherCondition] ADD CONSTRAINT [FK_tVoucherCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
