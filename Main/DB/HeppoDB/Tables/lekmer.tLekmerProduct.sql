CREATE TABLE [lekmer].[tLekmerProduct]
(
[ProductId] [int] NOT NULL,
[BrandId] [int] NULL,
[IsBookable] [bit] NOT NULL,
[AgeFromMonth] [int] NOT NULL,
[AgeToMonth] [int] NOT NULL,
[IsNewFrom] [datetime] NULL,
[IsNewTo] [datetime] NULL,
[Measurement] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[BatteryTypeId] [int] NULL,
[NumberOfBatteries] [int] NULL,
[IsBatteryIncluded] [bit] NOT NULL,
[ExpectedBackInStock] [datetime] NULL,
[HYErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_tLekmerProduct_CreatedDate] DEFAULT (getdate()),
[SizeDeviationId] [int] NULL,
[LekmerErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ShowVariantRelations] [bit] NOT NULL CONSTRAINT [DF_tLekmerProduct_ShowVariantRelations] DEFAULT ((0)),
[Weight] [decimal] (18, 3) NULL,
[ProductTypeId] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [lekmer].[tLekmerProduct] ADD 
CONSTRAINT [PK_tLekmerProduct] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tLekmerProduct_BrandId] ON [lekmer].[tLekmerProduct] ([BrandId]) ON [PRIMARY]

CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct_HYErpId] ON [lekmer].[tLekmerProduct] ([HYErpId]) ON [PRIMARY]

ALTER TABLE [lekmer].[tLekmerProduct] ADD
CONSTRAINT [FK_tLekmerProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD
CONSTRAINT [FK_tLekmerProduct_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD
CONSTRAINT [FK_tLekmerProduct_tBatteryType] FOREIGN KEY ([BatteryTypeId]) REFERENCES [lekmer].[tBatteryType] ([BatteryTypeId]) ON DELETE SET NULL
ALTER TABLE [lekmer].[tLekmerProduct] ADD
CONSTRAINT [FK_tLekmerProduct_tSizeDeviation] FOREIGN KEY ([SizeDeviationId]) REFERENCES [lekmer].[tSizeDeviation] ([SizeDeviationId])
ALTER TABLE [lekmer].[tLekmerProduct] ADD
CONSTRAINT [FK_tLekmerProduct_tProductType] FOREIGN KEY ([ProductTypeId]) REFERENCES [productlek].[tProductType] ([ProductTypeId])




GO
