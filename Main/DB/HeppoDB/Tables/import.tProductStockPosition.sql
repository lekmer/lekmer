CREATE TABLE [import].[tProductStockPosition]
(
[HYErpIdSize] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Lagerplatstyp] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[LagerplatstypBenamning] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[Lagerplats] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [import].[tProductStockPosition] ADD CONSTRAINT [PK__tProduct__82246DB012B44A1E] PRIMARY KEY CLUSTERED  ([HYErpIdSize], [Lagerplats]) ON [PRIMARY]
GO
