CREATE TABLE [campaignlek].[tCampaignActionConfigurator]
(
[CampaignActionConfiguratorId] [int] NOT NULL IDENTITY(1, 1),
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tCampaignActionConfigurator_IncludeAllProducts] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionConfigurator] ADD CONSTRAINT [PK_tCampaignActionConfigurator] PRIMARY KEY CLUSTERED  ([CampaignActionConfiguratorId]) ON [PRIMARY]
GO
