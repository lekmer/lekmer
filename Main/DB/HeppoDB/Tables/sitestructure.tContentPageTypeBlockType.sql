CREATE TABLE [sitestructure].[tContentPageTypeBlockType]
(
[ContentPageTypeId] [int] NOT NULL,
[BlockTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageTypeBlockType] ADD CONSTRAINT [PK_tContentPageTypeBlockType] PRIMARY KEY CLUSTERED  ([ContentPageTypeId], [BlockTypeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageTypeBlockType] ADD CONSTRAINT [FK_tContentPageTypeBlockType_tBlockType] FOREIGN KEY ([BlockTypeId]) REFERENCES [sitestructure].[tBlockType] ([BlockTypeId])
GO
ALTER TABLE [sitestructure].[tContentPageTypeBlockType] ADD CONSTRAINT [FK_tContentPageTypeBlockType_tContentPageType] FOREIGN KEY ([ContentPageTypeId]) REFERENCES [sitestructure].[tContentPageType] ([ContentPageTypeId])
GO
