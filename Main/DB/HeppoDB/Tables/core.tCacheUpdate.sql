CREATE TABLE [core].[tCacheUpdate]
(
[ManagerName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UpdateType] [int] NOT NULL,
[Key] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[CreationDate] [datetime] NOT NULL CONSTRAINT [DF_tCacheUpdate_CreationDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
