CREATE TABLE [product].[tBlockProductListProduct]
(
[BlockId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductListProduct] ADD CONSTRAINT [PK_tBlockProductListProduct] PRIMARY KEY CLUSTERED  ([BlockId], [ProductId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductListProduct_BlockId] ON [product].[tBlockProductListProduct] ([BlockId]) INCLUDE ([Ordinal], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductListProduct] ADD CONSTRAINT [FK_tBlockProductListProduct_tBlockProductList] FOREIGN KEY ([BlockId]) REFERENCES [product].[tBlockProductList] ([BlockId]) ON DELETE CASCADE
GO
ALTER TABLE [product].[tBlockProductListProduct] ADD CONSTRAINT [FK_tBlockProductListProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
