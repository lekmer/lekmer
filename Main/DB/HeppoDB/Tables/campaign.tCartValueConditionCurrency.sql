CREATE TABLE [campaign].[tCartValueConditionCurrency]
(
[ConditionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartValueConditionCurrency] ADD CONSTRAINT [PK_tCartValueConditionCurrency] PRIMARY KEY CLUSTERED  ([ConditionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartValueConditionCurrency] ADD CONSTRAINT [FK_tCartValueConditionCurrency_tCartValueCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCartValueCondition] ([ConditionId])
GO
ALTER TABLE [campaign].[tCartValueConditionCurrency] ADD CONSTRAINT [FK_tCartValueConditionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
