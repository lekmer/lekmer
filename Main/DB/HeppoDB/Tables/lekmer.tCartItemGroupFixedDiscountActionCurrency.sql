CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[MonetaryValue] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionCurrency_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionCurrency] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
