CREATE TABLE [core].[tLanguage]
(
[LanguageId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (64) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ISO] [char] (2) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [core].[tLanguage] ADD CONSTRAINT [PK_tLanguage] PRIMARY KEY CLUSTERED  ([LanguageId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Language_ISO] ON [core].[tLanguage] ([ISO]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Languages used on the site.', 'SCHEMA', N'core', 'TABLE', N'tLanguage', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'2 letter country code (ISO 639-1)', 'SCHEMA', N'core', 'TABLE', N'tLanguage', 'COLUMN', N'ISO'
GO
EXEC sp_addextendedproperty N'MS_Description', N'A unique ID for the languae created automatically by the database engine.', 'SCHEMA', N'core', 'TABLE', N'tLanguage', 'COLUMN', N'LanguageId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The name of the language in swedish, for example "Danska".', 'SCHEMA', N'core', 'TABLE', N'tLanguage', 'COLUMN', N'Title'
GO
