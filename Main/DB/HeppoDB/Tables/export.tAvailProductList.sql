CREATE TABLE [export].[tAvailProductList]
(
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tAvailProductList] ADD CONSTRAINT [PK_tAvailProductList] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
