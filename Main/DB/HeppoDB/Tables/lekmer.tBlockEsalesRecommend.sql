CREATE TABLE [lekmer].[tBlockEsalesRecommend]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[EsalesRecommendationTypeId] [int] NULL,
[PanelPath] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[FallbackPanelPath] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] ADD CONSTRAINT [PK_BlockEsalesRecommend] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] ADD CONSTRAINT [FK_BlockEsalesRecommend_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockEsalesRecommend] ADD CONSTRAINT [FK_tBlockEsalesRecommend_tEsalesRecommendationType] FOREIGN KEY ([EsalesRecommendationTypeId]) REFERENCES [lekmer].[tEsalesRecommendationType] ([EsalesRecommendationTypeId])
GO
