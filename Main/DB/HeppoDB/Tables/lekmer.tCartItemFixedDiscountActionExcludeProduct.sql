CREATE TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemFixedDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeProduct_tCartItemFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tCartItemFixedDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
