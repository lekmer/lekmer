CREATE TABLE [product].[tProductStatus]
(
[ProductStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductStatus] ADD CONSTRAINT [PK_tStatus] PRIMARY KEY CLUSTERED  ([ProductStatusId]) ON [PRIMARY]
GO
