IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'scensum')
CREATE LOGIN [scensum] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [scensum] FOR LOGIN [scensum]
GO
GRANT CONNECT TO [scensum]
