SET IDENTITY_INSERT [order].[tPaymentType] ON
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1, N'CreditCard', N'Kreditkort', N'26')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000002, N'Invoice', N'Faktura ', N'27')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000003, N'KlarnaPartpayment', N'Delbetala (Klarna)', N'28')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000004, N'MoneybookersCarteBlue', N'Moneybookers Carte Blue', N'29')
INSERT INTO [order].[tPaymentType] ([PaymentTypeId], [CommonName], [Title], [ErpId]) VALUES (1000005, N'MoneybookersIDeal', N'Moneybookers iDeal', N'29')
SET IDENTITY_INSERT [order].[tPaymentType] OFF
