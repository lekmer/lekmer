SET IDENTITY_INSERT [integration].[tCdonExportStatus] ON
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (1, N'InProgress', N'InProgress')
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (2, N'Finished', N'Finished')
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (3, N'Failed', N'Failed')
INSERT INTO [integration].[tCdonExportStatus] ([CdonExportStatusId], [CommonName], [Title]) VALUES (4, N'InvalidXml', N'InvalidXml')
SET IDENTITY_INSERT [integration].[tCdonExportStatus] OFF
