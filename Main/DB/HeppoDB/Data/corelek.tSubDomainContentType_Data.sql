INSERT INTO [corelek].[tSubDomainContentType] ([SubDomainContentTypeId], [Title], [CommonName]) VALUES (1, N'StaticMedia', N'StaticMedia')
INSERT INTO [corelek].[tSubDomainContentType] ([SubDomainContentTypeId], [Title], [CommonName]) VALUES (2, N'MediaArchiveWeb', N'MediaArchiveWeb')
INSERT INTO [corelek].[tSubDomainContentType] ([SubDomainContentTypeId], [Title], [CommonName]) VALUES (3, N'MediaArchiveBackOffice', N'MediaArchiveBackOffice')
INSERT INTO [corelek].[tSubDomainContentType] ([SubDomainContentTypeId], [Title], [CommonName]) VALUES (4, N'MediaArchiveExternal', N'MediaArchiveExternal')
