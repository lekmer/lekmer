SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomModelSetting]
as
	select
		*
	from
		[template].[vModelSetting]
GO
