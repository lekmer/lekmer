SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vProductDiscountActionItem]
as
	select
		p.ProductActionId AS 'ProductDiscountActionItem.ActionId',
		p.ProductId AS 'ProductDiscountActionItem.ProductId',
		p.CurrencyId AS 'ProductDiscountActionItem.CurrencyId',
		p.DiscountPrice AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		lekmer.tProductDiscountActionItem p
		inner join core.vCustomCurrency c on p.CurrencyId = c.[Currency.Id]
GO
