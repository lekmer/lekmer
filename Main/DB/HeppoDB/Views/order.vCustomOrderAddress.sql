
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vCustomOrderAddress]
AS
	SELECT
		oa.*,
		loa.[OrderAddress.HouseNumber],
		loa.[OrderAddress.HouseExtension],
		loa.[OrderAddress.Reference]
	FROM
		[order].[vOrderAddress] oa
		LEFT OUTER JOIN [orderlek].[vOrderAddress] loa
			ON loa.[OrderAddress.OrderAddressId] = oa.[OrderAddress.OrderAddressId]

GO
