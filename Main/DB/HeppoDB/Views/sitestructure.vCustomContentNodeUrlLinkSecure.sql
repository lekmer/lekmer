SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeUrlLinkSecure]
as
	select
		*
	from
		[sitestructure].[vContentNodeUrlLinkSecure]
GO
