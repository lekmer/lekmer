SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vProductSeoSetting]
AS
SELECT
    PSS.[ProductId] 'SeoSetting.ProductId',
	COALESCE(PSST.[Title],PSS.[Title]) 'SeoSetting.Title',
	COALESCE(PSST.[Description],PSS.[Description]) 'SeoSetting.Description',
	COALESCE(PSST.[Keywords],PSS.[Keywords]) 'SeoSetting.Keywords',
	L.LanguageId 'SeoSetting.LanguageId'
FROM
	product.tProductSeoSetting AS PSS
	CROSS JOIN core.tLanguage AS L
	LEFT JOIN product.tProductSeoSettingTranslation AS PSST ON PSST.[ProductId] = PSS.[ProductId] AND PSST.LanguageId = L.LanguageId
GO
