SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vBlockProductImageList]
AS
	SELECT 
		bpil.BlockId 'BlockProductImageList.BlockId',
		bpil.ColumnCount 'BlockProductImageList.ColumnCount',
		bpil.[RowCount] 'BlockProductImageList.RowCount',
		bpil.ProductImageGroupId 'BlockProductImageList.ProductImageGroupId'
	FROM 
		product.tBlockProductImageList bpil
GO
