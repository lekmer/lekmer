SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [esales].[vAdFolder]
AS
	SELECT
		af.[AdFolderId]		'AdFolder.AdFolderId',
		af.[ParentFolderId]	'AdFolder.ParentFolderId',
		af.[Title]			'AdFolder.Title'
	FROM 
		[esales].[tAdFolder] af
GO
