SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentNodeUrlLink]
AS
	select 
		cn.*,
		cnu.[LinkUrl] 'ContentNodeUrlLink.LinkUrl'
	from 
		[sitestructure].[tContentNodeUrlLink] as cnu
		inner join [sitestructure].[vCustomContentNode] as cn on cnu.[ContentNodeId] = cn.[ContentNode.ContentNodeId]

GO
