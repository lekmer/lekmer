SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vCartItemsGroupValueCondition]
AS
	SELECT
		cigvc.[ConditionId] AS 'CartItemsGroupValueCondition.ConditionId',
		cac.[IncludeAllProducts] AS 'CartItemsGroupValueCondition.IncludeAllProducts',
		cigvc.[ConfigId] AS 'CartItemsGroupValueCondition.ConfigId',
		cc.*
	FROM
		[campaignlek].[tCartItemsGroupValueCondition] cigvc
		INNER JOIN [campaign].[vCustomCondition] cc ON cc.[Condition.Id] = cigvc.[ConditionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = cigvc.[ConfigId]
GO
