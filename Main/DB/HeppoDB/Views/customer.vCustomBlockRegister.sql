SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomBlockRegister]
as
	select
		*
	from
		[customer].[vBlockRegister]
GO
