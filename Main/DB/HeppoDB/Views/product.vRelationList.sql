SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vRelationList]
AS
SELECT     
	RL.RelationListId 'RelationList.Id',
	RL.Title 'RelationList.Title',
	RL.RelationListTypeId 'RelationList.TypeId',
	RLT.CommonName 'RelationList.CommonName'
FROM
	[product].[tRelationList] as RL
	INNER JOIN [product].[tRelationListType] as RLT ON  RL.RelationListTypeId = RLT.RelationListTypeId
GO
