SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomFunctionType]
as
	select
		*
	from
		[template].[vFunctionType]
GO
