SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vRatingGroupRating]
AS
	SELECT
		rgr.[RatingGroupId]	'RatingGroupRating.RatingGroupId',
		rgr.[RatingId]		'RatingGroupRating.RatingId',
		rgr.[Ordinal]		'RatingGroupRating.Ordinal'
	FROM 
		[review].[tRatingGroupRating] rgr

GO
