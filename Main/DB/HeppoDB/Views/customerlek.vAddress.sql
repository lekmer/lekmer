
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [customerlek].[vAddress]
AS
	SELECT
		AddressId AS 'Address.AddressId',
		CustomerId AS 'Address.CustomerId',
		HouseNumber AS 'Address.HouseNumber',
		HouseExtension AS 'Address.HouseExtension',
		Reference AS 'Address.Reference'
	FROM
		[customerlek].[tAddress]

GO
