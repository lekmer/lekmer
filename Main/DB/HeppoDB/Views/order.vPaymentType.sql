SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vPaymentType]
AS
SELECT
	pt.PaymentTypeId AS 'PaymentType.PaymentTypeId',
	pt.CommonName AS 'PaymentType.CommonName',
	pt.ErpId AS 'PaymentType.ErpId',
	pt.Title AS 'PaymentType.Title',
	cpt.ChannelId AS 'PaymentType.ChannelId'
FROM 
	[order].[tPaymentType] pt
	INNER JOIN [order].[tChannelPaymentType] cpt
		ON cpt.PaymentTypeId = pt.PaymentTypeId


GO
