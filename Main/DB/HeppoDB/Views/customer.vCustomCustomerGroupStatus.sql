SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomerGroupStatus]
as
	select
		*
	from
		[customer].[vCustomerGroupStatus]
GO
