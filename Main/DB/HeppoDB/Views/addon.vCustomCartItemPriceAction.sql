SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vCustomCartItemPriceAction]
AS 
	SELECT 
		*
	FROM 
		addon.vCartItemPriceAction

GO
