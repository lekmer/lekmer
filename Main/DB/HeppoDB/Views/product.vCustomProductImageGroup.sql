SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductImageGroup]
as
	select
		*
	from
		[product].[vProductImageGroup]
GO
