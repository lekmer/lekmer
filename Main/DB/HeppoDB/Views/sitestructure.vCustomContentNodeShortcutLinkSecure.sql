SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeShortcutLinkSecure]
as
	select
		*
	from
		[sitestructure].[vContentNodeShortcutLinkSecure]
GO
