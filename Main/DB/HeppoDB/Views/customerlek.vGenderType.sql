SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [customerlek].[vGenderType]
AS
	SELECT     
		GT.GenderTypeId AS 'GenderType.GenderTypeId',
		GT.ErpId AS 'GenderType.ErpId',
		GT.Title AS 'GenderType.Title',
		GT.CommonName AS 'GenderType.CommonName'
	FROM
		[customerlek].[tGenderType] as GT
GO
