SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBlockBrandListBrand]
AS
SELECT 
	bb.[BlockId] AS 'BlockBrandListBrand.BlockId',
	bb.[BrandId] AS 'BlockBrandListBrand.BrandId',
	bb.[Ordinal] AS 'BlockBrandListBrand.Ordinal',
	b.[Title] AS 'Brand.Title'
FROM 
	[lekmer].[tBlockBrandListBrand] bb
	INNER JOIN [lekmer].[tBrand] b ON bb.BrandId = b.BrandId
GO
