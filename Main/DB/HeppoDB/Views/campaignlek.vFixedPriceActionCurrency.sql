SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [campaignlek].[vFixedPriceActionCurrency]
AS
	SELECT
		fdac.[ProductActionId] AS 'FixedPriceActionCurrency.ProductActionId',
		fdac.[CurrencyId] AS 'FixedPriceActionCurrency.CurrencyId',
		fdac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tFixedPriceActionCurrency] fdac
		INNER JOIN [core].[vCustomCurrency] c ON fdac.[CurrencyId] = c.[Currency.Id]




GO
