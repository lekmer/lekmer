SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [lekmer].[vAgeIntervalSecure]
AS
SELECT     
      AgeIntervalId AS 'AgeInterval.AgeIntervalId', 
      FromMonth AS 'AgeInterval.FromMonth', 
      ToMonth AS 'AgeInterval.ToMonth', 
      Ordinal AS 'AgeInterval.Ordinal', 
      Title AS 'AgeInterval.Title',
      NULL AS 'AgeInterval.ContentNodeId'
FROM
      [lekmer].[tAgeInterval]
GO
