
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vPercentagePriceDiscountAction]
AS
	SELECT
		ppda.[ProductActionId] AS 'PercentagePriceDiscountAction.ProductActionId',
		cac.[IncludeAllProducts] AS 'PercentagePriceDiscountAction.IncludeAllProducts',
		ppda.[DiscountAmount] AS 'PercentagePriceDiscountAction.DiscountAmount',
		ppda.[ConfigId] AS 'PercentagePriceDiscountAction.ConfigId',
		pa.*
	FROM
		[campaign].[tPercentagePriceDiscountAction] ppda
		INNER JOIN [campaign].[vCustomProductAction] pa on pa.[ProductAction.Id] = ppda.[ProductActionId]
		INNER JOIN [campaignlek].[tCampaignActionConfigurator] cac ON cac.[CampaignActionConfiguratorId] = ppda.[ConfigId]
GO
