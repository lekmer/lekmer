SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [orderlek].[vCartItemPackageElement]
AS
SELECT
	[cipe].[CartItemPackageElementId] AS 'CartItemPackageElement.CartItemPackageElementId',
	[cipe].[CartItemId] AS 'CartItemPackageElement.CartItemId',
	[cipe].[ProductId] AS 'CartItemPackageElement.ProductId',
	[cipe].[SizeId] AS 'CartItemPackageElement.SizeId',
	[cipe].[ErpId] AS 'CartItemPackageElement.ErpId'
FROM
	[orderlek].[tCartItemPackageElement] cipe

GO
