SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vCartItemFixedDiscountAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemFixedDiscountAction.CartActionId',
		cid.[IncludeAllProducts]		AS 'CartItemFixedDiscountAction.IncludeAllProducts',
		ca.*
	FROM
		[lekmer].[tCartItemFixedDiscountAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]



GO
