SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vOrderModuleChannel]
AS
SELECT 
	 [ChannelId] AS 'OrderModuleChannel.ChannelId'
	,[OrderTemplateContentNodeId] AS 'OrderModuleChannel.OrderTemplateContentNodeId'
FROM
	[order].[tOrderModuleChannel]
GO
