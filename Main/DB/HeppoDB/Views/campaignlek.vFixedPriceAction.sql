SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vFixedPriceAction]
AS
	SELECT
		fda.[ProductActionId] AS 'FixedPriceAction.ProductActionId',
		fda.[IncludeAllProducts] AS 'FixedPriceAction.IncludeAllProducts',
		pa.*
	FROM
		[campaignlek].[tFixedPriceAction] fda
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = fda.[ProductActionId]

GO
