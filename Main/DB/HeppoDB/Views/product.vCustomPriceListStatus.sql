SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomPriceListStatus]
as
	select
		*
	from
		[product].[vPriceListStatus]
GO
