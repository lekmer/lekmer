SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [addon].[vBlockTopList]
as
	select
		[BlockId] as 'BlockTopList.BlockId',
		[ColumnCount] as 'BlockTopList.ColumnCount',
		[RowCount] as 'BlockTopList.RowCount',
		[TotalProductCount] as 'BlockTopList.TotalProductCount',
		[IncludeAllCategories] as 'BlockTopList.IncludeAllCategories',
		[OrderStatisticsDayCount] as 'BlockTopList.OrderStatisticsDayCount'
	from
		addon.tBlockTopList



GO
