SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [template].[vCustomAliasSecure]
as
	select
		*
	from
		[template].[vAliasSecure]
GO
