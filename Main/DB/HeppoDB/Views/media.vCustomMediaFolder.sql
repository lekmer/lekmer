SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [media].[vCustomMediaFolder]
as
	select
		*
	from
		[media].[vMediaFolder]
GO
