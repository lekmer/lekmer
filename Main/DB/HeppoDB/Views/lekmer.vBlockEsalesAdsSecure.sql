SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBlockEsalesAdsSecure]
AS
	SELECT
		b.*,
		bea.[ColumnCount] AS 'BlockEsalesAds.ColumnCount',
		bea.[RowCount] AS 'BlockEsalesAds.RowCount',
		bea.[PanelPath] AS 'BlockEsalesAds.PanelPath',
		bea.[Format] AS 'BlockEsalesAds.Format',
		bea.[Gender] AS 'BlockEsalesAds.Gender',
		bea.[ProductCategory] AS 'BlockEsalesAds.ProductCategory'
	FROM
		[lekmer].[tBlockEsalesAds] bea
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON b.[Block.BlockId] = bea.[BlockId]
GO
