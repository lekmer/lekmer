SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCartValueConditionCurrency]
as
	select
		cv.ConditionId AS 'CartValueConditionCurrency.ConditionId',
		cv.CurrencyId AS 'CartValueConditionCurrency.CurrencyId',
		cv.MonetaryValue as 'CurrencyValue.MonetaryValue',
		c.*
	from
		campaign.tCartValueConditionCurrency cv
		inner join core.vCustomCurrency c on cv.CurrencyId = c.[Currency.Id]

GO
