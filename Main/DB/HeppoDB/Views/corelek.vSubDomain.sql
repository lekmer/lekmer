SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [corelek].[vSubDomain]
AS
SELECT
	[sb].[SubDomainId] AS 'SubDomain.SubDomainId',
	[sb].[CommonName] AS 'SubDomain.CommonName',
	[sb].[DomainUrl] AS 'SubDomain.DomainUrl',
	[sb].[UrlTypeId] AS 'SubDomain.UrlTypeId',
	[sb].[ContentTypeId] AS 'SubDomain.ContentTypeId'
FROM 
	[corelek].[tSubDomain] sb


GO
