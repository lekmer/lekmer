SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vCustomerGroupCondition]
AS
	SELECT
		c.*
	FROM
		[campaignlek].[tCustomerGroupCondition] cgc
		INNER JOIN [campaign].[vCustomCondition] c ON c.[Condition.Id] = cgc.[ConditionId]
GO
