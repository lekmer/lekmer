
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vTag]
AS
SELECT     
      t.TagId AS 'Tag.TagId', 
      t.TagGroupId AS 'Tag.TagGroupId', 
      COALESCE (tt.Value, t.Value) AS 'Tag.Value',
      t.CommonName AS 'Tag.CommonName',
      l.LanguageId AS 'Tag.LanguageId'
FROM
      [lekmer].[tTag] AS t
      CROSS JOIN core.tLanguage AS l
      LEFT JOIN [lekmer].tTagTranslation AS tt ON tt.TagId = t.TagId AND tt.LanguageId = l.LanguageId
GO
