SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vNewsletterSubscriber]
AS
SELECT
	[SubscriberId] AS 'NewsletterSubscriber.SubscriberId',
	[ChannelId] AS 'NewsletterSubscriber.ChannelId',
	[Email] AS 'NewsletterSubscriber.Email',
	[SubscriberTypeId] AS 'NewsletterSubscriber.SubscriberTypeId',
	[SentStatus] AS 'NewsletterSubscriber.SentStatus',
	[CreatedDate] AS 'NewsletterSubscriber.CreatedDate',
	[UpdatedDate] AS 'NewsletterSubscriber.UpdatedDate'
FROM
	[lekmer].[tNewsletterSubscriber]
GO
