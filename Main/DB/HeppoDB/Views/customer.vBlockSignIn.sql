SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  VIEW [customer].[vBlockSignIn]
AS
SELECT   
       [BlockId] AS 'BlockSignIn.BlockId'
      ,[RedirectContentNodeId] AS 'BlockSignIn.RedirectContentNodeId'
  FROM [customer].[tBlockSignIn]
GO
