SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vCustomProductSimilar]
AS
	SELECT
		*
	FROM
		[lekmer].[vProductSimilar]
GO
