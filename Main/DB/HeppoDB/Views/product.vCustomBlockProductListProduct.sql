SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockProductListProduct]
as
	select
		*
	from
		[product].[vBlockProductListProduct]
GO
