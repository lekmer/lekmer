SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vContentNodeType]
AS
SELECT 
	[ContentNodeTypeId] AS 'ContentNodeType.ContentNodeTypeId',
	[Title] AS 'ContentNodeType.Title', 
	[CommonName] AS 'ContentNodeType.CommonName'
FROM 
	[sitestructure].[tContentNodeType]
GO
