SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vRelationListType]
AS
SELECT     
	RLT.RelationListTypeId 'RelationListType.Id',
	RLT.CommonName 'RelationListType.CommonName',
	RLT.Description 'RelationListType.Description'
FROM
	product.tRelationListType RLT
GO
