
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vRelationListProduct]
AS
SELECT      
       rlp.RelationListId 'RelationListProduct.RelationListId',
       p.*
       
FROM       
       [product].[tRelationListProduct] rlp       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = rlp.ProductId



GO
