SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vCustomBlockProductSimilarList]
AS
	SELECT
		*
	FROM
		[lekmer].[vBlockProductSimilarList]
GO
