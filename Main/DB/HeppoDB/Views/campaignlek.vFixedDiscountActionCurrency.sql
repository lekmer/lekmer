
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vFixedDiscountActionCurrency]
AS
	SELECT
		fdac.[ProductActionId] AS 'FixedDiscountActionCurrency.ProductActionId',
		fdac.[CurrencyId] AS 'FixedDiscountActionCurrency.CurrencyId',
		fdac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tFixedDiscountActionCurrency] fdac
		INNER JOIN [core].[vCustomCurrency] c ON fdac.[CurrencyId] = c.[Currency.Id]




GO
