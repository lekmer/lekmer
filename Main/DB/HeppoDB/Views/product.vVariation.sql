SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vVariation]
AS
SELECT 
	   v.[VariationGroupId] 'Variation.GroupId',
	   v.[VariationId] 'Variation.Id',
	   VT.[VariationType.Id] 'Variation.TypeId',
	   v.[Ordinal] 'Variation.Ordinal',
      COALESCE (transl.[Value],v.[Value]) AS 'Variation.Value'
	  ,l.LanguageId 'Variation.LanguageId'
	  ,vt.[VariationType.Title] AS 'Variation.TypeName'
FROM 
	 [product].[tVariation] AS v
	 CROSS JOIN core.tLanguage AS l
	 LEFT JOIN product.tVariationTranslation AS transl ON transl.VariationId = v.VariationId AND transl.LanguageId = l.LanguageId
	 INNER JOIN [product].vCustomVariationType VT ON VT.[VariationType.Id] = v.VariationTypeId AND  VT.[VariationType.LanguageId] = l.LanguageId

GO
