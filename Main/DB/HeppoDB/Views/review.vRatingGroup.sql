
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vRatingGroup]
AS
	SELECT
		rg.[RatingGroupId]			'RatingGroup.RatingGroupId',
		rg.[RatingGroupFolderId]	'RatingGroup.RatingGroupFolderId',
		rg.[CommonName]				'RatingGroup.CommonName',
		rg.[Title]					'RatingGroup.Title',
		rg.[Description]			'RatingGroup.Description'
	FROM 
		[review].[tRatingGroup] rg
GO
