SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vCart]
AS
select	
	CartId AS 'Cart.CartId',
	CartGuid AS 'Cart.CartGuid',
	CreatedDate AS 'Cart.CreatedDate'
from
	[order].tCart
GO
