SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [core].[vCustomCurrency]
as
	select
		*
	from
		[core].[vCurrency]
GO
