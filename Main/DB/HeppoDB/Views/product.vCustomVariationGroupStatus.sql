SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomVariationGroupStatus]
as
	select
		*
	from
		[product].[vVariationGroupStatus]
GO
