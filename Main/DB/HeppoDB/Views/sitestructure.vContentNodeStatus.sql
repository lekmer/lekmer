SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vContentNodeStatus]
AS
	select 
		[ContentNodeStatusId] 'ContentNodeStatus.Id',
		[Title] 'ContentNodeStatus.Title',
		[CommonName] 'ContentNodeStatus.CommonName',
		[Ordinal]  'ContentNodeStatus.Ordinal'
	from 
		[sitestructure].[tContentNodeStatus]

GO
