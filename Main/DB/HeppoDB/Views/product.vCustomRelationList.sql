SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomRelationList]
as
	select
		*
	from
		[product].[vRelationList]
GO
