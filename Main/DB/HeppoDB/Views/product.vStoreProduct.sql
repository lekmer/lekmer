SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vStoreProduct]
AS
SELECT 
	t.ProductId 'StoreProduct.ProductId',
	t.StoreId 'StoreProduct.StoreId',
	t.Quantity 'StoreProduct.Quantity',
	t.Threshold 'StoreProduct.Threshold'
FROM
	product.tStoreProduct t
GO
