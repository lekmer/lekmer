SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [order].[tOrderItemProduct]'
GO
ALTER TABLE [order].[tOrderItemProduct] ALTER COLUMN [EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [order].[pOrderItemSave]'
GO
ALTER PROCEDURE [order].[pOrderItemSave]
	@OrderItemId INT,
	@OrderId INT,
	@ProductId INT,
	@Quantity INT,
	@ActualPriceIncludingVat DECIMAL(16,2),
	@OriginalPriceIncludingVat DECIMAL(16,2),
	@VAT DECIMAL(16,2),
	@ErpId VARCHAR(50),
	@EanCode VARCHAR(20),
	@Title NVARCHAR(50),
	@OrderItemStatusId int = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[order].[tOrderItem]
	SET
		OrderId = @OrderId,
		Quantity = @Quantity,
		ActualPriceIncludingVat = @ActualPriceIncludingVat,
		OriginalPriceIncludingVat = @OriginalPriceIncludingVat,
		VAT = @VAT,
		OrderItemStatusId = @OrderItemStatusId
	WHERE
		OrderItemId = @OrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrderItem]
		(
			OrderId,
			Quantity,
			ActualPriceIncludingVat,
			OriginalPriceIncludingVat,
			VAT,
			OrderItemStatusId
		)
		VALUES
		(
			@OrderId,
			@Quantity,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @OrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [order].[tOrderItemProduct]
		(
			OrderItemId,
			ProductId,
			ErpId,
			EanCode,
			Title
		)
		VALUES
		(
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title
		)
	END
	
	RETURN @OrderItemId
END


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[tProduct]'
GO
ALTER TABLE [product].[tProduct] ALTER COLUMN [EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

PRINT N'Altering [product].[pProductSave]'
GO
alter procedure [product].[pProductSave]
	@ProductId			int,
	@ItemsInPackage		int,
	@ErpId				varchar(50),
	@EANCode			varchar(20),
	@Title				nvarchar(256),
	@WebShopTitle		nvarchar(256),
	@ProductStatusId	int,
	@Description		nvarchar(max),
	@ShortDescription	nvarchar(max),
	@CategoryId			int,
	@MediaId			int	
as
begin
	set nocount on

	update
		[product].[tProduct]
	set
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	where
		[ProductId] = @ProductId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO


IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
PRINT N'Adding full text indexing to tables'
GO
