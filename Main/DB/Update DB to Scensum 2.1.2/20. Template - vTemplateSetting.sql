Alter VIEW [template].[vTemplateSetting]
AS

SELECT  T.[TemplateId] AS [TemplateSetting.TemplateId],
		MS.[ModelSetting.Id] AS [TemplateSetting.ModelSettingId],
		TS.[Value] AS [TemplateSetting.Value], 
        TS.[AlternateValue] AS [TemplateSetting.AlternateValue], 
        MS.[ModelSetting.CommonName],
		MS.[ModelSetting.Title],
		MS.[ModelSetting.TypeId],
		MST.[ModelSettingType.Title]

FROM	[template].[tTemplate] AS T
		INNER JOIN [template].[vCustomModelSetting] AS MS ON T.[ModelId] = MS.[ModelSetting.ModelId]
		INNER JOIN [template].[vCustomModelSettingType] AS MST ON MST.[ModelSettingType.Id] = MS.[ModelSetting.TypeId]
		LEFT JOIN [template].[tTemplateSetting] AS TS ON MS.[ModelSetting.Id] = TS.[ModelSettingId] AND T.TemplateId = TS.TemplateId

GO