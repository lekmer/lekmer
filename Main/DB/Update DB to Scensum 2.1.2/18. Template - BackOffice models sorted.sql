alter PROCEDURE [template].[pModelGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[ModelFolder.Id] * (-1),
			NULL,
			v.[ModelFolder.Title]
		FROM template.vCustomModelFolder AS v

	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[Model.Id],
					v.[Model.ModelFolderId]  * (-1),
					v.[Model.Title]
				FROM template.vCustomModel AS v
				WHERE v.[Model.ModelFolderId] = @SelectedId
		END

		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN Id > 0 THEN 0 ELSE (CASE WHEN EXISTS (SELECT 1 FROM template.vCustomModel AS v
				WHERE v.[Model.ModelFolderId] = -1 * Id) THEN 1 ELSE 0 END) END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY Title ASC
END

GO