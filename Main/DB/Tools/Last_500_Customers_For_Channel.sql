-- Last 500 customers for Lekmer

SELECT TOP (500)
	a1_1.CommonName AS 'Channel',
	a1_1.FirstName,
	a1_1.LastName,
	a0_Email.Email AS 'Email'
FROM
	(
		SELECT
			DISTINCT CI.Email AS 'Email'
		FROM
			customer.tCustomerInformation CI
		WHERE
			CI.Email IS NOT NULL
	) a0_Email
	CROSS APPLY
	(
		SELECT
			MAX(O.CreatedDate) AS 'CustomerLaterOrderDate'
		FROM
			[order].tOrder O
			INNER JOIN customer.tCustomerInformation CI ON CI.CustomerId = O.CustomerId
		WHERE
			CI.Email = a0_Email.Email
			AND
			O.ChannelId = 3
	) a1_O		
	CROSS APPLY
	(
		SELECT
			CI.FirstName,
			CI.LastName,
			C.CommonName
		FROM
			[order].tOrder O
			INNER JOIN customer.tCustomerInformation CI ON CI.CustomerId = O.CustomerId
			INNER JOIN core.tChannel c ON c.ChannelId = o.ChannelId
		WHERE
			CI.Email = a0_Email.Email
			AND
			O.CreatedDate = a1_O.CustomerLaterOrderDate
	) a1_1
ORDER BY a1_O.CustomerLaterOrderDate DESC