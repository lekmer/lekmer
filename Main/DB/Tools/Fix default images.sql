/*This script add product default image to tProductImage table, with group=1*/

BEGIN TRANSACTION

INSERT product.tProductImage(
	ProductId, 
	MediaId, 
	ProductImageGroupId, 
	Ordinal)
SELECT
	p.ProductId,
	p.MediaId,
	1,
	1
FROM
	product.tProduct p
WHERE
	p.MediaId IS NOT NULL
	AND 
	NOT EXISTS (
		SELECT 1 FROM product.tProductImage pim 
		WHERE 
			pim.ProductImageGroupId = 1
			AND
			pim.ProductId = p.ProductId
			--AND
			--pim.MediaId = p.MediaId
	)

ROLLBACK