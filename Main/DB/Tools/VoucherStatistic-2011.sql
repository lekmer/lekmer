DECLARE
	@From DATETIME,
	@To DATETIME,
	@Channel_SE INT,
	@Channel_NO INT,
	@Channel_DK INT,
	@Channel_FI INT
	
SELECT
	@From = '2011-01-01',
	@To = '2012-01-01',
	@Channel_SE = (SELECT c.ChannelId FROM Lekmer.core.tChannel c WHERE c.CommonName = 'Sweden'),
	@Channel_NO = (SELECT c.ChannelId FROM Lekmer.core.tChannel c WHERE c.CommonName = 'Norway'),
	@Channel_DK = (SELECT c.ChannelId FROM Lekmer.core.tChannel c WHERE c.CommonName = 'Denmark'),
	@Channel_FI = (SELECT c.ChannelId FROM Lekmer.core.tChannel c WHERE c.CommonName = 'Finland')
	
SELECT
	v.[VoucherCode] 'Voucher code',
	'=|=',
	ISNULL(vi.[DiscountTitle], '') AS 'Discount title',
	'=|=',
	vi.[Description] AS 'Voucher description',
	'=|=',
	v.[VoucherInfoId] AS 'Batch id',
	'=|=',
	vi.[DiscountValue] AS 'Discount value',
	'=|=',
	CASE
		WHEN vi.[DiscountTypeId] = 1 THEN '%'
		WHEN vi.[DiscountTypeId] = 2 THEN ':-'
		ELSE 'NA'
	END AS 'Discount type',
	'=|=',
	vi.[OriginalStartQuantityPerCode] AS 'No issued',
	'=|=',
	a0.[No used Log] AS 'No used',
	'=|=',
	v.[QuantityLeft] AS 'No left',
	'=|=',
	cg0.[Description] AS 'Channel group',
	'=|=',
	a1_DK.[No used Log] AS 'No .dk',
	'=|=',
	a1_FI.[No used Log] AS 'No .fi',
	'=|=',
	a1_NO.[No used Log] AS 'No .no',
	'=|=',
	a1_SE.[No used Log] AS 'No .se',
	'=|=',
	ISNULL(a2_DK.[Order AVG], '0') AS 'Average order value (excl VAT after discount) DK',
	'=|=',
	ISNULL(a2_FI.[Order AVG], '0') AS 'Average order value (excl VAT after discount) FI',
	'=|=',
	ISNULL(a2_NO.[Order AVG], '0') AS 'Average order value (excl VAT after discount) NO',
	'=|=',
	ISNULL(a2_SE.[Order AVG], '0') AS 'Average order value (excl VAT after discount) SE'
FROM
	[Voucher].[product].[tVoucher] v
	INNER JOIN [Voucher].[product].[tVoucherInfo] vi ON vi.[VoucherInfoId] = v.[VoucherInfoId]
	
	OUTER APPLY -- 'No of used from Log table'
	(
		SELECT TOP 1
			cg.[Description]
		FROM
			[Voucher].[product].[tChannelGroup] cg
		WHERE
			cg.[ChannelGroupId] = vi.[ChannelGroupId]
	) cg0
	
	OUTER APPLY -- 'No of used from Log table'
	(
		SELECT
			COUNT(1) AS 'No used Log'
		FROM
			[Voucher].[product].[tVoucherLog] vl
		WHERE
			vl.[VoucherId] = v.[VoucherId]
			AND
			vl.Used > @From
			AND
			vl.Used < @To
	) a0
	
	OUTER APPLY -- 'No of used for DK'
	(
		SELECT
			COUNT(1) AS 'No used Log'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_DK
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a1_DK
	OUTER APPLY -- 'No of used for FI'
	(
		SELECT
			COUNT(1) AS 'No used Log'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_FI
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a1_FI
	OUTER APPLY -- 'No of used for NO'
	(
		SELECT
			COUNT(1) AS 'No used Log'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_NO
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a1_NO
	OUTER APPLY -- 'No of used for SE'
	(
		SELECT
			COUNT(1) AS 'No used Log'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_SE
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a1_SE
	
	OUTER APPLY -- 'Average order value (excl VAT after discount) DK'
	(
		SELECT
			AVG(op.[Price] - op.[Vat]) AS 'Order AVG'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
			INNER JOIN [Lekmer].[order].[tOrderPayment] op ON op.[OrderId] = o.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_DK
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a2_DK
	OUTER APPLY -- 'Average order value (excl VAT after discount) FI'
	(
		SELECT
			AVG(op.[Price] - op.[Vat]) AS 'Order AVG'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
			INNER JOIN [Lekmer].[order].[tOrderPayment] op ON op.[OrderId] = o.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_FI
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a2_FI
	OUTER APPLY -- 'Average order value (excl VAT after discount) NO'
	(
		SELECT
			AVG(op.[Price] - op.[Vat]) AS 'Order AVG'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
			INNER JOIN [Lekmer].[order].[tOrderPayment] op ON op.[OrderId] = o.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_NO
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a2_NO
	OUTER APPLY -- 'Average order value (excl VAT after discount) SE'
	(
		SELECT
			AVG(op.[Price] - op.[Vat]) AS 'Order AVG'
		FROM
			[Voucher].[product].[tVoucherLog] vl
			INNER JOIN [Lekmer].[order].[tOrder] o ON o.[OrderId] = vl.[OrderId]
			INNER JOIN [Lekmer].[order].[tOrderPayment] op ON op.[OrderId] = o.[OrderId]
		WHERE
			VoucherId = v.VoucherId
			AND
			o.ChannelId = @Channel_SE
			AND
			o.CreatedDate > @From
			AND
			o.CreatedDate < @To
	) a2_SE
	
WHERE
	vi.ValidFrom < @To
	AND
	(vi.ValidTo > @From OR vi.ValidTo IS NULL)
	AND
	(DATEADD(day, vi.ValidInDays, vi.ValidFrom) > @From OR vi.ValidInDays IS NULL)
	AND
	(v.ValidTo > @From OR v.ValidTo IS NULL)
	AND
	cg0.[Description] like '%lekmer%'
	
	AND
	(
		a1_DK.[No used Log] > 0
		OR
		a1_FI.[No used Log] > 0
		OR
		a1_NO.[No used Log] > 0
		OR
		a1_SE.[No used Log] > 0
		OR
		a2_DK.[Order AVG] > 0
		OR
		a2_FI.[Order AVG] > 0
		OR
		a2_NO.[Order AVG] > 0
		OR
		a2_SE.[Order AVG] > 0
	)