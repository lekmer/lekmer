SELECT
	s0.CivicNumber,
	a2.Email
FROM
	(
	select
		DISTINCT
		REPLACE(REPLACE(ci.CivicNumber, ' ', ''), '-', '') AS 'CivicNumber'
	from
		[order].tOrder o
		inner join [customer].[tCustomerInformation] ci on ci.CustomerId = o.CustomerId
	where
		o.ChannelId = 1
		and
		(
		LEN(REPLACE(REPLACE(ci.CivicNumber, ' ', ''), '-', '')) = 12
		or
		LEN(REPLACE(REPLACE(ci.CivicNumber, ' ', ''), '-', '')) = 10
		)
	) s0
	outer apply
	(
		SELECT LEN(s0.CivicNumber) AS 'Len'
	) a0
	outer apply
	(
		SELECT
			CASE 
				WHEN a0.Len = 10 THEN s0.CivicNumber
				WHEN a0.Len = 12 THEN SUBSTRING(s0.CivicNumber, 3, 12)
				ELSE '-'
			END AS 'CivicNumberNormalized'
	) a1
	outer apply
	(
		select
			TOP 1
			o.Email AS 'Email'
		from
			[order].tOrder o
			inner join [customer].[tCustomerInformation] ci on ci.CustomerId = o.CustomerId
		where
			o.ChannelId = 1
			and
			REPLACE(REPLACE(ci.CivicNumber, ' ', ''), '-', '') = s0.CivicNumber
		order by
			o.CreatedDate desc
	) a2
ORDER BY
	a1.CivicNumberNormalized