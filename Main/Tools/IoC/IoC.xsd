<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="Windsor"
    targetNamespace="http://www.tigraine.at/windsor-configuration.xsd"
    elementFormDefault="qualified"
    xmlns="http://www.tigraine.at/windsor-configuration.xsd"
    xmlns:mstns="http://www.tigraine.at/windsor-configuration.xsd"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
>
  <xs:annotation>
    <xs:documentation>
      This schema was created by Daniel Hoelbling (http://www.tigraine.at) from the Windsor Documentation Reference
      http://www.castleproject.org/container/documentation/trunk/manual/windsorconfigref.html

      Copyright 2008 Daniel Hoelbling (http://www.tigraine.at)
      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0
    </xs:documentation>
  </xs:annotation>
  
  <xs:element name="configuration">
    <xs:complexType>
      <xs:sequence>
        
        <xs:element name="include" minOccurs="0" maxOccurs="unbounded">
          <xs:complexType>
            <xs:attribute name="uri" type="xs:string" use="required" />
          </xs:complexType>
        </xs:element>

        <xs:element name="bootstrap" minOccurs="0" maxOccurs="1">
          <xs:annotation>
            <xs:documentation>Used to register components before registering the facilities</xs:documentation>
          </xs:annotation>
          <xs:complexType>
            <xs:sequence>
              <xs:element name="component" type="component" minOccurs="0" maxOccurs="unbounded" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
        <xs:element name="components" minOccurs="0" maxOccurs="1">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="component" type="component" minOccurs="0" maxOccurs="unbounded" />      
            </xs:sequence>
          </xs:complexType>
        </xs:element>

        <xs:element name="facilities" minOccurs="0" maxOccurs="1">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="facility" type="facility" minOccurs="0" maxOccurs="unbounded" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
        
      </xs:sequence>
    </xs:complexType>
  </xs:element>

  <xs:complexType name="facility">
    <xs:sequence>
      <xs:any processContents="skip" minOccurs="0" maxOccurs="unbounded" />
    </xs:sequence>
    <xs:attribute name="id" type="xs:string" use="required" />
    <xs:attribute name="type" type="xs:string" />
  </xs:complexType>
  
  <xs:complexType name="component">
    <xs:sequence>
      <xs:element name="parameters" minOccurs="0" maxOccurs="1">
        <xs:annotation>
          <xs:documentation>Specify a list of parameters that the container can use when resolving dependencies. The child nodes should use properties names or constructors arguments names.</xs:documentation>
        </xs:annotation>
        <xs:complexType>
          <xs:sequence minOccurs="0" maxOccurs="unbounded">
            <xs:any processContents="skip"></xs:any>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
      
      <xs:element name="interceptors" minOccurs="0" maxOccurs="1">
        <xs:annotation>
          <xs:documentation>Specify a list of interceptors. Should use the reference notation.</xs:documentation>
        </xs:annotation>
        <xs:complexType>
          <xs:sequence minOccurs="0" maxOccurs="unbounded">
            <xs:element name="interceptor"></xs:element>
          </xs:sequence>
        </xs:complexType>
      </xs:element>
    </xs:sequence>

    <xs:attribute name="id" type="xs:string" use="required">
      <xs:annotation>
        <xs:documentation>The key that identifies a component or facility</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    
    
    <xs:attribute name="service" type="xs:string">
      <xs:annotation>
        <xs:documentation>The full type name of the service (interface) implemented by the component.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    
    <xs:attribute name="type" type="xs:string">
      <xs:annotation>
        <xs:documentation>The full type name of the facility or component implementation</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    
    <xs:attribute name="inspectionBehavior">
      <xs:annotation>
        <xs:documentation>Defines what properties the PropertiesDependenciesModelInspector  should extract from the component. Defaults to all</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="all" />
          <xs:enumeration value="declaredonly" />
          <xs:enumeration value="none" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>
    
    <xs:attribute name="lifestyle">
      <xs:annotation>
        <xs:documentation>Defines the component lifestyle.</xs:documentation>
      </xs:annotation>
      <xs:simpleType>
        <xs:restriction base="xs:string">
          <xs:enumeration value="singleton" />
          <xs:enumeration value="thread" />
          <xs:enumeration value="transient" />
          <xs:enumeration value="pooled" />
          <xs:enumeration value="custom" />
        </xs:restriction>
      </xs:simpleType>
    </xs:attribute>

    <xs:attribute name="customLifestyleType" type="xs:string">
      <xs:annotation>
        <xs:documentation>Only required if the lifestyle=custom. Specify a custom implementation of ILifestyleManager</xs:documentation>
      </xs:annotation>
    </xs:attribute>

    <xs:attribute name="initialPoolSize" type="xs:int">
      <xs:annotation>
        <xs:documentation>Only required if the lifestyle=pooled. Defines the initial pool size</xs:documentation>
      </xs:annotation>
    </xs:attribute>
    
    <xs:attribute name="maxPoolSize" type="xs:int">
      <xs:annotation>
        <xs:documentation>Only required if the lifestyle=pooled. Defines the pool maximum size.</xs:documentation>
      </xs:annotation>
    </xs:attribute>
  </xs:complexType>
</xs:schema>
