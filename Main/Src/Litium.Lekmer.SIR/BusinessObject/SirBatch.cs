using System;

namespace Litium.Lekmer.SIR
{
	[Serializable]
	public class SirBatch : ISirBatch
	{
		public int Id { get; set; }
		public DateTime? ChangesFromDate { get; set; }
		public DateTime StartedDate { get; set; }
		public DateTime? CompletedDate { get; set; }
	}
}