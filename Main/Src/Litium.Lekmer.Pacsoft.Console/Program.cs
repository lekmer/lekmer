﻿using System;
using System.Diagnostics;
using System.Reflection;
using Litium.Lekmer.Pacsoft.Contract;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Pacsoft.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.InfoFormat("Pacsoft.Console started.");

				var exporter = IoC.Resolve<IExporter>("PacsoftExporter");
				exporter.Execute();
				_log.InfoFormat("Pacsoft.Console completed.");

				stopwatch.Stop();
				_log.InfoFormat("Pacsoft.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch (Exception ex)
			{
				_log.Error("Pacsoft.Console failed.", ex);
			}
		}
	}
}