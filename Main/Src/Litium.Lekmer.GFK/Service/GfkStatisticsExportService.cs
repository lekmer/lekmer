﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.GFK.Contract;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.GFK
{
	public class GfkStatisticsExportService : IGfkStatisticsExportService
	{
		public string Period { get; set; }

		protected IGfkSetting Setting { get; private set; }
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ILekmerOrderService OrderService { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }

		public GfkStatisticsExportService(
			IGfkSetting setting,
			ILekmerChannelService channelService,
			IOrderService orderService,
			ILekmerProductService productService,
			ICategoryService categoryService,
			IProductSizeService productSizeService)
		{
			Setting = setting;
			ChannelService = channelService;
			OrderService = (ILekmerOrderService) orderService;
			ProductService = productService;
			CategoryService = categoryService;
			ProductSizeService = productSizeService;
		}

		public IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			var channels = ChannelService.GetAll().Where(c => Setting.GetChannelNameReplacementExists(c.CommonName));
			return channels.Select(channel =>
			{
				var context = IoC.Resolve<IUserContext>();
				context.Channel = channel;
				return context;
			}).ToList();
		}

		public Dictionary<string, IGfkInfo> GetGfkInfoCollectionToExport(IUserContext context, string startDate, string endDate)
		{
			var gfkInfoCollection = new Dictionary<string, IGfkInfo>();

			Collection<IOrder> orders = OrderService.GetToSendGfkStatistics(context, startDate, endDate);
			foreach (IOrderFull order in orders)
			{
				foreach (IOrderItem orderItem in order.GetOrderItems())
				{
					var product = (ILekmerProduct) ProductService.GetByIdWithoutStatusFilter(context, orderItem.ProductId, true);
					if (product == null) continue;

					var category = CategoryService.GetViewById(context, product.CategoryId);
					if (category == null) continue;

					var item = (ILekmerOrderItem) orderItem;
					var orderItemSize = item.OrderItemSize;
					var key = orderItemSize != null && !string.IsNullOrEmpty(orderItemSize.ErpId) ? orderItemSize.ErpId : item.ErpId;
					if (gfkInfoCollection.ContainsKey(key))
					{
						var gfkInfo = gfkInfoCollection[key];
						gfkInfo.SalesUnits += item.Quantity;
						gfkInfo.SalesValue += item.Quantity * item.ActualPrice.IncludingVat;
						gfkInfo.StockUnits = GetNumberInStock(orderItemSize, product);
					}
					else
					{
						var shopNumber = (ShopNumbers) Enum.Parse(typeof (ShopNumbers), context.Channel.CommonName);
						var gfkInfo = CreateGfkInfo(item, category, product, key, shopNumber);
						gfkInfoCollection.Add(key, gfkInfo);
					}
				}
			}

			return gfkInfoCollection;
		}

		protected virtual IGfkInfo CreateGfkInfo(ILekmerOrderItem item, ICategoryView category, ILekmerProduct product, string key, ShopNumbers shopNumber)
		{
			var gfkInfo = IoC.Resolve<IGfkInfo>();

			gfkInfo.Period = Period;
			gfkInfo.GroupNo = category.ErpId;
			gfkInfo.GroupText = category.Title;
			gfkInfo.ArticleText = product.DisplayTitle;
			gfkInfo.Brand = product.Brand != null ? product.Brand.Title : string.Empty;
			gfkInfo.ArticleNo = key;
			gfkInfo.ArticleNoUniversal = !string.IsNullOrEmpty(product.EanCode) ? product.EanCode : string.Empty;
			gfkInfo.ShopNo = shopNumber.GetEnumDescription() ?? string.Empty;
			gfkInfo.SalesUnits = item.Quantity;
			gfkInfo.SalesValue = item.Quantity * item.ActualPrice.IncludingVat;
			gfkInfo.StockUnits = GetNumberInStock(item.OrderItemSize, product);

			return gfkInfo;
		}
		protected virtual int GetNumberInStock(IOrderItemSize orderItemSize, ILekmerProduct product)
		{
			int numberInStock;
			if (orderItemSize != null && orderItemSize.SizeId.HasValue)
			{
				var size = ProductSizeService.GetById(product.Id, orderItemSize.SizeId.Value);
				numberInStock = size.NumberInStock;
			}
			else
			{
				numberInStock = product.NumberInStock;
			}
			return numberInStock;
		}
	}
}