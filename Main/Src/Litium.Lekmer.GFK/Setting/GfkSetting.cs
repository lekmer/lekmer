﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.GFK.Contract;

namespace Litium.Lekmer.GFK
{
	public class GfkSetting : SettingBase, IGfkSetting
	{
		protected override string StorageName
		{
			get { return "GfkStatisticsExport"; }
		}
		protected virtual string GroupName
		{
			get { return "GfkStatisticsExportSettings"; }
		}

		public string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}
		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}
		public string FileName
		{
			get { return GetString(GroupName, "FileName"); }
		}
		public int MaxDaysAfterPurchase
		{
			get { return GetInt32(GroupName, "MaxDaysAfterPurchase", 7); }
		}
		public int MinDaysAfterPurchase
		{
			get { return GetInt32(GroupName, "MinDaysAfterPurchase", 0); }
		}
		public string Delimiter
		{
			get { return GetString(GroupName, "Delimiter"); }
		}

		public string FtpUrl
		{
			get { return GetString(GroupName, "FtpUrl"); }
		}
		public string FtpDirectory
		{
			get { return GetString(GroupName, "FtpDirectory"); }
		}
		public int Port
		{
			get { return GetInt32(GroupName, "Port", 22); }
		}
		public string Username
		{
			get { return GetString(GroupName, "UserName"); }
		}
		public string Password
		{
			get { return GetString(GroupName, "Password"); }
		}

		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames, ',');
			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}
		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames, ',');
			return dictionary.ContainsKey(channelName);
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue, seperator);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
		// value1,value2, ... , valueN => List<value>
		public ICollection<string> ConvertToList(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { seperator }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
	}
}