﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.BrandTopList
{
	[Serializable]
	public class BlockBrandTopList : BlockBase, IBlockBrandTopList
	{
		private int _orderStatisticsDayCount;
		private bool _includeAllCategories;
		private int? _linkContentNodeId;
		private IBlockSetting _setting;
		private Collection<IBlockBrandTopListBrand> _brandTopListBrands;
		private Collection<IBlockBrandTopListCategory> _blockBrandTopListCategory;

		public int OrderStatisticsDayCount
		{
			get
			{
				return _orderStatisticsDayCount;
			}
			set
			{
				CheckChanged(_orderStatisticsDayCount, value);
				_orderStatisticsDayCount = value;
			}
		}

		public bool IncludeAllCategories
		{
			get
			{
				return _includeAllCategories;
			}
			set
			{
				CheckChanged(_includeAllCategories, value);
				_includeAllCategories = value;
			}
		}

		public int? LinkContentNodeId
		{
			get
			{
				return _linkContentNodeId;
			}
			set
			{
				CheckChanged(_linkContentNodeId, value);
				_linkContentNodeId = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}

		public Collection<IBlockBrandTopListBrand> BrandTopListBrands
		{
			get { return _brandTopListBrands; }
			set
			{
				CheckChanged(_brandTopListBrands, value);
				_brandTopListBrands = value;
			}
		}

		public Collection<IBlockBrandTopListCategory> BlockBrandTopListCategory
		{
			get { return _blockBrandTopListCategory; }
			set
			{
				CheckChanged(_blockBrandTopListCategory, value);
				_blockBrandTopListCategory = value;
			}
		}
	}
}