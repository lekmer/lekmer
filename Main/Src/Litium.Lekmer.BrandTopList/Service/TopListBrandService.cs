using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.BrandTopList
{
	public class TopListBrandService : ITopListBrandService
	{
		protected TopListBrandRepository Repository { get; private set; }
		protected IBlockBrandTopListBrandService BlockBrandTopListBrandService { get; private set; }
		protected IBlockBrandTopListCategoryService BlockBrandTopListCategoryService { get; private set; }
		protected IBrandService BrandService { get; private set; }

		public TopListBrandService(
			TopListBrandRepository repository,
			IBlockBrandTopListBrandService blockBrandTopListBrandService,
			IBlockBrandTopListCategoryService blockBrandTopListCategoryService,
			IBrandService brandService)
		{
			Repository = repository;
			BlockBrandTopListBrandService = blockBrandTopListBrandService;
			BlockBrandTopListCategoryService = blockBrandTopListCategoryService;
			BrandService = brandService;
		}

		public virtual BrandCollection GetAllByBlock(IUserContext context, IBlockBrandTopList block, int pageNumber, int pageSize)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			BrandIdCollection brandIds = TopListBrandCollectionCache.Instance.TryGetItem(
				new TopListBrandCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize),
				() => GetIdAllByBlock(context, block, pageNumber, pageSize));

			return BrandService.PopulateBrands(context, brandIds);
		}

		protected virtual BrandIdCollection GetIdAllByBlock(IUserContext context, IBlockBrandTopList block, int pageNumber, int pageSize)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (pageNumber < 1)
			{
				throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "pageNumber must be 1 or higher.");
			}

			if (pageSize < 1)
			{
				throw new ArgumentOutOfRangeException("pageSize", pageSize, "pageSize must be 1 or higher.");
			}

			// All the forced brands. The ones out of range are filtered so that they are not included.
			var forcedBrands = block.BrandTopListBrands.Where(fb => fb.Position > 0 && fb.Position <= block.Setting.ActualTotalItemCount);

			int displayFrom = ((pageNumber - 1) * pageSize) + 1;
			int displayTo = Math.Min(pageNumber * pageSize, block.Setting.ActualTotalItemCount);

			int forcedBeforeRangeCount = forcedBrands.Count(fp => fp.Position < displayFrom);
			var forcedWithinRange = forcedBrands.Where(fp => fp.Position >= displayFrom && fp.Position <= displayTo);

			int fetchFrom = displayFrom - forcedBeforeRangeCount;
			int fetchTo = displayTo - (forcedBeforeRangeCount + forcedWithinRange.Count());

			// Get categories.
			BrandIdCollection statisticsBrands = GetIdAllByStatisticsExcludeForced(context, block, fetchFrom, fetchTo);

			bool thisPageHasStatisticsBrands = statisticsBrands.Count > 0;

			// Add forced brands.
			// Forced brands not added the page doesn't have statistics brands. Those will be added in the next step.
			if (thisPageHasStatisticsBrands)
			{
				foreach (IBlockBrandTopListBrand forcedBrand in forcedWithinRange)
				{
					int index = forcedBrand.Position - displayFrom;

					if (index < statisticsBrands.Count)
					{
						statisticsBrands.Insert(index, forcedBrand.Brand.Id);
					}
					else
					{
						statisticsBrands.Add(forcedBrand.Brand.Id);
					}
				}
			}
			else if (forcedWithinRange.Count() == pageSize)
			{
				foreach (IBlockBrandTopListBrand forcedBrand in forcedWithinRange)
				{
					statisticsBrands.Add(forcedBrand.Brand.Id);
				}
			}

			// Additional brands that can be outside of the current display range.
			if (statisticsBrands.Count < pageSize && block.Setting.ActualTotalItemCount > statisticsBrands.TotalCount)
			{
				int additionalForcedUsedBeforeThisPage = Math.Max((displayFrom - 1) - statisticsBrands.TotalCount, 0);
				int additionalForcedRequiredOnThisPage = (displayTo - (displayFrom - 1)) - statisticsBrands.Count;

				IEnumerable<IBlockBrandTopListBrand> additionalForcedBrands;
				if (thisPageHasStatisticsBrands)
				{
					// Only add brands from position outside of this range.
					additionalForcedBrands = forcedBrands.Where(fp => fp.Position > displayTo).Take(additionalForcedRequiredOnThisPage);
				}
				else
				{
					// Add brands from position in this range and outside of this range.
					additionalForcedBrands = forcedBrands.Skip(additionalForcedUsedBeforeThisPage).Take(additionalForcedRequiredOnThisPage);
				}

				foreach (IBlockBrandTopListBrand forcedBrand in additionalForcedBrands)
				{
					statisticsBrands.Add(forcedBrand.Brand.Id);
				}
			}

			// Set the totalcount (never bigger then block.TotalBrandCount).
			statisticsBrands.TotalCount = Math.Min(statisticsBrands.TotalCount + forcedBrands.Count(), block.Setting.ActualTotalItemCount);

			return statisticsBrands;
		}

		protected virtual BrandIdCollection GetIdAllByStatisticsExcludeForced(IUserContext context, IBlockBrandTopList block, int positionFrom, int positionTo)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			Collection<ICategoryView> categories = null;
			if (!block.IncludeAllCategories)
			{
				categories = BlockBrandTopListCategoryService.GetViewAllByBlockWithChildren(context, block);
			}

			// Get brand ids by statistics and categories.
			return GetIdAllByStatisticsAndCategoriesExcludeForced(context, block, block.IncludeAllCategories, categories, positionFrom, positionTo);
		}

		protected virtual BrandIdCollection GetIdAllByStatisticsAndCategoriesExcludeForced(IUserContext context, IBlockBrandTopList block, bool includeAllCategories, Collection<ICategoryView> categories, int positionFrom, int positionTo)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			DateTime countOrdersFrom = DateTime.Now.Date.AddDays(-block.OrderStatisticsDayCount);

			return Repository.GetIdAllByBlock(context.Channel.Id, block.Id, includeAllCategories, categories, countOrdersFrom, positionFrom, positionTo);
		}
	}
}