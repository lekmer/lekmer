using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.BrandTopList.Contract;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListBrandSecureService : IBlockBrandTopListBrandSecureService
	{
		protected BlockBrandTopListBrandRepository Repository { get; private set; }

		public BlockBrandTopListBrandSecureService(BlockBrandTopListBrandRepository repository)
		{
			Repository = repository;
		}

		public virtual void Save(int blockId, Collection<IBlockBrandTopListBrand> blockBrands)
		{
			if (blockBrands == null)
			{
				throw new ArgumentNullException("blockBrands");
			}

			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAll(blockId);

				foreach (var blockBrand in blockBrands)
				{
					blockBrand.BlockId = blockId;

					Repository.Save(blockBrand);
				}

				transaction.Complete();
			}
		}

		public virtual Collection<IBlockBrandTopListBrand> GetAllByBlock(int blockId)
		{
			return Repository.GetAllByBlockSecure(blockId);
		}
	}
}