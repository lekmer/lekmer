using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListBrandDataMapper : DataMapperBase<IBlockBrandTopListBrand>
	{
		private DataMapperBase<IBrand> _brandDataMapper;

		public BlockBrandTopListBrandDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_brandDataMapper = DataMapperResolver.Resolve<IBrand>(DataReader);
		}

		protected override IBlockBrandTopListBrand Create()
		{
			IBlockBrandTopListBrand blockBrandTopListBrand = IoC.Resolve<IBlockBrandTopListBrand>();

			blockBrandTopListBrand.BlockId = MapValue<int>("BlockBrandTopListBrand.BlockId");
			blockBrandTopListBrand.Position = MapValue<int>("BlockBrandTopListBrand.Position");
			blockBrandTopListBrand.Brand = _brandDataMapper.MapRow();
			blockBrandTopListBrand.SetUntouched();

			return blockBrandTopListBrand;
		}
	}
}