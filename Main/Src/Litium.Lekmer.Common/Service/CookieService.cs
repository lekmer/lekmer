﻿using System;
using System.Web;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Common
{
	public class CookieService: ICookieService
	{
		public virtual HttpCookie GetRequestCookie(string cookieName)
		{
			if (cookieName.IsEmpty())
			{
				throw new ArgumentNullException("cookieName");
			}

			return HttpContext.Current.Request.Cookies[cookieName];
		}

		public virtual void SetResponseCookie(HttpCookie cookie)
		{
			if (cookie == null)
			{
				throw new ArgumentNullException("cookie");
			}
			
			HttpContext.Current.Response.Cookies.Add(cookie);
		}
	}
}