using System;
using Litium.Framework.Setting;

namespace Litium.Lekmer.Common
{
	public class LekmerWebSetting : SettingBase
	{
		#region Singleton

		private LekmerWebSetting()
		{
		}

		public static LekmerWebSetting Instance
		{
			get { return SingletonCreator.CreatorInstance; }
		}

		private static class SingletonCreator
		{
			private static readonly LekmerWebSetting _instance = new LekmerWebSetting();

			public static LekmerWebSetting CreatorInstance
			{
				get { return _instance; }
			}
		}

		#endregion

		protected override string StorageName
		{
			get { return "Core"; }
		}

		protected virtual string GroupName
		{
			get { return "WebSetting"; }
		}

		public string OverrideApplicationName
		{
			get { return GetString(GroupName, "OverrideApplicationName", null); }
		}

		public string MediaUrl
		{
			get { return GetString(GroupName, "MediaUrl"); }
		}

		public bool SupportSsl
		{
			get { return GetBoolean(GroupName, "SupportSsl", false); }
		}

		public string SecureConnectionPort
		{
			get { return GetString(GroupName, "SecureConnectionPort", string.Empty); }
		}

		public string MediaUriSchemeHttp
		{
			get { return GetString(GroupName, "MediaUriSchemeHttp", "http://"); }
		}

		public string MediaUriSchemeHttps
		{
			get { return GetString(GroupName, "MediaUriSchemeHttps", "https://"); }
		}

		public bool DeviceDetectionEnabled
		{
			get { return GetBoolean(GroupName, "DeviceDetectionEnabled", false); }
		}
	}
}