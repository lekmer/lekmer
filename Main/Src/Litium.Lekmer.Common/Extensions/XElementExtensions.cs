﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Litium.Lekmer.Common.Extensions
{
	/// <summary>
	/// Some usefull XElement extensions.
	/// </summary>
	public static class XElementExtensions
	{
		public static XElement ElementOrDefault(this XContainer element, XName name)
		{
			if (element == null)
			{
				return null;
			}

			return element.Element(name);
		}

		public static IEnumerable<XElement> ElementsOrEmpty(this XContainer element, XName name)
		{
			if (element == null)
			{
				return Enumerable.Empty<XElement>();
			}

			return element.Elements(name);
		}

		public static string ValueOrDefault(this XElement element)
		{
			if (element == null)
			{
				return string.Empty;
			}

			return element.Value;
		}
	}
}
