﻿using System;
using System.Web;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Scensum.Core.Web;

namespace Litium.Lekmer.Common
{
	public static class LekmerUrlHelper
	{
		public static string BackOfficeBaseUrl
		{
			get
			{
				HttpContext context = HttpContext.Current;

				if (context == null)
				{
					return string.Empty;
				}

				string uriScheme = ResolveUriScheme();

				string domain = context.Request.Url.Host;

				string directory = "/";
				if (context.Request.ApplicationPath != null && context.Request.ApplicationPath.Equals("/") == false)
				{
					directory = context.Request.ApplicationPath + "/";
				}

				return uriScheme + domain + directory;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "SubDomain"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "subDomain")]
		public static string ResolveSubDomainUrl(ISubDomain subDomain)
		{
			string resolvedUlr = string.Empty;

			if (subDomain.UrlType == (int)UrlType.Absolute)
			{
				if (HasUriScheme(subDomain.DomainUrl))
				{
					resolvedUlr = subDomain.DomainUrl;
				}
				else
				{
					string uriScheme = ResolveMediaUriScheme();

					resolvedUlr = uriScheme + subDomain.DomainUrl;
				}
			}
			else if (subDomain.UrlType == (int)UrlType.Relative)
			{
				resolvedUlr = ResolveRelativeUrl(subDomain.DomainUrl);
			}

			return resolvedUlr.TrimEnd('/');
		}

		public static string ResolveRelativeUrl(string relativeUrl)
		{
			string resolvedUlr;

			if (HttpContext.Current != null)
			{
				resolvedUlr = UrlHelper.ResolveUrl(relativeUrl);
			}
			else
			{
				resolvedUlr = relativeUrl.TrimStart('~');
			}

			return resolvedUlr.TrimEnd('/');
		}

		public static bool HasUriScheme(string url)
		{
			if (url.StartsWith("http", StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}

			if (url.StartsWith("//", StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}

			return false;
		}

		public static string ResolveUriScheme()
		{
			if (HttpContext.Current == null)
			{
				return "http://";
			}

			string protocol = HttpContext.Current.Request.IsSecureConnection()
				? "https://" // HTTPS
				: "http://"; // HTTP

			return protocol;
		}

		public static string ResolveMediaUriScheme()
		{
			if (HttpContext.Current == null)
			{
				return LekmerWebSetting.Instance.MediaUriSchemeHttp;
			}

			string protocol = HttpContext.Current.Request.IsSecureConnection()
				? LekmerWebSetting.Instance.MediaUriSchemeHttps // HTTPS
				: LekmerWebSetting.Instance.MediaUriSchemeHttp; // HTTP

			return protocol;
		}
	}
}
