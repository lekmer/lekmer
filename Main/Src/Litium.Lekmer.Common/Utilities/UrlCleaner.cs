﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.RegularExpressions;

namespace Litium.Lekmer.Common
{
	/// <summary>
	/// Replacement for buggy <see cref="Litium.Scensum.Foundation.Utilities.UrlCleaner"/>
	/// </summary>
	public static class UrlCleaner
	{
		private static readonly Regex _unwantedCharactersRegex = new Regex(@"[^a-z0-9-]", RegexOptions.Compiled);
		private static readonly Regex _duplicateDashesRegex = new Regex(@"-{2,}", RegexOptions.Compiled);
		private static readonly Regex _dashesInBeginningOrEndOfStringRegex = new Regex(@"^-|-$", RegexOptions.Compiled);

		/// <summary>
		/// Removes/replaces characters not suitable for urls.
		/// </summary>
		/// <param name="value">The value to clean.</param>
		/// <returns>The cleaned value.</returns>
		[SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
		public static string CleanUp(string value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}

			// Lower it.
			value = value.ToLowerInvariant();

			// Separate diacritics from letter.
			value = value.Normalize(NormalizationForm.FormD);

			// Replace some characters.
			const string find = @"æø€£$&*\/ +=!?:;,._";
			const string repl = @"aoelsox------------";

			if (find.Length != repl.Length)
			{
				throw new ArgumentException("find");
			}

			for (int i = 0; i < find.Length; i++)
			{
				value = value.Replace(find[i], repl[i]);
			}

			// Remove unwanted characters so only a-z, 0-9 and "-" are left.
			value = _unwantedCharactersRegex.Replace(value, string.Empty);

			// Remove duplicate "-".
			value = _duplicateDashesRegex.Replace(value, "-");

			// Remove "-" in the start/end.
			value = _dashesInBeginningOrEndOfStringRegex.Replace(value, string.Empty);

			return value;
		}

		/// <summary>
		/// Removes/replaces characters not suitable for urls. Copy from product import.
		/// </summary>
		/// <param name="value">The value to clean.</param>
		/// <returns>The cleaned value.</returns>
		public static string CleanUpProductUrlTitle(string value)
		{
			value = Regex.Replace(value, @"[^\w\såäöæø]", " ");
			value = Regex.Replace(value, @"\s{2,}", " ");
			value = Regex.Replace(value, @"\s$", "");
			value = Regex.Replace(value, @"^\s", "");
			return value;
		}
	}
}