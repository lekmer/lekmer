﻿using System;
using System.Globalization;
using System.Reflection;
using System.Timers;
using log4net;

namespace Litium.Lekmer.Common.Job
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1001:TypesThatOwnDisposableFieldsShouldBeDisposable")]
	public abstract class BaseJob: IJob
	{
		private ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Timer _timerWeekly;
		private Timer _timerDaily;
		private Timer _timerRepeat;

		public IScheduleSetting ScheduleSetting { get; set; }
		public IIntervalCalculator IntervalCalculator { get; set; }

		public object SyncToken { get; set; }

		public virtual string Group
		{
			get { return "Base"; }
		}

		public virtual string Name
		{
			get { return "Base job"; }
		}


		public virtual void StartJob()
		{
			ScheduleJob();
		}

		public virtual void StopJob()
		{
			if (_timerDaily != null)
			{
				_timerDaily.Stop();
			}

			if (_timerRepeat != null)
			{
				_timerRepeat.Stop();
			}
		}


		protected virtual void ScheduleJob()
		{
			if (ScheduleSetting.UseWeeklyExecution)
			{
				_timerWeekly = new Timer();

				_timerWeekly.Elapsed += OnTimerWeeklyElapsed;
				_timerWeekly.Interval = CalculateIntervalToNextWeeklyExecution();
				_timerWeekly.AutoReset = false;

				_timerWeekly.Start(); // Sets Enabled=true
			}

			if (ScheduleSetting.UseDailyExecution)
			{
				_timerDaily = new Timer();

				_timerDaily.Elapsed += OnTimerDailyElapsed;
				_timerDaily.Interval = CalculateIntervalToNextDailyExecution();
				_timerDaily.AutoReset = false;

				_timerDaily.Start(); // Sets Enabled=true
			}

			if (ScheduleSetting.ExecuteOnStartUp)
			{
				_timerRepeat = new Timer();

				_timerRepeat.Elapsed += OnTimerRepeatElapsed;
				_timerRepeat.Interval = 1000 * 5; // 5 sec
				_timerRepeat.AutoReset = false;

				_timerRepeat.Start(); // Sets Enabled=true
			}
			else if (ScheduleSetting.UseRepeatExecution)
			{
				_timerRepeat = new Timer();

				_timerRepeat.Elapsed += OnTimerRepeatElapsed;
				_timerRepeat.Interval = CalculateIntervalToFirstRepeatExecution();
				_timerRepeat.AutoReset = false;

				_timerRepeat.Start(); // Sets Enabled=true
			}
		}

		protected virtual int CalculateIntervalToNextWeeklyExecution()
		{
			TimeSpan delay = IntervalCalculator.NextWeeklyExecution(DateTime.Now, ScheduleSetting);

			_log.Info(Name + " - Time to next weekly execution: " + delay);

			return (int)delay.TotalMilliseconds;
		}

		protected virtual int CalculateIntervalToNextDailyExecution()
		{
			TimeSpan delay = IntervalCalculator.NextDailyExecution(DateTime.Now, ScheduleSetting);

			_log.Info(Name + " - Time to next daily execution: " + delay);

			return (int)delay.TotalMilliseconds;
		}

		protected virtual int CalculateIntervalToFirstRepeatExecution()
		{
			TimeSpan delay = IntervalCalculator.FirstRepeatExecution(DateTime.Now, ScheduleSetting);

			_log.Info(Name + " - Time to next repeat execution: " + delay);

			return (int)delay.TotalMilliseconds;
		}

		protected virtual int CalculateIntervalToNextRepeatExecution()
		{
			TimeSpan delay = IntervalCalculator.NextRepeatExecution(DateTime.Now, ScheduleSetting);

			_log.Info(Name + " - Time to next repeat execution: " + delay);

			return (int)delay.TotalMilliseconds;
		}


		protected virtual void OnTimerWeeklyElapsed(object sender, ElapsedEventArgs e)
		{
			Execute();

			if (ScheduleSetting.UseWeeklyExecution)
			{
				_timerWeekly.Interval = CalculateIntervalToNextWeeklyExecution();
				_timerWeekly.Start();
			}
		}

		protected virtual void OnTimerDailyElapsed(object sender, ElapsedEventArgs e)
		{
			Execute();

			if (ScheduleSetting.UseDailyExecution)
			{
				_timerDaily.Interval = CalculateIntervalToNextDailyExecution();
				_timerDaily.Start();
			}
		}

		protected virtual void OnTimerRepeatElapsed(object sender, ElapsedEventArgs e)
		{
			Execute();

			if (ScheduleSetting.UseRepeatExecution)
			{
				_timerRepeat.Interval = CalculateIntervalToNextRepeatExecution();
				_timerRepeat.Start();
			}
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
		protected virtual void Execute()
		{
			lock (SyncToken ?? (SyncToken = new object()))
			{
				try
				{
					_log.Info(string.Format(CultureInfo.CurrentCulture, "Execution '{0}' ...", Name));

					ExecuteAction();

					_log.Info(string.Format(CultureInfo.CurrentCulture, "Execution '{0}' ...Done", Name));
				}
				catch (Exception ex)
				{
					_log.Error(string.Format(CultureInfo.CurrentCulture, "Execution '{0}' failed.", Name), ex);
				}
			}
		}

		protected abstract void ExecuteAction();
	}
}
