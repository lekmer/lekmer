﻿using System;
using System.Data;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Framework.DataMapper;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	public class LekmerCacheUpdateFeedItemMapper : DataMapperBase<ILekmerCacheUpdateFeedItem>
	{
		public LekmerCacheUpdateFeedItemMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ILekmerCacheUpdateFeedItem Create()
		{
			long id = MapValue<long>("Id");
			string managerName = MapValue<string>("ManagerName");
			CacheUpdateType updateType = (CacheUpdateType)MapValue<int>("UpdateType");
			string key = MapNullableValue<string>("Key");
			DateTime creationDate = MapValue<DateTime>("CreationDate");

			return new LekmerCacheUpdateFeedItem(id, managerName, updateType, key, creationDate);
		}
	}
}