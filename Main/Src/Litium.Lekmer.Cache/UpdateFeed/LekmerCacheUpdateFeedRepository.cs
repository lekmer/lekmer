﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	public class LekmerCacheUpdateFeedRepository
	{
		protected virtual LekmerCacheUpdateFeedItemMapper CreateDataMapper(IDataReader dataReader)
		{
			return new LekmerCacheUpdateFeedItemMapper(dataReader);
		}

		/// <summary>
		/// Get new update items that are newer then <paramref name="fromId"/>.
		/// </summary>
		/// <param name="fromId">Items newer then this id should be fetched.</param>
		/// <returns>A list of new updates.</returns>
		public virtual Collection<ILekmerCacheUpdateFeedItem> GetNewUpdateItems(long fromId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("FromId", fromId, SqlDbType.BigInt)
			};

			var dbSettings = new DatabaseSetting("LekmerCacheUpdateFeed.GetNewUpdateItems");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[corelek].[pCacheUpdateGetFromId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		/// <summary>
		/// Adds a new update to the feed.
		/// </summary>
		/// <param name="updateItem">The update item.</param>
		public virtual void AddUpdateItem(ICacheUpdateFeedItem updateItem)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ManagerName", updateItem.ManagerName, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("UpdateType", (int) updateItem.UpdateType, SqlDbType.Int),
				ParameterHelper.CreateParameter("Key", updateItem.Key, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("CreationDate", updateItem.CreationDate, SqlDbType.DateTime)
			};

			var dbSettings = new DatabaseSetting("LekmerCacheUpdateFeed.AddUpdateItem");

			new DataHandler().ExecuteCommand("[corelek].[pCacheUpdateSave]", parameters, dbSettings);
		}

		/// <summary>
		/// Deletes update items older then <paramref name="toDate"/>.
		/// </summary>
		/// <param name="toDate">Items older then this should be removed.</param>
		public virtual void DeleteOldUpdateItems(DateTime toDate)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ToDate", toDate, SqlDbType.DateTime)
			};

			var dbSettings = new DatabaseSetting("LekmerCacheUpdateFeed.DeleteOldItems");

			new DataHandler().ExecuteCommand("[corelek].[pCacheUpdateDeleteOld]", parameters, dbSettings);
		}
	}
}