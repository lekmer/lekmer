﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Oculus.Setting
{
	public class OculusSetting : SettingBase
	{
		private const string _storageName = "OculusService";

		protected override string StorageName
		{
			get { return _storageName; }
		}

		public static string GetStorageName()
		{
			return _storageName;
		}

		private static readonly string GROUP_NAME = "Oculus";

		public int TimerInterval()
		{
			return GetInt32(GROUP_NAME, "TimerInterval", 60*24);
		}

		public string OculusServiceUrl()
		{
			return GetString(GROUP_NAME, "OculusServiceUrl");
		}

		public int OculusServiceTimeout()
		{
			return GetInt32(GROUP_NAME, "OculusServiceTimeout", 20);
		}

		public string FileName()
		{
			return GetString(GROUP_NAME, "FileName");
		}

		public string FilePath()
		{
			return GetString(GROUP_NAME, "FilePath");
		}

		public string ProductNodePath()
		{
			return GetString(GROUP_NAME, "ProductNodePath");
		}

		public string SimilarNodePath()
		{
			return GetString(GROUP_NAME, "SimilarNodePath");
		}

		public string ProductErpIdAttribute()
		{
			return GetString(GROUP_NAME, "ProductErpIdAttribute");
		}

		public string ProductScoreAttribute()
		{
			return GetString(GROUP_NAME, "ProductScoreAttribute");
		}
	}
}