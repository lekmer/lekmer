﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Oculus.Mapper
{
	public class SimilarProductItemDataMapper : DataMapperBase<ISimilarProductItem>
	{
		public SimilarProductItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISimilarProductItem Create()
		{
			ISimilarProductItem similarProductItem = IoC.Resolve<ISimilarProductItem>();

			similarProductItem.ProductId = MapValue<int>("ProductSimilar.SimilarProductId");
			similarProductItem.Score = MapValue<decimal>("ProductSimilar.Score");

			return similarProductItem;
		}
	}
}