﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Oculus.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Oculus
{
	public class ProductSimilarSecureService : IProductSimilarSecureService
	{
		protected ProductSimilarRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductSimilarSecureService(ProductSimilarRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public IProductSimilar Save(IProductSimilar productSimilar)
		{
			if (productSimilar == null) throw new ArgumentNullException("productSimilar");

			using (var transactedOperation = new TransactedOperation())
			{
				productSimilar = Repository.Save(productSimilar);
				transactedOperation.Complete();
			}

			ProductSimilarCache.Instance.Remove(new ProductSimilarKey(productSimilar.ProductId));

			return productSimilar;
		}
	}
}