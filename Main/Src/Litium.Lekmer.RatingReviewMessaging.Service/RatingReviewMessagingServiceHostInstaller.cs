﻿using System.ComponentModel;
using System.Configuration.Install;


namespace Litium.Lekmer.RatingReviewMessaging.Service
{
	[RunInstaller(true)]
	public partial class RatingReviewMessagingServiceHostInstaller : Installer
	{
		public RatingReviewMessagingServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}
