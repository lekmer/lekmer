﻿namespace Litium.Lekmer.Payment.Maksuturva
{
	public enum MaksuturvaResponseCode
	{
		NotPaid = 00,
		QueryFailed = 01,
		WaitingForPayment = 10,
		PaymentUncertain = 11,
		WebStoreNotYetCompensated = 30,
		WebStoreCompensated = 40,
		Unknown = 100,
		ValidationFailed = 101,
		NotPaidYet = 102
	}
}