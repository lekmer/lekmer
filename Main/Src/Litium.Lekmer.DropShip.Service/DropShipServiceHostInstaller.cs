﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Litium.Lekmer.DropShip.Service
{
	[RunInstaller(true)]
	public partial class DropShipServiceHostInstaller : Installer
	{
		public DropShipServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}