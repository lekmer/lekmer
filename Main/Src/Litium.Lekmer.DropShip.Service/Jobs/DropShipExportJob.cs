﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DropShip.Service
{
	public class DropShipExportJob : BaseJob
	{
		public override string Name
		{
			get { return "DropShipExportJob"; }
		}

		protected override void ExecuteAction()
		{
			var dropShipExport = IoC.Resolve<ITask>("DropShipExport");
			dropShipExport.Execute();
		}
	}
}