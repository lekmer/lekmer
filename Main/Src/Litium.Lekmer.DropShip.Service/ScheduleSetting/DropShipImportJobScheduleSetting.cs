﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.DropShip.Service
{
	public class DropShipImportJobScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "DropShipService"; }
		}

		protected override string GroupName
		{
			get { return "DropShipImportJob"; }
		}
	}
}