using System;

namespace Litium.Lekmer.Voucher
{
	[Serializable]
	public class VoucherCheckResult : IVoucherCheckResult
	{
		public string VoucherCode { get; set; }
		public bool IsValid { get; set; }
		public int? VoucherbatchId { get; set; }
		public decimal DiscountValue { get; set; }
		public VoucherValueType ValueType { get; set; }
		public decimal AmountLeft { get; set; }
		public bool SpecialOffer { get; set; }
	}
}
