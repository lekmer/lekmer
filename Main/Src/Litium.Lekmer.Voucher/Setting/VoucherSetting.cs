using Litium.Framework.Setting;

namespace Litium.Lekmer.Voucher.Setting
{
	public class VoucherSetting : SettingBase
	{
		#region Singleton

		private static readonly VoucherSetting _instance = new VoucherSetting();

		private VoucherSetting()
		{
		}

		public static VoucherSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "VoucherSetting";

		protected override string StorageName
		{
			get { return "VoucherSetting"; }
		}

		public string WebServiceUrl
		{
			get { return GetString(_groupName, "WebServiceUrl"); }
		}
	}
}