using System;
using Litium.Lekmer.Voucher.Setting;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Voucher
{
	public class VoucherService : IVoucherService
	{
		private static readonly ILog _log = LogManager.GetLogger(typeof(VoucherService));
		private readonly VoucherWebReference.Service1 _voucherWebService;

		public VoucherService()
		{
			_voucherWebService = new VoucherWebReference.Service1
				{
					Url = VoucherSetting.Instance.WebServiceUrl
				};
		}

		public IVoucherCheckResult VoucherCheck(string code, string siteApplicationName)
		{
			VoucherWebReference.VoucherConsumeResult voucherConsumeResult = _voucherWebService.VoucherCheck(code, siteApplicationName);

			var voucherCheckResult = IoC.Resolve<IVoucherCheckResult>();
			voucherCheckResult.VoucherCode = code;
			voucherCheckResult.VoucherbatchId = voucherConsumeResult.VoucherBatchId;
			voucherCheckResult.IsValid = voucherConsumeResult.IsValid;
			voucherCheckResult.DiscountValue = voucherConsumeResult.DiscountValue;
			voucherCheckResult.AmountLeft = voucherConsumeResult.AmountLeft;
			voucherCheckResult.SpecialOffer = voucherConsumeResult.SpecialOffer;

			switch (voucherConsumeResult.ValueType)
			{
				case VoucherWebReference.VoucherValueType.Price:
					voucherCheckResult.ValueType = VoucherValueType.Price;
					break;
				case VoucherWebReference.VoucherValueType.Percentage:
					voucherCheckResult.ValueType = VoucherValueType.Percentage;
					break;
				case VoucherWebReference.VoucherValueType.GiftCard:
					voucherCheckResult.ValueType = VoucherValueType.GiftCard;
					break;
				case VoucherWebReference.VoucherValueType.Code:
					voucherCheckResult.ValueType = VoucherValueType.Code;
					break;
			}

			return voucherCheckResult;
		}

		public void VoucherConsume(string code, int orderId, decimal usedAmount, string siteApplicationName)
		{
			try
			{
				_voucherWebService.VoucherConsume(code, orderId, usedAmount, siteApplicationName);
			}
			catch (Exception ex)
			{
				_log.Error(string.Format("Error occured while consuming voucher. Voucher code: {0}, order id: {1}, used amount: {2}, site: {3}", code, orderId, usedAmount, siteApplicationName), ex);
			}
		}

		public void VoucherRelease(string code)
		{
			try
			{
				_voucherWebService.VoucherRelease(code);
			}
			catch (Exception ex)
			{
				_log.Error(string.Format("Error occured while releasing voucher. Voucher code: {0}", code), ex);
			}
		}

		public void VoucherReserve(string code)
		{
			try
			{
				_voucherWebService.VoucherReserve(code);
			}
			catch (Exception ex)
			{
				_log.Error(string.Format("Error occured while reserving voucher. Voucher code: {0}", code), ex);
			}
		}


		// Gift card via email.
		public int GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue(decimal discountValue, string appName, string createdBy, out int quantity)
		{
			quantity = 0;
			int voucherInfoId = -1;

			try
			{
				var voucherInfo = _voucherWebService.GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue(discountValue, appName, createdBy);

				voucherInfoId = voucherInfo.Id;
				quantity = voucherInfo.Quantity;
			}
			catch (Exception ex)
			{
				_log.Error("Error occured while getting voucher info by cnahhel, campaign and discount value", ex);
			}

			return voucherInfoId;
		}

		public int VoucherCountGetByVoucherInfo(int voucherInfoId)
		{
			int count = -1;

			try
			{
				string value = _voucherWebService.VoucherCountGetByVoucherInfo(voucherInfoId);
				int.TryParse(value, out count);
			}
			catch (Exception ex)
			{
				_log.Error("Error occured while getting vouchers count by voucher info", ex);
			}

			return count;
		}

		public int VoucherBatchCreate(DateTime validFrom, string prefix, string suffix, int quantity, decimal discountValue, string appName, int nbrOfToken, string createdBy)
		{
			int voucherInfoId = -1;

			try
			{
				string value = _voucherWebService.VoucherBatchCreate(validFrom, prefix, suffix, quantity, discountValue, appName, nbrOfToken, createdBy, false);
				int.TryParse(value, out voucherInfoId);
			}
			catch (Exception ex)
			{
				_log.Error("Error occured while creating voucher info", ex);
			}

			return voucherInfoId;
		}

		public string VoucherGenerateCodes(int voucherInfoId, int nbrOfVouchers, DateTime validTo)
		{
			string voucherCode = null;

			try
			{
				voucherCode = _voucherWebService.VoucherGenerateCodes(voucherInfoId, nbrOfVouchers, validTo);
			}
			catch (Exception ex)
			{
				_log.Error("Error occured while creating voucher code", ex);
			}

			return voucherCode;
		}
	}
}