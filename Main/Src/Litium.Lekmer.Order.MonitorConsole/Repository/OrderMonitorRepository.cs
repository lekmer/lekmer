using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.MonitorConsole
{
	public class OrderMonitorRepository
	{
		public Collection<OrderStatistic> GetOrderStatistics()
		{
			var statistics = new Collection<OrderStatistic>();

			IDataParameter[] parameters = new IDataParameter[0];
			var dbSettings = new DatabaseSetting("OrderMonitorRepository.GetOrderStatistics");
			IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pReportLekmerDailyOrdersResultset]", parameters, dbSettings);
			while (dataReader.Read())
			{
				var orderStatistic = new OrderStatistic
					{
						Date = DateTime.Parse(dataReader["Date"].ToString()),
						NumberOfOrders = (int) dataReader["NumberOfOrders"],
						AverageOrderPrice = (decimal) dataReader["AverageOrderPrice"],
						TotalOrderValue = (decimal) dataReader["TotalOrderValue"],
						Channel = dataReader["Channel"].ToString()
					};

				statistics.Add(orderStatistic);
			}

			return statistics;
		}
	}
}