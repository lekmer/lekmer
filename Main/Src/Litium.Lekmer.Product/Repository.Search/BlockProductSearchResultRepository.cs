﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockProductSearchResultRepository
	{
		protected virtual DataMapperBase<IBlockProductSearchResult> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductSearchResult>(dataReader);
		}
		public virtual IBlockProductSearchResult GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			var setting = new DatabaseSetting("BlockProductSearchResultRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductSearchResultGetByIdSecure]", parameters, setting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
		public virtual IBlockProductSearchResult GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			var setting = new DatabaseSetting("BlockProductSearchResultRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductSearchResultGetById]", parameters, setting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}
