﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductRegistryRestrictionBrandSecureService : IProductRegistryRestrictionBrandSecureService
	{
		protected ProductRegistryRestrictionBrandRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductRegistryRestrictionBrandSecureService(ProductRegistryRestrictionBrandRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public virtual IProductRegistryRestrictionItem Create()
		{
			var productRegistryRestrictionBrand = IoC.Resolve<IProductRegistryRestrictionItem>();
			productRegistryRestrictionBrand.CreatedDate = DateTime.Now;
			productRegistryRestrictionBrand.Status = BusinessObjectStatus.New;
			return productRegistryRestrictionBrand;
		}

		public virtual void Save(ISystemUserFull systemUserFull, int brandId, Collection<IProductRegistryRestrictionItem> productRegistryRestrictionBrandList)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			if (productRegistryRestrictionBrandList == null)
			{
				throw new NullReferenceException("productRegistryRestrictionBrandList");
			}

			using (var transaction = new TransactedOperation())
			{
				Delete(systemUserFull, brandId);

				foreach (var productRegistryRestrictionBrand in productRegistryRestrictionBrandList)
				{
					Repository.Save(productRegistryRestrictionBrand);
				}

				transaction.Complete();
			}

			BlockBrandListBrandCollectionCache.Instance.Flush();
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int brandId)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);
			Repository.Delete(brandId);

			BlockBrandListBrandCollectionCache.Instance.Flush();
		}

		public Collection<IProductRegistryRestrictionItem> GetAllByBrand(int brandId)
		{
			return Repository.GetAllByBrand(brandId);
		}

		public Collection<IProductRegistryRestrictionItem> GetAllWithChannel()
		{
			return Repository.GetAllWithChannel();
		}
	}
}