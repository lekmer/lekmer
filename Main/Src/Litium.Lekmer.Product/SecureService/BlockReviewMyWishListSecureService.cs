﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockReviewMyWishListSecureService : IBlockReviewMyWishListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockReviewMyWishListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockReviewMyWishListSecureService(
			IAccessValidator accessValidator,
			BlockReviewMyWishListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockReviewMyWishList Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

			var blockReviewMyWishList = IoC.Resolve<IBlockReviewMyWishList>();
			blockReviewMyWishList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockReviewMyWishList.Setting = BlockSettingSecureService.Create();
			blockReviewMyWishList.Status = BusinessObjectStatus.New;
			return blockReviewMyWishList;
		}

		public virtual IBlockReviewMyWishList GetById(int id)
		{
			return Repository.GetByIdSecure((id));
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			IBlockReviewMyWishList blockReviewMyWishList = Create();
			blockReviewMyWishList.ContentNodeId = contentNodeId;
			blockReviewMyWishList.ContentAreaId = contentAreaId;
			blockReviewMyWishList.BlockTypeId = blockTypeId;
			blockReviewMyWishList.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockReviewMyWishList.Title = title;
			blockReviewMyWishList.TemplateId = null;
			blockReviewMyWishList.Id = Save(systemUserFull, blockReviewMyWishList);
			return blockReviewMyWishList;
		}
		public virtual int Save(ISystemUserFull systemUserFull, IBlockReviewMyWishList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}
			BlockReviewMyWishListCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				transactedOperation.Complete();
			}
		}
	}
}