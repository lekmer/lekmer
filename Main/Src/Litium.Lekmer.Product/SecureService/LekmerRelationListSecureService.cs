﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerRelationListSecureService : RelationListSecureService, ILekmerRelationListSecureService
	{
		protected LekmerRelationListRepository LekmerRepository { get; private set; }

		public LekmerRelationListSecureService(IAccessValidator accessValidator, RelationListRepository repository) : base(accessValidator, repository)
		{
			LekmerRepository = (LekmerRelationListRepository) repository;
		}

		public virtual Collection<IRelationList> GetAllByProductAndType(int productId, string commonName)
		{
			return LekmerRepository.GetAllByProductAndType(productId, commonName);
		}

		public virtual RelationListCollection SearchWithoutColorVariant(IRelationListSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null) throw new ArgumentNullException("searchCriteria");
			return LekmerRepository.SearchWithoutColorVariant(searchCriteria, page, pageSize, null, false);
		}
	}
}