﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class SizeTableRowSecureService : ISizeTableRowSecureService
	{
		protected SizeTableRowRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public SizeTableRowSecureService(SizeTableRowRepository sizeTableRowRepository, IAccessValidator accessValidator, IChannelSecureService channelSecureService)
		{
			Repository = sizeTableRowRepository;
			AccessValidator = accessValidator;
			ChannelSecureService = channelSecureService;
		}

		public virtual ISizeTableRow Save(ISystemUserFull systemUserFull, ISizeTableRow sizeTableRow, Collection<ISizeTableRowTranslation> translations)
		{
			if (sizeTableRow == null)
			{
				throw new ArgumentNullException("sizeTableRow");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				int sizeTableRowId = Repository.Save(sizeTableRow);
				sizeTableRow.Id = sizeTableRowId;
				if (sizeTableRowId > 0)
				{
					// Save translations.
					SaveTranslations(sizeTableRowId, translations);
				}
				transaction.Complete();
			}
			SizeTableCache.Instance.Remove(sizeTableRow.SizeTableId, ChannelSecureService.GetAll());
			return sizeTableRow;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int sizeTableId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(sizeTableId);
				transaction.Complete();
			}

			SizeTableCache.Instance.Remove(sizeTableId, ChannelSecureService.GetAll());
		}

		public virtual Collection<ISizeTableRow> GetAllBySizeTable(int sizeTableId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllBySizeTable(sizeTableId);
		}

		// Translations.

		public virtual Collection<ISizeTableRowTranslation> GetAllTranslations(int sizeTableRowId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllTranslations(sizeTableRowId);
		}

		protected virtual void SaveTranslations(int sizeTableRowId, Collection<ISizeTableRowTranslation> translations)
		{
			foreach (var translation in translations)
			{
				translation.SizeTableRowId = sizeTableRowId;
				Repository.SaveTranslations(translation);
			}
		}
	}
}