﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class AgeSecureService : IAgeSecureService
	{
		protected AgeRepository Repository { get; private set; }

		public AgeSecureService(AgeRepository repository)
		{
			Repository = repository;
		}

		public Collection<IAge> GetAll()
		{
			return Repository.GetAll();
		}
	}
}
