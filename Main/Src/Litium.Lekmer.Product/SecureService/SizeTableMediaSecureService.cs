﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	public class SizeTableMediaSecureService : ISizeTableMediaSecureService
	{
		protected SizeTableMediaRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected ILekmerMediaItemSecureService MediaItemSecureService { get; private set; }
		protected ISizeTableMediaRegistrySecureService SizeTableMediaRegistrySecureService { get; private set; }

		public SizeTableMediaSecureService(
			SizeTableMediaRepository sizeTableMediaRepository,
			IAccessValidator accessValidator,
			IMediaItemSecureService mediaItemSecureService,
			ISizeTableMediaRegistrySecureService sizeTableMediaRegistrySecureService
			)
		{
			Repository = sizeTableMediaRepository;
			AccessValidator = accessValidator;
			MediaItemSecureService = (ILekmerMediaItemSecureService)mediaItemSecureService;
			SizeTableMediaRegistrySecureService = sizeTableMediaRegistrySecureService;
		}

		public virtual ISizeTableMediaRecord CreateRecord()
		{
			var sizeTableMedia = IoC.Resolve<ISizeTableMediaRecord>();

			sizeTableMedia.SetNew();

			return sizeTableMedia;
		}

		public virtual Collection<ISizeTableMediaRecord> GetAllBySizeTable(int sizeTableId)
		{
			Collection<ISizeTableMediaRecord> sizeTableMediaRecords = Repository.GetAllBySizeTable(sizeTableId);
			Collection<IMediaItem> mediaItems = MediaItemSecureService.GetAllBySizeTable(sizeTableId);
			Collection<ISizeTableMediaRegistry> sizeTableMediaRegistries = SizeTableMediaRegistrySecureService.GetAllBySizeTable(sizeTableId);

			foreach (ISizeTableMediaRecord sizeTableMedia in sizeTableMediaRecords)
			{
				sizeTableMedia.MediaItem = mediaItems.First(mi => mi.Id == sizeTableMedia.MediaId);

				sizeTableMedia.MediaRegistries = new Collection<ISizeTableMediaRegistry>(sizeTableMediaRegistries.Where(r => r.SizeTableMediaId == sizeTableMedia.Id).ToList());

				sizeTableMedia.SetUntouched();
			}

			return sizeTableMediaRecords;
		}

		public virtual void Save(ISystemUserFull systemUserFull, Collection<ISizeTableMediaRecord> sizeTableMediaRecords)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transaction = new TransactedOperation())
			{
				foreach (var sizeTableMediaRecord in sizeTableMediaRecords)
				{
					if (sizeTableMediaRecord.IsDeleted)
					{
						Delete(systemUserFull, sizeTableMediaRecord.Id);
					}
				}

				foreach (var sizeTableMediaRecord in sizeTableMediaRecords)
				{
					if (sizeTableMediaRecord.IsDeleted == false)
					{
						Save(systemUserFull, sizeTableMediaRecord);
					}
				}

				transaction.Complete();
			}
		}

		public virtual void Save(ISystemUserFull systemUserFull, ISizeTableMediaRecord sizeTableMediaRecord)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			if (sizeTableMediaRecord.IsDeleted)
			{
				throw new InvalidOperationException("Can't save sizeTableMediaRecord merked for deletion!");
			}

			using (var transaction = new TransactedOperation())
			{
				int sizeTableMediaId = Repository.Save(sizeTableMediaRecord);

				sizeTableMediaRecord.Id = sizeTableMediaId;

				foreach (var sizeTableMediaRegistry in sizeTableMediaRecord.MediaRegistries)
				{
					sizeTableMediaRegistry.SizeTableMediaId = sizeTableMediaId;
				}

				SizeTableMediaRegistrySecureService.Save(systemUserFull, sizeTableMediaRecord.MediaRegistries);

				sizeTableMediaRecord.SetUntouched();

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int sizeTableMediaRecordId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			Repository.Delete(sizeTableMediaRecordId);
		}
	}
}