﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerCategorySecureService : CategorySecureService, ILekmerCategorySecureService
	{
		protected IProductRegistryRestrictionCategorySecureService ProductRegistryRestrictionCategorySecureService { get; private set; }
		protected IIconSecureService IconSecureService { get; private set; }

		public LekmerCategorySecureService(
			IAccessValidator accessValidator,
			CategoryRepository repository,
			ICategorySiteStructureRegistrySecureService categorySiteStructureRegistrySecureService,
			IProductRegistryRestrictionCategorySecureService productRegistryRestrictionCategorySecureService,
			IIconSecureService iconSecureService)
				: base(accessValidator, repository, categorySiteStructureRegistrySecureService)
		{
			ProductRegistryRestrictionCategorySecureService = productRegistryRestrictionCategorySecureService;
			IconSecureService = iconSecureService;
		}

		public Collection<ITranslationGeneric> GetAllTranslationsByCategory(int id)
		{
			return ((LekmerCategoryRepository) Repository).GetAllTranslationsByCategory(id);
		}

		public Collection<ITranslationGeneric> GetAllPackageInfoTranslationsByCategory(int id)
		{
			return ((LekmerCategoryRepository) Repository).GetAllPackageInfoTranslationsByCategory(id);
		}

		public void Update(ISystemUserFull systemUserFull,
			ICategoryRecord category,
			IEnumerable<ITranslationGeneric> titleTranslations,
			IEnumerable<ITranslationGeneric> packageInfoTranslations,
			Collection<IProductRegistryRestrictionItem> registryRestrictions,
			Collection<int> icons)
		{
			if (titleTranslations == null)
			{
				throw new ArgumentNullException("titleTranslations");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentCategory);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				base.Update(systemUserFull, category);
				SaveTranslations(category.Id, titleTranslations, packageInfoTranslations);
				ProductRegistryRestrictionCategorySecureService.Save(systemUserFull, category.Id, registryRestrictions);
				IconSecureService.SaveCategoryIcon(systemUserFull, category.Id, icons);

				transactedOperation.Complete();
			}
		}

		private void SaveTranslations(int categoryId, IEnumerable<ITranslationGeneric> titleTranslations, IEnumerable<ITranslationGeneric> packageInfoTranslations)
		{
			foreach (var titleTranslation in titleTranslations)
			{
				var languageId = titleTranslation.LanguageId;
				var packageInfoTranslation = packageInfoTranslations.First(i => i.LanguageId == languageId);
				((LekmerCategoryRepository)Repository).SaveTranslation(categoryId, languageId, titleTranslation.Value, packageInfoTranslation.Value);
			}
		}
	}
}