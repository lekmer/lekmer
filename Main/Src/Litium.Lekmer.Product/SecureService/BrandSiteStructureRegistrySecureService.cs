using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class BrandSiteStructureRegistrySecureService : IBrandSiteStructureRegistrySecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BrandSiteStructureRegistryRepository Repository { get; private set; }

		public BrandSiteStructureRegistrySecureService(IAccessValidator accessValidator, BrandSiteStructureRegistryRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual IBrandSiteStructureRegistry Create()
		{
			var brand = IoC.Resolve<IBrandSiteStructureRegistry>();
			brand.Status = BusinessObjectStatus.New;
			return brand;
		}

		public virtual Collection<IBrandSiteStructureRegistry> GetAllByBrand(int brandId)
		{
			return Repository.GetAllByBrand(brandId);
		}


		public Collection<IBrandSiteStructureRegistry> GetAll()
		{
			return Repository.GetAll();
		}

		public virtual void Save(ISystemUserFull systemUserFull, IBrandSiteStructureRegistry registry)
		{
			if (registry == null) throw new ArgumentNullException("registry");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(registry);
				transactedOperation.Complete();
			}
		}

		public virtual void DeleteByContentNode(ISystemUserFull systemUserFull, int contentNodeId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteByContentNode(contentNodeId);
				transactedOperation.Complete();
			}
		}

		public virtual void DeleteByBrand(ISystemUserFull systemUserFull, int contentNodeId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteByBrand(contentNodeId);
				transactedOperation.Complete();
			}
		}
	}
}