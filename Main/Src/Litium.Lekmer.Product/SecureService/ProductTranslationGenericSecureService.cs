﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Core;
using Litium.Lekmer.Product.Repository;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;

namespace Litium.Lekmer.Product
{
	public class ProductTranslationGenericSecureService<T>
		where T : ProductTranslationGenericRepository
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected T Repository { get; private set; }

		public ProductTranslationGenericSecureService(IAccessValidator accessValidator, T repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual Collection<ITranslationGeneric> GetAllByProduct(int id)
		{
			return Repository.GetAllByProduct(id);
		}
		public virtual void Save(ISystemUserFull user, Collection<ITranslationGeneric> translations)
		{
			if (translations == null) throw new ArgumentNullException("translations");

			AccessValidator.ForceAccess(user, PrivilegeConstant.AssortmentProduct);
			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var translation in translations)
				{
					Repository.Save(user, translation);
				}
				transactedOperation.Complete();
			}
		}
	}
}
