﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerCategoryRepository : CategoryRepository
	{
		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual Collection<ITranslationGeneric> GetAllTranslationsByCategory(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerCategoryRepository.GetAllTranslationsByCategory");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCategoryTranslationGetAllByCategory]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITranslationGeneric> GetAllPackageInfoTranslationsByCategory(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerCategoryRepository.GetAllPackageInfoTranslationsByCategory");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pCategoryPackageInfoTranslationGetAllByCategory]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void SaveTranslation(int categoryId, int languageId, string title, string packageInfo)
		{
			var dbSettings = new DatabaseSetting("LekmerCategoryRepository.SaveTranslation");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("PackageInfo", packageInfo, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pCategoryTranslationSave]", parameters, dbSettings);
		}

		public override ICategoryRecord GetRecordById(int categoryId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("CategoryRepository.GetCategoryRecordById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].pCategoryGetRecordById", parameters, dbSettings))
			{
				return DataMapperResolver.Resolve<ICategoryRecord>(dataReader).ReadRow();
			}
		}

		public override ICategoryView GetViewById(IChannel channel, int categoryId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("CategoryRepository.GetViewById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].pCategoryGetViewById", parameters, dbSettings))
			{
				return DataMapperResolver.Resolve<ICategoryView>(dataReader).ReadRow();
			}
		}

		/// <summary>
		/// Just for test purposes. Should return only one category view in a collection.
		/// </summary>
		public virtual Collection<ICategoryView> GetViewByIdAll(IChannel channel, int categoryId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
			};

			DatabaseSetting dbSettings = new DatabaseSetting("CategoryRepository.GetViewByIdAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].pCategoryGetViewById", parameters, dbSettings))
			{
				return DataMapperResolver.Resolve<ICategoryView>(dataReader).ReadMultipleRows();
			}
		}

		public override Collection<ICategoryView> GetViewAll(IChannel channel)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("CategoryRepository.GetViewAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].pCategoryGetViewAll", parameters, dbSettings))
			{
				return DataMapperResolver.Resolve<ICategoryView>(dataReader).ReadMultipleRows();
			}
		}

		public override void Update(ICategoryRecord category)
		{
			if (category == null) throw new ArgumentNullException("category");

			var lekmerCategory = category as ILekmerCategoryRecord;
			if (lekmerCategory == null) throw new ArgumentException("category should be of ILekmerCategoryRecord type");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CategoryId", lekmerCategory.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("Title", lekmerCategory.Title, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("AllowMultipleSizesPurchase", lekmerCategory.AllowMultipleSizesPurchase, SqlDbType.Bit),
				ParameterHelper.CreateParameter("PackageInfo", lekmerCategory.PackageInfo, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("MonitorThreshold", lekmerCategory.MonitorThreshold, SqlDbType.Int),
				ParameterHelper.CreateParameter("MaxQuantityPerOrder", lekmerCategory.MaxQuantityPerOrder, SqlDbType.Int),
				ParameterHelper.CreateParameter("DeliveryTimeId", lekmerCategory.DeliveryTimeId, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("CategoryRepository.SaveByCategory");
			new DataHandler().ExecuteCommand("[lekmer].[pCategorySave]", parameters, dbSettings);
		}
	}
}