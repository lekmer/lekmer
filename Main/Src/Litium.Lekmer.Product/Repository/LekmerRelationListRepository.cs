﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerRelationListRepository : RelationListRepository
	{
		public virtual Collection<IRelationList> GetAllByProductAndType(int productId, string commonName)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CommonName", commonName, SqlDbType.VarChar)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerRelationListRepository.GetAllByProductAndType");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pRelationListGetAllByProductAndType]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual RelationListCollection SearchWithoutColorVariant(IRelationListSearchCriteria searchCriteria, int page, int pageSize, string sortBy, bool? sortByDescending)
		{
			if (searchCriteria == null) throw new ArgumentNullException("searchCriteria");
			
			int itemsCount = 0;
			var relationList = new RelationListCollection();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Title", !string.IsNullOrEmpty(searchCriteria.Title) ? searchCriteria.Title : (object) DBNull.Value, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("@RelationListTypeId", !string.IsNullOrEmpty(searchCriteria.TypeId) ? searchCriteria.TypeId : (object) DBNull.Value, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
				ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
				ParameterHelper.CreateParameter("SortBy", sortBy, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("SortDescending", sortByDescending, SqlDbType.Bit)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("RelationListRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pRelationListSearchWithoutColorVariant]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					relationList = CreateDataMapper(dataReader).ReadMultipleRows<RelationListCollection>();
				}
			}
			relationList.TotalCount = itemsCount;
			return relationList;
		}
	}
}