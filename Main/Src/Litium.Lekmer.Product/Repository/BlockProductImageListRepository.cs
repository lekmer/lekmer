﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockProductImageListRepository
	{
		protected virtual DataMapperBase<IBlockProductImageList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductImageList>(dataReader);
		}

		public virtual IBlockProductImageList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductImageListRepository.GetById");
			using (var dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductImageListGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductImageList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductImageListRepository.GetByIdSecure");
			using (var dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductImageListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockProductImageList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductImageGroupId", block.ProductImageGroupId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductImageListRepository.Save");
			new DataHandler().ExecuteCommand("[productlek].[pBlockProductImageListSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("BlockProductImageListRepository.Delete");
			new DataHandler().ExecuteCommand("[productlek].[pBlockProductImageListDelete]", parameters, dbSettings);
		}
	}
}
