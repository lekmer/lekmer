﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductShortDescriptionTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductShortDescriptionTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductShortDescriptionTranslationSave]";
			}
		}
	}
}
