﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SupplierRepository
	{
		protected virtual DataMapperBase<ISupplier> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISupplier>(dataReader);
		}

		public virtual Collection<ISupplier> Search(string searchCriteria)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchCriteria", searchCriteria, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("SupplierRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pSupplierSearch]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ISupplier> GetAll()
		{
			var dbSettings = new DatabaseSetting("SupplierRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pSupplierGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual ISupplier GetById(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("SupplierRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pSupplierGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual ISupplier GetByNo(string supplierNo)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SupplierNo", supplierNo, SqlDbType.VarChar)
			};
			var dbSettings = new DatabaseSetting("SupplierRepository.GetByNo");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pSupplierGetByNo]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(ISupplier supplier)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SupplierId", supplier.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("IsDropShip", supplier.IsDropShip, SqlDbType.Bit),
				ParameterHelper.CreateParameter("DropShipEmail", supplier.DropShipEmail, SqlDbType.VarChar)
			};
			var dbSettings = new DatabaseSetting("SupplierRepository.Save");
			new DataHandler().ExecuteCommand("[productlek].[pSupplierSave]", parameters, dbSettings);
		}
	}
}