﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockBrandProductListRepository
	{
		protected DataMapperBase<IBlockBrandProductList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBrandProductList>(dataReader);
		}

		public virtual void Save(IBlockBrandProductList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductSortOrderId", block.ProductSortOrderId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductList.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandProductListSave]", parameters, dbSettings);
		}

		public virtual IBlockBrandProductList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductListRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandProductListGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockBrandProductList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductListRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandProductListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductListRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandProductListDelete]", parameters, dbSettings);
		}
	}
}