using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class CategoryTagGroupRepository
	{
		protected virtual DataMapperBase<ICategoryTagGroup> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICategoryTagGroup>(dataReader);
		}

		public virtual void Save(int categoryId, int tagGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagGroupId", tagGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CategoryTagGroupRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pCategoryTagGroupSave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CategoryTagGroupRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[lekmer].[pCategoryTagGroupDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<ICategoryTagGroup> GetAllByCategory(int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CategoryTagGroupRepository.GetAllByCategory");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCategoryTagGroupGetAllByCategory]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}