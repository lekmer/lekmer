﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeTableRepository
	{
		protected virtual DataMapperBase<ISizeTable> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTable>(dataReader);
		}

		protected virtual DataMapperBase<ISizeTableTranslation> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableTranslation>(dataReader);
		}

		protected virtual DataMapperBase<ISizeTableRow> CreateRowDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableRow>(dataReader);
		}

		protected virtual DataMapperBase<ISizeTableCategoryBrandPair> CreateSizeTableCategoryBrandPairDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISizeTableCategoryBrandPair>(dataReader);
		}

		public virtual int Save(ISizeTable sizeTable)
		{
			if (sizeTable == null)
			{
				throw new ArgumentNullException("sizeTable");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTable.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeTableFolderId", sizeTable.SizeTableFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CommonName", sizeTable.CommonName, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Title", sizeTable.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", sizeTable.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Column1Title", sizeTable.Column1Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Column2Title", sizeTable.Column2Title, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeTableSave]", parameters, dbSettings);
		}

		public virtual void Delete(int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableDelete]", parameters, dbSettings);
		}

		public virtual ISizeTable GetById(int languageId, int sizeTableId)
		{
			ISizeTable sizeTable;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableGetById]", parameters, dbSettings))
			{
				sizeTable = CreateDataMapper(dataReader).ReadRow();
				if (dataReader.NextResult() && sizeTable != null)
				{
					sizeTable.Rows = CreateRowDataMapper(dataReader).ReadMultipleRows();
				}
			}

			return sizeTable;
		}
		public virtual ISizeTable GetByIdSecure(int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual int GetByProduct(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetByProduct");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeTableGetByProduct]", parameters, dbSettings);
		}

		public virtual Collection<ISizeTable> GetAllByFolder(int sizeTableFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableFolderId", sizeTableFolderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetAllByFolder");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableGetAllByFolder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ISizeTable> GetAll()
		{
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ISizeTable> Search(string searchCriteria)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchCriteria", searchCriteria, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableSearch]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Includes.

		public virtual void SaveIncludes(int sizeTableId, string productIdList, string categoryIdList, string brandIdList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductIds", productIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CategoryIds", categoryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandIds", brandIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.SaveIncludes");
			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableIncludesSave]", parameters, dbSettings);
		}

		public virtual ISizeTable GetAllIncludes(ISizeTable sizeTable)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTable.Id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetAllIncludes");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableIncludesGetAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					sizeTable.IncludeProducts.Add(dataReader.GetInt32(0));
				}
				if (dataReader.NextResult())
				{
					var sizeTableCategoryBrandPairs = CreateSizeTableCategoryBrandPairDataMapper(dataReader).ReadMultipleRows();
					sizeTable.IncludeCategoryBrandPairs.AddRange(sizeTableCategoryBrandPairs);
				}
			}
			return sizeTable;
		}

		// Translations.

		public virtual void SaveTranslations(ISizeTableTranslation translation)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", translation.SizeTableId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", translation.LanguageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", translation.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", translation.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Column1Title", translation.Column1Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Column2Title", translation.Column2Title, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.SaveTranslations");
			new DataHandler().ExecuteCommand("[lekmer].[pSizeTableTranslationSave]", parameters, dbSettings);
		}

		public virtual Collection<ISizeTableTranslation> GetAllTranslations(int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeTableRepository.GetAllTranslations");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeTableTranslationGetAll]", parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}