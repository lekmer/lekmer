﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductRegistryRestrictionBrandRepository
	{
		protected virtual DataMapperBase<IProductRegistryRestrictionItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductRegistryRestrictionItem>(dataReader);
		}

		public virtual void Save(IProductRegistryRestrictionItem productRegistryRestrictionBrand)
		{
			if (productRegistryRestrictionBrand == null)
			{
				throw new ArgumentNullException("productRegistryRestrictionBrand");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductRegistryId", productRegistryRestrictionBrand.ProductRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", productRegistryRestrictionBrand.ItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RestrictionReason", productRegistryRestrictionBrand.RestrictionReason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UserId", productRegistryRestrictionBrand.UserId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", productRegistryRestrictionBrand.CreatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionBrandRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryRestrictionBrandSave]", parameters, dbSettings);
		}

		public virtual void Delete(int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionBrandRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryRestrictionBrandDelete]", parameters, dbSettings);
		}

		public virtual Collection<IProductRegistryRestrictionItem> GetAllByBrand(int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionBrandRepository.GetAllByBrand");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryRestrictionBrandGetAllByBrand]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductRegistryRestrictionItem> GetAllWithChannel()
		{
			var dbSettings = new DatabaseSetting("ProductRegistryRestrictionBrandRepository.GetAllWithChannel");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryRestrictionBrandGetAllWithChannel]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}