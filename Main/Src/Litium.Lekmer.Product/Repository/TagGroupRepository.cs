using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class TagGroupRepository
	{
		protected virtual DataMapperBase<ITagGroup> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITagGroup>(dataReader);
		}

		public virtual ITagGroup GetById(int id)
		{
			var dbSettings = new DatabaseSetting("TagGroupRepository.GetById");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGroupGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<ITagGroup> GetAll()
		{
			var dbSettings = new DatabaseSetting("TagGroupRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGroupGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Dictionary<string, string> InfoPointTagGroupGetAll()
		{
			var infoPointTagGroups = new Dictionary<string, string>();

			var dbSettings = new DatabaseSetting("TagGroupRepository.InfoPointTagGroupGetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pInfoPointTagGroupGetAll]", dbSettings))
			{
				while (dataReader.Read())
				{
					var infoPointCommonName = dataReader.GetString(0);
					var tagGroupCommonName = dataReader.GetString(1);

					if (!infoPointTagGroups.ContainsKey(infoPointCommonName))
					{
						infoPointTagGroups.Add(infoPointCommonName, tagGroupCommonName);
					}
				}
			}

			return infoPointTagGroups;
		}
	}
}