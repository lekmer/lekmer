using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductRegistryProductRepository
	{
		public virtual void Insert(int? productId, int? brandId, int? categoryId, string productRegistryIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductRegistryIds", productRegistryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryProductRepository.Insert");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteByProduct(int productId, string productRegistryIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductRegistryIds", productRegistryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryProductRepository.DeleteByProduct");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryProductDeleteByProduct]", parameters, dbSettings);
		}

		public virtual void DeleteByBrand(int brandId, string productRegistryIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductRegistryIds", productRegistryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryProductRepository.DeleteByBrand");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryProductDeleteByBrand]", parameters, dbSettings);
		}

		public virtual void DeleteByCategory(int categoryId, string productRegistryIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductRegistryIds", productRegistryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryProductRepository.DeleteByCategory");

			new DataHandler().ExecuteCommand("[lekmer].[pProductRegistryProductDeleteByCategory]", parameters, dbSettings);
		}

		public virtual List<int> GetRestrictionsByProductId(int productId)
		{
			var registryIds = new List<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRegistryProductRepository.GetRestrictionsByProductId");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductRegistryProductGetRestrictionsByProductId]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					registryIds.Add(dataReader.GetInt32(0));
				}
			}

			return registryIds;
		}
	}
}