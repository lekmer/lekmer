using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class TagRepository
	{
		protected virtual DataMapperBase<ITag> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITag>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual ITag GetById(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("TagRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<ITag> GetAll(int languageId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("TagRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITag> GetAllSecure()
		{
			var dbSettings = new DatabaseSetting("TagRepository.GetAllSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllSecure]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITag> GetAllByTagGroup(int languageId, int tagGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagGroupId", tagGroupId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByTagGroup");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByTagGroup]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITag> GetAllByProduct(int languageId, int productId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByProduct");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
			};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITag> GetAllByTagGroupAndProduct(int languageId, int tagGroupId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagGroupId", tagGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByTagGroupAndProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByTagGroupAndProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITag> GetAllByTagGroupSecure(int tagGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagGroupId", tagGroupId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByTagGroupSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByTagGroupSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ITag> GetAllByProductAndTagGroupSecure(int productId, int tagGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagGroupId", tagGroupId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByProductAndTagGroupSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByProductAndTagGroupSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual IdCollection GetAllIdsByCategory(int categoryId)
		{
			var tagIds = new IdCollection();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("TagRepository.GetAllIdsByCategory");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllIdsByCategory]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					tagIds.Add(dataReader.GetInt32(0));
				}
			}

			return tagIds;
		}


		public virtual int Save(ITag tag)
		{
			var dbSettings = new DatabaseSetting("TagRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", tag.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagGroupId", tag.TagGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", tag.Value, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CommonName", tag.CommonName, SqlDbType.VarChar)
				};
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pTagSave]", parameters, dbSettings);
		}

		public virtual void Delete(int tagId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.Delete");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", tagId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pTagDelete]", parameters, dbSettings);
		}


		public virtual void AddAutoCampaignTag(int productId, int tagId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.AddAutoCampaignTag");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagId", tagId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductTagInsert]", parameters, dbSettings);
		}

		public virtual void DeleteProductTagsByProduct(int productId, string tagIdList, int objectId, char delimiter)
		{
			var dbSettings = new DatabaseSetting("TagRepository.DeleteProductTagsByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagIds", tagIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ObjectId", objectId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductTagsDeleteByProduct]", parameters, dbSettings);
		}

		public virtual void SaveProductTag(int productId, int tagId, int objectId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.SaveProductTag");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagId", tagId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ObjectId", objectId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductTagSave]", parameters, dbSettings);
		}

		public virtual Collection<int> GetAllProductTagIdsByCampaign(int campaignId)
		{
			var productTagIds = new Collection<int>();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("TagRepository.GetAllProductTagIdsByCampaign");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductTagsGetAllByCampaign]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productTagIds.Add(dataReader.GetInt32(0));
				}
			}

			return productTagIds;
		}

		public virtual void DeleteProductTagsUsage(int objectId, char delimiter)
		{
			var dbSettings = new DatabaseSetting("TagRepository.DeleteProductTagsUsage");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", null, SqlDbType.Int),
					ParameterHelper.CreateParameter("TagIds", string.Empty, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ObjectId", objectId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductTagUsageDelete]", parameters, dbSettings);
		}

		public virtual void DeleteProductTagsByIds(string productTagIdList, char delimiter)
		{
			var dbSettings = new DatabaseSetting("TagRepository.DeleteProductTagsByIds");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductTagIds", productTagIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductTagsDeleteByIds]", parameters, dbSettings);
		}


		public virtual Collection<ITranslationGeneric> GetAllTranslationsByTag(int id)
		{
			var dbSettings = new DatabaseSetting("TagRepository.GetAllTranslationsByTag");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", id, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagTranslationGetAllByTag]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void SaveTranslation(ITranslationGeneric translation)
		{
			var dbSettings = new DatabaseSetting("TagRepository.SaveTranslation");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", translation.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", translation.LanguageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", translation.Value, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pTagTranslationSave]", parameters, dbSettings);
		}


		// Tag-Flag
		public virtual void TagFlagSave(int tagId, int flagId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.TagFlagSave");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", tagId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FlagId", flagId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[productlek].[pTagFlagSave]", parameters, dbSettings);
		}
		public virtual void TagFlagDeleteByTag(int tagId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.TagFlagDeleteByTag");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", tagId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[productlek].[pTagFlagDeleteByTag]", parameters, dbSettings);
		}
	}
}