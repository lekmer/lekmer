﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductUrlHistoryRepository
	{
		protected DataMapperBase<IProductUrlHistory> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductUrlHistory>(dataReader);
		}


		public Collection<IProductUrlHistory> GetAllByProduct(int productId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.GetAllByProduct");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductUrlHistoryGetByProductId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<IProductUrlHistory> GetAllByLanguage(int languageId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.GetAllByLanguage");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductUrlHistoryGetAllByLanguageId]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<IProductUrlHistory> GetAllByUrlTitle(int languageId, string urlTitle)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.GetAllByUrlTitle");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitleOld", urlTitle, SqlDbType.NVarChar)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductUrlHistoryGetByUrlTitle]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}


		public int Save(IProductUrlHistory productUrlHistory)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.Save");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productUrlHistory.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", productUrlHistory.LanguageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitleOld", productUrlHistory.UrlTitleOld, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@HistoryLifeIntervalTypeId", productUrlHistory.TypeId, SqlDbType.Int)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pProductUrlHistorySave]", parameters, dbSettings);
		}


		public void Update(int productUrlHistoryId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductUrlHistoryId", productUrlHistoryId, SqlDbType.Int),
				};

			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlHistoryUpdate]", parameters, dbSettings);
		}


		public void DeleteById(int productUrlHistoryId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.DeleteById");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductUrlHistoryId", productUrlHistoryId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlHistoryDeleteById]", parameters, dbSettings);
		}

		public void DeleteAllByProduct(int productId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.DeleteAllByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlHistoryDeleteByProductId]", parameters, dbSettings);
		}

		public void DeleteAllByProductAndUrlTitle(int productId, int languageId, string urlTitle)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.DeleteAllByProductAndUrlTitle");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitleOld", urlTitle, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlHistoryDeleteByProductIdAndUrlTitle]", parameters, dbSettings);
		}


		public void DeleteExpiredItems()
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.DeleteExpiredItems");
			IDataParameter[] parameters = new IDataParameter[0];
			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlHistoryDeleteExpiredItems]", parameters, dbSettings);
		}

		public void CleanUp(int maxCount)
		{
			var dbSettings = new DatabaseSetting("ProductUrlHistoryRepository.CleanUp");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@MaxCount", maxCount, SqlDbType.Int),
				};

			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlHistoryCleanUp]", parameters, dbSettings);
		}
	}
}