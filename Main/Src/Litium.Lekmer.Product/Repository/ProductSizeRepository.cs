﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductSizeRepository
	{
		protected virtual DataMapperBase<IProductSize> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductSize>(dataReader);
		}

		protected virtual DataMapperBase<IProductSizeRelation> CreateProductSizeRelationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductSizeRelation>(dataReader);
		}

		public virtual void Save(IProductSize productSize)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productSize.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SizeId", productSize.SizeInfo.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@OverrideMillimeter", productSize.OverrideMillimeter, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MillimeterDeviation", productSize.MillimeterDeviation, SqlDbType.Int),
					ParameterHelper.CreateParameter("@OverrideEU", productSize.OverrideEu, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("ProductSizeRepository.Save");

			new DataHandler().ExecuteSelect("[lekmer].[pProductSizeSave]", parameters, dbSettings);
		}

		public virtual IProductSize GetById(int productId, int sizeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SizeId", sizeId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IProductSize> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductSize> GetAllByProductIdList(string productIdList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductIds", productIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetAllByProductIdList");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetAllByProductIdList]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductSizeRelation> GetAll()
		{
			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetAll]", dbSettings))
			{
				return CreateProductSizeRelationDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}