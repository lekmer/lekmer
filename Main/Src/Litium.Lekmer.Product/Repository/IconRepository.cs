﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class IconRepository
	{
		protected virtual DataMapperBase<IIcon> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IIcon>(dataReader);
		}
		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual int Save(IIcon icon)
		{
			if (icon == null)
			{
				throw new ArgumentNullException("icon");
			}

			var dbSettings = new DatabaseSetting("IconRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("IconId", icon.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", icon.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", icon.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("MediaId", icon.MediaId, SqlDbType.Int)
				};
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pIconSave]", parameters, dbSettings);
		}
		public virtual void Delete(int iconId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.Delete");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pIconDelete]", parameters, dbSettings);
		}
		public virtual IIcon GetByIdSecure(int iconId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetByIdSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pIconGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
		public Collection<IIcon> GetAll()
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pIconGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
		public virtual IconCollection GetAll(int page, int pageSize, string sortBy, bool? sortByDescending)
		{
			int numberOfItems = 0;
			var iconList = new IconCollection();

			var dbSettings = new DatabaseSetting("IconRepository.pIconGetAllWithSizes");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("PageSize", pageSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("SortBy", sortBy, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("SortDescending", sortByDescending, SqlDbType.Bit)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pIconGetAllWithSizes]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					numberOfItems = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					iconList = CreateDataMapper(dataReader).ReadMultipleRows<IconCollection>();
				}
			}
			iconList.TotalCount = numberOfItems;
			return iconList;
		}
		
		// Translations.
		public virtual void SaveTranslations(int iconId, int languageId, string title, string description)
		{
			var dbSettings = new DatabaseSetting("IconRepository.SaveTranslations");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", description, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pIconTranslationSave]", parameters, dbSettings);
		}
		public virtual Collection<ITranslationGeneric> GetAllTitleTranslations(int iconId)
		{
			return GetAllTranslations(iconId, "[lekmer].[pIconTranslationTitleGetAll]");
		}
		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int iconId)
		{
			return GetAllTranslations(iconId, "[lekmer].[pIconTranslationDescriptionGetAll]");
		}
		protected virtual Collection<ITranslationGeneric> GetAllTranslations(int iconId, string storedProcedureName)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAllTranslations");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(storedProcedureName, parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Product Icons.
		public void SaveProductIcon(int productId, int iconId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.SaveProductIcon");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pProductIconSave]", parameters, dbSettings);
		}
		public void DeleteAllByProduct(int productId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.DeleteAllByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pProductIconDeleteAll]", parameters, dbSettings);
		}
		public Collection<IIcon> GetAll(int productId, int categoryId, int? brandId, int languageId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAllByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductIconGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
		public Collection<IIcon> GetAllByProductSecure(int productId, int categoryId, int? brandId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAllByProductSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductIconGetAllSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
		public Collection<IIcon> GetAllByCategoryAndBrandSecure(int categoryId, int? brandId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAllByCategoryAndBrandSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductIconGetAllByCategoryAndBrandSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Brand Icons.
		public void SaveBrandIcon(int brandId, int iconId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.SaveBrandIcon");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pBrandIconSave]", parameters, dbSettings);
		}
		public void DeleteAllByBrand(int brandId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.DeleteAllByBrand");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pBrandIconDeleteAll]", parameters, dbSettings);
		}
		public Collection<IIcon> GetAllByBrandSecure(int brandId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAllByBrandSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandIconGetAllSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Category Icons.
		public void SaveCategoryIcon(int categoryId, int iconId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.SaveCategoryIcon");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IconId", iconId, SqlDbType.Int)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pCategoryIconSave]", parameters, dbSettings);
		}
		public void DeleteAllByCategory(int categoryId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.DeleteAllByCategory");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pCategoryIconDeleteAll]", parameters, dbSettings);
		}
		public Collection<IIcon> GetAllByCategorySecure(int categoryId)
		{
			var dbSettings = new DatabaseSetting("IconRepository.GetAllByCategorySecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCategoryIconGetAllSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}