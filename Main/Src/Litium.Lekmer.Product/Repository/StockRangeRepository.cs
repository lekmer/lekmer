using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class StockRangeRepository
	{
		protected virtual DataMapperBase<IStockRange> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IStockRange>(dataReader);
		}

		public Collection<IStockRange> GetAll()
		{
			var dbSettings = new DatabaseSetting("StockRangeRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pStockRangeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}