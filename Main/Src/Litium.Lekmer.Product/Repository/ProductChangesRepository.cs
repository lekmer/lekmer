using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductChangesRepository
	{
		protected virtual DataMapperBase<IProductChangeEvent> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductChangeEvent>(dataReader);
		}


		public virtual int Insert(IProductChangeEvent productChangeEvent)
		{
			if (productChangeEvent == null)
			{
				throw new ArgumentNullException("productChangeEvent");
			}

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductId", productChangeEvent.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("EventStatusId", productChangeEvent.EventStatusId, SqlDbType.Int),
				ParameterHelper.CreateParameter("CdonExportEventStatusId", productChangeEvent.CdonExportEventStatusId, SqlDbType.Int),
				ParameterHelper.CreateParameter("CreatedDate", productChangeEvent.CreatedDate, SqlDbType.DateTime),
				ParameterHelper.CreateParameter("ActionAppliedDate", productChangeEvent.ActionAppliedDate.HasValue() ? (object) productChangeEvent.ActionAppliedDate : null, SqlDbType.DateTime),
				ParameterHelper.CreateParameter("Reference", productChangeEvent.Reference, SqlDbType.NVarChar),
			};

			var dbSettings = new DatabaseSetting("ProductChangesRepository.Insert");

			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pProductChangeEventInsert]", parameters, dbSettings);
		}

		public virtual Collection<IProductChangeEvent> GetAll(int numberOfItems, ProductChangeEventStatus productChangeEventStatus)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("NumberOfItems", numberOfItems, SqlDbType.Int),
				ParameterHelper.CreateParameter("EventStatusId", productChangeEventStatus, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("ProductChangesRepository.GetAllInQueue");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductChangeEventGetAllInQueue]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IProductChangeEvent> GetCdonExportAll(int numberOfItems, ProductChangeEventStatus productChangeCdonExportEventStatus)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("NumberOfItems", numberOfItems, SqlDbType.Int),
				ParameterHelper.CreateParameter("CdonExportEventStatusId", productChangeCdonExportEventStatus, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("ProductChangesRepository.GetCdonExportAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductChangeCdonExportEventGetAllInQueue]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void SetStatusByIdList(string idList, char delimiter, ProductChangeEventStatus status, DateTime actionAppliedDate)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ProductChangeEventIds", idList, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1),
				ParameterHelper.CreateParameter("EventStatusId", status, SqlDbType.Int),
				ParameterHelper.CreateParameter("ActionAppliedDate", actionAppliedDate.HasValue() ? (object) actionAppliedDate : null, SqlDbType.DateTime)
			};

			var dbSettings = new DatabaseSetting("ProductChangesRepository.SetStatusByIdList");

			new DataHandler().ExecuteCommand("[productlek].[pProductChangeEventSetStatusByIdList]", parameters, dbSettings);
		}

		public virtual void SetCdonExportStatusByIdList(string idList, char delimiter, ProductChangeEventStatus status, DateTime actionAppliedDate)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductChangeEventIds", idList, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1),
				ParameterHelper.CreateParameter("CdonExportEventStatusId", status, SqlDbType.Int),
				ParameterHelper.CreateParameter("ActionAppliedDate", actionAppliedDate.HasValue() ? (object) actionAppliedDate : null, SqlDbType.DateTime)
			};

			var dbSettings = new DatabaseSetting("ProductChangesRepository.SetCdonExportStatusByIdList");

			new DataHandler().ExecuteCommand("[productlek].[pProductChangeEventSetCdonExportStatusByIdList]", parameters, dbSettings);
		}

		public virtual void DeleteExpiredItems()
		{
			var parameters = new IDataParameter[0];
			var dbSettings = new DatabaseSetting("ProductChangesRepository.DeleteExpiredItems");
			new DataHandler().ExecuteCommand("[productlek].[pProductChangeEventDeleteExpiredItems]", parameters, dbSettings);
		}
	}
}