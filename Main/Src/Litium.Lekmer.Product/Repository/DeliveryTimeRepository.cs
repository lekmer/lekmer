﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class DeliveryTimeRepository
	{
		protected virtual DataMapperBase<IDeliveryTime> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IDeliveryTime>(dataReader);
		}

		public virtual int Insert()
		{
			var dbSettings = new DatabaseSetting("DeliveryTimeRepository.Insert");
			return new DataHandler().ExecuteCommandWithReturnValue("[productlek].[pDeliveryTimeInsert]", new IDataParameter[0], dbSettings);
		}

		public virtual void Delete(int id)
		{
			var dbSettings = new DatabaseSetting("DeliveryTimeRepository.Delete");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[productlek].[pDeliveryTimeDelete]", parameters, dbSettings);
		}


		public virtual void SaveProductRegistryDeliveryTime(int id, int? fromDays, int? toDays, string format, int? productRegistryId)
		{
			var dbSettings = new DatabaseSetting("DeliveryTimeRepository.SaveProductRegistryDeliveryTime");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductRegistryId", productRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FromDays", fromDays, SqlDbType.Int),
					ParameterHelper.CreateParameter("ToDays", toDays, SqlDbType.Int),
					ParameterHelper.CreateParameter("Format", format, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[productlek].[pProductRegistryDeliveryTimeSave]", parameters, dbSettings);
		}

		public virtual void DeleteProductRegistryDeliveryTime(int id)
		{
			var dbSettings = new DatabaseSetting("DeliveryTimeRepository.DeleteProductRegistryDeliveryTime");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[productlek].[pProductRegistryDeliveryTimeDelete]", parameters, dbSettings);
		}

		public virtual Collection<IDeliveryTime> GetAllProductRegistryDeliveryTimeById(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("DeliveryTimeId", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("DeliveryTimeRepository.GetAllProductRegistryDeliveryTimeById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pProductRegistryDeliveryTimeGetAllById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}