﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockProductRelationListRepository
	{
		protected virtual DataMapperBase<IBlockProductRelationList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductRelationList>(dataReader);
		}

		public virtual IBlockProductRelationList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductRelationListRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductRelationListGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductRelationList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductRelationListRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pBlockProductRelationListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockProductRelationList block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductSortOrderId", block.ProductSortOrder.Id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductRelationListRepository.Save");
			new DataHandler().ExecuteCommand("[productlek].[pBlockProductRelationListSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("BlockProductRelationListRepository.Delete");
			new DataHandler().ExecuteCommand("[productlek].[pBlockProductRelationListDelete]", parameters, dbSettings);
		}
	}
}
