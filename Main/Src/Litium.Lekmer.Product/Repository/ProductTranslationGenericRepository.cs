﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using System.Data;
using System.Collections.ObjectModel;
using Litium.Framework.DataAccess;

namespace Litium.Lekmer.Product.Repository
{
	public abstract class ProductTranslationGenericRepository
	{
		protected virtual DataMapperBase<ITranslationGeneric> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}
		public abstract string GetAllByProductStoredProcedureName { get; }
		public abstract string SaveStoredProcedureName { get; }

		public virtual Collection<ITranslationGeneric> GetAllByProduct(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("ProductTranslationGenericRepository.GetAllByProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(GetAllByProductStoredProcedureName, parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
		public virtual void Save(ISystemUserFull user, ITranslationGeneric translation)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", translation.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", translation.LanguageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Value", translation.Value, SqlDbType.NVarChar)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("ProductTranslationGenericRepository.Save");
			new DataHandler().ExecuteCommand(SaveStoredProcedureName, parameters, dbSettings);
		}
	}
}
