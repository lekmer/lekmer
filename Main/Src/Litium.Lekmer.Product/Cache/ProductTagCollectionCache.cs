﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductTagCollectionCache : ScensumCacheBase<ProductTagCollectionKey, Collection<ITag>>
	{
		#region Singleton

		private static readonly ProductTagCollectionCache _instance = new ProductTagCollectionCache();

		private ProductTagCollectionCache()
		{
		}

		public static ProductTagCollectionCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int productId)
		{
			foreach (var channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new ProductTagCollectionKey(channel.Id, productId));
			}
		}

		#endregion
	}

	public class ProductTagCollectionKey : ICacheKey
	{
		public int ChannelId { get; set; }
		public int ProductId { get; set; }

		public ProductTagCollectionKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}