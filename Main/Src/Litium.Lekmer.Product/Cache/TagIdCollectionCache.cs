using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class TagIdCollectionCache : ScensumCacheBase<TagIdCollectionKey, IdCollection>
	{
		#region Singleton

		private static readonly TagIdCollectionCache _instance = new TagIdCollectionCache();

		private TagIdCollectionCache()
		{
		}

		public static TagIdCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class TagIdCollectionKey : ICacheKey
	{
		public int CategoryId { get; set; }

		public TagIdCollectionKey(int categoryId)
		{
			CategoryId = categoryId;
		}

		public string Key
		{
			get { return CategoryId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}