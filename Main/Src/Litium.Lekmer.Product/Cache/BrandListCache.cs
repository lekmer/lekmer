﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BrandListCache : ScensumCacheBase<BrandListKey, BrandCollection>
	{
		// Singleton

		private static readonly BrandListCache _brandCache = new BrandListCache();

		private BrandListCache()
		{
		}

		public static BrandListCache Instance
		{
			get { return _brandCache; }
		}

		// End of Singleton
	}

	public class BrandListKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public BrandListKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return "c_" + ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}