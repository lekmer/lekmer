using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductVariantCache : ScensumCacheBase<ProductVariantKey, ProductIdCollection>
	{
		#region Singleton

		private static readonly ProductVariantCache _instance = new ProductVariantCache();

		private ProductVariantCache()
		{
		}

		public static ProductVariantCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class ProductVariantKey : ICacheKey
	{
		public ProductVariantKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return string.Format("ProductVariant_{0}", ProductId); }
		}
	}
}