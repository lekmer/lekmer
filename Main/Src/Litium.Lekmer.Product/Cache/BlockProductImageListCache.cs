﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IBlockProductImageList"/>.
	/// </summary>
	public sealed class BlockProductImageListCache : ScensumCacheBase<BlockProductImageListKey, IBlockProductImageList>
	{
		private static readonly BlockProductImageListCache _instance = new BlockProductImageListCache();

		private BlockProductImageListCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static BlockProductImageListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockProductImageListKey(channel.Id, blockId));
			}
		}
	}

	/// <summary>
	/// Key used for caching <see cref="IBlockProductImageList"/>.
	/// </summary>
	public class BlockProductImageListKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="channelId">Channel id of the <see cref="IBlockProductImageList"/>.</param>
		/// <param name="blockId">Id of the <see cref="IBlockProductImageList"/>.</param>
		public BlockProductImageListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		/// <summary>
		/// Channel id of the <see cref="IBlockProductImageList"/>.
		/// </summary>
		public int ChannelId { get; set; }

		/// <summary>
		/// Id of the <see cref="IBlockProductImageList"/>.
		/// </summary>
		public int BlockId { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
