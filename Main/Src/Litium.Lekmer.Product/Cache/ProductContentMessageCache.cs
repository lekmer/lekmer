﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product
{
	public sealed class ProductContentMessageCache : ScensumCacheBase<ProductContentMessageCacheKey, Collection<IContentMessage>>
	{
		private static readonly ProductContentMessageCache _instance = new ProductContentMessageCache();

		private ProductContentMessageCache()
		{
		}

		public static ProductContentMessageCache Instance
		{
			get { return _instance; }
		}
	}

	public class ProductContentMessageCacheKey : ICacheKey
	{
		public ProductContentMessageCacheKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public int ChannelId { get; set; }

		public int ProductId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					ProductId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}