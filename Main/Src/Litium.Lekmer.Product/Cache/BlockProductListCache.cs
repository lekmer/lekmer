﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IBlockProductList"/>.
	/// </summary>
	public sealed class BlockProductListCache : ScensumCacheBase<BlockProductListKey, IBlockProductList>
	{
		private static readonly BlockProductListCache _instance = new BlockProductListCache();

		private BlockProductListCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static BlockProductListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockProductListKey(channel.Id, blockId));
			}
		}
	}

	/// <summary>
	/// Key used for caching <see cref="IBlockProductList"/>.
	/// </summary>
	public class BlockProductListKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="channelId">Channel id of the <see cref="IBlockProductList"/>.</param>
		/// <param name="blockId">Id of the <see cref="IBlockProductList"/>.</param>
		public BlockProductListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		/// <summary>
		/// Channel id of the <see cref="IBlockProductList"/>.
		/// </summary>
		public int ChannelId { get; set; }

		/// <summary>
		/// Id of the <see cref="IBlockProductList"/>.
		/// </summary>
		public int BlockId { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
