﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductRelationCache : ScensumCacheBase<ProductRelationKey, ProductIdCollection>
	{
		// Singleton

		private static readonly ProductRelationCache _instance = new ProductRelationCache();

		private ProductRelationCache()
		{
		}

		public static ProductRelationCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton
	}

	public class ProductRelationKey : ICacheKey
	{
		public ProductRelationKey(int productId, string relationType)
		{
			ProductId = productId;
			RelationType = relationType;
		}

		public int ProductId { get; set; }
		public string RelationType { get; set; }

		public string Key
		{
			get { return string.Format("ProductRelation_{0}_{1}", ProductId, RelationType); }
		}
	}
}
