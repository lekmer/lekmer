﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockBrandProductListCache : ScensumCacheBase<BlockBrandProductListKey, IBlockBrandProductList>
	{
		private static readonly BlockBrandProductListCache _instance = new BlockBrandProductListCache();

		private BlockBrandProductListCache()
		{
		}

		public static BlockBrandProductListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockBrandProductListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockBrandProductListKey : ICacheKey
	{
		public BlockBrandProductListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}