﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableMediaRegistry : BusinessObjectBase, ISizeTableMediaRegistry
	{
		private int _id;
		private int _sizeTableMediaId;
		private int _productRegistryId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int SizeTableMediaId
		{
			get { return _sizeTableMediaId; }
			set
			{
				CheckChanged(_sizeTableMediaId, value);
				_sizeTableMediaId = value;
			}
		}

		public int ProductRegistryId
		{
			get { return _productRegistryId; }
			set
			{
				CheckChanged(_productRegistryId, value);
				_productRegistryId = value;
			}
		}
	}
}