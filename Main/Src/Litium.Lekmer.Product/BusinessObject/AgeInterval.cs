using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class AgeInterval : BusinessObjectBase, IAgeInterval
	{
		private int _id;
		private string _title;
		private int _fromMonth;
		private int _toMonth;
		private int _ordinal;
		private int? _contentNodeId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}
		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
		public int FromMonth
		{
			get { return _fromMonth; }
			set
			{
				CheckChanged(_fromMonth, value);
				_fromMonth = value;
			}
		}
		public int ToMonth
		{
			get { return _toMonth; }
			set
			{
				CheckChanged(_toMonth, value);
				_toMonth = value;
			}
		}
		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
		public int? ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}
	}
}