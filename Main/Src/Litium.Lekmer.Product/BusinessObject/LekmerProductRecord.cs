﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerProductRecord : LekmerProduct, ILekmerProductRecord
	{
		private string _description;
		private int _ageFromMonth;
		private int _ageToMonth;
		private string _measurement;
		private int? _batteryTypeId;
		private int? _numberOfBatteries;
		private bool _isBatteryIncluded;
		private DateTime? _expectedBackInStock;
		private int? _sizeDeviationId;
		private string _supplierId;
		private decimal? _purchasePrice;
		private int? _purchaseCurrencyId;
		private string _supplierArticleNumber;
		private decimal? _averagePrice;


		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public Collection<IProductSiteStructureRegistry> ProductSiteStructureRegistries
		{
			get;
			set;
		}

		public int AgeFromMonth
		{
			get { return _ageFromMonth; }
			set
			{
				CheckChanged(_ageFromMonth, value);
				_ageFromMonth = value;
			}
		}

		public int AgeToMonth
		{
			get { return _ageToMonth; }
			set
			{
				CheckChanged(_ageToMonth, value);
				_ageToMonth = value;
			}
		}

		public string Measurement
		{
			get { return _measurement; }
			set
			{
				CheckChanged(_measurement, value);
				_measurement = value;
			}
		}

		public int? BatteryTypeId
		{
			get { return _batteryTypeId; }
			set
			{
				CheckChanged(_batteryTypeId, value);
				_batteryTypeId = value;
			}
		}

		public int? NumberOfBatteries
		{
			get { return _numberOfBatteries; }
			set
			{
				CheckChanged(_numberOfBatteries, value);
				_numberOfBatteries = value;
			}
		}

		public bool IsBatteryIncluded
		{
			get { return _isBatteryIncluded; }
			set
			{
				CheckChanged(_isBatteryIncluded, value);
				_isBatteryIncluded = value;
			}
		}

		public Collection<IIcon> Icons
		{
			get;
			set;
		}

		public DateTime? ExpectedBackInStock
		{
			get { return _expectedBackInStock; }
			set
			{
				CheckChanged(_expectedBackInStock, value);
				_expectedBackInStock = value;
			}
		}

		public int? SizeDeviationId
		{
			get { return _sizeDeviationId; }
			set
			{
				CheckChanged(_sizeDeviationId, value);
				_sizeDeviationId = value;
			}
		}

		public string SupplierId
		{
			get { return _supplierId; }
			set
			{
				CheckChanged(_supplierId, value);
				_supplierId = value;
			}
		}

		public decimal? PurchasePrice
		{
			get { return _purchasePrice; }
			set
			{
				CheckChanged(_purchasePrice, value);
				_purchasePrice = value;
			}
		}

		public int? PurchaseCurrencyId
		{
			get { return _purchaseCurrencyId; }
			set
			{
				CheckChanged(_purchaseCurrencyId, value);
				_purchaseCurrencyId = value;
			}
		}

		public decimal? AveragePrice
		{
			get { return _averagePrice; }
			set
			{
				CheckChanged(_averagePrice, value);
				_averagePrice = value;
			}
		}
	}
}