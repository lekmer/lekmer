﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Icon : BusinessObjectBase, IIcon
	{
		private int _id;
		private string _title;
		private string _description;
		private IImage _image;
		private int _mediaId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public int MediaId
		{
			get { return _mediaId; }
			set
			{
				CheckChanged(_mediaId, value);
				_mediaId = value;
			}
		}

		public IImage Image
		{
			get { return _image; }
			set
			{
				CheckChanged(_image, value);
				_image = value;
			}
		}
	}
}