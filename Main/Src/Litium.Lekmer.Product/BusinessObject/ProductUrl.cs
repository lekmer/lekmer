﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductUrl : BusinessObjectBase, IProductUrl
	{
		private int _productId;
		private int _languageId;
		private string _urlTitle;

		public int ProductId
		{
			get { return _productId; }
			set 
			{
				CheckChanged(_productId, value); 
				_productId = value; 
			}
		}
		public int LanguageId
		{
			get { return _languageId; }
			set
			{
				CheckChanged(_languageId, value);
				_languageId = value;
			}
		}
		public string UrlTitle
		{
			get { return _urlTitle; }
			set
			{
				CheckChanged(_urlTitle, value);
				_urlTitle = value;
			}
		}
	}
}
