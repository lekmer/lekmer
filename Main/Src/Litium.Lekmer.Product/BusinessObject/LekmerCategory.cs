﻿using System;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerCategory : Category, ILekmerCategory
	{
		private bool? _allowMultipleSizesPurchase;
		private string _packageInfo;
		private int? _monitorThreshold;
		private int? _maxQuantityPerOrder;
		private int? _minQuantityInStock;
		private int? _deliveryTimeId;
		private IDeliveryTime _deliveryTime;

		public bool? AllowMultipleSizesPurchase
		{
			get { return _allowMultipleSizesPurchase; }
			set
			{
				CheckChanged(_allowMultipleSizesPurchase, value);
				_allowMultipleSizesPurchase = value;
			}
		}
		public string PackageInfo
		{
			get { return _packageInfo; }
			set
			{
				CheckChanged(_packageInfo, value);
				_packageInfo = value;
			}
		}

		/// <summary>
		/// The minimum number in stock value for sending monitor product mails
		/// </summary>
		public int? MonitorThreshold
		{
			get { return _monitorThreshold; }
			set
			{
				CheckChanged(_monitorThreshold, value);
				_monitorThreshold = value;
			}
		}

		/// <summary>
		/// The maximum quantity of product allowed in a single order
		/// </summary>
		public int? MaxQuantityPerOrder
		{
			get { return _maxQuantityPerOrder; }
			set
			{
				CheckChanged(_maxQuantityPerOrder, value);
				_maxQuantityPerOrder = value;
			}
		}

		/// <summary>
		/// will only be shown as in stock in the store if there are equval or more in actual stock
		/// </summary>
		public int? MinQuantityInStock
		{
			get { return _minQuantityInStock ?? 1; }
			set
			{
				CheckChanged(_minQuantityInStock, value);
				_minQuantityInStock = value;
			}
		}

		public int? DeliveryTimeId
		{
			get { return _deliveryTimeId; }
			set
			{
				CheckChanged(_deliveryTimeId, value);
				_deliveryTimeId = value;
			}
		}

		public IDeliveryTime DeliveryTime
		{
			get { return _deliveryTime; }
			set
			{
				CheckChanged(_deliveryTime, value);
				_deliveryTime = value;
			}
		}
	}
}