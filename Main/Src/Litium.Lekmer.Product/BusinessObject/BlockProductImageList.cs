﻿using System;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockProductImageList : BlockBase, IBlockProductImageList
	{
		private int? _productImageGroupId;
		private IBlockSetting _setting;

		public int? ProductImageGroupId
		{
			get { return _productImageGroupId; }
			set
			{
				CheckChanged(_productImageGroupId, value);
				_productImageGroupId = value;
			}
		}

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}
