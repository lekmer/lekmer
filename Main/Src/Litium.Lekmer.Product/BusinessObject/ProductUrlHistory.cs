﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductUrlHistory : BusinessObjectBase, IProductUrlHistory
	{
		private int _id;
		private int _productId;
		private int _languageId;
		private string _urlTitleOld;
		private DateTime _creationDate;
		private DateTime _lastUsageDate;
		private int _usageAmount;
		private int _typeId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set 
			{
				CheckChanged(_productId, value); 
				_productId = value; 
			}
		}

		public int LanguageId
		{
			get { return _languageId; }
			set
			{
				CheckChanged(_languageId, value);
				_languageId = value;
			}
		}

		public string UrlTitleOld
		{
			get { return _urlTitleOld; }
			set
			{
				CheckChanged(_urlTitleOld, value);
				_urlTitleOld = value;
			}
		}

		public DateTime CreationDate
		{
			get { return _creationDate; }
			set
			{
				CheckChanged(_creationDate, value);
				_creationDate = value;
			}
		}

		public DateTime LastUsageDate
		{
			get { return _lastUsageDate; }
			set
			{
				CheckChanged(_lastUsageDate, value);
				_lastUsageDate = value;
			}
		}

		public int UsageAmount
		{
			get { return _usageAmount; }
			set
			{
				CheckChanged(_usageAmount, value);
				_usageAmount = value;
			}
		}

		public int TypeId
		{
			get { return _typeId; }
			set
			{
				CheckChanged(_typeId, value);
				_typeId = value;
			}
		}
	}
}