using System;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductTag : IProductTag
	{
		public int TagId { get; set; }
		public int ProductId { get; set; }
	}
}