using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class StockRange : BusinessObjectBase, IStockRange
	{
		private int _stockRangeId;
		private string _commonName;
		private int? _startValue;
		private int? _endValue;

		public int StockRangeId
		{
			get { return _stockRangeId; }
			set
			{
				CheckChanged(_stockRangeId, value);
				_stockRangeId = value;
			}
		}
		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}
		public int? StartValue
		{
			get { return _startValue; }
			set
			{
				CheckChanged(_startValue, value);
				_startValue = value;
			}
		}
		public int? EndValue
		{
			get { return _endValue; }
			set
			{
				CheckChanged(_endValue, value);
				_endValue = value;
			}
		}
	}
}