using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class AgeIntervalService : IAgeIntervalService
	{
		protected AgeIntervalRepository Repository { get; private set; }

		public AgeIntervalService(AgeIntervalRepository repository)
		{
			Repository = repository;
		}

		public virtual IAgeIntervalBand GetAgeInterval(int? fromMonth, int? toMonth)
		{
			var ageInterval = IoC.Resolve<IAgeIntervalBand>();

			ageInterval.FromMonth = fromMonth;
			ageInterval.ToMonth = toMonth;

			return ageInterval;
		}

		public virtual IAgeIntervalBand GetAgeInterval(IUserContext context, int ageIntervalId)
		{
			IAgeInterval ageInterval = GetAll(context).FirstOrDefault(a => a.Id == ageIntervalId);

			var ageIntervalBand = IoC.Resolve<IAgeIntervalBand>();

			if (ageInterval != null)
			{
				ageIntervalBand.FromMonth = ageInterval.FromMonth;
				ageIntervalBand.ToMonth = ageInterval.ToMonth;
			}

			return ageIntervalBand;
		}

		public virtual IAgeIntervalBand GetAgeIntervalOuter(Collection<IAgeInterval> ageIntervals)
		{
			var ageInterval = IoC.Resolve<IAgeIntervalBand>();

			ageInterval.FromMonth = ageIntervals.Min(a => a.FromMonth);
			ageInterval.ToMonth = ageIntervals.Max(a => a.ToMonth);

			return ageInterval;
		}

		public virtual Collection<IAgeInterval> GetAll(IUserContext context)
		{
			return AgeIntervalListCache.Instance.TryGetItem(
				new AgeIntervalListKey(context.Channel.Id),
				delegate { return Repository.GetAll(context.Channel.Language.Id); });
		}

		public virtual IEnumerable<IAgeInterval> GetAllMatching(IUserContext context, int ageFromMonth, int ageToMonth)
		{
			return GetAll(context)
				.Where(interval => interval.FromMonth <= ageToMonth && interval.ToMonth >= ageFromMonth);
		}
	}
}