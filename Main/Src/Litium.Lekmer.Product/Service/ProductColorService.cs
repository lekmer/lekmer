using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class ProductColorService : IProductColorService
	{
		protected ProductColorRepository Repository { get; private set; }

		public ProductColorService(ProductColorRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IProductColor> GetAll()
		{
			return Repository.GetAll();
		}

		public virtual Collection<IProductColor> GetAllByProductErpId(string erpId)
		{
			return Repository.GetAllByProductErpId(erpId);
		}
	}
}