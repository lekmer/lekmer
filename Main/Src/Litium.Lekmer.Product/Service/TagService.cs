using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class TagService : ITagService
	{
		protected TagRepository Repository { get; private set; }

		public TagService(TagRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<ITag> GetAll(IUserContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return TagCollectionCache.Instance.TryGetItem(
				new TagCollectionKey(context.Channel.Id),
				() => Repository.GetAll(context.Channel.Language.Id));
		}

		public virtual Collection<ITag> GetAllByTagGroup(IUserContext context, int tagGroupId)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return TagGroupTagCache.Instance.TryGetItem(
				new TagGroupTagKey(context.Channel.Id, tagGroupId),
				() => Repository.GetAllByTagGroup(context.Channel.Language.Id, tagGroupId));
		}

		public virtual Collection<ITag> GetAllByProduct(IUserContext context, int productId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return ProductTagCollectionCache.Instance.TryGetItem(
				new ProductTagCollectionKey(context.Channel.Id, productId),
				() => Repository.GetAllByProduct(context.Channel.Language.Id, productId));
		}

		public virtual Collection<ITag> GetAllByTagGroupAndProduct(IUserContext context, int tagGroupId, int productId)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			return Repository.GetAllByTagGroupAndProduct(context.Channel.Language.Id, tagGroupId, productId);
		}

		public virtual IdCollection GetAllIdsByCategory(int categoryId)
		{
			return TagIdCollectionCache.Instance.TryGetItem(
				new TagIdCollectionKey(categoryId),
				() => Repository.GetAllIdsByCategory(categoryId));
		}

		public void SaveProductTag(int productId, int tagId, int objectId)
		{
			Repository.SaveProductTag(productId, tagId, objectId);
		}

		public Collection<int> GetAllProductTagIdsByCampaign(int campaignId)
		{
			return Repository.GetAllProductTagIdsByCampaign(campaignId);
		}

		public void DeleteProductTagsUsage(int objectId)
		{
			Repository.DeleteProductTagsUsage(objectId, Convert.DefaultListSeparator);
		}

		public void DeleteProductTagsByIds(Collection<int> productTagIdList)
		{
			Repository.DeleteProductTagsByIds(Convert.ToStringIdentifierList(productTagIdList), Convert.DefaultListSeparator);
		}
	}
}