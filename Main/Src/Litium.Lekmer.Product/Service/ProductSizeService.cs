﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductSizeService : IProductSizeService
	{
		protected ProductSizeRepository Repository { get; private set; }

		public ProductSizeService(ProductSizeRepository repository)
		{
			Repository = repository;
		}

		public virtual IProductSize GetById(int productId, int sizeId)
		{
			return ProductSizeCache.Instance.TryGetItem(
				new ProductSizeKey(productId, sizeId),
				() => Repository.GetById(productId, sizeId));
		}

		public virtual Collection<IProductSize> GetAllByProduct(int productId)
		{
			return ProductSizeCollectionCache.Instance.TryGetItem(
				new ProductSizeCollectionKey(productId),
				() => Repository.GetAllByProduct(productId));
		}

		public virtual Collection<IProductSize> GetAllByProductIdList(IEnumerable<int> productIds)
		{
			return Repository.GetAllByProductIdList(Convert.ToStringIdentifierList(productIds), Convert.DefaultListSeparator);
		}

		public virtual Collection<IProductSizeRelation> GetAll()
		{
			return Repository.GetAll();
		}
	}
}