using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class ProductImageService : IProductImageService
	{
		protected LekmerProductImageRepository Repository { get; private set; }

		public ProductImageService(ProductImageRepository repository)
		{
			Repository = (LekmerProductImageRepository) repository;
		}

		public virtual Collection<IProductImage> GetAllByProduct(int productId)
		{
			return ProductImageCollectionCache.Instance.TryGetItem(
				new ProductImageCollectionKey(productId),
				() => Repository.GetAllByProduct(productId)
			);
		}
	}
}