﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockReviewMyWishListService : IBlockReviewMyWishListService
	{
		protected BlockReviewMyWishListRepository Repository { get; private set; }

		public BlockReviewMyWishListService(
			BlockReviewMyWishListRepository repository)
		{
			Repository = repository;
		}

		public IBlockReviewMyWishList GetById(IUserContext context, int id)
		{
			return BlockReviewMyWishListCache.Instance.TryGetItem(
				new BlockReviewMyWishListKey(context.Channel.Id, id),
				delegate { return Repository.GetById(context.Channel, id); });
		}
	}
}