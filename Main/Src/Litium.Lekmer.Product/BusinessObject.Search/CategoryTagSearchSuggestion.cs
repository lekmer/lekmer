using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class CategoryTagSearchSuggestion : ICategoryTagSearchSuggestion
	{
		public CategoryCollection ParentCategories { get; set; }

		public ICategory Category { get; set; }

		public string CategoryTicket { get; set; }

		public Collection<ITag> Tags { get; set; }
	}
}