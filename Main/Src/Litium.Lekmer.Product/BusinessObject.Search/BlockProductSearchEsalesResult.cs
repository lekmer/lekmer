﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockProductSearchEsalesResult : BlockBase, IBlockProductSearchEsalesResult
	{
		private int? _secondaryTemplateId;

		/// <summary>
		/// Secondary template id for block.
		/// </summary>
		public int? SecondaryTemplateId
		{
			get { return _secondaryTemplateId; }
			set
			{
				CheckChanged(_secondaryTemplateId, value);
				_secondaryTemplateId = value;
			}
		}
	}
}