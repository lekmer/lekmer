using Litium.Framework.Setting;

namespace Litium.Lekmer.Product.Setting
{
	public class MonitorProductSetting : SettingBase, IMonitorProductSetting
	{
		protected override string StorageName
		{
			get { return "MonitorProduct"; }
		}

		protected string GroupName
		{
			get { return "MonitorProductParameters"; }
		}


		public virtual int BreakDuration
		{
			get
			{
				return GetInt32(GroupName, "BreakDuration", 10);
			}
		}

		public virtual int PortionSize
		{
			get
			{
				return GetInt32(GroupName, "PortionSize", 100);
			}
		}

		public virtual int MinNumberInStockDefault
		{
			get
			{
				return GetInt32(GroupName, "MinNumberInStockDefault", 1);
			}
		}
	}
}