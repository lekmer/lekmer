﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Framework.Setting;

namespace Litium.Lekmer.Product.Setting
{
	public class PulsenSetting : SettingBase
	{
		protected override string StorageName
		{
			get { return "Pulsen"; }
		}

		private static readonly PulsenSetting _instance = new PulsenSetting();
		private const string _groupName = "Default";
		public static PulsenSetting Instance
		{
			get { return _instance; }
		}

		public string Appl
		{
			get { return GetValue(_groupName, "Appl");}
		}

		public string AnvBehgId
		{
			get { return GetValue(_groupName, "AnvBehgId"); }
		}
		public string Sys
		{
			get { return GetValue(_groupName, "Sys"); }
		}
		public string Fok
		{
			get { return GetValue(_groupName, "Fok"); }
		}
		public string Anv
		{
			get { return GetValue(_groupName, "Anv"); }
		}
		public string Losen
		{
			get { return GetValue(_groupName, "Losen"); }
		}
		public string Funk
		{
			get { return GetValue(_groupName, "Funk"); }
		}
		public string Sprak
		{
			get { return GetValue(_groupName, "Sprak"); }
		}
		public string Typ
		{
			get { return GetValue(_groupName, "Typ"); }
		}

		public string Url
		{
			get { return GetValue(_groupName, "Url"); }
		}

		public string AcceptansUrl
		{
			get { return GetValue(_groupName, "AcceptansUrl"); }
		}
	}
}
