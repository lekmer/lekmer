﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product.Mapper
{
	public class BrandDataMapper : DataMapperBase<IBrand>
	{
		private DataMapperBase<IImage> _imageDataMapper;
		private DataMapperBase<IDeliveryTime> _deliveryTimeDataMapper;

		public BrandDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}


		protected override void Initialize()
		{
			base.Initialize();
			_imageDataMapper = DataMapperResolver.Resolve<IImage>(DataReader);
			_deliveryTimeDataMapper = DataMapperResolver.Resolve<IDeliveryTime>(DataReader);
		}

		protected override IBrand Create()
		{
			var brand = IoC.Resolve<IBrand>();
			brand.Id = MapValue<int>("Brand.BrandId");
			brand.Title = MapValue<string>("Brand.Title");
			brand.MediaId = MapNullableValue<int?>("Brand.MediaId");
			brand.Description = MapNullableValue<string>("Brand.Description");
			brand.ExternalUrl = MapNullableValue<string>("Brand.ExternalUrl");
			brand.ErpId = MapNullableValue<string>("Brand.ErpId");
			brand.ContentNodeId = MapNullableValue<int?>("Brand.ContentNodeId");
			brand.PackageInfo = MapNullableValue<string>("Brand.PackageInfo");
			brand.MonitorThreshold = MapNullableValue<int?>("Brand.MonitorThreshold");
			brand.MaxQuantityPerOrder = MapNullableValue<int?>("Brand.MaxQuantityPerOrder");
			brand.DeliveryTimeId = MapNullableValue<int?>("Brand.DeliveryTimeId");
			brand.IsOffline = MapValue<bool>("Brand.IsOffline");
			if (brand.MediaId.HasValue)
			{
				brand.Image = _imageDataMapper.MapRow();
			}
			if (brand.DeliveryTimeId.HasValue)
			{
				brand.DeliveryTime = _deliveryTimeDataMapper.MapRow();
			}

			return brand;
		}
	}
}