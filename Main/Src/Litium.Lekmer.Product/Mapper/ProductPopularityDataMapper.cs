using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductPopularityDataMapper : DataMapperBase<IProductPopularity>
	{
		public ProductPopularityDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductPopularity Create()
		{
			var productTag = IoC.Resolve<IProductPopularity>();

			productTag.ChannelId = MapValue<int>("ProductPopularity.ChannelId");
			productTag.ProductId = MapValue<int>("ProductPopularity.ProductId");
			productTag.Popularity = MapValue<int>("ProductPopularity.Popularity");
			productTag.SalesAmount = MapValue<int>("ProductPopularity.SalesAmount");

			return productTag;
		}
	}
}