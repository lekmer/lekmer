﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class RecommendedPriceDataMapper : DataMapperBase<IRecommendedPrice>
	{
		public RecommendedPriceDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IRecommendedPrice Create()
		{
			var recommendedPrice = IoC.Resolve<IRecommendedPrice>();
			recommendedPrice.ChannelId = MapValue<int>("ChannelId");
			recommendedPrice.Price = MapValue<decimal>("Price");
			recommendedPrice.ProductId = MapValue<int>("ProductId");
			return recommendedPrice;
		}
	}
}