﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product.Mapper
{
	public class IconDataMapper : DataMapperBase<IIcon>
	{
		private DataMapperBase<IImage> _imageDataMapper;

		public IconDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_imageDataMapper = DataMapperResolver.Resolve<IImage>(DataReader);
		}

		protected override IIcon Create()
		{
			var icon = IoC.Resolve<IIcon>();
			icon.Id = MapValue<int>("Icon.Id");
			icon.Title = MapValue<string>("Icon.Title");
			icon.Description = MapNullableValue<string>("Icon.Description");
			icon.Image = _imageDataMapper.MapRow();
			icon.SetUntouched();
			return icon;
		}
	}
}