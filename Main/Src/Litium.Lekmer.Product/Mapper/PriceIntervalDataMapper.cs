using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Mapper
{
	public class PriceIntervalDataMapper : DataMapperBase<IPriceInterval>
	{
		private DataMapperBase<ICurrency> _currencyDataMapper;

		public PriceIntervalDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_currencyDataMapper = DataMapperResolver.Resolve<ICurrency>(DataReader);
		}

		protected override IPriceInterval Create()
		{
			var priceInterval = IoC.Resolve<IPriceInterval>();
			priceInterval.Id = MapValue<int>("PriceInterval.PriceIntervalId");
			priceInterval.From = MapValue<decimal>("PriceInterval.From");
			priceInterval.To = MapValue<decimal>("PriceInterval.To");
			priceInterval.Title = MapValue<string>("PriceInterval.Title");
			priceInterval.Currency = _currencyDataMapper.MapRow();
			priceInterval.ContentNodeId = MapNullableValue<int?>("PriceInterval.ContentNodeId");
			return priceInterval;
		}
	}
}