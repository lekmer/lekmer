﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	public class LekmerCategoryViewDataMapper : CategoryViewDataMapper
	{
		private DataMapperBase<IDeliveryTime> _deliveryTimeDataMapper;

		public LekmerCategoryViewDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_deliveryTimeDataMapper = DataMapperResolver.Resolve<IDeliveryTime>(DataReader);
		}

		protected override ICategoryView Create()
		{
			var category = (ILekmerCategoryView)base.Create();
			category.AllowMultipleSizesPurchase = MapNullableValue<bool?>("LekmerCategory.AllowMultipleSizesPurchase");
			category.PackageInfo = MapNullableValue<string>("LekmerCategory.PackageInfo");
			category.MonitorThreshold = MapNullableValue<int?>("LekmerCategory.MonitorThreshold");
			category.MaxQuantityPerOrder = MapNullableValue<int?>("LekmerCategory.MaxQuantityPerOrder");
			category.DeliveryTimeId = MapNullableValue<int?>("LekmerCategory.DeliveryTimeId");
			category.MinQuantityInStock = MapNullableValue<int?>("LekmerCategory.MinQuantityInStock");
			if (category.DeliveryTimeId.HasValue)
			{
				category.DeliveryTime = _deliveryTimeDataMapper.MapRow();
			}
			return category;
		}
	}
}