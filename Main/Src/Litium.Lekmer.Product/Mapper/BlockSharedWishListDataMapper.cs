﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockSharedWishListDataMapper : DataMapperBase<IBlockSharedWishList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;
		public BlockSharedWishListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}
		protected override IBlockSharedWishList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockSharedWishList = IoC.Resolve<IBlockSharedWishList>();
			block.ConvertTo(blockSharedWishList);
			blockSharedWishList.Setting = _blockSettingDataMapper.MapRow();
			blockSharedWishList.Status = BusinessObjectStatus.Untouched;
			return blockSharedWishList;
		}
	}
}
