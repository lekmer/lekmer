using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class DeliveryTimeDataMapper : DataMapperBase<IDeliveryTime>
	{
		public DeliveryTimeDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IDeliveryTime Create()
		{
			var deliveryTime = IoC.Resolve<IDeliveryTime>();
			deliveryTime.Id = MapValue<int>("DeliveryTime.Id");
			deliveryTime.FromDays = MapNullableValue<int?>("DeliveryTime.From");
			deliveryTime.ToDays = MapNullableValue<int?>("DeliveryTime.To");
			deliveryTime.Format = MapNullableValue<string>("DeliveryTime.Format");
			deliveryTime.ProductRegistryid = MapNullableValue<int?>("DeliveryTime.ProductRegistryId");
			deliveryTime.ChannelId = MapNullableValue<int?>("DeliveryTime.ProductRegistryChannelId");
			return deliveryTime;
		}
	}
}