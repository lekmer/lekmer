﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ImageRotatorGroupDataMapper : DataMapperBase<IImageRotatorGroup>
	{
		public ImageRotatorGroupDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IImageRotatorGroup Create()
		{
			var imageRotatorGroup = IoC.Resolve<IImageRotatorGroup>();
			imageRotatorGroup.ImageGroupId = MapValue<int>("ImageGroupId");
			imageRotatorGroup.BlockId = MapValue<int>("BlockId");
			imageRotatorGroup.MainImageId = MapNullableValue<int?>("MainImageId");
			imageRotatorGroup.ThumbnailImageId = MapNullableValue<int?>("ThumbnailImageId");
			imageRotatorGroup.InternalLinkContentNodeId = MapNullableValue<int?>("InternalLinkContentNodeId");
			imageRotatorGroup.ExternalLink = MapNullableValue<string>("ExternalLink");
			imageRotatorGroup.IsMovie = MapNullableValue<bool>("IsMovie");


			imageRotatorGroup.InternalLinkContentNodeId2 = MapNullableValue<int?>("InternalLinkContentNodeId2");
			imageRotatorGroup.ExternalLink2 = MapNullableValue<string>("ExternalLink2");
			imageRotatorGroup.IsMovie2 = MapNullableValue<bool>("IsMovie2");
			imageRotatorGroup.InternalLinkContentNodeId3 = MapNullableValue<int?>("InternalLinkContentNodeId3");
			imageRotatorGroup.ExternalLink3 = MapNullableValue<string>("ExternalLink3");
			imageRotatorGroup.IsMovie3 = MapNullableValue<bool>("IsMovie3");

			imageRotatorGroup.Title = MapNullableValue<string>("Title");
			imageRotatorGroup.HtmalMarkup = MapNullableValue<string>("HtmalMarkup");


			imageRotatorGroup.StartDateTime = MapNullableValue<DateTime?>("StartDateTime");
			imageRotatorGroup.EndDateTime = MapNullableValue<DateTime?>("EndDateTime");
			imageRotatorGroup.Ordinal = MapNullableValue<int>("Ordinal");
			imageRotatorGroup.SetUntouched();
			return imageRotatorGroup;
		}
	}
}