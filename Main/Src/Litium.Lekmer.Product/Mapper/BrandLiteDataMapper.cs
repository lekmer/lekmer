﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class BrandLiteDataMapper : DataMapperBase<IBrandLite>
	{
			public BrandLiteDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBrandLite Create()
		{
			var brand = IoC.Resolve<IBrandLite>();
			brand.Id = MapValue<int>("Brand.BrandId");
			brand.Title = MapValue<string>("Brand.Title");
			brand.ContentNodeId = MapValue<int>("Brand.ContentNodeId");

			return brand;
		}
	}
}
