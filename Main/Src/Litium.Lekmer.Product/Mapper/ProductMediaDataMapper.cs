﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductMediaDataMapper : DataMapperBase<IProductMedia>
	{


		public ProductMediaDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductMedia Create()
		{
			var productMedia = IoC.Resolve<IProductMedia>();
			productMedia.ProductId = MapValue<int>("ProductMedia.ProductId");
			productMedia.MediaId = MapValue<int>("ProductMedia.MediaId");
			productMedia.GroupId = MapValue<int>("ProductMedia.GroupId");
			productMedia.Ordinal = MapValue<int>("ProductMedia.Ordinal");
			productMedia.FormatId = MapValue<int>("Media.FormatId");
			productMedia.Title = MapNullableValue<string>("Media.Title");
			productMedia.FileName = MapNullableValue<string>("Media.FileName");
			productMedia.Extension = MapNullableValue<string>("MediaFormat.Extension");
			return productMedia;
		}
	}
}
