﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class WishListDataMapper : DataMapperBase<IWishList>
	{
		public WishListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IWishList Create()
		{
			var wishList = IoC.Resolve<IWishList>();
			wishList.Id = MapValue<int>("WishListId");
			wishList.Key = MapValue<Guid>("WishListKey");
			wishList.Title = MapNullableValue<string>("WishListTitle");
			wishList.CreatedDate = MapNullableValue<DateTime?>("CreatedDate");
			wishList.UpdatedDate = MapNullableValue<DateTime?>("UpdatedDate");
			wishList.IpAddress = MapNullableValue<string>("IpAddress");
			wishList.NumberOfRemovedItems = MapNullableValue<int?>("NumberOfRemovedItems");
			wishList.Status = BusinessObjectStatus.Untouched;

			return wishList;
		}
	}
}
