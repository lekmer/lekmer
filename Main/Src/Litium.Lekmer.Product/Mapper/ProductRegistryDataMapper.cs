using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductRegistryDataMapper : DataMapperBase<IProductRegistry>
	{
		public ProductRegistryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductRegistry Create()
		{
			var productRegistry = IoC.Resolve<IProductRegistry>();

			productRegistry.ProductRegistryId = MapValue<int>("ProductRegistryId");
			productRegistry.Title = MapValue<string>("Title");

			return productRegistry;
		}
	}
}