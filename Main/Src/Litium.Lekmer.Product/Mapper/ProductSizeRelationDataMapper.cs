﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductSizeRelationDataMapper : DataMapperBase<IProductSizeRelation>
	{
		public ProductSizeRelationDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductSizeRelation Create()
		{
			var productSizeRelation = IoC.Resolve<IProductSizeRelation>();

			productSizeRelation.ProductId = MapValue<int>("ProductId");
			productSizeRelation.SizeId = MapValue<int>("SizeId");
			productSizeRelation.SetUntouched();

			return productSizeRelation;
		}
	}
}