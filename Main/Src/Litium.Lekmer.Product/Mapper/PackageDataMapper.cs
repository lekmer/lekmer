using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class PackageDataMapper : DataMapperBase<IPackage>
	{
		public PackageDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IPackage Create()
		{
			var package = IoC.Resolve<IPackage>();

			package.PackageId = MapValue<int>("Package.PackageId");
			package.MasterProductId = MapValue<int>("Package.MasterProductId");
			package.GeneralInfo = MapNullableValue<string>("Package.GeneralInfo");

			return package;
		}
	}
}