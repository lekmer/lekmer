using Litium.Scensum.Core;
using Litium.Scensum.Review.Repository;

namespace Litium.Scensum.Review
{
	public class ReviewStatusService : IReviewStatusService
	{
		protected ReviewStatusRepository Repository { get; private set; }

		public ReviewStatusService(ReviewStatusRepository repository)
		{
			Repository = repository;
		}

		public virtual IReviewStatus GetByCommonName(IUserContext context, string commonName)
		{
			return Repository.GetByCommonName(commonName);
		}
	}
}