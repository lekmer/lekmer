using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Review.Repository;

namespace Litium.Scensum.Review
{
	public class ReviewSecureService : IReviewSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected ReviewRepository Repository { get; private set; }

		public ReviewSecureService(IAccessValidator accessValidator, ReviewRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual ReviewRecordCollection Search(IReviewSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null) throw new ArgumentNullException("searchCriteria");
			return Repository.Search(searchCriteria, page, pageSize);
		}

		public virtual void SetStatus(ISystemUserFull user, int reviewId, int statusId)
		{
			AccessValidator.ForceAccess(user, LekmerPrivilegeConstant.AssortmentReview);
			Repository.SetStatus(reviewId, statusId);
			ClearCache();
		}

		public virtual void SetStatus(ISystemUserFull user, IEnumerable<int> reviewIds, int statusId)
		{
			if (reviewIds == null) throw new ArgumentNullException("reviewIds");

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var id in reviewIds)
				{
					SetStatus(user, id, statusId);
				}
				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull user, int reviewId)
		{
			AccessValidator.ForceAccess(user, LekmerPrivilegeConstant.AssortmentReview);
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(reviewId);
				transactedOperation.Complete();
			}
			ClearCache();
		}

		public virtual void Delete(ISystemUserFull user, IEnumerable<int> reviewIds)
		{
			if (reviewIds == null) throw new ArgumentNullException("reviewIds");

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var id in reviewIds)
				{
					Delete(user, id);
				}
				transactedOperation.Complete();
			}
		}

		protected virtual void ClearCache()
		{
			ReviewCollectionCache.Instance.Flush();
			ReviewSummaryCache.Instance.Flush();
		}
	}
}