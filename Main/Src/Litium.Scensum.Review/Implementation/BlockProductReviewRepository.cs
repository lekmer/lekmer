using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Repository
{
	public class BlockProductReviewRepository
	{
		protected virtual DataMapperBase<IBlockProductReview> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductReview>(dataReader);
		}

		public virtual IBlockProductReview GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect("[addon].[pBlockProductReviewGetByIdSecure]", parameters,
					                                new DatabaseSetting("BlockProductReviewRepository.GetByIdSecure")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductReview GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect("[addon].[pBlockProductReviewGetById]", parameters,
					                                new DatabaseSetting("BlockProductReviewRepository.GetById")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockProductReview block)
		{
			if (block == null) throw new ArgumentNullException("block");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("RowCount", block.RowCount, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[addon].[pBlockProductReviewSave]", parameters,
			                                 new DatabaseSetting("BlockProductReviewRepository.Save"));
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockProductReviewRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pBlockProductReviewDelete]", parameters, dbSettings);
		}
	}
}