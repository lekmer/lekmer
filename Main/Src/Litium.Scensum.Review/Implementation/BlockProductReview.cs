using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Review
{
	[Serializable]
	public class BlockProductReview : BlockBase, IBlockProductReview
	{
		private int _rowCount;

		public int RowCount
		{
			get { return _rowCount; }
			set
			{
				if (value < 1) throw new ArgumentException("RowCount can't be less then '1'.");
				CheckChanged(_rowCount, value);
				_rowCount = value;
			}
		}
	}
}