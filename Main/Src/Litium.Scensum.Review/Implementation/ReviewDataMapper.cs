using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Mapper
{
	public class ReviewDataMapper<T> : DataMapperBase<T>
		where T : class, IReview
	{
		public ReviewDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override T Create()
		{
			var review = IoC.Resolve<T>();
			review.Id = MapValue<int>("Review.ReviewId");
			review.ProductId = MapValue<int>("Review.ProductId");
			review.ReviewStatusId = MapValue<int>("Review.ReviewStatusId");
			review.CustomerId = MapNullableValue<int?>("Review.CustomerId");
			review.Rating = MapValue<byte>("Review.Rating");
			review.Message = MapValue<string>("Review.Message");
			review.Author = MapValue<string>("Review.Author");
			review.CreatedDate = MapValue<DateTime>("Review.CreatedDate");
			review.ChannelId = MapValue<int>("Review.ChannelId");
			review.SetUntouched();
			return review;
		}
	}
}