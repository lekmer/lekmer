﻿using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core;
using Litium.Scensum.Review.Repository;

namespace Litium.Scensum.Review
{
	public class ReviewSummaryService : IReviewSummaryService
	{
		protected ReviewSummaryRepository Repository { get; private set; }

		public ReviewSummaryService(ReviewSummaryRepository repository)
		{
			Repository = repository;
		}

		public virtual IReviewSummary GetByProduct(IUserContext context, int productId)
		{
			return ReviewSummaryCache.Instance.TryGetItem(
				new ReviewSummaryKey(context.Channel.Id, productId),
				delegate { return GetByProductCore(context, productId); });
		}

		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		protected virtual IReviewSummary GetByProductCore(IUserContext context, int productId)
		{
			return Repository.GetByProduct(context.Channel.Id, productId);
		}
	}
}