﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Scensum.Review
{
	public class ReviewRecordCollection : Collection<IReviewRecord>
	{
		public ReviewRecordCollection()
		{
		}

		public ReviewRecordCollection(IList<IReviewRecord> list) : base(list)
		{
		}

		public ReviewRecordCollection(IEnumerable<IReviewRecord> list) : base(new List<IReviewRecord>(list))
		{
		}

		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		public int TotalCount { get; set; }
	}
}