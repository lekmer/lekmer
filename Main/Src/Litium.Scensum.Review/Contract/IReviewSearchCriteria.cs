namespace Litium.Scensum.Review
{
	public interface IReviewSearchCriteria
	{
		string ProductTitle { get; set; }

		string ProductId { get; set; }

		string Message { get; set; }

		string Author { get; set; }

		string StatusId { get; set; }

		string CreatedFrom { get; set; }

		string CreatedTo { get; set; }

		string SortBy { get; set; }

		string SortByDescending { get; set; }
	}
}