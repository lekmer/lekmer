﻿using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace Litium.Scensum.Review
{
	public interface IReviewStatusSecureService
	{
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IReviewStatus> GetAll();
	}
}