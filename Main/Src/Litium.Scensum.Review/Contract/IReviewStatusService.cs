﻿using Litium.Scensum.Core;

namespace Litium.Scensum.Review
{
	public interface IReviewStatusService
	{
		IReviewStatus GetByCommonName(IUserContext context, string commonName);
	}
}