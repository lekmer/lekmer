﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.SiteStructure.Cache
{
	public sealed class TreeItemsHashCache : ScensumCacheBase<TreeItemsHashKey, ITreeItemsHash>
	{
		#region Singleton

		private TreeItemsHashCache()
		{
		}

		public static TreeItemsHashCache Instance
		{
			get { return SingletonCreator.CreatorInstance; }
		}

		private static class SingletonCreator
		{
			private static readonly TreeItemsHashCache _instance = new TreeItemsHashCache();

			public static TreeItemsHashCache CreatorInstance
			{
				get { return _instance; }
			}
		}

		#endregion
	}

	public class TreeItemsHashKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public TreeItemsHashKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}
