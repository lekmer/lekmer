﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Mapper
{
	public class ContentPageUrlHistoryDataMapper : DataMapperBase<IContentPageUrlHistory>
	{
		public ContentPageUrlHistoryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IContentPageUrlHistory Create()
		{
			var contentPageUrlHistory = IoC.Resolve<IContentPageUrlHistory>();

			contentPageUrlHistory.Id = MapValue<int>("ContentPageUrlHistory.Id");
			contentPageUrlHistory.ContentPageId = MapValue<int>("ContentPageUrlHistory.ContentPageId");
			contentPageUrlHistory.UrlTitleOld = MapValue<string>("ContentPageUrlHistory.UrlTitleOld");
			contentPageUrlHistory.CreationDate = MapValue<DateTime>("ContentPageUrlHistory.CreationDate");
			contentPageUrlHistory.LastUsageDate = MapValue<DateTime>("ContentPageUrlHistory.LastUsageDate");
			contentPageUrlHistory.UsageAmount = MapValue<int>("ContentPageUrlHistory.UsageAmount");
			contentPageUrlHistory.TypeId = MapValue<int>("ContentPageUrlHistory.HistoryLifeIntervalTypeId");

			contentPageUrlHistory.SetUntouched();

			return contentPageUrlHistory;
		}
	}
}