﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class BlockSettingRepository
	{
		protected virtual DataMapperBase<IBlockSetting> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockSetting>(dataReader);
		}

		public virtual int Save(IBlockSetting blockSetting)
		{
			var dbSettings = new DatabaseSetting("BlockSettingRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockSetting.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ColumnCount", blockSetting.ColumnCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("ColumnCountMobile", blockSetting.ColumnCountMobile, SqlDbType.Int),
					ParameterHelper.CreateParameter("RowCount", blockSetting.RowCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("RowCountMobile", blockSetting.RowCountMobile, SqlDbType.Int),
					ParameterHelper.CreateParameter("TotalItemCount", blockSetting.TotalItemCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("TotalItemCountMobile", blockSetting.TotalItemCountMobile, SqlDbType.Int)
				};
			return new DataHandler().ExecuteCommandWithReturnValue("[sitestructurelek].[pBlockSettingSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockSettingRepository.Delete");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommandWithReturnValue("[sitestructurelek].[pBlockSettingDelete]", parameters, dbSettings);
		}

		public virtual IBlockSetting GetByBlock(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockSettingRepository.GetByBlock");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[sitestructurelek].[pBlockSettingGetByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}