﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure.Repository
{
	public class BlockListRepository
	{
		protected virtual DataMapperBase<IBlockList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockList>(dataReader);
		}

		public virtual IBlockList GetById(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSetting = new DatabaseSetting("BlockListRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[sitestructure].[pBlockListGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSetting = new DatabaseSetting("BlockListRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[sitestructure].[pBlockListGetByIdSecure]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockListRepository.Delete");
			new DataHandler().ExecuteCommand("[sitestructure].[pBlockDelete]", parameters, dbSettings);
		}




	}
}
