﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure
{
	public class BlockListService : IBlockListService
	{
		protected BlockListRepository Repository { get; private set; }

		public BlockListService(BlockListRepository repository)
		{
			Repository = repository;
		}

		public IBlockList GetById(int blockId)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(blockId);
		}
	}
}