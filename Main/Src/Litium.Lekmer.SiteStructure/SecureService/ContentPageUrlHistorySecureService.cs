﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.SiteStructure.Cache;
using Litium.Lekmer.SiteStructure.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	public class ContentPageUrlHistorySecureService : IContentPageUrlHistorySecureService
	{
		protected ContentPageUrlHistoryRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ContentPageUrlHistorySecureService(ContentPageUrlHistoryRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public Collection<IContentPageUrlHistory> GetAllByContentPage(int contentPageId)
		{
			return Repository.GetAllByContentPage(contentPageId);
		}

		public IContentPageUrlHistory Create(int contentPageId, string urlTitle, int typeId)
		{
			var contentPageUrlHistory = IoC.Resolve<IContentPageUrlHistory>();

			contentPageUrlHistory.ContentPageId = contentPageId;
			contentPageUrlHistory.TypeId = typeId;
			contentPageUrlHistory.UrlTitleOld = urlTitle;
			contentPageUrlHistory.CreationDate = contentPageUrlHistory.LastUsageDate = DateTime.Now;

			contentPageUrlHistory.SetNew();

			return contentPageUrlHistory;
		}

		public int Save(ISystemUserFull systemUserFull, IContentPageUrlHistory contentPageUrlHistory)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			int contentPageUrlHistoryId = Repository.Save(contentPageUrlHistory);

			ContentPageUrlHistoryCache.Instance.Flush();

			return contentPageUrlHistoryId;
		}

		public List<int> Save(ISystemUserFull systemUserFull, Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection)
		{
			List<int> contentPageUrlHistoryIds = new List<int>();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				foreach (IContentPageUrlHistory contentPageUrlHistory in contentPageUrlHistoryCollection)
				{
					contentPageUrlHistoryIds.Add(Repository.Save(contentPageUrlHistory));

					ContentPageUrlHistoryCache.Instance.Flush();
				}

				transactedOperation.Complete();
			}

			return contentPageUrlHistoryIds;
		}

		public void DeleteById(ISystemUserFull systemUserFull, int contentPageUrlHistoryId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			Repository.DeleteById(contentPageUrlHistoryId);

			ContentPageUrlHistoryCache.Instance.Flush();
		}

		public void DeleteAllByContentPage(ISystemUserFull systemUserFull, int contentPageId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			Repository.DeleteAllByContentPage(contentPageId);

			ContentPageUrlHistoryCache.Instance.Flush();
		}

		public void DeleteAllByContentPageAndUrlTitle(ISystemUserFull systemUserFull, int contentPageId, string urlTitle)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			Repository.DeleteAllByContentPageAndUrlTitle(contentPageId, urlTitle);

			ContentPageUrlHistoryCache.Instance.Flush();
		}
	}
}