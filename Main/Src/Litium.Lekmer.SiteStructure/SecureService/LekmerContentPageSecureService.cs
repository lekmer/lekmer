﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common;
using Litium.Lekmer.SiteStructure.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Cache;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure
{
	public class LekmerContentPageSecureService : ContentPageSecureService
	{
		protected IContentPageUrlHistorySecureService ContentPageUrlHistorySecureService { get; private set; }

		public LekmerContentPageSecureService(
			IAccessValidator accessValidator,
			ContentPageRepository repository,
			IAccessSecureService accessSecureService,
			IContentNodeSecureService contentNodeSecureService,
			IBlockSecureService blockSecureService,
			IContentPageSeoSettingSecureService contentPageSeoSettingSecureService,
			IContentPageUrlHistorySecureService contentPageUrlHistorySecureService)
			: base(accessValidator, repository, accessSecureService, contentNodeSecureService, blockSecureService, contentPageSeoSettingSecureService)
		{
			ContentPageUrlHistorySecureService = contentPageUrlHistorySecureService;
		}

		public override int Save(ISystemUserFull systemUserFull, IContentPage contentPage)
		{
			if (ContentPageUrlHistorySecureService == null)
			{
				throw new InvalidOperationException("ContentPageUrlHistorySecureService must be set before calling Save.");
			}

			IContentPage existedPage = GetById(contentPage.Id);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				int contentPageId = base.Save(systemUserFull, contentPage);

				if (existedPage != null && contentPageId > 0)
				{
					if (existedPage.UrlTitle != contentPage.UrlTitle)
					{
						Collection<IContentPageUrlHistory> contentPageUrlHistories = ContentPageUrlHistorySecureService.GetAllByContentPage(contentPageId);
						if (contentPageUrlHistories.FirstOrDefault(cp => cp.UrlTitleOld == existedPage.UrlTitle) == null)
						{
							IContentPageUrlHistory contentPageUrlHistory = ContentPageUrlHistorySecureService.Create(existedPage.Id, existedPage.UrlTitle, (int)HistoryLifeIntervalType.Month);

							ContentPageUrlHistorySecureService.Save(systemUserFull, contentPageUrlHistory);
						}
					}

					ContentPageUrlHistorySecureService.DeleteAllByContentPageAndUrlTitle(systemUserFull, contentPageId, contentPage.UrlTitle);
				}

				transactedOperation.Complete();
			}

			ContentNodeTreeCache.Instance.Flush();
			ContentPageFullCache.Instance.Remove(contentPage.Id);
			ContentPageUrlHistoryCache.Instance.Flush();

			return contentPage.Id;
		}

		public override void Delete(ISystemUserFull systemUserFull, int contentNodeId)
		{
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				ContentPageUrlHistorySecureService.DeleteAllByContentPage(systemUserFull, contentNodeId);

				base.Delete(systemUserFull, contentNodeId);

				transactedOperation.Complete();
			}

			// Clean cache
			ContentNodeTreeCache.Instance.Flush();
			ContentPageFullCache.Instance.Remove(contentNodeId);
			ContentPageUrlHistoryCache.Instance.Flush();
		}
	}
}
