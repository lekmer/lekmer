﻿using System;
using Litium.Lekmer.Common;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	[Serializable]
	public class BlockSetting : BusinessObjectBase, IBlockSetting
	{
		private int _blockId;
		private int _columnCount;
		private int? _columnCountMobile;
		private int _rowCount;
		private int? _rowCountMobile;
		private int _totalItemCount;
		private int? _totalItemCountMobile;

		public virtual int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}
		public virtual int ColumnCount
		{
			get { return _columnCount; }
			set
			{
				CheckChanged(_columnCount, value);
				_columnCount = value;
			}
		}
		public virtual int? ColumnCountMobile
		{
			get { return _columnCountMobile; }
			set
			{
				CheckChanged(_columnCountMobile, value);
				_columnCountMobile = value;
			}
		}
		public virtual int RowCount
		{
			get { return _rowCount; }
			set
			{
				CheckChanged(_rowCount, value);
				_rowCount = value;
			}
		}
		public virtual int? RowCountMobile
		{
			get { return _rowCountMobile; }
			set
			{
				CheckChanged(_rowCountMobile, value);
				_rowCountMobile = value;
			}
		}
		public virtual int TotalItemCount
		{
			get { return _totalItemCount; }
			set
			{
				CheckChanged(_totalItemCount, value);
				_totalItemCount = value;
			}
		}
		public virtual int? TotalItemCountMobile
		{
			get { return _totalItemCountMobile; }
			set
			{
				CheckChanged(_totalItemCountMobile, value);
				_totalItemCountMobile = value;
			}
		}


		[NonSerialized]
		private ICommonSession _commonSession;
		private ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
		}

		[NonSerialized]
		private bool? _isViewportMobile;
		private bool IsViewportMobile
		{
			get
			{
				if (_isViewportMobile == null)
				{
					_isViewportMobile = CommonSession.IsViewportMobile;
				}

				return (bool)_isViewportMobile;
			}
		}

		public int ActualColumnCount
		{
			get { return IsViewportMobile && ColumnCountMobile.HasValue ? ColumnCountMobile.Value : ColumnCount; }
		}
		public int ActualRowCount
		{
			get { return IsViewportMobile && RowCountMobile.HasValue ? RowCountMobile.Value : RowCount; }
		}
		public int ActualTotalItemCount
		{
			get { return IsViewportMobile && TotalItemCountMobile.HasValue ? TotalItemCountMobile.Value : TotalItemCount; }
		}

		public void SetValues(IBlockSetting blockSetting)
		{
			ColumnCount = blockSetting.ColumnCount;
			ColumnCountMobile = blockSetting.ColumnCountMobile;
			RowCount = blockSetting.RowCount;
			RowCountMobile = blockSetting.RowCountMobile;
			TotalItemCount = blockSetting.TotalItemCount;
			TotalItemCountMobile = blockSetting.TotalItemCountMobile;
		}
	}
}