﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignCategoryDictionary : Dictionary<int, bool>
	{
		public CampaignCategoryDictionary() { }

		protected CampaignCategoryDictionary(SerializationInfo info, StreamingContext context) : base(info, context) { }

		public CampaignCategoryDictionary(IEnumerable<int> categoryIds)
		{
			foreach (int categoryId in categoryIds)
			{
				Add(categoryId, false);
			}
		}

		public void Add(int categoryId)
		{
			Add(categoryId, false);
		}

		public new void Add(int categoryId, bool includeSubCategories)
		{
			if (!ContainsKey(categoryId))
			{
				base.Add(categoryId, includeSubCategories);
			}
		}
	}
}