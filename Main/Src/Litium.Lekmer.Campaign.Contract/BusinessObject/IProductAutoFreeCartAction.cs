﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface IProductAutoFreeCartAction : ILekmerCartAction
	{
		int ProductId { get; set; }
		int Quantity { get; set; }
	}
}