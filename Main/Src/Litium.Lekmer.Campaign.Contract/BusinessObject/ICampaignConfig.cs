﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignConfig : IBusinessObjectBase
	{
		int Id { get; set; }

		bool IncludeAllProducts { get; set; }

		ProductIdDictionary IncludeProducts { get; set; }
		ProductIdDictionary ExcludeProducts { get; set; }

		CampaignCategoryDictionary IncludeCategories { get; set; }
		CampaignCategoryDictionary ExcludeCategories { get; set; }

		BrandIdDictionary IncludeBrands { get; set; }
		BrandIdDictionary ExcludeBrands { get; set; }

		void Validate();
		bool IsRangeMember(int productId, int categoryId, int? brandId);
		bool IsTargetType(int productId);
	}
}