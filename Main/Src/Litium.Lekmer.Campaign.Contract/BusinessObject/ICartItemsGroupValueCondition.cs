﻿namespace Litium.Lekmer.Campaign
{
	public interface ICartItemsGroupValueCondition : ILekmerCondition
	{
		int CampaignConfigId { get; set; }
		ICampaignConfig CampaignConfig { get; set; }
		LekmerCurrencyValueDictionary Amounts { get; set; }
	}
}