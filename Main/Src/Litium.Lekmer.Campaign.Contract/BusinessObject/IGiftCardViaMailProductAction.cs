﻿namespace Litium.Lekmer.Campaign
{
	public interface IGiftCardViaMailProductAction : ILekmerProductAction
	{
		int SendingInterval { get; set; }
		int? TemplateId { get; set; }
		int CampaignConfigId { get; set; }
		ICampaignConfig CampaignConfig { get; set; }
		LekmerCurrencyValueDictionary Amounts { get; set; }
	}
}