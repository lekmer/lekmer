﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCartValueCondition : ICartValueCondition
	{
		new LekmerCurrencyValueDictionary CartValues { get; set; }
	}
}