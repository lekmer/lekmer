﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerProductCampaign : IProductCampaign, ILekmerCampaign
	{
	}
}