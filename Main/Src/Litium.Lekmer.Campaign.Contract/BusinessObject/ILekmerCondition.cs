﻿using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCondition : ICondition
	{
		IdDictionary TargetProductTypes { get; set; }
	}
}