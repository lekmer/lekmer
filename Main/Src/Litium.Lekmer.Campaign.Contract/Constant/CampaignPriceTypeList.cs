﻿namespace Litium.Lekmer.Campaign
{
	public enum CampaignPriceTypeList
	{
		OutletPrice = 1,
		CampaignPrice = 2,
		LoweredPrice = 3
	}
}