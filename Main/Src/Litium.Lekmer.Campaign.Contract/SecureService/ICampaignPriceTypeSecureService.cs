﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignPriceTypeSecureService
	{
		Collection<ICampaignPriceType> GetAll();
	}
}