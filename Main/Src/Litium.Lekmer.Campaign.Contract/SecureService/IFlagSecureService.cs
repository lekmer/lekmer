﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface IFlagSecureService
	{
		IFlag Create();
		IFlag GetById(int flagId);
		IFlag GetByCampaign(int campaignId);
		IFlag GetByCampaignFolder(int folderId);
		Collection<IFlag> GetAll();
		int Save(ISystemUserFull user, IFlag flag, IEnumerable<ITranslationGeneric> translations);
		void SetOrder(ISystemUserFull user, IEnumerable<IFlag> flags);
		void SaveCampaignFlag(ISystemUserFull user, int campaignId, int? flagId);
		void SaveCampaignFolderFlag(ISystemUserFull user, int campaignFolderId, int? flagId);
		void Delete(ISystemUserFull systemUserFull, int flagId);
		void Delete(ISystemUserFull user, IEnumerable<int> ids);
		Collection<ITranslationGeneric> GetAllTranslationsByFlag(int flagId);
	}
}