﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignLevelSecureService
	{
		Collection<ICampaignLevel> GetAll();
	}
}