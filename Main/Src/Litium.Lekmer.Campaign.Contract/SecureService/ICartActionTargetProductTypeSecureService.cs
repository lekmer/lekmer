﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface ICartActionTargetProductTypeSecureService
	{
		Collection<ICartActionTargetProductType> GetAllByCampaign(int campaignId);
		void Save(ISystemUserFull systemUserFull, ICartAction cartAction);
		void Delete(ISystemUserFull systemUserFull, int cartActionId);
	}
}