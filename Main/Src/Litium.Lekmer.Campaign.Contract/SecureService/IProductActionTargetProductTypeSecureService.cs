﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface IProductActionTargetProductTypeSecureService
	{
		Collection<IProductActionTargetProductType> GetAllByCampaign(int campaignId);
		void Save(ISystemUserFull systemUserFull, IProductAction productAction);
		void Delete(ISystemUserFull systemUserFull, int productActionId);
	}
}