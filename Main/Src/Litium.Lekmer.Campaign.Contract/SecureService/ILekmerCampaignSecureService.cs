﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCampaignSecureService
	{
		/// <summary>
		/// Saves campaigns to database.
		/// </summary>
		/// <returns>First saved campaign</returns>
		ICampaign Save(
			ISystemUserFull systemUserFull,
			ICampaign campaign,
			int? campaignFlagId,
			Collection<int> campaignRegistries,
			ICampaignLandingPage landingPage,
			Collection<ITranslationGeneric> webTitleTranslations,
			Collection<ITranslationGeneric> descriptionTranslations);
	}
}