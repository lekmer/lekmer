﻿namespace Litium.Lekmer.Campaign
{
	public interface ICampaignConfigService
	{
		ICampaignConfig GetById(int campaignConfigId);
	}
}