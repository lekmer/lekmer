﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignRegistryCampaignService
	{
		Collection<ICampaignRegistryCampaign> GetRegistriesByCampaignId(int campaignId);
	}
}