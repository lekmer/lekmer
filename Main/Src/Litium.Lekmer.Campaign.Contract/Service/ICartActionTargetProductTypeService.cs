﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface ICartActionTargetProductTypeService
	{
		Collection<ICartActionTargetProductType> GetAllByCampaign(int campaignId);
	}
}