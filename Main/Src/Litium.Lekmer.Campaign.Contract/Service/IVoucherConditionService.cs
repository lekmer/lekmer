﻿namespace Litium.Lekmer.Campaign
{
    public interface IVoucherConditionService
    {
        IVoucherCondition GetById(int id);
    }
}
