﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignLandingPageService
	{
		ICampaignLandingPage GetByCampaignId(IUserContext userContext, int campaignId);
	}
}