﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ICartCampaignSharedService
	{
		ICartCampaign GetById(int campaignId);
	}
}