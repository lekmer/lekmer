﻿using System;
using System.Reflection;
using Litium.Scensum.Foundation;
using log4net;
using log4net.Config;

namespace Litium.Lekmer.NewsletterSubscriber.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main()
		{
			try
			{
				XmlConfigurator.Configure();
				_log.InfoFormat("NewsletterSubscriber.Console started.");

				Execute();

				_log.InfoFormat("NewsletterSubscriber.Console completed.");
			}
			catch (Exception ex)
			{
				_log.Error("NewsletterSubscriber.Console failed.", ex);
				throw;
			}
		}

		private static void Execute()
		{
			var newsletterSubscriberExporter = IoC.Resolve<INewsletterSubscriberExporter>();
			newsletterSubscriberExporter.Execute();

			var newsletterUnsubscriberExporter = IoC.Resolve<INewsletterUnsubscriberExporter>();
			newsletterUnsubscriberExporter.Execute();
		}
	}
}