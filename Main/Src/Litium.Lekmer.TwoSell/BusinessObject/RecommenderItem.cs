using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.TwoSell
{
	[Serializable]
	public class RecommenderItem : IRecommenderItem
	{
		public int Id { get; set; }
		public int RecommenderListId { get; set; }
		public int? ProductId { get; set; }
		public Collection<IRecommendationProduct> Suggestions { get; set; }
	}
}