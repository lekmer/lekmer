﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.TwoSell.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell
{
	public class RecommenderItemSecureService : IRecommenderItemSecureService
	{
		protected RecommenderItemRepository Repository { get; private set; }
		protected IRecommendationProductSecureService RecommendationProductSecureService { get; private set; }

		public RecommenderItemSecureService(
			RecommenderItemRepository repository,
			IRecommendationProductSecureService recommendationProductSecureService
		)
		{
			Repository = repository;
			RecommendationProductSecureService = recommendationProductSecureService;
		}

		public IRecommenderItem Create()
		{
			var recommenderItem = IoC.Resolve<IRecommenderItem>();

			recommenderItem.Suggestions = new Collection<IRecommendationProduct>();

			return recommenderItem;
		}

		public virtual IRecommenderItem Save(IRecommenderItem recommenderItem)
		{
			if (recommenderItem == null)
			{
				throw new ArgumentNullException("recommenderItem");
			}

			Repository.EnsureNotNull();

			using (var transaction = new TransactedOperation())
			{
				int recommenderItemId = Repository.Save(recommenderItem);
				recommenderItem.Id = recommenderItemId;

				if (recommenderItemId > 0)
				{
					foreach (IRecommendationProduct recommendationProduct in recommenderItem.Suggestions)
					{
						recommendationProduct.RecommenderItemId = recommenderItemId;
					}

					RecommendationProductSecureService.DeleteAllByItem(recommenderItemId);
					RecommendationProductSecureService.Save(recommenderItem.Suggestions);
				}

				transaction.Complete();
			}

			return recommenderItem;
		}

		public virtual void Save(Collection<IRecommenderItem> recommenderItems)
		{
			if (recommenderItems == null)
			{
				throw new ArgumentNullException("recommenderItems");
			}

			using (var transaction = new TransactedOperation())
			{
				foreach (IRecommenderItem recommenderItem in recommenderItems)
				{
					Save(recommenderItem);
				}

				transaction.Complete();
			}
		}
	}
}