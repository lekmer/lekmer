﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.TwoSell.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell
{
	public class RecommendationProductSecureService : IRecommendationProductSecureService
	{
		protected RecommendationProductRepository Repository { get; private set; }

		public RecommendationProductSecureService(
			RecommendationProductRepository repository
		)
		{
			Repository = repository;
		}

		public IRecommendationProduct Create()
		{
			var recommenderItem = IoC.Resolve<IRecommendationProduct>();

			return recommenderItem;
		}

		public virtual IRecommendationProduct Save(IRecommendationProduct recommendationProduct)
		{
			if (recommendationProduct == null)
			{
				throw new ArgumentNullException("recommendationProduct");
			}

			Repository.EnsureNotNull();

			int recommenderItemId = Repository.Save(recommendationProduct);
			recommendationProduct.Id = recommenderItemId;

			return recommendationProduct;
		}

		public virtual void Save(Collection<IRecommendationProduct> recommendationProducts)
		{
			if (recommendationProducts == null)
			{
				throw new ArgumentNullException("recommendationProducts");
			}

			using (var transaction = new TransactedOperation())
			{
				foreach (IRecommendationProduct recommendationProduct in recommendationProducts)
				{
					Save(recommendationProduct);
				}

				transaction.Complete();
			}
		}

		public virtual void DeleteAllByItem(int recommenderItemId)
		{
			Repository.EnsureNotNull();

			Repository.DeleteAllByItem(recommenderItemId);
		}
	}
}