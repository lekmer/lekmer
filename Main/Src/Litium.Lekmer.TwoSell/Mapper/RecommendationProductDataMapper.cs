using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell.Mapper
{
	public class RecommendationProductDataMapper : DataMapperBase<IRecommendationProduct>
	{
		public RecommendationProductDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRecommendationProduct Create()
		{
			var recommendationProduct = IoC.Resolve<IRecommendationProduct>();

			recommendationProduct.Id = MapValue<int>("RecommendationProduct.RecommendationProductId");
			recommendationProduct.ProductId = MapValue<int>("RecommendationProduct.ProductId");
			recommendationProduct.Ordinal = MapValue<int>("RecommendationProduct.Ordinal");

			return recommendationProduct;
		}
	}
}