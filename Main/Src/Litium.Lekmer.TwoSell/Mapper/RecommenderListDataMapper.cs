using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell.Mapper
{
	public class RecommenderListDataMapper : DataMapperBase<IRecommenderList>
	{
		public RecommenderListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRecommenderList Create()
		{
			var recommenderList = IoC.Resolve<IRecommenderList>();

			recommenderList.Id = MapValue<int>("RecommenderList.RecommenderListId");
			recommenderList.SessionId = MapNullableValue<string>("RecommenderList.SessionId");

			recommenderList.Items = new Collection<IRecommenderItem>();

			return recommenderList;
		}
	}
}