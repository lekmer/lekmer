﻿namespace Litium.Lekmer.Order
{
	public interface ICheckoutProfile
	{
		bool IsCompany { get; set; }

		IProfileCustomerInformation CustomerInformation { get; set; }
		IProfileAddress BillingAddress { get; set; }
		IProfileAddress DeliveryAddress { get; set; }
		bool UseAlternativeDeliveryAddress { get; set; }

		IProfileCompanyInformation CompanyInformation { get; set; }
		IProfileAddress CompanyAddress { get; set; }
		IProfileAddress CompanyAlternateAddress { get; set; }
		bool UseAlternateCompanyAddress { get; set; }

		int PaymentMethodId { get; set; }
		int KlarnaPartPaymentId { get; set; }
		string MaksuturvaSubPaymentId { get; set; }
		string QliroPartPaymentId { get; set; }
		string CollectorPartPaymentId { get; set; }

		int DeliveryMethodId { get; set; }
		int OptionalDeliveryMethodId { get; set; }
	}
}