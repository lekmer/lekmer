﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderSearchCriteria : ICustomerSearchCriteria
	{
		int OrderId { get; set; }
	}
}