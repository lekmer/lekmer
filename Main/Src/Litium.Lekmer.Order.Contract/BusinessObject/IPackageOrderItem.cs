using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface IPackageOrderItem : IBusinessObjectBase
	{
		int PackageOrderItemId { get; set; }
		int OrderId { get; set; }
		int OrderItemId { get; set; }
		int Quantity { get; set; }
		Price PackagePrice { get; set; }
		Price ActualPrice { get; set; }
		Price OriginalPrice { get; set; }
		IOrderItemStatus OrderItemStatus { get; set; }
		int ProductId { get; set; }
		string ErpId { get; set; }
		string EanCode { get; set; }
		string Title { get; set; }
		int? SizeId { get; set; }
		string SizeErpId { get; set; }

		decimal GetOriginalVatAmount();
		decimal GetActualVatAmount();
		decimal GetOriginalVatPercent();
	}
}