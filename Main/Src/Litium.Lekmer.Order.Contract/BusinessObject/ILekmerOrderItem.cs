using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderItem : IOrderItem
	{
		IOrderItemSize OrderItemSize { get; set; }
		IProductSize Size { get; set; }
		int? BrandId { get; set; }
		IBrand Brand { get; set;}
		IImage Image { get; set;}
		Collection<ITagGroup> TagGroups { get; set; }
		int ProductTypeId { get; set; }
		bool IsDropShip { get; set; }
		Collection<IPackageOrderItem> PackageOrderItems { get; set; }

		bool HasSize { get; }
		bool HasBrand { get; }
		bool HasImage { get; }

		void MergeWithPackage(IUserContext context, ICartItem cartItem);
		void CalculatePackageItemsPrice();

		decimal GetOriginalVatAmount();
		decimal GetOriginalVatPercent();

		decimal RowDiscount(bool isDiscountIncludingVat);
		double RowDiscountPercent(bool isDiscountIncludingVat);

		void SplitPackageElements(ref Collection<int> itemWithoutSizes, ref Collection<IPackageElement> itemWithSizes);
	}
}