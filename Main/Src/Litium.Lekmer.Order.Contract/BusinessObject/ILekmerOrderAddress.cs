using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderAddress : IOrderAddress
	{
		string HouseNumber { get; set; }
		string HouseExtension { get; set; }
		string Reference { get; set; }
		string DoorCode { get; set; }
	}
}