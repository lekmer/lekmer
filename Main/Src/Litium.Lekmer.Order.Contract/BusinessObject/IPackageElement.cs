using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public interface IPackageElement
	{
		int ProductId { get; set; }
		int? SizeId { get; set; }
	}
}