﻿using System.Collections.Generic;

namespace Litium.Lekmer.Order.Setting
{
	public interface ILekmerCartSetting
	{
		string ShoppingCardCookiename { get; }
		int LifeCycleValue { get; }
		bool CheckoutProfileEnabled { get; }
		int CompanyCartAmount { get; }
		int DiapersBrand { get; }
		int? MaxProductQuantityPerOrder { get; }
		int? MaxNumberOfItems { get; }
		bool IsDoorCodeValidate { get; }
		int MaxDoorCodeSymbols { get; }
		string ApplyStarSymbolForChannels { get; }
		List<string> GetChannelsThatApplyStarSymbol();
	}
}