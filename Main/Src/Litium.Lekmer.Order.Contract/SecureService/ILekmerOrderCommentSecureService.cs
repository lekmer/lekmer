﻿using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderCommentSecureService : IOrderCommentSecureService
	{
		void DeleteByOrder(ISystemUserFull systemUserFull, int orderId);
	}
}