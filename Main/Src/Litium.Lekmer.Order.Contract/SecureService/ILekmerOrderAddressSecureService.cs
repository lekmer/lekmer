﻿using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderAddressSecureService : IOrderAddressSecureService
	{
		void Delete(ISystemUserFull systemUserFull, int orderAddressId);
	}
}