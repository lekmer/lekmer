﻿namespace Litium.Lekmer.Order
{
	public interface ICollectorTimeoutOrderService
	{
		void ManageCollectorTimeoutOrders();
	}
}