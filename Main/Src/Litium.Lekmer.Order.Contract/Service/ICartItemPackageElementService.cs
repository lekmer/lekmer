﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface ICartItemPackageElementService
	{
		ICartItemPackageElement Create();

		void Save(ICartItemPackageElement cartItemPackageElement);
		void Save(int cartItemId, Collection<ICartItemPackageElement> cartItemPackageElementList);

		void DeleteByCartItem(int cartItemId);

		Collection<ICartItemPackageElement> GetAllByCart(IUserContext context, int cartId);
	}
}