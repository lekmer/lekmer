﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class BlockOrderHistoryControl : BlockControlBase
	{
		private readonly ICustomerSession _customerSession;
		private readonly IOrderService _orderService;

		public BlockOrderHistoryControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICustomerSession customerSession,
			IOrderService orderService)
			: base(templateFactory, blockService)
		{
			_customerSession = customerSession;
			_orderService = orderService;
		}

		protected override BlockContent RenderCore()
		{
			if (!_customerSession.IsSignedIn) return new BlockContent();

			var historyOrders = _orderService.GetAllByCustomer(UserContext.Current, _customerSession.SignedInCustomer.Id);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddCondition("HasOrders", historyOrders.Count > 0);
			fragmentContent.AddVariable("OrderList", RenderOrderList(historyOrders), VariableEncoding.None);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		protected virtual string RenderOrderList(Collection<IOrder> orders)
		{
			if (orders == null) throw new ArgumentNullException("orders");
			if (orders.Count == 0) return null;

			var stringBuilder = new StringBuilder();
			foreach (var order in orders)
			{
				stringBuilder.AppendLine(RenderOrder(order));
			}

			Fragment fragmentOrderList = Template.GetFragment("OrderList");
			fragmentOrderList.AddVariable("Iterate:Order", stringBuilder.ToString(), VariableEncoding.None);
			return fragmentOrderList.Render();
		}

		protected virtual string RenderOrder(IOrder order)
		{
			Fragment fragmentOrder = Template.GetFragment("Order");
			fragmentOrder.AddEntity((IOrderFull)order);
			return fragmentOrder.Render();
		}
	}
}