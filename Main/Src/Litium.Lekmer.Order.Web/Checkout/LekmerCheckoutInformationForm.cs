using System;
using Litium.Lekmer.Customer.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutInformationForm : LekmerInformationForm
	{
		private bool _lockFields;

		public bool IsCivicNumberRequired { get; set; }

		public bool ManualEdit { get; set; }

		public void LockFields()
		{
			_lockFields = true;
		}


		public virtual void MapFromProfileToForm(IProfileCustomerInformation profileCustomerInformation)
		{
			if (profileCustomerInformation == null) throw new ArgumentNullException("profileCustomerInformation");
			
			FirstName = profileCustomerInformation.FirstName;
			LastName = profileCustomerInformation.LastName;

			PhoneNumber = profileCustomerInformation.PhoneNumber;
			CellPhoneNumber = profileCustomerInformation.CellPhoneNumber;

			Email = profileCustomerInformation.Email;

			GenderTypeId = profileCustomerInformation.GenderTypeId;
		}

		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();
			if (CivicNumber != null)
			{
				CivicNumber = CivicNumber.Trim();
			}
		}

		public override void MapFromFormToCustomerInformation(ICustomerInformation customerInformation)
		{
			if (!Validate(IsCivicNumberRequired).IsValid)
			{
				throw new ValidationException("Couldn't validate the customer information.");
			}

			base.MapFromFormToCustomerInformation(customerInformation);
		}

		public virtual void MapFromFormToProfile(IProfileCustomerInformation profileCustomerInformation)
		{
			if (profileCustomerInformation == null) throw new ArgumentNullException("profileCustomerInformation");

			profileCustomerInformation.FirstName = FirstName;
			profileCustomerInformation.LastName = LastName;

			profileCustomerInformation.PhoneNumber = PhoneNumber;
			profileCustomerInformation.CellPhoneNumber = CellPhoneNumber;

			profileCustomerInformation.Email = Email;

			profileCustomerInformation.GenderTypeId = GenderTypeId;
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			string readonlyValue = _lockFields ? "readonly" : string.Empty;

			fragment.AddVariable("Form.CustomerInformation.FirstName.Readonly", readonlyValue);
			fragment.AddVariable("Form.CustomerInformation.LastName.Readonly", readonlyValue);

			fragment.AddCondition("Form.CustomerInformation.ManualEdit", ManualEdit);
		}

		public override ValidationResult Validate()
		{
			return Validate(IsCivicNumberRequired);
		}
	}
}