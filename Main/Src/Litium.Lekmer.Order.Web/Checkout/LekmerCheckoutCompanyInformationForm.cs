﻿using System;
using System.Collections.Specialized;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutCompanyInformationForm : ControlBase
	{
		private bool _lockFields;

		public bool IsCivicNumberRequired { get; set; }

		public string OrganizationNumberFormName
		{
			get { return "company-organizationnumber"; }
		}

		public string NameFormName
		{
			get { return "company-name"; }
		}

		public string FullNameFormName
		{
			get { return "company-fullname"; }
		}

		public string PhoneFormName
		{
			get { return "company-phone"; }
		}

		public string EmailFormName
		{
			get { return "company-email"; }
		}

		public string OrganizationNumber { get; set; }

		public string Name { get; set; }

		public string FullName { get; set; }

		public string Phone { get; set; }

		public string Email { get; set; }

		public bool ManualEdit { get; set; }

		public void MapFromCustomerInformationToForm(ILekmerCustomerInformation customerInformation)
		{
			if (customerInformation == null) throw new ArgumentNullException("customerInformation");

			OrganizationNumber = customerInformation.CivicNumber;
			Name = customerInformation.FirstName;
			FullName = customerInformation.LastName;
			Phone = customerInformation.CellPhoneNumber;
			Email = customerInformation.Email;
		}

		public void MapFromProfileToForm(IProfileCompanyInformation profileCompanyInformation)
		{
			if (profileCompanyInformation == null) throw new ArgumentNullException("profileCompanyInformation");

			Name = profileCompanyInformation.Name;
			FullName = profileCompanyInformation.FullName;
			Phone = profileCompanyInformation.CellPhoneNumber;
			Email = profileCompanyInformation.Email;
		}

		public void MapFromRequestToForm()
		{
			NameValueCollection form = Request.Form;
			OrganizationNumber = form[OrganizationNumberFormName].NullWhenTrimmedEmpty();
			Name = form[NameFormName].NullWhenTrimmedEmpty();
			FullName = form[FullNameFormName].NullWhenTrimmedEmpty();
			Phone = form[PhoneFormName].NullWhenTrimmedEmpty();
			Email = form[EmailFormName].NullWhenTrimmedEmpty();
		}

		public void MapFromFormToCustomerInformation(ILekmerCustomerInformation customerInformation)
		{
			if (!Validate().IsValid) throw new ValidationException("Couldn't validate the company information.");

			customerInformation.CivicNumber = OrganizationNumber.HasValue() ? OrganizationNumber : customerInformation.CivicNumber;
			customerInformation.FirstName = Name;
			customerInformation.LastName = FullName;
			customerInformation.CellPhoneNumber = Phone;
			customerInformation.Email = Email;
			customerInformation.CreatedDate = DateTime.Now;
		}

		public void MapFromFormToProfile(IProfileCompanyInformation profileCompanyInformation)
		{
			if (profileCompanyInformation == null) throw new ArgumentNullException("profileCompanyInformation");

			profileCompanyInformation.Name = Name;
			profileCompanyInformation.FullName = FullName;
			profileCompanyInformation.CellPhoneNumber = Phone;
			profileCompanyInformation.Email = Email;
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.CompanyInformation.OrganizationNumber.Name", OrganizationNumberFormName);
			fragment.AddVariable("Form.CompanyInformation.Name.Name", NameFormName);
			fragment.AddVariable("Form.CompanyInformation.FullName.Name", FullNameFormName);
			fragment.AddVariable("Form.CompanyInformation.CellPhoneNumber.Name", PhoneFormName);
			fragment.AddVariable("Form.CompanyInformation.Email.Name", EmailFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.CompanyInformation.OrganizationNumber.Value", OrganizationNumber);
			fragment.AddVariable("Form.CompanyInformation.Name.Value", Name);
			fragment.AddVariable("Form.CompanyInformation.FullName.Value", FullName);
			fragment.AddVariable("Form.CompanyInformation.CellPhoneNumber.Value", Phone);
			fragment.AddVariable("Form.CompanyInformation.Email.Value", Email);

			string readonlyValue = _lockFields ? "readonly" : string.Empty;

			fragment.AddVariable("Form.CompanyInformation.Readonly", readonlyValue);

			fragment.AddCondition("Form.CompanyInformation.ManualEdit", ManualEdit);
		}

		public ValidationResult Validate(bool isCivicNumberRequired)
		{
			var validationResult = new ValidationResult();
			if (isCivicNumberRequired)
			{
				ValidateOrganizationNumber(validationResult);
			}
			ValidateName(validationResult);
			ValidateFullName(validationResult);
			ValidateEmail(validationResult);
			ValidatePhone(validationResult);
			return validationResult;
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();
			if (IsCivicNumberRequired)
			{
				ValidateOrganizationNumber(validationResult);
			}
			ValidateName(validationResult);
			ValidateFullName(validationResult);
			ValidateEmail(validationResult);
			ValidatePhone(validationResult);
			return validationResult;
		}

		protected void ValidateName(ValidationResult validationResult)
		{
			if (Name.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.NameNotProvided"));
			}
		}

		protected void ValidateFullName(ValidationResult validationResult)
		{
			if (FullName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.FullNameNotProvided"));
			}
		}

		protected void ValidateEmail(ValidationResult validationResult)
		{
			if (Email.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.EmailNotProvided"));
			}
			else if (Email.Length > 320)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.EmailTooLong"));
			}
			else if (!ValidationUtil.IsValidEmail(Email))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.EmailIncorrect"));
			}
		}

		protected void ValidatePhone(ValidationResult validationResult)
		{
			if (Phone.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.PhoneNumberNotProvided"));
			}
		}

		protected void ValidateOrganizationNumber(ValidationResult validationResult)
		{
			if (OrganizationNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.OrganizationNumberNotProvided"));
			}
			//else
			//{
			//	// Organization number validation is disabled, we haven't all logic.
			//	var iso = (ValidationUtil.CountryIso)Enum.Parse(typeof(ValidationUtil.CountryIso), UserContext.Current.Channel.Country.Iso, true);
			//	if (!ValidationUtil.IsValidCivicNumber(OrganizationNumber, iso))
			//	{
			//		validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.Company.OrganizationNumberNotValid"));
			//	}
			//}
		}

		public void LockFields()
		{
			_lockFields = true;
		}
	}
}
