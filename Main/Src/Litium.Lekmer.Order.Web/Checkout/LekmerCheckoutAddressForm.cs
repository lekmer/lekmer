using System.Collections.Specialized;
using Litium.Lekmer.Customer.Web;
using Litium.Lekmer.Payment.Klarna;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutAddressForm : LekmerAddressForm
	{
		private bool _lockFields;

		public LekmerCheckoutAddressForm(string formName, string fieldPrefix)
			: base(formName, fieldPrefix)
		{
		}

		public virtual string FirstName { get; set; }

		public string FirstNameFormName
		{
			get { return "customer-firstname"; }
		}

		public virtual string LastName { get; set; }

		public string LastNameFormName
		{
			get { return "customer-lastname"; }
		}

		public virtual void MapFromAddressToForm(IKlarnaAddress address)
		{
			IKlarnaAddress klarnaAddress = address;
			base.MapFromAddressToForm(address);

			FirstName = klarnaAddress.FirstName;
			LastName = klarnaAddress.LastName;
		}

		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();

			NameValueCollection form = Request.Form;

			FirstName = form[FirstNameFormName].NullWhenTrimmedEmpty();
			LastName = form[LastNameFormName].NullWhenTrimmedEmpty();
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			string readonlyValue = _lockFields ? "readonly" : string.Empty;

			fragment.AddVariable("Form." + FormName + ".City.Readonly", readonlyValue);
			fragment.AddVariable("Form." + FormName + ".StreetAddress.Readonly", readonlyValue);
			fragment.AddVariable("Form." + FormName + ".StreetAddress2.Readonly", readonlyValue);
			fragment.AddVariable("Form." + FormName + ".PostalCode.Readonly", readonlyValue);

			fragment.AddVariable("Form." + FormName + ".HouseNumber.Readonly", readonlyValue);
			fragment.AddVariable("Form." + FormName + ".HouseExtension.Readonly", readonlyValue);
		}

		public void LockFields()
		{
			_lockFields = true;
		}
	}
}