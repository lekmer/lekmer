﻿using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutAlternateAddressForm : LekmerCheckoutCompanyAddressForm
	{
		public LekmerCheckoutAlternateAddressForm(string formName, string fieldPrefix)
			: base(formName, fieldPrefix)
		{
		}

		protected override void ValidateAddressee(ValidationResult validationResult)
		{
			if (Addressee.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(
					AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.AddresseeNotProvided"));
			}
		}
	}
}
