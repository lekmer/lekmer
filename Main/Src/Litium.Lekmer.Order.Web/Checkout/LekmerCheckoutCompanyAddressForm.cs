﻿using System.Collections.Specialized;
using Litium.Lekmer.Customer;
using Litium.Scensum.Core;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutCompanyAddressForm : AddressForm
	{
		public string ReferenceFormName
		{
			get { return FieldPrefix + "-reference"; }
		}

		public string Reference { get; set; }

		public LekmerCheckoutCompanyAddressForm(string formName, string fieldPrefix) 
			: base(formName, fieldPrefix)
		{
		}

		protected override void ValidateAddressee(ValidationResult validationResult)
		{
		}

		public override void MapFieldNamesToFragment(Scensum.Template.Engine.Fragment fragment)
		{
			base.MapFieldNamesToFragment(fragment);
			fragment.AddVariable("Form." + FormName + ".Reference.Name", ReferenceFormName);
		}

		public override void MapFieldValuesToFragment(Scensum.Template.Engine.Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);
			fragment.AddVariable("Form." + FormName + ".Reference.Value", Reference);
		}

		public override void MapFromAddressToForm(Scensum.Customer.IAddress address)
		{
			base.MapFromAddressToForm(address);
			var lekmerAddress = address as ILekmerAddress;
			if (lekmerAddress != null)
			{
				Reference = lekmerAddress.Reference;
			}
		}

		public override void MapFromFormToAddress(Scensum.Customer.IAddress address)
		{
			base.MapFromFormToAddress(address);
			var lekmerAddress = address as ILekmerAddress;
			if (lekmerAddress != null)
			{
				(lekmerAddress).Reference = Reference;
			}
		}

		public virtual void MapFromProfileToForm(IProfileAddress address)
		{
			Addressee = address.Addressee;
			StreetAddress = address.StreetAddress;
			StreetAddress2 = address.StreetAddress2;
			PostalCode = address.PostalCode;
			City = address.City;
			PhoneNumber = address.PhoneNumber;

			Reference = address.Reference;
		}

		public virtual void MapFromFormToProfile(IProfileAddress address)
		{
			address.Addressee = Addressee;
			address.StreetAddress = StreetAddress;
			address.StreetAddress2 = StreetAddress2;
			address.PostalCode = PostalCode;
			address.City = City;
			address.PhoneNumber = PhoneNumber;

			address.Reference = Reference;
		}

		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();
			NameValueCollection form = Request.Form;
			Reference = form[ReferenceFormName].NullWhenTrimmedEmpty();
		}
	}
}