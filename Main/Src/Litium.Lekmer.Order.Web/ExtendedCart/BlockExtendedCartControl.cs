using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Common.Web;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.Order.Web
{
	public class BlockExtendedCartControl : BlockControlBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly ILekmerFormatter _formatter;
		private readonly ICartService _cartService;
		private readonly IOrderService _orderService;

		private int _maxQuantity;
		private CartHelper _cartHelper;

		private readonly Dictionary<LekmerCartItemKey, bool> _updatedItems = new Dictionary<LekmerCartItemKey, bool>();
		private Delivery _delivery;
		private Payment _payment;

		private VoucherDiscountForm _voucherDiscountForm;
		private ExtendedCartForm _extendedCartForm;
		private ExtendedCartFreeProductForm _extendedCartFreeProductForm;

		public BlockExtendedCartControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICartService cartService,
			IOrderService orderService,
			IFormatter formatter) : base(templateFactory, blockService)
		{
			_formatter = (ILekmerFormatter)formatter;
			_cartService = cartService;
			_orderService = orderService;
		}

		public VoucherDiscountForm VoucherDiscountForm
		{
			get { return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm()); }
		}

		public ExtendedCartForm ExtendedCartForm
		{
			get { return _extendedCartForm ?? (_extendedCartForm = new ExtendedCartForm(ResolveUrl(ContentNodeTreeItem.Url))); }
		}

		public ExtendedCartFreeProductForm ExtendedCartFreeProductForm
		{
			get { return _extendedCartFreeProductForm ?? (_extendedCartFreeProductForm = new ExtendedCartFreeProductForm(ResolveUrl(ContentNodeTreeItem.Url))); }
		}


		/// <summary>
		/// Render the component's body and head.
		/// </summary>
		protected override BlockContent RenderCore()
		{
			Initialize();

			ILekmerCartFull cart;
			Collection<ILekmerCartItem> cartItems;
			string body = RenderCart(out cart, out cartItems) ?? RenderEmptyCart();

			string head;
			string footer;
			RenderHeadAndFooter(cart, cartItems, out head, out footer);
			return new BlockContent(head, body, footer);
		}

		private void RenderHeadAndFooter(ILekmerCartFull cart, IEnumerable<ILekmerCartItem> cartItems, out string head, out string footer)
		{
			Fragment fHead = Template.GetFragment("Head");
			Fragment fFooter = Template.GetFragment("Footer");

			var fHeadContentInfo = new FragmentVariableInfo(fHead);
			var fFooterContentInfo = new FragmentVariableInfo(fFooter);

			fHead.AddEntity(Block);
			fFooter.AddEntity(Block);

			// Cart price
			if (cart != null)
			{
				Price cartPrice = cart.GetActualPriceSummary();
				decimal vatSummary = cartPrice.IncludingVat - cartPrice.ExcludingVat;

				AddVariable("Cart.PriceExcludingVatSummary_Formatted.Json", cartPrice.ExcludingVat.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
				AddVariable("Cart.PriceIncludingVatSummary_Formatted.Json", cartPrice.IncludingVat.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
				AddVariable("Cart.VatSummary_Formatted.Json", vatSummary.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			}
			else
			{
				AddVariable("Cart.PriceExcludingVatSummary_Formatted.Json", 0m.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
				AddVariable("Cart.PriceIncludingVatSummary_Formatted.Json", 0m.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
				AddVariable("Cart.VatSummary_Formatted.Json", 0m.ToString(CultureInfo.InvariantCulture), fHead, fFooter);
			}

			if (fHeadContentInfo.HasVariable("Iterate:HeadCartItem") || fFooterContentInfo.HasVariable("Iterate:FooterCartItem"))
			{
				string headCartItems;
				string footerCartItems;
				RenderHeadAndFooterCartItems(cartItems, out headCartItems, out footerCartItems);
				fHead.AddVariable("Iterate:HeadCartItem", headCartItems, VariableEncoding.None);
				fFooter.AddVariable("Iterate:FooterCartItem", footerCartItems, VariableEncoding.None);
			}

			head = fHead.Render();
			footer = fFooter.Render();
		}
		private void RenderHeadAndFooterCartItems(IEnumerable<ILekmerCartItem> cartItems, out string headCartItems, out string footerCartItems)
		{
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			if(cartItems != null)
			{
				int index = 0;
				foreach (var cartItem in cartItems)
				{
					if (index > 2) break;
					Fragment headCartItem = Template.GetFragment("HeadCartItem");
					Fragment footerCartItem = Template.GetFragment("FooterCartItem");

					var product = cartItem.Product;
					var price = cartItem.GetActualPrice();

					AddVariable("CartItem.Id.Json", product.Id.ToString(CultureInfo.InvariantCulture), headCartItem, footerCartItem);
					AddVariable("CartItem.ErpId.Json", product.ErpId, headCartItem, footerCartItem);
					AddVariable("CartItem.Title.Json", product.DisplayTitle, headCartItem, footerCartItem);
					AddVariable("CartItem.PriceInclVat.Json", price.IncludingVat.ToString(CultureInfo.InvariantCulture), headCartItem, footerCartItem);
					AddVariable("CartItem.PriceExclVat.Json", price.ExcludingVat.ToString(CultureInfo.InvariantCulture), headCartItem, footerCartItem);
					AddVariable("CartItem.Quantity.Json", cartItem.Quantity.ToString(CultureInfo.InvariantCulture), headCartItem, footerCartItem);

					headItemBuilder.AppendLine(headCartItem.Render());
					footerItemBuilder.AppendLine(footerCartItem.Render());

					index++;
				}
			}

			headCartItems = headItemBuilder.ToString();
			footerCartItems = footerItemBuilder.ToString();
		}
		private void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}

		private void Initialize()
		{
			_payment = new Payment();
			_delivery = new Delivery();

			_payment.PaymentTypeSelected += OnPaymentTypeSelected;
			_payment.Initialize();
			_delivery.Initialize();

			_cartHelper = new CartHelper();

			if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
			{
				_maxQuantity = 10;
			}
		}

		private void OnPaymentTypeSelected(object sender, Payment.PaymentTypeSelectedEventArgs e)
		{
			_delivery.PaymentTypeId = e.PaymentTypeId;
		}

		/// <summary>
		/// Renders the cart when there's no items added to it.
		/// </summary>
		/// <returns>Rendered empty cart.</returns>
		private string RenderEmptyCart()
		{
			return Template.GetFragment("Empty").Render();
		}

		/// <summary>
		/// Renders a cart with items.
		/// </summary>
		/// <returns>Rendered cart. Null if no items is added to the cart.</returns>
		private string RenderCart(out ILekmerCartFull cart, out Collection<ILekmerCartItem> cartItems)
		{
			cart = null;
			cartItems = null;

			if (!CheckCart(out cart, out cartItems))
			{
				return null;
			}

			if (IsPostBack && !IsVoucherPost() && IsChangeQuantityPost())
			{
				ChangeQuantityOfItems(cart);

				if (!CheckCart(out cart, out cartItems))
				{
					return null;
				}
				cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			}
			else if (ExtendedCartFreeProductForm.IsFormPostBack(BlockId))
			{
				string cartItemOption = ExtendedCartFreeProductForm.GetCartItemOptionId();
				int cartItemOptionId;
				if (!string.IsNullOrEmpty(cartItemOption) && int.TryParse(cartItemOption, out cartItemOptionId) && cartItemOptionId > 0)
				{
					var cartItemOptionService = IoC.Resolve<ICartItemOptionService>();
					cartItemOptionService.Delete(cartItemOptionId);
				}

				if (!CheckCart(out cart, out cartItems))
				{
					return null;
				}
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			var fragmentContentInfo = new FragmentVariableInfo(fragmentContent);

			fragmentContent.AddEntity(Block);

			var freeCartItemOptions = cart.GetFreeCartItems(UserContext.Current);
			fragmentContent.AddVariable("Iterate:CartItem", RenderCartItems(cartItems.Cast<ICartItem>()), VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:FreeCartItem", RenderFreeCartItems(freeCartItemOptions), VariableEncoding.None);

			fragmentContent.AddCondition("Cart.IsProductAbove60LExist", _cartHelper.CheckForMixedWarehouses(cart));
			fragmentContent.AddCondition("Cart.ContainsMixedItems", _cartHelper.CheckForMixedItems(cart));
			fragmentContent.AddCondition("Cart.ContainsOnlyDiapers", _cartHelper.ContainsOnlyDiapers(cart));

			// Cart info
			_cartHelper.RenderCartInfo(fragmentContent, cart, _formatter);

			// Cart price
			_cartHelper.RenderCartPrices(fragmentContent, cart, _formatter);

			fragmentContent.AddVariable("ErrorVoucherWasUsed", GetVoucherWasUsedMessage(), VariableEncoding.None);

			ExtendedCartForm.MapFieldNamesToFragment(fragmentContent);
			ExtendedCartForm.MapFieldValuesToFragment(fragmentContent, BlockId);

			_delivery.Render();

			decimal paymentCost = _payment.GetPaymentCost();
			decimal deliveryPrice = _delivery.GetActualDeliveryPrice(cart);
			decimal optionalDeliveryPrice = _delivery.GetActualOptionalDeliveryPrice(cart);
			decimal diapersDeliveryPrice = _delivery.GetActualDiapersDeliveryPrice(cart);
			bool optionalDeliveryIsUsed = _delivery.GetOrderOptionalDelivery(cart) != null;
			bool diapersDeliveryIsUsed = _delivery.GetOrderDiapersDelivery(cart) != null;

			fragmentContent.AddCondition("PaymentType.HasCost", paymentCost > 0);
			fragmentContent.AddVariable("PaymentType.Cost", _formatter.FormatPrice(Channel.Current, paymentCost));

			fragmentContent.AddCondition("DeliveryMethod.IsUsed", _delivery.CheckForDeliveryUsage(cart));
			fragmentContent.AddVariable("DeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, deliveryPrice));

			fragmentContent.AddCondition("OptionalDeliveryMethod.IsUsed", optionalDeliveryIsUsed);
			fragmentContent.AddVariable("OptionalDeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, optionalDeliveryPrice));

			fragmentContent.AddCondition("DiapersDeliveryMethod.IsUsed", diapersDeliveryIsUsed);
			fragmentContent.AddVariable("DiapersDeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, diapersDeliveryPrice));

			Price cartPrice = cart.GetActualPriceSummary();
			decimal totalFreight = deliveryPrice + optionalDeliveryPrice + diapersDeliveryPrice;
			decimal totalPrice = cartPrice.IncludingVat + totalFreight + paymentCost;
			decimal totalVat = GetVatTotal(cartPrice, totalFreight, paymentCost);

			fragmentContent.AddVariable("Checkout.TotalPriceWithFreight", _formatter.FormatPriceTwoOrLessDecimals(Channel.Current, totalPrice));
			fragmentContent.AddVariable("Checkout.TotalVatSummary", _formatter.FormatPriceTwoOrLessDecimals(Channel.Current, totalVat));
			RenderCampaignDiscount(cart, fragmentContent);

			fragmentContent.AddCondition("isKlarnaPartPayment", IsActivePayment((int)PaymentMethodType.KlarnaPartPayment));

			RenderMonthlypartPaymentCost(fragmentContent, fragmentContentInfo, totalPrice);

			fragmentContent.AddCondition("KlarnaCheckout.IsSelected", PaymentTypeHelper.IsKlarnaCheckoutPayment(_payment.ActivePaymentTypeId));

			return fragmentContent.Render();
		}

		/// <summary>
		/// Renders the items in a cart.
		/// </summary>
		/// <param name="cartItems">The items to render.</param>
		/// <returns>Rendered items.</returns>
		private string RenderCartItems(IEnumerable<ICartItem> cartItems)
		{
			var itemBuilder = new StringBuilder();

			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var groupedCartItems = cartItems.GroupBy(item => string.Format("{0}-{1}-{2}", item.Product.Id, ((ILekmerCartItem)item).SizeId, ((ILekmerCartItem)item).PackageElements.GetPackageElementsHashCode()));

			foreach (ILekmerCartItem cartItem in cartItems)
			{
				itemBuilder.Append(RenderCartItem(cartItem, groupedCartItems));
			}

			return itemBuilder.ToString();
		}

		/// <summary>
		/// Renders a cart item.
		/// </summary>
		/// <returns>Rendered item.</returns>
		private string RenderCartItem(ILekmerCartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems)
		{
			var cartItemKey = new LekmerCartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
			bool itemWasUpdated = _updatedItems.ContainsKey(cartItemKey);

			Fragment fragment = Template.GetFragment("CartItem");
			fragment.AddEntity(cartItem.Product);

			// Price
			_cartHelper.RenderCartItemPrices(fragment, cartItem, _formatter);

			fragment.AddVariable("Product.CampaignInfoPriceHidden", cartItem.Product.CampaignInfo.Price.IncludingVat.ToString());
			fragment.AddVariable("CartItem.Quantity", _formatter.FormatNumber(Channel.Current, cartItem.Quantity));

			fragment.AddCondition("Product.IsInStock", ((ILekmerOrderService)_orderService).CheckNumberInStock(UserContext.Current, cartItem, groupedCartItems));

			int quantityLimit = ObtainQuantityLimit(cartItem);
			fragment.AddVariable("Product.NumberInStockForSize", quantityLimit.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);
			fragment.AddVariable("Iterate:QuantityOption", RenderQuantityOptions(cartItem.Quantity, quantityLimit), VariableEncoding.None);
			fragment.AddCondition("ItemWasUpdated", itemWasUpdated);
			fragment.AddCondition("IsNumberInStockTooSmall", cartItem.Quantity > quantityLimit);
			fragment.AddCondition("CartItem.HasSize", cartItem.SizeId.HasValue);
			fragment.AddEntity(GetSize(cartItem));

			bool isPackageWithSizes = _cartHelper.IsPackageWithSizes(cartItem);
			fragment.AddCondition("CartItem.IsPackageWithSizes", isPackageWithSizes);
			if (isPackageWithSizes)
			{
				fragment.AddVariable("CartItem.PackageElementsHashCode", cartItem.PackageElements.GetPackageElementsHashCode().ToString());
			}

			fragment.AddRegexVariable(
				@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;
					return RenderTagGroup(tagGroupCommonName, cartItem);
				},
				VariableEncoding.None);

			return fragment.Render();
		}

		private string RenderFreeCartItems(IEnumerable<ICartItemOption> cartItemOptions)
		{
			var itemBuilder = new StringBuilder();

			foreach (var cartItemOption in cartItemOptions)
			{
				itemBuilder.Append(RenderFreeCartItem(cartItemOption));
			}

			return itemBuilder.ToString();
		}

		private string RenderFreeCartItem(ICartItemOption cartItemOption)
		{
			if (cartItemOption.Product.CampaignInfo == null)
			{
				cartItemOption.Product.CampaignInfo = IoC.Resolve<IProductCampaignInfo>();
			}

			Fragment fragment = Template.GetFragment("FreeCartItem");
			ExtendedCartFreeProductForm.MapToFreeProductItem(fragment, cartItemOption.Id, BlockId);
			fragment.AddEntity(cartItemOption.Product);

			int cartItemOptionQty = cartItemOption.Quantity;
			int productNumberInStock = cartItemOption.Product.NumberInStock;
			int avaliableCartItemOptionQty = productNumberInStock >= cartItemOptionQty
												? cartItemOptionQty
												: (productNumberInStock > 0 ? productNumberInStock : 0);

			// Price
			_cartHelper.RenderFreeCartItemPrices(fragment, cartItemOption, _formatter);

			fragment.AddCondition("Product.IsInStock", productNumberInStock > 0);

			fragment.AddVariable("CartItemOption.Quantity", _formatter.FormatNumber(Channel.Current, cartItemOptionQty));
			fragment.AddVariable("CartItemOption.AvaliableQuantity", _formatter.FormatNumber(Channel.Current, avaliableCartItemOptionQty));

			return fragment.Render();
		}

		private void RenderMonthlypartPaymentCost(Fragment fragmentContent, FragmentVariableInfo fragmentContentInfo, decimal klarnaSum)
		{
			if (PaymentHelper.IsKlarnaSupported(Channel.Current))
			{
				if (fragmentContentInfo.HasCondition("ShowMonthlyPartPaymentCost") || fragmentContentInfo.HasVariable("KlarnaPartPaymentMonthlyCosts"))
				{
					try
					{
						decimal monthlyPayment = (decimal)PaymentHelper.GetLowestPartPayment(Channel.Current, klarnaSum);

						fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", monthlyPayment > 0);
						fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", _formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyPayment));
					}
					catch (Exception ex)
					{
						_log.Error("Error while getting lowest part payment.", ex);

						fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", false);
						fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", "[ Error while getting lowest part payment. ]", VariableEncoding.None);
					}
				}
			}
			else
			{
				fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", false);
				fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", "[ Klarna payment not supported in current channel. ]");
			}
		}

		private string RenderQuantityOptions(int quantity, int quantitiesToDisplay)
		{
			var optionBuilder = new StringBuilder();

			for (int i = 0; i <= Math.Max(quantitiesToDisplay, quantity); i++)
			{
				Fragment fragmentOption = Template.GetFragment("QuantityOption");
				fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddCondition("IsSelected", i == quantity);
				optionBuilder.AppendLine(fragmentOption.Render());
			}

			return optionBuilder.ToString();
		}

		private string RenderTagGroup(string commonName, ICartItem cartItem)
		{
			if (commonName == null)
			{
				throw new ArgumentNullException("commonName");
			}

			Collection<ITagGroup> tagGroups = IoC.Resolve<ITagGroupService>().GetAllByProduct(UserContext.Current, cartItem.Product.Id);
			ITagGroup tagGroup = tagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}

			Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("TagGroup.Title", AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName), VariableEncoding.None);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);

			return fragmentTagGroup.Render();
		}

		private string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();

			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				Fragment fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}

			return tagBuilder.ToString();
		}

		private static void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}

		private void RenderCampaignDiscount(ILekmerCartFull cart, Fragment fragmentContent)
		{
			decimal totalFixedDiscount;
			bool isFixedDiscount = cart.GetTotalFixedDiscount(UserContext.Current, out totalFixedDiscount);

			decimal totalPercentageDiscount;
			bool isPercentageDiscount = cart.GetTotalPercentageDiscount(isFixedDiscount, totalFixedDiscount, out totalPercentageDiscount);

			fragmentContent.AddCondition("IsCartFixedDiscountApplied", isFixedDiscount);
			if (isFixedDiscount)
			{
				fragmentContent.AddVariable("CartFixedDiscount", _formatter.FormatPriceTwoOrLessDecimals(Channel.Current, totalFixedDiscount));
			}

			fragmentContent.AddCondition("IsCartPercentageDiscountApplied", isPercentageDiscount);
			if (isPercentageDiscount)
			{
				fragmentContent.AddVariable("CartPercentageDiscount", string.Format("{0} %", Math.Round(totalPercentageDiscount, 0, MidpointRounding.AwayFromZero)));
			}
		}

		private bool CheckCart(out ILekmerCartFull cart, out Collection<ILekmerCartItem> cartItems)
		{
			cartItems = null;

			cart = GetCart();
			if (cart == null)
			{
				return false;
			}

			cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (cartItems.Count == 0)
			{
				return false;
			}

			return true;
		}

		protected virtual ILekmerCartFull GetCart()
		{
			LekmerCartHelper.RefreshSessionCart();
			return IoC.Resolve<ICartSession>().Cart as ILekmerCartFull;
		}

		private bool IsVoucherPost()
		{
			string useVoucherValue = Request.Form[VoucherDiscountForm.UseVoucherFormName];

			return useVoucherValue == VoucherDiscountForm.UseVoucherFormValue
				|| useVoucherValue == VoucherDiscountForm.UseVoucherFormText;
		}

		private bool IsChangeQuantityPost()
		{
			return PostMode.Equals(string.Format(CultureInfo.InvariantCulture, ExtendedCartForm.PostModeChangeQuantityValue, BlockId));
		}

		private void ChangeQuantityOfItems(ILekmerCartFull cart)
		{
			foreach (ILekmerCartItem cartItem in cart.GetGroupedCartItemsByProductAndPriceAndSize())
			{
				string key = GetQuantityFormKey(cartItem);

				int? newQuantity = Request.Form.GetInt32OrNull(key);
				if (!newQuantity.HasValue)
				{
					continue;
				}

				if (newQuantity.Value <= 0)
				{
					//Delete item by productId and price and size
					cart.DeleteItem(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
				}
				else if (newQuantity.Value != cartItem.Quantity)
				{
					//Get actual quantity which need add or remove
					int quantity = newQuantity.Value - cartItem.Quantity;

					cart.ChangeItemQuantityForBlockExtendedCartControl(
						cartItem.Product,
						quantity,
						cartItem.SizeId,
						cartItem.IsAffectedByCampaign,
						cartItem.PackageElements);

					var cartItemKey = new LekmerCartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
					_updatedItems[cartItemKey] = true;
				}
			}

			_cartService.Save(UserContext.Current, cart);
			LekmerCartHelper.RefreshSessionCartHard();
		}

		private string GetVoucherWasUsedMessage()
		{
			string voucherWasUsedMessage = string.Empty;
			if (IoC.Resolve<IVoucherSession>().WasUsed)
			{
				voucherWasUsedMessage = AliasHelper.GetAliasValue("Order.Checkout.VoucherWasUsed");
			}
			return voucherWasUsedMessage;
		}

		private static IProductSize GetSize(ILekmerCartItem item)
		{
			return !item.SizeId.HasValue
					? IoC.Resolve<IProductSize>()
					: IoC.Resolve<IProductSizeService>().GetById(item.Product.Id, item.SizeId.Value);
		}

		private int ObtainQuantityLimit(ILekmerCartItem lekmerCartItem)
		{
			var itemWithoutSizes = new Collection<int>();
			var itemWithSizes = new Collection<IPackageElement>();
			lekmerCartItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);
			int numberInStock = ((ILekmerOrderService)_orderService).GetAvaliableNumberInStock(UserContext.Current, lekmerCartItem.Product.Id, lekmerCartItem.SizeId, itemWithoutSizes, itemWithSizes);

			int quantityLimit = 0;
			if (numberInStock > 0) //buy
			{
				quantityLimit = Math.Min(_maxQuantity, numberInStock);
			}
			else if (numberInStock == 0 && ((ILekmerProduct)lekmerCartItem.Product).IsBookable) //book
			{
				quantityLimit = _maxQuantity;
			}

			return quantityLimit;
		}

		private static string GetQuantityFormKey(ILekmerCartItem cartItem)
		{
			if (cartItem.SizeId.HasValue)
			{
				return "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat + "-" + cartItem.SizeId;
			}

			if (cartItem.PackageElements != null && cartItem.PackageElements.Count > 0)
			{
				return "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat + "-" + cartItem.PackageElements.GetPackageElementsHashCode();
			}

			return "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat;
		}

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT.
		/// </summary>
		protected virtual decimal GetVatTotal(Price cartItemsPrice, decimal deliveryPrice, decimal paymentPrice)
		{
			var cartItemsVatSummary = cartItemsPrice.IncludingVat - cartItemsPrice.ExcludingVat;

			var cartItemsAvarageVat = cartItemsPrice.IncludingVat == 0 ? 0 : cartItemsVatSummary / cartItemsPrice.IncludingVat;

			var deliveryVat = deliveryPrice * cartItemsAvarageVat;
			var paymentVat = paymentPrice * cartItemsAvarageVat;

			return cartItemsVatSummary + deliveryVat + paymentVat;
		}

		protected virtual bool IsActivePayment(int paymentTypeId)
		{
			return _payment.ActivePaymentTypeId == paymentTypeId;
		}
	}
}