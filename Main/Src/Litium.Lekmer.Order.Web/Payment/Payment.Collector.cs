﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Collector;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	/// <summary>
	/// Payment control - Collector logic
	/// </summary>
	public partial class Payment
	{
		// Properties.
		private ICollectorPaymentTypeService _collectorPaymentTypeService;
		protected ICollectorPaymentTypeService CollectorPaymentTypeService
		{
			get { return _collectorPaymentTypeService ?? (_collectorPaymentTypeService = IoC.Resolve<ICollectorPaymentTypeService>()); }
		}

		private Collection<ICollectorPaymentType> _availableCollectorPaymentTypes;
		protected Collection<ICollectorPaymentType> AvailableCollectorPaymentTypes
		{
			get { return _availableCollectorPaymentTypes ?? (_availableCollectorPaymentTypes = CollectorPaymentTypeService.GetAllByChannel(UserContext.Current.Channel)); }
		}

		private ICollectorClient _collectorClient;
		protected ICollectorClient CollectorClient
		{
			get { return _collectorClient ?? (_collectorClient = CollectorFactory.CreateCollectorClient(UserContext.Current.Channel.CommonName)); }
		}

		private ICollectorCalculation _collectorCalculation;
		protected ICollectorCalculation CollectorCalculation
		{
			get { return _collectorCalculation ?? (_collectorCalculation = IoC.Resolve<ICollectorCalculation>()); }
		}

		protected string ProfileCollectorPartPaymentId { get; set; }

		public string ActiveCollectorPartPaymentId { get; private set; }

		public bool IsCollectorPaymentActive
		{
			get
			{
				var paymentMethodType = (PaymentMethodType)ActivePaymentTypeId;

				switch (paymentMethodType)
				{
					case PaymentMethodType.CollectorInvoice:
					case PaymentMethodType.CollectorPartPayment:
					case PaymentMethodType.CollectorSpecialPartPayment:
						return true;
				}

				return false;
			}
		}

		// Methods
		protected virtual void InitializeActiveCollectorPartPayment()
		{
			if (PaymentTypeHelper.IsCollectorPartPayment(ActivePaymentTypeId) == false) // just for storing selected option in profile
			{
				if (PaymentForm.CollectorPartPaymentId.HasValue())
				{
					ActiveCollectorPartPaymentId = PaymentForm.CollectorPartPaymentId;
				}
				return;
			}

			string collectorPartPaymentId = PaymentForm.CollectorPartPaymentId;
			bool paymentTypeIdIsValid = IsValidCollectorPartPaymentType(ActivePaymentTypeId, collectorPartPaymentId);

			if (paymentTypeIdIsValid == false)
			{
				collectorPartPaymentId = ProfileCollectorPartPaymentId;
				paymentTypeIdIsValid = IsValidCollectorPartPaymentType(ActivePaymentTypeId, collectorPartPaymentId);
			}

			if (paymentTypeIdIsValid) // == true
			{
				ActiveCollectorPartPaymentId = collectorPartPaymentId;
			}
		}


		// Render
		protected virtual void RenderPaymentTypeExtensionForCollectorAll(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isCollectorPayment = PaymentTypeHelper.IsCollectorPayment(paymentTypeId);
			bool isCollectorPaymentAndSelected = paymentTypeIsSelected && isCollectorPayment;

			fragment.AddCondition("isCollectorPayment", isCollectorPayment);
			fragment.AddCondition("isCollectorPaymentAndSelected", isCollectorPaymentAndSelected);

			if (isCollectorPayment)
			{
				var collectorSetting = CollectorClient.CollectorSetting;
				fragment.AddVariable("Collector.StoreId", collectorSetting.StoreId.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);
			}
		}

		protected virtual void RenderPaymentTypeExtensionForCollectorPersonInvoice(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isCollectorPersonInvoice = PaymentTypeHelper.IsCollectorPersonInvoicePayment(paymentTypeId);
			bool isCollectorPersonInvoiceAndSelected = paymentTypeIsSelected && isCollectorPersonInvoice;

			fragment.AddCondition("isCollectorInvoice", isCollectorPersonInvoice);
			fragment.AddCondition("isCollectorInvoiceAndSelected", isCollectorPersonInvoiceAndSelected);
		}

		protected virtual void RenderPaymentTypeExtensionForCollectorPartPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isCollectorPartPayment = PaymentTypeHelper.IsCollectorPartPayment(paymentTypeId);
			bool isCollectorPartPaymentAndSelected = paymentTypeIsSelected && isCollectorPartPayment;

			string collectorPartPaymentContent = string.Empty;
			if (isCollectorPartPaymentAndSelected)
			{
				collectorPartPaymentContent = RenderCollectorPartPaymentAlternativ(paymentTypeId);
			}

			fragment.AddCondition("isCollectorPartPayment", isCollectorPartPayment);
			fragment.AddCondition("isCollectorPartPaymentAndSelected", isCollectorPartPaymentAndSelected);

			fragment.AddVariable("Iterate:CollectorPartPayment", collectorPartPaymentContent, VariableEncoding.None);
		}

		protected virtual void RenderPaymentTypeExtensionForCollectorSpecialPartPayment(Fragment fragment, int paymentTypeId, bool paymentTypeIsSelected)
		{
			bool isCollectorSpecialPartPayment = PaymentTypeHelper.IsCollectorSpecialPartPayment(paymentTypeId);
			bool isCollectorSpecialPartPaymentAndSelected = paymentTypeIsSelected && isCollectorSpecialPartPayment;

			fragment.AddCondition("isCollectorSpecialPartPayment", isCollectorSpecialPartPayment);
			fragment.AddCondition("isCollectorSpecialPartPaymentAndSelected", isCollectorSpecialPartPaymentAndSelected);

			string collectorSpecialPartPaymentId = "none";
			if (isCollectorSpecialPartPayment)
			{
				string specialPartPaymentId = GetCollectorSpecialPartPaymentId(OrderTotalSum);
				if (specialPartPaymentId.HasValue())
				{
					collectorSpecialPartPaymentId = specialPartPaymentId;
				}
			}

			fragment.AddVariable("Collector.SpecialPartPayment.Id", collectorSpecialPartPaymentId);
		}


		/// <summary>
		/// Gets the Collector special part payment type code.
		/// </summary>
		protected virtual string GetCollectorSpecialPartPaymentId(double totalOrderCost)
		{
			if (totalOrderCost <= 0)
			{
				totalOrderCost = 1;
			}

			ICollectorPaymentType collectorPaymentType = AvailableCollectorPaymentTypes
				.Where(pt => pt.MinPurchaseAmount <= (decimal)totalOrderCost)
				.FirstOrDefault(pt => pt.PartPaymentType == CollectorPartPaymentType.SpecialCampaign);

			if (collectorPaymentType != null)
			{
				return collectorPaymentType.Code;
			}

			return null;
		}


		/// <summary>
		/// List the part payment alternatives.
		/// </summary>
		/// <param name="paymentTypeId"> </param>
		protected virtual string RenderCollectorPartPaymentAlternativ(int paymentTypeId)
		{
			decimal totalOrderCost = (decimal)OrderTotalSum;
			var partPaymentBuilder = new StringBuilder();

			foreach (ICollectorPaymentType collectorPaymentType in AvailableCollectorPaymentTypes)
			{
				if (collectorPaymentType.PartPaymentType == CollectorPartPaymentType.Account && totalOrderCost >= collectorPaymentType.MinPurchaseAmount)
				{
					decimal monthlyCost = CollectorCalculation.CalculateMonthlyCost(totalOrderCost, collectorPaymentType);

					string description = AliasHelper.GetAliasValue("Order.Checkout.Payment.Collector.PartPaymentAccount").Replace("x", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyCost));

					partPaymentBuilder.Append(RenderCollectorPartPaymentSubType(description, collectorPaymentType.Code));
				}
			}

			foreach (ICollectorPaymentType collectorPaymentType in AvailableCollectorPaymentTypes)
			{
				if ((collectorPaymentType.PartPaymentType == CollectorPartPaymentType.InterestFree ||
					collectorPaymentType.PartPaymentType == CollectorPartPaymentType.Annuity) &&
					totalOrderCost >= collectorPaymentType.MinPurchaseAmount)
				{
					decimal monthlyCost = CollectorCalculation.CalculateMonthlyCost(totalOrderCost, collectorPaymentType);

					string description = AliasHelper.GetAliasValue("Order.Checkout.Payment.Collector.PartPayment").Replace("x", Formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyCost));
					description = description.Replace("y", collectorPaymentType.NoOfMonths.ToString(CultureInfo.InvariantCulture));

					partPaymentBuilder.Append(RenderCollectorPartPaymentSubType(description, collectorPaymentType.Code));
				}
			}

			return partPaymentBuilder.ToString();
		}

		protected virtual string RenderCollectorPartPaymentSubType(string description, string collectorPaymentTypeId)
		{
			bool selected = ActiveCollectorPartPaymentId == collectorPaymentTypeId;

			Fragment fragmentPartPayment = _template.GetFragment("PartPayment");

			fragmentPartPayment.AddCondition("IsSelected", selected);
			fragmentPartPayment.AddVariable("PartPayment.Title", description, VariableEncoding.None);
			fragmentPartPayment.AddVariable("PartPayment.Id", collectorPaymentTypeId);

			return fragmentPartPayment.Render();
		}


		// Validation
		protected virtual bool IsValidCollectorPartPaymentType(int paymentTypeId, string partPaymentTypeId)
		{
			bool isPaymentValid = false;

			if (partPaymentTypeId.HasValue())
			{
				isPaymentValid = AvailableCollectorPaymentTypes.Any(p => p.Code == partPaymentTypeId);
			}

			return isPaymentValid;
		}
	}
}