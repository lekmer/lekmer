﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public interface IPaymentForm
	{
		int PaymentTypeId { get; set; }
		int KlarnaPartPaymentId { get; set; }
		string MaksuturvaSubPaymentId { get; set; }
		string QliroPartPaymentId { get; set; }
		string CollectorPartPaymentId { get; set; }

		void MapFromRequest();
		void MapFieldNamesToFragment(Fragment fragment);
	}
}