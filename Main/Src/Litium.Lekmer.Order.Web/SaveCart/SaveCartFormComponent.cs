using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class SaveCartFormComponent : ComponentControl
	{
		private bool _isSaved;
		private bool _isError;

		private SaveCartForm _saveCartForm;
		protected SaveCartForm SaveCartForm
		{
			get { return _saveCartForm ?? (_saveCartForm = new SaveCartForm(ResolveUrl("~" + Request.RelativeUrlWithoutQueryString()))); }
		}

		public SaveCartFormComponent(ITemplateFactory templateFactory)
			: base(templateFactory)
		{
		}

		public override ComponentContent Render()
		{
			if (SaveCartForm == null)
			{
				return new ComponentContent(null, null, null);
			}

			string content = RenderContent();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");

			return new ComponentContent(head, content, footer);
		}

		protected virtual string RenderContent()
		{
			ValidationResult validationResult = null;

			if (SaveCartForm.IsFormPostBack)
			{
				SaveCartForm.MapFromRequest();
				validationResult = SaveCartForm.Validate();
				if (validationResult.IsValid)
				{
					SaveCart();
				}
			}
			else
			{
				SaveCartForm.ClearFrom();
			}

			return RenderSaveCartForm(validationResult);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			return fragment.Render();
		}

		protected virtual string RenderSaveCartForm(ValidationResult validationResult)
		{
			var fragment = Template.GetFragment("Content");

			SaveCartForm.MapFieldsToFragment(fragment);

			RenderValidationResult(fragment, validationResult);
			RenderSaveCartInfo(fragment);

			return fragment.Render();
		}

		protected virtual void RenderValidationResult(Fragment fragment, ValidationResult validationResult)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragment.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		protected virtual void RenderSaveCartInfo(Fragment fragment)
		{
			var message = string.Empty;
			if (_isSaved)
			{
				message = AliasHelper.GetAliasValue("SaveCart.Successfully");
			}
			else if (_isError)
			{
				message = AliasHelper.GetAliasValue("SaveCart.Error");
			}

			fragment.AddVariable("SaveCart.Message", message, VariableEncoding.None);
			fragment.AddCondition("SaveCart.IsSaved", _isSaved);
		}

		protected virtual void SaveCart()
		{
			string email = SaveCartForm.EmailFormValue;

			var saveCartMessengerHelper = new SaveCartMessengerHelper(UserContext.Current.Channel, email);
			bool isCartSaveMailSend = saveCartMessengerHelper.ProcessSaveCartMessage();

			if (isCartSaveMailSend)
			{
				_isSaved = true;
			}
			else
			{
				_isError = true;
			}
		}

		protected override string ModelCommonName
		{
			get { return "BlockSaveCartForm"; } // same model as for BlockSaveCartFormControl
		}
	}
}