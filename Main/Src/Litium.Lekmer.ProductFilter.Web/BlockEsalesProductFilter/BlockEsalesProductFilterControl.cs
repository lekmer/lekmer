﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Web.BlockEsalesProductFilter;
using Litium.Lekmer.ProductFilter.Web.BlockProductFilter;
using Litium.Lekmer.RatingReview;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter.Web
{
	[SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
	public class BlockEsalesProductFilterControl : BlockControlBase<IBlockEsalesProductFilter>, IBlockEsalesControl
	{
		private readonly Regex _tagGroupParameterRegex = new Regex(@"^taggroup(\d+)-tag-id$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		private readonly ITemplateFactory _templateFactory;
		private readonly IBlockEsalesProductFilterService _blockEsalesProductFilterService;
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IPriceIntervalService _priceIntervalService;
		private readonly ILekmerProductService _lekmerProductService;
		private readonly ICategoryService _categoryService;
		private readonly ITagGroupService _tagGroupService;
		private readonly IRatingService _ratingService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingItemProductScoreService _ratingItemProductScoreService;
		private readonly IProductTypeService _productTypeService;
		private readonly ICampaignLandingPageService _campaignLandingPageService;
		private readonly IEsalesProductFilterService _esalesProductFilterService;
		private readonly IEsalesModelService _esalesModelService;

		private readonly BlockFilterOptions _blockFilterOptions = new BlockFilterOptions();
		private readonly FormFilterOptions _formFilterOptions = new FormFilterOptions();
		private bool _filterOptionsInitialized;
		private string _redirectUrl;
		private string _brandTitle;
		private string _categoryTitle;

		private readonly AvailableFilterOptions _availableFilterOptions = new AvailableFilterOptions();

		private ICategoryTree _categoryTree;
		protected ICategoryTree CategoryTree { get { return _categoryTree ?? (_categoryTree = _categoryService.GetAllAsTree(UserContext.Current)); } }

		private EsalesProductFilterFormControl _filterForm;
		protected EsalesProductFilterFormControl FilterForm
		{
			get { return _filterForm ?? (_filterForm = CreateFilterFormControl()); }
		}

		private ProductCollection _products;
		private ILekmerPagingControl _pagingControl;

		private ICommonSession _commonSession;
		protected ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
			set { _commonSession = value; }
		}

		protected IEsalesProductFilterFacet BlockFilterFacet { get; set; }
		protected IEsalesProductFilterFacet UserFilterFacet { get; set; }

		private IEsalesModelComponent _esalesModelComponent;

		private IEsalesPanelRequest _esalesPanelRequest;
		public IEsalesPanelRequest EsalesPanelRequest
		{
			get { return _esalesPanelRequest ?? (_esalesPanelRequest = CreateEsalesPanelRequest()); }
		}

		public IEsalesResponse EsalesResponse { get; set; }

		public new IBlockEsalesProductFilter Block { get; private set; }

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "Block" + Block.BlockType.CommonName; }
		}

		/// <summary>
		/// Template to use when rendering.
		/// </summary>
		protected override int? TemplateId
		{
			get { return Block.TemplateId; }
		}

		public void RedirectPermanent(string newPath)
		{
			Context.Response.Clear();
			Context.Response.Status = "301 Moved Permanently";
			Context.Response.AddHeader("Location", newPath);
			Context.Response.End();
		}

		public BlockEsalesProductFilterControl(
			ITemplateFactory templateFactory,
			IBlockEsalesProductFilterService blockEsalesProductFilterService,
			IAgeIntervalService ageIntervalService,
			IPriceIntervalService priceIntervalService,
			ILekmerProductService lekmerProductService,
			ITagGroupService tagGroupService,
			ICategoryService categoryService,
			IRatingService ratingService,
			IRatingGroupService ratingGroupService,
			IRatingItemProductScoreService ratingItemProductScoreService,
			IProductTypeService productTypeService,
			ICampaignLandingPageService campaignLandingPageService,
			IEsalesProductFilterService esalesProductFilterService,
			IEsalesModelService esalesModelService)
			: base(templateFactory)
		{
			_templateFactory = templateFactory;
			_blockEsalesProductFilterService = blockEsalesProductFilterService;
			_ageIntervalService = ageIntervalService;
			_priceIntervalService = priceIntervalService;
			_lekmerProductService = lekmerProductService;
			_tagGroupService = tagGroupService;
			_categoryService = categoryService;
			_ratingService = ratingService;
			_ratingGroupService = ratingGroupService;
			_ratingItemProductScoreService = ratingItemProductScoreService;
			_productTypeService = productTypeService;
			_campaignLandingPageService = campaignLandingPageService;
			_esalesProductFilterService = esalesProductFilterService;
			_esalesModelService = esalesModelService;
		}

		protected override IBlockEsalesProductFilter GetBlockById(int blockId)
		{
			IBlockEsalesProductFilter blockProductFilter = _blockEsalesProductFilterService.GetById(UserContext.Current, blockId);

			if (blockProductFilter == null)
			{
				throw new BlockNotFoundException(string.Format(CultureInfo.InvariantCulture, "Block eSales product filter with id {0} could not be found.", blockId));
			}

			return blockProductFilter;
		}

		/// <summary>
		/// Renders the control content.
		/// </summary>
		/// <returns>Rendered <see cref="BlockContent"/>.</returns>
		protected override BlockContent RenderCore()
		{
			Initialize();

			if (EsalesResponse == null)
			{
				// if no eSales data available, render empty fragment
				return RenderNoResultsContent();
			}

			FilterForm.Template = Template;

			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");

			fragmentContent.AddVariable("FilterForm", FilterForm.Render(), VariableEncoding.None);
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("ProductTotalCount", _products.TotalCount.ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddVariable("ProductDisplayedCount", _products.Count.ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddVariable("PageCount", _pagingControl.PageCount.ToString(CultureInfo.InvariantCulture));

			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			if (useSecondaryTemplate.HasValue && (bool)useSecondaryTemplate)
			{
				fragmentContent.AddVariable("CampaignPromotion", RenderCampaignPromotion().Replace(Environment.NewLine, ""), VariableEncoding.JavaScriptStringEncode);
			}
			else
			{
				fragmentContent.AddVariable("CampaignPromotion", RenderCampaignPromotion(), VariableEncoding.None);
			}

			fragmentContent.AddEntity(Block);

			string head;
			string footer;
			RenderHeadAndFooter(pagingContent, out head, out footer);

			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#"),
		 SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
		protected virtual void RenderHeadAndFooter(PagingContent pagingContent, out string head, out string footer)
		{
			var idsBuilder = new StringBuilder();
			var erpIdsBuilder = new StringBuilder();
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			for (int i = 0; i < _products.Count; i++)
			{
				if (i > 2) break;

				var product = _products[i];

				Fragment headProductItem = Template.GetFragment("HeadProductItem");
				Fragment footerProductItem = Template.GetFragment("FooterProductItem");
				AddVariable("ProductItem.Id", product.Id.ToString(CultureInfo.InvariantCulture), headProductItem, footerProductItem);
				AddVariable("ProductItem.ErpId", product.ErpId, headProductItem, footerProductItem);
				headItemBuilder.AppendLine(headProductItem.Render());
				footerItemBuilder.AppendLine(footerProductItem.Render());

				if (i == 0)//first or the only item in array
				{
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "\'{0}\'", product.ErpId));
				}
				else
				{
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",\'{0}\'", product.ErpId));
				}
			}

			for (int i = 0; i < _products.Count; i++)
			{
				var product = _products[i];

				if (i == 0)//first or the only item in array
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", product.Id));
				}
				else
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", product.Id));
				}
			}

			var ids = idsBuilder.ToString();
			var erpIds = erpIdsBuilder.ToString();
			var headProductItems = headItemBuilder.ToString();
			var footerProductItems = footerItemBuilder.ToString();

			head = RenderFragment("Head", ids, erpIds, headProductItems, pagingContent.Head);
			footer = RenderFragment("Footer", ids, erpIds, footerProductItems, null);
		}
		protected virtual void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}
		protected virtual string RenderFragment(string fragmentName, string ids, string erpIds, string productItems, string pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", ids);
			fragment.AddVariable("Param.ProductErpIds", erpIds, VariableEncoding.None);
			fragment.AddVariable(fragmentName == "Head" ? "Iterate:HeadProductItem" : "Iterate:FooterProductItem", productItems, VariableEncoding.None);
			return (fragment.Render() ?? string.Empty) + (pagingContent ?? string.Empty);
		}

		protected virtual BlockContent RenderNoResultsContent()
		{
			var fragment = Template.GetFragment("Empty");

			fragment.AddEntity(Block);

			return new BlockContent(string.Empty, fragment.Render(), string.Empty);
		}

		private string RenderCampaignPromotion()
		{
			/*AUTO CAMPAIGN*/

			Fragment fragmentCampaignPromotion = Template.GetFragment("CampaignPromotion");

			var hasSingleCampaignWithImage = false;
			var campaignTag = GetCampaignTagGroup();
			if (campaignTag != null)
			{
				var tags = IoC.Resolve<ITagService>().GetAllByTagGroup(UserContext.Current, campaignTag.Keys.First());
				if (tags != null)
				{
					var selectedCampaign = tags.FirstOrDefault(t => t.Id == campaignTag.Values.First());
					if (selectedCampaign != null)
					{
						var campaign = IoC.Resolve<IProductCampaignService>().GetById(UserContext.Current, Int32.Parse(selectedCampaign.CommonName, CultureInfo.CurrentCulture)) as ILekmerCampaign;
						if (campaign != null)
						{
							var landingPage = _campaignLandingPageService.GetByCampaignId(UserContext.Current, campaign.Id);
							if (landingPage != null && landingPage.RegistryDataList != null && landingPage.RegistryDataList.Count > 0)
							{
								var registryData = landingPage.RegistryDataList[0];
								if (registryData.ImageMediaId.HasValue && registryData.ImageMediaId > 0)
								{
									hasSingleCampaignWithImage = true;
									var image = IoC.Resolve<IImageService>().GetById(UserContext.Current, (int)registryData.ImageMediaId);
									CampaignImageHelper.AddImageVariables(fragmentCampaignPromotion, image, campaign.Title);
									fragmentCampaignPromotion.AddVariable("Campaign.Title", campaign.Title);
									fragmentCampaignPromotion.AddVariable("Campaign.Description", landingPage.Description ?? string.Empty);
								}
							}
						}
					}
				}
			}

			fragmentCampaignPromotion.AddCondition("Campaign.IsSingleWithImage", hasSingleCampaignWithImage);

			return fragmentCampaignPromotion.Render();
		}

		private string RenderProductList()
		{
			int columnCount = GetColumnCount();
			int rowCount = (_pagingControl.PageSize + columnCount - 1) / columnCount;

			return new ProductFilterGridControl<IProduct>
			{
				LekmerProductService = _lekmerProductService,
				RatingService = _ratingService,
				RatingGroupService = _ratingGroupService,
				RatingItemProductScoreService = _ratingItemProductScoreService,
				Items = _products,
				ColumnCount = columnCount,
				RowCount = rowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace",
				BlockRating = Block.BlockRating
			}.Render();
		}

		private void Initialize()
		{
			InitializeFilterOptions();

			_products = GetProducts();
			_pagingControl.TotalCount = _products.TotalCount;

			PopulateAvailableFilterOptions();
			if ((_products.TotalCount == 0) && (!string.IsNullOrEmpty(_redirectUrl)))
			{
				PageTitle = _categoryTitle;
				RedirectPermanent(_redirectUrl);
			}
			else if (!string.IsNullOrEmpty(_redirectUrl))
			{
				PageTitle = _brandTitle;
			}
		}

		private void InitializeBlock()
		{
			if (ContentPage == null || ContentArea == null || BlockId == 0)
			{
				throw new InvalidOperationException("ContentPage, ContentArea and BlockId must be set before calling Render.");
			}

			if (Block == null)
			{
				Block = GetBlockById(BlockId);
			}

			if (Block == null)
			{
				throw new BlockNotFoundException(string.Format(CultureInfo.InvariantCulture, "Block with id {0} could not be found.", BlockId));
			}
		}

		private void InitializeEsalesModel()
		{
			if (ContentPage == null || Block == null)
			{
				throw new InvalidOperationException("ContentPage and Block must be set before calling InitializeEsalesModel.");
			}

			if (Block.EsalesSetting.EsalesModelComponentId.HasValue)
			{
				_esalesModelComponent = _esalesModelService.GetModelComponentById(ContentPage.SiteStructureRegistryId, Block.EsalesSetting.EsalesModelComponentId.Value);
			}
		}

		private void InitializeFilterOptions()
		{
			if (_filterOptionsInitialized)
			{
				return;
			}

			InitializeBlock();
			InitializeEsalesModel();

			PopulateBlockFilterOptions();
			PopulateFormFilterOptions();

			_pagingControl = CreatePagingControl();

			_filterOptionsInitialized = true;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ProductCollection GetProducts()
		{
			if (EsalesResponse != null)
			{
				BlockFilterFacet = _esalesProductFilterService.GetBlockFilterFacet(UserContext.Current, EsalesResponse, _esalesModelComponent);
				UserFilterFacet = _esalesProductFilterService.GetUserFilterFacet(UserContext.Current, EsalesResponse, _esalesModelComponent);

				FilterForm.FilterStatistics = UserFilterFacet;

				return _esalesProductFilterService.GetProducts(UserContext.Current, EsalesResponse, _esalesModelComponent);
			}
			else
			{
				BlockFilterFacet = IoC.Resolve<IEsalesProductFilterFacet>();
				UserFilterFacet = IoC.Resolve<IEsalesProductFilterFacet>();

				FilterForm.FilterStatistics = UserFilterFacet;

				return new ProductCollection();
			}
		}

		private void PopulateBlockFilterOptions()
		{
			if (Block.DefaultCategoryId.HasValue)
			{
				SetBlockFilterCategoryIds(Block.DefaultCategoryId.Value);
			}

			foreach (int brandId in Block.DefaultBrandIds)
			{
				_blockFilterOptions.BrandIds.Add(brandId);
			}

			foreach (int tagId in Block.DefaultTagIds)
			{
				_blockFilterOptions.TagIds.Add(tagId);
			}

			var context = UserContext.Current;

			if (Block.DefaultAgeIntervalId.HasValue)
			{
				IAgeIntervalBand ageIntervalBand = _ageIntervalService.GetAgeInterval(context, Block.DefaultAgeIntervalId.Value);
				if (ageIntervalBand != null)
				{
					_blockFilterOptions.AgeInterval = _ageIntervalService.GetAgeInterval(ageIntervalBand.FromMonth, ageIntervalBand.ToMonth);
				}
			}

			if (Block.DefaultPriceIntervalId.HasValue)
			{
				IPriceIntervalBand priceIntervalBand = _priceIntervalService.GetPriceInterval(context, Block.DefaultPriceIntervalId.Value);
				if (priceIntervalBand != null)
				{
					_blockFilterOptions.PriceInterval = _priceIntervalService.GetPriceInterval(priceIntervalBand.From, priceIntervalBand.To);
				}
			}

			if (Block.DefaultSizeId.HasValue)
			{
				_blockFilterOptions.SizeId = Block.DefaultSizeId.Value;
			}

			_blockFilterOptions.ProductTypeIds.AddRange(Block.TargetProductTypeIds); // Defined only by block

			AddBrandFilter();
		}

		private void AddBrandFilter()
		{
			//check for brand title in url 
			string url = Request.Url.ToString();
			if (url.Contains('_'))
			{
				string title = url.Substring(url.IndexOf('_') + 1);
				if (title.EndsWith("/", StringComparison.Ordinal))
				{
					title = title.Remove(title.Length - 1);
				}
				var contentNodeService = IoC.Resolve<ILekmerContentNodeService>();
				var contentNodeTree = contentNodeService.GetAllAsTree(UserContext.Current);

				var brandNode = contentNodeTree.FindItemByUrl(string.Format(CultureInfo.CurrentCulture, "{0}/", title));
				if (null != brandNode)
				{
					var brandSiteStructureService = IoC.Resolve<IBrandService>();
					var brand = brandSiteStructureService.GetByNode(brandNode.ContentNode.Id);
					if (null != brand)
					{
						var brandId = brand.BrandId;
						_blockFilterOptions.BrandIds.Add(brandId);
						int categoryId = _blockFilterOptions.Level3CategoryId > 0 ? _blockFilterOptions.Level3CategoryId : _blockFilterOptions.Level2CategoryId > 0 ? _blockFilterOptions.Level2CategoryId : _blockFilterOptions.Level1CategoryId > 0 ? _blockFilterOptions.Level1CategoryId : -1;
						var category = _categoryService.GetViewById(UserContext.Current, categoryId);
						if (category != null)
						{
							_redirectUrl = string.Format(CultureInfo.InvariantCulture, "{0}/", url.Substring(0, url.IndexOf('_')));
							_brandTitle = string.Format(CultureInfo.InvariantCulture, "{0} {1} - {2}", title, category.Title, UserContext.Current.Channel.Title);
							_categoryTitle = string.Format(CultureInfo.InvariantCulture, "{0} - {1}", category.Title, UserContext.Current.Channel.Title);
						}
					}
				}
			}
			else
			{
				var categoryId = _blockFilterOptions.Level3CategoryId > 0 ? _blockFilterOptions.Level3CategoryId : _blockFilterOptions.Level2CategoryId > 0 ? _blockFilterOptions.Level2CategoryId : _blockFilterOptions.Level1CategoryId > 0 ? _blockFilterOptions.Level1CategoryId : -1;
				var category = _categoryService.GetViewById(UserContext.Current, categoryId);
				if (category != null)
				{
					PageTitle = string.Format(CultureInfo.InvariantCulture, "{0} - {1}", category.Title, UserContext.Current.Channel.Title);
				}
			}
		}

		private string PageTitle
		{
			set
			{
				ContentPageFull contentPageFull = (ContentPageFull)ContentPage;
				IContentPageSeoSetting contentPageSeoSetting = contentPageFull.ContentPageSeoSetting;
				if (contentPageSeoSetting != null)
				{
					contentPageSeoSetting.Title = value;
				}
			}
		}

		private void PopulateFormFilterOptions()
		{
			FilterForm.MapFromRequest();
			FilterForm.AdjustSortOption(Block.DefaultSort);
			FilterForm.AdjustPageSize(GetPageSizeOptions(), GetDefaultPageSize());
		}

		private Dictionary<int, int> GetCampaignTagGroup()
		{
			var campaignTagGroupId = AutoCampaignPageSetting.Instance.CampaignTagGroupId;
			var parameters = Request.QueryString.AllKeys.Distinct();
			foreach (string parameter in parameters)
			{
				if (parameter.IsEmpty()) continue;

				Match match = _tagGroupParameterRegex.Match(parameter);
				if (!match.Success) continue;

				int tagGroupId;
				if (!int.TryParse(match.Groups[1].Value, out tagGroupId)) continue;

				if (tagGroupId == campaignTagGroupId)
				{
					IEnumerable<int> tagIds = GetSelectedFromQuery(parameter);
					if (tagIds.Count() == 1)
					{
						var newDic = new Dictionary<int, int> { { tagGroupId, tagIds.First() } };
						return newDic;
					}
				}
			}
			if (Block.DefaultTagIds.Count > 0)
			{
				var tags = IoC.Resolve<ITagService>().GetAllByTagGroup(UserContext.Current, campaignTagGroupId);
				if (tags != null)
				{
					foreach (var tag in Block.DefaultTagIds)
					{
						if (tags.FirstOrDefault(t => t.Id == tag) != null)
						{
							var newDic = new Dictionary<int, int> { { campaignTagGroupId, tag } };
							return newDic;
						}
					}
				}
			}
			return null;
		}

		private void SetBlockFilterCategoryIds(int categoryId)
		{
			var treeItem = CategoryTree.FindItemById(categoryId);
			if (treeItem == null)
			{
				return;
			}

			int categoryLevel = treeItem.Level;

			switch (categoryLevel)
			{
				case 1:
					_blockFilterOptions.Level1CategoryId = categoryId;
					break;
				case 2:
					_blockFilterOptions.Level2CategoryId = categoryId;
					break;
				case 3:
					_blockFilterOptions.Level3CategoryId = categoryId;
					break;
			}
		}

		private void PopulateAvailableFilterOptions()
		{
			var context = UserContext.Current;

			// Level 1 categories.
			_availableFilterOptions.Level1Categories = BlockFilterFacet.MainCategories;

			// Level 2 categories.
			if (_blockFilterOptions.Level1CategoryId > 0)
			{
				_availableFilterOptions.Level2Categories = BlockFilterFacet.ParentCategories;
			}

			// Level 3 categories.
			if (_blockFilterOptions.Level2CategoryId > 0)
			{
				_availableFilterOptions.Level3Categories = BlockFilterFacet.Categories;
			}

			// Brands
			_availableFilterOptions.Brands = BlockFilterFacet.Brands;

			// Tags, Sizes
			_availableFilterOptions.TagGroups = _tagGroupService.GetAll(UserContext.Current);
			_availableFilterOptions.Sizes = BlockFilterFacet.Sizes;

			// Age, Price
			PopulateAvailableAgeAndPriceFilterOptions();

			// Sort options.
			_availableFilterOptions.SortOptions = GetSortOptions();

			// SecondaryTemplateId
			_availableFilterOptions.HasSecondaryTemplate = Block.SecondaryTemplateId.HasValue;

			// Page size options.
			_availableFilterOptions.PageSizeOptions = GetPageSizeOptions();

			// Product types
			_availableFilterOptions.ProductTypes = _productTypeService.GetAll(context);
		}

		private void PopulateAvailableAgeAndPriceFilterOptions()
		{
			var context = UserContext.Current;

			Collection<IAgeInterval> ageIntervals = _ageIntervalService.GetAll(context);
			Collection<IPriceInterval> priceIntervals = _priceIntervalService.GetAll(context);

			_availableFilterOptions.AgeIntervalOuter = _ageIntervalService.GetAgeIntervalOuter(ageIntervals);
			_availableFilterOptions.PriceIntervalOuter = _priceIntervalService.GetPriceIntervalOuter(priceIntervals);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private Dictionary<SortOption, string> GetSortOptions()
		{
			var sortOptions = new Dictionary<SortOption, string>
			{
				{SortOption.Popularity, AliasHelper.GetAliasValue("ProductFilter.SortOption.Popularity")},
				{SortOption.IsNewFrom, AliasHelper.GetAliasValue("ProductFilter.SortOption.IsNewFrom")},
			
				{SortOption.TitleAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleAsc")},
				{SortOption.TitleDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleDesc")},

				{SortOption.PriceAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceAsc")},
				{SortOption.PriceDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceDesc")},

				{SortOption.DiscountPercentDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.DiscountPercentDesc")}
			};

			return sortOptions;
		}

		private IEnumerable<int> GetPageSizeOptions()
		{
			string settingName = CommonSession.IsViewportMobile ? "PageSizeOptionsMobile" : "PageSizeOptions";
			string settingPageSizeOptions = Template.GetSettingOrNull(settingName) ?? string.Empty;

			var pageSizeOptions = settingPageSizeOptions
				.Split(',')
				.Select(pageSize =>
				{
					int intPageSize;
					return int.TryParse(pageSize, out intPageSize) ? intPageSize : 0;
				})
				.Where(pageSize => pageSize > 0)
				.ToList();

			if (pageSizeOptions.Count < 1)
			{
				return new List<int> { GetDefaultPageSize() };
			}

			return pageSizeOptions;
		}

		private int GetDefaultPageSize()
		{
			string settingName = CommonSession.IsViewportMobile ? "DefaultPageSizeMobile" : "DefaultPageSize";
			string settingDefaultPageSize = Template.GetSettingOrNull(settingName);

			int defaultPageSize;
			if (!int.TryParse(settingDefaultPageSize, out defaultPageSize))
			{
				defaultPageSize = 20;
			}

			return defaultPageSize;
		}

		private int GetColumnCount()
		{
			string settingName = CommonSession.IsViewportMobile ? "ColumnCountMobile" : "ColumnCount";
			int columnCount;
			if (!int.TryParse(Template.GetSettingOrNull(settingName), out columnCount))
			{
				columnCount = 1;
			}
			return columnCount;
		}

		private IEnumerable<int> GetSelectedFromQuery(string parameter)
		{
			if (Request.QueryString[parameter].IsNullOrTrimmedEmpty())
			{
				return new int[0];
			}

			return Request.QueryString[parameter]
				.Split(',')
				.Select(value =>
				{
					int intValue;
					return int.TryParse(value, out intValue) ? intValue : 0;
				})
				.Where(value => value != 0).Distinct();
		}

		private ILekmerPagingControl CreatePagingControl()
		{
			var pagingControl = (ILekmerPagingControl)IoC.Resolve<IPagingControl>();

			pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = FilterForm.PageSize;
			pagingControl.ViewAllModeAllowed = true;

			pagingControl.Initialize();

			return pagingControl;
		}

		protected virtual EsalesProductFilterFormControl CreateFilterFormControl()
		{
			return new EsalesProductFilterFormControl
			{
				BlockFilterOptions = _blockFilterOptions,
				FormFilterOptions = _formFilterOptions,
				AvailableFilterOptions = _availableFilterOptions,
				//FilterStatistics = _filterStatistics
			};
		}

		protected virtual IEsalesPanelRequest CreateEsalesPanelRequest()
		{
			InitializeFilterOptions();

			EsalesPanelRequestBuilder esalesPanelRequestBuilder = new EsalesPanelRequestBuilder
			{
				EsalesModelComponent = _esalesModelComponent,
				Channel = Channel.Current,
				BlockFilterOptions = _blockFilterOptions,
				FormFilterOptions = _formFilterOptions,
				SortOption = FilterForm.SortOption,
				Paging = new Paging(_pagingControl.SelectedPage, _pagingControl.PageSize)
			};

			IEsalesPanelRequest panelRequest = esalesPanelRequestBuilder.BuildEsalesPanelRequest();

			return panelRequest;
		}

		protected override Template CreateTemplate()
		{
			if (FilterForm.UseSecondaryTemplate && Block.SecondaryTemplateId.HasValue)
			{
				return _templateFactory.Create(Block.SecondaryTemplateId.Value);
			}

			return base.CreateTemplate();
		}
	}
}