using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.ProductFilter.Web.BlockEsalesProductFilter
{
	public class FormFilterOptions
	{
		private readonly Collection<int> _level1CategoryIds = new Collection<int>();
		public Collection<int> Level1CategoryIds
		{
			get { return _level1CategoryIds; }
		}
		
		private readonly Collection<int> _level2CategoryIds = new Collection<int>();
		public Collection<int> Level2CategoryIds
		{
			get { return _level2CategoryIds; }
		}

		private readonly Collection<int> _level3CategoryIds = new Collection<int>();
		public Collection<int> Level3CategoryIds
		{
			get { return _level3CategoryIds; }
		}

		private readonly Collection<int> _brandIds = new Collection<int>();
		public Collection<int> BrandIds
		{
			get { return _brandIds; }
		}

		private readonly Collection<IEnumerable<int>> _groupedTagIds = new Collection<IEnumerable<int>>();
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
		public Collection<IEnumerable<int>> GroupedTagIds
		{
			get { return _groupedTagIds; }
		}

		public IAgeIntervalBand AgeInterval { get; set; }

		public IPriceIntervalBand PriceInterval { get; set; }

		private readonly Collection<int> _sizeIds = new Collection<int>();
		public Collection<int> SizeIds
		{
			get { return _sizeIds; }
		}
	}
}