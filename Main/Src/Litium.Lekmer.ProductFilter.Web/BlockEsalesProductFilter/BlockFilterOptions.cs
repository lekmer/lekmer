using System.Collections.ObjectModel;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.ProductFilter.Web.BlockEsalesProductFilter
{
	public class BlockFilterOptions
	{
		public int Level1CategoryId { get; set; }

		public int Level2CategoryId { get; set; }

		public int Level3CategoryId { get; set; }

		private readonly Collection<int> _brandIds = new Collection<int>();
		public Collection<int> BrandIds
		{
			get { return _brandIds; }
		}

		private readonly Collection<int> _tagIds = new Collection<int>();
		public Collection<int> TagIds
		{
			get { return _tagIds; }
		}

		public IAgeIntervalBand AgeInterval { get; set; }

		public IPriceIntervalBand PriceInterval { get; set; }

		public int SizeId { get; set; }

		private readonly Collection<int> _productTypeIds = new Collection<int>();
		public Collection<int> ProductTypeIds
		{
			get { return _productTypeIds; }
		}

		public SortOption SortOption { get; set; }
	}
}