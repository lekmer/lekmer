using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter.Web.BlockEsalesProductFilter
{
	public class EsalesPanelRequestBuilder
	{
		private const string KeyPanelPath = "panel-path";
		private const string DefaultPanelPath = "/category-page-";

		private const string KeyArgBlockFilter = "arg-block-filer";
		private const string DefaultArgBlockFilter = "block_filter";

		private const string KeyArgUserFilter = "arg-user-filter";
		private const string DefaultArgUserFilter = "user_filter";

		private const string KeyArgProductsSortBy = "arg-products-sort-by";
		private const string DefaultArgProductsSortBy = "products_sort_by";

		private const string KeyArgProductsWindowFirst = "arg-products-window-first";
		private const string DefaultArgProductsWindowFirst = "products_window_first";

		private const string KeyArgProductsWindowLast = "arg-products-window-last";
		private const string DefaultArgProductsWindowLast = "products_window_last";

		private const string KeyPopularitySortValue = "arg-popularity-sort-value";

		private IEsalesPanelRequest _panelRequest;

		public BlockFilterOptions BlockFilterOptions { get; set; }
		public FormFilterOptions FormFilterOptions { get; set; }
		public SortOption SortOption { get; set; }
		public IPaging Paging { get; set; }

		public IChannel Channel { get; set; }

		public IEsalesModelComponent EsalesModelComponent { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
		public virtual IEsalesPanelRequest BuildEsalesPanelRequest()
		{
			_panelRequest = new EsalesPanelRequest();

			string panelPath = EsalesModelComponent.Model.TryGetParameterValue(KeyPanelPath, DefaultPanelPath + Channel.Country.Iso.ToLowerInvariant());
			_panelRequest.PanelPath = panelPath;

			_panelRequest.PanelArguments = new Dictionary<string, string>();

			string buildBlockFilter = BuildBlockFilter();
			if (buildBlockFilter.HasValue())
			{
				string argBlockFilter = EsalesModelComponent.TryGetParameterValue(KeyArgBlockFilter, DefaultArgBlockFilter);
				_panelRequest.PanelArguments[argBlockFilter] = buildBlockFilter;
			}

			string buildUserFilter = BuildUserFilter();
			if (buildUserFilter.HasValue())
			{
				string argUserFilter = EsalesModelComponent.TryGetParameterValue(KeyArgUserFilter, DefaultArgUserFilter);
				_panelRequest.PanelArguments[argUserFilter] = buildUserFilter;
			}

			string argProductsSortBy = EsalesModelComponent.TryGetParameterValue(KeyArgProductsSortBy, DefaultArgProductsSortBy);
			_panelRequest.PanelArguments[argProductsSortBy] = BuildSortOptions();

			string argProductsWindowFirst = EsalesModelComponent.TryGetParameterValue(KeyArgProductsWindowFirst, DefaultArgProductsWindowFirst);
			_panelRequest.PanelArguments[argProductsWindowFirst] = Paging.FirstItemNumber.ToString(CultureInfo.InvariantCulture);

			string argProductsWindowLast = EsalesModelComponent.TryGetParameterValue(KeyArgProductsWindowLast, DefaultArgProductsWindowLast);
			_panelRequest.PanelArguments[argProductsWindowLast] = Paging.LastItemNumber.ToString(CultureInfo.InvariantCulture);

			return _panelRequest;
		}

		/// <summary>
		/// Compose set of filters
		/// </summary>
		protected virtual string BuildBlockFilter()
		{
			var filter = new StringBuilder();

			if (BlockFilterOptions != null)
			{
				AppendFilter(BlockFilterOptions.Level1CategoryId, EsalesXmlNodes.NodeMainCategoryId, filter);
				AppendFilter(BlockFilterOptions.Level2CategoryId, EsalesXmlNodes.NodeParentCategoryId, filter);
				AppendFilter(BlockFilterOptions.Level3CategoryId, EsalesXmlNodes.NodeCategoryId, filter);

				AppendFilter(BlockFilterOptions.BrandIds, EsalesXmlNodes.NodeBrandId, filter);

				AppendFilter(BlockFilterOptions.SizeId, EsalesXmlNodes.NodeSizeId, filter);

				AppendAgeFilter(BlockFilterOptions.AgeInterval, filter);
				AppendPriceFilter(BlockFilterOptions.PriceInterval, filter);

				AppendTagsFilter(filter);

				AppendProductTypeFilter(filter);
			}

			return filter.ToString();
		}

		/// <summary>
		/// Compose set of filters
		/// </summary>
		protected virtual string BuildUserFilter()
		{
			var filter = new StringBuilder();

			if (FormFilterOptions != null)
			{
				AppendFilter(FormFilterOptions.Level1CategoryIds, EsalesXmlNodes.NodeMainCategoryId, filter);
				AppendFilter(FormFilterOptions.Level2CategoryIds, EsalesXmlNodes.NodeParentCategoryId, filter);
				AppendFilter(FormFilterOptions.Level3CategoryIds, EsalesXmlNodes.NodeCategoryId, filter);

				AppendFilter(FormFilterOptions.BrandIds, EsalesXmlNodes.NodeBrandId, filter);

				AppendFilter(FormFilterOptions.SizeIds, EsalesXmlNodes.NodeSizeId, filter);

				AppendAgeFilter(FormFilterOptions.AgeInterval, filter);
				AppendPriceFilter(FormFilterOptions.PriceInterval, filter);

				if (FormFilterOptions.GroupedTagIds != null && FormFilterOptions.GroupedTagIds.Count > 0)
				{
					foreach (IEnumerable<int> tagGroup in FormFilterOptions.GroupedTagIds)
					{
						var tags = new Collection<int>(tagGroup.ToList());
						AppendFilter(tags, EsalesXmlNodes.NodeTagId, filter);
					}
				}
			}

			return filter.ToString();
		}

		/// <summary>
		/// Compose set of sort options
		/// </summary>
		protected virtual string BuildSortOptions()
		{
			var sortOptions = new StringBuilder();
			sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", EsalesXmlNodes.NodeProductInStock, EsalesXmlNodes.ValueDescending));

			switch (SortOption)
			{
				case SortOption.PriceAsc:
				{
					sortOptions.Append(",");
					sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductCurrentPrice), EsalesXmlNodes.ValueAscending));
					break;
				}
				case SortOption.PriceDesc:
				{
					sortOptions.Append(",");
					sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductCurrentPrice), EsalesXmlNodes.ValueDescending));
					break;
				}
				case SortOption.TitleAsc:
				{
					sortOptions.Append(",");
					sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductTitle), EsalesXmlNodes.ValueAscending));
					break;
				}
				case SortOption.TitleDesc:
				{
					sortOptions.Append(",");
					sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductTitle), EsalesXmlNodes.ValueDescending));
					break;
				}
				case SortOption.IsNewFrom:
				{
					sortOptions.Append(",");
					sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", EsalesXmlNodes.NodeIsNewFrom, EsalesXmlNodes.ValueDescending));
					break;
				}
				case SortOption.DiscountPercentDesc:
				{
					sortOptions.Append(",");
					sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", GetElementName(Channel, EsalesXmlNodes.NodeProductDiscountPercent), EsalesXmlNodes.ValueDescending));
					break;
				}
			}

			// Popularity
			string popularitySortValue = EsalesModelComponent.TryGetParameterValue(KeyPopularitySortValue, EsalesXmlNodes.ValueSales);
			sortOptions.Append(",");
			sortOptions.Append(string.Format(CultureInfo.InvariantCulture, "{0} {1}", popularitySortValue, EsalesXmlNodes.ValueDescending));

			return sortOptions.ToString();
		}


		protected virtual string GetElementName(IChannel channel, string elementName)
		{
			return string.Concat(elementName, "_", channel.Id.ToString(CultureInfo.InvariantCulture));
		}

		protected virtual void AppendFilter(Collection<int> ids, string nodeName, StringBuilder filter)
		{
			if (ids != null && ids.Count > 0)
			{
				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");

				foreach (int id in ids)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", nodeName, id));
				}

				filter.Length -= 4;
				filter.Append(")");
			}
		}

		protected virtual void AppendFilter(int id, string nodeName, StringBuilder filter)
		{
			if (id > 0)
			{
				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");

				filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}'", nodeName, id));

				filter.Append(")");
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "AppendAge")]
		protected virtual void AppendAgeFilter(IAgeIntervalBand ageInterval, StringBuilder filter)
		{
			if (ageInterval != null)
			{
				int ageFrom = ageInterval.FromMonth ?? 0;
				int ageTo = ageInterval.ToMonth ?? 1000000;

				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");
				filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:['{1}','{2}']", EsalesXmlNodes.NodeAgeFromMonth, ageFrom, ageTo));
				filter.Append(")");

				filter.Append(" AND ");

				filter.Append("(");
				filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:['{1}','{2}']", EsalesXmlNodes.NodeAgeToMonth, ageFrom, ageTo));
				filter.Append(")");
			}
		}

		protected virtual void AppendPriceFilter(IPriceIntervalBand priceInterval, StringBuilder filter)
		{
			if (priceInterval != null)
			{
				decimal priceFrom = priceInterval.From ?? 1;
				decimal priceTo = priceInterval.To ?? 1000000;

				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");
				filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:['{1}','{2}']", GetElementName(Channel, EsalesXmlNodes.NodeProductCurrentPrice), priceFrom, priceTo));
				filter.Append(")");
			}
		}

		protected virtual void AppendTagsFilter(StringBuilder filter)
		{
			if (BlockFilterOptions.TagIds != null && BlockFilterOptions.TagIds.Count > 0)
			{
				foreach (int tagId in BlockFilterOptions.TagIds)
				{
					AppendFilter(tagId, EsalesXmlNodes.NodeTagId, filter); // Tags are added here with 'AND'
				}
			}
		}

		protected virtual void AppendProductTypeFilter(StringBuilder filter)
		{
			if (BlockFilterOptions.ProductTypeIds != null && BlockFilterOptions.ProductTypeIds.Count == 1)
			{
				int productTypeId = BlockFilterOptions.ProductTypeIds[0];
				
				string productTypeValue = string.Empty;
				if (ProductExtensions.IsProduct(productTypeId))
				{
					productTypeValue = EsalesXmlNodes.ValueProductTypeProduct;
				}
				else if (ProductExtensions.IsPackage(productTypeId))
				{
					productTypeValue = EsalesXmlNodes.ValueProductTypePackage;
				}

				if (productTypeValue.HasValue())
				{
					if (filter.Length > 0)
					{
						filter.Append(" AND ");
					}

					filter.Append("(");

					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}'", EsalesXmlNodes.NodeProductType, productTypeValue));

					filter.Append(")");
				}
			}
		}
	}
}
