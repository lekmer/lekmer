﻿using System;
using System.Globalization;

namespace Litium.Lekmer.ProductFilter.Web
{
	public static class Converter
	{
		public static bool TryParseLocalDecimalAsCurrency(this string value, out decimal result)
		{
			return decimal.TryParse(value, NumberStyles.Number, CreateCurrencyDecimalCulture(), out result)
				&& MatchDecimalFormatInDb(result);
		}

		private static NumberFormatInfo CreateCurrencyDecimalCulture()
		{
			NumberFormatInfo decimalFormatInfoForCurrency = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
			decimalFormatInfoForCurrency.NumberDecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
			return decimalFormatInfoForCurrency;
		}

		private static bool MatchDecimalFormatInDb(decimal value)
		{
			const int currencyTotalNumberOfDigits = 16;
			const int currencyNumberOfDigitsAfterPoint = 2;
			const int maxNumberOfIntegralDigits = currencyTotalNumberOfDigits - currencyNumberOfDigitsAfterPoint;

			var integralPart = Math.Abs(Math.Truncate(Math.Round(value, currencyNumberOfDigitsAfterPoint)));
			var currentNumberOfIntegralDigits = integralPart.ToString(CultureInfo.InvariantCulture).Length;
			return currentNumberOfIntegralDigits <= maxNumberOfIntegralDigits;
		}
	}
}
