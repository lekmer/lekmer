﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Web.BlockProductFilter;
using Litium.Lekmer.RatingReview;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter.Web
{
	[SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
	public class BlockProductFilterControl : BlockControlBase<IBlockProductFilter>
	{
		private bool _isCookieMode;
		private readonly ITemplateFactory _templateFactory;
		private readonly IBlockProductFilterService _blockProductFilterService;
		private readonly IBrandService _brandService;
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IPriceIntervalService _priceIntervalService;
		private readonly IFilterProductService _filterProductService;
		private readonly ILekmerProductService _lekmerProductService;
		private readonly ICategoryService _categoryService;
		private readonly ITagGroupService _tagGroupService;
		private readonly ISizeService _sizeService;
		private readonly IRatingService _ratingService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingItemProductScoreService _ratingItemProductScoreService;
		private readonly IProductTypeService _productTypeService;
		private readonly ICampaignLandingPageService _campaignLandingPageService;

		private readonly SelectedFilterOptions _selectedFilterOptions = new SelectedFilterOptions();
		private readonly AvailableFilterOptions _availableFilterOptions = new AvailableFilterOptions();

		private ProductCollection _products;
		private FilteredProductRangeStatistics _filterStatistics;
		private ILekmerPagingControl _pagingControl;
		private ICommonSession _commonSession;

		private readonly Regex _tagGroupParameterRegex = new Regex(@"^taggroup(\d+)-tag-id$", RegexOptions.IgnoreCase | RegexOptions.Compiled);
		private string _redirectUrl;
		private string _brandTitle;
		private string _categoryTitle;

		public BlockProductFilterControl(
			ITemplateFactory templateFactory, IBlockProductFilterService blockProductFilterService,
			IBrandService brandService, IAgeIntervalService ageIntervalService, IPriceIntervalService priceIntervalService,
			IFilterProductService filterProductService, ILekmerProductService lekmerProductService,
			ITagGroupService tagGroupService, ICategoryService categoryService,
			ISizeService sizeService, IRatingService ratingService, IRatingGroupService ratingGroupService,
			IRatingItemProductScoreService ratingItemProductScoreService,
			IProductTypeService productTypeService,
			ICampaignLandingPageService campaignLandingPageService)
			: base(templateFactory)
		{
			_templateFactory = templateFactory;
			_blockProductFilterService = blockProductFilterService;
			_brandService = brandService;
			_ageIntervalService = ageIntervalService;
			_priceIntervalService = priceIntervalService;
			_filterProductService = filterProductService;
			_lekmerProductService = lekmerProductService;
			_tagGroupService = tagGroupService;
			_categoryService = categoryService;
			_sizeService = sizeService;
			_ratingService = ratingService;
			_ratingGroupService = ratingGroupService;
			_ratingItemProductScoreService = ratingItemProductScoreService;
			_productTypeService = productTypeService;
			_campaignLandingPageService = campaignLandingPageService;
		}

		public ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
			set { _commonSession = value; }
		}

		protected override IBlockProductFilter GetBlockById(int blockId)
		{
			IBlockProductFilter blockProductFilter = _blockProductFilterService.GetById(UserContext.Current, blockId);

			if (blockProductFilter == null)
			{
				throw new BlockNotFoundException(
					string.Format(CultureInfo.InvariantCulture, "Block product filter with id {0} could not be found.", blockId));
			}

			return blockProductFilter;
		}

		/// <summary>
		/// Renders the control content.
		/// </summary>
		/// <returns>Rendered <see cref="BlockContent"/>.</returns>
		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingContent pagingContent = _pagingControl.Render();

			var filterFormControl = CreateFilterFormControl();

			Fragment fragmentContent = Template.GetFragment("Content");

			fragmentContent.AddVariable("FilterForm", filterFormControl.Render(), VariableEncoding.None);
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("ProductTotalCount", _products.TotalCount.ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddVariable("ProductDisplayedCount", _products.Count.ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddVariable("PageCount", _pagingControl.PageCount.ToString(CultureInfo.InvariantCulture));

			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			if (useSecondaryTemplate.HasValue && (bool)useSecondaryTemplate)
			{
				fragmentContent.AddVariable("CampaignPromotion", RenderCampaignPromotion().Replace(Environment.NewLine, ""), VariableEncoding.JavaScriptStringEncode);
			}
			else
			{
				fragmentContent.AddVariable("CampaignPromotion", RenderCampaignPromotion(), VariableEncoding.None);
			}
			
			fragmentContent.AddEntity(Block);

			string head;
			string footer;
			RenderHeadAndFooter(pagingContent, out head, out footer);

			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#"),
		 SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "2#")]
		protected virtual void RenderHeadAndFooter(PagingContent pagingContent, out string head, out string footer)
		{
			var idsBuilder = new StringBuilder();
			var erpIdsBuilder = new StringBuilder();
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			for (int i = 0; i < _products.Count; i++)
			{
				if (i > 2) break;

				var product = _products[i];

				Fragment headProductItem = Template.GetFragment("HeadProductItem");
				Fragment footerProductItem = Template.GetFragment("FooterProductItem");
				AddVariable("ProductItem.Id", product.Id.ToString(CultureInfo.InvariantCulture), headProductItem, footerProductItem);
				AddVariable("ProductItem.ErpId", product.ErpId, headProductItem, footerProductItem);
				headItemBuilder.AppendLine(headProductItem.Render());
				footerItemBuilder.AppendLine(footerProductItem.Render());

				if (i == 0)//first or the only item in array
				{
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "\'{0}\'", product.ErpId));
				}
				else
				{
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",\'{0}\'", product.ErpId));
				}
			}

			for (int i = 0; i < _products.Count; i++)
			{
				var product = _products[i];

				if (i == 0)//first or the only item in array
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", product.Id));
				}
				else
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", product.Id));
				}
			}

			var ids = idsBuilder.ToString();
			var erpIds = erpIdsBuilder.ToString();
			var headProductItems = headItemBuilder.ToString();
			var footerProductItems = footerItemBuilder.ToString();

			head = RenderFragment("Head", ids, erpIds, headProductItems, pagingContent.Head);
			footer = RenderFragment("Footer", ids, erpIds, footerProductItems, null);
		}
		protected virtual void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}
		protected virtual string RenderFragment(string fragmentName, string ids, string erpIds, string productItems, string pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", ids);
			fragment.AddVariable("Param.ProductErpIds", erpIds, VariableEncoding.None);
			fragment.AddVariable(fragmentName == "Head" ? "Iterate:HeadProductItem" : "Iterate:FooterProductItem", productItems, VariableEncoding.None);
			return (fragment.Render() ?? string.Empty) + (pagingContent ?? string.Empty);
		}

		private string RenderCampaignPromotion()
		{
			/*AUTO CAMPAIGN*/

			Fragment fragmentCampaignPromotion = Template.GetFragment("CampaignPromotion");

			var hasSingleCampaignWithImage = false;
			var campaignTag = GetCampaignTagGroup();
			if (campaignTag != null)
			{
				var tags = IoC.Resolve<ITagService>().GetAllByTagGroup(UserContext.Current, campaignTag.Keys.First());
				if (tags != null)
				{
					var selectedCampaign = tags.FirstOrDefault(t => t.Id == campaignTag.Values.First());
					if (selectedCampaign != null)
					{
						var campaign = IoC.Resolve<IProductCampaignService>().GetById(UserContext.Current, Int32.Parse(selectedCampaign.CommonName, CultureInfo.CurrentCulture)) as ILekmerCampaign;
						if (campaign != null)
						{
							var landingPage = _campaignLandingPageService.GetByCampaignId(UserContext.Current, campaign.Id);
							if (landingPage != null && landingPage.RegistryDataList != null && landingPage.RegistryDataList.Count > 0)
							{
								var registryData = landingPage.RegistryDataList[0];
								if (registryData.ImageMediaId.HasValue && registryData.ImageMediaId > 0)
								{
									hasSingleCampaignWithImage = true;
									var image = IoC.Resolve<IImageService>().GetById(UserContext.Current, (int)registryData.ImageMediaId);
									CampaignImageHelper.AddImageVariables(fragmentCampaignPromotion, image, campaign.Title);
									fragmentCampaignPromotion.AddVariable("Campaign.Title", campaign.Title);
									fragmentCampaignPromotion.AddVariable("Campaign.Description", landingPage.Description ?? string.Empty);
								}
							}
						}
					}
				}
			}

			fragmentCampaignPromotion.AddCondition("Campaign.IsSingleWithImage", hasSingleCampaignWithImage);

			return fragmentCampaignPromotion.Render();
		}

		private string RenderProductList()
		{
			int columnCount = GetColumnCount();
			int rowCount = (_pagingControl.PageSize + columnCount - 1) / columnCount;

			if (_isCookieMode)
			{
				return new LekmerGridControl<IProduct>
				{
					RatingService = _ratingService,
					RatingGroupService = _ratingGroupService,
					RatingItemProductScoreService = _ratingItemProductScoreService,
					Items = _products,
					ColumnCount = columnCount,
					RowCount = rowCount,
					Template = Template,
					ListFragmentName = "ProductList",
					RowFragmentName = "ProductRow",
					ItemFragmentName = "Product",
					EmptySpaceFragmentName = "EmptySpace",
					BlockRating = Block.BlockRating
				}.Render();
			}

			return new ProductFilterGridControl<IProduct>
			{
				LekmerProductService = _lekmerProductService,
				RatingService = _ratingService,
				RatingGroupService = _ratingGroupService,
				RatingItemProductScoreService = _ratingItemProductScoreService,
				Items = _products,
				ColumnCount = columnCount,
				RowCount = rowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace",
				BlockRating = Block.BlockRating
			}.Render();
		}

		public void RedirectPermanent(string newPath)
		{
			Context.Response.Clear();
			Context.Response.Status = "301 Moved Permanently";
			Context.Response.AddHeader("Location", newPath);
			Context.Response.End();
		}

		private void Initialize()
		{
			if (Block.ProductListCookie.HasValue())
			{
				_isCookieMode = true;
			}

			PopulateSelectedFilterOptions();
			PopulateAvailableFilterOptions();

			_pagingControl = CreatePagingControl();
			_products = GetProducts();

			_pagingControl.TotalCount = _products.TotalCount;
			if ((_products.TotalCount == 0) && (!string.IsNullOrEmpty(_redirectUrl)))
			{
				PageTitle = _categoryTitle;
				RedirectPermanent(_redirectUrl);
			}
			else if (!string.IsNullOrEmpty(_redirectUrl))
			{
				PageTitle = _brandTitle;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ProductCollection GetProducts()
		{
			var query = _selectedFilterOptions.BuildQuery(_availableFilterOptions);

			if (_isCookieMode)
			{
				return _filterProductService.GetAllViewByQuery(
					UserContext.Current, query, _pagingControl.SelectedPage, _pagingControl.PageSize, out _filterStatistics);
			}

			return _filterProductService.GetAllByQuery(
			 UserContext.Current, query, _pagingControl.SelectedPage, _pagingControl.PageSize, out _filterStatistics);
		}

		[SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
		private void PopulateSelectedFilterOptions()
		{
			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			_selectedFilterOptions.UseSecondaryTemplate = useSecondaryTemplate ?? false;

			if (_isCookieMode)
			{
				SetCookieProductList(_selectedFilterOptions.ProductIds);
			}

			string mode = Request.QueryString["mode"];
			if (mode != null && mode.Equals("filter"))
			{
				SetSelectedFromQuery(_selectedFilterOptions.BrandIds, "brand-id");

				SetSelectedAgeFromQuery(_selectedFilterOptions, "age_month-from", "age_month-to");
				SetSelectedPriceFromQuery(_selectedFilterOptions, "price-from", "price-to");

				SetSelectedFromQuery(_selectedFilterOptions.Level1CategoryIds, "level1category-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level2CategoryIds, "level2category-id");
				SetSelectedFromQuery(_selectedFilterOptions.Level3CategoryIds, "level3category-id");
				SetSelectedFromQuery(_selectedFilterOptions.SizeIds, "size-id");
				_selectedFilterOptions.SetSortOption(Request.QueryString["sort"]);
				SetSelectedTagIds();

				int? pageSize = Request.QueryString.GetInt32OrNull("pagesize");
				_selectedFilterOptions.PageSize = pageSize ?? GetDefaultPageSize();
			}
			else
			{
				foreach (int brandId in Block.DefaultBrandIds)
				{
					_selectedFilterOptions.BrandIds.Add(brandId);
				}

				foreach (int tagId in Block.DefaultTagIds)
				{
					_selectedFilterOptions.GroupedTagIds.Add(new[] { tagId });
				}

				if (Block.DefaultCategoryId.HasValue)
				{
					SetDefaultCategoryIds(Block.DefaultCategoryId.Value);
				}

				var context = UserContext.Current;

				if (Block.DefaultAgeIntervalId.HasValue)
				{
					IAgeIntervalBand ageIntervalBand = _ageIntervalService.GetAgeInterval(context, Block.DefaultAgeIntervalId.Value);
					if (ageIntervalBand != null)
					{
						_selectedFilterOptions.AgeInterval = _ageIntervalService.GetAgeInterval(ageIntervalBand.FromMonth, ageIntervalBand.ToMonth);
					}
				}

				if (Block.DefaultPriceIntervalId.HasValue)
				{
					IPriceIntervalBand priceIntervalBand = _priceIntervalService.GetPriceInterval(context, Block.DefaultPriceIntervalId.Value);
					if (priceIntervalBand != null)
					{
						_selectedFilterOptions.PriceInterval = _priceIntervalService.GetPriceInterval(priceIntervalBand.From, priceIntervalBand.To);
					}
				}

				if (Block.DefaultSizeId.HasValue)
				{
					_selectedFilterOptions.SizeIds.Add(Block.DefaultSizeId.Value);
				}

				// Default values.
				_selectedFilterOptions.SetSortOption(Block.DefaultSort.IsEmpty() ? SortOption.Popularity.ToString() : Block.DefaultSort);

				_selectedFilterOptions.PageSize = GetDefaultPageSize();
			}

			_selectedFilterOptions.ProductTypeIds.AddRange(Block.TargetProductTypeIds); // Defined only by block

			ManageBrandUrl();
		}

		private void ManageBrandUrl()
		{
			//check for brand title in url 
			string url = Request.Url.ToString();
			if (url.Contains('_'))
			{
				string title = url.Substring(url.IndexOf('_') + 1);
				if (title.EndsWith("/", StringComparison.Ordinal))
				{
					title = title.Remove(title.Length - 1);
				}
				var contentNodeService = IoC.Resolve<ILekmerContentNodeService>();
				var contentNodeTree = contentNodeService.GetAllAsTree(UserContext.Current);

				var brandNode = contentNodeTree.FindItemByUrl(string.Format(CultureInfo.CurrentCulture, "{0}/", title));
				if (null != brandNode)
				{
					var brandSiteStructureService = IoC.Resolve<IBrandService>();
					var brand = brandSiteStructureService.GetByNode(brandNode.ContentNode.Id);
					if (null != brand)
					{
						var brandId = brand.BrandId;
						title = brandSiteStructureService.GetById(UserContext.Current, brandId).Title;

						_selectedFilterOptions.BrandIds.Add(brandId);
						int categoryId = _selectedFilterOptions.Level3CategoryIds.Any() ? _selectedFilterOptions.Level3CategoryIds[0] : _selectedFilterOptions.Level2CategoryIds.Any() ? _selectedFilterOptions.Level2CategoryIds[0] : _selectedFilterOptions.Level1CategoryIds.Any() ? _selectedFilterOptions.Level1CategoryIds[0] : -1;
						var category = _categoryService.GetViewById(UserContext.Current, categoryId);
						if (category != null)
						{
							_redirectUrl = string.Format(CultureInfo.InvariantCulture, "{0}/", url.Substring(0, url.IndexOf('_')));
							_brandTitle = string.Format(CultureInfo.InvariantCulture, "{0} {1} - {2}", title, category.Title, UserContext.Current.Channel.Title);
							_categoryTitle = string.Format(CultureInfo.InvariantCulture, "{0} - {1}", category.Title, UserContext.Current.Channel.Title);
						}
					}
				}
			}
			else
			{
				var categoryId = _selectedFilterOptions.Level3CategoryIds.Any() ? _selectedFilterOptions.Level3CategoryIds[0] : _selectedFilterOptions.Level2CategoryIds.Any() ? _selectedFilterOptions.Level2CategoryIds[0] : _selectedFilterOptions.Level1CategoryIds.Any() ? _selectedFilterOptions.Level1CategoryIds[0] : -1;
				var category = _categoryService.GetViewById(UserContext.Current, categoryId);
				if (category != null)
				{
					PageTitle = string.Format(CultureInfo.InvariantCulture, "{0} - {1}", category.Title, UserContext.Current.Channel.Title);
				}
			}
		}

		private string PageTitle
		{
			set
			{
				ContentPageFull contentPageFull = (ContentPageFull)ContentPage;
				IContentPageSeoSetting contentPageSeoSetting = contentPageFull.ContentPageSeoSetting;
				if (contentPageSeoSetting != null)
				{
					contentPageSeoSetting.Title = value;
				}
			}
		}

		private void SetSelectedTagIds()
		{
			var parameters = Request.QueryString.AllKeys.Distinct();
			foreach (string parameter in parameters)
			{
				if (parameter.IsEmpty()) continue;

				Match match = _tagGroupParameterRegex.Match(parameter);
				if (!match.Success) continue;

				int tagGroupId;
				if (!int.TryParse(match.Groups[1].Value, out tagGroupId)) continue;

				IEnumerable<int> tagIds = GetSelectedFromQuery(parameter);
				_selectedFilterOptions.GroupedTagIds.Add(tagIds);
			}
		}

		private Dictionary<int, int> GetCampaignTagGroup()
		{
			var campaignTagGroupId = AutoCampaignPageSetting.Instance.CampaignTagGroupId;
			var parameters = Request.QueryString.AllKeys.Distinct();
			foreach (string parameter in parameters)
			{
				if (parameter.IsEmpty()) continue;

				Match match = _tagGroupParameterRegex.Match(parameter);
				if (!match.Success) continue;

				int tagGroupId;
				if (!int.TryParse(match.Groups[1].Value, out tagGroupId)) continue;

				if (tagGroupId == campaignTagGroupId)
				{
					IEnumerable<int> tagIds = GetSelectedFromQuery(parameter);
					if (tagIds.Count() == 1)
					{
						var newDic = new Dictionary<int, int> { { tagGroupId, tagIds.First() } };
						return newDic;
					}
				}
			}
			if (Block.DefaultTagIds.Count > 0)
			{
				var tags = IoC.Resolve<ITagService>().GetAllByTagGroup(UserContext.Current, campaignTagGroupId);
				if (tags != null)
				{
					foreach (var tag in Block.DefaultTagIds)
					{
						if (tags.FirstOrDefault(t => t.Id == tag) != null)
						{
							var newDic = new Dictionary<int, int> { { campaignTagGroupId, tag } };
							return newDic;
						}
					}
				}
			}
			return null;
		}

		private void SetDefaultCategoryIds(int categoryId)
		{
			var categoryTree = _categoryService.GetAllAsTree(UserContext.Current);

			var categoryTrail = new List<int>();
			var treeItem = categoryTree.FindItemById(categoryId);
			if (treeItem == null)
			{
				return;
			}

			categoryTrail.Add(treeItem.Id);
			foreach (ICategoryTreeItem item in treeItem.GetAncestors())
			{
				if (item.Parent == null) break;

				categoryTrail.Add(item.Id);
			}

			categoryTrail.Reverse();

			if (categoryTrail.Count >= 1)
			{
				_selectedFilterOptions.Level1CategoryIds.Add(categoryTrail[0]);
			}
			if (categoryTrail.Count >= 2)
			{
				_selectedFilterOptions.Level2CategoryIds.Add(categoryTrail[1]);
			}
			if (categoryTrail.Count >= 3)
			{
				_selectedFilterOptions.Level3CategoryIds.Add(categoryTrail[2]);
			}
		}

		private void PopulateAvailableFilterOptions()
		{
			var context = UserContext.Current;

			var categoryTree = _categoryService.GetAllAsTree(context);

			_availableFilterOptions.Brands = _brandService.GetAll(context);

			Collection<IAgeInterval> ageIntervals = _ageIntervalService.GetAll(context);
			Collection<IPriceInterval> priceIntervals = _priceIntervalService.GetAll(context);

			_availableFilterOptions.AgeIntervalOuter = _ageIntervalService.GetAgeIntervalOuter(ageIntervals);
			_availableFilterOptions.PriceIntervalOuter = _priceIntervalService.GetPriceIntervalOuter(priceIntervals);

			_availableFilterOptions.TagGroups = _tagGroupService.GetAll(context);
			_availableFilterOptions.Sizes = _sizeService.GetAll(context);

			// Level 1 categories.
			_availableFilterOptions.Level1Categories = categoryTree.Root.Children
				.Select(item => (ICategory)item.Category);

			// Level 2 categories.
			if (_selectedFilterOptions.Level1CategoryIds.Count == 1)
			{
				ICategoryTreeItem level1TreeItem = categoryTree.FindItemById(_selectedFilterOptions.Level1CategoryIds.First());

				if (level1TreeItem != null)
				{
					_availableFilterOptions.Level2Categories = level1TreeItem.Children
						.Select(category => (ICategory)category.Category);
				}
			}

			// Level 3 categories.
			if (_selectedFilterOptions.Level2CategoryIds.Count == 1)
			{
				ICategoryTreeItem level2TreeItem = categoryTree.FindItemById(_selectedFilterOptions.Level2CategoryIds.First());

				if (level2TreeItem != null)
				{
					_availableFilterOptions.Level3Categories = level2TreeItem.Children
						.Select(category => (ICategory)category.Category);
				}
			}

			// Sort options.
			_availableFilterOptions.SortOptions =
				new Dictionary<SortOption, string>
					{
						{SortOption.Popularity, AliasHelper.GetAliasValue("ProductFilter.SortOption.Popularity")},
						{SortOption.IsNewFrom, AliasHelper.GetAliasValue("ProductFilter.SortOption.IsNewFrom")},
			
						{SortOption.TitleAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleAsc")},
						{SortOption.TitleDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleDesc")},

						{SortOption.PriceAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceAsc")},
						{SortOption.PriceDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceDesc")},

						{SortOption.DiscountPercentDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.DiscountPercentDesc")}
					};

			int? secondaryTemplateId = Block.SecondaryTemplateId;
			_availableFilterOptions.HasSecondaryTemplate = secondaryTemplateId.HasValue;

			// Page size options.
			_availableFilterOptions.PageSizeOptions = GetPageSizeOptions();

			// Product types
			_availableFilterOptions.ProductTypes = _productTypeService.GetAll(context);
		}

		private IEnumerable<int> GetPageSizeOptions()
		{
			string settingName = CommonSession.IsViewportMobile ? "PageSizeOptionsMobile" : "PageSizeOptions";
			string settingPageSizeOptions = Template.GetSettingOrNull(settingName) ?? string.Empty;

			var pageSizeOptions = settingPageSizeOptions
				.Split(new[] { ',' })
				.Select(pageSize =>
				{
					int intPageSize;
					return int.TryParse(pageSize, out intPageSize) ? intPageSize : 0;
				})
				.Where(pageSize => pageSize > 0)
				.ToList();

			if (pageSizeOptions.Count < 1)
			{
				return new List<int> { GetDefaultPageSize() };
			}

			return pageSizeOptions;
		}

		private int GetDefaultPageSize()
		{
			string settingName = CommonSession.IsViewportMobile ? "DefaultPageSizeMobile" : "DefaultPageSize";
			string settingDefaultPageSize = Template.GetSettingOrNull(settingName);

			int defaultPageSize;
			if (!int.TryParse(settingDefaultPageSize, out defaultPageSize))
			{
				defaultPageSize = 20;
			}

			return defaultPageSize;
		}

		private int GetColumnCount()
		{
			string settingName = CommonSession.IsViewportMobile ? "ColumnCountMobile" : "ColumnCount";
			int columnCount;
			if (!int.TryParse(Template.GetSettingOrNull(settingName), out columnCount))
			{
				columnCount = 1;
			}
			return columnCount;
		}

		private void SetSelectedFromQuery(ICollection<int> selectedItems, string parameter)
		{
			IEnumerable<int> items = GetSelectedFromQuery(parameter);
			foreach (int item in items)
			{
				selectedItems.Add(item);
			}
		}

		private void SetSelectedAgeFromQuery(SelectedFilterOptions selectedFilterOptions, string parameterFrom, string parameterTo)
		{
			int? fromMonth = Request.QueryString.GetInt32OrNull(parameterFrom);
			int? toMonth = Request.QueryString.GetInt32OrNull(parameterTo);

			if (fromMonth.HasValue || toMonth.HasValue)
			{
				selectedFilterOptions.AgeInterval = _ageIntervalService.GetAgeInterval(fromMonth, toMonth);
			}
		}

		private void SetSelectedPriceFromQuery(SelectedFilterOptions selectedFilterOptions, string parameterFrom, string parameterTo)
		{
			decimal? from = null;
			decimal? to = null;
			decimal parsedValue;

			if (!Request.QueryString[parameterFrom].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterFrom], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					from = parsedValue;
				}
			}

			if (!Request.QueryString[parameterTo].IsNullOrTrimmedEmpty())
			{
				if (decimal.TryParse(Request.QueryString[parameterTo], NumberStyles.Number, CultureInfo.InvariantCulture, out parsedValue))
				{
					to = parsedValue;
				}
			}

			if (from.HasValue || to.HasValue)
			{
				selectedFilterOptions.PriceInterval = _priceIntervalService.GetPriceInterval(from, to);
			}
		}

		private void SetCookieProductList(ICollection<int> selectedItems)
		{
			if (Block.ProductListCookie.IsEmpty())
			{
				return;
			}

			IEnumerable<int> items = GetCookieProductList();

			foreach (int item in items)
			{
				selectedItems.Add(item);
			}
		}

		private IEnumerable<int> GetCookieProductList()
		{
			//test
			HttpCookie cookie = HttpContext.Current.Request.Cookies[Block.ProductListCookie];
			if (cookie == null || cookie.Value.IsEmpty())
			{
				return new Collection<int> { 0 };
			}

			string productTempIds = HttpUtility.UrlDecode(cookie.Value, Encoding.Default);
			IEnumerable<int> productIds = productTempIds
				.Split(new[] { ',' })
				.Select(value =>
				{
					int intValue;
					return int.TryParse(value, out intValue) ? intValue : 0;
				})
				.Where(value => value != 0);
			productIds = productIds.Distinct();
			if (productIds.Count() < 1)
			{
				return new Collection<int> { 0 };
			}
			return productIds;
		}

		private IEnumerable<int> GetSelectedFromQuery(string parameter)
		{
			if (Request.QueryString[parameter].IsNullOrTrimmedEmpty())
			{
				return new int[0];
			}

			return Request.QueryString[parameter]
				.Split(new[] { ',' })
				.Select(value =>
				{
					int intValue;
					return int.TryParse(value, out intValue) ? intValue : 0;
				})
				.Where(value => value != 0).Distinct();
		}

		private ILekmerPagingControl CreatePagingControl()
		{
			var pagingControl = (ILekmerPagingControl)IoC.Resolve<IPagingControl>();

			pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = _selectedFilterOptions.PageSize;
			pagingControl.ViewAllModeAllowed = true;

			pagingControl.Initialize();

			return pagingControl;
		}

		private FilterFormControl CreateFilterFormControl()
		{
			return new FilterFormControl
			{
				Template = Template,
				AvailableFilterOptions = _availableFilterOptions,
				FilterStatistics = _filterStatistics,
				SelectedFilterOptions = _selectedFilterOptions
			};
		}

		protected override Template CreateTemplate()
		{
			if (_selectedFilterOptions.UseSecondaryTemplate && Block.SecondaryTemplateId.HasValue)
			{
				return _templateFactory.Create(Block.SecondaryTemplateId.Value);
			}

			return base.CreateTemplate();
		}
	}
}