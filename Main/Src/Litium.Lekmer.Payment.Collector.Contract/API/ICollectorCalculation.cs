﻿namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorCalculation
	{
		decimal CalculateCheapestMonthlyInvoiceFee();
		decimal CalculateMonthlyCost(decimal sum, ICollectorPaymentType paymentType);
		decimal CalculateLowestAmountToPay(decimal sum);

		decimal CalculateMonthlyCostForInterestFree(decimal sum, ICollectorPaymentType paymentType);
		decimal CalculateMonthlyCostForAnnuity(decimal sum, ICollectorPaymentType paymentType);
		decimal CalculateMonthlyCostForAccount(decimal sum, ICollectorPaymentType paymentType);
	}
}