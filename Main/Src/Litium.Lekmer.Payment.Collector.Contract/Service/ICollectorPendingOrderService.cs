﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorPendingOrderService
	{
		ICollectorPendingOrder Create();
		int Insert(ICollectorPendingOrder collectorPendingOrder);
		Collection<ICollectorPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkFromDate);
		void Update(ICollectorPendingOrder collectorPendingOrder);
		void Delete(int collectorPendingOrderId);
	}
}
