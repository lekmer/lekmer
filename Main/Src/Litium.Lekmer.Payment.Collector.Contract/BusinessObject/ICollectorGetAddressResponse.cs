﻿using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorGetAddressResponse : ICollectorResult
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		string CivicNumber { get; set; }

		BaseAddress[] Addresses { get; set; }
	}
}
