﻿using System;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorPendingOrder
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		int OrderId { get; set; }
		DateTime FirstAttempt { get; set; }
		DateTime LastAttempt { get; set; }
	}
}
