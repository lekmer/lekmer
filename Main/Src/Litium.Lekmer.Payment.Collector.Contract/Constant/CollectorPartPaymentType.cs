﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorPartPaymentType
	{
		Unknown = 0,
		InterestFree = 1,
		Annuity = 2,
		Account = 3,
		SpecialCampaign = 4
	}
}
