﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorInvoiceStatus
	{
		/// <summary>
		/// 0 | The invoice is temporarily blocked, and Collector needs to look into it. The invoice can not be activated when it have this status.
		/// </summary>
		OnHold = 0,
		/// <summary>
		/// 1 | The invoice is pending and can be activated by Merchant (usually within 14 days).
		/// </summary>
		Pending = 1,
		/// <summary>
		/// 2 | The invoice is activated by Merchant. Collector has purchased the invoice.
		/// </summary>
		Activated = 2,
		/// <summary>
		/// 5 | The invoice was rejected by Collector, which means that the customer and invoice was not approved, you need to offer another payment option to customer.
		/// </summary>
		Rejected = 5
	}
}
