﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Pacsoft.Contract;

namespace Litium.Lekmer.Pacsoft
{
	public class PacsoftExportSetting : SettingBase, IPacsoftExportSetting
	{
		protected override string StorageName
		{
			get { return "PacsoftExport"; }
		}
		protected virtual string GroupName
		{
			get { return "PacsoftExportSettings"; }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}
		public string FileName
		{
			get { return GetString(GroupName, "FileName"); }
		}
		public string Encoding
		{
			get { return GetString(GroupName, "Encoding"); }
		}

		public string ProductErpId
		{
			get { return GetString(GroupName, "ProductErpId"); }
		}
		public string SupplierId
		{
			get { return GetString(GroupName, "SupplierId"); }
		}
		public int DaysAfterPurchase
		{
			get { return GetInt32(GroupName, "DaysAfterPurchase"); }
		}
		public string QuickId
		{
			get { return GetString(GroupName, "QuickId"); }
		}
		public string RcvId
		{
			get { return GetString(GroupName, "RcvId"); }
		}
		public string Srvid
		{
			get { return GetString(GroupName, "Srvid"); }
		}
		public string SmsFrom
		{
			get { return GetString(GroupName, "SmsFrom"); }
		}
		public string Contents
		{
			get { return GetString(GroupName, "Contents"); }
		}

		public string FtpUrl
		{
			get { return GetString(GroupName, "FtpUrl"); }
		}
		public string Username
		{
			get { return GetString(GroupName, "Username"); }
		}
		public string Password
		{
			get { return GetString(GroupName, "Password"); }
		}

		public string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}

		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames, ',');
			return dictionary.ContainsKey(channelName);
		}
		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames, ',');
			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}
		public string GetChannelUserNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(Username, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}
		public string GetChannelPasswordReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(Password, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue, seperator);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
		// value1,value2, ... , valueN => List<value>
		public ICollection<string> ConvertToList(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { seperator }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
	}
}