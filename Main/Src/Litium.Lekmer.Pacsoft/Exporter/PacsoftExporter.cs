﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using Litium.Lekmer.Pacsoft.Contract;
using Litium.Scensum.Core;
using log4net;

namespace Litium.Lekmer.Pacsoft
{
	public class PacsoftExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IPacsoftExportSetting Setting { get; private set; }
		protected IPacsoftExportService PacsoftExportService { get; private set; }

		public PacsoftExporter(IPacsoftExportSetting pacsoftExportSetting, IPacsoftExportService pacsoftExportService)
		{
			Setting = pacsoftExportSetting;
			PacsoftExportService = pacsoftExportService;
		}

		public void Execute()
		{
			_log.Info("Execution started.");

			ExecutePacsoftExport();

			_log.Info("Execution complited.");
		}

		protected virtual void ExecutePacsoftExport()
		{
			var userContexts = PacsoftExportService.GetUserContextForAllChannels();

			foreach (IUserContext context in userContexts)
			{
				Stopwatch stopwatch = Stopwatch.StartNew();

				_log.Info("Get orders...");
				var pacsoftInfoCollection = PacsoftExportService.GetPacsoftInfoCollectionToExport(context);
				if (pacsoftInfoCollection.Count <= 0)
				{
					stopwatch.Stop();
					_log.InfoFormat("There are no orders with product {1} for channel {0}", context.Channel.CommonName, Setting.ProductErpId);
					continue;
				}

				foreach (var pacsoftInfo in pacsoftInfoCollection)
				{
					string filePath = PacsoftExportService.GetFilePath(context.Channel.CommonName, pacsoftInfo.OrderId);
					if (File.Exists(filePath))
					{
						File.Delete(filePath);
					}

					_log.InfoFormat("File preparing... //{0}", filePath);

					using (Stream stream = File.OpenWrite(filePath))
					{
						var pacsoftInfoFile = new PacsoftInfoFile();
						pacsoftInfoFile.Initialize(Setting, pacsoftInfo);
						pacsoftInfoFile.Save(stream);
					}

					_log.InfoFormat("File saved... //{0}", filePath);

					_log.Info("File start uploaded to FTP...");
					UploadToFtp(context.Channel.CommonName, filePath, pacsoftInfo.OrderId);
					PacsoftExportService.UpdatePacsoftOrder(pacsoftInfo.OrderId);
				}

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}
		}

		protected virtual void UploadToFtp(string channelCommonName, string filePath, int orderid)
		{
			try
			{
				string channelName = Setting.GetChannelNameReplacement(channelCommonName);

				string fileName = string.Format(Setting.FileName, orderid, DateTime.Now.Date.ToString("yyyyMMdd"));
				fileName = fileName.Replace("[ChannelName]", channelName);

				var ftpFileUrl = string.Format(Setting.FtpUrl, fileName);
				var username = Setting.Username;
				var password = Setting.Password;

				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpFileUrl));
				ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
				ftpRequest.Proxy = null;
				ftpRequest.UseBinary = true;
				ftpRequest.Credentials = new NetworkCredential(username, password);

				var fileInfo = new FileInfo(filePath);
				var fileContents = new byte[fileInfo.Length];

				using (FileStream stream = fileInfo.OpenRead())
				{
					stream.Read(fileContents, 0, Convert.ToInt32(fileInfo.Length));
				}

				using (Stream writer = ftpRequest.GetRequestStream())
				{
					writer.Write(fileContents, 0, fileContents.Length);
				}
			}
			catch (WebException webex)
			{
				_log.Error(webex);
			}
		}
	}
}