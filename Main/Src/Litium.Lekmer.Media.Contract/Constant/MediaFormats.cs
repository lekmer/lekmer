﻿namespace Litium.Lekmer.Media
{
	public enum MediaFormats
	{
		Gif = 2,
		Jpeg = 3,
		Archive = 4,
		Png = 5,
		Flash = 20,
		Docx = 21,
		Doc = 22,
		Xlsx = 23,
		Xls = 24,
		Txt = 25,
		Html = 26,
		Pdf = 27
	}
}