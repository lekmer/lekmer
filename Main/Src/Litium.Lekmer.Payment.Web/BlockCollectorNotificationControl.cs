﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Collector;
using Litium.Lekmer.Payment.Collector.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using log4net;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	/// <summary>
	/// InvoiceChangeNotification
	/// </summary>
	public class BlockCollectorNotificationControl : BlockControlBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Collection<IChannel> _channels;
		private ICollectorSetting _collectorSetting;

		protected ILekmerOrderService OrderService { get; private set; }
		protected IOrderStatusService OrderStatusService { get; private set; }
		protected IOrderItemStatusService OrderItemStatusService { get; private set; }
		protected IGiftCardViaMailInfoService GiftCardViaMailInfoService { get; private set; }
		protected ICollectorNotificationService CollectorNotificationService;
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ICollectorTransactionService CollectorTransactionService { get; private set; }
		protected ICollectorSetting CollectorSetting
		{
			get { return _collectorSetting ?? (_collectorSetting = CollectorFactory.CreateCollectorSetting(UserContext.Current.Channel.CommonName)); }
		}

		protected ICollectorNotificationInfo CollectorNotificationInfo { get; private set; }
		protected bool CollectorNotificationInfoValid { get; private set; }

		public BlockCollectorNotificationControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IOrderService orderService,
			IOrderStatusService orderStatusService,
			IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			ICollectorNotificationService collectorNotificationService,
			IChannelService channelService,
			ICollectorTransactionService collectorTransactionService
		)
			: base(templateFactory, blockService)
		{
			OrderService = (ILekmerOrderService)orderService;
			OrderStatusService = orderStatusService;
			OrderItemStatusService = orderItemStatusService;
			GiftCardViaMailInfoService = giftCardViaMailInfoService;
			CollectorNotificationService = collectorNotificationService;
			ChannelService = (ILekmerChannelService)channelService;
			CollectorTransactionService = collectorTransactionService;
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "CollectorNotification"; }
		}

		protected override BlockContent RenderCore()
		{
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, RenderBody(), footer);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}


		protected virtual string RenderBody()
		{
			int transactionId = CollectorTransactionService.CreateInvoiceNotification(CollectorSetting.StoreId, Request.Url.Query);

			Initialize();

			if (CollectorNotificationInfoValid == false)
			{
				LogError("Collector notification is not valid.");
				return RenderSuccessBody();
			}

			// CollectorNotificationInfoValid == true

			ICollectorNotificationResult collectorNotificationResult = new CollectorNotificationResult
			{
				TransactionId = transactionId,
				InvoiceNo = CollectorNotificationInfo.InvoiceNo,
				OrderNo = CollectorNotificationInfo.OrderNo,
				InvoiceStatus = CollectorNotificationInfo.InvoiceStatus
			};
			CollectorTransactionService.SaveInvoiceNotificationResult(collectorNotificationResult);

			var order = (ILekmerOrderFull)OrderService.GetFullById(UserContext.Current, CollectorNotificationInfo.OrderNo);
			if (order == null)
			{
				LogError("Can't find order.");
				return RenderSuccessBody();
			}

			if (order.Payments.Count > 0)
			{
				if (order.Payments.First().ReferenceId != CollectorNotificationInfo.InvoiceNo)
				{
					LogError("Order ReferenceId doesn't match with incoming InvoiceNo.", order);
					return RenderSuccessBody();
				}
			}

			IUserContext orderUserContext = GetUserContext(order.ChannelId);

			if (CollectorNotificationInfo.InvoiceStatus == (int)CollectorInvoiceStatus.Pending) // 1
			{
				ConfirmOrder(orderUserContext, order);
			}
			else if (CollectorNotificationInfo.InvoiceStatus == (int)CollectorInvoiceStatus.Rejected) // 5
			{
				RejectOrder(orderUserContext, order);
			}
			else
			{
				LogError("Unknown incoming InvoiceStatus.", order);
			}

			return RenderSuccessBody();
		}

		private string RenderSuccessBody()
		{
			return "<!-- Collector_notification:_OK -->";
		}


		private void Initialize()
		{
			try
			{
				CollectorNotificationInfo = CollectorNotificationService.ParseFromQueryString(Request.QueryString);
				CollectorNotificationInfoValid = CollectorNotificationService.Validate(CollectorNotificationInfo);
			}
			catch (Exception ex)
			{
				LogError("Can't initialize BlockCollectorNotificationControl. ", ex);
			}
		}

		protected virtual void ConfirmOrder(IUserContext orderUserContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentOnHold)
			{
				OrderService.OrderVoucherConsume(orderUserContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(orderUserContext, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(orderUserContext, OrderStatusName.PaymentConfirmed));

				OrderService.SaveFull(orderUserContext, order);

				OrderService.UpdateNumberInStock(orderUserContext, order);
				GiftCardViaMailInfoService.Save(orderUserContext, order);

				// Order confirmation has been sent already !!!
			}
			else
			{
				LogError("Unable confirm order - status differs from PaymentOnHold.", order);
			}
		}

		private void RejectOrder(IUserContext orderUserContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentOnHold)
			{
				OrderService.OrderVoucherRelease(orderUserContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(orderUserContext, OrderStatusName.RejectedByPaymentProvider);
				OrderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);

				OrderService.SendRejectMail(orderUserContext, order);
			}
			else
			{
				LogError("Unable reject order - status differs from PaymentOnHold.", order);
			}
		}


		protected virtual IUserContext GetUserContext(int channelId)
		{
			var userContext = IoC.Resolve<IUserContext>();

			userContext.Channel = GetChannel(channelId);

			return userContext;
		}

		protected virtual IChannel GetChannel(int channelId)
		{
			if (_channels == null)
			{
				_channels = ChannelService.GetAll();
			}

			var channel = _channels.FirstOrDefault(c => c.Id == channelId);

			if (channel == null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not find channel. Id = {0}", channelId));
			}

			return channel;
		}


		private void LogError(string message)
		{
			_log.ErrorFormat("{0}\r\nUrl: {1}\r\nQueryString: \r\n{2}", message, Request.Url.AbsoluteUri, LogRequestQueryString());
		}

		private void LogError(string message, Exception exception)
		{
			string errorMessage = string.Format("{0}\r\nUrl: {1}\r\nQueryString: \r\n{2}", message, Request.Url.AbsoluteUri, LogRequestQueryString());
			_log.Error(errorMessage, exception);
		}

		private void LogError(string message, ILekmerOrderFull order)
		{
			_log.ErrorFormat("{0}\r\nOrder id: {1}\r\nOrder status: {2}\r\nUrl: {3}\r\nQueryString: \r\n{4}", message, order.Id, order.OrderStatus.CommonName, Request.Url.AbsoluteUri, LogRequestQueryString());
		}

		private void LogWarning(string message, ILekmerOrderFull order)
		{
			_log.WarnFormat("{0}\\r\nOrder id: {1}r\nOrder status: {2}\r\nUrl: {3}\r\nQueryString: \r\n{4}", message, order.Id, order.OrderStatus.CommonName, Request.Url.AbsoluteUri, LogRequestQueryString());
		}

		private string LogRequestQueryString()
		{
			return Request.Url.Query;
		}
	}
}