﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Klarna;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Newtonsoft.Json.Linq;
using log4net;
using KlarnaCheckout = Klarna.Checkout;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockKlarnaOrderAcknowledgementControl : BlockControlBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly ILekmerOrderService _orderService;
		private readonly IOrderStatusService _orderStatusService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;
		private readonly IAddressTypeService _addressTypeService;

		public BlockKlarnaOrderAcknowledgementControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IOrderService orderService,
			IOrderStatusService orderStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			IAddressTypeService addressTypeService)
			: base(templateFactory, blockService)
		{
			_orderService = (ILekmerOrderService)orderService;
			_orderStatusService = orderStatusService;
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
			_addressTypeService = addressTypeService;
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "KlarnaOrderAcknowledgement"; }
		}

		protected override BlockContent RenderCore()
		{
			var blockContent = new BlockContent(string.Empty, string.Empty, string.Empty);

			var checkoutId = new Uri(GetValueFromCollection("klarna_order", Request.QueryString));
			try
			{
				var uri = checkoutId.ToString();
				var splittedUri = uri.Split('/').ToList();
				var orderKlarnaId = splittedUri.Last();

				var order = (ILekmerOrderFull)_orderService.GetFullByKcoId(UserContext.Current, orderKlarnaId);
				if (order == null)
				{
					_log.ErrorFormat("There are no order in DB with KcoId = {0}", orderKlarnaId);
					return blockContent;
				}

				// Fetch order from Klarna
				IKlarnaCheckoutClient klarnaCheckoutClient = KlarnaFactory.CreateKlarnaCheckoutClient(UserContext.Current.Channel.CommonName);
				KlarnaCheckout.Order orderKlarna = klarnaCheckoutClient.MakeOrder(checkoutId);
				orderKlarna.Fetch();

				string orderStatus = (string)orderKlarna.GetValue("status");
				if (orderStatus == "checkout_complete")
				{
					string klarnaReferenceId = (string)orderKlarna.GetValue("reservation");

					if (order.OrderStatus.CommonName == OrderStatusName.KcoPaymentPending)
					{
						ConfirmOrder(order, klarnaReferenceId, orderKlarna);
					}
					else
					{
						UpdateOrderReference(order, klarnaReferenceId);
					}

					var reference = new Dictionary<string, object>
					{
						{"orderid1", order.Id.ToString(CultureInfo.InvariantCulture)}
					};
					var data = new Dictionary<string, object>
					{
						{"status", "created"},
						{"merchant_reference", reference}
					};

					orderKlarna.Update(data);
				}
				else if (orderStatus == "created")
				{
					var klarnaReferenceId = (string)orderKlarna.GetValue("reservation");

					if (order.OrderStatus.CommonName == OrderStatusName.KcoPaymentPending)
					{
						ConfirmOrder(order, klarnaReferenceId, orderKlarna);

						_log.ErrorFormat("Order = {0} has status {1} while in klarna this order has status 'created'. (Klarna Order Id = {2}).", order.Id.ToString(CultureInfo.InvariantCulture), order.OrderStatus.CommonName, orderKlarnaId);
					}
					else
					{
						if (UpdateOrderReference(order, klarnaReferenceId))
						{
							_log.ErrorFormat("Order = {0} has empty reference id in status {1}.", order.Id.ToString(CultureInfo.InvariantCulture), order.OrderStatus.CommonName);
						}
					}
				}
				else if (orderStatus == "checkout_incomplete ")
				{
					RejectOrder(order, orderKlarna);
				}
			}
			catch (Exception ex)
			{
				_log.Error("Error occurred at KlarnaOrderAcknowledgementControl for 'klarna_order' url = " + checkoutId, ex);
			}

			return blockContent;
		}


		private void RejectOrder(ILekmerOrderFull order, KlarnaCheckout.Order orderKlarna)
		{
			_orderService.OrderVoucherRelease(UserContext.Current, order);

			order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.RejectedByPaymentProvider);
			order.Email = GetOrderEmail(orderKlarna);
			UpdateCustomer(ref order, orderKlarna);
			UpdateAddresses(ref order, orderKlarna);
			_orderService.SaveFull(UserContext.Current, order);
		}

		private void ConfirmOrder(ILekmerOrderFull order, string reference, KlarnaCheckout.Order orderKlarna)
		{
			_orderService.OrderVoucherConsume(UserContext.Current, order);

			order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);
			order.Email = GetOrderEmail(orderKlarna);

			if (order.Payments.Count > 0)
			{
				order.Payments.First().ReferenceId = reference.IsEmpty() ? string.Empty : reference;
			}

			UpdateCustomer(ref order, orderKlarna);
			UpdateAddresses(ref order, orderKlarna);
			_orderService.SaveFull(UserContext.Current, order);
			_orderService.UpdateNumberInStock(UserContext.Current, order);
			_giftCardViaMailInfoService.Save(UserContext.Current, order);
			_orderService.SendConfirm(UserContext.Current, order);
		}

		private bool UpdateOrderReference(ILekmerOrderFull order, string reference)
		{
			string referenceId = string.Empty;
			if (order.Payments.Count > 0)
			{
				referenceId = order.Payments.First().ReferenceId;
			}

			if (string.IsNullOrEmpty(referenceId))
			{
				if (order.Payments.Count > 0)
				{
					order.Payments.First().ReferenceId = reference.IsEmpty() ? string.Empty : reference;
					_orderService.SaveFull(UserContext.Current, order);
				}

				return true;
			}

			return false;
		}

		private void UpdateCustomer(ref ILekmerOrderFull order, KlarnaCheckout.Order orderKlarna)
		{
			var billingAddress = orderKlarna.GetValue("billing_address") as JObject;
			if (billingAddress != null)
			{
				order.Customer.CustomerInformation.FirstName = billingAddress["given_name"].ToString();
				order.Customer.CustomerInformation.LastName = billingAddress["family_name"].ToString();
				order.Customer.CustomerInformation.CellPhoneNumber = billingAddress["phone"].ToString();
				order.Customer.CustomerInformation.Email = billingAddress["email"].ToString();
			}
		}

		private void UpdateAddresses(ref ILekmerOrderFull order, KlarnaCheckout.Order orderKlarna)
		{
			var billingAddress = orderKlarna.GetValue("billing_address") as JObject;
			if (billingAddress != null)
			{
				var customerBilling = order.Customer.CustomerInformation.DefaultBillingAddress;
				customerBilling.Addressee = "See customer-firstname and customer-lastname";
				customerBilling.StreetAddress = billingAddress["street_address"].ToString();
				customerBilling.PostalCode = billingAddress["postal_code"].ToString();
				customerBilling.City = billingAddress["city"].ToString();
				customerBilling.PhoneNumber = billingAddress["phone"].ToString();

				order.BillingAddress.Addressee = customerBilling.Addressee;
				order.BillingAddress.StreetAddress = customerBilling.StreetAddress;
				order.BillingAddress.PostalCode = customerBilling.PostalCode;
				order.BillingAddress.City = customerBilling.City;
				order.BillingAddress.PhoneNumber = customerBilling.PhoneNumber;
			}

			var deliveryAddress = orderKlarna.GetValue("shipping_address") as JObject;
			if (deliveryAddress != null)
			{
				bool needUpdate = false;

				var klarnaDeliveryAddress = order.Customer.CustomerInformation.DefaultDeliveryAddress;
				klarnaDeliveryAddress.StreetAddress = deliveryAddress["street_address"].ToString();
				klarnaDeliveryAddress.PostalCode = deliveryAddress["postal_code"].ToString();
				klarnaDeliveryAddress.City = deliveryAddress["city"].ToString();
				klarnaDeliveryAddress.PhoneNumber = deliveryAddress["phone"].ToString();

				var customerDelivery = order.Customer.CustomerInformation.DefaultDeliveryAddress;
				var orderDelivery = order.DeliveryAddress;

				if (order.BillingAddress.City != klarnaDeliveryAddress.City
					|| order.BillingAddress.StreetAddress != klarnaDeliveryAddress.StreetAddress
					|| order.BillingAddress.PostalCode != klarnaDeliveryAddress.PostalCode
					|| order.BillingAddress.PhoneNumber != klarnaDeliveryAddress.PhoneNumber)
				{
					needUpdate = true;
				}

				if (needUpdate)
				{
					IAddressType addressType = _addressTypeService.GetByCommonName(UserContext.Current, "DeliveryAddress");
					customerDelivery.AddressTypeId = addressType != null ? addressType.Id : -1;
					customerDelivery.Id = -1;
					customerDelivery.Addressee = "See customer-firstname and customer-lastname";
					customerDelivery.StreetAddress = klarnaDeliveryAddress.StreetAddress;
					customerDelivery.PostalCode = klarnaDeliveryAddress.PostalCode;
					customerDelivery.City = klarnaDeliveryAddress.City;
					customerDelivery.PhoneNumber = klarnaDeliveryAddress.PhoneNumber;

					orderDelivery.Id = -1;
					orderDelivery.Addressee = customerDelivery.Addressee;
					orderDelivery.StreetAddress = klarnaDeliveryAddress.StreetAddress;
					orderDelivery.PostalCode = klarnaDeliveryAddress.PostalCode;
					orderDelivery.City = klarnaDeliveryAddress.City;
					orderDelivery.PhoneNumber = klarnaDeliveryAddress.PhoneNumber;
				}
			}
		}


		private string GetOrderEmail(KlarnaCheckout.Order orderKlarna)
		{
			var orderEmail = string.Empty;

			var billingAddress = orderKlarna.GetValue("billing_address") as JObject;
			if (billingAddress != null)
			{
				orderEmail = billingAddress["email"].ToString();
			}

			return orderEmail;
		}

		private static string GetValueFromCollection(string key, NameValueCollection collection)
		{
			string value = collection[key];
			if (string.IsNullOrEmpty(value))
			{
				throw new ArgumentException(string.Format("Parameter '{0}' is missing or invalid.", key), "collection");
			}
			return value;
		}
	}
}