﻿using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.TwoSell
{
	public interface IRecommenderProductService
	{
		ProductCollection GetProductsBySessionAndProduct(IUserContext current, string sessionId, int productId, int itemsToReturn);
	}
}