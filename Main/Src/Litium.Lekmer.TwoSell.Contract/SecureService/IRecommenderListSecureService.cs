﻿namespace Litium.Lekmer.TwoSell
{
	public interface IRecommenderListSecureService
	{
		IRecommenderList Create();

		IRecommenderList Save(IRecommenderList recommenderList);

		IRecommenderList GetBySessionAndProduct(string sessionId, int? productId);
	}
}