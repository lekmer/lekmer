﻿namespace Litium.Lekmer.TwoSell
{
	public interface IRecommendationProduct
	{
		int Id { get; set; }
		int RecommenderItemId { get; set; }
		int ProductId { get; set; }
		int Ordinal { get; set; }
	}
}