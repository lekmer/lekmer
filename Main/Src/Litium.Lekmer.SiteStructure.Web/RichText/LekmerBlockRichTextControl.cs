﻿using System;
using System.Linq;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.SiteStructure.Web
{
	public class LekmerBlockRichTextControl : BlockRichTextControl
	{
		public LekmerBlockRichTextControl(IBlockRichTextService blockRichTextService, ITemplateFactory templateFactory)
			: base(blockRichTextService, templateFactory)
		{
		}

		protected override BlockContent RenderCore()
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);
			RenderContent(fragmentContent);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private void RenderContent(Fragment fragmentContent)
		{
			string pageTitle = PageTitle();
			string blockTitle = Block.Title;

			pageTitle = pageTitle.Trim();
			blockTitle = blockTitle.Trim();

			bool blockTitleSameAsPageTitle = blockTitle.Equals(pageTitle, StringComparison.CurrentCultureIgnoreCase);

			fragmentContent.AddCondition("Block.Title.SameAsPageTitle", blockTitleSameAsPageTitle);
		}

		/// <summary>
		/// Gets the title of the page. (Copied from ContentPageHandler)
		/// </summary>
		/// <returns>The title of the page.</returns>
		protected virtual string PageTitle()
		{
			var contentPageFull = ContentPage as IContentPageFull;
			string seoTitle = string.Empty;
			if (contentPageFull != null)
			{
				seoTitle = contentPageFull.ContentPageSeoSetting != null ? contentPageFull.ContentPageSeoSetting.Title : null;
			}

			return Coalesce(seoTitle, ContentPage.PageTitle, ContentPage.Title);
		}

		/// <summary>
		/// Returns the first value that is not null or empty.
		/// </summary>
		/// <param name="values">String values.</param>
		/// <returns>The first item in values that is not null or empty. Returns null if none found.</returns>
		protected static string Coalesce(params string[] values)
		{
			return values.FirstOrDefault(value => !string.IsNullOrEmpty(value));
		}
	}
}