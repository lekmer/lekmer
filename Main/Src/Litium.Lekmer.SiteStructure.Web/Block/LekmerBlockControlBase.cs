﻿using System;
using Litium.Lekmer.Esales;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.SiteStructure.Web
{
	public abstract class LekmerBlockControlBase<TBlock> : BlockControlBase<TBlock> where TBlock : class, IBlock
	{
		protected LekmerBlockControlBase(ITemplateFactory templateFactory)
			: base(templateFactory)
		{
		}

		[Obsolete("Obsolete since 2.1. Use constructor with templateFactory parameter.")]
		protected LekmerBlockControlBase()
		{
		}

		private IProductView _product;
		/// <summary>
		/// The product that should be displayed on the page.
		/// </summary>
		public IProductView Product
		{
			get
			{
				if (_product == null)
				{
					var productPageHandler = ContentPageHandler as ProductPageHandler;
					if (productPageHandler == null) return null;
					_product = productPageHandler.Product;
				}
				return _product;
			}
		}

		private IEsalesResponse _esalesGeneralResponse;
		/// <summary>
		/// Esales data that could be displayed on page.
		/// </summary>
		public IEsalesResponse EsalesGeneralResponse
		{
			get
			{
				if (_esalesGeneralResponse == null)
				{
					var contentPageHandler = ContentPageHandler as IEsalesGeneralControlResponse;
					if (contentPageHandler == null) return null;
					_esalesGeneralResponse = contentPageHandler.EsalesGeneralResponse;
				}
				return _esalesGeneralResponse;
			}
			set
			{
				var contentPageHandler = ContentPageHandler as IEsalesGeneralControlResponse;
				if (contentPageHandler != null)
				{
					contentPageHandler.EsalesGeneralResponse = value;
				}
			}
		}
	}
}
