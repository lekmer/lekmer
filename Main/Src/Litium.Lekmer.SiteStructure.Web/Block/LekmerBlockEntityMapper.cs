﻿using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.SiteStructure.Web
{
	public class LekmerBlockEntityMapper : LekmerBlockEntityMapper<IBlock>
	{
	}

	public class LekmerBlockEntityMapper<TBlock> : BlockEntityMapper<TBlock>
		where TBlock : class, IBlock
	{
		public override void AddEntityVariables(Fragment fragment, TBlock item)
		{
			base.AddEntityVariables(fragment, item);

			LekmerBlockHelper.AddLekmerBlockVariables(fragment, item);
		}
	}
}
