﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.SiteStructure.Web
{
	public abstract class LekmerContentPageHandler : ContentPageHandler, ILekmerContentPageHandler, IEsalesGeneralControlResponse
	{
		private readonly Dictionary<string, AreaHandler> _risedAreaHandlers;

		protected PageParameters PageParameters { get; set; }

		protected LekmerContentPageHandler()
		{
			_risedAreaHandlers = new Dictionary<string, AreaHandler>();
		}

		protected override string RenderInternal()
		{
			//Instantiate all area handlers and block on the page.
			InstantiateAreaHandlers();

			return base.RenderInternal();
		}

		protected override string RenderCompletePage()
		{
			SetMasterTemplateId();

			Master.DynamicProperties["ActiveContentNodeId"] = ActiveContentNodeId;
			AppendBreadcrumbItems(Master.BreadcrumbItems);
			AppendMetaTags(Master.Page.MetaTags);

			Master.Page.Title = PageTitle();
			Master.Page.Content = Render();
			Master.Page.Title = PageTitle();

			return Master.Render();
		}

		/// <summary>
		/// Renders a single block on the page.
		/// </summary>
		/// <param name="blockId"></param>
		/// <returns></returns>
		protected override string RenderBlock(int blockId)
		{
			if (PageParameters.LoadBlockId.HasValue)
			{
				string esalesDataSourceBlockContent = RenderEsalesDataSourceBlock();

				return esalesDataSourceBlockContent + base.RenderBlock(blockId);
			}

			return base.RenderBlock(blockId);
		}

		protected override string RenderArea(string commonName, string tabs)
		{
			if (PageParameters.LoadAreaCommonName.HasValue())
			{
				string esalesDataSourceBlockContent = RenderEsalesDataSourceBlock();

				return esalesDataSourceBlockContent + base.RenderArea(commonName, tabs);
			}

			return base.RenderArea(commonName, tabs);
		}

		/// <summary>
		/// Renders a esales data source block on the page.
		/// </summary>
		/// <returns></returns>
		protected virtual string RenderEsalesDataSourceBlock()
		{
			IContentAreaFull contentArea;
			IBlock block = FindBlockOnPage(ContentPage, "EsalesDataSource", out contentArea);

			if (block == null && ContentPage.MasterPageId.HasValue)
			{
				var contentPageService = IoC.Resolve<IContentPageService>();
				IContentPageFull masterPage = contentPageService.GetFullById(UserContext.Current, ContentPage.MasterPageId.Value);

				block = FindBlockOnPage(masterPage, "EsalesDataSource", out contentArea);
			}

			if (block == null)
			{
				return string.Empty;
			}

			if (PageParameters.LoadBlockId.HasValue && block.Id == PageParameters.LoadBlockId.Value)
			{
				return string.Empty;
			}

			if (PageParameters.LoadAreaCommonName.HasValue() && contentArea.CommonName.Equals(PageParameters.LoadAreaCommonName, StringComparison.OrdinalIgnoreCase))
			{
				return string.Empty;
			}

			if (ShowBlockBasic(block) == false)
			{
				return string.Empty;
			}

			AreaHandler areaHandler = CreateAreaHandlerInstance(contentArea);
			BlockContent blockContent = areaHandler.RenderBlock(block);

			return blockContent.Body;
		}

		protected virtual IBlock FindBlockOnPage(IContentPageFull contentPage, string blockTypeCommonName, out IContentAreaFull contentArea)
		{
			foreach (IContentAreaFull area in contentPage.ContentAreas)
			{
				foreach (IBlock block in area.Blocks)
				{
					if (block.BlockType.CommonName == blockTypeCommonName)
					{
						contentArea = area;
						return block;
					}
				}
			}

			contentArea = null;
			return null;
		}

		protected virtual void InstantiateAreaHandlers()
		{
			Collection<IContentAreaFull> contentAreas = ContentPage.ContentAreas;

			foreach (IContentAreaFull contentArea in contentAreas)
			{
				LekmerAreaHandler areaHandler = CreateAreaHandlerInstance(contentArea).Cast<LekmerAreaHandler>();

				areaHandler.InstantiateBlockControls();
			}
		}

		protected override AreaHandler CreateAreaHandlerInstance(IContentAreaFull contentArea)
		{
			if (_risedAreaHandlers.ContainsKey(contentArea.CommonName))
			{
				return _risedAreaHandlers[contentArea.CommonName];
			}

			AreaHandler areaHandler = CreateAreaHandlerInstanceCore(contentArea);

			_risedAreaHandlers[contentArea.CommonName] = areaHandler;

			return areaHandler;
		}

		protected virtual AreaHandler CreateAreaHandlerInstanceCore(IContentAreaFull contentArea)
		{
			return LekmerAreaHandlerFactory.CreateAreaHandlerInstance(ContentPage, ContentNodeTreeItem, contentArea, this);
		}

		protected override void Initialize()
		{
			base.Initialize();

			PageParameters = GetPageParameters();
		}

		protected virtual bool ShowBlockBasic(IBlock block)
		{
			if ((BlockStatusInfo) block.BlockStatusId != BlockStatusInfo.Online)
			{
				return false;
			}

			var lekmerBlock = block as ILekmerBlock;
			if (lekmerBlock != null && !lekmerBlock.IsInTimeLimit)
			{
				return false;
			}

			return true;
		}

		/// <summary>
		/// Finds specified type of blocks rised on the page.
		/// </summary>
		public virtual Collection<TBlockControl> FindBlockControls<TBlockControl>() where TBlockControl : class
		{
			IEnumerable<TBlockControl> blockControls = _risedAreaHandlers.Values.Cast<LekmerAreaHandler>().SelectMany(a => a.FindBlockControls<TBlockControl>());

			return new Collection<TBlockControl>(blockControls.ToList());
		}

		/// <summary>
		/// Esales data that could be displayed on page.
		/// </summary>
		public IEsalesResponse EsalesGeneralResponse { get; set; }
	}
}