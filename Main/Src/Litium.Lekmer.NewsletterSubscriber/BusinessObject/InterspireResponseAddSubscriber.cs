﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponseAddSubscriber : InterspireResponse, IInterspireResponseAddSubscriber
	{
		public string ContactsIdNumber { get; set; }
	}
}