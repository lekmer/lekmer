﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class NewsletterSubscriber : BusinessObjectBase, INewsletterSubscriber
	{
		private int _id;
		private int _channelId;
		private string _email;
		private int _subscriberTypeId;
		private DateTime _createdDate;
		private DateTime _updatedDate;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}

		public int SubscriberTypeId
		{
			get { return _subscriberTypeId; }
			set
			{
				CheckChanged(_subscriberTypeId, value);
				_subscriberTypeId = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public DateTime UpdatedDate
		{
			get { return _updatedDate; }
			set
			{
				CheckChanged(_updatedDate, value);
				_updatedDate = value;
			}
		}
	}
}