﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireContactList : IInterspireContactList
	{
		public int ListId { get; set; }
		public string Name { get; set; }
		public int SubscribeCount { get; set; }
		public int UnsubscribeCount { get; set; }
		public int AutoResponderCount { get; set; }
	}
}