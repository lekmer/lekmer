﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponseIsContactOnList : InterspireResponse, IInterspireResponseIsContactOnList
	{
		public bool ContactIsLocated { get; set; }
	}
}