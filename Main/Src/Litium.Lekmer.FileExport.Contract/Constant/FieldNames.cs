﻿using System.ComponentModel;

namespace Litium.Lekmer.FileExport.Contract
{
	public enum FieldNames
	{
		[Description("Currency")]
		Currency = 1,
		[Description("Product Id")]
		ProductErpId = 2,
		[Description("Active")]
		Active = 3,
		[Description("Parent Category")]
		ParentCategoryTitle = 4,
		[Description("Category")]
		CategoryTitle = 5,
		[Description("Brand")]
		Brand = 6,
		[Description("Ean Code")]
		EanCode = 7,
		[Description("Product Title")]
		ProductTitle = 8,
		[Description("Product Title 1")]
		ProductTitle1 = 9,
		[Description("Product Title 2")]
		ProductTitle2 = 10,
		[Description("Product Title 3")]
		ProductTitle3 = 11,
		[Description("Lekmer Erp Id")]
		LekmerErpId = 12,
		[Description("Color")]
		Color = 13,
		[Description("Affected By Campaign")]
		AffectedByCampaign = 14,
		[Description("Campaign Titles")]
		CampaignTitles = 15,
		[Description("Delivery Time Days")]
		DeliveryTimeDays = 16,
		[Description("Select Me")]
		SelectMe = 17,
		[Description("Actual Price")]
		ActualPrice = 18,
		[Description("Original Price")]
		OriginalPrice = 19,
		[Description("In Stock")]
		InStock = 20,
		[Description("Freight")]
		Freight = 21,
		[Description("Description")]
		Description = 22,
		[Description("Product Url")]
		ProductUrl = 23,
		[Description("Category Level 2 Url")]
		SecondCategoryLevelUrl = 24,
		[Description("Category Level 3 Url")]
		ThirdCategoryLevelUrl = 25,
		[Description("Category Level 1 Url")]
		FirstCategoryLevelUrl = 26,
		[Description("Brand Url")]
		BrandUrl = 27,
		[Description("Original Image Url")]
		OriginalImageUrl = 28,
		[Description("Main Image Url")]
		MainImageUrl = 29,
		[Description("Thumbnail Image Url")]
		ThumbnailImageUrl = 30

	}
}