﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.CdonExport
{
	public interface ICdonProductInfoList
	{
		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		int TotalCount { get; set; }

		void AddOrReplace(ProductIdCollection productIdCollection);
		void Add(IChannel channel, ProductCollection productCollection);

		void Delete(int productId);

		Collection<ICdonProductInfo> GetCdonProductInfoCollection();
	}
}