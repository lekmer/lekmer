using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.CdonExport
{
	public interface ICdonChannelProductInfo
	{
		IChannel Channel { get; set; }
		ILekmerProductView Product { get; set; }
		IColor Color { get; set; }
	}
}