﻿namespace Litium.Lekmer.CdonExport
{
	public enum CdonProductCountrySellableStatus
	{
		SELLABLE,
		NOT_SELLABLE
	}
}