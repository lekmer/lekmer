﻿using System.ComponentModel;

namespace Litium.Lekmer.CdonExport
{
	public enum UnitTypeTime
	{
		[Description("Mon")]
		Month = 1,
		[Description("Qty")]
		Quantity = 2
	}
}