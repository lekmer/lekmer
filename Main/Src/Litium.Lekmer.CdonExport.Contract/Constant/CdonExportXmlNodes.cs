﻿namespace Litium.Lekmer.CdonExport
{
	public static class CdonExportXmlNodes
	{
		public const string LekmerApplication = "Lekmer";
		public const string HeppoApplication = "Heppo";

		// cdon_shopping_mall_import.
		public const string CdonShoppingMallImport = "cdon_shopping_mall_import";
		public const string Version = "version";

		// import_info
		public const string ImportInfoNode = "import_info";
		public const string ImportSourceIdNode = "import_source_id";
		public const string ImportIdNode = "import_id";
		public const string ImportDateNode = "import_date";
		public const string ImportTypeNode = "import_type";
		public const string ImportPreviousIdNode = "import_previous_id";

		// classes
		public const string ClassesNode = "classes";
		public const string ClassNode = "class";
		public const string ClassIdNode = "class_id";
		public const string ClassNameNode = "class_name";
		public const string ClassNameLongNode = "class_name_long";
		public const string ClassDescriptionNode = "class_description";
		public const string ClassNameTranslationsNode = "class_name_translations";
		public const string ClassNameTranslationNode = "class_name_translation";
		public const string ClassNameTranslationLongNode = "class_name_translation_long";

		// units
		public const string UnitNode = "unit";
		public const string UnitIdNode = "unit_id";
		public const string UnitNameNode = "unit_name";
		public const string UnitDescriptionNode = "unit_description";
		public const string UnitAbbreviationNode = "unit_abbreviation";
		public const string UnitFactorNode = "unit_factor";
		public const string UnitNameTranslationsNode = "unit_name_translations";
		public const string UnitNameTranslationNode = "unit_name_translation";
		public const string UnitAbbreviationTranslationNode = "unit_abbreviation_translation";

		// dimensions
		public const string DimenstionsNode = "dimensions";
		public const string DimenstionNode = "dimension";
		public const string DimenstionIdNode = "dimension_id";
		public const string DimenstionNameNode = "dimension_name";
		public const string DimensionUnits = "dimension_units";

		// brands
		public const string BrandsNode = "brands";
		public const string BrandNode = "brand";
		public const string BrandIdNode = "brand_id";
		public const string BrandNameNode = "brand_name";
		public const string BrandDescriptionNode = "brand_description";
		public const string BrandUrlNode = "brand_url";
		public const string BrandImageUrlNode = "brand_image_url";
		public const string BrandTranslationsNode = "brand_translations";
		public const string BrandTranslationNode = "brand_translation";
		public const string BrandNameTranslationNode = "brand_name_translation";
		public const string BrandDescriptionTransalationNode = "brand_description_translation";
		public const string BrandUrlTransalationNode = "brand_url_translation";
		public const string BrandImageUrlTranslationNode = "brand_image_url_translation";

		// variation_templates
		public const string VariationTemplatesNode = "variation_templates";
		public const string VariationTemplateNode = "template";
		public const string VariationTemplateIdNode = "id";
		public const string VariationTemplateComponentNode = "component";

		// attributes
		public const string AttributesNode = "attributes";
		public const string AttributeNode = "attribute";
		public const string AttributeIdNode = "attribute_id";
		public const string AttributeValueNode = "attribute_value";
		public const string AttributeValueTranslationsNode = "attribute_value_translations";
		public const string AttributeValueTranslationNode = "attribute_value_translation";
		public const string AttributeValueTranslationValueNode = "attribute_value_translation_value";
		public const string AttributeTypeNode = "attribute_type";
		public const string AttributeNameNode = "attribute_name";
		public const string AttributeDataTypeNode = "attribute_data_type";
		public const string AttributeNameTranslationsNode = "attribute_name_translations";
		public const string AttributeNameTranslationNode = "attribute_name_translation";
		public const string AttributeNameTranslationLongNode = "attribute_name_translation_long";
		public const string AttributeClassRelationsNode = "class_relation";
		public const string AttributeClassRelationNode = "class_relation";
		public const string AttributeIsVariantNode = "attribute_is_variant";
		public const string AttributeSelectListValuesNode = "attribute_select_list_values";
		public const string AttributeSelectListValueNode = "attribute_select_list_value";
		public const string AttributeSelectListValueIdNode = "attribute_select_list_value_id";
		public const string AttributeSelectListValueValueNode = "attribute_select_list_value_value";
		public const string AttributeSelectListValueTranslationsNode = "attribute_select_list_value_translations";
		public const string AttributeSelectListValueTranslationNode = "attribute_select_list_value_translation";
		public const string AttributeSelectListValueTranslationValueNode = "attribute_select_list_value_translation_value";

		// products
		public const string ProductsNode = "products";
		public const string ProductNode = "product";
		public const string ProductIdNode = "product_id";
		public const string ProductGtinNode = "product_gtin";
		public const string ProductStatusNode = "product_status";
		public const string ProductExposeStatusNode = "product_expose_status";
		public const string ProductQuantityNode = "product_quantity";
		public const string QuantityNode = "quantity";
		public const string ProductPurchaseCurrencyNode = "product_purchase_currency";
		public const string ProductPurchaseListPriceNode = "product_purchase_list_price";
		public const string ProductCountriesNode = "product_countries";
		public const string ProductCountryNode = "product_country";
		public const string ProductCountryCurrencyNode = "product_country_currency";
		public const string ProductCountryCurrentPriceNode = "product_country_current_price";
		public const string ProductCountryOrdinaryPriceNode = "product_country_ordinary_price";
		public const string ProductCountryVatNode = "product_country_vat";
		public const string ProductCountrySellableStatusNode = "product_country_sellable_status";
		public const string ProductCountryBackInStockNode = "product_country_back_in_stock";
		public const string ProductAttributesNode = "product_attributes";
		public const string ProductAttributeNode = "product_attribute";
		public const string ProductAttributeSelectListValueIdsNode = "attribute_select_list_value_ids";
		public const string ProductAttributeSelectListValueIdNode = "attribute_select_list_value_id";
		public const string CountryNode = "country";
		public const string ProductClassRelationsNode = "class_relations";
		public const string ProductClassRelationNode = "class_relation";
		public const string ProductClassIdNode = "class_id";
		public const string ProductCountryQuantityNode = "product_country_quantity";
		public const string ProductVariationAttributesNode = "variation_attributes";
		public const string ProductVariationAttributeNode = "variation_attribute";
		public const string ProductVariationKeyAttributesNode = "key_attributes";
		public const string ProductVariationAdditionalAttributesNode = "additional_attributes";
		public const string ProductVariationsNode = "product_variations";
		public const string ProductVariationNode = "product_variation";
		public const string ProductVariationsTemplateIdNode = "template_id";

		// general
		public const string LanguageIsoCodeNode = "language_iso_code";
		public const string SkuNode = "sku";
		public const string IdNode = "id";
		public const string SkuIdNode = "sku_id";
	}
}