﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionBrandService
	{
		Collection<ICdonExportRestrictionItem> GetIncludeAll();
		Collection<ICdonExportRestrictionItem> GetRestrictionAll();
	}
}