﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionCategorySecureService
	{
		void InsertIncludeCategory(ICdonExportRestrictionItem category, List<int> productChannelIds);

		void InsertRestrictionCategory(ICdonExportRestrictionItem category, List<int> productChannelIds);

		void DeleteIncludeCategories(List<int> categoryIds);

		void DeleteRestrictionCategories(List<int> categoryIds);

		Collection<ICdonExportRestrictionItem> GetIncludeAll();

		Collection<ICdonExportRestrictionItem> GetRestrictionAll();
	}
}