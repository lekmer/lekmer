using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert=Litium.Scensum.Foundation.Convert;

namespace Litium.Scensum.TopList.Repository
{
	public class TopListProductRepository
	{
		public virtual void PopulateTopListProductStatistic(DateTime statisticDate, int keepOrderStatisticDays)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Date", statisticDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("KeepOrderStatisticDays", keepOrderStatisticDays, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListProductRepository.PopulateTopListProductStatistic");
			new DataHandler().ExecuteCommand("[statistics].[pPopulateTopListProduct]", parameters, dbSettings);
		}

		[SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "To")]
		public virtual ProductIdCollection GetIdAllByStatisticsAndCategoriesExcludeForced(int channelId, int blockId, bool includeAllCategories, Collection<ICategoryView> categories, DateTime countOrdersFrom, int from, int to)
		{
			const char delimiter = ',';
			string categoryIdList =
				categories == null
					? null
					: Convert.ToStringIdentifierList(categories.Select(c => c.Id), delimiter);

			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@IncludeAllCategories", includeAllCategories, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@CategoryIds", categoryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("@CountOrdersFrom", countOrdersFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@From", from, SqlDbType.Int),
					ParameterHelper.CreateParameter("@To", to, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockTopListProductRepository.GetIdAllByStatisticsAndCategoriesExcludeForced");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[addon].[pTopListProductGetIdAllByStatisticsAndCategoryIdListExcludeForced]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(0));
					}
				}
			}
			productIds.TotalCount = totalCount;
			return productIds;
		}
	}
}