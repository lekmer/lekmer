using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListSecureService
	{
		IBlockTopList Create();

		IBlockTopList GetById(int id);

		int Save(ISystemUserFull systemUserFull, IBlockTopList block);

		int Save(ISystemUserFull systemUserFull, IChannel channel, IBlockTopList block,
		         Collection<IBlockTopListCategory> blockCategories, Collection<IBlockTopListProduct> blockProducts);
	}
}