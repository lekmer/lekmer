using System;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	public interface ITopListProductService
	{
		/// <summary>
		/// Populate product statistic table with data for @date.
		/// </summary>
		void PopulateTopListProductStatistic(DateTime statisticDate, int keepOrderStatisticDays);

		/// <summary>
		/// Gets all top list products. Forced position is taken into account.
		/// </summary>
		ProductCollection GetAllByBlock(IUserContext context, IBlockTopList block, int pageNumber, int pageSize);

		/// <summary>
		/// Gets ids of top list products. Forced position is taken into account.
		/// </summary>
		ProductIdCollection GetIdAllByBlock(IUserContext context, IBlockTopList block, int pageNumber, int pageSize);
	}
}