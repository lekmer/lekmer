﻿using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Avail.Web;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesRecommendControl : BlockProductPageControlBase<IBlockEsalesRecommend>
	{
		private readonly IBlockEsalesRecommendService _blockEsalesRecommendService;
		private readonly IEsalesRecommendService _recommendService;

		private string _panelPath;
		private string _fallbackPanelPath;
		private string _customerKey;
		private int _productId;
		private ProductIdCollection _cartProductIds;
		private int _itemsToReturn;

		private ProductCollection _products;

		public BlockEsalesRecommendControl(
			ITemplateFactory templateFactory,
			IBlockEsalesRecommendService blockEsalesRecommendService,
			IEsalesRecommendService recommendService)
			: base(templateFactory)
		{
			_blockEsalesRecommendService = blockEsalesRecommendService;
			_recommendService = recommendService;
		}

		protected override IBlockEsalesRecommend GetBlockById(int blockId)
		{
			return _blockEsalesRecommendService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Block.CanBeUsedAsFallbackOption.HasValue && Block.CanBeUsedAsFallbackOption.Value)
			{
				var blocks = ((IContentAreaFull) ContentArea).Blocks;
				var filterBlocks = blocks.Cast<ILekmerBlock>().Where(
					b => b.BlockType.CommonName == BlockTypeInfo.ProductRelationList.ToString()
					&& b.CanBeUsedAsFallbackOption.HasValue && b.CanBeUsedAsFallbackOption.Value);
				if (filterBlocks.Any(block => !block.UseFallback))
				{
					return new BlockContent();
				}
			}

			if (RecommendationTypeIsNotValid())
			{
				return new BlockContent(null, "[ Recommendation type is not valid ]");
			}

			if (Product == null && ProductPageIsRequired())
			{
				return new BlockContent(null, "[ This type of recommendation can't work on this page ]");
			}

			Initialize();

			if (_products.Count <= 0)
			{
				return new BlockContent();
			}

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render() ?? string.Empty;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}


		private void Initialize()
		{
			InitializePanelPath();
			InitializeCustomerKey();
			InitializeCartProductIds();
			InitializeProductId();
			InitializeItemsToReturn();

			_products = _recommendService.FindRecommend(UserContext.Current, _panelPath, _fallbackPanelPath, _customerKey, _productId, _cartProductIds, _itemsToReturn);
		}

		private void InitializePanelPath()
		{
			IEsalesRecommendPanelSetting panelSetting = new EsalesRecommendPanelSetting();

			_panelPath = Block.PanelPath;
			_fallbackPanelPath = Block.FallbackPanelPath;

			if (_fallbackPanelPath.IsEmpty())
			{
				_fallbackPanelPath = panelSetting.FallbackPanelPath;
			}

			if (_panelPath.HasValue())
			{
				return;
			}

			int? recommendationType = Block.RecommendationType;

			if (recommendationType.HasValue)
			{
				if (recommendationType.Value == (int) EsalesRecomendationTypeInfo.AbandonedCarts)
				{
					_panelPath = panelSetting.AbandonedCartsPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecentlyViewed)
				{
					_panelPath = panelSetting.RecentlyViewedPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.TopSellers)
				{
					_panelPath = panelSetting.TopSellersPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoBoughtAlsoBought)
				{
					_panelPath = panelSetting.ThoseWhoBoughtAlsoBoughtPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedAlsoViewed)
				{
					_panelPath = panelSetting.ThoseWhoViewedAlsoViewedPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedBought)
				{
					_panelPath = panelSetting.ThoseWhoViewedBoughtPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnCart)
				{
					_panelPath = panelSetting.RecommendBasedOnCartPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnCustomer)
				{
					_panelPath = panelSetting.RecommendBasedOnCustomerPanelPath;
				}
				else if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnProduct)
				{
					_panelPath = panelSetting.RecommendBasedOnProductPanelPath;
				}
			}
		}

		private void InitializeCustomerKey()
		{
			_customerKey = string.Empty;

			int? recommendationType = Block.RecommendationType;

			if (recommendationType.HasValue)
			{
				if (recommendationType.Value == (int) EsalesRecomendationTypeInfo.AbandonedCarts ||
					recommendationType.Value == (int) EsalesRecomendationTypeInfo.RecentlyViewed ||
					recommendationType.Value == (int) EsalesRecomendationTypeInfo.RecommendBasedOnCustomer)
				{
					var customerSession = IoC.Resolve<ICustomerSession>();
					_customerKey = CustomerIdentificationCookie.GetCustomerIdentificationId(customerSession.SignedInCustomer);
				}
			}
		}

		private void InitializeCartProductIds()
		{
			_cartProductIds = new ProductIdCollection();

			int? recommendationType = Block.RecommendationType;

			if (recommendationType.HasValue)
			{
				if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnCart)
				{
					ICartFull cart = IoC.Resolve<ICartSession>().Cart;
					if (cart != null)
					{
						Collection<ICartItem> cartItems = cart.GetCartItems();

						_cartProductIds.AddRange(cartItems.Select(c => c.Product.Id).Distinct().ToArray());
					}
				}
			}
		}

		private void InitializeProductId()
		{
			int? recommendationType = Block.RecommendationType;

			if (recommendationType.HasValue)
			{
				if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoBoughtAlsoBought ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedAlsoViewed ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedBought ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnProduct)
				{
					IProductView product = Product;
					if (product != null)
					{
						_productId = product.Id;
					}
				}
			}
		}

		private void InitializeItemsToReturn()
		{
			_itemsToReturn = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
		}


		private bool RecommendationTypeIsNotValid()
		{
			int? recommendationType = Block.RecommendationType;

			if (recommendationType.HasValue)
			{
				if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.AbandonedCarts ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecentlyViewed ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.TopSellers ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoBoughtAlsoBought ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedAlsoViewed ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedBought ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnCart ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnCustomer ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnProduct)
				{
					return false;
				}
			}

			return true;
		}

		private bool ProductPageIsRequired()
		{
			int? recommendationType = Block.RecommendationType;

			if (recommendationType.HasValue)
			{
				if (recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoBoughtAlsoBought ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedAlsoViewed ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.ThoseWhoViewedBought ||
					recommendationType.Value == (int)EsalesRecomendationTypeInfo.RecommendBasedOnProduct)
				{
					return true;
				}
			}

			return false;
		}
	}
}