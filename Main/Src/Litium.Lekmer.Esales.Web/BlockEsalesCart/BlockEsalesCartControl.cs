﻿using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesCartControl : BlockProductPageControlBase<IBlockList>
	{
		private readonly IBlockListService _blockListService;
		private readonly IEsalesCartService _esalesCartService;

		private string _panelPath;
		private string _fallbackPanelPath;
		private int _productId;
		private ProductIdCollection _cartProductIds;
		private int _itemsToReturn;
		private ProductCollection _products;

		protected override IBlockList GetBlockById(int blockId)
		{
			return _blockListService.GetById(blockId);
		}

		protected override BlockContent RenderCore()
		{
			var blocks = ((IContentAreaFull)ContentArea).Blocks;
			var filterBlocks = blocks.Cast<ILekmerBlock>().Where(
				b => b.BlockType.CommonName == BlockTypeInfo.ProductRelationList.ToString()
				&& b.CanBeUsedAsFallbackOption.HasValue && b.CanBeUsedAsFallbackOption.Value);
			if (filterBlocks.Any(block => !block.UseFallback))
			{
				return new BlockContent();
			}

			Initialize();
			if (_products.Count <= 0)
			{
				return new BlockContent();
			}
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		public BlockEsalesCartControl(
			ITemplateFactory templateFactory,
			IBlockListService blockListService,
			IEsalesCartService esalesCartService)
			: base(templateFactory)
		{
			_blockListService = blockListService;
			_esalesCartService = esalesCartService;
		}

		private void Initialize()
		{
			InitializePanelPath();
			InitializeCartProductIds();
			InitializeProductId();
			InitializeItemsToReturn();

			_products = _esalesCartService.FindRecommend(UserContext.Current, _panelPath, _fallbackPanelPath, _productId, _cartProductIds, _itemsToReturn);
		}

		private void InitializePanelPath()
		{
			var panelSetting = new EsalesCartPanelSetting(UserContext.Current.Channel.CommonName);
			_panelPath = panelSetting.PanelPath;
			_fallbackPanelPath = panelSetting.FallbackPanelPath;
		}

		private void InitializeCartProductIds()
		{
			_cartProductIds = new ProductIdCollection();

			ICartFull cart = IoC.Resolve<ICartSession>().Cart;
			if (cart != null)
			{
				Collection<ICartItem> cartItems = cart.GetCartItems();
				_cartProductIds.AddRange(cartItems.Select(c => c.Product.Id).Distinct().ToArray());
			}
		}

		private void InitializeProductId()
		{
			IProductView product = Product;
			if (product != null)
			{
				_productId = product.Id;
			}
		}

		private void InitializeItemsToReturn()
		{
			_itemsToReturn = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render() ?? string.Empty;
		}
	}
}
