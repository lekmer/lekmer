﻿using System;
using Litium.Lekmer.Avail.Cache;
using Litium.Lekmer.Avail.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Avail
{
	public class BlockAvailCombineService : IBlockAvailCombineService
	{
		protected BlockAvailCombineRepository Repository { get; private set; }

		public BlockAvailCombineService(BlockAvailCombineRepository repository)
		{
			Repository = repository;
		}

		public IBlockAvailCombine GetById(IUserContext context, int id)
		{
			if (context == null) throw new ArgumentNullException("context");

			return BlockAvailCombineCache.Instance.TryGetItem(
				new BlockAvailCombineKey(id),
				delegate { return Repository.GetById(id); });
		}
	}
}