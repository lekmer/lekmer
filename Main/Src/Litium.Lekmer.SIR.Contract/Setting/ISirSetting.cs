﻿namespace Litium.Lekmer.SIR
{
	public interface ISirSetting
	{
		string DestinationDirectory { get; }
		string ProductStructureFileName { get; }
		string ProductsFileName { get; }
		string SuppliersFileName { get; }
		string Delimiter { get; }
		int PageSize { get; }
		string CodePageName { get; }
	}
}