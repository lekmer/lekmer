namespace Litium.Lekmer.SIR
{
	public interface IProductInfo
	{
		string ArticleNumber { get; set; }
		int ProductId { get; set; }
		string CategoryErpId { get; set; }
		string ArticleDescription { get; set; }
		string EanCode { get; set; }
		decimal? OurPrice { get; set; }
		decimal? ListPrice { get; set; }
		string SupplierId { get; set; }
		string Brand { get; set; }
		string LekmerArtNo { get; set; }
		string SupplierArtNo { get; set; }
		decimal? AveragePrice { get; set; }
	}
}