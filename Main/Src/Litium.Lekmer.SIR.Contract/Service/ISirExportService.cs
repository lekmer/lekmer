﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.SIR
{
	public interface ISirExportService
	{
		Collection<IProductStructure> GetProductStructureCollection();
		
		void RefreshProductsData();
		Collection<IProductInfo> GetProductInfoCollection(ISirBatch batch);

		Collection<ISupplier> GetSupplierCollection();

		ISirBatch StartNewBatch();
		ISirBatch CompleteBatch(ISirBatch batch);
	}
}