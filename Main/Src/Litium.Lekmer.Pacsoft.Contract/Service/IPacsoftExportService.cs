﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Pacsoft.Contract
{
	public interface IPacsoftExportService
	{
		IEnumerable<IUserContext> GetUserContextForAllChannels();
		string GetFilePath(string channelCommonName, int orderid);
		Collection<IPacsoftInfo> GetPacsoftInfoCollectionToExport(IUserContext context);
		void PopulatePacsoftOrders(IUserContext context, string productHyId, string supplierId, int daysAfterPurchase);
		void UpdatePacsoftOrder(int orderId);
	}
}