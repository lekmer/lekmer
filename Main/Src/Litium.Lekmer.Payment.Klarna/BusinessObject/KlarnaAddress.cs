using Litium.Scensum.Customer;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaAddress : Address, IKlarnaAddress
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public bool IsCompany { get; set; }
		public string CompanyName { get; set; }
	}
}