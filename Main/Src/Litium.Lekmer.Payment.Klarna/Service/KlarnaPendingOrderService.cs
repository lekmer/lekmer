﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Klarna.Repository;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaPendingOrderService : IKlarnaPendingOrderService
	{
		protected KlarnaPendingOrderRepository Repository { get; private set; }

		public KlarnaPendingOrderService(KlarnaPendingOrderRepository repository)
		{
			Repository = repository;
		}

		public virtual IKlarnaPendingOrder Create()
		{
			var klarnaPendingOrder = IoC.Resolve<IKlarnaPendingOrder>();

			klarnaPendingOrder.FirstAttempt = DateTime.Now;
			klarnaPendingOrder.LastAttempt = DateTime.Now;

			return klarnaPendingOrder;
		}

		public virtual int Insert(IKlarnaPendingOrder klarnaPendingOrder)
		{
			Repository.EnsureNotNull();

			return Repository.Insert(klarnaPendingOrder);
		}

		public virtual Collection<IKlarnaPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			Repository.EnsureNotNull();

			return Repository.GetPendingOrdersForStatusCheck(checkToDate);
		}

		public virtual void Update(IKlarnaPendingOrder klarnaPendingOrder)
		{
			Repository.EnsureNotNull();

			Repository.Update(klarnaPendingOrder);
		}

		public virtual void Delete(int klarnaPendingOrderId)
		{
			Repository.EnsureNotNull();

			Repository.Delete(klarnaPendingOrderId);
		}
	}
}
