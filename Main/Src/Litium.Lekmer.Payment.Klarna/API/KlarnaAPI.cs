﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;
using Klarna.Core;
using Klarna.Core.Checkout;
using Klarna.Core.Structs;
using Litium.Lekmer.Payment.Klarna.Setting;
using log4net;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaApi : IKlarnaApi
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// Holds the timeout value depending on channel and order value
		/// </summary>
		private IEnumerable<ITimeOutData> _timeOutDataList;
		private string _channelCommonName;

		protected API Klarna { get; private set; }
		public IKlarnaSetting KlarnaSetting { get; set; }
		protected IKlarnaTransactionService KlarnaTransactionService { get; private set; }


		public KlarnaApi(IKlarnaTransactionService klarnaTransactionService)
		{
			KlarnaTransactionService = klarnaTransactionService;
		}


		public virtual void CreateOrderValueSpecificClient(double orderValue)
		{
			InitializeTimeoutCache();

			var timeOutData = _timeOutDataList.FirstOrDefault(t => orderValue >= t.OrderValue);

			if (timeOutData != null)
			{
				Klarna = new API(_channelCommonName, KlarnaSetting, timeOutData.TimeOutValue);
			}
			else
			{
				CreateClient();
			}
		}

		protected virtual void CreateClient()
		{
			Klarna = new API(_channelCommonName, KlarnaSetting, KlarnaSetting.Timeout);
		}


		public virtual void FetchPClasses()
		{
			try
			{
				CreateClient();
				Klarna.FetchPClasses();
			}
			catch (Exception e)
			{
				string method = MethodBase.GetCurrentMethod().Name;
				_log.Error("Error in (" + method + "): " + e.Message);
				throw new KlarnaException("Error in (" + method + "): " + e.Message);
			}
		}

		public virtual List<object> GetPClasses()
		{
			try
			{
				CreateClient();
				List<PClass> klarnaPClasses = Klarna.GetPClasses(PClass.PClassType.NULL);
				return klarnaPClasses.Cast<object>().ToList();
			}
			catch (Exception e)
			{
				string method = MethodBase.GetCurrentMethod().Name;
				_log.Error("Error in (" + method + "): " + e.Message);
				throw new KlarnaException("Error in (" + method + "): " + e.Message);
			}
		}

		/// <summary>
		/// Retrieve address information from Klarna for specified civic number
		/// </summary>
		public virtual IKlarnaGetAddressResponse GetAddress(string civicNumber, int pnoEncoding, string ip)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Get addresses");
			}

			var duration = new Stopwatch();
			IKlarnaGetAddressResponse klarnaResult = null;

			int klarnaTransactionId = KlarnaTransactionService.CreateGetAddress(KlarnaSetting.EID, KlarnaSetting.Mode, civicNumber);

			try
			{
				CreateClient();
				Klarna.ClientIP = ip;

				duration.Start();
				List<Address> addresses = Klarna.GetAddresses(civicNumber, (API.Encoding)pnoEncoding, API.GetAddressFlag.GA_All /*GA_OLD*/);
				duration.Stop();

				klarnaResult = new KlarnaGetAddressResponse
				{
					ResponseCode = (int) KlarnaResponseCode.Ok,
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return BuildKlarnaGetAddressResponse(klarnaResult, addresses);
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Klarna call returned exception on get address for '{0}'.", civicNumber);

				LogException(logMessage, ex);

				klarnaResult = new KlarnaGetAddressResponse
				{
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleKlarnaException(ex, klarnaResult);

				return klarnaResult;
			}
			finally
			{
				KlarnaTransactionService.SaveResult(klarnaResult);
			}
		}

		/// <summary>
		/// Used to create an normal item for the goodsList
		/// </summary>
		/// <param name="quantity">Quantity of the articles</param>
		/// <param name="articleNumber">Article number</param>
		/// <param name="title">Article title</param>
		/// <param name="price">The articles price, including VAT</param>
		/// <param name="vat">VAT in percent</param>
		/// <param name="discount">Discount in percent</param>
		public virtual void AddArticle(int quantity, string articleNumber, string title, double price, double vat, double discount)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Add article");
			}

			Klarna.AddArticle(quantity, articleNumber, title, price, vat, discount, API.GoodsIs.IncVAT);
		}

		/// <summary>
		/// Used to create an shipment item for the goodsList
		/// </summary>
		/// <param name="quantity">Quantity of the articles</param>
		/// <param name="articleNumber">Article number</param>
		/// <param name="title">Article title</param>
		/// <param name="price">The articles price, including VAT</param>
		/// <param name="vat">VAT in percent</param>
		public virtual void AddShipment(int quantity, string articleNumber, string title, double price, double vat)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Add shipment");
			}

			// You cannot set discounts on a fee
			Klarna.AddArticle(quantity, articleNumber, title, price, vat, 0, API.GoodsIs.Shipping | API.GoodsIs.IncVAT);
		}

		public virtual void SetShippingAddress(
			string email,
			string telNo,
			string cellNo,
			string fName,
			string lName,
			string careof,
			string company,
			string street,
			string zip,
			string city,
			string countryIso,
			string houseNumber,
			string houseExtension)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Set shipping address");
			}

			var shippingAddress = new ShippingAddress(email, telNo, cellNo, fName, lName, careof, street, zip, city, GetCountry(countryIso), houseNumber, houseExtension, company);

			Klarna.SetAddress(shippingAddress);
		}

		public virtual void SetBillingAddress(
			string email,
			string telNo,
			string cellNo,
			string fName,
			string lName,
			string careof,
			string company,
			string street,
			string zip,
			string city,
			string countryIso,
			string houseNumber,
			string houseExtension)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Set billing address");
			}

			var billingAddress = new BillingAddress(email, telNo, cellNo, fName, lName, careof, street, zip, city, GetCountry(countryIso), houseNumber, houseExtension, company);

			Klarna.SetAddress(billingAddress);
		}

		/// <summary>
		/// Reserves an amount for a company.
		/// </summary>
		public virtual IKlarnaReservationResult ReserveAmountCompany(
			string orgNumber,
			int? gender,
			double amount,
			string reference,
			int orderId,
			string ip,
			int pClass)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Reserve amount company");
			}

			var duration = new Stopwatch();
			IKlarnaReservationResult klarnaResult = null;

			int klarnaTransactionId = KlarnaTransactionService.CreateReservation(KlarnaSetting.EID, KlarnaSetting.Mode, (int)KlarnaTransactionType.ReserveAmountCompany, orgNumber, orderId, (decimal)amount, (int)Klarna.CurrencyCode, pClass, 0);

			try
			{
				SetKlarnaApiInfo(ip, orderId, 0, reference);

				duration.Start();
				string[] klarnaResultRaw = Klarna.ReserveAmount(orgNumber, (API.Gender?)gender, amount, (API.Flag)KlarnaSetting.Mode, pClass);
				duration.Stop();

				KlarnaResponseCode responseCode;
				string reservationNr;
				ParseKlarnaReserveAmountResult(klarnaResultRaw, out responseCode, out reservationNr);

				_log.InfoFormat("Klarna reservation made!. OrderId: {0}. ReferenceNumber: {1}", orderId, reservationNr);

				// Return reservation number.
				klarnaResult = new KlarnaReservationResult
				{
					ResponseCode = (int) responseCode,
					ReservationId = reservationNr,
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return klarnaResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Klarna call returned exception on reserve amount company. OrderId: {0}", orderId);

				LogException(logMessage, ex);

				klarnaResult = new KlarnaReservationResult
				{
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleKlarnaException(ex, klarnaResult);

				return klarnaResult;
			}
			finally
			{
				KlarnaTransactionService.SaveReservationResponse(klarnaResult);
			}
		}

		/// <summary>
		/// Reserves a purchased amount to a particular customer. The reservation is valid, by default, in 7 days.
		/// </summary>
		public virtual IKlarnaReservationResult ReserveAmount(
			string civicNumber,
			int? gender,
			double amount,
			int orderId,
			string ip,
			int pClass,
			double yearlySalary)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Reserve amount");
			}

			var duration = new Stopwatch();
			IKlarnaReservationResult klarnaResult = null;

			int klarnaTransactionId = KlarnaTransactionService.CreateReservation(KlarnaSetting.EID, KlarnaSetting.Mode, (int)KlarnaTransactionType.ReserveAmount, civicNumber, orderId, (decimal)amount, (int)Klarna.CurrencyCode, pClass, (int)(yearlySalary * 100));

			try
			{
				SetKlarnaApiInfo(ip, orderId, yearlySalary, null);

				duration.Start();
				string[] klarnaResultRaw = Klarna.ReserveAmount(civicNumber, (API.Gender?)gender, amount, (API.Flag)KlarnaSetting.Mode, pClass);
				duration.Stop();

				KlarnaResponseCode responseCode;
				string reservationNr;
				ParseKlarnaReserveAmountResult(klarnaResultRaw, out responseCode, out reservationNr);

				_log.InfoFormat("Klarna reservation made!. OrderId: {0}. ReferenceNumber: {1}", orderId, reservationNr);

				// Return reservation number.
				klarnaResult = new KlarnaReservationResult
				{
					ResponseCode = (int) responseCode,
					ReservationId = reservationNr,
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return klarnaResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Klarna call returned exception on reserve amount. OrderId: {0}", orderId);

				LogException(logMessage, ex);

				klarnaResult = new KlarnaReservationResult
				{
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleKlarnaException(ex, klarnaResult);

				return klarnaResult;
			}
			finally
			{
				KlarnaTransactionService.SaveReservationResponse(klarnaResult);
			}
		}

		/// <summary>
		/// Cancelling a reservation
		/// </summary>
		public virtual IKlarnaResult CancelReservation(int orderId, string reservationNumber)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Cancel reservation");
			}

			var duration = new Stopwatch();
			IKlarnaResult klarnaResult = null;

			int klarnaTransactionId = KlarnaTransactionService.CreateCancelReservation(KlarnaSetting.EID, KlarnaSetting.Mode, (int)KlarnaTransactionType.CancelReservation, orderId, reservationNumber);

			try
			{
				CreateClient();

				duration.Start();
				bool cancelationResult = Klarna.CancelReservation(reservationNumber);
				duration.Stop();

				KlarnaResponseCode responseCode = cancelationResult ? KlarnaResponseCode.Ok : KlarnaResponseCode.UnspecifiedError;

				_log.InfoFormat("Klarna reservation result: reservation number: {0}, canceled: {1}", reservationNumber, cancelationResult);

				klarnaResult = new KlarnaResult
				{
					ResponseCode = (int)responseCode,
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return klarnaResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Klarna call returned exception on cancel reservation. Reservation number: {0}", reservationNumber);

				LogException(logMessage, ex);

				klarnaResult = new KlarnaResult
				{
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleKlarnaException(ex, klarnaResult);

				return klarnaResult;
			}
			finally
			{
				KlarnaTransactionService.SaveResult(klarnaResult);
			}
		}

		/// <summary>
		/// Checks status on yor order
		/// </summary>
		public virtual IKlarnaResult CheckOrderStatus(int orderId, string reservationNumber)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Check order status");
			}

			var duration = new Stopwatch();
			IKlarnaResult klarnaResult = null;

			int klarnaTransactionId = KlarnaTransactionService.CreateCheckOrderStatus(KlarnaSetting.EID, KlarnaSetting.Mode, (int)KlarnaTransactionType.CheckOrderStatus, orderId, reservationNumber);

			try
			{
				CreateClient();

				duration.Start();
				API.OrderStatus result = Klarna.CheckOrderStatus(reservationNumber, API.OrderStatusFor.InvoiceOrReservation);
				duration.Stop();

				var responseCode = KlarnaResponseCode.None;

				switch (result)
				{
					case API.OrderStatus.Accepted:
						responseCode = KlarnaResponseCode.Ok;
						break;
					case API.OrderStatus.Denied:
						responseCode = KlarnaResponseCode.Denied;
						break;
					case API.OrderStatus.Pending:
						responseCode = KlarnaResponseCode.Pending;
						break;
				}

				_log.InfoFormat("Klarna check order status result: reservation number: {0}, status: {1}", reservationNumber, result);

				klarnaResult = new KlarnaResult
				{
					ResponseCode = (int)responseCode,
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return klarnaResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Klarna call returned exception on check order status. Reservation number: {0}", reservationNumber);

				LogException(logMessage, ex);

				klarnaResult = new KlarnaResult
				{
					KlarnaTransactionId = klarnaTransactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleKlarnaException(ex, klarnaResult);

				return klarnaResult;
			}
			finally
			{
				KlarnaTransactionService.SaveResult(klarnaResult);
			}
		}

		public virtual double CalculateCheapestMonthlyFee()
		{
			try
			{
				CreateClient();
				return Klarna.GetCheapestMonthlyFee();
			}
			catch (Exception e)
			{
				string method = MethodBase.GetCurrentMethod().Name;
				_log.Error("Error in (" + method + "): " + e.Message);
				throw new KlarnaException("Error in (" + method + "): " + e.Message);
			}
		}
		public virtual double CalculateCheapestMonthlyCost(double sum, int flags)
		{
			var calculationOn = (API.CalculateOn) flags;

			CreateClient();
			PClass pclass = Klarna.GetCheapestPClass(sum, calculationOn);

			var calc = new Calc();
			double monthlyCost = calc.CalcMonthlyCost(sum, pclass, calculationOn);

			return monthlyCost;
		}
		public virtual double CalculateMonthlyCost(double sum, object pClass, int flags)
		{
			var calc = new Calc();
			double monthlyCost = calc.CalcMonthlyCost(sum, (PClass)pClass, (API.CalculateOn)flags);
			return monthlyCost;
		}

		public virtual void Initialize(string channelCommonName)
		{
			_channelCommonName = channelCommonName;
			KlarnaSetting.Initialize(channelCommonName);
		}


		//Helper methods

		private void InitializeTimeoutCache()
		{
			if (_timeOutDataList != null)
			{
				return;
			}

			try
			{
				_timeOutDataList = KlarnaSetting.GetTimeoutSteps().ToArray();
			}
			catch (Exception ex)
			{
				_log.Error("Could not read timeout settings!", ex);
			}
		}

		private API.Country GetCountry(string countryIso)
		{
			switch (countryIso.ToLower())
			{
				case "dk":
					return API.Country.Denmark;
				case "fi":
					return API.Country.Finland;
				case "nl":
					return API.Country.Netherlands;
				case "no":
					return API.Country.Norway;
				case "se":
					return API.Country.Sweden;
				default:
					throw new ArgumentException(string.Format("GetCountry: \"{0}\" not supported.", countryIso));
			}
		}

		private void SetKlarnaApiInfo(string ip, int orderId, double yearlySalary, string reference)
		{
			var tm = new ThreatMetrix(Klarna.EId);
			var sid = new SessionID { DeviceID1 = tm.SessionID };
			Klarna.SessionID = sid;
			Klarna.ClientIP = ip;
			Klarna.OrderID1 = orderId.ToString(CultureInfo.InvariantCulture);
			Klarna.OrderID2 = orderId.ToString(CultureInfo.InvariantCulture);
			Klarna.IncomeExpense = new IncomeExpense { YearlySalary = (int)yearlySalary };
			if (reference != null)
			{
				Klarna.Reference = reference;
			}
		}

		private void ParseKlarnaReserveAmountResult(string[] klarnaResult, out KlarnaResponseCode responseCode, out string reservationNr)
		{
			API.OrderStatus invoiceStatus = (API.OrderStatus)Convert.ToInt32(klarnaResult[1]);
			if (invoiceStatus == API.OrderStatus.Accepted)
			{
				//The order is accepted
				responseCode = KlarnaResponseCode.Ok;
			}
			else if (invoiceStatus == API.OrderStatus.Denied)
			{
				//The order is denied
				responseCode = KlarnaResponseCode.Denied;
			}
			else
			{
				/* The order is under manual review and will be accepted or denied at a later stage.
				  Use cronjob with CheckOrderStatus() or visit Klarna Online to check to see if the status has changed.
				  You should still show it to the customer as it was accepted, to avoid further attempts to fraud. */
				responseCode = KlarnaResponseCode.Pending;
			}

			reservationNr = klarnaResult[0];
		}

		private void LogException(string message, Exception ex)
		{
			var klarnaException = ex as KlarnaException;
			if (klarnaException == null)
			{
				_log.Error(message, ex);
			}
			else
			{
				message = message + string.Format(
					" // \r\nFault code: {0}, \r\nFault string: {1} \r\n",
					klarnaException.FaultCode, klarnaException.FaultString);

				if (klarnaException.FaultCode == -99)
				{
					_log.Error(message, klarnaException);
				}
				else
				{
					_log.Warn(message, klarnaException);
				}
			}
		}

		private void TryHandleKlarnaException<T>(Exception ex, T klarnaResult) where T : IKlarnaResult
		{
			if (ex is KlarnaException)
			{
				// Something wnt wrong in Klarna, check error code and reason

				var klarnaException = ex as KlarnaException;

				klarnaResult.FaultCode = klarnaException.FaultCode;
				klarnaResult.FaultReason = klarnaException.FaultString;

				klarnaResult.ResponseCode =
					klarnaException.FaultCode == -99
					? (int) KlarnaResponseCode.UnspecifiedError
					: (int) KlarnaResponseCode.SpecifiedError;
			}
			else if (ex is System.Net.WebException && ex.Message == "The operation has timed out")
			{
				// Timeout error

				klarnaResult.FaultCode = -999;
				klarnaResult.FaultReason = ex.ToString();

				if (KlarnaSetting.HandleTimeoutResponse)
				{
					klarnaResult.ResponseCode = (int)KlarnaResponseCode.TimeoutResponse;
				}
				else
				{
					klarnaResult.ResponseCode = (int)KlarnaResponseCode.UnspecifiedError;
				}
			}
			else
			{
				// Unspecified error

				klarnaResult.FaultCode = -9999;
				klarnaResult.FaultReason = ex.ToString();

				klarnaResult.ResponseCode = (int)KlarnaResponseCode.UnspecifiedError;
			}

		}

		/// <summary>
		/// Builds a IKlarnaResponseItem from a Klarna call result.
		/// </summary>
		private static IKlarnaGetAddressResponse BuildKlarnaGetAddressResponse(IKlarnaGetAddressResponse klarnaGetAddressResponse, List<Address> addresses)
		{
			try
			{
				if (addresses != null && addresses.Count > 0)
				{
					klarnaGetAddressResponse.Addresses = addresses.Cast<object>().ToList();
				}
			}
			catch (Exception)
			{
				_log.Error("Klarna call returned an invalid format in BuildKlarnaGetAddressResponse.");
			}

			try
			{
				if (addresses != null && addresses.Count > 0)
				{
					var serializer = new JavaScriptSerializer();
					string json = serializer.Serialize(addresses);
					klarnaGetAddressResponse.ResponseContent = json;
				}
			}
			catch (Exception)
			{
				_log.Error("Json serialization fails in BuildKlarnaGetAddressResponse.");
			}

			return klarnaGetAddressResponse;
		}
	}
}