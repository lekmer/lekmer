﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna.Repository
{
	public class KlarnaTransactionRepository
	{
		public int CreateGetAddress(int shopId, int mode, int transactionType, string civicNumber, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("KlarnaTransactionRepository.CreateGetAddress");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaShopId", shopId, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaMode", mode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaTransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CivicNumber", civicNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pKlarnaTransactionCreateGetAddress]", parameters, dbSettings);
		}

		public int CreateReservation(int shopId, int mode, int transactionType, string civicNumber, int orderId, decimal amount, int currencyId, int pClass, int yearlySalary, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("KlarnaTransactionRepository.CreateReservation");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaShopId", shopId, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaMode", mode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaTransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CivicNumber", civicNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Amount", amount, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("CurrencyId", currencyId, SqlDbType.Int),
					ParameterHelper.CreateParameter("PClassId", pClass, SqlDbType.Int),
					ParameterHelper.CreateParameter("YearlySalary", yearlySalary, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pKlarnaTransactionCreateReservation]", parameters, dbSettings);
		}

		public int CreateCancelReservation(int shopId, int mode, int transactionType, int orderId, string reservationNumber, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("KlarnaTransactionRepository.CreateCancelReservation");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaShopId", shopId, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaMode", mode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaTransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReservationNumber", reservationNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pKlarnaTransactionCreateCancelReservation]", parameters, dbSettings);
		}

		public int CreateCheckOrderStatus(int shopId, int mode, int transactionType, int orderId, string reservationNumber, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("KlarnaTransactionRepository.CreateCheckOrderStatus");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaShopId", shopId, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaMode", mode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaTransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReservationNumber", reservationNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pKlarnaTransactionCreateCheckOrderStatus]", parameters, dbSettings);
		}

		public void SaveResult(IKlarnaResult klarnaResult)
		{
			var dbSettings = new DatabaseSetting("KlarnaTransactionRepository.SaveResult");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaTransactionId", klarnaResult.KlarnaTransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ResponseCodeId", klarnaResult.ResponseCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaFaultCode", klarnaResult.FaultCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaFaultReason", klarnaResult.FaultReason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", klarnaResult.Duration, SqlDbType.BigInt),
					ParameterHelper.CreateParameter("ResponseContent", klarnaResult.ResponseContent, SqlDbType.NVarChar)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pKlarnaTransactionSaveResult]", parameters, dbSettings);
		}

		public void SaveReservationResponse(IKlarnaReservationResult klarnaReservationResult)
		{
			var dbSettings = new DatabaseSetting("KlarnaTransactionRepository.SaveReservationResponse");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("KlarnaTransactionId", klarnaReservationResult.KlarnaTransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReservationNumber", klarnaReservationResult.ReservationId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ResponseCodeId", klarnaReservationResult.ResponseCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaFaultCode", klarnaReservationResult.FaultCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("KlarnaFaultReason", klarnaReservationResult.FaultReason, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", klarnaReservationResult.Duration, SqlDbType.BigInt)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pKlarnaTransactionSaveReservationResponse]", parameters, dbSettings);
		}
	}
}
