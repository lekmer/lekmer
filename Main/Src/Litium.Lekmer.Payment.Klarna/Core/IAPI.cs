﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections;
using System.Collections.Generic;
using CookComputing.XmlRpc;

using Klarna.Core.Structs;

namespace Klarna.Core
{
    [XmlRpcUrl("https://payment.klarna.com:443")]
    public interface IAPI : IXmlRpcProxy
    {

        [XmlRpcMethod("get_addresses")]
        Array get_addresses (string proto, string client, string pno, int eid,
            string digest, int pnoEncoding, int type, string ip
        );

        [XmlRpcMethod("add_invoice")]
        Array add_invoice (
            string proto,
            string client,
            string pno,
            int gender,
            string reference,
            string reference_code,
            string orderid1,
            string orderid2,
            AddressStruct shipping,
            AddressStruct billing,
            string clientip,
            int flags,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int encoding,
            int pclass,
            Goods[] goodslist,
            string comment,
            XmlRpcStruct shipmentinfo,
            XmlRpcStruct travelinfo,
            XmlRpcStruct incomeexpense,
            XmlRpcStruct bankinfo,
            XmlRpcStruct sessionid,
            XmlRpcStruct extrainfo
        );

        [XmlRpcMethod("add_invoice")]
        Array add_invoice (
            string proto,
            string client,
            string pno,
            string gender,
            string reference,
            string reference_code,
            string orderid1,
            string orderid2,
            AddressStruct shipping,
            AddressStruct billing,
            string clientip,
            int flags,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int encoding,
            int pclass,
            Goods[] goodslist,
            string comment,
            XmlRpcStruct shipmentinfo,
            XmlRpcStruct travelinfo,
            XmlRpcStruct incomeexpense,
            XmlRpcStruct bankinfo,
            XmlRpcStruct sessionid,
            XmlRpcStruct extrainfo
        );

        [XmlRpcMethod("activate_invoice")]
        string activate_invoice (
            string proto,
            string client,
            int eid,
            string invono,
            string digest,
            int? pclass,
            XmlRpcStruct sinfo
        );

        [XmlRpcMethod("delete_invoice")]
        string delete_invoice (
            string proto,
            string client,
            int eid,
            string invno,
            string digest
        );

        [XmlRpcMethod("activate_part")]
        XmlRpcStruct activate_part (
            string proto,
            string client,
            int eid,
            string invno,
            ArticleNo[] artnos,
            string digest,
            int pclass,
            XmlRpcStruct sinfo
        );

        [XmlRpcMethod("invoice_amount")]
        int invoice_amount (
            string proto,
            string client,
            int eid,
            string invoiceNo,
            string digest
        );

        [XmlRpcMethod("invoice_part_amount")]
        int invoice_part_amount (
            string proto,
            string client,
            int eid,
            string invoiceNo,
            ArticleNo[] articleNumbers,
            string digest
        );

        [XmlRpcMethod("invoice_address")]
        string[] invoice_address (
            string proto,
            string client,
            int eid,
            string invoiceNo,
            string digest
        );

        [XmlRpcMethod("update_orderno")]
        string update_orderno (
            string proto,
            string client,
            int eid,
            string digest,
            string invoiceNo,
            string newOrderID
        );

        [XmlRpcMethod("email_invoice")]
        string email_invoice (
            string proto,
            string client,
            int eid,
            string invoiceNumber,
            string digest
        );

        [XmlRpcMethod("send_invoice")]
        string send_invoice (
            string proto,
            string client,
            int eid,
            string invoiceNumber,
            string digest
        );

        [XmlRpcMethod("reserve_amount")]
        Array reserve_amount (
            string proto,
            string client,
            string pno,
            int gender,
            int amount,
            string reference,
            string referenceCode,
            string orderID1,
            string orderID2,
            AddressStruct shipping,
            AddressStruct billing,
            string clientIP,
            int flags,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int pnoEncoding,
            int pClass,
            Goods[] goodsList,
            string comment,
            XmlRpcStruct sinfo,
            XmlRpcStruct tinfo,
            XmlRpcStruct ice,
            XmlRpcStruct binfo,
            XmlRpcStruct sid,
            XmlRpcStruct einfo
        );

        [XmlRpcMethod("reserve_amount")]
        Array reserve_amount (
            string proto,
            string client,
            string pno,
            string gender,
            int amount,
            string reference,
            string referenceCode,
            string orderID1,
            string orderID2,
            AddressStruct shipping,
            AddressStruct billing,
            string clientIP,
            int flags,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int pnoEncoding,
            int pClass,
            Goods[] goodsList,
            string comment,
            XmlRpcStruct sinfo,
            XmlRpcStruct tinfo,
            XmlRpcStruct ice,
            XmlRpcStruct binfo,
            XmlRpcStruct sid,
            XmlRpcStruct einfo
        );

        [XmlRpcMethod("cancel_reservation")]
        string cancel_reservation (
            string proto,
            string client,
            string rno,
            int eid,
            string digest
        );

        [XmlRpcMethod("change_reservation")]
        string change_reservation (
            string proto,
            string client,
            string rno,
            int amount,
            int eid,
            string digest,
            int flags
        );

        [XmlRpcMethod("activate_reservation")]
        string[] activate_reservation (
            string proto,
            string client,
            string rno,
            string ocr,
            string pno,
            int gender,
            string reference,
            string referenceCode,
            string orderID1,
            string orderID2,
            AddressStruct shipping,
            AddressStruct billing,
            string clientIP,
            int flags,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int pnoEncoding,
            int pClass,
            Goods[] goodsList,
            string comment,
            XmlRpcStruct sinfo,
            XmlRpcStruct tinfo,
            XmlRpcStruct ice,
            XmlRpcStruct binfo,
            XmlRpcStruct einfo
        );

        [XmlRpcMethod("activate_reservation")]
        string[] activate_reservation (
            string proto,
            string client,
            string rno,
            string ocr,
            string pno,
            string gender,
            string reference,
            string referenceCode,
            string orderID1,
            string orderID2,
            AddressStruct shipping,
            AddressStruct billing,
            string clientIP,
            int flags,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int pnoEncoding,
            int pClass,
            Goods[] goodsList,
            string comment,
            XmlRpcStruct sinfo,
            XmlRpcStruct tinfo,
            XmlRpcStruct ice,
            XmlRpcStruct binfo,
            XmlRpcStruct einfo
        );

        [XmlRpcMethod("split_reservation")]
        object[] split_reservation (
            string proto,
            string client,
            string rno,
            int amount,
            string orderID1,
            string orderID2,
            int flags,
            int eid,
            string digest
        );

        [XmlRpcMethod("check_reservation")]
        string check_reservation (
            string proto,
            string client,
            int eid,
            string reservationNumber,
            int amount,
            AddressStruct address,
            string digest,
            string pno
        );

        [XmlRpcMethod("reserve_ocr_nums")]
        string[] reserve_ocr_nums (
            string proto,
            string client,
            int numberOfOCR,
            int eid,
            string digest,
            int country
        );

        [XmlRpcMethod("reserve_ocr_nums_email")]
        string reserve_ocr_nums_email (
            string proto,
            string client,
            int numberOfOCR,
            string email,
            int eid,
            string digest,
            int country
        );

        [XmlRpcMethod("credit_invoice")]
        string credit_invoice (
            string proto,
            string client,
            int eid,
            string invoiceNumber,
            string creditNote,
            string digest
        );

        [XmlRpcMethod("credit_part")]
        string credit_part (
            string proto,
            string client,
            int eid,
            string invoiceNumber,
            ArticleNo[] articleNumbers,
            string creditNote,
            string digest
        );

        [XmlRpcMethod("credit_part")]
        string credit_part (
            string proto,
            string client,
            int eid,
            string invoiceNumber,
            ArticleNo[] articleNumbers,
            string creditNote,
            string digest,
            int flag,
            Goods[] goodsList
        );

        [XmlRpcMethod("return_amount")]
        string return_amount (
            string proto,
            string client,
            int eid,
            string invoiceNumber,
            int amount,
            double VAT,
            string digest,
            int flags,
            string description);

        [XmlRpcMethod("update_goods_qty")]
        string update_goods_qty (
            string proto,
            string client,
            int eid,
            string digest,
            string invoiceNumber,
            string articleNumber,
            int quantity);

        [XmlRpcMethod("update_charge_amount")]
        string update_charge_amount (
            string proto,
            string client,
            int eid,
            string digest,
            string invoiceNumber,
            int type,
            int newAmount);

        [XmlRpcMethod("check_order_status")]
        int check_order_status (
            string proto,
            string client,
            int eid,
            string digest,
            string id,
            int type);

        [XmlRpcMethod("get_customer_no")]
        string[] get_customer_no (
            string proto,
            string client,
            string pno,
            int eid,
            string digest,
            int encoding);

        [XmlRpcMethod("set_customer_no")]
        string set_customer_no (
            string proto,
            string client,
            string pno,
            string customerNumber,
            int eid,
            string digest,
            int encoding);

        [XmlRpcMethod("remove_customer_no")]
        string remove_customer_no (
            string proto,
            string client,
            string customerNumber,
            int eid,
            string digest);

        [XmlRpcMethod("update_email")]
        string update_email (
            string proto,
            string client,
            int eid,
            string digest,
            string pno,
            string email);

        [XmlRpcMethod("update_notes")]
        string update_notes (
            string proto,
            string client,
            int eid,
            string digest,
            string invoiceNumber,
            string notes);

        [XmlRpcMethod("get_pclasses")]
        Array get_pclasses (
            string proto,
            string client,
            int eid,
            int currency,
            string digest,
            int county,
            int language);

        [XmlRpcMethod("has_account")]
        string has_account (
            string proto,
            string client,
            int eid,
            string pno,
            string digest,
            int encoding);

        [XmlRpcMethod("check_ilt")]
        XmlRpcStruct check_ilt (
            string proto,
            string client,
            string pno,
            int gender,
            AddressStruct shipping,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int encoding,
            int pclass,
            int amount);

        [XmlRpcMethod("check_ilt")]
        XmlRpcStruct check_ilt (
            string proto,
            string client,
            string pno,
            string gender,
            AddressStruct shipping,
            int currency,
            int country,
            int language,
            int eid,
            string digest,
            int encoding,
            int pclass,
            int amount);

        [XmlRpcMethod("activate")]
        string[] activate (

            string proto,
            string client,
            int eid,
            string digest,
            string rno,
            XmlRpcStruct optional_info
        );

        [XmlRpcMethod("update")]
        string update(
            string proto,
            string client,
            int eid,
            string digest,
            string rno,
            XmlRpcStruct optional_info
        );
    }
}
