/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using Klarna.Core.Structs;

namespace Klarna.Core
{
    partial class API
    {
        /// <summary>
        /// Reserves a purchase amount for a specific customer.
        /// The reservation is valid, by default, for 7 days.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="gender">Customers gender</param>
        /// <param name="amount">Amount to be reserved, including VAT, in cents</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">PClass</param>
        /// <param name="enc">Which PNO encoding to be used</param>
        /// <returns>An array with a reservation number and order status</returns>
        /// <remarks>Activation must be done with activate_reservation, i.e. you cannot activate through Klarna Online.</remarks>
        private string[] ReserveAmount (string pno, API.Gender? gender, int amount, API.Flag flags, int pclass, API.Encoding enc)
        {
            string[] invoiceInfo = null;
            string digest = this.CreateDigest (this.eid + ":" + pno + ":" + amount + ":" + this.secret);

            object[] reservationData = (object[])CallKlarna (
                "reserve_amount",
                pno,
                (gender != null ? (object)(int)gender : (object)""),
                amount,
                this.reference,
                this.referenceCode,
                this.orderID1,
                this.orderID2,
                this.shippingaddr,
                this.billingaddr,
                this.ip,
                (int)flags,
                (int)this.currency,
                (int)this.country,
                (int)this.language,
                this.eid,
                digest,
                (int)enc,
                pclass,
                this.goodslist.GetGoodsList (),
                this.comment,
                ToXmlRpcStruct(this.ShipmentInfo),
                ToXmlRpcStruct(this.TravelInfo),
                ToXmlRpcStruct(this.IncomeExpense),
                ToXmlRpcStruct(this.BankInfo),
                ToXmlRpcStruct(this.sid),
                ToXmlRpcStruct(this.ExtraInfo)
            );


            invoiceInfo = new string[reservationData.Length];

            for (int i = 0; i < reservationData.Length; i++) {
                invoiceInfo [i] = reservationData [i].ToString ();

            }

            if (!this.keepData) {
                // Clear the goodslist so we do not add more
                // goods to the same goods list
                this.goodslist.Clear ();

                // Clear the addresses so we dont use them again by accident
                this.shippingaddr = null;
                this.billingaddr = null;
            }

            return invoiceInfo;
        }

        private string[] ReserveAmount (string pno, API.Gender? gender, int amount, API.Flag flags, int pclass)
        {
            return ReserveAmount (pno, gender, amount, flags, pclass, this.PNOEncoding);
        }

        /// <summary>
        /// Reserves a purchase amount for a specific customer.
        /// The reservation is valid, by default, for 7 days.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="gender">Customers gender</param>
        /// <param name="amount">Amount to be reserved, including VAT</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">PClass</param>
        /// <param name="enc">Which PNO encoding to be used</param>
        /// <returns>An array with a reservation number and order status</returns>
        /// <remarks>Activation must be done with activate_reservation, i.e. you cannot activate through Klarna Online.</remarks>
        public string[] ReserveAmount (string pno, API.Gender? gender, double amount, API.Flag flags, int pclass, API.Encoding enc)
        {
            int amountToReserve = (int)Math.Round ((double)amount * 100);

            return ReserveAmount (
                pno,
                gender,
                amountToReserve,
                flags,
                pclass,
                enc
            );
        }

        /// <summary>
        /// Reserves a purchase amount for a specific customer.
        /// The reservation is valid, by default, for 7 days.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="gender">Customers gender</param>
        /// <param name="amount">Amount to be reserved, including VAT</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">PClass</param>
        /// <returns>An array with a reservation number and order status</returns>
        /// <remarks>Activation must be done with activate_reservation, i.e. you cannot activate through Klarna Online.</remarks>
        public string[] ReserveAmount (string pno, API.Gender? gender, double amount, API.Flag flags, int pclass)
        {
            return ReserveAmount (
                pno,
                gender,
                amount,
                flags,
                pclass,
                this.PNOEncoding
            );
        }

        /// <summary>
        /// Reserves a purchase amount for a specific customer.
        /// The reservation is valid, by default, for 7 days.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="gender">Customers gender</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">PClass</param>
        /// <param name="enc">Which PNO encoding to be used</param>
        /// <returns>An array with a reservation number and order status</returns>
        /// <remarks>Activation must be done with activate_reservation, i.e. you cannot activate through Klarna Online.</remarks>
        /// <remarks>Total amount will be calcualted from goods list</remarks>
        public string[] ReserveAmount (string pno, API.Gender? gender, API.Flag flags, int pclass, API.Encoding enc)
        {
            int amount = 0;
            foreach (Goods g in this.goodslist.GetGoodsList()) {
                amount += g.Quantity * g.Article.Price;
            }

            return ReserveAmount (
                pno,
                gender,
                amount,
                flags,
                pclass
            );
        }

        /// <summary>
        /// Reserves a purchase amount for a specific customer.
        /// The reservation is valid, by default, for 7 days.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="gender">Customers gender</param>
        /// <param name="amount">Amount to be reserved, including VAT</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">PClass</param>
        /// <returns>An array with a reservation number and order status</returns>
        /// <remarks>Activation must be done with activate_reservation, i.e. you cannot activate through Klarna Online.</remarks>
        public string[] ReserveAmount (string pno, API.Gender? gender, API.Flag flags, int pclass)
        {
            return ReserveAmount (
                pno,
                gender,
                flags,
                pclass,
                this.PNOEncoding
            );
        }

        /// <summary>
        /// Activates a previously created reservation.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="reservationNumber">Reservation to be activated</param>
        /// <param name="gender">Gender of the customer</param>
        /// <param name="OCR">A OCR number</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">Pclass ID</param>
        /// <param name="enc">PNo encoding</param>
        /// <returns>An array with risk status and invoice number [string, string]</returns>
        public string[] ActivateReservation (string pno, string reservationNumber, API.Gender? gender, string OCR, Flag flags, int pclass, Encoding enc)
        {
            string digest = this.eid + ":" + pno + ":";
            foreach (Goods goods in this.goodslist.GetGoodsList()) {
                digest += goods.Article.ArticleNumber + ":" + goods.Quantity + ":";
            }

            digest = this.CreateDigest (digest + this.secret);

            string[] activationResult = (string[])CallKlarna (
                "activate_reservation",
                reservationNumber,
                OCR,
                pno,
                (gender != null ? (object)(int)gender : (object)""),
                this.reference,
                this.referenceCode,
                this.orderID1,
                this.orderID2,
                this.shippingaddr,
                this.billingaddr,
                this.ip,
                (int)flags,
                (int)this.currency,
                (int)this.country,
                (int)this.language,
                this.eid,
                digest,
                (int)enc,
                pclass,
                this.goodslist.GetGoodsList (),
                this.comment,
                ToXmlRpcStruct(this.ShipmentInfo),
                ToXmlRpcStruct(this.TravelInfo),
                ToXmlRpcStruct(this.IncomeExpense),
                ToXmlRpcStruct(this.BankInfo),
                ToXmlRpcStruct(this.ExtraInfo)
            );

            if (!this.keepData) {
                // Clear the goodslist
                this.goodslist.Clear ();

                // Clear the addresses so we dont use them again by accident
                this.shippingaddr = null;
                this.billingaddr = null;

            }

            return activationResult;
        }

        /// <summary>
        /// Activates a previously created reservation.
        /// </summary>
        /// <param name="pno">Personal number, SSN, date of birth, etc</param>
        /// <param name="reservationNumber">Reservation to be activated</param>
        /// <param name="gender">Gender of the customer</param>
        /// <param name="OCR">A OCR number</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <param name="pclass">Pclass ID</param>
        /// <returns>An array with risk status and invoice number [string, string]</returns>
        public string[] ActivateReservation (string pno, string reservationNumber, API.Gender? gender, string OCR, Flag flags, int pclass)
        {
            return ActivateReservation (pno, reservationNumber, gender, OCR, flags, pclass, this.PNOEncoding);
        }

        public bool CheckReservation (string pno, string reservationNumber, double amount)
        {
            int amountToReserve = (int)Math.Round (amount * 100.0);
            string digest = this.CreateDigest (this.eid + ":" + reservationNumber + ":" + amountToReserve + ":" + this.secret);

            string result = (string)CallKlarna ("check_reservation", this.eid, reservationNumber, amountToReserve, this.shippingaddr, digest, pno);

            return (result == "ok");

        }

        /// <summary>
        /// Cancels a reservation.
        /// </summary>
        /// <param name="reservationNumber">Reservation to be cancelled</param>
        /// <returns>True, if the cancellation was successful</returns>
        public bool CancelReservation (string reservationNumber)
        {
            string digest = this.CreateDigest (this.eid + ":" + reservationNumber + ":" + this.secret);

            bool isOK = ((string)CallKlarna ("cancel_reservation", reservationNumber, this.eid, digest) == "ok");

            return isOK;
        }

        /// <summary>
        /// Changes specified reservation to a new amount
        /// </summary>
        /// <param name="reservationNumber">Reservation to be changed</param>
        /// <param name="amount">The new amount incl. VAT</param>
        /// <param name="flags">Options which affect the behaviour</param>
        /// <returns>True, if the change was successful</returns>
        public bool ChangeReservation (string reservationNumber, double amount, Flag flags)
        {
            string digest = this.CreateDigest (this.eid + ":" + reservationNumber + ":" + (int)Math.Round (amount * 100) + ":" + this.secret);

            bool isOK = ((string)CallKlarna ("change_reservation", reservationNumber, (int)Math.Round (amount * 100), this.eid, digest, (int)flags) == "ok");

            return isOK;

        }

        /// <summary>
        /// Splits a reservation due to for example outstanding articles.
        /// </summary>
        /// <param name="reservationNumber">Reservation to split</param>
        /// <param name="amount">The amount to be subtracted from the reservation.</param>
        /// <param name="flags"></param>
        /// <returns>A new reservation number</returns>
        public object[] SplitReservation (string reservationNumber, double amount, Flag flags)
        {
            string digest = this.CreateDigest (this.eid + ":" + reservationNumber + ":" + (int)Math.Round (amount * 100) + ":" + this.secret);

            return (object[])CallKlarna (
                "split_reservation", reservationNumber, (int)Math.Round (amount * 100), this.orderID1, this.orderID2, (int)flags, this.eid, digest);
        }
    }
}
