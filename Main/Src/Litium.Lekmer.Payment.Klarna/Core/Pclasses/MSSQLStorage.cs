﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.SqlClient;

using Klarna.Core;

namespace Klarna.Core.PClasses
{
    class MSSQLStorage : PCStorage
    {
        /// <summary>
        /// Database name
        /// </summary>
        protected string dbname;

        /// <summary>
        /// Database table
        /// </summary>
        protected string dbtable;

        /// <summary>
        /// Database address
        /// </summary>
        protected string addr;

        /// <summary>
        /// Database username
        /// </summary>
        protected string user;

        /// <summary>
        /// Database password
        /// </summary>
        protected string passwd;

        /// <summary>
        /// MySQL DB link ressource
        /// </summary>
        protected SqlConnection link;

        /// <summary>
        /// Connects to the DB and checks if the DB and table exists
        /// </summary>
        protected void Connect()
        {
            try
            {
                this.link = new SqlConnection(
                    "user id=" + this.user + ";" +
                    "password=" + this.passwd + ";" +
                    "data source=" + this.addr + ";");
            }
            catch (Exception e)
            {
                throw new Exception("Failed to connect to database! (" + e.Message + ")");
            }

            string dbSQL = "CREATE DATABASE IF NOT EXISTS `" + this.dbname + "`";
            SqlCommand dbCom = new SqlCommand(dbSQL, this.link);
            try
            {
                this.link.Open();
                dbCom.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                //Try to close the connection
                if (this.link != null)
                {
                    this.link.Dispose();
                }
                throw new Exception("Database not existing, failed to create! {0}\n(" + e.Message + ")");
            }
            finally
            {
                this.link.Close();
            }


            string createSQL = "CREATE TABLE IF NOT EXISTS `" + this.dbname + "`.`" + this.dbtable + "` (" +
                    "`eid` int(10) unsigned NOT NULL," +
                    "`id` int(10) unsigned NOT NULL," +
                    "`type` tinyint(4) NOT NULL," +
                    "`desc` varchar(255) NOT NULL," +
                    "`months` int(11) NOT NULL," +
                    "`interestrate` int(11) NOT NULL," +
                    "`invoicefee` int(11) NOT NULL," +
                    "`startfee` int(11) NOT NULL," +
                    "`minamount` int(11) NOT NULL," +
                    "`country` int(11) NOT NULL," +
                    "`expire` int(11) NOT NULL," +
                    "KEY `id` (`id`)" +
                ")";
            SqlCommand createCom = new SqlCommand(createSQL, this.link);
            try
            {
                this.link.Open();
                createCom.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw new Exception("Table not existing, failed to create! {0}\n (" + e.Message + ")");
            }
            finally
            {
                this.link.Close();
                this.link.Dispose();
            }


        }

        /// <summary>
        /// Splits the URI in format: user:passwd@addr/dbname.dbtable
        /// </summary>
        /// <param name="uri"></param>
        protected void SplitURI(string uri)
        {
            string pattern = @"^([\w]+):([\w]+)@([\w\.]+|[\w\.]+:[\d]+)\/([\w]+).([\w]+)$";
            if (Regex.IsMatch(uri, pattern, RegexOptions.IgnoreCase))
            {
                string[] strings = Regex.Split(uri, pattern, RegexOptions.IgnoreCase);
                /*
                  [0] => __blank__
                  [1] => user
                  [2] => passwd
                  [3] => addr
                  [4] => dbName
                  [5] => dbTable
                  [6] => __blank__
                */
                if (strings.Length != 7)
                {
                    throw new Exception("URI is invalid! Missing field or invalid characters used!");
                }

                this.user = strings[1];
                this.passwd = strings[2];
                this.addr = strings[3];
                this.dbname = strings[4];
                this.dbtable = strings[5];
            }
            else
            {
                throw new Exception("URI to MySQL is not valid! ( user:passwd@addr/dbName.dbTable )");
            }
        }

        public override void Load(string uri)
        {
            try
            {
                this.SplitURI(uri);
                this.Connect();

                SqlDataReader reader = null;
                string selectSQL = "SELECT * FROM `" + this.dbname + "`.`" + this.dbtable + "`";
                SqlCommand selectCom = new SqlCommand(selectSQL, this.link);
                try
                {
                    this.link.Open();
                    reader = selectCom.ExecuteReader();
                    while (reader.Read())
                    {
                        PClass pclass = new PClass(
                            Convert.ToInt32(reader["eid"]),
                            Convert.ToInt32(reader["id"]),
                            Convert.ToString(reader["desc"]),
                            Convert.ToInt32(reader["months"]),
                            Convert.ToDouble(reader["startfee"]),
                            Convert.ToDouble(reader["invoicefee"]),
                            Convert.ToDouble(reader["interestrate"]),
                            Convert.ToDouble(reader["minamount"]),
                            (API.Country)reader["country"],
                            (PClass.PClassType)reader["type"],
                            Convert.ToDateTime(reader["expire"])
                        );
                        this.AddPClass(pclass);
                    }
                }
                catch (Exception e)
                {
                    //Try to close the connection
                    if (this.link != null)
                    {
                        this.link.Dispose();
                    }
                    throw new Exception("SELECT query failed! (" + e.Message + ")");
                }
                finally
                {
                    this.link.Close();
                }

            }
            catch (Exception e)
            {
                throw new KlarnaException("Error in __METHOD__ : " + e.Message);
            }

        }

        public override void Save(string uri)
        {
            try
            {
                this.SplitURI(uri);
                this.Clear(uri);
                this.Connect();

                foreach (KeyValuePair<int, List<PClass>> pclassList in this.pclasses)
                {
                    foreach (PClass pclass in pclassList.Value)
                    {
                        //Remove the pclass if it already exists.
                        string deleteSQL = "DELETE FROM  `" + this.dbname + "`.`" + this.dbtable + "` WHERE `id` = `" + pclass.PClassID + "` AND `eid` = `" + pclass.EID + "`";
                        SqlCommand deleteCom = new SqlCommand(deleteSQL, this.link);

                        try
                        {
                            this.link.Open();
                            deleteCom.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            throw new Exception("DELETE FROM query failed! (" + e.Message + ")");
                        }
                        finally
                        {
                            this.link.Close();
                        }

                        //Insert it (again)
                        string insertSQL = "INSERT INTO `" + this.dbname + "`.`" + this.dbtable + "`" +
                                "(`eid`, `id`, `type`, `desc`, `months`, `interestrate`, `invoicefee`, `startfee`, `minamount`, `country`, `expire`)" +
                                "VALUES" +
                                "('" + pclass.EID + "'," +
                                "'" + pclass.PClassID + "'," +
                                "'" + pclass.Type + "'," +
                                "'" + pclass.Description + "'," +
                                "'" + pclass.Months + "'," +
                                "'" + pclass.InterestRate + "'," +
                                "'" + pclass.InvoiceFee + "'," +
                                "'" + pclass.StartFee + "'," +
                                "'" + pclass.MinAmountForPClass + "'," +
                                "'" + pclass.Country + "'," +
                                "'" + pclass.ExpireDate + "')";
                        SqlCommand insertCom = new SqlCommand(insertSQL, this.link);

                        try
                        {
                            this.link.Open();
                            insertCom.ExecuteNonQuery();
                        }
                        catch (Exception e)
                        {
                            //Try to close the connection
                            if (this.link != null)
                            {
                                this.link.Dispose();
                            }
                            throw new Exception("INSERT INTO query failed! (" + e.Message + ")");
                        }
                        finally
                        {
                            this.link.Close();
                        }

                    }
                }
            }
            catch (Exception e)
            {
                //MySQLStorage error message __METHOD__
                throw new KlarnaException("Error: " + e.Message);
            }
        }

        public override void Clear(string uri)
        {
            try
            {
                this.SplitURI(uri);
                this.Connect();

                string dropSQL = "DROP TABLE `" + this.dbname + "`.`" + this.dbtable + "`";
                SqlCommand dropCom = new SqlCommand(dropSQL, this.link);
                try
                {

                    this.link.Open();
                    dropCom.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //Try to close the connection
                    if (this.link != null)
                    {
                        this.link.Dispose();
                    }
                    throw new KlarnaException("DROP TABLE query failed! (" + e.Message + ")");
                }
                finally
                {
                    this.link.Close();
                }
            }
            catch(Exception e)
            {
                throw new KlarnaException("Error in __METHOD__ " + e.Message);
            }
        }

    }
}
