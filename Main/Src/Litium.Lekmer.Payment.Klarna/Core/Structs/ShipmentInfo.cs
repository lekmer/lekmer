/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using CookComputing.XmlRpc;

namespace Klarna.Core.Structs
{
    public class ShipmentInfo : XmlRpcStruct
    {
        public override void Add(object key, object value)
        {
            base.Add(key, value);
        }

        public override object this[object key]
        {
            get
            {
                return base[key];
            }
            set
            {
                base[key] = value;
            }
        }

        public int shipmentType {
            get {
                return (int) this["delay_adjust"];
            }
            set {
                Remove("delay_adjust");
                this["delay_adjust"] = value;
            }
        }

        public ShipmentInfo ()
        {
            this["delay_adjust"] = (int) API.ShipmentType.NormalShipment;
        }

        public ShipmentInfo (API.ShipmentType type)
        {
            this["delay_adjust"] = (int) type;
        }

        public override bool Equals (object obj)
        {
            ShipmentInfo s;
            try {
                s = (ShipmentInfo) obj;
            } catch (InvalidCastException e) {
                return false;
            }

            if (s.Keys.Count != this.Keys.Count) {
                return false;
            }

            foreach (DictionaryEntry v in this) {
                if (!s.ContainsKey(v.Key)) {
                    return false;
                }
                if (s[v.Key] != v.Value) {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode ()
        {
            return this.shipmentType;
        }
    }
}
