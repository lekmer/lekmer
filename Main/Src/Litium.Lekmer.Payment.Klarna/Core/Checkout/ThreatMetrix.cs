﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace Klarna.Core.Checkout
{
    public class ThreatMetrix
    {
        private string orgID = "qicrzsu4";
        private string sessionID;
        private string host = "h.online-metrix.net";
        private string proto = "https";

        public string SessionID { get { return this.sessionID; } }


        public ThreatMetrix(int eid)
        {
            // Generate the session ID
            this.sessionID = this.GenerateSessionID(eid);
        }

        /// <summary>
        /// Generates the session ID to be used with Threat metrix
        /// </summary>
        /// <param name="eid">Estore ID (eid)</param>
        /// <returns>Session ID to be used with TM</returns>
        private string GenerateSessionID(int eid)
        {
            string sessionID = "";
            string eidString = eid.ToString();
            // Create a "Randomizer"
            Random random = new Random(eid);

            // Pad the sessionID with zeros (0)
            while (eidString.Length < 10)
            {
                eidString += "0";
            }

            long milliSeconds = DateTime.Now.Ticks;

            sessionID = milliSeconds.ToString();
            // Make sure we do not start with zero (0)
            sessionID = sessionID.Insert(0, random.Next(1, 9).ToString());

            while (sessionID.Length < 30)
            {
                sessionID += random.Next(0, 9999).ToString();
            }

            // Substring to make sure sessionID isnt longer than 30 characters
            sessionID = sessionID.Substring(0, 30) + eidString;

            return sessionID;
        }

        public string ToHtml()
        {
                return "<p style=\"background:url(" + this.proto + "://" + this.host + "/fp/clear.png?org_id=" + this.orgID + "&session_id=" + this.sessionID + "&m=1)\"></p><script src=\"" + this.proto + "://" + this.host + "/fp/check.js?org_id=" + this.orgID + "&session_id=" + this.sessionID + "\" type=\"text/javascript\"></script><img src=\"" + this.proto + "://" + this.host + "/fp/clear.png?org_id="+  this.orgID + "&session_id=" +this.sessionID + "&m=2\" alt=\"\" ><object type=\"application/x-shockwave-flash\" data=\"" + this.proto + "://" + this.host + "/fp/fp.swf?org_id=" + this.orgID +"&session_id=" +this.sessionID + "\" width=\"1\" height=\"1\" id=\"obj_id\"><param name=\"movie\" value=\"" + this.proto + "://" + this.host + "/fp/fp.swf?org_id=" + this.orgID + "&session_id=" + this.sessionID + "\" /><div></div></object>";
        }

    }
}
