﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.ProductFilter.Mapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter.Repository
{
	public class BlockProductFilterRepository
	{
		protected virtual DataMapperBase<IBlockProductFilter> CreateDataMapper(IDataReader dataReader)
		{
			return new BlockProductFilterDataMapper(dataReader);
		}

		public IBlockProductFilter GetByIdSecure(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.GetByIdSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockProductFilterGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<int> GetAllBrandsByBlock(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.GetAllBrandsByBlock");
			IDataParameter[] parameters = 
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			var brandIds = new Collection<int>();
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockProductFilterBrandGetAllByBlock]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}
			return brandIds;
		}

		public virtual Collection<int> GetAllTagsByBlock(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.GetAllBrandsByBlock");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			var tagIds = new Collection<int>();
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockProductFilterTagGetAllByBlock]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					tagIds.Add(dataReader.GetInt32(0));
				}
			}
			return tagIds;
		}

		public virtual void Delete(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.Delete");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductFilterDelete]", parameters, dbSettings);
		}

		public virtual void DeleteAllBrands(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.DeleteAllBrands");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductFilterDeleteAllBrands]", parameters, dbSettings);
		}

		public virtual void DeleteAllTags(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.DeleteAllTags");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductFilterDeleteAllTags]", parameters, dbSettings);
		}

		public virtual void DeleteTag(int tagId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.DeleteTag");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@TagId", tagId, SqlDbType.Int)
			};

			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductFilterTagDelete]", parameters, dbSettings);
		}

		public virtual int Save(IBlockProductFilter blockProductFilter)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockProductFilter.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SecondaryTemplateId", blockProductFilter.SecondaryTemplateId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DefaultCategoryId", blockProductFilter.DefaultCategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DefaultPriceIntervalId", blockProductFilter.DefaultPriceIntervalId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DefaultAgeIntervalId", blockProductFilter.DefaultAgeIntervalId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DefaultSizeId", blockProductFilter.DefaultSizeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductListCookie", blockProductFilter.ProductListCookie, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@DefaultSort", blockProductFilter.DefaultSort, SqlDbType.NVarChar)
				};
			var blockProductFilterId = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pBlockProductFilterSave]", parameters, dbSettings);
			return blockProductFilterId;
		}

		public virtual void SaveBrand(int blockId, int brandId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.SaveBrand");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BrandId", brandId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductFilterSaveBrand]", parameters, dbSettings);
		}

		public virtual void SaveTag(int blockId, int tagId)
		{
			var dbSettings = new DatabaseSetting("BlockProductFilterRepository.SaveTag");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@TagId", tagId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pBlockProductFilterSaveTag]", parameters, dbSettings);
		}

		public virtual IBlockProductFilter GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect(
					"[lekmer].[pBlockProductFilterGetById]",
					parameters,
					new DatabaseSetting("BlockProductFilterRepository.GetById")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}
