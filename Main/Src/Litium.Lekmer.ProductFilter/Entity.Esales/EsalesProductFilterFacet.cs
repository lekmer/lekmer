using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter
{
	public class EsalesProductFilterFacet : IEsalesProductFilterFacet
	{
		public EsalesProductFilterFacet()
		{
			MainCategories = new CategoryCollection();
			MainCategoryStats = new Dictionary<int, int>();

			ParentCategories = new CategoryCollection();
			ParentCategoryStats = new Dictionary<int, int>();

			Categories = new CategoryCollection();
			CategoryStats = new Dictionary<int, int>();

			Brands = new BrandCollection();
			BrandStats = new Dictionary<int, int>();

			AgeInterval = IoC.Resolve<IAgeIntervalBand>();
			PriceInterval = IoC.Resolve<IPriceIntervalBand>();

			Sizes = new Collection<ISize>();
			SizeStats = new Dictionary<int, int>();

			TagStats = new Dictionary<int, int>();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CategoryCollection MainCategories { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> MainCategoryStats { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CategoryCollection ParentCategories { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> ParentCategoryStats { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CategoryCollection Categories { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> CategoryStats { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public BrandCollection Brands { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> BrandStats { get; set; }

		public IAgeIntervalBand AgeInterval { get; set; }
		public IPriceIntervalBand PriceInterval { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ISize> Sizes { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> SizeStats { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> TagStats { get; set; }
	}
}