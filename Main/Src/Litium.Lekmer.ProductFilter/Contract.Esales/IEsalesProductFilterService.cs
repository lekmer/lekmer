using Litium.Lekmer.Esales;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter
{
	public interface IEsalesProductFilterService
	{
		ProductCollection GetProducts(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent);
		IEsalesProductFilterFacet GetBlockFilterFacet(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent);
		IEsalesProductFilterFacet GetUserFilterFacet(IUserContext context, IEsalesResponse esalesResponse, IEsalesModelComponent esalesModelComponent);
	}
}