using System;
using System.ComponentModel;
using System.Timers;
using Litium.Lekmer.ProductFilter.Setting;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter.Cache
{
	internal class CacheRenewWorker
	{
		private BackgroundWorker _worker;
		private Timer _timer;

		internal IUserContext UserContext { get; set; }

		internal static TimeSpan RefreshIntervalInSeconds
		{
			get { return ProductFilterCacheSetting.Instance.ProductFilterCacheRefreshInterval; }
		}

		private void OnRenew(object sender, DoWorkEventArgs e)
		{
			FilterProductStickyCache.RenewCache(UserContext);
		}

		#region Background worker

		internal void Start()
		{
			if (UserContext == null)
			{
				throw new InvalidOperationException("UserContext property was not set.");
			}

			if (_worker == null)
			{
				_worker = CreateBackgroundWorker();
			}

			if (_timer == null)
			{
				_timer = CreateTimer();
			}

			_timer.Start();
		}

		private BackgroundWorker CreateBackgroundWorker()
		{
			var worker = new BackgroundWorker();
			worker.DoWork += OnRenew;

			return worker;
		}

		private Timer CreateTimer()
		{
			var timer = new Timer();

			timer.Elapsed += OnTimerElapsed;
			timer.Interval = RefreshIntervalInSeconds.TotalMilliseconds;
			timer.Stop();

			return timer;
		}

		private void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			if (!_worker.IsBusy)
			{
				_worker.RunWorkerAsync();
			}
		}

		#endregion
	}
}