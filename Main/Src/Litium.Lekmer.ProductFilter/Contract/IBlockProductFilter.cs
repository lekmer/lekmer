﻿using Litium.Lekmer.RatingReview;
using Litium.Lekmer.SiteStructure;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.ProductFilter
{
	public interface IBlockProductFilter : ILekmerBlock
	{
		int? SecondaryTemplateId { get; set; }
		int? DefaultCategoryId { get; set; }
		int? DefaultPriceIntervalId { get; set; }
		int? DefaultAgeIntervalId { get; set; }
		int? DefaultSizeId { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<int> DefaultBrandIds { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<int> DefaultTagIds { get; set; }
		string ProductListCookie { get; set; }
		string DefaultSort { get; set; }
		IBlockRating BlockRating { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<int> TargetProductTypeIds { get; set; }
	}
}