using System;

namespace Litium.Lekmer.Voucher
{
	[Serializable]
	public enum VoucherValueType
	{
		Unknown = 0,
		Percentage = 1,
		Price = 2,
		GiftCard = 3,
		Code = 4
	}
}