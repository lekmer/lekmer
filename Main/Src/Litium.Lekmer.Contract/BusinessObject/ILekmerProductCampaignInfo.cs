using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerProductCampaignInfo : IProductCampaignInfo
	{
		new Collection<IProductActionAppliedItem> CampaignActionsApplied { get; }

		/// <summary>
		/// Action that affect price to set the minimum price of product.
		/// </summary>
		IProductActionAppliedItem AffectedPriceAction { get; set; }

		/// <summary>
		/// Set new AffectedPriceAction and remove old one from applied action list.
		/// </summary>
		/// <param name="newAffectedPriceAction">New action that affect price to set the minimum price of product.</param>
		void UpdateAppliedActions(IProductAction newAffectedPriceAction);
	}
}