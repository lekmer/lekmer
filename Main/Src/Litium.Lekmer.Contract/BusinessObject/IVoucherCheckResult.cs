namespace Litium.Lekmer.Voucher
{
	public interface IVoucherCheckResult
	{
		string VoucherCode { get; set; }
		VoucherValueType ValueType { get; set; }
		bool IsValid { get; set; }
		int? VoucherbatchId { get; set; }
		decimal DiscountValue { get; set; }
		decimal AmountLeft { get; set; }
		bool SpecialOffer { get; set; }
	}
}
