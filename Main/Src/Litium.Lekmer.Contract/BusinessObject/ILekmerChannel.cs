﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ILekmerChannel : IChannel
	{
		IChannelFormatInfo FormatInfo { get; set; }
		string ErpId { get; set; }
		decimal? VatPercentage { get; set; }
	}
}
