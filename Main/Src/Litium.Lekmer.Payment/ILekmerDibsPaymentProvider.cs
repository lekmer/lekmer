﻿using Litium.Scensum.Order.Payment;

namespace Litium.Lekmer.Payment
{
	public interface ILekmerDibsPaymentProvider : IPaymentProvider
	{
		bool CaptureIsAllowed(string channelCommonName, string paymentType);
		void InitializeSettings(string channelCommonName, string paymentType);
	}
}