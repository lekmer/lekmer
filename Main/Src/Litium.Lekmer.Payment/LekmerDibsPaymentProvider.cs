﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Payment.Dibs;

namespace Litium.Lekmer.Payment
{
	public class LekmerDibsPaymentProvider : DibsPaymentProvider, ILekmerDibsPaymentProvider
	{
		protected readonly string _returnUrlParameters = "?reply=[ver reply]&replyText=[ver reply text]&verifyid=[ver id no]&referenceNo=[ver valueof=referenceNo]&invoiceNo=[ver valueof=invoiceNo]&orderno=[ver valueof=orderNo]&sum=[ver sum]&currency=[ver valueof=currency]&[ver MAC]";

		protected ILekmerDibsSetting DibsSetting { get; private set; }

		public LekmerDibsPaymentProvider(
			IChannelService channelService,
			ICountryService countryService,
			ILekmerDibsSetting dibsSetting)
			: base(channelService, countryService)
		{
			DibsSetting = dibsSetting;
		}

		public virtual bool CaptureIsAllowed(string channelCommonName, string paymentType)
		{
			DibsSetting.Initialize(channelCommonName, paymentType);

			return DibsSetting.AllowCapture;
		}

		public virtual void InitializeSettings(string channelCommonName, string paymentType)
		{
			DibsSetting.Initialize(channelCommonName, paymentType);
		}

		protected override bool AuthenticateTransaction(string channelCommonName, NameValueCollection providerResponse)
		{
			var sum = GetValueFromCollection("sum", providerResponse);
			var currency = GetValueFromCollection("currency", providerResponse);
			var reply = GetValueFromCollection("reply", providerResponse);
			var verifyId = GetValueFromCollection("verifyId", providerResponse);

			string referenceData = GetReferenceData(providerResponse);

			var responseMac = GetValueFromCollection("MAC", providerResponse);
			var mac = ParseResponseMac(channelCommonName, sum, currency, reply, verifyId, referenceData);
			return mac.Equals(responseMac, StringComparison.CurrentCultureIgnoreCase);
		}

		/// <summary>
		/// ReferenceData should have the value of the most significant parameter in the list:
		///  1. referenceNo
		///  2. invoiceNo
		///  3. orderNo.
		/// </summary>
		protected virtual string GetReferenceData(NameValueCollection providerResponse)
		{
			var referenceNo = providerResponse["referenceNo"];
			if (referenceNo.HasValue())
			{
				return referenceNo;
			}

			var invoiceNo = providerResponse["invoiceNo"];
			if (invoiceNo.HasValue())
			{
				return invoiceNo;
			}

			var orderNo = GetValueFromCollection("orderNo", providerResponse);
			return orderNo;
		}

		protected override decimal GetOrderPrice(IOrderFull order)
		{
			Price price = order.GetActualPriceSummary();
			decimal priceFreight = ((ILekmerOrderFull)order).GetTotalFreightCost();
			var priceIncludingVat = price.IncludingVat + priceFreight;
			var adjustedPriceIncludingVat = priceIncludingVat * 100;
			return adjustedPriceIncludingVat;
		}

		protected override string CreateServiceUrl(
			ICustomerInformation customer,
			IOrderFull order,
			ICurrency currency,
			IPaymentType paymentType)
		{
			var channel = ChannelService.GetById(order.ChannelId);

			DibsSetting.Initialize(channel.CommonName, paymentType.CommonName);

			var url = new ServiceUrl(DibsSetting.ServiceUrl);
			url.Add("merchant", DibsSetting.MerchantId);
			url.AddEncoded("ResponseUrl", DibsSetting.ResponseUrl);
			url.Add("pageSet", DibsSetting.PageSet);
			url.Add("method", DibsSetting.AquirerName);
			url.Add("currency", currency.Iso);
			url.AddEncoded("billingFirstName", customer.FirstName);
			url.AddEncoded("billingLastName", customer.LastName);
			ICountry country = CountryService.GetById(customer.DefaultBillingAddress.CountryId);
			url.AddEncoded("billingCountry", country.Iso);
			url.AddEncoded("billingAddress", customer.DefaultBillingAddress.StreetAddress);
			url.AddEncoded("billingCity", customer.DefaultBillingAddress.City);
			url.AddEncoded("eMail", customer.Email);
			url.Add("orderNo", order.Id);
			url.Add("customerNo", customer.Id);

			var orderDataFormatString = DibsSetting.DataFormatString;
			var orderData = string.Format(orderDataFormatString, GetOrderPrice(order));
			url.Add("data", orderData);

			if (DibsSetting.AuthorizeOnly && OrderRequireCapture(order) == false)
			{
				url.Add("authOnly", "true");
			}

			if (DibsSetting.TestCode > 0)
			{
				url.Add("Test", DibsSetting.TestCode);
			}

			url.Add("uses3dsecure", DibsSetting.Use3DSecure);
			url.Add("MAC", ParseRequestMac(channel.CommonName, orderData, currency.Iso, order.Id.ToString(CultureInfo.InvariantCulture)));
			url.AddEncoded("returnUrl", string.Format("{0}{1}", DibsSetting.ResponseUrl, _returnUrlParameters));

			return url.ToString();
		}

		protected override string ParseRequestMac(
			string channelCommonName,
			string orderData,
			string currency,
			string orderNumber)
		{
			var text = new StringBuilder(DibsSetting.MacOrderIncoming);

			text.Replace(":", "&");
			text.Replace("[data]", orderData);
			text.Replace("[currency]", currency);
			text.Replace("[method]", DibsSetting.AquirerName);
			text.Replace("[orderNo]", orderNumber);
			text.Replace("[SecretKey]", DibsSetting.SecretKey);

			var hash = CalculateSha1(text.ToString(), Encoding.GetEncoding(DibsSetting.EncodingIso));

			return hash;
		}


		protected override string ParseResponseMac(
			string channelCommonName,
			string sum,
			string currency,
			string reply,
			string verifyId,
			string orderNumber)
		{
			var text = string.Format(
				CultureInfo.InvariantCulture,
				"{0}&{1}&{2}&{3}&{4}&{5}&",
				sum,
				currency,
				reply,
				verifyId,
				orderNumber,
				DibsSetting.SecretKey);
			var hash = CalculateSha1(text, Encoding.GetEncoding(DibsSetting.EncodingIso));
			return hash;
		}

		protected virtual bool OrderRequireCapture(IOrderFull order)
		{
			var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => p.PaymentTypeId == (int)PaymentMethodType.DibsCreditCard);

			if (orderPayment != null)
			{
				return orderPayment.Captured ?? false;
			}

			return false;
		}
	}

	internal class ServiceUrl
	{
		private readonly StringBuilder _url = new StringBuilder();

		/// <summary>
		/// Initializes an instance of ServiceUrl.
		/// </summary>
		/// <param name="urlBase">An url without the QueryString part and without a question mark.</param>
		public ServiceUrl(string urlBase)
		{
			_url.Append(urlBase);
			_url.Append("?");
		}

		/// <summary>
		/// Adds a QueryString variable to the url.
		/// </summary>
		/// <param name="name">The name of the QueryString variable.</param>
		/// <param name="value">The value of the QueryString variable.</param>
		public void Add(string name, object value)
		{
			_url.AppendFormat(CultureInfo.CurrentCulture, "{0}={1}&", name, value);
		}

		/// <summary>
		/// Adds a QueryString variable with a url encoded value to the url.
		/// </summary>
		/// <param name="name">The name of the QueryString variable.</param>
		/// <param name="value">The value of the QueryString variable.</param>
		public void AddEncoded(string name, string value)
		{
			Add(name, HttpUtility.UrlEncode(value, Encoding.GetEncoding("iso-8859-1")));
		}

		public override string ToString()
		{
			return _url.ToString().TrimEnd('&');
		}
	}
}