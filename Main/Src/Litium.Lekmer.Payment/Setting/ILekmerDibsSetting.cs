﻿namespace Litium.Lekmer.Payment.Setting
{
	public interface ILekmerDibsSetting
	{
		string EncodingIso { get; }
		string ResponseUrl { get; }
		string DataFormatString { get; }
		bool AuthorizeOnly { get; }
		int TestCode { get; }
		bool Use3DSecure { get; }
		string SecretKey { get; }
		string ServiceUrl { get; }
		string PageSet { get; }
		string AquirerName { get; }
		string MerchantId { get; }
		string MacOrderIncoming { get; }
		bool AllowCapture { get; }

		void Initialize(string channelCommonName, string paymentType);
	}
}