﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DataExport.Mapper
{
	public class ProductPriceInfoDataMapper : DataMapperBase<IProductPriceInfo>
	{
		public ProductPriceInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductPriceInfo Create()
		{
			var productPriceInfo = IoC.Resolve<IProductPriceInfo>();

			productPriceInfo.ChannelId = MapValue<int>("ProductPrice.ChannelId");
			productPriceInfo.ProductId = MapValue<int>("ProductPrice.ProductId");

			productPriceInfo.PriceIncludingVat = MapValue<decimal>("ProductPrice.PriceIncludingVat");
			productPriceInfo.PriceExcludingVat = MapValue<decimal>("ProductPrice.PriceExcludingVat");

			productPriceInfo.DiscountPriceIncludingVat = MapNullableValue<decimal?>("ProductPrice.DiscountPriceIncludingVat");
			productPriceInfo.DiscountPriceExcludingVat = MapNullableValue<decimal?>("ProductPrice.DiscountPriceExcludingVat");

			productPriceInfo.SetUntouched();

			return productPriceInfo;
		}
	}
}