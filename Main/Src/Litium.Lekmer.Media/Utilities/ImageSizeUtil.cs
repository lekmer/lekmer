﻿using System;
using System.Globalization;
using Litium.Framework.Image;
using Litium.Lekmer.Common;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public class ImageSizeUtil
	{
		public static string GetHeight(IImage image, string sizeCommonName)
		{
			if (image == null) throw new ArgumentNullException("image");

			Size imageSize = GetSize(image, sizeCommonName);
			return imageSize.Height.ToString(CultureInfo.InstalledUICulture);
		}

		public static string GetWidth(IImage image, string sizeCommonName)
		{
			if (image == null) throw new ArgumentNullException("image");

			Size imageSize = GetSize(image, sizeCommonName);
			return imageSize.Width.ToString(CultureInfo.InstalledUICulture);
		}

		private static Size GetSize(IImage image, string sizeCommonName)
		{
			var commonSession = IoC.Resolve<ICommonSession>();
			var imageSizeService = (ILekmerImageSizeService)IoC.Resolve<IImageSizeService>();

			IImageSize targeImageSize = imageSizeService.GetByCommonName(sizeCommonName, commonSession.IsViewportMobile);
			if(targeImageSize == null)
			{
				return new Size(image.Width, image.Height);
			}

			var originSize = new Size(image.Width, image.Height);
			var targetSize = new Size(targeImageSize.Width, targeImageSize.Height);

			Size newSize = originSize.ProportionalResize(targetSize);

			return newSize;
		}
	}
}
