﻿using System.Data;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Mapper;

namespace Litium.Lekmer.Media.Mapper
{
	public class LekmerImageSizeDataMapper : ImageSizeDataMapper
	{
		public LekmerImageSizeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IImageSize Create()
		{
			var imageSize = (ILekmerImageSize)base.Create();

			imageSize.MobileWidth = MapNullableValue<int?>("ImageSize.MobileWidth");
			imageSize.MobileHeight = MapNullableValue<int?>("ImageSize.MobileHeight");
			imageSize.MobileQuality = MapNullableValue<int?>("ImageSize.MobileQuality");

			imageSize.SetUntouched();

			return imageSize;
		}
	}
}
