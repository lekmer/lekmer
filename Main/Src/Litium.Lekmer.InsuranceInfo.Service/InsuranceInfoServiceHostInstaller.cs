﻿using System.ComponentModel;
using System.Configuration.Install;

namespace Litium.Lekmer.InsuranceInfo.Service
{
	[RunInstaller(true)]
	public partial class InsuranceInfoServiceHostInstaller : Installer
	{
		public InsuranceInfoServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}
