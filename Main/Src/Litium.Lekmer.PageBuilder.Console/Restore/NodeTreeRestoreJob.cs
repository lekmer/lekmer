using System;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class NodeTreeRestoreJob : JobBase
	{
		private readonly ISystemUserFull _systemUser;

		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
		private readonly ISiteStructureRegistrySecureService _siteStructureRegistrySecureService = IoC.Resolve<ISiteStructureRegistrySecureService>();

		public NodeTreeRestoreJob(Configuration configuration, ISystemUserFull systemUser)
			: base(configuration)
		{
			_systemUser = systemUser;
		}

		protected override void ExecuteCore()
		{
			Console.WriteLine("NodeTreeRestoreJob running)");

			if (Configuration.ReplicateConfiguration == null) return;

			ISiteStructureRegistry siteStructureRegistry = _siteStructureRegistrySecureService.GetByChannel(Configuration.ChannelId);

			var contentNodes = _contentNodeSecureService.GetAllByRegistry(siteStructureRegistry.Id);
			int countBefore = contentNodes.Count;
			int countAfter = 0;

			while (contentNodes.Count > 0 && countBefore != countAfter)
			{
				countBefore = contentNodes.Count;

				foreach (IContentNode contentNode in contentNodes)
				{
					try
					{
						IoC.Resolve<IContentNodeDeleteSecureService>(contentNode.ContentNodeType.CommonName).Delete(_systemUser, contentNode.Id);
					}
					catch {/**/}
				}

				contentNodes = _contentNodeSecureService.GetAllByRegistry(siteStructureRegistry.Id);

				countAfter = contentNodes.Count;
			}

			if (contentNodes.Count > 0)
			{
				Console.WriteLine("--Warning, some nodes are not removed, try to do that manually !!!");
			}
		}
	}
}