﻿using System;
using System.Diagnostics;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.PageBuilder
{
	internal class Program
	{
		private static void Main()
		{
			Init();

			var sw = Stopwatch.StartNew();
			IoC.Resolve<IChannelSecureService>();
			sw.Stop();
			Console.WriteLine("IoC:" + sw.ElapsedMilliseconds);

			var configurations = ConfigurationFactory.GetConfigurations();
			Console.WriteLine("? (replicate|[build]|[update]|restore|[producturl])");

			while (true)
			{
				string input = Console.ReadLine() ?? string.Empty;

				sw.Reset();
				sw.Start();

				if (input.Equals("replicate", StringComparison.OrdinalIgnoreCase))
				{
					//Console.WriteLine("Replicate disabled)");

					foreach (Configuration configuration in configurations)
					{
						var replicateJob = new ReplicateJob(configuration);
						replicateJob.Execute();
					}
				}
				else if (input.Equals("build", StringComparison.OrdinalIgnoreCase))
				{
					Console.WriteLine("Build disabled)");

					//foreach (Configuration configuration in configurations)
					//{
					//	var buildJob = new BuildJob(configuration);
					//	buildJob.Execute();
					//}
				}
				else if (input.Equals("update", StringComparison.OrdinalIgnoreCase))
				{
					Console.WriteLine("Update disabled)");

					//foreach (Configuration configuration in configurations)
					//{
					//	var buildJob = new UpdateJob(configuration);
					//	buildJob.Execute();
					//}
				}
				else if (input.Equals("restore", StringComparison.OrdinalIgnoreCase))
				{
					foreach (Configuration configuration in configurations)
					{
						var restoreJob = new RestoreJob(configuration);
						restoreJob.Execute();
					}
				}
				else if (input.Equals("producturl", StringComparison.OrdinalIgnoreCase))
				{
					Console.WriteLine("Producturl disabled)");

					//var productUrlJob = new ProductUrlJob();
					//productUrlJob.Execute();
				}
				else
				{
					break;
				}

				sw.Stop();

				Console.WriteLine("Done:" + sw.ElapsedMilliseconds);
			}
		}

		private static void Init()
		{

			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();
#if DEBUG
			CacheUpdateCleaner.WorkerCleanInterval = 1000; // Clean interval lowered in debug mode.
#endif
			CacheUpdateCleaner.StartWorker();
		}
	}
}