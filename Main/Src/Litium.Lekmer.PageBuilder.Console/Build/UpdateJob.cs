using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class UpdateJob : JobBase
	{
		private readonly IChannelSecureService _channelSecureService =
			IoC.Resolve<IChannelSecureService>();

		private readonly ISiteStructureRegistrySecureService _siteStructureRegistrySecureService =
			IoC.Resolve<ISiteStructureRegistrySecureService>();

		private readonly ISystemUserSecureService _systemUserSecureService =
			IoC.Resolve<ISystemUserSecureService>();

		private int _languageId;
		private int _siteStructureRegistryId;
		private ISystemUserFull _systemUser;

		public UpdateJob(Configuration configuration)
			: base(configuration)
		{
		}

		protected override void ExecuteCore()
		{
			var categoryUpdateJob = new CategoryUpdateJob(Configuration, _systemUser, _languageId, _siteStructureRegistryId);
			categoryUpdateJob.Execute();
		}

		internal override void Initialize()
		{
			IChannel channel = _channelSecureService.GetById(Configuration.ChannelId);
			_languageId = channel.Language.Id;

			ISiteStructureRegistry siteStructureRegistry =
				_siteStructureRegistrySecureService.GetByChannel(Configuration.ChannelId);
			_siteStructureRegistryId = siteStructureRegistry.Id;

			_systemUser = _systemUserSecureService.GetFullByUserName(Configuration.SystemUserName);
		}
	}
}