using System.Collections.Generic;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.TopList;

namespace Litium.Lekmer.PageBuilder
{
	internal class BrandConfiguration
	{
		public int ParentContentNodeId { get; set; }
		public int MasterPageId { get; set; }

		public IBlockProductFilter FilterBlockTemplate { get; set; }

		public IBlockBrandList BrandListBlockTemplate { get; set; }

		public IBlock MenuBlockTemplate { get; set; }

		public IBlockTopList TopListBlockTemplate { get; set; }

		public BlockBrandList BrandListBlock2Template { get; set; }

		public LekmerBlockRichText RichTextBlockTemplate { get; set; }

		public IEnumerable<BrandSubPageConfiguration> SubPageConfigurations { get; set; }
	}
}