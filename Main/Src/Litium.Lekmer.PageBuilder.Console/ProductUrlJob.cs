using System.Data;
using System.Text.RegularExpressions;
using Litium.Framework.DataAccess;
using Litium.Lekmer.Product.Cache;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Cache;

namespace Litium.Lekmer.PageBuilder
{
	internal class ProductUrlJob
	{
		private readonly DataHandler _dataHandler = new DataHandler();
		private readonly DatabaseSetting _databaseSetting = new DatabaseSetting();

		public void Execute()
		{
			const int channelId = 1;
			GenerateUrls(channelId);
		}

		private void GenerateUrls(int channelId)
		{
			int languageId;
			using (
				var reader = _dataHandler.ExecuteSqlSelect("select LanguageId from core.tChannel where ChannelId = " + channelId,
				                                           _databaseSetting))
			{
				reader.Read();
				languageId = reader.GetInt32(0);
			}

			_dataHandler.ExecuteSqlCommand("delete from lekmer.tProductUrl where LanguageId = " + languageId, _databaseSetting);

			DataTable dt;
			string sql =
				string.Format(
					@"
select
	p.ProductId,
	coalesce(pt.Title, p.Title) as Title,
	coalesce(pt.WebShopTitle, p.WebShopTitle) as WebShopTitle
from
	product.tProduct p
	left outer join product.tProductTranslation pt
		on p.ProductId = pt.ProductId
		and pt.LanguageId = {0}
order by
	p.ProductId",
					languageId);
			using (IDataReader reader = _dataHandler.ExecuteSqlSelect(sql, _databaseSetting))
			{
				dt = new DataTable();
				dt.Load(reader);
			}

			foreach (DataRow row in dt.Rows)
			{
				var productId = (int) row["ProductId"];
				var title = (string) row["Title"];
				var webShopTitle = row["WebShopTitle"] as string;

				string urlTitle = title;
				if (!webShopTitle.IsNullOrTrimmedEmpty())
				{
					urlTitle = webShopTitle;
				}

				urlTitle = CleanUpUrlTitle(urlTitle);
				if (urlTitle.IsNullOrTrimmedEmpty())
				{
					urlTitle = productId.ToString();
				}

				string urlTitleToInsert = urlTitle;

				int i = 0;
				while (UrlTitleExists(languageId, urlTitleToInsert))
				{
					i++;
					urlTitleToInsert = urlTitle + " " + i;
				}

				string sqlInsert = string.Format(
					@"
insert into lekmer.tProductUrl(ProductId, LanguageId, UrlTitle)
values({0}, {1}, '{2}')"
					, productId, languageId, urlTitleToInsert);
				_dataHandler.ExecuteSqlCommand(sqlInsert, _databaseSetting);
			}

			dt.Dispose();

			ProductCache.Instance.Flush();
			ProductViewCache.Instance.Flush();
			ProductUrlIdCache.Instance.Flush();
		}

		private bool UrlTitleExists(int languageId, string urlTitle)
		{
			string sql = string.Format(
				@"
select ProductId from lekmer.tProductUrl
where LanguageId = {0} and UrlTitle = '{1}'",
				languageId, urlTitle);
			using (var reader = _dataHandler.ExecuteSqlSelect(sql, _databaseSetting))
			{
				return reader.Read();
			}
		}

		private static string CleanUpUrlTitle(string urlTitle)
		{
			urlTitle = Regex.Replace(urlTitle, @"[^\w\s�����]", " ");
			urlTitle = Regex.Replace(urlTitle, @"\s{2,}", " ");
			urlTitle = Regex.Replace(urlTitle, @"\s$", "");
			urlTitle = Regex.Replace(urlTitle, @"^\s", "");
			return urlTitle;
		}
	}
}