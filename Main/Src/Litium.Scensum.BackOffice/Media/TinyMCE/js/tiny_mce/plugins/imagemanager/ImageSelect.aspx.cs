using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager
{
	public partial class ImageSelect : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			imageSelect.Selected += ImageSelectImageSelection;
		}

		private void ImageSelectImageSelection(object sender, ImageSelectEventArgs e)
		{
			IImage image = e.Image;
			string imageUrl = GetImageUrl(image);

			Response.Redirect(PathHelper.Media.GetDialogCloseUrl(imageUrl, image.AlternativeText, image.Title));
		}

		private static string GetImageUrl(IImage image)
		{
			var mediaUrl = IoC.Resolve<IMediaUrlSecureService>().GetBackOfficeMediaUrl();

			string imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image);

			return imageUrl;
		}

		protected override void PopulateForm()
		{
		}
	}
}
