﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager
{
	public partial class DialogClose : System.Web.UI.Page
	{
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			string imageSrc = Request.QueryString["src"];
			if (!string.IsNullOrEmpty(imageSrc))
			{
				src.Value = imageSrc;
			}
			string imageTitle = Request.QueryString["title"];
			if (!string.IsNullOrEmpty(imageTitle))
			{
				title.Value = imageTitle;
			}
			string imageAlt = Request.QueryString["alt"];
			if (!string.IsNullOrEmpty(imageAlt))
			{
				alt.Value = imageAlt;
			}

			var ds = IoC.Resolve<IImageSizeSecureService>().GetAll();
			imgSize.Items.Clear();
			imgSize.Items.Add(new ListItem("Default", "default"));
			foreach (var size in ds)
			{
			    imgSize.Items.Add(
			        new ListItem(
			            string.Format(CultureInfo.InvariantCulture, "{0} {1}px x {2}px", size.Title, size.Width, size.Height), 
                        size.CommonName.ToLowerInvariant()));
			}

		}

	}
}
