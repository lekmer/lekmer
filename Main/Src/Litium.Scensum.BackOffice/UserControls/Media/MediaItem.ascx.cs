using System;
using System.Globalization;
using System.IO;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using MediaSetting=Litium.Scensum.BackOffice.Setting.MediaSetting;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	public partial class MediaItem : ControlBase
	{
		public MediaListViewDataSource DataSource
		{
			set
			{
				if (DisplayMode == DisplayMode.Icons)
				{
					BindIconViewItem(value);
				}
				else
				{
					BindListViewItem(value);
				}
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			lnkDelete.ServerClick += LnkDelete_Click;
			lnkListDelete.ServerClick += LnkDelete_Click;
			A2.ServerClick += LnkDetails_Click;
			hlEdit.Click += LnkDetails_Click;
		}

		private void BindIconViewItem(MediaListViewDataSource media)
		{
			hfMediaItemId.Value = media.Id.ToString(CultureInfo.CurrentCulture);
			string imgTitle = string.Concat(media.DisplayText, ".", media.FormatExtension);
			litName.Text = imgTitle;
			imgMedia.Src = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(media.Id, media.FormatExtension));
			lnkDetails.HRef = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(media.Id, media.FormatExtension));
			lnkDetails.Title = imgTitle;
			FileSizeLiteral.Text = GetFileSize(media.Id, media.FormatExtension);
			
			string width;
			if (MediaSetting.Instance.NormalWidth > 150)
			{
				width = (MediaSetting.Instance.NormalWidth + 26) + "px";
			}
			else
			{
				width = "176px";
			}
			divItemContainer.Style.Add("width", width);

			var image = IoC.Resolve<IImageSecureService>().GetById(media.Id);
			if (image != null)
			{
				litDimensions.Text = string.Format(CultureInfo.CurrentCulture, "{0}:({1}x{2})", Resources.Media.Literal_Dimensions, image.Width, image.Height);
				lnkDetails.Attributes.Add("class", "light-box");
				imgZoomIcon.Src = "~/Media/Images/Common/zoom.png";
			}
			else
			{
				imgZoomIcon.Src = "~/Media/Images/Media/download.png";
			}
		}

		private void BindListViewItem(MediaListViewDataSource media)
		{
			hfMediaItemId.Value = media.Id.ToString(CultureInfo.CurrentCulture);
			hlEdit.Text = media.DisplayText;
			imgImageList.Src = ResolveUrl(PathHelper.Media.GetImageThumbnailLoaderUrl(media.Id, media.FormatExtension));
			lnkDetailsList.HRef = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(media.Id, media.FormatExtension));
			litMediaTypeList.Text = media.FileType;
			lnkDetailsList.Title = string.Concat(media.DisplayText, ".", media.FormatExtension);
			FileSizeListLiteral.Text = GetFileSize(media.Id, media.FormatExtension);

			var image = IoC.Resolve<IImageSecureService>().GetById(media.Id);
			if (image != null)
			{
				litDimensionsList.Text = string.Format(CultureInfo.CurrentCulture, "{0}:({1}x{2})", Resources.Media.Literal_Dimensions, image.Width, image.Height);
				lnkDetailsList.Attributes.Add("class", "light-box");
				imgZoom.Src = "~/Media/Images/Common/zoom.png";
			}
			else
			{
				imgZoom.Src = "~/Media/Images/Media/download.png";
			}
		}

		protected void LnkDelete_Click(object sender, EventArgs e)
		{
			if (null != Delete) Delete(this, new MediaNavigationEventArgs { MediaId = int.Parse(hfMediaItemId.Value , CultureInfo.CurrentCulture), IsFolder = DisplayMode == DisplayMode.Icons ? !divDimensions.Visible : !litDimensionsList.Visible });
		}

		private void LnkDetails_Click(object sender, EventArgs e)
		{
			if (null != Navigate) Navigate(this, new MediaNavigationEventArgs { MediaId = int.Parse(hfMediaItemId.Value, CultureInfo.CurrentCulture), IsFolder = DisplayMode == DisplayMode.Icons ? !divDimensions.Visible : !litDimensionsList.Visible });
		}

		protected virtual string GetFileSize(int mediaId, string extension)
		{
			var mediaFilePath = IoC.Resolve<IMediaItemSharedService>().GetMediaItemPath(mediaId, extension);
			var fileInfo = new FileInfo(mediaFilePath);
			var size = fileInfo.Exists ? fileInfo.Length : 0;

			const int byteConversion = 1024;
			var bytes = System.Convert.ToDouble(size);
			string convertedSize;
			if (bytes >= Math.Pow(byteConversion, 3))
			{
				convertedSize = string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 3), 2), " GB");
			}
			else if (bytes >= Math.Pow(byteConversion, 2))
			{
				convertedSize = string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 2), 2), " MB");
			}
			else if (bytes >= byteConversion)
			{
				convertedSize = string.Concat(Math.Round(bytes / byteConversion, 2), " KB");
			}
			else
			{
				convertedSize = string.Concat(bytes, " Bytes");
			}

			return string.Format(CultureInfo.CurrentCulture, "{0}: {1}", Resources.Media.Literal_FileSize, convertedSize);
		}

		public event EventHandler<MediaNavigationEventArgs> Delete;
		public event EventHandler<MediaNavigationEventArgs> Navigate;

	}
}
