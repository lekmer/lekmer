﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	/// <summary>
	/// Summary description for FileGroupDropHandler
	/// </summary>
	public class FileGroupDropHandler : IHttpHandler
	{
		public void ProcessRequest(HttpContext context)
		{

			if (context == null) throw new ArgumentNullException("context");

				int productId;
				if (Int32.TryParse(context.Request["productId"], out productId))
				{
					var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
					var productImageService = (LekmerProductImageSecureService)IoC.Resolve<IProductImageSecureService>();
					var groups = productImageGroupSecureService.GetAllFullByProduct(productId);
					var liteImageList = new List<ProductImageLite>();
					var groupId =  Int32.Parse(ConfigurationManager.AppSettings["ImageGroupId"]);
					foreach (var group in groups)
					{
						if (group.Id == groupId)
						{
							group.ProductImages = Sort(group.ProductImages, 1);
							liteImageList.AddRange(@group.ProductImages.Where(p =>!((ILekmerImage)p.Image).IsLink).Select(image => new ProductImageLite()
							{
								GroupId = @group.Id,
								GroupName = @group.Title,
								Id = image.MediaId,
								Url = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Image.Id, image.Image.FormatExtension)),
								Title = image.Image.Title,
								Extension = image.Image.FormatExtension,
								Size = image.Image.Width + " x " + image.Image.Height
							}));
						}
					}
					var mediaList = productImageService.GetAllProductMediaByProduct(productId);
					liteImageList.AddRange(mediaList.Select(media => new ProductImageLite()
					{
						GroupId =  media.GroupId,
						Id = media.MediaId,
						Title = media.Title,
						Extension = media.Extension,
						Url = ResolveUrl("~/Media/Images/Common/pdf.png")					
						
					}));
					var js = new JavaScriptSerializer();
					context.Response.ContentType = "application/json";
					context.Response.Write(js.Serialize(liteImageList));
				}

		}


		public static string ResolveUrl(string originalUrl)
		{
			if (originalUrl == null)
				return null;

			if (originalUrl.IndexOf("://") != -1)
				return originalUrl;


			if (originalUrl.StartsWith("~"))
			{
				string newUrl = "";
				if (HttpContext.Current != null)
				{
					Uri originalUri = HttpContext.Current.Request.Url;
					newUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host +
					         originalUrl.Substring(1).Replace("\\", "/");
				}
				else		
					throw new ArgumentException("Invalid URL: Relative URL not allowed.");
				return newUrl;
			}

			return originalUrl;
		}

		private static Collection<IProductImage> Sort(IEnumerable<IProductImage> list, int ordinalsStep)
		{
			var productImages = new List<IProductImage>(list);
			productImages.Sort(delegate(IProductImage image1, IProductImage image2)
			{
				if (image1.Ordinal < image2.Ordinal)
					return -1;
				if (image1.Ordinal > image2.Ordinal)
					return 1;
				return 0;
			});
			for (int i = 0; i < productImages.Count; i++)
			{
				productImages[i].Ordinal = (i + 1) * ordinalsStep;
			}
			return new Collection<IProductImage>(productImages);
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}

	public class ProductImageLite
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Url { get; set; }
		public string Extension { get; set; }
		public string GroupName { get; set; }
		public int GroupId { get; set; }
		public string Size { get; set; }
	}
}