﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	/// <summary>
	/// Summary description for FileGroupDropHandlerMove
	/// </summary>
	public class FileGroupDropHandlerMove : IHttpHandler
	{
		private const int StepMove = 10;
		private const int ImageMoveUpStep = (int)(-1.5 * StepMove);
		private const int ImageMoveDownStep = (int)(1.5 * StepMove);
		public void ProcessRequest(HttpContext context)
		{
					
			if (context == null) throw new ArgumentNullException("context");
			if (context.Request["moveMediaId1"] != null)
			{
				var editId1 = Int32.Parse(context.Request["moveMediaId1"]);
				var productId = Int32.Parse(context.Request["productId"]);
				var groupId = Int32.Parse(context.Request["groupId"]);

				var productImageSecureService = (LekmerProductImageSecureService)IoC.Resolve<IProductImageSecureService>();
				var images = productImageSecureService.GetAllByProductAndGroup(productId, groupId);
				var image1 = images.SingleOrDefault(m => m.MediaId == editId1);
				if (image1 != null)
				{
					if (context.Request["moveMediaId2"] != null)
					{
						var editId2 = Int32.Parse(context.Request["moveMediaId2"]);
						var image2 = images.SingleOrDefault(m => m.MediaId == editId2);

						if (image2 != null)
						{
							var ordinal = image1.Ordinal;
							image1.Ordinal = image2.Ordinal;
							image2.Ordinal = ordinal;

							productImageSecureService.Save(image1);
							productImageSecureService.Save(image2);
						}
					}
					if (context.Request["nextGroupId"] != null)
					{
						var nextGroupId = Int32.Parse(context.Request["nextGroupId"]);
						var moveUp = Boolean.Parse(context.Request["moveUp"]);
						image1.ProductImageGroupId = nextGroupId;
						var images2 = productImageSecureService.GetAllByProductAndGroup(productId, nextGroupId);
						if (moveUp)
						{
							image1.Ordinal = images2.Count >0 ?  images2.Max(item => item.Ordinal) + StepMove : 1;						
						}
						else
						{
							image1.Ordinal = images2.Count >0 ? images2.Min(item => item.Ordinal) - StepMove : 1;
						}
						productImageSecureService.Save(image1);
					}
				}
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}