﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media.FileDrop.Handlers
{
	/// <summary>
	/// Summary description for FileDropMovieUploadHandler
	/// </summary>
	public class FileDropMovieUploadHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");
			
				var userName = context.Request["userName"];
				var groupId = Int32.Parse(ConfigurationManager.AppSettings["ImageGroupId"]);
				
				
				var folder = Int32.Parse(context.Request["folderId"]);
				var productId = Int32.Parse(context.Request["productId"]);
				var link = context.Request["link"];
				var parameter = context.Request["parameters"];

				var service = IoC.Resolve<IMediaItemSecureService>();
				var imageSecureService = (ILekmerImageSecureService)IoC.Resolve<IImageSecureService>();
				var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
				var userFull = systemUserSecureService.GetFullByUserName(userName);
				var imageService = IoC.Resolve<IImageSecureService>();
				
				var movie = (ILekmerImage)imageService.Create();
				if (context.Request.Files.Count > 0)
				{
					var file = context.Request.Files[0];
					movie.Id = service.Insert(userFull, file.InputStream, folder, file.FileName);
					movie.HasImage = true;
					file.InputStream.Close();
				}
				else
				{
					movie.Id = -1;

				}
							
				var extension = "jpg";
				IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension);
				if (mediaFormat == null)
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}

				var productImageSecureService = (LekmerProductImageSecureService)IoC.Resolve<IProductImageSecureService>();
		
				movie.MediaFormatId = mediaFormat.Id;
				movie.MediaFolderId = folder;

				movie.Link = link;
				movie.Parameter = parameter;
				movie.Title = "movie";
				movie.FileName = "movie";

				imageSecureService.SaveMovie(movie);
				productImageSecureService.Save(movie.Id, productId, groupId, 0);	
					
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}