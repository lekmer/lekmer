﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;
using log4net;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;


namespace Litium.Scensum.BackOffice.UserControls.Media
{
	/// <summary>
	/// Summary description for FileDropHandler
	/// </summary>
	/// 
	public class FileGroupDropHandlerUpload :  IHttpHandler
	{
		protected const int StepMove = 10;
		protected const int ImageMoveUpStep = (int)(-1.5 * StepMove);
		protected const int ImageMoveDownStep = (int)(1.5 * StepMove);

		public  void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");
			//upload images
			if (context.Request.Files.Count > 0)
			{
				var userName = context.Request["userName"];
				var groupId = Int32.Parse(context.Request["groupId"]);
				var title = context.Request["title"];
				var file = context.Request.Files[0];
				var imageFolder = Int32.Parse(context.Request["imageFolderId"]);
				var pdfFolder = Int32.Parse(context.Request["pdfFolderId"]);
				var productId = Int32.Parse(context.Request["productId"]);

				var service = IoC.Resolve<IMediaItemSecureService>();
				var extension = Path.GetExtension(file.FileName).TrimStart(new[] {'.'});
				if (string.IsNullOrEmpty(extension))
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}
				IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension);
				if (mediaFormat == null)
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}

				var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
				var userFull = systemUserSecureService.GetFullByUserName(userName);
				var fileId = service.Insert(userFull, file.InputStream, mediaFormat.Mime.Contains("pdf") ? pdfFolder: imageFolder, title ?? file.FileName);
				file.InputStream.Close();

				var imageService = IoC.Resolve<IImageSecureService>();
				var image = imageService.GetById(fileId);
				
				var productImageSecureService = (LekmerProductImageSecureService)IoC.Resolve<IProductImageSecureService>();
				var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
				var groups = productImageGroupSecureService.GetAllFullByProduct(productId);
				var imageGroup = groups.FirstOrDefault(item => item.Id == groupId);
				var ordinal = 0;
				
				if (imageGroup.ProductImages.Count > 0)
				{
					ordinal = imageGroup.ProductImages.Max(item => item.Ordinal) + StepMove;
				}

				if (image != null)
				{
					
					var productImage = productImageSecureService.Create(image, productId, groupId);
					productImage.Ordinal = ordinal;
					productImageSecureService.Save(productImage);
				}
				else
				{
					productImageSecureService.Save(fileId, productId, groupId, ordinal);	
				}
				context.Response.ContentType = "application/json";

				var wapper = new
				{
					mediaId = fileId
				};
				var js = new JavaScriptSerializer();
				context.Response.Write(js.Serialize(wapper));
			}


		}

		public bool IsReusable
		{
			get { return false; }
		}
	}

}