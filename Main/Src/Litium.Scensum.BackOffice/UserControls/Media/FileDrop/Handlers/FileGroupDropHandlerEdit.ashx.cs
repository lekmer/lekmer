﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	/// <summary>
	/// Summary description for FileGroupDropHandlerEdit
	/// </summary>
	public class FileGroupDropHandlerEdit : IHttpHandler
	{

		public new void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (context.Request["editMediaId"] != null)
			{
				var editId = Int32.Parse(context.Request["editMediaId"]);
				var userName = context.Request["userName"];
				var title = context.Request["title"];

				var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
				var userFull = systemUserSecureService.GetFullByUserName(userName);

				var imageService = IoC.Resolve<IImageSecureService>();
				var image = imageService.GetById(editId);
				image.Title = title;

				imageService.Update(userFull, image);
			}
			
		}

		public bool IsReusable
		{
			get { return false; }
		}
	}
}