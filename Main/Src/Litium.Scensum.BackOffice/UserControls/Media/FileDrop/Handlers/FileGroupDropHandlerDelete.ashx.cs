﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	/// <summary>
	/// Summary description for FileGroupDropHandlerDelete
	/// </summary>
	public class FileGroupDropHandlerDelete : IHttpHandler
	{
		public void ProcessRequest(HttpContext context)
		{
		 if (context.Request["deleteMediaId"] != null)
			{
				var productId = Int32.Parse(context.Request["productId"]);
				var deleteId = Int32.Parse(context.Request["deleteMediaId"]);

				var productImageSecureService = (LekmerProductImageSecureService) IoC.Resolve<IProductImageSecureService>();
				productImageSecureService.Delete(productId, deleteId);
				var wapper = new
				{
					status = true
				};
				context.Response.ContentType = "application/json";
				var js = new JavaScriptSerializer();
				context.Response.Write(js.Serialize(wapper));

			}
		}

		public bool IsReusable
		{
			get { return false; }
		}
	}

}