using System;
namespace Litium.Scensum.BackOffice.UserControls.Media.Events
{
	public class MediaNavigationEventArgs : EventArgs
	{
		public int MediaId { get; set; }
		public bool IsFolder { get; set; }
	}
}