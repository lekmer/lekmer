using System;

namespace Litium.Scensum.BackOffice.UserControls.Media.Events
{
	public class SelectedFolderChangedEventArgs : EventArgs
	{
		public int? SelectedFolderId
		{
			get; set;
		}
	}
}
