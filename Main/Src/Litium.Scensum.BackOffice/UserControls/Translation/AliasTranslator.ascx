﻿<%@ Control Language="C#" CodeBehind="AliasTranslator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Translation.AliasTranslator" %>

<%@ Import Namespace="Litium.Scensum.BackOffice.UserControls.Translation"%>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<link href="<%=ResolveUrl("~/Media/Css/translation-tabs.css") %>" rel="stylesheet" type="text/css" />

<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>

<script type='text/javascript' language='javascript'>
	switch ('<%=Mode%>') {
		case '<%=TranslationItemMode.Multiline%>':
			$(document).ready(function() { $('#tabs-multi').tabs(); });
			break;
		case '<%=TranslationItemMode.Wysiwyg%>':
			$(document).ready(function() { $('#tabs-wysiwyg').tabs(); });
			break;
	}

	function RemoveRedundantSpaces(str) {
		return str.replace(/(&nbsp;)+|\s+/g, " ").trim();
	}

	function closePopup() {
		$find('<%=TranslatePopup.BehaviorID%>').hide();
		return false;
	}

	function clearPopup() {
		switch ('<%=Mode%>') {
			case '<%=TranslationItemMode.Simple%>':
				document.getElementById('<%=SimpleValueTextBox.ClientID%>').value = document.getElementById('<%=SimpleValueHiddenField.ClientID%>').value;
				var rptTranslations = document.getElementById('simpleTranslatinList');
				var rptTranslationValues = rptTranslations.getElementsByTagName('input');
				for (var i = 0; i < rptTranslationValues.length; i++) {
					var translationValue = rptTranslationValues[i];
					if (translationValue.type === "text" && translationValue.nextSibling.nextSibling.type === "hidden") {
						translationValue.value = translationValue.nextSibling.nextSibling.value;
					}
				}
				break;
			case '<%=TranslationItemMode.Multiline%>':
				document.getElementById('<%=MultiValueTextBox.ClientID%>').value = document.getElementById('<%=MultiValueHiddenField.ClientID%>').value;
				var rptTranslations = document.getElementById('multiTranslatinList');
				var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
				for (var i = 0; i < rptTranslationValues.length; i++) {
					var translationValue = rptTranslationValues[i];
					if (translationValue.type === "textarea" && translationValue.nextSibling.nextSibling.type === "hidden") {
						translationValue.value = translationValue.nextSibling.nextSibling.value;
					}
				}
				break;
			case '<%=TranslationItemMode.Wysiwyg%>':
				tinyMCE.getInstanceById('<%=ValueTextBox.ClientID%>').setContent(document.getElementById('<%=ValueWysiwygHiddenField.ClientID%>').value);
				var rptTranslations = document.getElementById('wysiwygTranslatinList');
				var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
				for (var i = 0; i < rptTranslationValues.length; i++) {
					var translationValue = rptTranslationValues[i];
					if (translationValue.type === "textarea" && translationValue.nextSibling.nextSibling.nextSibling.type === "hidden") {
						tinyMCE.getInstanceById(translationValue.id).setContent(translationValue.nextSibling.nextSibling.nextSibling.value);
					}
				}
				break;
		}

		closePopup();
		return false;
	}

	function changeDefaultAndTranslations() {
		switch ('<%=Mode%>') {
			case '<%=TranslationItemMode.Simple%>':
				var defValue = RemoveRedundantSpaces(document.getElementById('<%=SimpleValueTextBox.ClientID%>').value);
				document.getElementById('<%=SimpleValueTextBox.ClientID%>').value = defValue;
				document.getElementById('<%=DefaultValueControlClientId%>').value = defValue;
				document.getElementById('<%=SimpleValueHiddenField.ClientID%>').value = defValue;
				var rptTranslations = document.getElementById('simpleTranslatinList');
				var rptTranslationValues = rptTranslations.getElementsByTagName('input');
				var rptTranslationImages = rptTranslations.getElementsByTagName('img');
				var imgCount = 0;
				for (var i = 0; i < rptTranslationValues.length; i++) {
					var translationValue = rptTranslationValues[i];
					if (translationValue.type === "text" && translationValue.nextSibling.nextSibling.type === "hidden") {
						var translationVal = RemoveRedundantSpaces(translationValue.value);
						translationValue.value = translationVal;
						translationValue.nextSibling.nextSibling.value = translationVal;
						var translationImg = rptTranslationImages[imgCount];
						imgCount = imgCount + 1;
						if (translationValue.value === "") {
							translationImg.style.display = 'inline-block';
						}
						else {
							translationImg.style.display = 'none';
						}
					}
				}
				break;
			case '<%=TranslationItemMode.Multiline%>':
				var defValue = RemoveRedundantSpaces(document.getElementById('<%=MultiValueTextBox.ClientID%>').value);
				document.getElementById('<%=MultiValueTextBox.ClientID%>').value = defValue;
				document.getElementById('<%=DefaultValueControlClientId%>').value = defValue;
				document.getElementById('<%=MultiValueHiddenField.ClientID%>').value = defValue;
				var rptTranslations = document.getElementById('multiTranslatinList');
				var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
				var rptLanguages = document.getElementById('tabs-multi');
				var rptTranslationImages = rptLanguages.getElementsByTagName('img');
				var imgCount = 0;
				for (var i = 0; i < rptTranslationValues.length; i++) {
					var translationValue = rptTranslationValues[i];
					if (translationValue.type === "textarea" && translationValue.nextSibling.nextSibling.type === "hidden") {
						var translationVal = RemoveRedundantSpaces(translationValue.value);
						translationValue.value = translationVal;
						translationValue.nextSibling.nextSibling.value = translationVal;
						var translationImg = rptTranslationImages[imgCount];
						imgCount = imgCount + 1;
						if (translationValue.value === "") {
							translationImg.style.display = 'inline-block';
						}
						else {
							translationImg.style.display = 'none';
						}
					}
				}
				break;
			case '<%=TranslationItemMode.Wysiwyg%>':
				var defValue = RemoveRedundantSpaces(tinyMCE.getInstanceById('<%=ValueTextBox.ClientID%>').getContent());
				tinyMCE.getInstanceById('<%=ValueTextBox.ClientID%>').setContent(defValue);
				tinyMCE.getInstanceById('<%=DefaultValueControlClientId%>').setContent(defValue);
				document.getElementById('<%=ValueWysiwygHiddenField.ClientID%>').value = defValue;
				var rptTranslations = document.getElementById('wysiwygTranslatinList');
				var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
				var rptLanguages = document.getElementById('tabs-wysiwyg');
				var rptTranslationImages = rptLanguages.getElementsByTagName('img');
				var imgCount = 0;
				for (var i = 0; i < rptTranslationValues.length; i++) {
					var translationValue = rptTranslationValues[i];
					if (translationValue.type === "textarea" && translationValue.nextSibling.nextSibling.nextSibling.type === "hidden") {
						var translationVal = RemoveRedundantSpaces(tinyMCE.getInstanceById(translationValue.id).getContent());
						tinyMCE.getInstanceById(translationValue.id).setContent(translationVal);
						translationValue.nextSibling.nextSibling.nextSibling.value = translationVal;
						var translationImg = rptTranslationImages[imgCount];
						imgCount = imgCount + 1;
						if (tinyMCE.getInstanceById(translationValue.id).getContent() === "") {
							translationImg.style.display = 'inline-block';
						}
						else {
							translationImg.style.display = 'none';
						}
					}
				}
				break;
		}

		closePopup();
		return false;
	}

	function SetDefaultValue() {
		switch ('<%=Mode%>') {
			case '<%=TranslationItemMode.Simple%>':
				var defValue = RemoveRedundantSpaces(document.getElementById('<%=DefaultValueControlClientId%>').value);
				document.getElementById('<%=DefaultValueControlClientId%>').value = defValue;
				document.getElementById('<%=SimpleValueTextBox.ClientID%>').value = defValue;
				document.getElementById('<%=SimpleValueHiddenField.ClientID%>').value = defValue;
				break;
			case '<%=TranslationItemMode.Multiline%>':
				var defValue = RemoveRedundantSpaces(document.getElementById('<%=DefaultValueControlClientId%>').value);
				document.getElementById('<%=DefaultValueControlClientId%>').value = defValue;
				document.getElementById('<%=MultiValueTextBox.ClientID%>').value = defValue;
				document.getElementById('<%=MultiValueHiddenField.ClientID%>').value = defValue;
				break;
			case '<%=TranslationItemMode.Wysiwyg%>':
				var defValue = RemoveRedundantSpaces(tinyMCE.getInstanceById('<%=DefaultValueControlClientId%>').getContent());
				tinyMCE.getInstanceById('<%=DefaultValueControlClientId%>').setContent(defValue);
				tinyMCE.getInstanceById('<%=ValueTextBox.ClientID%>').setContent(defValue);
				document.getElementById('<%=ValueWysiwygHiddenField.ClientID%>').value = defValue;
				break;
		}
	}
</script>
<asp:ImageButton runat="server" ID="TranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" OnClientClick="SetDefaultValue();" />
<ajaxToolkit:ModalPopupExtender ID="TranslatePopup" runat="server" PopupControlID="TranslateDiv" BackgroundCssClass="popup-background" Y="100" BehaviorID="translatorPopup" TargetControlID="TranslateButton" />

<div id="TranslateDiv" runat="server" class="translation-popup-container" style="z-index: 10010; display: none;">
	<div id="translation-popup-header">
		<div id="translation-popup-header-left">
		</div>
		<div id="translation-popup-header-center">
			<span>Translate</span>
			<asp:Button ID="CloseTranslationButton" runat="server" Text="x" OnClientClick="javascript:clearPopup(); return false;" />
		</div>
		<div id="translation-popup-header-right">
		</div>
	</div>
	<div id="translation-popup-content">
		<div id="SimpleDiv" runat="server">
			<span>Default</span>
			<br />
			<asp:TextBox ID="SimpleValueTextBox" runat="server" />
			<asp:HiddenField ID="SimpleValueHiddenField" runat="server" />
			<div id="simpleTranslatinList">
				<asp:Repeater ID="SimpleTranslationRepeater" runat="server">
					<ItemTemplate>
						<div id="value-translate" class="translation-item">
							<asp:Label ID="lblLanguageTitleSimple" runat="server" Text='<%# Eval("Language.Title") %>' />
							<asp:Image ID="imgExclamationSimple" runat="server" ImageUrl="~/Media/Images/Interface/exclamation.gif" style='<%# GetExclamationVisibility((string)Eval("Value")) %>' />
							<br />
							<asp:TextBox ID="tbTranslateValueSimple" runat="server" Text='<%# Eval("Value") %>' />
							<asp:HiddenField ID="hfTranslateValueSimple" runat="server" Value='<%# Eval("Value") %>' />
							<asp:HiddenField ID="hfLangIdSimple" runat="server" Value='<%# Eval("Language.Id").ToString() %>' />
						</div>
					</ItemTemplate>
				</asp:Repeater>
			</div>
		</div>
		<div id="MultiLineDiv" runat="server">
			<span>Default</span>
			<br />
			<asp:TextBox ID="MultiValueTextBox" runat="server" TextMode="MultiLine" />
			<asp:HiddenField ID="MultiValueHiddenField" runat="server" />
			<div id="tabs-multi" class="translations-multi-line">
				<ul>
					<asp:Repeater ID="MultiLanguageRepeater" runat="server">
						<ItemTemplate>
							<li>
								<a id="tabLnkMulti" runat="server" href='<%# string.Concat("#multiLine_", Eval("Language.Id")) %>'>
									<asp:Label ID="lblLanguageTitleMulti" runat="server" Text='<%# Eval("Language.Title") %>' />
									&nbsp;
									<asp:Image ID="imgExclamationMulti" runat="server" ImageUrl="~/Media/Images/Interface/exclamation.gif" ImageAlign="AbsMiddle" style='<%# GetExclamationVisibility((string)Eval("Value")) %>' />
								</a>
							</li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<br clear="all" />
				<div id="multiTranslatinList" class="translation-popup-tabs-container">
					<asp:Repeater ID="MultiTranslationRepeater" runat="server">
						<ItemTemplate>
							<div id='<%# string.Concat("multiLine_", Eval("Language.Id")) %>'>
								<asp:TextBox ID="tbTranslateValueMulti" runat="server" TextMode="MultiLine" Text='<%# Eval("Value") %>' />
								<asp:HiddenField ID="hfTranslateMulti" runat="server" Value='<%# Eval("Value") %>' />
								<asp:HiddenField ID="hfLangIdMulti" runat="server" Value='<%# Eval("Language.Id").ToString() %>' />
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
		</div>	
		<div id="WysiwygDiv" runat="server">
			<span>Default</span>
			<div class="translation-wysiwyg">
				<uc:LekmerTinyMceEditor ID="ValueTextBox" runat="server" SkinID="tinyMCE" Width="200px" />
				<asp:HiddenField ID="ValueWysiwygHiddenField" runat="server" />
			</div>
			<div id="tabs-wysiwyg">
				<ul>
					<asp:Repeater ID="WysiwygLanguageRepeater" runat="server">
						<ItemTemplate>
							<li>
								<a id="tabLnkWysiwyg" runat="server" href='<%# string.Concat("#wysiwyg_", Eval("Language.Id")) %>'>
									<asp:Label ID="lblLanguageTitleWysiwyg" runat="server" Text='<%# Eval("Language.Title") %>' />
									<asp:Image ID="imgExclamationWysiwyg" runat="server" ImageUrl="~/Media/Images/Interface/exclamation.gif" ImageAlign="AbsMiddle" style='<%# GetExclamationVisibility((string)Eval("Value")) %>' />
								</a>
							</li>
						</ItemTemplate>
					</asp:Repeater>
				</ul>
				<br clear="all" />
				<div id="wysiwygTranslatinList" class="translation-popup-tabs-container">
					<asp:Repeater ID="WysiwygTranslationRepeater" runat="server">
						<ItemTemplate>
							<div id='<%# string.Concat("wysiwyg_", Eval("Language.Id")) %>'>
								<uc:LekmerTinyMceEditor ID="tmTranslateValueWysiwyg" runat="server" SkinID="tinyMCETranslate" Width="200px" />
								<asp:HiddenField ID="hfTranslateWysiwyg" runat="server" Value='<%# Eval("Value") %>' />
								<asp:HiddenField ID="hfLangIdWysiwyg" runat="server" Value='<%# Eval("Language.Id").ToString() %>' />
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
		</div>
		<br class="clear"/>
		<div class="translation-popup-acton-button">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" OnClientClick="javascript:changeDefaultAndTranslations(); return false;" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="Cancel" SkinID="DefaultButton" OnClientClick="javascript:clearPopup(); return false;" />
		</div>
	</div>
</div>