﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Common
{
	public partial class CurrencyValueInlineManager : UserControl
	{
		protected class CurrencyRepeaterDataSourceItem
		{
			public ICurrency Currency { get; set; }
			public string CurrencyIsoWithChannels { get; set; }
			public string MonetaryValue { get; set; }
		}

		protected Collection<ICurrency> Currencies { get; set; }
		protected Dictionary<int, string> ChannelsCountryIso { get; set; }

		public event EventHandler<EventArgs> DataRequired;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary DataSource { get; set; }

		public override void DataBind()
		{
			if (DataSource == null)
			{
				return;
			}
			var currencyValues = new Collection<CurrencyRepeaterDataSourceItem>();

			CheckRequiredData();

			foreach (ICurrency currency in Currencies)
			{
				CurrencyRepeaterDataSourceItem item;
				decimal discountValue;
				if (!DataSource.TryGetValue(currency.Id, out discountValue))
				{
					item = new CurrencyRepeaterDataSourceItem
					{
						Currency = currency,
						MonetaryValue = string.Empty
					};
				}
				else
				{
					item = new CurrencyRepeaterDataSourceItem
					{
						Currency = currency,
						MonetaryValue = discountValue.ToString("C", CurrencyCulture)
					};
				}
				item.CurrencyIsoWithChannels = item.Currency.Iso + ChannelsCountryIso[item.Currency.Id];
				currencyValues.Add(item);
			}
			CurrencyValueRepeater.DataSource = currencyValues;
			CurrencyValueRepeater.DataBind();
		}

		protected void OnCurrencyValueRepeaterDataBound(object sender, RepeaterItemEventArgs e)
		{
			var validator = (RangeValidator) e.Item.FindControl("CurrencyValueValidator");
			validator.MinimumValue = decimal.Zero.ToString("C", CurrencyCulture);
			validator.MaximumValue = decimal.MaxValue.ToString("C", CurrencyCulture);
		}

		public LekmerCurrencyValueDictionary CollectCurrencyValues()
		{
			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();
			foreach (RepeaterItem repeaterItem in CurrencyValueRepeater.Items)
			{
				var monetaryValueText = ((TextBox)repeaterItem.FindControl("ValueTextBox")).Text;
				if (monetaryValueText.IsNullOrTrimmedEmpty())
				{
					continue;
				}
				
				decimal monetaryValue;
				if (!decimal.TryParse(monetaryValueText, NumberStyles.Currency, CurrencyCulture, out monetaryValue))
				{
					continue;
				}

				if (monetaryValue < 0)
				{
					continue;
				}

				var currencyIdText = ((HiddenField)repeaterItem.FindControl("CurrencyIdHiddenField")).Value;
				var currencySecureService = IoC.Resolve<ICurrencySecureService>();
				int currencyId = int.Parse(currencyIdText, CultureInfo.CurrentCulture);
				ICurrency currency = currencySecureService.GetById(currencyId);

				var currencyValue = new CurrencyValue { Currency = currency, MonetaryValue = monetaryValue };
				lekmerCurrencyValueDictionary.Add(currencyValue);
			}
			return lekmerCurrencyValueDictionary;
		}

		public void ProvideRequiredData(Collection<ICurrency> currencies, Dictionary<int, string> channelsCountryIso)
		{
			Currencies = currencies;
			ChannelsCountryIso = channelsCountryIso;
		}

		private void CheckRequiredData()
		{
			if (Currencies == null || ChannelsCountryIso == null)
			{
				if (DataRequired != null)
				{
					DataRequired(this, new EventArgs());
				}
			}

			if (Currencies == null || ChannelsCountryIso == null)
			{
				Collection<IChannel> channels = IoC.Resolve<ILekmerChannelSecureService>().GetAll(BackOfficeSetting.Instance.ChannelCommonNamesToIgnore);
				Collection<ICountry> countries = IoC.Resolve<ILekmerCountrySecureService>().GetAll(BackOfficeSetting.Instance.CountryIdsToIgnore);
				Currencies = IoC.Resolve<ILekmerCurrencySecureService>().GetAll(BackOfficeSetting.Instance.CurrencyIdsToIgnore);
				
				ChannelsCountryIso = new Dictionary<int, string>();
				var campaignCurrencyValueHelper = new CampaignCurrencyValueHelper();

				foreach (ICurrency currency in Currencies)
				{
					ChannelsCountryIso[currency.Id] = campaignCurrencyValueHelper.GetChannelsCountryIso(channels, countries, currency.Id);
				}
			}
		}

		private CultureInfo _currencyCulture;
		protected CultureInfo CurrencyCulture
		{
			get
			{
				if (_currencyCulture == null)
				{
					_currencyCulture = (CultureInfo)HttpContext.Current.Items["CurrencyValueInlineManager_CurrencyCulture"];
				}

				if (_currencyCulture == null)
				{
					_currencyCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
					_currencyCulture.NumberFormat.CurrencySymbol = string.Empty;
					_currencyCulture.NumberFormat.NumberGroupSizes = _currencyCulture.NumberFormat.CurrencyGroupSizes;
					_currencyCulture.NumberFormat.NumberGroupSeparator = _currencyCulture.NumberFormat.CurrencyGroupSeparator;
					_currencyCulture.NumberFormat.NumberDecimalSeparator = _currencyCulture.NumberFormat.CurrencyDecimalSeparator;
					_currencyCulture.NumberFormat.NumberDecimalDigits = _currencyCulture.NumberFormat.CurrencyDecimalDigits;

					HttpContext.Current.Items["CurrencyValueInlineManager_CurrencyCulture"] = _currencyCulture;
				}

				return _currencyCulture;
			}
		}
	}
}