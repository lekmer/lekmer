﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteStructureNodeSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Tree.SiteStructureNodeSelector" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree.SiteStructure"
    TagPrefix="CustomControls" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>

<asp:UpdatePanel ID="FolderPathUpdatePanel" runat="server" UpdateMode="Conditional">
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click"/>
	</Triggers>
	<ContentTemplate>
        <asp:TextBox ID="FolderTextBox" runat="server" ReadOnly="true" CssClass="media-item-information-value" />       
		<asp:ImageButton ID="FolderButton" runat="server" ImageUrl="~/Media/Images/Tree/folder.png" ImageAlign="AbsMiddle" CausesValidation="False"/>
		
		<div id="FolderDiv" runat="server" class="mainDiv block-save" style="display: none;">
			<asp:Panel ID="Panel1" runat="server" DefaultButton="SaveButton">
				<div id="divHeader" class="headerDiv">
					<asp:Label ID="Label1" runat="server" Text="Select folder" />
					<asp:Button ID="CancelButton" runat="server" Text="X" />
				</div>
				<div id="page-select-subdiv">
					<asp:UpdatePanel ID="LeftUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="select-folder-header">
								Folders
							</div>
							<div id="select-folder">
								<asp:UpdatePanel ID="UpdatePanel1" runat="server">
									<ContentTemplate>
										<CustomControls:SiteStructureTreeView Id="NodesTreeView" runat="server" DisplayTextControl="lbName"
											NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="ss-node-type"
											MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
											NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
											NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
											NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
											MenuContainerElementId="node-menu" FolderImageUrlOnline="~/Media/Images/SiteStructure/folder.png"
											FolderImageUrlOffline="~/Media/Images/SiteStructure/folder-off.png" FolderImageUrlHidden="~/Media/Images/SiteStructure/folder-hid.png"
											MasterPageImageUrl="~/Media/Images/SiteStructure/master.png" SubPageImageUrlOnline="~/Media/Images/SiteStructure/sub-page.png"
											SubPageImageUrlOffline="~/Media/Images/SiteStructure/sub-page-off.png" SubPageImageUrlHidden="~/Media/Images/SiteStructure/sub-page-hid.png"
											PageImageUrlOnline="~/Media/Images/SiteStructure/page.png" PageImageUrlOffline="~/Media/Images/SiteStructure/page-off.png"
											PageImageUrlHidden="~/Media/Images/SiteStructure/page-hid.png" ShortcutImageUrlOnline="~/Media/Images/SiteStructure/shortcut.png"
											ShortcutImageUrlOffline="~/Media/Images/SiteStructure/shortcut-off.png" ShortcutImageUrlHidden="~/Media/Images/SiteStructure/shortcut-hid.png"
											UrlLinkImageUrlOnline="~/Media/Images/SiteStructure/link.png" UrlLinkImageUrlOffline="~/Media/Images/SiteStructure/link-off.png"
											UrlLinkImageUrlHidden="~/Media/Images/SiteStructure/link-hid.png" TypeImageContainerId="img"
											TypeImageContainerCssClass="ss-node-type" MenuToOnlineContainerId="to-online"
											MenuToOfflineContainerId="to-offline" MenuToHiddenContainerId="to-hidden" MenuContainerElementCssClass="menu-container"
											MenuContainerShadowElementCssClass="menu-shadow" MenuContainerElementTypeOfPageCssClass="menu-container-typeof-page"
											MenuContainerShadowElementTypeOfPageCssClass="menu-shadow-typeof-page" MenuContainerShadowElementId="menu-shadow"
											MenuPageSpecificContainerId="menu-pages-container" MenuItemsContainerElementId="menu-container"
											NodeNavigationAnchorId="aName" NodeSelectedCssClass="treeview-node-selected">
											<HeaderTemplate>
												<div id="clear-selection" style="padding-left:20px; padding-top:3px; height:17px;  border-bottom-width:2px;">
													<asp:LinkButton runat="server" ID="lbClearSelection" Font-Underline="false" OnClick="ClearSelectionButtonClick" Text="<%$ Resources:SiteStructure, Button_ClearSelection %>" ></asp:LinkButton>
												</div>
											</HeaderTemplate>
											<NodeTemplate>
												<div class="tree-item-cell-expand">
													<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
													<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
												</div>
												<div class="tree-item-cell-main">
													<img runat="server" id="img" class="ss-node-type tree-node-img" />
													<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
												</div>
												<br />
											</NodeTemplate>
										</CustomControls:SiteStructureTreeView>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
							<div class="right clear">
								<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" />
								<uc:ImageLinkButton ID="CancelSaveButton" runat="server" Text="Cancel" SkinID="DefaultButton"/>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>			
			</asp:Panel>
		</div>
		
		<ajaxToolkit:ModalPopupExtender 
			ID="TreeModalPopupExtender"
			runat="server"  
			TargetControlID="FolderButton"
			PopupControlID="FolderDiv"
			CancelControlID="CancelButton"
			BackgroundCssClass="popup-background"
		/>
	
	</ContentTemplate>
</asp:UpdatePanel>
