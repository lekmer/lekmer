using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.Tree
{
	public partial class NodeSelector : UserControlController
	{
		public event EventHandler<SelectedFolderChangedEventArgs> SelectedFolderChangedEvent;
		
		/// <summary>
		/// Require data source update and data bind.
		/// </summary>
		public event EventHandler<CommandEventArgs> NodeCommand;

		private int? _selectedNodeId;
		public int? SelectedNodeId
		{
			get { return _selectedNodeId; }
			set { NodesTreeView.SelectedNodeId = _selectedNodeId = value; }
		}

		public IEnumerable<INode> DataSource
		{
			get { return CorrectSource(NodesTreeView.DataSource); }
			set { NodesTreeView.DataSource = value; }
		}

		public bool DenySelection
		{
			get { return NodesTreeView.DenySelection; }
			set { NodesTreeView.DenySelection = value; }
		}

		public bool UseRootNode
		{
			get { return NodesTreeView.UseRootNode; }
			set { NodesTreeView.UseRootNode = value; }
		}

		public int RootNodeId
		{
			get { return NodesTreeView.RootNodeId; }
			set { NodesTreeView.RootNodeId = value; }
		}

		public bool AllowClearSelection { get; set; }

		protected override void SetEventHandlers()
		{
			SaveButton.Click += BtnSaveFolder_Click;
			CancelSaveButton.Click += OnCancel;
			CancelButton.Click += OnCancel;
			NodesTreeView.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateControl()
		{
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			//Event handler is responsible for supplying new data source.
			if (NodeCommand != null)
			{
				NodeCommand(this, new CommandEventArgs(e.EventName, e.Id));
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CancelSaveButton.OnClientClick = "document.getElementById('" + CancelButton.ClientID + "').click();";
		}

		protected override object SaveViewState()
		{
			ViewState["NodeSelectDataSource" + ClientID] = DataSource;
			ViewState["SelectedNodeId" + ClientID] = SelectedNodeId;
			return base.SaveViewState();
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			_selectedNodeId = (int?)ViewState["SelectedNodeId" + ClientID];
			DataSource = (IEnumerable<INode>)ViewState["NodeSelectDataSource" + ClientID];
		}
		
		protected void ClearSelectionButtonClick(object sender, EventArgs e)
		{
			DenySelection = true;
			SelectedNodeId = null;
		}

		private void BtnSaveFolder_Click(object sender, EventArgs e)
		{
			TreeModalPopupExtender.Hide();
			if (!NodesTreeView.SelectedNodeId.HasValue || DenySelection)
			{
				FolderTextBox.Text = string.Empty;
				return;
			}

			int folderId = NodesTreeView.SelectedNodeId.Value;
			SelectedNodeId = folderId;
			PopulatePath(folderId);
			if (SelectedFolderChangedEvent != null)
			{
				SelectedFolderChangedEvent(this, new SelectedFolderChangedEventArgs { SelectedFolderId = folderId });
			}
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			NodesTreeView.SelectedNodeId = SelectedNodeId;
			DenySelection = DataSource.FirstOrDefault(n => n.Id == SelectedNodeId) == null;
		}

		public void PopulatePath(int? folderId)
		{
			IEnumerable<INode> folders = DataSource;
			INode folder = folders.FirstOrDefault(f => f.Id == folderId);
			if (folder == null)
			{
				return;
			}

			var foldersPath = new StringBuilder(folder.Title);
			
			while (folder.ParentId.HasValue)
			{
				var clone = folder; // to avoid access to modified closure
				folder = folders.FirstOrDefault(f => f.Id == clone.ParentId);
				if (folder == null || folder.Id == clone.Id) break;

				foldersPath.Insert(0, @"\");
				foldersPath.Insert(0, folder.Title);
			}
			FolderTextBox.Text = foldersPath.ToString();
		}

		public void Clear()
		{
			SelectedNodeId = null;
			FolderTextBox.Text = string.Empty;
		}

		/// <summary>
		/// Returns data source without filtering node and its children.
		/// Update has children property for the parent of filtering node.
		/// </summary>
		public virtual IEnumerable<INode> GetFilteredSource(IEnumerable<INode> nodes, int filterNodeId)
		{
			return NodesTreeView.GetFilteredSource(nodes, filterNodeId);
		}

		//To avoid FileSystemStatePage error when Linq is used
		private static IEnumerable<INode> CorrectSource(IEnumerable<INode> nodes)
		{
			if (nodes == null) return nodes;

			var source = new Collection<INode>();
			foreach (var node in nodes)
			{
				source.Add(node);
			}
			return source;
		}

		/// <summary>
		/// Performs DataBind method for tree view control.
		/// </summary>
		public virtual void RestoreTree()
		{
			NodesTreeView.DataBind();
		}
	}
}
