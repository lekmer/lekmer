﻿using System.Collections.Generic;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.Lekmer
{
	public interface ILekmerSelectorProcessor
	{
		bool IsAvailable(ISiteStructureNode node);
		IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId);
	}
}
