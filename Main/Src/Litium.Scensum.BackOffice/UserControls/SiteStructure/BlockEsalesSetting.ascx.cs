﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class BlockEsalesSetting : UserControlController
	{
		public int BlockTypeId { get; set; }
		public IBlockEsalesSetting EsalesSetting { get; set; }
		public string ValidationGroup { get; set; }

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
			if (EsalesSetting == null)
			{
				throw new ConfigurationErrorsException("Setting property is not initialized");
			}
			if (BlockTypeId == 0)
			{
				throw new ConfigurationErrorsException("BlockTypeId property is not initialized");
			}

			PopulateData(EsalesSetting);
		}

		public void SetSettings(int blockTypeId, IBlockEsalesSetting setting)
		{
			BlockTypeId = blockTypeId;
			setting.EsalesModelComponentId = EsalesModelList.SelectedValue.HasValue() ? (int?) EsalesModelList.SelectedValue.ToLocalInt32() : null;

			PopulateData(setting);
		}

		private void PopulateData(IBlockEsalesSetting setting)
		{
			PopulateEsalesModelList(setting.EsalesModelComponentId);

			EsalesModelRequiredValidator.ValidationGroup = ValidationGroup;
			EsalesModelRequiredValidator.InitialValue = string.Empty;
		}

		protected virtual void PopulateEsalesModelList(int? modelComponentId)
		{
			var esalesModelSecureService = IoC.Resolve<IEsalesModelSecureService>();

			Collection<IEsalesModelComponent> modelComponents = esalesModelSecureService.GetModelComponentsAllByBlockType(BlockTypeId);
			Collection<KeyValuePair<int, string>> modelComponentsInfo = new Collection<KeyValuePair<int, string>>();

			foreach (IEsalesModelComponent modelComponent in modelComponents)
			{
				int key = modelComponent.Id;
				string value = string.Concat(modelComponent.Model.Title, " - ", modelComponent.Title);
				modelComponentsInfo.Add(new KeyValuePair<int, string>(key, value));
			}

			EsalesModelList.DataSource = modelComponentsInfo;
			EsalesModelList.DataBind();

			var listItem = new ListItem(Resources.Lekmer.Literal_SelectModel, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			EsalesModelList.Items.Insert(0, listItem);

			if (modelComponentId.HasValue && modelComponentId.Value > 0)
			{
				var item = EsalesModelList.Items.FindByValue(modelComponentId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}
	}
}
