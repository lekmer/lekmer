﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public class MasterPageProcessor : ISelectorProcessor
	{
		private IEnumerable<IContentPage> _selectablePages;
		
		private int SiteStructureRegistryId { get; set; }

		public MasterPageProcessor(int siteStructureRegistryId)
		{
			SiteStructureRegistryId = siteStructureRegistryId;
		}

		public IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId)
		{
			return PopulateMasterPages();
		}

		public virtual bool IsAvailable(ISiteStructureNode node)
		{
			return node.IsMasterPage;
		}

		private IEnumerable<ISiteStructureNode> PopulateMasterPages()
		{
			var masterPages = GetMasterPages();
			IEnumerable<ISiteStructureNode> selectedNodes = new Collection<ISiteStructureNode>();
			var contentNodeService = IoC.Resolve<IContentNodeSecureService>();
			foreach (var page in masterPages)
			{
				selectedNodes = selectedNodes.Union(contentNodeService.GetTree(page.Id, false));
			}
			var result = new Collection<ISiteStructureNode>();
			foreach (var node in selectedNodes)
				{
				var nodeId = node.Id;
				if (result.Any(n => n.Id == nodeId)) continue;
				node.HasChildren = selectedNodes.Any(n => n.ParentId == nodeId);
					result.Add(node);
				}
			return result;
		}

		private IEnumerable<IContentPage> GetMasterPages()
			{
			return _selectablePages ??
			       (_selectablePages = IoC.Resolve<IContentPageSecureService>().GetAllMaster(SiteStructureRegistryId));
		}
	}
}