using System.Collections.Generic;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public interface ISelectorProcessor
	{
		IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId);
		bool IsAvailable(ISiteStructureNode node);
	}
}