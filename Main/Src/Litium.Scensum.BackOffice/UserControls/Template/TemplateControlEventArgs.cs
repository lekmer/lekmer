using System;

namespace Litium.Scensum.BackOffice.UserControls.Template
{
	public class TemplateControlEventArgs: EventArgs
	{
		public TemplateControlEventArgs(bool showAlternate)
		{
			ShowAlternate = showAlternate;
		}
		public bool ShowAlternate { get; set; }
	}
}
