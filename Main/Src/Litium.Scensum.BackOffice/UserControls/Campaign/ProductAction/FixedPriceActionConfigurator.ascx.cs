﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.BackOffice.UserControls.Common;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class FixedPriceActionConfigurator : ProductActionControl<IFixedPriceAction>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get
			{
				if (_campaignConfiguratorHelper == null)
				{
					_campaignConfiguratorHelper = new CampaignConfiguratorHelper();
				}

				return _campaignConfiguratorHelper;
			}
		}

		private BrandCollection _brandCollection;
		public BrandCollection BrandCollection
		{
			get
			{
				if (_brandCollection == null)
				{
					_brandCollection = IoC.Resolve<IBrandSecureService>().GetAll();
				}

				return _brandCollection;
			}
		}

		protected override void SetEventHandlers()
		{
			SetIncludeProductEventHandlers();
			SetExcludeProductEventHandlers();

			SetIncludeCategoryEventHandlers();
			SetExcludeCategoryEventHandlers();

			SetIncludeBrandEventHandlers();
			SetExcludeBrandEventHandlers();

			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		protected override void PopulateControl() { }

		public override void DataBind(IProductAction dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var action = (IFixedPriceAction)dataSource;

			PriceTypeTextBox.Text = action.ActionType.Title;
			CurrencyValueManager.DataSource = action.Amounts;
			CurrencyValueManager.DataBind();

			IncludeAllProductsCheckbox.Checked = action.IncludeAllProducts;

			DataBindIncludeProducts(action);
			DataBindExcludeProducts(action);

			DataBindIncludeCategories(action);
			DataBindExcludeCategories(action);

			DataBindIncludeBrands(action);
			DataBindExcludeBrands(action);

			TargetProductTypeSelectorControl.DataSource = action.TargetProductTypes;
			TargetProductTypeSelectorControl.DataBind();

			State = action;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				DataBindProductIncludeGrid(new ProductIdDictionary());
				DataBindCategoryIncludeGrid(new Collection<ICategory>());
				DataBindBrandIncludeGrid(new Collection<IBrand>());
			}
			else
			{
				DataBindProductIncludeGrid(State.IncludeProducts);
				DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
				DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.IncludeBrands));
			}
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			DataBindProductIncludeGrid(State.IncludeProducts);

			ProductExcludeGrid.PageIndex = 0;
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		public override bool TryGet(out IProductAction entity)
		{
			if (IsValid())
			{
				if (IncludeAllProductsCheckbox.Checked)
				{
					State.IncludeProducts.Clear();
					State.IncludeCategories.Clear();
					State.IncludeBrands.Clear();
				}

				var action = State;

				action.Amounts = CurrencyValueManager.CollectCurrencyValues();
				action.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;
				action.TargetProductTypes = TargetProductTypeSelectorControl.CollectSelectedValues();

				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			Collection<string> validationMessages;
			if (!CurrencyValueManager.IsValid(out validationMessages))
			{
				isValid = false;
				foreach (string validationMessage in validationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			Collection<string> targetProductTypesValidationMessages;
			if (!TargetProductTypeSelectorControl.IsValid(out targetProductTypesValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in targetProductTypesValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			StringBuilder b = new StringBuilder();

			Collection<IChannel> channels = IoC.Resolve<IChannelSecureService>().GetAll();
			Collection<ICountry> countries = IoC.Resolve<ICountrySecureService>().GetAll();
			Collection<ICurrency> currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
			var campaignCurrencyValueHelper = new CampaignCurrencyValueHelper();

			var amounts = CurrencyValueManager.CollectCurrencyValues();

			foreach (var amount in amounts)
			{
				var currencyId = amount.Key;
				var currency = currencies.FirstOrDefault(c => c.Id == currencyId);
				string currencyIso = currency != null ? currency.Iso : string.Empty;

				var currencyIsoWithChannels = currencyIso + campaignCurrencyValueHelper.GetChannelsCountryIso(channels, countries, currencyId);
				var productsNotValid = ((ILekmerProductSecureService)IoC.Resolve<IProductSecureService>()).GetPriceByChannelAndCampaign(currencyId, amount.Value, State);
				if (productsNotValid.Count > 0)
				{
					b.AppendLine("Currency " + currencyIsoWithChannels + "<br/>");
					isValid = false;
					foreach (var product in productsNotValid)
					{
						b.AppendLine(product.Key + " with a price of " + product.Value + " " + currencyIso + "<br/>");
					}
					b.AppendLine("<br/>");
				}

			}
			if (!isValid)
			{
				ValidationMessages.Add(Resources.CampaignMessage.FixedPriceActionInvalidProducts);
				AdditionalValidationMessage = b.ToString();
			}

			return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmBrandRemove", "function ConfirmBrandRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteBrand + "');}", true);

			const string script = "$(function() {" +
								"ResizePopup('ActionPopupDiv', 0.8, 0.8,  0.06);" +
								"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('ProductExcludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('BrandIncludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('BrandExcludePopupDiv', 0.4, 0.45, 0.09);" +
								"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);

			string targetCollapsiblescript = "OpenClose('" + TargetPanelDiv.ClientID + "','" + TargetPanelStateHidden.ClientID + "','" + TargetPanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			TargetCollapsibleDiv.Attributes.Add("onclick", targetCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			BrandIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandIncludeGrid) ? "block" : "none";

			ProductExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			BrandExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandExcludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0 || BrandIncludeGrid.Rows.Count > 0 ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 || BrandExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}
	}
}