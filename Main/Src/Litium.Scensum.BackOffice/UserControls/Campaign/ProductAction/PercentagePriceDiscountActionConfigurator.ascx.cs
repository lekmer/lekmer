﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class PercentagePriceDiscountActionConfigurator : ProductActionControl<ILekmerPercentagePriceDiscountAction>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		private CampaignConfiguratorProduct _campaignConfiguratorIncludeProduct;
		private CampaignConfiguratorProduct CampaignConfiguratorIncludeProduct
		{
			get { return _campaignConfiguratorIncludeProduct ?? (_campaignConfiguratorIncludeProduct = new CampaignConfiguratorProduct()); }
		}

		private CampaignConfiguratorProduct _campaignConfiguratorExcludeProduct;
		private CampaignConfiguratorProduct CampaignConfiguratorExcludeProduct
		{
			get { return _campaignConfiguratorExcludeProduct ?? (_campaignConfiguratorExcludeProduct = new CampaignConfiguratorProduct()); }
		}

		private CampaignConfiguratorCategory _campaignConfiguratorIncludeCategory;
		private CampaignConfiguratorCategory CampaignConfiguratorIncludeCategory
		{
			get { return _campaignConfiguratorIncludeCategory ?? (_campaignConfiguratorIncludeCategory = new CampaignConfiguratorCategory()); }
		}

		private CampaignConfiguratorCategory _campaignConfiguratorExcludeCategory;
		private CampaignConfiguratorCategory CampaignConfiguratorExcludeCategory
		{
			get { return _campaignConfiguratorExcludeCategory ?? (_campaignConfiguratorExcludeCategory = new CampaignConfiguratorCategory()); }
		}

		private CampaignConfiguratorBrand _campaignConfiguratorIncludeBrand;
		private CampaignConfiguratorBrand CampaignConfiguratorIncludeBrand
		{
			get { return _campaignConfiguratorIncludeBrand ?? (_campaignConfiguratorIncludeBrand = new CampaignConfiguratorBrand()); }
		}

		private CampaignConfiguratorBrand _campaignConfiguratorExcludeBrand;
		private CampaignConfiguratorBrand CampaignConfiguratorExcludeBrand
		{
			get { return _campaignConfiguratorExcludeBrand ?? (_campaignConfiguratorExcludeBrand = new CampaignConfiguratorBrand()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (State != null)
			{
				CampaignConfiguratorIncludeProduct.Products = State.CampaignConfig.IncludeProducts;
				CampaignConfiguratorExcludeProduct.Products = State.CampaignConfig.ExcludeProducts;

				CampaignConfiguratorIncludeCategory.Categories = State.CampaignConfig.IncludeCategories;
				CampaignConfiguratorExcludeCategory.Categories = State.CampaignConfig.ExcludeCategories;

				CampaignConfiguratorIncludeBrand.Brands = State.CampaignConfig.IncludeBrands;
				CampaignConfiguratorExcludeBrand.Brands = State.CampaignConfig.ExcludeBrands;
			}
		}

		protected override void SetEventHandlers()
		{
			InitializeConfiguratorIncludeProducts();
			InitializeConfiguratorExcludeProducts();

			InitializeConfiguratorIncludeCategories();
			InitializeConfiguratorExcludeCategories();

			InitializeConfiguratorIncludeBrands();
			InitializeConfiguratorExcludeBrands();

			CampaignConfiguratorIncludeProduct.SetEventHandlers();
			CampaignConfiguratorExcludeProduct.SetEventHandlers();

			CampaignConfiguratorIncludeCategory.SetEventHandlers();
			CampaignConfiguratorExcludeCategory.SetEventHandlers();

			CampaignConfiguratorIncludeBrand.SetEventHandlers();
			CampaignConfiguratorExcludeBrand.SetEventHandlers();

			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
		}

		protected override void PopulateControl() { }

		public override void DataBind(IProductAction dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var action = (ILekmerPercentagePriceDiscountAction)dataSource;
			State = action;

			DiscountPercentageTextBox.Text = action.DiscountAmount.ToString(CultureInfo.CurrentCulture);
			DiscountTypeTextBox.Text = action.ActionType.Title;

			IncludeAllProductsCheckbox.Checked = action.CampaignConfig.IncludeAllProducts;

			CampaignConfiguratorIncludeProduct.DataBind(action.CampaignConfig.IncludeProducts);
			CampaignConfiguratorExcludeProduct.DataBind(action.CampaignConfig.ExcludeProducts);

			CampaignConfiguratorIncludeCategory.DataBind(action.CampaignConfig.IncludeCategories);
			CampaignConfiguratorExcludeCategory.DataBind(action.CampaignConfig.ExcludeCategories);

			CampaignConfiguratorIncludeBrand.DataBind(action.CampaignConfig.IncludeBrands);
			CampaignConfiguratorExcludeBrand.DataBind(action.CampaignConfig.ExcludeBrands);

			TargetProductTypeSelectorControl.DataSource = action.TargetProductTypes;
			TargetProductTypeSelectorControl.DataBind();

			State = action;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				CampaignConfiguratorIncludeProduct.DataBindGrid(new ProductIdDictionary());
				CampaignConfiguratorIncludeCategory.DataBindGrid(new Collection<ICategory>());
				CampaignConfiguratorIncludeBrand.DataBindGrid(new Collection<IBrand>());
			}
			else
			{
				CampaignConfiguratorIncludeProduct.DataBindGrid(State.CampaignConfig.IncludeProducts);
				CampaignConfiguratorIncludeCategory.DataBindGrid(CampaignConfiguratorHelper.ResolveCategories(State.CampaignConfig.IncludeCategories));
				CampaignConfiguratorIncludeBrand.DataBindGrid(CampaignConfiguratorHelper.ResolveBrands(State.CampaignConfig.IncludeBrands));
			}
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			CampaignConfiguratorIncludeProduct.DataBindGrid(State.CampaignConfig.IncludeProducts);

			ProductExcludeGrid.PageIndex = 0;
			CampaignConfiguratorExcludeProduct.DataBindGrid(State.CampaignConfig.ExcludeProducts);
		}

		public override bool TryGet(out IProductAction entity)
		{
			if (IsValid())
			{
				if (IncludeAllProductsCheckbox.Checked)
				{
					State.CampaignConfig.IncludeProducts.Clear();
					State.CampaignConfig.IncludeCategories.Clear();
					State.CampaignConfig.IncludeBrands.Clear();
				}

				var action = State;

				action.DiscountAmount = decimal.Parse(DiscountPercentageTextBox.Text, CultureInfo.CurrentCulture);
				action.CampaignConfig.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;
				action.TargetProductTypes = TargetProductTypeSelectorControl.CollectSelectedValues();

				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			decimal discount;
			if (!decimal.TryParse(DiscountPercentageTextBox.Text, out discount))
			{
				isValid = false;
				ValidationMessages.Add(Resources.CampaignMessage.DiscountInvalid);
			}
			else if (discount < 0 || discount > 100)
			{
				isValid = false;
				ValidationMessages.Add(Resources.CampaignMessage.DiscountInvalidRange);
			}

			Collection<string> targetProductTypesValidationMessages;
			if (!TargetProductTypeSelectorControl.IsValid(out targetProductTypesValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in targetProductTypesValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmBrandRemove", "function ConfirmBrandRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteBrand + "');}", true);

			const string script = "$(function() {" +
								"ResizePopup('ActionPopupDiv', 0.8, 0.8,  0.06);" +
								"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('ProductExcludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('BrandIncludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('BrandExcludePopupDiv', 0.4, 0.45, 0.09);" +
								"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);

			string targetCollapsiblescript = "OpenClose('" + TargetPanelDiv.ClientID + "','" + TargetPanelStateHidden.ClientID + "','" + TargetPanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			TargetCollapsibleDiv.Attributes.Add("onclick", targetCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		protected virtual bool IsIncludeSubCategoriesIncludeGrid(object id)
		{
			return IsIncludeSubCategories(id, State.CampaignConfig.IncludeCategories);
		}
		protected virtual bool IsIncludeSubCategoriesExcludeGrid(object id)
		{
			return IsIncludeSubCategories(id, State.CampaignConfig.ExcludeCategories);
		}
		private bool IsIncludeSubCategories(object id, CampaignCategoryDictionary categories)
		{
			var categoryId = System.Convert.ToInt32(id, CultureInfo.CurrentCulture);
			bool include;
			return categories.TryGetValue(categoryId, out include) ? include : false;
		}

		protected virtual void OnSubCategoriesChangedIncludeGrid(object sender, EventArgs e)
		{
			CampaignConfiguratorIncludeCategory.OnSubCategoriesChanged(sender, e);
		}
		protected virtual void OnSubCategoriesChangedExcludeGrid(object sender, EventArgs e)
		{
			CampaignConfiguratorExcludeCategory.OnSubCategoriesChanged(sender, e);
		}

		protected virtual string GetCategoryPathIncludeGrid(object cat)
		{
			return CampaignConfiguratorIncludeCategory.GetCategoryPath(cat);
		}
		protected virtual string GetCategoryPathExcludeGrid(object cat)
		{
			return CampaignConfiguratorExcludeCategory.GetCategoryPath(cat);
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			BrandIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandIncludeGrid) ? "block" : "none";

			ProductExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			BrandExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandExcludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0 || BrandIncludeGrid.Rows.Count > 0 ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 || BrandExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}

		private void InitializeConfiguratorIncludeProducts()
		{
			CampaignConfiguratorIncludeProduct.Grid = ProductIncludeGrid;
			CampaignConfiguratorIncludeProduct.DataSource = IncludeProductDataSource;
			CampaignConfiguratorIncludeProduct.GridPageSizeSelect = ProductGridPageSizeSelect;
			CampaignConfiguratorIncludeProduct.SearchResultControl = ProductIncludeSearchResultControl;
			CampaignConfiguratorIncludeProduct.SearchFormControl = ProductIncludeSearchFormControl;
			CampaignConfiguratorIncludeProduct.SearchButton = ProductIncludePopupSearchButton;
			CampaignConfiguratorIncludeProduct.OkButton = ProductIncludePopupOkButton;
			CampaignConfiguratorIncludeProduct.AddAllButton = ProductIncludePopupAddAllButton;
			CampaignConfiguratorIncludeProduct.RemoveSelectionGridButton = RemoveSelectionFromProductIncludeGridButton;
			CampaignConfiguratorIncludeProduct.GridUpdatePanel = ProductIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeProduct.ApplyToAllDiv = ProductIncludeApplyToAllSelectedDiv;
			CampaignConfiguratorIncludeProduct.IncludeAllCheckbox = IncludeAllProductsCheckbox;
		}
		private void InitializeConfiguratorExcludeProducts()
		{
			CampaignConfiguratorExcludeProduct.Grid = ProductExcludeGrid;
			CampaignConfiguratorExcludeProduct.DataSource = ExcludeProductDataSource;
			CampaignConfiguratorExcludeProduct.GridPageSizeSelect = ProductGridPageSizeSelect;
			CampaignConfiguratorExcludeProduct.SearchResultControl = ProductExcludeSearchResultControl;
			CampaignConfiguratorExcludeProduct.SearchFormControl = ProductExcludeSearchFormControl;
			CampaignConfiguratorExcludeProduct.SearchButton = ProductExcludePopupSearchButton;
			CampaignConfiguratorExcludeProduct.OkButton = ProductExcludePopupOkButton;
			CampaignConfiguratorExcludeProduct.AddAllButton = ProductExcludePopupAddAllButton;
			CampaignConfiguratorExcludeProduct.RemoveSelectionGridButton = RemoveSelectionFromProductExcludeGridButton;
			CampaignConfiguratorExcludeProduct.GridUpdatePanel = ProductExcludeGridUpdatePanel;
			CampaignConfiguratorExcludeProduct.ApplyToAllDiv = ProductExcludeApplyToAllSelectedDiv;
			CampaignConfiguratorExcludeProduct.IncludeAllCheckbox = null;
		}

		private void InitializeConfiguratorIncludeCategories()
		{
			CampaignConfiguratorIncludeCategory.Grid = CategoryIncludeGrid;
			CampaignConfiguratorIncludeCategory.OkButton = CategoryIncludePopupOkButton;
			CampaignConfiguratorIncludeCategory.RemoveSelectionGridButton = RemoveSelectionFromCategoryIncludeGridButton;
			CampaignConfiguratorIncludeCategory.GridUpdatePanel = CategoryIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeCategory.PopupCategoryTree = CategoryIncludePopupCategoryTree;
			CampaignConfiguratorIncludeCategory.ApplyToAllDiv = CategoryIncludeApplyToAllSelectedDiv;
			CampaignConfiguratorIncludeCategory.IncludeAllCheckbox = IncludeAllProductsCheckbox;
		}
		private void InitializeConfiguratorExcludeCategories()
		{
			CampaignConfiguratorExcludeCategory.Grid = CategoryExcludeGrid;
			CampaignConfiguratorExcludeCategory.OkButton = CategoryExcludePopupOkButton;
			CampaignConfiguratorExcludeCategory.RemoveSelectionGridButton = RemoveSelectionFromCategoryExcludeGridButton;
			CampaignConfiguratorExcludeCategory.GridUpdatePanel = CategoryExcludeGridUpdatePanel;
			CampaignConfiguratorExcludeCategory.PopupCategoryTree = CategoryExcludePopupCategoryTree;
			CampaignConfiguratorExcludeCategory.ApplyToAllDiv = CategoryExcludeApplyToAllSelectedDiv;
			CampaignConfiguratorExcludeCategory.IncludeAllCheckbox = null;
		}

		private void InitializeConfiguratorIncludeBrands()
		{
			CampaignConfiguratorIncludeBrand.Grid = BrandIncludeGrid;
			CampaignConfiguratorIncludeBrand.SearchGrid = BrandIncludeSearchGrid;
			CampaignConfiguratorIncludeBrand.OkButton = BrandIncludePopupOkButton;
			CampaignConfiguratorIncludeBrand.RemoveSelectionGridButton = RemoveSelectionFromBrandIncludeGridButton;
			CampaignConfiguratorIncludeBrand.GridUpdatePanel = BrandIncludeGridUpdatePanel;
			CampaignConfiguratorIncludeBrand.PopupUpdatePanel = BrandIncludePopupUpdatePanel;
			CampaignConfiguratorIncludeBrand.ApplyToAllDiv = BrandIncludeApplyToAllSelectedDiv;
			CampaignConfiguratorIncludeBrand.IncludeAllCheckbox = IncludeAllProductsCheckbox;
		}
		private void InitializeConfiguratorExcludeBrands()
		{
			CampaignConfiguratorExcludeBrand.Grid = BrandExcludeGrid;
			CampaignConfiguratorExcludeBrand.SearchGrid = BrandExcludeSearchGrid;
			CampaignConfiguratorExcludeBrand.OkButton = BrandExcludePopupOkButton;
			CampaignConfiguratorExcludeBrand.RemoveSelectionGridButton = RemoveSelectionFromBrandExcludeGridButton;
			CampaignConfiguratorExcludeBrand.GridUpdatePanel = BrandExcludeGridUpdatePanel;
			CampaignConfiguratorExcludeBrand.PopupUpdatePanel = BrandExcludePopupUpdatePanel;
			CampaignConfiguratorExcludeBrand.ApplyToAllDiv = BrandExcludeApplyToAllSelectedDiv;
			CampaignConfiguratorExcludeBrand.IncludeAllCheckbox = null;
		}
	}
}