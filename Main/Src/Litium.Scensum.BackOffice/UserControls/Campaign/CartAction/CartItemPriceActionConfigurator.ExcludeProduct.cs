﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartItemPriceAction;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemPriceActionConfigurator
	{
		private void SetExcludeProductEventHandlers()
		{
			ProductExcludePopupSearchButton.Click += OnProductExcludeSearch;
			ProductExcludeGrid.PageIndexChanging += ProductExcludeGridPageIndexChanging;
			ProductExcludeGrid.RowDataBound += OnProductExcludeGridRowDataBound;
			ProductExcludeGrid.RowCommand += OnProductExcludeGridRowCommand;
			ProductExcludePopupOkButton.Click += OnExcludeProducts;
			ProductExcludePopupAddAllButton.Click += OnExcludeAllProducts;
			RemoveSelectionFromProductExcludeGridButton.Click += OnRemoveSelectionFromProductExcludeGrid;
		}

		private void DataBindExcludeProducts(ICartItemPriceAction action)
		{
			DataBindProductExcludeGrid(action.ExcludeProducts);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "criteria")]
		protected virtual void OnProductExcludeSearch(object sender, EventArgs e)
		{
			if (ProductExcludeSearchFormControl.Validate())
			{
				var criteria = ProductExcludeSearchFormControl.CreateSearchCriteria();
				ProductExcludeSearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void ProductExcludeGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductExcludeGrid.PageIndex = e.NewPageIndex;
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		protected virtual void OnProductExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ProductExcludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnProductExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				RemoveExcludedProduct(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveExcludedProduct(int productId)
		{
			State.ExcludeProducts.Remove(productId);
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		protected virtual void OnExcludeProducts(object sender, EventArgs e)
		{
			var products = ProductExcludeSearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (!State.ExcludeProducts.ContainsKey(product.Id))
				{
					State.ExcludeProducts.Add(product.Id);
				}
			}
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		protected virtual void OnExcludeAllProducts(object sender, EventArgs e)
		{
			if (ProductExcludeSearchFormControl.Validate())
			{
				var productSecureService = IoC.Resolve<IProductSecureService>();

				var criteriaList = new List<IProductSearchCriteria>();
				var criteria = ProductExcludeSearchFormControl.CreateSearchCriteria();
				criteriaList.Add(criteria);
				var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
				var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

				var products = productSecureService.AdvancedSearch(
					ChannelHelper.CurrentChannel.Id,
					searchCriteriaInclusive,
					searchCriteriaExclusive,
					1,
					int.MaxValue);

				foreach (var product in products)
				{
					if (!State.ExcludeProducts.ContainsKey(product.Id))
					{
						State.ExcludeProducts.Add(product.Id);
					}
				}
				DataBindProductExcludeGrid(State.ExcludeProducts);
			}
		}

		protected virtual void OnRemoveSelectionFromProductExcludeGrid(object sender, EventArgs e)
		{
			foreach (var productId in GetSelectedProductsFromExcludeGrid())
			{
				State.ExcludeProducts.Remove(productId);
			}
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedProductsFromExcludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(ProductExcludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindProductExcludeGrid(ProductIdDictionary productIds)
		{
			ProductExcludeGrid.PageSize = Convert.ToInt32(ProductGridPageSizeSelect.SelectedValue);

			ExcludeProductDataSource.SelectParameters.Clear();
			ExcludeProductDataSource.SelectParameters.Add("maximumRows", ProductGridPageSizeSelect.SelectedValue.ToString(CultureInfo.CurrentCulture));
			ExcludeProductDataSource.SelectParameters.Add("startRowIndex", ProductExcludeGrid.PageIndex.ToString(CultureInfo.CurrentCulture));
			ExcludeProductDataSource.SelectParameters.Add("productIdsString", Foundation.Convert.ToStringIdentifierList(productIds.Keys.ToList()));

			ProductExcludeGrid.DataBind();
			ProductExcludeGridUpdatePanel.Update();
		}
	}
}
