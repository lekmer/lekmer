﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class ProductAutoFreeActionConfigurator : CartActionControl<IProductAutoFreeCartAction>
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get
			{
				if (_campaignConfiguratorHelper == null)
				{
					_campaignConfiguratorHelper = new CampaignConfiguratorHelper();
				}

				return _campaignConfiguratorHelper;
			}
		}

		protected override void SetEventHandlers()
		{
			SetIncludeProductEventHandlers();
		}

		protected override void PopulateControl() { }

		public override void DataBind(ICartAction dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var action = (IProductAutoFreeCartAction)dataSource;
			State = action;

			MaxQuantityTextBox.Text = State.Quantity.ToString(CultureInfo.CurrentCulture);

			DataBindIncludeProducts(action);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		public override bool TryGet(out ICartAction entity)
		{
			if (IsValid())
			{
				var action = State;
				action.Quantity  = int.Parse(MaxQuantityTextBox.Text, CultureInfo.CurrentCulture);

				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			if (State.ProductId <= 0)
			{
				isValid = false;
				ValidationMessages.Add(Resources.CampaignMessage.Message_SelectFreeProduct);
			}

			int maxQuantity;
			if (!int.TryParse(MaxQuantityTextBox.Text, out maxQuantity) || maxQuantity < 0)
			{
				isValid = false;
				ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.CartItemPriceAction.Message_MaxQuantityInvalid, 0, int.MaxValue));
			}

			return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);

			const string script = "$(function() {" +
								  "ResizePopup('ActionPopupDiv', 0.75, 0.3, 0.14);" +
								  "ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								  "});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
			{
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();
			}

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
		}
	}
}