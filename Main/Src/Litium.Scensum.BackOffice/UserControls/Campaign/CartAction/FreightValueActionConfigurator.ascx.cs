﻿using System;
using System.Collections.ObjectModel;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class FreightValueActionConfigurator :
		CartActionControl<ILekmerFreightValueAction>
	{
		protected override void SetEventHandlers() { }
		protected override void PopulateControl() { }

		public override void DataBind(ICartAction dataSource)
		{
			var freightValueAction = (ILekmerFreightValueAction)dataSource;

			AmountsManager.DataSource = freightValueAction.Amounts;
			AmountsManager.DataBind();

			DeliveryMethodsSelectorControl.DataSource = freightValueAction.DeliveryMethodIds;
			DeliveryMethodsSelectorControl.DataBind();

			State = freightValueAction;
		}

		public override bool TryGet(out ICartAction entity)
		{
			if (IsValid())
			{
				var action = State;

				action.Amounts = AmountsManager.CollectCurrencyValues();
				action.DeliveryMethodIds = DeliveryMethodsSelectorControl.CollectSelectedValues();

				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			Collection<string> amountsValidationMessages;
			if (!AmountsManager.IsValid(out amountsValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in amountsValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			Collection<string> deliveryMethodsValidationMessages;
			if (!DeliveryMethodsSelectorControl.IsValid(out deliveryMethodsValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in deliveryMethodsValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
		}

		protected virtual void RegisterClientScript()
		{
			//string script = "$(function() { ResizePopup('ActionPopupDiv', 0.2, 0.4, 0.1); });";

			//ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);

			const string script = "$(function() {" +
				"var popupDiv = $(\"div[id$='ActionPopupDiv']\");" +
				"popupDiv.attr('class','campaign-popup-container small-campaign-popup-container campaign-action-freight-value-popup-container');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-header-center']:first\").attr('class','campaign-popup-header-center small-campaign-popup-header-center campaign-action-freight-value-campaign-popup-header-center');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-content']:first\").attr('class','campaign-popup-content small-campaign-popup-content campaign-action-freight-value-popup-content');" +
				"});";

			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_fvac_smallPopup", script, true);
		}
	}
}