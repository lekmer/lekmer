using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using CustomButton = Litium.Scensum.Web.Controls.Button.ImageLinkButton;
using GridControl = System.Web.UI.WebControls.GridView;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Helper
{
	public class CampaignConfiguratorSupplier
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		private CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		private Collection<ISupplier> _supplierCollection;
		public Collection<ISupplier> SupplierCollection
		{
			get { return _supplierCollection ?? (_supplierCollection = IoC.Resolve<ISupplierSecureService>().GetAll()); }
		}

		public GridControl Grid { get; set; }
		public GridControl SearchGrid { get; set; }
		public CustomButton OkButton { get; set; }
		public CustomButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public UpdatePanel PopupUpdatePanel { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public IdDictionary Suppliers { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;

			SearchGrid.RowDataBound += SearchGridRowDataBound;

			OkButton.Click += OkButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(IdDictionary suppliers)
		{
			DataBindSearchGrid();
			DataBindGrid(CampaignConfiguratorHelper.ResolveSuppliers(suppliers));
		}

		public virtual void DataBindGrid(Collection<ISupplier> suppliers)
		{
			Grid.DataSource = suppliers;
			Grid.DataBind();
			GridUpdatePanel.Update();
		}

		protected virtual void DataBindSearchGrid()
		{
			SearchGrid.DataSource = SupplierCollection;
			SearchGrid.DataBind();
			PopupUpdatePanel.Update();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ApplyToAllDiv, false);
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveSupplier"))
			{
				RemoveSupplier(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void SearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, null, false);
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			var supplierIds = CampaignConfiguratorHelper.GetSelectedIdsFromGrid(SearchGrid, "IdHiddenField");

			foreach (var supplierId in supplierIds)
			{
				if (!Suppliers.ContainsKey(supplierId))
				{
					Suppliers.Add(supplierId);
				}
			}

			DataBindGrid(CampaignConfiguratorHelper.ResolveSuppliers(Suppliers));

			if (IncludeAllCheckbox != null && Suppliers.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}

			CampaignConfiguratorHelper.ClearSelectedIdsFromGrid(SearchGrid);
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			foreach (var supplierId in GetSelectedFromGrid())
			{
				Suppliers.Remove(supplierId);
			}

			DataBindGrid(CampaignConfiguratorHelper.ResolveSuppliers(Suppliers));
		}

		protected virtual void RemoveSupplier(int supplierId)
		{
			Suppliers.Remove(supplierId);

			DataBindGrid(CampaignConfiguratorHelper.ResolveSuppliers(Suppliers));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}
	}
}