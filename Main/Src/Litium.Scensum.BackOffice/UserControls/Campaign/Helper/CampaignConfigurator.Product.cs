using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.BackOffice.UserControls.Assortment;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using CustomButton = Litium.Scensum.Web.Controls.Button.ImageLinkButton;
using GridControl = System.Web.UI.WebControls.GridView;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Helper
{
	public class CampaignConfiguratorProduct
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		private CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		public GridControl Grid { get; set; }
		public ObjectDataSource DataSource { get; set; }
		public DropDownList GridPageSizeSelect { get; set; }
		public ProductSearchResult SearchResultControl { get; set; }
		public ProductSearchForm SearchFormControl { get; set; }
		public CustomButton SearchButton { get; set; }
		public CustomButton OkButton { get; set; }
		public CustomButton AddAllButton { get; set; }
		public CustomButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public ProductIdDictionary Products { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;
			Grid.PageIndexChanging += GridPageIndexChanging;
			SearchButton.Click += SearchButtonClick;
			OkButton.Click += OkButtonClick;
			AddAllButton.Click += AddAllButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(ProductIdDictionary products)
		{
			DataBindGrid(products);
		}

		public virtual void DataBindGrid(ProductIdDictionary productIds)
		{
			Grid.PageSize = System.Convert.ToInt32(GridPageSizeSelect.SelectedValue);

			DataSource.SelectParameters.Clear();
			DataSource.SelectParameters.Add("maximumRows", GridPageSizeSelect.SelectedValue.ToString(CultureInfo.CurrentCulture));
			DataSource.SelectParameters.Add("startRowIndex", Grid.PageIndex.ToString(CultureInfo.CurrentCulture));
			DataSource.SelectParameters.Add("productIdsString", Foundation.Convert.ToStringIdentifierList(productIds.Keys.ToList()));

			Grid.DataBind();
			GridUpdatePanel.Update();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ApplyToAllDiv, false);
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				RemoveProduct(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void GridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			Grid.PageIndex = e.NewPageIndex;
			DataBindGrid(Products);
		}

		protected virtual void SearchButtonClick(object sender, EventArgs e)
		{
			if (SearchFormControl.Validate())
			{
				var criteria = SearchFormControl.CreateSearchCriteria();
				SearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			var products = SearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (!Products.ContainsKey(product.Id))
				{
					Products.Add(product.Id);
				}
			}

			DataBindGrid(Products);

			if (IncludeAllCheckbox != null && products.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}
		}

		private void AddAllButtonClick(object sender, EventArgs e)
		{
			if (SearchFormControl.Validate())
			{
				var productSecureService = IoC.Resolve<IProductSecureService>();

				var criteriaList = new List<IProductSearchCriteria>();
				var criteria = SearchFormControl.CreateSearchCriteria();
				criteriaList.Add(criteria);
				var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
				var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

				var products = productSecureService.AdvancedSearch(
					ChannelHelper.CurrentChannel.Id,
					searchCriteriaInclusive,
					searchCriteriaExclusive,
					1,
					int.MaxValue);

				foreach (var product in products)
				{
					if (!Products.ContainsKey(product.Id))
					{
						Products.Add(product.Id);
					}
				}

				DataBindGrid(Products);

				if (products.Count > 0 && IncludeAllCheckbox != null)
				{
					IncludeAllCheckbox.Checked = false;
				}
			}
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			foreach (var productId in GetSelectedFromGrid())
			{
				Products.Remove(productId);
			}

			DataBindGrid(Products);
		}

		protected virtual void RemoveProduct(int productId)
		{
			Products.Remove(productId);
			DataBindGrid(Products);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}
	}
}