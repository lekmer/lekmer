using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Resources;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class VoucherConditionConfigurator
	{
		protected virtual void SetEventHandlersExclude()
		{
			BatchExcludeGrid.RowDataBound += OnBatchExcludeGridRowDataBound;
			BatchExcludeGrid.RowCommand += OnBatchExcludeGridRowCommand;
			OpenBatchExcludeSearchPopupButton.Click += OpenBatchExcludeSearchPopupButtonOnClick;

			RemoveSelectionFromBatchExcludeGridButton.Click += OnRemoveSelectionFromBatchExcludeGrid;
			BatchExcludeSaveButton.Click += BatchExcludeSaveButtonOnClick;
		}


		protected virtual void BatchExcludeSaveButtonOnClick(object sender, EventArgs e)
		{
			int batchId = int.Parse(BatchIdExcludeTextBox.Text.Trim(), CultureInfo.CurrentCulture);
			foreach (int id in State.BatchIdsExclude)
			{
				if (id == batchId)
				{
					BatchExcludeMessageContainer.Add(LekmerMessage.ExistBatchId);
					BatchExcludePopup.Show();
					return;
				}
			}
			BatchIdExcludeTextBox.Text = string.Empty;
			State.BatchIdsExclude.Add(batchId);
			DataBindBatchExcludeGrid(State.BatchIdsExclude);
			BatchExcludePopup.Hide();
		}

		protected virtual void OpenBatchExcludeSearchPopupButtonOnClick(object sender, EventArgs e)
		{
			BatchExcludeUpdatePanel.Update();
			BatchExcludePopup.Show();
		}

		protected virtual void OnRemoveSelectionFromBatchExcludeGrid(object sender, EventArgs e)
		{
			foreach (var batchId in GetSelectedBatchsFromExcludeGrid())
			{
				State.BatchIdsExclude.Remove(batchId);
			}

			DataBindBatchExcludeGrid(State.BatchIdsExclude);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
		protected virtual void OnBatchExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveBatchExclude"))
			{
				RemoveBatchExclude(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void OnBatchExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, BatchExcludeApplyToAllSelectedDiv, true);
		}


		protected virtual void RemoveBatchExclude(int batchId)
		{
			State.BatchIdsExclude.Remove(batchId);
			DataBindBatchExcludeGrid(State.BatchIdsExclude);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedBatchsFromExcludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(BatchExcludeGrid, "BatchIdHiddenField");
		}

		protected virtual void DataBindBatchExcludeGrid(Collection<int> batchIds)
		{
			BatchExcludeGrid.DataSource = batchIds;
			BatchExcludeGrid.DataBind();
			BatchExcludeGridUpdatePanel.Update();
		}

		protected virtual void SetVisibilityExclude()
		{
			BatchExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BatchExcludeGrid) ? "block" : "none";
		}

		protected virtual void RegisterCollapsibleExcludePanelScripts()
		{
			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);
		}
	}
}