﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="CartValueRangeConditionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.Condition.CartValueRangeConditionConfigurator" %>

<script type="text/javascript">
	$(function () {
		$("div[id$='ConditionPopupDiv']").attr('class', 'campaign-popup-container');
		ResizePopup('ConditionPopupDiv', 0.2, 0.5, 0.09);
		$("div[class^='campaign-popup-content']").css('overflow-y', 'scroll');
	});
</script>

<br style="clear:both;" />

<div class="currencyvalue-wrapper">
	<asp:Repeater runat="server" ID="CurrencyValuesRepeater">
		<HeaderTemplate>
			<div class="currencyvalue-header-wrapper">
				<span class="bold">
					<%=Resources.Campaign.Literal_ValueFrom%>
				</span>
				<span class="bold" style="margin-left: 25px; margin-right: 40px;">
					<%=Resources.Campaign.Literal_ValueTo%>
				</span>
				<span class="bold">
					<%=Resources.General.Literal_Currency%>
				</span>
			</div>
		</HeaderTemplate>
		<ItemTemplate>
			<div class="currencyvalue-content">
				<asp:TextBox runat="server" ID="ValueFromTextBox" Text='<%# Eval("MonetaryValueFrom") %>' Width="60px" />
				<asp:TextBox runat="server" ID="ValueToTextBox" Text='<%# Eval("MonetaryValueTo") %>' Width="60px" style="margin-left: 25px; margin-right: 25px;" />
				<asp:Literal runat="server" ID="CurrencyIsoLiteral" Text='<%# Eval("CurrencyIsoWithChannels") %>' />
				<asp:HiddenField runat="server" ID="CurrencyIdHiddenField" Value='<%# Eval("Currency.Id") %>' />
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>