using System.Collections.ObjectModel;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
    public interface ICampaignPluginControl<T>
    {
        Collection<string> ValidationMessages { get; }
        string AdditionalValidationMessage { get; }
        void DataBind(T dataSource);
        bool TryGet(out T entity);
    }
}