﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConditionList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.ConditionList" %>
<div class="campaign-action-grid">
	<asp:GridView ID="ConditionsGrid" AutoGenerateColumns="false" OnRowDataBound="OnGridRowDataBound" runat="server" SkinID="campaign-actions-grid" Width="100%" BorderWidth="0" CellPadding="0">
		<Columns>
			<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>">
				<ItemTemplate>
					<asp:LinkButton ID="EditButton" CommandName="Configure" runat="server"></asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Description %>">
				<ItemTemplate>
					<asp:Literal ID="DescriptionLiteral" runat="server"></asp:Literal>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-HorizontalAlign="Right" ItemStyle-Width="15px">
				<ItemTemplate>
					<asp:ImageButton ID="RemoveButton" runat="server" CommandName="Remove" 
						ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmActionDelete();" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	</asp:GridView>
</div>