﻿using System.Web.UI.WebControls;
using System.Web.UI;
using System.Web;
using System;

namespace Litium.Scensum.BackOffice.UserControls
{
	public class ExtendedDropDownList : DropDownList
	{
		private ExtendedListItemCollection _extendedItems;
		internal const string _optGroupAttributeKey = "optgroup";
		internal const string _separator = "#";
		bool _optGroupStarted = false;
		bool _selected = false;

		public ExtendedListItemCollection ExtendedItems
		{
			get
			{
				if (null == _extendedItems)
				{
					_extendedItems = new ExtendedListItemCollection(base.Items);
				}
				return _extendedItems;
			}
		}

		public new ListItemCollection Items
		{
			get
			{
				if (null == _extendedItems)
				{
					_extendedItems = new ExtendedListItemCollection(base.Items);
				}
				return _extendedItems._wrappedCollection;
			}
		}

		public ExtendedListItem SelectedItemExtended
		{
			get { return new ExtendedListItem(SelectedItem); }
		}

		public string SelectedGroup
		{
			get
			{
				ExtendedListItem item = SelectedItemExtended;

				if (item.GroupingType == ListItemGroupingType.New)
				{
					return item.GroupingText;
				}
				else if (item.GroupingType == ListItemGroupingType.None)
				{
					return string.Empty;
				}
				else
				{
					int tmpIndex = ExtendedItems.IndexOf(item) - 1;
					while (tmpIndex >= 0)
					{
						if (ExtendedItems[tmpIndex].GroupingType == ListItemGroupingType.New)
						{
							return ExtendedItems[tmpIndex].GroupingText;
						}
						tmpIndex--;
					}
					return string.Empty;
				}
			}
		}

		protected override object SaveViewState()
		{
			var state = new object[Items.Count + 1];

			object baseState = base.SaveViewState();
			state[0] = baseState;

			bool itemHasAttributes = false;
			for (int i = 0; i < Items.Count; i++)
			{
				if (Items[i].Attributes.Count > 0)
				{
					itemHasAttributes = true;

					object[] attributes = new object[Items[i].Attributes.Count * 2];
					int k = 0;
					foreach (string key in Items[i].Attributes.Keys)
					{
						attributes[k++] = key;
						attributes[k++] = Items[i].Attributes[key];
					}

					state[i + 1] = attributes;
				}
			}

			return itemHasAttributes ? state : baseState;
		}

		protected override void LoadViewState(object savedState)
		{
			if (savedState == null)
			{
				return;
			}

			var state = savedState as object[];
			if (state != null)
			{
				base.LoadViewState(state[0]);
				for (int i = 1; i < state.Length; i++)
				{
					if (state[i] != null)
					{
						var attributes = (object[])state[i];
						for (int k = 0; k < attributes.Length; k += 2)
						{
							Items[i - 1].Attributes.Add(attributes[k].ToString(), attributes[k + 1].ToString());
						}
					}
				}
			}
			else
			{
				base.LoadViewState(savedState);
			}
		}

		protected override void RenderContents(HtmlTextWriter writer)
		{
			if (Items.Count == 0)
			{
				return;
			}
			for (int i = 0; i < ExtendedItems.Count; i++)
			{
				ExtendedListItem item = ExtendedItems[i];

				if (item.GroupingType == ListItemGroupingType.New)
				{
					WriteOptionGroup(item, writer);
					WriteOption(item, writer);
				}
				else if (item.GroupingType == ListItemGroupingType.Inherit)
				{
					WriteOption(item, writer);
				}
				else
				{
					if (_optGroupStarted)
					{
						writer.WriteEndTag("optgroup");
					}
					_optGroupStarted = false;
					WriteOption(item, writer);
				}
			}
			if (_optGroupStarted)
			{
				writer.WriteEndTag("optgroup");
			}
		}

		private void WriteOptionGroup(ExtendedListItem item, HtmlTextWriter writer)
		{
			if (_optGroupStarted)
			{
				writer.WriteEndTag("optgroup");
			}
			writer.WriteBeginTag("optgroup");
			writer.WriteAttribute("label", item.GroupingText);
			if (!item.Enabled)
			{
				writer.WriteAttribute("disabled", "disabled");
			}
			if (!string.IsNullOrEmpty(item.GroupCssClass))
			{
				writer.WriteAttribute("class", item.GroupCssClass);
			}
			writer.Write('>');
			_optGroupStarted = true;
		}

		private void WriteOption(ExtendedListItem item, HtmlTextWriter writer)
		{
			writer.WriteBeginTag("option");
			if (item.Selected)
			{
				if (_selected)
				{
					VerifyMultiSelect();
				}
				_selected = true;
				writer.WriteAttribute("selected", "selected");
			}

			if (!string.IsNullOrEmpty(item.CssClass))
			{
				writer.WriteAttribute("class", item.CssClass);
			}

			writer.WriteAttribute("value", item.Value, true);

			if (item.Attributes.Count > 0)
			{
				var bag = new StateBag();
				foreach (string attrKey in item.Attributes.Keys)
				{
					if (attrKey.IndexOf(ExtendedListItem._attrPrefix, StringComparison.OrdinalIgnoreCase) == -1)
					{
						bag.Add(attrKey, item.Attributes[attrKey]);
					}
				}
				var attributes = new AttributeCollection(bag);
				attributes.Render(writer);
			}

			if (Page != null)
			{
				Page.ClientScript.RegisterForEventValidation(UniqueID, item.Value);
			}

			writer.Write('>');
			HttpUtility.HtmlEncode(item.Text, writer);
			writer.WriteEndTag("option");
			writer.WriteLine();
		}
	}
}
