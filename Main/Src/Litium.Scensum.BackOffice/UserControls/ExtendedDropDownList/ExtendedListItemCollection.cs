using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Web;
using System.Security.Permissions;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Collections.ObjectModel;

namespace Litium.Scensum.BackOffice.UserControls
{
    /// <summary>
    /// Wrapper over ListItemCollection containing OptGroupListItems.
    /// </summary>
	[SuppressMessage("Microsoft.Design", "CA1010:CollectionsShouldImplementGenericInterface")]
	public sealed class ExtendedListItemCollection : IList, ICollection, IEnumerable, IStateManager
    {
        #region Private members
        internal readonly ListItemCollection _wrappedCollection; 
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedListItemCollection"/> class.
        /// </summary>
        /// <param name="wrappedCollection">The wrapped collection.</param>
        public ExtendedListItemCollection(ListItemCollection wrappedCollection)
        {
            if (null == wrappedCollection)
            {
                throw new ArgumentNullException("wrappedCollection");
            }
            _wrappedCollection = wrappedCollection;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExtendedListItemCollection"/> class.
        /// </summary>
        public ExtendedListItemCollection()
        {
            _wrappedCollection = new ListItemCollection();
        } 
        #endregion

        #region Public methods
        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Add(string item)
        {
            _wrappedCollection.Add(item);
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Add(ExtendedListItem item)
        {
            _wrappedCollection.Add(GetSafeWrappedItem(item));
        }

        /// <summary>
        /// Adds the range.
        /// </summary>
        /// <param name="items">The items.</param>
        public void AddRange(ExtendedListItem[] items)
        {
            if (null != items)
            {
                ListItem[] listItems = new ListItem[items.Length];
                for (int i = 0; i < items.Length; i++)
                {
                    listItems[i] = items[i]._listItem;
                }
                _wrappedCollection.AddRange(listItems);
            }
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.IList"/>.
        /// </summary>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.IList"/> is read-only. </exception>
        public void Clear()
        {
            _wrappedCollection.Clear();
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        public bool Contains(ExtendedListItem item)
        {
            return _wrappedCollection.Contains(GetSafeWrappedItem(item));
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection"/> to an <see cref="T:System.Array"/>, starting at a particular <see cref="T:System.Array"/> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array"/> that is the destination of the elements copied from <see cref="T:System.Collections.ICollection"/>. The <see cref="T:System.Array"/> must have zero-based indexing.</param>
        /// <param name="index">The zero-based index in <paramref name="array"/> at which copying begins.</param>
        /// <exception cref="T:System.ArgumentNullException">
        /// 	<paramref name="array"/> is null. </exception>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is less than zero. </exception>
        /// <exception cref="T:System.ArgumentException">
        /// 	<paramref name="array"/> is multidimensional.-or- <paramref name="index"/> is equal to or greater than the length of <paramref name="array"/>.-or- The number of elements in the source <see cref="T:System.Collections.ICollection"/> is greater than the available space from <paramref name="index"/> to the end of the destination <paramref name="array"/>. </exception>
        /// <exception cref="T:System.ArgumentException">The type of the source <see cref="T:System.Collections.ICollection"/> cannot be cast automatically to the type of the destination <paramref name="array"/>. </exception>
        public void CopyTo(Array array, int index)
        {
            List<ExtendedListItem> list = ToList();
            Array.Copy(list.ToArray(), 0, array, index, list.Count);
        }

		public void CopyTo(Collection<ExtendedListItem> list, int index)
		{
			CopyTo(list, index);
		}

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator"/> object that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return ToList().GetEnumerator();
        }

        /// <summary>
        /// Indexes the of.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public int IndexOf(ExtendedListItem item)
        {
            return _wrappedCollection.IndexOf(GetSafeWrappedItem(item));
        }

        /// <summary>
        /// Inserts the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        public void Insert(int index, string item)
        {
            _wrappedCollection.Insert(index, item);
        }

        /// <summary>
        /// Inserts the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        public void Insert(int index, ExtendedListItem item)
        {
            _wrappedCollection.Insert(index, GetSafeWrappedItem(item));
        } 
        #endregion

        #region Internal behaviour
        /// <summary>
        /// When implemented by a class, loads the server control's previously saved view state to the control.
        /// </summary>
        /// <param name="state">An <see cref="T:System.Object"/> that contains the saved view state values for the control.</param>
        internal void LoadViewState(object state)
        {
            if (null != state)
            {
                object[] s = (object[])state;

                // first obj is the wrapped state

                ((IStateManager)_wrappedCollection).LoadViewState(s[0]);

                // second is a list of grouping types
                IList<ListItemGroupingType> types = s[1] as IList<ListItemGroupingType>;

                // third is a list of grouping texts
                IList<string> texts = s[2] as IList<string>;

                // restore grouping type and text
                if ((null != types) && (types.Count == _wrappedCollection.Count) && (null != texts) && (texts.Count == _wrappedCollection.Count))
                {
                    for (int i = 0; i < _wrappedCollection.Count; i++)
                    {
                        this[i].GroupingType = types[i];
                        this[i].GroupingText = texts[i];
                    }
                }
            }
        }

        /// <summary>
        /// When implemented by a class, saves the changes to a server control's view state to an <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Object"/> that contains the view state changes.
        /// </returns>
        internal object SaveViewState()
        {
            object[] state = new object[3];

            // first obj is the wrapped state
            object wrappedViewState = SaveViewState();

            IList<ExtendedListItem> items = ToList();

            // second is a list of grouping types
            IList<ListItemGroupingType> listItemGroupingTypes = new List<ListItemGroupingType>();
            foreach (ExtendedListItem item in items)
            {
                listItemGroupingTypes.Add(item.GroupingType);
            }

            // third is a list of grouping texts
            IList<string> listItemGroupingTexts = new List<string>();
            foreach (ExtendedListItem item in items)
            {
                listItemGroupingTexts.Add(item.GroupingText);
            }

            state[0] = wrappedViewState;
            state[1] = listItemGroupingTypes;
            state[2] = listItemGroupingTexts;

            return state;
        }

        /// <summary>
        /// When implemented by a class, instructs the server control to track changes to its view state.
        /// </summary>
        internal void TrackViewState()
        {
            ((IStateManager)_wrappedCollection).TrackViewState();
        }


        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Remove(string item)
        {
            _wrappedCollection.Remove(item);
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Remove(ExtendedListItem item)
        {
            _wrappedCollection.Remove(GetSafeWrappedItem(item));
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.IList"/> item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        /// <exception cref="T:System.ArgumentOutOfRangeException">
        /// 	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.IList"/>. </exception>
        /// <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.IList"/> is read-only.-or- The <see cref="T:System.Collections.IList"/> has a fixed size. </exception>
        public void RemoveAt(int index)
        {
            _wrappedCollection.RemoveAt(index);
        }

        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int IList.Add(object item)
        {
            return ((IList)_wrappedCollection).Add((object)GetSafeWrappedItem((ExtendedListItem)item));
        }

        /// <summary>
        /// Determines whether [contains] [the specified item].
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified item]; otherwise, <c>false</c>.
        /// </returns>
        bool IList.Contains(object item)
        {
            return ((IList)_wrappedCollection).Contains((object)GetSafeWrappedItem((ExtendedListItem)item));
        }

        /// <summary>
        /// Indexes the of.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        int IList.IndexOf(object item)
        {
            return ((IList)_wrappedCollection).IndexOf((object)GetSafeWrappedItem((ExtendedListItem)item));
        }

        /// <summary>
        /// Inserts the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="item">The item.</param>
        void IList.Insert(int index, object item)
        {
            ((IList)_wrappedCollection).Insert(index, (object)GetSafeWrappedItem((ExtendedListItem)item));
        }

        /// <summary>
        /// Removes the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        void IList.Remove(object item)
        {
            ((IList)this).Remove((object)GetSafeWrappedItem((ExtendedListItem)item));
        }

        /// <summary>
        /// When implemented by a class, loads the server control's previously saved view state to the control.
        /// </summary>
        /// <param name="state">An <see cref="T:System.Object"/> that contains the saved view state values for the control.</param>
        void IStateManager.LoadViewState(object state)
        {
            LoadViewState(state);
        }

        /// <summary>
        /// When implemented by a class, saves the changes to a server control's view state to an <see cref="T:System.Object"/>.
        /// </summary>
        /// <returns>
        /// The <see cref="T:System.Object"/> that contains the view state changes.
        /// </returns>
        object IStateManager.SaveViewState()
        {
            return SaveViewState();
        }

        /// <summary>
        /// When implemented by a class, instructs the server control to track changes to its view state.
        /// </summary>
        void IStateManager.TrackViewState()
        {
            TrackViewState();
        }

        /// <summary>
        /// Toes the list.
        /// </summary>
        /// <returns></returns>
        private List<ExtendedListItem> ToList()
        {
            List<ExtendedListItem> list = new List<ExtendedListItem>();
            foreach (ListItem wrappedItem in _wrappedCollection)
            {
                list.Add(new ExtendedListItem(wrappedItem));
            }
            return list;
        } 
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the capacity.
        /// </summary>
        /// <value>The capacity.</value>
        public int Capacity
        {
            get { return _wrappedCollection.Capacity; }
        }

        /// <summary>
        /// Gets the number of elements contained in the <see cref="T:System.Collections.ICollection"/>.
        /// </summary>
        /// <value></value>
        /// <returns>The number of elements contained in the <see cref="T:System.Collections.ICollection"/>.</returns>
        public int Count
        {
            get { return _wrappedCollection.Count; }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList"/> is read-only.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.IList"/> is read-only; otherwise, false.</returns>
        public bool IsReadOnly
        {
            get { return _wrappedCollection.IsReadOnly; }
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection"/> is synchronized (thread safe).
        /// </summary>
        /// <value></value>
        /// <returns>true if access to the <see cref="T:System.Collections.ICollection"/> is synchronized (thread safe); otherwise, false.</returns>
        public bool IsSynchronized
        {
            get { return _wrappedCollection.IsSynchronized; }
        }

        /// <summary>
        /// Gets the <see cref="SharpPieces.Web.Controls.ExtendedListItem"/> at the specified index.
        /// </summary>
        /// <value></value>
        public ExtendedListItem this[int index]
        {
            get { return new ExtendedListItem(_wrappedCollection[index]); }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection"/>.
        /// </summary>
        /// <value></value>
        /// <returns>An object that can be used to synchronize access to the <see cref="T:System.Collections.ICollection"/>.</returns>
        public object SyncRoot
        {
            get { return _wrappedCollection.SyncRoot; }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList"/> has a fixed size.
        /// </summary>
        /// <value></value>
        /// <returns>true if the <see cref="T:System.Collections.IList"/> has a fixed size; otherwise, false.</returns>
        bool IList.IsFixedSize
        {
            get { return ((IList)_wrappedCollection).IsFixedSize; }
        }

        /// <summary>
        /// Gets or sets the <see cref="System.Object"/> at the specified index.
        /// </summary>
        /// <value></value>
        object IList.this[int index]
        {
            get { return ((IList)_wrappedCollection)[index]; }
            set { }
        }

        /// <summary>
        /// When implemented by a class, gets a value indicating whether a server control is tracking its view state changes.
        /// </summary>
        /// <value></value>
        /// <returns>true if a server control is tracking its view state changes; otherwise, false.</returns>
        bool IStateManager.IsTrackingViewState
        {
            get { return ((IStateManager)_wrappedCollection).IsTrackingViewState; }
        }

        /// <summary>
        /// Gets the safe wrapped item.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        private static ListItem GetSafeWrappedItem(ExtendedListItem item)
        {
            return (null != item) ? item._listItem : null;
        } 
	#endregion
    }
}
