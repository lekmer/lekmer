﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerAddresses.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Customer.CustomerAddresses" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2"
	Assembly="Litium.Scensum.BackOffice" %>
<%@ Register Assembly="Litium.Scensum.BackOffice" Namespace="Litium.Scensum.BackOffice.UserControls.ContextMenu"
	TagPrefix="sc" %>

<script type="text/javascript">
	function confirmDelete() {
		return DeleteConfirmation("<%= Resources.Customer.Label_Addresses %>");
	}
</script>

<div class="customer-edit-container">
	<label class="assortment-header">
		<%= Resources.Customer.Label_Addresses %></label>

	<div class="grid">
		<sc:GridViewWithCustomPager ID="AddressGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
			Width="100%">
			<Columns>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
					ItemStyle-Width="2%">
					<HeaderTemplate>
						<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
					</HeaderTemplate>
					<ItemTemplate>
						<asp:CheckBox ID="SelectCheckBox" runat="server" />
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-Width="18%" HeaderText="<%$ Resources:Customer, ColumnHeader_Addressee %>">
					<ItemTemplate>
						<div style="width: 100%; overflow: hidden;">
							<div style="float: left; max-width: 96%;">
							<%# EncodeHelper.HtmlEncode((string)Eval("Addressee"))%></div>
							<div style="float: left">
								<uc:ContextMenu ID="AddressContextMenu" runat="server" CallerImageSrc="~/Media/Images/Common/context-menu.png"
									CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png" MenuContainerCssClass="context-menu-container"
									MenuShadowCssClass="context-menu-shadow" MenuCallerCssClass="context-menu-caller">
									<div class="context-menu-header">
										<%= Resources.General.Literal_Manage %>
									</div>
									<div class="menu-row-separator">
									</div>
									<div class="context-menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Customer/Address/edit.gif") %>" />
										<asp:LinkButton ID="MenuEditButton" runat="server" CommandArgument='<%# Eval("Id") %>'
											CommandName="EditAddress" Text="<%$ Resources:Customer, Literal_EditAddress %>" />
									</div>
									<div class="context-menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
										<asp:LinkButton ID="MenuDeleteButton" runat="server" OnClientClick='return confirmDelete();'
											CommandArgument='<%# Eval("Id") %>' CommandName="DeleteAddress" Text="<%$ Resources:General, Button_Delete%>" />
									</div>
								</uc:ContextMenu>
							</div>
						</div>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Phone %>" DataField="PhoneNumber"
					ItemStyle-Width="10%" />
				<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Address %>" DataField="StreetAddress"
					ItemStyle-Width="19%" />
				<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Address2 %>" DataField="StreetAddress2"
					ItemStyle-Width="19%" />
				<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_PostalCode %>" DataField="PostalCode"
					ItemStyle-Width="11%" />
				<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_City %>" DataField="City"
					ItemStyle-Width="10%" />
				<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Country %>" ItemStyle-Width="9%">
					<ItemTemplate>
						<asp:Label ID="CountryLabel" runat="server" />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
					<ItemTemplate>
						<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteAddress" CommandArgument='<%# Eval("Id") %>'
							ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
		<div id="AllSelectedDiv" class="customer-address-apply" runat="server">
		<div style="float:left; padding-top: 5px;">
			<span><%= Resources.General.Literal_ApplyToAllSelectedItems %>:&nbsp;&nbsp;</span>
		</div> 
			<div style="float: left">
				<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="DeleteSelectedButton"
					runat="server" Text="<%$ Resources:General,Button_Delete %>" OnClientClick="return confirmDelete();" />
			</div>
		</div>
		<div id="AddNewAddressDiv"><asp:LinkButton runat="server" ID="AddNewAdressButon" Text="<%$ Resources:Customer,Button_AddNewAddress %>"></asp:LinkButton></div>
	</div>
</div>
<asp:HiddenField runat="server" ID="FakeControl" />

<div id="AddressViewDiv" runat="server" class="customer-address-mainDiv" style="z-index: 10010;
	display: none;">
	<div id="customer-address-edit-header">
		<div id="customer-address-edit-header-left">
		</div>
		<div id="customer-address-edit-header-center">
			<span>
				<%= Resources.Customer.Label_CustomerAddress %></span>
			<asp:Button ID="AddressViewCancelButton" runat="server" Text="X" UseSubmitBehavior="false" />
		</div>
		<div id="customer-address-edit-header-right">
		</div>
	</div>
	
	
	<div id="customer-address-subMainDiv">

	<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" />

		<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server"
			CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="AddressValidationGroup" />
		<div class="column1">
			<div class="customer-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Addressee %></span>
				<asp:RequiredFieldValidator ID="AddresseeValidator" runat="server" ControlToValidate="AddresseeEditTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyAddressee%>" Display="None"
					ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="AddresseeEditTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Address %></span>
				<asp:RequiredFieldValidator ID="AddressValidator" runat="server" ControlToValidate="AddressTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyAddress%>" Display="None"
					ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="AddressTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box input-box-zip">
				<span>
					<%= Resources.Customer.Literal_PostalCode %></span>
				<asp:RequiredFieldValidator ID="ZipValidator" runat="server" ControlToValidate="ZipteTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyZip%>" Display="None" ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="ZipteTextBox" runat="server" CssClass="zip" />
			</div>
			<div class="customer-input-box input-box input-box-city">
				<span>
					<%= Resources.Customer.Literal_City %></span>
				<asp:RequiredFieldValidator ID="CityValidator" runat="server" ControlToValidate="CityTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,VM_EmptyCity%>" Display="None" ValidationGroup="AddressValidationGroup" />
				<br />
				<asp:TextBox ID="CityTextBox" runat="server" CssClass="city" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Country %></span><br />
				<asp:DropDownList runat="server" ID="Country" DataTextField="Title" DataValueField="Id">
				</asp:DropDownList>
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span>
					<%= Resources.Lekmer.Order_Reference %>
				</span>
				<br />
				<asp:TextBox ID="ReferenceTextBox" runat="server" />
			</div>
			<br clear="all" />
		</div>
		<div class="column2">
			<div class="customer-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_PhoneNumber %></span>
				<br />
				<asp:TextBox ID="PhoneNumberTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span>
					<%= Resources.Customer.Literal_Address2 %></span><br />
				<asp:TextBox ID="Address2TextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_HouseNumber %></span>
				<br />
				<asp:TextBox ID="HouseNumberTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_HouseExtension %></span>
				<br />
				<asp:TextBox ID="HouseExtensionTextBox" runat="server" />
			</div>
			<br clear="all" />
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_DoorCode %></span>
				<br />
				<asp:TextBox ID="DoorCodeTextBox" runat="server" />
			</div>
			<br clear="all" />
		</div>
		<br />
		<div class="customer-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Ok %>"
				SkinID="DefaultButton" ValidationGroup="AddressValidationGroup" />
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				SkinID="DefaultButton" />
		</div>
		
	</div>
	<ajaxToolkit:ModalPopupExtender ID="AddressPopup" runat="server" TargetControlID="FakeControl"
		PopupControlID="AddressViewDiv" CancelControlID="AddressViewCancelButton" BackgroundCssClass="popup-background" />
		
</div>
