﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderCommentControl.ascx.cs" 
	Inherits="Litium.Scensum.BackOffice.UserControls.Customer.Order.OrderCommentControl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<div class="control-container">
<div><%= Resources.Customer.Label_Comments %></div>

<div id="customer-comment">
	<asp:Repeater runat="server" ID="CommentList">
		<ItemTemplate>
			<div class="customer-comment-item">
				<span class="date"><%#((DateTime)Eval("CreationDate")).ToString("yyyy-MM-dd HH:mm")%></span><br />
				<span class="author"><%# Eval("Author.UserName") %></span><br />
				<span class="comment"><%#Litium.Scensum.Foundation.Utilities.Encoder.EncodeMultiline(EncodeHelper.HtmlEncode((string)Eval("Comment")))%></span><br />
			</div>
		</ItemTemplate>
	</asp:Repeater>
</div>

<div id="AddNewCommentDiv">
	<asp:LinkButton runat="server" ID="AddNewCommentButon" Text="<%$ Resources:Customer,Button_AddNewComment %>"></asp:LinkButton>
</div>
</div>
<asp:HiddenField runat="server" ID="FakeControl" />
<div id="CommentViewDiv" runat="server" class="customer-comment-mainDiv" style="z-index: 10010;
	display: none;">
	<div id="customer-comment-header">
		<div id="customer-comment-header-left">
		</div>
		<div id="customer-comment-header-center">
			<span>
				<%= Resources.Customer.Label_AddComment%></span>
			<asp:Button ID="CommentViewCancelButton" runat="server" Text="X" UseSubmitBehavior="false" />
		</div>
		<div id="customer-comment-header-right">
		</div>
	</div>
	<div id="customer-comment-subMainDiv">
	<asp:Panel ID="Panel1" runat="server" DefaultButton="SaveButton">
		<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Warning" HideMessagesControlId="SaveButton" />
		<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="CommentValidationGroup" />
		<div class="column1">
			<div class="input-box">
				<span>
					<%= Resources.Customer.Label_Comment%></span>
				<asp:RequiredFieldValidator ID="CommentValidator" runat="server" ControlToValidate="CommentEditTextBox"
					ErrorMessage="<%$ Resources:GeneralMessage,EmptyComment%>" Display="None" ValidationGroup="CommentValidationGroup" />
				<br />
				<asp:TextBox ID="CommentEditTextBox" TextMode="MultiLine" Rows="4" Columns="53" runat="server" />
			</div>
			<br clear="all" />
		</div>
		<br />
		<div class="customer-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
				SkinID="DefaultButton" ValidationGroup="CommentValidationGroup" />
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				SkinID="DefaultButton" />
		</div>
			</asp:Panel>
	</div>
	<ajaxToolkit:ModalPopupExtender ID="CommentPopup" runat="server" TargetControlID="FakeControl"
		PopupControlID="CommentViewDiv" CancelControlID="CommentViewCancelButton" BackgroundCssClass="popup-background" />
</div>

