using System;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Events
{
	public class CustomerSearchCriteriaEventArgs : EventArgs
	{
		public ICustomerSearchCriteria SearchCriteria
		{
			get; set;
		}
		public int? PageIndex
		{
			get; set;
		}
	}
}