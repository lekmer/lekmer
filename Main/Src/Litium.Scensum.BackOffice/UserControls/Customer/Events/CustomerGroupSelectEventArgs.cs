﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Events
{
	public class CustomerGroupSelectEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ICustomerGroup> CustomerGroups
		{
			get;
			set;
		}
	}
}
