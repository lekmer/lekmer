﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerSearch.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Customer.CustomerSearch" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Customer/CustomerSearchCriteria.ascx" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type="text/javascript">
	function ClosePopup() {
		$("div#popup-customers-header-center input[name*='CancelButton']").click();
		return false;
	}
</script>

<div id="SearchCustomersDiv" runat="server" class="popup-customers-container" style="z-index: 10010; display: none;">
    <div id="popup-customers-header">
		<div id="popup-customers-header-left">
		</div>
		<div id="popup-customers-header-center">
			<span><%= Resources.Customer.Literal_SelectCustomers %></span>
			<input type="button" id="CancelButton" runat="server" value="x"/>
		</div>
		<div id="popup-customers-header-right">
		</div>
	</div>
	<div id="popup-customers-content">
        <uc:Search ID="SearchCriteria" ShowSearch="true" runat="server" />
	    <asp:UpdatePanel runat="server" ID="MainUpdatePanel" ChildrenAsTriggers="true">
		    <ContentTemplate>			
                <div id="popup-customers-result" class="clear left">
				    <Scensum:Grid ID="CustomersSearchScensumGrid" runat="server">
					    <sc:GridViewWithCustomPager 
						    ID="CustomersSearchGrid" 
						    SkinID="grid" 
						    runat="server"
						    AllowPaging="true" 
						    PageSize="<%$AppSettings:DefaultGridPageSize%>" 
						    DataSourceID="CustomersDataSource"
						    AutoGenerateColumns="false" 
						    Width="100%">
						    <Columns>
							    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
							    <asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_ErpId %>" DataField="ErpId" ItemStyle-Width="13%" />
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Customer_FirstOrCompanyName %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="FirstNameLiteral" Text='<%# Eval("CustomerInformation.FirstName")%>' />
									</ItemTemplate >
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Customer_LastOrFullName %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="LastNameLiteral" runat="server" Text='<%# Eval("CustomerInformation.LastName")%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Status %>" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="StatusLiteral" runat="server" Text='<%# GetCustomerStatus(Eval("CustomerStatusId"))%>' />
									</ItemTemplate>
									</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_CivicNumber %>" ItemStyle-Width="9%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="CivicNumberLiteral" runat="server" Text='<%# Eval("CustomerInformation.CivicNumber")%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" ItemStyle-Width="11%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="CreatedDateLiteral" runat="server" Text='<%# Convert.ToDateTime(Eval("CustomerInformation.CreatedDate")).ToShortDateString() %>' /> 
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="EmailLiteral" runat="server" Text='<%# Eval("CustomerInformation.Email")%>'/>
									</ItemTemplate>
								</asp:TemplateField>
						    </Columns>
					    </sc:GridViewWithCustomPager>
					    <asp:ObjectDataSource ID="CustomersDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch.CustomerDataSource" />
				    </Scensum:Grid>
                    <div class="clear right">
                        <uc:ImageLinkButton  UseSubmitBehaviour="true" ID="AddButton" runat="server" Text="<%$ Resources:Customer, Button_Ok %>" Enabled="false" SkinID="DefaultButton" />
                        <uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelBottomButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" OnClientClick="ClosePopup();" />
                    </div>
                </div>
		    </ContentTemplate>
		    <Triggers>
		        <asp:AsyncPostBackTrigger ControlID="SearchCriteria" EventName="SearchEvent" />
		    </Triggers>
	    </asp:UpdatePanel>
	    	    
     </div>
</div>
<div style="float:left;" >
    <uc:ImageLinkButton ID="AddCustomersButton" runat="server" Text="<%$ Resources:Customer, Button_AddMembers %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
    <ajaxToolkit:ModalPopupExtender 
        ID="CustomersSearchPopup" 
        runat="server" 
        TargetControlID="AddCustomersButton"
		PopupControlID="SearchCustomersDiv"
		BackgroundCssClass="popup-background" Y="20"
		CancelControlID="CancelButton"/>
</div>