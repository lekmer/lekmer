<%@ Control Language="C#" CodeBehind="CustomerSearchCriteria.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Customer.CustomerSearchCriteria" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<script type='text/javascript' language='javascript'>
	function ResetSerchForm(d) {
		if (!confirm("<%= Resources.CustomerMessage.SearchCriteriasClearConfirm %>")) return;
		d = document;
		d.getElementById('<%= tbFirstName.ClientID %>').value = '';
		d.getElementById('<%= tbLastName.ClientID %>').value = '';
		d.getElementById('<%= tbCivicNumber.ClientID %>').value = '';
		d.getElementById('<%= tbEmail.ClientID %>').value = '';
		d.getElementById('<%= tbPhoneHome.ClientID %>').value = '';
		d.getElementById('<%= tbPhoneMobile.ClientID %>').value = '';
		d.getElementById('<%= tbErpId.ClientID %>').value = '';
		d.getElementById('<%= ddlStatus.ClientID %>').value = '';
		d.getElementById('<%= tbCreatedFrom.ClientID %>').value = '';
		d.getElementById('<%= tbCreatedTo.ClientID %>').value = '';
	};
</script>

<asp:Panel ID="panel" runat="server" DefaultButton="btnSearch">
	<div class="customer-search-errors">
			<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" DisplayMode="List" ForeColor="Black" CssClass="advance-validation-summary" ValidationGroup="vgCustomerSearch" />
		</div>
	<div id="customer-search">

		<div id="customer-search-container">
			<label class="assortment-header"><%= Resources.General.Literal_Search %></label>
			<br />
			<div class="customer-input-box input-box">
				<span><%= Resources.Lekmer.Customer_FirstOrCompanyName %></span><br />
				<asp:TextBox ID="tbFirstName" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.Lekmer.Customer_LastOrFullName %></span><br />
				<asp:TextBox ID="tbLastName" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.Lekmer.Customer_CivicOrOrganizationName %></span>
				<asp:RegularExpressionValidator ID="vCivicNumber" runat="server" ControlToValidate="tbCivicNumber"
					ErrorMessage="<%$ Resources:CustomerMessage, CivicNumberRequirement %>" Text=" *"
					ValidationExpression="\d{6}-\d{4}" ValidationGroup="vgCustomerSearch" />
				<br />
				<asp:TextBox ID="tbCivicNumber" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_PhoneNumber %></span><br />
				<asp:TextBox ID="tbPhoneHome" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_MobilePhone %></span><br />
				<asp:TextBox ID="tbPhoneMobile" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_Email %></span>
				<asp:RegularExpressionValidator ID="vEmail" runat="server" ControlToValidate="tbEmail"
					ErrorMessage="<%$ Resources:CustomerMessage, EmailRequirement %>" Text=" *" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
					ValidationGroup="vgCustomerSearch" />
				<br />
				<asp:TextBox ID="tbEmail" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.General.Literal_ErpId %></span><br />
				<asp:TextBox ID="tbErpId" runat="server" />
			</div>
			<div class="customer-input-box input-box">
				<span><%= Resources.Customer.Literal_CustomerStatus %></span><br />
				<asp:DropDownList runat="server" ID="ddlStatus" DataTextField="Title" DataValueField="Id">
				</asp:DropDownList>
			</div>
			<div class="input-box-date">
				<span><%= Resources.Customer.Literal_AccountCreatedFrom %></span>
				<asp:CustomValidator ID="cvDateFrom" runat="server" ControlToValidate="tbCreatedFrom" ErrorMessage="<%$ Resources:GeneralMessage, IncorectDateType %>" Text=" *" ValidationGroup="vgCustomerSearch" />
				<br />
				<asp:TextBox ID="tbCreatedFrom" runat="server" />
				<asp:ImageButton ID="ibDateFrom" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
				<ajaxToolkit:CalendarExtender ID="ceDateFrom" runat="server" TargetControlID="tbCreatedFrom" PopupButtonID="ibDateFrom" />
			</div>
			<div class="input-box-date">
				<span><%= Resources.Customer.Literal_AccountCreatedTo %></span>
				<asp:CustomValidator ID="cvDateTo" runat="server" ControlToValidate="tbCreatedTo" ErrorMessage="<%$ Resources:GeneralMessage, IncorectDateType %>" Text=" *" ValidationGroup="vgCustomerSearch" />
				<asp:CustomValidator ID="cvDateCompare" runat="server" ControlToValidate="tbCreatedTo" ErrorMessage="<%$ Resources:GeneralMessage, DateCreateFromGreaterCreateTo %>" Text=" *" ValidationGroup="vgCustomerSearch" />
				<br />
				<asp:TextBox ID="tbCreatedTo" runat="server" />
				<asp:ImageButton ID="ibDateTo" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle" CausesValidation="False" />
				<ajaxToolkit:CalendarExtender ID="ceDateTo" runat="server" TargetControlID="tbCreatedTo" PopupButtonID="ibDateTo" />
			</div>
		</div>
		<br />
		<div class="buttons-container"><uc:ImageLinkButton ID="btnSearch" runat="server" Text="<%$ Resources:General, Button_Search %>" ValidationGroup="vgCustomerSearch"
				SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="btnReset" runat="server" Text="<%$ Resources:Customer, Button_Reset %>" OnClientClick='ResetSerchForm();'
				SkinID="DefaultButton" />
			
		</div>
		<br class="customer-clear"/>
		<div class="customer-horizontal-separator"></div>
		<div style="float: none;"><span class="customer-header">
				<%= Resources.General.Literal_SearchResults %></span></div>
			
	</div>
	
</asp:Panel>
