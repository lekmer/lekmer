﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Common;
using Resources;
using Image = System.Web.UI.WebControls.Image;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
    public partial class ImageRotatorGroup : StateUserControlController<ImageRotatorGroupsState>
    {
        private int _imageRotatorGroupMaxQuantity = 100;

        IImageSecureService imageSecureService = IoC.Resolve<IImageSecureService>();

        public event EventHandler ImageUploaded;
        public event EventHandler<ImageSelectEventArgs> DefaltImageSet;

        public int ImageRotatorId
        {
            get { return State.ImageRotatorId; }
            set
            {
                EnsureState();
                State.ImageRotatorId = value;
            }
        }

	    public string UserName
	    {
		    get { return SignInHelper.SignedInSystemUser.UserName; }
	    }


	    private Collection<IImageRotatorGroup> ImageRotatorGroups
        {
            get { return State.ImageRotatorGroups; }
            set
            {
                EnsureState();
                State.ImageRotatorGroups = value;
            }
        }

        public string GroupTitle { get; set; }

        protected override void SetEventHandlers()
        {
            ImageGroupsRepeater.ItemDataBound += ImageGroupsItemDataBound;
            ImageGroupsRepeater.ItemCommand += ImageGroupsItemCommand;

            ImageSelectControl.Selected += ImageSelected;
            ImageSelectControl.FileUploaded += ImageSelectFileUploated;
            AddGroup.Click += AddGroup_Click;
        }

        void AddGroup_Click(object sender, EventArgs e)
        {
            ImageRotatorGroups = GetImageRotatorGroups();
            IImageRotatorGroup imageRotatorGroup = IoC.Resolve<IImageRotatorGroupSecureService>().Create();
            imageRotatorGroup.BlockId = ImageRotatorId;
            imageRotatorGroup.ImageGroupId = ImageRotatorGroups.Count - _imageRotatorGroupMaxQuantity;
            ImageRotatorGroups.Add(imageRotatorGroup);
            ImageGroupsRepeater.DataSource = ImageRotatorGroups;
            ImageGroupsRepeater.DataBind();
            if (ImageRotatorGroups.Count >= _imageRotatorGroupMaxQuantity) { AddGroup.Visible = false; }
        }

        protected override void PopulateControl()
        {
            var productImageGroupSecureService = IoC.Resolve<IImageRotatorGroupSecureService>();
            ImageRotatorGroups = productImageGroupSecureService.GetByBlockId(SignInHelper.SignedInSystemUser, ImageRotatorId);

            if (ImageRotatorGroups.Count >= _imageRotatorGroupMaxQuantity) { AddGroup.Visible = false; }

            ImageGroupsRepeater.DataSource = ImageRotatorGroups;
            ImageGroupsRepeater.DataBind();
        }

        protected void EnsureState()
        {
            if (State == null)
            {
                State = new ImageRotatorGroupsState();
            }
        }

        private void AllGridsBind()
        {
            foreach (RepeaterItem repeaterItem in ImageGroupsRepeater.Items)
            {
                var groupIdHiddenField = (HiddenField)repeaterItem.FindControl("ImageGroupIdValueHiddenField");
                int groupId = int.Parse(groupIdHiddenField.Value, CultureInfo.CurrentCulture);
                IImageRotatorGroup imageGroup = ImageRotatorGroups.FirstOrDefault(item => item.ImageGroupId == groupId);
                DataBindImages(repeaterItem, imageGroup);
            }
        }

        private void ImageGroupsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var repeaterItem = e.Item;
            var imageGroup = (IImageRotatorGroup)repeaterItem.DataItem;
            DataBindGroup(repeaterItem);
            DataBindImages(repeaterItem, imageGroup);
            PopulateInternalLinkContentNode("SiteStructureNodeSelector", imageGroup.InternalLinkContentNodeId, repeaterItem);
            PopulateInternalLinkContentNode("SiteStructureNodeSelector2", imageGroup.InternalLinkContentNodeId2, repeaterItem);
            PopulateInternalLinkContentNode("SiteStructureNodeSelector3", imageGroup.InternalLinkContentNodeId3, repeaterItem);
            PopulateExternalLink("ExternalLinkTextBox", "IsMoveCheckBox", imageGroup.ExternalLink, imageGroup.IsMovie, repeaterItem);
            PopulateExternalLink("ExternalLinkTextBox2", "IsMoveCheckBox2", imageGroup.ExternalLink2, imageGroup.IsMovie2, repeaterItem);
            PopulateExternalLink("ExternalLinkTextBox3", "IsMoveCheckBox3", imageGroup.ExternalLink3, imageGroup.IsMovie3, repeaterItem);

            PopulateDate("Start", imageGroup.StartDateTime, repeaterItem);
            PopulateDate("End", imageGroup.EndDateTime, repeaterItem);
            var ordinalTextBox = (TextBox)repeaterItem.FindControl("OrdinalTextBox");
            ordinalTextBox.Text = imageGroup.Ordinal.ToString(CultureInfo.CurrentCulture);
            var titleTextBox = (TextBox)repeaterItem.FindControl("TitleTextBox");
            titleTextBox.Text = imageGroup.Title;

            var captionsEditor = (Litium.Scensum.BackOffice.WebControls.LekmerTinyMceEditor)repeaterItem.FindControl("CaptionsEditor");
            captionsEditor.SetValue(imageGroup.HtmalMarkup ?? string.Empty);
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual int? GetNodeId()
        {
            return Request.QueryString.GetInt32OrNull("id");
        }
        void PopulateInternalLinkContentNode(string internalLinkId, int? selectedId, RepeaterItem repeaterItem)
        {
            var nodeId = selectedId ?? GetNodeId();
            var internalLinkContentNodeid = (LekmerContentPageSelector)repeaterItem.FindControl(internalLinkId);
            IContentNode node = IoC.Resolve<IContentNodeSecureService>().GetById(nodeId.Value);
            internalLinkContentNodeid.SiteStructureRegistryId = node.SiteStructureRegistryId;
            internalLinkContentNodeid.SelectedNodeId = selectedId;
            internalLinkContentNodeid.SetContentPageTitle();
        }

        private static void PopulateDate(string dateId, DateTime? date, RepeaterItem repeaterItem)
        {
            var dateTextBox = (TextBox)repeaterItem.FindControl(dateId + "DateTextBox");
            dateTextBox.Text = date.HasValue ?
              ConvertDateTime(date.Value) : string.Empty;
        }
        private static string ConvertDateTime(DateTime? dateTime)
        {
            return dateTime.HasValue
                ? dateTime.Value.ToString(ShortDateTimePattern, CultureInfo.CurrentCulture)
                : string.Empty;
        }
        private static void PopulateExternalLink(string externalLinkId, string isMoveCheckboxId, string externalLink, bool isMovie, RepeaterItem repeaterItem)
        {
            var externalLinkTextBox = (TextBox)repeaterItem.FindControl(externalLinkId);
            externalLinkTextBox.Text = externalLink;
            var isMoveCheckBox = (CheckBox)repeaterItem.FindControl(isMoveCheckboxId);
            isMoveCheckBox.Checked = isMovie;
        }

        public virtual IEnumerable<ISiteStructureNode> GetTreeNodes(int? selectedId, bool getAllRegistries)
        {
            return IoC.Resolve<IContentNodeSecureService>().GetTree(selectedId, getAllRegistries);
        }

        private static void DataBindGroup(RepeaterItem repeaterItem)
        {
            var areaCollapsiblePanel = (ImageRotatorGroupCollapsiblePanel)repeaterItem.FindControl("GroupCollapsiblePanel");
            string divBlockGvId = repeaterItem.FindControl("ImageGroupGridViewDiv").ClientID;
            areaCollapsiblePanel.ControlToExpand = divBlockGvId;
        }

        private static string PopulateFoldersPath(IImage image)
        {
            var mediaFolderService = IoC.Resolve<IMediaFolderSecureService>();
            IMediaFolder folder = mediaFolderService.GetById(image.MediaFolderId);
            var foldersPath = new StringBuilder(folder.Title);

            while (folder.MediaFolderParentId.HasValue)
            {
                folder = mediaFolderService.GetById(folder.MediaFolderParentId.Value);
                foldersPath.Insert(0, @"\");
                foldersPath.Insert(0, folder.Title);
            }
            foldersPath.Insert(0, @"[Root]\");
            return foldersPath.ToString();
        }

        private void DataBindImages(RepeaterItem repeaterItem, IImageRotatorGroup imageGroup)
        {
			//var mainImage = (Image)repeaterItem.FindControl("MainImage");
			//var addMainImage = (LinkButton)repeaterItem.FindControl("AddMainImage");
			//var deleteMainImage = (ImageButton)repeaterItem.FindControl("DeleteMainImage");
			//var mainImageFormatExtension = (Label)repeaterItem.FindControl("mainImageFormatExtension");
			//var mainImageFormatSize = (Label)repeaterItem.FindControl("mainImageFormatSize");
			//var mainImageContextMenu = (Litium.Scensum.Web.Controls.ContextMenu)repeaterItem.FindControl("MainImageContextMenu");
			//if (imageGroup.MainImageId.HasValue)
			//{
			//	var imageMain = imageSecureService.GetById(imageGroup.MainImageId.Value);
			//	mainImageFormatExtension.Visible = true;
			//	mainImageFormatSize.Visible = true;
			//	mainImageContextMenu.Visible = true;
			//	deleteMainImage.Visible = true;
			//	mainImage.Visible = true;
			//	mainImage.ImageUrl =
			//		   ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(imageGroup.MainImageId.Value, imageMain.FormatExtension));

			//	mainImageFormatExtension.Text = imageMain.FormatExtension;
			//	mainImageFormatSize.Text = imageMain.Width + " x " + imageMain.Height;
			//	addMainImage.Visible = false;
			//}
			//else
			//{
			//	mainImageFormatExtension.Visible = false;
			//	mainImageFormatSize.Visible = false;
			//	mainImageContextMenu.Visible = false;
			//	deleteMainImage.Visible = false;
			//	addMainImage.Visible = true;
			//	addMainImage.CommandArgument = imageGroup.ImageGroupId.ToString(CultureInfo.CurrentCulture); ;
			//	mainImage.Visible = false;
			//}
			//var thumbImage = (Image)repeaterItem.FindControl("ThumbImage");
			//var addThumbImage = (LinkButton)repeaterItem.FindControl("AddThumbImage");
			//var deleteThumbImage = (ImageButton)repeaterItem.FindControl("DeleteThumbImage");
			//var thumbImageFormatExtension = (Label)repeaterItem.FindControl("thumbImageFormatExtension");
			//var thumbImageFormatSize = (Label)repeaterItem.FindControl("thumbImageFormatSize");
			//var thumbImageContextMenu = (Litium.Scensum.Web.Controls.ContextMenu)repeaterItem.FindControl("ThumbImageContextMenu");
			//if (imageGroup.ThumbnailImageId.HasValue)
			//{
			//	var imageThumb = imageSecureService.GetById(imageGroup.ThumbnailImageId.Value);
			//	thumbImageFormatExtension.Visible = true;
			//	thumbImageFormatSize.Visible = true;
			//	thumbImageContextMenu.Visible = true;
			//	deleteThumbImage.Visible = true;
			//	thumbImage.Visible = true;
			//	thumbImage.ImageUrl =
			//		  ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(imageGroup.ThumbnailImageId.Value, imageThumb.FormatExtension));
			//	addThumbImage.Visible = false;

			//	thumbImageFormatExtension.Text = imageThumb.FormatExtension;
			//	thumbImageFormatSize.Text = imageThumb.Width + " x " + imageThumb.Height;
			//}
			//else
			//{
			//	thumbImageFormatExtension.Visible = false;
			//	thumbImageFormatSize.Visible = false;
			//	thumbImageContextMenu.Visible = false;
			//	deleteThumbImage.Visible = false;
			//	addThumbImage.CommandArgument = imageGroup.ImageGroupId.ToString(CultureInfo.CurrentCulture); ;
			//	addThumbImage.Visible = true;
			//	thumbImage.Visible = false;
			//}

        }

        protected virtual void ImageGroupGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowType != DataControlRowType.DataRow) return;
            var productImage = (IProductImage)row.DataItem;
            var titleLiteral = (LiteralEncoded)row.FindControl("TitleLiteral");
            titleLiteral.Text = productImage.Image.Title;

            var imagePathLiteral = (LiteralEncoded)row.FindControl("ImagePathLiteral");
            imagePathLiteral.Text = PopulateFoldersPath(productImage.Image);

            var imgArchiveImage = (Image)row.FindControl("ArchiveImage");
            imgArchiveImage.ImageUrl =
                ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(productImage.Image.Id, productImage.Image.FormatExtension));
            var archiveImageFileTypeLAbel = (Label)row.FindControl("ArchiveImageFileTypeLAbel");
            archiveImageFileTypeLAbel.Text = productImage.Image.FormatExtension;
            var archiveImageDimensionsLabel = (Label)row.FindControl("ArchiveImageDimensionsLabel");
            archiveImageDimensionsLabel.Text = productImage.Image.Width + " x " + productImage.Image.Height;

            var ordinalTextBox = (TextBox)row.FindControl("OrdinalTextBox");
            ordinalTextBox.Text = productImage.Ordinal.ToString(CultureInfo.CurrentCulture);
        }

        private void ImageGroupsItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int groupId;

            if (!int.TryParse(e.CommandArgument.ToString(), out groupId))
            {
                groupId = 0;
            }

            switch (e.CommandName)
            {
                case "AddImagePopup":
                    GroupIdValueHiddenField.Value = groupId.ToString(CultureInfo.CurrentCulture);
                    ImageAddPopup.Show();
                    break;
                case "AddMainImage":
                    GroupIdValueHiddenField.Value = groupId.ToString(CultureInfo.CurrentCulture);
                    ChosenImageHiddenField.Value = "AddMainImage";
                    ImageAddPopup.Show();
                    break;
                case "AddThumbImage":
                    GroupIdValueHiddenField.Value = groupId.ToString(CultureInfo.CurrentCulture);
                    ChosenImageHiddenField.Value = "AddThumbImage";
                    ImageAddPopup.Show();
                    break;

                case "DeleteMainImage":
                    ImageRotatorGroups.FirstOrDefault(item => item.ImageGroupId == groupId).MainImageId = null;
                    AllGridsBind();
                    break;

                case "DeleteThumbImage":
                    ImageRotatorGroups.FirstOrDefault(item => item.ImageGroupId == groupId).ThumbnailImageId = null;
                    AllGridsBind();
                    break;
                case "RemoveGroup":
                    ImageRotatorGroups = GetImageRotatorGroups();
                    ImageRotatorGroups.Remove(ImageRotatorGroups.FirstOrDefault(item => item.ImageGroupId == groupId));
                    ImageGroupsRepeater.DataSource = ImageRotatorGroups;
                    ImageGroupsRepeater.DataBind();
                    if (ImageRotatorGroups.Count < _imageRotatorGroupMaxQuantity) { AddGroup.Visible = true; }
                    break;
            }
        }
        private void ImageSelectFileUploated(object sender, EventArgs e)
        {
            ImageAddPopup.Show();
            ImageAddPopup.X = 227;
            if (ImageUploaded != null)
            {
                ImageUploaded(this, EventArgs.Empty);
            }
        }

        private void ImageSelected(object sender, ImageSelectEventArgs e)
        {
            if (string.IsNullOrEmpty(ChosenImageHiddenField.Value)) return;


            int groupId = int.Parse(GroupIdValueHiddenField.Value, CultureInfo.CurrentCulture);

            IImageRotatorGroup imageGroup = ImageRotatorGroups.FirstOrDefault(item => item.ImageGroupId == groupId);
            if (ChosenImageHiddenField.Value.Equals("AddMainImage"))
            {
                imageGroup.MainImageId = e.Image.Id;
            }


            if (ChosenImageHiddenField.Value.Equals("AddThumbImage"))
            {
                imageGroup.ThumbnailImageId = e.Image.Id;
            }
            if (ImageUploaded != null)
            {
                ImageUploaded(this, EventArgs.Empty);
            }
            AllGridsBind();
        }

        public Collection<string> Validate()
        {
            var errors = new Collection<string>();
            string groupName = "Group";
            int groupCount = 0;
            foreach (RepeaterItem repeaterItem in ImageGroupsRepeater.Items)
            {
                groupCount++;
                var ordinalTextBox = (TextBox)repeaterItem.FindControl("OrdinalTextBox");
                if (!string.IsNullOrEmpty(ordinalTextBox.Text.Trim()))
                {
                    int ordinal;
                    if (!int.TryParse(ordinalTextBox.Text, out ordinal))
                    {
                        var ordinalValidator = (CustomValidator)repeaterItem.FindControl("OrdinalValidator");
                        ordinalValidator.IsValid = false;
                        errors.Add(groupName + " " + groupCount + " - " + GeneralMessage.OrdinalShouldInteger);
                    }
                }

                DateTime minDate = Constants.MinDate;
                DateTime maxDate = Constants.MaxDate;
                var endDateValidator = (CustomValidator)repeaterItem.FindControl("EndDateValidator");
                var startDateValidator = (CustomValidator)repeaterItem.FindControl("StartDateValidator");
                string startDate = ((TextBox)repeaterItem.FindControl("StartDateTextBox")).Text.Trim();
                string endDate = ((TextBox)repeaterItem.FindControl("EndDateTextBox")).Text.Trim();

                if (string.IsNullOrEmpty(startDate) && string.IsNullOrEmpty(endDate))
                {
                    continue;
                }

                if ((string.IsNullOrEmpty(startDate) && !string.IsNullOrEmpty(endDate)) || (!string.IsNullOrEmpty(startDate) && string.IsNullOrEmpty(endDate)))
                {
                    errors.Add(groupName + " " + groupCount + " - " + GeneralMessage.YouCanNotEnteringJustEndDateOrStartDate);
                    if (string.IsNullOrEmpty(startDate))
                    {
                        startDateValidator.IsValid = false;
                    }
                    else
                    {
                        endDateValidator.IsValid = false;
                    }
                    continue;
                }

                DateTime endDateTime;
                DateTime startDateTime;

                var validFormats = new[]{
           		CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
           		ShortDateTimePattern};

                if (!DateTime.TryParseExact(startDate, validFormats, CultureInfo.CurrentCulture, DateTimeStyles.None, out startDateTime))
                {
                    startDateValidator.IsValid = false;
                }
                else if (startDateTime < minDate || startDateTime > maxDate)
                {
                    startDateValidator.IsValid = false;
                }

                if (!DateTime.TryParseExact(endDate, validFormats, CultureInfo.CurrentCulture, DateTimeStyles.None, out endDateTime))
                {
                    endDateValidator.IsValid = false;
                }
                if (endDateTime < minDate || endDateTime > maxDate)
                {
                    endDateValidator.IsValid = false;
                }

                if (!startDateValidator.IsValid)
                {
                    errors.Add(groupName + " " + groupCount + " - " + GetIncorrectDateTimeMessage(GeneralMessage.StartDateTimeIncorrect));
                }
                if (!endDateValidator.IsValid)
                {
                    errors.Add(groupName + " " + groupCount + " - " + GetIncorrectDateTimeMessage(GeneralMessage.EndDateTimeIncorrect));
                }

                if (startDateValidator.IsValid && endDateValidator.IsValid)
                    if (startDateTime > endDateTime)
                    {
                        var startEndDateLimitValidator = (CustomValidator)repeaterItem.FindControl("StartEndDateLimitValidator");
                        startEndDateLimitValidator.IsValid = false;
                        errors.Add(groupName + " " + groupCount + " - " + GeneralMessage.EndDateGreaterStartDate);
                    }

            } return errors;
        }
        private static string GetIncorrectDateTimeMessage(string beginning)
        {
            return string.Format(CultureInfo.CurrentCulture, beginning,
                CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
                ShortDateTimePattern);
        }

        private static string ShortDateTimePattern
        {
            get
            {
                return string.Format(CultureInfo.CurrentCulture, "{0} {1}",
                                     CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
                                     CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
            }
        }
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public Collection<IImageRotatorGroup> GetImageRotatorGroups()
        {
            foreach (RepeaterItem repeaterItem in ImageGroupsRepeater.Items)
            {
                var groupIdHiddenField = (HiddenField)repeaterItem.FindControl("ImageGroupIdValueHiddenField");
                int groupId = int.Parse(groupIdHiddenField.Value, CultureInfo.CurrentCulture);
                IImageRotatorGroup imageGroup = ImageRotatorGroups.FirstOrDefault(item => item.ImageGroupId == groupId);
                if (imageGroup == null)
                    continue;
                var siteStructureNodeSelector = (LekmerContentPageSelector)repeaterItem.FindControl("SiteStructureNodeSelector");
                imageGroup.InternalLinkContentNodeId = siteStructureNodeSelector.SelectedNodeId;

                var siteStructureNodeSelector2 = (LekmerContentPageSelector)repeaterItem.FindControl("SiteStructureNodeSelector2");
                imageGroup.InternalLinkContentNodeId2 = siteStructureNodeSelector2.SelectedNodeId;

                var siteStructureNodeSelector3 = (LekmerContentPageSelector)repeaterItem.FindControl("SiteStructureNodeSelector3");
                imageGroup.InternalLinkContentNodeId3 = siteStructureNodeSelector3.SelectedNodeId;

                var externalLinkTextBox = (TextBox)repeaterItem.FindControl("ExternalLinkTextBox");
                imageGroup.ExternalLink = externalLinkTextBox.Text.Trim();
                var isMoveCheckBox = (CheckBox)repeaterItem.FindControl("IsMoveCheckBox");
                imageGroup.IsMovie = isMoveCheckBox.Checked;

                var externalLinkTextBox2 = (TextBox)repeaterItem.FindControl("ExternalLinkTextBox2");
                imageGroup.ExternalLink2 = externalLinkTextBox2.Text.Trim();
                var isMoveCheckBox2 = (CheckBox)repeaterItem.FindControl("IsMoveCheckBox2");
                imageGroup.IsMovie2 = isMoveCheckBox2.Checked;

                var externalLinkTextBox3 = (TextBox)repeaterItem.FindControl("ExternalLinkTextBox3");
                imageGroup.ExternalLink3 = externalLinkTextBox3.Text.Trim();
                var isMoveCheckBox3 = (CheckBox)repeaterItem.FindControl("IsMoveCheckBox3");
                imageGroup.IsMovie3 = isMoveCheckBox3.Checked;

                var titleTextBox = (TextBox)repeaterItem.FindControl("TitleTextBox");
                imageGroup.Title = titleTextBox.Text.Trim();

                var ordinalTextBox = (TextBox)repeaterItem.FindControl("OrdinalTextBox");

                int ordinal = 0;
                if (!string.IsNullOrEmpty(ordinalTextBox.Text.Trim()))
                {
                    ordinal = int.Parse(ordinalTextBox.Text, CultureInfo.CurrentCulture);

                }
                imageGroup.Ordinal = ordinal;

                string startDate = ((TextBox)repeaterItem.FindControl("StartDateTextBox")).Text;
                string endDate = ((TextBox)repeaterItem.FindControl("EndDateTextBox")).Text;
                imageGroup.StartDateTime = string.IsNullOrEmpty(startDate) ? null : (DateTime?)DateTime.Parse(startDate, CultureInfo.CurrentCulture);
                imageGroup.EndDateTime = string.IsNullOrEmpty(endDate) ? null : (DateTime?)DateTime.Parse(endDate, CultureInfo.CurrentCulture);

                var captionsEditor = (Litium.Scensum.BackOffice.WebControls.LekmerTinyMceEditor)repeaterItem.FindControl("CaptionsEditor");
                imageGroup.HtmalMarkup = captionsEditor.GetValue();
            }
            return ImageRotatorGroups;
        }
    }
    [Serializable]
    public class ImageRotatorGroupsState
    {
        public int ImageRotatorId
        {
            get;
            set;
        }
        [SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<IImageRotatorGroup> ImageRotatorGroups
        {
            get;
            set;
        }
    }
}