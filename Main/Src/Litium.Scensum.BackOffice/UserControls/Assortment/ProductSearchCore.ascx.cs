﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class ProductSearchCore : UserControlController
	{
		private Collection<IProductStatus> _productStatuses;
		private Collection<IProductType> _productTypes;

		public event EventHandler<ProductSearchEventArgs> SearchEvent;

		protected virtual void OnSearch(ProductSearchEventArgs e)
		{
			if (SearchEvent != null)
			{
				SearchEvent(this, e);
			}
		}

		protected override void SetEventHandlers()
		{
			GVSearch.PageIndexChanging += ProductGridPageIndexChanging;
			GVSearch.RowDataBound += ProductGridRowDataBound;

			search.SearchEvent += ProductSearchEvent;
			btnAdd.Click += BtnAddClick;
			btnCancel.ServerClick += BtnCancelClick;
		}

		protected override void PopulateControl()
		{
		}

		private void BtnCancelClick(object sender, EventArgs e)
		{
			productDataSource.SelectParameters.Clear();
		}

		private void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var chkAllSearched = (CheckBox)row.FindControl("chkAllSearched");
				chkAllSearched.Checked = false;
				chkAllSearched.Attributes.Add("onclick", 
					"SelectAll1('" + chkAllSearched.ClientID + @"','" + GVSearch.ClientID + @"'); ShowBulkUpdatePanel('"
					+ chkAllSearched.ID + "', '" + GVSearch.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			if (row.RowType != DataControlRowType.DataRow)
				return;
			var product = (IProduct) row.DataItem;
			var lekmerProduct = (ILekmerProduct) product;

			var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
			lblProductStatus.Text = ResolveStatusTitle(product.ProductStatusId);

			var lblType = (Label)e.Row.FindControl("lblType");
			lblType.Text = ResolveTypeTitle(lekmerProduct.ProductTypeId);

			var lblPrice = (Label)e.Row.FindControl("lblPrice");
			if (lblPrice != null && product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}

			var lblSuplierArtNo = (Label)e.Row.FindControl("lblSuplierArtNo");
			lblSuplierArtNo.Text = lekmerProduct.SupplierArticleNumber;

			var selectAllCheckBox = (CheckBox)GVSearch.HeaderRow.FindControl("chkAllSearched");
			var cbSelect = (CheckBox)row.FindControl("cbSelect");
			cbSelect.Attributes.Add("onclick",
									"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" 
									+ cbSelect.ID + "', '" + GVSearch.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
		}

		private void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GVSearch.PageIndex = e.NewPageIndex;
		}

		protected virtual void ProductSearchEvent(object sender, ProductSearchCriteriaEventArgs e)
		{
			GVSearch.PageIndex = 0;
			SearchParamInit(e.SearchCriteria);
		}

		[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
		private void SearchParamInit(ILekmerProductSearchCriteria searchCriteria)
		{
			productDataSource.SelectParameters.Clear();
			productDataSource.SelectParameters.Add("maximumRows", GVSearch.PageSize.ToString(CultureInfo.CurrentCulture));
			productDataSource.SelectParameters.Add("startRowIndex", (GVSearch.PageIndex * GVSearch.PageSize).ToString(CultureInfo.CurrentCulture));
			productDataSource.SelectParameters.Add("categoryId", searchCriteria.CategoryId);
			productDataSource.SelectParameters.Add("erpId", searchCriteria.ErpId);
			productDataSource.SelectParameters.Add("title", searchCriteria.Title);
			productDataSource.SelectParameters.Add("statusId", searchCriteria.StatusId);
			productDataSource.SelectParameters.Add("priceFrom", searchCriteria.PriceFrom);
			productDataSource.SelectParameters.Add("priceTo", searchCriteria.PriceTo);
			productDataSource.SelectParameters.Add("eanCode", searchCriteria.EanCode);
			productDataSource.SelectParameters.Add("brandId", searchCriteria.BrandId);
			productDataSource.SelectParameters.Add("productTypeId", searchCriteria.ProductTypeId);
			productDataSource.SelectParameters.Add("supplierArticleNumber", searchCriteria.SupplierArticleNumber);
		}

		private void BtnAddClick(object sender, EventArgs e)
		{
			var products = new Collection<IProduct>();
			bool isAnyRowSelected = false;
			if (GVSearch.Rows.Count > 0)
			{
				foreach (GridViewRow row in GVSearch.Rows)
				{
					var selectCheckBox = (CheckBox)row.FindControl("cbSelect");
					if (selectCheckBox.Checked)
					{
						isAnyRowSelected = true;
						int productId = System.Convert.ToInt32(((HiddenField)row.FindControl("hfId")).Value, CultureInfo.CurrentCulture);
						if (products.FirstOrDefault(item => item.Id == productId) == null)
						{
							var productSecureService = IoC.Resolve<IProductSecureService>();
							IProduct product = productSecureService.GetById(ChannelHelper.CurrentChannel.Id, productId);
							products.Add(product);
						}
						selectCheckBox.Checked = false;
					}
				}
				var chkAllSearched = (CheckBox)GVSearch.HeaderRow.FindControl("chkAllSearched");
				chkAllSearched.Checked = false;
			}

			if (isAnyRowSelected)
			{
				productDataSource.SelectParameters.Clear();
				OnSearch(new ProductSearchEventArgs { Products = products });
			}
		}

		protected virtual string ResolveStatusTitle(int statusId)
		{
			if (_productStatuses == null)
			{
				var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
				_productStatuses = productStatusSecureService.GetAll();
			}
			if (_productStatuses == null)
			{
				_productStatuses = new Collection<IProductStatus>();
			}

			var productStatus = _productStatuses.First(item => item.Id == statusId).Title;

			return productStatus;
		}

		protected virtual string ResolveTypeTitle(int typeId)
		{
			if (_productTypes == null)
			{
				var productTypeSecureService = IoC.Resolve<IProductTypeSecureService>();
				_productTypes = productTypeSecureService.GetAll();
			}
			if (_productTypes == null)
			{
				_productTypes = new Collection<IProductType>();
			}

			var productType = _productTypes.First(item => item.Id == typeId).Title;

			return productType;
		}
	}
}