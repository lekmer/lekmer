﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using ProductTypeConstant = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class ProductListControlPackage : StateUserControlController<Collection<ProductStateItem>>
	{
		private Collection<IProductStatus> _productStatuses;
		private Collection<IProductType> _productTypes;

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ProductStateItem> Products
		{
			get
			{
				object productsStored = State;
				return (Collection<ProductStateItem>)productsStored;
			}
			set
			{
				State = value;
			}
		}

		public event EventHandler<PackageProductSearchEventArgs> ChangeProductListEvent;
		protected virtual void OnChangeProductListEvent(PackageProductSearchEventArgs e)
		{
			if (ChangeProductListEvent != null)
			{
				ChangeProductListEvent(this, e);
			}
		}

		protected override void SetEventHandlers()
		{
			GVSave.RowDataBound += ProductGridRowDataBound;
			GVSave.RowCommand += ProductGridRowCommand;
			GVSave.PageIndexChanging += ProductGridPageIndexChanging;
			productSearch.SearchEvent += ProductSearchSearchEvent;
			RemoveSelectedButton.Click += RemoveSelectedClick;
		}

		protected override void PopulateControl() { GridDataBind(); }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			GridDataBind();
		}

		private void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GVSave.PageIndex = e.NewPageIndex;
			GridDataBind();
		}

		private void ProductSearchSearchEvent(object sender, ProductSearchEventArgs e)
		{
			foreach (IProduct product in e.Products)
			{
				var newId = Guid.NewGuid().ToString();
				var productStateItem = new ProductStateItem {Id = newId, ProductItem = product};
				if (Products.FirstOrDefault(item => item.Id == newId) == null)
				{
					Products.Add(productStateItem);
				}
			}
			
			GridDataBind();
			upProductList.Update();

			OnChangeProductListEvent(new PackageProductSearchEventArgs { Products = Products });
		}

		private void GridDataBind()
		{
			GVSave.DataSource = Products;
			GVSave.DataBind();
			SetClientFunction();
		}

		private void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var product = ((ProductStateItem)row.DataItem).ProductItem;
			var lblPrice = (Label)e.Row.FindControl("lblPrice");
			if (product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}

			var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
			lblProductStatus.Text = ResolveStatusTitle(product.ProductStatusId);

			var lblType = (Label)e.Row.FindControl("lblType");
			lblType.Text = ResolveTypeTitle(((ILekmerProduct)product).ProductTypeId);
		}

		private void ProductGridRowCommand(object sender, CommandEventArgs e)
		{
			string id = e.CommandArgument.ToString();

			switch (e.CommandName)
			{
				case "DeleteProduct":
					var productStateItem = Products.First(item => item.Id == id);
					Products.Remove(productStateItem);
					break;
			}
			GridDataBind();
			OnChangeProductListEvent(new PackageProductSearchEventArgs { Products = Products });
		}

		protected string GetCategory(int categoryId)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			var category = categorySecureService.GetById(categoryId);
			return category != null ? category.Title : string.Empty;
		}

		protected virtual void RemoveSelectedClick(object sender, EventArgs e)
		{
			var ids = GVSave.GetSelectedStringIds();
			if (ids.Count() == 0) return;

			foreach (var id in ids)
			{
				var productStateItem = Products.First(item => item.Id == id);
				if (productStateItem == null) continue;

				Products.Remove(productStateItem);
			}
			GridDataBind();
			OnChangeProductListEvent(new PackageProductSearchEventArgs { Products = Products });
		}

		protected virtual void SetClientFunction()
		{
			if (GVSave.HeaderRow == null) return;
			var selectAllCheckBox = (CheckBox)GVSave.HeaderRow.FindControl("SelectAllCheckBox");
			selectAllCheckBox.Checked = false;
			selectAllCheckBox.Attributes.Add("onclick", 
				"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + GVSave.ClientID + @"'); ShowBulkUpdatePanel('"
				+ selectAllCheckBox.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			foreach (GridViewRow row in GVSave.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick", 
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" 
					+ cbSelect.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual string ResolveStatusTitle(int statusId)
		{
			if (_productStatuses == null)
			{
				var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
				_productStatuses = productStatusSecureService.GetAll();
			}
			if (_productStatuses == null)
			{
				_productStatuses = new Collection<IProductStatus>();
			}

			var productStatus = _productStatuses.First(item => item.Id == statusId).Title;

			return productStatus;
		}

		protected virtual string ResolveTypeTitle(int typeId)
		{
			if (_productTypes == null)
			{
				var productTypeSecureService = IoC.Resolve<IProductTypeSecureService>();
				_productTypes = productTypeSecureService.GetAll();
			}
			if (_productTypes == null)
			{
				_productTypes = new Collection<IProductType>();
			}

			var productType = _productTypes.First(item => item.Id == typeId).Title;

			return productType;
		}
	}

	[Serializable]
	public class ProductStateItem
	{
		public string Id { get; set; }
		public IProduct ProductItem { get; set; }
	}
}