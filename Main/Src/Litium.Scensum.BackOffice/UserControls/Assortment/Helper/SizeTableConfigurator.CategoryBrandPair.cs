using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.UserControls.Tree;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Helper
{
	public class SizeTableConfiguratorCategoryBrandPair
	{
		private const string DefaultValue = "-1";

		private Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private BrandCollection _brandCollection;
		public BrandCollection BrandCollection
		{
			get { return _brandCollection ?? (_brandCollection = IoC.Resolve<IBrandSecureService>().GetAll()); }
		}

		private SizeTableConfiguratorHelper _sizeTableConfiguratorHelper;
		private SizeTableConfiguratorHelper SizeTableConfiguratorHelper
		{
			get { return _sizeTableConfiguratorHelper ?? (_sizeTableConfiguratorHelper = new SizeTableConfiguratorHelper()); }
		}

		public System.Web.UI.WebControls.GridView Grid { get; set; }
		public ImageLinkButton OkButton { get; set; }
		public ImageLinkButton CancelButton { get; set; }
		public ImageLinkButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public UpdatePanel PopupUpdatePanel { get; set; }
		public NodeSelector PopupCategoryTree { get; set; }
		public DropDownList PopupBrands { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public Collection<ISizeTableCategoryBrandPair> CategoryBrandPairs { get; set; }
		public ModalPopupExtender Popup { get; set; }
		public Label ErrorLabel { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;

			PopupCategoryTree.NodeCommand += OnCategoryNodeCommand;

			OkButton.Click += OkButtonClick;
			CancelButton.Click += CancelButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(Collection<ISizeTableCategoryBrandPair> categoryBrandPairs)
		{
			PopulateCategoryNodeSelectTree(null);
			PopulateBrandSelection();
			DataBindGrid(ResolveCategoryBrandPairs(categoryBrandPairs));
		}

		protected virtual void PopulateCategoryNodeSelectTree(int? categoryId)
		{
			PopupCategoryTree.DataSource = CategorySelector(categoryId);
			PopupCategoryTree.DataBind();
		}

		private void PopulateBrandSelection()
		{
			PopupBrands.DataSource = BrandCollection;
			PopupBrands.DataBind();
			PopupBrands.Items.Insert(0, new ListItem("-- Select Brand --", DefaultValue));
		}

		public virtual void DataBindGrid(Collection<ISizeTableCategoryBrandPair> categories)
		{
			Grid.DataSource = categories;
			Grid.DataBind();

			GridUpdatePanel.Update();
		}

		public string GetCategoryPath(object cat)
		{
			var categoryBrandPair = cat as ISizeTableCategoryBrandPair;
			if (categoryBrandPair == null) return string.Empty;

			var category = categoryBrandPair.Category;

			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = RetrieveCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SizeTableConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ApplyToAllDiv);
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategoryBrandPair"))
			{
				RemoveCategoryBrandPair(e.CommandArgument.ToString());
				DataBindGrid(ResolveCategoryBrandPairs(CategoryBrandPairs));
			}
		}

		protected virtual void OnCategoryNodeCommand(object sender, CommandEventArgs e)
		{
			PopulateCategoryNodeSelectTree(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			int brandId = int.Parse(PopupBrands.SelectedValue);

			if (PopupCategoryTree.SelectedNodeId.HasValue)
			{
				var categoryId = PopupCategoryTree.SelectedNodeId.Value;

				var categoryBrandPair = brandId > 0 
					? CategoryBrandPairs.FirstOrDefault(cbp => cbp.CategoryId == categoryId && cbp.BrandId != null && cbp.BrandId.Value == brandId) 
					: CategoryBrandPairs.FirstOrDefault(cbp => cbp.CategoryId == categoryId && cbp.BrandId == null);

				if (categoryBrandPair == null)
				{
					var sizeTableCategoryBrandPair = IoC.Resolve<ISizeTableCategoryBrandPair>();
					sizeTableCategoryBrandPair.Id = Guid.NewGuid().ToString();
					sizeTableCategoryBrandPair.CategoryId = categoryId;
					sizeTableCategoryBrandPair.BrandId = brandId > 0 ? brandId : (int?) null;

					CategoryBrandPairs.Add(sizeTableCategoryBrandPair);
				}

				DataBindGrid(ResolveCategoryBrandPairs(CategoryBrandPairs));

				if (IncludeAllCheckbox != null && CategoryBrandPairs.Count > 0)
				{
					IncludeAllCheckbox.Checked = false;
				}

				PopupBrands.SelectedValue = DefaultValue;
				PopupCategoryTree.Clear();
				ErrorLabel.Visible = false;
			}
			else if (brandId > 0)
			{
				ErrorLabel.Visible = true;

				Popup.X = 227;
				Popup.Y = 227;
				Popup.Show();
			}

			ScriptManager.RegisterStartupScript(OkButton, GetType(), "include_items", "$(document).ready(function() { $('#tabs').tabs('select', 1); });", true);
		}

		protected virtual void CancelButtonClick(object sender, EventArgs e)
		{
			PopupBrands.SelectedValue = DefaultValue;
			PopupCategoryTree.Clear();
			ErrorLabel.Visible = false;

			ScriptManager.RegisterStartupScript(CancelButton, GetType(), "include_items", "$(document).ready(function() { $('#tabs').tabs('select', 1); });", true);
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			foreach (var categoryBrandPairId in GetSelectedFromGrid())
			{
				RemoveCategoryBrandPair(categoryBrandPairId);
			}

			DataBindGrid(ResolveCategoryBrandPairs(CategoryBrandPairs));
		}

		protected virtual void RemoveCategoryBrandPair(string categoryBrandPairId)
		{
			var categoryBrandPair = CategoryBrandPairs.FirstOrDefault(cbp => cbp.Id == categoryBrandPairId);
			if (categoryBrandPair != null)
			{
				CategoryBrandPairs.Remove(categoryBrandPair);
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<string> GetSelectedFromGrid()
		{
			return SizeTableConfiguratorHelper.GetSelectedStringIdsFromGrid(Grid, "IdHiddenField");
		}

		private static IEnumerable<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		public Collection<ISizeTableCategoryBrandPair> ResolveCategoryBrandPairs(Collection<ISizeTableCategoryBrandPair> categoryBrandPairs)
		{
			foreach (var categoryBrandPair in categoryBrandPairs)
			{
				var category = RetrieveCategory(categoryBrandPair.CategoryId);
				if (category == null)
				{
					throw new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Category with ID {0} could not be found.", categoryBrandPair.CategoryId));
				}

				var brand = RetrieveBrand(categoryBrandPair.BrandId);

				categoryBrandPair.Category = category;
				categoryBrandPair.Brand = brand;
			}
			return categoryBrandPairs;
		}

		protected virtual ICategory RetrieveCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}

		protected virtual IBrand RetrieveBrand(int? brandId)
		{
			var brand = IoC.Resolve<IBrand>();

			if (brandId.HasValue)
			{
				brand = BrandCollection.FirstOrDefault(b => b.Id == brandId.Value);
			}

			return brand;
		}
	}
}