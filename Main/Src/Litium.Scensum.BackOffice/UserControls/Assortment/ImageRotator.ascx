﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageRotator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ImageRotator" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree" TagPrefix="TemplatedTreeView" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/SiteStructure/ContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>
<%@ Register TagPrefix="uc" TagName="ImageGroup" Src="~/UserControls/Assortment/ImageRotatorGroup.ascx" %>

<script type="text/javascript">


	function ResetDefaultMediaMessages() {
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
	}

	
</script>





<uc:ImageGroup ID="ImageGroupControl" runat="server" />
	<div id="ImagesDiv" runat="server" class="product-popup-images-container" style="z-index: 10010;
		display: none;">
		<div id="product-popup-images-header">
			<div id="product-popup-images-header-left">
					
			</div>
			<div id="product-popup-images-header-center">
				<span><%= Resources.Product.Literal_AddImages %></span>
				<input type="button" id="_inpCloseImages" class="_inpClose" value="x"  />
			</div>
			<div id="product-popup-images-header-right">
				
			</div>
		</div>
		<br clear="all" />
		<div class="product-popup-images-body">
	    <uc:ImageSelect id="ImageSelectControl" runat="server"/>
	    </div>
    </div>    