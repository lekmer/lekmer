﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageRotatorGroupCollapsiblePanel.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ImageRotatorGroupCollapsiblePanel" %>
<div class="collapsible-items left">
	<div id="CollapsibleItemDiv" runat="server" class="collapsible-item left">
		<div class="collapsible-item-header left">
			<uc:LiteralEncoded ID="TitleLiteral" runat="server" />
		</div>
		<div id="ActionBlockDiv" class="action-block left" runat="server">
			<div class="left">
				<span class="collapsible-caption"><asp:Literal runat="server" ID="ActionCaptionLiteral" /></span>
			</div>
			<div class="link-button-removeGroup">
				<asp:Button ID="RemoveGroup"  OnClientClick='<%# "return DeleteConfirmation(\"" + "group" + "\");" %>' runat="server" Height="25px" ></asp:Button>		
			</div>
		</div>
		<div class="image-block right">
			<asp:HiddenField ID="CommandArgumentHidden" runat="server" />
			<asp:HiddenField ID="IsOpenedHidden" runat="server" Value="true" />	
			<asp:Image ID="IndicatorImage" runat="server" CssClass="right" />
		</div>
	</div>
</div>