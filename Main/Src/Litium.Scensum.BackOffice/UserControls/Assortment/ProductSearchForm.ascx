﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearchForm.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ProductSearchForm" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<asp:Panel ID="panel" runat="server">
	<div id="search-product-criteria">
		<div class="column-first">
			<div class="input-box">
				<span><%= Resources.Product.Label_CategoryChoose%></span>
				<br />
				<uc:NodeSelect ID="CategoryNodeSelector" runat="server" AllowClearSelection="true" />
			</div>
			<div class="input-box">
				<span><%= Resources.Lekmer.Choose_Brand%></span><br />
				<asp:DropDownList ID="ddlBrand" DataTextField="Title" DataValueField="Id" runat="server" />
			</div>
			<div class="input-box">
				<span><%= Resources.Product.Label_ArticleNumber%></span>
				<br />
				<asp:TextBox ID="tbErpId" runat="server" TextMode="MultiLine" />
			</div>
		</div>
		<div class="column">
			<div class="input-box">
				<span><%= Resources.General.Literal_Title %></span>
				<br />
				<asp:TextBox ID="tbTitle" runat="server" />
			</div>
			<div class="input-box">
				<span><%= Resources.General.Literal_Status %></span>
				<br />
				<asp:DropDownList ID="ddlStatus" DataTextField="Title" DataValueField="Id" runat="server" />
			</div>
			<div class="input-box">
				<span><%= Resources.General.Literal_Type %></span>
				<br />
				<asp:DropDownList ID="ddlProductType" DataTextField="Title" DataValueField="Id" runat="server" />
			</div>
		</div>
		<div class="column-last">
			<div style="width: 51%; float: left">
				<div class="input-box" style="width: 100%;">
					<span><%= Resources.Product.Label_PriceFrom%></span>
					&nbsp;
					<asp:CustomValidator ID="cvPriceFrom" runat="server" ControlToValidate="tbPriceFrom" Text="*" ClientValidationFunction="CheckPrice" />
					<asp:CompareValidator Display="None" runat="server" ID="comperePrice" ControlToValidate="tbPriceFrom" Text="*" ControlToCompare="tbPriceTo" Type="Double" Operator="LessThanEqual" />
					<br />
					<asp:TextBox ID="tbPriceFrom" runat="server" CssClass="price" />
					<span>-</span>
				</div>
			</div>
			<div style="width: 49%; float: left">
				<div class="input-box" style="width: 100%;">
					&nbsp;<span><%= Resources.Product.Label_PriceTo%></span> &nbsp;
					<asp:CustomValidator runat="server" Text="*" ClientValidationFunction="CheckPrice" ControlToValidate="tbPriceTo" ID="cvPriceTo" />
					<br />
					&nbsp;&nbsp;<asp:TextBox ID="tbPriceTo" runat="server" CssClass="price price-to" />
				</div>
			</div>
		</div>
		<div class="column-last">
			<div class="input-box">
				<span><%= Resources.Product.Label_EanCode%></span>
				<br />
				<asp:TextBox ID="tbEanCode" runat="server" />
			</div>
		</div>
	</div>
</asp:Panel>
