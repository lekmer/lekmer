<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VariationTypeItem.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.VariationTypeItem"%>

<div class="variation-type-item">
<div class="variation-type-item-main">
	<div class="top">
		<div class="variation-type-header">
			<asp:Label runat="server" ID="lblHeader" ></asp:Label>
		</div>
		<div class="action">
			<asp:ImageButton ID="imbVariationTypeUp" runat="server" ImageUrl="~/Media/Images/SiteStructure/up.png" />
			<asp:ImageButton ID="imbVariationTypeDown" runat="server" ImageUrl="~/Media/Images/SiteStructure/down.png" />
			<asp:ImageButton ID="imbVariationTypeDelete" runat="server" ImageUrl="~/Media/Images/Common/delete.gif" />
		</div>
	</div>	
	<div class="variation-type-content">
		<div class="first-column">
			<span><%= Resources.General.Literal_Title %> *</span>&nbsp;				
			<asp:RequiredFieldValidator ID="rfvVariationTypeTitle" runat="server" EnableClientScript="false"  Display ="None" 
			        ControlToValidate="tbTitle" />
			<asp:Label ID="lblSameTitleError" runat="server" ForeColor="Red" Text="<%$ Resources:ProductMessage, VariationTypeTitleExist %>"></asp:Label>
			<br />
			<asp:TextBox ID="tbTitle" runat="server" class="variation-field" AutoPostBack="true" MaxLength="250"></asp:TextBox>
			<asp:HiddenField ID="hdfVariationTypeOrdinal" runat="server" />
		</div>
	
	<div class="column">
			<asp:Panel runat="server" ID="pnlValue" runat="server" DefaultButton="btnVariationCriterieAdd">
				<div style="float: left;"><span><%= Resources.Product.Literal_Value %>&nbsp; *</span>&nbsp;				
				<asp:RequiredFieldValidator ID="rfvVariationValue" runat="server" EnableClientScript="false"
						ErrorMessage="<%$Resources:ProductMessage,ValueEmpty %>"  Display ="None" ValidationGroup="vgVariation" ControlToValidate="tbCriterieValue" /><br />
					
			
				<asp:TextBox ID="tbCriterieValue" runat="server" style="width :225px; margin-top: 5px;" MaxLength="50"></asp:TextBox> </div><div style="padding-left: 3px;padding-top: 17px;float: left;width: 50px; vertical-align: middle;">
				<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnVariationCriterieAdd" runat="server" SkinID="DefaultButton" Text="<%$ Resources:General, Button_Add %>"/>
				</div>
			</asp:Panel>
				
			<asp:Label ID="lblSameValue" Visible="false" runat="server" ForeColor="Red" Text="<%$ Resources:ProductMessage, ValueExist %>"></asp:Label><br />
						<div class="variation-items left">
								
				<asp:GridView ID="gvVariations" runat="server" AutoGenerateColumns="false" SkinID="grid" Width="100%">
					<Columns>
						<asp:TemplateField HeaderText="<%$Resources:General, Literal_Title %>" ItemStyle-Width="100px" ItemStyle-Wrap=true>
							<ItemTemplate>	
							    <div style="overflow: hidden; width: 150px;">	
									<asp:LinkButton runat="server" ID="lbtnEdit" CommandName="EditVariation" CommandArgument='<%#Eval("Ordinal")%>' />
								</div>
							</ItemTemplate>
						</asp:TemplateField>						
						<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
							ItemStyle-Width="15%">
							<HeaderTemplate>
								<div class="inline">
									<asp:ImageButton ID="btnRefreshOrder" runat="server" CommandName="RefreshOrder" ImageUrl="~/Media/Images/Common/refresh.png"
										ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vg"/>
								</div>
							</HeaderTemplate>
							<ItemTemplate>
								<div class="inline" style="white-space: nowrap;">
								   
									<asp:RequiredFieldValidator runat="server" ID="v" ControlToValidate="tbOrdinal" Text="*" ValidationGroup="vg" />
									 <asp:TextBox runat="server" ID="tbOrdinal" Text='<%#Eval("Ordinal") %>'  />
									<asp:RangeValidator runat="server" ID="rv" ControlToValidate="tbOrdinal" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vg"></asp:RangeValidator>
									
									<asp:ImageButton runat="server" ID="ibtnUp" CommandName="UpOrdinal" CommandArgument='<%#Eval("Ordinal") %>'
										ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="<%$ Resources:General, Button_Up %>" ValidationGroup="vg"/>
									<asp:ImageButton runat="server" ID="ibtnDown" CommandName="DownOrdinal" CommandArgument='<%#Eval("Ordinal") %>'
										ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="<%$ Resources:General, Button_Down %>" ValidationGroup="vg"/>
									</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
							ItemStyle-Width="5%" HeaderText="<%$ Resources:General, Literal_Delete %>">
							<ItemTemplate>
									<asp:ImageButton runat="server" ID="ibtnDelete" CommandName="DeleteVariation" CommandArgument='<%#Eval("Ordinal") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"/>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>	</div>
				<br />	
		</div>
		
	</div>
		
</div>
</div>

	
	
