﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Handlers
{
	/// <summary>
	/// Summary description for ImageRotatorGroupDeleteHandler
	/// </summary>
	public class ImageRotatorGroupDeleteHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");
			//upload images

				var userName = context.Request["userName"];
				var blockId = Int32.Parse(context.Request["blockId"]);
				var groupId = Int32.Parse(context.Request["groupsId"]);
				var imageType = context.Request["imageType"];

				var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
				var userFull = systemUserSecureService.GetFullByUserName(userName);


				var productImageGroupSecureService = IoC.Resolve<IImageRotatorGroupSecureService>();
				var imageRotatorGroup = productImageGroupSecureService.GetByBlockId(userFull, blockId).FirstOrDefault(t => t.ImageGroupId == groupId);
				if (imageRotatorGroup != null)
				{
					if (imageType == "main")
					{
						imageRotatorGroup.MainImageId = null;
					}
					else
					{
						imageRotatorGroup.ThumbnailImageId = null;
					}
					var blockService = IoC.Resolve<IImageRotatorGroupSecureService>();
					blockService.Save(imageRotatorGroup);
				}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}