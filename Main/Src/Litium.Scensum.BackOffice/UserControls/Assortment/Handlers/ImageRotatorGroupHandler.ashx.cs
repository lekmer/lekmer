﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Media.MediaFolder;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Handlers
{
	/// <summary>
	/// Summary description for ImageRotatorGroupHandler
	/// </summary>
	public class ImageRotatorGroupHandler : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");
			//upload images
			if (context.Request.Files.Count > 0)
			{
				var userName = context.Request["userName"];
				var blockId = Int32.Parse(context.Request["blockId"]);
				var groupId = Int32.Parse(context.Request["groupsId"]);
				var imageType = context.Request["imageType"];
				
				var file = context.Request.Files[0];

				var service = IoC.Resolve<IMediaItemSecureService>();
				var extension = Path.GetExtension(file.FileName).TrimStart(new[] { '.' });
				if (string.IsNullOrEmpty(extension))
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}
				IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension);
				if (mediaFormat == null)
				{
					throw new UploadException(UploadExceptionReason.IncorrectFormat);
				}

				var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
				var userFull = systemUserSecureService.GetFullByUserName(userName);
				var mediaFolderService = IoC.Resolve<ILekmerMediaFolderSecureService>();
				var folder = MediaFolderHepler.GetFolderByType(mediaFolderService, MediaFolderType.Banners);
				if (null != folder)
				{					
					var fileId = service.Insert(userFull, file.InputStream, folder.Id, file.FileName);
					file.InputStream.Close();

					var productImageGroupSecureService = IoC.Resolve<IImageRotatorGroupSecureService>();
					var imageRotatorGroup =
						productImageGroupSecureService.GetByBlockId(userFull, blockId).FirstOrDefault(t => t.ImageGroupId == groupId);
					if (imageRotatorGroup != null)
					{
						if (imageType == "main")
						{
							imageRotatorGroup.MainImageId = fileId;
						}
						else
						{
							imageRotatorGroup.ThumbnailImageId = fileId;
						}
						var blockService = IoC.Resolve<IImageRotatorGroupSecureService>();
						blockService.Save(imageRotatorGroup);
					}
				}
			}
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}