﻿using System;
using Litium.Scensum.BackOffice.Modules.Assortment.Sizes;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class SizeTableRowSelectEventArgs : EventArgs
	{
		public SizeTableRowItem SizeTableRowItem
		{
			get;
			set;
		}
	}
}