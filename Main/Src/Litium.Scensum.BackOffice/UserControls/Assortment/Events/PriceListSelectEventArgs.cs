﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class PriceListSelectEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IPriceList> PriceLists
		{
			get;
			set;
		}
	}
}
