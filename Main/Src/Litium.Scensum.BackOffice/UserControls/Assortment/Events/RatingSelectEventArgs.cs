﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class RatingSelectEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<RatingState> Ratings { get; set; }
	}
}