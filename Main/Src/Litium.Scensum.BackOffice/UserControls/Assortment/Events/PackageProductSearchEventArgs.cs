﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class PackageProductSearchEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ProductStateItem> Products
		{
			get;
			set;
		}
	}
}
