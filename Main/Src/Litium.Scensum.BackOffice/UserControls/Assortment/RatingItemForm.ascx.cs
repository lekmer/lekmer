﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using AjaxControlToolkit;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Ratings;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	[SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
	public partial class RatingItemForm : StateUserControlController<RatingItemState>
	{
		public event EventHandler<RatingItemSelectEventArgs> SaveEvent;

		public ModalPopupExtender Popup
		{
			get
			{
				return RatingItemPopup;
			}
		}

		protected override void SetEventHandlers()
		{
			AddButton.Click += OnAdd;
			CancelButton.ServerClick += OnCancel;

			DescriptionTranslator.TriggerImageButton = DescriptionTranslateButton;
		}

		protected override void PopulateControl()
		{
			BindData(null);
		}

		public virtual void Populate(RatingStateItem ratingStateItem)
		{
			BindData(ratingStateItem);
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			BindData(null);
		}

		protected virtual void OnAdd(object sender, EventArgs e)
		{
			var ratingItem = IoC.Resolve<IRatingItem>();
			ratingItem.Id = !string.IsNullOrEmpty(IdHiddenField.Value) ? int.Parse(IdHiddenField.Value) : 0;
			ratingItem.Title = TitleTextBox.Text;
			ratingItem.Description = DescriptionTextBox.Text;
			ratingItem.Score = int.Parse(ScoreTextBox.Text);
			ratingItem.TitleTranslations = TitleTranslator.GetTranslations();
			ratingItem.DescriptionTranslations = DescriptionTranslator.GetTranslations();

			var ratingStateItem = new RatingStateItem
			{
				Id = !string.IsNullOrEmpty(State.Id) ? State.Id : Guid.NewGuid().ToString(),
				RatingItem = ratingItem
			};

			SaveEvent(this, new RatingItemSelectEventArgs
				{
					RatingStateItem = ratingStateItem
				});

			BindData(null);

			RatingItemPopup.Hide();
		}

		protected virtual void BindData(RatingStateItem ratingStateItem)
		{
			if (State == null)
			{
				State = new RatingItemState();
			}

			State.Id = ratingStateItem != null ? ratingStateItem.Id : string.Empty;

			IdHiddenField.Value = ratingStateItem != null ? ratingStateItem.RatingItem.Id.ToString(CultureInfo.InvariantCulture) : string.Empty;
			TitleTextBox.Text = ratingStateItem != null ? ratingStateItem.RatingItem.Title : string.Empty;
			DescriptionTextBox.Text = ratingStateItem != null ? ratingStateItem.RatingItem.Description : string.Empty;
			ScoreTextBox.Text = ratingStateItem != null ? ratingStateItem.RatingItem.Score.ToString(CultureInfo.InvariantCulture) : string.Empty;

			ScoreTextBox.ReadOnly = !string.IsNullOrEmpty(ScoreTextBox.Text);

			PopulateTranslations(ratingStateItem);

			RatingItemFormUpdatePanel.Update();
		}

		protected virtual void PopulateTranslations(RatingStateItem ratingStateItem)
		{
			DescriptionTranslator.TriggerImageButton = DescriptionTranslateButton;

			TitleTranslator.DefaultValueControlClientId = TitleTextBox.ClientID;
			DescriptionTranslator.DefaultValueControlClientId = DescriptionTextBox.ClientID;

			if (ratingStateItem != null)
			{
				TitleTranslator.BusinessObjectId = 1;
				DescriptionTranslator.BusinessObjectId = 1;

				ratingStateItem.RatingItem.TitleTranslations.ReplaceNullValuesWithEmptyString();
				ratingStateItem.RatingItem.DescriptionTranslations.ReplaceNullValuesWithEmptyString();

				TitleTranslator.DataSource = ratingStateItem.RatingItem.TitleTranslations ?? new Collection<ITranslationGeneric>();
				DescriptionTranslator.DataSource = ratingStateItem.RatingItem.DescriptionTranslations ?? new Collection<ITranslationGeneric>();
			}
			else
			{
				TitleTranslator.BusinessObjectId = 0;
				DescriptionTranslator.BusinessObjectId = 0;

				TitleTranslator.DataSource = new Collection<ITranslationGeneric>();
				DescriptionTranslator.DataSource = new Collection<ITranslationGeneric>();
			}

			TitleTranslator.DataBind();
			DescriptionTranslator.DataBind();
			DescriptionTranslator.Populate();
		}
	}

	[Serializable]
	public sealed class RatingItemState
	{
		public string Id { get; set; }
	}
}