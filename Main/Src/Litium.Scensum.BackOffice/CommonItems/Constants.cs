using System;

namespace Litium.Scensum.BackOffice.CommonItems
{
	public static class Constants
	{
		public static readonly DateTime MinDate = new DateTime(1753, 1, 1);
		public static readonly DateTime MaxDate = new DateTime(9999, 12, 31);
		public static readonly DateTime ExampleDate = new DateTime(2015, 11, 22, 12, 45, 30);

		public static readonly string MceBogusLine = "<br mce_bogus=\"1\">";
	}
}