using System.Web;

namespace Litium.Scensum.BackOffice.Base
{
	public static class PageInfoHelper
	{
		private const string GRID_PAGE_INDEX = "p";

		/// <summary>
		/// Get parameters from QueryString
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public static int? GetGridPageIndex()
		{
			int value;
			return int.TryParse(HttpContext.Current.Request.QueryString[GRID_PAGE_INDEX], out value) ? (int?)value : null;
		}
	}
}
