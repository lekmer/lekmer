using Litium.Framework.Setting;

namespace Litium.Scensum.BackOffice.Setting
{
	public sealed class OrderFeatureSetting : SettingBase
	{
		#region Singleton

		private static readonly OrderFeatureSetting _instance = new OrderFeatureSetting();

		private OrderFeatureSetting()
		{
		}

		public static OrderFeatureSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "OrderFeature.BackOffice";

		protected override string StorageName
		{
			get { return "OrderFeature.BackOffice"; }
		}

		public bool EnableAddressEdit
		{
			get { return GetBoolean(_groupName, "EnableAddressEdit"); }
		}

		public bool EnableOrderItemAdd
		{
			get { return GetBoolean(_groupName, "EnableOrderItemAdd"); }
		}

		public bool EnableOrderItemDelete
		{
			get { return GetBoolean(_groupName, "EnableOrderItemDelete"); }
		}

		public bool EnableOrderItemChangeQuantity
		{
			get { return GetBoolean(_groupName, "EnableOrderItemChangeQuantity"); }
		}

		public bool EnableOrderSplit
		{
			get { return GetBoolean(_groupName, "EnableOrderSplit"); }
		}

		public bool EnableFreightEdit
		{
			get { return GetBoolean(_groupName, "EnableFreightEdit"); }
		}

		public bool EnablePaymentAdd
		{
			get { return GetBoolean(_groupName, "EnablePaymentAdd"); }
		}

		public bool EnablePaymentEdit
		{
			get { return GetBoolean(_groupName, "EnablePaymentEdit"); }
		}

		public bool EnablePaymentDelete
		{
			get { return GetBoolean(_groupName, "EnablePaymentDelete"); }
		}

		public bool EnablePaymentCostEdit
		{
			get { return GetBoolean(_groupName, "EnablePaymentCostEdit"); }
		}

		public bool EnableStatusChange
		{
			get { return GetBoolean(_groupName, "EnableStatusChange"); }
		}

		public bool EnableDeliveryTypeChange
		{
			get { return GetBoolean(_groupName, "EnableDeliveryTypeChange"); }
		}

		public bool EnableDeliveryTrackingIdEdit
		{
			get { return GetBoolean(_groupName, "EnableDeliveryTrackingIdEdit"); }
		}

		public bool EnableCommentAdd
		{
			get { return GetBoolean(_groupName, "EnableCommentAdd"); }
		}

		public bool EnableResendOrderConfirmation
		{
			get { return GetBoolean(_groupName, "EnableResendOrderConfirmation"); }
		}
	}
}