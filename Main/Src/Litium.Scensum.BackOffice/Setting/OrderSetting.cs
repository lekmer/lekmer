using Litium.Framework.Setting;

namespace Litium.Scensum.BackOffice.Setting
{
	public class OrderSetting : SettingBase
	{
		#region Singleton

		private static readonly OrderSetting _instance = new OrderSetting();

		private OrderSetting()
		{
		}

		public static OrderSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "Order.BackOffice"; }
		}

		public string OrderStatusesIncludedInStatistics
		{
			get { return GetString(_groupName, "OrderStatusesIncludedInStatistics"); }
		}
	}
}