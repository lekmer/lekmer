﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Customers/CustomerGroups/CustomerGroups.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.Default" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
</asp:Content>
<asp:Content ID="DefaultContent" ContentPlaceHolderID="CustomerGroupsPlaceHolder" runat="server">
	<script type="text/javascript">
        function confirmDelete() {
        	return DeleteConfirmation("<%= Resources.Customer.Literal_customerGroup %>");
        }
	</script>
	<div class="customer-groups">
		<sc:GridViewWithCustomPager ID="GroupGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" Width="100%">
			<Columns>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<HeaderTemplate>
						<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
					</HeaderTemplate>
					<ItemTemplate>
						<asp:CheckBox ID="SelectCheckBox" runat="server" />
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
					</ItemTemplate>
				</asp:TemplateField>	
				<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Title %>" ItemStyle-Width="64%" >
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="GroupTitleLink" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>'> </uc:HyperLinkEncoded>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_ErpId %>" DataField="ErpId" ItemStyle-Width="15%" />
				<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Status %>" ItemStyle-Width="15%" >
					<ItemTemplate>
						<uc:LiteralEncoded ID="GroupStatusLiteral" runat="server" Text='<%# GetCustomerGropStatus(Eval("StatusId")) %>'></uc:LiteralEncoded>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Path" ItemStyle-Width="60%">
					<ItemTemplate>
						<asp:Label ID="PathLabel" runat="server" Text='<%# IsSearchResult ? GetPath(Eval("FolderId")) : string.Empty %>' ></asp:Label>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<ItemTemplate>
						<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteGroup"
							CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
							OnClientClick="return confirmDelete();" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
	<div id="AllSelectedDiv" class="apply-to-all-selected-2" runat="server" style="display: none;">
		<div style="float:left">
			<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
			<asp:DropDownList ID="StatusList" DataTextField="Title" Width="100px" DataValueField="Id" runat="server" /> &nbsp;
		</div> 
		<div style="float:left">
			<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="SetStatusButton" runat="server" Text="<%$ Resources:General,Button_Set %>" />
			<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="DeleteGroupButton" runat="server" Text="<%$ Resources:General,Button_Delete %>" OnClientClick="return confirmDelete();" />
		</div> 
	</div>
</asp:Content>
