using System.Collections.ObjectModel;
using Litium.Lekmer.NewsletterSubscriber;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Customers.Newsletter
{
	public class NewsletterSubscriberDataSource
	{
		private int _rowCount;

		public Collection<INewsletterSubscriber> GetMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public int SelectCount(int maximumRows, int startRowIndex, string email)
		{
			return _rowCount;
		}

		public Collection<INewsletterSubscriber> GetMethod(int maximumRows, int startRowIndex, string email)
		{
			SearchCriteriaState<string>.Instance.Save(LekmerPathHelper.Newsletter.GetDefaultUrl(), email);
			var newsletterSubscriberService = IoC.Resolve<INewsletterSubscriberSecureService>();
			return newsletterSubscriberService.SearchByEmail(ChannelHelper.CurrentChannel.Id, email, startRowIndex / maximumRows + 1, maximumRows, out _rowCount);
		}
	}
}