using System;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.NewsletterSubscriber;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Customers.Newsletter
{
	public partial class Default : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			SearchButtonSubscriber.Click += SearchByEmail;
			SearchSubscribedResultsGrid.PageIndexChanging += SubscribedGridPageIndexChanging;
			SearchSubscribedResultsGrid.RowCommand += SubscribedResultsGridRowCommand;
			SearchUnsubscribedResultsGrid.PageIndexChanging += UnsubscribedGridPageIndexChanging;
			SearchUnsubscribedResultsGrid.RowCommand += UnsubscribedResultsGridRowCommand;
			SearchUnsubscribedResultsGrid.RowDataBound += UnsubscribedResultsGridRowDataBound;
		}

		protected override void PopulateForm()
		{
			RestoreSearchCriteria();
			DoSearch();
		}

		protected virtual void SubscribedGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SearchSubscribedResultsGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void UnsubscribedGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SearchUnsubscribedResultsGrid.PageIndex = e.NewPageIndex;
		}

		protected void UnsubscribedResultsGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var options = ((INewsletterUnsubscriber)row.DataItem).UnsubscriberOptions;
			if (options != null)
			{
				string unregOptions = string.Empty;
				
				foreach (var option in options)
				{
					switch (option.NewsletterTypeId)
					{
						case (int) NewsletterType.InterspireContactList:
							unregOptions += "All" + ", ";
							break;
						case (int) NewsletterType.MonitorProduct:
							unregOptions += "Monitor product" + ", ";
							break;
						case (int) NewsletterType.OrderReview:
							unregOptions += "Order review" + ", ";
							break;
					}
				}
				unregOptions = unregOptions.Remove(unregOptions.Length - 2, 2);

				var lblUnregisteredFrom = (Label)e.Row.FindControl("UnregisteredFromLabel");
				lblUnregisteredFrom.Text = unregOptions;
			}
		}

		protected virtual void SubscribedResultsGridRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "Unsubscribe":
					var subscriberSecureService = IoC.Resolve<INewsletterSubscriberSecureService>();
					var unsubscriberSecureService = IoC.Resolve<INewsletterUnsubscriberSecureService>();
					var unsubscriberOptionSecureService = IoC.Resolve<INewsletterUnsubscriberOptionSecureService>();

					var subscriber = subscriberSecureService.Get(id);
					bool unregistered = subscriberSecureService.Delete(SignInHelper.SignedInSystemUser, subscriber.Id);

					var unsubscriber = unsubscriberSecureService.Get(subscriber.Id) ?? unsubscriberSecureService.Create(ChannelHelper.CurrentChannel.Id);
					unsubscriber.Email = subscriber.Email;

					ApplyCustomUnsubscriberOption(unsubscriber, (int)NewsletterType.MonitorProduct, unsubscriberOptionSecureService);
					ApplyCustomUnsubscriberOption(unsubscriber, (int)NewsletterType.OrderReview, unsubscriberOptionSecureService);
					ApplyCustomUnsubscriberOption(unsubscriber, (int)NewsletterType.InterspireContactList, unsubscriberOptionSecureService);

					int unsubscriberId = unsubscriberSecureService.Save(SignInHelper.SignedInSystemUser, unsubscriber);

					unregistered = unregistered || unsubscriberId > 0;

					var monitorProductSecureService = IoC.Resolve<IMonitorProductSecureService>();
					monitorProductSecureService.Delete(SignInHelper.SignedInSystemUser, ChannelHelper.CurrentChannel.Id, subscriber.Email);

					DoSearch();

					SearchSubscribedResultUpdatePanel.Update();
					SearchUnsubscribedResultUpdatePanel.Update();

					if (!unregistered)
					{
						SystemMessageContainer.Add(Resources.GeneralMessage.SaveFailed, InfoType.Failure);
					}
					else
					{
						SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessful, InfoType.Success);
					}

					break;
			}
		}

		protected virtual void UnsubscribedResultsGridRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "Subscribe":
					var unsubscriberSecureService = IoC.Resolve<INewsletterUnsubscriberSecureService>();
					var unsubscriber = unsubscriberSecureService.Get(id);
					unsubscriberSecureService.Delete(SignInHelper.SignedInSystemUser, id);
					if (unsubscriber != null)
					{
						var subscriberSecureService = IoC.Resolve<INewsletterSubscriberSecureService>();
						var subscriber = subscriberSecureService.Create(ChannelHelper.CurrentChannel.Id);
						subscriber.Email = unsubscriber.Email;
						subscriber.SubscriberTypeId = (int)NewsletterSubscriberType.BackOffice;
						subscriberSecureService.Save(SignInHelper.SignedInSystemUser, subscriber);
					}

					DoSearch();

					SearchSubscribedResultUpdatePanel.Update();
					SearchUnsubscribedResultUpdatePanel.Update();

					SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessful, InfoType.Success);

					break;
			}
		}

		protected virtual void SearchByEmail(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				return;
			}

			DoSearch();
		}

		protected virtual void DoSearch()
		{
			SearchSubscribedResultsGrid.PageIndex = 0;
			SearchUnsubscribedResultsGrid.PageIndex = 0;
			PopulateSubscribedCustomersSearch();
			PopulateUnsubscribedCustomersSearch();
			ResultsDiv.Visible = true;
		}

		protected void RestoreSearchCriteria()
		{
			string email = SearchCriteriaState<string>.Instance.Get(LekmerPathHelper.Newsletter.GetDefaultUrl()) ?? string.Empty;
			EmailTextBox.Text = email;
		}

		protected void PopulateSubscribedCustomersSearch()
		{
			NewsletterSubscriberDataSource.SelectParameters.Clear();

			NewsletterSubscriberDataSource.SelectParameters.Add("maximumRows", SearchSubscribedResultsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			NewsletterSubscriberDataSource.SelectParameters.Add("startRowIndex", (SearchSubscribedResultsGrid.PageIndex * SearchSubscribedResultsGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			NewsletterSubscriberDataSource.SelectParameters.Add("email", EmailTextBox.Text.Length != 0 ? EmailTextBox.Text : null);

			SearchSubscribedResultsGrid.DataBind();
		}

		protected void PopulateUnsubscribedCustomersSearch()
		{
			NewsletterUnsubscriberDataSource.SelectParameters.Clear();

			NewsletterUnsubscriberDataSource.SelectParameters.Add("maximumRows", SearchUnsubscribedResultsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			NewsletterUnsubscriberDataSource.SelectParameters.Add("startRowIndex", (SearchUnsubscribedResultsGrid.PageIndex * SearchUnsubscribedResultsGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			NewsletterUnsubscriberDataSource.SelectParameters.Add("email", EmailTextBox.Text.Length != 0 ? EmailTextBox.Text : null);

			SearchUnsubscribedResultsGrid.DataBind();
		}

		protected virtual void ApplyCustomUnsubscriberOption(INewsletterUnsubscriber unsubscriber, int optionType, INewsletterUnsubscriberOptionSecureService optionService)
		{
			if (unsubscriber.UnsubscriberOptions.All(o => o.NewsletterTypeId != optionType))
			{
				INewsletterUnsubscriberOption unsubscriberOption = optionService.Create();
				unsubscriberOption.NewsletterTypeId = optionType;
				unsubscriber.UnsubscriberOptions.Add(unsubscriberOption);
			}
		}
	}
}