using System.Collections.ObjectModel;
using Litium.Lekmer.NewsletterSubscriber;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Customers.Newsletter
{
	public class NewsletterUnsubscriberDataSource
	{
		private int _rowCount;

		public Collection<INewsletterUnsubscriber> GetMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public int SelectCount(int maximumRows, int startRowIndex, string email)
		{
			return _rowCount;
		}

		public Collection<INewsletterUnsubscriber> GetMethod(int maximumRows, int startRowIndex, string email)
		{
			var newsletterUnsubscriberService = IoC.Resolve<INewsletterUnsubscriberSecureService>();
			return newsletterUnsubscriberService.SearchByEmail(ChannelHelper.CurrentChannel.Id, email, startRowIndex / maximumRows + 1, maximumRows, out _rowCount);
		}
	}
}