﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.DropShip.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="DropShipTab" ContentPlaceHolderID="body" runat="server">
	<sc:ToolBoxPanel ID="DropShipToolBoxPanel" runat="server" Text="DropShip information" />
	<div class="stores-place-holder">
		<sc:GridViewWithCustomPager
			ID="DropShipInfoGrid"
			SkinID="grid"
			AutoGenerateColumns="false"
			Width="100%"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			PagerSettings-Mode="NumericFirstLast"
			runat="server">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:Order,Literal_OrderId %>" ItemStyle-Width="25%">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="DropShipInfoLink" runat="server" Text='<%# Eval("OrderId")%>' NavigateUrl='<%# GetEditUrl(Eval("OrderId")) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_SupplierName %>" ItemStyle-Width="25%">
					<ItemTemplate>
						<%# GetSupplier(Eval("SupplierNo").ToString())%>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:Lekmer,Literal_CreatedDate %>" DataField="CreatedDate" ItemStyle-Width="25%" />
				<asp:BoundField HeaderText="<%$ Resources:Lekmer,Literal_SendDate %>" DataField="SendDate" ItemStyle-Width="25%" />
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
</asp:Content>