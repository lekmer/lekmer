﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="PageLayout.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.PageLayout" %>
<%@ Register Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" TagName="ContentPageSelector" TagPrefix="uc" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer runat="server" ID="Messages" MessageType="Success" HideMessagesControlId="SaveButton" />
	<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" ValidationGroup="DefaultValidationGroup" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" />
	
			</ContentTemplate>
		</asp:UpdatePanel>


</asp:Content>
<asp:Content ID="PageEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:UpdatePanel id="EditUpdatePanel" runat="server">
		<ContentTemplate>
   
	<div class="content-box" style="width:626px;height:180px">
	    <div class="left" style="width:50%;">
			<div class="div-label">
				<asp:Label runat="server" ID="CurrentLabel"></asp:Label>
			</div>
			<div class="div-label"><asp:Label runat="server" ID="TemplateTitleLabel" /></div>
	        <asp:Image runat="server" ID="TemplateImage" style="max-width:150px;max-height:150px;" />
	    </div>		
		<div class="left" style="width:50%;">
			<div class="div-label">
				<asp:Label runat="server" ID="NewLabel"></asp:Label>
			</div>
			<div class="div-label">
				<asp:DropDownList runat="server" ID="AvailableTemplatesList" AutoPostBack="true" />
				<uc:ContentPageSelector ID="MasterPageSelector" runat="server" AllowClearSelection="false" OnlyMasterPagesSelectable="true"/>
			</div>
	        <asp:Image runat="server" ID="NewTemplateImage" style="max-width:150px;max-height:150px;" />
		</div>
	</div>
	<br />	
   
	<div class="content-box" style="width:626px;">
		<div class="div-label"><%= Resources.SiteStructureMessage.MoveAllBlocksFromOriginToDestinationArea %></div>
		<asp:GridView runat="server" ID="AreasGrid" AutoGenerateColumns="false" SkinID="grid" Width="100%">
			<Columns>
				<asp:TemplateField HeaderText="Origin area">
					<ItemTemplate>
						<asp:HiddenField
							runat="server"
							id="OriginAreaIdHiddenField"
							Value='<%# Eval("Id") %>'/>
						<uc:LiteralEncoded
							runat="server"
							id="OriginAreaTitleLiteral"
							Text='<%# Eval("Title") %>'	/>&nbsp;
							(<asp:Label
							runat="server"
							id="OriginAreaBlocksCountLabel"
							Text='<%# Eval("Blocks.Count") %>'
						/>&nbsp;blocks)
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="Destination area">
					<ItemTemplate>
						<asp:DropDownList runat="server" id="DestinationAreasList" Width="97%" />
						<asp:RequiredFieldValidator runat="server" ID="DestinationAreasValidator" Text="*" ErrorMessage='<%# string.Format(Resources.SiteStructureMessage.DestinationAreasIsRequired,Eval("Title") )%>' ControlToValidate="DestinationAreasList" InitialValue="<%# NotSelectedItemValue %>" ValidationGroup="DefaultValidationGroup" Display="Static" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
	</div>
	<br />	
	<div runat="server" id="SubPagesDiv" class="content-box" style="width:626px;" visible="false">
		<div class="div-label"><%= Resources.SiteStructure.Literal_ChildPages %></div>
		<sc:GridViewWithCustomPager runat="server" ID="SubPagesGrid" AutoGenerateColumns="false" SkinID="grid" Width="100%" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="30%">
					<ItemTemplate>
						<asp:Image ID="TypeImage" runat="server" ImageUrl='<%# GetImageUrl(Eval("ContentNodeStatusId")) %>' />
						<Scensum:HyperLinkEncoded ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>'></Scensum:HyperLinkEncoded>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:TemplateField HeaderText="<%$ Resources:SiteStructure, Literal_Path %>" ItemStyle-Width="70%">
					<ItemTemplate>
						<Scensum:LiteralEncoded ID="PathLiteral" runat="server" Text='<%# GetPath(Eval("Id")) %>' ></Scensum:LiteralEncoded>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		
</sc:GridViewWithCustomPager>
	</div>      
   
	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="DefaultValidationGroup" />
		<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
	</div>					
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
