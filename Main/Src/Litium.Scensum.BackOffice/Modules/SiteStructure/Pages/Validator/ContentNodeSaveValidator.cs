﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Validator
{
    public static class ContentNodeSaveValidator
    {
        public static bool IsCommonNameUnique(IContentNode contentNode)
        {
            var contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
        
        	if (!string.IsNullOrEmpty(contentNode.CommonName))
            {
                IContentNode node = contentNodeSecureService.GetByCommonName(contentNode.CommonName, contentNode.SiteStructureRegistryId);
                if (node != null && node.Id != contentNode.Id)
                {
                    return false;
                }
            }
            else
            {
                if (contentNode.CommonName != null) contentNode.CommonName = null;
            }
            return true;
        }
    }
}
