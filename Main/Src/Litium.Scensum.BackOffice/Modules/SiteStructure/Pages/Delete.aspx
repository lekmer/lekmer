<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Delete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="errors" MessageType="Failure"   runat="server" />
</asp:Content>
<asp:Content ID="SiteStructureDeleteContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
<script type="text/javascript">
	function ConfirmParentPageDelete(message) {
		if (confirm(message)) {
			$('.delete-page-button').click();
		}
	}
</script>
<asp:Button ID="DeletePageButton" runat="server" style="display:none;" CssClass="delete-page-button" />
</asp:Content>
