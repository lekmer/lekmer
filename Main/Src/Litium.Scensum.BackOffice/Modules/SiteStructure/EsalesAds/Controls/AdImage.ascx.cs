﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds.Controls
{
	public partial class AdImage : UserControlController
	{
		public event EventHandler<ImageSelectEventArgs> Selected;
		public event EventHandler FileUploaded;
		public event EventHandler DeleteImage;

		public int? ImageId
		{
			get { return ImageIdHidden.Value.HasValue() ? int.Parse(ImageIdHidden.Value) : (int?)null; }
			set { ImageIdHidden.Value = value.HasValue ? value.ToString() : string.Empty; }
		}

		protected override void SetEventHandlers()
		{
			ImageSelectControl.Selected += ImageSelected;
			ImageSelectControl.FileUploaded += ImageUploaded;
			ImageSelectControl.Canceled += ImageSelectCanceled;

			DeleteImageButton.Click += DeleteImageButtonClick;
		}

		protected override void PopulateControl()
		{
			PopulateImage(ImageId);
		}

		protected virtual void PopulateImage(int? imageId)
		{
			ImageTitleLiteral.Text = ImageAlternativeTextLiteral.Text = ImageFileExtensionLabel.Text = ImageDimensionsLabel.Text = string.Empty;
			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = false;

			if (imageId.HasValue)
			{
				var imageService = IoC.Resolve<IImageSecureService>();
				IImage image = imageService.GetById((int)imageId);
				PopulateImage(image);
			}
		}

		protected virtual void PopulateImage(IImage image)
		{
			ImageTitleLiteral.Text = ImageAlternativeTextLiteral.Text = ImageFileExtensionLabel.Text = ImageDimensionsLabel.Text = string.Empty;
			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible =  false;

			ImageTitleLiteral.Text = image.Title;
			ImageAlternativeTextLiteral.Text = image.AlternativeText;
			ImageFileExtensionLabel.Text = image.FormatExtension;
			ImageDimensionsLabel.Text = string.Format("{0}px x {1}px", image.Width, image.Height);

			string imageUrl = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(image.Id, image.FormatExtension));

			ZoomProductImageAnchor.HRef = imageUrl;

			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = DefaultImage.Visible = true;
			ZoomProductImageAnchor.Title = image.Title;

			DefaultImage.ImageUrl = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
		}

		protected virtual void ImageSelectCanceled(object sender, EventArgs e)
		{
			ImagesPopup.Hide();
		}

		protected virtual void ImageUploaded(object sender, EventArgs e)
		{
			ImagesPopup.Show();

			if(FileUploaded != null)
			{
				FileUploaded(this, EventArgs.Empty);
			}
		}

		protected virtual void ImageSelected(object sender, ImageSelectEventArgs e)
		{
			ImageId = e.Image.Id;
			ImagesPopup.Hide();
			PopulateImage(e.Image);

			if(Selected != null)
			{
				Selected(this, e);
			}
		}

		protected virtual void DeleteImageButtonClick(object sender, EventArgs e)
		{
			ImageId = null;
			PopulateImage((int?)null);
			if(DeleteImage != null)
			{
				DeleteImage(this, EventArgs.Empty);
			}
		}
	}
}
