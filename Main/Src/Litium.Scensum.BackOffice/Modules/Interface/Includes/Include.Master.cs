using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Includes
{
	public partial class Include : MasterPage
	{
		//TODO: Remove when add cache logic
		private Collection<IIncludeFolder> _includeFolderTreeViewDataSourse;

		public static readonly string IncludeSelectedNodeId = "IncludeSelectedNodeId";

		#region Property
		private const string BreadcrumbsSeparator = " > ";

		private Collection<string> _breadcrumbAppend;
		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				if(_breadcrumbAppend == null)
				{
					_breadcrumbAppend = new Collection<string>();
				}
				return _breadcrumbAppend;
			}
		}
		
		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session[IncludeSelectedNodeId];

			}
			set
			{
				Session[IncludeSelectedNodeId] = value;
			}
		}
		public virtual bool DenySelection
		{
			get
			{
				return IncludeTree.DenySelection;
			}
			set
			{
				IncludeTree.DenySelection = value;
			}
		}
		public virtual int RootNodeId
		{
			get
			{
				return IncludeTree.RootNodeId;

			}
		}
		public virtual bool ShowFolderContent { get; set; }
		public virtual bool IsMainNode { get; set; }
		#endregion

		#region Page Events

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			CreateFolderButton.Click += LbtnCreateFolder_ServerClick;
			SearchButton.Click += BtnSearch_Click;
			IncludeTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnPreRender(EventArgs e)
		{
			if (!IsPostBack)
			{
				PopulateTreeView(SelectedFolderId);
				RestoreSearchFields();
			}

			RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), "root menu",
			                      string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');",
			                                    IncludeTree.ClientID), true);
			if (IsMainNode)
			{
				BuildBreadcrumbs();
				return;
			}
			if (DenySelection) return;

			if (SelectedFolderId == null || IncludeTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}
			BuildBreadcrumbs();
		}

		public virtual void BuildBreadcrumbs()
		{
			Collection<IIncludeFolder> folders = IoC.Resolve<IIncludeFolderSecureService>().GetAll();
			IIncludeFolder folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
			string breadcrumbs = string.Empty;

			while (folder != null)
			{
				breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", BreadcrumbsSeparator, folder.Title, breadcrumbs);
				int? parentNodeId = folder.ParentIncludeFolderId;
				folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, BreadcrumbsSeparator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", Resources.Interface.Literal_Includes, breadcrumbs);

			Breadcrumbs.Text = breadcrumbs;
		}

		protected virtual void BtnSearch_Click(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			SearchCriteriaState<string>.Instance.Save(PathHelper.Template.Include.GetSearchResultUrl(), SearchTextBox.Text);
			Response.Redirect(PathHelper.Template.Include.GetSearchResultUrl());
		}

		protected virtual void LbtnCreateFolder_ServerClick(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			Response.Redirect(PathHelper.Template.Include.Folder.GetCreateUrl());
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					DenySelection = true;
					break;
				case "Navigate":
					SelectedFolderId = e.Id;
					Response.Redirect(PathHelper.Template.Include.GetDefaultUrlWithShowFolderContent());
					break;
			}
		}

		protected virtual void LbtCreateFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = IncludeTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Template.Include.Folder.GetCreateUrl());
		}
		protected virtual void LbtnEdit_Click(object sender, EventArgs e)
		{
			SelectedFolderId = IncludeTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Template.Include.Folder.GetEditUrl());
		}

		protected virtual void LbtnCreateInclude_Click(object sender, EventArgs e)
		{
			SelectedFolderId = IncludeTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Template.Include.GetCreateUrl());
		}

		protected virtual void LbtnDelete_Click(object sender, EventArgs e)
		{
			SelectedFolderId = GetAllIncludeFolders().FirstOrDefault(inc => inc.Id == IncludeTree.MenuLastClickedNodeId.Value).ParentIncludeFolderId;
			IIncludeFolderSecureService includeFolderService = IoC.Resolve<IIncludeFolderSecureService>();
			includeFolderService.Delete(SignInHelper.SignedInSystemUser, (int)IncludeTree.MenuLastClickedNodeId);
			PopulateTreeView(SelectedFolderId);
			if (SelectedFolderId == null)
			{
				Response.Redirect(PathHelper.Template.Include.GetDefaultUrl());
			}
			else
			{
				Response.Redirect(PathHelper.Template.Include.GetDefaultUrlWithShowFolderContent());
			}
		}

		#endregion

		#region Public Methods

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IIncludeFolder> GetAllIncludeFolders()
		{
			return _includeFolderTreeViewDataSourse ??
				   (_includeFolderTreeViewDataSourse = (IoC.Resolve<IIncludeFolderSecureService>()).GetAll());
		}

		public static void RegisterStartupScript(Control control, Type type, string key, string script, bool addScriptTags)
		{
			ScriptManager.RegisterStartupScript(control, type, key, script, addScriptTags);
		}

		public virtual void UpdateMasterTree()
		{
			PopulateTreeView(SelectedFolderId);
			LeftUpdatePanel.Update();
		}

		public virtual void UpdateSelectedFolder(int selectedFolderId)
		{
			IncludeTree.SelectedNodeId = SelectedFolderId = selectedFolderId;
			PopulateTreeView(null);
			LeftUpdatePanel.Update();
		}

		#endregion

		protected void RestoreSearchFields()
		{
			string searchTitle = SearchCriteriaState<string>.Instance.Get(PathHelper.Template.Include.GetSearchResultUrl());
			if (!string.IsNullOrEmpty(searchTitle))
			{
				SearchTextBox.Text = searchTitle;
			}
		}

		protected void PopulateTreeView(int? folderId)
		{
			IncludeTree.DataSource = GetTreeDataSource(folderId);
			IncludeTree.RootNodeTitle = Resources.Interface.Literal_Includes;
			IncludeTree.DataBind();
			IncludeTree.SelectedNodeId = folderId ?? SelectedFolderId ?? IncludeTree.RootNodeId;
		}

		public virtual Collection<INode> GetTreeDataSource(int? folderId)
		{
			var id = folderId ?? SelectedFolderId;
			id = id != IncludeTree.RootNodeId ? id : null;
			return IoC.Resolve<IIncludeFolderSecureService>().GetTree(id);
		}
	}
}
