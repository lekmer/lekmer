using System;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Includes
{
	public partial class IncludeEdit : LekmerPageController, IEditor
	{
		private bool IsCopy
		{
			get { return Request.QueryString["CopyOfId"] != null; }
		}

		protected override int GetId()
		{
			return Request.QueryString.GetInt32OrNull("CopyOfId") ?? base.GetId();
		}

		private IInclude _include;
		protected IInclude Include
		{
			get
			{
				if (_include == null)
				{
					var service = IoC.Resolve<IIncludeSecureService>();
					_include = IsCopy ? service.CreateCopyOfId(GetId(), "Copy of -") : service.GetById(GetId());
				}

				return _include;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			var master = Master as Include;
			if (master == null) return;

			master.BreadcrumbAppend.Clear();
			master.BreadcrumbAppend.Add(Include.CommonName);
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			PutIncludeTreeControl.NodeCommand += OnNodeCommand;
			DeleteButton.Click += DeleteButton_Click;
		}

		protected void DeleteButton_Click(object sender, EventArgs e)
		{
			IoC.Resolve<IIncludeSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
			Response.Redirect(PathHelper.Template.Include.GetDefaultUrl());
		}

		protected override void PopulateForm()
		{
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
			{
				messager.MessageType = InfoType.Success;
				messager.Add(Resources.GeneralMessage.SaveSuccessInclude);
			}

			PopulateIncludeFields(Include);
			PopulateTreeView(Include.IncludeFolderId);
			UpdateMasterSelectedFolder(Include.IncludeFolderId);

			DeleteButton.Visible = GetIdOrNull().HasValue;
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (PutIncludeTreeControl.SelectedNodeId == null) return;

			if (Include == null)
				throw new BusinessObjectNotExistsException(GetId());

			Include.CommonName = SystemNameTextBox.Text.Trim();
			Include.Description = string.IsNullOrEmpty(DescriptionTextBox.Text) ? null : DescriptionTextBox.Text;
			Include.Content = ValueTextBox.Text;
			Include.IncludeFolderId = (int)PutIncludeTreeControl.SelectedNodeId;

			var includeSecureService = IoC.Resolve<IIncludeSecureService>();

			ValidationResult validationResult = includeSecureService.Validate(Include);
			if (!validationResult.IsValid)
			{
				messager.AddRange(validationResult.Errors);
				return;
			}
			int includeId = includeSecureService.Save(SignInHelper.SignedInSystemUser, Include);

			if (IsCopy)
				Response.Redirect(string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", PathHelper.Template.Include.GetEditUrl(includeId)));

			messager.MessageType = InfoType.Success;
			messager.Add(Resources.GeneralMessage.SaveSuccessInclude);

			UpdateMasterSelectedFolder(Include.IncludeFolderId);
		}

		private void UpdateMasterSelectedFolder(int folderId)
		{
			var master = Master as Include;
			if (master != null && master.SelectedFolderId != folderId)
			{
				master.UpdateSelectedFolder(folderId);
			}
		}

		protected void PopulateIncludeFields(IInclude include)
		{
			DescriptionTextBox.Text = include.Description;
			SystemNameTextBox.Text = include.CommonName;
			ValueTextBox.Text = include.Content;
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Template.Include.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Template.Include.GetDefaultUrlWithShowFolderContent());
			}
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var master = (Include)Page.Master;
			var folders = master.GetTreeDataSource(folderId ?? master.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;

			PutIncludeTreeControl.DataSource = folders;
			PutIncludeTreeControl.DataBind();
			PutIncludeTreeControl.SelectedNodeId = folderId;
		}
	}
}
