<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Aliases/Alias.Master" CodeBehind="FolderCreate.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Interface.Aliases.FolderCreate" %>
<%@ MasterType TypeName="Litium.Scensum.BackOffice.Modules.Interface.Aliases.Alias" %>
<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree"
	Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer ID="errors" MessageType="Failure" HideMessagesControlId="SaveButton"
								runat="server" />
							<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
								ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgFolder" />
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="FolderCreateContent" ContentPlaceHolderID="AliasContent" runat="server">
	<asp:Panel ID="FolderCreatePanel" runat="server" DefaultButton="SaveButton">
		<div class="alias-create-edit">
			<div class="column">
				<div class="input-box">
					<span><%= Resources.General.Literal_Title %>&nbsp; *</span>
					<asp:RequiredFieldValidator runat="server" ID="TitleValidator" ControlToValidate="TitleTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgFolder" />
					<br />
					<asp:TextBox ID="TitleTextBox" runat="server"  MaxLength="50"/>
				</div>
				<asp:UpdatePanel ID="FolderUpdatePanel" runat="server">
					<ContentTemplate>
						<div class="input-box">
							<div class="buttons">
								<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" ValidationGroup="vgFolder"
									UseSubmitBehavior="true" SkinID="DefaultButton" />
								<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
			<asp:UpdatePanel ID="TreeViewFoldersUpdatePanel" runat="server">
				<ContentTemplate>
					<div id="PutAliasTreeDiv" runat="server" class="column">
						<div class="input-box">
							<span><%= Resources.Interface.Literal_PutInto %></span>
							<CustomControls:TemplatedTreeView runat="server" ID="ParentAliasTree" DisplayTextControl="lbName"
								UseRootNode="true" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
								MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
								NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
								NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
								NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
								MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
								<HeaderTemplate>
								</HeaderTemplate>
								<NodeTemplate>
									<div class="tree-item-cell-expand">
										<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
										<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
									</div>
									<div class="tree-item-cell-main">
										<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img"/> 
										<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
									</div>
								</NodeTemplate>
							</CustomControls:TemplatedTreeView>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>
	</asp:Panel>
</asp:Content>
