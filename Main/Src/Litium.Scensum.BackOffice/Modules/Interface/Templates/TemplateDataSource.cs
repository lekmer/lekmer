using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
    public class TemplateDataSource
    {
        private int rowCount;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "id")]
		public int SelectCount(int maximumRows, int startRowIndex, int id)
        {
            return rowCount;
        }

        public Collection<ITemplate> GetMethod(int maximumRows, int startRowIndex, int id)
        {
			ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
			return templateSecureService.GetAllByModel(id, maximumRows == 0 ? 0 : startRowIndex / maximumRows + 1, maximumRows, out rowCount);
        }
    }
}
