using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls.SiteStructure;
using Litium.Scensum.BackOffice.UserControls.Template;
using Litium.Scensum.BackOffice.UserControls.Template.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Common;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;
using ITemplate = Litium.Scensum.Template.ITemplate;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
	public partial class TemplateEdit : LekmerPageController, IEditor 
	{
		private const string SiteStructureModelName = "ContentPage";
		private ContentAreas ctrlContentAreas;
		private const string ctrlContentAreasPath = "~/UserControls/SiteStructure/ContentAreas.ascx";

		private TemplateState _state;
		protected virtual TemplateState State
		{
			get
			{
				if (_state == null)
				{
					_state = (Session["State" + GetGuid()] as TemplateState) ?? new TemplateState();
				}
				return _state;
			}
			set
			{
				Session["State" + GetGuid()] = _state = value;
			}
		}

		protected virtual string GetGuid()
		{
			return Request.QueryString.Get("guid");
		}

		protected virtual int? GetModelIdOrNull()
		{
			return Request.QueryString.GetInt32OrNull("model-id");
		}

		protected virtual int GetModelId()
		{
			return Request.QueryString.GetInt32("model-id");
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			UploadButton.Click += BtnUpload_Click;
			RemoveButton.Click += BtnRemove_Click;
			DeleteButton.Click += BtnDelete_Click;
			ShowAlternateCheckBox.CheckedChanged += CBShowAlternate_CheckedChanged;
			FragmentRegionGrid.ItemDataBound += RptFragmentRegion_ItemDataBound;
			AliasTreeViewControl.NodeCommand += OnAliasNodeCommand;
			IncludeTreeViewControl.NodeCommand += OnIncludeNodeCommand;
			FunctionTreeViewControl.NodeCommand += OnFunctionNodeCommand;
			ComponentRepeater.ItemDataBound += OnComponentRepeaterDataBound;
		}

		protected void SetButtonDeleteVisibility(ITemplate template)
		{
			DeleteButton.Visible = State.Id > 0;

			IModelSecureService modelSecureService = IoC.Resolve<IModelSecureService>();
			IModel model = modelSecureService.GetById(template.ModelId);
			bool isDisabled = State.Id < 1 || template.Id == model.DefaultTemplateId;
			DeleteButton.Enabled = !isDisabled;
			DeleteButton.ToolTip = isDisabled ? Resources.InterfaceMessage.NotDeleteDefaultTemplate : string.Empty;

			IContentPageSecureService contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			if (contentPageSecureService.GetAllByTemplate(State.Id).Count > 0)
			{
				DeleteButton.Enabled = false;
				DeleteButton.ToolTip = Resources.InterfaceMessage.NotDeleteTemplateWithContentPages;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

            // Content Areas
			IModelSecureService modelSecureService = IoC.Resolve<IModelSecureService>();
			IModel model = modelSecureService.GetById(GetModelIdOrNull() != null ? GetModelId() : GetTemplate().ModelId);
			if (model == null) return;

			if (model.CommonName == SiteStructureModelName)
			{
				ctrlContentAreas = LoadControl(ctrlContentAreasPath) as ContentAreas;
				if (ctrlContentAreas != null)
				{
					ContentAreasPlaceHolder.Controls.Add(ctrlContentAreas);
					ctrlContentAreas.TemplateId = State.Id;
					ctrlContentAreas.CopyOfId = State.CopyOfId;
				}
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			IdDiv.Visible = State.Id > 0;
			IdTextBox.Text = State.Id.ToString(CultureInfo.CurrentCulture);
			TemplateIdPanel.Update();       
			
			if (State.Id <= 0 || !DeleteButton.Enabled) return;
			IContentAreaSecureService contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
			DeleteButton.Attributes.Add("onclick", contentAreaSecureService.GetAllByTemplate(State.Id).Count > 0
													? @"javascript: return DeleteTemplateWithContentAreasConfirmation();"
													: @"javascript: return DeleteConfirmation('template');");
		}

		protected override void PopulateForm()
		{
			// If page save redierected on save, display success message
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
				success.Add(Resources.GeneralMessage.SaveSuccessTemplate);

			State = new TemplateState
			            {
                            Id = GetIdOrNull() ?? -1,
                            CopyOfId = Request.QueryString.GetInt32OrNull("CopyOfId")
			            };

            var template = GetTemplate();

			TitleTextBox.Text = template.Title;
			ShowAlternateCheckBox.Checked = template.UseAlternate;
			SetButtonDeleteVisibility(template);

			if (string.IsNullOrEmpty(template.ImageFileName) || State.Id < 1)
			{
				PopulateSelectedImage(string.Empty);
			}
			else
			{
				PopulateSelectedImage(template.ImageFileName);
			}

			IModelFragmentRegionSecureService modelFragmentRegionSecureService = IoC.Resolve<IModelFragmentRegionSecureService>();
			Collection<IModelFragmentRegion> fragmentRegions = State.IdOrCopyOfId > 0
                                                                ? modelFragmentRegionSecureService.GetAllByTemplate(State.IdOrCopyOfId.Value)
																: modelFragmentRegionSecureService.GetAllByModel(GetModelId());

			if (fragmentRegions != null && fragmentRegions.Count > 0)
			{
				FragmentRegionGrid.DataSource = fragmentRegions;
				FragmentRegionGrid.DataBind();
			}

			ITemplateSettingSecureService templateSettingSecureService = IoC.Resolve<ITemplateSettingSecureService>();
			Collection<ITemplateSetting> templateSettings = State.IdOrCopyOfId > 0
                                                                ? templateSettingSecureService.GetAllByTemplate(State.IdOrCopyOfId.Value)
																: templateSettingSecureService.GetAllByModel(GetModelId());

			if (templateSettings != null && templateSettings.Count > 0)
			{
				SettingRegionControl.TemplateSettings.Clear();
				SettingRegionControl.TemplateSettings.AddRange(templateSettings);
			}
			SettingRegionControl.ShowAlternate = ShowAlternateCheckBox.Checked;

			PopulateAliasesAndIncludesAndComponents();
            SetTemplateBreadCrumb(template);

			if (template.ModelId > 0)
			{
				var model = IoC.Resolve<IModelSecureService>().GetById(template.ModelId);
				if (model != null)
				{
					var master = ((Template) Master);
					master.ModelId = model.Id;
					master.ModelFolderId = model.ModelFolderId;
					master.PopulateTreeView();
					master.DeniedSelection(false);
				}
			}
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		protected ITemplate GetTemplate()
		{
			ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
			ITemplate template;

            if (State.CopyOfId != null)
            {
                if (State.CopyOfId <= 0)
                    throw new BusinessObjectNotExistsException(State.CopyOfId.Value);

                template = templateSecureService.CreateCopyOfId(State.CopyOfId.Value, "Copy of - ");
                if (template == null)
                    throw new BusinessObjectNotExistsException(State.CopyOfId.Value);
            }
			else if (State.Id > 0)
			{
				template = templateSecureService.GetById(State.Id);
				if (template == null)
				    throw new BusinessObjectNotExistsException(State.Id);
				
				template.Status = BusinessObjectStatus.New;
			}
			else
			{
				template = templateSecureService.Create();
				template.ModelId = GetModelId();
			}

			return template;
		}

	    public virtual void OnSave(object sender, EventArgs e)
		{
			ITemplate template = GetTemplate();
			template.UseAlternate = ShowAlternateCheckBox.Checked;
			template.ImageFileName = ImgHiddenField.Value;
			template.Title = TitleTextBox.Text;
			//Update title after redundant spaces removing.
			TitleTextBox.Text = template.Title;

			Collection<ITemplateFragment> fragmentList = new Collection<ITemplateFragment>();
			foreach (RepeaterItem repeaterItem in FragmentRegionGrid.Items)
			{
				FragmentRegion fragmentRegion = repeaterItem.FindControl("FragmentRegionControl") as FragmentRegion;
				if (fragmentRegion == null) continue;

				Collection<ITemplateFragment> templateFragmentList = fragmentRegion.GetTemplateFragments();
				foreach (ITemplateFragment templateFragment in templateFragmentList)
				{
					fragmentList.Add(templateFragment);
				}
			}
            
			ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
			template.Id = templateSecureService.Save(SignInHelper.SignedInSystemUser, template, fragmentList, SettingRegionControl.GetSettings());
			if (template.Id == -1)
			{
				errors.Add(Resources.InterfaceMessage.TemplateTitleExists);
				return;
			}

	        State.Id = template.Id;

			if (State.Id < 1)
			{
				IModelSecureService modelSecureService = IoC.Resolve<IModelSecureService>();
				IModel model = modelSecureService.GetById(template.ModelId);
				if (model.DefaultTemplateId == -1)
				{
					templateSecureService.SetDefault(SignInHelper.SignedInSystemUser, template);
				}
			}

			// Content areas
			if (ctrlContentAreas != null)
			{
				ctrlContentAreas.TemplateId = State.Id;
				ctrlContentAreas.DeleteContentAreas();
				Collection<string> errorsList = ctrlContentAreas.SaveContentAreas();
				if (errorsList.Count > 0)
				{
					foreach (string s in errorsList)
					{
						errors.Add(s);
					}

					return;
				}
			}
			//If copy was saved redirect to edit page to allow 
			if (State.CopyOfId.HasValue)
			{
				var editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", PathHelper.Template.GetEditUrl(template.Id));
				Response.Redirect(editUrl);
			}
			if (!success.HasMessages)
			{
				success.Add(Resources.GeneralMessage.SaveSuccessTemplate);
			}
            
			SetButtonDeleteVisibility(template);
	        SetTemplateBreadCrumb(template);
		}

        private void SetTemplateBreadCrumb(ITemplate template)
        {
            ((Template) Master).PageCaption = State.Id > 0 ? template.Title : Resources.Interface.Literal_CreateTemplate;
        }

	    protected virtual void BtnUpload_Click(object sender, EventArgs e)
		{
			if (!FileUploadControl.PostedFile.ContentType.Contains("image"))
			{
				errors.Add(Resources.InterfaceMessage.NotValidImageFormat);
				return;
			}
			string imageFileName = Path.GetFileName(FileUploadControl.PostedFile.FileName);

			IoC.Resolve<ITemplateSecureService>().UploadImage(FileUploadControl.PostedFile.InputStream, imageFileName);

			PopulateSelectedImage(imageFileName);
		}

		protected virtual void BtnRemove_Click(object sender, EventArgs e)
		{
			PopulateSelectedImage(string.Empty);
		}

		protected virtual void BtnDelete_Click(object sender, EventArgs e)
		{
			IContentPageSecureService contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			if (contentPageSecureService.GetAllByTemplate(State.Id).Count > 0)
			{
				errors.Add(Resources.InterfaceMessage.NotDeleteTemplateWithContentPages);
				return;
			}

			if (CheckUsageByBlocks(State.Id)) return;

			if (ctrlContentAreas != null)
			{
				ctrlContentAreas.DeleteAllContentAreas();
			}

			if (State.Id > 0)
			{
				ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
				templateSecureService.Delete(SignInHelper.SignedInSystemUser, State.Id);
			}

			RedirectBack();
		}

		protected bool CheckUsageByBlocks(int id)
		{
			var relatedBlocks = IoC.Resolve<IBlockSecureService>().GetAllByTemplate(id);
			if (relatedBlocks.Count > 0)
			{
				var titles = new StringBuilder(string.Empty);
				foreach (var block in relatedBlocks)
				{
					var settings = new SiteStructureBlockSetting(block.BlockType.CommonName);
					var link = PathHelper.SiteStructure.Page.GetBlockEditUrl(settings.EditPageName, block.ContentNodeId, block.Id);
					titles.Append("<a style=\"color:#004CD5; text-decoration:underline;\" href=\"" + ResolveUrl(link) + "\">" + block.Title + "</a>");
					titles.Append(", ");
				}
				errors.Add(string.Format(CultureInfo.CurrentCulture,
					Resources.InterfaceMessage.NotDeleteRelatedBlocks,
					titles.ToString().Substring(0, titles.Length - 2)), false);
				return true;
			}
			return false;
		}

		protected virtual void CBShowAlternate_CheckedChanged(object sender, EventArgs e)
		{
			TemplateControlHelper.OnShowAlternate(this, ShowAlternateCheckBox.Checked);
		}

		protected virtual void OnAliasNodeCommand(object sender, TreeViewEventArgs e)
		{
			switch(e.EventName)
			{
				case "Expand":
					AliasTreeViewControl.DenySelection = true;
					break;
				case "Navigate":
					PopulateAliases(e.Id);
					break;
			}
			PopulateAliasTree(e.Id);
			AliasPopup.Show();
		}

		protected virtual void OnIncludeNodeCommand(object sender, TreeViewEventArgs e)
		{
			switch (e.EventName)
			{
				case "Expand":
					IncludeTreeViewControl.DenySelection = true;
					break;
				case "Navigate":
					PopulateIncludes(e.Id);
					break;
			}
			PopulateIncludeTree(e.Id);
			IncludePopup.Show();
		}

		protected virtual void OnFunctionNodeCommand(object sender, TreeViewEventArgs e)
		{
			var nodeId = e.Id;
			if (nodeId == 0)
			{
				PopulateModelFragmentFunctions(State.ActiveModelFragmentId.Value);
			}
			else
			{
				PopulateEntityFunctions(nodeId);
			}
			FunctionPopup.Show();
		}

		protected void OnComponentRepeaterDataBound(object sender, RepeaterItemEventArgs e)
		{
			var item = e.Item;
			var component = item.DataItem as IComponent;
			if (component == null) return;

			var titleLink = (HyperLinkEncoded)item.FindControl("TitleLink");
			titleLink.Text = component.CommonName;
			var scriptStart = "javascript:closeComponentPopup('[Component(\"" + component.CommonName + "\"";
			var scriptEnd = ")]');";
			titleLink.NavigateUrl = scriptStart + scriptEnd;

			var parameters = IoC.Resolve<IComponentParameterSecureService>().GetAllByComponent(component.Id);
            if (parameters.Count == 0) return;

			var parameterRepeater = (Repeater)item.FindControl("ComponentParameterRepeater");
			parameterRepeater.DataSource = parameters;
			parameterRepeater.DataBind();

			var titleBuilder = new StringBuilder(titleLink.Text);
			var scriptBuilder = new StringBuilder(scriptStart);
			titleBuilder.Append(" (");
			foreach (var parameter in parameters)
			{
				titleBuilder.Append("[" + parameter.Title + "], ");
				scriptBuilder.Append(", \"" + parameter.Title + "=?\"");
			}
			titleLink.Text = titleBuilder.Remove(titleBuilder.Length - 2, 2).Append(")").ToString();
			titleLink.NavigateUrl = scriptBuilder.Append(scriptEnd).ToString();
		}

		protected virtual void OnFragmentRegionFunctionAdded(object sender, FunctionAddEventArgs e)
		{
			int modelFragmentId = e.ModelFragmentId;
			if (!State.ActiveModelFragmentId.HasValue || State.ActiveModelFragmentId.Value != modelFragmentId)
			{
				PopulateFunctionsTree(modelFragmentId);
			}
			FunctionPopup.Show();
			State.ActiveModelFragmentId = modelFragmentId;
		}

		protected virtual void RptFragmentRegion_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			RepeaterItem repeaterItem = e.Item;
			if (repeaterItem.ItemType != ListItemType.Item && repeaterItem.ItemType != ListItemType.AlternatingItem)
			{
				return;
			}

			var fragmentRegion = repeaterItem.FindControl("FragmentRegionControl") as FragmentRegion;
			if (fragmentRegion == null)
			{
				return;
			}

			var modelFragmentRegion = (IModelFragmentRegion)repeaterItem.DataItem;

			var templateFragmentSecureService = IoC.Resolve<ITemplateFragmentSecureService>();
			Collection<ITemplateFragment> templateFragments;
			if (State.IdOrCopyOfId > 0)
			{
				templateFragments = templateFragmentSecureService.GetAllByTemplate(State.IdOrCopyOfId.Value);
			}
			else
			{
				templateFragments = templateFragmentSecureService.GetAllByModel(GetModelId());
			}
			fragmentRegion.Fragments = templateFragments.Where(
				templateFragment =>
				templateFragment.ModelFragmentRegionId == modelFragmentRegion.Id);
			fragmentRegion.ShowAlternate = ShowAlternateCheckBox.Checked;
			fragmentRegion.Header = modelFragmentRegion.Title;
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				if (referrer.Contains("IsSearchResult=true"))
				{
					var master = ((Template)Master);
					master.ModelId = -1;
					master.ModelFolderId = -1;
					master.PopulateTreeView();
					master.DeniedSelection(true);
				}
				Response.Redirect(PathHelper.Template.GetEditReferrerUrl(referrer));
			}
			else
			{
                Response.Redirect(PathHelper.Template.GetDefaultUrl());
			}
		}

		protected void PopulateSelectedImage(string imageFileName)
		{
			ImgHiddenField.Value = imageFileName;
			TemplateImage.ImageUrl = ResolveUrl(PathHelper.Template.GetTemplateImageLoaderUrl(imageFileName));
		}

		protected virtual void PopulateAliasesAndIncludesAndComponents()
		{
			PopulateAliasPopup();
			PopulateIncludePopup();
			PopulateComponentPopup();
		}

		protected virtual void PopulateAliasPopup()
		{
			var aliasFolders = IoC.Resolve<IAliasFolderSecureService>().GetTree(null);
			if (aliasFolders.Count == 0) return;

			var firstAliasFolder = aliasFolders[0];
			PopulateAliasTree(firstAliasFolder.Id);
			PopulateAliases(firstAliasFolder.Id);
		}

		protected virtual void PopulateIncludePopup()
		{
			var includeFolders = IoC.Resolve<IIncludeFolderSecureService>().GetTree(null);
			if (includeFolders.Count == 0) return;

			var firstIncludeFolder = includeFolders[0];
			PopulateIncludeTree(firstIncludeFolder.Id);
			PopulateIncludes(firstIncludeFolder.Id);
		}

		protected virtual void PopulateComponentPopup()
		{
			ComponentRepeater.DataSource = IoC.Resolve<IComponentSecureService>().GetAll();
			ComponentRepeater.DataBind();
		}

		protected virtual void PopulateAliasTree(int? selectedId)
		{
			var service = IoC.Resolve<IAliasFolderSecureService>();
			AliasTreeViewControl.DataSource = service.GetTree(selectedId);
			AliasTreeViewControl.DataBind();
			AliasTreeViewControl.SelectedNodeId = selectedId;
		}

		protected virtual void PopulateIncludeTree(int? selectedId)
		{
			var includeFolderService = IoC.Resolve<IIncludeFolderSecureService>();
			IncludeTreeViewControl.DataSource = includeFolderService.GetTree(selectedId);
			IncludeTreeViewControl.DataBind();
			IncludeTreeViewControl.SelectedNodeId = selectedId;
		}
		
		protected virtual void PopulateAliases(int aliasFolderId)
		{
			var aliasService = IoC.Resolve<IAliasSecureService>();
			AliasRepeater.DataSource = aliasService.GetAllByAliasFolder(aliasFolderId);
			AliasRepeater.DataBind();
		}

		protected virtual void PopulateIncludes(int includeFolderId)
		{
			var includeService = IoC.Resolve<IIncludeSecureService>();
			IncludeRepeater.DataSource = includeService.GetAllByIncludeFolder(includeFolderId);
			IncludeRepeater.DataBind();
		}

		protected virtual void PopulateFunctionsTree(int modelFragmentId)
		{
			var entityNodes = IoC.Resolve<IEntitySecureService>().GetAllByModelFragment(modelFragmentId).Select(
				e => (INode)new Node { Id = e.Id, HasChildren = false, ParentId = null, Title = e.Title });
			var modelFragmentNodes = new Collection<INode> { new Node { Id = 0, HasChildren = false, ParentId = null, Title = "Other" } };
			var allNodes = entityNodes.Union(modelFragmentNodes);
			if (allNodes.Count() == 0)
			{
				FunctionTreeViewControl.Style.Add("display", "none");
				FunctionsLabel.Style.Remove("display");
				return;
			}

			FunctionTreeViewControl.DataSource = allNodes;
			FunctionTreeViewControl.DataBind();
			FunctionTreeViewControl.SelectedNodeId = allNodes.First().Id;
			FunctionTreeViewControl.Style.Remove("display");
			FunctionsLabel.Style.Add("display", "none");

			if (entityNodes.Count() > 0)
			{
				PopulateEntityFunctions(entityNodes.First().Id);
			}
			else
			{
				PopulateModelFragmentFunctions(modelFragmentId);
			}
		}

		protected virtual void PopulateEntityFunctions(int entityId)
		{
            var service = IoC.Resolve<IEntityFunctionSecureService>();
			FunctionsRepeater.DataSource = service.GetAllByEntity(entityId);
			FunctionsRepeater.DataBind();
		}

		protected virtual void PopulateModelFragmentFunctions(int modelFragmentId)
		{
			var service = IoC.Resolve<IModelFragmentFunctionSecureService>();
			FunctionsRepeater.DataSource = service.GetAllByModelFragment(modelFragmentId);
			FunctionsRepeater.DataBind();
		}
	}
}
