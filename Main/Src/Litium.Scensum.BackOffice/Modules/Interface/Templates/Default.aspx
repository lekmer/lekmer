<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Templates/Template.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Templates.Default" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel"  runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="errors"  MessageType="Failure" HideMessagesControlId="CreateTemplateButton" runat="server" />
					</ContentTemplate>	
						</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="TemplateTabContent" ContentPlaceHolderID="TemplateContent" runat="server">
	<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="CreateTemplateButton">
	<asp:UpdatePanel ID="RightUpdatePanel" runat="server" UpdateMode="Always">
		<ContentTemplate>		                       
			<div id="template-item">
   				<sc:GridViewWithCustomPager
					ID="TemplateGrid" 
					SkinID="grid" 
					runat="server" 
					Width="100%" 
					AllowPaging="true"
					PageSize="<%$AppSettings:DefaultGridPageSize%>"
					AutoGenerateColumns="false">		
					<Columns>
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="50%">
							<ItemTemplate>
								 <div style="width: 100%; overflow:hidden;">
							        <div style="float:left; max-width:96%;">
								        <asp:HiddenField ID="hfId" runat="server" Value='<%# Eval("Id") %>' />
								        <uc:HyperLinkEncoded ID="hlEdit" runat="server" Text='<%# Eval("Title")%>' />&nbsp
								    </div>
								    <div style="float:left;">
								        <uc:ContextMenu ID="ContextMenuControl" runat="server" 
						                    CallerImageSrc="~/Media/Images/Common/context-menu.png" 
						                    CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png"
						                    MenuContainerCssClass="context-menu-container"
								            MenuShadowCssClass="context-menu-shadow"
								            MenuCallerCssClass="context-menu-caller" 
						                    >  
                                            <div class="context-menu-header">
							                    <%= Resources.General.Literal_Manage %>
						                    </div>
						                    <div class="menu-row-separator">
						                    </div>
						                    <div class="context-menu-row">
							                    <img src="<%=ResolveUrl("~/Media/Images/interface/move-copy.png") %>" />
							                    <asp:LinkButton ID="TemplateMenuCopyButton" runat="server" CommandArgument = '<%# Eval("Id") %>' CommandName="TemplateCopy" Text= "<%$ Resources:Interface, Button_Copy %>" />
						                    </div>                    						
						                    <div class="context-menu-row">
                                                <asp:Image ID="TemplateMenuDeleteImage" ImageUrl="~/Media/Images/Common/delete.gif" runat="server"/>
							                    <asp:LinkButton ID="TemplateMenuDeleteButton" runat="server" CommandArgument='<%# Eval("Id") %>' CommandName="TemplateDelete" Text = "<%$ Resources:General, Button_Delete %>" />
						                    </div>
                                        </uc:ContextMenu>
								    </div>
								 </div>							
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="Path" ItemStyle-Width="45%">
							<ItemTemplate>
								<asp:Label ID="PathLabel" runat="server" Text='<%# GetPath(int.Parse(Eval("ModelId").ToString())) %>' ></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>	
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
							<ItemTemplate>
								<asp:ImageButton ID="DeleteButton" runat="server" CommandName="TemplateDelete" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" />								
							</ItemTemplate>
						</asp:TemplateField>			
					</Columns>
				</sc:GridViewWithCustomPager>
				<asp:ObjectDataSource ID="TemplateObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Interface.Templates.TemplateDataSource" ></asp:ObjectDataSource>
			</div>
			<div class="buttons right">
				<uc:ImageLinkButton ID="CreateTemplateButton" runat="server" Text="<%$ Resources:Interface, Button_CreateTemplate %>" SkinID="DefaultButton" />
			</div>
			
		</ContentTemplate>
	</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>
