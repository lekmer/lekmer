﻿<%@ Page Language="C#" MasterPageFile="~/Master/Start.Master" CodeBehind="NoPrivileges.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Errors.NoPrivileges" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="ErrorStartTabContent" ContentPlaceHolderID="StartBodyContentPlaceHolder" runat="server">
	<div id="content-wrapper">
		<Scensum:ToolBoxPanel ID="ErrorStartHeaderToolBoxPanel" Text="Error" runat="server" />
		<div id="default-wrapper">
			<div style="min-height: 600px;">No privileges. Click <a href="../../Default.aspx" style="COLOR: #004cd5">here</a> to go to start page.</div>
		</div>
	</div>
</asp:Content>