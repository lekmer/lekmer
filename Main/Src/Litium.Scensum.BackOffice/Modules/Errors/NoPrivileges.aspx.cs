﻿using Litium.Lekmer.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Errors
{
	public partial class NoPrivileges : LekmerPageController
	{
		protected override void SetEventHandlers() { }
		protected override void PopulateForm() { }
	}
}
