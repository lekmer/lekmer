﻿using System;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Security.ChangeOwnPassword
{
	public partial class Default : LekmerPageController
	{
		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			UserCredentialControl.UserName = SignInHelper.SignedInSystemUser.UserName;
		}

		public void OnSave(object sender, EventArgs e)
		{
			Page.Validate("vgUserEdit");
			if (!Page.IsValid)
			{
				return;
			}

			var userService = IoC.Resolve<ISystemUserSecureService>();
			ISystemUser user = SignInHelper.SignedInSystemUser;
			string clearTextPassword;
			user.Password = UserCredentialControl.GetPassword(out clearTextPassword);
			userService.Save(user);
			
			user.Password = clearTextPassword;
			userService.SendChangePasswordConfirm(user);

			if (!messager.HasMessages)
			{
				messager.MessageType = InfoType.Success;
				messager.Add(Resources.GeneralMessage.SaveSuccessPassword);
			}
		}

	}
}
