﻿using System;
using System.Globalization;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Security.Roles
{
	public partial class Default : LekmerPageController
	{
		private readonly ISystemRoleSecureService _roleService = 
            IoC.Resolve<ISystemRoleSecureService>();

		protected override void SetEventHandlers()
		{
			SearchGrid.RowCommand += SearchGridRowCommand;
		}

		protected void RestoreSearchData()
		{
			string roleName = SearchCriteriaState<string>.Instance.Get(
                PathHelper.Role.GetDefaultUrl());
			RoleObjectDataSource.SelectParameters.Clear();
			SearchRoles(RoleObjectDataSource, SearchGrid, roleName);
			SearchGrid.DataBind();
			
		}

		protected virtual void SearchGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteRole")
			{
				int roleId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				var users = IoC.Resolve<ISystemUserSecureService>().GetAllByAssignedRole(roleId);
				
				if (users.Count > 0)
				{
					var sb = new StringBuilder(Resources.UserMessage.CantDeleteRole);
					foreach (var user in users)
					{
						sb.Append("'");
						sb.Append(user.UserName);
						sb.Append("', ");
					}
					Messager.Add(sb.Remove(sb.Length - 2, 2).Append('.').ToString());
				}
				else
				{
					_roleService.Delete(SignInHelper.SignedInSystemUser, roleId);
					if (SearchGrid.Rows.Count == 1 && SearchGrid.PageIndex > 0)
						SearchGrid.PageIndex -= 1;
				}
                RestoreSearchData();
			}
		}

		protected override void PopulateForm()
		{
			RestoreSearchData();
		}

		public static void SearchRoles(ObjectDataSource ods, GridView gv,string roleTitle)
		{
			ods.SelectParameters.Clear();
			ods.SelectParameters.Add("maximumRows", gv.PageSize.ToString(CultureInfo.CurrentCulture));
			ods.SelectParameters.Add("startRowIndex", (gv.PageIndex*gv.PageSize).ToString(CultureInfo.CurrentCulture));
			ods.SelectParameters.Add("title", roleTitle);
		}
	}
}
