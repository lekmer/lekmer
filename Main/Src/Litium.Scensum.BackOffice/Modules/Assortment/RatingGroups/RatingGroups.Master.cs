﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RatingGroups
{
	public partial class RatingGroupsMaster : MasterPage
	{
		// Private Members.

		private Collection<string> _breadcrumbAppend;


		// Public Properties.

		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				return _breadcrumbAppend ?? (_breadcrumbAppend = new Collection<string>());
			}
		}

		public virtual bool DenySelection
		{
			get
			{
				return RatingGroupFoldersTree.DenySelection;
			}
			set
			{
				RatingGroupFoldersTree.DenySelection = value;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session["SelectedRatingGroupFolderId"];
			}
			set
			{
				Session["SelectedRatingGroupFolderId"] = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return RatingGroupFoldersTree.RootNodeId;
			}
		}

		public virtual string RootNodeTitle
		{
			get
			{
				return RatingGroupFoldersTree.RootNodeTitle;
			}
		}


		// Protected Properties.

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}


		// Protected Methods.

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			SearchButton.Click += OnSearch;
			CreateButton.Click += OnRatingGroupCreateFromPanel;
			RatingGroupFoldersTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (SelectedFolderId == null)
			{
				SelectedFolderId = RootNodeId;
			}

			if (!IsPostBack)
			{
				PopulateTree(null);
				RestoreSearchField();
				BuildBreadcrumbs();
			}

			SetupTabAndPanel();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (RatingGroupFoldersTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}

			ScriptManager.RegisterStartupScript(
				LeftUpdatePanel,
				LeftUpdatePanel.GetType(),
				"root menu",
				string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", RatingGroupFoldersTree.ClientID),
				true);
		}

		protected virtual void RestoreSearchField()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(LekmerPathHelper.RatingGroup.GetSearchResultUrl());

			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		protected virtual void BuildBreadcrumbs()
		{
			if (IsSearchResult)
			{
				Breadcrumbs.Text = Resources.General.Literal_SearchResults;
				return;
			}

			string breadcrumbs = string.Empty;
			const string bredcrumbsSeperator = " > ";

			if (SelectedFolderId.HasValue && SelectedFolderId.Value != RootNodeId)
			{
				var folders = GetNodes(SelectedFolderId);
				var folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
				while (folder != null)
				{
					breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", bredcrumbsSeperator, folder.Title, breadcrumbs);

					int? parentNodeId = folder.ParentId;

					folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
				}
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, bredcrumbsSeperator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", RootNodeTitle, breadcrumbs);
			Breadcrumbs.Text = breadcrumbs;
		}

		protected virtual void OnRatingGroupCreateFromPanel(object sender, EventArgs e)
		{
			SelectedFolderId = null;

			Response.Redirect(LekmerPathHelper.RatingGroup.GetCreateUrl(RatingGroupFoldersTree.RootNodeId));
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			SelectedFolderId = null;

			var searchUrl = LekmerPathHelper.RatingGroup.GetSearchResultUrl();
			SearchCriteriaState<string>.Instance.Save(searchUrl, SearchTextBox.Text);

			Response.Redirect(searchUrl);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);

			if (e.Id == -1)
			{
				OnFolderCreate(sender, e);
			}
			else
			{
				switch (e.EventName)
				{
					case "Expand":
						DenySelection = true;
						break;
					case "Navigate":
						SelectedFolderId = e.Id;
						RedirectToDefaultPage();
						break;
				}
			}
		}

		protected virtual void OnFolderCreate(object sender, EventArgs e)
		{
			SelectedFolderId = RatingGroupFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.RatingGroupFolder.GetCreateUrl(SelectedFolderId.Value));
			}
		}

		protected virtual void OnFolderEdit(object sender, EventArgs e)
		{
			SelectedFolderId = RatingGroupFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.RatingGroupFolder.GetEditUrl(SelectedFolderId.Value));
			}
		}

		protected virtual void OnFolderDelete(object sender, EventArgs e)
		{
			if (RatingGroupFoldersTree.MenuLastClickedNodeId == null)
			{
				return;
			}

			var folderId = RatingGroupFoldersTree.MenuLastClickedNodeId.Value;

			var ratingGroupFolderSecureService = IoC.Resolve<IRatingGroupFolderSecureService>();

			int? parentFolderId = ratingGroupFolderSecureService.GetById(folderId).ParentRatingGroupFolderId;

			if (!ratingGroupFolderSecureService.TryDelete(SignInHelper.SignedInSystemUser, folderId))
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingGroupFolderDeleteFailed);
				return;
			}

			SystemMessageContainer.MessageType = InfoType.Success;
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);

			SelectedFolderId = parentFolderId.HasValue ? parentFolderId.Value : RootNodeId;

			PopulateTree(null);

			RedirectToDefaultPage();
		}

		protected virtual void OnRatingGroupCreate(object sender, EventArgs e)
		{
			SelectedFolderId = RatingGroupFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.RatingGroup.GetCreateUrl(SelectedFolderId.Value));
			}
		}


		// Public Methods.

		public virtual void PopulateTree(int? folderId)
		{
			RatingGroupFoldersTree.DataSource = GetNodes(folderId);
			RatingGroupFoldersTree.RootNodeTitle = Resources.RatingReview.Literal_RatingGroupFolders;
			RatingGroupFoldersTree.DataBind();

			LeftUpdatePanel.Update();

			RatingGroupFoldersTree.SelectedNodeId = folderId ?? SelectedFolderId;
		}

		public virtual void SetupTabAndPanel()
		{
			if (Master != null)
			{
				var topMaster = (Master.Start)(Master).Master;
				if (topMaster != null)
				{
					topMaster.SetActiveTab("Assortment", "RatingGroups");
				}
			}

			RatingGroupPanel.Text = Resources.RatingReview.Literal_RatingGroups;
			CreateButton.Text = Resources.RatingReview.Literal_RatingGroups;
		}

		public virtual Collection<INode> GetNodes(int? folderId)
		{
			var id = folderId ?? ((SelectedFolderId == RootNodeId) ? null : SelectedFolderId);

			var ratingGroupFolderSecureService = IoC.Resolve<IRatingGroupFolderSecureService>();
			return ratingGroupFolderSecureService.GetTree(id);
		}

		public virtual void UpdateSelection(int nodeId)
		{
			RatingGroupFoldersTree.SelectedNodeId = SelectedFolderId = nodeId;
			LeftUpdatePanel.Update();
		}

		public virtual void UpdateBreadcrumbs(string value)
		{
			Breadcrumbs.Text = value;
		}

		public virtual void UpdateBreadcrumbs(int? folderId)
		{
			SelectedFolderId = folderId;
			BuildBreadcrumbs();
		}

		public virtual void RedirectToDefaultPage()
		{
			Response.Redirect(LekmerPathHelper.RatingGroup.GetDefaultUrl());
		}
	}
}