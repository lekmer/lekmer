<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Assortment/PriceLists/PriceLists.Master" CodeBehind="FolderEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.FolderEdit" %>
<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
					<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vg" />
					</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="fec" ContentPlaceHolderID="PriceListContentPlaceHolder" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<asp:Label ID="FolderEditLabel" runat="server" CssClass="header-text"></asp:Label>
		<br />
		<div class="include-create-edit">	
			<div class="column">
				<div class="input-box">
					<span><%=Resources.General.Literal_Title%>&nbsp;*</span>
					<asp:RequiredFieldValidator runat="server" 
						ID="FolderTitleValidator" 
						ControlToValidate="FolderTitleTextBox" 
						Display ="None" 
						ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty%>" 
						ValidationGroup="vg" />
					<br />
					<asp:TextBox ID="FolderTitleTextBox" runat="server" />
				</div>			
				<br />

				<div class="buttons">
					<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save%>" ValidationGroup="vg" SkinID="DefaultButton" />
					<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel%>" CausesValidation="false" SkinID="DefaultButton" />
				</div>

			</div>
			<div class="column">
				<div class="input-box">
					<div runat="server" ID="DestinationDiv">
						<span><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:General, Literal_PutInto%>" /></span>
						<asp:UpdatePanel ID="TreeUpdatePanel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<CustomControls:TemplatedTreeView runat="server" ID="FolderTree"
									UseRootNode="true" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
									DisplayTextControl="lbName"  MainContainerCssClass="treeview-main-container"
									NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"
									NodeMainContainerCssClass="treeview-node" NodeParentContainerCssClass="treeview-node-parent" 
									NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png" NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
									MenuCallerElementCssClass="tree-menu-caller" MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
									<HeaderTemplate>
										<div class="treeview-header"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:General, Literal_Title%>" /></div>
									</HeaderTemplate>              
									<NodeTemplate>
										<div class="tree-item-cell-expand">
											<img src='<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>' alt="" class="tree-icon" />
											<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
										</div>
										<div class="tree-item-cell-main">
											<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt=""  class="tree-node-img"/>
											<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
										</div>                 
										<br />
									</NodeTemplate>
								</CustomControls:TemplatedTreeView>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
				</div>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
