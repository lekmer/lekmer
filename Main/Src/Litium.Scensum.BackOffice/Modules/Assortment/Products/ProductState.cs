using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	[Serializable]
	public class ProductState
	{
		public string TempMediaFileName { get; set; }
		public bool SetNewImage { get; set; }
		public int? MediaId { get; set; }
		public object TempRelationList { get; set; }
		public IRelationList ColorVariantRelationList { get; set; }
		public Collection<IProductRegistryRestrictionItem> RegistryRestrictionProducts { get; set; }

		private readonly Collection<ProductRatingGroupState> _productRatingGroups;

		public Collection<ProductRatingGroupState> ProductRatingGroups
		{
			get { return _productRatingGroups; }
		}

		public ProductState(Collection<ProductRatingGroupState> productRatingGroups)
		{
			_productRatingGroups = productRatingGroups;
		}
	}

	[Serializable]
	public class ProductRatingGroupState
	{
		private IRatingGroup _ratingGroup;
		private IRatingGroupProduct _ratingGroupProduct;

		public IRatingGroup RatingGroup
		{
			get { return _ratingGroup; }
		}

		public IRatingGroupProduct RatingGroupProduct
		{
			get { return _ratingGroupProduct; }
		}

		public ProductRatingGroupState(IRatingGroup ratingGroup, IRatingGroupProduct ratingGroupProduct)
		{
			_ratingGroup = ratingGroup;
			_ratingGroupProduct = ratingGroupProduct;
		}
	}
}
