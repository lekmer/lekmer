using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Assortment;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using ProductTypeConstant = Litium.Lekmer.Product.Constant.ProductType;
using RelationListTypeConstant = Litium.Lekmer.Product.RelationListType;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	[SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling")]
	public partial class ProductEdit : LekmerStatePageController<ProductState>, IEditor
	{
		private const int SeoFieldsMaxLength = 500;
		//TODO: Remove when add cache logic
		private Collection<ICurrency> _currencies;
		//TODO: Remove when add cache logic
		private Collection<IPriceListStatus> _statuses;

		private Collection<ICategoryTagGroup> _categoryTagGroups;

		private Collection<IChannel> _channels;
		private Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = IoC.Resolve<IChannelSecureService>().GetAll()); }
		}

		private ILekmerRelationListSecureService _relationListSecureService;
		protected ILekmerRelationListSecureService RelationListSecureService
		{
			get { return _relationListSecureService ?? (_relationListSecureService = (ILekmerRelationListSecureService) IoC.Resolve<IRelationListSecureService>()); }
		}

		private IDeliveryTimeSecureService _deliveryTimeSecureService;
		protected IDeliveryTimeSecureService DeliveryTimeSecureService
		{
			get { return _deliveryTimeSecureService ?? (_deliveryTimeSecureService = IoC.Resolve<IDeliveryTimeSecureService>()); }
		}

		private IProductSecureService _productSecureService;
		protected IProductSecureService ProductSecureService
		{
			get { return _productSecureService ?? (_productSecureService = IoC.Resolve<IProductSecureService>()); }
		}

		private Collection<IRelationList> _allRelationLists;
		protected Collection<IRelationList> AllRelationLists
		{
			get { return _allRelationLists ?? (_allRelationLists = RelationListSecureService.GetAllByProduct(GetId())); }
		}

		private IRelationList _colorVariant;
		protected IRelationList ColorVariantRelationList
		{
			get { return _colorVariant ?? (_colorVariant = AllRelationLists.FirstOrDefault(rl => rl.RelationListTypeId == (int) RelationListTypeConstant.ColorVariant)); }
		}

		protected virtual Collection<IRelationList> GetRelationListState(int productId)
		{
			Collection<IRelationList> relationList;
			object relationListStored = State == null ? null : State.TempRelationList;
			if (relationListStored != null)
			{
				relationList = (Collection<IRelationList>) relationListStored;
			}
			else
			{
				IList<IRelationList> tempRelationList = AllRelationLists.Where(rl => rl.RelationListTypeId != (int)RelationListTypeConstant.ColorVariant).ToList();
				relationList = new Collection<IRelationList>(tempRelationList);
				SetTempRelationList(relationList);
			}
			return relationList;
		}

		protected virtual string GetPriceListStatus(int priceListStatusId)
		{
			//TODO: Remove when add cache logic
			if (_statuses == null)
			{
				_statuses = IoC.Resolve<IPriceListStatusSecureService>().GetAll();
			}

			return _statuses.FirstOrDefault(item => item.Id == priceListStatusId).Title;
		}

		protected virtual ICurrency GetCurrency(int? currencyId)
		{
			if (!currencyId.HasValue) return null;

			//TODO: Remove when add cache logic
			if (_currencies == null)
			{
				_currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
			}

			return _currencies.FirstOrDefault(item => item.Id == currencyId);
		}

		private int _priceListId;
		private IPriceList _priceList;

		private delegate void ProductSavedHandler(IProduct product);
		private event ProductSavedHandler ProductSaved;
		private void OnProductSaved(IProduct product)
		{
			ProductTitleLiteral.Text = product.DisplayTitle;
		}

		protected virtual IPriceList GetPriceList(int id)
		{
			if (_priceListId != id)
			{
				_priceList = null;
				_priceListId = id;
			}
			return _priceList ?? (_priceList = IoC.Resolve<IPriceListSecureService>().GetById(id));
		}

		protected virtual string GetPrice(int priceListId, decimal price)
		{
			var currencyId = GetPriceList(priceListId).CurrencyId;
			var currency = GetCurrency(currencyId);
			return IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, currency, price);
		}

		protected virtual string GetPriceVat(decimal vatPercentage)
		{
			return string.Format(CultureInfo.CurrentCulture, "{0}%", Math.Round(vatPercentage));
		}

		protected override void SetEventHandlers()
		{
			ProductMediaControl.Selected += ImageSelected;
			ProductMediaControl.FileUploaded += ImageSelectUnload;
			ProductMediaControl.DeleteImage += ProductMediaDeleteImage;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			SiteStructureRegistryGrid.RowDataBound += SiteStructureRegistryGridRowDataBound;
			RelationsGrid.RowCommand += RelationsGridRowCommand;
			RelationSearchResultGrid.RowDataBound += RelationSearchResultGridRowDataBound;
			AddRelationsButton.Click += AddRelationsButtonClick;
			RelationSearchButton.Click += RelationSearchButtonClick;
			StoresGrid.RowDataBound += OnStoresRowDataBound;
			TagRepeater.ItemDataBound += TagRepeaterItemDataBound;

			RatingGroupsGrid.PageIndexChanging += OnRatingGroupsPageIndexChanging;
			RatingGroupsGrid.RowDataBound += OnRatingGroupsRowDataBound;
			RatingGroupsGrid.RowCommand += OnRatingGroupsRowCommand;
			RemoveRatingGroupsButton.Click += OnRatingGroupsRemove;
			RatingGroupSelector.SelectEvent += OnRatingGroupsAdd;

			EditTagGroupsDefaultSettingsButton.Click += EditTagGroupsDefaultSettingsButtonClick;

			ProductSaved += OnProductSaved;

			ColorVariantProducts.ChangeProductListEvent += ColorVariantProductsChanged;
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			ProductShortDescriptionTranslator.TriggerImageButton = ShortDescriptionTranslateButton;
			ProductDescriptionTranslator.TriggerImageButton = DescriptionTranslateButton;
			ProductSeoDescriptionTranslator.TriggerImageButton = SeoDescriptionTranslateButton;
			ProductSeoKeywordsTranslator.TriggerImageButton = SeoKeywordsTranslateButton;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			WebShopTitleValidator.Enabled = UseWebShopTitleCheckBox.Checked;
		}

		protected override void PopulateForm()
		{
			IProductRecord product = ProductSecureService.GetRecordById(ChannelHelper.CurrentChannel.Id, GetId());
			Populate(product);

			ProductMediaControl.ProductId = product.Id;

			PriceListObjectDataSource.SelectParameters.Clear();
			PriceListObjectDataSource.SelectParameters.Add("productId", product.Id.ToString(CultureInfo.CurrentCulture));

			var relationListTypeSecureService = IoC.Resolve<IRelationListTypeSecureService>();
			RelationTypeList.DataSource = relationListTypeSecureService.GetAll().Where(t => t.CommonName != RelationListTypeConstant.ColorVariant.ToString());
			RelationTypeList.DataBind();
			RelationTypeList.Items.Insert(0, new ListItem(string.Empty, string.Empty));

			PopulateRatingGroups();
			PopulateProductRegistryRestrictions();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			RestoreTitle();
			SetClientFunctionForRelations();
			ChangeImage();
	
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var productUrls = CollectProductUrls();
			IProductRecord product = GetProduct();

			int? oldBrandId = ((ILekmerProduct) product).BrandId;

			ValidateSettings(productUrls);
			ValidateSeoFields();
			if (SystemMessageContainer.HasMessages)
			{
				//ScriptManager.RegisterStartupScript(this, GetType(), "9",
				//                                    "$(document).ready(function() { $('#tabs').tabs('select', 3); });", true);
				return;
			}

			IProductSeoSetting seoSetting = GetProductSeoSetting();
			GetSiteStructureRegistries(product);
			ApplySettings(product);
			ApplySizes(product);

			var productRegistryRestrictions = GetProductRegistryRestrictions();
			int? newBrandId = ((ILekmerProduct) product).BrandId;

			Collection<int> initialTags;
			Collection<int> selectedTags;
			GetTags(out initialTags, out selectedTags);
			var tagsToRemove = new Collection<int>(initialTags.Except(selectedTags).ToList());
			var tagsToAdd = new Collection<int>(selectedTags.Except(initialTags).ToList());

			int oldDeliveryTimeId;
			bool isNeedDeleteDeliveryTime = ManageDeliveryTime((ILekmerProductRecord) product, out oldDeliveryTimeId);

			var productSecureService = (ILekmerProductSecureService) ProductSecureService;
			productSecureService.Save(
				SignInHelper.SignedInSystemUser,
				product,
				GetRelationListState(product.Id),
				seoSetting,
				ProductMediaControl.ProductImageGroups,
				GetStores(),
				ProductWebShopTitleTranslator.GetTranslations(),
				ProductShortDescriptionTranslator.GetTranslations(),
				ProductDescriptionTranslator.GetTranslations(),
				ProductSeoTitleTranslator.GetTranslations(),
				ProductSeoDescriptionTranslator.GetTranslations(),
				ProductSeoKeywordsTranslator.GetTranslations(),
				MeasurementTranslator.GetTranslations(),
				GetSelectedIcons(),
				tagsToRemove,
				tagsToAdd,
				CollectProductSizes(),
				productUrls,
				productRegistryRestrictions,
				GetRecommendedPrices(product.Id));

			SaveProductRatingGroups();
			UpdateRelatedPackages(product);
			ManageColorVariants(product);

			ProductSaved(product);

			// Update product registry product.
			ManageRestrictionProduct(productRegistryRestrictions, product.Id);
			ManageRestrictionBrand(oldBrandId, newBrandId, product.Id);

			// Delete DeliveryTime
			if (isNeedDeleteDeliveryTime)
			{
				DeliveryTimeSecureService.Delete(oldDeliveryTimeId);
			}

			SystemMessageContainer.Add(Resources.Product.Message_ProductSaved);
			SystemMessageContainer.MessageType = InfoType.Success;

			PopulateUrlHistory();
			PopulateProductRegistryRestrictions();
			PopulateTagRepeaterItems(product.CategoryId);
			PopulateDeliveryTimes((ILekmerProductRecord) product);
		}

		protected virtual void ProductMediaDeleteImage(object sender, EventArgs e)
		{
			SetMediaId(null);
			PopulateImage((int?)null);
			RedirectToMediaTab();
		}

		private void ImageSelectUnload(object sender, EventArgs e)
		{
			ProductMediaControl.DefaultImageSelectMode = true;
			RedirectToMediaTab();
		}

		private void ImageSelected(object sender, ImageSelectEventArgs e)
		{
			if (ProductMediaControl.DefaultImageSelectMode)
			{
				PopulateImage(e.Image);
				SetMediaId(e.Image.Id);
			}
			else
			{
				ProductMediaControl.DefaultImageSelectMode = true;
			}
			RedirectToMediaTab();
		}

		private void SelectImage(IImage image)
		{
			PopulateImage(image);
			SetMediaId(image.Id);
			RedirectToMediaTab();
		}

		protected virtual void BtnCancelSaveImageClick(object sender, EventArgs e)
		{
			if (State != null && !string.IsNullOrEmpty(State.TempMediaFileName))
			{
				IoC.Resolve<IMediaItemSecureService>().DeleteTemporary(State.TempMediaFileName);
			}
			RedirectToMediaTab();
		}

		protected IProductRecord GetProduct()
		{
			IProductRecord product = ProductSecureService.GetRecordById(ChannelHelper.CurrentChannel.Id, GetId());
			if (product == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}
			var lekmerProduct = (ILekmerProductRecord) product;

			if (StatusList.SelectedItem != null)
				product.ProductStatusId = System.Convert.ToInt32(StatusList.SelectedItem.Value, CultureInfo.CurrentCulture);
			product.ErpId = ErpTextBox.Text;
			lekmerProduct.LekmerErpId = LekmerErpTextBox.Text;
			lekmerProduct.SellOnlyInPackage = SellOnlyInPackage.Checked;
			lekmerProduct.IsDropShip = IsDropShipCheckBox.Checked;
			product.EanCode = EanTextBox.Text;

			product.WebShopTitle = UseWebShopTitleCheckBox.Checked ? WebShopTitleTextBox.Text : null;

			product.ShortDescription = ShortArticleDescriptionEditor.GetValue();
			product.Description = LongArticleDescriptionEditor.GetValue();
			if (State != null)
			{
				if (State.SetNewImage)
				{
					product.MediaId = State.MediaId;
				}
			}
			return product;
		}

		protected IProductSeoSetting GetProductSeoSetting()
		{
			var seoSettingService = IoC.Resolve<IProductSeoSettingSecureService>();

			IProductSeoSetting productSeoSetting = seoSettingService.Create();
			productSeoSetting.Title = SeoTitleTextBox.Text;
			productSeoSetting.Description = SeoDescrTextBox.Text;
			productSeoSetting.Keywords = SeoKeywordsTextBox.Text;
			return productSeoSetting;
		}

		protected void ValidateSeoFields()
		{
			if (SeoTitleTextBox.Text.Length > SeoFieldsMaxLength)
			{
				SystemMessageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.SeoTitleToLong, SeoFieldsMaxLength), InfoType.Warning);
			}
			if (SeoDescrTextBox.Text.Length > SeoFieldsMaxLength)
			{
				SystemMessageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.SeoDescriptionToLong, SeoFieldsMaxLength), InfoType.Warning);
			}
			if (SeoKeywordsTextBox.Text.Length > SeoFieldsMaxLength)
			{
				SystemMessageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.SeoKeywordsToLong, SeoFieldsMaxLength), InfoType.Warning);
			}
		}

		protected void Populate(IProductRecord product)
		{
			var lekmerProduct = (ILekmerProductRecord)product;
			PopulateStatus(product);
			PopulateTitle(product);
			PopulateErp(product);
			PopulatePrice(product);
			PopulateSeoSettings(product);
			PopulateImage(product.MediaId);
			PopulateCategory(product);
			PopulateRelationsList();
			PopulateColorVariants(product);
			PopulateSiteStructure(product);
			PopulateStores();
			PopulateSettingsTab(lekmerProduct);
			PopulateTranslation();
			PopulateTagsTab(product.CategoryId);
			PopulateSizesTab(lekmerProduct);
			PopulateUrlHistory();
			PopulateRecommendedPrices(lekmerProduct);
		}

		protected void PopulateStatus(IProduct product)
		{
			var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
			BindList(StatusList, productStatusSecureService.GetAll(), product.ProductStatusId.ToString(CultureInfo.CurrentCulture));

			if (product.IsPackage())
			{
				StatusList.Enabled = false;
			}
		}

		protected void PopulateTitle(IProductRecord product)
		{
			UseWebShopTitleCheckBox.Checked = !string.IsNullOrEmpty(product.WebShopTitle);
			ShowTitle();
			ProductTitleLiteral.Text = product.DisplayTitle;
			WebShopTitleTextBox.Text = product.WebShopTitle;
			TitleTextBox.Text = product.Title;
			UseWebShopTitleCheckBox.Attributes.Add("onClick",
											  "ShowHide('"
											  + UseWebShopTitleCheckBox.ClientID
											  + "','"
											  + WebShopTitleTextBox.ClientID +
											  "','"
											  + ProductWebShopTitleTranslator.TriggerImageButton.ClientID +
											  "','"
											  + TitleTextBox.ClientID
											  + "');");

			ShortArticleDescriptionEditor.SetValue(product.ShortDescription ?? string.Empty);
			LongArticleDescriptionEditor.SetValue(product.Description);
		}

		protected void RestoreTitle()
		{
			if (Page.IsPostBack)
			{
				ShowTitle();
			}
		}

		protected void ChangeImage()
		{
			if (Page.IsPostBack)
			{

				var mediaIdStr = this.Request.Params.Get("__EVENTARGUMENT");
				int mediaId;

				if (Int32.TryParse(mediaIdStr, out mediaId))
				{

					ProductMediaControl.SetDefaltImage(mediaId);
				}

			}
		}

		protected void ShowTitle()
		{
			if (UseWebShopTitleCheckBox.Checked)
			{
				TitleTextBox.Attributes.Add("style", "display : none;");
				WebShopTitleTextBox.Attributes.Add("style", "display : block;");
				ProductWebShopTitleTranslator.TriggerImageButton.Attributes.Add("style", "display : block;");
			}
			else
			{
				TitleTextBox.Attributes.Add("style", "display : block;");
				WebShopTitleTextBox.Attributes.Add("style", "display : none;");
				ProductWebShopTitleTranslator.TriggerImageButton.Attributes.Add("style", "display : none;");
			}
		}

		protected void PopulateErp(IProduct product)
		{
			var lekmerProduct = (ILekmerProductRecord) product;

			LekmerErpTextBox.Text = lekmerProduct.LekmerErpId;
			ErpTextBox.Text = product.ErpId;
			SupplierArticleNoTextBox.Text = lekmerProduct.SupplierArticleNumber;
			EanTextBox.Text = product.EanCode;
			NumberInStockTextBox.Text = product.NumberInStock.ToString(CultureInfo.CurrentCulture);
			StockStatusTextBox.Text = ((StockStatus)lekmerProduct.StockStatusId).ToString();
			SupplierIdTextBox.Text = lekmerProduct.SupplierId;
			SellOnlyInPackage.Checked = lekmerProduct.SellOnlyInPackage;
			IsDropShipCheckBox.Checked = lekmerProduct.IsDropShip;

			var productTypeId = lekmerProduct.ProductTypeId;
			ProductTypeLink.Text = ((ProductTypeConstant)productTypeId).ToString();
			if (product.IsPackage())
			{
				var package = IoC.Resolve<IPackageSecureService>().GetByProductIdSecure(ChannelHelper.CurrentChannel.Id, product.Id);
				if (package != null)
				{
					ProductTypeLink.NavigateUrl = LekmerPathHelper.Package.GetEditUrl(package.PackageId);
					ProductTypeLink.CssClass = "blue";
				}
				SellOnlyInPackage.Enabled = false;
			}
		}

		protected void PopulatePrice(IProduct product)
		{
			var formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>();

			if (product.Price != null)
			{
				PriceTextBox.Text = formatter.FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
				PriceVatTypeTextBox.Text = string.Format(CultureInfo.CurrentCulture, "{0}%", Math.Round(product.Price.GetVatAmount() / product.Price.PriceExcludingVat * 100));
			}
			else
			{
				PriceTextBox.Text = Resources.LekmerMessage.ProductEdit_NotAvailableForCurrentChannel;
			}

			// Purchase price.
			var purchasePrice = string.Empty;
			var productRecord = (ILekmerProductRecord) product;
			if (productRecord.PurchasePrice.HasValue)
			{
				purchasePrice = productRecord.PurchasePrice.Value.ToLocalStringAsNumber();
				var currency = GetCurrency(productRecord.PurchaseCurrencyId);
				if (currency != null)
				{
					purchasePrice += string.Format(" ({0})", currency.Iso);
				}
			}
			PurchasePriceTextBox.Text = purchasePrice;

			// Average price.
			var averagePrice = string.Empty;
			if (productRecord.AveragePrice.HasValue)
			{
				averagePrice = productRecord.AveragePrice.Value.ToLocalStringAsNumber();
				var currency = GetCurrency(1); // Always SE/SEK
				if (currency != null)
				{
					averagePrice += string.Format(" ({0})", currency.Iso);
				}
			}
			AveragePriceTextBox.Text = averagePrice;
		}

		protected void PopulateSeoSettings(IProduct product)
		{
			var seoSettingService = IoC.Resolve<IProductSeoSettingSecureService>();
			IProductSeoSetting productSeoSetting = seoSettingService.GetByProductId(product.Id);
			if (productSeoSetting == null)
			{
				return;
			}

			SeoTitleTextBox.Text = productSeoSetting.Title;
			SeoDescrTextBox.Text = productSeoSetting.Description;
			SeoKeywordsTextBox.Text = productSeoSetting.Keywords;
		}

		protected void PopulateTranslation()
		{
			int productId = GetId();

			ProductWebShopTitleTranslator.DefaultValueControlClientId = WebShopTitleTextBox.ClientID;
			ProductWebShopTitleTranslator.BusinessObjectId = productId;
			ProductWebShopTitleTranslator.DataSource = IoC.Resolve<ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository>>().GetAllByProduct(productId);
			ProductWebShopTitleTranslator.DataBind();

			ProductShortDescriptionTranslator.DefaultValueControlClientId = ShortArticleDescriptionEditor.ClientID;
			ProductShortDescriptionTranslator.BusinessObjectId = productId;
			ProductShortDescriptionTranslator.DataSource = IoC.Resolve<ProductTranslationGenericSecureService<ProductShortDescriptionTranslationRepository>>().GetAllByProduct(productId);
			ProductShortDescriptionTranslator.DataBind();

			ProductDescriptionTranslator.DefaultValueControlClientId = LongArticleDescriptionEditor.ClientID;
			ProductDescriptionTranslator.BusinessObjectId = productId;
			ProductDescriptionTranslator.DataSource = IoC.Resolve<ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository>>().GetAllByProduct(productId);
			ProductDescriptionTranslator.DataBind();


			ProductSeoTitleTranslator.DefaultValueControlClientId = SeoTitleTextBox.ClientID;
			ProductSeoTitleTranslator.BusinessObjectId = productId;
			ProductSeoTitleTranslator.DataSource = IoC.Resolve<ProductTranslationGenericSecureService<ProductSeoSettingTitleTranslationRepository>>().GetAllByProduct(productId);
			ProductSeoTitleTranslator.DataBind();

			ProductSeoDescriptionTranslator.DefaultValueControlClientId = SeoDescrTextBox.ClientID;
			ProductSeoDescriptionTranslator.BusinessObjectId = productId;
			ProductSeoDescriptionTranslator.DataSource = IoC.Resolve<ProductTranslationGenericSecureService<ProductSeoSettingDescriptionTranslationRepository>>().GetAllByProduct(productId);
			ProductSeoDescriptionTranslator.DataBind();

			ProductSeoKeywordsTranslator.DefaultValueControlClientId = SeoKeywordsTextBox.ClientID;
			ProductSeoKeywordsTranslator.BusinessObjectId = productId;
			ProductSeoKeywordsTranslator.DataSource = IoC.Resolve<ProductTranslationGenericSecureService<ProductSeoSettingKeywordsTranslationRepository>>().GetAllByProduct(productId);
			ProductSeoKeywordsTranslator.DataBind();
		}

		protected void PopulateUrlHistory()
		{
			int productId = GetId();

			ProductUrlHistoryControl.ProductId = productId;
			ProductUrlHistoryControl.DataBind();
		}

		[SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
		protected void PopulateCategory(IProductRecord product)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			ICategory category = categorySecureService.GetById(product.CategoryId);
			CategoryTextBox.Text = category.Title;
		}

		protected static void BindList(ListControl ddl, object dataSource, string defaultValue)
		{
			ddl.DataSource = dataSource;
			ddl.DataBind();
			ListItem item = ddl.Items.FindByValue(defaultValue);
			if (item != null)
			{
				item.Selected = true;
			}
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Assortment.Product.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Assortment.Product.GetDefaultUrl());
			}
		}

		#region Images

		protected void PopulateImage(int? imageId)
		{
			DefaultImage.Visible = DefaultZoomImage.Visible = false;

			ProductMediaControl.ImageId = imageId;
			if (imageId.HasValue)
			{
				var imageService = IoC.Resolve<IImageSecureService>();
				IImage image = imageService.GetById((int)imageId);
				PopulateImage(image);
			}
		}


		protected void PopulateImage(IImage image)
		{
			DefaultImage.Visible = DefaultZoomImage.Visible = false;

			string imageUrl = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(image.Id, image.FormatExtension));

			ImageLeftAnchor.HRef = imageUrl;

			DefaultImage.Visible = DefaultZoomImage.Visible = true;
			ImageLeftAnchor.Title = image.Title;
			DefaultImage.ImageUrl =
				ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
			ProductMediaControl.PopulateImage(image);
		}

		protected void SetMediaId(int? id)
		{
			EnsureState();
			State.MediaId = id;
			State.SetNewImage = true;
		}

		protected void EnsureState()
		{
			if (State == null)
			{
				State = new ProductState(new Collection<ProductRatingGroupState>());
			}
		}
		protected void SetTempRelationList(Collection<IRelationList> relationLists)
		{
			EnsureState();
			State.TempRelationList = relationLists;
		}


		protected void RedirectToMediaTab()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), "media",
												"$(document).ready(function() { $('#tabs').tabs('select', 2); });", true);
		}

		#endregion

		#region SiteStructure

		private void PopulateSiteStructure(IProductRecord product)
		{
			SiteStructureRegistryGrid.DataSource = product.ProductSiteStructureRegistries;
			SiteStructureRegistryGrid.DataBind();
		}

		protected void SiteStructureRegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var reg = (IProductSiteStructureRegistry)row.DataItem;
			var lblTitle = (Label)row.FindControl("TitleLabel");
			lblTitle.Text = IoC.Resolve<ISiteStructureRegistrySecureService>().GetById(reg.SiteStructureRegistryId).Title;
			var pcns = (LekmerContentPageSelector)row.FindControl("ParentContentNodeSelector");
			pcns.SiteStructureRegistryId = reg.SiteStructureRegistryId;
			pcns.SelectedNodeId = reg.ParentContentNodeId;
			pcns.SetContentPageTitle();
			var tcns = (LekmerContentPageSelector)row.FindControl("TemplateContentNodeSelector");
			tcns.SiteStructureRegistryId = reg.SiteStructureRegistryId;
			tcns.SelectedNodeId = reg.TemplateContentNodeId;
			tcns.SetContentPageTitle();
		}

		private void GetSiteStructureRegistries(IProductRecord product)
		{
			foreach (GridViewRow row in SiteStructureRegistryGrid.Rows)
			{
				var pcns = ((LekmerContentPageSelector)row.FindControl("ParentContentNodeSelector"));
				IProductSiteStructureRegistry reg =
					product.ProductSiteStructureRegistries.First(r => r.SiteStructureRegistryId == pcns.SiteStructureRegistryId);
				reg.ParentContentNodeId = pcns.SelectedNodeId;
				reg.TemplateContentNodeId = ((LekmerContentPageSelector)row.FindControl("TemplateContentNodeSelector")).SelectedNodeId;
			}
		}

		// PopulateProductRegistryRestrictions.
		private void PopulateProductRegistryRestrictions()
		{
			var productId = GetIdOrNull();
			if (productId.HasValue)
			{
				var registryRestrictionProducts = IoC.Resolve<IProductRegistryRestrictionProductSecureService>().GetAllByProduct(productId.Value);

				EnsureState();
				State.RegistryRestrictionProducts = registryRestrictionProducts;

				var registryRestrictions = registryRestrictionProducts.ToDictionary(restriction => restriction.ProductRegistryId, restriction => restriction.RestrictionReason);

				ProductRegistryRestriction.RegistryRestrictions = registryRestrictions;
			}

			ProductRegistryRestriction.BindData();
		}

		private Collection<IProductRegistryRestrictionItem> GetProductRegistryRestrictions()
		{
			var productRegistryRestrictionProductList = new Collection<IProductRegistryRestrictionItem>();

			var productId = GetIdOrNull();
			if (productId.HasValue)
			{
				var productRegistryRestrictionProductSecureService = IoC.Resolve<IProductRegistryRestrictionProductSecureService>();

				var restrictions = ProductRegistryRestriction.GetRestrictions();
				foreach (var restriction in restrictions)
				{
					var productRegistryRestrictionProduct = productRegistryRestrictionProductSecureService.Create();
					productRegistryRestrictionProduct.ProductRegistryId = restriction.Key;
					productRegistryRestrictionProduct.RestrictionReason = restriction.Value;
					productRegistryRestrictionProduct.ItemId = productId.Value;
					productRegistryRestrictionProduct.UserId = SignInHelper.SignedInSystemUser.Id;

					productRegistryRestrictionProductList.Add(productRegistryRestrictionProduct);
				}
			}

			return productRegistryRestrictionProductList;
		}

		private void ManageRestrictionProduct(Collection<IProductRegistryRestrictionItem> productRegistryRestrictions, int productId)
		{
			var oldRestrictions = State.RegistryRestrictionProducts.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);
			var newRestrictions = productRegistryRestrictions.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);

			UpdateProductRegistryProduct(oldRestrictions, newRestrictions, productId);
		}

		private void ManageRestrictionBrand(int? oldBrandId, int? newBrandId, int productId)
		{
			if ((newBrandId.HasValue && !oldBrandId.HasValue)
				|| (oldBrandId.HasValue && !newBrandId.HasValue)
				|| (newBrandId.HasValue && oldBrandId.Value != newBrandId.Value))
			{
				var productRegistryRestrictionBrandSecureService = IoC.Resolve<IProductRegistryRestrictionBrandSecureService>();

				var oldBrandRestrictions = oldBrandId.HasValue ? productRegistryRestrictionBrandSecureService.GetAllByBrand(oldBrandId.Value) : new Collection<IProductRegistryRestrictionItem>();
				var newBrandRestrictions = newBrandId.HasValue ? productRegistryRestrictionBrandSecureService.GetAllByBrand(newBrandId.Value) : new Collection<IProductRegistryRestrictionItem>();

				var oldRestrictions = oldBrandRestrictions.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);
				var newRestrictions = newBrandRestrictions.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);

				UpdateProductRegistryProduct(oldRestrictions, newRestrictions, productId);
			}
		}

		private void UpdateProductRegistryProduct(Dictionary<int, int> oldRestrictions, Dictionary<int, int> newRestrictions, int productId)
		{
			var toRemoveRestrictions = new Dictionary<int, int>();
			var toAddRestrictions = new Dictionary<int, int>();

			foreach (var oldRestriction in oldRestrictions)
			{
				if (!newRestrictions.ContainsKey(oldRestriction.Key))
				{
					toRemoveRestrictions.Add(oldRestriction.Key, oldRestriction.Key);
				}
			}

			foreach (var newRestriction in newRestrictions)
			{
				if (!oldRestrictions.ContainsKey(newRestriction.Key))
				{
					toAddRestrictions.Add(newRestriction.Key, newRestriction.Key);
				}
			}

			var productRegistryProductSecureService = IoC.Resolve<IProductRegistryProductSecureService>();

			if (toAddRestrictions.Count > 0)
			{
				productRegistryProductSecureService.DeleteByProduct(SignInHelper.SignedInSystemUser, productId, toAddRestrictions.Keys.ToList());
			}

			if (toRemoveRestrictions.Count > 0)
			{
				productRegistryProductSecureService.Insert(SignInHelper.SignedInSystemUser, productId, null, null, toRemoveRestrictions.Keys.ToList());
			}
		}

		#endregion

		protected void PopulateRelationsList()
		{
			RelationsGrid.DataSource = GetRelationListState(GetId());
			RelationsGrid.DataBind();
		}
		private void RelationsGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteRL")
			{
				DeleteRelationList(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			}
		}
		protected void DeleteRelationList(int rlId)
		{
			Collection<IRelationList> relationsList = GetRelationListState(GetId());
			IRelationList relationItem = relationsList.FirstOrDefault(item => item.Id == rlId);
			if (relationItem != null)
			{
				relationsList.Remove(relationItem);
				SetTempRelationList(relationsList);
				PopulateRelationsList();
			}
		}
		private void SetClientFunctionForRelations()
		{
			if (RelationSearchResultGrid.HeaderRow == null) return;
			var chkAll = (CheckBox)RelationSearchResultGrid.HeaderRow.FindControl("AllRelationsCheckBox");
			foreach (GridViewRow row in RelationSearchResultGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + chkAll.ClientID + @"')");
			}
			CancelRelationsButton.Attributes.Add("onclick", "SelectAll1('" + chkAll.ClientID + @"','" + RelationSearchResultGrid.ClientID + @"')");
		}
		void RelationSearchResultGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var cbAll = (CheckBox)row.FindControl("AllRelationsCheckBox");
				cbAll.Checked = false;
				cbAll.Attributes.Add("onclick", "SelectAll1('" + cbAll.ClientID + @"','" + RelationSearchResultGrid.ClientID + @"')");
			}
		}

		protected virtual void AddRelationsButtonClick(object sender, EventArgs e)
		{
			Collection<IRelationList> relationsList = GetRelationListState(GetId());
			foreach (GridViewRow row in RelationSearchResultGrid.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (!selectCheckBox.Checked) continue;
				IRelationList relation = RelationListSecureService.Create();
				int relationId = System.Convert.ToInt32(((HiddenField)row.FindControl("RelationIdHiddenField")).Value, CultureInfo.CurrentCulture);
				if (relationsList.FirstOrDefault(relationItem => relationItem.Id == relationId) == null)
				{
					relation.Id = relationId;
					relation.Title = row.Cells[1].Text;
					relation.RelationListTypeName = row.Cells[2].Text;
					relationsList.Add(relation);
				}
				selectCheckBox.Checked = false;
			}
			if (RelationSearchResultGrid.HeaderRow != null)
			{
				var chkAllSearched = (CheckBox)RelationSearchResultGrid.HeaderRow.FindControl("AllRelationsCheckBox");
				chkAllSearched.Checked = false;
			}
			SetTempRelationList(relationsList);
			PopulateRelationsList();
			RedirectToRelationsTab();
		}
		protected virtual void RelationSearchButtonClick(object sender, EventArgs e)
		{
			var searchCriteria = IoC.Resolve<IRelationListSearchCriteria>();
			searchCriteria.Title = RelationNameTextBox.Text;
			searchCriteria.TypeId = RelationTypeList.SelectedValue;
			RelationSearchResultGrid.PageIndex = 0;
			PopulateSearch(searchCriteria);
			AddRelationsButton.Visible = true;
		}

		protected void PopulateSearch(IRelationListSearchCriteria searchCriteria)
		{
			RelationListsObjectDataSource.SelectParameters.Clear();
			RelationListsObjectDataSource.SelectParameters.Add("maximumRows", RelationSearchResultGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			RelationListsObjectDataSource.SelectParameters.Add("startRowIndex", (RelationSearchResultGrid.PageIndex * RelationSearchResultGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			RelationListsObjectDataSource.SelectParameters.Add("title", searchCriteria.Title);
			RelationListsObjectDataSource.SelectParameters.Add("typeId", searchCriteria.TypeId);
		}
		protected void RedirectToRelationsTab()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), "Relation",
												"$(document).ready(function() { $('#tabs').tabs('select', 5); });", true);
		}

		#region Relation - Color Variants

		protected void ColorVariantProductsChanged(object sender, UserControls.Assortment.Events.ProductSearchEventArgs e)
		{
			var colorVariantRelationList = State.ColorVariantRelationList;
			var colorVariants = e.Products;
			var usedProducts = new Dictionary<int, IProduct>();

			foreach (var colorVariant in colorVariants)
			{
				var relationListCollection = RelationListSecureService.GetAllByProductAndType(colorVariant.Id, RelationListTypeConstant.ColorVariant.ToString());
				if (relationListCollection == null || relationListCollection.Count <= 0) continue;

				var relationList = relationListCollection[0];
				if (colorVariantRelationList != null && relationList.Id == colorVariantRelationList.Id) continue;

				usedProducts.Add(colorVariant.Id, colorVariant);
			}

			if (usedProducts.Count > 0)
			{
				StringBuilder sb = new StringBuilder();
				sb.AppendLine("WARNING! Following items are color variants for some other products:<br />");
				foreach (var product in usedProducts.Values)
				{
					sb.AppendLine(" - " + product.ErpId + "<br />");
				}
				ColorVariantsWarning.Text = sb.ToString();
				ColorVariantsWarning.Visible = true;
			}
			else
			{
				ColorVariantsWarning.Text = string.Empty;
				ColorVariantsWarning.Visible = false;
			}
			ColorVariantsWarningUpdatePanel.Update();
		}

		protected void PopulateColorVariants(IProduct product)
		{
			List<IProduct> colorVariants = null;
			if (ColorVariantRelationList != null)
			{
				colorVariants = ProductSecureService.GetAllByRelationList(ChannelHelper.CurrentChannel.Id, ColorVariantRelationList.Id).Where(p => p.Id != product.Id).ToList();
			}

			ColorVariantProducts.Products = colorVariants != null 
				? new Collection<IProduct>(colorVariants)
				: new Collection<IProduct>();
			ColorVariantProducts.DeniedProducts = new Collection<IProduct> {product};

			State.ColorVariantRelationList = ColorVariantRelationList;
		}

		protected void ManageColorVariants(IProduct product)
		{
			var stateColorVariantRelationList = State.ColorVariantRelationList;

			// Get color variant relation list from all color variant products.
			var colorVariants = ColorVariantProducts.Products;
			foreach (var colorVariant in colorVariants)
			{
				var relationListCollection = RelationListSecureService.GetAllByProductAndType(colorVariant.Id, RelationListTypeConstant.ColorVariant.ToString());
				if (relationListCollection == null || relationListCollection.Count <= 0) continue;

				var relationList = relationListCollection[0];
				if (stateColorVariantRelationList != null && relationList.Id == stateColorVariantRelationList.Id) continue;

				var relationListProduct = ProductSecureService.GetAllUsingRelationList(ChannelHelper.CurrentChannel.Id, relationList.Id).ToDictionary(i => i.Id, i => i);

				// Remove current color variant products from their color variant relation lists.
				foreach (var tempColorVariant in colorVariants)
				{
					relationListProduct.Remove(tempColorVariant.Id);
				}

				if (relationListProduct.Count <= 1)
				{
					RelationListSecureService.Delete(SignInHelper.SignedInSystemUser, relationList.Id);
				}
				else
				{
					var products = new Collection<IProduct>(relationListProduct.Values.ToList());
					RelationListSecureService.Save(SignInHelper.SignedInSystemUser, relationList, products, products);
				}
			}

			// Delete/Save/Update current color variant relation list.
			if (colorVariants.Count <= 0)
			{
				if (stateColorVariantRelationList != null)
				{
					RelationListSecureService.Delete(SignInHelper.SignedInSystemUser, stateColorVariantRelationList.Id);
				}
			}
			else
			{
				IRelationList colorVariantRelationList;
				if (stateColorVariantRelationList == null)
				{
					colorVariantRelationList = RelationListSecureService.Create();
					colorVariantRelationList.Title = Guid.NewGuid().ToString();
					colorVariantRelationList.RelationListTypeId = (int)RelationListTypeConstant.ColorVariant;
				}
				else
				{
					colorVariantRelationList = stateColorVariantRelationList;
				}

				colorVariants.Add(product);
				RelationListSecureService.Save(SignInHelper.SignedInSystemUser, colorVariantRelationList, colorVariants, colorVariants);

				PopulateColorVariants(product);
			}
		}

		#endregion

		#region Stores

		private Collection<IStoreStatus> _storeStatuses;
		private Collection<IStoreProduct> _storeProducts;

		protected virtual void PopulateStores()
		{
			StoresGrid.DataSource = IoC.Resolve<IStoreSecureService>().GetAll(1, int.MaxValue, null, null);
			StoresGrid.DataBind();
		}

		protected virtual void OnStoresRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var store = row.DataItem as IStore;
			if (store == null) return;

			var storeProduct = GetStoreProduct(store.Id);

			var quantityBox = row.FindControl("StoreProductQuantityBox") as TextBox;
			if (quantityBox != null)
			{
				quantityBox.Text = storeProduct.Quantity < 0
									? string.Empty
									: storeProduct.Quantity.ToString(CultureInfo.CurrentCulture);
			}

			var thresholdBox = row.FindControl("StoreProductThresholdBox") as TextBox;
			if (thresholdBox != null)
			{
				thresholdBox.Text = storeProduct.Threshold < 0
									? string.Empty
									: storeProduct.Threshold.ToString(CultureInfo.CurrentCulture);
			}
		}

		protected virtual string GetStoreStatus(int statusId)
		{
			var statuses = _storeStatuses ?? (_storeStatuses = IoC.Resolve<IStoreStatusSecureService>().GetAll());
			return statuses.FirstOrDefault(item => item.Id == statusId).Title;
		}

		protected virtual IStoreProduct GetStoreProduct(int storeId)
		{
			var storeProducts = _storeProducts
				?? (_storeProducts = IoC.Resolve<IStoreProductSecureService>().GetAllByProduct(GetId()));
			var storeProduct = storeProducts.FirstOrDefault(item => item.StoreId == storeId);
			return storeProduct ?? CreateStoreProduct(storeId, -1, -1);
		}

		protected virtual IStoreProduct CreateStoreProduct(int storeId, int quantity, int threshold)
		{
			var storeProduct = IoC.Resolve<IStoreProductSecureService>().Create();
			storeProduct.StoreId = storeId;
			storeProduct.ProductId = GetId();
			storeProduct.Quantity = quantity;
			storeProduct.Threshold = threshold;
			return storeProduct;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IEnumerable<IStoreProduct> GetStores()
		{
			var service = IoC.Resolve<IStoreProductSecureService>();
			var existedStoreProducts = service.GetAllByProduct(GetId());
			var actualStoreProducts =
				CollectStoreProducts().Where(i => i.Quantity >= 0
					|| existedStoreProducts.FirstOrDefault(e => e.StoreId == i.StoreId) != null);
			return actualStoreProducts;
		}

		protected virtual Collection<IStoreProduct> CollectStoreProducts()
		{
			var storeProducts = new Collection<IStoreProduct>();
			foreach (GridViewRow row in StoresGrid.Rows)
			{
				int storeId;
				if (!int.TryParse(((HiddenField)row.FindControl("IdHiddenField")).Value, out storeId))
					continue;

				int quantity;
				if (!int.TryParse(((TextBox)row.FindControl("StoreProductQuantityBox")).Text, out quantity))
					quantity = -1;

				int threshold;
				if (!int.TryParse(((TextBox)row.FindControl("StoreProductThresholdBox")).Text, out threshold))
					threshold = 0;

				storeProducts.Add(CreateStoreProduct(storeId, quantity, threshold));
			}
			return storeProducts;
		}

		#endregion

		#region SETTINGS TAB

		private void ValidateSettings(IEnumerable<IProductUrl> productUrls)
		{
			if (!IsValidDate(NewFromBox))
				SystemMessageContainer.Add(Resources.LekmerMessage.ProductEdit_NewFromIncorrect);

			if (!IsValidDate(NewToBox))
				SystemMessageContainer.Add(Resources.LekmerMessage.ProductEdit_NewToIncorrect);

			if (!IsValidDateInterval(NewFromBox, NewToBox))
				SystemMessageContainer.Add(Resources.LekmerMessage.ProductEdit_NewIntervalIncorrect);

			if (!IsValidDate(ExpectedBox))
				SystemMessageContainer.Add(Resources.LekmerMessage.ProductEdit_ExpectedBackInStockIncorrect);

			if (!IsValidDate(CreatedDateBox))
				SystemMessageContainer.Add(Resources.LekmerMessage.ProductEdit_CreatedDateIncorrect);

			var productUrlService = IoC.Resolve<IProductUrlSecureService>();
			var languages = IoC.Resolve<ILanguageSecureService>().GetAll();
			foreach (var productUrl in productUrls)
			{
				if (!productUrlService.Check(productUrl))
					SystemMessageContainer.Add(string.Format(CultureInfo.CurrentCulture,
						Resources.LekmerMessage.ProductEdit_UrlTitleExists,
						languages.First(i => i.Id == productUrl.LanguageId).Title));
			}
		}

		private void ApplySettings(IProductRecord product)
		{
			var lekmerProduct = (ILekmerProductRecord)product;

			int brandId;
			lekmerProduct.BrandId = int.TryParse(BrandList.SelectedValue, out brandId) ? (int?)brandId : null;

			lekmerProduct.AgeFromMonth = int.Parse(AgeFromList.SelectedValue, CultureInfo.CurrentCulture);
			lekmerProduct.AgeToMonth = int.Parse(AgeToList.SelectedValue, CultureInfo.CurrentCulture);

			lekmerProduct.Measurement = string.IsNullOrEmpty(MeasurementBox.Text) ? null : MeasurementBox.Text;

			lekmerProduct.BatteryTypeId = GetInteger(BatteryTypeList.SelectedValue);
			lekmerProduct.NumberOfBatteries = GetInteger(BatteryNumberBox.Text);
			lekmerProduct.IsBatteryIncluded = BatteryIncludedCheckBox.Checked;

			lekmerProduct.IsNewFrom = GetDate(NewFromBox.Text);
			lekmerProduct.IsNewTo = GetDate(NewToBox.Text);

			lekmerProduct.IsBookable = IsBookableCheckBox.Checked;

			lekmerProduct.ShowVariantRelations = ShowVariantsCheckBox.Checked;

			lekmerProduct.ExpectedBackInStock = GetDate(ExpectedBox.Text);

			lekmerProduct.CreatedDate = GetDate(CreatedDateBox.Text);

			lekmerProduct.MonitorThreshold = GetInteger(MonitorThresholdBox.Text);
			lekmerProduct.MaxQuantityPerOrder = GetInteger(MaxQuantityPerOrderBox.Text);
		}

		private void PopulateSettingsTab(ILekmerProductRecord product)
		{
			PopulateBrands(product);
			PopulateAges(product);
			PopulateMeasurement(product);
			PopulateBatteries(product);
			PopulateIcons(product);
			PopulateIsNew(product);
			PopulateIsBookable(product);
			PopulateShowVariantRelations(product);
			PopulateExpectedBackInStock(product);
			PopulateCreatedDate(product);
			PopulateUrlTitles(product);
			PopulateMonitorThreshold(product);
			PopulateMaxQuantityPerOrder(product);
			PopulateDeliveryTimes(product);
		}

		private void PopulateBrands(ILekmerProduct product)
		{
			var brandService = IoC.Resolve<IBrandSecureService>();
			DataBindList(BrandList, brandService.GetAll(), product.BrandId, true);
		}

		private void PopulateAges(ILekmerProductRecord product)
		{
			var ageService = IoC.Resolve<IAgeSecureService>();
			var ages = ageService.GetAll();
			DataBindList(AgeFromList, ages, product.AgeFromMonth, false);
			DataBindList(AgeToList, ages, product.AgeToMonth, false);
		}

		private void PopulateMeasurement(ILekmerProductRecord product)
		{
			MeasurementBox.Text = product.Measurement ?? string.Empty;
			MeasurementTranslator.DefaultValueControlClientId = MeasurementBox.ClientID;
			MeasurementTranslator.BusinessObjectId = product.Id;
			var translationService = IoC.Resolve<ProductTranslationGenericSecureService<ProductMeasurementTranslationRepository>>();
			MeasurementTranslator.DataSource = translationService.GetAllByProduct(product.Id);
			MeasurementTranslator.DataBind();
		}

		private void PopulateBatteries(ILekmerProductRecord product)
		{
			var batteryTypeService = IoC.Resolve<IBatteryTypeSecureService>();
			DataBindList(BatteryTypeList, batteryTypeService.GetAll(), product.BatteryTypeId, true);
			BatteryNumberBox.Text = product.NumberOfBatteries.HasValue
				? product.NumberOfBatteries.Value.ToString(CultureInfo.CurrentCulture)
				: string.Empty;
			BatteryIncludedCheckBox.Checked = product.IsBatteryIncluded;
		}

		private void PopulateIcons(ILekmerProductRecord product)
		{
			var iconService = IoC.Resolve<IIconSecureService>();

			var icons = iconService.GetAllByCategoryAndBrand(product.CategoryId, product.BrandId);

			IconRepeater.DataSource = iconService.GetAll();
			IconRepeater.DataBind();
			foreach (RepeaterItem item in IconRepeater.Items)
			{
				var iconId = int.Parse(((HiddenField)item.FindControl("IconIdHidden")).Value, CultureInfo.CurrentCulture);
				var iconCheck = (CheckBox)item.FindControl("IconCheckBox");
				iconCheck.Checked = product.Icons.Any(i => i.Id == iconId);
				iconCheck.Enabled = !(icons.Any(i => i.Id == iconId));
			}
		}

		private void PopulateIsNew(ILekmerProduct product)
		{
			NewFromBox.Text = ConvertDateToString(product.IsNewFrom);
			NewToBox.Text = ConvertDateToString(product.IsNewTo);
		}

		private void PopulateIsBookable(ILekmerProduct product)
		{
			IsBookableCheckBox.Checked = product.IsBookable;
		}

		private void PopulateShowVariantRelations(ILekmerProduct product)
		{
			ShowVariantsCheckBox.Checked = product.ShowVariantRelations;
		}

		private void PopulateExpectedBackInStock(ILekmerProductRecord product)
		{
			ExpectedBox.Text = ConvertDateToString(product.ExpectedBackInStock);
		}

		private void PopulateCreatedDate(ILekmerProduct product)
		{
			CreatedDateBox.Text = ConvertDateToString(product.CreatedDate);
		}

		private void PopulateMonitorThreshold(ILekmerProduct product)
		{
			MonitorThresholdBox.Text = product.MonitorThreshold.HasValue
				? product.MonitorThreshold.Value.ToString(CultureInfo.CurrentCulture)
				: string.Empty;
		}

		private void PopulateMaxQuantityPerOrder(ILekmerProduct product)
		{
			MaxQuantityPerOrderBox.Text = product.MaxQuantityPerOrder.HasValue
				? product.MaxQuantityPerOrder.Value.ToString(CultureInfo.CurrentCulture)
				: string.Empty;
		}

		private void PopulateDeliveryTimes(ILekmerProductRecord product)
		{
			var service = IoC.Resolve<IDeliveryTimeSecureService>();
			var productRegistryDeliveryTimeList = product != null && product.DeliveryTimeId.HasValue
				? service.GetAllProductRegistryDeliveryTimeById(product.DeliveryTimeId.Value)
				: new Collection<IDeliveryTime>();

			var deliveryTime = productRegistryDeliveryTimeList.FirstOrDefault(dt => dt.ChannelId == ChannelHelper.CurrentChannel.Id);
			var from = string.Empty;
			var to = string.Empty;
			var format = string.Empty;
			if (deliveryTime != null)
			{
				from = deliveryTime.FromDays.HasValue ? deliveryTime.FromDays.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				to = deliveryTime.ToDays.HasValue ? deliveryTime.ToDays.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				format = deliveryTime.Format ?? string.Empty;
			}

			DeliveryTimeFromBox.Text = from;
			DeliveryTimeToBox.Text = to;
			DeliveryTimeFormatBox.Text = format;

			DeliveryTimeConfigurator.DefaultFromValueControlClientId = DeliveryTimeFromBox.ClientID;
			DeliveryTimeConfigurator.DefaultToValueControlClientId = DeliveryTimeToBox.ClientID;
			DeliveryTimeConfigurator.DefaultFormatValueControlClientId = DeliveryTimeFormatBox.ClientID;
			DeliveryTimeConfigurator.DefaultRegistryId = deliveryTime != null && deliveryTime.ProductRegistryid.HasValue ? deliveryTime.ProductRegistryid.Value : -1;
			DeliveryTimeConfigurator.BusinessObjectId = (product != null && product.DeliveryTimeId.HasValue) ? product.DeliveryTimeId.Value : 0;
			DeliveryTimeConfigurator.DataSource = productRegistryDeliveryTimeList;
			DeliveryTimeConfigurator.DataBind();
		}

		private Collection<IProductUrl> _productUrls;

		private void PopulateUrlTitles(IProduct product)
		{
			_productUrls = IoC.Resolve<IProductUrlSecureService>().GetAllByProduct(product.Id);
			var languages = IoC.Resolve<ILanguageSecureService>().GetAll();
			ProductUrlRepeater.DataSource = languages;
			ProductUrlRepeater.DataBind();
		}

		protected string GetUrlTitle(object languageId)
		{
			var productUrl = _productUrls.FirstOrDefault(i => i.LanguageId == System.Convert.ToInt32(languageId, CultureInfo.CurrentCulture));
			return productUrl != null ? productUrl.UrlTitle : string.Empty;
		}

		private IEnumerable<IProductUrl> CollectProductUrls()
		{
			var productUrlService = IoC.Resolve<IProductUrlSecureService>();
			var productUrls = new Collection<IProductUrl>();
			foreach (RepeaterItem item in ProductUrlRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem) continue;
				var urlTitle = ((TextBox)item.FindControl("UrlTitleBox")).Text;
				if (string.IsNullOrEmpty(urlTitle)) continue;
				var languageId = int.Parse(((HiddenField)item.FindControl("IdHidden")).Value, CultureInfo.CurrentCulture);
				var productUrl = productUrlService.Create(GetId(), languageId, urlTitle);
				productUrls.Add(productUrl);
			}
			return productUrls;
		}

		private static void DataBindList<T>(ListControl list, Collection<T> dataSource, int? selectedValue, bool addEmptyItem)
		{
			list.DataSource = dataSource;
			list.DataBind();

			if (addEmptyItem)
				list.Items.Insert(0, new ListItem(string.Empty, string.Empty));

			if (!selectedValue.HasValue) return;
			var selectedItem = list.Items.FindByValue(selectedValue.Value.ToString(CultureInfo.CurrentCulture));
			if (selectedItem != null)
				selectedItem.Selected = true;
		}

		private static string ConvertDateToString(DateTime? dateTime)
		{
			return dateTime.HasValue
				? dateTime.Value.ToString(CultureInfo.CurrentCulture)
				: string.Empty;
		}

		private static bool IsValidDate(ITextControl dateBox)
		{
			var dateTimeString = dateBox.Text.Trim();
			DateTime date;
			return string.IsNullOrEmpty(dateTimeString) || DateTime.TryParse(dateTimeString, out date);
		}

		private static bool IsValidDateInterval(ITextControl dateFromBox, ITextControl dateToBox)
		{
			DateTime startDate;
			DateTime endDate;
			if (DateTime.TryParse(dateFromBox.Text.Trim(), out startDate)
				&& DateTime.TryParse(dateToBox.Text.Trim(), out endDate)
				&& endDate < startDate) return false;
			return true;
		}

		private static DateTime? GetDate(string dateValue)
		{
			return string.IsNullOrEmpty(dateValue)
				? null
				: (DateTime?)DateTime.Parse(dateValue, CultureInfo.CurrentCulture);
		}

		private static int? GetInteger(string selectedValue)
		{
			return string.IsNullOrEmpty(selectedValue)
				? null
				: (int?)int.Parse(selectedValue, CultureInfo.CurrentCulture);
		}

		private Collection<int> GetSelectedIcons()
		{
			var ids = new Collection<int>();
			foreach (RepeaterItem item in IconRepeater.Items)
			{
				var iconId = int.Parse(((HiddenField)item.FindControl("IconIdHidden")).Value, CultureInfo.CurrentCulture);
				var iconCheck = (CheckBox)item.FindControl("IconCheckBox");
				if (!iconCheck.Checked || !iconCheck.Enabled) continue;
				ids.Add(iconId);
			}
			return ids;
		}

		#endregion SETTINGS TAB

		#region SETTINGS TAB - RATINGS

		protected virtual void OnRatingGroupsAdd(object sender, UserControls.Assortment.Events.RatingGroupSelectEventArgs e)
		{
			foreach (var group in e.RatingGroups)
			{
				var clone = group;
				if (State.ProductRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == clone.Id) == null)
				{
					var ratingGroupProduct = IoC.Resolve<IRatingGroupProduct>();
					ratingGroupProduct.ProductId = GetId();
					ratingGroupProduct.RatingGroupId = group.Id;

					var productRatingGroupState = new ProductRatingGroupState(@group, ratingGroupProduct);
					State.ProductRatingGroups.Add(productRatingGroupState);
				}
			}
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = RatingGroupsGrid.GetSelectedIds();
			foreach (var id in ids)
			{
				var cloneId = id;
				var group = State.ProductRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == cloneId);
				if (group != null)
				{
					State.ProductRatingGroups.Remove(group);
				}
			}
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnRatingGroupsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGroupsGrid.PageIndex = e.NewPageIndex;
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteGroup")
			{
				var group = State.ProductRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				if (group != null)
				{
					State.ProductRatingGroups.Remove(group);
					PopulateRatingGroupsGrid();
				}
			}
		}


		protected virtual void PopulateRatingGroups()
		{
			var ratingGroupProductSecureService = IoC.Resolve<IRatingGroupProductSecureService>();
			var ratingGroupSecureService = IoC.Resolve<IRatingGroupSecureService>();

			Collection<IRatingGroupProduct> ratingGroupProducts = ratingGroupProductSecureService.GetAllByProduct(GetId());

			EnsureState();

			foreach (var ratingGroupCategory in ratingGroupProducts)
			{
				IRatingGroup ratingGroup = ratingGroupSecureService.GetById(ratingGroupCategory.RatingGroupId);

				State.ProductRatingGroups.Add(new ProductRatingGroupState(ratingGroup, ratingGroupCategory));
			}

			PopulateRatingGroupsGrid();
		}

		protected virtual void PopulateRatingGroupsGrid()
		{
			RatingGroupsGrid.DataSource = State.ProductRatingGroups;
			RatingGroupsGrid.DataBind();
			RatingGroupsUpdatePanel.Update();
		}

		protected virtual void SaveProductRatingGroups()
		{
			var service = IoC.Resolve<IRatingGroupProductSecureService>();

			var ratingGroupProducts = new Collection<IRatingGroupProduct>();

			foreach (var productRatingGroupState in State.ProductRatingGroups)
			{
				ratingGroupProducts.Add(productRatingGroupState.RatingGroupProduct);
			}

			service.SaveAll(SignInHelper.SignedInSystemUser, GetId(), ratingGroupProducts);
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('"
					+ selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
					cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual string GetRatingGropEditUrl(object groupId)
		{
			return LekmerPathHelper.RatingGroup.GetEditUrlForProductEdit(System.Convert.ToInt32(groupId, CultureInfo.CurrentCulture), GetId());
		}

		#endregion

		#region TAGS TAB

		protected void EditTagGroupsDefaultSettingsButtonClick(object sender, EventArgs e)
		{
			IProductRecord product = ProductSecureService.GetRecordById(ChannelHelper.CurrentChannel.Id, GetId());

			if (product != null)
			{
				Response.Redirect(LekmerPathHelper.Category.GetEditUrlForProductEdit(product.CategoryId, product.Id));
			}
		}

		protected void PopulateTagRepeaterItems(int categoryId)
		{
			PopulateTagsTab(categoryId);

			foreach (RepeaterItem item in TagRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem) continue;
				var tagList = (TagList)item.FindControl("TagList");
				tagList.BindData();
			}
		}

		protected void TagRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

			var tagGroup = e.Item.DataItem as ITagGroup;
			if (tagGroup == null) return;

			var tagList = (TagList)e.Item.FindControl("TagList");
			tagList.TagGroup = tagGroup;
			tagList.ProductId = GetId();
			tagList.DefaultTagGroups = _categoryTagGroups;
		}

		private void PopulateTagsTab(int categoryId)
		{
			_categoryTagGroups = IoC.Resolve<ICategoryTagGroupSecureService>().GetAllByCategory(categoryId);

			var defaultTagGroups = new Collection<ITagGroup>();
			var notDefaultTagGroups = new Collection<ITagGroup>();
			
			var allTagGroups = IoC.Resolve<ITagGroupSecureService>().GetAll().OrderBy(t => t.Title);
			foreach (var tagGroup in allTagGroups)
			{
				if (_categoryTagGroups.FirstOrDefault(tg => tg.TagGroupId == tagGroup.Id) != null)
				{
					defaultTagGroups.Add(tagGroup);
				}
				else
				{
					notDefaultTagGroups.Add(tagGroup);
				}
			}

			var tagGroups = new Collection<ITagGroup>();
			tagGroups.AddRange(defaultTagGroups);
			tagGroups.AddRange(notDefaultTagGroups);

			TagRepeater.DataSource = tagGroups;
			TagRepeater.DataBind();
		}

		private void GetTags(out Collection<int> initialTags, out Collection<int> selectedTags)
		{
			var selectedProductTags = new List<int>();
			var initialProductTags = new List<int>();
			foreach (RepeaterItem item in TagRepeater.Items)
			{
				if (item.ItemType != ListItemType.Item && item.ItemType != ListItemType.AlternatingItem) continue;
				var tagList = (TagList)item.FindControl("TagList");
				selectedProductTags.AddRange(tagList.SelectedTags.Select(tag => tag.Id));
				initialProductTags.AddRange(tagList.InitialProductTags.Select(tag => tag.Id));
			}
			selectedTags = new Collection<int>(selectedProductTags);
			initialTags = new Collection<int>(initialProductTags);
		}

		#endregion TAGS TAB

		#region SIZES TAB

		private void PopulateSizesTab(ILekmerProductRecord product)
		{
			PopulateSizes();
			PopulateDeviations(product);
		}

		private void PopulateSizes()
		{
			var service = IoC.Resolve<IProductSizeSecureService>();
			SizeGrid.DataSource = service.GetAllByProduct(GetId());
			SizeGrid.DataBind();
		}

		private void PopulateDeviations(ILekmerProductRecord product)
		{
			var service = IoC.Resolve<ISizeDeviationSecureService>();
			SizeDeviationList.DataSource = service.GetAll();
			SizeDeviationList.DataBind();
			SizeDeviationList.Items.Insert(0, new ListItem { Text = Resources.Lekmer.Literal_Normal, Value = string.Empty, Selected = true });
			if (!product.SizeDeviationId.HasValue) return;
			var selectedItem = SizeDeviationList.Items.FindByValue(product.SizeDeviationId.Value.ToString(CultureInfo.CurrentCulture));
			if (selectedItem != null) selectedItem.Selected = true;
		}

		private void ApplySizes(IProductRecord product)
		{
			var lekmerProduct = (ILekmerProductRecord)product;
			int sizeDeviationId;
			lekmerProduct.SizeDeviationId = int.TryParse(SizeDeviationList.SelectedValue, out sizeDeviationId)
				? (int?)sizeDeviationId
				: null;
		}

		private IEnumerable<IProductSize> CollectProductSizes()
		{
			var productSizes = IoC.Resolve<IProductSizeSecureService>().GetAllByProduct(GetId());

			foreach (GridViewRow row in SizeGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
				{
					continue;
				}

				var sizeId = int.Parse(((HiddenField)row.FindControl("IdHidden")).Value, CultureInfo.CurrentCulture);
				var productSize = productSizes.FirstOrDefault(i => i.SizeInfo.Id == sizeId);
				if (productSize == null) continue;

				int millimeter;
				productSize.OverrideMillimeter = int.TryParse(((TextBox)row.FindControl("MillimeterTextBox")).Text, out millimeter)
					? (int?)millimeter
					: null;

				int deviation;
				productSize.MillimeterDeviation = int.TryParse(((TextBox)row.FindControl("DeviationBox")).Text, out deviation)
					? (int?)deviation
					: null;
				
				decimal overrideEu;
				productSize.OverrideEu = decimal.TryParse(((TextBox)row.FindControl("OverrideEuBox")).Text, out overrideEu)
					? (decimal?)overrideEu
					: null;
			}

			return productSizes;
		}

		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		protected string FormatSize(object size)
		{
			var decSize = System.Convert.ToDecimal(size, CultureInfo.CurrentCulture);
			var intSize = decimal.ToInt32(decSize);
			return decSize - intSize == 0
				? intSize.ToString(CultureInfo.CurrentCulture)
				: decSize.ToString(CultureInfo.CurrentCulture);
		}

		#endregion SIZES TAB

		#region PRICING TAB - RECOMMENDED PRICES

		protected class RecommendedPriceData
		{
			public int ChannelId { get; set; }
			public string ChannelTitle { get; set; }
			public string CurrencyTitle { get; set; }
			public decimal? Price { get; set; }
	}

		private void PopulateRecommendedPrices(ILekmerProductRecord product)
		{
			var service = IoC.Resolve<IRecommendedPriceSecureService>();
			var recommendedPrices = service.GetAllByProduct(product.Id);
			var dataSource = Channels.Select(c => new RecommendedPriceData
			                                      	{
			                                      		ChannelId = c.Id,
			                                      		ChannelTitle = c.Title,
			                                      		CurrencyTitle = c.Currency.Iso,
			                                      		Price = recommendedPrices.Any(p => p.ChannelId == c.Id)
			                                      		        	? recommendedPrices.Single(p => p.ChannelId == c.Id).Price
			                                      		        	: (decimal?)null
			                                      	});
			RecommendedPriceGrid.DataSource = dataSource;
			RecommendedPriceGrid.DataBind();
		}

		private Collection<IRecommendedPrice> GetRecommendedPrices(int productId)
		{
			var recommendedPrices = new Collection<IRecommendedPrice>();
			foreach (GridViewRow row in RecommendedPriceGrid.Rows)
			{
				int channelId;
				if (!int.TryParse(((HiddenField)row.FindControl("ChannelIdHidden")).Value, out channelId))
					continue;

				decimal price;
				if (!decimal.TryParse(((TextBox)row.FindControl("PriceBox")).Text, out price))
					continue;

				if (price < 0)
					continue;

				var recommendedPrice = IoC.Resolve<IRecommendedPrice>();
				recommendedPrice.ProductId = productId;
				recommendedPrice.ChannelId = channelId;
				recommendedPrice.Price = price;
				recommendedPrices.Add(recommendedPrice);
			}
			return recommendedPrices;
		}

		#endregion PRICING TAB - RECOMMENDED PRICES

		protected virtual void UpdateRelatedPackages(IProductRecord product)
		{
			if (product.IsProduct() && product.ProductStatusId == (int)Lekmer.Product.ProductStatus.Offline)
			{
				var packageSecureService = IoC.Resolve<IPackageSecureService>();
				packageSecureService.SetOfflineByIncludeProduct(SignInHelper.SignedInSystemUser, product.Id);
			}
		}

		#region Delivery Time

		protected virtual bool ManageDeliveryTime(ILekmerProductRecord product, out int oldDeliveryTimeId)
		{
			bool isNeedDeleteDeliveryTime = false;
			oldDeliveryTimeId = 0;

			int? deliveryTimeId = product.DeliveryTimeId;
			var deliveryTimeList = DeliveryTimeConfigurator.GetDeliveryTimeList();
			bool isRegistryDeliveryTimeExist = deliveryTimeList.FirstOrDefault(dt => dt.FromDays != null || dt.ToDays != null || !string.IsNullOrEmpty(dt.Format)) != null;
			if (!deliveryTimeId.HasValue)
			{
				if (isRegistryDeliveryTimeExist)
				{
					deliveryTimeId = DeliveryTimeSecureService.Insert();
					foreach (var deliveryTime in deliveryTimeList)
					{
						DeliveryTimeSecureService.SaveProductRegistryDeliveryTime(deliveryTimeId.Value, deliveryTime.FromDays, deliveryTime.ToDays, deliveryTime.Format, deliveryTime.ProductRegistryid);
					}
					product.DeliveryTimeId = deliveryTimeId;
				}
			}
			else
			{
				if (isRegistryDeliveryTimeExist)
				{
					foreach (var deliveryTime in deliveryTimeList)
					{
						DeliveryTimeSecureService.SaveProductRegistryDeliveryTime(deliveryTimeId.Value, deliveryTime.FromDays, deliveryTime.ToDays, deliveryTime.Format, deliveryTime.ProductRegistryid);
					}
				}
				else
				{
					oldDeliveryTimeId = deliveryTimeId.Value;
					product.DeliveryTimeId = null;
					isNeedDeleteDeliveryTime = true;
				}
			}

			return isNeedDeleteDeliveryTime;
		}

		#endregion
	}
}