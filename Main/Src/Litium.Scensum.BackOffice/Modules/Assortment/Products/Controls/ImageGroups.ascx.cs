using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Media;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls;
using Litium.Scensum.Web.Controls.Common;
using Resources;
using Image = System.Web.UI.WebControls.Image;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls
{
    public partial class ImageGroup : StateUserControlController<ProductImageGroupsState>
    {
        private const int StepMove = 10;
        private const int ImageMoveUpStep = (int)(-1.5 * StepMove);
        private const int ImageMoveDownStep = (int)(1.5 * StepMove);

        public event EventHandler ImageUploaded;
        public event EventHandler<ImageSelectEventArgs> DefaltImageSet;

        public int ProductId
        {
            get { return State.ProductId; }
            set
            {
                EnsureState();
                State.ProductId = value;
            }
        }

        private Collection<IProductImageGroupFull> ProductImageGroups
        {
            get { return State.ProductImageGroups; }
            set
            {
                EnsureState();
                State.ProductImageGroups = value;
            }
        }

        public string GroupTitle { get; set; }

        protected override void SetEventHandlers()
        {
            ImageGroupsRepeater.ItemDataBound += ImageGroupsItemDataBound;
            ImageGroupsRepeater.ItemCommand += ImageGroupsItemCommand;

            ImageSelectControl.Selected += ImageSelected;
            ImageSelectControl.FileUploaded += ImageSelectFileUploated;

            SaveChangeGroupButton.Click += SaveChangeGroupButtonClick;
            AddTumbnailImage.Click += AddTumbnailImage_Click;
            NodeSelectorControl.SelectedFolderChangedEvent += NodeSelectorControlSelectedFolderChangedEvent;
            NodeSelectorControl.NodeCommand += OnNodeSelectorCommand;
            MovieSaveButton.Click += MovieSaveButton_Click;
        }

        protected void MovieSaveButton_Click(object sender, EventArgs e)
        {
            int? folderId = NodeSelectorControl.SelectedNodeId;
            if (!folderId.HasValue)
            {
                MovieMessageContainer.Add(MediaMessage.MediaFolderIsNotSelected);
                MoviePopup.Show();
                return;
            } if (ImageUploaded != null)
            {
                ImageUploaded(this, EventArgs.Empty);
            }

            int groupId = int.Parse(GroupIdValueHiddenField.Value, CultureInfo.CurrentCulture);
            IProductImageGroupFull imageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == groupId);
            var imageSecureService = (ILekmerImageSecureService)IoC.Resolve<IImageSecureService>();
            var movie = State.Movie;
            if (movie == null)
            {
                movie = (ILekmerImage)imageSecureService.Create();
                IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension("jpg");
                movie.MediaFormatId = mediaFormat.Id;
                movie.MediaFolderId = folderId.Value;
                movie.Id = 0 - imageGroup.ProductImages.Count;
            }

            movie.Link = LinkTextBox.Text.Trim();
            movie.Parameter = ParameterTextBox.Text.Trim();
            movie.TumbnailImageUrl = TumbnailImageUrlTextBox.Text.Trim();
            movie.Title = "movie";
            movie.FileName = "movie";

            //imageSecureService.SaveMovie(SignInHelper.SignedInSystemUser, movie);

            var productImageSecureService = IoC.Resolve<IProductImageSecureService>();

            IProductImage productImage = productImageSecureService.Create(movie, ProductId, groupId);

            if (imageGroup.ProductImages.Count > 0)
            {
                productImage.Ordinal = imageGroup.ProductImages.Max(item => item.Ordinal) + StepMove;
            }
            MoviePopup.Hide();
            imageGroup.ProductImages.Add(productImage);
            SetActiveGroups(GetRepeaterItemByAreaId(groupId));
            AllGridsBind();
            ImagesUpdatePanel.Update();
        }

        protected void AddTumbnailImage_Click(object sender, EventArgs e)
        {
            ImageAddPopup.Show();
        }

        protected override void PopulateControl()
        {
            var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
            ProductImageGroups = productImageGroupSecureService.GetAllFullByProduct(ProductId);

            ImageGroupsRepeater.DataSource = ProductImageGroups;
            ImageGroupsRepeater.DataBind();

            OtherGroupRepeater.DataSource = ProductImageGroups;
            OtherGroupRepeater.DataBind();
            PopulateTree(null);
        }

        protected void EnsureState()
        {
            if (State == null)
            {
                State = new ProductImageGroupsState();
            }
        }

        private void AllGridsBind()
        {
            foreach (RepeaterItem repeaterItem in ImageGroupsRepeater.Items)
            {
                var groupIdHiddenField = (HiddenField)repeaterItem.FindControl("ProductImageGroupIdValueHiddenField");
                int groupId = int.Parse(groupIdHiddenField.Value, CultureInfo.CurrentCulture);
                IProductImageGroupFull imageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == groupId);
                DataBindImages(repeaterItem, imageGroup);
            }
        }

        private void ImageGroupsItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var repeaterItem = e.Item;
            var imageGroup = (IProductImageGroupFull)repeaterItem.DataItem;

            DataBindGroup(repeaterItem);
            if (imageGroup.ProductImages.Count > 0)
            {
                DataBindImages(repeaterItem, imageGroup);
            }
        }

        private static void DataBindGroup(RepeaterItem repeaterItem)
        {
            var areaCollapsiblePanel = (ImageGroupCollapsiblePanel)repeaterItem.FindControl("GroupCollapsiblePanel");
            string divBlockGvId = repeaterItem.FindControl("ImageGroupGridViewDiv").ClientID;
            areaCollapsiblePanel.ControlToExpand = divBlockGvId;
        }

        private static string PopulateFoldersPath(IImage image)
        {
            var mediaFolderService = IoC.Resolve<IMediaFolderSecureService>();
            IMediaFolder folder = mediaFolderService.GetById(image.MediaFolderId);
            var foldersPath = new StringBuilder(folder.Title);

            while (folder.MediaFolderParentId.HasValue)
            {
                folder = mediaFolderService.GetById(folder.MediaFolderParentId.Value);
                foldersPath.Insert(0, @"\");
                foldersPath.Insert(0, folder.Title);
            }
            foldersPath.Insert(0, @"[Root]\");
            return foldersPath.ToString();
        }

        private static void DataBindImages(RepeaterItem repeaterItem, IProductImageGroupFull imageGroup)
        {
            var gridView = (GridView)repeaterItem.FindControl("ImageGroupGridViewDiv").FindControl("ImageGroupGrid");
            gridView.DataSource = Sort(imageGroup.ProductImages, StepMove);
            gridView.DataBind();
        }

        protected virtual void ImageGroupGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowType != DataControlRowType.DataRow) return;
            var productImage = (IProductImage)row.DataItem;
            var titleLiteral = (LiteralEncoded)row.FindControl("TitleLiteral");

            var imagePathLiteral = (LiteralEncoded)row.FindControl("ImagePathLiteral");
            var archiveImageDimensionsLabel = (Label)row.FindControl("ArchiveImageDimensionsLabel");
            var archiveImageFileTypeLAbel = (Label)row.FindControl("ArchiveImageFileTypeLAbel");
            var imgArchiveImage = (Image)row.FindControl("ArchiveImage");

            if (((ILekmerImage)productImage.Image).IsLink)
            {
                var linkLabel = (Label)row.FindControl("LinkLabel");
                linkLabel.Visible = true;
                var parameterLabel = (Label)row.FindControl("ParameterLabel");
                parameterLabel.Visible = true;
                var imageContextMenu = (ContextMenu)row.FindControl("ImageContextMenu");
                // imageContextMenu.Visible = false;


                titleLiteral.Text = ((ILekmerImage)productImage.Image).Link;
                imagePathLiteral.Text = ((ILekmerImage)productImage.Image).Parameter;
                if (((ILekmerImage)productImage.Image).HasImage)
                {
                    archiveImageDimensionsLabel.Text = productImage.Image.Width + " x " + productImage.Image.Height;
                    archiveImageFileTypeLAbel.Text = productImage.Image.FormatExtension;
                }
                else
                {
                    archiveImageDimensionsLabel.Visible = false;
                    archiveImageFileTypeLAbel.Visible = false;
                }
                if (((ILekmerImage)productImage.Image).HasImage)
                {
                    imgArchiveImage.ImageUrl =
                 ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(productImage.Image.Id, productImage.Image.FormatExtension));

                }
                else if (!string.IsNullOrEmpty(((ILekmerImage)productImage.Image).TumbnailImageUrl))
                {
                    imgArchiveImage.ImageUrl = ((ILekmerImage)productImage.Image).TumbnailImageUrl;
                    imgArchiveImage.AlternateText = ((ILekmerImage)productImage.Image).TumbnailImageUrl;
                }
                else
                {
                    imgArchiveImage.ImageUrl = "~/Media/Images/Assortment/defaultMovie.jpg";
                }
            }
            else
            {
                titleLiteral.Text = productImage.Image.Title;
                imagePathLiteral.Text = PopulateFoldersPath(productImage.Image);
                archiveImageFileTypeLAbel.Text = productImage.Image.FormatExtension;
                archiveImageDimensionsLabel.Text = productImage.Image.Width + " x " + productImage.Image.Height;
                imgArchiveImage.ImageUrl =
                    ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(productImage.Image.Id, productImage.Image.FormatExtension));

            }

            var ordinalTextBox = (TextBox)row.FindControl("OrdinalTextBox");
            ordinalTextBox.Text = productImage.Ordinal.ToString(CultureInfo.CurrentCulture);
        }

        private void ImageGroupsItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int groupId;

            if (!int.TryParse(e.CommandArgument.ToString(), out groupId))
            {
                groupId = 0;
            }

            switch (e.CommandName)
            {
                case "AddImagePopup":
                    GroupIdValueHiddenField.Value = groupId.ToString(CultureInfo.CurrentCulture);
                    IsItMovieHiddenField.Value = "false";
                    ImageAddPopup.Show();
                    break;
                case "AddMoviePopup":
                    TumbnailImage.ImageUrl = "~/Media/Images/Assortment/defaultMovie.jpg";
                    IsItMovieHiddenField.Value = "true";
                    State.Movie = null;
                    LinkTextBox.Text = string.Empty;
                    ParameterTextBox.Text = string.Empty;
                    TumbnailImageUrlTextBox.Text = string.Empty;
                    GroupIdValueHiddenField.Value = groupId.ToString(CultureInfo.CurrentCulture);
                    MovieUpdatePanel.Update();
                    MoviePopup.Show();
                    break;
            }
        }

        private static RepeaterItem FindParentRepeaterItem(Control control)
        {
            var parentControl = control.Parent;
            if (parentControl == null)
            {
                return null;
            }
            var repeaterItem = parentControl as RepeaterItem;
            if (repeaterItem != null)
            {
                return repeaterItem;
            }
            return FindParentRepeaterItem(parentControl);
        }

        private static void SetActiveGroups(Control itemToOpen)
        {
            var groupCollapsiblePanel = (ImageGroupCollapsiblePanel)itemToOpen.FindControl("GroupCollapsiblePanel");
            groupCollapsiblePanel.Open();
        }

        protected RepeaterItem GetRepeaterItemByAreaId(int groupId)
        {
            foreach (RepeaterItem item in ImageGroupsRepeater.Items)
            {
                var imagesGridHiddenField = (HiddenField)item.FindControl("ProductImageGroupIdValueHiddenField");
                int groupIdFromHiddenField = int.Parse(imagesGridHiddenField.Value, CultureInfo.CurrentCulture);
                if (groupId == groupIdFromHiddenField)
                {
                    return item;
                }
            }
            return null;
        }

        private IProductImageGroupFull GetImageGroupFromGrid(int itemId, int productImageGroupId)
        {
            if (!ValidateOrdinals())
            {
                SystemMessageContainer.Add(ProductMessage.ImageOrdinalsValidationFailed);
            }
            IProductImageGroupFull productImageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == productImageGroupId);
            var imagesGrid =
                (GridView)ImageGroupsRepeater.Items[itemId].FindControl("ImageGroupGridViewDiv").FindControl("ImageGroupGrid");

            foreach (GridViewRow row in imagesGrid.Rows)
            {
                int mediaId = int.Parse(((HiddenField)row.FindControl("MediaIdValueHiddenField")).Value, CultureInfo.CurrentCulture);
                IProductImage productImage = productImageGroup.ProductImages.FirstOrDefault(item => item.MediaId == mediaId);
                productImage.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
            }
            return productImageGroup;
        }

        protected bool ValidateOrdinals()
        {
            Page.Validate("vgOrdinals");
            return Page.IsValid;
        }

        protected virtual void ImageGroupGridRowCommand(object sender, CommandEventArgs e)
        {
            int itemId = FindParentRepeaterItem((GridView)sender).ItemIndex;
            var groupIdHiddenField =
                (HiddenField)ImageGroupsRepeater.Items[itemId].FindControl("ProductImageGroupIdValueHiddenField");
            int groupId = int.Parse(groupIdHiddenField.Value, CultureInfo.CurrentCulture);
            IProductImageGroupFull productImageGroup = GetImageGroupFromGrid(itemId, groupId);

            if (e.CommandName == "RefreshProductImageOrder")
            {
                AllGridsBind();
                return;
            }

            int mediaId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
            IProductImage image = productImageGroup.ProductImages.FirstOrDefault(item => item.MediaId == mediaId);

            switch (e.CommandName)
            {
                case "UpOrdinal":
                    MoveBlock(image, ImageMoveUpStep);
                    break;
                case "DownOrdinal":
                    MoveBlock(image, ImageMoveDownStep);
                    break;
                case "SetAsDefaultImage":
                    SetAsDefaultImage(image);
                    break;
                case "ChangeImageGroup":
                    ChangeImageGroup(groupId, mediaId);
                    break;
                case "DeleteImage":
                    productImageGroup.ProductImages.Remove(image);
                    break;
            }
            AllGridsBind();
        }

        private void ImageSelectFileUploated(object sender, EventArgs e)
        {
            ImageAddPopup.Show();
            ImageAddPopup.X = 227;
            if (ImageUploaded != null)
            {
                ImageUploaded(this, EventArgs.Empty);
            }
        }

        private void ImageSelected(object sender, ImageSelectEventArgs e)
        {
            if (ImageUploaded != null)
            {
                ImageUploaded(this, EventArgs.Empty);
            }

            var productImageSecureService = IoC.Resolve<IProductImageSecureService>();
            int groupId = int.Parse(GroupIdValueHiddenField.Value, CultureInfo.CurrentCulture);
            var validationResult = new ValidationResult();
            ValidateAddImage(e.Image.Id, groupId, validationResult);

            if (!validationResult.IsValid)
            {
                AddImageMessageContainer.AddRange(validationResult.Errors);
                if (ImageUploaded != null)
                {
                    ImageUploaded(this, EventArgs.Empty);
                }
                ImageAddPopup.Show();
                ImageAddPopup.X = 227;
                return;
            }

            bool isMovie = bool.Parse(IsItMovieHiddenField.Value);
            if (isMovie)
            {
                State.Movie = (ILekmerImage)IoC.Resolve<IImageSecureService>().GetById(e.Image.Id);
                TumbnailImage.ImageUrl =
                     ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(State.Movie.Id, State.Movie.FormatExtension));
                PopulateTree(State.Movie.MediaFolderId);
                State.Movie.HasImage = true;
                MoviePopup.Show();
                return;
            }

            IProductImage productImage = productImageSecureService.Create(e.Image, ProductId, groupId);
            IProductImageGroupFull imageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == groupId);
            if (imageGroup.ProductImages.Count > 0)
            {
                productImage.Ordinal = imageGroup.ProductImages.Max(item => item.Ordinal) + StepMove;
            }

            imageGroup.ProductImages.Add(productImage);
            SetActiveGroups(GetRepeaterItemByAreaId(groupId));
            AllGridsBind();
        }

        private void ChangeImageGroup(IProductImage productImage, int imageGrouId)
        {
            var productImageSecureService = IoC.Resolve<IProductImageSecureService>();
            IProductImage productImageCopy = productImageSecureService.Create(productImage.Image, productImage.ProductId,
                                                                              imageGrouId);
            IProductImageGroupFull imageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == imageGrouId);

            if (imageGroup.ProductImages != null)
            {
                if (imageGroup.ProductImages.Count > 0)
                {
                    productImageCopy.Ordinal = imageGroup.ProductImages.Max(item => item.Ordinal) + StepMove;
                }
                imageGroup.ProductImages.Add(productImageCopy);
                SetActiveGroups(GetRepeaterItemByAreaId(imageGrouId));
            }
        }

        private void SaveChangeGroupButtonClick(object sender, EventArgs e)
        {
            Collection<int> groupIdsMoveTo = GetImageGroupIdsMoveTo();
            int groupIdFrom = int.Parse(GroupIdValueHiddenField.Value, CultureInfo.CurrentCulture);
            int imageId = int.Parse(MediaIdValueHiddenField.Value, CultureInfo.CurrentCulture);
            var validationResult = new ValidationResult();
            foreach (var id in groupIdsMoveTo)
            {
                if (id != groupIdFrom)
                {
                    ValidateAddImage(imageId, id, validationResult);
                }
            }
            if (!validationResult.IsValid)
            {
                GroupChangeMessageContainer.AddRange(validationResult.Errors);
                ChangeImageGroupPopup.Show();
                return;
            }
            if (groupIdsMoveTo.Count > 0)
            {
                IProductImageGroupFull productImageGroupFrom = ProductImageGroups.FirstOrDefault(item => item.Id == groupIdFrom);
                var productImage = productImageGroupFrom.ProductImages.FirstOrDefault(item => item.MediaId == imageId);
                productImageGroupFrom.ProductImages.Remove(productImage);

                foreach (int groupIdTo in groupIdsMoveTo)
                {
                    ChangeImageGroup(productImage, groupIdTo);
                }

                ImagesUpdatePanel.Update();
                AllGridsBind();
            }
        }

        private static void MoveBlock(IProductImage image, int step)
        {
            image.Ordinal += step;
        }

        private void ChangeImageGroup(int groupId, int mediaId)
        {
            GroupIdValueHiddenField.Value = groupId.ToString(CultureInfo.CurrentCulture);
            MediaIdValueHiddenField.Value = mediaId.ToString(CultureInfo.CurrentCulture);
            ChangeImageGroupPopup.Show();
        }

        private void SetAsDefaultImage(IProductImage image)
        {
            if (DefaltImageSet != null)
            {
                DefaltImageSet(this, new ImageSelectEventArgs { Image = image.Image });
            }
        }

        private void ValidateAddImage(int imageId, int groupId, ValidationResult validationResult)
        {
            IProductImageGroupFull productImageGroup = ProductImageGroups.FirstOrDefault(item => item.Id == groupId);
            var productImage = productImageGroup.ProductImages.FirstOrDefault(item => item.MediaId == imageId);
            if (productImage != null)
            {
                validationResult.Errors.Add(string.Format(CultureInfo.CurrentCulture, "{0} group contain image {1}.",
                                                          productImageGroup.Title, productImage.Image.Title));
            }
        }

        private Collection<int> GetImageGroupIdsMoveTo()
        {
            var ids = new Collection<int>();
            foreach (RepeaterItem repeaterItem in OtherGroupRepeater.Items)
            {
                var imageGroupCheckBox = ((CheckBox)repeaterItem.FindControl("ImageGroupCheckBox"));
                if (imageGroupCheckBox.Checked)
                {
                    int groupIdTo = int.Parse(((HiddenField)repeaterItem.FindControl("ImageGroupIdHiddenField")).Value,
                                              CultureInfo.CurrentCulture);
                    ids.Add(groupIdTo);
                    imageGroupCheckBox.Checked = false;
                }
            }
            return ids;
        }

        private void SortProductImageGroup()
        {
            foreach (var imageGroup in ProductImageGroups)
            {
                imageGroup.ProductImages = Sort(imageGroup.ProductImages, 1);
            }
        }

        private static Collection<IProductImage> Sort(IEnumerable<IProductImage> list, int ordinalsStep)
        {
            var productImages = new List<IProductImage>(list);
            productImages.Sort(delegate(IProductImage image1, IProductImage image2)
            {
                if (image1.Ordinal < image2.Ordinal)
                    return -1;
                if (image1.Ordinal > image2.Ordinal)
                    return 1;
                return 0;
            });
            for (int i = 0; i < productImages.Count; i++)
            {
                productImages[i].Ordinal = (i + 1) * ordinalsStep;
            }
            return new Collection<IProductImage>(productImages);
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        public Collection<IProductImageGroupFull> GetProductImageGroups()
        {
            SortProductImageGroup();
            return ProductImageGroups;
        }
        protected virtual void NodeSelectorControlSelectedFolderChangedEvent(object sender, SelectedFolderChangedEventArgs e)
        {
            if (!e.SelectedFolderId.HasValue)
            {
                return;
            }
        }

        protected virtual void OnNodeSelectorCommand(object sender, CommandEventArgs e)
        {
            NodeSelectorControl.DataSource = GetFolders(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
            NodeSelectorControl.DataBind();
        }

        protected void PopulateTree(int? folderId)
        {
            var source = GetFolders(folderId);
            NodeSelectorControl.SelectedNodeId = folderId;
            NodeSelectorControl.DataSource = source;
            NodeSelectorControl.DataBind();
            if (folderId.HasValue)
                NodeSelectorControl.PopulatePath(folderId.Value);
        }

        protected virtual IEnumerable<INode> GetFolders(int? folderId)
        {
            return IoC.Resolve<IMediaFolderSecureService>().GetTree(folderId);
        }
    }
}