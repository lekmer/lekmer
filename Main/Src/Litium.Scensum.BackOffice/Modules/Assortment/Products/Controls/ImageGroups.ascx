﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageGroups.ascx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls.ImageGroup" %>
<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>
<%@ Register TagPrefix="uc" TagName="ImageGroupCollapsiblePanel" Src="~/UserControls/Assortment/ImageGroupCollapsiblePanel.ascx" %>
<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>
<script type="text/javascript">
	function ResetMessages(){
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
}



    var divContainer = document.getElementById('tabs-main-container');
    divContainer.onscroll = function() {     
        $('.context-menu-shadow').hide();
        $('.context-menu-container').hide();
    }

    function CloseMovieSelect() {
        $('.MovieCancelButton').click();     
    }
    $(document).ready(function() {
    $('.customer-comment-mainDiv').css("z-index", "100000");
    });
</script>

<asp:UpdatePanel ID="ImagesUpdatePanel" runat="server" UpdateMode="Conditional">
<ContentTemplate>
 <asp:Repeater ID="ImageGroupsRepeater" runat="server">
 <ItemTemplate> 

    <div id="image-group">
			    <asp:Panel ID="ImageGroupsPanel" runat="server">
				<uc:ImageGroupCollapsiblePanel runat="server" Id="GroupCollapsiblePanel"
					HasAction="true"
					CommandArgument='<%# Eval("Id") %>'
					Title='<%# Eval("Title") %>'
					ActionCaption="Add"
					ActionButtonCaption="Image"
					ActionButtonImageUrl="./../../Media/Images/Assortment/add-product-image.png"
					ActionButtonCommandName="AddImagePopup"
					ActionButtonCommandArgument='<%# Eval("Id") %>'
					
					MovieButtonCaption="Movie"
					MovieButtonImageUrl="./../../Media/Images/Assortment/add-product-image.png"
					MovieButtonCommandName="AddMoviePopup"
					MovieButtonCommandArgument='<%# Eval("Id") %>'
				/>
				<asp:HiddenField ID="ProductImageGroupIdValueHiddenField" runat="server" Value='<%# Eval("Id") %>' />
			    <div id="ImageGroupGridViewDiv" runat="server" class="area-block-items left" style="table-layout:fixed;">
				    <asp:GridView ID="ImageGroupGrid" runat="server" AutoGenerateColumns="false" SkinID="gridWithIcon" Width="100%"
					    OnRowDataBound="ImageGroupGridRowDataBound"
					    OnRowCommand="ImageGroupGridRowCommand" GridLines="None"
				    >
					    <Columns>
    			            <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row-wo-padding" HeaderText="<%$ Resources:General, Literal_Image %>"  ItemStyle-Height="50px" ItemStyle-Width="8%" >
					            <ItemTemplate>
					              <asp:HiddenField ID="MediaIdValueHiddenField" runat="server" Value='<%# Eval("MediaId") %>' />					              
					              <asp:Image id="ArchiveImage" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
					            </ItemTemplate>
				            </asp:TemplateField>
						    <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row" HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="40%">
							    <ItemTemplate>
							        <div style="width: 100%; overflow:hidden;">
							            <div style="float:left; max-width:230px; overflow:hidden;">
								         <asp:Label runat="server" ID="LinkLabel" Text="<%$ Resources:LekmerMessage,MovieLink%>" Visible="false"/>   <uc:LiteralEncoded ID="TitleLiteral" runat="server"></uc:LiteralEncoded><br />
								           <asp:Label runat="server" ID="ParameterLabel" Text="<%$ Resources:LekmerMessage,Parameter%>" Visible="false"/>    <uc:LiteralEncoded ID="ImagePathLiteral" runat="server"></uc:LiteralEncoded>
								        </div>
								        <div style="float:left;">
								            <uc:ContextMenu ID="ImageContextMenu" runat="server" 
								            CallerImageSrc="~/Media/Images/Common/context-menu.png" 
								            CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png"
								            MenuContainerCssClass="context-menu-container"
								            MenuShadowCssClass="context-menu-shadow"
								            MenuCallerCssClass="context-menu-caller">  
	                                                    <div class="context-menu-header">
					                                        <%= Resources.General.Literal_Manage %>
				                                        </div>
				                                        <div class="menu-row-separator">
				                                        </div>
				                                        <div class="context-menu-row">
					                                        <img src="<%=ResolveUrl("~/Media/Images/sitestructure/block-move-to-area.png") %>" />
					                                        <asp:LinkButton ID="ChangeImageGroupButton" runat="server" CommandName="ChangeImageGroup"  CommandArgument='<%# Eval("MediaId") %>' Text= "<%$ Resources:Product , Literal_ChangeImageGroup %>" />
				                                        </div>
                                        				
				                                        <div class="context-menu-row">
					                                        <img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
					                                        <asp:LinkButton ID="ImageDeleteButton" runat="server" OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_image + "\");" %>' CommandName="DeleteImage"  CommandArgument='<%# Eval("MediaId") %>' Text = "<%$ Resources:General, Button_Delete %>"/>
				                                        </div>
				                                        <div class="menu-row-separator">
				                                        </div>
				                                        <div class="context-menu-row">
					                                        <img src="<%=ResolveUrl("~/Media/Images/SiteStructure/set-start-page.png") %>" />
					                                        <asp:LinkButton ID="SetAsDefaultImageButton" runat="server" CommandName="SetAsDefaultImage"  CommandArgument='<%# Eval("MediaId") %>' Text= "<%$ Resources:Product, Literal_SetDefaultImage %>"/>
				                                        </div>                          
	                                         </uc:ContextMenu>
								        </div>
								     </div>															
							    </ItemTemplate>
						    </asp:TemplateField>
						    <asp:TemplateField  HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row" HeaderText="<%$ Resources:Product, Literal_ImageType %>" ItemStyle-Width="15%">
							    <ItemTemplate>
								    <asp:Label ID="ArchiveImageFileTypeLabel" runat="server" />
							    </ItemTemplate>
						    </asp:TemplateField>
				             <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row" HeaderText="<%$ Resources:Product, Literal_WidthXHeight %>" ItemStyle-Width="15%">
					            <ItemTemplate>
					                <asp:Label ID="ArchiveImageDimensionsLabel" runat="server" ></asp:Label><br />
					            </ItemTemplate>
				            </asp:TemplateField>
						    <asp:TemplateField HeaderStyle-CssClass="grid-header-with-asterisk" ItemStyle-CssClass="grid-row-with-asterisk" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
							    ItemStyle-Width="10%">
							    <HeaderTemplate>
								    <div class="inline">
									   <%= Resources.General.Literal_SortOrder %>
									    <asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshProductImageOrder" 
										    CommandArgument='<%# Eval("MediaId") %>'  ImageUrl="~/Media/Images/Common/refresh.png"
										    ImageAlign="AbsMiddle" AlternateText= "<%$ Resources:General,Literal_Refresh %>" ValidationGroup="vgOrdinals" />
								    </div>
							    </HeaderTemplate>
							    <ItemTemplate>
								    <div class="inline">
									    <asp:RequiredFieldValidator runat="server" 
											ID="OrdinalValidator" 
											ControlToValidate="OrdinalTextBox" 
											Text="*" 
											ErrorMessage="<%$ Resources:GeneralMessage, OrdinalMandatory %>" 
											ValidationGroup="vgOrdinals" />
									    <asp:RangeValidator runat="server" 
											ID="OrdinalRangeValidator" 
											ControlToValidate="OrdinalTextBox" 
											Type="Integer" 
											MaximumValue='<%# int.MaxValue %>' 
											MinimumValue='<%# int.MinValue %>' 
											Text="*" 
											ErrorMessage="<%$ Resources:GeneralMessage, OrdinalShouldInteger%>" 
											ValidationGroup="vgOrdinals"/>
									    <asp:TextBox runat="server" ID="OrdinalTextBox" />									    									
									    <asp:ImageButton runat="server" 
											ID="UpButton" 
											CommandName="UpOrdinal"  
											CommandArgument='<%# Eval("MediaId") %>'
											ImageUrl="~/Media/Images/SiteStructure/up.png" 
											ImageAlign="AbsMiddle" 
											AlternateText="<%$ Resources:General, Button_Up %>" 
											ValidationGroup="vgOrdinals" />
									    <asp:ImageButton runat="server" 
											ID="DownButton" 
											CommandName="DownOrdinal"  
											CommandArgument='<%# Eval("MediaId") %>'
											ImageUrl="~/Media/Images/SiteStructure/down.png" 
											ImageAlign="AbsMiddle" 
											AlternateText="<%$ Resources:General, Button_Down %>" 
											ValidationGroup="vgOrdinals" />
									    </div>
							    </ItemTemplate>
						    </asp:TemplateField>
						    <asp:TemplateField HeaderStyle-CssClass="grid-row-wo-padding" ItemStyle-CssClass="grid-row-wo-padding"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
							    ItemStyle-Width="4%">
							    <ItemTemplate>
									    <asp:ImageButton runat="server" 
											ID="DeleteButton" 
											CommandName="DeleteImage" 
											CommandArgument='<%# Eval("MediaId") %>' 
											ImageUrl="~/Media/Images/Common/delete.gif" 
											OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_image + "\");" %>' 
											AlternateText="<%$ Resources:General, Button_Delete %>"  />
							    </ItemTemplate>
						    </asp:TemplateField>
					    </Columns>
				    </asp:GridView>
				    <br />				
			    </div>
			    </asp:Panel>
			   		 	    	
    </div> 
 </ItemTemplate>
 </asp:Repeater> 
<ajaxToolkit:ModalPopupExtender ID="ImageAddPopup" runat="server" BehaviorID="modalpopup-image"  TargetControlID="ImgBrowseButton"
        PopupControlID="ImagesDiv" BackgroundCssClass="popup-background" CancelControlID="_inpClose"
        Y="100" X="229"  OnCancelScript="ResetMessages();">
</ajaxToolkit:ModalPopupExtender>


<div Style="display: none;">
     <asp:Button ID="ImgBrowseButton" runat="server"/>
</div>

 <asp:HiddenField ID="GroupIdValueHiddenField" runat="server" />
 
		<asp:HiddenField runat="server" ID="IsItMovieHiddenField" />
<uc:MessageContainer ID="SystemMessageContainer" MessageType="Warning" HideMessagesControlId="SaveButton"
	runat="server" /> 
</ContentTemplate> 
</asp:UpdatePanel>

<div id="ImagesDiv" runat="server" class="product-popup-images-container">
    <div id="product-popup-images-header">
        <div id="product-popup-images-header-left">
				
        </div>
        <div id="product-popup-images-header-center">
            <span><%= Resources.Product.Literal_AddImages %></span>
            <input runat="server" type="button" id="_inpClose" class="_inpClose" value="x"  />
        </div>
        <div id="product-popup-images-header-right">
			
        </div>
    </div>
    <br clear="all" />
	<div class="product-popup-images-body">
		<asp:UpdatePanel id="ImageSelectUpdatePannel" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<uc:MessageContainer ID="AddImageMessageContainer" MessageType="Failure" HideMessagesControlId="ImageSelectControl" runat="server" />
		</ContentTemplate>
		</asp:UpdatePanel> 
		<uc:ImageSelect id="ImageSelectControl" runat="server"/>
    </div>
</div>

	<asp:UpdatePanel id="ImageChangeGroupUpdatePanel" runat="server">
		<ContentTemplate>
		<asp:HiddenField ID="MovePopupHiddenField" runat="server" />
        <ajaxToolkit:ModalPopupExtender 
	        ID="ChangeImageGroupPopup" 
	        BehaviorID="BlockMoveUpdatePanel"
	        runat="server"
	        TargetControlID="MovePopupHiddenField"
	        PopupControlID="ImageChangeGroupDivPopup"
	        CancelControlID="CancelChangeGroupButton"
	        BackgroundCssClass="popup-background"/>	
	
		    <asp:HiddenField ID="MediaIdValueHiddenField" runat="server" />
			<div id="ImageChangeGroupDiv" runat="server" class="block-move">
				<div id="ImageChangeGroupDivPopup" class="mainDiv" runat="server" style="display: none;">
				    <asp:Panel ID="ImageChangeGroupSavePanel" runat="server" DefaultButton="SaveChangeGroupButton">
					    <div id="HeaderChangePopupDiv" class="headerDiv">
						    <asp:Label ID="ChangeGroupHeaderLabel" runat="server" Text="<%$ Resources:Product, Literal_ChangeImageGroup %>" />
						    <asp:Button ID="ChangeGroupCloseButton" runat="server" Text="X" />
					    </div>
					    <div id="main-change-group-popup">				            
				            <div>
				                <div class="input-box">				                    
				                    <asp:Repeater ID="OtherGroupRepeater" runat="server">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="ImageGroupIdHiddenField" runat="server" Value='<%# Eval("Id") %>'/>
                                            <asp:CheckBox ID="ImageGroupCheckBox" runat="server" Text='<%# Eval("Title") %>'/><br />
                                        </ItemTemplate>
                                    </asp:Repeater>				                   
				                </div>
				                <div class="right clear" style="width:120px">
				                    <uc:ImageLinkButton ID="SaveChangeGroupButton" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton"/>
				                    <uc:ImageLinkButton ID="CancelChangeGroupButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton"/>
				                </div>
				            </div>
				            <div runat="server" id="MoveSameNameErrorDiv"  style="clear:both">							
							    <uc:MessageContainer ID="GroupChangeMessageContainer"  MessageType="Failure" HideMessagesControlId="SaveChangeGroupButton"  runat="server" />	
					        </div>	
				        </div>
				    </asp:Panel>
				    
				</div>			
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	
	
	
	<asp:HiddenField runat="server" ID="FakeControl" />
<div id="MovieViewDiv" runat="server" class="customer-comment-mainDiv" style="z-index: 10;
	display: none;">
	<div id="customer-comment-header">
		<div id="customer-comment-header-left">
		</div>
		<div id="customer-comment-header-center">
			<span>
				<%= Resources.Lekmer.Label_AddMovie%></span>
			   <input runat="server" type="button" id="MovieCancelButton" class="MovieCancelButton"  value="x"  />
		</div>
		<div id="customer-comment-header-right">
		</div>
	</div>
	
<asp:UpdatePanel ID="MovieUpdatePanel" runat="server" UpdateMode="Conditional">
<ContentTemplate>
	<div id="customer-freight-subMainDiv">
	<asp:Panel ID="Panel1" runat="server" DefaultButton="MovieSaveButton">
		<uc:MessageContainer runat="server" ID="MovieMessageContainer" MessageType="Warning" HideMessagesControlId="MovieSaveButton" />
		<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="MovieValidationGroup" />
		<div class="column1">
			<div class="input-box">
				<span>
					<%= Resources.Lekmer.Literal_Link%> *</span>
				<asp:RequiredFieldValidator ID="MovieValidator" runat="server" ControlToValidate="LinkTextBox"
					ErrorMessage="<%$ Resources:LekmerMessage,EmptyLink%>" Display="None" ValidationGroup="MovieValidationGroup" />
				<br />
				<asp:TextBox ID="LinkTextBox"  runat="server" />
			</div>
			<div class="input-box">
				<span>
					<%= Resources.Lekmer.Literal_Parameter%></span>
				
				<br />
				<asp:TextBox ID="ParameterTextBox"  runat="server" />
			</div>
			<div class="input-box">
				<span>
					<%= Resources.Lekmer.Literal_TumbnailImageUrl%></span>
				<br />
				<asp:TextBox ID="TumbnailImageUrlTextBox"  runat="server" />
			</div>
		
			<div class="input-box">
			   
			    <span><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:General, Literal_PlaceInFolder%>" /> *</span><br />
			        <span class="media-item-information-value">
				        <uc:NodeSelect ID="NodeSelectorControl" runat="server" UseRootNodeInPath="true"  />
			        </span>
		      </div><br />
			<div class="input-box">
			    <asp:LinkButton ID="AddTumbnailImage" runat="server"   >
			    <asp:Image runat="server" ID="ActionButtonImage" AlternateText="" ImageUrl="~/Media/Images/Assortment/add-product-image.png"
					                />
			 <asp:Literal runat="server" ID="ActionButtonCaptionLiteral" Text='<%$ Resources:Lekmer,Literal_AddTumbnailImage%>'			                />
			</asp:LinkButton><br /><br />
		    <asp:Image id="TumbnailImage" ImageUrl="~/Media/Images/Assortment/defaultMovie.jpg" Width="50" Height="50" runat="server" />
		
			</div>	
		    <div class="input-box">	
           		</div>		           
			<br clear="all" />
		</div>
		<br />
		<div class="customer-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="false"  ID="MovieSaveButton" runat="server" Text="<%$ Resources:General, Button_Next %>"
				SkinID="DefaultButton" ValidationGroup="MovieValidationGroup" />
				        <div class="button-container" onclick="CloseMovieSelect();" style="cursor: pointer">
                                <span class="button-left-border"></span><span class="button-middle"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:General, Button_Cancel%>"></asp:Literal></span>
                                <span class="button-right-border"></span>
                            </div>
		</div>
			</asp:Panel>
	</div>
	</ContentTemplate> 
</asp:UpdatePanel>
</div>

	<ajaxToolkit:ModalPopupExtender ID="MoviePopup" runat="server" TargetControlID="FakeControl"
		PopupControlID="MovieViewDiv" CancelControlID="MovieCancelButton"  />
