﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.Media;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Media.MediaFolder;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls.Media;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls
{
	public partial class ProductMedia : UserControlController
	{
		private const string DefaultImageSelectModeKey = "DefaultImageSelectMode";
		public event EventHandler<ImageSelectEventArgs> Selected;
		public event EventHandler FileUploaded;
		public event EventHandler DeleteImage;

		public int? ImageId
		{
			get; set;
		}

		public Collection<IProductImageGroupFull> ProductImageGroups
		{
			get { return FileDropControl.GetProductImageGroups(); }
		}

		public int ProductId
		{
			get; set;
		}

		protected override void SetEventHandlers()
		{
			ImageSelectControl.Selected += ImageSelected;
			ImageSelectControl.FileUploaded += ImageUnloaded;
			ImageSelectControl.Canceled += ImageSelectCanceled;

			FileDropControl.DefaltImageSet += ImageGroupControlDefaltImageSet;
			FileDropControl.UploadMediaFromFolder += FileDropControl_UploadMediaFromFolder;
			DeleteImageButton.Click += DeleteImageButtonClick;
		}


		public bool DefaultImageSelectMode
		{
			get { return ViewState[DefaultImageSelectModeKey] == null ? true : (bool)ViewState[DefaultImageSelectModeKey]; }
			set { ViewState[DefaultImageSelectModeKey] = value; }
		}

		void FileDropControl_UploadMediaFromFolder(object sender, EventArgs e)
		{
			DefaultImageSelectMode = false;
			ImagesPopup.X = 227;
			ImagesPopup.Show();
		}

		protected override void PopulateControl()
		{
			PopulateImage(ImageId);
			FileDropControl.AcceptedFileSize = HttpRuntimeSetting.MaxRequestLength;
			FileDropControl.AcceptedTypes = "image/*,application/pdf";
			FileDropControl.ProductId = ProductId;

			var mediaFolderService = IoC.Resolve<ILekmerMediaFolderSecureService>();
			var imageFolder = MediaFolderHepler.GetFolderByType(mediaFolderService, MediaFolderType.Images);
			var movieFolder = MediaFolderHepler.GetFolderByType(mediaFolderService, MediaFolderType.Movies);
			var pdfFolder = MediaFolderHepler.GetFolderByType(mediaFolderService, MediaFolderType.Pdf);

			AddMovieControl.FolderId = movieFolder != null ? (object) movieFolder.Id : "1005724";
			FileDropControl.ImageFolderId = imageFolder != null ? (object)imageFolder.Id : "1005724";
			FileDropControl.PdfFolderId = pdfFolder != null ? (object)pdfFolder.Id : "1005724";
			AddMovieControl.UserName = FileDropControl.UserName = SignInHelper.SignedInSystemUser.UserName;

			if (null != imageFolder)
			{
				ImageSelectControl.PreSelectFolder(imageFolder.Id);
			}
			//ShowMedia(this, null);
		}

		public void PopulateImage(IImage image)
		{
			ImageTitleLiteral.Text =
				ImageAlternativeTextLiteral.Text = ImageFileExtensionLabel.Text = ImageDimensionsLabel.Text = string.Empty;
			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = false;


			ImageTitleLiteral.Text = image.Title;
			ImageAlternativeTextLiteral.Text = image.AlternativeText;
			ImageFileExtensionLabel.Text = image.FormatExtension;
			ImageDimensionsLabel.Text = image.Width + "px x " + image.Height + "px";

			string imageUrl = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(image.Id, image.FormatExtension));

			ZoomProductImageAnchor.HRef = imageUrl;

			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = DefaultImage.Visible = true;
			ZoomProductImageAnchor.Title = image.Title;

			DefaultImage.ImageUrl = DefaultImage.ImageUrl =
				ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
		}

		protected void PopulateImage(int? imageId)
		{
			ImageTitleLiteral.Text =
				ImageAlternativeTextLiteral.Text = ImageFileExtensionLabel.Text = ImageDimensionsLabel.Text = string.Empty;
			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = false;

			if (imageId.HasValue)
			{
				var imageService = IoC.Resolve<IImageSecureService>();
				IImage image = imageService.GetById((int)imageId);
				PopulateImage(image);
			}
		}

		private void ImageSelectCanceled(object sender, EventArgs e)
		{
			DefaultImageSelectMode = true;
			ImagesPopup.Hide();
		}

		private void ImageUnloaded(object sender, EventArgs e)
		{

				ImagesPopup.X = 227;
				ImagesPopup.Show();
				if (FileUploaded != null)
				{
					FileUploaded(this, EventArgs.Empty);
				}

		}


		public void ImageGroupControlDefaltImageSet(object sender, ImageSelectEventArgs e)
		{
			PopulateImage(e.Image);
			ImagesUpdatePanel.Update();
			if (Selected != null)
			{
				Selected(this, e);
			}
			ScriptManager.RegisterStartupScript(ImagesUpdatePanel, ImagesUpdatePanel.GetType(), "image lightbox",
						"$(document).ready(function() { DefaultImageLitbox(); });", true);
		}

		public void SetDefaltImage(int mediaId)
		{
			FileDropControl.SetAsDefaultImage(mediaId);
		}


		private void ImageSelected(object sender, ImageSelectEventArgs e)
		{

			ImagesPopup.Hide();
			if (DefaultImageSelectMode)
			{
				PopulateImage(e.Image);
			}
			else
			{
				AddNewImage(e.Image);
			}
			if (Selected != null)
			{
				Selected(this, e);
			}
			else
			{
				DefaultImageSelectMode = true;
			}


		}

		private void AddNewImage(IImage image)
		{
			var productImageSecureService = IoC.Resolve<IProductImageSecureService>();

			var productImage = productImageSecureService.Create(image, FileDropControl.ProductId, FileDropControl.MediaGroupId);
			var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
			var groups = productImageGroupSecureService.GetAllFullByProduct(FileDropControl.ProductId);
			var imageGroup = groups.FirstOrDefault(item => item.Id == FileDropControl.MediaGroupId);
			if (imageGroup.ProductImages.Count > 0)
			{
				productImage.Ordinal = imageGroup.ProductImages.Max(item => item.Ordinal) + FileDropControl.StepMove;
			}
			productImageSecureService.Save(productImage);
		}

		protected virtual void DeleteImageButtonClick(object sender, EventArgs e)
		{
			PopulateImage((int?)null);
			if(DeleteImage != null)
			{
				DeleteImage(this, EventArgs.Empty);
			}
		}
	}
}
