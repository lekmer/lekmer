using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	public class MediaItemDataSource
	{
		private int _rowCount;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public ImageCollection GetMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "id")]
		public int SelectCount(int id)
		{
			return _rowCount;
		}

		public MediaItemCollection GetMethod(int maximumRows, int startRowIndex, int id)
		{
			var mediaItemSecureService = IoC.Resolve<IMediaItemSecureService>();
			var mediaItemCollection = mediaItemSecureService.GetAllByFolder(id, maximumRows == 0 ? 0 : startRowIndex / maximumRows + 1, maximumRows, null, null);
			_rowCount = mediaItemCollection.TotalCount;
			return mediaItemCollection;
		}
	}
}