<%@ Import Namespace="Litium.Scensum.BackOffice" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems" %>

<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs"
     Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Variations.Default" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="VariationGroupContent" ContentPlaceHolderID="body" runat="server">

<script type="text/javascript">
	function confirmVariationGroupDelete() {
		return DeleteConfirmation("<%= Resources.Product.Literal_VariationsGroup %>");
	}
</script>

    <Scensum:ToolBoxPanel ID="VariationPanelToolBoxPanel" runat="server" Text="Variations"
        ShowSeparator="true">
        <div class="item" style="width: 100px; color: #CBCBCB; float: left; padding-bottom: 8px;
            padding-top: 10px; padding-left:15px;">
            <%= Resources.General.Literal_Create %></div>
       <div style="float: left; padding-bottom: 8px; padding-top: 8px; font-size: 0.7em; font-weight: normal;">
            <uc:ImageLinkButton Id="CreateVariationGroupButton" runat="server" Text="<%$ Resources:Product, Button_VariationGroup %>" 
            ImageUrl="~/Media/Images/Assortment/variation.gif"
                SkinID="HeaderPanelButton" UseSubmitBehaviour="false" />
            
        </div>
    </Scensum:ToolBoxPanel>
    <br /> <div style="float: left; width:984px; margin-bottom:-10px; margin-top:-5px;">
    	<asp:UpdatePanel ID="MessageContainerUpdatePanel" UpdateMode="Always" runat="server">
			<ContentTemplate>	
                <uc:MessageContainer ID="SystemMessageContainer" MessageType="Success" runat="server" />
					</ContentTemplate>
		</asp:UpdatePanel>
		</div>
    <div id="rl-content" >
    
        <div style="padding-top: 16px; margin-bottom: 26px; padding-left: 20px; padding-right: 20px;">
        <asp:UpdatePanel ID="VariationGroupUpdatePanel" runat="server" UpdateMode="Always">
            <ContentTemplate>
                <sc:GridViewWithCustomPager ID="VariationGroupGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
                    AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" DataSourceID="VariationGroupObjectDataSource"
                    Width="100%">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="4%">
                            <HeaderTemplate>
                                <asp:CheckBox ID="AllConnectedCheckBox" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="SelectCheckBox" runat="server" />
                                <asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="82%">
                            <ItemTemplate>
                                <uc:HyperLinkEncoded runat="server" ID="EditLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Variation.VariationGroup.GetEditUrl(int.Parse(Eval("Id").ToString())) %>'
                                    Text='<%# Eval("Title")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="<%$ Resources:General,Literal_Status %>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:DropDownList ID="StatusList" DataTextField="Title"
                                    AutoPostBack="true" Width="100px" DataValueField="Id" OnSelectedIndexChanged="Ddl_SelectedIndexChanged" runat="server">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Width="4%">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteVariationGroup"
                                    CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
                                    AlternateText="<%$ Resources:General, Button_Delete %>" OnClientClick='return confirmVariationGroupDelete();' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </sc:GridViewWithCustomPager>
                <asp:ObjectDataSource ID="VariationGroupObjectDataSource" runat="server" EnablePaging="true"
                    SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Variations.VariationGroupDataSource" />
				<div id="AllSelectedDiv" class="apply-to-all-selected-2" runat="server" style="display: none;">
					<div style="float:left">
						<span><%= Resources.General.Literal_ApplyToAllSelectedItems%></span>
						<asp:DropDownList runat="server" ID="VariationGroupStatusList" DataTextField="Title" DataValueField="Id" Width="100px" />
						&nbsp;
					</div>
					<div style="float:left">
						<uc:ImageLinkButton runat="server" ID="SetStatusButton" UseSubmitBehaviour="true" Text="<%$ Resources:General, Button_Set %>" SkinID="DefaultButton" />
						<uc:ImageLinkButton runat="server" ID="DeleteVariationGroupButton" UseSubmitBehaviour="true" Text="<%$ Resources:General, Button_Delete %>" SkinID="DefaultButton" OnClientClick='return confirmVariationGroupDelete();' /> 
					</div>
				</div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
	</div>
	
</asp:Content>
