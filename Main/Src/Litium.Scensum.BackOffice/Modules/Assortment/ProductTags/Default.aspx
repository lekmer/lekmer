﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Tags.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="TagGroupsTab" ContentPlaceHolderID="body" runat="server">
	<sc:ToolBoxPanel ID="TagGroupsToolBoxPanel" runat="server" Text="Tag groups" />
	<div class="stores-place-holder">
		<sc:GridViewWithCustomPager
			ID="TagGroupsGrid"
			SkinID="grid"
			AutoGenerateColumns="false"
			Width="100%"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			PagerSettings-Mode="NumericFirstLast"
			runat="server">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="100%">
					<ItemTemplate>
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
						<uc:HyperLinkEncoded ID="TagGroupLink" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
</asp:Content>