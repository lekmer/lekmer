﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Resources;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Tags
{
	public partial class TagEdit : LekmerPageController
	{
		private int _id;
		protected int Id
		{
			get { return _id > 0 ? _id : (_id = GetId()); }
		}

		private IFlagSecureService _flagService;
		protected IFlagSecureService FlagService
		{
			get { return _flagService ?? (_flagService = IoC.Resolve<IFlagSecureService>()); }
		}

		private ITagSecureService _tagService;
		protected ITagSecureService TagService
		{
			get { return _tagService ?? (_tagService = IoC.Resolve<ITagSecureService>()); }
		}

		private ITag _tag;
		protected ITag Tag
		{
			get { return _tag ?? (_tag = TagService.GetById(Id)); }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			PopulateFlags();

			TagHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"", Resources.General.Literal_EditTag, Tag.Value);
			FlagList.SelectedValue = Tag.FlagId.HasValue ? Tag.FlagId.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var flagId = FlagList.SelectedValue;
			if (!string.IsNullOrEmpty(flagId))
			{
				TagService.TagFlagSave(SignInHelper.SignedInSystemUser, Id, Convert.ToInt32(flagId));
			}
			else
			{
				TagService.TagFlagDeleteByTag(SignInHelper.SignedInSystemUser, Id);
			}

			Messager.Add(GeneralMessage.SaveSuccessful, InfoType.Success);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer == null)
			{
				Response.Redirect(LekmerPathHelper.Tags.GetDefaultUrl());
			}
			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		protected virtual void PopulateFlags()
		{
			FlagList.DataSource = FlagService.GetAll();
			FlagList.DataBind();
			FlagList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
	}
}