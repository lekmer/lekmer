﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="TagEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Tags.TagEdit" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="TagEditTab" ContentPlaceHolderID="body" runat="server">
	<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValidatorSummary" DisplayMode="List" ValidationGroup="TagValidationGroup" />
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SaveButton" runat="server" />
	<sc:ToolBoxPanel ID="TagToolBoxPanel" runat="server" Text="Tag" />

	<div class="stores-place-holder">
		<div class="main-caption">
			<uc:LiteralEncoded ID="TagHeader" runat="server" />
		</div>

		<span class="bold"><%= Resources.Campaign.Literal_Flags %></span>
		<br />
		<asp:DropDownList ID="FlagList" runat="server" DataValueField="Id" DataTextField="Title" />

		<br class="clear">

		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" ValidationGroup="TagValidationGroup" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
		</div>
	</div>
</asp:Content>