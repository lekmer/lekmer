﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.BackOffice.UserControls.Translation;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Sizes
{
	public partial class SizeTableEdit : LekmerStatePageController<SizeTableState>, IEditor
	{
		private const int MoveUpStep = -15;
		private const int MoveDownStep = 15;
		private const int DifferenceStep = 10;
		private const int MinStep = 1;

		private List<string> _validationMessage = new List<string>();
		private ICollection<IProductRegistry> _productRegistries;
		private ICollection<ISize> _allSizes;
		private ISizeTableSecureService _sizeTableSecureService;
		private ISizeTableRowSecureService _sizeTableRowSecureService;
		private ILekmerMediaItemSecureService _mediaItemSecureService;
		private ISizeTableMediaSecureService _sizeTableMediaSecureService;
		private ISizeTableMediaRegistrySecureService _sizeTableMediaRegistrySecureService;

		private ICollection<IMediaFormat> _mediaFormats;
		protected ICollection<IMediaFormat> MediaFormats
		{
			get { return _mediaFormats ?? (_mediaFormats = IoC.Resolve<IMediaFormatSecureService>().GetAll()); }
		}

		protected virtual SizeTablesMaster MasterPage
		{
			get { return (SizeTablesMaster) Master; }
		}
		protected ICollection<IProductRegistry> ProductRegistries
		{
			get { return _productRegistries ?? (_productRegistries = IoC.Resolve<IProductRegistrySecureService>().GetAll(BackOfficeSetting.Instance.ProductRegistryIdsToIgnore)); }
		}
		protected ICollection<ISize> AllSizes
		{
			get { return _allSizes ?? (_allSizes = IoC.Resolve<ISizeSecureService>().GetAll()); }
		}
		protected virtual ISizeTableSecureService SizeTableSecureService
		{
			get { return _sizeTableSecureService ?? (_sizeTableSecureService = IoC.Resolve<ISizeTableSecureService>()); }
			set { _sizeTableSecureService = value; }
		}
		protected virtual ISizeTableRowSecureService SizeTableRowSecureService
		{
			get { return _sizeTableRowSecureService ?? (_sizeTableRowSecureService = IoC.Resolve<ISizeTableRowSecureService>()); }
			set { _sizeTableRowSecureService = value; }
		}
		protected virtual ILekmerMediaItemSecureService MediaItemSecureService
		{
			get { return _mediaItemSecureService ?? (_mediaItemSecureService = (ILekmerMediaItemSecureService) IoC.Resolve<IMediaItemSecureService>()); }
			set { _mediaItemSecureService = value; }
		}
		protected virtual ISizeTableMediaSecureService SizeTableMediaSecureService
		{
			get { return _sizeTableMediaSecureService ?? (_sizeTableMediaSecureService = IoC.Resolve<ISizeTableMediaSecureService>()); }
			set { _sizeTableMediaSecureService = value; }
		}
		protected virtual ISizeTableMediaRegistrySecureService SizeTableMediaRegistrySecureService
		{
			get { return _sizeTableMediaRegistrySecureService ?? (_sizeTableMediaRegistrySecureService = IoC.Resolve<ISizeTableMediaRegistrySecureService>()); }
			set { _sizeTableMediaRegistrySecureService = value; }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderSelector.NodeCommand += OnFolderSelectorCommand;
			FolderSelector.SelectedFolderChangedEvent += OnSelectedFolderChanged;
			SizeTableRowsGrid.RowCommand += OnSizeTableRowsRowCommand;
			SizeTableRowsGrid.PageIndexChanging += OnSizeTableRowsPageIndexChanging;
			SizeTableRowsGrid.RowDataBound += OnSizeTableRowsRowDataBound;

			MediaItemsGrid.RowCommand += OnMediaItemsRowCommand;
			MediaItemsGrid.PageIndexChanging += OnMediaItemsPageIndexChanging;
			MediaItemsGrid.RowDataBound += OnMediaItemsRowDataBound;

			SizeTableRowForm.SaveEvent += OnSizeTableRowSave;

			SizeTableDescriptionTranslator.TriggerImageButton = SizeTableDescriptionTranslateButton;

			MediaItemSelectControl.Selected += MediaItemSelected;
			MediaItemSelectControl.FileUploadError += MediaItemUploadError;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var sizeTable = SizeTableSecureService.GetById(id.Value);
				sizeTable = SizeTableSecureService.GetAllIncludes(sizeTable);

				TitleTextBox.Text = sizeTable.Title;
				CommonNameTextBox.Text = sizeTable.CommonName;
				DescriptionTextBox.Text = sizeTable.Description ?? string.Empty;
				Column1TitleTextBox.Text = sizeTable.Column1Title;
				Column2TitleTextBox.Text = sizeTable.Column2Title;

				InitializeState(sizeTable.SizeTableFolderId);
				PopulateSizeTableRows(sizeTable);
				PopulateMediaItems(sizeTable);
				SizeTableIncludeItemsConfigurator.DataSource = sizeTable;
				MasterPage.BreadcrumbAppend.Add(Resources.Lekmer.Literal_EditSizeTable);
			}
			else
			{
				InitializeState(null);
				PopulateSizeTableRowsGridDefault();
				PopulateMediaItemsDefault();
				SizeTableIncludeItemsConfigurator.DataSource = SizeTableSecureService.Create();
				MasterPage.BreadcrumbAppend.Add(Resources.Lekmer.Literal_CreateSizeTable);
			}

			PopulatePath(State.FolderId);
			PopulateTranslations(id);
			PopulateAvaliableSizes();
			SizeTableRowForm.AvaliableSizes = new List<ISize>(State.AvaliableSizes.Values.ToList());

			MasterPage.SelectedFolderId = State.FolderId;
			MasterPage.PopulateTree(State.FolderId);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.LekmerMessage.SizeTable_SaveSuccess);
				}
			}
			MasterPage.SetupTabAndPanel();
		}

		// Page events.

		public void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}
				SystemMessageContainer.MessageType = InfoType.Warning;
				return;
			}

			int? id = GetIdOrNull();

			var sizeTable = id.HasValue ? SizeTableSecureService.GetById(id.Value) : SizeTableSecureService.Create();
			sizeTable.Title = TitleTextBox.Text.Trim();
			sizeTable.CommonName = CommonNameTextBox.Text.Trim();
			sizeTable.Description = DescriptionTextBox.Text.Trim();
			sizeTable.SizeTableFolderId = State.FolderId ?? -1;
			sizeTable.Column1Title = Column1TitleTextBox.Text.Trim();
			sizeTable.Column2Title = Column2TitleTextBox.Text.Trim();

			var sizeTableIncludes = SizeTableIncludeItemsConfigurator.GetResults();
			sizeTable.IncludeProducts = sizeTableIncludes.IncludeProducts;
			sizeTable.IncludeCategoryBrandPairs = sizeTableIncludes.IncludeCategoryBrandPairs;

			FetchMediaItemRegistriesFromGrid();

			sizeTable = SizeTableSecureService.Save(SignInHelper.SignedInSystemUser, sizeTable, GetTranslations(), State.MediaItems);

			if (sizeTable.Id == -1)
			{
				SystemMessageContainer.Add(Resources.LekmerMessage.SizeTable_TitleExist);
			}
			else if (sizeTable.Id == -2)
			{
				SystemMessageContainer.Add(Resources.LekmerMessage.SizeTable_CommonNameExist);
			}
			else
			{
				SizeTableRowSecureService.Delete(SignInHelper.SignedInSystemUser, sizeTable.Id);

				foreach (var item in GetSizeTableRows())
				{
					item.SizeTableRow.SizeTableId = sizeTable.Id;
					SizeTableRowSecureService.Save(SignInHelper.SignedInSystemUser, item.SizeTableRow, item.SizeTableRowTranslation);
				}

				MasterPage.SelectedFolderId = State.FolderId;
				MasterPage.PopulateTree(State.FolderId);
				MasterPage.UpdateBreadcrumbs(State.FolderId);

				Response.Redirect(LekmerPathHelper.SizeTable.GetEditUrlWithMessage(sizeTable.Id));
			}
		}

		protected virtual void InitializeState(int? folderId)
		{
			if (State == null)
			{
				State = new SizeTableState();
			}

			if (State.SizeTableRowStateItems == null)
			{
				State.SizeTableRowStateItems = new List<SizeTableRowItem>();
			}

			if (State.MediaItems == null)
			{
				State.MediaItems = new Collection<ISizeTableMediaRecord>();
			}

			if (folderId.HasValue)
			{
				State.FolderId = folderId;
			}
			else
			{
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					State.FolderId = MasterPage.SelectedFolderId.Value;
				}
			}
		}

		protected virtual void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				MasterPage.RedirectToDefaultPage();
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		// Translations.

		protected virtual void PopulateTranslations(int? id)
		{
			SizeTableTitleTranslator.DefaultValueControlClientId = TitleTextBox.ClientID;
			SizeTableDescriptionTranslator.DefaultValueControlClientId = DescriptionTextBox.ClientID;
			Column1TitleTranslator.DefaultValueControlClientId = Column1TitleTextBox.ClientID;
			Column2TitleTranslator.DefaultValueControlClientId = Column2TitleTextBox.ClientID;

			if (id.HasValue)
			{
				var translations = SizeTableSecureService.GetAllTranslations(id.Value);

				var titleTranslations = new Collection<ITranslationGeneric>();
				var descriptionTranslations = new Collection<ITranslationGeneric>();
				var column1Translations = new Collection<ITranslationGeneric>();
				var column2Translations = new Collection<ITranslationGeneric>();
				foreach (var translation in translations)
				{
					var titleTranslation = BuildTranslationGeneric(translation);
					titleTranslation.Value = translation.Title;
					titleTranslations.Add(titleTranslation);
					
					var descriptionTranslation = BuildTranslationGeneric(translation);
					descriptionTranslation.Value = translation.Description;
					descriptionTranslations.Add(descriptionTranslation);

					var column1Translation = BuildTranslationGeneric(translation);
					column1Translation.Value = translation.Column1Title;
					column1Translations.Add(column1Translation);

					var column2Translation = BuildTranslationGeneric(translation);
					column2Translation.Value = translation.Column2Title;
					column2Translations.Add(column2Translation);
				}

				DataBindTranslator(SizeTableTitleTranslator, id.Value, titleTranslations);
				DataBindTranslator(SizeTableDescriptionTranslator, id.Value, descriptionTranslations);
				DataBindTranslator(Column1TitleTranslator, id.Value, column1Translations);
				DataBindTranslator(Column2TitleTranslator, id.Value, column2Translations);
			}
			else
			{
				DataBindTranslator(SizeTableTitleTranslator, 0, new Collection<ITranslationGeneric>());
				DataBindTranslator(SizeTableDescriptionTranslator, 0, new Collection<ITranslationGeneric>());
				DataBindTranslator(Column1TitleTranslator, 0, new Collection<ITranslationGeneric>());
				DataBindTranslator(Column2TitleTranslator, 0, new Collection<ITranslationGeneric>());
			}

			SizeTableTitleTranslator.DataBind();
			SizeTableDescriptionTranslator.DataBind();
			Column1TitleTranslator.DataBind();
			Column2TitleTranslator.DataBind();
		}

		protected virtual ITranslationGeneric BuildTranslationGeneric(ISizeTableTranslation sizeTableTranslation)
		{
			var translation = IoC.Resolve<ITranslationGeneric>();
			translation.Id = sizeTableTranslation.SizeTableId;
			translation.LanguageId = sizeTableTranslation.LanguageId;
			return translation;
		}

		protected virtual void DataBindTranslator(GenericTranslator translator, int objectId, Collection<ITranslationGeneric> dataSource)
		{
			translator.BusinessObjectId = objectId;
			translator.DataSource = dataSource;
		}

		protected virtual void DataBindTranslator(GenericMultilineTranslator translator, int objectId, Collection<ITranslationGeneric> dataSource)
		{
			translator.BusinessObjectId = objectId;
			translator.DataSource = dataSource;
		}

		protected virtual Collection<ISizeTableTranslation> GetTranslations()
		{
			var translations = new Collection<ISizeTableTranslation>();

			var titleTranslations = SizeTableTitleTranslator.GetTranslations();
			var descriptionTranslations = SizeTableDescriptionTranslator.GetTranslations();
			var column1Translations = Column1TitleTranslator.GetTranslations();
			var column2Translations = Column2TitleTranslator.GetTranslations();
			titleTranslations.ReplaceEmptyStringValuesWithNull();
			descriptionTranslations.ReplaceEmptyStringValuesWithNull();
			column1Translations.ReplaceEmptyStringValuesWithNull();
			column2Translations.ReplaceEmptyStringValuesWithNull();

			for (int i = 0; i < titleTranslations.Count; i++)
			{
				var translation = IoC.Resolve<ISizeTableTranslation>();
				translation.SizeTableId = titleTranslations[i].Id;
				translation.LanguageId = titleTranslations[i].LanguageId;
				translation.Title = titleTranslations[i].Value;
				translation.Description = descriptionTranslations[i].Value;
				translation.Column1Title = column1Translations[i].Value;
				translation.Column2Title = column2Translations[i].Value;
				translations.Add(translation);
			}

			return translations;
		}

		// Parent folder functionality.

		protected virtual void OnFolderSelectorCommand(object sender, CommandEventArgs e)
		{
			FolderSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			FolderSelector.DataBind();
		}

		protected virtual void OnSelectedFolderChanged(object sender, SelectedFolderChangedEventArgs e)
		{
			if (e.SelectedFolderId != null)
			{
				State.FolderId = e.SelectedFolderId.Value;
			}
		}

		protected virtual void PopulatePath(int? folderId)
		{
			FolderSelector.SelectedNodeId = folderId;

			FolderSelector.DataSource = GetPathSource(folderId);
			FolderSelector.DataBind();

			FolderSelector.PopulatePath(folderId);
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			return IoC.Resolve<ISizeTableFolderSecureService>().GetTree(folderId);
		}

		// Validation.

		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidStringValue(TitleTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.SizeTable_TitleEmpty);
				isValid = false;
			}

			if (!IsValidStringValue(CommonNameTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.SizeTable_CommonNameEmpty);
				isValid = false;
			}

			if (!IsValidStringValue(Column1TitleTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.SizeTable_Column1TitleEmpty);
				isValid = false;
			}

			if (!IsValidFolder())
			{
				_validationMessage.Add(Resources.LekmerMessage.SizeTable_FolderEmpty);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidStringValue(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return false;
			}
			return value.Length <= 50;
		}

		protected virtual bool IsValidFolder()
		{
			return State.FolderId > 0;
		}

		// Size Table Rows.

		protected virtual void OnSizeTableRowSave(object sender, UserControls.Assortment.Events.SizeTableRowSelectEventArgs e)
		{
			var sizeTableRowItem = e.SizeTableRowItem;
			var existedRowItem = State.SizeTableRowStateItems.FirstOrDefault(row => row.Id == sizeTableRowItem.Id);
			if (existedRowItem == null)
			{
				var lastRow = State.SizeTableRowStateItems.LastOrDefault();
				int ordinal = lastRow != null ? lastRow.SizeTableRow.Ordinal : 1;
				sizeTableRowItem.SizeTableRow.Ordinal = ++ordinal;
				State.SizeTableRowStateItems.Add(sizeTableRowItem);
			}
			else
			{
				IncreaseAvaliableSizes(existedRowItem.SizeTableRow.SizeId);
				existedRowItem.SizeTableRow = sizeTableRowItem.SizeTableRow;
				existedRowItem.SizeTableRowTranslation = sizeTableRowItem.SizeTableRowTranslation;
			}

			State.AvaliableSizes.Remove(sizeTableRowItem.SizeTableRow.SizeId);
			PopulateSizeTableRowsGrid();
			SizeTableRowForm.AvaliableSizes = new List<ISize>(State.AvaliableSizes.Values.ToList());
		}

		protected virtual void OnSizeTableRowsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			string id = e.CommandArgument.ToString();
			switch (e.CommandName)
			{
				case "DeleteSizeTableRow":
					var itemToDelate = State.SizeTableRowStateItems.FirstOrDefault(i => i.Id == id);
					if (itemToDelate != null)
					{
						State.SizeTableRowStateItems.Remove(itemToDelate);
						PopulateSizeTableRowsGrid();

						IncreaseAvaliableSizes(itemToDelate.SizeTableRow.SizeId);
						SizeTableRowForm.AvaliableSizes = new List<ISize>(State.AvaliableSizes.Values.ToList());
						SizeTableRowForm.PopulateSizes();
					}
					break;
				case "EditSizeTableRow":
					var itemToEdit = State.SizeTableRowStateItems.FirstOrDefault(i => i.Id == id);
					if (itemToEdit != null)
					{
						SizeTableRowForm.AvaliableSizes = new List<ISize>(State.AvaliableSizes.Values.ToList());

						var sizeToAdd = AllSizes.FirstOrDefault(s => s.Id == itemToEdit.SizeTableRow.SizeId);
						if (sizeToAdd != null)
						{
							SizeTableRowForm.Populate(itemToEdit, sizeToAdd);
							SizeTableRowForm.Popup.Show();
						}
					}
					break;
				case "UpOrdinal":
					Move(id, MoveUpStep);
					break;

				case "DownOrdinal":
					Move(id, MoveDownStep);
					break;

				case "RefreshOrdinal":
					RefreshOrdinal();
					break;
			}
		}

		protected virtual void OnSizeTableRowsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SizeTableRowsGrid.PageIndex = e.NewPageIndex;
			PopulateSizeTableRowsGrid();
		}

		protected virtual void OnSizeTableRowsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var sizeTableRowItem = (SizeTableRowItem) row.DataItem;
			var item = AllSizes.FirstOrDefault(s => s.Id == sizeTableRowItem.SizeTableRow.SizeId);
			if (item != null)
			{
				var lblLink = (LinkButton)row.FindControl("SizeTableRowLink");
				lblLink.Text = string.Format("{0} - {1}", item.ErpId, item.ErpTitle);
			}
		}

		protected virtual void PopulateSizeTableRows(ISizeTable sizeTable)
		{
			sizeTable.Rows = SizeTableRowSecureService.GetAllBySizeTable(sizeTable.Id);
			foreach (var sizeTableRow in sizeTable.Rows)
			{
				var translations = SizeTableRowSecureService.GetAllTranslations(sizeTableRow.Id);

				var sizeTableRowItem = new SizeTableRowItem
				{
					Id = sizeTableRow.Id.ToString(CultureInfo.InvariantCulture),
					SizeTableRow = sizeTableRow,
					SizeTableRowTranslation = translations
				};

				State.SizeTableRowStateItems.Add(sizeTableRowItem);
			}

			PopulateSizeTableRowsGrid();
		}

		protected virtual void PopulateSizeTableRowsGrid()
		{
			SizeTableRowsGrid.DataSource = SortSizeTableRows(State.SizeTableRowStateItems, DifferenceStep);
			SizeTableRowsGrid.DataBind();
			SizeTableRowsPanel.Update();
		}

		protected virtual void PopulateSizeTableRowsGridDefault()
		{
			SizeTableRowsGrid.DataSource = null;
			SizeTableRowsGrid.DataBind();
		}

		protected virtual void PopulateAvaliableSizes()
		{
			State.AvaliableSizes.Clear();
			foreach (var size in AllSizes)
			{
				var usedSize = State.SizeTableRowStateItems.FirstOrDefault(r => r.SizeTableRow.SizeId == size.Id);
				if (usedSize == null && !State.AvaliableSizes.ContainsKey(size.Id))
				{
					State.AvaliableSizes.Add(size.Id, size);
				}
			}
		}
		protected virtual void IncreaseAvaliableSizes(int sizeId)
		{
			var sizeToAdd = AllSizes.FirstOrDefault(s => s.Id == sizeId);
			if (sizeToAdd != null && !State.AvaliableSizes.ContainsKey(sizeToAdd.Id))
			{
				State.AvaliableSizes.Add(sizeToAdd.Id, sizeToAdd);
			}
		}

		protected virtual string GetSizeTitle(object sizeid)
		{
			var size = AllSizes.FirstOrDefault(s => s.Id == (int) sizeid);
			return size != null ? size.EuTitle : string.Empty;
		}

		protected virtual List<SizeTableRowItem> GetSizeTableRows()
		{
			RefreshOrdinal();
			var items = SortSizeTableRows(State.SizeTableRowStateItems, MinStep);

			foreach (var item in items)
			{
				item.SizeTableRowTranslation.ReplaceTrimmedEmptyStringValuesWithNull();
			}

			return items;
		}

		// Ordinals.

		protected void Move(string elementId, int step)
		{
			var items = State.SizeTableRowStateItems;
			var item = items.FirstOrDefault(i => i.Id == elementId);
			if (item == null)
			{
				return;
			}

			item.SizeTableRow.Ordinal += step;
			PopulateSizeTableRowsGrid();
		}

		protected void RefreshOrdinal()
		{
			if (!ValidateOrdinals())
			{
				SystemMessageContainer.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return;
			}

			var items = State.SizeTableRowStateItems;

			foreach (GridViewRow row in SizeTableRowsGrid.Rows)
			{
				var id = ((HiddenField) row.FindControl("IdHiddenField")).Value;
				var item = items.FirstOrDefault(i => i.Id == id);
				if (item == null)
				{
					continue;
				}

				item.SizeTableRow.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
			}

			PopulateSizeTableRowsGrid();
		}

		protected bool ValidateOrdinals()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}

		protected static List<SizeTableRowItem> SortSizeTableRows(List<SizeTableRowItem> items, int step)
		{
			items.Sort(delegate(SizeTableRowItem element1, SizeTableRowItem element2)
			{
				if (element1.SizeTableRow.Ordinal < element2.SizeTableRow.Ordinal)
				{
					return -1;
				}

				if (element1.SizeTableRow.Ordinal > element2.SizeTableRow.Ordinal)
				{
					return 1;
				}

				return 0;
			});

			for (int i = 0; i < items.Count(); i++)
			{
				items[i].SizeTableRow.Ordinal = (i + 1) * step;
			}

			return items;
		}

		// Media.

		private void MediaItemSelected(object sender, MediaItemSelectEventArgs e)
		{
			MediaItemsPopup.Hide();

			var sizeTableMedia = SizeTableMediaSecureService.CreateRecord();

			int id = State.MediaItems.Count > 0 ? State.MediaItems.Min(m => m.Id) - 1 : -1;
			if (id > 0)
			{
				id = -1;
			}

			sizeTableMedia.Id = id;
			sizeTableMedia.SizeTableId = -1;
			sizeTableMedia.MediaId = e.MediaItem.Id;
			sizeTableMedia.MediaItem = e.MediaItem;
			sizeTableMedia.MediaRegistries = new Collection<ISizeTableMediaRegistry>();

			State.MediaItems.Add(sizeTableMedia);

			PopulateMediaItemsGrid();

			ScriptManager.RegisterStartupScript(this, GetType(), "media", "$(document).ready(function() { $('#tabs').tabs('select', 2); });", true);
		}
		private void MediaItemUploadError(object sender, EventArgs e)
		{
			MediaItemsPopup.X = 227;
			MediaItemsPopup.Show();

			ScriptManager.RegisterStartupScript(this, GetType(), "media", "$(document).ready(function() { $('#tabs').tabs('select', 2); });", true);
		}

		protected virtual void OnMediaItemsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			int id = int.Parse(e.CommandArgument.ToString());
			switch (e.CommandName)
			{
				case "DeleteMediaItem":
					var itemToDelate = State.MediaItems.FirstOrDefault(i => i.Id == id && i.IsDeleted == false);
					if (itemToDelate != null)
					{
						if (itemToDelate.Id > 0)
						{
							itemToDelate.SetDeleted();
						}
						else
						{
							State.MediaItems.Remove(itemToDelate);
						}

						PopulateMediaItemsGrid();
					}
					break;
			}
		}

		protected virtual void OnMediaItemsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			MediaItemsGrid.PageIndex = e.NewPageIndex;
			PopulateMediaItemsGrid();
		}

		protected virtual void OnMediaItemsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var mediaItem = (ISizeTableMediaRecord)row.DataItem;
			var mediaFormat = MediaFormats.FirstOrDefault(i => i.Id == mediaItem.MediaItem.MediaFormatId);
			string mediaFormatExtension = string.Empty;
			if (mediaFormat != null)
			{
				mediaFormatExtension = mediaFormat.Extension;
			}

			var lblArchiveImageFileType = (Label)row.FindControl("lblArchiveImageFileType");
			lblArchiveImageFileType.Text = mediaFormatExtension;

			var imgArchiveImage = (System.Web.UI.WebControls.Image)row.FindControl("imgArchiveImage");
			var image = IoC.Resolve<IImageSecureService>().GetById(mediaItem.MediaItem.Id);
			if (image != null)
			{
				var lblArchiveImageDimensions = (Label)row.FindControl("lblArchiveImageDimensions");
				lblArchiveImageDimensions.Text = image.Width + " x " + image.Height;
				imgArchiveImage.ImageUrl = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
			}
			else
			{
				imgArchiveImage.ImageUrl = ResolveUrl(PathHelper.Media.GetImageThumbnailLoaderUrl(mediaItem.MediaItem.Id, mediaFormatExtension));
			}

			// Populate registries
			var registryRepeater = (Repeater)row.FindControl("RegistryRepeater");
			PopulateMediaItemRegistries(mediaItem, registryRepeater);
		}

		protected virtual void PopulateMediaItems(ISizeTable sizeTable)
		{
			var mediaItems = SizeTableMediaSecureService.GetAllBySizeTable(sizeTable.Id);

			State.MediaItems = mediaItems;

			PopulateMediaItemsGrid();
		}

		protected virtual void PopulateMediaItemsGrid()
		{
			// read registry selections and save to state
			FetchMediaItemRegistriesFromGrid();

			MediaItemsGrid.DataSource = State.MediaItems.Where(i => i.IsDeleted == false).ToList();
			MediaItemsGrid.DataBind();
			MediaUpdatePanel.Update();
		}

		protected virtual void PopulateMediaItemsDefault()
		{
			MediaItemsGrid.DataSource = null;
			MediaItemsGrid.DataBind();
		}

		protected virtual void PopulateMediaItemRegistries(ISizeTableMediaRecord mediaItem, Repeater registryRepeater)
		{
			var registryItems = new Collection<RegistryItem>();

			foreach (IProductRegistry productRegistry in ProductRegistries)
			{
				var registryItem = new RegistryItem();

				registryItem.Id = productRegistry.ProductRegistryId;
				registryItem.Title = productRegistry.Title;

				registryItem.IsSelected = mediaItem.MediaRegistries.Any(r => r.ProductRegistryId == productRegistry.ProductRegistryId && r.IsDeleted == false);

				registryItems.Add(registryItem);
			}

			registryRepeater.DataSource = registryItems;
			registryRepeater.DataBind();
		}

		protected virtual void FetchMediaItemRegistriesFromGrid()
		{
			foreach (GridViewRow row in MediaItemsGrid.Rows)
			{
				var sizeTableMediaIdHiddenField = (HiddenField)row.FindControl("IdHiddenField");

				int sizeTableMediaId = int.Parse(sizeTableMediaIdHiddenField.Value, CultureInfo.CurrentCulture);

				ISizeTableMediaRecord sizeTableMedia = State.MediaItems.FirstOrDefault(m => m.Id == sizeTableMediaId);

				if (sizeTableMedia == null)
				{
					// item was removed by user
					continue;
				}

				var registryRepeater = (Repeater)row.FindControl("RegistryRepeater");
				foreach (RepeaterItem item in registryRepeater.Items)
				{
					var registryIdHiddenField = (HiddenField)item.FindControl("RegistryIdHidden");
					int registryId = int.Parse(registryIdHiddenField.Value, CultureInfo.CurrentCulture);

					var registryCheckbox = (CheckBox)item.FindControl("RegistryCheckbox");
					bool registrySelected = registryCheckbox.Checked;

					ISizeTableMediaRegistry mediaRegistry = sizeTableMedia.MediaRegistries.FirstOrDefault(r => r.ProductRegistryId == registryId && r.IsDeleted == false);
					if (mediaRegistry == null && registrySelected)
					{
						//Add registry
						mediaRegistry = SizeTableMediaRegistrySecureService.Create();
						mediaRegistry.SizeTableMediaId = sizeTableMedia.Id;
						mediaRegistry.ProductRegistryId = registryId;

						sizeTableMedia.MediaRegistries.Add(mediaRegistry);
					}
					else if (mediaRegistry != null && registrySelected == false)
					{
						//Remove registry
						if (mediaRegistry.Id > 0)
						{
							// should be deleted from db, so just set deleted status
							mediaRegistry.SetDeleted();
						}
						else
						{
							// is new and can be simply removed from collection
							sizeTableMedia.MediaRegistries.Remove(mediaRegistry);
						}
					}
				}
			}
		}
	}

	[Serializable]
	public sealed class SizeTableState
	{
		private Dictionary<int, ISize> _avaliableSizes;

		public int? FolderId { get; set; }
		public List<SizeTableRowItem> SizeTableRowStateItems { get; set; }
		public Collection<ISizeTableMediaRecord> MediaItems { get; set; }
		public Dictionary<int, ISize> AvaliableSizes
		{
			get { return _avaliableSizes ?? (_avaliableSizes = new Dictionary<int, ISize>()); }
		}
	}

	[Serializable]
	public class SizeTableRowItem
	{
		public string Id { get; set; }
		public ISizeTableRow SizeTableRow { get; set; }
		public Collection<ISizeTableRowTranslation> SizeTableRowTranslation { get; set; }
	}

	[Serializable]
	public class RegistryItem
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public bool IsSelected { get; set; }
	}
}