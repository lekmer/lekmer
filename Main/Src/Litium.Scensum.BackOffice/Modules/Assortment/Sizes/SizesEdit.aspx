﻿<%@ page
	language="C#"
	masterpagefile="~/Modules/Assortment/Sizes/SizeTables.Master"
	codebehind="SizesEdit.aspx.cs"
	inherits="Litium.Scensum.BackOffice.Modules.Assortment.Sizes.SizesEdit"
	maintainscrollpositiononpostback="true" %>


<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="SizeTableEditContent" ContentPlaceHolderID="SizeTablePlaceHolder" runat="server">
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>

	<script type="text/javascript">

		function confirmDelete() {
			return DeleteConfirmation("<%= Resources.Lekmer.Literal_SizeRow%>");
		}
	</script>
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div id="sort-content">
				<asp:GridView
					ID="SizeEditRowsGrid"
					SkinID="grid"
					runat="server"
					AutoGenerateColumns="false"
					Width="100%">
					<Columns>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_ErpId %>">
							<ItemTemplate>
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
								<uc:HyperLinkEncoded ID="SizeRowLink" runat="server" Text='<%# Eval("ErpId") %>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_ErpTitle %>">
							<ItemTemplate>
								<uc:literalencoded runat="server" text='<%# Eval("ErpTitle") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Eu %>">
							<ItemTemplate>
								<uc:literalencoded runat="server" text='<%# FormatSize(Eval("Eu"))%>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_EuTitle %>">
							<ItemTemplate>
								<uc:literalencoded runat="server" text='<%# Eval("EuTitle") %>' />
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<ItemTemplate>
								<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteSizeRow" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
		</div>

		<div class="buttons right">
			<uc:imagelinkbutton id="SaveButton" usesubmitbehaviour="true" runat="server" text="<%$ Resources:General, Button_Save %>" validationgroup="vgOrdinals" skinid="DefaultButton" />
			<uc:imagelinkbutton id="CancelButton" usesubmitbehaviour="true" runat="server" text="<%$ Resources:General, Button_Cancel %>" causesvalidation="false" skinid="DefaultButton" />
		</div>
	</asp:Panel>
</asp:Content>
