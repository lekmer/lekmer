﻿<%@ Master Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="SizeTables.Master.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Sizes.SizeTablesMaster" %>

<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="SizeTablesContent" ContentPlaceHolderID="body" runat="server">

	<script type="text/javascript">
		function HideRootExpander(treeId) {
			if (null != treeId) {
				$('span#' + treeId).find('div.tree-item-cell-expand:first').hide();
				DisableRootSelect();
			}
		}

		function DisableRootSelect() {
			$('div.tree-item-cell-main a:first').removeAttr("href");
			$('div.tree-item-cell-main img:first').mouseover(function () {
				$(this).css('cursor', 'default');
				$(this).unbind('click');
			});
		}

		function PrepareRootMenu(treeId) {
			if (null != treeId) $('span#' + treeId).find('img.tree-menu-caller').click(function () {
				ShowHideNonRootSubMenu(this);
			});
		}

		function ShowHideNonRootSubMenu(obj) {
			if (FindNodeKeyU(obj) == '<%= SizeTableFoldersTree.RootNodeId %>') {
				$('div#menu-container').attr('id', 'menu-container-root');
				$('div#menu-container-root').attr('class', 'menu-container-root');
				$('div#menu-shadow').attr('id', 'menu-shadow-root');
				$('div#menu-shadow-root').attr('class', 'menu-shadow-root');
				$('div.menu-hide').hide();
				$('div.menu-root').show();
			}
			else {
				$('div#menu-container-root').attr('id', 'menu-container');
				$('div#menu-container').attr('class', 'menu-container');
				$('div#menu-shadow-root').attr('id', 'menu-shadow');
				$('div#menu-shadow').attr('class', 'menu-shadow');
				$('div.menu-hide').show();
				$('div.menu-root').hide();
			}
		}

		function confirmFolderDelete() {
			return DeleteConfirmation("<%= Resources.Lekmer.Literal_SizeTableFolder %>");
		}
	</script>

	<Scensum:ToolBoxPanel ID="SizeTablePanel" runat="server" ShowSeparator="true" style="float:left">
		<div class="item" style="width: 100px; color: #CBCBCB; float: left; padding-bottom: 8px; padding-top: 10px; padding-left:15px;">
			<%= Resources.General.Literal_Create %>
		</div>

		<div style="float: left; padding-bottom: 8px; padding-top: 8px; font-size: 0.7em; font-weight: normal;">
			<uc:ImageLinkButton Id="CreateButton" runat="server" ImageUrl="~/Media/Images/Assortment/table-add-icon.png" SkinID="HeaderPanelButton" UseSubmitBehaviour="false" />
		</div>

		<div style="padding-bottom: 10px; border-right: solid 1px #ffffff; width: 20px; height: 29px; float: left;">
			&nbsp;
		</div>

		<asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
			<div class="fontSearch" style="padding-bottom:8px; padding-right:5px; padding-top: 10px; float:left;">
				<span style="float:left; font-size:16px; padding-right:35px; padding-left:15px;">
					<%= Resources.General.Literal_Search %>
				</span>
				<asp:TextBox ID="SearchTextBox" runat="server" Style="float: left;" />
			</div>

			<div style="float: left; padding-bottom: 8px; padding-top: 8px; font-size: 0.7em; font-weight: normal;">
				<uc:ImageLinkButton ID="SearchButton" runat="server" Text="<%$ Resources:General, Button_Find %>" SkinID="HeaderPanelButton"  UseSubmitBehaviour="false" />
			</div>
		</asp:Panel>
	</Scensum:ToolBoxPanel>

	<br />
	
	<div class="size-tables-tree">
		<asp:UpdatePanel ID="LeftUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<CustomControls:TemplatedTreeView 
					runat="server"
					ID="SizeTableFoldersTree" 
					DisplayTextControl="TitleLabel" 
					UseRootNode="true" 
					NodeExpanderHiddenCssClass="tree-item-expander-hidden" 
					NodeImgCssClass="tree-node-img"
					MainContainerCssClass="treeview-main-container" 
					NodeMainContainerCssClass="treeview-node"
					NodeChildContainerCssClass="treeview-node-child" 
					NodeExpandCollapseControlCssClass="tree-icon"  
					NodeParentContainerCssClass="treeview-node-parent" 
					NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
					NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" 
					MenuCallerElementCssClass="tree-menu-caller"
					MenuContainerElementId="node-menu" 
					MenuCloseElementId="menu-close">

					<NodeTemplate>
						<div class="tree-item-cell-expand">
							<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
							<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
						</div>

						<div class="tree-item-cell-main">
							<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
							<asp:LinkButton ID="TitleLabel" runat="server" CommandName="Navigate" />
						</div>

						<div class="tree-item-cell-action">
							<img 
								src="<%=ResolveUrl("~/Media/Images/Tree/tree-item-action.png") %>"
								alt="Menu"
								class="tree-menu-caller"
								onmouseover='this.src="<%=ResolveUrl("~/Media/Images/Common/context-menu-over.png") %>";'
								onmouseout='this.src="<%=ResolveUrl("~/Media/Images/Common/context-menu.png") %>";' />
						</div>

						<br />
					</NodeTemplate>

					<MenuTemplate>
						<div id="node-menu" style="position: absolute;">
							<div id="menu-shadow" class="menu-shadow"></div>

							<div id="menu-container" class="menu-container">
								<div class="menu-header">
									<%= Resources.General.Literal_Manage %>
								</div>

								<div class="menu-row-separator">
								</div>

								<div class="menu-row">
									<img src="<%=ResolveUrl("~/Media/Images/Assortment/new-folder.gif") %>" />
									<asp:LinkButton ID="CreateFolderButton" runat="server" OnClick="OnFolderCreate"><%= Resources.General.Literal_CreateChildFolder %></asp:LinkButton>
								</div>

								<div class="menu-root">
									<div class="menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Assortment/cardinality.png") %>" />
										<asp:LinkButton ID="SizesSortButton" runat="server" OnClick="OnSizesSort"><%= Resources.Lekmer.Literal_SizeSorting %></asp:LinkButton>
									</div>
								</div>								
								
								<div class="menu-root">
									<div class="menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Assortment/table-add-icon.png") %>" />
										<asp:LinkButton ID="SizeCreateButton" runat="server" OnClick="OnSizeCreate"><%= Resources.Lekmer.Literal_AddSize %></asp:LinkButton>
									</div>
								</div>
								
								<div class="menu-root">
									<div class="menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Assortment/edit.gif") %>" />
										<asp:LinkButton ID="SizeEditButton" runat="server" OnClick="OnSizeEdit"><%= Resources.Lekmer.Literal_EditDeleteSize%></asp:LinkButton>
									</div>
								</div>

								<div class="menu-hide">
									<div class="menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Assortment/edit.gif") %>" />
										<asp:LinkButton ID="EditButton" runat="server" OnClick="OnFolderEdit"><%= Resources.General.Button_MoveEdit %></asp:LinkButton>
									</div>

									<div class="menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
										<asp:LinkButton ID="DeleteButton" runat="server" OnClick="OnFolderDelete" OnClientClick="return confirmFolderDelete();"><%= Resources.General.Literal_Delete %></asp:LinkButton>
									</div>

									<div class="menu-row-separator">
									</div>

									<div class="menu-header">
										<%= Resources.General.Literal_Create %>
									</div>

									<div class="menu-row-separator">
									</div>

									<div class="menu-row">
										<img src="<%=ResolveUrl("~/Media/Images/Assortment/table-add-icon.png") %>" />
										<asp:LinkButton ID="CreateButton" runat="server" OnClick="OnSizeTableCreate"><%= Resources.Lekmer.Literal_SizeTable%></asp:LinkButton>
									</div>
								</div>
							</div>
						</div>
					</MenuTemplate>
				</CustomControls:TemplatedTreeView>

				<br />

			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	
	<div class="size-tables-content">
		<div style="float: left; width:738px; margin-top:-10px;">
			 <asp:UpdatePanel ID="MessageUpdatePanel" runat="server" UpdateMode="Always">
				<ContentTemplate>
					<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="DeleteButton" runat="server" />
				</ContentTemplate>
			</asp:UpdatePanel>
			<asp:ContentPlaceHolder ID="MessageContainer"  runat="server" />	
		</div>

		<asp:UpdatePanel ID="RightUpdatePanel" runat="server" UpdateMode="Always">
			<ContentTemplate>
				<span class="main-caption">
					<uc:LiteralEncoded ID="Breadcrumbs" runat="server"></uc:LiteralEncoded>
				</span>
			</ContentTemplate>
		</asp:UpdatePanel>

		<br />

		<asp:ContentPlaceHolder ID="SizeTablePlaceHolder" runat="server" />
	</div>
</asp:Content>