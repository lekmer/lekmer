﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Review;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Reviews
{
	public class ReviewsDataSource_addon
	{
		private int _rowCount;

		public int SelectCount()
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex)
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public virtual ReviewRecordCollection SelectMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "productTitle"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "productId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "message"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "author"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "statusId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "dateFrom"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "dateTo"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sortBy"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sortByDescending")]
		public int SelectCount(int maximumRows, int startRowIndex, string productTitle, string productId, string message, string author, string statusId, string dateFrom, string dateTo, string sortBy, string sortByDescending)
		{
			return _rowCount;
		}

		public virtual ReviewRecordCollection SelectMethod(int maximumRows, int startRowIndex, string productTitle, string productId, string message, string author, string statusId, string dateFrom, string dateTo, string sortBy, string sortByDescending)
		{
			var searchCriteria = IoC.Resolve<IReviewSearchCriteria>();
			searchCriteria.ProductTitle = productTitle;
			searchCriteria.ProductId = productId;
			searchCriteria.Message = message;
			searchCriteria.Author = author;
			searchCriteria.StatusId = statusId;
			searchCriteria.CreatedFrom = dateFrom;
			searchCriteria.CreatedTo = dateTo;
			searchCriteria.SortBy = sortBy;
			searchCriteria.SortByDescending = sortByDescending;

			var service = IoC.Resolve<IReviewSecureService>();
			var reviews = service.Search(searchCriteria, startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = reviews.TotalCount;
			return reviews;
		}
	}
}
