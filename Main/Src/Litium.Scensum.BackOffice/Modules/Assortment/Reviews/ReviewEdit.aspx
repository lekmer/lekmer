﻿<%@ Page
	Language="C#"
	MasterPageFile="~/Master/Main.Master"
	CodeBehind="ReviewEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Reviews.ReviewEdit"
	AutoEventWireup="true" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:Content ID="ReviewEditContent" ContentPlaceHolderID="body" runat="server">
	<link href="Media/Review.css"rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		function confirmDeleteReview() {
			return DeleteConfirmation("<%= Resources.Review.Literal_DeleteReview %>");
		}
	</script>

	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SaveButton" runat="server" />
	<Scensum:ToolBoxPanel ID="ReviewEditToolBoxPanel" runat="server" Text="Review edit" />
	<br />
	<asp:Panel ID="ReviewEditPanel" runat="server">
		<div class="content-box">
			<div class="column" style="width: 50%;">
				<div class="input-box">
					<span class="bold"><%=Resources.Review.Literal_Product%>:</span>
					<asp:Literal ID="ProductLiteral" runat="server" /> - 
					<asp:HyperLink ID="ProductLink" runat="server" CssClass="link" />
					<div id="OrderInfoDiv" runat="server" Visible="False">
						<span class="bold"><%=Resources.Review.Literal_OrderId%>:</span>
						<asp:Literal ID="OrderLiteral" runat="server" />
					</div>
				</div>
			</div>
			<div class="column" style="width: 50%;">
				<div class="input-box">
					<span class="bold"><%=Resources.Lekmer.Product_ChannelTitle%>:</span>
					<asp:Literal ID="ChannelLiteral" runat="server" />
					<br />
					<span class="bold"><%=Resources.Review.Literal_Created%></span>
					<asp:Literal ID="CreatedLiteral" runat="server" />
					<asp:HyperLink ID="AuthorLink" runat="server" CssClass="link" Target="_blank" />
					<asp:HyperLink ID="EmailLink" runat="server" CssClass="link" Target="_blank" />
				</div>
			</div>
		</div>

		<div class="content-box">
			<div class="column" style="width: 50%;">
				<div class="input-box">
					<span class="bold"><%=Resources.Review.Literal_Title%></span>
					<br />
					<asp:TextBox ID="TitleTextBox" runat="server" MaxLength="50" Width="100%" />
				</div>
			</div>
			<div class="column" style="width: 15%;">
				<div class="input-box">
					<span class="bold"><%=Resources.Review.Literal_Status%></span>
					<br />
					<asp:DropDownList ID="StatusList" runat="server" />
				</div>
			</div>
			<div class="column">
				<div class="input-box">
					<span class="bold"><%=Resources.Review.Literal_InappropriateContent%>:</span>
					<br />
					<asp:CheckBox ID="InappropriateCheckBox" runat="server" />
				</div>
			</div>
		</div>

		<div class="content-box">
			<div class="column" style="width: 100%;">
				<div class="input-box">
					<span class="bold"><%=Resources.Review.Literal_Review%></span>
					<br />
					<asp:TextBox ID="MessageTextBox" runat="server" TextMode="MultiLine" Rows="5" MaxLength="3000" style="resize: none;" Width="100%"/>
				</div>
			</div>
		</div>

		<div class="content-box">
			<div class="column">
				<div class="input-box">
					<span class="bold"><%=Resources.Review.Literal_Ratings%></span>
					<br />
					<asp:Repeater runat="server" ID="RatingRepeater">
						<ItemTemplate>
							<asp:Literal runat="server" Text='<%# Eval("Rating.Title") %>' />
							&nbsp;-&nbsp;
							<asp:Literal runat="server" Text='<%# string.IsNullOrEmpty(Eval("RatingItem.Title").ToString()) ? Eval("RatingItem.Score") : Eval("RatingItem.Title") %>' />
							<br />
						</ItemTemplate>
					</asp:Repeater>
				</div>
			</div>
		</div>

		<br class="clear" />
		<br class="clear" />
		
		<div class="left">
			<uc:ImageLinkButton ID="DeleteButton" runat="server" Text="<%$ Resources:General, Button_Delete %>" SkinID="DefaultButton" OnClientClick="return confirmDeleteReview();" />
		</div>

		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
		</div>
	</asp:Panel>
</asp:Content>