﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default_addon.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Reviews.Default_addon" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register Assembly="Litium.Scensum.BackOffice" Namespace="Litium.Scensum.BackOffice.UserControls.ContextMenu" TagPrefix="sc" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
    
<asp:Content ID="ReviewsTab" ContentPlaceHolderID="body" runat="server">
	<link href="Media/Review.css"rel="stylesheet" type="text/css" />
	<script type='text/javascript' language='javascript'>
		function ResetSerchForm() {
			if (!confirm("<%= Resources.Review.Message_ClearAll%>")) return;
			$('div#review-search-form input').val('');
			$('div#review-search-form select').val('');
		}

		function confirmDeleteReviews() {
			return DeleteConfirmation("<%= Resources.Review.Literal_DeleteReviews %>");
		}
	</script>
	<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
	<Scensum:ToolBoxPanel ID="ReviewsToolBoxPanel" runat="server" Text="Review search">
    </Scensum:ToolBoxPanel>
    <br />
    <asp:Panel ID="ReviewPanel" runat="server" DefaultButton="SearchButton">
    <div class="review-place-holder">
		<div>
			<span class="review-header"><%= Resources.General.Literal_Search %></span>
		</div>
		<div>
			<div id="review-search-form" class="review-search-form">
				<div>
					<div class="column">
						<div class="input-box">
							<span><%= Resources.Review.Literal_Author%></span><br />
							<asp:TextBox ID="AuthorTextBox" runat="server" />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><%= Resources.Review.Literal_MessageContains %></span><br />
							<asp:TextBox ID="MessageTextBox" runat="server" Width="287px"/>
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><%= Resources.General.Literal_Status %></span><br />
							<asp:DropDownList ID="StatusList" runat="server" DataTextField="Title" DataValueField="Id"></asp:DropDownList>
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><%=Resources.Review.Literal_CreatedDateFrom%></span>
							<br />
							<asp:TextBox ID="StartDateTextBox" runat="server"></asp:TextBox>
							<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
								ImageAlign="AbsMiddle"/>
							<ajaxToolkit:CalendarExtender ID="StartDateCalendarControl" runat="server" TargetControlID="StartDateTextBox" 
								PopupButtonID="StartDateButton" />&nbsp;&nbsp;&nbsp;-&nbsp;
						</div>
					</div>
					<div class="column">
						<div class="input-box-last">
							<span><%=Resources.Review.Literal_CreatedDateTo%></span>
							<br />
							<asp:TextBox ID="EndDateTextBox" runat="server"></asp:TextBox>
							<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
								ImageAlign="AbsMiddle"/>
							<ajaxToolkit:CalendarExtender ID="EndDateCalendarControl" runat="server" TargetControlID="EndDateTextBox" 
								PopupButtonID="EndDateButton" />
						</div>
					</div>
				</div>
				<br />
				<div>
					<div class="column">
						<div class="input-box">
							<span><%= Resources.Review.Literal_ProductId %></span><br />
							<asp:TextBox ID="ProductIdTextBox" runat="server" />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><%= Resources.Review.Literal_ProductTitle%></span><br />
							<asp:TextBox ID="ProductTitleTextBox" runat="server" Width="287px"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="buttons-container">
			<uc:ImageLinkButton ID="ResetButton" runat="server" Text="<%$ Resources:Review, Button_ClearAll %>"	SkinID="DefaultButton" OnClientClick='ResetSerchForm(); return false;'/>
			<uc:ImageLinkButton ID="SearchButton" runat="server" Text="<%$ Resources:General, Button_Search %>" SkinID="DefaultButton" />
		</div>
		<br class="clear" /><br />
		<div id="SearchResultDiv" runat="server" class="review-result-container">
			<div class="review-result-header">
				<div class="review-result-header-left">
					<asp:Label ID="SearchResultLabel" runat="server" CssClass="review-header"></asp:Label>
				</div>
				<div class="review-result-header-right">
					<span><%= Resources.Review.Literal_SortCreationDateDD %></span>
					<asp:LinkButton ID="AscLink" runat="server"><%= Resources.Review.Literal_Ascending %></asp:LinkButton> |
					<asp:LinkButton ID="DescLink" runat="server"><%= Resources.Review.Literal_Descending %></asp:LinkButton>
				</div>
			</div>
			<br />
			<div class="review-result-grid">
				<sc:GridViewWithCustomPager
					ID="ReviewsGrid"
					SkinID="grid"
					runat="server"
					AutoGenerateColumns="false"
					AllowPaging="true"
					PageSize="<%$AppSettings:DefaultGridPageSize%>"
					DataSourceID="ReviewsObjectDataSource"
					Width="100%"
					PagerSettings-Mode="NumericFirstLast">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server"/>
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>	
						<asp:TemplateField HeaderText="<%$ Resources:Review,Literal_Review %>" ItemStyle-Width="96%">
							<ItemTemplate>
								<div class="review-result-grid">
									<div class="review-result-grid">
										<div class="review-first-row">
											<asp:Literal ID="ProductLiteral" runat="server" Text='<%# string.Format("{0}: {1}", Resources.Review.Literal_Product, Eval("ProductId")) %>'></asp:Literal>
											<asp:HyperLink ID="ProductLink" runat="server" Text='<%# Eval("ProductTitle") %>' NavigateUrl='<%# PathHelper.Assortment.Product.GetEditUrl(Convert.ToInt32(Eval("ProductId"))) %>'></asp:HyperLink>
										</div>
										<div class="review-second-row">
											<asp:Literal ID="StatusLiteral" runat="server" Text="<%$ Resources:Review,Literal_Status %>"></asp:Literal>
										</div>
										<div class="review-third-row">
											<sc:ContextMenu3 ID="MenuStatusList" runat="server" DataSource="<%# GetStatusDataSource() %>" SelectedValue='<%# Eval("ReviewStatusId") %>' ImageExpandSrc="~/Media/Images/Common/context-menu.png" ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png" OnSelectedValueChanged="ReviewStatusChanged"></sc:ContextMenu3>
										</div>
										<div class="review-fourth-row">
											<asp:Literal ID="RatingLiteral" runat="server" Text='<%# string.Format(Resources.Review.Literal_Rating, Eval("Rating")) %>'></asp:Literal>
										</div>
									</div>
									<br />
									<div class="review-message-row">
										<uc:LiteralEncoded ID="MessageLiteral" runat="server" Text='<%# Eval("Message")%>'></uc:LiteralEncoded>
									</div>
									<br class="clear" />
									<div class="left">
										<asp:Literal ID="CreatedLiteral" runat="server" Text='<%# string.Format(Resources.Review.Literal_Created, Eval("CreatedDate"), Eval("Author")) %>'></asp:Literal>
										<asp:HyperLink ID="AuthorLink" runat="server" Text='<%# Eval("UserName") == null ? string.Empty : string.Format("({0})", Eval("UserName")) %>' NavigateUrl='<%# string.Format("mailto:{0}", Eval("UserName")) %>' Target="_blank"></asp:HyperLink>
									</div>
								</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<ItemTemplate>
								<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteReview"
									CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
									OnClientClick='<%# "return DeleteConfirmation(\""+Resources.Review.Literal_DeleteReview+"\");"%>' />
									
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</sc:GridViewWithCustomPager>
			</div>
			<br />
			<div id="AllSelectedDiv" runat="server" class="review-apply-all">
				<div class="review-status-container">
					<div class="review-status-label">
						<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
					</div>
					<div class="all-status-list">
						<sc:ContextMenu3 ID="SetStatusContextMenu" runat="server" ImageExpandSrc="~/Media/Images/Common/context-menu.png" ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png"></sc:ContextMenu3>
					</div> 
				</div> 
				<div class="left">
					<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="SetStatusButton" runat="server" Text="<%$ Resources:General,Button_Set%>" />
				</div> 
				<div class="left">
					<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="MultipleDeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete%>" OnClientClick="return confirmDeleteReviews();" />
				</div> 
			</div>
			<asp:ObjectDataSource ID="ReviewsObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Reviews.ReviewsDataSource_addon" />
		</div>
	</div>
	</asp:Panel>
</asp:Content>