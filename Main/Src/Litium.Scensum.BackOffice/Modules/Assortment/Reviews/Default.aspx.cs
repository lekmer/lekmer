﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Resources;
using log4net;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Reviews
{
	public partial class Default : LekmerStatePageController<IReviewSearchCriteria>
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private const string SORT_BY = "RatingReviewFeedback.CreatedDate";

		private Collection<IRatingReviewStatus> _statuses;
		private Collection<IChannel> _channels;
		private IRatingReviewFeedbackSecureService _ratingReviewFeedbackService;
		private IReviewSecureService _reviewSecureService;

		protected Collection<IRatingReviewStatus> Statuses
		{
			get { return _statuses ?? (_statuses = IoC.Resolve<IRatingReviewStatusSecureService>().GetAll()); }
		}
		protected Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = IoC.Resolve<IChannelSecureService>().GetAll()); }
		}
		protected IRatingReviewFeedbackSecureService RatingReviewFeedbackService
		{
			get { return _ratingReviewFeedbackService ?? (_ratingReviewFeedbackService = IoC.Resolve<IRatingReviewFeedbackSecureService>()); }
		}
		protected IReviewSecureService ReviewSecureService
		{
			get { return _reviewSecureService ?? (_reviewSecureService = IoC.Resolve<IReviewSecureService>()); }
		}

		private IReviewSearchCriteria CriteriaDataSourceViewState
		{
			get { return State ?? IoC.Resolve<IReviewSearchCriteria>(); }
			set { State = value; }
		}

		private IReviewSearchCriteria DefaultSearchItem
		{
			get
			{
				SetSortDirection(DescLink, AscLink);

				var searchCriteria = IoC.Resolve<IReviewSearchCriteria>();
				searchCriteria.Author = string.Empty;
				searchCriteria.Message = string.Empty;
				searchCriteria.StatusId = StatusList.SelectedValue;
				searchCriteria.CreatedFrom = GetDate(DateTime.Today.AddMonths(-1).ToShortDateString(), false);
				searchCriteria.CreatedTo = GetDate(DateTime.Today.ToShortDateString(), true);
				searchCriteria.ProductId = string.Empty;
				searchCriteria.ProductTitle = string.Empty;
				searchCriteria.OrderId = string.Empty;
				searchCriteria.ChannelId = CountryList.SelectedValue;
				searchCriteria.RatingId = RatingList.SelectedValue;
				searchCriteria.InappropriateContent = InappropriateContentCheckBox.Checked.ToString();
				searchCriteria.SortBy = SORT_BY;
				searchCriteria.SortByDescending = (!DescLink.Enabled).ToString(CultureInfo.CurrentCulture);
				return searchCriteria;
			}
		}

		protected override void SetEventHandlers()
		{
			UploadReviewsButton.Click += UploadReviews;
			ExportToExcelButton.Click += ExportToExcelClick;
			SearchButton.Click += Search;
			AscLink.Click += SortByAsc;
			DescLink.Click += SortByDesc;
			RemoveInappropriateButton.Click += RemoveInappropriateFlags;
			SetStatusButton.Click += SetStatuses;
			MultipleDeleteButton.Click += MultipleDelete;

			ReviewsGrid.RowDataBound += RowDataBound;
			ReviewsGrid.RowCommand += RowCommand;
			ReviewsGrid.PageIndexChanging += PageIndexChanging;

			ReviewsObjectDataSource.Selected += ReviewsObjectDataSourceSelected;
		}

		protected override void PopulateForm()
		{
			PopulateStatuses();
			PopulateDates();
			PopulateCountries();
			PopulateRatings();

			RestoreSearchData();
		}

		protected virtual void PopulateStatuses()
		{
			StatusList.DataSource = Statuses;
			StatusList.DataBind();

			var contextMenuSource = GetStatusDataSource();
			contextMenuSource.Insert(0, new ContextMenuDataSourceItem { Value = string.Empty, Text = Resources.General.Literal_Status + "..." });
			SetStatusContextMenu.DataSource = contextMenuSource;
			SetStatusContextMenu.DataBind();
		}
		protected virtual Collection<ContextMenuDataSourceItem> GetStatusDataSource()
		{
			return new Collection<ContextMenuDataSourceItem>(Statuses.Select(
				item => new ContextMenuDataSourceItem
				{
					Text = item.Title,
					Value = item.Id.ToString(CultureInfo.CurrentCulture),
					ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture, "Media/{0}.png", item.CommonName))
				}).ToList());
		}
		protected virtual void PopulateDates()
		{
			StartDateCalendarControl.Format = EndDateCalendarControl.Format = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern;
		}
		protected virtual void PopulateCountries()
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			var dataSource = channels.ToDictionary(x => x.Id, y => y.Country.Title);

			CountryList.DataSource = dataSource;
			CountryList.DataBind();
			CountryList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
		protected virtual void PopulateRatings()
		{
			var ratings = new Collection<IRating>();
			var ratingService = IoC.Resolve<IRatingSecureService>();
			var ratingFolders = IoC.Resolve<IRatingFolderSecureService>().GetAll();
			foreach (var ratingFolder in ratingFolders)
			{
				ratings.AddRange(ratingService.GetAllByFolder(ratingFolder.Id));
			}

			RatingList.DataSource = ratings;
			RatingList.DataBind();
			RatingList.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
		protected virtual void SetSortDirection(LinkButton current, LinkButton alternate)
		{
			current.Enabled = false;
			current.Style.Add("color", "#000000");
			current.Style.Remove("text-decoration");
			current.Font.Underline = false;

			alternate.Enabled = true;
			alternate.Style.Add("color", "#004CD5");
			alternate.Style.Add("text-decoration", "underline");
			alternate.Font.Underline = true;
		}

		protected virtual void RestoreSearchData()
		{
			bool isNeedPopulate = false;

			IReviewSearchCriteria source = SearchCriteriaState<IReviewSearchCriteria>.Instance.Get();
			if (source != null)
			{
				CriteriaDataSourceViewState = source;
				isNeedPopulate = !IsPostBack;
			}
			else
			{
				CriteriaDataSourceViewState = DefaultSearchItem;
			}

			PopulateSearchFields(CriteriaDataSourceViewState);

			if (isNeedPopulate)
			{
				ReviewsGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
				PopulateGrid();
			}
		}
		protected virtual void PopulateSearchFields(IReviewSearchCriteria searchCriteria)
		{
			AuthorTextBox.Text = searchCriteria.Author;
			MessageTextBox.Text = searchCriteria.Message;
			StatusList.SelectedValue = searchCriteria.StatusId;
			StartDateTextBox.Text = searchCriteria.CreatedFrom;
			EndDateTextBox.Text = searchCriteria.CreatedTo;
			ProductIdTextBox.Text = searchCriteria.ProductId;
			ProductTitleTextBox.Text = searchCriteria.ProductTitle;
			OrderIdTextBox.Text = searchCriteria.OrderId;
			CountryList.SelectedValue = searchCriteria.ChannelId;
			RatingList.SelectedValue = searchCriteria.RatingId;
			InappropriateContentCheckBox.Checked = bool.Parse(searchCriteria.InappropriateContent);
			if (bool.Parse(searchCriteria.SortByDescending))
			{
				SetSortDirection(DescLink, AscLink);
			}
			else
			{
				SetSortDirection(AscLink, DescLink);
			}
		}
		protected virtual void PopulateGrid()
		{
			var searchCriteria = GetSearchCriteria();
			if (searchCriteria == null) return;

			CriteriaDataSourceViewState = searchCriteria;
			SearchCriteriaState<IReviewSearchCriteria>.Instance.Save(CriteriaDataSourceViewState);

			ReviewsObjectDataSource.SelectParameters.Clear();
			ReviewsObjectDataSource.SelectParameters.Add("maximumRows", ReviewsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			ReviewsObjectDataSource.SelectParameters.Add("startRowIndex", (ReviewsGrid.PageIndex * ReviewsGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			ReviewsObjectDataSource.SelectParameters.Add("productTitle", searchCriteria.ProductTitle);
			ReviewsObjectDataSource.SelectParameters.Add("productId", searchCriteria.ProductId);
			ReviewsObjectDataSource.SelectParameters.Add("orderId", searchCriteria.OrderId);
			ReviewsObjectDataSource.SelectParameters.Add("channelId", searchCriteria.ChannelId);
			ReviewsObjectDataSource.SelectParameters.Add("ratingId", searchCriteria.RatingId);
			ReviewsObjectDataSource.SelectParameters.Add("inappropriateContent", searchCriteria.InappropriateContent);
			ReviewsObjectDataSource.SelectParameters.Add("message", searchCriteria.Message);
			ReviewsObjectDataSource.SelectParameters.Add("author", searchCriteria.Author);
			ReviewsObjectDataSource.SelectParameters.Add("statusId", searchCriteria.StatusId);
			ReviewsObjectDataSource.SelectParameters.Add("dateFrom", searchCriteria.CreatedFrom);
			ReviewsObjectDataSource.SelectParameters.Add("dateTo", searchCriteria.CreatedTo);
			ReviewsObjectDataSource.SelectParameters.Add("sortBy", searchCriteria.SortBy);
			ReviewsObjectDataSource.SelectParameters.Add("sortByDescending", searchCriteria.SortByDescending);

			ExportToExcelButton.Style.Add("display", "inline");
			SearchResultDiv.Style.Add("display", "block");
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IReviewSearchCriteria GetSearchCriteria()
		{
			if (!ValidateSearchForm()) return null;

			var searchCriteria = IoC.Resolve<IReviewSearchCriteria>();

			searchCriteria.Author = AuthorTextBox.Text;
			searchCriteria.Message = MessageTextBox.Text;
			searchCriteria.StatusId = StatusList.SelectedValue;
			searchCriteria.CreatedFrom = GetDate(StartDateTextBox.Text, false);
			searchCriteria.CreatedTo = GetDate(EndDateTextBox.Text, true);
			searchCriteria.ProductId = ProductIdTextBox.Text;
			searchCriteria.ProductTitle = ProductTitleTextBox.Text;
			searchCriteria.OrderId = OrderIdTextBox.Text;
			searchCriteria.ChannelId = CountryList.SelectedValue;
			searchCriteria.RatingId = RatingList.SelectedValue;
			searchCriteria.InappropriateContent = InappropriateContentCheckBox.Checked.ToString(CultureInfo.InvariantCulture);
			searchCriteria.SortBy = SORT_BY;
			searchCriteria.SortByDescending = (!DescLink.Enabled).ToString(CultureInfo.CurrentCulture);

			return searchCriteria;
		}
		protected virtual string GetDate(string text, bool isEndDate)
		{
			if (string.IsNullOrEmpty(text))
			{
				return string.Empty;
			}

			DateTime dateTime;
			if (!DateTime.TryParse(text, out dateTime))
			{
				return string.Empty;
			}

			//If user write "from 2010-10-10 to 2010-10-10" it will be parsed as "from 2010-10-10 00:00:00 to 2010-10-10 00:00:00"
			//so he will see reviews posted at 2010-10-10 00:00:00 only, but he expect to see all reviews posted at this day.
			//Therefor date range should be transformed to following: "from 2010-10-10 00:00:00 to 2010-10-10 23:59:59"
			if (isEndDate && dateTime.Hour == 0 && dateTime.Minute == 0 && dateTime.Second == 0 && dateTime.ToShortDateString() == text)
			{
				return dateTime.AddDays(1).AddSeconds(-1).ToShortDateString();
			}

			return text.ToString(CultureInfo.CurrentCulture);
		}


		protected virtual bool ValidateSearchForm()
		{
			var isValid = true;

			if (!IsValidProductId())
			{
				Messager.Add(Resources.Review.Message_InvalidProductId, InfoType.Warning);
				isValid = false;
			}

			if (!IsValidOrderId())
			{
				Messager.Add(Resources.Review.Message_InvalidOrderId, InfoType.Warning);
				isValid = false;
			}

			if (!IsValidStartDate())
			{
				Messager.Add(Resources.Review.Message_InvalidStartDate, InfoType.Warning);
				isValid = false;
			}

			if (!IsValidEndDate())
			{
				Messager.Add(Resources.Review.Message_InvalidEndDate, InfoType.Warning);
				isValid = false;
			}

			if (!IsValidDateRange())
			{
				Messager.Add(Resources.Review.Message_InvalidDateRange, InfoType.Warning);
				isValid = false;
			}

			return isValid;
		}
		protected virtual bool IsValidProductId()
		{
			var productId = ProductIdTextBox.Text.Trim();
			int id;
			return string.IsNullOrEmpty(productId) || int.TryParse(productId, out id);
		}
		protected virtual bool IsValidOrderId()
		{
			var orderId = OrderIdTextBox.Text.Trim();
			int id;
			return string.IsNullOrEmpty(orderId) || int.TryParse(orderId, out id);
		}
		protected virtual bool IsValidStartDate()
		{
			var dateTimeString = StartDateTextBox.Text.Trim();
			DateTime date;
			return string.IsNullOrEmpty(dateTimeString) || DateTime.TryParse(dateTimeString, out date);
		}
		protected virtual bool IsValidEndDate()
		{
			var dateTimeString = EndDateTextBox.Text.Trim();
			DateTime date;
			return string.IsNullOrEmpty(dateTimeString) || DateTime.TryParse(dateTimeString, out date);
		}
		protected virtual bool IsValidDateRange()
		{
			DateTime startDate;
			DateTime endDate;
			return !DateTime.TryParse(StartDateTextBox.Text.Trim(), out startDate)
				|| !DateTime.TryParse(EndDateTextBox.Text.Trim(), out endDate)
				|| endDate >= startDate;
		}


		protected virtual void UploadReviews(object sender, EventArgs e)
		{
			ImportReviews();
		}
		protected virtual void ExportToExcelClick(object sender, EventArgs e)
		{
			ExportToExcel();
		}
		protected virtual void Search(object sender, EventArgs e)
		{
			ReviewsGrid.PageIndex = 0;
			PopulateGrid();
		}
		protected virtual void SortByAsc(object sender, EventArgs e)
		{
			SetSortDirection(AscLink, DescLink);
			ReviewsGrid.PageIndex = 0;
			PopulateGrid();
		}
		protected virtual void SortByDesc(object sender, EventArgs e)
		{
			SetSortDirection(DescLink, AscLink);
			ReviewsGrid.PageIndex = 0;
			PopulateGrid();
		}
		protected virtual void RemoveInappropriateFlags(object sender, EventArgs e)
		{
			var ratingReviewFeedbackIds = ReviewsGrid.GetSelectedIds().ToList();
			if (ratingReviewFeedbackIds.Count == 0)
			{
				Messager.Add(Resources.Review.Message_NoReviewSelected, InfoType.Warning);
				return;
			}

			RatingReviewFeedbackService.RemoveInappropriateFlag(SignInHelper.SignedInSystemUser, ratingReviewFeedbackIds);

			Messager.Add(RatingReview.Message_InappropriateFlagsRemovedSuccessfully, InfoType.Success);

			PopulateGrid();
		}

		protected virtual void SetStatuses(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(SetStatusContextMenu.SelectedValue))
			{
				Messager.Add(GeneralMessage.ChooseStatusValue, InfoType.Warning);
				return;
			}

			var ratingReviewFeedbackIds = ReviewsGrid.GetSelectedIds().ToList();
			if (ratingReviewFeedbackIds.Count == 0)
			{
				Messager.Add(Resources.Review.Message_NoReviewSelected, InfoType.Warning);
				return;
			}

			var statusId = int.Parse(SetStatusContextMenu.SelectedValue, CultureInfo.CurrentCulture);

			RatingReviewFeedbackService.SetStatus(SignInHelper.SignedInSystemUser, ratingReviewFeedbackIds, statusId);

			Messager.Add(GeneralMessage.StatusesChangedSuccessfully, InfoType.Success);

			PopulateGrid();
		}

		protected virtual void MultipleDelete(object sender, EventArgs e)
		{
			var ratingReviewFeedbackIds = ReviewsGrid.GetSelectedIds().ToList();
			if (ratingReviewFeedbackIds.Count == 0)
			{
				Messager.Add(Resources.Review.Message_NoReviewSelected, InfoType.Warning);
				return;
			}

			RatingReviewFeedbackService.Delete(SignInHelper.SignedInSystemUser, ratingReviewFeedbackIds);

			Messager.Add(GeneralMessage.DeleteSeccessful, InfoType.Success);

			if (ReviewsGrid.Rows.Count == ratingReviewFeedbackIds.Count() && ReviewsGrid.PageIndex > 0)
			{
				ReviewsGrid.PageIndex--;
			}

			PopulateGrid();
		}
		
		protected void RowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add("onclick",
														 "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + ReviewsGrid.ClientID + 
														 @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" +
														 ReviewsGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
					}
					break;
				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox)ReviewsGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add("onclick",
												"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
												cbSelect.ID + "', '" + ReviewsGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");

						var ratingRepeater = (Repeater)row.FindControl("RatingRepeater");
						if (ratingRepeater != null)
						{
							ratingRepeater.DataSource = ((IRatingReviewFeedbackRecord) row.DataItem).RatingItemProductVotes;
							ratingRepeater.DataBind();
						}
					}
					break;
			}
		}

		protected void RowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "EditReview")
			{
				var reviewId = Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				Response.Redirect(LekmerPathHelper.Review.GetEditUrl(reviewId));
			}

			if (e.CommandName == "DeleteReview")
			{
				var id = Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				RatingReviewFeedbackService.Delete(SignInHelper.SignedInSystemUser, new Collection<int>{id});

				if (ReviewsGrid.Rows.Count == 1 && ReviewsGrid.PageIndex > 0)
				{
					ReviewsGrid.PageIndex--;
				}

				PopulateGrid();
			}
		}

		protected virtual void PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ReviewsGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void ReviewStatusChanged(object sender, ContextMenu3EventArgs e)
		{
			var row = ((GridViewRow)(((ContextMenu3)sender).Parent.Parent));
			var hiddenField = row.FindControl("IdHiddenField") as HiddenField;
			if (hiddenField == null)
			{
				return;
			}

			var ratingReviewFeedbackId = Convert.ToInt32(hiddenField.Value, CultureInfo.CurrentCulture);
			int newStatusId;
			if (!int.TryParse(e.SelectedValue, out newStatusId))
			{
				return;
			}

			RatingReviewFeedbackService.SetStatus(SignInHelper.SignedInSystemUser, new Collection<int> {ratingReviewFeedbackId}, newStatusId);

			Messager.Add(GeneralMessage.StatusesChangedSuccessfully, InfoType.Success);

			PopulateGrid();
		}

		protected virtual void ReviewsObjectDataSourceSelected(object sender, ObjectDataSourceStatusEventArgs e)
		{
			var reviews = e.ReturnValue as RatingReviewFeedbackRecordCollection;
			if (reviews == null)
			{
				return;
			}

			SearchResultLabel.Text = string.Format(CultureInfo.CurrentCulture, Resources.Review.Literal_SearchResult, reviews.Count, reviews.TotalCount);
		}


		// Excel export/import.
		protected virtual void ExportToExcel()
		{
			try
			{
				var searchCriteria = GetSearchCriteria();
				if (searchCriteria == null)
				{
					return;
				}

				var feedbacks = RatingReviewFeedbackService.Search(searchCriteria, 0, 0);
				if (feedbacks == null || feedbacks.Count <= 0)
				{
					return;
				}

				var originalStatus = Statuses.FirstOrDefault(s => s.Id.ToString(CultureInfo.InvariantCulture) == searchCriteria.StatusId);
				var status = (originalStatus != null && originalStatus.Title == RatingReviewStatusType.Pending.ToString()) ? 0 : 1;

				var additionalColumns = new Dictionary<string, string>();
				var rows = new Collection<Dictionary<string, string>>();
				foreach (var feedback in feedbacks)
				{
					var channel = Channels.FirstOrDefault(c => c.Id == feedback.ChannelId);

					var row = new Dictionary<string, string>
					{
						{FeedbackSpreadsheetColumns.FeedbackId.ToString(), feedback.Id.ToString(CultureInfo.InvariantCulture)},
						{FeedbackSpreadsheetColumns.HYID.ToString(), feedback.ProductErpId.ToString(CultureInfo.InvariantCulture)},
						{FeedbackSpreadsheetColumns.CustomerName.ToString(), feedback.Review.AuthorName.ToString(CultureInfo.InvariantCulture)},
						{FeedbackSpreadsheetColumns.Title.ToString(), feedback.Review.Title.StripIllegalXmlCharacters()},
						{FeedbackSpreadsheetColumns.Review.ToString(), feedback.Review.Message.StripIllegalXmlCharacters()},
						{FeedbackSpreadsheetColumns.Date.ToString(), feedback.CreatedDate.ToString(CultureInfo.InvariantCulture)},
						{FeedbackSpreadsheetColumns.Channel.ToString(), channel != null ? channel.CommonName : string.Empty},
						{FeedbackSpreadsheetColumns.PendingApproved.ToString(), status.ToString(CultureInfo.InvariantCulture)},
						{FeedbackSpreadsheetColumns.IsNeedDelete.ToString(), "0"}
					};

					foreach (var item in feedback.RatingItemProductVotes)
					{
						var key = item.Rating.CommonName;
						var value = item.RatingItem.Title.IsNullOrEmpty() ? item.RatingItem.Score.ToString(CultureInfo.InvariantCulture) : item.RatingItem.Title;
						row.Add(key, value);

						AddToDictionary(additionalColumns, key, item.Rating.Title);
					}

					rows.Add(row);
				}

				var columns = new Dictionary<string, string>();
				foreach (FeedbackSpreadsheetColumns type in Enum.GetValues(typeof(FeedbackSpreadsheetColumns)))
				{
					if (type == FeedbackSpreadsheetColumns.PendingApproved)
					{
						foreach (var column in additionalColumns)
						{
							AddToDictionary(columns, column.Key, column.Value);
						}
					}

					var key = type.ToString();
					AddToDictionary(columns, key, type.GetEnumDescription());
				}

				var spreadsheetData = new SpreadsheetDocumentData { SheetName = "Reviews", Columns = columns, Rows = rows };


				var filePath = GetExportFilePath();
				if (File.Exists(filePath))
				{
					RemoveTempFile(filePath);
				}

				var spreadsheetHelper = new SpreadsheetHelper();
				spreadsheetHelper.CreateSpreadsheetWorkbook(filePath, new Collection<SpreadsheetDocumentData> { spreadsheetData });

				SaveFile(filePath);
				//RemoveTempFile(filePath);
			}
			catch (Exception exception)
			{
				Messager.Add(string.Format("ERROR: {0}", exception.Message), InfoType.Warning);
				_log.Error(exception);
			}
		}
		protected virtual void AddToDictionary(Dictionary<string, string> dictionary, string key, string value)
		{
			if (!dictionary.ContainsKey(key))
			{
				dictionary.Add(key, value);
			}
		}
		protected virtual string GetExportFilePath()
		{
			var path = BackOfficeSetting.Instance.TempReviewsExcelFilePathExport;
			return string.Format(path, SignInHelper.SignedInSystemUser.Id, DateTime.Now.ToString("yyyyMMdd-HHmmss"));
		}
		protected virtual void SaveFile(string filePath)
		{
			var sourceFile = new FileStream(filePath, FileMode.Open);
			var getContent = new byte[sourceFile.Length];
			sourceFile.Position = 0;
			sourceFile.Read(getContent, 0, (int)sourceFile.Length);
			sourceFile.Close();
			Response.ClearContent();
			Response.ClearHeaders();
			Response.Buffer = true;
			Response.ContentType = "application/vnd.ms-excel";
			Response.AddHeader("Content-Length", getContent.Length.ToString(CultureInfo.InvariantCulture));
			Response.AddHeader("Content-Disposition", "attachment; filename=" + BackOfficeSetting.Instance.ReviewsExcelFileName);
			Response.BinaryWrite(getContent);
			Response.Flush();
			Response.End();
		}
		protected virtual void RemoveTempFile(string filePath)
		{
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}
		}

		protected virtual void ImportReviews()
		{
			try
			{
				HttpPostedFile file = FileUploadControl.PostedFile;

				if (!ValidateFileToUpload(file)) return;

				var filePath = GetImportFilePath(file.FileName);
				if (File.Exists(filePath))
				{
					RemoveTempFile(filePath);
				}
				using (var fileStream = File.Create(filePath))
				{
					StreamAppender.AppendToStream(file.InputStream, fileStream, 8 * 1024);
				}

				var returnColumns = new Collection<string>
					{
						FeedbackSpreadsheetColumns.FeedbackId.GetEnumDescription(),
						FeedbackSpreadsheetColumns.CustomerName.GetEnumDescription(),
						FeedbackSpreadsheetColumns.PendingApproved.GetEnumDescription(),
						FeedbackSpreadsheetColumns.IsNeedDelete.GetEnumDescription()
					};
				var spreadsheetHelper = new SpreadsheetHelper();
				var reviews = spreadsheetHelper.ReadSpreadsheetDom(file.InputStream, returnColumns, false); //File does not contain all needed information. Please check if imported file contains columns with titles: 'Feedback Id', 'Customer Name', 'Pending|Approved (0|1)', 'IsNeedDelete'
				file.InputStream.Close();

				ManageReviews(returnColumns, reviews);
				
				Messager.Add("Reviews were uploaded successfully", InfoType.Success);
			}
			catch (Exception exception)
			{
				Messager.Add(string.Format("ERROR: {0}", exception.Message), InfoType.Warning);
				_log.Error(exception);
			}
		}
		private void ManageReviews(Collection<string> returnColumns, IEnumerable<Collection<string>> reviews)
		{
			List<int> toDelete;
			List<int> toApprove;
			List<int> toPending;
			Dictionary<int, string> customerNames;
			GetReviewLists(returnColumns, reviews, out toDelete, out toApprove, out toPending, out customerNames);

			if (toDelete.Count > 0)
			{
				RatingReviewFeedbackService.Delete(SignInHelper.SignedInSystemUser, toDelete);
			}

			ReviewSecureService.Update(SignInHelper.SignedInSystemUser, customerNames.Keys, customerNames.Values);

			if (toApprove.Count > 0)
			{
				var status = Statuses.FirstOrDefault(s => s.CommonName == RatingReviewStatusType.Approved.ToString());
				if (status != null)
				{
					RatingReviewFeedbackService.SetStatus(SignInHelper.SignedInSystemUser, toApprove, status.Id);
				}
			}

			if (toPending.Count > 0)
			{
				var status = Statuses.FirstOrDefault(s => s.CommonName == RatingReviewStatusType.Pending.ToString());
				if (status != null)
				{
					RatingReviewFeedbackService.SetStatus(SignInHelper.SignedInSystemUser, toPending, status.Id);
				}
			}

			PopulateGrid();
		}
		private void GetReviewLists(
			Collection<string> returnColumns,
			IEnumerable<Collection<string>> reviews,
			out List<int> toDelete,
			out List<int> toApprove,
			out List<int> toPending,
			out Dictionary<int, string> customerNames)
		{
			int idIndex = returnColumns.IndexOf(FeedbackSpreadsheetColumns.FeedbackId.GetEnumDescription());
			int customerNameIndex = returnColumns.IndexOf(FeedbackSpreadsheetColumns.CustomerName.GetEnumDescription());
			int isPendingApprovedIndex = returnColumns.IndexOf(FeedbackSpreadsheetColumns.PendingApproved.GetEnumDescription());
			int isNeedDeleteIndex = returnColumns.IndexOf(FeedbackSpreadsheetColumns.IsNeedDelete.GetEnumDescription());

			toDelete = new List<int>();
			toApprove = new List<int>();
			toPending = new List<int>();
			customerNames = new Dictionary<int, string>();

			foreach (var review in reviews)
			{
				int id;
				int isPendingApprove;
				int isNeedDelete;

				bool isCorrectId = int.TryParse(review[idIndex], out id);
				bool isCorrectStatus = int.TryParse(review[isPendingApprovedIndex], out isPendingApprove);
				bool isCorrectIsDelete = int.TryParse(review[isNeedDeleteIndex], out isNeedDelete);
				string customerName = review[customerNameIndex];
				bool isCorrectCustomerName = !string.IsNullOrEmpty(customerName);

				if (!isCorrectId) continue;

				if (isCorrectIsDelete && isNeedDelete == 1)
				{
					toDelete.Add(id);
					continue;
				}

				if (!isCorrectCustomerName) continue;
				if (!customerNames.ContainsKey(id))
				{
					customerNames.Add(id, customerName);
				}

				if (!isCorrectStatus) continue;
				switch (isPendingApprove)
				{
					case 1:
						toApprove.Add(id);
						break;
					case 0:
						toPending.Add(id);
						break;
				}
			}
		}
		protected virtual string GetImportFilePath(string fileName)
		{
			var path = BackOfficeSetting.Instance.TempReviewsExcelFilePathImport;
			return string.Format(path, SignInHelper.SignedInSystemUser.Id, DateTime.Now.ToString("yyyyMMdd-HHmmss"), fileName);
		}

		private bool ValidateFileToUpload(HttpPostedFile file)
		{
			bool isValid = true;

			if (!FileUploadControl.HasFile)
			{
				Messager.Add("Please select excel file for upload", InfoType.Warning);
				isValid = false;
			}

			int maxFileSize = HttpRuntimeSetting.MaxRequestLength;
			if (maxFileSize <= 0)
			{
				Messager.Add(MediaMessage.NoConfirmMaxFileSize, InfoType.Warning);
				isValid = false;
			}

			if (FileUploadControl.PostedFile.ContentLength/1024 > maxFileSize)
			{
				Messager.Add("There is large file to upload", InfoType.Warning);
				isValid = false;
			}

			var extension = Path.GetExtension(file.FileName);
			if (string.IsNullOrEmpty(extension) || (extension != ".xls" && extension != ".xlsx"))
			{
				Messager.Add(MediaMessage.IncorrectFileFormat, InfoType.Warning);
				isValid = false;
			}

			return isValid;
		}
	}
}