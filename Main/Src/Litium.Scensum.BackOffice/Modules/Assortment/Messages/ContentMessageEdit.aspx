﻿<%@ Page Language="C#" MasterPageFile="ContentMessage.Master" CodeBehind="ContentMessageEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Messages.ContentMessageEdit" AutoEventWireup="true"%>

<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericMultilineTranslator" Src="~/UserControls/Translation/GenericMultilineTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="IncludeItemsConfigurator" Src="~/UserControls/Assortment/IncludeItemsConfigurator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="TimeLimiter" Src="~/UserControls/Common/EntityTimeLimiter.ascx" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="ContentMessageEditContent" runat="server" ContentPlaceHolderID="ContentMessagePlaceHolder">
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<script type="text/javascript">
		function submitForm() {
			tinyMCE.triggerSave();
		}
	</script>

	<div class="content-message">
		<div class="column">
			<div class="input-box">
				<span><%=Resources.General.Literal_CommonName%> *</span>
				<br />
				<asp:TextBox ID="CommonNameTextBox" runat="server" MaxLength="50" />
			</div>
		</div>
		<div class="column">
			<div class="input-box">
				<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
				<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
			</div>
		</div>
		<div class="column">
			<div class="input-box">
				<span ><%= Resources.Lekmer.Literal_IsDropShip %></span>
				<br />
				<asp:CheckBox ID="IsDropShipCheckBox" runat="server" />
			</div>
		</div>
	</div>

	<br class="clear" />

	<div class="size-tables">
		<div class="input-box">
			<span><%=Resources.Lekmer.Literal_Message%></span>
			<asp:ImageButton runat="server" ID="ContentMessageTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
			<br />
			<uc:LekmerTinyMceEditor ID="MessageEditor" runat="server" SkinID="tinyMCE" />
		</div>
	</div>

	<br class="clear" />
	<hr/>

	<uc:IncludeItemsConfigurator ID="IncludeItemsConfigurator" runat="server"/>
	
	<br class="clear" />
	<br />

	<uc:TimeLimiter runat="server" Id="TimeLimiter"/>

	<br class="clear" />
	<br />

	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
	</div>
	
	<uc:GenericWysiwygTranslator ID="MessageTranslator" runat="server" />
</asp:Content>