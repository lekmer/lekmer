﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Translation;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Messages
{
	public partial class ContentMessageEdit : LekmerStatePageController<ContentMessageState>, IEditor
	{
		private List<string> _validationMessage = new List<string>();
		private IContentMessageSecureService _contentMessageSecureService;

		protected virtual ContentMessageMaster MasterPage
		{
			get { return (ContentMessageMaster) Master; }
		}
		protected virtual IContentMessageSecureService ContentMessageSecureService
		{
			get { return _contentMessageSecureService ?? (_contentMessageSecureService = IoC.Resolve<IContentMessageSecureService>()); }
			set { _contentMessageSecureService = value; }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderSelector.NodeCommand += OnFolderSelectorCommand;
			FolderSelector.SelectedFolderChangedEvent += OnSelectedFolderChanged;

			MessageTranslator.TriggerImageButton = ContentMessageTranslateButton;
		}

		protected override void PopulateForm()
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				var contentMessage = ContentMessageSecureService.GetById(id.Value);
				contentMessage = ContentMessageSecureService.GetAllIncludes(contentMessage);

				CommonNameTextBox.Text = contentMessage.CommonName;
				MessageEditor.SetValue(contentMessage.Message ?? string.Empty);
				IsDropShipCheckBox.Checked = contentMessage.IsDropShip;

				InitializeState(contentMessage.ContentMessageFolderId);
				IncludeItemsConfigurator.DataSource = contentMessage;
				TimeLimiter.TimeLimit = contentMessage;
				MasterPage.BreadcrumbAppend.Add(Resources.Lekmer.Literal_EditContentMessage);
			}
			else
			{
				InitializeState(null);
				var emptyContentMessage = IoC.Resolve<IContentMessage>();
				IncludeItemsConfigurator.DataSource = emptyContentMessage;
				TimeLimiter.TimeLimit = emptyContentMessage;
				MasterPage.BreadcrumbAppend.Add(Resources.Lekmer.Literal_CreateContentMessage);
			}

			PopulatePath(State.FolderId);
			PopulateTranslations(id);

			MasterPage.SelectedFolderId = State.FolderId;
			MasterPage.PopulateTree(State.FolderId);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!IsPostBack)
			{
				bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
				if (hasMessage.HasValue && hasMessage.Value && !SystemMessageContainer.HasMessages)
				{
					SystemMessageContainer.MessageType = InfoType.Success;
					SystemMessageContainer.Add(Resources.LekmerMessage.ContentMessage_SaveSuccess);
				}
			}
			MasterPage.SetupTabAndPanel();
		}

		// Page events.

		public void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					SystemMessageContainer.Add(warning);
				}
				SystemMessageContainer.MessageType = InfoType.Warning;
				return;
			}

			int? id = GetIdOrNull();
			var contentMessage = id.HasValue ? ContentMessageSecureService.GetById(id.Value) : IoC.Resolve<IContentMessage>();

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(contentMessage);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			contentMessage.CommonName = CommonNameTextBox.Text.Trim();
			contentMessage.Message = MessageEditor.GetValue().Trim();
			contentMessage.ContentMessageFolderId = State.FolderId ?? -1;
			contentMessage.IsDropShip = IsDropShipCheckBox.Checked;
			
			var contentMessageIncludes = IncludeItemsConfigurator.GetResults();
			contentMessage.IncludeProducts = contentMessageIncludes.IncludeProducts;
			contentMessage.IncludeCategories = contentMessageIncludes.IncludeCategories;
			contentMessage.IncludeBrands = contentMessageIncludes.IncludeBrands;
			contentMessage.IncludeSuppliers = contentMessageIncludes.IncludeSuppliers;

			contentMessage = ContentMessageSecureService.Save(SignInHelper.SignedInSystemUser, contentMessage, GetTranslations());

			if (contentMessage.Id == -2)
			{
				SystemMessageContainer.Add(Resources.LekmerMessage.ContentMessage_CommonNameExist);
			}
			else
			{
				MasterPage.SelectedFolderId = State.FolderId;
				MasterPage.PopulateTree(State.FolderId);
				MasterPage.UpdateBreadcrumbs(State.FolderId);

				Response.Redirect(LekmerPathHelper.ContentMessage.GetEditUrlWithMessage(contentMessage.Id));
			}
		}

		protected virtual void InitializeState(int? folderId)
		{
			if (State == null)
			{
				State = new ContentMessageState();
			}

			if (folderId.HasValue)
			{
				State.FolderId = folderId;
			}
			else
			{
				if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
				{
					State.FolderId = MasterPage.SelectedFolderId.Value;
				}
			}
		}

		protected virtual void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				MasterPage.RedirectToDefaultPage();
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		// Translations.

		protected virtual void PopulateTranslations(int? id)
		{
			MessageTranslator.DefaultValueControlClientId = MessageEditor.ClientID;

			if (id.HasValue)
			{
				var translations = ContentMessageSecureService.GetAllTranslations(id.Value);

				var messageTranslations = new Collection<ITranslationGeneric>();
				foreach (var translation in translations)
				{
					var messageTranslation = BuildTranslationGeneric(translation);
					messageTranslation.Value = translation.Message;
					messageTranslations.Add(messageTranslation);
				}

				DataBindTranslator(MessageTranslator, id.Value, messageTranslations);
			}
			else
			{
				DataBindTranslator(MessageTranslator, 0, new Collection<ITranslationGeneric>());
			}

			MessageTranslator.DataBind();
		}

		protected virtual ITranslationGeneric BuildTranslationGeneric(IContentMessageTranslation contentMessageTranslation)
		{
			var translation = IoC.Resolve<ITranslationGeneric>();
			translation.Id = contentMessageTranslation.ContentMessageId;
			translation.LanguageId = contentMessageTranslation.LanguageId;
			return translation;
		}

		protected virtual void DataBindTranslator(GenericWysiwygTranslator translator, int objectId, Collection<ITranslationGeneric> dataSource)
		{
			translator.BusinessObjectId = objectId;
			translator.DataSource = dataSource;
		}

		protected virtual Collection<IContentMessageTranslation> GetTranslations()
		{
			var translations = new Collection<IContentMessageTranslation>();

			var descriptionTranslations = MessageTranslator.GetTranslations();
			descriptionTranslations.ReplaceEmptyStringValuesWithNull();

			for (int i = 0; i < descriptionTranslations.Count; i++)
			{
				var translation = IoC.Resolve<IContentMessageTranslation>();
				translation.ContentMessageId = descriptionTranslations[i].Id;
				translation.LanguageId = descriptionTranslations[i].LanguageId;
				translation.Message = descriptionTranslations[i].Value;
				translations.Add(translation);
			}

			return translations;
		}

		// Parent folder functionality.

		protected virtual void OnFolderSelectorCommand(object sender, CommandEventArgs e)
		{
			FolderSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			FolderSelector.DataBind();
		}

		protected virtual void OnSelectedFolderChanged(object sender, UserControls.Media.Events.SelectedFolderChangedEventArgs e)
		{
			if (e.SelectedFolderId != null)
			{
				State.FolderId = e.SelectedFolderId.Value;
			}
		}

		protected virtual void PopulatePath(int? folderId)
		{
			FolderSelector.SelectedNodeId = folderId;
			FolderSelector.DataSource = GetPathSource(folderId);
			FolderSelector.DataBind();
			FolderSelector.PopulatePath(folderId);
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			return IoC.Resolve<IContentMessageFolderSecureService>().GetTree(folderId);
		}

		// Validation.

		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidStringValue(CommonNameTextBox.Text.Trim()))
			{
				_validationMessage.Add(Resources.LekmerMessage.ContentMessage_CommonNameEmpty);
				isValid = false;
			}

			if (!IsValidFolder())
			{
				_validationMessage.Add(Resources.LekmerMessage.ContentMessage_FolderEmpty);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidStringValue(string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return false;
			}
			return value.Length <= 50;
		}

		protected virtual bool IsValidFolder()
		{
			return State.FolderId > 0;
		}
	}

	[Serializable]
	public sealed class ContentMessageState
	{
		public int? FolderId { get; set; }
	}
}