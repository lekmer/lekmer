﻿using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
	public class BrandDataSource
	{
		private int _rowCount;

		public int SelectCount()
		{
			return _rowCount;
		}

		[SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		 SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex)
		{
			return _rowCount;
		}

		public virtual BrandCollection SelectMethod(int maximumRows, int startRowIndex)
		{
			var service = IoC.Resolve<IBrandSecureService>();
			var brand = service.GetAll(startRowIndex/maximumRows + 1, maximumRows, null, null);
			_rowCount = brand.TotalCount;
			return brand;
		}
	}
}