﻿using System;
using System.Web.UI;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
	public partial class BrandMaster : MasterPage
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			CreateBrandButton.Click += OnBrandCreate;
			BrandToolBoxPanel.Text = Resources.Product.Literal_Brand;
		}

		protected virtual void OnBrandCreate(object sender, EventArgs e)
		{
			Response.Redirect("~/Modules/Assortment/Brands/BrandEdit.aspx");
		}
	}
}