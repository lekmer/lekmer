using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Exceptions;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Categories
{
	public partial class CategoryEdit : LekmerStatePageController<CategoryState>, IEditor
	{
		private IIconSecureService _iconSecureService;
		protected IIconSecureService IconSecureService
		{
			get { return _iconSecureService ?? (_iconSecureService = IoC.Resolve<IIconSecureService>()); }
		}

		private IDeliveryTimeSecureService _deliveryTimeSecureService;
		protected IDeliveryTimeSecureService DeliveryTimeSecureService
		{
			get { return _deliveryTimeSecureService ?? (_deliveryTimeSecureService = IoC.Resolve<IDeliveryTimeSecureService>()); }
		}

		private Collection<IRatingGroupFolder> _allRatingGroupFolders;

		protected override void SetEventHandlers()
		{
			ProductGrid.PageIndexChanging += ProductGridPageIndexChanging;
			RegistryGrid.RowDataBound += RegistryGridRowDataBound;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			RatingGroupsGrid.PageIndexChanging += OnRatingGroupsPageIndexChanging;
			RatingGroupsGrid.RowDataBound += OnRatingGroupsRowDataBound;
			RatingGroupsGrid.RowCommand += OnRatingGroupsRowCommand;
			RemoveRatingGroupsButton.Click += OnRatingGroupsRemove;

			RatingGroupSelector.SelectEvent += OnRatingGroupsAdd;

			AddTagGroupButton.Click += AddTagGroupClick;
			RemoveTagGroupButton.Click += RemoveTagGroupClick;

			CategoryPackageInfoTranslator.TriggerImageButton = CategoryPackageInfoTranslateButton;
		}

		protected override void PopulateForm()
		{
			var categoryId = GetId();

			var categoryService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();
			var category = categoryService.GetCategoryRecordById(categoryId) as ILekmerCategoryRecord;

			if (category == null)
			{
				throw new BusinessObjectNotExistsException(categoryId);
			}

			CategoryTitleTextBox.Text = category.Title;
			CategoryErpIdTextBox.Text = category.ErpId;
			MonitorThresholdBox.Text = category.MonitorThreshold.HasValue ? category.MonitorThreshold.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
			MaxQuantityPerOrderBox.Text = category.MaxQuantityPerOrder.HasValue ? category.MaxQuantityPerOrder.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;

			CategoryProductsObjectDataSource.SelectParameters.Clear();
			CategoryProductsObjectDataSource.SelectParameters.Add("maximumRows", ProductGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			CategoryProductsObjectDataSource.SelectParameters.Add("startRowIndex", (ProductGrid.PageIndex * ProductGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			CategoryProductsObjectDataSource.SelectParameters.Add("categoryId", categoryId.ToString(CultureInfo.CurrentCulture));

			RegistryGrid.DataSource = category.CategorySiteStructureRegistries;
			RegistryGrid.DataBind();

			SetCategoryBreadcrumbPath(category);

			CategoryTitleTranslator.DefaultValueControlClientId = CategoryTitleTextBox.ClientID;
			CategoryTitleTranslator.BusinessObjectId = category.Id;
			CategoryTitleTranslator.DataSource = categoryService.GetAllTranslationsByCategory(category.Id);
			CategoryTitleTranslator.DataBind();

			CategoryPackageInfoEditor.SetValue(category.PackageInfo);
			CategoryPackageInfoTranslator.DefaultValueControlClientId = CategoryPackageInfoEditor.ClientID;
			CategoryPackageInfoTranslator.BusinessObjectId = categoryId;
			CategoryPackageInfoTranslator.DataSource = categoryService.GetAllPackageInfoTranslationsByCategory(categoryId);
			CategoryPackageInfoTranslator.DataBind();

			PopulateRatingGroups();

			InitializeStateTagGroups(categoryId);
			PopulateTagGroups(State.SelectedTagGroups);
			PopulateProductRegistryRestrictions();
			PopulateMultipleSizesSetting(category);

			Collection<IIcon> icons = IconSecureService.GetAllByCategory(categoryId);
			PopulateIcons(icons);

			PopulateDeliveryTimes(category);
		}

		private void PopulateMultipleSizesSetting(ILekmerCategoryRecord category)
		{
			if (category.AllowMultipleSizesPurchase.HasValue)
			{
				if (category.AllowMultipleSizesPurchase.Value)
				{
					//Allow
					AllowMultipleSizesRadioButton.Checked = true;
					DisallowMultipleSizesRadioButton.Checked = false;
					InheritMultipleSizesCheckBox.Checked = false;
				}
				else
				{
					//Disallow
					AllowMultipleSizesRadioButton.Checked = false;
					DisallowMultipleSizesRadioButton.Checked = true;
					InheritMultipleSizesCheckBox.Checked = false;
				}
			}
			else
			{
				//Inherit from parent
				AllowMultipleSizesRadioButton.Checked = false;
				DisallowMultipleSizesRadioButton.Checked = false;
				InheritMultipleSizesCheckBox.Checked = true;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (IsPostBack)
			{
				GetSelectedIncludeSubCategories();
			}
		}


		public virtual void OnCancel(object sender, EventArgs e)
		{
			string referrer = Page.Request.QueryString["Referrer"];

			if (referrer == null)
			{
				Response.Redirect(Request.Url.PathAndQuery);
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var categoryId = GetId();

			var categoryService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();
			var category = categoryService.GetCategoryRecordById(categoryId) as ILekmerCategoryRecord;
			if (category == null)
			{
				throw new BusinessObjectNotExistsException(categoryId);
			}

			bool needUpdateMasterTree = category.Title != CategoryTitleTextBox.Text;
			category.Title = CategoryTitleTextBox.Text;
			category.MonitorThreshold = string.IsNullOrEmpty(MonitorThresholdBox.Text)
				? null
				: (int?)int.Parse(MonitorThresholdBox.Text, CultureInfo.CurrentCulture);
			category.MaxQuantityPerOrder = string.IsNullOrEmpty(MaxQuantityPerOrderBox.Text)
				? null
				: (int?)int.Parse(MaxQuantityPerOrderBox.Text, CultureInfo.CurrentCulture);

			foreach (GridViewRow row in RegistryGrid.Rows)
			{
				var pcns = ((LekmerContentPageSelector)row.FindControl("ParentContentNodeSelector"));
				ICategorySiteStructureRegistry reg = category.CategorySiteStructureRegistries.First(r => r.SiteStructureRegistryId == pcns.SiteStructureRegistryId);
				reg.ProductParentContentNodeId = pcns.SelectedNodeId;
				reg.ProductTemplateContentNodeId = ((LekmerContentPageSelector)row.FindControl("TemplateContentNodeSelector")).SelectedNodeId;
			}

			var productRegistryRestrictions = GetProductRegistryRestrictions();

			category.AllowMultipleSizesPurchase = InheritMultipleSizesCheckBox.Checked ? (bool?) null : AllowMultipleSizesRadioButton.Checked;
			string packageInfo = CategoryPackageInfoEditor.GetValue();
			category.PackageInfo = packageInfo.IsEmpty() ? null : packageInfo;

			int oldDeliveryTimeId;
			bool isNeedDeleteDeliveryTime = ManageDeliveryTime(category, out oldDeliveryTimeId);

			categoryService.Update(
				SignInHelper.SignedInSystemUser,
				category,
				CategoryTitleTranslator.GetTranslations(),
				CategoryPackageInfoTranslator.GetTranslations(),
				productRegistryRestrictions,
				GetSelectedIcons());

			SaveCategoryRatingGroups();

			SaveCategoryTagGroups(categoryId);

			// Update product registry product.
			var oldRestrictions = State.RegistryRestrictionCategories.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);
			var newRestrictions = productRegistryRestrictions.ToDictionary(r => r.ProductRegistryId, r => r.ProductRegistryId);
			UpdateProductRegistryProduct(oldRestrictions, newRestrictions, category.Id);

			// Delete DeliveryTime
			if (isNeedDeleteDeliveryTime)
			{
				DeliveryTimeSecureService.Delete(oldDeliveryTimeId);
			}

			SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCategory);
			
			if (needUpdateMasterTree)
			{
				UpdateMasterTree(category.Id);
			}

			SetCategoryBreadcrumbPath(category);

			PopulateProductRegistryRestrictions();
			PopulateDeliveryTimes(category);
		}


		protected virtual void OnRatingGroupsAdd(object sender, UserControls.Assortment.Events.RatingGroupSelectEventArgs e)
		{
			foreach (var group in e.RatingGroups)
			{
				var clone = group;
				if (State.CategoryRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == clone.Id) == null)
				{
					var ratingGroupCategory = IoC.Resolve<IRatingGroupCategory>();
					ratingGroupCategory.CategoryId = GetId();
					ratingGroupCategory.RatingGroupId = group.Id;
					ratingGroupCategory.IncludeSubcategories = true;

					var categoryRatingGroupState = new CategoryRatingGroupState(@group, ratingGroupCategory);
					State.CategoryRatingGroups.Add(categoryRatingGroupState);
				}
			}
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = RatingGroupsGrid.GetSelectedIds();
			foreach (var id in ids)
			{
				var cloneId = id;
				var group = State.CategoryRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == cloneId);
				if (group != null)
				{
					State.CategoryRatingGroups.Remove(group);
				}
			}
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnRatingGroupsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGroupsGrid.PageIndex = e.NewPageIndex;
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteGroup")
			{
				var group = State.CategoryRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				if (group != null)
				{
					State.CategoryRatingGroups.Remove(group);
					PopulateRatingGroupsGrid();
				}
			}
		}


		protected virtual void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void RegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var reg = (ICategorySiteStructureRegistry)row.DataItem;
			var lblTitle = (Label)row.FindControl("RegistryTitleLabel");
			lblTitle.Text = IoC.Resolve<ISiteStructureRegistrySecureService>().GetById(reg.SiteStructureRegistryId).Title;
			var pcns = (LekmerContentPageSelector)row.FindControl("ParentContentNodeSelector");
			pcns.SiteStructureRegistryId = reg.SiteStructureRegistryId;
			pcns.SelectedNodeId = reg.ProductParentContentNodeId;
			pcns.SetContentPageTitle();
			var tcns = (LekmerContentPageSelector)row.FindControl("TemplateContentNodeSelector");
			tcns.SiteStructureRegistryId = reg.SiteStructureRegistryId;
			tcns.SelectedNodeId = reg.ProductTemplateContentNodeId;
			tcns.SetContentPageTitle();
		}


		protected virtual void PopulateRatingGroups()
		{
			var ratingGroupCategorySecureService = IoC.Resolve<IRatingGroupCategorySecureService>();
			var ratingGroupSecureService = IoC.Resolve<IRatingGroupSecureService>();

			Collection<IRatingGroupCategory> ratingGroupCategories = ratingGroupCategorySecureService.GetAllByCategory(GetId());

			State = new CategoryState(new Collection<CategoryRatingGroupState>());

			foreach (var ratingGroupCategory in ratingGroupCategories)
			{
				IRatingGroup ratingGroup = ratingGroupSecureService.GetById(ratingGroupCategory.RatingGroupId);

				State.CategoryRatingGroups.Add(new CategoryRatingGroupState(ratingGroup, ratingGroupCategory));
			}

			PopulateRatingGroupsGrid();
		}

		protected virtual void PopulateRatingGroupsGrid()
		{
			RatingGroupsGrid.DataSource = State.CategoryRatingGroups;
			RatingGroupsGrid.DataBind();
			RatingGroupsUpdatePanel.Update();
		}


		protected virtual void SaveCategoryRatingGroups()
		{
			var service = IoC.Resolve<IRatingGroupCategorySecureService>();

			var ratingGroupCategories = new Collection<IRatingGroupCategory>();

			foreach (var categoryRatingGroupState in State.CategoryRatingGroups)
			{
				ratingGroupCategories.Add(categoryRatingGroupState.RatingGroupCategory);
			}

			service.SaveAll(SignInHelper.SignedInSystemUser, GetId(), new Collection<IRatingGroupCategory>(ratingGroupCategories));
		}


		protected virtual void GetSelectedIncludeSubCategories()
		{
			foreach (GridViewRow row in RatingGroupsGrid.Rows)
			{
				var selectCheckBox = row.FindControl("ApplyForSubCategoriesCheckBox") as CheckBox;
				if (selectCheckBox == null)
				{
					throw new ControlIntegrationException("RatingGroupsGrid should have TemplateField column with check box with ID = 'ApplyForSubCategoriesCheckBox'.");
				}

				var hiddenFiled = row.FindControl("IdHiddenField") as HiddenField;
				if (hiddenFiled == null)
				{
					throw new ControlIntegrationException("RatingGroupsGrid should have TemplateField column with hidden field with ID = 'Id'.");
				}

				var id = Convert.ToInt32(hiddenFiled.Value, CultureInfo.CurrentCulture);

				CategoryRatingGroupState categoryRatingGroupState = State.CategoryRatingGroups.FirstOrDefault(c => c.RatingGroup.Id == id);
				if (categoryRatingGroupState != null)
				{
					categoryRatingGroupState.RatingGroupCategory.IncludeSubcategories = selectCheckBox.Checked;
				}
			}
		}

		protected virtual string GetRatingGropEditUrl(object groupId)
		{
			return LekmerPathHelper.RatingGroup.GetEditUrlForCategoryEdit(Convert.ToInt32(groupId, CultureInfo.CurrentCulture), GetId());
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allRatingGroupFolders == null)
			{
				var service = IoC.Resolve<IRatingGroupFolderSecureService>();
				_allRatingGroupFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allRatingGroupFolders, Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetCategoryBreadcrumbPath(ICategory category)
		{
			var breadcrumb = new StringBuilder(category.Title);

			ICategorySecureService service = null;
			var parentId = category.ParentCategoryId;
			if (parentId != null)
				service = IoC.Resolve<ICategorySecureService>();

			while (parentId != null)
			{
				ICategory parentCategory = service.GetCategoryRecordById((int)parentId);
				breadcrumb.Insert(0, string.Format(CultureInfo.InvariantCulture, "{0} > ", parentCategory.Title));

				parentId = parentCategory.ParentCategoryId;
			}

			breadcrumb.Insert(0, string.Format(CultureInfo.InvariantCulture, "{0} > ", Resources.Product.Literal_Category));
			CategoryTitleLabel.Text = breadcrumb.ToString();
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('"
					+ selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
					cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual void UpdateMasterTree(int selectedNodeId)
		{
			var master = Master as Categories;
			if (master != null)
			{
				master.UpdateMasterTree(selectedNodeId);
			}
		}


		protected virtual void InitializeStateTagGroups(int categoryId)
		{
			if (State == null)
			{
				State = new CategoryState(new Collection<CategoryRatingGroupState>());
			}

			State.AllTagGroups = new Collection<ITagGroup>();
			State.SelectedTagGroups = new Collection<ITagGroup>();

			var allTagGroups = IoC.Resolve<ITagGroupSecureService>().GetAll();
			State.AllTagGroups = allTagGroups;

			var categoryTagGroups = IoC.Resolve<ICategoryTagGroupSecureService>().GetAllByCategory(categoryId);

			foreach (var tagGroup in allTagGroups)
			{
				if (categoryTagGroups.FirstOrDefault(tg => tg.TagGroupId == tagGroup.Id) != null)
				{
					State.SelectedTagGroups.Add(tagGroup);
				}
			}
		}

		protected virtual void AddTagGroupClick(object sender, EventArgs e)
		{
			Collection<ITagGroup> tagGroups = State.SelectedTagGroups;

			foreach (ListItem item in AvailableTagGroupsListBox.Items)
			{
				if (item.Selected)
				{
					var tagGroupId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					var tagGroup = State.AllTagGroups.FirstOrDefault(tg => tg.Id == tagGroupId);
					if (tagGroup!= null)
					{
						tagGroups.Add(tagGroup);
					}
				}
			}

			State.SelectedTagGroups = tagGroups;

			PopulateTagGroups(tagGroups);
		}

		protected virtual void RemoveTagGroupClick(object sender, EventArgs e)
		{
			Collection<ITagGroup> tagGroups = State.SelectedTagGroups;

			foreach (ListItem item in SelectedTagGroupsListBox.Items)
			{
				if (item.Selected)
				{
					var tagGroupId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					var tagGroup = tagGroups.FirstOrDefault(tg => tg.Id == tagGroupId);
					if (tagGroup != null)
					{
						tagGroups.Remove(tagGroup);
					}
				}
			}

			State.SelectedTagGroups = tagGroups;

			PopulateTagGroups(tagGroups);
		}

		protected virtual void PopulateTagGroups(Collection<ITagGroup> selectedTagGroups)
		{
			var allTagGroups = State.AllTagGroups;

			var availableTagGroups = new Collection<ITagGroup>();
			foreach (var tagGroup in allTagGroups)
			{
				if (selectedTagGroups.FirstOrDefault(tg => tg.Id == tagGroup.Id) == null)
				{
					availableTagGroups.Add(tagGroup);
				}
			}

			AvailableTagGroupsListBox.DataSource = availableTagGroups.OrderBy(tg => tg.Title);
			AvailableTagGroupsListBox.DataBind();

			SelectedTagGroupsListBox.DataSource = selectedTagGroups.OrderBy(tg => tg.Title);
			SelectedTagGroupsListBox.DataBind();

			DefaultTagGroups.Update();
		}

		protected virtual void SaveCategoryTagGroups(int categoryId)
		{
			var service = IoC.Resolve<ICategoryTagGroupSecureService>();

			var tagGroupIds = new Collection<int>();

			foreach (var tagGroup in State.SelectedTagGroups)
			{
				tagGroupIds.Add(tagGroup.Id);
			}

			service.SaveAll(categoryId, tagGroupIds);
		}


		// PopulateProductRegistryRestrictions.
		private void PopulateProductRegistryRestrictions()
		{
			var categoryId = GetIdOrNull();
			if (categoryId.HasValue)
			{
				var registryRestrictionCategories = IoC.Resolve<IProductRegistryRestrictionCategorySecureService>().GetAllByCategory(categoryId.Value);

				if (State == null)
				{
					State = new CategoryState(new Collection<CategoryRatingGroupState>());
				}
				State.RegistryRestrictionCategories = registryRestrictionCategories;

				ProductRegistryRestriction.RegistryRestrictions = registryRestrictionCategories.ToDictionary(restriction => restriction.ProductRegistryId, restriction => restriction.RestrictionReason);
			}

			ProductRegistryRestriction.BindData();
		}

		private Collection<IProductRegistryRestrictionItem> GetProductRegistryRestrictions()
		{
			var productRegistryRestrictionCategoryList = new Collection<IProductRegistryRestrictionItem>();

			var categoryId = GetIdOrNull();
			if (categoryId.HasValue)
			{
				var productRegistryRestrictionCategorySecureService = IoC.Resolve<IProductRegistryRestrictionCategorySecureService>();

				var restrictions = ProductRegistryRestriction.GetRestrictions();
				foreach (var restriction in restrictions)
				{
					var productRegistryRestrictionCategory = productRegistryRestrictionCategorySecureService.Create();
					productRegistryRestrictionCategory.ProductRegistryId = restriction.Key;
					productRegistryRestrictionCategory.RestrictionReason = restriction.Value;
					productRegistryRestrictionCategory.ItemId = categoryId.Value;
					productRegistryRestrictionCategory.UserId = SignInHelper.SignedInSystemUser.Id;

					productRegistryRestrictionCategoryList.Add(productRegistryRestrictionCategory);
				}
			}

			return productRegistryRestrictionCategoryList;
		}

		private void UpdateProductRegistryProduct(Dictionary<int, int> oldRestrictions, Dictionary<int, int> newRestrictions, int categoryId)
		{
			var toRemoveRestrictions = new Dictionary<int, int>();
			var toAddRestrictions = new Dictionary<int, int>();

			foreach (var oldRestriction in oldRestrictions)
			{
				if (!newRestrictions.ContainsKey(oldRestriction.Key))
				{
					toRemoveRestrictions.Add(oldRestriction.Key, oldRestriction.Key);
				}
			}

			foreach (var newRestriction in newRestrictions)
			{
				if (!oldRestrictions.ContainsKey(newRestriction.Key))
				{
					toAddRestrictions.Add(newRestriction.Key, newRestriction.Key);
				}
			}

			var productRegistryProductSecureService = IoC.Resolve<IProductRegistryProductSecureService>();

			if (toAddRestrictions.Count > 0)
			{
				productRegistryProductSecureService.DeleteByCategory(SignInHelper.SignedInSystemUser, categoryId, toAddRestrictions.Keys.ToList());
			}

			if (toRemoveRestrictions.Count > 0)
			{
				productRegistryProductSecureService.Insert(SignInHelper.SignedInSystemUser, null, null, categoryId, toRemoveRestrictions.Keys.ToList());
			}
		}

		// Icons.
		private void PopulateIcons(Collection<IIcon> icons)
		{
			var iconService = IoC.Resolve<IIconSecureService>();
			IconRepeater.DataSource = iconService.GetAll();
			IconRepeater.DataBind();

			foreach (RepeaterItem item in IconRepeater.Items)
			{
				var iconId = int.Parse(((HiddenField)item.FindControl("IconIdHidden")).Value, CultureInfo.CurrentCulture);
				var iconCheck = (CheckBox)item.FindControl("IconCheckBox");
				iconCheck.Checked = icons.Any(i => i.Id == iconId);
			}
		}

		private Collection<int> GetSelectedIcons()
		{
			var ids = new Collection<int>();
			foreach (RepeaterItem item in IconRepeater.Items)
			{
				var iconId = int.Parse(((HiddenField)item.FindControl("IconIdHidden")).Value, CultureInfo.CurrentCulture);
				var iconCheck = (CheckBox)item.FindControl("IconCheckBox");
				if (!iconCheck.Checked) continue;
				ids.Add(iconId);
			}
			return ids;
		}

		// Delivery Time.
		private void PopulateDeliveryTimes(ILekmerCategoryRecord category)
		{
			var service = IoC.Resolve<IDeliveryTimeSecureService>();
			var productRegistryDeliveryTimeList = category != null && category.DeliveryTimeId.HasValue
				? service.GetAllProductRegistryDeliveryTimeById(category.DeliveryTimeId.Value)
				: new Collection<IDeliveryTime>();

			var deliveryTime = productRegistryDeliveryTimeList.FirstOrDefault(dt => dt.ChannelId == ChannelHelper.CurrentChannel.Id);
			var from = string.Empty;
			var to = string.Empty;
			var format = string.Empty;
			if (deliveryTime != null)
			{
				from = deliveryTime.FromDays.HasValue ? deliveryTime.FromDays.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				to = deliveryTime.ToDays.HasValue ? deliveryTime.ToDays.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
				format = deliveryTime.Format ?? string.Empty;
			}

			DeliveryTimeFromBox.Text = from;
			DeliveryTimeToBox.Text = to;
			DeliveryTimeFormatBox.Text = format;

			DeliveryTimeConfigurator.DefaultFromValueControlClientId = DeliveryTimeFromBox.ClientID;
			DeliveryTimeConfigurator.DefaultToValueControlClientId = DeliveryTimeToBox.ClientID;
			DeliveryTimeConfigurator.DefaultFormatValueControlClientId = DeliveryTimeFormatBox.ClientID;
			DeliveryTimeConfigurator.DefaultRegistryId = deliveryTime != null && deliveryTime.ProductRegistryid.HasValue ? deliveryTime.ProductRegistryid.Value : -1;
			DeliveryTimeConfigurator.BusinessObjectId = (category != null && category.DeliveryTimeId.HasValue) ? category.DeliveryTimeId.Value : 0;
			DeliveryTimeConfigurator.DataSource = productRegistryDeliveryTimeList;
			DeliveryTimeConfigurator.DataBind();
		}

		protected virtual bool ManageDeliveryTime(ILekmerCategoryRecord category, out int oldDeliveryTimeId)
		{
			bool isNeedDeleteDeliveryTime = false;
			oldDeliveryTimeId = 0;

			int? deliveryTimeId = category.DeliveryTimeId;
			var deliveryTimeList = DeliveryTimeConfigurator.GetDeliveryTimeList();
			bool isRegistryDeliveryTimeExist = deliveryTimeList.FirstOrDefault(dt => dt.FromDays != null || dt.ToDays != null || !string.IsNullOrEmpty(dt.Format)) != null;
			if (!deliveryTimeId.HasValue)
			{
				if (isRegistryDeliveryTimeExist)
				{
					deliveryTimeId = DeliveryTimeSecureService.Insert();
					foreach (var deliveryTime in deliveryTimeList)
					{
						DeliveryTimeSecureService.SaveProductRegistryDeliveryTime(deliveryTimeId.Value, deliveryTime.FromDays, deliveryTime.ToDays, deliveryTime.Format, deliveryTime.ProductRegistryid);
					}
					category.DeliveryTimeId = deliveryTimeId;
				}
			}
			else
			{
				if (isRegistryDeliveryTimeExist)
				{
					foreach (var deliveryTime in deliveryTimeList)
					{
						DeliveryTimeSecureService.SaveProductRegistryDeliveryTime(deliveryTimeId.Value, deliveryTime.FromDays, deliveryTime.ToDays, deliveryTime.Format, deliveryTime.ProductRegistryid);
					}
				}
				else
				{
					oldDeliveryTimeId = deliveryTimeId.Value;
					category.DeliveryTimeId = null;
					isNeedDeleteDeliveryTime = true;
				}
			}

			return isNeedDeleteDeliveryTime;
		}
	}

	[Serializable]
	public class CategoryState
	{
		private readonly Collection<CategoryRatingGroupState> _categoryRatingGroups;

		public Collection<CategoryRatingGroupState> CategoryRatingGroups
		{
			get { return _categoryRatingGroups; }
		}

		public CategoryState(Collection<CategoryRatingGroupState> categoryRatingGroups)
		{
			_categoryRatingGroups = categoryRatingGroups;
		}

		public Collection<ITagGroup> AllTagGroups { get; set; }
		public Collection<ITagGroup> SelectedTagGroups { get; set; }

		public Collection<IProductRegistryRestrictionItem> RegistryRestrictionCategories { get; set; }
	}

	[Serializable]
	public class CategoryRatingGroupState
	{
		private IRatingGroup _ratingGroup;
		private IRatingGroupCategory _ratingGroupCategory;

		public IRatingGroup RatingGroup
		{
			get { return _ratingGroup; }
		}

		public IRatingGroupCategory RatingGroupCategory
		{
			get { return _ratingGroupCategory; }
		}

		public CategoryRatingGroupState(IRatingGroup ratingGroup, IRatingGroupCategory ratingGroupCategory)
		{
			_ratingGroup = ratingGroup;
			_ratingGroupCategory = ratingGroupCategory;
		}
	}
}
