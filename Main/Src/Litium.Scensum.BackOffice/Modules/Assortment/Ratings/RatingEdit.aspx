﻿<%@ Page Language="C#" MasterPageFile="Ratings.Master" CodeBehind="RatingEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Ratings.RatingEdit" AutoEventWireup="true"%>
<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericMultilineTranslator" Src="~/UserControls/Translation/GenericMultilineTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="RatingItemForm" Src="~/UserControls/Assortment/RatingItemForm.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="RatingEditContent" runat="server" ContentPlaceHolderID="RatingPlaceHolder">
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	
	<script type="text/javascript">
		function confirmDelete() {
			return DeleteConfirmation("<%= Resources.RatingReview.Literal_RatingItem %>");
		}
	</script>

	<div>
		<div class="rating-groups">
			<div class="column">
				<div class="input-box">
					<span><%=Resources.General.Literal_Title%> *</span>
					<uc:GenericTranslator ID="RatingTitleTranslator" runat="server" />
					<br />
					<asp:TextBox ID="TitleTextBox" runat="server" />
				</div>

				<div class="input-box">
					<span><%=Resources.General.Literal_CommonName%> *</span>
					<br />
					<asp:TextBox ID="CommonNameTextBox" runat="server" />
				</div>
				
				<div class="input-box">
					<span><%=Resources.RatingReview.Literal_CommonForVariants%> *</span>
					<br />
					<asp:CheckBox ID="CommonForVariantsCheckBox" runat="server" />
				</div>
			</div>
			
			<div class="column">
				<div class="input-box">
					<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
					<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
				</div>

				<div class="input-box">
					<span><%=Resources.RatingReview.Literal_ApplyToRegistry%> *</span>
					<br />
					<asp:DropDownList ID="RatingRegistryList" runat="server" />
				</div>
			</div>
		</div>
		
		<div class="rating-groups">
			<div class="column">
				<div class="input-box">
					<span><%=Resources.General.Literal_Description%></span>
					<asp:ImageButton runat="server" ID="RatingDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
					<uc:GenericMultilineTranslator ID="RatingDescriptionTranslator" runat="server" />
					<br />
					<asp:TextBox ID="DescriptionTextBox" runat="server" TextMode="MultiLine" Rows="5" />
				</div>
			</div>
		</div>

		<br class="clear"/>

		<div style="margin-top: 10px;">
			<span class="assortment-header"><%=Resources.RatingReview.Literal_RatingItems%></span>
			<br class="clear" />
			<asp:UpdatePanel ID="RatingItemsUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<sc:GridViewWithCustomPager 
						ID="RatingItemsGrid"
						SkinID="grid"
						runat="server"
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>"
						Width="100%">

						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
								<ItemTemplate>
									<asp:LinkButton ID="TitleLink" runat="server" CommandName="EditRatingItem" CommandArgument='<%# Eval("Id") %>' Text='<%# GetRatingItemTitle(Eval("RatingItem.Title")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:RatingReview, Literal_Score%>">
								<ItemTemplate>
									<uc:LiteralEncoded ID="ScoreLiteral" runat="server" Text='<%# Eval("RatingItem.Score") %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteRatingItem" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>
				</ContentTemplate>
			</asp:UpdatePanel>

			<br class="clear" />

			<uc:RatingItemForm ID="RatingItemForm" runat="server" />
		</div>
	</div>

	<br class="clear" />

	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
	</div>
</asp:Content>