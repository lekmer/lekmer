﻿using System;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Stores
{
	public partial class StoresMaster : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CreateStoreButton.Click += OnStoreCreate;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			StoresToolBoxPanel.Text = Resources.Product.Literal_Stores;
		}

		protected virtual void OnStoreCreate(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Assortment.Store.GetCreateUrl());
		}
	}
}