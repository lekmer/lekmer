<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/RelationLists/RelationLists.Master" CodeBehind="RelationListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.RelationLists.RelationListEdit" %>
<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ProductSearch" Src="~/UserControls/Assortment/ProductSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductListControl" Src="~/UserControls/Assortment/ProductListControlRelation.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
			<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgRelationList" />
		<uc:MessageContainer ID="message" MessageType="Success" HideMessagesControlId="SaveButton"
                            runat="server" />
</asp:Content>
<asp:Content ID="RelationListEditContent" ContentPlaceHolderID="RelationListPlaceHolder" runat="server">
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<script type="text/javascript">		
		$(document).ready(function() { $('#tabs').tabs(); });		
	</script>
	
                  
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
	<div id="rl-block">
	<div id="top-wrapper-relations">
	    <asp:UpdatePanel ID="RelationListTitleLabelUpdatePanel" runat="server" UpdateMode="Conditional">
	        <Triggers>
	            <asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click" />
	        </Triggers>
	        <ContentTemplate>
	            <label class="relation-header">
	                <uc:LiteralEncoded ID="RelationListTitleLabel" runat="server" />
	            </label>
		        <br />
	        </ContentTemplate>
	    </asp:UpdatePanel>
		<div class="column-first">
			<div class="input-box">
				<span><%= Resources.Product.Literal_ListTitle %>&nbsp; *</span> &nbsp
			<asp:RequiredFieldValidator 
				runat="server" 
				ID="RelationListTitleValidator" 
				ControlToValidate="RelationListTitleTextBox"
				ValidationGroup="vgRelationList" 
				ErrorMessage="<%$ Resources:ProductMessage, RelationListTitleEmpty %>" 
				Display ="None"/>
			<br />
				<asp:TextBox ID="RelationListTitleTextBox" runat="server" MaxLength="50" />
			</div>
		</div>
		<div class="column">
			<div class="input-box">
				<span><%= Resources.Product.Literal_ChooseListType%></span><br />
				<asp:DropDownList ID="TypeList" runat="server" DataTextField="CommonName" DataValueField="Id" />
			</div>
		</div>		
	</div>
	
		<div class="product-horizontal-separator" >&nbsp;</div>
		
		<div id="tabs">
				<ul>
					<li><a href="#fragment-1"><span><%= Resources.Product.Literal_RelationList%></span></a></li>
					<li><a href="#fragment-2"><span><%= Resources.Product.Literal_UsedBy %></span></a></li>					
				</ul>
				<br clear="all" />
				<div class="relationlist-tabs-main-container">
					<div id="fragment-1">
						<div class="tabs-inner-tab">
							<uc:ProductListControl ID="RelationListProducts" runat="server" />
						</div>
					</div>
					<div id="fragment-2">
						<div class="tabs-inner-tab">
							<uc:ProductListControl ID="RelationListUsedTo" runat="server" />
						</div>
					</div>
			</div>
    </div>
        <br class="clear" /><br class="clear" />
        <div class="buttons left">
            <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('relation list');" />
        </div>
        <div class="buttons right">
            <uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" ValidationGroup="vgRelationList"
                runat="server" Text="Save" SkinID="DefaultButton" />
            <uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="Cancel"
                SkinID="DefaultButton" />
        </div>
	</div>
	</asp:Panel>
</asp:Content>