﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/Packages/Packages.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Packages.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Assortment/ProductSearchCriteria.ascx" %>

<asp:Content ID="PackagesTabContent" ContentPlaceHolderID="PackagesPlaceHolder" runat="server">
	<asp:UpdatePanel id="ValidationSummaryUpdatePanel" runat="server">
		<ContentTemplate>
			<div style="width:978px;">
				<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgPackageSearchCriteria" />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>

	<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="SearchButton">
		<div id="articles-search" class="left">
			<div >
				<label ID="SearchCaptionLabel" runat="server" class="assortment-header" ><%= Resources.General.Literal_Search %></label>
			</div>
			<div id="search-block-criteria" class="left clear">
				<uc:Search ID="SearchControl" ValidationGroup="vgPackageSearchCriteria" ShowEanCriteria="False" ShowSupplierArticleNumberCriteria="False" runat="server" />
			</div>
			<asp:UpdatePanel id="SearchUpdatePanel" runat="server">
				<ContentTemplate>
					<div class="articles-search-block">
						<div class="buttons right">
							<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SearchButton" Text="<%$ Resources:General, Button_Search %>" ValidationGroup="vgPackageSearchCriteria" runat="server" SkinID="DefaultButton" />
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<div class="product-horizontal-separator" >&nbsp;</div>

		<asp:UpdatePanel ID="PackagesUpdatePanel" runat="server" UpdateMode="Always">
			<ContentTemplate>
				<div class="rl-search-result">
					<sc:GridViewWithCustomPager
						ID="PackagesGrid"
						SkinID="grid"
						runat="server"
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>"
						DataSourceID="PackagesObjectDataSource"
						Width="100%"
						PagerSettings-Mode="NumericFirstLast">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ArtNo %>" ItemStyle-Width="10%">
								<ItemTemplate>
									<%# Eval("MasterProduct.ErpId")%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="Title" ItemStyle-Width="46%">
								<ItemTemplate>
									<uc:HyperLinkEncoded runat="server" ID="EditLink" Text='<%# Eval("MasterProduct.DisplayTitle")%>' NavigateUrl='<%# Litium.Scensum.BackOffice.Base.LekmerPathHelper.Package.GetEditUrl(int.Parse(Eval("PackageId").ToString())) %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Product,Literal_Category %>" ItemStyle-Width="23%">
								<ItemTemplate>
									<%# GetCategory((int)Eval("MasterProduct.CategoryId"))%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
								<ItemTemplate>
									<asp:Label id="StatusLabel" runat="server" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<asp:Label ID="TypeLabel" runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="7%" >
								<ItemTemplate>
									<asp:Label ID="PriceLabel" Text ="" runat="server"/>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="4%">
								<ItemTemplate>
									<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeletePackage" CommandArgument='<%# Eval("PackageId") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="Delete" OnClientClick="return DeleteConfirmation('package');" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>
				</div>
				<asp:ObjectDataSource ID="PackagesObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Packages.PackagesDataSource" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>