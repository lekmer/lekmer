﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockBrandListEdit : LekmerStatePageController<Collection<IBlockBrandListBrand>>, IEditor
	{
		private const int _moveUpStep = -15;
		private const int _moveDownStep = 15;
		private const int _ordinalsStep = 10;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockBrandListBrand> BrandsState
		{
			get
			{
				object brands = State;
				return brands as Collection<IBlockBrandListBrand>;
			}
			set
			{
				State = value;
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnLoad(e);
		}

		protected override void SetEventHandlers()
		{
			LinkNodeSelector.NodeCommand += OnLinkNodeSelectorCommand;

			SelectedBrandsRadio.CheckedChanged += OnBrandsRadioChanged;
			AllBrandsRadio.CheckedChanged += OnBrandsRadioChanged;

			BrandsGrid.RowCommand += BrandsGridRowCommand;
			BrandsGrid.PageIndexChanging += BrandsGridPageIndexChanging;

			RemoveSelectedButton.Click += RemoveSelectedClick;

			BrandsAddButton.Click += BrandsAddClick;
			BrandsAddOkButton.Click += BrandsAddOkClick;

			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			PopulateData();
			PopulateTranslation();

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.LekmerMessage.BlockBrandListEdit);
		}

		public void OnCancel(object sender, EventArgs e)
		{
			IBlockBrandList blockBrandList = IoC.Resolve<IBlockBrandListSecureService>().GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockBrandList.ContentNodeId));
		}

		public void OnSave(object sender, EventArgs e)
		{
			if (!ValidateOrdinals())
			{
				SystemMessageContainer.Add(Resources.LekmerMessage.BlockBrandList_OrdinalsIncorrect);
				return;
			}
			if (!ValidateCounts())
			{
				SystemMessageContainer.Add(Resources.LekmerMessage.BlockBrandList_CountsIncorrect);
				return;
			}

			var blockBrandListService = IoC.Resolve<IBlockBrandListSecureService>();
			var blockBrandList = blockBrandListService.GetById(GetBlockId());
			if (blockBrandList == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			BlockSetting.SetSettings(blockBrandList.Setting);
			int templateId;
			blockBrandList.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockBrandList.Title = BlockTitleTextBox.Text;
			blockBrandList.LinkContentNodeId = LinkNodeSelector.SelectedNodeId;
			blockBrandList.IncludeAllBrands = AllBrandsRadio.Checked;
			blockBrandListService.Save(SignInHelper.SignedInSystemUser, blockBrandList, BrandsState);
			if (blockBrandList.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlock);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		protected void OnLinkNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
		}

		private void BrandsGridRowCommand(object sender, CommandEventArgs e)
		{
			int brandId;
			if (!int.TryParse(e.CommandArgument.ToString(), out brandId))
			{
				brandId = 0;
			}
			switch (e.CommandName)
			{
				case "UpOrdinal":
					MoveBrand(brandId, _moveUpStep);
					break;
				case "DownOrdinal":
					MoveBrand(brandId, _moveDownStep);
					break;
				case "DeleteBrand":
					var brand = BrandsState.First(item => item.BrandId == brandId);
					BrandsState.Remove(brand);
					break;
				case "RefreshOrdinal":
					RefreshOrder();
					break;
			}
			DataBindBrandsGrid();
		}

		private void BrandsGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			BrandsGrid.PageIndex = e.NewPageIndex;
			DataBindBrandsGrid();
		}

		protected void RemoveSelectedClick(object sender, EventArgs e)
		{
			var ids = BrandsGrid.GetSelectedIds();
			if (ids.Count() == 0) return;

			foreach (var id in ids)
			{
				var brandId = id;
				var brand = BrandsState.First(item => item.BrandId == brandId);
				if (brand == null) continue;

				BrandsState.Remove(brand);
			}
			DataBindBrandsGrid();
		}

		protected void OnBrandsRadioChanged(object sender, EventArgs e)
		{
			DataBindBrandsGrid();
		}

		protected void BrandsAddClick(object sender, EventArgs e)
		{
			BrandsAddList.ClearSelection();
			BrandsAddPopup.Show();
		}

		protected void BrandsAddOkClick(object sender, EventArgs e)
		{
			var selectedIndices = BrandsAddList.GetSelectedIndices();
			foreach (var index in selectedIndices)
			{
				var item = BrandsAddList.Items[index];
				var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
				if (BrandsState.Any(b => b.BrandId == brandId)) continue;

				var blockBrand = IoC.Resolve<IBlockBrandListBrand>();
				blockBrand.BlockId = GetBlockId();
				blockBrand.BrandId = brandId;
				blockBrand.Title = item.Text;
				blockBrand.Ordinal = int.MaxValue;
				BrandsState.Add(blockBrand);
			}
			DataBindBrandsGrid();
		}

		protected void PopulateData()
		{
			var blockBrandListService = IoC.Resolve<IBlockBrandListSecureService>();
			var blockBrandList = blockBrandListService.GetById(GetBlockId());

			BlockTitleTextBox.Text = blockBrandList.Title;
			BlockSetting.Setting = blockBrandList.Setting;

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockBrandList");
			TemplateList.DataBind();
			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			var item = TemplateList.Items.FindByValue(blockBrandList.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			if (blockBrandList.IncludeAllBrands)
			{
				AllBrandsRadio.Checked = true;
			}
			else
			{
				SelectedBrandsRadio.Checked = true;
			}

			var blockBrandListBrandService = IoC.Resolve<IBlockBrandListBrandSecureService>();
			State = blockBrandListBrandService.GetAllByBlock(GetBlockId());
			DataBindBrandsGrid();
			DataBindBrandsAddList();
			DataBindLinkNodeSelector(blockBrandList.LinkContentNodeId);
		}

		private void DataBindBrandsGrid()
		{
			if (AllBrandsRadio.Checked)
			{
				BrandsDiv.Visible = false;
				return;
			}
			BrandsDiv.Visible = true;
			BrandsState = Sort(BrandsState);
			BrandsGrid.DataSource = BrandsState;
			BrandsGrid.DataBind();
			SetClientFunction();
		}

		private void DataBindBrandsAddList()
		{
			var brandService = IoC.Resolve<IBrandSecureService>();
			BrandsAddList.DataSource = brandService.GetAll();
			BrandsAddList.DataBind();
		}

		protected void DataBindLinkNodeSelector(int? nodeId)
		{
			LinkNodeSelector.SelectedNodeId = nodeId;
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
			LinkNodeSelector.PopulatePath(nodeId);
		}

		protected void RefreshOrder()
		{
			IBlockBrandListBrand brand;
			foreach (GridViewRow row in BrandsGrid.Rows)
			{
				int brandId = int.Parse(((HiddenField)row.FindControl("IdHidden")).Value, CultureInfo.CurrentCulture);
				brand = BrandsState.FirstOrDefault(b => b.BrandId == brandId);
				if (brand == null) continue;
				brand.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalBox")).Text, CultureInfo.CurrentCulture);
			}
			DataBindBrandsGrid();
		}

		protected void MoveBrand(int elementId, int step)
		{
			var brand = BrandsState.FirstOrDefault(b => b.BrandId == elementId);
			if (brand == null) return;

			brand.Ordinal += step;
			DataBindBrandsGrid();
		}
      
		protected static Collection<IBlockBrandListBrand> Sort(Collection<IBlockBrandListBrand> list)
		{
			var brands = new List<IBlockBrandListBrand>(list);
			brands.Sort(delegate(IBlockBrandListBrand element1, IBlockBrandListBrand element2)
			{
				if (element1.Ordinal < element2.Ordinal)
					return -1;
				if (element1.Ordinal > element2.Ordinal)
					return 1;
				return 0;
			});
			for (int i = 0; i < brands.Count; i++)
			{
				brands[i].Ordinal = (i + 1) * _ordinalsStep;
			}
			return new Collection<IBlockBrandListBrand>(brands);
		}

		protected bool ValidateOrdinals()
		{
			Page.Validate("OrdinalsValidationGroup");
			return Page.IsValid;
		}

		protected bool ValidateCounts()
		{
			Page.Validate("vgCount");
			return Page.IsValid;
		}

		protected void PopulateTranslation()
		{
			int blockId = GetBlockId();
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected void SetClientFunction()
		{
			if (BrandsGrid.HeaderRow == null) return;
			var selectAllCheckBox = (CheckBox)BrandsGrid.HeaderRow.FindControl("SelectAllCheckBox");
			selectAllCheckBox.Checked = false;
			selectAllCheckBox.Attributes.Add("onclick",
				"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + BrandsGrid.ClientID + @"'); ShowBulkUpdatePanel('"
				+ selectAllCheckBox.ID + "', '" + BrandsGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			foreach (GridViewRow row in BrandsGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('"
					+ cbSelect.ID + "', '" + BrandsGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
	}
}
