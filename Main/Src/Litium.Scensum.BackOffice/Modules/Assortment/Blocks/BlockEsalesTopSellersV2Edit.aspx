﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="BlockEsalesTopSellersV2Edit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockEsalesTopSellersV2Edit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Src="~/UserControls/Tree/SelectTreeView.ascx" TagName="SelectTreeView" %>
<%@ Register TagPrefix="uc" TagName="ProductSearch" Src="~/UserControls/Assortment/ProductSearch.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockTimeLimiter" Src="~/UserControls/SiteStructure/BlockTimeLimiter.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockTargetDevice" Src="~/UserControls/SiteStructure/BlockTargetDevice.ascx" %>
<%@ Register TagPrefix="uc" TagName="SiteStructureNodeSelector" Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>

<asp:Content ID="Content" ContentPlaceHolderID="MessageContainer" runat="server">
	<script type="text/javascript">
		function confirmDeleteCategories() {
			return DeleteConfirmation("<%= Resources.Campaign.Literal_CategoriesConfirmRemove %>");
		}
		function confirmDeleteProducts() {
			return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
		}
		function SelectAllBelow(mainCheckBoxId, gridId) {
			$("#" + gridId).find("input[id$='SelectCheckBox']").attr('checked', $("#" + mainCheckBoxId).attr('checked'));
		}
	</script>

	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValdationSummary" DisplayMode="List" ValidationGroup="vgBlockEsalesTopSellersV2" />
</asp:Content>

<asp:Content ID="BlockEsalesTopSellersV2Content" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div>
			<div class="block-assortment">
				<div class="input-box">
					<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_Title%>" /></span>&nbsp;
					<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
					<asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" Display="None" ValidationGroup="vgBlockEsalesTopSellersV2" />
					<br />
					<asp:TextBox ID="BlockTitleTextBox" runat="server" />
				</div>

				<br class="clear" />

				<div>
					<asp:UpdatePanel ID="CategoriesUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:RadioButton ID="WholeRangeRadio" runat="server" Font-Bold="true" AutoPostBack="true" Text="<%$ Resources:TopList,Literal_WholeRange%>" GroupName="CategoriesRadioGroup" />
							<br />
							<asp:RadioButton ID="CategoriesRadio" runat="server" Font-Bold="true" AutoPostBack="true" Text="<%$ Resources:TopList,Literal_Categories%>" GroupName="CategoriesRadioGroup" />
							<br />
							<div id="CategoriesDiv" runat="server">
								<div>
									<sc:GridViewWithCustomPager
										ID="CategoriesGrid"
										SkinID="grid"
										runat="server"
										PageSize="<%$AppSettings:DefaultGridPageSize%>"
										AllowPaging="true"
										AutoGenerateColumns="false"
										Width="100%">
										<Columns>
											<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<HeaderTemplate>
													<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox ID="SelectCheckBox" runat="server" />
													<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Category.Id") %>' runat="server" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="72%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Category.Title")%>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:TopList, Literal_IncludeSubcategory %>" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="22%">
												<ItemTemplate>
													<asp:CheckBox ID="IncludeSubcategoryCheckBox" runat="server" Checked='<%# Eval("IncludeSubcategories")%>' AutoPostBack="true" OnCheckedChanged="OnIncludeSubcategoriesChanged" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<ItemTemplate>
													<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteCategory" CommandArgument='<%# Eval("Category.Id") %>'
														UseSubmitBehaviour="true" ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
														OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_Category + "\");" %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</sc:GridViewWithCustomPager>
								</div>
								<div id="CategoriesAllSelectedDiv" runat="server" style="display: none;">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoriesRemoveSelectedButton"
										Text="<%$Resources:General, Button_RemoveSelected %>" OnClientClick="return confirmDeleteCategories();"
										runat="server" SkinID="DefaultButton" />
								</div>
								<div class="right">
									<uc:ImageLinkButton ID="CategoriesShowPopupButton" UseSubmitBehaviour="true" runat="server"
										Text="<%$ Resources:General,Button_Add%>" SkinID="DefaultButton" CausesValidation="false" />
								</div>
								<br style="clear: both;" />
							</div>
							<ajaxToolkit:ModalPopupExtender ID="CategoriesPopup" runat="server" TargetControlID="CategoriesShowPopupButton"
								PopupControlID="CategoriesPopupPanel" CancelControlID="CategoriesCancelPopupButton"
								BackgroundCssClass="PopupBackground" />
							<div id="CategoriesPopupPanel" class="PopupContainer" style="display: none;" runat="server">
								<div class="PopupHeader">
									<span>
										<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Product,Label_AddCategoriesToBlock%>" />
									</span>
									<asp:Button ID="CategoriesCancelPopupButton" CausesValidation="false" runat="server" Text="X" />
								</div>
								<div class="PopupBody">
									<uc:SelectTreeView runat="server" ID="CategoriesTree" />
									<br />
									<uc:ImageLinkButton ID="CategoriesOkPopupButton" UseSubmitBehaviour="true" runat="server" Text="Ok" SkinID="DefaultButton" CausesValidation="false" CssClass="PopupButton" />
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>

				<div>
					<asp:UpdatePanel ID="ProductsUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="left">
								<asp:Label ID="ProductsPositionLabel" runat="server" Font-Bold="true" Text="<%$ Resources:TopList,Literal_ProductForcePosition%>" />
							</div>
							<br />
							<div>
								<sc:GridViewWithCustomPager
									ID="ProductsGrid"
									SkinID="grid"
									runat="server"
									PageSize="<%$AppSettings:DefaultGridPageSize%>"
									AllowPaging="true"
									AutoGenerateColumns="false"
									Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<HeaderTemplate>
												<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox ID="SelectCheckBox" runat="server" />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Product.Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="40%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Product.DisplayTitle")%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Category %>" ItemStyle-Width="36%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# Eval("Product.ShortDescription")%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:TopList, Literal_Position %>" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="8%">
											<ItemTemplate>
												<asp:TextBox ID="PositionTextBox" runat="server" Text='<%# Eval("Position") %>' Width="20px" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteProduct" CommandArgument='<%# Eval("Product.Id") %>'
													UseSubmitBehaviour="true" ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
													OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_product + "\");" %>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</sc:GridViewWithCustomPager>
							</div>
							<div id="ProductsAllSelectedDiv" runat="server" style="display: none;">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductsRemoveSelectedButton" Text="<%$Resources:General, Button_RemoveSelected %>"
									OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton" />
							</div>
							<uc:ProductSearch runat="server" ID="ProductsSearch" />
							<br style="clear: both;" />
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>

				<br class="clear"/>

				<div>
					<uc:BlockTargetDevice runat="server" Id="TargetDevice"/>
				</div>

				<br class="clear" />

				<div id="Settings" runat="server">
					<label class="block-header"><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:General,Label_Settings%>" /></label>
					<fieldset>
						<div class="column">
							<div class="input-box">
								<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate%>" /></span>
								<br />
								<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_EsalesSubPanelName %>" /></span>
								<asp:RequiredFieldValidator 
									runat="server" 
									ID="EsalesSubPanelNameValidator"
									ControlToValidate="EsalesSubPanelNameTextBox" 
									ErrorMessage="<%$ Resources:LekmerMessage,EsalesSubPanelNameEmpty %>"
									Display ="None" ValidationGroup="vgBlockEsalesTopSellersV2" />
								<br />
								<asp:TextBox ID="EsalesSubPanelNameTextBox" runat="server" />
							</div>
						</div>
						<div class="column">
							<div class="input-box full-width">
								<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_WindowLastEsalesValue %>" /></span>
								<asp:RequiredFieldValidator 
									runat="server" 
									ID="WindowLastEsalesValueValidator"
									ControlToValidate="WindowLastEsalesValueTextBox" 
									ErrorMessage="<%$ Resources:LekmerMessage,WindowLastEsalesValueEmpty%>"
									Display ="None" ValidationGroup="vgBlockEsalesTopSellersV2" />
								<asp:RangeValidator
									runat="server"
									ID="WindowLastEsalesValueRangeValidator"
									ControlToValidate="WindowLastEsalesValueTextBox"
									Type="Integer"
									MinimumValue="1"
									MaximumValue="10000"
									ErrorMessage="<%$ Resources:LekmerMessage,WindowLastEsalesValueRange %>"
									Display="None" ValidationGroup="vgBlockEsalesTopSellersV2" />
								<br />
								<asp:TextBox ID="WindowLastEsalesValueTextBox" runat="server" />
							</div>
						</div>

						<br class="clear" />

						<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgBlockEsalesTopSellersV2"/>
					</fieldset>
				</div>

				<br class="clear" />
				
				<div>
					<label class="block-header">Custom URL type</label>
					<fieldset>
						<div class="column">
							<div class="input-box">
								<span><asp:Literal runat="server" Text="<%$ Resources:Lekmer,Literal_UrlTitle%>" /></span>&nbsp;
								<uc:GenericTranslator ID="UrlTitleTranslator" runat="server" />&nbsp;
								<br />
								<asp:TextBox ID="UrlTitleTextBox" runat="server"></asp:TextBox>
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<asp:RadioButton ID="CustomUrl" runat="server" GroupName="CustomUrl"/>
								<span><asp:Literal runat="server" Text= "<%$ Resources:Lekmer,Literal_CustomUrl%>"/></span>
								<asp:TextBox ID="CustomUrlTexBox" runat="server" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<asp:RadioButton ID="NavLink" runat="server" GroupName="CustomUrl" />
								<span><asp:Literal runat="server" Text= "<%$ Resources:Lekmer,Literal_NavigationLink%>"/></span>
								<uc:SiteStructureNodeSelector ID="LinkNodeSelector" runat="server" AllowClearSelection="True" />
							</div>
						</div>
					</fieldset>
				</div>

				<br class="clear" />

				<div>
					<uc:BlockTimeLimiter runat="server" Id="TimeLimiter"/>
				</div>
			</div>
			<br />
			<div class="right">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>"
					SkinID="DefaultButton" ValidationGroup="vgBlockEsalesTopSellersV2" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>"
					SkinID="DefaultButton" CausesValidation="false" />
			</div>
		</div>
	</asp:Panel>
</asp:Content>