﻿<%@ Page
	Language="C#"
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockBestRatedProductListEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockBestRatedProductListEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<%@ Register TagPrefix="uc" TagName="RatingSelector" Src="~/UserControls/Assortment/RatingSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>
<%@ Register TagPrefix="sc" TagName="CategoryNodeSelector" Src="~/UserControls/Tree/CategoryNodeSelector.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" runat="server" ValidationGroup="vgBlockBestRatedProductList" />
</asp:Content>

<asp:Content ID="EditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><%=Resources.General.Literal_Title%>&nbsp;*</span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator runat="server" 
				ID="BlockTitleValidator" 
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$Resources:GeneralMessage, TitleEmpty %>" 
				Display ="None" 
				ValidationGroup="vgBlockBestRatedProductList" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" runat="server" />
		</div>

		<div style="margin-left: 5px;">
			<span class="bold"><%=Resources.RatingReview.Literal_Rating%></span>
			<br class="clear" />
			<asp:UpdatePanel ID="RatingUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<sc:GridViewWithCustomPager 
						runat="server" ID="RatingGrid"
						SkinID="grid"
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>"
						Width="60%">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
								<ItemTemplate>
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									<uc:HyperLinkEncoded ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetRatingEditUrl(Eval("Id")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
								<ItemTemplate>
									<uc:LiteralEncoded ID="CommonNameLiteral" runat="server" Text='<%# Eval("CommonName") %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Path">
								<ItemTemplate>
									<asp:Label ID="PathLabel" runat="server" Text='<%# GetRatingPath(Eval("RatingFolderId")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteRating" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>

					<br />

					<div class="left">
						<uc:RatingSelector ID="RatingSelector" runat="server" DenyMultipleSelection="True" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<br class="clear" />

		<div class="content-box">
			<span class="bold"><%= Resources.Product.Literal_Category %></span>
			<sc:CategoryNodeSelector ID="CategoryNodeSelector" runat="server" Width="300" />
		</div>

		<asp:UpdatePanel ID="BrandListUpdatePanel" runat="server" UpdateMode="Conditional">                                
			<ContentTemplate>
				<div class="content-box">
					<div class="user-roles-list-box" style="width:185px;">
						<span class="bold"><%= Resources.Lekmer.Literal_AvailableBrands %></span>
						<br />
						<asp:ListBox id="AvailableBrandsListBox" DataTextField="Title" DataValueField="Id"
							Rows="10"
							Width="180px"
							SelectionMode="Multiple"
							runat="server">
						</asp:ListBox>
					</div>
					<div class="user-roles-button" style="width:38px;">
						<uc:ImageLinkButton ID="AddBrandsButton" runat="server" Text=">>" SkinID="DefaultButton" />
						<br />
						<br />
						<uc:ImageLinkButton ID="RemoveBrandsButton" runat="server" Text="<<" SkinID="DefaultButton" />
					</div>
					<div class="user-roles-list-box" style="width:220px;">
						<span class="bold"><%= Resources.Lekmer.Literal_SelectedBrands %></span>
						<br />
						<asp:ListBox id="SelectedBrandsListBox" DataTextField="Title" DataValueField="Id"
							Rows="10"
							Width="180px"
							SelectionMode="Multiple"
							runat="server">
						</asp:ListBox>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>

		<br clear="all" />

		<div class="content-box">
			<span class="bold"><%= Resources.General.Label_Settings %></span>
			<br />
			<div class="column">
				<div class="input-box">
					<span><%=Resources.General.Literal_ChooseTemplate%></span>
					<br />
					<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" width="130px" />
				</div>
			</div>

			<br class="clear" />
			<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgBlockBestRatedProductList" />
		</div>

		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgBlockBestRatedProductList" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>

	</asp:Panel>
</asp:Content>