﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockBestRatedProductListEdit : LekmerStatePageController<BestRatedProductListState>, IEditor
	{
		private Collection<IRatingFolder> _allRatingFolders;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.RatingReview.Literal_BlockBestRatedProductListEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			CategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
			AddBrandsButton.Click += AddBrandsClick;
			RemoveBrandsButton.Click += RemoveBrandsClick;

			RatingGrid.RowCommand += OnRowCommand;
			RatingGrid.PageIndexChanging += OnPageIndexChanging;
			RatingSelector.SelectEvent += OnRatingsAdd;
		}

		protected override void PopulateForm()
		{
			int blockId = GetBlockId();
			var block = IoC.Resolve<IBlockBestRatedProductListSecureService>().GetById(blockId);

			InitializeState(block);

			BlockTitleTextBox.Text = block.Title;
			BlockSetting.Setting = block.Setting;
			PopulateRating(block.RatingId);
			PopulateCategory(block.CategoryId);
			PopulateBrands(State.SelectedBrands);
			PopulateTemplateList(block);
			PopulateTranslation(blockId);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var block = IoC.Resolve<IBlockBestRatedProductListSecureService>().GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockBestRatedProductListSecureService>();

			int blockId = GetBlockId();
			var block = blockService.GetById(blockId);
			if (block == null)
			{
				throw new BusinessObjectNotExistsException(blockId);
			}

			var rating = GetRating();

			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			block.Title = BlockTitleTextBox.Text;
			block.CategoryId = CategoryNodeSelector.SelectedNodeId;
			block.RatingId = rating != null ? rating.RatingId : (int?) null;
			BlockSetting.SetSettings(block.Setting);
			block.BlockBestRatedProductListBrands = GetSelectedBrands(blockId);

			blockService.Save(SignInHelper.SignedInSystemUser, block);
			if (block.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockBestRatedProductListSavedSuccessfully, InfoType.Success);
			}
		}

		protected virtual void OnCategoryNodeCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);

			CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(nodeId);
			CategoryNodeSelector.DataBind();
		}

		protected virtual void AddBrandsClick(object sender, EventArgs e)
		{
			Collection<IBrand> brandsList = State.SelectedBrands;

			foreach (ListItem item in AvailableBrandsListBox.Items)
			{
				if (item.Selected)
				{
					var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					var brand = State.AllBrands.FirstOrDefault(b => b.Id == brandId);
					if (brand != null)
					{
						brandsList.Add(brand);
					}
				}
			}

			State.SelectedBrands = brandsList;

			PopulateBrands(brandsList);
		}

		protected virtual void RemoveBrandsClick(object sender, EventArgs e)
		{
			Collection<IBrand> brandsList = State.SelectedBrands;

			foreach (ListItem item in SelectedBrandsListBox.Items)
			{
				if (item.Selected)
				{
					var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
					var brand = brandsList.FirstOrDefault(b => b.Id == brandId);
					if (brand != null)
					{
						brandsList.Remove(brand);
					}
				}
			}

			State.SelectedBrands = brandsList;

			PopulateBrands(brandsList);
		}

		// Ratings grid.

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteRating":
					var ratingToDelate = State.RatingList.FirstOrDefault(r => r.Id == id);
					if (ratingToDelate != null)
					{
						State.RatingList.Remove(ratingToDelate);
						PopulateRatingGrid();
					}
					break;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGrid.PageIndex = e.NewPageIndex;

			PopulateRatingGrid();
		}

		protected virtual void OnRatingsAdd(object sender, UserControls.Assortment.Events.RatingSelectEventArgs e)
		{
			var rating = e.Ratings[0];

			if (State.RatingList.FirstOrDefault(r => r.Id == rating.Id) == null)
			{
				State.RatingList.Clear();
				State.RatingList.Add(rating);
				PopulateRatingGrid();
			}
		}

		protected virtual void PopulateRating(int? ratingId)
		{
			if (State.RatingList == null)
			{
				State.RatingList = new List<IRating>();
			}

			if (ratingId.HasValue)
			{
				IRating rating = IoC.Resolve<IRatingSecureService>().GetById(ratingId.Value);
				State.RatingList.Add(rating);
			}

			PopulateRatingGrid();
		}

		protected virtual void PopulateRatingGrid()
		{
			RatingGrid.DataSource = State.RatingList;
			RatingGrid.DataBind();

			RatingUpdatePanel.Update();
		}

		protected virtual string GetRatingEditUrl(object ratingId)
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				int blockId = GetBlockId();

				return LekmerPathHelper.Rating.GetEditUrlForBlockBestRatedProductListEdit(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture), id.Value, blockId);
			}

			return LekmerPathHelper.Rating.GetEditUrl(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture));
		}

		protected virtual string GetRatingPath(object folderId)
		{
			if (_allRatingFolders == null)
			{
				var service = IoC.Resolve<IRatingFolderSecureService>();
				_allRatingFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allRatingFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual IBlockRating GetRating()
		{
			IBlockRating blockRating = null;

			if (State.RatingList.Count > 0)
			{
				blockRating = IoC.Resolve<IBlockRating>();
				blockRating.BlockId = GetBlockId();
				blockRating.RatingId = State.RatingList[0].Id;
			}

			return blockRating;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void InitializeState(IBlockBestRatedProductList block)
		{
			if (State == null)
			{
				State = new BestRatedProductListState
				{
					AllBrands = new Collection<IBrand>(),
					SelectedBrands = new Collection<IBrand>(),
					RatingList = new List<IRating>()
				};
			}

			State.AllBrands = IoC.Resolve<IBrandSecureService>().GetAll();
			foreach (var brand in block.BlockBestRatedProductListBrands.Select(b => b.Brand))
			{
				State.SelectedBrands.Add(brand);
			}
		}

		protected virtual void PopulateCategory(int? id)
		{
			if (id != null)
			{
				CategoryNodeSelector.SelectedNodeId = id;
			}

			CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(id);
			CategoryNodeSelector.DataBind();
			CategoryNodeSelector.PopulatePath(id);
		}

		protected virtual void PopulateBrands(Collection<IBrand> stateSelectedBrands)
		{
			var allBrands = State.AllBrands;

			var availableBrands = new Collection<IBrand>();
			foreach (var brand in allBrands)
			{
				if (stateSelectedBrands.FirstOrDefault(b => b.Id == brand.Id) == null)
				{
					availableBrands.Add(brand);
				}
			}

			AvailableBrandsListBox.DataSource = availableBrands;
			AvailableBrandsListBox.DataBind();

			SelectedBrandsListBox.DataSource = stateSelectedBrands;
			SelectedBrandsListBox.DataBind();

			BrandListUpdatePanel.Update();
		}

		protected virtual void PopulateTemplateList(IBlockBestRatedProductList block)
		{
			TemplateList.DataSource = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockBestRatedProductList");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");

			TemplateList.Items.Insert(0, listItem);

			var item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected virtual Collection<IBlockBestRatedProductListBrand> GetSelectedBrands(int blockId)
		{
			var blockBestRatedProductListBrand = new Collection<IBlockBestRatedProductListBrand>();

			foreach (var selectedBrand in State.SelectedBrands)
			{
				var bestRatedProductListBrand = IoC.Resolve<IBlockBestRatedProductListBrand>();
				bestRatedProductListBrand.Brand = selectedBrand;
				bestRatedProductListBrand.BlockId = blockId;

				blockBestRatedProductListBrand.Add(bestRatedProductListBrand);
			}

			return blockBestRatedProductListBrand;
		}
	}

	[Serializable]
	public sealed class BestRatedProductListState
	{
		public Collection<IBrand> AllBrands { get; set; }
		public Collection<IBrand> SelectedBrands { get; set; }
		public List<IRating> RatingList { get; set; }
	}
}