﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockTwosellRecommendEdit : LekmerPageController, IEditor
	{
		private IBlockList _block;
		private IBlockListSecureService _blockListSecureService;

		private IBlockList Block
		{
			get { return _block ?? (_block = BlockService.GetById(GetBlockId())); }
		}

		private IBlockListSecureService BlockService
		{
			get { return _blockListSecureService ?? (_blockListSecureService = IoC.Resolve<IBlockListSecureService>()); }
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void OnLoad(EventArgs e)
		{
			((Master.Start) (Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnLoad(e);

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.Lekmer.Literal_EditTwosellRecommendBlock);
		}

		protected override void PopulateForm()
		{
			PopulateData();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockList block = Block;
			if (block == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			var timeLimitErrors = TimeLimiter.TrySetTimeLimit(block);
			if (timeLimitErrors.Count > 0)
			{
				SystemMessageContainer.AddRange(timeLimitErrors, InfoType.Warning);
				return;
			}

			TargetDevice.SetTargetDevices(block);

			int templateId;

			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?) templateId : null;
			block.Title = BlockTitleTextBox.Text;

			BlockSetting.SetSettings(block.Setting);

			BlockService.Save(SignInHelper.SignedInSystemUser, block);

			if (block.Id == -1)
			{
				SystemMessageContainer.Add(GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(ProductSimilarList.Message_BlockProductSimilarListSavedSuccessfully, InfoType.Success);
			}
		}

		protected void PopulateData()
		{
			int blockId = GetBlockId();
			IBlockList block = BlockService.GetById(blockId);

			BlockTitleTextBox.Text = block.Title;

			PopulateTemplateList(block.TemplateId);
			
			BlockSetting.Setting = block.Setting;

			TimeLimiter.Block = block;

			TargetDevice.Block = block;

			PopulateTranslation(blockId);
		}

		protected virtual void PopulateTemplateList(int? templateId)
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();

			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockTwosellRecommend");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);

			if (templateId.HasValue)
			{
				var item = TemplateList.Items.FindByValue(templateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
	}
}
