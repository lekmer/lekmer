﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockBrandTopListEdit : LekmerStatePageController<BrandTopListState>, IEditor
	{
		private readonly Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			CategoriesRadio.CheckedChanged += OnCategoriesUseChanged;
			WholeRangeRadio.CheckedChanged += OnCategoriesUseChanged;
			CategoriesGrid.RowDataBound += OnCategoriesRowDataBound;
			CategoriesGrid.RowCommand += OnCategoriesRowCommand;
			CategoriesGrid.PageIndexChanging += OnCategoriesPageIndexChanging;
			CategoriesOkPopupButton.Click += OnCategoriesAdd;
			CategoriesRemoveSelectedButton.Click += OnCategoriesRemoveSelected;
			LinkNodeSelector.NodeCommand += OnLinkNodeSelectorCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			var masterPage = (Master).Master;
			if (masterPage != null)
			{
				var master = (Master.Start)masterPage.Master;
				if (master != null)
				{
					master.SetActiveTab("SiteStructure", "Pages");
				}
			}

			base.OnLoad(e);

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(BrandTopList.Literal_EditBrandTopListBlock);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockBrandTopListSecureService>();
			var block = blockService.GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockBrandTopListSecureService>();
			var block = blockService.GetById(GetBlockId());

			if (block == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;

			block.Title = BlockTitleTextBox.Text;
			block.IncludeAllCategories = WholeRangeRadio.Checked;
			block.OrderStatisticsDayCount = int.Parse(OrderStatisticsDayCountTextBox.Text, CultureInfo.CurrentCulture);
			block.LinkContentNodeId = LinkNodeSelector.SelectedNodeId;
			BlockSetting.SetSettings(block.Setting);

			if (block.IncludeAllCategories)
			{
				State.Categories.Clear();
			}

			block.BrandTopListBrands = State.Brands;
			block.BlockBrandTopListCategory = State.Categories;

			blockService.Save(SignInHelper.SignedInSystemUser, ChannelHelper.CurrentChannel, block);

			if (block.Id == -1)
			{
				SystemMessageContainer.Add(GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();

				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(BrandTopList.Message_BlockBrandTopListSavedSuccessfully, InfoType.Success);
			}
		}

		protected override void PopulateForm()
		{
			InitializeState();

			PopulateData();
			PopulateCategoriesTree();
			PopulateCategoriesGrid();
		}

		protected void InitializeState()
		{
			if (GetIdOrNull().HasValue)
			{
				var block = IoC.Resolve<IBlockBrandTopListSecureService>().GetById(GetBlockId());
				var blockCategories = block.BlockBrandTopListCategory;

				ExtendCategoryTitles(blockCategories);

				var blockBrands = block.BrandTopListBrands;

				State = new BrandTopListState(blockCategories, blockBrands);
			}
			else
			{
				State = new BrandTopListState();
			}
		}

		protected void PopulateData()
		{
			var block = IoC.Resolve<IBlockBrandTopListSecureService>().GetById(GetBlockId());

			// Populate Template List.
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockBrandTopList");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);

			if (block.TemplateId.HasValue)
			{
				var item = TemplateList.Items.FindByValue(block.TemplateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}

			// Populate Settings.
			BlockSetting.Setting = block.Setting;
			OrderStatisticsDayCountTextBox.Text = block.OrderStatisticsDayCount.ToString(CultureInfo.CurrentCulture);

			if (block.IncludeAllCategories)
			{
				WholeRangeRadio.Checked = true;
			}
			else
			{
				CategoriesRadio.Checked = true;
			}

			BlockTitleTextBox.Text = block.Title;

			PopulateTranslation(GetBlockId());

			DataBindLinkNodeSelector(block.LinkContentNodeId);
		}

		protected void PopulateCategoriesTree()
		{
			CategoriesTree.Selector = CategorySelector;
			CategoriesTree.DataBind();
		}

		protected void PopulateCategoriesGrid()
		{
			if (WholeRangeRadio.Checked)
			{
				CategoriesDiv.Visible = false;
				return;
			}

			CategoriesDiv.Visible = true;

			CategoriesGrid.DataSource = State.Categories;
			CategoriesGrid.DataBind();

			CategoriesGrid.Style.Add("display", CategoriesGrid.Rows.Count > 0 ? "table" : "none");
		}

		protected virtual void ExtendCategoryTitles(Collection<IBlockBrandTopListCategory> blockCategories)
		{
			if (blockCategories == null)
			{
				throw new ArgumentNullException("blockCategories");
			}

			foreach (var blockCategory in blockCategories)
			{
				if (blockCategory.Category != null)
				{
					blockCategory.Category.Title = GetCategoryPath(blockCategory.Category);
				}
			}
		}

		protected virtual string GetCategoryPath(ICategory category)
		{
			var path = new StringBuilder(category.Title);

			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");

				var parent = GetCategory(category.ParentCategoryId.Value);

				path.Insert(0, parent.Title);

				category = parent;
			}

			return path.ToString();
		}

		protected virtual ICategory GetCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);

			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);

				_categoriesPool.Add(category);
			}

			return category;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected void DataBindLinkNodeSelector(int? nodeId)
		{
			LinkNodeSelector.SelectedNodeId = nodeId;
			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
			LinkNodeSelector.PopulatePath(nodeId);
		}

		private static Collection<INode> CategorySelector(int? id)
		{
			var categorySecureService = IoC.Resolve<ICategorySecureService>();
			return categorySecureService.GetTree(id);
		}

		// Categories grid events.

		protected void OnCategoriesRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			switch (row.RowType)
			{
				case DataControlRowType.Header:
					{
						var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
						selectAllCheckBox.Checked = false;
						selectAllCheckBox.Attributes.Add(
							"onclick",
							"javascript:SelectAllBelow('"
								+ selectAllCheckBox.ClientID + @"','"
								+ CategoriesGrid.ClientID
								+ @"'); ShowBulkUpdatePanel('"
									+ selectAllCheckBox.ID + "', '"
									+ CategoriesGrid.ClientID + "', '"
									+ CategoriesAllSelectedDiv.ClientID + @"');");
					}
					break;

				case DataControlRowType.DataRow:
					{
						var selectAllCheckBox = (CheckBox)CategoriesGrid.HeaderRow.FindControl("SelectAllCheckBox");
						var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
						cbSelect.Attributes.Add(
							"onclick",
							"javascript:UnselectMain(this,'"
								+ selectAllCheckBox.ClientID
								+ @"'); ShowBulkUpdatePanel('"
									+ cbSelect.ID + "', '"
									+ CategoriesGrid.ClientID + "', '"
									+ CategoriesAllSelectedDiv.ClientID + @"');");
					}
					break;
			}
		}

		protected virtual void OnCategoriesRemoveSelected(object sender, EventArgs e)
		{
			var ids = CategoriesGrid.GetSelectedIds();

			if (!ids.Any())
			{
				return;
			}

			foreach (var id in ids)
			{
				var categoryId = id;
				var category = State.Categories.FirstOrDefault(c => c.Category.Id == categoryId);

				State.Categories.Remove(category);
			}

			PopulateCategoriesGrid();
		}

		protected void OnCategoriesRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName != "DeleteCategory")
			{
				return;
			}

			var categoryId = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
			var category = State.Categories.FirstOrDefault(c => c.Category.Id == categoryId);

			State.Categories.Remove(category);

			PopulateCategoriesGrid();
		}

		public virtual void OnCategoriesAdd(object sender, EventArgs e)
		{
			var categories = CategoriesTree.SelectedIds;

			if (categories.Count == 0)
			{
				return;
			}

			var categoryService = IoC.Resolve<ICategorySecureService>();
			foreach (var categoryId in categories)
			{
				var id = categoryId;
				if (State.Categories.FirstOrDefault(c => c.Category.Id == id) != null)
				{
					continue;
				}

				var blockCategory = IoC.Resolve<IBlockBrandTopListCategory>();
				blockCategory.BlockId = GetIdOrNull() ?? -1;
				blockCategory.Category = categoryService.GetById(id);
				blockCategory.Category.Title = GetCategoryPath(blockCategory.Category);

				State.Categories.Add(blockCategory);
			}

			PopulateCategoriesGrid();

			CategoriesTree.SelectedIds.Clear();
		}

		protected virtual void OnCategoriesUseChanged(object sender, EventArgs e)
		{
			PopulateCategoriesGrid();
		}

		protected virtual void OnIncludeSubcategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox)sender;
			var row = (GridViewRow)checkBox.Parent.Parent;
			var idHidden = (HiddenField)row.FindControl("IdHiddenField");
			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);
			var category = State.Categories.First(c => c.Category.Id == categoryId);

			category.IncludeSubcategories = checkBox.Checked;

			PopulateCategoriesGrid();
		}

		private void OnCategoriesPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			if (e != null)
			{
				CategoriesGrid.PageIndex = e.NewPageIndex;
			}

			PopulateCategoriesGrid();
		}

		// Content Node Selector events.

		protected void OnLinkNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);

			LinkNodeSelector.DataSource = Master.GetTreeNodes(nodeId, true);
			LinkNodeSelector.DataBind();
		}
	}

	[Serializable]
	public sealed class BrandTopListState
	{
		public BrandTopListState()
		{
			Categories = new Collection<IBlockBrandTopListCategory>();
			Brands = new Collection<IBlockBrandTopListBrand>();
		}

		public BrandTopListState(Collection<IBlockBrandTopListCategory> categories, Collection<IBlockBrandTopListBrand> brands)
		{
			Categories = categories;
			Brands = brands;
		}

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockBrandTopListCategory> Categories { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockBrandTopListBrand> Brands { get; set; }
	}
}