﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductRatingSummaryEdit : LekmerStatePageController<ProductRatingSummaryState>, IEditor
	{
		private Collection<IRatingFolder> _allRatingFolders;
		private Collection<IRatingGroupFolder> _allRatingGroupFolders;
		
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.RatingReview.Literal_BlockProductRatingSummaryEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;

			RatingsGrid.RowDataBound += OnRatingsRowDataBound;
			RatingsGrid.RowCommand += OnRowCommand;
			RatingsGrid.PageIndexChanging += OnPageIndexChanging;
			RemoveRatingsButton.Click += OnRatingsRemove;
			RatingSelector.SelectEvent += OnRatingsAdd;

			RatingGroupsGrid.RowDataBound += OnRatingGroupsRowDataBound;
			RatingGroupsGrid.RowCommand += OnRatingGroupsRowCommand;
			RatingGroupsGrid.PageIndexChanging += OnRatingGroupsPageIndexChanging;
			RemoveRatingGroupsButton.Click += OnRatingGroupsRemove;
			RatingGroupSelector.SelectEvent += OnRatingGroupsAdd;
		}

		protected override void PopulateForm()
		{
			int blockId = GetBlockId();

			var blockProductRatingSummary = IoC.Resolve<IBlockProductRatingSummarySecureService>().GetById(blockId);
			BlockTitleTextBox.Text = blockProductRatingSummary.Title;

			TemplateList.DataSource = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockProductRatingSummary");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");

			TemplateList.Items.Insert(0, listItem);

			var item = TemplateList.Items.FindByValue(blockProductRatingSummary.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}

			PopulateTranslation(blockId);

			InitializeState();

			PopulateRatings();

			PopulateRatingGroups();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockProductRatingSummary = IoC.Resolve<IBlockProductRatingSummarySecureService>().GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductRatingSummary.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductRatingSummarySecureService>();

			var blockProductRatingSummary = blockService.GetById(GetBlockId());
			if (blockProductRatingSummary == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockProductRatingSummary.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductRatingSummary.Title = BlockTitleTextBox.Text;
			blockProductRatingSummary.BlockRatings = GetRatings();
			blockProductRatingSummary.BlockRatingGroups = GetRatingGroups();

			blockService.Save(SignInHelper.SignedInSystemUser, blockProductRatingSummary);

			if (blockProductRatingSummary.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockProductRatingSummarySavedSuccessfully);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}


		// Ratings grid.

		protected virtual void OnRatingsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row, AllSelectedDivRating);
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			switch (e.CommandName)
			{
				case "DeleteRating":
					var ratingItemToDelate = State.RatingList.FirstOrDefault(r => r.Id == id);
					if (ratingItemToDelate != null)
					{
						State.RatingList.Remove(ratingItemToDelate);
						PopulateRatingsGrid();
					}
					break;
			}
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingsGrid.PageIndex = e.NewPageIndex;

			PopulateRatingsGrid();
		}

		protected virtual void OnRatingsAdd(object sender, UserControls.Assortment.Events.RatingSelectEventArgs e)
		{
			foreach (var rating in e.Ratings)
			{
				var clone = rating;

				if (State.RatingList.FirstOrDefault(r => r.Id == clone.Id) == null)
				{
					State.RatingList.Add(rating);
				}
			}

			PopulateRatingsGrid();
		}

		protected virtual void OnRatingsRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = RatingsGrid.GetSelectedIds();

			foreach (var id in ids)
			{
				var cloneId = id;
				var rating = State.RatingList.FirstOrDefault(r => r.Id == cloneId);
				if (rating != null)
				{
					State.RatingList.Remove(rating);
				}
			}

			PopulateRatingsGrid();
		}

		protected virtual void PopulateRatings()
		{
			if (State.RatingList == null)
			{
				State.RatingList = new List<IRating>();
			}

			var blockRatings = IoC.Resolve<IBlockRatingSecureService>().GetAllByBlock(GetBlockId());

			foreach (var blockRating in blockRatings)
			{
				IRating rating = IoC.Resolve<IRatingSecureService>().GetById(blockRating.RatingId);
				
				State.RatingList.Add(rating);
			}

			PopulateRatingsGrid();
		}

		protected virtual void PopulateRatingsGrid()
		{
			RatingsGrid.DataSource = State.RatingList;
			RatingsGrid.DataBind();

			RatingsUpdatePanel.Update();
		}

		protected virtual string GetRatingEditUrl(object ratingId)
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				int blockId = GetBlockId();

				return LekmerPathHelper.Rating.GetEditUrlForBlockProductRatingSummaryEdit(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture), id.Value, blockId);
			}

			return LekmerPathHelper.Rating.GetEditUrl(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture));
		}

		protected virtual string GetRatingPath(object folderId)
		{
			if (_allRatingFolders == null)
			{
				var service = IoC.Resolve<IRatingFolderSecureService>();
				_allRatingFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allRatingFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<IBlockRating> GetRatings()
		{
			int blockId = GetBlockId();

			Collection<IBlockRating> blockRatings = new Collection<IBlockRating>();

			var ratings = State.RatingList;
			foreach (var rating in ratings)
			{
				var blockRating = IoC.Resolve<IBlockRating>();
				blockRating.BlockId = blockId;
				blockRating.RatingId = rating.Id;

				blockRatings.Add(blockRating);
			}

			return blockRatings;
		}

		// Rating groups grid.

		protected virtual void OnRatingGroupsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row, AllSelectedDivRatingGroup);
		}

		protected virtual void OnRatingGroupsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteGroup")
			{
				var group = State.RatingGroupList.FirstOrDefault(rg => rg.Id == System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				if (group != null)
				{
					State.RatingGroupList.Remove(group);
					PopulateRatingGroupsGrid();
				}
			}
		}

		protected virtual void OnRatingGroupsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingGroupsGrid.PageIndex = e.NewPageIndex;
			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsAdd(object sender, UserControls.Assortment.Events.RatingGroupSelectEventArgs e)
		{
			foreach (var group in e.RatingGroups)
			{
				var clone = group;
				if (State.RatingGroupList.FirstOrDefault(rg => rg.Id == clone.Id) == null)
				{
					State.RatingGroupList.Add(group);
				}
			}

			PopulateRatingGroupsGrid();
		}

		protected virtual void OnRatingGroupsRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = RatingGroupsGrid.GetSelectedIds();

			foreach (var id in ids)
			{
				var cloneId = id;
				var group = State.RatingGroupList.FirstOrDefault(rg => rg.Id == cloneId);
				if (group != null)
				{
					State.RatingGroupList.Remove(group);
				}
			}

			PopulateRatingGroupsGrid();
		}

		protected virtual void PopulateRatingGroups()
		{
			if (State.RatingGroupList == null)
			{
				State.RatingGroupList = new List<IRatingGroup>();
			}

			var blockRatingGroups = IoC.Resolve<IBlockRatingGroupSecureService>().GetAllByBlock(GetBlockId());

			foreach (var blockRatingGroup in blockRatingGroups)
			{
				IRatingGroup ratingGroup = IoC.Resolve<IRatingGroupSecureService>().GetById(blockRatingGroup.RatingGroupId);

				State.RatingGroupList.Add(ratingGroup);
			}

			PopulateRatingGroupsGrid();
		}

		protected virtual void PopulateRatingGroupsGrid()
		{
			RatingGroupsGrid.DataSource = State.RatingGroupList;
			RatingGroupsGrid.DataBind();

			RatingGroupsUpdatePanel.Update();
		}

		protected virtual string GetRatingGropEditUrl(object groupId)
		{
			int? id = GetIdOrNull();

			if (id.HasValue)
			{
				int blockId = GetBlockId();

				return LekmerPathHelper.RatingGroup.GetEditUrlForBlockProductRatingSummaryEdit(System.Convert.ToInt32(groupId, CultureInfo.CurrentCulture), id.Value, blockId);
			}

			return LekmerPathHelper.RatingGroup.GetEditUrl(System.Convert.ToInt32(groupId, CultureInfo.CurrentCulture));
		}

		protected virtual string GetRatingGroupPath(object folderId)
		{
			if (_allRatingGroupFolders == null)
			{
				var service = IoC.Resolve<IRatingGroupFolderSecureService>();
				_allRatingGroupFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allRatingGroupFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row, HtmlGenericControl allSelectedDiv)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('"
					+ selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + allSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
					cbSelect.ID + "', '" + grid.ClientID + "', '" + allSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual Collection<IBlockRatingGroup> GetRatingGroups()
		{
			int blockId = GetBlockId();

			Collection<IBlockRatingGroup> blockRatingGroups = new Collection<IBlockRatingGroup>();

			var ratingGroups = State.RatingGroupList;
			foreach (var ratingGroup in ratingGroups)
			{
				var blockRatingGroup = IoC.Resolve<IBlockRatingGroup>();
				blockRatingGroup.BlockId = blockId;
				blockRatingGroup.RatingGroupId = ratingGroup.Id;

				blockRatingGroups.Add(blockRatingGroup);
			}

			return blockRatingGroups;
		}


		protected virtual void InitializeState()
		{
			if (State == null)
			{
				State = new ProductRatingSummaryState
					{
						RatingList = new List<IRating>(),
						RatingGroupList = new List<IRatingGroup>()
					};
			}
		}
	}

	[Serializable]
	public sealed class ProductRatingSummaryState
	{
		public List<IRating> RatingList { get; set; }
		public List<IRatingGroup> RatingGroupList { get; set; }
	}
}