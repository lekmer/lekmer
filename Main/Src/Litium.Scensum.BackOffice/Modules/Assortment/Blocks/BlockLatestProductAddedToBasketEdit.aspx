﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"CodeBehind="BlockLatestProductAddedToBasketEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockLatestProductAddedToBasketEdit" %>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgCount" />
	</asp:Content>
<asp:Content ID="cBPRL" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><asp:Literal ID="Literal1" runat="server" Text= "<%$ Resources:General,Literal_Title %>"/></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator 
				runat="server" 
				ID="BlockTitleValidator"
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"  
				Display ="None" ValidationGroup="vgCount" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br /><br />
			<span><asp:Literal ID="Literal2" runat = "server" Text="<%$ Resources:Product,Literal_ChooseDestinationUrl%>" /></span>
			<div class="input-box">
			
			 <asp:RadioButton GroupName="RedirectTo" ID="RedirectToCategoryRadioButton" runat="server" />
			    <span><asp:Literal ID="Literal6" runat = "server" Text="<%$ Resources:Product,Literal_RedirectToCategory%>" /></span>
               
                <br />
                <asp:RadioButton GroupName="RedirectTo" ID="RedirectToMainCategoryRadioButton" runat="server" />
                <span><asp:Literal ID="Literal5" runat = "server" Text="<%$ Resources:Product,Literal_RedirectToMainCategory%>" /></span>
                
                <br />
                  <asp:RadioButton GroupName="RedirectTo" ID="RedirectToStartPageRadioButton" runat="server" />  
			    <span><asp:Literal ID="Literal4" runat = "server" Text="<%$ Resources:Product,Literal_RedirectToStartPage%>" /></span>
               
              
			
			</div>
			<br />
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
						<span><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate %>" /></span><br />
						<asp:DropDownList ID="TemplateList"  runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" />
						</div>
					</div>
						<div class="column">
						
					</div>
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgCount" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>"  SkinID="DefaultButton" CausesValidation="false" />
		</div>
		</asp:Panel>
</asp:Content>
