﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockEsalesAdsEdit : LekmerPageController, IEditor
	{
		private IBlockEsalesAds _block;
		private IBlockEsalesAdsSecureService _blockEsalesAdsSecureService;

		private IBlockEsalesAds Block
		{
			get { return _block ?? (_block = BlockService.GetById(GetBlockId())); }
		}

		private IBlockEsalesAdsSecureService BlockService
		{
			get { return _blockEsalesAdsSecureService ?? (_blockEsalesAdsSecureService = IoC.Resolve<IBlockEsalesAdsSecureService>()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockEsalesRecommend);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var block = Block;
			BlockTitleTextBox.Text = block.Title;
			BlockSetting.Setting = block.Setting;
			EsalesPanelPathTextBox.Text = block.PanelPath;
			AdFormatTextBox.Text = block.Format;
			AdGenderTextBox.Text = block.Gender;
			AdProductCategoryTextBox.Text = block.ProductCategory;

			PopulateTemplateList();
			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockEsalesAds blockEsalesAds = BlockService.GetById(GetBlockId());

			if (blockEsalesAds == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockEsalesAds.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockEsalesAds.Title = BlockTitleTextBox.Text;
			BlockSetting.SetSettings(blockEsalesAds.Setting);

			blockEsalesAds.PanelPath = EsalesPanelPathTextBox.Text.HasValue() ? EsalesPanelPathTextBox.Text : null;
			blockEsalesAds.Format = AdFormatTextBox.Text.HasValue() ? AdFormatTextBox.Text : null;
			blockEsalesAds.Gender = AdGenderTextBox.Text.HasValue() ? AdGenderTextBox.Text : null;
			blockEsalesAds.ProductCategory = AdProductCategoryTextBox.Text.HasValue() ? AdProductCategoryTextBox.Text : null;

			BlockService.Save(SignInHelper.SignedInSystemUser, blockEsalesAds);
			if (blockEsalesAds.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductAvailCombine);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTemplateList()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockEsalesAds");
			TemplateList.DataBind();

			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);

			if (Block.TemplateId.HasValue)
			{
				ListItem item = TemplateList.Items.FindByValue(Block.TemplateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}