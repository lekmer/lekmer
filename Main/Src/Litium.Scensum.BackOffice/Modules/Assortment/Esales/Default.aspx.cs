using System;
using System.IO;
using System.Reflection;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Common;
using Litium.Lekmer.Esales.Connector;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using log4net;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Esales
{
	public partial class Default : LekmerPageController
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IEsalesSetting EsalesSetting { get; private set; }
		protected IEsalesConnector EsalesConnector { get; private set; }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			EsalesSetting = new EsalesSetting();
			EsalesConnector = new EsalesConnector(EsalesSetting, new SessionStateService());
		}

		protected override void SetEventHandlers()
		{
			ImportButton.Click += ImportButtonClick;
		}

		protected override void PopulateForm()
		{
		}


		protected virtual void ImportButtonClick(object sender, EventArgs e)
		{
			if (EsalesFileUpload.HasFile)
				try
				{
					_log.Info("Esales ads import started by - " + SignInHelper.SignedInSystemUser.UserName);

					SaveFile();
					SendToEsales();

					SystemMessageContainer.MessageType = InfoType.Information;
				}
				catch (Exception ex)
				{
					SystemMessageContainer.Add("ERROR: " + ex.Message);
					_log.Error(ex);
				}
			else
			{
				SystemMessageContainer.Add("You have not specified a file.");
			}
		}

		protected virtual void SaveFile()
		{
			string filePath = GetFilePath();
			string filePathLog = GetFilePath(EsalesFileUpload.FileName);

			EsalesFileUpload.SaveAs(filePath);
			EsalesFileUpload.SaveAs(filePathLog);

			string message = "File saved. File name: " +
				EsalesFileUpload.PostedFile.FileName +
				" Size: " +
				EsalesFileUpload.PostedFile.ContentLength + " kb" +
				" Content type: " +
				EsalesFileUpload.PostedFile.ContentType;

			SystemMessageContainer.Add(message);
			_log.Info(message);
		}

		protected virtual void SendToEsales()
		{
			_log.Info("Sending data to Esales is started.");

			string filePath = GetFilePath();

			if (File.Exists(filePath))
			{
				EsalesConnector.ImportAds(filePath);

				SystemMessageContainer.Add("Ads are sent to Esales.");
			}

			_log.Info("Sending data to Esales is completed.");
		}

		protected virtual string GetFilePath()
		{
			string dirr = EsalesSetting.DestinationDirectoryProduct;
			string name = EsalesSetting.XmlFileNameAds;

			return Path.Combine(dirr, name);
		}

		protected virtual string GetFilePath(string fileName)
		{
			string dirr = EsalesSetting.DestinationDirectoryProduct;
			string name = DateTime.Now.ToString("yyyyMMdd_HH-mm-ss_FFFFFFF") +" -- "+ fileName;

			return Path.Combine(dirr, name);
		}
	}
}