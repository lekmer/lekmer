﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IconMedia.ascx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.Icons.IconMedia" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>

<script type="text/javascript">
	function ResetDefaultMediaMessages() {
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
	}
</script>

<asp:UpdatePanel ID="ImagesUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<div>
			<div class="column">
				<div class="input-box">
					<span><%= Resources.Product.Literal_DefaultImage %> *</span><br />
					<asp:Image ID="DefaultImage" ImageUrl="~/Media/Images/Assortment/defaultProduct.jpg" runat="server" />
					<br />
					<uc:ImageLinkButton runat="server" ID="ImgBrowseButton" Text="<%$ Resources:Product,Button_ChangeImage %>" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();" />
					<uc:ImageLinkButton runat="server" ID="DeleteImageButton" Text="<%$ Resources:Product,Button_DeleteImage %>" SkinID="DefaultButton" />
				</div>
			</div>
			<ajaxToolkit:ModalPopupExtender
				ID="ImagesPopup" runat="server" TargetControlID="ImgBrowseButton" 
				PopupControlID="ImagesDiv" BackgroundCssClass="popup-background" 
				CancelControlID="_inpCloseImages" Y="100" X="229" 
				OnCancelScript="ResetDefaultMediaMessages();">
			</ajaxToolkit:ModalPopupExtender>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>

<div id="ImagesDiv" runat="server" class="product-popup-images-container" style="z-index: 10010; display: none;">
	<div id="product-popup-images-header">
		<div id="product-popup-images-header-left">
		</div>
		<div id="product-popup-images-header-center">
			<span><%= Resources.Product.Literal_AddImages %></span>
			<input type="button" id="_inpCloseImages" class="_inpClose" value="x" />
		</div>
		<div id="product-popup-images-header-right">
		</div>
	</div>
	<br clear="all" />
	<div class="product-popup-images-body">
		<uc:ImageSelect ID="ImageSelectControl" runat="server" />
	</div>
</div>