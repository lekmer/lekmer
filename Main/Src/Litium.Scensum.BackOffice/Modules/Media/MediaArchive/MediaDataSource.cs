using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.BackOffice.UserControls.Media;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public class MediaDataSource
	{
		private int _rowCount;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sortBy"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "sortByDesc"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "id")]
		public int SelectCount(int id, string sortBy, bool? sortByDesc)
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public static Collection<MediaListViewDataSource> GetMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public Collection<MediaListViewDataSource> GetMethod(int id, string sortBy, bool? sortByDesc, int maximumRows, int startRowIndex)
		{
			var dataSources = new Collection<MediaListViewDataSource>();
			var mediaService = IoC.Resolve<IMediaItemSecureService>();
			var mediaItems = mediaService.GetAllByFolder(id, maximumRows == 0 ? 0 : startRowIndex / maximumRows + 1, maximumRows, sortBy, sortByDesc);
			_rowCount = mediaItems.TotalCount;
            var mediaFormats = IoC.Resolve<IMediaFormatSecureService>().GetAll();

			foreach (var mediaItem in mediaItems)
			{
				var clone = mediaItem;
				var mediaFormat = mediaFormats.FirstOrDefault(i => i.Id == clone.MediaFormatId);
				var dataItem = new MediaListViewDataSource
				{
					Id = mediaItem.Id,
					DisplayText = mediaItem.Title,
					FormatExtension = mediaFormat.Extension,
					FileType = mediaFormat.Title
				};
				dataSources.Add(dataItem);
			}
			return dataSources;
		}
	}
}