using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public partial class CampaignsMaster : MasterPage
	{
		private const string SelectedNode = "SelectedCampaignFolderId";
		private const string BreadcrumbsSeparator = " > ";
		private const string CampaignTypeKey = "CampaignType";

		private ICampaignSecureService _campaignSecureService;
		public ICampaignSecureService CampaignSecureService
		{
			get { return _campaignSecureService ?? (_campaignSecureService = IoC.Resolve<ICampaignSecureService>()); }
		}

		private Collection<string> _breadcrumbAppend;

		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				if (_breadcrumbAppend == null)
				{
					_breadcrumbAppend = new Collection<string>();
				}
				return _breadcrumbAppend;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session[SelectedNode + CurrentCampaignType];
			}
			set
			{
				Session[SelectedNode + CurrentCampaignType] = value;
			}
		}

		public virtual bool DenySelection
		{
			get
			{
				return CampaignsTree.DenySelection;
			}
			set
			{
				CampaignsTree.DenySelection = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return CampaignsTree.RootNodeId;
			}
		}

		public virtual string RootNodeTitle
		{
			get
			{
				return CampaignsTree.RootNodeTitle;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			SearchButton.Click += OnSearch;
			CreateButton.Click += OnCampaignCreateFromPanel;
			CampaignsTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (SelectedFolderId == null)
			{
				SelectedFolderId = RootNodeId;
			}
			if (!IsPostBack)
			{
				PopulateTree(null);
				RestoreSearchFields();
				BuildBreadcrumbs();
			}
			SetupTabAndPanel();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
            
			if (CampaignsTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}
			ScriptManager.RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", CampaignsTree.ClientID), true);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{

			PopulateTree(e.Id);
            if (-1 == e.Id)
            {
                OnFolderCreate(sender, e);
            }
            else
            {
                switch (e.EventName)
                {
                    case "Expand":
                        DenySelection = true;
                        break;
                    case "Navigate":
                        SelectedFolderId = e.Id;
                        RedirectToDefaultPage();
                        break;
                }
            }
		}

		protected virtual void OnCampaignCreateFromPanel(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			RedirectWithType(PathHelper.Campaign.GetCreateUrl(CampaignsTree.RootNodeId));
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			var searchUrl = GetSearchPageUrl();
			SearchCriteriaState<string>.Instance.Save(searchUrl, SearchTextBox.Text);
			RedirectWithType(searchUrl);
		}

		protected virtual void OnFolderCreate(object sender, EventArgs e)
		{
			SelectedFolderId = CampaignsTree.MenuLastClickedNodeId;
			RedirectWithType(PathHelper.Campaign.GetFolderCreateUrl(SelectedFolderId.Value));
		}

		protected virtual void OnFolderEdit(object sender, EventArgs e)
		{
			SelectedFolderId = CampaignsTree.MenuLastClickedNodeId;
			RedirectWithType(PathHelper.Campaign.GetFolderEditUrl(SelectedFolderId.Value));
		}

		protected virtual void OnFolderDelete(object sender, EventArgs e)
		{
			var folderId = CampaignsTree.MenuLastClickedNodeId.Value;
			var service = IoC.Resolve<ICampaignFolderSecureService>();
			int? parentId = service.GetById(folderId).ParentFolderId;
            if (!service.TryDelete(SignInHelper.SignedInSystemUser, folderId))
			{
				SystemMessageContainer.Add(Resources.CampaignMessage.FolderDeleteFailed);
				return;
			}
			SystemMessageContainer.MessageType = InfoType.Success;
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);
			SelectedFolderId = parentId;
			PopulateTree(null);
			RedirectToDefaultPage();
		}

		protected virtual void OnCardinalitySet(object sender, EventArgs e)
		{
			SelectedFolderId = CampaignsTree.MenuLastClickedNodeId;
			RedirectWithType(PathHelper.Campaign.GetCardinalityUrl());
		}

		protected virtual void OnCampaignCreate(object sender, EventArgs e)
		{
			SelectedFolderId = CampaignsTree.MenuLastClickedNodeId;
			RedirectWithType(PathHelper.Campaign.GetCreateUrl(SelectedFolderId.Value));
		}

		public virtual void PopulateTree(int? folderId)
		{
			CampaignsTree.DataSource = GetNodes(folderId);
			CampaignsTree.RootNodeTitle = CurrentCampaignType == CampaignType.ProductCampaign ? Resources.Campaign.Literal_ProductCampaigns : Resources.Campaign.Literal_CartCampaigns;
			CampaignsTree.DataBind();
			LeftUpdatePanel.Update();
			CampaignsTree.SelectedNodeId = folderId ?? SelectedFolderId;
		}

		public virtual Collection<INode> GetNodes(int? folderId)
		{
			var id = folderId ?? ((SelectedFolderId == RootNodeId) ? null : SelectedFolderId);
			
			var service = IoC.Resolve<ICampaignFolderSecureService>();

			return CurrentCampaignType == CampaignType.ProductCampaign
				? service.GetProductCampaignFolderTree(id)
				: service.GetCartCampaignFolderTree(id);
		}

		public virtual void UpdateSelection(int nodeId)
		{
			CampaignsTree.SelectedNodeId = SelectedFolderId = nodeId;
			LeftUpdatePanel.Update();
		}

		protected virtual void RestoreSearchFields()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(GetSearchPageUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		protected virtual void BuildBreadcrumbs()
		{
			if (IsSearchResult)
			{
				Breadcrumbs.Text = Resources.General.Literal_SearchResults;
				return;
			}

			string breadcrumbs = string.Empty;

			if (SelectedFolderId.HasValue && SelectedFolderId.Value != RootNodeId)
			{
				var folders = GetNodes(SelectedFolderId);
                var folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
				while (folder != null)
				{
					breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", 
					                            BreadcrumbsSeparator, folder.Title, breadcrumbs);

					int? parentNodeId = folder.ParentId;
					folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
				}
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", 
						                            breadcrumbs, BreadcrumbsSeparator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", RootNodeTitle, breadcrumbs);
			Breadcrumbs.Text = breadcrumbs;
		}

		public virtual void UpdateBreadcrumbs(string value)
		{
			Breadcrumbs.Text = value;
		}

		public virtual void UpdateBreadcrumbs(int? folderId)
		{
			SelectedFolderId = folderId;
			BuildBreadcrumbs();
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		/// <summary>
		/// Get search page url according to the current campaign type
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual string GetSearchPageUrl()
		{
			return CurrentCampaignType == CampaignType.ProductCampaign
				? PathHelper.Campaign.GetSearchResultUrlForProductCampaigns()
				: PathHelper.Campaign.GetSearchResultUrlForCartCampaigns();
		}

		/// <summary>
		/// Return if the query string contains campaign type parameter
		/// </summary>
		public virtual bool IsCampaignTypeSupplyed()
		{
			return !string.IsNullOrEmpty(Request.QueryString.Get(CampaignTypeKey));
		}

		/// <summary>
		/// Get the current campaign type from the query string
		/// </summary>
		/// <exception cref="InvalidOperationException">When campaign type is not supplyed or can't be defined</exception>
		public virtual CampaignType CurrentCampaignType
		{
			get
			{
				string type = Request.QueryString[CampaignTypeKey];
				if (string.IsNullOrEmpty(type))
				{
					throw new InvalidOperationException("Query string must contain campaign type parameter");
				}
				if (type == CampaignType.ProductCampaign.ToString())
				{
					return CampaignType.ProductCampaign;
				}
				if (type == CampaignType.CartCampaign.ToString())
				{
					return CampaignType.CartCampaign;
				}
				throw new InvalidOperationException("Campaign type is not defined");
			}
		}

		/// <summary>
		/// Redirect response to the input url with appending the current campaign type as parameter
		/// </summary>
		public virtual void RedirectWithType(string url)
		{
			RedirectWithType(url, CurrentCampaignType);
		}

		/// <summary>
		/// Redirect response to the input url with appending the input campaign type as parameter
		/// </summary>
		public virtual void RedirectWithType(string url, CampaignType type)
		{
			Response.Redirect(GetUrlWithType(url, type));
		}

		/// <summary>
		/// Return the input url with appending the current campaign type as parameter
		/// </summary>
		public virtual string GetUrlWithType(string url)
		{
			return GetUrlWithType(url, CurrentCampaignType);
		}

		/// <summary>
		/// Return the input url with appending the input campaign type as parameter
		/// </summary>
		public virtual string GetUrlWithType(string url, CampaignType type)
		{
			var separator = url.Contains("?") ? '&' : '?';
			return string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}={3}", url, separator, CampaignTypeKey, type);
		}

		/// <summary>
		/// Redirect to the default page according to the current campaign type with appending it as parameter
		/// </summary>
		public virtual void RedirectToDefaultPage()
		{
			if (CurrentCampaignType == CampaignType.ProductCampaign)
			{
				RedirectWithType(PathHelper.Campaign.GetDefaultUrlForProductCampaigns(), CampaignType.ProductCampaign);
			}
			else
			{
				RedirectWithType(PathHelper.Campaign.GetDefaultUrlForCartCampaigns(), CampaignType.CartCampaign);
			}
		}

		/// <summary>
		/// Set the correct tab at top master and initialize titles at master panel according to the current campaign type
		/// </summary>
		public virtual void SetupTabAndPanel()
		{
			var topMaster = (Master.Start)(Master).Master;
			if (CurrentCampaignType == CampaignType.ProductCampaign)
			{
				topMaster.SetActiveTab("Campaigns", "ProductCampaigns");
				CampaignPanel.Text = Resources.Campaign.Literal_ProductCampaigns;
				CreateButton.Text = Resources.Campaign.Literal_ProductCampaign;
			}
			else
			{
				topMaster.SetActiveTab("Campaigns", "CartCampaigns");
				CampaignPanel.Text = Resources.Campaign.Literal_CartCampaigns;
				CreateButton.Text = Resources.Campaign.Literal_CartCampaign;
			}
		}
	}
}