﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Campaigns/CampaignsMaster.Master" CodeBehind="CampaignMove.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Campaigns.CampaignMove" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" TagPrefix="CustomControls" %>

<asp:Content ID="CampaignMoveContent" ContentPlaceHolderID="ProductCampaignsPlaceHolder" runat="server">
	<asp:Panel ID="MovePanel" runat="server" DefaultButton="SaveButton">
		<asp:UpdatePanel runat="server" ID="MoveUpdatePanel">
			<ContentTemplate>
				<span><%=Resources.Campaign.Literal_SelectFolder %></span>
				<br />
				<CustomControls:TemplatedTreeView runat="server" ID="CampaignsTree" DisplayTextControl="TitleLabel" 
					UseRootNode="false" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
					MainContainerCssClass="treeview-main-container" NodeMainContainerCssClass="treeview-node"
					NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"  
					NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
					NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
					MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
					<HeaderTemplate>
					</HeaderTemplate>
					<NodeTemplate>
						<div class="tree-item-cell-expand">
							<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
							<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
						</div>
						<div class="tree-item-cell-main">
							<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
							<asp:LinkButton ID="TitleLabel" runat="server" CommandName="Navigate"></asp:LinkButton>
						</div>
					</NodeTemplate>
				</CustomControls:TemplatedTreeView>
				<br /><br />
				<div class="buttons right">
					<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton"/>
					<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
				</div>
				<br /><br />
				<uc:MessageContainer ID="SystemMessageContainer" MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>
