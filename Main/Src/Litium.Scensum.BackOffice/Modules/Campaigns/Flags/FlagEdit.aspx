﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Campaigns/Flags/FlagsMaster.master" CodeBehind="FlagEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Campaigns.Flags.FlagEdit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessagePlaceHolder" runat="server">
	<div>
		<uc:MessageContainer ID="Messager" runat="server" MessageType="Warning" HideMessagesControlId="SetStatusButton" />
		<uc:ScensumValidationSummary ID="ValidatorSummary" runat="server" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="FlagValidationGroup" />
	</div>
</asp:Content>

<asp:Content ID="FlagContent" ContentPlaceHolderID="FlagsPlaceHolder" runat="server">
	<asp:Panel ID="FlagEditPanel" runat="server" DefaultButton="SaveButton">
		<div class="flag-edit-container">
			<div class="flag-edit-header">
				<uc:LiteralEncoded ID="FlagHeader" runat="server"></uc:LiteralEncoded>
			</div>
			<br style="clear: both;"/>
			<div class="column">
				<div class="column input-box">
					<div class="column input-box">
						<span><%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp;
						<uc:GenericTranslator ID="FlagTitleTranslator" runat="server" />&nbsp;
						<asp:RequiredFieldValidator runat="server" ID="FlagTitleValidator" ControlToValidate="FlagTitleBox"
							ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="FlagValidationGroup" />
						<br />
						<asp:TextBox ID="FlagTitleBox" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
					</div>
					<div class="column input-box" style="padding-top: 5px;">
						<span><%= Resources.Campaign.Literal_Class %>&nbsp;*</span>&nbsp;
						<asp:RequiredFieldValidator runat="server" ID="FlagClassValidator" ControlToValidate="FlagClassBox"
							ErrorMessage="<%$ Resources:CampaignMessage, ClassEmpty %>" Display="None" ValidationGroup="FlagValidationGroup" />
						<br />
						<asp:TextBox ID="FlagClassBox" runat="server" MaxLength="50" Width="400px"></asp:TextBox>
					</div>
				</div>
			</div>
			<br style="clear: both;"/>
			<div class="buttons right">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
					SkinID="DefaultButton" ValidationGroup="FlagValidationGroup" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
					SkinID="DefaultButton" />
			</div>
		</div>
	</asp:Panel>
</asp:Content>
