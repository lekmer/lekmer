using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.BackOffice.UserControls.Assortment;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.Modules.Tools.Cdon
{
	public class CdonExportConfiguratorProduct
	{
		private CdonExportConfiguratorHelper _cdonExportConfiguratorHelper;
		private CdonExportConfiguratorHelper CdonExportConfiguratorHelper
		{
			get { return _cdonExportConfiguratorHelper ?? (_cdonExportConfiguratorHelper = new CdonExportConfiguratorHelper()); }
		}

		public GridView Grid { get; set; }
		public ObjectDataSource DataSource { get; set; }
		public DropDownList GridPageSizeSelect { get; set; }
		public ProductSearchResult SearchResultControl { get; set; }
		public ProductSearchForm SearchFormControl { get; set; }
		public ImageLinkButton SearchButton { get; set; }
		public ImageLinkButton OkButton { get; set; }
		public ImageLinkButton AddAllButton { get; set; }
		public ImageLinkButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public Collection<ICdonExportRestrictionItem> Products { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;
			Grid.PageIndexChanging += GridPageIndexChanging;
			SearchButton.Click += SearchButtonClick;
			OkButton.Click += OkButtonClick;
			AddAllButton.Click += AddAllButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(Collection<ICdonExportRestrictionItem> products)
		{
			DataBindGrid(products);
		}

		public virtual void DataBindGrid(Collection<ICdonExportRestrictionItem> productIds)
		{
			Grid.PageSize = System.Convert.ToInt32(GridPageSizeSelect.SelectedValue);

			DataSource.SelectParameters.Clear();
			DataSource.SelectParameters.Add("maximumRows", GridPageSizeSelect.SelectedValue.ToString(CultureInfo.CurrentCulture));
			DataSource.SelectParameters.Add("startRowIndex", Grid.PageIndex.ToString(CultureInfo.CurrentCulture));
			DataSource.SelectParameters.Add("productIdsString", Foundation.Convert.ToStringIdentifierList(ConvertToBrandDictionary(productIds).Keys.ToList()));

			Grid.DataBind();
			GridUpdatePanel.Update();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CdonExportConfiguratorHelper.SetSelectionFunction((GridView) sender, e.Row, ApplyToAllDiv, false);
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				CdonExportConfiguratorHelper.PopulateChannels(e.Row, ((IProduct)e.Row.DataItem).Id);
			}
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				Update();
				RemoveProduct(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void GridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			Update();
			Grid.PageIndex = e.NewPageIndex;
			DataBindGrid(Products);
		}

		protected virtual void SearchButtonClick(object sender, EventArgs e)
		{
			if (SearchFormControl.Validate())
			{
				var criteria = SearchFormControl.CreateSearchCriteria();
				SearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			Update();

			var products = SearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (Products.FirstOrDefault(p => p.ItemId == product.Id) == null)
				{
					var cdonExportProduct = IoC.Resolve<ICdonExportRestrictionItem>();
					cdonExportProduct.ItemId = product.Id;

					Products.Add(cdonExportProduct);
				}
			}

			DataBindGrid(Products);

			if (IncludeAllCheckbox != null && products.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}
		}

		private void AddAllButtonClick(object sender, EventArgs e)
		{
			Update();

			if (SearchFormControl.Validate())
			{
				var productSecureService = IoC.Resolve<IProductSecureService>();

				var criteriaList = new List<IProductSearchCriteria>();
				var criteria = SearchFormControl.CreateSearchCriteria();
				criteriaList.Add(criteria);
				var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
				var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

				var products = productSecureService.AdvancedSearch(
					ChannelHelper.CurrentChannel.Id,
					searchCriteriaInclusive,
					searchCriteriaExclusive,
					1,
					int.MaxValue);

				foreach (var product in products)
				{
					if (Products.FirstOrDefault(p => p.ItemId == product.Id) == null)
					{
						var cdonExportProduct = IoC.Resolve<ICdonExportRestrictionItem>();
						cdonExportProduct.ItemId = product.Id;

						Products.Add(cdonExportProduct);
					}
				}

				DataBindGrid(Products);

				if (IncludeAllCheckbox != null && products.Count > 0)
				{
					IncludeAllCheckbox.Checked = false;
				}
			}
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			Update();

			foreach (var productId in GetSelectedFromGrid())
			{
				var cdonExportRestrictionProduct = Products.FirstOrDefault(p => p.ItemId == productId);
				if (cdonExportRestrictionProduct != null)
				{
					Products.Remove(cdonExportRestrictionProduct);
				}
			}

			DataBindGrid(Products);
		}

		protected virtual void RemoveProduct(int productId)
		{
			var cdonExportRestrictionProduct = Products.FirstOrDefault(p => p.ItemId == productId);
			if (cdonExportRestrictionProduct != null)
			{
				Products.Remove(cdonExportRestrictionProduct);
			}

			DataBindGrid(Products);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CdonExportConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}

		private Dictionary<int, int> ConvertToBrandDictionary(Collection<ICdonExportRestrictionItem> products)
		{
			return products == null || products.Count == 0 ? new Dictionary<int, int>() : products.ToDictionary(p => p.ItemId, p => p.ItemId);
		}

		public bool Update()
		{
			foreach (GridViewRow row in Grid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				var id = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var reason = ((TextBox)row.FindControl("reasonTextBox")).Text;

				var product = Products.FirstOrDefault(p => p.ItemId == id);
				if (product != null)
				{
					product.Reason = reason;

					// Channels.
					var channelsRepeater = (Repeater)row.FindControl("ChannelsRepeater");
					if (channelsRepeater != null)
					{
						if (product.Channels == null)
						{
							product.Channels = new Dictionary<int, int>();
						}
						else
						{
							product.Channels.Clear();
						}

						foreach (RepeaterItem item in channelsRepeater.Items)
						{
							var hfChannelId = (HiddenField)item.FindControl("hfChannelId");
							var cbChannel = (CheckBox)item.FindControl("cbChannel");
							int channelId;
							if (hfChannelId != null && cbChannel != null && int.TryParse(hfChannelId.Value, out channelId))
							{
								if (cbChannel.Checked && !product.Channels.ContainsKey(channelId))
								{
									product.Channels.Add(channelId, channelId);
								}
							}
						}
					}
				}
			}

			return true;
		}
	}
}