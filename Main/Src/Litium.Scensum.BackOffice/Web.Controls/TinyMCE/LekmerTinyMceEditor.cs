﻿using System.Diagnostics.CodeAnalysis;
using System.Web;

namespace Litium.Scensum.BackOffice.WebControls
{
	/// <summary>
	/// WYSIWYG editor that allows edit html text.
	/// </summary>
	[SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Mce")]
	public class LekmerTinyMceEditor : LekmerTextArea
	{
		private static string BackOfficeUrl
		{
			get
			{
				HttpContext context = HttpContext.Current;
				string protocol = context.Request.IsSecureConnection ? "https://" : "http://";

				string domain = context.Request.Url.Host;

				string directory = "/";
				if (!context.Request.ApplicationPath.Equals("/"))
				{
					directory = context.Request.ApplicationPath + "/";
				}

				return protocol + domain + directory;
			}
		}

		/// <summary>
		/// Gets or sets the html text.
		/// </summary>
		public new string Value
		{
			get
			{
				if (string.IsNullOrEmpty(base.Value))
				{
					return string.Empty;
				}
				return base.Value.Replace(BackOfficeUrl, "[Channel.BaseUrl]/");
			}
			set
			{
				base.Value = string.IsNullOrEmpty(value)
					? value
					: value.Replace("[Channel.BaseUrl]/", BackOfficeUrl);
			}
		}
	}
}
