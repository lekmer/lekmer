﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Lekmer.TwosellExport.Contract
{
	public interface ITwosellExportService
	{
		string GetOrderInfoFilePath();
		Collection<IOrderInfo> GetOrderInfoCollectionToExport();

		string GetProductInfoFilePath(IUserContext context);
		IEnumerable<IUserContext> GetUserContextForAllChannels();
		Collection<IProductImageGroup> GetAllowedImageGroups();
		Collection<IImageSize> GetAllowedImageSizes();
		Collection<ITagGroup> GetAllowedTagGroups();
		Dictionary<string, string> GetAllowedCategoryLevelUrls();
		Collection<ICampaign> GetCampaigns();
		Collection<IProductInfo> GetProductInfoCollectionToExport(IUserContext context);
	}
}