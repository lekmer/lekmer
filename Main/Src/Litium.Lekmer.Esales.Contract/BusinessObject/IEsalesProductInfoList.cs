﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesProductInfoList
	{
		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		int TotalCount { get; set; }

		void AddOrReplace(ProductIdCollection productIdCollection);
		void Add(IChannel channel, ProductCollection productCollection);

		Collection<IEsalesProductInfo> GetEsalesProductInfoCollection();
	}
}