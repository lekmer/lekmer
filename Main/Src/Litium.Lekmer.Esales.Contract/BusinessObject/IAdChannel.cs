﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public interface IAdChannel : IBusinessObjectBase
	{
		int Id { get; set; }
		int AdId { get; set; }
		int RegistryId { get; set; }

		string Title { get; set; }
		bool ShowOnSite { get; set; }
		string LandingPageUrl { get; set; }
		int? LandingPageId { get; set; }
		string ImageUrl { get; set; }
		int? ImageId { get; set; }
	}
}