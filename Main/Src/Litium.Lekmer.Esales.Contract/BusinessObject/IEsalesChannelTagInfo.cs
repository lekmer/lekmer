using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesChannelTagInfo
	{
		IChannel Channel { get; set; }

		ITag Tag { get; set; }
	}
}