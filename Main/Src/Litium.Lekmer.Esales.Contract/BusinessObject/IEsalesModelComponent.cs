﻿using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesModelComponent
	{
		int Id { get; set; }
		int ModelId { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		int Ordinal { get; set; }

		Dictionary<string, IEsalesModelParameter> Parameters { get; set; }

		IEsalesModel Model { get; set; }

		string TryGetParameterValue(string parameterName, string defaultValue);
	}
}
