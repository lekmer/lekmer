﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesRecommendationType : IBusinessObjectBase
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
	}
}
