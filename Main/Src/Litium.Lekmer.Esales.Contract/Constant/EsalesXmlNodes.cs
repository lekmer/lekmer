﻿namespace Litium.Lekmer.Esales
{
	public static class EsalesXmlNodes
	{
		public const string NodeOperations = "operations";
		public const string NodeRemove = "remove";
		public const string NodeClear = "clear";
		public const string NodeAdd = "add";
		public const string NodeProduct = "product";
		public const string NodeEntityType = "type";
		public const string NodeProductType = "product_type";
		public const string NodeSequenceNumber = "sequence_number";

		public const string NodeAvailableInChannels = "available_in_channels";
		public const string NodeProductKey = "product_key";
		public const string NodeProductErpId = "product_erp_id";
		public const string NodeProductStatus = "product_status";
		public const string NodeProductIsActive = "product_is_active";
		public const string NodeProductInStock = "product_in_stock";
		public const string NodeProductTitle = "product_title";
		public const string NodeSplittedProductTitle = "product_title_splitted";
		public const string NodeProductDescription = "product_description";
		public const string NodeProductShowVariants = "product_show_variants";
		public const string NodeProductEanCode = "product_ean_code";
		public const string NodeProductCreatedDate = "product_created_date";
		public const string NodeProductIsBookable = "product_is_bookable";
		public const string NodeProductOriginalPrice = "product_original_price";
		public const string NodeProductCurrentPrice = "product_current_price";
		public const string NodeProductDiscountPercent = "discount_percent";

		public const string NodeAgeFromMonth = "age_from_month";
		public const string NodeAgeToMonth = "age_to_month";
		public const string NodeIsNewFrom = "is_new_from";
		public const string NodeIsNewTo = "is_new_to";
		public const string NodeIsNew = "is_new";

		public const string NodeSupplierArticleNumber = "supplier_article_number";

		public const string NodeBrandId = "brand_id";
		public const string NodeBrandTitle = "brand_title";
		public const string NodeBrandDescription = "brand_description";

		public const string NodeCategoryId = "category_id";
		public const string NodeCategoryTitle = "category_title";
		public const string NodeParentCategoryId = "parent_category_id";
		public const string NodeParentCategoryTitle = "parent_category_title";
		public const string NodeMainCategoryId = "main_category_id";
		public const string NodeMainCategoryTitle = "main_category_title";

		public const string NodeTagId = "tag_id";
		public const string NodeTagTitle = "tag_title";
		public const string NodeTagCommonName = "tag_common_name";
		public const string NodeTagGroupId = "tag_group_id";

		public const string NodeSizeId = "size_id";
		public const string NodeSizeErpId = "size_erp_id";
		public const string NodeSizeStock = "size_stock";

		public const string NodeCampaignId = "campaign_id";
		public const string NodeCampaignTitle = "campaign_title";

		public const string NodeCustomerKey = "customer_key";

		public const string NodeFormat = "format";
		public const string NodeGender = "gender";
		public const string NodeProductCategory = "product_category";

		public const string ValueEntityTypeProduct = "product";
		public const string ValueEntityTypeTag = "tag";

		public const string ValueProductTypeProduct = "product";
		public const string ValueProductTypePackage = "package";

		public const string ValueTrue = "true";
		public const string ValueFalse = "false";

		public const string ValueDelimeter = ",";
		public const string TextDelimeter = "|";

		public const string ValueDescending = "desc";
		public const string ValueAscending = "asc";
		public const string ValueRelevance = "relevance";
		public const string ValuePersonal = "personal";
		public const string ValueSales = "sales";
		public const string ValueClicks = "clicks";
	}
}
