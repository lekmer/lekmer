﻿namespace Litium.Lekmer.Esales
{
	public interface IEsalesProductKeyService
	{
		string ComposeProductKey(int productId);
		string ComposeTagKey(int tagId);

		int ParseProductKey(string key);
		int ParseTagKey(string key);
	}
}