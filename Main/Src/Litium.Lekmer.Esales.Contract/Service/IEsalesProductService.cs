﻿using System.Collections.Generic;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesProductService
	{
		IEsalesProductInfoList GetNextProductListPortion(IPageTracker pageTracker, IEnumerable<IUserContext> userContexts);
		IEsalesProductInfoList GetNextProductListPortion(IEnumerable<IUserContext> userContexts, IEsalesProductInfoList productInfoList, ProductIdCollection productIdCollection);
		IEsalesTagInfoList GetTags(IEnumerable<IUserContext> userContexts);
	}
}