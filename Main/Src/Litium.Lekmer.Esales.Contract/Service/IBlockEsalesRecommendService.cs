using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesRecommendService
	{
		IBlockEsalesRecommend GetById(IUserContext context, int blockId);
	}
}