namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesSettingService
	{
		IBlockEsalesSetting GetByBlock(int blockId);
	}
}
