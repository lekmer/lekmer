using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersCategoryServiceV2
	{
		Collection<IBlockEsalesTopSellersCategoryV2> GetAllByBlock(IUserContext context, int blockId);
	}
}