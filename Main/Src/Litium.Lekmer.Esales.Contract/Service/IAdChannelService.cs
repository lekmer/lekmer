﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IAdChannelService
	{
		Collection<IAdChannel> GetAllByAd(int adId);
	}
}