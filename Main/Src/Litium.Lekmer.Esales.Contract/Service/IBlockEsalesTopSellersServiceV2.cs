using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersServiceV2
	{
		IBlockEsalesTopSellersV2 GetById(IUserContext context, int id);
		ProductCollection GetTopSellersFromEsalesResponse(IUserContext context, IBlockEsalesTopSellersV2 block, IPanelContent esalesGeneralResponse);
	}
}