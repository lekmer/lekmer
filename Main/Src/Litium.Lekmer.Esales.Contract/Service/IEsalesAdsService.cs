﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesAdsService
	{
		IAdHits FindAds(IUserContext context, string panelPath, string formatFilter, string genderFilter, string productCategotyFilter, string customerKey, int itemsToReturn);
	}
}