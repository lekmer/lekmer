﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IProductTicketPanelSetting : IEsalesPanelSetting
	{
		string ProductTicketPanelName { get; }
	}
}
