﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface ISearchFacetPanelSetting : IEsalesPanelSetting
	{
		string BrandFilterPanelName { get; }
		string MainCategoryFilterPanelName { get; }
		string ParentCategoryFilterPanelName { get; }
		string CategoryFilterPanelName { get; }
		string AgeFromMonthFilterPanelName { get; }
		string AgeToMonthFilterPanelName { get; }
		string PriceFilterPanelName { get; }
		string SizeFilterPanelName { get; }
		string TagFilterPanelName { get; }
	}
}