﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IEsalesSetting
	{
		bool Enabled { get; }

		string ApplicationName { get; }

		string ControllerUrl { get; }

		string DestinationDirectoryProduct { get; }
		string XmlFileNameAds { get; }
		string XmlFileNameProduct { get; }
		string XmlFileNameProductIncremental { get; }
		string ReplaceChannelNames { get; }

		int PageSize { get; }
		int BatchSize { get; }

		string GetChannelNameReplacement(string channelName);
		bool GetChannelNameReplacementExists(string channelName);
	}
}
