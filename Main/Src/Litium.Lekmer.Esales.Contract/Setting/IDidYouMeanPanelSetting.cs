﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IDidYouMeanPanelSetting : IEsalesPanelSetting
	{
		string DidYouMeanPanelName { get; }
	}
}
