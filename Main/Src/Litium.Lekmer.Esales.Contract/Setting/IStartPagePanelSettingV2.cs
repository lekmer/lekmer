﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IStartPagePanelSettingV2 : IEsalesPanelSetting
	{
		string TopSellersPanelName { get; }
		string PredefinedProductListPanelName { get; }
	}
}