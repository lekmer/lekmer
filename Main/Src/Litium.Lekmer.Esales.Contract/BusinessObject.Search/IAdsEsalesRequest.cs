using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	public interface IAdsEsalesRequest : IEsalesRequest
	{
		string Site { get; set; }
		string CustomerKey { get; set; }

		List<string> FormatList { get; set; }
		List<string> GenderList { get; set; }
		List<string> ProductCategoryList { get; set; }

		/// <summary>
		/// Limits the result.
		/// </summary>
		int MaxAds { get; set; }
	}
}