using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	public interface IEsalesRequest
	{
		/// <summary>
		/// Esales panel path
		/// </summary>
		string PanelPath { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		/// <returns></returns>
		Dictionary<string, string> ComposeArguments();
	}
}