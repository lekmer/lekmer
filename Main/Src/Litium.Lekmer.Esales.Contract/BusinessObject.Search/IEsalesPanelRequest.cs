using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	public interface IEsalesPanelRequest
	{
		/// <summary>
		/// Esales panel path
		/// </summary>
		string PanelPath { get; set; }

		/// <summary>
		/// Set of panel arguments
		/// </summary>
		Dictionary<string, string> PanelArguments { get; set; }
	}
}