﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IAdChannelSecureService
	{
		IAdChannel Create();

		IAdChannel Save(ISystemUserFull systemUserFull, IAdChannel adChannel);

		void DeleteAllByAd(ISystemUserFull systemUserFull, int adId);

		Collection<IAdChannel> GetAllByAd(int adId);
	}
}