﻿namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesSettingSecureService
	{
		IBlockEsalesSetting Create();
		void Save(IBlockEsalesSetting blockEsalesSetting);
		void Delete(int blockId);
	}
}