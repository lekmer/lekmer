﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Litium.Lekmer.Cleaner.Service
{
	[RunInstaller(true)]
	public partial class CleanerServiceHostInstaller : System.Configuration.Install.Installer
	{
		public CleanerServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}
