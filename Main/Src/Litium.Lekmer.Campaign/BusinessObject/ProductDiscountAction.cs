﻿using System;
using System.Collections.Generic;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class ProductDiscountAction : LekmerProductAction, IProductDiscountAction
	{
		private Dictionary<int, LekmerCurrencyValueDictionary> _productDiscountPrices;
		public Dictionary<int, LekmerCurrencyValueDictionary> ProductDiscountPrices
		{
			get { return _productDiscountPrices; }
			set
			{
				CheckChanged(_productDiscountPrices, value);
				_productDiscountPrices = value;
			}
		}

		public override bool Apply(IUserContext context, IProduct product)
		{
			LekmerCurrencyValueDictionary productDiscountPrice;
			if (!ProductDiscountPrices.TryGetValue(product.Id, out productDiscountPrice))
			{
				return false;
			}
			decimal discountValue;
			if (!productDiscountPrice.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			decimal priceIncludingVat = discountValue.Round();
			if (priceIncludingVat >= product.CampaignInfo.Price.IncludingVat)
			{
				return false;
			}

			var priceExcludingVat = (priceIncludingVat / (1 + product.Price.VatPercentage / 100)).Round();

			product.CampaignInfo.Price = new Price(priceIncludingVat, priceExcludingVat);

			((ILekmerProductCampaignInfo)product.CampaignInfo).UpdateAppliedActions(this);

			return true;
		}

		public override bool Verify(IProduct product)
		{
			return true; // Product type does not matter here
		}

		public override object[] GetInfoArguments()
		{
			return new object[]{};
		}


		protected override bool IsTargetType(IProduct product)
		{
			return true; //Do not need separate by product type, action has list of products to apply.
		}
	}
}
