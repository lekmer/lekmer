using System;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class FixedDiscountCartAction : LekmerCartAction, IFixedDiscountCartAction
	{
		private LekmerCurrencyValueDictionary _amounts;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, since the Amounts list is null or empty.");
			}

			return new object[]
				{
					Amounts.ElementAt(0).Value,
					Amounts.ElementAt(0).Key
				};
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			decimal discountValue;
			if (Amounts == null || !Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			return true;
		}

		public override Price ApplyDiscount(Price priceSummary, ICurrency currency)
		{
			if (priceSummary.IncludingVat <= 0 || priceSummary.ExcludingVat <= 0)
			{
				return priceSummary;
			}

			decimal discountValue;
			if (Amounts == null || !Amounts.TryGetValue(currency.Id, out discountValue))
			{
				return priceSummary;
			}

			decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;

			var priceIncludingVat = priceSummary.IncludingVat - discountValue;

			if (priceIncludingVat < 0)
			{
				priceIncludingVat = 0;
			}

			var priceExcludingVat = priceIncludingVat / (1 + vatPercentage);

			priceSummary = new Price(priceIncludingVat, priceExcludingVat);

			return priceSummary;
		}
	}
}