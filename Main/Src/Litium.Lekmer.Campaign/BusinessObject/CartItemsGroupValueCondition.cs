﻿using System;
using System.Linq;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartItemsGroupValueCondition : LekmerCondition, ICartItemsGroupValueCondition
	{
		private int _campaignConfigId;
		private ICampaignConfig _campaignConfig;
		private LekmerCurrencyValueDictionary _amounts;

		public int CampaignConfigId
		{
			get { return _campaignConfigId; }
			set
			{
				CheckChanged(_campaignConfigId, value);
				_campaignConfigId = value;
			}
		}
		public ICampaignConfig CampaignConfig
		{
			get { return _campaignConfig; }
			set
			{
				CheckChanged(_campaignConfig, value);
				_campaignConfig = value;
			}
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public override bool Fulfilled(IUserContext context)
		{
			if (Amounts == null)
			{
				throw new InvalidOperationException("The Amounts property must be set.");
			}

			if (CampaignConfig == null)
			{
				throw new InvalidOperationException("The CampaignConfig property must be set.");
			}
			CampaignConfig.Validate();

			ICartFull cart = context.Cart;
			if (cart == null)
			{
				return false;
			}

			decimal discountValue;
			if (!Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			decimal sum = 0;
			foreach (var item in cart.GetCartItems())
			{
				var product = (ILekmerProduct) item.Product;

				if (!IsTargetType(product)) continue; // skip
				if (!IsRangeMember(product)) continue; // skip

				sum += item.GetActualPriceSummary().IncludingVat;
			}

			return sum >= discountValue;
		}

		protected virtual bool IsRangeMember(ILekmerProduct product)
		{
			return CampaignConfig.IsRangeMember(product.Id, product.CategoryId, product.BrandId);
		}

		protected override bool IsTargetType(IProduct product)
		{
			return CampaignConfig.IsTargetType(product.Id) || base.IsTargetType(product);
		}

		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, " + "since the CartValues list is null or empty.");
			}

			return new object[]
			{
				Amounts.ElementAt(0).Value,
				Amounts.ElementAt(0).Key
			};
		}
	}
}