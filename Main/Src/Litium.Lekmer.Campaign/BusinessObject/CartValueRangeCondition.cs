﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartValueRangeCondition : Condition, ICartValueRangeCondition
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<CurrencyValueRange> CartValueRangeList { get; set; }

		public override bool Fulfilled(IUserContext context)
		{
			if (CartValueRangeList == null)
			{
				throw new InvalidOperationException("The CartValueRangeList property must be set.");
			}

			var cart = context.Cart;
			if (cart == null)
			{
				return false;
			}

			CurrencyValueRange currencyValueRange = CartValueRangeList.FirstOrDefault(cvr => cvr.CurrencyId == context.Channel.Currency.Id);

			if (currencyValueRange == null)
			{
				return false;
			}

			var actualPrice = cart.GetActualPriceSummary().IncludingVat;

			return actualPrice >= currencyValueRange.MonetaryValueFrom
				&& actualPrice <= currencyValueRange.MonetaryValueTo;
		}

		public override object[] GetInfoArguments()
		{
			if (CartValueRangeList == null || CartValueRangeList.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, " + "since the CartValueRangeList is null or empty.");
			}

			return new object[]
				{
					CartValueRangeList.ElementAt(0).MonetaryValueFrom,
					CartValueRangeList.ElementAt(0).CurrencyId,
					CartValueRangeList.ElementAt(0).MonetaryValueTo
				};
		}
	}
}