﻿using System;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class FixedDiscountAction : LekmerProductAction, IFixedDiscountAction
	{
		private bool _includeAllProducts;
		private LekmerCurrencyValueDictionary _amounts;
		private ProductIdDictionary _includeProducts;
		private ProductIdDictionary _excludeProducts;
		private CategoryIdDictionary _includeCategories;
		private CategoryIdDictionary _excludeCategories;
		private BrandIdDictionary _includeBrands;
		private BrandIdDictionary _excludeBrands;

		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CategoryIdDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CategoryIdDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public BrandIdDictionary IncludeBrands
		{
			get { return _includeBrands; }
			set
			{
				CheckChanged(_includeBrands, value);
				_includeBrands = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public BrandIdDictionary ExcludeBrands
		{
			get { return _excludeBrands; }
			set
			{
				CheckChanged(_excludeBrands, value);
				_excludeBrands = value;
			}
		}


		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, since the Amounts list is null or empty.");
			}

			return new object[]
				{
					Amounts.ElementAt(0).Value,
					Amounts.ElementAt(0).Key
				};
		}

		public override bool Apply(IUserContext context, IProduct product)
		{
			decimal discountValue;
			if (!Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			if (!Verify(product))
			{
				return false;
			}

			decimal priceIncludingVat = (product.Price.PriceIncludingVat - discountValue).Round();
			if (priceIncludingVat < 0)
			{
				priceIncludingVat = 0;
			}

			if (priceIncludingVat >= product.CampaignInfo.Price.IncludingVat)
			{
				return false;
			}

			decimal priceExcludingVat = (priceIncludingVat / (1 + product.Price.VatPercentage / 100)).Round();
			product.CampaignInfo.Price = new Price(priceIncludingVat, priceExcludingVat);
			((ILekmerProductCampaignInfo) product.CampaignInfo).UpdateAppliedActions(this);

			return true;
		}

		public override bool Verify(IProduct product)
		{
			return Validate()
				&& base.Verify(product)
				&& IsIncluded(product)
				&& !IsExcluded(product);
		}


		protected virtual bool Validate()
		{
			if (IncludeProducts == null)
			{
				throw new InvalidOperationException("The IncludeProducts property must be set.");
			}

			if (ExcludeProducts == null)
			{
				throw new InvalidOperationException("The ExcludeProducts property must be set.");
			}

			if (IncludeCategories == null)
			{
				throw new InvalidOperationException("The IncludeCategories property must be set.");
			}

			if (ExcludeCategories == null)
			{
				throw new InvalidOperationException("The ExcludeCategories property must be set.");
			}

			if (IncludeBrands == null)
			{
				throw new InvalidOperationException("The IncludeBrands property must be set.");
			}

			if (ExcludeBrands == null)
			{
				throw new InvalidOperationException("The ExcludeBrands property must be set.");
			}

			return true;
		}

		protected virtual bool IsIncluded(IProduct product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (IncludeAllProducts)
			{
				return true;
			}

			if (IncludeCategories.ContainsKey(product.CategoryId))
			{
				return true;
			}

			if (lekmerProduct.BrandId.HasValue
				&& IncludeBrands.ContainsKey(lekmerProduct.BrandId.Value))
			{
				return true;
			}

			if (IncludeProducts.ContainsKey(product.Id))
			{
				return true;
			}

			return false;
		}

		protected virtual bool IsExcluded(IProduct product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (ExcludeCategories.ContainsKey(product.CategoryId))
			{
				return true;
			}

			if (lekmerProduct.BrandId.HasValue
				&& ExcludeBrands.ContainsKey(lekmerProduct.BrandId.Value))
			{
				return true;
			}

			if (ExcludeProducts.ContainsKey(product.Id))
			{
				return true;
			}

			return false;
		}

		protected override bool IsTargetType(IProduct product)
		{
			// When product resides in "IncludeProducts" list explicitly, we ignore target options and return true !!!
			if (IncludeAllProducts == false)
			{
				if (IncludeProducts.ContainsKey(product.Id))
				{
					return true;
				}
			}

			return base.IsTargetType(product);
		}
	}
}