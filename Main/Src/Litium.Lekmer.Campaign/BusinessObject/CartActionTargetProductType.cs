﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartActionTargetProductType : BusinessObjectBase, ICartActionTargetProductType
	{
		private int _cartActionId;
		private int _productTypeId;

		public int CartActionId
		{
			get { return _cartActionId; }
			set
			{
				CheckChanged(_cartActionId, value);
				_cartActionId = value;
			}
		}

		public int ProductTypeId
		{
			get { return _productTypeId; }
			set
			{
				CheckChanged(_productTypeId, value);
				_productTypeId = value;
			}
		}
	}
}