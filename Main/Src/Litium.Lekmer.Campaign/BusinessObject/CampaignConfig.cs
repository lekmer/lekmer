﻿using System;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignConfig : BusinessObjectBase, ICampaignConfig
	{
		private int _id;
		private bool _includeAllProducts;

		private ProductIdDictionary _includeProducts;
		private ProductIdDictionary _excludeProducts;

		private CampaignCategoryDictionary _includeCategories;
		private CampaignCategoryDictionary _excludeCategories;

		private BrandIdDictionary _includeBrands;
		private BrandIdDictionary _excludeBrands;


		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CampaignCategoryDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CampaignCategoryDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public BrandIdDictionary IncludeBrands
		{
			get { return _includeBrands; }
			set
			{
				CheckChanged(_includeBrands, value);
				_includeBrands = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public BrandIdDictionary ExcludeBrands
		{
			get { return _excludeBrands; }
			set
			{
				CheckChanged(_excludeBrands, value);
				_excludeBrands = value;
			}
		}


		public virtual void Validate()
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");
			if (ExcludeProducts == null) throw new InvalidOperationException("The ExcludeProducts property must be set.");

			if (IncludeCategories == null) throw new InvalidOperationException("The IncludeCategories property must be set.");
			if (ExcludeCategories == null) throw new InvalidOperationException("The ExcludeCategories property must be set.");

			if (IncludeBrands == null) throw new InvalidOperationException("The IncludeBrands property must be set.");
			if (ExcludeBrands == null) throw new InvalidOperationException("The ExcludeBrands property must be set.");
		}
		public virtual bool IsRangeMember(int productId, int categoryId, int? brandId)
		{
			if (ExcludeCategories.ContainsKey(categoryId))
			{
				return false;
			}

			if (brandId.HasValue && ExcludeBrands.ContainsKey(brandId.Value))
			{
				return false;
			}

			if (ExcludeProducts.ContainsKey(productId))
			{
				return false;
			}

			if (IncludeAllProducts)
			{
				return true;
			}

			if (IncludeCategories.ContainsKey(categoryId))
			{
				return true;
			}

			if (brandId.HasValue && IncludeBrands.ContainsKey(brandId.Value))
			{
				return true;
			}

			if (IncludeProducts.ContainsKey(productId))
			{
				return true;
			}

			return false;
		}
		public virtual bool IsTargetType(int productId)
		{
			// When product resides in "IncludeProducts" list explicitly, we ignore target options and return true !!!
			return IncludeAllProducts == false && IncludeProducts.ContainsKey(productId);
		}
	}
}