﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartItemPercentageDiscountAction : LekmerCartAction, ICartItemPercentageDiscountAction
	{
		private decimal _discountAmount;
		private bool _includeAllProducts;
		private ProductIdDictionary _includeProducts;
		private ProductIdDictionary _excludeProducts;
		private CampaignCategoryDictionary _includeCategories;
		private CampaignCategoryDictionary _excludeCategories;
		private BrandIdDictionary _includeBrands;
		private BrandIdDictionary _excludeBrands;

		public decimal DiscountAmount
		{
			get { return _discountAmount; }
			set
			{
				if (value < 0 || value > 100)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				CheckChanged(_discountAmount, value);
				_discountAmount = value;
			}
		}

		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}

		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}

		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}

		public CampaignCategoryDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}

		public CampaignCategoryDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}

		public BrandIdDictionary IncludeBrands
		{
			get { return _includeBrands; }
			set
			{
				CheckChanged(_includeBrands, value);
				_includeBrands = value;
			}
		}

		public BrandIdDictionary ExcludeBrands
		{
			get { return _excludeBrands; }
			set
			{
				CheckChanged(_excludeBrands, value);
				_excludeBrands = value;
			}
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			AddVoucherDiscountTypeInfo(context, cart);

			bool isApplied = false;
			decimal appliedDiscount = 0;

			foreach (var cartItem in cart.GetCartItems())
			{
				if (!IsTargetType(cartItem.Product)) continue;

				if (!IsInRange(cartItem.Product)) continue;

				decimal priceIncludingVat = (cartItem.Product.CampaignInfo.Price.IncludingVat * (1 - DiscountAmount / 100)).Round();
				decimal priceExcludingVat = (priceIncludingVat / (1 + cartItem.Product.Price.VatPercentage / 100)).Round();

				appliedDiscount += (cartItem.Product.CampaignInfo.Price.IncludingVat - priceIncludingVat) * cartItem.Quantity;

				cartItem.Product.CampaignInfo.Price = new Price(priceIncludingVat, priceExcludingVat);
				isApplied = true;
			}

			AddVoucherDiscountValueInfo(context, cart, appliedDiscount);

			return isApplied;
		}
		
		public override object[] GetInfoArguments()
		{
			return new object[] { DiscountAmount };
		}

		protected virtual bool IsInRange(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");
			if (ExcludeProducts == null) throw new InvalidOperationException("The ExcludeProducts property must be set.");
			if (IncludeCategories == null) throw new InvalidOperationException("The IncludeCategories property must be set.");
			if (ExcludeCategories == null) throw new InvalidOperationException("The ExcludeCategories property must be set.");
			if (IncludeBrands == null) throw new InvalidOperationException("The IncludeBrands property must be set.");
			if (ExcludeBrands == null) throw new InvalidOperationException("The ExcludeBrands property must be set.");

			var lekmerProduct = (ILekmerProduct)product;
			return IsIncluded(lekmerProduct) && !IsExcluded(lekmerProduct);
		}

		protected virtual bool IsIncluded(ILekmerProduct product)
		{
			return IncludeAllProducts ||
				   IncludeCategories.ContainsKey(product.CategoryId) ||
				   (product.BrandId.HasValue && IncludeBrands.ContainsKey(product.BrandId.Value)) ||
				   IncludeProducts.ContainsKey(product.Id);
		}

		protected virtual bool IsExcluded(ILekmerProduct product)
		{
			return ExcludeCategories.ContainsKey(product.CategoryId) ||
				   (product.BrandId.HasValue && ExcludeBrands.ContainsKey(product.BrandId.Value)) ||
				   ExcludeProducts.ContainsKey(product.Id);
		}

		protected override bool IsTargetType(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");

			// When product resides in "IncludeProducts" list explicitly, we ignore target options and return true !!!
			if (IncludeAllProducts == false)
			{
				if (IncludeProducts.ContainsKey(product.Id))
				{
					return true;
				}
			}

			return base.IsTargetType(product);
		}

		private void AddVoucherDiscountTypeInfo(IUserContext context, ICartFull cart)
		{
			var voucher = ((ILekmerUserContext) context).Voucher;
			if (voucher != null && voucher.IsValid)
			{
				var lekmerCart = (ILekmerCartFull) cart;
				lekmerCart.VoucherInfo.DiscountType = voucher.ValueType;
			}
		}

		private void AddVoucherDiscountValueInfo(IUserContext context, ICartFull cart, decimal appliedDiscount)
		{
			var voucher = ((ILekmerUserContext) context).Voucher;
			if (voucher != null && voucher.IsValid)
			{
				((ILekmerCartCampaignInfo)cart.CampaignInfo).VoucherDiscountOnCartItemPercentageDiscountActionItems += appliedDiscount;
			}
		}
	}
}