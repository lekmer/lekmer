﻿using System;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class GiftCardViaMailProductAction : LekmerProductAction, IGiftCardViaMailProductAction
	{
		private int _sendingInterval;
		private int? _templateId;
		private int _campaignConfigId;
		private ICampaignConfig _campaignConfig;
		private LekmerCurrencyValueDictionary _amounts;

		public int SendingInterval
		{
			get { return _sendingInterval; }
			set
			{
				CheckChanged(_sendingInterval, value);
				_sendingInterval = value;
			}
		}
		public int? TemplateId
		{
			get { return _templateId; }
			set
			{
				CheckChanged(_templateId, value);
				_templateId = value;
			}
		}
		public int CampaignConfigId
		{
			get { return _campaignConfigId; }
			set
			{
				CheckChanged(_campaignConfigId, value);
				_campaignConfigId = value;
			}
		}
		public ICampaignConfig CampaignConfig
		{
			get { return _campaignConfig; }
			set
			{
				CheckChanged(_campaignConfig, value);
				_campaignConfig = value;
			}
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public override bool Apply(IUserContext context, IProduct product)
		{
			if (!Verify(product))
			{
				return false;
			}

			decimal discountValue;
			if (!Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			return true;
		}

		public override bool Verify(IProduct product)
		{
			if (CampaignConfig == null)
			{
				throw new InvalidOperationException("The CampaignConfig property must be set.");
			}
			CampaignConfig.Validate();

			return IsTargetType(product) && IsRangeMember((ILekmerProduct) product);
		}

		protected virtual bool IsRangeMember(ILekmerProduct product)
		{
			return CampaignConfig.IsRangeMember(product.Id, product.CategoryId, product.BrandId);
		}

		protected override bool IsTargetType(IProduct product)
		{
			return CampaignConfig.IsTargetType(product.Id) || base.IsTargetType(product);
		}

		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, since the Amounts list is null or empty.");
			}

			return new object[]
				{
					Amounts.ElementAt(0).Value,
					Amounts.ElementAt(0).Key
				};
		}
	}
}