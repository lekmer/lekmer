﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignLandingPage : BusinessObjectBase, ICampaignLandingPage
	{
		private int _campaignLandingPageId;
		public int CampaignLandingPageId
		{
			get { return _campaignLandingPageId; }
			set
			{
				CheckChanged(_campaignLandingPageId, value);
				_campaignLandingPageId = value;
			}
		}

		private int _campaignId;
		public int CampaignId
		{
			get { return _campaignId; }
			set
			{
				CheckChanged(_campaignId, value);
				_campaignId = value;
			}
		}

		private string _webTitle;
		public string WebTitle
		{
			get { return _webTitle; }
			set
			{
				CheckChanged(_webTitle, value);
				_webTitle = value;
			}
		}

		private string _description;
		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ICampaignRegistryCampaignLandingPage> RegistryDataList { get; set; }
	}
}