﻿using System;
using System.Linq;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Order;


namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerCartValueCondition : CartValueCondition, ILekmerCartValueCondition
	{
		private LekmerCurrencyValueDictionary _cartValues;
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public new LekmerCurrencyValueDictionary CartValues
		{
			get { return _cartValues; }
			set
			{
				CheckChanged(_cartValues, value);
				_cartValues = value;
			}
		}

		public override bool Fulfilled(IUserContext context)
		{
			if (CartValues == null) throw new InvalidOperationException("The CartValues property must be set.");
			ICartFull cart = context.Cart;
			if (cart == null)
			{
				return false;
			}

			decimal discountValue;
			if (!CartValues.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			return cart.GetActualPriceSummary().IncludingVat >= discountValue;
		}

		public override object[] GetInfoArguments()
		{
			if (CartValues == null || CartValues.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, " + "since the CartValues list is null or empty.");
			}
			return new object[]
				{
					CartValues.ElementAt(0).Value, 
					CartValues.ElementAt(0).Key
				};
		}
	}
}