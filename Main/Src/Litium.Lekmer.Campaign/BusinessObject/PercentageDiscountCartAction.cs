using System;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class PercentageDiscountCartAction : LekmerCartAction, IPercentageDiscountCartAction
	{
		private decimal _discountAmount;
		public decimal DiscountAmount
		{
			get { return _discountAmount; }
			set
			{
				CheckChanged(_discountAmount, value);
				_discountAmount = value;
			}
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			return true;
		}

		public override object[] GetInfoArguments()
		{
			return new object[] { DiscountAmount };
		}

		public override Price ApplyDiscount(Price priceSummary, ICurrency currency)
		{
			if (priceSummary.ExcludingVat <= 0)
			{
				return priceSummary;
			}

			if (DiscountAmount < 100)
			{
				decimal priceIncludingVat = priceSummary.IncludingVat;
				decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;

				priceIncludingVat = priceIncludingVat * (1 - DiscountAmount / 100);
				var priceExcludingVat = priceIncludingVat / (1 + vatPercentage);

				priceSummary = new Price(priceIncludingVat, priceExcludingVat);
			}

			return priceSummary;
		}
	}
}