﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	public class CampaignConfigService : ICampaignConfigService
	{
		protected CampaignConfigRepository Repository { get; private set; }
		protected CampaignActionRepository CampaignActionRepository { get; private set; }

		public CampaignConfigService(CampaignConfigRepository repository, CampaignActionRepository campaignActionRepository)
		{
			Repository = repository;
			CampaignActionRepository = campaignActionRepository;
		}

		public virtual ICampaignConfig GetById(int campaignConfigId)
		{
			Repository.EnsureNotNull();
			CampaignActionRepository.EnsureNotNull();

			ICampaignConfig campaignConfig = Repository.GetById(campaignConfigId);

			campaignConfig.IncludeProducts = CampaignActionRepository.GetIncludeProducts(campaignConfigId);
			campaignConfig.ExcludeProducts = CampaignActionRepository.GetExcludeProducts(campaignConfigId);

			campaignConfig.IncludeCategories = CampaignActionRepository.GetIncludeCategoriesRecursive(campaignConfigId);
			campaignConfig.ExcludeCategories = CampaignActionRepository.GetExcludeCategoriesRecursive(campaignConfigId);

			campaignConfig.IncludeBrands = CampaignActionRepository.GetIncludeBrands(campaignConfigId);
			campaignConfig.ExcludeBrands = CampaignActionRepository.GetExcludeBrands(campaignConfigId);

			campaignConfig.SetUntouched();

			return campaignConfig;
		}
	}
}
