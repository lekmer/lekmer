﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class CartValueRangeConditionService : IConditionPluginService
	{
		protected CartValueRangeConditionRepository Repository { get; private set; }

		public CartValueRangeConditionService(CartValueRangeConditionRepository repository)
		{
			Repository = repository;
		}

		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int conditionId).")]
		public virtual ICondition GetById(int conditionId)
		{
			throw new NotImplementedException();
		}

		public virtual ICondition GetById(IUserContext userContext, int conditionId)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var condition = Repository.GetById(conditionId);

			if (condition == null)
			{
				return null;
			}

			condition.CartValueRangeList = Repository.GetCartValueRangeByCondition(conditionId);

			return condition;
		}
	}
}