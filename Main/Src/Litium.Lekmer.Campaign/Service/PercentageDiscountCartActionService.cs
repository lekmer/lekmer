using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class PercentageDiscountCartActionService : ICartActionPluginService
	{
		protected PercentageDiscountCartActionRepository Repository { get; private set; }

		public PercentageDiscountCartActionService(PercentageDiscountCartActionRepository repository)
		{
			Repository = repository;
		}

		public virtual ICartAction GetById(int cartActionId)
		{
			throw new NotImplementedException();
		}

		public virtual ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(cartActionId);
		}
	}
}