﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	public class CartActionTargetProductTypeService : ICartActionTargetProductTypeService
	{
		protected CartActionTargetProductTypeRepository Repository { get; private set; }

		public CartActionTargetProductTypeService(CartActionTargetProductTypeRepository cartActionTargetProductTypeRepository)
		{
			Repository = cartActionTargetProductTypeRepository;
		}

		public virtual Collection<ICartActionTargetProductType> GetAllByCampaign(int campaignId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCampaign(campaignId);
		}
	}
}
