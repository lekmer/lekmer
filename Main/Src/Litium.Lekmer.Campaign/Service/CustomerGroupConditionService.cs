﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class CustomerGroupConditionService : IConditionPluginService
	{
		protected CustomerGroupConditionRepository Repository { get; private set; }

		public CustomerGroupConditionService(CustomerGroupConditionRepository customerGroupConditionRepository)
		{
			Repository = customerGroupConditionRepository;
		}

		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int conditionId).")]
		public virtual ICondition GetById(int conditionId)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var condition = Repository.GetById(conditionId);
			if (condition == null)
			{
				return null;
			}

			condition.IncludeCustomerGroups = Repository.GetIncludeCustomerGroups(conditionId);
			condition.ExcludeCustomerGroups = Repository.GetExcludeCustomerGroups(conditionId);

			return condition;
		}

		public virtual ICondition GetById(IUserContext userContext, int conditionId)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			var condition = Repository.GetById(conditionId);
			if (condition == null)
			{
				return null;
			}

			condition.IncludeCustomerGroups = Repository.GetIncludeCustomerGroups(conditionId);
			condition.ExcludeCustomerGroups = Repository.GetExcludeCustomerGroups(conditionId);

			return condition;
		}
	}
}