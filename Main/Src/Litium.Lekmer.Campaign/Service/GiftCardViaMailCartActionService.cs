﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class GiftCardViaMailCartActionService : ICartActionPluginService
	{
		protected GiftCardViaMailCartActionRepository Repository { get; private set; }

		public GiftCardViaMailCartActionService(GiftCardViaMailCartActionRepository giftCardViaMailCartActionRepository)
		{
			Repository = giftCardViaMailCartActionRepository;
		}


		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int cartActionId).")]
		public ICartAction GetById(int cartActionId)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(cartActionId);

			if (action == null)
			{
				return null;
			}

			action.Amounts = GetCurrencyValues(cartActionId);

			return action;
		}

		public ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(cartActionId);

			if (action == null)
			{
				return null;
			}

			action.Amounts = GetCurrencyValues(cartActionId);

			return action;
		}


		protected virtual LekmerCurrencyValueDictionary GetCurrencyValues(int cartActionId)
		{
			return Repository.GetCurrencyValuesByAction(cartActionId);
		}
	}
}