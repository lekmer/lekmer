﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Campaign
{
	public class ProductActionTargetProductTypeService : IProductActionTargetProductTypeService
	{
		protected ProductActionTargetProductTypeRepository Repository { get; private set; }

		public ProductActionTargetProductTypeService(ProductActionTargetProductTypeRepository productActionTargetProductTypeRepository)
		{
			Repository = productActionTargetProductTypeRepository;
		}

		public virtual Collection<IProductActionTargetProductType> GetAllByCampaign(int campaignId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCampaign(campaignId);
		}
	}
}
