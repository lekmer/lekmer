﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class ProductDiscountActionService : IProductActionPluginService, IProductDiscountActionService
	{
		protected ProductDiscountActionRepository Repository { get; private set; }

		public ProductDiscountActionService(ProductDiscountActionRepository cartCampaignRepository)
		{
			Repository = cartCampaignRepository;
		}

		public IProductAction GetById(IUserContext context, int productActionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var productDiscountAction = Repository.GetById(productActionId);
			if (productDiscountAction == null)
			{
				return null;
			}
			productDiscountAction.ProductDiscountPrices = Repository.GetItems(productActionId, null);
			return productDiscountAction;
		}

		public Collection<int> GetProductIds(int productActionId)
		{
			Repository.EnsureNotNull();
			return Repository.GetProductIds(productActionId);
		}
	}
}