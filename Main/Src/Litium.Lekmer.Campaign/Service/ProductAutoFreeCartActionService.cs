﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class ProductAutoFreeCartActionService : ICartActionPluginService
	{
		protected ProductAutoFreeCartActionRepository Repository { get; private set; }

		public ProductAutoFreeCartActionService(ProductAutoFreeCartActionRepository productAutoFreeCartActionRepository)
		{
			Repository = productAutoFreeCartActionRepository;
		}

		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int cartActionId).")]
		public ICartAction GetById(int cartActionId)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(cartActionId);
		}

		public ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			Repository.EnsureNotNull();
			return Repository.GetById(cartActionId);
		}
	}
}