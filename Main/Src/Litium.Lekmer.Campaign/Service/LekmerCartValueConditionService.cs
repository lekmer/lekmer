﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCartValueConditionService : CartValueConditionService
	{
		protected LekmerCartValueConditionRepository LekmerRepository { get; private set; }

		public LekmerCartValueConditionService(CartValueConditionRepository repository) : base(repository)
		{
			LekmerRepository = (LekmerCartValueConditionRepository) repository;
		}

		/// <summary>
		/// Gets cart value condition by Id
		/// </summary>
		/// <param name="conditionId">Condition Id</param>
		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int conditionId).")]
		public override ICondition GetById(int conditionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var condition = Repository.GetById(conditionId);
			if (condition == null) return null;
			((ILekmerCartValueCondition)condition).CartValues = LekmerRepository.GetCartValuesByCondition(conditionId);
			return condition;
		}

		/// <summary>
		/// Gets cart value condition by Id
		/// </summary>
		/// <param name="userContext">User context</param>
		/// <param name="conditionId">Condition Id</param>
		public override ICondition GetById(IUserContext userContext, int conditionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var condition = Repository.GetById(conditionId);
			if (condition == null) return null;
			((ILekmerCartValueCondition) condition).CartValues = LekmerRepository.GetCartValuesByCondition(conditionId);
			return condition;
		}
	}
}