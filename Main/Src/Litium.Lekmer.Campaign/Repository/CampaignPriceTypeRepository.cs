﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CampaignPriceTypeRepository
	{
		protected virtual DataMapperBase<ICampaignPriceType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignPriceType>(dataReader);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<ICampaignPriceType> GetAll()
		{
			var dbSettings = new DatabaseSetting("CampaignPriceTypeRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignPriceTypeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual ICampaignPriceType GetById(int id)
		{
			var dbSettings = new DatabaseSetting("CampaignPriceTypeRepository.GetById");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Id", id, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignPriceTypeGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}