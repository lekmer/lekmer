﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartItemDiscountCartActionRepository
	{
		protected virtual DataMapperBase<ICartItemDiscountCartAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemDiscountCartAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(ICartItemDiscountCartAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Quantity", action.Quantity, SqlDbType.Int),
					ParameterHelper.CreateParameter("ApplyForCheapest", action.ApplyForCheapest, SqlDbType.Bit),
					ParameterHelper.CreateParameter("IsPercentageDiscount", action.IsPercentageDiscount, SqlDbType.Bit),
					ParameterHelper.CreateParameter("PercentageDiscountAmount", action.PercentageDiscountAmount, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("IncludeAllProducts", action.IncludeAllProducts, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.Save");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.Delete");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionDelete]", parameters, dbSettings);
		}

		public virtual ICartItemDiscountCartAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int cartActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("MonetaryValue", currencyValue.Value, SqlDbType.Decimal)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.SaveCurrencyValue");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteAllCurrencyValue");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int cartActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetCurrencyValuesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}
			return lekmerCurrencyValueDictionary;
		}


		// Products

		public virtual void InsertIncludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCartItemDiscountCartActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCartItemDiscountCartActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetIncludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetExcludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Categories

		public virtual void InsertIncludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCartItemDiscountCartActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCartItemDiscountCartActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CampaignCategoryDictionary GetIncludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetIncludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetIncludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetIncludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionIncludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetExcludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetExcludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionExcludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}


		// Brands

		public virtual void InsertIncludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.InsertIncludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCartItemDiscountCartActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.InsertExcludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCartItemDiscountCartActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteIncludeBrands");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.DeleteExcludeBrands");
			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemDiscountCartActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetIncludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemDiscountCartActionRepository.GetExcludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemDiscountCartActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}