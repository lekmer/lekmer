using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CampaignConfigRepository
	{
		protected virtual DataMapperBase<ICampaignConfig> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignConfig>(dataReader);
		}

		public virtual ICampaignConfig GetById(int campaignConfigId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfiguratorId", campaignConfigId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignConfigRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionConfiguratorGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual int Save(ICampaignConfig campaignConfig)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfiguratorId", campaignConfig.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllProducts", campaignConfig.IncludeAllProducts, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("CampaignConfigRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionConfiguratorSave]", parameters, dbSettings);
		}
	}
}