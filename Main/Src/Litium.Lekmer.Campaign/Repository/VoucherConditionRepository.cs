﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class VoucherConditionRepository
	{
		protected virtual DataMapperBase<IVoucherCondition> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IVoucherCondition>(dataReader);
		}

		/// <summary>
		/// Gets cart value condition by Id
		/// </summary>
		/// <param name="conditionId">Condition Id</param>
		/// <returns>IVoucherCondition object</returns>
		public virtual IVoucherCondition GetById(int conditionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherConditionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		/// <summary>
		/// Saves cart value condition
		/// </summary>
		public virtual void Save(IVoucherCondition condition)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", condition.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@IncludeAllBatchIds", condition.IncludeAllBatchIds, SqlDbType.Bit)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionSave]", parameters, dbSettings);
		}

		/// <summary>
		/// Deletes cart value condition
		/// </summary>
		public virtual void Delete(int conditionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionDelete]", parameters, dbSettings);
		}

		/// <summary>
		/// Saves condition include batch id
		/// </summary>
		public virtual void BatchIncludeInsert(int conditionId, int batchId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@BatchId",batchId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.BatchIncludeInsert");

			new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionBatchIncludeInsert]", parameters, dbSettings);
		}

		/// <summary>
		/// Saves condition exclude batch id
		/// </summary>
		public virtual void BatchExcludeInsert(int conditionId, int batchId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@BatchId",batchId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.BatchExcludeInsert");

			new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionBatchExcludeInsert]", parameters, dbSettings);
		}

		public virtual Collection<int> GetBatchesIncludeByConditionId(int conditionId)
		{
			var batchIds = new Collection<int>();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.GetBatchesIncludeByConditionId");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherConditionGetBatchIdsIncludeByConditionId]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					batchIds.Add(dataReader.GetInt32(0));
				}
			}

			return batchIds;
		}

		public virtual Collection<int> GetBatchesExcludeByConditionId(int conditionId)
		{
			var batchIds = new Collection<int>();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.GetBatchesExcludeByConditionId");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherConditionGetBatchIdsExcludeByConditionId]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					batchIds.Add(dataReader.GetInt32(0));
				}
			}

			return batchIds;
		}

		/// <summary>
		/// Deletes all batche ids for appropriate condition
		/// </summary>
		public virtual void DeleteAllBatchIds(int conditionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("VoucherConditionRepository.DeleteAllBatchIds");

			new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionDeleteBatchesAll]", parameters, dbSettings);
		}
	}
}