﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class LekmerFreightValueActionRepository : FreightValueActionRepository
	{
		/// <summary>
		/// Gets currency value dictionary by action Id.
		/// </summary>
		/// <param name="actionId">Action Id</param>
		/// <returns>The <see cref="LekmerCurrencyValueDictionary"/> object</returns>
		public new virtual LekmerCurrencyValueDictionary GetCartValuesByAction(int actionId)
		{
			IEnumerable<CurrencyValue> currencyValues;
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FreightValueActionRepository.GetFreightValuesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaign].[pFreightValuesGetByActionId]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}
			var dictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValue in currencyValues)
			{
				dictionary.Add(currencyValue.Currency.Id, currencyValue.MonetaryValue);
			}
			return dictionary;
		}

		/// <summary>
		/// Saves cart value
		/// </summary>
		public virtual void SaveFreightValue(int actionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", actionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CurrencyId", currencyValue.Key, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Value", currencyValue.Value, SqlDbType.Decimal)
			};
			var dbSettings = new DatabaseSetting("FreightValueActionRepository.SaveFreightValue");
			new DataHandler().ExecuteCommand("[campaign].[pFreightValueSave]", parameters, dbSettings);
		}

		// Delivery Methods

		public virtual void InsertDeliveryMethod(int actionId, int deliveryMethodId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ActionId", actionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("DeliveryMethodId", deliveryMethodId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerFreightValueActionRepository.InsertDeliveryMethod");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFreightValueActionDeliveryMethodInsert]", parameters, dbSettings);
		}

		public virtual void DeleteDeliveryMethods(int actionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ActionId", actionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerFreightValueActionRepository.DeleteDeliveryMethods");

			new DataHandler().ExecuteCommand("[campaignlek].[pFreightValueActionDeliveryMethodDeleteAll]", parameters, dbSettings);
		}

		public virtual IdDictionary GetDeliveryMethods(int actionId)
		{
			var ids = new IdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerFreightValueActionRepository.GetDeliveryMethods");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFreightValueActionDeliveryMethodGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					ids.Add(dataReader.GetInt32(0));
				}
			}

			return ids;
		}
	}
}