﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartActionTargetProductTypeRepository
	{
		protected virtual DataMapperBase<ICartActionTargetProductType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartActionTargetProductType>(dataReader);
		}

		public virtual Collection<ICartActionTargetProductType> GetAllByCampaign(int campaignId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartActionTargetProductTypeRepository.GetAllByCampaign");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartActionTargetProductTypeGetAllByCampaign]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Save(int cartActionId, string targetProductTypeIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TargetProductTypeIds", targetProductTypeIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("CartActionTargetProductTypeRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartActionTargetProductTypeSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartActionTargetProductTypeRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartActionTargetProductTypeDelete]", parameters, dbSettings);
		}
	}
}