﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class GiftCardViaMailCartActionRepository
	{
		protected virtual DataMapperBase<IGiftCardViaMailCartAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IGiftCardViaMailCartAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(IGiftCardViaMailCartAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("SendingInterval", action.SendingInterval, SqlDbType.Int),
					ParameterHelper.CreateParameter("TemplateId", action.TemplateId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailCartActionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailCartActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailCartActionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailCartActionDelete]", parameters, dbSettings);
		}

		public virtual IGiftCardViaMailCartAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailCartActionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailCartActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int cartActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", currencyValue.Value, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailCartActionRepository.SaveCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailCartActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailCartActionRepository.DeleteAllCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailCartActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int cartActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailCartActionRepository.GetCurrencyValuesByAction");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailCartActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}
	}
}