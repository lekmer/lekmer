﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Campaign.Cache
{
	public sealed class CampaignPriceTypeCache : ScensumCacheBase<CampaignPriceTypeKey, ICampaignPriceType>
	{
		private static readonly CampaignPriceTypeCache _instance = new CampaignPriceTypeCache();

		private CampaignPriceTypeCache()
		{
		}

		public static CampaignPriceTypeCache Instance
		{
			get { return _instance; }
		}
	}

	public class CampaignPriceTypeKey : ICacheKey
	{
		public CampaignPriceTypeKey(int id)
		{
			Id = id;
		}

		public int Id { get; set; }

		public string Key
		{
			get { return string.Format(CultureInfo.InvariantCulture, "PriceType_{0}", Id); }
		}
	}
}