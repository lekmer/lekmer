using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Campaign.Cache
{
	public sealed class CampaignRegistryCampaignCache : ScensumCacheBase<CampaignRegistryCampaignKey, Collection<ICampaignRegistryCampaign>>
	{
		private static readonly CampaignRegistryCampaignCache _instance = new CampaignRegistryCampaignCache();

		private CampaignRegistryCampaignCache()
		{
		}

		public static CampaignRegistryCampaignCache Instance
		{
			get { return _instance; }
		}
	}

	public class CampaignRegistryCampaignKey : ICacheKey
	{
		public CampaignRegistryCampaignKey(int campaignId)
		{
			CampaignId = campaignId;
		}

		public int CampaignId { get; set; }

		public string Key
		{
			get { return string.Format(CultureInfo.InvariantCulture, "CampaignId_{0}", CampaignId); }
		}
	}
}