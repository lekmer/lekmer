﻿using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public class CartCampaignSharedService : ICartCampaignSharedService
	{
		protected ICartCampaignService CartCampaignService { get; private set; }

		public CartCampaignSharedService(ICartCampaignService cartCampaignService)
		{
			CartCampaignService = cartCampaignService;
		}
		
		public virtual ICartCampaign GetById(int campaignId)
		{
			return CartCampaignService.GetById(Scensum.Core.Web.UserContext.Current, campaignId);
		}
	}
}