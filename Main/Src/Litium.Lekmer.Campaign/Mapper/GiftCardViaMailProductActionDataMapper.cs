﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class GiftCardViaMailProductActionDataMapper : ProductActionDataMapper<IGiftCardViaMailProductAction>
	{
		public GiftCardViaMailProductActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IGiftCardViaMailProductAction Create()
		{
			var giftCardViaMailProductAction = base.Create();
			giftCardViaMailProductAction.SendingInterval = MapValue<int>("GiftCardViaMailProductAction.SendingInterval");
			giftCardViaMailProductAction.TemplateId = MapNullableValue<int?>("GiftCardViaMailProductAction.TemplateId");
			giftCardViaMailProductAction.CampaignConfigId = MapValue<int>("GiftCardViaMailProductAction.ConfigId");
			return giftCardViaMailProductAction;
		}
	}
}