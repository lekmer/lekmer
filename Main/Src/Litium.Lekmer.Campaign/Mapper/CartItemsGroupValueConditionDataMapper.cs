﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartItemsGroupValueConditionDataMapper : ConditionDataMapper<ICartItemsGroupValueCondition>
	{
		public CartItemsGroupValueConditionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICartItemsGroupValueCondition Create()
		{
			var cartItemsGroupValueCondition = base.Create();
			cartItemsGroupValueCondition.CampaignConfigId = MapValue<int>("CartItemsGroupValueCondition.ConfigId");
			return cartItemsGroupValueCondition;
		}
	}
}