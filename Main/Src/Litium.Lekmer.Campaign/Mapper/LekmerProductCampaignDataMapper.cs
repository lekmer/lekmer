﻿using System.Data;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class LekmerProductCampaignDataMapper : LekmerCampaignDataMapper<IProductCampaign>
	{
		public LekmerProductCampaignDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
	}
}