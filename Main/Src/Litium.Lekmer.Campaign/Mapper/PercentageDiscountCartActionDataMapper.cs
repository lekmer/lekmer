﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class PercentageDiscountCartActionDataMapper : CartActionDataMapper<IPercentageDiscountCartAction>
	{
		public PercentageDiscountCartActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IPercentageDiscountCartAction Create()
		{
			var action = base.Create();

			action.DiscountAmount = MapValue<decimal>("DiscountAmount");

			return action;
		}
	}
}