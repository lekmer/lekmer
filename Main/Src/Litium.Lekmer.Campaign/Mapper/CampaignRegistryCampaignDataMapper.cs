﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignRegistryCampaignDataMapper : DataMapperBase<ICampaignRegistryCampaign>
	{
		public CampaignRegistryCampaignDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignRegistryCampaign Create()
		{
			var campaignRegistryCampaign = IoC.Resolve<ICampaignRegistryCampaign>();

			campaignRegistryCampaign.CampaignRegistryId = MapValue<int>("CampaignRegistryCampaign.CampaignRegistryId");
			campaignRegistryCampaign.CampaignId = MapValue<int>("CampaignRegistryCampaign.CampaignId");
			campaignRegistryCampaign.ChannelId = MapNullableValue<int?>("CampaignRegistryCampaign.ChannelId");
			campaignRegistryCampaign.CountryIso = MapNullableValue<string>("CampaignRegistryCampaign.CountryIso");

			return campaignRegistryCampaign;
		}
	}
}