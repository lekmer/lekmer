﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignLevelDataMapper : DataMapperBase<ICampaignLevel>
	{
		public CampaignLevelDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignLevel Create()
		{
			var campaignLevel = IoC.Resolve<ICampaignLevel>();

			campaignLevel.Id = MapValue<int>("CampaignLevel.Id");
			campaignLevel.Title = MapValue<string>("CampaignLevel.Title");
			campaignLevel.Priority = MapValue<int>("CampaignLevel.Priority");

			return campaignLevel;
		}
	}
}