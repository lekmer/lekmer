﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CurrencyValueRangeDataMapper : DataMapperBase<CurrencyValueRange>
	{
		public CurrencyValueRangeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override CurrencyValueRange Create()
		{
			var currencyValueRange = IoC.Resolve<CurrencyValueRange>();

			currencyValueRange.CurrencyId = MapValue<int>("CartValueRangeConditionCurrency.CurrencyId");
			currencyValueRange.MonetaryValueFrom = MapValue<decimal>("CartValueRangeConditionCurrency.MonetaryValueFrom");
			currencyValueRange.MonetaryValueTo = MapValue<decimal>("CartValueRangeConditionCurrency.MonetaryValueTo");

			return currencyValueRange;
		}
	}
}