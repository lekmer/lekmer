﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartItemGroupFixedDiscountActionDataMapper : CartActionDataMapper<ICartItemGroupFixedDiscountAction>
	{
		public CartItemGroupFixedDiscountActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICartItemGroupFixedDiscountAction Create()
		{
			var action = base.Create();
			action.IncludeAllProducts = MapValue<bool>("CartItemGroupFixedDiscountAction.IncludeAllProducts");
			return action;
		}
	}
}