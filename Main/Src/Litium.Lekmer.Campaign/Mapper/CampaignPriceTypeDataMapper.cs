﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignPriceTypeDataMapper : DataMapperBase<ICampaignPriceType>
	{
		public CampaignPriceTypeDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignPriceType Create()
		{
			var campaignPriceType = IoC.Resolve<ICampaignPriceType>();

			campaignPriceType.Id = MapValue<int>("CampaignPriceType.Id");
			campaignPriceType.CommonName = MapValue<string>("CampaignPriceType.CommonName");
			campaignPriceType.Title = MapValue<string>("CampaignPriceType.Title");

			return campaignPriceType;
		}
	}
}