﻿using System.Data;
using Litium.Framework.DataMapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class ProductDiscountActionItemDataMapper : DataMapperBase<ProductDiscountActionItem>
	{
		public ProductDiscountActionItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ProductDiscountActionItem Create()
		{
			var item = new ProductDiscountActionItem
			{
				ProductId = MapValue<int>("ProductDiscountActionItem.ProductId"),
				CurrencyId = MapValue<int>("ProductDiscountActionItem.CurrencyId"),
				MonetaryValue = MapValue<decimal>("CurrencyValue.MonetaryValue")
			};
			return item;
		}
	}
}