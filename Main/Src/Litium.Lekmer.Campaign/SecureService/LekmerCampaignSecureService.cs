﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCampaignSecureService : CampaignSecureService
	{
		protected LekmerCampaignRepository LekmerCampaignRepository { get; private set; }
		protected ICampaignRegistryCampaignSecureService CampaignRegistryCampaignSecureService { get; private set; }
		protected ICampaignLandingPageSecureService CampaignLandingPageSecureService { get; private set; }

		public LekmerCampaignSecureService(
			IAccessValidator accessValidator,
			CampaignRepository repository,
			ICampaignRegistryCampaignSecureService campaignRegistryCampaignSecureService,
			ICampaignLandingPageSecureService campaignLandingPageSecureService)
			: base(accessValidator, repository)
		{
			CampaignRegistryCampaignSecureService = campaignRegistryCampaignSecureService;
			CampaignLandingPageSecureService = campaignLandingPageSecureService;
			LekmerCampaignRepository = (LekmerCampaignRepository) repository;
		}

		public override void Delete(ISystemUserFull user, int id)
		{
			CampaignRegistryCampaignSecureService.Delete(id);
			CampaignLandingPageSecureService.Delete(user, id);
			base.Delete(user, id);
			CampaignToTagSave(id);
			CampaignRegistryCampaignCache.Instance.Remove(new CampaignRegistryCampaignKey(id));
		}

		public override int Save(ISystemUserFull systemUserFull, ICampaign campaign)
		{
			var campaignId = base.Save(systemUserFull, campaign);
			CampaignToTagSave(campaignId);
			CampaignRegistryCampaignCache.Instance.Remove(new CampaignRegistryCampaignKey(campaignId));
			return campaignId;
		}

		public override void SetStatus(ISystemUserFull user, int id, int statusId)
		{
			if (Repository == null)
			{
				throw new InvalidOperationException("Repository cannot be null.");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			Repository.SetStatus(id, statusId);
			CampaignToTagSave(id);

			RemoveFromCache(id);
			FlushCampaignCollectionCache();
		}

		protected virtual void CampaignToTagSave(int campaignId)
		{
			LekmerCampaignRepository.CampaignToTagSave(campaignId);
		}

		public Collection<ICampaignHyId> SearchProductCampaignsByHyId(string searchPhrase)
		{
			Repository.EnsureNotNull();
			return LekmerCampaignRepository.SearchProductCampaignsByHyId(searchPhrase);
		}

		public Collection<ICampaignHyId> SearchCartCampaignsByHyId(string searchPhrase)
		{
			Repository.EnsureNotNull();
			return LekmerCampaignRepository.SearchCartCampaignsByHyId(searchPhrase);
		}
	}
}