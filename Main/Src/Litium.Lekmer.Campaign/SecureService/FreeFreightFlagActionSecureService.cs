﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class FreeFreightFlagActionSecureService : IProductActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected FreeFreightFlagActionRepository Repository { get; private set; }
		protected IProductActionTypeSecureService ProductActionTypeSecureService { get; set; }

		public FreeFreightFlagActionSecureService(
			IAccessValidator accessValidator,
			FreeFreightFlagActionRepository repository,
			IProductActionTypeSecureService productActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			ProductActionTypeSecureService = productActionTypeSecureService;
		}

		public IProductAction Create()
		{
			if (ProductActionTypeSecureService == null)
				throw new InvalidOperationException("ProductActionTypeSecureService cannot be null.");

			var action = IoC.Resolve<IFreeFreightFlagAction>();
			action.ActionType = ProductActionTypeSecureService.GetByCommonName("FreeFreightFlag");
			action.ProductPrices = new LekmerCurrencyValueDictionary();
			action.Status = BusinessObjectStatus.New;
			return action;
		}

		public IProductAction GetById(int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var action = Repository.GetById(id);
			if (action == null)
			{
				return null;
			}
			action.ProductPrices = Repository.GetProductPricesByAction(id);
			return action;
		}

		public void Save(ISystemUserFull user, IProductAction action)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var freeFreightFlagAction = action as IFreeFreightFlagAction;
			if (freeFreightFlagAction == null) throw new InvalidOperationException("action is not IFreeFreightFlagAction type");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(freeFreightFlagAction);
				SaveProductPrices(freeFreightFlagAction);
				transactedOperation.Complete();
			}
		}

		private void SaveProductPrices(IFreeFreightFlagAction action)
		{
			Repository.DeleteAllProductPrices(action.Id);

			foreach (var productPrice in action.ProductPrices)
			{
				Repository.SaveProductPrice(action.Id, productPrice);
			}
		}

		public void Delete(ISystemUserFull user, int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllProductPrices(id);
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}
	}
}