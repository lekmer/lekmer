﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class GiftCardViaMailProductActionSecureService : IProductActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected GiftCardViaMailProductActionRepository Repository { get; private set; }
		protected IProductActionTypeSecureService ProductActionTypeSecureService { get; set; }
		protected ICampaignConfigSecureService CampaignConfigSecureService { get; private set; }

		public GiftCardViaMailProductActionSecureService(
			IAccessValidator accessValidator,
			GiftCardViaMailProductActionRepository giftCardViaMailProductActionRepository,
			IProductActionTypeSecureService productActionTypeSecureService,
			ICampaignConfigSecureService campaignConfigSecureService)
		{
			AccessValidator = accessValidator;
			Repository = giftCardViaMailProductActionRepository;
			ProductActionTypeSecureService = productActionTypeSecureService;
			CampaignConfigSecureService = campaignConfigSecureService;
		}

		public virtual IProductAction Create()
		{
			if (ProductActionTypeSecureService == null)
			{
				throw new InvalidOperationException("ProductActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IGiftCardViaMailProductAction>();
			action.ActionType = ProductActionTypeSecureService.GetByCommonName("GiftCardViaMailProduct");
			action.Amounts = new LekmerCurrencyValueDictionary();
			action.CampaignConfig = CampaignConfigSecureService.Create();
			action.Status = BusinessObjectStatus.New;
			return action;
		}

		public virtual void Save(ISystemUserFull user, IProductAction action)
		{
			Repository.EnsureNotNull();

			var giftCardViaMailProductAction = action as IGiftCardViaMailProductAction;
			if (giftCardViaMailProductAction == null)
			{
				throw new InvalidOperationException("action is not IGiftCardViaMailProductAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				giftCardViaMailProductAction.CampaignConfigId = CampaignConfigSecureService.Save(giftCardViaMailProductAction.CampaignConfig);
				Repository.Save(giftCardViaMailProductAction);
				SaveAmounts(giftCardViaMailProductAction);
				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull user, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public virtual IProductAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);
			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(id);
			action.CampaignConfig = CampaignConfigSecureService.GetById(action.CampaignConfigId);
			return action;
		}


		protected virtual void SaveAmounts(IGiftCardViaMailProductAction giftCardViaMailProductAction)
		{
			Repository.DeleteAllCurrencyValue(giftCardViaMailProductAction.Id);

			foreach (var amount in giftCardViaMailProductAction.Amounts)
			{
				Repository.SaveCurrencyValue(giftCardViaMailProductAction.Id, amount);
			}
		}
	}
}