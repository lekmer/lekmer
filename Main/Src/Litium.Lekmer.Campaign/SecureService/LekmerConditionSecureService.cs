﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product.Constant;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerConditionSecureService : ConditionSecureService
	{
		protected IConditionTargetProductTypeSecureService ConditionTargetProductTypeSecureService { get; private set; }

		public LekmerConditionSecureService(
			IAccessValidator accessValidator,
			ConditionRepository repository,
			ConditionSecureServiceLocator secureServiceLocator,
			IConditionTargetProductTypeSecureService conditionTargetProductTypeSecureService)
			: base(accessValidator, repository, secureServiceLocator)
		{
			ConditionTargetProductTypeSecureService = conditionTargetProductTypeSecureService;
		}

		/// <summary>
		/// Create new <see cref="ICondition"/> object.
		/// </summary>
		/// <param name="typeCommonName">Condition type common name</param>
		/// <returns>The <see cref="ICondition"/> object.</returns>
		public override ICondition Create(string typeCommonName)
		{
			var condition = base.Create(typeCommonName);

			var lekmerCondition = condition as ILekmerCondition;
			if (lekmerCondition != null)
			{
				lekmerCondition.TargetProductTypes = new IdDictionary { (int)ProductType.Product }; // Target = Product by default
			}

			return condition;
		}

		/// <summary>
		/// Gets a collection of <see cref="ICondition"/> objects assigned to proper campaign.
		/// </summary>
		/// <param name="campaignId">Campaign Id.</param>
		/// <returns>The collection of <see cref="ICondition"/> objects.</returns>
		public override Collection<ICondition> GetAllByCampaign(int campaignId)
		{
			Collection<ICondition> conditions = base.GetAllByCampaign(campaignId);

			var actionTargetProductTypes = ConditionTargetProductTypeSecureService.GetAllByCampaign(campaignId);

			foreach (ICondition condition in conditions)
			{
				var lekmerCondition = condition as ILekmerCondition;

				if (lekmerCondition != null)
				{
					int conditionId = condition.Id;
					IEnumerable<int> productTypes = actionTargetProductTypes.Where(t => t.ConditionId == conditionId).Select(t => t.ProductTypeId);

					lekmerCondition.TargetProductTypes = new IdDictionary(productTypes);
				}
			}

			return conditions;
		}

		/// <summary>
		/// Saves <see cref="ICondition"/> object.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="condition">Condition to save</param>
		/// <returns>The <see cref="ICondition"/> object identifier</returns>
		public override int Save(ISystemUserFull systemUserFull, ICondition condition)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				base.Save(systemUserFull, condition);

				var lekmerCondition = condition as ILekmerCondition;
				if (lekmerCondition != null)
				{
					ConditionTargetProductTypeSecureService.Save(systemUserFull, condition);
				}

				transactedOperation.Complete();
			}

			return condition.Id;
		}

		/// <summary>
		/// Deletes condition.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="id">Condition identifier</param>
		/// <param name="typeCommonName">Condition type common name</param>
		public override void Delete(ISystemUserFull systemUserFull, int id, string typeCommonName)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				ConditionTargetProductTypeSecureService.Delete(systemUserFull, id);

				base.Delete(systemUserFull, id, typeCommonName);

				transactedOperation.Complete();
			}
		}
	}
}
