﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCartValueConditionSecureService : CartValueConditionSecureService
	{
		protected LekmerCartValueConditionRepository LekmerRepository { get; private set; }

		public LekmerCartValueConditionSecureService(
			IAccessValidator accessValidator,
			CartValueConditionRepository repository,
			IConditionTypeSecureService conditionTypeSecureService) : base(accessValidator, repository, conditionTypeSecureService)
		{
			LekmerRepository = (LekmerCartValueConditionRepository) repository;
		}

		/// <summary>
		/// Creates new <see cref="ICartValueCondition"/> object.
		/// </summary>
		/// <returns>The <see cref="ICartValueCondition"/> object</returns>
		public override ICondition Create()
		{
			if (ConditionTypeSecureService == null)
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");

			var condition = (ILekmerCartValueCondition) IoC.Resolve<ICartValueCondition>();
			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("CartValue");
			condition.CartValues = new LekmerCurrencyValueDictionary();
			condition.Status = BusinessObjectStatus.New;
			return condition;
		}

		/// <summary>
		/// Gets <see cref="ICartValueCondition"/> object by id.
		/// </summary>
		/// <param name="id">Condition id</param>
		/// <returns>The <see cref="ICartValueCondition"/> object</returns>
		public override ICondition GetById(int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var condition = Repository.GetById(id);
			if (condition == null) return null;
			((ILekmerCartValueCondition) condition).CartValues = LekmerRepository.GetCartValuesByCondition(id);
			return condition;
		}

		/// <summary>
		/// Saves <see cref="ICartValueCondition"/> object.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="condition">Condition to save</param>
		public override void Save(ISystemUserFull systemUserFull, ICondition condition)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var cartValueCondition = condition as ILekmerCartValueCondition;
			if (cartValueCondition == null) throw new InvalidOperationException("condition is not ICartValueCondition type");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(cartValueCondition);

				Repository.DeleteAllCartValues(cartValueCondition.Id);
				foreach (var cartValue in cartValueCondition.CartValues)
				{
					LekmerRepository.SaveCartValue(cartValueCondition.Id, cartValue);
				}

				transactedOperation.Complete();
			}
		}
	}
}