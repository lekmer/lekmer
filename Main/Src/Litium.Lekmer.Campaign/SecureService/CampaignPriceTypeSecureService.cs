﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;

namespace Litium.Lekmer.Campaign
{
	public class CampaignPriceTypeSecureService : ICampaignPriceTypeSecureService
	{
		protected CampaignPriceTypeRepository Repository { get; private set; }

		public CampaignPriceTypeSecureService(CampaignPriceTypeRepository repository)
		{
			Repository = repository;
		}

		public Collection<ICampaignPriceType> GetAll()
		{
			return Repository.GetAll();
		}
	}
}