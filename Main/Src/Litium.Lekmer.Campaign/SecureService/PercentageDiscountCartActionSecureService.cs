using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class PercentageDiscountCartActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected PercentageDiscountCartActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; set; }

		public PercentageDiscountCartActionSecureService(
			IAccessValidator accessValidator,
			PercentageDiscountCartActionRepository repository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public virtual ICartAction Create()
		{
			CartActionTypeSecureService.EnsureNotNull();

			var action = IoC.Resolve<IPercentageDiscountCartAction>();

			action.ActionType = CartActionTypeSecureService.GetByCommonName("PercentageDiscount");

			return action;
		}

		public virtual void Save(ISystemUserFull user, ICartAction action)
		{
			Repository.EnsureNotNull();

			var percentageDiscountCartAction = action as IPercentageDiscountCartAction;
			if (percentageDiscountCartAction == null)
			{
				throw new InvalidOperationException("action is not IPercentageDiscountCartAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			Repository.Save(percentageDiscountCartAction);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public virtual ICartAction GetById(int id)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(id);
		}
	}
}