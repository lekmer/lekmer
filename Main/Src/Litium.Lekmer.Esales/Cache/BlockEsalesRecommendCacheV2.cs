﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales
{
	public sealed class BlockEsalesRecommendCacheV2 : ScensumCacheBase<BlockEsalesRecommendKeyV2, IBlockEsalesRecommendV2>
	{
		private static readonly BlockEsalesRecommendCacheV2 _instance = new BlockEsalesRecommendCacheV2();

		private BlockEsalesRecommendCacheV2()
		{
		}

		public static BlockEsalesRecommendCacheV2 Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockEsalesRecommendKeyV2(channel.Id, blockId));
			}
		}
	}

	public class BlockEsalesRecommendKeyV2 : ICacheKey
	{
		public BlockEsalesRecommendKeyV2(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return "BlockEsalesRecommendV2-" + ChannelId + "-" + BlockId; }
		}
	}
}