﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales
{
	public sealed class BlockEsalesRecommendCache : ScensumCacheBase<BlockEsalesRecommendKey, IBlockEsalesRecommend>
	{
		private static readonly BlockEsalesRecommendCache _instance = new BlockEsalesRecommendCache();

		private BlockEsalesRecommendCache()
		{
		}

		public static BlockEsalesRecommendCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockEsalesRecommendKey(channel.Id, blockId));
			}
		}
	}

	public class BlockEsalesRecommendKey : ICacheKey
	{
		public BlockEsalesRecommendKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}