﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesAdsService : IBlockEsalesAdsService
	{
		protected BlockEsalesAdsRepository Repository { get; private set; }

		public BlockEsalesAdsService(BlockEsalesAdsRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockEsalesAds GetById(IUserContext context, int blockId)
		{
			Repository.EnsureNotNull();

			return BlockEsalesAdsCache.Instance.TryGetItem(
				new BlockEsalesAdsKey(context.Channel.Id, blockId),
				() => GetByIdCore(context, blockId));
		}

		protected virtual IBlockEsalesAds GetByIdCore(IUserContext context, int blockId)
		{
			return Repository.GetById(context.Channel, blockId);
		}
	}
}