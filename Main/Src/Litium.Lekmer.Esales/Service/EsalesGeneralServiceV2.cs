using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public class EsalesGeneralServiceV2 : IEsalesGeneralServiceV2
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected IProductService ProductService { get; private set; }

		public EsalesGeneralServiceV2(
			IEsalesService esalesService,
			IEsalesProductKeyService esalesProductKeyService,
			IProductService productService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = productService;
		}

		public virtual object GetResults(string panelPath, Dictionary<string, string> parameters)
		{
			EsalesService.EnsureNotNull();

			var esalesRequest = IoC.Resolve<IEsalesGeneralRequestV2>();
			esalesRequest.PanelPath = panelPath;
			esalesRequest.Parameters = parameters;
			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);
			return esalesResponse;
		}

		public virtual ProductCollection FindProducts(IUserContext context, IPanelContent esalesResponse, string panelName, int returnItemsCount)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var productsResults = panelContent.FindResults(EsalesResultType.Products);
				if (productsResults != null)
				{
					var productHitsToReturn = new Collection<IProductHitInfo>();
					var productKeysDictionary = new Dictionary<string, string>();

					IEnumerable<IProductHitInfo> allProductHits = productsResults.Cast<IProductHits>().SelectMany(ph => ph.ProductsInfo);
					foreach (IProductHitInfo productHitInfo in allProductHits)
					{
						if (productHitsToReturn.Count >= returnItemsCount) break;
						if (productKeysDictionary.ContainsKey(productHitInfo.Key)) continue;

						productHitsToReturn.Add(productHitInfo);
						productKeysDictionary[productHitInfo.Key] = productHitInfo.Key;
					}

					if (productHitsToReturn.Count > 0)
					{
						Dictionary<int, string> tickets = productHitsToReturn.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

						var productIds = new ProductIdCollection(productHitsToReturn.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
						productIds.TotalCount = productHitsToReturn.Count;
						ProductCollection products = ProductService.PopulateProducts(context, productIds);
						foreach (ILekmerProduct product in products)
						{
							product.EsalesTicket = tickets[product.Id];
						}

						return products;
					}
				}
			}

			return new ProductCollection();
		}

		public virtual string FindTicket(IEsalesResponse esalesResponse, string panelName)
		{
			IPanelContent panelContent = esalesResponse.FindPanel(panelName);
			if (panelContent != null)
			{
				var productsResult = panelContent.FindResult(EsalesResultType.Products) as IProductHits;
				if (productsResult != null)
				{
					return productsResult.ProductsInfo.Select(p => p.Ticket).FirstOrDefault();
				}
			}

			return string.Empty;
		}
	}
}