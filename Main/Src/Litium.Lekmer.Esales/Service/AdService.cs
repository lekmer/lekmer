﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class AdService : IAdService
	{
		protected AdRepository Repository { get; private set; }

		public AdService(AdRepository adRepository)
		{
			Repository = adRepository;
		}

		public virtual IAd GetById(int adId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(adId);
		}

		public virtual Collection<IAd> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}
	}
}