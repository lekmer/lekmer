﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Esales
{
	public class EsalesAdsService : IEsalesAdsService
	{
		protected IEsalesService EsalesService { get; private set; }

		public EsalesAdsService(IEsalesService esalesService)
		{
			EsalesService = esalesService;
		}

		public virtual IAdHits FindAds(IUserContext context, string panelPath, string formatFilter, string genderFilter, string productCategotyFilter, string customerKey, int itemsToReturn)
		{
			if (context == null) throw new ArgumentNullException("context");
			EsalesService.EnsureNotNull();

			var esalesRequest = IoC.Resolve<IAdsEsalesRequest>();
			esalesRequest.PanelPath = panelPath;
			esalesRequest.Site = context.Channel.Country.Iso;
			esalesRequest.CustomerKey = customerKey;

			esalesRequest.FormatList = ConvertToList(formatFilter);
			esalesRequest.GenderList = ConvertToList(genderFilter);
			esalesRequest.ProductCategoryList = ConvertToList(productCategotyFilter);

			esalesRequest.MaxAds = itemsToReturn;

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			return FindAds(context, esalesResponse, itemsToReturn);
		}

		protected virtual IAdHits FindAds(IUserContext context, IEsalesResponse esalesResponse, int itemsToReturn)
		{
			IResult adsResult = esalesResponse.FindResult(EsalesResultType.Ads);

			if (adsResult == null)
			{
				var adHits = IoC.Resolve<IAdHits>();
				adHits.AdsInfo = new Collection<IAdInfo>();

				return adHits;
			}

			return (IAdHits)adsResult;
		}

		protected virtual List<string> ConvertToList(string commaSeparatedPhrases)
		{
			var phraseList = new List<string>();

			if (commaSeparatedPhrases.HasValue())
			{
				string[] phrases = commaSeparatedPhrases.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

				phraseList.AddRange(phrases.Select(phrase => phrase.Trim()));
			}

			return phraseList;
		}
	}
}