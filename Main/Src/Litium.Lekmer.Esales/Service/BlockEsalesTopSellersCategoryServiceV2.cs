using System.Collections.ObjectModel;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesTopSellersCategoryServiceV2 : IBlockEsalesTopSellersCategoryServiceV2
	{
		protected BlockEsalesTopSellersCategoryRepositoryV2 Repository { get; private set; }
		protected ICategoryService CategoryService;

		public BlockEsalesTopSellersCategoryServiceV2(BlockEsalesTopSellersCategoryRepositoryV2 repository, ICategoryService categoryService)
		{
			Repository = repository;
			CategoryService = categoryService;
		}

		public virtual Collection<IBlockEsalesTopSellersCategoryV2> GetAllByBlock(IUserContext context, int blockId)
		{
			return Repository.GetAllByBlock(blockId, context.Channel.Language.Id);
		}
	}
}