﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchProductFilterPanelSetting : SearchProductPanelSetting, ISearchProductFilterPanelSetting
	{
		public SearchProductFilterPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public override string PanelPath
		{
			get { return GetString(GroupName, "PanelPathProductFilter"); }
		}

		public override string ProductPanelName
		{
			get { return GetString(GroupName, "ProductFilterPanelName"); }
		}
	}
}
