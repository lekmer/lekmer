﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Esales.Setting
{
	public class EsalesRecommendPanelSetting : SettingBase, IEsalesRecommendPanelSetting
	{
		protected override string StorageName
		{
			get { return "EsalesPanels"; }
		}

		protected virtual string GroupName
		{
			get { return "RecommendPanel"; }
		}


		public string AbandonedCartsPanelPath
		{
			get { return GetString(GroupName, "AbandonedCartsPanelPath"); }
		}

		public string RecentlyViewedPanelPath
		{
			get { return GetString(GroupName, "RecentlyViewedPanelPath"); }
		}


		public string TopSellersPanelPath
		{
			get { return GetString(GroupName, "TopSellersPanelPath"); }
		}


		public string ThoseWhoBoughtAlsoBoughtPanelPath
		{
			get { return GetString(GroupName, "ThoseWhoBoughtAlsoBoughtPanelPath"); }
		}

		public string ThoseWhoViewedAlsoViewedPanelPath
		{
			get { return GetString(GroupName, "ThoseWhoViewedAlsoViewedPanelPath"); }
		}

		public string ThoseWhoViewedBoughtPanelPath
		{
			get { return GetString(GroupName, "ThoseWhoViewedBoughtPanelPath"); }
		}


		public string RecommendBasedOnCartPanelPath
		{
			get { return GetString(GroupName, "RecommendBasedOnCartPanelPath"); }
		}

		public string RecommendBasedOnCustomerPanelPath
		{
			get { return GetString(GroupName, "RecommendBasedOnCustomerPanelPath"); }
		}

		public string RecommendBasedOnProductPanelPath
		{
			get { return GetString(GroupName, "RecommendBasedOnProductPanelPath"); }
		}


		public string FallbackPanelPath
		{
			get { return GetString(GroupName, "FallbackPanelPath"); }
		}
	}
}
