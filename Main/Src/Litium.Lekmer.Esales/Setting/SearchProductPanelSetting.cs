﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchProductPanelSetting : SearchFacetPanelSetting, ISearchProductPanelSetting
	{
		protected override string GroupName
		{
			get { return "SearchProductPanel-" + ChannelCommonName; }
		}

		public SearchProductPanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public virtual string ProductPanelName
		{
			get { return GetString(GroupName, "ProductPanelName"); }
		}

		public virtual string AutoCompletePanelName
		{
			get { return GetString(GroupName, "AutoCompletePanelName"); }
		}

		public virtual string DidYouMeanPanelName
		{
			get { return GetString(GroupName, "DidYouMeanPanelName"); }
		}

		public virtual string CategoryPanelName
		{
			get { return GetString(GroupName, "CategoryPanelName"); }
		}

		public virtual string TagPanelName
		{
			get { return GetString(GroupName, "TagPanelName"); }
		}

		public virtual string BrandPanelName
		{
			get { return GetString(GroupName, "BrandPanelName"); }
		}

		public virtual string BrandAllPanelName
		{
			get { return GetString(GroupName, "BrandAllPanelName"); }
		}
	}
}