﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Esales.Setting
{
	public class EsalesSetting : SettingBase, IEsalesSetting
	{
		protected override string StorageName
		{
			get { return "Esales"; }
		}

		protected virtual string GroupName
		{
			get { return "EsalesSettings"; }
		}


		public bool Enabled
		{
			get { return GetBoolean(GroupName, "Enabled"); }
		}

		public string ApplicationName
		{
			get { return GetString(GroupName, "ApplicationName"); }
		}

		public int PageSize
		{
			get { return GetInt32(GroupName, "PageSize", 100); }
		}

		public int BatchSize
		{
			get { return GetInt32(GroupName, "BatchSize", 1000); }
		}

		public string ControllerUrl
		{
			get { return GetString(GroupName, "ControllerUrl"); }
		}

		public string DestinationDirectoryProduct
		{
			get { return GetString(GroupName, "DestinationDirectoryProduct"); }
		}

		public string XmlFileNameAds
		{
			get { return GetString(GroupName, "XmlFileNameAds"); }
		}

		public string XmlFileNameProduct
		{
			get { return GetString(GroupName, "XmlFileNameProduct"); }
		}

		public string XmlFileNameProductIncremental
		{
			get { return GetString(GroupName, "XmlFileNameProductIncremental"); }
		}

		public string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}

		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);

			if (dictionary.ContainsKey(channelName))
			{
				return dictionary[channelName];
			}

			return channelName;
		}

		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);

			if (dictionary.ContainsKey(channelName))
			{
				return true;
			}

			return false;
		}

		// value1,value2, ... , valueN => List<value>
		protected virtual ICollection<string> ConvertToList(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
	}
}
