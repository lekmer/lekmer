﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesTopSellersV2 : LekmerBlockBase, IBlockEsalesTopSellersV2
	{
		private string _panelName;
		private int _windowLastEsalesValue;
		private bool _includeAllCategories;
		private int? _linkContentNodeId;
		private string _customUrl;
		private string _urlTitle;
		private IBlockSetting _setting;

		public string PanelName
		{
			get { return _panelName; }
			set
			{
				CheckChanged(_panelName, value);
				_panelName = value;
			}
		}
		public int WindowLastEsalesValue
		{
			get { return _windowLastEsalesValue; }
			set
			{
				CheckChanged(_windowLastEsalesValue, value);
				_windowLastEsalesValue = value;
			}
		}
		public bool IncludeAllCategories
		{
			get { return _includeAllCategories; }
			set
			{
				CheckChanged(_includeAllCategories, value);
				_includeAllCategories = value;
			}
		}
		public int? LinkContentNodeId
		{
			get { return _linkContentNodeId; }
			set
			{
				CheckChanged(_linkContentNodeId, value);
				_linkContentNodeId = value;
			}
		}
		public string CustomUrl
		{
			get { return _customUrl; }
			set
			{
				CheckChanged(_customUrl, value);
				_customUrl = value;
			}
		}
		public string UrlTitle
		{
			get { return _urlTitle; }
			set
			{
				CheckChanged(_urlTitle, value);
				_urlTitle = value;
			}
		}
		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}

		public Collection<IBlockEsalesTopSellersCategoryV2> Categories { get; set; }
		public Collection<IBlockEsalesTopSellersProductV2> Products { get; set; }

		public void PopulateEsalesParameters(Dictionary<string, string> parameters)
		{
			var windowLastKey = string.Format(EsalesConstants.WindowLastTemplate, PanelName);
			if (!parameters.ContainsKey(windowLastKey))
			{
				parameters.Add(windowLastKey, WindowLastEsalesValue.ToString(CultureInfo.InvariantCulture));
			}

			var filterTopSellersKey = string.Format(EsalesConstants.FilterTopSellersTemplate, PanelName);
			if (!parameters.ContainsKey(filterTopSellersKey))
			{
				if (!IncludeAllCategories && Categories != null && Categories.Count > 0)
				{
					parameters.Add(filterTopSellersKey, BuildFilterTopSellers());
				}
			}

			var filterProductListKey = string.Format(EsalesConstants.FilterProductListTemplate, PanelName);
			if (!parameters.ContainsKey(filterProductListKey))
			{
				if (Products != null && Products.Count > 0)
				{
					parameters.Add(filterProductListKey, BuildFilterProductList());
				}
				else
				{
					parameters.Add(filterProductListKey, EsalesConstants.DefaultFakeFilter);
				}
			}
		}

		protected virtual string BuildFilterTopSellers()
		{
			var filter = new StringBuilder();
			foreach (var category in Categories)
			{
				if (!category.IncludeSubcategories)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeCategoryId, category.Category.Id));
				}
				else
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeMainCategoryId, category.Category.Id));
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeParentCategoryId, category.Category.Id));
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeCategoryId, category.Category.Id));
				}
			}
			filter.Length -= 4;
			return filter.ToString();
		}

		protected virtual string BuildFilterProductList()
		{
			var filter = new StringBuilder();
			foreach (var product in Products)
			{
				filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeProductKey, product.Product.Id));
			}
			filter.Length -= 4;
			return filter.ToString();
		}
	}
}