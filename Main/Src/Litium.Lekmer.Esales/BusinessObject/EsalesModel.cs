﻿using System;
using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesModel : IEsalesModel
	{
		public int Id { get; set; }
		public string CommonName { get; set; }
		public string Title { get; set; }
		public int Ordinal { get; set; }

		public Dictionary<string, IEsalesModelParameter> Parameters { get; set; }

		public virtual string TryGetParameterValue(string parameterName, string defaultValue)
		{
			if (Parameters == null || Parameters.ContainsKey(parameterName) == false)
			{
				return defaultValue;
			}

			return Parameters[parameterName].Value;
		}
	}
}
