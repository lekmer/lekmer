﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesSetting : BusinessObjectBase, IBlockEsalesSetting
	{
		private int _blockId;
		private int? _esalesModelComponentId;

		public virtual int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public virtual int? EsalesModelComponentId
		{
			get { return _esalesModelComponentId; }
			set
			{
				CheckChanged(_esalesModelComponentId, value);
				_esalesModelComponentId = value;
			}
		}
	}
}
