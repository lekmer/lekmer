using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Apptus.ESales.Connector;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	[Serializable]
	public class AdsEsalesRequest : EsalesRequest, IAdsEsalesRequest
	{
		public virtual string Site { get; set; }

		public virtual string CustomerKey { get; set; }

		public virtual List<string> FormatList { get; set; }

		public virtual List<string> GenderList { get; set; }

		public virtual List<string> ProductCategoryList { get; set; }

		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		public virtual int MaxAds { get; set; }

		/// <summary>
		/// Compose set of search arguments
		/// </summary>
		/// <returns></returns>
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();

			argMap[EsalesArguments.Site] = Site;
			argMap[EsalesArguments.AdFilter] = BuildFilter();

			if (CustomerKey.HasValue())
			{
				argMap[EsalesArguments.CustomerKey] = CustomerKey;
			}

			argMap.Add(EsalesArguments.MaxAds, MaxAds.ToString(CultureInfo.InvariantCulture));

			return argMap;
		}


		/// <summary>
		/// Compose set of filters
		/// </summary>
		/// <returns></returns>
		protected virtual string BuildFilter()
		{
			var filter = new StringBuilder();

			if (FormatList != null && FormatList.Count > 0)
			{
				filter.Append("(");

				foreach (string format in FormatList)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeFormat, format));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			if (GenderList != null && GenderList.Count > 0)
			{
				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");

				foreach (string gender in GenderList)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeGender, gender));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			if (ProductCategoryList != null && ProductCategoryList.Count > 0)
			{
				if (filter.Length > 0)
				{
					filter.Append(" AND ");
				}

				filter.Append("(");

				foreach (string productCategory in ProductCategoryList)
				{
					filter.Append(string.Format(CultureInfo.InvariantCulture, "{0}:'{1}' OR ", EsalesXmlNodes.NodeProductCategory, productCategory));
				}

				filter.Length -= 4;
				filter.Append(")");
			}

			return filter.ToString();
		}
	}
}