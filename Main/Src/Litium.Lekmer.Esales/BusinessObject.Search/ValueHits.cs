using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class ValueHits : Result, IValueHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Values; }
		}

		public Collection<IValueInfo> ValuesInfo { get; set; }
	}
}