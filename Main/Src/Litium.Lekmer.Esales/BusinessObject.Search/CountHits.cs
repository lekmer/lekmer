using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class CountHits : Result, ICountHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Count; }
		}

		public int TotalHitCount { get; set; }
	}
}