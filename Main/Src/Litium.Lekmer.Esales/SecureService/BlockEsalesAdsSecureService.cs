using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesAdsSecureService : IBlockEsalesAdsSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockEsalesAdsRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public BlockEsalesAdsSecureService(
			IAccessValidator accessValidator,
			BlockEsalesAdsRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockEsalesAds Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockEsalesAds = IoC.Resolve<IBlockEsalesAds>();

			blockEsalesAds.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockEsalesAds.Setting = BlockSettingSecureService.Create();

			blockEsalesAds.Status = BusinessObjectStatus.New;

			return blockEsalesAds;
		}

		public virtual IBlockEsalesAds GetById(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetByIdSecure(blockId);
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockEsalesAds blockEsalesAds = Create();

			blockEsalesAds.ContentNodeId = contentNodeId;
			blockEsalesAds.ContentAreaId = contentAreaId;
			blockEsalesAds.BlockTypeId = blockTypeId;
			blockEsalesAds.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockEsalesAds.Title = title;
			blockEsalesAds.TemplateId = null;
			blockEsalesAds.Id = Save(systemUserFull, blockEsalesAds);

			return blockEsalesAds;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockEsalesAds block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			BlockEsalesAdsCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockEsalesAdsCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}