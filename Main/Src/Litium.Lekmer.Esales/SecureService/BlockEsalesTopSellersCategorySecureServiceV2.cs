using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesTopSellersCategorySecureServiceV2 : IBlockEsalesTopSellersCategorySecureServiceV2
	{
		protected BlockEsalesTopSellersCategoryRepositoryV2 Repository { get; private set; }

		public BlockEsalesTopSellersCategorySecureServiceV2(BlockEsalesTopSellersCategoryRepositoryV2 repository)
		{
			Repository = repository;
		}

		public virtual Collection<IBlockEsalesTopSellersCategoryV2> GetAllByBlock(int blockId)
		{
			return Repository.GetAllByBlockSecure(blockId);
		}

		public virtual void Save(int blockId, Collection<IBlockEsalesTopSellersCategoryV2> blockCategories)
		{
			if (blockCategories == null) throw new ArgumentNullException("blockCategories");

			Repository.DeleteAll(blockId);
			foreach (var blockCategory in blockCategories)
			{
				blockCategory.BlockId = blockId;
				Repository.Save(blockCategory);
			}
		}
	}
}