﻿using Litium.Lekmer.Esales.Cache;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesSettingSecureService : IBlockEsalesSettingSecureService
	{
		protected BlockEsalesSettingRepository Repository { get; private set; }

		public BlockEsalesSettingSecureService(BlockEsalesSettingRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockEsalesSetting Create()
		{
			var blockSetting = IoC.Resolve<IBlockEsalesSetting>();
			blockSetting.Status = BusinessObjectStatus.New;
			return blockSetting;
		}

		public virtual void Save(IBlockEsalesSetting blockEsalesSetting)
		{
			Repository.Save(blockEsalesSetting);
			BlockEsalesSettingCache.Instance.Remove(new BlockEsalesSettingKey(blockEsalesSetting.BlockId));
		}

		public virtual void Delete(int blockId)
		{
			Repository.Delete(blockId);
			BlockEsalesSettingCache.Instance.Remove(new BlockEsalesSettingKey(blockId));
		}
	}
}
