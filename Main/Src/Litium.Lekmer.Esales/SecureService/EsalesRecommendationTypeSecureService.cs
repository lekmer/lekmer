using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class EsalesRecommendationTypeSecureService : IEsalesRecommendationTypeSecureService
	{
		protected EsalesRecommendationTypeRepository Repository { get; private set; }

		public EsalesRecommendationTypeSecureService(EsalesRecommendationTypeRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IEsalesRecommendationType> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}
	}
}