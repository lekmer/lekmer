﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesSettingRepository
	{
		protected virtual DataMapperBase<IBlockEsalesSetting> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesSetting>(dataReader);
		}

		public virtual int Save(IBlockEsalesSetting blockEsalesSetting)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesSettingRepository.Save");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", blockEsalesSetting.BlockId, SqlDbType.Int),
				ParameterHelper.CreateParameter("EsalesModelComponentId", blockEsalesSetting.EsalesModelComponentId, SqlDbType.Int)
			};

			return new DataHandler().ExecuteCommandWithReturnValue("[esales].[pBlockEsalesSettingSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesSettingRepository.Delete");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
			};

			new DataHandler().ExecuteCommandWithReturnValue("[esales].[pBlockEsalesSettingDelete]", parameters, dbSettings);
		}

		public virtual IBlockEsalesSetting GetByBlock(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesSettingRepository.GetByBlock");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
			};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesSettingGetByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}
