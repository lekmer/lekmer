﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class AdRepository
	{
		protected virtual DataMapperBase<IAd> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IAd>(dataReader);
		}

		public virtual int Save(IAd ad)
		{
			if (ad == null)
			{
				throw new ArgumentNullException("ad");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdId", ad.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("FolderId", ad.FolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Key", ad.Key, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Title", ad.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("DisplayFrom", ad.DisplayFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("DisplayTo", ad.DisplayTo, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("ProductCategory", ad.ProductCategory, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Gender", ad.Gender, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Format", ad.Format, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("SearchTags", ad.SearchTags, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("AdRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[esales].[pAdSave]", parameters, dbSettings);
		}

		public virtual void Delete(int adId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdId", adId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdRepository.Delete");

			new DataHandler().ExecuteCommand("[esales].[pAdDelete]", parameters, dbSettings);
		}

		public virtual IAd GetById(int adId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdId", adId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IAd> GetAll()
		{
			var dbSettings = new DatabaseSetting("AdRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IAd> GetAllByFolder(int folderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("FolderId", folderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("AdRepository.GetAllByFolder");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pAdGetAllByFolder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual bool IsUniqueKey(int? adId, string key)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("AdId", adId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Key", key, SqlDbType.NVarChar),
				};

			var dbSettings = new DatabaseSetting("AdRepository.IsUniqueKey");

			return new DataHandler().ExecuteCommandWithReturnValue("[esales].[pAdIsUniqueKey]", parameters, dbSettings) == 0;
		}
	}
}