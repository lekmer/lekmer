﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesTopSellersProductRepositoryV2
	{
		protected virtual DataMapperBase<IBlockEsalesTopSellersProductV2> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesTopSellersProductV2>(dataReader);
		}


		public virtual void Save(IBlockEsalesTopSellersProductV2 blockProduct)
		{
			if (blockProduct == null) throw new ArgumentNullException("blockProduct");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockProduct.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", blockProduct.Product.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Position", blockProduct.Position, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersProductRepositoryV2.Save");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2ProductSave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersProductRepositoryV2.DeleteAll");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2ProductDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockEsalesTopSellersProductV2> GetAllByBlockSecure(int blockId, int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersProductRepositoryV2.GetAllByBlockSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2ProductGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockEsalesTopSellersProductV2> GetAllByBlock(int blockId, int languageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersProductRepositoryV2.GetAllByBlock");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2ProductGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}