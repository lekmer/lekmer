﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class AdFolderDataMapper : DataMapperBase<IAdFolder>
	{
		public AdFolderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IAdFolder Create()
		{
			var folder = IoC.Resolve<IAdFolder>();

			folder.Id = MapValue<int>("AdFolder.AdFolderId");
			folder.ParentFolderId = MapNullableValue<int?>("AdFolder.ParentFolderId");
			folder.Title = MapValue<string>("AdFolder.Title");

			folder.SetUntouched();

			return folder;
		}
	}
}