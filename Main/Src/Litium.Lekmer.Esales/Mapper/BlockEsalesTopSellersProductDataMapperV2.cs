using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesTopSellersProductDataMapperV2 : DataMapperBase<IBlockEsalesTopSellersProductV2>
	{
		private DataMapperBase<IProduct> _productDataMapper;

		public BlockEsalesTopSellersProductDataMapperV2(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_productDataMapper = DataMapperResolver.Resolve<IProduct>(DataReader);
		}

		protected override IBlockEsalesTopSellersProductV2 Create()
		{
			var blockProduct = IoC.Resolve<IBlockEsalesTopSellersProductV2>();
			blockProduct.BlockId = MapValue<int>("BlockEsalesTopSellersV2Product.BlockId");
			blockProduct.Position = MapValue<int>("BlockEsalesTopSellersV2Product.Position");
			blockProduct.Product = _productDataMapper.MapRow();
			blockProduct.SetUntouched();
			return blockProduct;
		}
	}
}