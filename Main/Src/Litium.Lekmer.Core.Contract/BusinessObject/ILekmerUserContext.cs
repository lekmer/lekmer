using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ILekmerUserContext : IUserContext
	{
		IVoucherCheckResult Voucher { get; set; }
	}
}