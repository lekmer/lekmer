using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ILekmerChannelService : IChannelService
	{
		Collection<IChannel> GetAll();
	}
}