﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Campaign.GiftCardViaMailMessaging.Service
{
	public class GiftCardViaMailScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "GiftCardViaEmailMessagingService"; }
		}

		protected override string GroupName
		{
			get { return "GiftCardViaMailJob"; }
		}
	}
}