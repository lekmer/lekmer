using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Review.Web
{
	public class BlockProductReviewSummaryControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IReviewSummaryService _reviewSummaryService;
		private readonly IBlockService _blockService;

		public BlockProductReviewSummaryControl(ITemplateFactory templateFactory, IReviewSummaryService reviewSummaryService, IBlockService blockService)
			: base(templateFactory)
		{
			_reviewSummaryService = reviewSummaryService;
			_blockService = blockService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null) return new BlockContent(null, "[ Can't review summary control on this page ]");

			IReviewSummary reviewSummary = _reviewSummaryService.GetByProduct(UserContext.Current, Product.Id);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(reviewSummary);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}
	}
}