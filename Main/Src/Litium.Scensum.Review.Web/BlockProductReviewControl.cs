using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Scensum.Review.Web
{
	public class BlockProductReviewControl : BlockProductPageControlBase<IBlockProductReview>
	{
		private readonly IBlockProductReviewService _blockProductReviewService;
		private readonly IReviewService _reviewService;
		private IPagingControl _pagingControl;
		private ReviewCollection _reviews;
		private ReviewForm _reviewForm;
		private readonly ICustomerSession _customerSession;
		private bool _reviewAdded;

		public BlockProductReviewControl(ITemplateFactory templateFactory,
										 IBlockProductReviewService blockProductReviewService,
										 IReviewService reviewService, ICustomerSession customerSession)
			: base(templateFactory)
		{
			_blockProductReviewService = blockProductReviewService;
			_reviewService = reviewService;
			_customerSession = customerSession;
		}

		protected override IBlockProductReview GetBlockById(int blockId)
		{
			return _blockProductReviewService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null) return new BlockContent(null, "[ Can't render review control on this page ]");

			Initialize();

			_pagingControl.TotalCount = _reviews.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ReviewList", RenderReviewList(), VariableEncoding.None);
			fragmentContent.AddVariable("ReviewForm", ProcessReviewForm(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);
			fragmentContent.AddCondition("ReviewAdded", _reviewAdded);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private void Initialize()
		{
			_reviewAdded = Request.QueryString.GetBooleanOrNull(BlockId + "-review-added") ?? false;
			_pagingControl = CreatePagingControl();
			_reviews = _reviewService.GetAllByProduct(UserContext.Current, Product.Id, _pagingControl.SelectedPage, _pagingControl.PageSize, !_reviewAdded);
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		private string RenderReviewList()
		{
			var list = new ListControl<IReview>
			{
				Template = Template,
				ListFragmentName = "ReviewList",
				ItemFragmentName = "Review",
				Items = _reviews
			};
			return list.Render();
		}

		private IPagingControl CreatePagingControl()
		{
			var lekmerProduct = (ILekmerProduct)Product;
			var pagingControl = IoC.Resolve<IReviewPagingControl>();
			string url = lekmerProduct.LekmerUrl;
			if (lekmerProduct.ParentPageUrl != null && !string.IsNullOrEmpty(lekmerProduct.ParentPageUrl))
			{
				string tempParentPageUrl = lekmerProduct.ParentPageUrl.Replace("~/", "");
				url = url.Replace(tempParentPageUrl, "");
			}
			pagingControl.PageBaseUrl = ResolveUrl(url);
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.ReviewAddedQueryStringParameterName = BlockId + "-review-added";
			pagingControl.PageSize = Block.RowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		private ReviewForm ReviewForm
		{
			get
			{
				if (_reviewForm == null)
				{
					var queryBuilder = new QueryBuilder(Request.QueryString);
					queryBuilder.Remove(BlockId + "-review-added");

					string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);
					_reviewForm = new ReviewForm(postUrl);
				}
				return _reviewForm;
			}
		}

		private string ProcessReviewForm()
		{
			var signedInCustomer = _customerSession.SignedInCustomer;

			ValidationResult validationResult = null;
			if (ReviewForm.IsFormPostBack)
			{
				ReviewForm.MapFromRequest();
				validationResult = ReviewForm.Validate();
				if (validationResult.IsValid)
				{
					IReview review = _reviewService.Create(UserContext.Current);
					ReviewForm.MapToReview(review);
					review.ProductId = Product.Id;

					_reviewService.Save(UserContext.Current, review);

					return RenderReviewSavedResponse();
				}
			}
			else
			{
				ReviewForm.SetDefaultValues(signedInCustomer);
			}

			return RenderReviewForm(validationResult);
		}

		private string RenderReviewForm(ValidationResult validationResult)
		{
			string validationError = null;
			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragment = Template.GetFragment("ReviewForm");
			ReviewForm.MapToFragment(fragment);
			fragment.AddVariable("ValidationError", validationError, VariableEncoding.None);
			return fragment.Render();
		}

		private string RenderReviewSavedResponse()
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			queryBuilder.ReplaceOrAdd(BlockId + "-review-added", "true");

			// Remove the page parameter to redirect to the first page.
			queryBuilder.Remove(BlockId + "-page");

			Response.Redirect("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);

			return null;
		}
	}
}