﻿using System.ServiceProcess;

namespace Litium.Lekmer.IndexBuilder.Service
{
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		private static void Main()
		{
			var servicesToRun = new ServiceBase[]
			                    	{
			                    		new IndexBuilderServiceHost()
			                    	};
			ServiceBase.Run(servicesToRun);
		}
	}
}