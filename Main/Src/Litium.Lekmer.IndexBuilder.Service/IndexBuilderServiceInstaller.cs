﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Litium.Lekmer.IndexBuilder.Service
{
	[RunInstaller(true)]
	public partial class IndexBuilderServiceInstaller : System.Configuration.Install.Installer
	{
		public IndexBuilderServiceInstaller()
		{
			InitializeComponent();
		}
	}
}
