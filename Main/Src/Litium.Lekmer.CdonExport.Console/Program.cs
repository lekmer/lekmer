﻿using System;
using System.Linq;
using System.Reflection;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.CdonExport.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main(string[] args)
		{
			try
			{
				string value = "/full";

				if (args.Length == 0)
				{
					System.Console.WriteLine("Posible arguments:");
					System.Console.WriteLine("    /full  runs export to cdon of all products;");
					System.Console.WriteLine("    /inc  runs incremental export to cdon of changed products;");
					value = System.Console.In.ReadLine();
				}

				if (value == "/full")
				{
					CdonExportFull();
				}

				if (value == "/inc")
				{
					CdonExportIncremental();
				}
			}
			catch (Exception ex)
			{
				_log.Error("CdonExport.Console failed.", ex);
			}
		}

		private static void CdonExportFull()
		{
			var exporter = IoC.Resolve<IExporter>("CdonFullExporter");
			exporter.Execute();
		}

		private static void CdonExportIncremental()
		{
			var exporter = IoC.Resolve<IExporter>("CdonIncrementalExporter");
			exporter.Execute();
		}
	}
}