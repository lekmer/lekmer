﻿namespace Litium.Lekmer.Customer
{
	public interface IFacebookUserService
	{
		IFacebookUser Create();
		void Save(IFacebookUser user);
		void DeleteAlternateAccount(IFacebookUser user);
	}
}
