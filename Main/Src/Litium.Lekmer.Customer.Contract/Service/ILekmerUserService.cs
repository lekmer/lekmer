﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerUserService : IUserService
	{
		void DeleteAlternateAccount(IUser user);
	}
}