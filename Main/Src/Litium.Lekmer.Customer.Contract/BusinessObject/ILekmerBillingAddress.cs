﻿using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerBillingAddress : IBillingAddress, ILekmerAddress
	{
	}
}
