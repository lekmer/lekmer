﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Customer
{
	public interface IGenderTypeSecureService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IGenderType> GetAll();
	}
}
