using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public class LekmerDeliveryMethodSecureService : DeliveryMethodSecureService, ILekmerDeliveryMethodSecureService
	{
		protected LekmerDeliveryMethodRepository LekmerRepository { get; private set; }

		public LekmerDeliveryMethodSecureService(LekmerDeliveryMethodRepository repository)
			: base(repository)
		{
			LekmerRepository = repository;
		}

		public virtual Collection<IDeliveryMethod> GetAll()
		{
			return LekmerRepository.GetAll();
		}
	}
}