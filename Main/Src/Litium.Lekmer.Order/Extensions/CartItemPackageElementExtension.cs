﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Order
{
	public static class CartItemPackageElementExtension
	{
		public static int? GetPackageElementsHashCode(this Collection<ICartItemPackageElement> packageElements)
		{
			if (packageElements == null || packageElements.Count == 0) return null;

			var elements = new List<ICartItemPackageElement>(packageElements);
			elements.Sort(delegate(ICartItemPackageElement element1, ICartItemPackageElement element2)
			{
				if (element1.ProductId < element2.ProductId)
					return -1;
				if (element1.ProductId > element2.ProductId)
					return 1;
				return 0;
			});

			string elementsInfo = string.Empty;
			foreach (var element in elements)
			{
				elementsInfo += element.ProductId + '-' + element.SizeId;
			}

			var hashCode = elementsInfo.GetHashCode();
			if (hashCode < 0)
			{
				hashCode = hashCode*(-1);
			}

			return hashCode;
		}
	}
}