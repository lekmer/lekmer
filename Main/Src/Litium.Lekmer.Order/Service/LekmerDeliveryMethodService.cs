﻿using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Order.Cache;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Cache;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerDeliveryMethodService : DeliveryMethodService, ILekmerDeliveryMethodService
	{
		protected IDeliveryMethodWeightPriceService DeliveryMethodWeightPriceService { get; private set; }
		protected LekmerDeliveryMethodRepository LekmerRepository { get; private set; }

		private static IDeliveryMethod _deliveryMethodNull;

		protected static IDeliveryMethod DeliveryMethodNull
		{
			get
			{
				if (_deliveryMethodNull == null)
				{
					_deliveryMethodNull = IoC.Resolve<IDeliveryMethod>();
					_deliveryMethodNull.Id = int.MinValue;
					_deliveryMethodNull.CommonName = "DeliveryMethodNull";
					_deliveryMethodNull.Title = "DeliveryMethodNull";
				}

				return _deliveryMethodNull;
			}
		}

		public LekmerDeliveryMethodService(DeliveryMethodRepository repository, IDeliveryMethodWeightPriceService deliveryMethodWeightPriceService)
			: base(repository)
		{
			DeliveryMethodWeightPriceService = deliveryMethodWeightPriceService;
			LekmerRepository = (LekmerDeliveryMethodRepository)repository;
		}

		public override IDeliveryMethod GetById(IUserContext context, int deliveryMethodId)
		{
			return DeliveryMethodCache.Instance.TryGetItem(
				new DeliveryMethodKey(context.Channel.Id, deliveryMethodId),
				() => GetByIdCore(context, deliveryMethodId)
			);
		}
		protected override IDeliveryMethod GetByIdCore(IUserContext context, int deliveryMethodId)
		{
			var deliveryMethod = Repository.GetById(context.Channel.Id, deliveryMethodId);

			if (deliveryMethod != null)
			{
				((ILekmerDeliveryMethod) deliveryMethod).WeightPrices = DeliveryMethodWeightPriceService.GetAllByDeliveryMethod(context, deliveryMethod.Id);
			}

			return deliveryMethod;
		}

		public override Collection<IDeliveryMethod> GetAll(IUserContext context)
		{
			return DeliveryMethodCollectionCache.Instance.TryGetItem(
				new DeliveryMethodCollectionKeyAll(context.Channel.Id),
				() => GetAllCore(context)
			);
		}
		protected virtual Collection<IDeliveryMethod> GetAllCore(IUserContext context)
		{
			var deliveryMethods = base.GetAll(context);

			foreach (var deliveryMethod in deliveryMethods)
			{
				((ILekmerDeliveryMethod) deliveryMethod).WeightPrices = DeliveryMethodWeightPriceService.GetAllByDeliveryMethod(context, deliveryMethod.Id);
			}

			return deliveryMethods;
		}

		public override Collection<IDeliveryMethod> GetAllByPaymentType(IUserContext context, int paymentTypeId)
		{
			return DeliveryMethodCollectionCache.Instance.TryGetItem(
				new DeliveryMethodCollectionKeyByPaymentType(context.Channel.Id, paymentTypeId),
				() => GetAllByPaymentTypeCore(context, paymentTypeId)
			);
		}
		protected virtual Collection<IDeliveryMethod> GetAllByPaymentTypeCore(IUserContext context, int paymentTypeId)
		{
			var deliveryMethods = base.GetAllByPaymentType(context, paymentTypeId);

			foreach (var deliveryMethod in deliveryMethods)
			{
				((ILekmerDeliveryMethod) deliveryMethod).WeightPrices = DeliveryMethodWeightPriceService.GetAllByDeliveryMethod(context, deliveryMethod.Id);
			}

			return deliveryMethods;
		}

		public virtual IDeliveryMethod GetOptionalByDeliveryMethod(IUserContext context, int deliveryMethodId)
		{
			IDeliveryMethod deliveryMethod = OptionalDeliveryMethodCache.Instance.TryGetItem(
				new OptionalDeliveryMethodKey(context.Channel.Id, deliveryMethodId), 
				() => GetOptionalByDeliveryMethodCore(context, deliveryMethodId)
			);

			if (deliveryMethod != null && deliveryMethod.Id == DeliveryMethodNull.Id)
			{
				return null;
			}

			return deliveryMethod;
		}

		protected virtual IDeliveryMethod GetOptionalByDeliveryMethodCore(IUserContext context, int deliveryMethodId)
		{
			return LekmerRepository.GetOptionalByDeliveryMethod(context.Channel.Id, deliveryMethodId) ?? DeliveryMethodNull;
		}

		public virtual IDeliveryMethodWeightPrice GetMatchingWeightPrice(ILekmerDeliveryMethod deliveryMethod, decimal cartWeight)
		{
			return deliveryMethod.WeightPrices.FirstOrDefault(weightPrice => 
				(weightPrice.WeightFrom == null && weightPrice.WeightTo == null) || 
				(weightPrice.WeightFrom == null && cartWeight <= weightPrice.WeightTo) || 
				(weightPrice.WeightTo == null && weightPrice.WeightFrom < cartWeight ) || 
				(weightPrice.WeightFrom < cartWeight && cartWeight <= weightPrice.WeightTo));
		}
	}
}