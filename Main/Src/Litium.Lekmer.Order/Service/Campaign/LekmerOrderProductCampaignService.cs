﻿using System.Collections.ObjectModel;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public class LekmerOrderProductCampaignService : OrderProductCampaignService, ILekmerOrderProductCampaignService
	{
		protected OrderProductCampaignRepository Repository { get; private set; }

		public LekmerOrderProductCampaignService(OrderProductCampaignRepository repository)
			: base(repository)
		{
			Repository = repository;
		}

		public Collection<IOrderCampaign> GetAllByOrderItem(int orderItemId)
		{
			return Repository.GetAllByOrderItem(orderItemId);
		}
	}
}