﻿using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerPaymentTypeService : PaymentTypeService
	{
		public LekmerPaymentTypeService(PaymentTypeRepository repository)
			: base(repository)
		{
		}
	}
}
