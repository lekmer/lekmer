using System.Collections.ObjectModel;
using Litium.Framework.Messaging;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public class TrustPilotService : ITrustPilotService
	{
		public ITrustPilotSetting TrustPilotSetting { get; set; }

		public TrustPilotService(ITrustPilotSetting trustPilotSetting)
		{
			TrustPilotSetting = trustPilotSetting;
		}

		public void InscribeMirrorRecipient(IChannel channel, Collection<Recipient> recipients)
		{
			if (TrustPilotSetting.Active(channel))
			{
				recipients.Add(new EmailRecipient
				{
					Name = TrustPilotSetting.RecipientName(channel),
					Address = TrustPilotSetting.RecipientAddress(channel)
				});
			}
		}
	}
}