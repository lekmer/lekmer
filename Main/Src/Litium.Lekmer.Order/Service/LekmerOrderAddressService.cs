﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderAddressService : OrderAddressService
	{
		protected LekmerOrderAddressRepository LekmerRepository { get; private set; }

		public LekmerOrderAddressService(OrderAddressRepository repository)
			: base(repository)
		{
			LekmerRepository = repository as LekmerOrderAddressRepository;
		}

		public override int Save(IUserContext context, IOrderAddress orderAddress)
		{
			if (orderAddress == null) throw new ArgumentNullException("orderAddress");

			LekmerRepository.EnsureNotNull();

			using (var transactedOperation = new TransactedOperation())
			{
				orderAddress.Id = LekmerRepository.Save(orderAddress);
				LekmerRepository.SaveLekmerPart(orderAddress as ILekmerOrderAddress);

				orderAddress.SetUntouched();

				transactedOperation.Complete();
			}

			return orderAddress.Id;
		}
	}
}
