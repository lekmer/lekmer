using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Avail.Web.Helper;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerCartItemService : ICartItemService
	{
		protected LekmerCartItemRepository Repository { get; private set; }
		protected ICartItemPackageElementService CartItemPackageElementService { get; private set; }

		public LekmerCartItemService(
			LekmerCartItemRepository cartItemRepository,
			ICartItemPackageElementService cartItemPackageElementService
			)
		{
			Repository = cartItemRepository;
			CartItemPackageElementService = cartItemPackageElementService;
		}

		public virtual ICartItem Create()
		{
			var cartItem = IoC.Resolve<ICartItem>();
			cartItem.Status = BusinessObjectStatus.New;
			cartItem.CreatedDate = DateTime.Now;

			var product = IoC.Resolve<IProduct>();
			cartItem.Product = product;

			var priceListItem = IoC.Resolve<IPriceListItem>();
			cartItem.Product.Price = priceListItem;

			return cartItem;
		}

		public virtual void Save(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			using (var transactedOperation = new TransactedOperation())
			{
				BaseSave(context, cartItem);

				var lekmerCartItem = (ILekmerCartItem)cartItem;

				Repository.Save(lekmerCartItem);

				if (cartItem.Product.IsPackage() && lekmerCartItem.PackageElements != null)
				{
					CartItemPackageElementService.Save(lekmerCartItem.Id, lekmerCartItem.PackageElements);
				}

				transactedOperation.Complete();
			}
		}

		public virtual void Delete(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			using (var transactedOperation = new TransactedOperation())
			{
				if (cartItem.Product.IsPackage())
				{
					CartItemPackageElementService.DeleteByCartItem(cartItem.Id);
				}

				Repository.DeleteLekmerCartItem(cartItem.Id);

				BaseDelete(context, cartItem);

				transactedOperation.Complete();
			}

			var availLoggingHelper = new AvailLoggingHelper();
			availLoggingHelper.SendAvailLogRemovedFromCart(cartItem.Product.Id);
		}

		public virtual Collection<ICartItem> GetAllByCart(IUserContext context, int cartId)
		{
			Collection<ICartItem> cartItems = Repository.GetAllByCart(cartId, context.Customer, context.Channel.Id);

			if (cartItems != null && ContainPackages(cartItems))
			{
				Collection<ICartItemPackageElement> cartItemPackageElements = CartItemPackageElementService.GetAllByCart(context, cartId);

				MergeWithPackageElements(cartItems, cartItemPackageElements);
			}

			return cartItems ?? new Collection<ICartItem>();
		}


		protected virtual void BaseSave(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			Repository.Save(cartItem);
		}

		protected virtual void BaseDelete(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			Repository.Delete(cartItem.Id);
		}

		protected virtual void MergeWithPackageElements(Collection<ICartItem> cartItems, Collection<ICartItemPackageElement> cartItemPackageElements)
		{
			Dictionary<int, ICartItem> cartItemsDict = cartItems.ToDictionary(ci => ci.Id);

			foreach (ICartItemPackageElement cartItemPackageElement in cartItemPackageElements)
			{
				if (cartItemsDict.ContainsKey(cartItemPackageElement.CartItemId))
				{
					var cartItem = (ILekmerCartItem) cartItemsDict[cartItemPackageElement.CartItemId];

					if (cartItem.PackageElements == null)
					{
						cartItem.PackageElements = new Collection<ICartItemPackageElement>();
					}

					cartItem.PackageElements.Add(cartItemPackageElement);
				}
			}
		}

		protected virtual bool ContainPackages(Collection<ICartItem> cartItems)
		{
			if (cartItems == null)
			{
				return false;
			}

			return cartItems.Any(cartItem => cartItem.Product.IsPackage());
		}
	}
}