﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Cache;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;
using Litium.Scensum.Product;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Order
{
	public class LekmerCartService : CartService, ILekmerCartService
	{
		protected virtual LekmerCartRepository LekmerCartRepository { get; private set; }
		protected virtual ILekmerCartSession LekmerCartSession { get; private set; }
		protected virtual ICartCookieService CartCookieService { get; private set; }
		protected virtual ILekmerCartSetting CartSetting { get; private set; }
		
		public LekmerCartService(
			CartRepository cartRepository,
			ICartItemService cartItemService,
			ICartCampaignProcessor cartCampaignProcessor,
			IProductService productService,
			ILekmerCartSession lekmerCartSession,
			ICartCookieService cartCookieService,
			ILekmerCartSetting cartSetting)
			: base(cartRepository, cartItemService, cartCampaignProcessor, productService)
		{
			LekmerCartRepository = (LekmerCartRepository)cartRepository;
			LekmerCartSession = lekmerCartSession;
			CartCookieService = cartCookieService;
			CartSetting = cartSetting;
		}

		public virtual ICartFull Create(IUserContext context, string ipAddress)
		{
			var cart = (ILekmerCartFull) base.Create(context);
			cart.IPAddress = ipAddress;
			return cart;
		}

		/// <summary>
		/// Gets a Cart by id
		/// </summary>
		public override ICartFull GetById(IUserContext context, int id)
		{
			if (CartItemService == null)
			{
				throw new InvalidOperationException("CartItemService must be set before calling GetById.");
			}

			// Since cart is cached, we use session to detect obsolete cache items.
			Guid cartVersionToken = LekmerCartSession.CartVersionToken;
			if (cartVersionToken == Guid.Empty)
			{
				cartVersionToken = LekmerCartSession.CartVersionToken = Guid.NewGuid();
			}

			var cart = (ILekmerCartFull)CartFullCache.Instance.GetData(new CartFullKey(context.Channel.Id, id));

			if (cart != null)
			{
				if (cart.VersionToken != cartVersionToken)
				{
					cart = null;
				}
			}

			if (cart == null)
			{
				cart = (ILekmerCartFull)GetByIdCore(context, id);

				if (cart != null)
				{
					cart.VersionToken = cartVersionToken;
					CartFullCache.Instance.Add(new CartFullKey(context.Channel.Id, id), cart);
				}
			}

			if (cart != null)
			{
				context.Cart = cart;
				ApplyCampaigns(context, cart);

				CheckItemsType(cart);
			}

			return cart;
		}

		/// <summary>
		/// Gets a Cart by cart guid
		/// </summary>
		public override ICartFull GetByCartGuid(IUserContext context, Guid cartGuid)
		{
			if (CartItemService == null)
			{
				throw new InvalidOperationException("CartItemService must be set before calling GetByCartGuid.");
			}

			ICartFull cart = Repository.GetByCartGuid(cartGuid);
			if (cart == null)
			{
				return null;
			}

			cart.SetItems(CartItemService.GetAllByCart(context, cart.Id));

			context.Cart = cart;
			ApplyCampaigns(context, cart);
			CheckItemsType(cart);

			return cart;
		}

		/// <summary>
		/// Saves a Cart and its CartItems to the database.
		/// </summary>
		public override int Save(IUserContext context, ICartFull cart)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (cart == null) throw new ArgumentNullException("cart");
			if (CartItemService == null) throw new InvalidOperationException("CartItemService must be set before calling Save.");

			Collection<ICartItem> cartItems;
			var lekmerCart = (ILekmerCartFull) cart;

			using (var transactedOperation = new TransactedOperation())
			{
				bool isNeedUpdate = false;
				if (cart.IsNew)
				{
					cart.Id = Repository.Insert(cart);
				}
				else
				{
					isNeedUpdate = true;
				}

				cartItems = CloneCartItems(cart, false);
				cartItems = GetDistinguishCartItems(cartItems);
				foreach (ICartItem cartItem in cartItems)
				{
					cartItem.CartId = cart.Id;
					((ILekmerCartItem) cartItem).IPAddress = lekmerCart.IPAddress;
					CartItemService.Save(context, cartItem);
				}

				int deletedItemsCount = 0;
				var deletedCartItems = GetGarbageCartItems(cart, cartItems);
				foreach (ICartItem cartItem in deletedCartItems)
				{
					CartItemService.Delete(context, cartItem);
					deletedItemsCount++;
				}

				if (isNeedUpdate)
				{
					Update(cart.CartGuid, deletedItemsCount);
				}

				transactedOperation.Complete();
			}

			LekmerCartSession.CartVersionToken = lekmerCart.VersionToken = Guid.NewGuid();

			CartFullCache.Instance.Remove(new CartFullKey(context.Channel.Id, cart.Id));

			cart.DeleteAllItems();
			cart.SetItems(cartItems);

			cart.SetUntouched();

			context.Cart = cart;
			ApplyCampaigns(context, cart);

			if (cartItems.Count == 0)
			{
				CartCookieService.DeleteShoppingCardGuid();
			}
			else
			{
				Guid shoppingCardGiud = CartCookieService.GetShoppingCardGuid();
				if (Guid.Empty == shoppingCardGiud || shoppingCardGiud != cart.CartGuid)
				{
					CartCookieService.SetShoppingCardGuid(cart.CartGuid);
				}
			}

			return cart.Id;
		}

		public override void ApplyProductCampaigns(IUserContext context, ICartFull cart)
		{
			if (cart == null)
			{
				throw new ArgumentNullException("cart");
			}

			if (ProductService == null)
			{
				throw new InvalidOperationException("ProductService cannot be null.");
			}

			var items = new Collection<ICartItem>();

			var lekmerCart = (ILekmerCartFull)cart;
			foreach (ILekmerCartItem cartItem in lekmerCart.GetCartItems())
			{
				// TODO: Consider to improve this part
				var product = cartItem.Product;
				product = ProductService.GetById(context, product.Id);
				if (product != null)
				{
					for (int i = 0; i < cartItem.Quantity; i++)
					{
						items.Add(CreateCartItem(product, 1, cartItem, lekmerCart.Id));
					}
				}
			}

			lekmerCart.DeleteAllItems();
			foreach (ILekmerCartItem cartItem in items)
			{
				lekmerCart.AddItem(cartItem.Product, 1, cartItem.SizeId, cartItem.IsAffectedByCampaign, cartItem.Id, cartItem.PackageElements);
			}
		}

		public virtual void RetrivePreviousShoppingCard()
		{
			Guid shoppingCardGiud = CartCookieService.GetShoppingCardGuid();
			if (Guid.Empty != shoppingCardGiud)
			{
				var cart = (ILekmerCartFull) GetByCartGuid(UserContext.Current, shoppingCardGiud);
				if (cart != null)
				{
					DateTime startLifeCycleDate = DateTime.Now.AddDays(-CartSetting.LifeCycleValue);

					if ((cart.UpdatedDate == null && startLifeCycleDate <= cart.CreatedDate)
						|| (cart.UpdatedDate != null && startLifeCycleDate <= cart.UpdatedDate))
					{
						if (LekmerCartSession.Cart == null)
						{
							LekmerCartSession.Cart = cart;
							return;
						}
					}
				}

				CartCookieService.DeleteShoppingCardGuid();
			}
		}

		public virtual void Update(Guid cartGuid, int removedItemsCount)
		{
			LekmerCartRepository.Update(cartGuid, removedItemsCount);
		}

		public virtual void DeleteExpiredItems()
		{
			LekmerCartRepository.EnsureNotNull();

			LekmerCartRepository.DeleteExpiredItems(CartSetting.LifeCycleValue);
		}


		public virtual ILekmerCartFull CloneCart(ILekmerCartFull cart)
		{
			var cloneCart = ObjectCopier.Clone(cart);
			cloneCart.Id = -1;
			cloneCart.CreatedDate = DateTime.Now;
			cloneCart.CartGuid = Guid.NewGuid();
			cloneCart.VersionToken = Guid.NewGuid();
			cloneCart.Status = BusinessObjectStatus.New;
			return cloneCart;
		}

		public virtual int SaveCloneCart(IUserContext context, ILekmerCartFull cart)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (cart == null) throw new ArgumentNullException("cart");
			if (CartItemService == null) throw new InvalidOperationException("CartItemService must be set before calling Save.");

			Collection<ICartItem> cartItems;

			using (var transactedOperation = new TransactedOperation())
			{
				bool isNeedUpdate = false;
				if (cart.IsNew)
				{
					cart.Id = Repository.Insert(cart);
					cartItems = CloneCartItems(cart, true);
				}
				else
				{
					isNeedUpdate = true;
					cartItems = CloneCartItems(cart, false);
				}

				cartItems = GetDistinguishCartItems(cartItems);
				foreach (ICartItem cartItem in cartItems)
				{
					cartItem.CartId = cart.Id;
					((ILekmerCartItem) cartItem).IPAddress = cart.IPAddress;
					CartItemService.Save(context, cartItem);
				}

				int deletedItemsCount = 0;
				var deletedCartItems = GetGarbageCartItems(cart, cartItems);
				foreach (ICartItem cartItem in deletedCartItems)
				{
					CartItemService.Delete(context, cartItem);
					deletedItemsCount++;
				}

				if (isNeedUpdate)
				{
					Update(cart.CartGuid, deletedItemsCount);
				}

				transactedOperation.Complete();
			}

			var cacheKey = new CartFullKey(context.Channel.Id, cart.Id);
			if (CartFullCache.Instance.Contains(cacheKey))
			{
				CartFullCache.Instance.Remove(cacheKey);
			}

			cart.DeleteAllItems();
			cart.SetItems(cartItems);
			cart.SetUntouched();

			return cart.Id;
		}

		public virtual ICartFull GetByIdAndCartGuid(IUserContext context, int id, Guid cartGuid)
		{
			if (CartItemService == null)
			{
				throw new InvalidOperationException("CartItemService must be set before calling GetById.");
			}

			ICartFull cart = CartFullCache.Instance.GetData(new CartFullKey(context.Channel.Id, id));
			if (cart == null)
			{
				cart = LekmerCartRepository.GetByIdAndCartGuid(id, cartGuid);
				if (cart == null)
				{
					return null;
				}
				cart.SetItems(CartItemService.GetAllByCart(context, cart.Id));
				CartFullCache.Instance.Add(new CartFullKey(context.Channel.Id, id), cart);
			}

			ApplyCampaigns(context, cart);
			CheckItemsType(cart);
			return cart;
		}


		protected virtual ICartFull GetByIdCore(IUserContext context, int id)
		{
			ICartFull cart = Repository.GetById(id);

			if (cart != null)
			{
				cart.SetItems(CartItemService.GetAllByCart(context, cart.Id));
			}

			return cart;
		}

		protected virtual ILekmerCartItem CreateCartItem(IProduct product, int quantity, ILekmerCartItem cartItem, int cartId)
		{
			var item = (ILekmerCartItem)CartItemService.Create();

			item.CartId = cartId;
			item.Product = product;
			item.Quantity = quantity;
			item.SizeId = cartItem.SizeId;
			item.IsAffectedByCampaign = cartItem.IsAffectedByCampaign;
			item.Id = cartItem.Id;

			item.PackageElements = cartItem.PackageElements;

			return item;
		}

		protected virtual Collection<ICartItem> CloneCartItems(ICartFull cart, bool needCleanIds)
		{
			var originalCartItems = cart.GetCartItems();
			var cartItems = ObjectCopier.Clone(originalCartItems);

			if (needCleanIds)
			{
				foreach (var cartItem in cartItems)
				{
					cartItem.Id = -1;
					cartItem.CartId = cart.Id;
					cartItem.CreatedDate = cart.CreatedDate;

					if (cartItem.Product.IsPackage())
					{
						((ILekmerCartItem)cartItem).CleanPackageElementsId();
					}
				}
				cart.SetItems(cartItems);
			}

			return cartItems;
		}

		protected virtual Collection<ICartItem> GetDistinguishCartItems(Collection<ICartItem> cartItems)
		{
			var groupedCartItems = cartItems.GroupBy(c => string.Format("p{0}-s{1}-{2}-pi{3}", c.Product.Id, ((ILekmerCartItem)c).SizeId, ((ILekmerCartItem)c).IsAffectedByCampaign, ((ILekmerCartItem)c).PackageElements.GetPackageElementsHashCode()));
			var bundledCartItems = new Collection<ICartItem>();

			var usedIds = new Dictionary<int, int>();

			foreach (var groupedCartItem in groupedCartItems)
			{
				var bundledCartItem = groupedCartItem.First();

				bundledCartItem.Id = groupedCartItem.Select(c => c.Id).FirstOrDefault(id => id > 0 && !usedIds.ContainsKey(id));
				bundledCartItem.Quantity = groupedCartItem.Sum(c => c.Quantity);

				if (bundledCartItem.Id > 0)
				{
					usedIds[bundledCartItem.Id] = bundledCartItem.Id;
				}

				bundledCartItems.Add(bundledCartItem);
			}

			return bundledCartItems;
		}

		protected virtual Collection<ICartItem> GetGarbageCartItems(ICartFull cart, Collection<ICartItem> newCartItems)
		{
			var garbageCartItems = new Collection<ICartItem>();
			var usedIds = newCartItems.Select(c => c.Id).ToDictionary(id => id);

			// Find cart items that no longer used.
			var originalCartItems = cart.GetCartItems();
			foreach (ICartItem cartItem in originalCartItems)
			{
				if (cartItem.Id > 0 && !usedIds.ContainsKey(cartItem.Id))
				{
					garbageCartItems.Add(cartItem);
				}
			}

			// Find deleted cart items that still in use and exclude them from deleting.
			var deletedCartItems = cart.GetDeletedCartItems();
			foreach (ICartItem cartItem in deletedCartItems)
			{
				if (cartItem.Id > 0 && !usedIds.ContainsKey(cartItem.Id))
				{
					garbageCartItems.Add(cartItem);
				}
			}

			return garbageCartItems;
		}

		protected virtual void CheckItemsType(ICartFull cart)
		{
			if (cart == null)
			{
				throw new ArgumentNullException("cart");
			}

			var lekmerCart = (ILekmerCartFull)cart;
			lekmerCart.ItemsType = OrderItemsType.None;

			int diapersBrand = CartSetting.DiapersBrand;
			if (diapersBrand < 0)
			{
				return;
			}

			foreach (ILekmerCartItem cartItem in lekmerCart.GetCartItems())
			{
				var product = cartItem.Product as ILekmerProduct;
				if (product != null && product.BrandId.HasValue)
				{
					if (product.BrandId.Value == diapersBrand)
					{
						lekmerCart.ItemsType = lekmerCart.ItemsType | OrderItemsType.Diapers;
					}
					else
					{
						lekmerCart.ItemsType = lekmerCart.ItemsType | OrderItemsType.Regular;
					}
				}
			}
		}
	}
}