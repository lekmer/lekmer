﻿using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class ProfileCompanyInformation : IProfileCompanyInformation
	{
		public string Name { get; set; }
		public string FullName { get; set; }
		public string CellPhoneNumber { get; set; }
		public string Email { get; set; }
	}
}
