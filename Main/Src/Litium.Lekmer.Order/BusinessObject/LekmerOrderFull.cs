﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order.Campaign;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Product;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderFull : OrderFull, ILekmerOrderFull
	{
		private decimal _paymentCost;
		private string _customerIdentificationKey;
		private Guid? _feedbackToken;
		private int? _alternateAddressId;
		private ILekmerOrderAddress _alternateAddress;
		private string _civicNumber;
		private IOrderVoucherInfo _voucherInfo;
		private int? _optionalDeliveryMethodId;
		private decimal? _optionalFreightCost;
		private int? _diapersDeliveryMethodId;
		private decimal? _diapersFreightCost;
		private string _kcoId;
		private string _userAgent;


		public decimal PaymentCost
		{
			get { return _paymentCost; }
			set
			{
				CheckChanged(_paymentCost, value);
				_paymentCost = value;
			}
		}

		public string CustomerIdentificationKey
		{
			get { return _customerIdentificationKey; }
			set
			{
				CheckChanged(_customerIdentificationKey, value);
				_customerIdentificationKey = value;
			}
		}

		public Guid? FeedbackToken
		{
			get { return _feedbackToken; }
			set
			{
				CheckChanged(_feedbackToken, value);
				_feedbackToken = value;
			}
		}

		public int? AlternateAddressId
		{
			get { return _alternateAddressId; }
			set
			{
				CheckChanged(_alternateAddressId, value);
				_alternateAddressId = value;
			}
		}


		public IOrderVoucherInfo VoucherInfo
		{
			get { return _voucherInfo; }
			set
			{
				CheckChanged(_voucherInfo, value);
				_voucherInfo = value;
			}
		}

		public ILekmerOrderAddress AlternateAddress
		{
			get { return _alternateAddress; }
			set
			{
				CheckChanged(_alternateAddress, value);
				_alternateAddress = value;
			}
		}

		public string CivicNumber
		{
			get { return _civicNumber; }
			set
			{
				CheckChanged(_civicNumber, value);
				_civicNumber = value;
			}
		}

		public int? OptionalDeliveryMethodId
		{
			get { return _optionalDeliveryMethodId; }
			set
			{
				CheckChanged(_optionalDeliveryMethodId, value);
				_optionalDeliveryMethodId = value;
			}
		}

		public decimal? OptionalFreightCost
		{
			get { return _optionalFreightCost; }
			set
			{
				CheckChanged(_optionalFreightCost, value);
				_optionalFreightCost = value;
			}
		}

		public int? DiapersDeliveryMethodId
		{
			get { return _diapersDeliveryMethodId; }
			set
			{
				CheckChanged(_diapersDeliveryMethodId, value);
				_diapersDeliveryMethodId = value;
			}
		}

		public decimal? DiapersFreightCost
		{
			get { return _diapersFreightCost; }
			set
			{
				CheckChanged(_diapersFreightCost, value);
				_diapersFreightCost = value;
			}
		}

		public string KcoId
		{
			get { return _kcoId; }
			set
			{
				CheckChanged(_kcoId, value);
				_kcoId = value;
			}
		}

		public string UserAgent
		{
			get { return _userAgent; }
			set
			{
				CheckChanged(_userAgent, value);
				_userAgent = value;
			}
		}


		/// <summary>
		/// Gets the voucher discount from <see cref="ILekmerOrderCampaignInfo"/> or from <see cref="ILekmerOrder"/>.
		/// </summary>
		public virtual decimal? GetActualVoucherDiscount()
		{
			if (VoucherInfo != null && VoucherInfo.AmountUsed.HasValue)
			{
				return VoucherInfo.AmountUsed.Value;
			}

			return null;
		}

		/// <summary>
		/// Gets the payment cost VAT based on average over order items VAT rate.
		/// </summary>
		public virtual decimal GetPaymentCostVat()
		{
			decimal paymentCostInclVat = PaymentCost; // 49m
			decimal averageVatPercentage = GetOrderItemsAverageVat(); // 20.47m

			decimal paymentCostExclVat = PriceCalc.PriceExcludingVat1(paymentCostInclVat, averageVatPercentage / 100m);

			return paymentCostInclVat - paymentCostExclVat;
		}

		/// <summary>
		/// Gets the actual price summary.
		/// Includes: SUM(item.ActualPrice * item.Quantity)
		/// </summary>
		public virtual Price GetBaseActualPriceSummary()
		{
			return base.GetActualPriceSummary();
		}

		/// <summary>
		/// Gets the actual price summary campaign and voucher discounts.
		/// Includes: SUM(item.ActualPrice * item.Quantity) - cart.Discount - voucher.Discount
		/// </summary>
		public override Price GetActualPriceSummary()
		{
			var priceSummary = base.GetActualPriceSummary();

			if (CampaignInfo != null && CampaignInfo.Campaigns != null)
			{
				priceSummary = ApplyDiscount(priceSummary, CampaignInfo);
			}
			priceSummary = ApplyVoucherDiscount(priceSummary);

			return priceSummary;
		}

		/// <summary>
		/// Gets the total price including order items price summary, freight cost and payment cost.
		/// </summary>
		public override Price GetPriceTotal()
		{
			var priceIncludingVat = GetActualPriceSummary().IncludingVat + GetTotalFreightCost() + PaymentCost;
			var priceExcludingVat = priceIncludingVat - GetVatTotal();

			return new Price(priceIncludingVat, priceExcludingVat);
		}

		/// <summary>
		/// Gets the total price including order items price summary, freight cost and payment cost (taking into account campaigns).
		/// </summary>
		public override Price GetActualPriceTotal()
		{
			var priceIncludingVat = GetActualPriceSummary().IncludingVat + GetTotalFreightCost() + PaymentCost;
			var priceExcludingVat = priceIncludingVat - GetActualVatTotal();

			return new Price(priceIncludingVat, priceExcludingVat);
		}

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT.
		/// </summary>
		public override decimal GetVatTotal()
		{
			return GetActualVatSummary() + GetTotalFreightCostVat() + GetPaymentCostVat();
		}

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT (taking into account campaigns).
		/// </summary>
		public override decimal GetActualVatTotal()
		{
			return GetActualVatSummary() + GetTotalFreightCostVat() + GetPaymentCostVat();
		}

		/// <summary>
		/// Gets actualthe freight cost for the order.
		/// </summary>
		public override decimal GetActualFreightCost()
		{
			return FreightCost;
		}

		/// <summary>
		/// Gets the optional freight cost for the order.
		/// </summary>
		public virtual decimal GetOptionalFreightCost()
		{
			return OptionalFreightCost ?? 0m;
		}

		/// <summary>
		/// Gets the optional freight cost VAT for the order.
		/// </summary>
		public virtual decimal GetOptionalFreightCostVat()
		{
			decimal optionalFreightCostInclVat = GetOptionalFreightCost(); // 49m
			decimal averageVatPercentage = GetOrderItemsAverageVat(); // 20.47m

			decimal optionalFreightCostExclVat = PriceCalc.PriceExcludingVat1(optionalFreightCostInclVat, averageVatPercentage / 100m);

			return optionalFreightCostInclVat - optionalFreightCostExclVat;
		}

		/// <summary>
		/// Gets the diapers freight cost for the order.
		/// </summary>
		public virtual decimal GetDiapersFreightCost()
		{
			return DiapersFreightCost ?? 0m;
		}

		/// <summary>
		/// Gets the diapers freight cost VAT for the order.
		/// </summary>
		public virtual decimal GetDiapersFreightCostVat()
		{
			decimal diapersFreightCostInclVat = GetDiapersFreightCost(); // 49m
			decimal averageVatPercentage = GetOrderItemsAverageVat(); // 20.47m

			decimal diapersFreightCostExclVat = PriceCalc.PriceExcludingVat1(diapersFreightCostInclVat, averageVatPercentage / 100m);

			return diapersFreightCostInclVat - diapersFreightCostExclVat;
		}

		/// <summary>
		/// Gets the total freight cost for the order.
		/// </summary>
		public virtual decimal GetTotalFreightCost()
		{
			return 
				GetActualFreightCost() +
				GetOptionalFreightCost() +
				GetDiapersFreightCost();
		}

		/// <summary>
		/// Gets the total freight cost VAT for the order.
		/// </summary>
		public virtual decimal GetTotalFreightCostVat()
		{
			return
				GetActualFreightCostVat() +
				GetOptionalFreightCostVat() +
				GetDiapersFreightCostVat();
		}

		/// <summary>
		/// Gets the original price summary before any campaign and voucher discounts.
		/// </summary>
		/// <returns></returns>
		public virtual Price GetOriginalPriceSummary()
		{
			Collection<IOrderItem> orderItems = GetOrderItems();
			return new Price(
				orderItems.Sum(item => item.OriginalPrice.IncludingVat * (decimal)item.Quantity),
				orderItems.Sum(item => item.OriginalPrice.ExcludingVat * (decimal)item.Quantity));
		}

		/// <summary>
		/// Gets the dicount price summary (campaign and voucher discounts).
		/// </summary>
		/// <returns></returns>
		public virtual Price GetDiscountPriceSummary()
		{
			Price originalPriceSummary = GetOriginalPriceSummary();
			Price actualPriceSummary = GetActualPriceSummary();

			return new Price(
				originalPriceSummary.IncludingVat - actualPriceSummary.IncludingVat,
				originalPriceSummary.ExcludingVat - actualPriceSummary.ExcludingVat);
		}

		public virtual void MergeWith(IDeliveryMethod deliveryMethod, IDeliveryMethod optionalDeliveryMethod, IDeliveryMethod diapersDeliveryMethod)
		{
			MergeWith(deliveryMethod);

			if (optionalDeliveryMethod != null)
			{
				OptionalDeliveryMethodId = optionalDeliveryMethod.Id;
				OptionalFreightCost = optionalDeliveryMethod.FreightCost;
			}
			else
			{
				OptionalDeliveryMethodId = null;
				OptionalFreightCost = null;
			}

			if (diapersDeliveryMethod != null)
			{
				DiapersDeliveryMethodId = diapersDeliveryMethod.Id;
				DiapersFreightCost = diapersDeliveryMethod.FreightCost;
			}
			else
			{
				DiapersDeliveryMethodId = null;
				DiapersFreightCost = null;
			}
		}

		public virtual void MergeWith(IUserContext context, Collection<IOrderPayment> payments)
		{
			var paymentTypes = IoC.Resolve<IPaymentTypeService>().GetAll(context);
			foreach (IOrderPayment payment in payments)
			{
				Payments.Add(payment);

				var paymentTypeId = payment.PaymentTypeId;
				var paymentType = (ILekmerPaymentType)paymentTypes.First(t => t.Id == paymentTypeId);
				PaymentCost += paymentType.Cost;
			}
		}

		public virtual void MergeWith(ICartVoucherInfo cartVoucherInfo)
		{
			VoucherInfo = null;

			if (cartVoucherInfo.DiscountValue.HasValue)
			{
				VoucherInfo = IoC.Resolve<IOrderVoucherInfo>();
				VoucherInfo.SetNew();
				VoucherInfo.VoucherCode = cartVoucherInfo.VoucherCode;

				VoucherInfo.DiscountType = (int)cartVoucherInfo.DiscountType;
				VoucherInfo.DiscountValue = cartVoucherInfo.DiscountValue;

				if (cartVoucherInfo.DiscountType == VoucherValueType.Price)
				{
					VoucherInfo.AmountUsed = cartVoucherInfo.AmountUsed;
					VoucherInfo.AmountLeft = cartVoucherInfo.AmountLeft;
				}
				else if (cartVoucherInfo.DiscountType == VoucherValueType.GiftCard)
				{
					var priceSummary = GetActualPriceSummary();

					decimal amountLeftToPay = priceSummary.IncludingVat - cartVoucherInfo.DiscountValue.Value;
					if (amountLeftToPay < 0)
					{
						VoucherInfo.AmountUsed = priceSummary.IncludingVat;
						VoucherInfo.AmountLeft = cartVoucherInfo.DiscountValue.Value - priceSummary.IncludingVat;
					}
					else
					{
						VoucherInfo.AmountUsed = cartVoucherInfo.DiscountValue;
						VoucherInfo.AmountLeft = 0m;
					}
				}
				else if (cartVoucherInfo.DiscountType == VoucherValueType.Percentage)
				{
					var priceSummary = GetActualPriceSummary();
					decimal priceIncludingVat = priceSummary.IncludingVat;

					decimal amountUsed = priceIncludingVat * cartVoucherInfo.DiscountValue.Value / 100;
					VoucherInfo.AmountUsed = amountUsed.Round();
					VoucherInfo.AmountLeft = null;
				}
				else if (cartVoucherInfo.DiscountType == VoucherValueType.Code)
				{
					VoucherInfo.AmountUsed = null;
					VoucherInfo.AmountLeft = null;
				}
				else
				{
					throw new NotSupportedException("VoucherValueType '" + cartVoucherInfo.DiscountType + "' not supported.");
				}
			}
		}

		public virtual void MergeWithVoucherCode(IUserContext context)
		{
			if (VoucherInfo != null && VoucherInfo.VoucherCode.HasValue())
			{
				// Voucher code already in order.
				return;
			}

			var lekmerUserContext = (ILekmerUserContext)context;

			if (lekmerUserContext.Voucher == null || lekmerUserContext.Voucher.IsValid == false || lekmerUserContext.Voucher.VoucherCode.IsEmpty())
			{
				// No valid voucher in the system
				return;
			}

			bool vocherConditionFullFilled = false;

			// Find cart campaigns with voucher condition
			var orderCartCampaignIds = new Dictionary<int, int>();

			if (CampaignInfo != null && CampaignInfo.Campaigns != null)
			{
				foreach (IOrderCampaign orderCampaign in CampaignInfo.Campaigns)
				{
					orderCartCampaignIds[orderCampaign.CampaignId] = orderCampaign.CampaignId;
				}
			}

			if (orderCartCampaignIds.Count > 0)
			{
				var cartCampaignService = IoC.Resolve<ICartCampaignService>();

				// Now we have list of all cart campaigns, applied to order.
				foreach (KeyValuePair<int, int> orderCampaignId in orderCartCampaignIds)
				{
					ICartCampaign cartCampaign = cartCampaignService.GetById(context, orderCampaignId.Value);
					if (cartCampaign.Conditions.Any(c => c.ConditionType.CommonName.Equals("Voucher")))
					{
						vocherConditionFullFilled = true;
						break;
					}
				}
			}

			if (vocherConditionFullFilled == false)
			{
				// Find product campaigns with voucher condition
				var orderProductCampaignIds = new Dictionary<int, int>();

				Collection<IOrderItem> orderItems = GetOrderItems();
				if (orderItems != null)
				{
					foreach (IOrderItem orderItem in orderItems)
					{
						if (orderItem.CampaignInfo != null && orderItem.CampaignInfo.Campaigns != null)
						{
							foreach (IOrderCampaign orderCampaign in orderItem.CampaignInfo.Campaigns)
							{
								orderProductCampaignIds[orderCampaign.CampaignId] = orderCampaign.CampaignId;
							}
						}
					}
				}

				var productCampaignService = IoC.Resolve<IProductCampaignService>();

				// Now we have list of all cart campaigns, applied to order.
				foreach (KeyValuePair<int, int> orderProductCampaignId in orderProductCampaignIds)
				{
					IProductCampaign productCampaign = productCampaignService.GetById(context, orderProductCampaignId.Value);
					if (productCampaign.Conditions.Any(c => c.ConditionType.CommonName.Equals("Voucher")))
					{
						vocherConditionFullFilled = true;
						break;
					}
				}
			}

			// Now we know if voucher condition was fullfilled in order.
			if (vocherConditionFullFilled)
			{
				if (VoucherInfo == null)
				{
					VoucherInfo = IoC.Resolve<IOrderVoucherInfo>();
					VoucherInfo.SetNew();
				}

				VoucherInfo.VoucherCode = lekmerUserContext.Voucher.VoucherCode;
				VoucherInfo.DiscountType = (int)lekmerUserContext.Voucher.ValueType;
				VoucherInfo.DiscountValue = (int)lekmerUserContext.Voucher.DiscountValue;
			}
		}

		public override IOrderItem CreateOrderItem()
		{
			var orderItem = base.CreateOrderItem();

			var lekmerOrderItem = (ILekmerOrderItem)orderItem;
			lekmerOrderItem.OrderItemSize = IoC.Resolve<IOrderItemSize>();
			lekmerOrderItem.OrderItemSize.OrderItemId = lekmerOrderItem.Id;

			return lekmerOrderItem;
		}

		public override void MergeWith(ICustomer customer)
		{
			if (customer.CustomerInformation == null)
			{
				throw new ArgumentException("Argument is not valid: customer. No customer information.");
			}

			if (customer.CustomerInformation.DefaultBillingAddress == null)
			{
				throw new ArgumentException("Argument is not valid: DefaultBillingAddress. No default billing address.");
			}

			IOrderAddress deliveryAddress = null;
			IOrderAddress billingAddress = null;
			ILekmerOrderAddress alternateAddress = null;

			Customer = customer;

			if (customer.CustomerInformation.DefaultBillingAddress != null)
			{
				billingAddress = Convert(customer.CustomerInformation.DefaultBillingAddress);
			}

			if (customer.CustomerInformation.DefaultDeliveryAddress != null &&
				customer.CustomerInformation.DefaultDeliveryAddress != customer.CustomerInformation.DefaultBillingAddress)
			{
				deliveryAddress = Convert(customer.CustomerInformation.DefaultDeliveryAddress);
			}

			var information = (ILekmerCustomerInformation)customer.CustomerInformation;
			if (information.AlternateAddress != null)
			{
				alternateAddress = (ILekmerOrderAddress)Convert(information.AlternateAddress);
			}
			MergeWith(alternateAddress);

			MergeWith(billingAddress, deliveryAddress);
			Email = customer.CustomerInformation.Email;
		}

		public override void MergeWith(IUserContext context, ICartFull cart)
		{
			var lekmerCartFull = (ILekmerCartFull) cart;

			var cartItems = lekmerCartFull.GetGroupedCartItemsByProductAndPriceAndSize()
				.Cast<ICartItem>()
				.ToList();
			MergeWith(context, new Collection<ICartItem>(cartItems));
			MergeWith(context, cart.CampaignInfo);
			MergeWith(lekmerCartFull.VoucherInfo);

			// Be sure order campaign info and order items are merged already
			MergeWithVoucherCode(context);
		}

		public virtual void MergeWith(ILekmerOrderAddress orderAlternateAddress)
		{
			_alternateAddress = orderAlternateAddress;
		}

		/// <summary>
		/// Calculate total discount from the order.
		/// </summary>
		public virtual void GetTotalDiscount(IChannel channel, IOrderCampaignInfo orderCampaignInfo, out bool isFixedDiscount, out decimal totalFixedDiscount, out bool isPercentageDiscount, out decimal totalPercentageDiscount)
		{
			isFixedDiscount = false;
			totalFixedDiscount = 0;

			var fixedDiscountActions = new Collection<ICartAction>();
			var percentageDiscountActions = new Collection<ICartAction>();

			GetCampaignActions(orderCampaignInfo, fixedDiscountActions, percentageDiscountActions);

			// fixed.
			CalculateTotalFixedDiscount(channel, fixedDiscountActions, ref isFixedDiscount, ref totalFixedDiscount);

			// percentage.
			CalculateTotalPercentageDiscount(orderCampaignInfo, percentageDiscountActions, isFixedDiscount, totalFixedDiscount, out isPercentageDiscount, out totalPercentageDiscount);
		}


		protected override IOrderAddress Convert(IAddress address)
		{
			var orderAddress = base.Convert(address);

			var lekmerAddress = address as ILekmerAddress;
			var lekmerOrderAddress = orderAddress as ILekmerOrderAddress;

			if (lekmerOrderAddress != null && lekmerAddress != null)
			{
				lekmerOrderAddress.HouseNumber = lekmerAddress.HouseNumber;
				lekmerOrderAddress.HouseExtension = lekmerAddress.HouseExtension;
				lekmerOrderAddress.Reference = lekmerAddress.Reference;
				lekmerOrderAddress.DoorCode = lekmerAddress.DoorCode;
			}

			return orderAddress;
		}

		private Price ApplyDiscount(Price priceSummary, IOrderCampaignInfo campaignInfo)
		{
			ICurrency currency = IoC.Resolve<IChannelSharedService>().GetById(ChannelId).Currency;

			var cartCampaignSharedService = IoC.Resolve<ICartCampaignSharedService>();
			foreach (var orderCampaign in campaignInfo.Campaigns)
			{
				var campaign = cartCampaignSharedService.GetById(orderCampaign.CampaignId);
				if (campaign != null && campaign.Actions != null)
				{
					foreach (var campaignAction in campaign.Actions)
					{
						if (campaignAction.ActionType.CommonName == "PercentageDiscount"
							|| campaignAction.ActionType.CommonName == "FixedCartDiscount")
						{
							priceSummary = ((ILekmerCartAction)campaignAction).ApplyDiscount(priceSummary, currency);
						}
					}
				}
			}

			return priceSummary;
		}

		private Price ApplyVoucherDiscount(Price priceSummary)
		{
			decimal? voucherDiscount = GetActualVoucherDiscount();
			if (!voucherDiscount.HasValue || voucherDiscount == 0)
			{
				return priceSummary;
			}

			decimal priceIncludingVat = priceSummary.IncludingVat;
			priceIncludingVat -= voucherDiscount.Value;
			if (priceIncludingVat < 0)
			{
				return new Price(0, 0);
			}

			decimal vatPercentage = PriceCalc.VatPercentage2(priceSummary.IncludingVat, priceSummary.ExcludingVat);
			var priceExcludingVat = PriceCalc.PriceExcludingVat1(priceIncludingVat, vatPercentage);

			return new Price(priceIncludingVat, priceExcludingVat);
		}

		private void GetCampaignActions(IOrderCampaignInfo orderCampaignInfo, Collection<ICartAction> fixedDiscountActions, Collection<ICartAction> percentageDiscountActions)
		{
			var cartCampaignSharedService = IoC.Resolve<ICartCampaignSharedService>();
			foreach (var orderCampaign in orderCampaignInfo.Campaigns)
			{
				var campaign = cartCampaignSharedService.GetById(orderCampaign.CampaignId);
				if (campaign != null && campaign.Actions != null)
				{
					foreach (var action in campaign.Actions)
					{
						if (action.ActionType.CommonName == "FixedCartDiscount")
						{
							fixedDiscountActions.Add(action);
						}
						else if (action.ActionType.CommonName == "PercentageDiscount")
						{
							percentageDiscountActions.Add(action);
						}
					}
				}
			}
		}

		private void CalculateTotalFixedDiscount(IChannel channel, IEnumerable<ICartAction> fixedDiscountActions, ref bool isFixedDiscount, ref decimal totalFixedDiscount)
		{
			foreach (var action in fixedDiscountActions)
			{
				decimal discountValue;
				LekmerCurrencyValueDictionary amounts = ((IFixedDiscountCartAction)action).Amounts;
				if (amounts != null && amounts.TryGetValue(channel.Currency.Id, out discountValue))
				{
					isFixedDiscount = true;
					totalFixedDiscount = totalFixedDiscount + discountValue;
				}
			}
		}

		private void CalculateTotalPercentageDiscount(IOrderCampaignInfo orderCampaignInfo, Collection<ICartAction> percentageDiscountActions, bool isFixedDiscount, decimal totalFixedDiscount, out bool isPercentageDiscount, out decimal totalPercentageDiscount)
		{
			string percentageDiscount = string.Empty;
			isPercentageDiscount = percentageDiscountActions.Count > 0;
			totalPercentageDiscount = 0m;

			foreach (var action in percentageDiscountActions)
			{
				percentageDiscount = percentageDiscount + ((IPercentageDiscountCartAction) action).DiscountAmount + ';';
			}

			if (!string.IsNullOrEmpty(percentageDiscount))
			{
				percentageDiscount = percentageDiscount.Substring(0, percentageDiscount.Length - 1);
				string[] splittedPercentageDiscount = percentageDiscount.Split(';');

				if (splittedPercentageDiscount.Length == 1)
				{
					totalPercentageDiscount = System.Convert.ToDecimal(splittedPercentageDiscount[0], CultureInfo.CurrentCulture);
				}
				else
				{
					Price originalPrice = base.GetActualPriceSummary();
					Price priceWithFixedDiscount;

					if (isFixedDiscount)
					{
						// todo Round is missed

						decimal vatPercentage = originalPrice.IncludingVat/originalPrice.ExcludingVat - 1;

						var priceIncludingVat = originalPrice.IncludingVat - totalFixedDiscount;
						var priceExcludingVat = priceIncludingVat/(1 + vatPercentage/100);

						priceWithFixedDiscount = new Price(priceIncludingVat, priceExcludingVat);
					}
					else
					{
						priceWithFixedDiscount = originalPrice;
					}

					var actualPrice = ApplyDiscount(originalPrice, orderCampaignInfo);

					totalPercentageDiscount = CalculateDiscountPercent(priceWithFixedDiscount.IncludingVat, actualPrice.IncludingVat);
				}
			}
		}

		private static decimal CalculateDiscountPercent(decimal originalPrice, decimal campaignPrice)
		{
			var discountInPercent = (originalPrice - campaignPrice) * 100 / originalPrice;
			return Math.Round(discountInPercent, 0, MidpointRounding.AwayFromZero);
		}


		public bool IsProductAbove60LExist(IUserContext context, IEnumerable<IOrderItem> orderItems)
		{
			bool isProductAbove60L = false;
			bool isProductUnder60L = false;

			var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
			foreach (var item in orderItems)
			{
				if (isProductAbove60L && isProductUnder60L)
				{
					break;
				}

				var product = (ILekmerProduct) productService.GetById(context, item.ProductId);
				CheckWarehouse(context, productService, product, ref isProductAbove60L, ref isProductUnder60L);
			}

			return isProductAbove60L && isProductUnder60L;
		}

		public bool CheckForMixedItems(IUserContext context, IEnumerable<IOrderItem> orderItems)
		{
			bool regularItems = false;
			bool diapers = false;

			int diapersBrand = IoC.Resolve<ILekmerCartSetting>().DiapersBrand;

			var productService = (ILekmerProductService)IoC.Resolve<IProductService>();
			foreach (var item in orderItems)
			{
				if (regularItems && diapers)
				{
					break;
				}

				var product = (ILekmerProduct)productService.GetById(context, item.ProductId);
				if (product != null && product.BrandId.HasValue)
				{
					if (product.BrandId.Value == diapersBrand)
					{
						diapers = true;
					}
					else
					{
						regularItems = true;
					}
				}
			}

			return regularItems && diapers;
		}

		protected virtual void CheckWarehouse(IUserContext context, ILekmerProductService productService, IProduct product, ref bool isProductAbove60L, ref bool isProductUnder60L)
		{
			if (product.IsPackage())
			{
				var productIds = productService.GetIdAllByPackageMasterProduct(context, product.Id);
				var packageProducts = productService.PopulateViewWithOnlyInPackageProducts(context, productIds);
				foreach (var packageProduct in packageProducts)
				{
					if (isProductAbove60L && isProductUnder60L) break;
					IsProductAbove60(packageProduct, ref isProductAbove60L, ref isProductUnder60L);
				}
			}
			else
			{
				IsProductAbove60(product, ref isProductAbove60L, ref isProductUnder60L);
			}
		}

		protected virtual void IsProductAbove60(IProduct product, ref bool isProductAbove60L, ref bool isProductUnder60L)
		{
			if (((ILekmerProduct)product).IsAbove60L)
			{
				isProductAbove60L = true;
			}
			else
			{
				isProductUnder60L = true;
			}
		}

		public bool IsDropShipItemExist(IEnumerable<IOrderItem> orderItems)
		{
			bool hasDropShipItems = false;

			foreach (var orderItem in orderItems)
			{
				if (hasDropShipItems) break;
				hasDropShipItems = ((ILekmerOrderItem)orderItem).IsDropShip;
			}

			return hasDropShipItems;
		}

		public bool CheckForTaxFreeZone()
		{
			bool taxFreeZone = false;

			if (DeliveryAddress != null)
			{
				bool countryFinland = DeliveryAddress.CountryId == 1000003; // Finland
				bool zipAland = DeliveryAddress.PostalCode.Trim().StartsWith("22"); // Åland

				taxFreeZone = countryFinland && zipAland;
			}

			if (taxFreeZone == false && DeliveryAddress != null)
			{
				bool countryNorway = DeliveryAddress.CountryId == 1000001; // Norway
				bool zipSvalbard = DeliveryAddress.PostalCode.Trim().StartsWith("917"); // Svalbard

				taxFreeZone = countryNorway && zipSvalbard;
			}

			return taxFreeZone;
		}


		/// <summary>
		/// Gets the freight cost VAT (taking into account campaigns).
		/// </summary>
		public override decimal GetActualFreightCostVat()
		{
			decimal actualFreightCostInclVat = GetActualFreightCost(); // 49m
			decimal averageVatPercentage = GetOrderItemsAverageVat(); // 20.47m

			decimal actualFreightCostExclVat = PriceCalc.PriceExcludingVat1(actualFreightCostInclVat, averageVatPercentage / 100m);

			return actualFreightCostInclVat - actualFreightCostExclVat;
		}

		/// <summary>
		/// Gets the freight cost VAT.
		/// </summary>
		public new virtual Decimal GetFreightCostVat()
		{
			decimal freightCostInclVat = FreightCost; // 49m
			decimal averageVatPercentage = GetOrderItemsAverageVat(); // 20.47m

			decimal freightCostExclVat = PriceCalc.PriceExcludingVat1(freightCostInclVat, averageVatPercentage / 100m);

			return freightCostInclVat - freightCostExclVat;
		}

		/// <summary>
		/// Average VAT % (over order items).
		/// </summary>
		/// <returns>Example: 20.47m</returns>
		public virtual decimal GetOrderItemsAverageVatPercentage()
		{
			return GetOrderItemsAverageVat();
		}

		/// <summary>
		/// Average VAT % (over order items).
		/// </summary>
		/// <returns>Example: 20.47m</returns>
		protected override decimal GetOrderItemsAverageVat()
		{
			Collection<IOrderItem> orderItems = GetOrderItems();

			if (orderItems.Count == 0)
			{
				return 0m;
			}

			// ActualPrice is used. OriginalPrice can't be used, it could have tottaly wrong ExcludingVat - it is not stored with order, but calculated from ActualPrice
			decimal sumIncludingVat = orderItems.Sum(item => item.ActualPrice.IncludingVat * (decimal)item.Quantity);
			decimal sumExcludingVat = orderItems.Sum(item => item.ActualPrice.ExcludingVat * (decimal)item.Quantity);

			if (sumIncludingVat == 0 || sumExcludingVat == 0)
			{
				return 0m;
			}

			decimal vatPercentage = PriceCalc.VatPercentage100(sumIncludingVat, sumExcludingVat); // % example: 20.47m

			return vatPercentage;
		}
	}
}