﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class SubPaymentType : BusinessObjectBase, ISubPaymentType
	{
		private int _id;
		private int _paymentId;
		private string _commonName;
		private string _title;
		private string _code;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int PaymentId
		{
			get { return _paymentId; }
			set
			{
				CheckChanged(_paymentId, value);
				_paymentId = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Code
		{
			get { return _code; }
			set
			{
				CheckChanged(_code, value);
				_code = value;
			}
		}
	}
}