using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class OrderVoucherInfo : BusinessObjectBase, IOrderVoucherInfo
	{
		// public decimal? VoucherDiscount { get; set; }
		// public VoucherValueType VoucherDiscountType { get; set; }
		// public decimal? AmountUsed { get; set; }

		private string _voucherCode;
		private int? _discountType;
		private decimal? _discountValue;
		private decimal? _amountUsed;
		private decimal? _amountLeft;
		private int _voucherStatus;

		public string VoucherCode
		{
			get { return _voucherCode; }
			set
			{
				CheckChanged(_voucherCode, value);
				_voucherCode = value;
			}
		}

		public int? DiscountType
		{
			get { return _discountType; }
			set
			{
				CheckChanged(_discountType, value);
				_discountType = value;
			}
		}

		/// <summary>
		/// Original discount value that comes from Voucher.
		/// </summary>
		public decimal? DiscountValue
		{
			get { return _discountValue; }
			set
			{
				CheckChanged(_discountValue, value);
				_discountValue = value;
			}
		}

		/// <summary>
		/// Discount amount, which was applied in order.
		/// </summary>
		public decimal? AmountUsed
		{
			get { return _amountUsed; }
			set
			{
				CheckChanged(_amountUsed, value);
				_amountUsed = value;
			}
		}

		/// <summary>
		/// Discount amount, with is still available after voucher was used in order.
		/// </summary>
		public decimal? AmountLeft
		{
			get { return _amountLeft; }
			set
			{
				CheckChanged(_amountLeft, value);
				_amountLeft = value;
			}
		}

		/// <summary>
		/// Specifies if voucher was consumed.
		/// </summary>
		public int VoucherStatus
		{
			get { return _voucherStatus; }
			set
			{
				CheckChanged(_voucherStatus, value);
				_voucherStatus = value;
			}
		}
	}
}