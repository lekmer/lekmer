using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class PackageElement : IPackageElement
	{
		private int _productId;
		private int? _sizeId;

		public int? SizeId
		{
			get { return _sizeId; }
			set
			{
				_sizeId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				_productId = value;
			}
		}
	}
}