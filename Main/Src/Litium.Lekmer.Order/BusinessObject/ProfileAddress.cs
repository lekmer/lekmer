﻿using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class ProfileAddress : IProfileAddress
	{
		public string Addressee { get; set; }
		public string StreetAddress { get; set; }
		public string StreetAddress2 { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string PhoneNumber { get; set; }

		public string HouseNumber { get; set; }
		public string HouseExtension { get; set; }

		public string Reference { get; set; }

		public string DoorCode { get; set; }
	}
}
