﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerDeliveryMethod : DeliveryMethod, ILekmerDeliveryMethod
	{
		private bool _channelDefault;
		private int _priority;
		private bool _isCompany;
		private Collection<IDeliveryMethodWeightPrice> _weightPrices;
		private int _deliveryMethodTypeId;

		public bool ChannelDefault
		{
			get { return _channelDefault; }
			set
			{
				CheckChanged(_channelDefault, value);
				_channelDefault = value;
			}
		}

		public int Priority
		{
			get { return _priority; }
			set
			{
				CheckChanged(_priority, value);
				_priority = value;
			}
		}

		public bool IsCompany
		{
			get { return _isCompany; }
			set
			{
				CheckChanged(_isCompany, value);
				_isCompany = value;
			}
		}

		public Collection<IDeliveryMethodWeightPrice> WeightPrices
		{
			get { return _weightPrices; }
			set
			{
				CheckChanged(_weightPrices, value);
				_weightPrices = value;
			}
		}

		public bool FreightCostDependOnWeight
		{
			get
			{
				return WeightPrices != null && WeightPrices.Count > 0;
			}
		}

		public int DeliveryMethodTypeId
		{
			get { return _deliveryMethodTypeId; }
			set
			{
				CheckChanged(_deliveryMethodTypeId, value);
				_deliveryMethodTypeId = value;
			}
		}
	}
}