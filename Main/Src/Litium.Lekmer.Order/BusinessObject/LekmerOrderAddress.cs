using System;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderAddress : OrderAddress, ILekmerOrderAddress
	{
		private string _houseNumber;
		private string _houseExtension;
		private string _reference;
		private string _doorCode;

		public string HouseNumber
		{
			get { return _houseNumber; }
			set
			{
				CheckChanged(_houseNumber, value);
				_houseNumber = value;
			}
		}

		public string HouseExtension
		{
			get { return _houseExtension; }
			set
			{
				CheckChanged(_houseExtension, value);
				_houseExtension = value;
			}
		}

		public string Reference
		{
			get { return _reference; }
			set
			{
				CheckChanged(_reference, value);
				_reference = value;
			}
		}

		public string DoorCode
		{
			get { return _doorCode; }
			set
			{
				CheckChanged(_doorCode, value);
				_doorCode = value;
			}
		}
	}
}