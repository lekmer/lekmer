﻿using System;

namespace Litium.Lekmer.Order
{

    [Serializable]
    public class TradeDoubleProductGroup : ITradeDoubleProductGroup
    {
        public string ProductGroupId { get; set; }
    }
}
