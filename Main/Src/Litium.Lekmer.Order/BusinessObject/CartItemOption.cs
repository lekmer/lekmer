using System;
using Litium.Lekmer.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class CartItemOption : BusinessObjectBase, ICartItemOption
	{
		private int _id;
		private int _cartId;
		private IProduct _product;
		private int? _sizeId;
		private int _quantity;
		private DateTime _createdDate;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int CartId
		{
			get { return _cartId; }
			set
			{
				CheckChanged(_cartId, value);
				_cartId = value;
			}
		}

		public int? SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}

		public IProduct Product
		{
			get { return _product; }
			set
			{
				CheckChanged(_product, value);
				_product = value;
			}
		}

		public int Quantity
		{
			get { return _quantity; }
			set
			{
				CheckChanged(_quantity, value);
				_quantity = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public virtual Price GetActualPrice()
		{
			decimal priceIncludingVat;
			decimal priceExcludingVat;
			if (Product.CampaignInfo != null)
			{
				priceIncludingVat = Product.CampaignInfo.Price.IncludingVat;
				priceExcludingVat = Product.CampaignInfo.Price.ExcludingVat;
			}
			else
			{
				priceIncludingVat = Product.Price.PriceIncludingVat;
				priceExcludingVat = Product.Price.PriceExcludingVat;
			}
			var price = new Price(priceIncludingVat, priceExcludingVat);
			return price;
		}

		public virtual Price GetActualPriceSummary()
		{
			var price = GetActualPrice();
			var priceSummary = new Price(price.IncludingVat * Quantity, price.ExcludingVat * Quantity);
			return priceSummary;
		}
	}
}