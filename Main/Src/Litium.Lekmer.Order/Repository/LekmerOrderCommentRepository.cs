using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderCommentRepository : OrderCommentRepository
	{
		public virtual void DeleteByOrder(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerOrderCommentRepository.DeleteByOrder");
			new DataHandler().ExecuteCommand("[orderlek].[pOrderCommentDeleteByOrder]", parameters, dbSettings);
		}
	}
}