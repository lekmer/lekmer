﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	[Serializable]
	public class LekmerCartRepository : CartRepository
	{
		public virtual void Update(Guid cartGuid, int removedItemsCount)
		{
			IDataParameter[] parameters = {
				ParameterHelper.CreateParameter("@CartGuid", cartGuid, SqlDbType.UniqueIdentifier),
				ParameterHelper.CreateParameter("@UpdatedDate", DateTime.Now, SqlDbType.DateTime),
				ParameterHelper.CreateParameter("@RemovedItemsCount", removedItemsCount, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("LekmerCartRepository.Update");
			new DataHandler().ExecuteCommand("[lekmer].[pLekmerCartUpdate]", parameters, dbSettings);
		}

		public virtual void DeleteExpiredItems(int lifeCycleDays)
		{
			IDataParameter[] parameters = 
			{
				ParameterHelper.CreateParameter("@LifeCycleDays", lifeCycleDays, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerCartRepository.DeleteExpiredItems");

			new DataHandler().ExecuteCommand("[orderlek].[pCartDeleteExpiredItems]", parameters, dbSettings);
		}

		public virtual ICartFull GetByIdAndCartGuid(int id, Guid cartGuid)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartId", id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CartGuid", cartGuid, SqlDbType.UniqueIdentifier)
				};
			var dbSettings = new DatabaseSetting("LekmerCartRepository.GetByIdAndCartGuid");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pCartGetByIdAndCartGuid]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		/// <summary>
		/// Save just the cart object
		/// </summary>
		public override int Insert(ICartFull cart)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartGuid", cart.CartGuid, SqlDbType.UniqueIdentifier),
					ParameterHelper.CreateParameter("@CreatedDate", cart.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@IPAddress", ((ILekmerCartFull) cart).IPAddress, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("CartRepository.Save");
			cart.Id = new DataHandler().ExecuteCommandWithReturnValue("[order].[pCartSave]", parameters, dbSettings);
			return cart.Id;
		}
	}
}