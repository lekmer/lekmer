using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderRepository : OrderRepository
	{
		public virtual void UpdateNumberInStock(int productId, int quantity)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdateNumberInStock");

			new DataHandler().ExecuteCommand("lekmer.pProductDecreaseNumberInStock", parameters, dbSettings);
		}

		public virtual void UpdateNumberInStock(int productId, int sizeId, int quantity)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SizeId", sizeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdateNumberInStock");

			new DataHandler().ExecuteCommand("lekmer.pProductSizeDecreaseNumberInStock", parameters, dbSettings);
		}

		public virtual void UpdatePackageWithoutSizesNumberInStock(int packageMasterProductId, int quantity)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@PackageMasterProductId", packageMasterProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdatePackageWithoutSizesNumberInStock");

			new DataHandler().ExecuteCommand("productlek.pPackageWithoutSizesDecreaseNumberInStock", parameters, dbSettings);
		}

		public virtual void UpdatePackageWithSizesNumberInStock(int packageMasterProductId, int quantity, string itemsWithSizesXml, string itemsWithoutSizes, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@PackageMasterProductId", packageMasterProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ItemsWithoutSizes", itemsWithoutSizes, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ItemsWithSizes", itemsWithSizesXml, SqlDbType.Xml),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdatePackageWithSizesNumberInStock");

			new DataHandler().ExecuteCommand("productlek.pPackageWithSizesDecreaseNumberInStock", parameters, dbSettings);
		}

		public virtual void Save(ILekmerOrderFull order)
		{
			if (order == null)
			{
				throw new ArgumentNullException("order");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@OrderId", order.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerIdentificationKey", order.CustomerIdentificationKey, SqlDbType.NChar),
					ParameterHelper.CreateParameter("@PaymentCost", order.PaymentCost, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@AlternateAddressId", order.AlternateAddressId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CivicNumber", order.CivicNumber, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@OptionalDeliveryMethodId", order.OptionalDeliveryMethodId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@OptionalFreightCost", order.OptionalFreightCost, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@DiapersDeliveryMethodId", order.DiapersDeliveryMethodId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DiapersFreightCost", order.DiapersFreightCost, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@KcoId", order.KcoId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@UserAgent", order.UserAgent, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.Save");

			new DataHandler().ExecuteCommand("lekmer.pOrderSave", parameters, dbSettings);
		}

		public virtual Collection<IOrder> Search(ILekmerOrderSearchCriteria criteria, int page, int pageSize, out int items)
		{
			if (criteria == null)
			{
				throw new ArgumentNullException("criteria");
			}

			Collection<IOrder> collection = new Collection<IOrder>();

			IDataParameter[] parameters = new[]
				{
					ParameterHelper.CreateParameter("@OrderId", criteria.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.Search");

			items = 0;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pOrderSearch]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					items = dataReader.GetInt32(0);
				}

				if (dataReader.NextResult())
				{
					collection = CreateDataMapper<IOrder>(dataReader).ReadMultipleRows();
				}

				return collection;
			}
		}

		public virtual void SetFeedbackToken(int orderId, Guid feedbackToken)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FeedbackToken", feedbackToken, SqlDbType.UniqueIdentifier)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.SetFeedbackToken");

			new DataHandler().ExecuteCommand("[lekmer].[pLekmerOrderSetFeedbackToken]", parameters, dbSettings);
		}

		public virtual IOrderFull GetByFeedbackToken(Guid feedbackToken)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("FeedbackToken", feedbackToken, SqlDbType.UniqueIdentifier)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetByFeedbackToken");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[lekmer].[pLekmerOrderGetByFeedbackToken]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadRow();
			}
		}

		public virtual Collection<IOrderFull> GetLatest(DateTime date)
		{
			IDataParameter[] parameters = new[]
				{
					ParameterHelper.CreateParameter("Date", date, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetLatest");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[lekmer].[pLekmerOrderGetLatest]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual Collection<IOrderFull> GetToSendInsuranceInfo(int channelId, int maxDaysAfterPurchase, int minDaysAfterPurchase)
		{
			IDataParameter[] parameters = new[]
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("MaxDaysAfterPurchase", maxDaysAfterPurchase, SqlDbType.Int),
					ParameterHelper.CreateParameter("MinDaysAfterPurchase", minDaysAfterPurchase, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetToSendInsuranceInfo");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[lekmer].[pLekmerOrderGetToSendInsuranceInfo]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual Collection<IOrderFull> GetToSendGfkStatistics(int channelId, string startDate, string endDate)
		{
			IDataParameter[] parameters = new[]
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StartDate", startDate, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("EndDate", endDate, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetToSendGfkStatistics");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[orderlek].[pOrderGetToSendGfkStatistics]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual Collection<IOrderFull> GetToSendPacsoftInfo(int channelId)
		{
			var parameters = new[]
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetToSendPacsoftInfo");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[orderlek].[pLekmerOrderGetToSendPacsoftInfo]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual void UpdateSendInsuranceInfoFlag(string idList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdateSendInsuranceInfoFlag");

			new DataHandler().ExecuteCommand("[lekmer].[pLekmerOrderUpdateSendInsuranceInfoFlag]", parameters, dbSettings);
		}

		public virtual Collection<IOrderFull> GetOrders(DateTime date, int minutesAfterPurchase)
		{
			IDataParameter[] parameters = new[]
				{
					ParameterHelper.CreateParameter("Date", date, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("MinAfterPurchase", minutesAfterPurchase, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetOrders");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[lekmer].[pLekmerOrderMonitor]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual void UpdateOrderStatus(int orderId, int orderStatusId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderStatusId", orderStatusId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdateOrderStatus");

			new DataHandler().ExecuteCommand("[orderlek].[pOrderStatusUpdate]", parameters, dbSettings);
		}

		public virtual void Delete(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerOrderRepository.Delete");
			new DataHandler().ExecuteCommand("[orderlek].[pOrderDelete]", parameters, dbSettings);
		}

		public virtual Collection<IOrderFull> GetOrdersByEmailAndDate(string email, DateTime date)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Email", email, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Date", date, SqlDbType.DateTime)
				};
			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetOrdersByEmailAndDate");
			using (IDataReader reader = new DataHandler().ExecuteSelect("[orderlek].[pLekmerOrderGetByEmailAndDate]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual Collection<IOrderFull> GetOrdersByEmail(string email)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("Email", email, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetOrdersByEmail");
			using (IDataReader reader = new DataHandler().ExecuteSelect("[orderlek].[pOrderGetAllByEmail]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}

		public virtual IOrderFull GetByKcoId(string kcoId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@KcoId", kcoId, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetByKcoId");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[orderlek].[pLekmerOrderGetByKcoId]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadRow();
			}
		}

		public virtual Collection<IOrderFull> GetKlarnaCheckoutOrders(int days, string orderStatus)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@Days", days, SqlDbType.Int),
					ParameterHelper.CreateParameter("@OrderStatus", orderStatus, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("LekmerOrderRepository.GetKlarnaCheckoutOrders");

			using (IDataReader reader = new DataHandler().ExecuteSelect("[orderlek].[pGetKlarnaCheckoutOrders]", parameters, dbSettings))
			{
				return CreateDataMapper<IOrderFull>(reader).ReadMultipleRows();
			}
		}
	}
}