using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderPaymentRepository : OrderPaymentRepository
	{
		public override int Save(IOrderPayment payment)
		{
			var lekmerOrderPayment = (ILekmerOrderPayment)payment;

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@OrderPaymentId", payment.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@OrderId", payment.OrderId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@PaymentTypeId", payment.PaymentTypeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Price", payment.Price, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@Vat", payment.Vat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@ReferenceId", payment.ReferenceId, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@Captured", lekmerOrderPayment.Captured, SqlDbType.Bit),
				ParameterHelper.CreateParameter("@KlarnaEID", lekmerOrderPayment.KlarnaEid, SqlDbType.Int),
				ParameterHelper.CreateParameter("@KlarnaPClass", lekmerOrderPayment.KlarnaPClass, SqlDbType.Int),
				ParameterHelper.CreateParameter("@MaksuturvaCode", lekmerOrderPayment.MaksuturvaCode, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@QliroClientRef", lekmerOrderPayment.QliroClientRef, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@QliroPaymentCode", lekmerOrderPayment.QliroPaymentCode, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@CollectorStoreId", lekmerOrderPayment.CollectorStoreId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CollectorPaymentCode", lekmerOrderPayment.CollectorPaymentCode, SqlDbType.VarChar)
			};

			var dbSettings = new DatabaseSetting("LekmerOrderPaymentRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[order].[pOrderPaymentSave]", parameters, dbSettings);
		}

		public virtual void DeleteByOrder(int orderId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerOrderPaymentRepository.DeleteByOrder");

			new DataHandler().ExecuteCommand("[orderlek].[pOrderPaymentDeleteByOrder]", parameters, dbSettings);
		}
	}
}