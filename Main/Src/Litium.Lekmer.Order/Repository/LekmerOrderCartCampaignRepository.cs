using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository.Campaign;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderCartCampaignRepository : OrderCartCampaignRepository
	{
		public virtual void DeleteByOrder(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerOrderCartCampaignRepository.DeleteByOrder");
			new DataHandler().ExecuteCommand("[orderlek].[pOrderCartCampaignDeleteByOrder]", parameters, dbSettings);
		}
	}
}