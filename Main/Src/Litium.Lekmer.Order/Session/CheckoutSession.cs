using System;
using Litium.Lekmer.Common;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public class CheckoutSession : ICheckoutSession
	{
		protected ISessionStateService Session { get; private set; }
		protected ILekmerCartSetting CartSetting { get; private set; }

		public CheckoutSession(ISessionStateService session, ILekmerCartSetting cartSetting)
		{
			Session = session;
			CartSetting = cartSetting;
		}

		public ICheckoutProfile Profile
		{
			get
			{
				if (Session.Available)
				{
					return Session["CheckoutProfile"] as ICheckoutProfile;
				}

				return null;
			}
			set
			{
				if (Session.Available)
				{
					Session["CheckoutProfile"] = value;
				}
			}
		}

		public Uri KlarnaCheckoutId
		{
			get
			{
				if (Session.Available)
				{
					return (Uri)Session["klarna_checkout"];
				}

				return null;
			}
			set
			{
				if (Session.Available)
				{
					Session["klarna_checkout"] = value;
				}
			}
		}

		public virtual void CleanProfile()
		{
			Profile = null;
		}

		public virtual void InitializeProfile()
		{
			if (CartSetting.CheckoutProfileEnabled)
			{
				var checkoutProfile = IoC.Resolve<ICheckoutProfile>();

				checkoutProfile.CustomerInformation = IoC.Resolve<IProfileCustomerInformation>();
				checkoutProfile.BillingAddress = IoC.Resolve<IProfileAddress>();
				checkoutProfile.DeliveryAddress = IoC.Resolve<IProfileAddress>();

				checkoutProfile.CompanyInformation = IoC.Resolve<IProfileCompanyInformation>();
				checkoutProfile.CompanyAddress = IoC.Resolve<IProfileAddress>();
				checkoutProfile.CompanyAlternateAddress = IoC.Resolve<IProfileAddress>();

				Profile = checkoutProfile;
			}
			else
			{
				CleanProfile();
			}
		}
	}
}