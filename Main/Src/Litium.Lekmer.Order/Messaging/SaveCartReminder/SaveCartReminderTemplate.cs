﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class SaveCartReminderTemplate : Template
	{
		public SaveCartReminderTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public SaveCartReminderTemplate(IChannel channel)
			: base("SaveCartReminder", channel, false)
		{
		}
	}
}