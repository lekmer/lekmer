﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Template.Engine;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderMessenger : OrderMessenger
	{
		protected ILekmerFormatter LekmerFormatter { get; private set; }

		public LekmerOrderMessenger(IFormatter formatter, ICountryService countryService)
			: base(formatter, countryService)
		{
			LekmerFormatter = (ILekmerFormatter)formatter;
		}

		protected override Message CreateMessage(IOrderFull messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");
			if (messageArgs.Customer == null) throw new ArgumentException("messageArgs.Customer can't be null.", "messageArgs");
			if (messageArgs.Customer.CustomerInformation == null) throw new ArgumentException("messageArgs.Customer.CustomerInformation can't be null.", "messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.ChannelId);
			Template = GetTemplate(channel);

			string body = RenderOrder(channel, messageArgs);

			string subject = RenderSubject(messageArgs);

			string fromName = GetContent("FromName");
			string fromEmail = GetContent("FromEmail");

			var message = new EmailMessage
			{
				Body = body,
				ProviderName = "email",
				FromName = fromName,
				FromEmail = fromEmail,
				Subject = subject,
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name =
					messageArgs.Customer.CustomerInformation.FirstName + " " +
					messageArgs.Customer.CustomerInformation.LastName,
				Address = messageArgs.Customer.CustomerInformation.Email
			});

			return message;
		}

		protected override string RenderOrder(IChannel channel, IOrderFull order)
		{
			var lekmerOrderFull = (ILekmerOrderFull)order;

			Fragment fragmentContent = Template.GetFragment("Content");

			string paymentTypeTitle = string.Empty;
			string paymentTypeAlias = string.Empty;
			if (order.Payments.Count > 0)
			{
				IPaymentType paymentType = IoC.Resolve<IPaymentTypeService>().GetAll(UserContext.Current).FirstOrDefault(pt => pt.Id == order.Payments[0].PaymentTypeId);
				if (paymentType != null)
				{
					paymentTypeTitle = paymentType.Title;
					paymentTypeAlias = AliasHelper.GetAliasValue("Email.OrderConfirm.PaymentType_" + paymentType.CommonName);
				}
			}
			fragmentContent.AddVariable("Order.PaymentTypeTitle", paymentTypeTitle);
			fragmentContent.AddVariable("Order.PaymentType.Alias", paymentTypeAlias);

			// Delivery method variables.
			AddDeliveryMethodVariables(fragmentContent, order.DeliveryMethodId);

			// Optional delivery method variables.
			AddOptionalDeliveryMethodVariables(fragmentContent, lekmerOrderFull.OptionalDeliveryMethodId);

			// Diapers delivery method variables.
			AddDiapersDeliveryMethodVariables(fragmentContent, lekmerOrderFull.DiapersDeliveryMethodId);

			var orderItems = order.GetOrderItems();
			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(channel, orderItems), VariableEncoding.None);

			FullFillOrderWithCampaignInfo(order);

			fragmentContent.AddEntity(order);

			var orderCampaignInfo = order.CampaignInfo;
			if (orderCampaignInfo != null && orderCampaignInfo.Campaigns.Count > 0)
			{
				fragmentContent.AddVariable("Iterate:CartCampaign", RenderCartCampaigns(orderCampaignInfo.Campaigns), VariableEncoding.None);
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", true);
			}
			else
			{
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", false);
			}

			RenderCampaignDiscount(fragmentContent, order, orderCampaignInfo);
			RenderVoucherDiscount(fragmentContent, order);

			// Gift cards.
			var giftCardViaMailInfoCollection = IoC.Resolve<IGiftCardViaMailInfoService>().GetAllByOrder(order.Id);
			if (giftCardViaMailInfoCollection != null && giftCardViaMailInfoCollection.Count > 0)
			{
				fragmentContent.AddCondition("IsGiftCardWillBeSent", true);
				fragmentContent.AddVariable("Iterate:GiftCardViaEmail", RenderGiftCardsViaEmailList(channel, giftCardViaMailInfoCollection), VariableEncoding.None);
			}
			else
			{
				fragmentContent.AddCondition("IsGiftCardWillBeSent", false);
			}

			fragmentContent.AddCondition("Order.IsProductAbove60LExist", lekmerOrderFull.IsProductAbove60LExist(UserContext.Current, orderItems));

			fragmentContent.AddCondition("Order.ContainsMixedItems", lekmerOrderFull.CheckForMixedItems(UserContext.Current, orderItems));
			fragmentContent.AddCondition("Order.HasDropShipItems", lekmerOrderFull.IsDropShipItemExist(orderItems));

			return HttpUtility.HtmlDecode(fragmentContent.Render());
		}

		protected virtual string RenderCartCampaigns(Collection<IOrderCampaign> campaigns)
		{
			if (campaigns == null) return null;

			var campaignBuilder = new StringBuilder();

			foreach (IOrderCampaign campaign in campaigns)
			{
				Fragment fragmentCartCampaign = Template.GetFragment("CartCampaign");
				fragmentCartCampaign.AddVariable("Campaign.Title", campaign.Title);
				campaignBuilder.AppendLine(fragmentCartCampaign.Render());
			}
			return campaignBuilder.ToString();
		}

		protected override string RenderOrderItemList(IChannel channel, IEnumerable<IOrderItem> orderItems)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in orderItems)
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddEntity(orderItem);
				fragmentOrderItem.AddVariable("Channel.BaseUrlHttp", UrlHelper.BaseUrlHttp, VariableEncoding.None);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

		protected override string RenderSubject(IOrderFull order)
		{
			return HttpUtility.HtmlDecode(base.RenderSubject(order));
		}

		protected virtual void RenderCampaignDiscount(Fragment fragmentContent, IOrderFull order, IOrderCampaignInfo orderCampaignInfo)
		{
			var lekmerOrderFull = (ILekmerOrderFull) order;

			bool isFixedDiscount = false;
			decimal totalFixedDiscount = 0;

			bool isPercentageDiscount = false;
			decimal totalPercentageDiscount = 0;
			
			if (orderCampaignInfo != null && orderCampaignInfo.Campaigns.Count > 0)
			{
				lekmerOrderFull.GetTotalDiscount(
					UserContext.Current.Channel,
					orderCampaignInfo,
					out isFixedDiscount,
					out totalFixedDiscount,
					out isPercentageDiscount,
					out totalPercentageDiscount);
			}

			fragmentContent.AddCondition("IsFixedDiscountApplied", isFixedDiscount);
			if (isFixedDiscount)
			{
				fragmentContent.AddVariable("FixedDiscount", LekmerFormatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, totalFixedDiscount));
			}

			fragmentContent.AddCondition("IsPercentageDiscountApplied", isPercentageDiscount);
			if (isPercentageDiscount)
			{
				fragmentContent.AddVariable("PercentageDiscount", string.Format("{0} %", Math.Round(totalPercentageDiscount, 0, MidpointRounding.AwayFromZero)));
			}
		}

		protected virtual void RenderVoucherDiscount(Fragment fragmentContent, IOrderFull order)
		{
			var lekmerOrder = (ILekmerOrderFull)order;

			IOrderVoucherInfo voucherInfo = lekmerOrder.VoucherInfo;

			if (voucherInfo != null && voucherInfo.VoucherCode.HasValue())
			{
				fragmentContent.AddCondition("IsVoucherUsed", true);
				fragmentContent.AddCondition("IsVoucherApplied", voucherInfo.DiscountValue.HasValue);

				bool hasAmountUsed = voucherInfo.AmountUsed.HasValue && voucherInfo.AmountUsed.Value > 0m;
				fragmentContent.AddCondition("VoucherDiscount.HasAmountUsed", hasAmountUsed);

				if (voucherInfo.AmountUsed.HasValue)
				{
					fragmentContent.AddVariable("VoucherDiscount", LekmerFormatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, voucherInfo.AmountUsed.Value));
				}
				else
				{
					fragmentContent.AddVariable("VoucherDiscount", LekmerFormatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, 0m));
				}

				fragmentContent.AddCondition("IsVoucherWithoutDiscount", voucherInfo.DiscountValue.HasValue && voucherInfo.DiscountValue <= 0);
			}
			else
			{
				fragmentContent.AddCondition("IsVoucherUsed", false);
				fragmentContent.AddCondition("IsVoucherApplied", false);
				fragmentContent.AddCondition("VoucherDiscount.HasAmountUsed", false);
			}
		}

		private string RenderGiftCardsViaEmailList(IChannel channel, IEnumerable<IGiftCardViaMailInfo> giftCardViaMailInfoCollection)
		{
			var itemBuilder = new StringBuilder();

			foreach (IGiftCardViaMailInfo giftCardViaMailInfo in giftCardViaMailInfoCollection)
			{
				Fragment giftCardViaEmail = Template.GetFragment("GiftCardViaEmail");

				giftCardViaEmail.AddVariable("GiftCard.DiscountAmount", LekmerFormatter.FormatPriceChannelOrLessDecimals(channel, giftCardViaMailInfo.DiscountValue));
				giftCardViaEmail.AddVariable("GiftCard.DiscountAmount_Formatted", LekmerFormatter.FormatPriceTwoOrLessDecimals(channel, giftCardViaMailInfo.DiscountValue));
				giftCardViaEmail.AddVariable("GiftCard.DateToSend", Formatter.FormatShortDate(channel, giftCardViaMailInfo.DateToSend));
				giftCardViaEmail.AddVariable("GiftCard.Currency", channel.Currency.Iso);
				giftCardViaEmail.AddVariable("GiftCard.CampaignTitle", GetCampaignTitle(giftCardViaMailInfo.CampaignId));

				itemBuilder.AppendLine(giftCardViaEmail.Render());
			}

			return itemBuilder.ToString();
		}


		protected override void AddCustomerAddressVariables(Fragment fragment, string prefix, IAddress address)
		{
			base.AddCustomerAddressVariables(fragment, prefix, address);

			var lekmerAddress = address as ILekmerAddress;

			if (lekmerAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerAddress.HouseExtension);
				fragment.AddVariable(prefix + ".Reference", lekmerAddress.Reference);
				fragment.AddVariable(prefix + ".DoorCode", lekmerAddress.DoorCode);
			}
		}

		protected override void AddOrderVariables(Fragment fragment, IChannel channel, IOrderFull order)
		{
			base.AddOrderVariables(fragment, channel, order);

			var lekmerOrder = order as ILekmerOrderFull;
			if (lekmerOrder != null)
			{
				fragment.AddCondition("Order.HasAlternateAddress", lekmerOrder.AlternateAddress != null);
				if (lekmerOrder.AlternateAddress != null)
				{
					AddOrderAddressVariables(fragment, "Order.AlternateAddress", lekmerOrder.AlternateAddress);
				}
			}
		}

		protected override void AddCustomerVariables(Fragment fragment, IChannel channel, ICustomer customer)
		{
			if (fragment == null) throw new ArgumentNullException("fragment");
			if (channel == null) throw new ArgumentNullException("channel");
			if (customer == null) throw new ArgumentNullException("customer");
			
			fragment.AddVariable("CustomerInformation.Id", customer.Id.ToString((IFormatProvider)CultureInfo.InvariantCulture));
			fragment.AddVariable("CustomerInformation.ErpId", customer.ErpId);

			ICustomerInformation customerInformation = customer.CustomerInformation;
			fragment.AddCondition("Customer.HasCustomerInformation", customerInformation != null);
			if (customerInformation == null) return;

			fragment.AddVariable("CustomerInformation.FirstName", customerInformation.FirstName);
			fragment.AddVariable("CustomerInformation.LastName", customerInformation.LastName);
			fragment.AddVariable("CustomerInformation.CivicNumber", customerInformation.CivicNumber);
			fragment.AddVariable("CustomerInformation.PhoneNumber", customerInformation.PhoneNumber);
			fragment.AddVariable("CustomerInformation.CellPhoneNumber", customerInformation.CellPhoneNumber);
			fragment.AddVariable("CustomerInformation.Email", customerInformation.Email);
			fragment.AddVariable("CustomerInformation.CreatedDate", this.Formatter.FormatDateTime(channel, customerInformation.CreatedDate));
			fragment.AddCondition("CustomerInformation.HasDeliveryAddress", customerInformation.DefaultDeliveryAddress != null);
			if (customerInformation.DefaultDeliveryAddress != null)
			{
				AddCustomerAddressVariables(fragment, "CustomerInformation.DeliveryAddress", customerInformation.DefaultDeliveryAddress);
			}
			fragment.AddCondition("CustomerInformation.HasBillingAddress", customerInformation.DefaultBillingAddress != null);
			if (customerInformation.DefaultBillingAddress != null)
			{
				AddCustomerAddressVariables(fragment, "CustomerInformation.BillingAddress", customerInformation.DefaultBillingAddress);
			}
			int? deliveryAddressId = customerInformation.DefaultDeliveryAddressId;
			int? billingAddressId = customerInformation.DefaultBillingAddressId;
			bool flag = deliveryAddressId.GetValueOrDefault() == billingAddressId.GetValueOrDefault() && deliveryAddressId.HasValue == billingAddressId.HasValue;
			fragment.AddCondition("CustomerInformation.DeliveryAddressIsEqualToBillingAddress", flag);

			var info = customer.CustomerInformation as ILekmerCustomerInformation;
			if (info != null)
			{
				fragment.AddCondition("CustomerInformation.IsCompany", info.IsCompany);
				fragment.AddCondition("CustomerInformation.HasAlternateAddress", info.AlternateAddress != null);
				if (info.AlternateAddress != null)
				{
					AddCustomerAddressVariables(fragment, "CustomerInformation.AlternateAddress", info.AlternateAddress);
				}
			}
		}

		protected override void AddOrderAddressVariables(Fragment fragment, string prefix, IOrderAddress address)
		{
			base.AddOrderAddressVariables(fragment, prefix, address);

			var lekmerAddress = address as ILekmerOrderAddress;
			if (lekmerAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerAddress.HouseExtension);
				fragment.AddVariable(prefix + ".Reference", lekmerAddress.Reference);
			}
		}

		protected virtual void AddDeliveryMethodVariables(Fragment fragment, int deliveryMethodId)
		{
			IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodService>().GetById(UserContext.Current, deliveryMethodId);

			fragment.AddVariable("Order.DeliveryMethod.Id", deliveryMethodId.ToString(CultureInfo.InvariantCulture));

			if (deliveryMethod != null)
			{
				fragment.AddVariable("Order.DeliveryMethod.Title", deliveryMethod.Title);
				fragment.AddVariable("Order.DeliveryMethod.Alias", AliasHelper.GetAliasValue("Email.OrderConfirm.DeliveryMethod_" + deliveryMethod.CommonName));
			}
			else
			{
				fragment.AddVariable("Order.DeliveryMethod.Title", string.Empty);
				fragment.AddVariable("Order.DeliveryMethod.Alias", string.Empty);
			}
		}

		protected virtual void AddOptionalDeliveryMethodVariables(Fragment fragment, int? deliveryMethodId)
		{
			fragment.AddCondition("Order.OptionalDeliveryMethod.IsUsed", deliveryMethodId.HasValue);

			if (deliveryMethodId.HasValue)
			{
				IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodService>().GetById(UserContext.Current, deliveryMethodId.Value);

				fragment.AddVariable("Order.OptionalDeliveryMethod.Id", deliveryMethodId.Value.ToString(CultureInfo.InvariantCulture));

				if (deliveryMethod != null)
				{
					fragment.AddVariable("Order.OptionalDeliveryMethod.Title", deliveryMethod.Title);
					fragment.AddVariable("Order.OptionalDeliveryMethod.Alias", AliasHelper.GetAliasValue("Email.OrderConfirm.DeliveryMethod_" + deliveryMethod.CommonName));
				}
				else
				{
					fragment.AddVariable("Order.OptionalDeliveryMethod.Title", string.Empty);
					fragment.AddVariable("Order.OptionalDeliveryMethod.Alias", string.Empty);
				}
			}
		}

		protected virtual void AddDiapersDeliveryMethodVariables(Fragment fragment, int? deliveryMethodId)
		{
			fragment.AddCondition("Order.DiapersDeliveryMethod.IsUsed", deliveryMethodId.HasValue);

			if (deliveryMethodId.HasValue)
			{
				IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodService>().GetById(UserContext.Current, deliveryMethodId.Value);

				fragment.AddVariable("Order.DiapersDeliveryMethod.Id", deliveryMethodId.Value.ToString(CultureInfo.InvariantCulture));

				if (deliveryMethod != null)
				{
					fragment.AddVariable("Order.DiapersDeliveryMethod.Title", deliveryMethod.Title);
					fragment.AddVariable("Order.DiapersDeliveryMethod.Alias", AliasHelper.GetAliasValue("Email.OrderConfirm.DeliveryMethod_" + deliveryMethod.CommonName));
				}
				else
				{
					fragment.AddVariable("Order.DiapersDeliveryMethod.Title", string.Empty);
					fragment.AddVariable("Order.DiapersDeliveryMethod.Alias", string.Empty);
				}
			}
		}

		protected virtual string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}

		protected virtual void FullFillOrderWithCampaignInfo(IOrderFull order)
		{
			if (order.CampaignInfo == null)
			{
				order.CampaignInfo = IoC.Resolve<ILekmerOrderCampaignInfoService>().GetByOrder(order.Id);
			}
		}

		private string GetCampaignTitle(int campaignId)
		{
			string title = string.Empty;

			var cartCampaign =  IoC.Resolve<ICartCampaignSharedService>().GetById(campaignId);
			if (cartCampaign != null)
			{
				title = cartCampaign.Title;
			}
			else
			{
				var productCampaign = IoC.Resolve<IProductCampaignService>().GetById(UserContext.Current, campaignId);
				if (productCampaign != null)
				{
					title = productCampaign.Title;
				}
			}

			return title;
		}
	}
}