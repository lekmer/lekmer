﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Payment.Collector;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class CollectorTimeoutOrderDataMapper : DataMapperBase<ICollectorTimeoutOrder>
	{
		public CollectorTimeoutOrderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICollectorTimeoutOrder Create()
		{
			var timeoutOrder = IoC.Resolve<ICollectorTimeoutOrder>();

			timeoutOrder.TransactionId = MapValue<int>("TimeoutOrder.TransactionId");
			timeoutOrder.StatusCode = (CollectorTransactionStatus?)MapNullableValue<int?>("TimeoutOrder.StatusCode");
			timeoutOrder.InvoiceStatus = (CollectorInvoiceStatus?)MapNullableValue<int?>("TimeoutOrder.InvoiceStatus");
			timeoutOrder.OrderId = MapValue<int>("TimeoutOrder.OrderId");
			timeoutOrder.InvoiceNo = MapNullableValue<string>("TimeoutOrder.InvoiceNo");
			timeoutOrder.ChannelId = MapValue<int>("TimeoutOrder.ChannelId");

			return timeoutOrder;
		}
	}
}