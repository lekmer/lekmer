﻿using System;
using System.Data;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerOrderFullDataMapper : OrderDataMapper<IOrderFull>
	{
		public LekmerOrderFullDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IOrderFull Create()
		{
			var order = (ILekmerOrderFull)base.Create();

			order.PaymentCost = MapValue<decimal>("Lekmer.PaymentCost");
			order.CustomerIdentificationKey = MapNullableValue<string>("Lekmer.CustomerIdentificationKey");
			order.FeedbackToken = MapNullableValue<Guid?>("Lekmer.FeedbackToken");
			order.AlternateAddressId = MapNullableValue<int?>("Lekmer.AlternateAddressId");
			order.CivicNumber = MapNullableValue<string>("Lekmer.CivicNumber");
			order.OptionalDeliveryMethodId = MapNullableValue<int?>("Lekmer.OptionalDeliveryMethodId");
			order.OptionalFreightCost = MapNullableValue<decimal?>("Lekmer.OptionalFreightCost");
			order.DiapersDeliveryMethodId = MapNullableValue<int?>("Lekmer.DiapersDeliveryMethodId");
			order.DiapersFreightCost = MapNullableValue<decimal?>("Lekmer.DiapersFreightCost");
			order.KcoId = MapNullableValue<string>("Lekmer.KcoId");
			order.UserAgent = MapNullableValue<string>("Lekmer.UserAgent");

			return order;
		}
	}
}