﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using IChannel = Litium.Scensum.Core.IChannel;

namespace Litium.Lekmer.Order
{
	public class CartHelper
	{
		public virtual bool IsPackageWithSizes(ILekmerCartItem cartItem)
		{
			bool isPackageWithSizes = false;

			if (cartItem.Product.IsPackage() && cartItem.PackageElements != null)
			{
				if (cartItem.PackageElements.Any(packageElement => packageElement.SizeId != null))
				{
					isPackageWithSizes = true;
				}
			}

			return isPackageWithSizes;
		}

		/// <summary>
		/// Code "G" in “ProductVisibility” node indicates an item that will be sent from Fagerdala otherwise the item will be sent from Falkenberg.
		/// </summary>
		public virtual bool CheckForMixedWarehouses(ICartFull cartFull)
		{
			Collection<ICartItem> cartItems = cartFull.GetCartItems();

			bool isProductAbove60L = false; // item will be sent from Fagerdala
			bool isProductUnder60L = false; // item will be sent from Falkenberg

			foreach (var item in cartItems)
			{
				if (isProductAbove60L && isProductUnder60L)
				{
					break;
				}

				CheckForItemWarehouse((ILekmerProduct)item.Product, ref isProductAbove60L, ref isProductUnder60L);
			}

			return isProductAbove60L && isProductUnder60L;
		}

		/// <summary>
		///  When cart contains diapers and any other items - it has mixed items
		/// </summary>
		public virtual bool CheckForMixedItems(ICartFull cartFull)
		{
			if (DiapersAreSellable(UserContext.Current.Channel))
			{
				var cart = (ILekmerCartFull)cartFull;
				if (cart.ItemsType.IsFlagSet(OrderItemsType.Diapers) && cart.ItemsType.IsFlagSet(OrderItemsType.Regular))
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		///  When cart contains only diapers.
		/// </summary>
		public virtual bool ContainsOnlyDiapers(ICartFull cartFull)
		{
			if (DiapersAreSellable(UserContext.Current.Channel))
			{
				var cart = (ILekmerCartFull)cartFull;
				if (cart.ItemsType.IsFlagSet(OrderItemsType.Diapers) && !cart.ItemsType.IsFlagSet(OrderItemsType.Regular))
				{
					return true;
				}
			}

			return false;
		}

		/// <summary>
		///  Checks whether diapers are sellable or not in specified channel.
		/// </summary>
		public virtual bool DiapersAreSellable(IChannel channel)
		{
			string countryIso = UserContext.Current.Channel.Country.Iso.ToUpperInvariant();
			if (countryIso == "SE" || countryIso == "DK" || countryIso == "FI")
			{
				return true;
			}

			return false;
		}

		/// <summary>
		///  Checks whether home delivery is obligatory for non-diapers on order with diapers in specified channel.
		/// </summary>
		public virtual bool HomeDeliveryIsObligatory(IChannel channel)
		{
			string countryIso = UserContext.Current.Channel.Country.Iso.ToUpperInvariant();
			if (countryIso == "DK")
			{
				return true;
			}

			return false;
		}

		// Cart render

		public virtual void RenderCartInfo(Fragment fragment, ILekmerCartFull cart, ILekmerFormatter formatter)
		{
			RenderCartInfo(fragment, cart, formatter, Channel.Current);
		}

		public virtual void RenderCartInfo(Fragment fragment, ILekmerCartFull cart, ILekmerFormatter formatter, IChannel channel)
		{
			fragment.AddVariable("Cart.QuantitySummary", formatter.FormatNumber(channel, cart.GetQuantitySummary()));

			fragment.AddCondition("Cart.IsItemsLimitExceeded", cart.IsItemsLimitExceeded());
			fragment.AddCondition("Cart.HasDropShipItems", CheckForDropShipItems(cart));
		}

		public virtual void RenderCartPrices(Fragment fragment, ILekmerCartFull cart, ILekmerFormatter formatter)
		{
			RenderCartPrices(fragment, cart, formatter, Channel.Current);
		}

		public virtual void RenderCartPrices(Fragment fragment, ILekmerCartFull cart, ILekmerFormatter formatter, IChannel channel)
		{
			Price cartPrice = cart.GetActualPriceSummary(channel.Currency);
			decimal vatSummary = cartPrice.IncludingVat - cartPrice.ExcludingVat;

			// price with channel decimals / possible rounding
			fragment.AddVariable("Cart.PriceIncludingVatSummary", formatter.FormatPrice(channel, cartPrice.IncludingVat));
			fragment.AddVariable("Cart.PriceExcludingVatSummary", formatter.FormatPrice(channel, cartPrice.ExcludingVat));
			fragment.AddVariable("Cart.VatSummary", formatter.FormatPrice(channel, vatSummary));

			// price with decimals / without rounding
			fragment.AddVariable("Cart.PriceIncludingVatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, cartPrice.IncludingVat));
			fragment.AddVariable("Cart.PriceExcludingVatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, cartPrice.ExcludingVat));
			fragment.AddVariable("Cart.VatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, vatSummary));
		}

		public virtual void RenderCartItemPrices(Fragment fragment, ILekmerCartItem cartItem, ILekmerFormatter formatter)
		{
			RenderCartItemPrices(fragment, cartItem, formatter, Channel.Current);
		}

		public virtual void RenderCartItemPrices(Fragment fragment, ILekmerCartItem cartItem, ILekmerFormatter formatter, IChannel channel)
		{
			// Product price

			// price with decimals / without rounding
			fragment.AddVariable("Product.CampaignInfo.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, cartItem.Product.CampaignInfo.Price.IncludingVat));
			fragment.AddVariable("Product.CampaignInfo.Price_Formatted.Json", cartItem.Product.CampaignInfo.Price.IncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);
			fragment.AddVariable("Product.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, cartItem.Product.Price.PriceIncludingVat));
			fragment.AddVariable("Product.Price_Formatted.Json", cartItem.Product.Price.PriceIncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);

			Price actualPrice = cartItem.GetActualPriceSummary();
			decimal vatSummary = actualPrice.IncludingVat - actualPrice.ExcludingVat;

			// Cart item price

			// price with channel decimals / possible rounding
			fragment.AddVariable("CartItem.PriceIncludingVatSummary", formatter.FormatPrice(channel, actualPrice.IncludingVat));
			fragment.AddVariable("CartItem.PriceExcludingVatSummary", formatter.FormatPrice(channel, actualPrice.ExcludingVat));
			fragment.AddVariable("CartItem.VatSummary", formatter.FormatPrice(channel, vatSummary));

			// price with decimals / without rounding
			fragment.AddVariable("CartItem.PriceIncludingVatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPrice.IncludingVat));
			fragment.AddVariable("CartItem.PriceExcludingVatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPrice.ExcludingVat));
			fragment.AddVariable("CartItem.VatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, vatSummary));
		}

		public virtual void RenderFreeCartItemPrices(Fragment fragment, ICartItemOption cartItemOption, ILekmerFormatter formatter)
		{
			// price with decimals / without rounding
			fragment.AddVariable("Product.CampaignInfo.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(Channel.Current, cartItemOption.Product.CampaignInfo.Price.IncludingVat));
			fragment.AddVariable("Product.CampaignInfo.Price_Formatted.Json", cartItemOption.Product.CampaignInfo.Price.IncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);
			fragment.AddVariable("Product.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(Channel.Current, cartItemOption.Product.Price.PriceIncludingVat));
			fragment.AddVariable("Product.Price_Formatted.Json", cartItemOption.Product.Price.PriceIncludingVat.ToString(CultureInfo.InvariantCulture), VariableEncoding.JavaScriptStringEncode);

			Price actualPrice = cartItemOption.GetActualPriceSummary();
			decimal vatSummary = actualPrice.IncludingVat - actualPrice.ExcludingVat;

			// price with channel decimals / possible rounding
			fragment.AddVariable("CartItemOption.PriceIncludingVatSummary", formatter.FormatPrice(Channel.Current, actualPrice.IncludingVat));
			fragment.AddVariable("CartItemOption.PriceExcludingVatSummary", formatter.FormatPrice(Channel.Current, actualPrice.ExcludingVat));
			fragment.AddVariable("CartItemOption.VatSummary", formatter.FormatPrice(Channel.Current, vatSummary));

			// price with decimals / without rounding
			fragment.AddVariable("CartItemOption.PriceIncludingVatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(Channel.Current, actualPrice.IncludingVat));
			fragment.AddVariable("CartItemOption.PriceExcludingVatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(Channel.Current, actualPrice.ExcludingVat));
			fragment.AddVariable("CartItemOption.VatSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(Channel.Current, vatSummary));
		}


		protected virtual void CheckForItemWarehouse(ILekmerProduct product, ref bool isProductAbove60L, ref bool isProductUnder60L)
		{
			if (product.IsPackage())
			{
				var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
				var productIds = productService.GetIdAllByPackageMasterProduct(UserContext.Current, product.Id);
				var packageProducts = productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
				foreach (var packageProduct in packageProducts)
				{
					if (isProductAbove60L && isProductUnder60L) break;
					IsProductAbove60((ILekmerProduct) packageProduct, ref isProductAbove60L, ref isProductUnder60L);
				}
			}
			else
			{
				IsProductAbove60(product, ref isProductAbove60L, ref isProductUnder60L);
			}
		}

		protected virtual void IsProductAbove60(ILekmerProduct product, ref bool isProductAbove60L, ref bool isProductUnder60L)
		{
			if (product.IsAbove60L)
			{
				isProductAbove60L = true;
			}
			else
			{
				isProductUnder60L = true;
			}
		}


		public virtual bool CheckForDropShipItems(ICartFull cartFull)
		{
			bool hasDropShipItems = false;

			Collection<ICartItem> cartItems = cartFull.GetCartItems();
			foreach (var item in cartItems)
			{
				if (hasDropShipItems) break;

				var product = (ILekmerProduct) item.Product;

				hasDropShipItems = product.IsDropShip;

				//if (product.IsPackage())
				//{
				//	var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
				//	var productIds = productService.GetIdAllByPackageMasterProduct(UserContext.Current, product.Id);
				//	var packageProducts = productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
				//	foreach (var packageProduct in packageProducts)
				//	{
				//		if (hasDropShipItems) break;
				//		hasDropShipItems = ((ILekmerProduct)packageProduct).IsDropShip;
				//	}
				//}
				//else
				//{
				//	hasDropShipItems = product.IsDropShip;
				//}
			}

			return hasDropShipItems;
		}
	}
}