﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using Litium.Lekmer.Common.Job;
using log4net;

namespace Litium.Lekmer.NewsletterSubscriber.Service
{
	public partial class NewsletterSubscriberServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private readonly Collection<IJob> _jobs = new Collection<IJob>();

		public NewsletterSubscriberServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			_log.Info("Service stopped.");
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var newsletterSubscriberJob = new NewsletterSubscriberJob { ScheduleSetting = new NewsletterSubscriberScheduleSetting(), IntervalCalculator = intervalCalculator };
			_jobs.Add(newsletterSubscriberJob);

			var newsletterUnsubscriberJob = new NewsletterUnsubscriberJob { ScheduleSetting = new NewsletterUbsubscriberScheduleSetting(), IntervalCalculator = intervalCalculator };
			_jobs.Add(newsletterUnsubscriberJob);
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}