﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Service
{
	public class NewsletterSubscriberJob : BaseJob
	{
		public override string Group
		{
			get { return "Newsletter"; }
		}

		public override string Name
		{
			get { return "NewsletterSubscriberJob"; }
		}

		protected override void ExecuteAction()
		{
			var newsletterSubscriberExporter = IoC.Resolve<INewsletterSubscriberExporter>();

			newsletterSubscriberExporter.Execute();
		}
	}
}
