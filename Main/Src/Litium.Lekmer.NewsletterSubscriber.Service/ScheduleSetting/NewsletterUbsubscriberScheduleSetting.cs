﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.NewsletterSubscriber.Service
{
	public class NewsletterUbsubscriberScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "NewsletterSubscriber"; }
		}

		protected override string GroupName
		{
			get { return "NewsletterUnsubscriberJob"; }
		}
	}
}