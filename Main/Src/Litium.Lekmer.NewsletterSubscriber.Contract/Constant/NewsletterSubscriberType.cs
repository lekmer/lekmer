﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public enum NewsletterSubscriberType
	{
		Unknown = 0,
		Manual = 1,
		OrderBased = 2,
		BackOffice = 3
	}
}