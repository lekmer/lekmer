﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriberOptionService
	{
		INewsletterUnsubscriberOption Create();
		Collection<INewsletterUnsubscriberOption> GetByUnsubscriber(int unsubscriberId);
		int Save(INewsletterUnsubscriberOption unsubscriberOption);
		void Delete(int unsubscriberOptionId);
	}
}
