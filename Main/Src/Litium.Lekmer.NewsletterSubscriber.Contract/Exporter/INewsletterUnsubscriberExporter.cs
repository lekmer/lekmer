﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriberExporter
	{
		void Execute();
	}
}