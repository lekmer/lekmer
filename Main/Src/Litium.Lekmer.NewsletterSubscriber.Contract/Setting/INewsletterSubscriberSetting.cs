﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterSubscriberSetting
	{
		string ApplicationName { get; }
		string PostUrl { get; }
		string UserName { get; }
		string UserToken { get; }
		string[] GetMailingList(int channelId);
		bool SubscribeFromOrders { get; }
	}
}