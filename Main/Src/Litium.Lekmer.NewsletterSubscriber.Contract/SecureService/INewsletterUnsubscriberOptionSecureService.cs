﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriberOptionSecureService
	{
		INewsletterUnsubscriberOption Create();
		int Save(ISystemUserFull systemUserFull, INewsletterUnsubscriberOption unsubscriberOption);
		void Delete(ISystemUserFull systemUserFull, int unsubscriberOptionId);
		Collection<INewsletterUnsubscriberOption> GetByUnsubscriber(int unsubscriberId);
	}
}