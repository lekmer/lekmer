﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriberSecureService
	{
		INewsletterUnsubscriber Create(int channelId);
		int Save(ISystemUserFull systemUserFull, INewsletterUnsubscriber unsubscriber);
		bool Delete(ISystemUserFull systemUserFull, int id);
		INewsletterUnsubscriber Get(int id);
		Collection<INewsletterUnsubscriber> SearchByEmail(int channelId, string email, int page, int pageSize, out int itemsCount);
	}
}