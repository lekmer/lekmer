﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireResponse
	{
		int Status { get; set; }
		string ErrorMessage { get; set; }
	}
}