﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriberOption : IBusinessObjectBase
	{
		int Id { get; set; }
		int UnsubscriberId { get; set; }
		int NewsletterTypeId { get; set; }
		DateTime CreatedDate { get; set; }
	}
}