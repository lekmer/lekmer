﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireContactList
	{
		int ListId { get; set; }
		string Name { get; set; }
		int SubscribeCount { get; set; }
		int UnsubscribeCount { get; set; }
		int AutoResponderCount { get; set; }
	}
}