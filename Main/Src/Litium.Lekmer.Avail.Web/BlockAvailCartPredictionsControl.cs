﻿using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Avail.Web
{
	public class BlockAvailCartPredictionsControl : BlockControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		public BlockAvailCartPredictionsControl(ITemplateFactory factory, IBlockService blockService)
			: base(factory)
		{
			_blockService = blockService;
		}


		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var cart = IoC.Resolve<ICartSession>().Cart;
			if (cart == null)
			{
				return new BlockContent();
			}
			var cartItems = IoC.Resolve<ICartSession>().Cart.GetCartItems();

			if (cartItems.Count<=0)
			{
				return new BlockContent();
			}
			
			var fragmentHead = Template.GetFragment("Head");
            fragmentHead.AddEntity(Block);
			fragmentHead.AddVariable("Param.Cart.Items", RenderCartItemsArrayParam());
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);
			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}

		private static string RenderCartItemsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			var cart = IoC.Resolve<ICartSession>().Cart;
			if (cart == null)
			{
				return null;
			}

			var cartItems = IoC.Resolve<ICartSession>().Cart.GetCartItems();

			itemBuilder.Append("new Array(");

			for (int i = 0; i < cartItems.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", cartItems[i].Product.Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", cartItems[i].Product.Id));
				}
			}

			itemBuilder.Append(")");

			return itemBuilder.ToString();

		}
	}
}