﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.GoogleFeed
{
	public interface IGoogleFeedService
	{
		Collection<IGoogleFeedInfo> GetGoogleFeedInfoList(IUserContext userContext);
	}
}