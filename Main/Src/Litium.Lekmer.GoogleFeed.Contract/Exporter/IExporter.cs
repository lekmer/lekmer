﻿namespace Litium.Lekmer.GoogleFeed
{
	public interface IExporter
	{
		void Execute();
	}
}