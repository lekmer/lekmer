﻿namespace Litium.Lekmer.GoogleFeed
{
	public static class Const
	{
		public const string RssNode = "rss";
		public const string ChannelNode = "channel";
		public const string TitleNode = "title";
		public const string LinkNode = "link";
		public const string DescriptionNode = "description";
		public const string ItemNode = "item";
		public const string IdNode = "id";
		public const string ProductTypeNode = "product_type";
		public const string ImageLinkNode = "image_link";
		public const string AdditionalImageLinkNode = "additional_image_link";
		public const string ConditionNode = "condition";
		public const string AvailabilityNode = "availability";
		public const string PriceNode = "price";
		public const string SalePriceNode = "sale_price";
		public const string EffectiveDateNode = "sale_price_effective_date";
		public const string BrandNode = "brand";
		public const string IdentifierExistsNode = "identifier_exists";
		public const string AgeGroupNode = "age_group";
		public const string AdwordsRedirectNode = "adwords_redirect";
		public const string ShippingNode = "shipping";
		public const string CountryNode = "country";
		public const string ItemGroupIdNode = "item_group_id";
		public const string SizeNode = "size";

		public const string VersionAttr = "version";
		public const string NamespaceAttr = "xmlns:g";


		public const string RssVersion = "2.0";
		public const string RssPrefix = "g";
		public const string RssNamespaceUri = "http://base.google.com/ns/1.0";

		public const string Condition = "new";
		public const string InStock = "in stock";
		public const string OutOfStock = "out of stock";
		public const string IdentifierFalse = "FALSE";
		public const string AgeGroup = "Kids";
	}
}