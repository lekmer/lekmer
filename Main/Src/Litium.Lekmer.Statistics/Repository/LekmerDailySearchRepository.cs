﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Statistics;
using Litium.Scensum.Statistics.Repository;

namespace Litium.Lekmer.Statistics.Repository
{
	public class LekmerDailySearchRepository : DailySearchRepository
	{
		public override void Save(IDailySearch dailySearch)
		{
			if (dailySearch == null)
			{
				throw new ArgumentNullException("dailySearch");
			}

			var date = new DateTime(dailySearch.Year, dailySearch.Month, dailySearch.Day);
			var query = dailySearch.Query;
			if (!string.IsNullOrEmpty(query) && query.Length > 400)
			{
				query = dailySearch.Query.Substring(0, 400);
			}
			
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ChannelId", dailySearch.ChannelId, SqlDbType.Int),
				ParameterHelper.CreateParameter("Query", query, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("Date", date, SqlDbType.SmallDateTime),
				ParameterHelper.CreateParameter("HitCount", dailySearch.HitCount, SqlDbType.Int),
				ParameterHelper.CreateParameter("SearchCount", dailySearch.SearchCount, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("DailySearchRepository.Save");
			new DataHandler().ExecuteCommandWithReturnValue("[statistics].[pDailySearchSave]", parameters, dbSettings);
		}
	}
}