using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Lekmer.Cache.Setting;
using Litium.Scensum.Foundation;
using log4net;
using log4net.Config;

namespace Litium.Scensum.Media.Web
{
	public class Global : HttpApplication
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Start(object sender, EventArgs e)
		{
			RoutingRegistration.RegisterRoutes(RouteTable.Routes);

			InitializeCache();

			GlobalContext.Properties["SiteName"] = System.Web.Hosting.HostingEnvironment.SiteName;
			XmlConfigurator.Configure();

			Initializer.InitializeAll();
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_Start(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Error(object sender, EventArgs e)
		{
			HttpContext context = HttpContext.Current;

			var exception = context.Server.GetLastError();
			if (exception != null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "\r\nServer error (alien).\r\nUrl: {0}", context.Request.Url.AbsoluteUri), exception);
			}
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_End(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_End(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private void InitializeCache()
		{
			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();

			CacheUpdateCleaner.WorkerCleanInterval = new LekmerCacheUpdateCleanerSetting().CacheUpdateIntervalSeconds * 1000;
#if DEBUG
			CacheUpdateCleaner.WorkerCleanInterval = 1 * 1000; // Clean interval lowered in debug mode.
#endif
			CacheUpdateCleaner.StartWorker();
		}
	}
}