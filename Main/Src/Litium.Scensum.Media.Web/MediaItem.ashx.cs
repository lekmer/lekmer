using System;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Scensum.Media.Web
{
	public class MediaItem : IHttpHandler
	{
		private static int _bufferSizeBytes = 8 * 1024;
		private static string _lastModified = "Sat, 01 Jan 2000 00:00:00 GMT";

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var mediaId = (int) context.Items["MediaId"];
			var extension = (string) context.Items["Extension"];

			LoadMedia(context, mediaId, extension);
		}

		private static void LoadMedia(HttpContext context, int mediaId, string extension)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (string.IsNullOrEmpty(extension)) throw new ArgumentNullException("extension");

			string etag = string.Concat("\"", mediaId.ToString(CultureInfo.InvariantCulture), "\"");

			if (HasMatchingETag(context, etag))
			{
				SendNotModifiedHeader(context);
				return;
			}

			if (HasMatchingLastModified(context))
			{
				SendNotModifiedHeader(context);
				return;
			}

			var mediaItemService = IoC.Resolve<IMediaItemService>();

			string mime;
			using (Stream mediaStream = mediaItemService.LoadMedia(mediaId, extension, out mime))
			{
				if (mediaStream == null)
				{
					throw new HttpException(404, "Page not found");
				}

				context.Response.AppendHeader("ETag", etag);
				context.Response.AppendHeader("Last-Modified", _lastModified);

				context.Response.ContentType = mime;

				StreamAppender.AppendToStream(mediaStream, context.Response.OutputStream, _bufferSizeBytes);
			}
		}

		private static bool HasMatchingETag(HttpContext context, string etag)
		{
			string ifNoneMatchHeader = context.Request.Headers["If-None-Match"];
			return ifNoneMatchHeader != null && ifNoneMatchHeader.Equals(etag);
		}

		private static bool HasMatchingLastModified(HttpContext context)
		{
			string lastModifiedHeader = context.Request.Headers["If-Modified-Since"];
			return lastModifiedHeader != null && lastModifiedHeader.Equals(_lastModified);
		}

		private static void SendNotModifiedHeader(HttpContext context)
		{
			context.Response.StatusCode = 304;
			context.Response.StatusDescription = "Not modified";
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}