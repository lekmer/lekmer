﻿namespace Litium.Lekmer.Payment.Qliro
{
	public enum QliroInterestCalculationType
	{
		Flat = 1,
		Annuity = 2,
	}
}
