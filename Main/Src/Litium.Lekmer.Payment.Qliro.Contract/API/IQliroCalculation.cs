﻿namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroCalculation
	{
		decimal CalculateCheapestMonthlyInvoiceFee();
		decimal CalculateMonthlyCost(decimal sum, IQliroPaymentType paymentType);
		decimal CalculateLowestAmountToPay(decimal sum);

		decimal CalculateMonthlyCostForAnnuity(decimal sum, IQliroPaymentType paymentType);
		decimal CalculateMonthlyCostForAccount(decimal sum, IQliroPaymentType paymentType);
	}
}