﻿using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroClient
	{
		IQliroSetting QliroSetting { get; set; }

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Norway, Sweden and Denmark.
		/// </summary>
		IQliroGetAddressResult GetAddress(IChannel channel, string civicNumber, string ip, bool companyMode);

		/// <summary>
		/// Gets payment types for specific channel.
		/// </summary>
		IQliroGetPaymentTypesResult GetPaymentTypes(IChannel channel);

		/// <summary>
		/// Reserves an amount at Qliro.
		/// </summary>
		IQliroReservationResult ReserveAmount(IChannel channel, ICustomer customer, IOrderFull order, string paymentTypeId);

		IQliroGetStatusResult CheckOrderStatus(IChannel channel, int orderId, string reservationNumber);

		void Initialize(string channelCommonName);
	}
}