namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroPaymentType
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		string Code { get; set; }
		string Description { get; set; }
		decimal RegistrationFee { get; set; }
		decimal SettlementFee { get; set; }
		decimal InterestRate { get; set; }
		int InterestType { get; set; }
		QliroInterestCalculationType InterestCalculation { get; set; }
		int NoOfMonths { get; set; }
		decimal MinPurchaseAmount { get; set; }
	}
}