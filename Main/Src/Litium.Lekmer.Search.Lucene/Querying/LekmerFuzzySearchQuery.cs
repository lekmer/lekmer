using System;
using System.Globalization;
using Litium.Framework.Search;
using Litium.Framework.Search.Lucene;
using Litium.Framework.Search.Lucene.Querying;
using Litium.Lekmer.Search.Lucene.Setting;
using Lucene.Net.Index;
using Lucene.Net.Search;

namespace Litium.Lekmer.Search.Lucene.Querying
{
	public class LekmerFuzzySearchQuery : FuzzySearchQuery
	{
		protected override float MinimumSimilarity
		{
			get { return LuceneSetting.Instance.MinimumSimilarity; }
		}

		protected override int MinimumPrefixLength
		{
			get { return LuceneSetting.Instance.MinimumPrefixLength; }
		}

		public LekmerFuzzySearchQuery(SortFactory sortFactory, FilterFactory filterFactory, RangeQueryFactory rangeQueryFactory, DocumentReader documentReader)
			: base(sortFactory, filterFactory, rangeQueryFactory, documentReader)
		{
		}
		protected override Query Parse(SearchRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");
			if (request.Text == null) throw new ArgumentException("request.Text is null");

			string text = request.Text;

			string searchText = text.ToLower(CultureInfo.CurrentCulture);
			var tokens = searchText.Split(new[] { ' ' });
			var query = new BooleanQuery();
			var fuzzyReverseQuery = new BooleanQuery();
			var wildCardQuery = new BooleanQuery();
			var wildErpIdQuery = new BooleanQuery();
			var fuzzyTitleQuery = new BooleanQuery();
			var fuzzyBodyQuery = new BooleanQuery();
			foreach (var token in tokens)
			{
				if (string.IsNullOrEmpty(token))
					continue;

				string queryText;
				BooleanClause.Occur occur;
				switch (token.Substring(0, 1))
				{
					case "-":
						queryText = token.Substring(1);
						occur = BooleanClause.Occur.MUST_NOT;
						break;
					case "+":
						queryText = token.Substring(1);
						occur = BooleanClause.Occur.MUST;
						break;
					default:
						queryText = token;
						occur = BooleanClause.Occur.MUST;
						break;
				}

				//Add same query with WildcardQuery
				var fieldWildcardQuery = new Term(FieldNames.Title);
				var fieldLekmerErpId = new Term(LekmerFieldNames.LekmerErpId);
				wildCardQuery.Add(new WildcardQuery(fieldWildcardQuery.CreateTerm(queryText + "*")), BooleanClause.Occur.MUST);
				wildErpIdQuery.Add(new WildcardQuery(fieldLekmerErpId.CreateTerm(token)), BooleanClause.Occur.SHOULD);

				foreach (var targetField in DefaultTargetFields)
				{
					var fieldTermTemplate = new Term(targetField);
					if (occur == BooleanClause.Occur.MUST_NOT)
					{
						switch (targetField)
						{
							case FieldNames.Title:
								fuzzyTitleQuery.Add(new TermQuery(fieldTermTemplate.CreateTerm(queryText)), BooleanClause.Occur.MUST);
								break;
							case FieldNames.Body:
								fuzzyBodyQuery.Add(new TermQuery(fieldTermTemplate.CreateTerm(queryText)), BooleanClause.Occur.MUST);
								break;
						}
					}
					else
					{
						switch (targetField)
						{
							case FieldNames.Title:
								fuzzyTitleQuery.Add(new FuzzyQuery(fieldTermTemplate.CreateTerm(queryText), MinimumSimilarity, MinimumPrefixLength),
											   BooleanClause.Occur.MUST);
								break;
							case FieldNames.Body:
								fuzzyBodyQuery.Add(new FuzzyQuery(fieldTermTemplate.CreateTerm(queryText), MinimumSimilarity, MinimumPrefixLength),
											   BooleanClause.Occur.MUST);
								break;
						}
					}
				}

				if (occur != BooleanClause.Occur.MUST_NOT)
				{
					fuzzyReverseQuery.Add(
						new FuzzyQuery(
							new Term(
								FieldNames.TitleReversed, StringHelper.TryReverseString(queryText)), MinimumSimilarity, MinimumPrefixLength),
						BooleanClause.Occur.MUST);
				}
			}
			query.Add(wildCardQuery, BooleanClause.Occur.SHOULD);
			query.Add(wildErpIdQuery, BooleanClause.Occur.SHOULD);
			query.Add(fuzzyTitleQuery, BooleanClause.Occur.SHOULD);
			query.Add(fuzzyBodyQuery, BooleanClause.Occur.SHOULD);
			query.Add(fuzzyReverseQuery, BooleanClause.Occur.SHOULD);

			return query;
		}
	}
}