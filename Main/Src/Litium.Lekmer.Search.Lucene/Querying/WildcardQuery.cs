using System;
using System.Globalization;
using Litium.Framework.Search;
using Litium.Framework.Search.Lucene;
using Litium.Framework.Search.Lucene.Querying;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;

namespace Litium.Lekmer.Search.Lucene.Querying
{
	public class WildcardProductQuery : SearchQuery
	{
		public WildcardProductQuery(SortFactory sortFactory, FilterFactory filterFactory, RangeQueryFactory rangeQueryFactory,
		                     DocumentReader documentReader)
			: base(sortFactory, filterFactory, rangeQueryFactory, documentReader)
		{
		}

		protected override Query Parse(SearchRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");
			if (request.Text == null) throw new ArgumentException("request.Text is null");

			string text = request.Text;

			string searchText = QueryParser.Escape(text.ToLower(CultureInfo.CurrentCulture));

			string[] tokens = searchText.Split(new[] {' '});
			var query = new BooleanQuery();

			var fieldTermTemplate = new Term(FieldNames.Title);
			var fieldLekmerErpId = new Term(LekmerFieldNames.LekmerErpId);
			foreach (string token in tokens)
			{
				var fieldQuery = new BooleanQuery();
				fieldQuery.Add(
					new WildcardQuery(
						fieldTermTemplate.CreateTerm(token + "*")),
					BooleanClause.Occur.SHOULD);
				fieldQuery.Add(
					new WildcardQuery(
						fieldLekmerErpId.CreateTerm(token)),
					BooleanClause.Occur.SHOULD);
				query.Add(fieldQuery, BooleanClause.Occur.MUST);
			}

			return query;
		}
	}
	public static class LekmerFieldNames
	{
		public const string LekmerErpId = "LekmerErpId";
	}
}