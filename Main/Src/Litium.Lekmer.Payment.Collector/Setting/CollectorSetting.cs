﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Collector.Setting
{
	public class CollectorSetting : SettingBase, ICollectorSetting
	{
		private readonly string _storageName = "Collector";

		private string _groupName;

		protected override string StorageName
		{
			get { return _storageName; }
		}

		protected virtual string GroupName
		{
			get { return _groupName; }
		}


		public string Url
		{
			get { return GetString(GroupName, "Url"); }
		}

		public string UserName
		{
			get { return GetString(GroupName, "UserName"); }
		}

		public string Password
		{
			get { return GetString(GroupName, "Password"); }
		}

		public int StoreId
		{
			get { return GetInt32(GroupName, "StoreId"); }
		}

		public bool IgnoreCertificateValidation
		{
			get { return GetBoolean(GroupName, "IgnoreCertificateValidation"); }
		}

		public string FreightArticleNumber
		{
			get { return GetString(GroupName, "FreightArticleNumber"); }
		}

		public string OptionalFreightArticleNumber
		{
			get { return GetString(GroupName, "OptionalFreightArticleNumber"); }
		}

		public string DiapersFreightArticleNumber
		{
			get { return GetString(GroupName, "DiapersFreightArticleNumber"); }
		}

		public string RoundingArticleNumberPositive
		{
			get { return GetString(GroupName, "RoundingArticleNumberPositive"); }
		}
		public string RoundingArticleNumberNegative
		{
			get { return GetString(GroupName, "RoundingArticleNumberNegative"); }
		}

		public string PackageSendingMode
		{
			get { return GetString(GroupName, "PackageSendingMode"); }
		}

		public bool Use7SecRule
		{
			get { return GetBoolean(GroupName, "Use7SecRule"); }
		}

		public int DefaultResponseTimeout
		{
			get { return GetInt32(GroupName, "DefaultResponseTimeout"); }
		}

		public int Customer7SecResponseTimeout
		{
			get { return GetInt32(GroupName, "Customer7SecResponseTimeout"); }
		}

		public int Service7SecResponseTimeout
		{
			get { return GetInt32(GroupName, "Service7SecResponseTimeout"); }
		}


		public virtual void Initialize(string channelCommonName)
		{
			_groupName = channelCommonName;
		}
	}
}