﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector.Repository
{
	public class CollectorPendingOrderRepository
	{
		protected virtual DataMapperBase<ICollectorPendingOrder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICollectorPendingOrder>(dataReader);
		}

		public virtual int Insert(ICollectorPendingOrder collectorPendingOrder)
		{
			var dbSettings = new DatabaseSetting("CollectorPendingOrderRepository.Insert");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", collectorPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", collectorPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", collectorPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", collectorPendingOrder.LastAttempt, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pCollectorPendingOrderInsert]", parameters, dbSettings);
		}

		public virtual void Update(ICollectorPendingOrder collectorPendingOrder)
		{
			var dbSettings = new DatabaseSetting("CollectorPendingOrderRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CollectorPendingOrderId", collectorPendingOrder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", collectorPendingOrder.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", collectorPendingOrder.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstAttempt", collectorPendingOrder.FirstAttempt, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("LastAttempt", collectorPendingOrder.LastAttempt, SqlDbType.DateTime)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorPendingOrderUpdate]", parameters, dbSettings);
		}

		public virtual void Delete(int collectorPendingOrderId)
		{
			var dbSettings = new DatabaseSetting("CollectorPendingOrderRepository.Delete");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CollectorPendingOrderId", collectorPendingOrderId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorPendingOrderDelete]", parameters, dbSettings);
		}

		public virtual Collection<ICollectorPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			var dbSettings = new DatabaseSetting("CollectorPendingOrderRepository.GetPendingOrdersForStatusCheck");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CheckToDate", checkToDate, SqlDbType.DateTime)
				};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pCollectorPendingOrderGetPendingOrdersForStatusCheck]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
