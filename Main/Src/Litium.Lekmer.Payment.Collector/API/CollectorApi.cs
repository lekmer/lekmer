﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Web.Script.Serialization;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Lekmer.Payment.Collector.Setting;
using log4net;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorApi: ICollectorApi
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IInvoiceServiceV31 InvoiceServiceClient { get; private set; }
		protected ICollectorTransactionService CollectorTransactionService { get; private set; }
		public ICollectorSetting CollectorSetting { get; set; }

		public CollectorApi(ICollectorTransactionService collectorTransactionService)
		{
			CollectorTransactionService = collectorTransactionService;
		}

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in Sweden ?
		/// </summary>
		public virtual ICollectorGetAddressResponse GetAddress(string civicNumber, string clientIpAddress, string countryCode)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Get addresses");
			}

			var duration = new Stopwatch();
			ICollectorGetAddressResponse collectorResult = null;

			int transactionId = CollectorTransactionService.CreateGetAddress(CollectorSetting.StoreId, civicNumber);

			try
			{
				CreateClient();

				var getAddressRequest = new GetAddressRequest{
					Username = CollectorSetting.UserName,
					Password = CollectorSetting.Password,
					StoreId = CollectorSetting.StoreId,
					CorrelationId = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture),
					CountryCode = countryCode,
					RegNo = civicNumber,
					ClientIpAddress = clientIpAddress
				};

				duration.Start();
				GetAddressResponse getAddressResponse = InvoiceServiceClient.GetAddress(getAddressRequest);
				duration.Stop();

				collectorResult = new CollectorGetAddressResponse
				{
					StatusCode = (int)CollectorTransactionStatus.Ok,
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return BuildCollectorGetAddressResponse(collectorResult, getAddressResponse);
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Collector call returned exception on get address for '{0}'.", civicNumber);

				LogException(logMessage, ex);

				collectorResult = new CollectorGetAddressResponse
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleCollectorException(ex, collectorResult);

				return collectorResult;
			}
			finally
			{
				CollectorTransactionService.SaveResult(collectorResult);
			}
		}

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		public virtual ICollectorAddInvoiceResult AddInvoice(
			string clientIpAddress,
			string countryCode,
			string currencyCode,
			int customerId,
			Address deliveryAddress,
			CollectorGender gender,
			Address invoiceAddress,
			InvoiceRow[] invoiceRows,
			CollectorInvoiceType invoiceType,
			DateTime orderDate,
			int orderId,
			string productCode,
			string companyReference,
			string civicNumber
			)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Reserve amount");
			}

			var duration = new Stopwatch();
			ICollectorAddInvoiceResult collectorResult = null;

			int transactionId = CollectorTransactionService.CreateAddInvoice(
				CollectorSetting.StoreId,
				civicNumber,
				orderId,
				productCode
			);

			CollectorInvoiceDeliveryMethod invoiceDeliveryMethod = GetCollectorInvoiceDeliveryMethod(invoiceType);
			string productCodeForCollector = GetProductCodeForCollector(invoiceType, productCode);

			try
			{
				CreateClient();

				var addInvoiceRequest = new AddInvoiceRequest{
					Username = CollectorSetting.UserName,
					Password = CollectorSetting.Password,
					StoreId = CollectorSetting.StoreId,
					CorrelationId = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture),
					CountryCode = countryCode,
					RegNo = civicNumber,
					ClientIpAddress = clientIpAddress,
					Currency = currencyCode,
					CustomerNo = customerId.ToString(CultureInfo.InvariantCulture),
					OrderNo = orderId.ToString(CultureInfo.InvariantCulture),
					OrderDate = orderDate,
					InvoiceRows = invoiceRows,
					InvoiceType = (int)invoiceType,
					InvoiceAddress = invoiceAddress,
					DeliveryAddress = deliveryAddress,
					CreditTime = null, //Not used at the moment.
					ActivationOption = (int)CollectorActivationOption.Default,
					Reference = companyReference, // Only for companies.
					CostCenter = null, // For companies that needs to specify a cost center.
					Gender = (int)CollectorGender.Unknown,
					InvoiceDeliveryMethod = (int)invoiceDeliveryMethod,
					ProductCode = productCodeForCollector,
					PurchaseType = (int)CollectorPurchaseType.ECommerce,
					SalesPerson = null
				};

				duration.Start();
				AddInvoiceResponseV31 addInvoiceResponse = InvoiceServiceClient.AddInvoice(addInvoiceRequest);
				duration.Stop();

				_log.InfoFormat("Collector reservation made!. OrderId: {0}. InvoiceNo: {1}", orderId, addInvoiceResponse.InvoiceNo);

				collectorResult = new CollectorAddInvoiceResult()
				{
					StatusCode = (int)CollectorTransactionStatus.Ok,
					AvailableReservationAmount = addInvoiceResponse.AvailableReservationAmount,
					InvoiceNo = addInvoiceResponse.InvoiceNo,
					InvoiceStatus = addInvoiceResponse.InvoiceStatus,
					InvoiceUrl = addInvoiceResponse.InvoiceUrl,
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				return collectorResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Collector call returned exception on reserve amount. OrderId: {0}", orderId);

				LogException(logMessage, ex);

				collectorResult = new CollectorAddInvoiceResult
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleCollectorException(ex, collectorResult);

				return collectorResult;
			}
			finally
			{
				CollectorTransactionService.SaveAddInvoiceResponse(collectorResult);
			}
		}

		/// <summary>
		/// Reserves a purchased amount to a particular customer.
		/// </summary>
		public virtual ICollectorAddInvoiceResult AddInvoiceTimeout(
			string clientIpAddress,
			string countryCode,
			string currencyCode,
			int customerId,
			Address deliveryAddress,
			CollectorGender gender,
			Address invoiceAddress,
			InvoiceRow[] invoiceRows,
			CollectorInvoiceType invoiceType,
			DateTime orderDate,
			int orderId,
			string productCode,
			string companyReference,
			string civicNumber
			)
		{
			if (_log.IsDebugEnabled)
			{
				_log.Debug("Reserve amount");
			}

			var duration = new Stopwatch();
			ICollectorAddInvoiceResult collectorResult = null;

			int transactionId = CollectorTransactionService.CreateAddInvoice(
				CollectorSetting.StoreId,
				civicNumber,
				orderId,
				productCode
			);

			CollectorInvoiceDeliveryMethod invoiceDeliveryMethod = GetCollectorInvoiceDeliveryMethod(invoiceType);
			string productCodeForCollector = GetProductCodeForCollector(invoiceType, productCode);

			try
			{
				CreateClient();

				var addInvoiceRequest = new AddInvoiceRequest
				{
					Username = CollectorSetting.UserName,
					Password = CollectorSetting.Password,
					StoreId = CollectorSetting.StoreId,
					CorrelationId = DateTime.Now.Ticks.ToString(CultureInfo.InvariantCulture),
					CountryCode = countryCode,
					RegNo = civicNumber,
					ClientIpAddress = clientIpAddress,
					Currency = currencyCode,
					CustomerNo = customerId.ToString(CultureInfo.InvariantCulture),
					OrderNo = orderId.ToString(CultureInfo.InvariantCulture),
					OrderDate = orderDate,
					InvoiceRows = invoiceRows,
					InvoiceType = (int)invoiceType,
					InvoiceAddress = invoiceAddress,
					DeliveryAddress = deliveryAddress,
					CreditTime = null, //Not used at the moment.
					ActivationOption = (int)CollectorActivationOption.Default,
					Reference = companyReference, // Only for companies.
					CostCenter = null, // For companies that needs to specify a cost center.
					Gender = (int)CollectorGender.Unknown,
					InvoiceDeliveryMethod = (int)invoiceDeliveryMethod,
					ProductCode = productCodeForCollector,
					PurchaseType = (int)CollectorPurchaseType.ECommerce,
					SalesPerson = null
				};

				var request = new CollectorAddInvoiceRequest
					{
						Request = addInvoiceRequest,
						CollectorSetting = CollectorSetting,
						OrderId = orderId,
						ProductCode = productCode,
						InvoiceServiceClient = InvoiceServiceClient
					};

				var collectorManager = new CollectorManager(CollectorTransactionService) { AddInvoiceRequest = request };

				duration.Start();
				var collectorAddInvoiceResponse = collectorManager.ExecuteAddInvoice();
				duration.Stop();

				if (collectorAddInvoiceResponse == null)
				{
					collectorResult = new CollectorAddInvoiceResult
					{
						StatusCode = (int)CollectorTransactionStatus.TimeoutResponse,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};
				}
				else if (collectorAddInvoiceResponse.Response != null)
				{
					var addInvoiceResponse = collectorAddInvoiceResponse.Response;

					_log.InfoFormat("Collector reservation made!. OrderId: {0}. InvoiceNo: {1}", orderId, addInvoiceResponse.InvoiceNo);

					collectorResult = new CollectorAddInvoiceResult
					{
						StatusCode = (int)CollectorTransactionStatus.Ok,
						AvailableReservationAmount = addInvoiceResponse.AvailableReservationAmount,
						InvoiceNo = addInvoiceResponse.InvoiceNo,
						InvoiceStatus = addInvoiceResponse.InvoiceStatus,
						InvoiceUrl = addInvoiceResponse.InvoiceUrl,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};
				}
				else // Exception
				{
					Exception exception = collectorAddInvoiceResponse.AddInvoiceException;
					
					string logMessage = string.Format("Collector call returned exception on reserve amount. OrderId: {0}", orderId);

					LogException(logMessage, exception);

					collectorResult = new CollectorAddInvoiceResult
					{
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};

					TryHandleCollectorException(exception, collectorResult);
				}

				return collectorResult;
			}
			catch (Exception ex)
			{
				duration.Stop();

				string logMessage = string.Format("Collector call returned exception on reserve amount. OrderId: {0}", orderId);

				LogException(logMessage, ex);

				collectorResult = new CollectorAddInvoiceResult
				{
					TransactionId = transactionId,
					Duration = duration.ElapsedMilliseconds
				};

				TryHandleCollectorException(ex, collectorResult);

				return collectorResult;
			}
			finally
			{
				CollectorTransactionService.SaveAddInvoiceResponse(collectorResult);
			}
		}

		public virtual void Initialize(string channelCommonName)
		{
			CollectorSetting.Initialize(channelCommonName);
		}

		protected virtual void CreateClient()
		{
			if (InvoiceServiceClient != null) return;

			var invoiceServiceClient = new InvoiceServiceV31Client();
			invoiceServiceClient.Endpoint.Address = new EndpointAddress(CollectorSetting.Url);

			InvoiceServiceClient = invoiceServiceClient;

			((InvoiceServiceV31Client)InvoiceServiceClient).InnerChannel.OperationTimeout = TimeSpan.FromMilliseconds(CollectorSetting.DefaultResponseTimeout);

			if (CollectorSetting.IgnoreCertificateValidation)
			{
				ServicePointManager.ServerCertificateValidationCallback = ValidateServerCertificate;
			}
		}

		private void LogException(string message, Exception ex)
		{
			if (ex is FaultException)
			{
				_log.Warn(message, ex);
			}
			else
			{
				_log.Error(message, ex);
			}
		}

		private void TryHandleCollectorException<T>(Exception ex, T collectorResult) where T : ICollectorResult
		{
			collectorResult.ErrorMessage = ex.ToString();

			if (ex is FaultException)
			{
				var faultException = (FaultException) ex;

				collectorResult.FaultCode = faultException.Code.Name;
				collectorResult.FaultMessage = faultException.Message;

				// SOAP fault
				collectorResult.StatusCode = (int)CollectorTransactionStatus.SoapFault;
			}
			else
			{
				// Unspecified error
				collectorResult.StatusCode = (int)CollectorTransactionStatus.UnspecifiedError;
			}
		}

		/// <summary>
		/// Builds a ICollectorResponseItem from a Collector call result.
		/// </summary>
		private static ICollectorGetAddressResponse BuildCollectorGetAddressResponse(ICollectorGetAddressResponse collectorGetAddressResponse, GetAddressResponse getAddressResponse)
		{
			try
			{
				collectorGetAddressResponse.FirstName = getAddressResponse.Firstname;
				collectorGetAddressResponse.LastName = getAddressResponse.Lastname;
				collectorGetAddressResponse.CivicNumber = getAddressResponse.RegNo;

				if (getAddressResponse.Addresses != null && getAddressResponse.Addresses.Length > 0)
				{
					collectorGetAddressResponse.Addresses = getAddressResponse.Addresses;
				}
			}
			catch (Exception)
			{
				_log.Error("Collector call returned an invalid format in BuildCollectorGetAddressResponse.");
			}

			try
			{
				if (getAddressResponse != null)
				{
					var serializer = new JavaScriptSerializer();
					string json = serializer.Serialize(getAddressResponse);
					collectorGetAddressResponse.ResponseContent = json;
				}
			}
			catch (Exception)
			{
				_log.Error("Json serialization fails in BuildCollectorGetAddressResponse.");
			}

			return collectorGetAddressResponse;
		}

		private static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			if (sslPolicyErrors == SslPolicyErrors.None)
			{
				return true;
			}

			return true;
		}

		private CollectorInvoiceDeliveryMethod GetCollectorInvoiceDeliveryMethod(CollectorInvoiceType invoiceType)
		{
			switch (invoiceType)
			{
				case CollectorInvoiceType.Default:
					return CollectorInvoiceDeliveryMethod.Email;
				case CollectorInvoiceType.AggregatedInvoice:
				case CollectorInvoiceType.InterestFreeInvoice:
				case CollectorInvoiceType.AnnuityInvoice:
					return CollectorInvoiceDeliveryMethod.NormalMailOrInPackage;
			}

			return CollectorInvoiceDeliveryMethod.NormalMailOrInPackage;
		}

		private string GetProductCodeForCollector(CollectorInvoiceType invoiceType, string productCode)
		{
			if (invoiceType == CollectorInvoiceType.AggregatedInvoice) // Konto
			{
				return string.Empty;
			}

			return productCode;
		}
	}
}