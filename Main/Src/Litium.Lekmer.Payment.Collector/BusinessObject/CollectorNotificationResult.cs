﻿namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorNotificationResult : CollectorResult, ICollectorNotificationResult
	{
		public string InvoiceNo { get; set; }
		public int OrderNo { get; set; }
		public int InvoiceStatus { get; set; }
	}
}