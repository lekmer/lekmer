namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorPaymentType : ICollectorPaymentType
	{
		public int Id { get; set; }
		public int ChannelId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }

		public decimal StartFee { get; set; }
		public decimal InvoiceFee { get; set; }
		public decimal MonthlyInterestRate { get; set; }

		public int NoOfMonths { get; set; }
		public decimal MinPurchaseAmount { get; set; }
		public decimal LowestPayment { get; set; }

		public CollectorPartPaymentType PartPaymentType { get; set; }
		public CollectorInvoiceType InvoiceType { get; set; }

		public int Ordinal { get; set; }
	}
}