﻿using System;
using System.Reflection;
using Litium.Lekmer.Payment.Collector.Repository;
using log4net;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorTransactionService : ICollectorTransactionService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected CollectorTransactionRepository Repository { get; private set; }

		public CollectorTransactionService(CollectorTransactionRepository repository)
		{
			Repository = repository;
		}


		public virtual int CreateGetAddress(int storeId, string civicNumber)
		{
			try
			{
				return Repository.CreateGetAddress(storeId, (int)CollectorTransactionType.GetAddress, civicNumber, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateAddInvoice(int storeId, string civicNumber, int orderId, string productCode)
		{
			try
			{
				return Repository.CreateAddInvoice(storeId, (int)CollectorTransactionType.AddInvoice, civicNumber, orderId, productCode, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateAddInvoiceTimeout(int storeId, string civicNumber, int orderId, string productCode)
		{
			try
			{
				return Repository.CreateAddInvoice(storeId, (int)CollectorTransactionType.AddInvoiceTimeout, civicNumber, orderId, productCode, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateInvoiceNotification(int storeId, string queryString)
		{
			try
			{
				queryString = queryString.Length > 100 ? queryString.Substring(0, 100) : queryString; // limit string length
				return Repository.CreateInvoiceNotification(storeId, (int)CollectorTransactionType.InvoiceNotification, queryString, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}


		public virtual void SaveResult(ICollectorResult collectorResult)
		{
			try
			{
				if (collectorResult.TransactionId > 0)
				{
					Repository.SaveResult(collectorResult);
				}
				else
				{
					_log.Error("SaveResult: CollectorTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		public virtual void SaveAddInvoiceResponse(ICollectorAddInvoiceResult collectorAddInvoiceResult)
		{
			try
			{
				if (collectorAddInvoiceResult.TransactionId > 0)
				{
					Repository.SaveAddInvoiceResponse(collectorAddInvoiceResult);
				}
				else
				{
					_log.Error("SaveAddInvoiceResponse: CollectorTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		public virtual void SaveInvoiceNotificationResult(ICollectorNotificationResult collectorNotificationResult)
		{
			try
			{
				if (collectorNotificationResult.TransactionId > 0)
				{
					Repository.SaveInvoiceNotificationResult(collectorNotificationResult);
				}
				else
				{
					_log.Error("SaveInvoiceNotificationResult: CollectorTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}
	}
}
