﻿using System;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	[Serializable]
	public class LekmerChannel : Channel, ILekmerChannel
	{
		private IChannelFormatInfo _formatInfo;
		private string _erpId;
		private decimal? _vatPercentage;

		public IChannelFormatInfo FormatInfo
		{
			get { return _formatInfo; }
			set
			{
				CheckChanged(_formatInfo, value);
				_formatInfo = value;
			}
		}

		public string ErpId
		{
			get { return _erpId; }
			set
			{
				CheckChanged(_erpId, value);
				_erpId = value;
			}
		}

		public decimal? VatPercentage
		{
			get { return _vatPercentage; }
			set
			{
				CheckChanged(_vatPercentage, value);
				_vatPercentage = value;
			}
		}
	}
}
