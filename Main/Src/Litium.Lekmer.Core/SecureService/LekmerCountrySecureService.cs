﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Repository;

namespace Litium.Lekmer.Core
{
	public class LekmerCountrySecureService : CountrySecureService, ILekmerCountrySecureService
	{
		public LekmerCountrySecureService(CountryRepository countryRepository)
			: base(countryRepository)
		{
		}

		public virtual Collection<ICountry> GetAll(string countryIdsToIgnore)
		{
			Collection<ICountry> countries = Repository.GetAll();

			if (countryIdsToIgnore.IsEmpty())
			{
				return countries;
			}

			List<int> idsToIgnore = countryIdsToIgnore.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

			var countriesFiltered = new Collection<ICountry>();

			foreach (ICountry country in countries)
			{
				if (idsToIgnore.Contains(country.Id))
				{
					continue;
				}

				countriesFiltered.Add(country);
			}

			return countriesFiltered;
		}
	}
}
