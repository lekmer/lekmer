﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Repository;

namespace Litium.Lekmer.Core
{
	public class LekmerCurrencySecureService : CurrencySecureService, ILekmerCurrencySecureService
	{
		public LekmerCurrencySecureService(CurrencyRepository currencyRepository)
			: base(currencyRepository)
		{
		}

		public virtual Collection<ICurrency> GetAll(string currencyIdsToIgnore)
		{
			Collection<ICurrency> currencies = Repository.GetAll();

			if (currencyIdsToIgnore.IsEmpty())
			{
				return currencies;
			}

			List<int> idsToIgnore = currencyIdsToIgnore.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

			var currenciesFiltered = new Collection<ICurrency>();

			foreach (ICurrency currency in currencies)
			{
				if (idsToIgnore.Contains(currency.Id))
				{
					continue;
				}

				currenciesFiltered.Add(currency);
			}

			return currenciesFiltered;
		}
	}
}
