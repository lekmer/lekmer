﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Core.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core.Repository
{
	public class LekmerChannelRepository : ChannelRepository
	{
		/// <summary>
		/// Saves a ChannelFormatInfo to Db
		/// </summary>
		public virtual int SaveFormatInfo(IChannelFormatInfo channelFormatInfo)
		{
			if (channelFormatInfo == null) throw new ArgumentNullException("channelFormatInfo");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelFormatInfo.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@TimeFormat", channelFormatInfo.TimeFormat, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@WeekDayFormat", channelFormatInfo.WeekDayFormat, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@DayFormat", channelFormatInfo.DayFormat, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@DateTimeFormat", channelFormatInfo.DateTimeFormat, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@TimeZoneDiff", channelFormatInfo.TimeZoneDiff, SqlDbType.Int),
				};

			DatabaseSetting dbSettings = new DatabaseSetting("LekmerChannelRepository.SaveFormatInfo");
			channelFormatInfo.ChannelId = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pLekmerChannelSave]", parameters, dbSettings);
			return channelFormatInfo.ChannelId;
		}
	}
}
