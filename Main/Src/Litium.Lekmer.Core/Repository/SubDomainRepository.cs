﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core.Repository
{
	public class SubDomainRepository
	{
		protected virtual DataMapperBase<ISubDomain> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISubDomain>(dataReader);
		}

		public virtual Collection<ISubDomain> GetAllByChannelAndContentType(IChannel channel, int contentTypeId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("ContentTypeId", contentTypeId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SubDomainRepository.GetAllByContentType");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[corelek].[pSubDomainGetAllByChannelAndContentType]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<ISubDomain> GetAllByContentType(int contentTypeId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ContentTypeId", contentTypeId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("SubDomainRepository.GetAllByContentType");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[corelek].[pSubDomainGetAllByContentType]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
