﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Core.Cache
{
	public sealed class SubDomainCache : ScensumCacheBase<SubDomainKey, Collection<ISubDomain>>
	{
		private static readonly SubDomainCache _instance = new SubDomainCache();

		private SubDomainCache()
		{
		}

		public static SubDomainCache Instance
		{
			get { return _instance; }
		}
	}

	public class SubDomainKey : ICacheKey
	{
		public SubDomainKey(int channelId, int contentTypeId)
		{
			ChannelId = channelId;
			ContentTypeId = contentTypeId;
		}

		public int ChannelId { get; set; }

		public int ContentTypeId { get; set; }

		public string Key
		{
			get { return string.Concat(ChannelId.ToString(CultureInfo.InvariantCulture), "-", ContentTypeId.ToString(CultureInfo.InvariantCulture)); }
		}
	}
}