﻿using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web.BlockContactUs
{
	public class BlockContactUsEntityMapper : LekmerBlockEntityMapper<IBlockContactUs>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockContactUs item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.ReceiverEmail", item.Receiver, VariableEncoding.None);
		}
	}
}