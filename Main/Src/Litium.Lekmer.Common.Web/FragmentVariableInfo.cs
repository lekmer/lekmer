﻿using System;
using System.Reflection;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web
{
	public class FragmentVariableInfo
	{
		private static FieldInfo _contentFieldInfo;
		private readonly string _fragmentContent;

		public FragmentVariableInfo(Fragment fragmentItem)
		{
			if (fragmentItem == null)
			{
				throw new ArgumentNullException("fragmentItem");
			}

			_fragmentContent = fragmentItem.Render();
		}

		public FragmentVariableInfo(Fragment fragmentItem, bool readPrivateContent)
		{
			if (fragmentItem == null)
			{
				throw new ArgumentNullException("fragmentItem");
			}

			_fragmentContent = readPrivateContent ? GetFragmentPrivateContent(fragmentItem) : fragmentItem.Render();
		}

		public virtual bool HasVariable(string variableName)
		{
			return _fragmentContent.Contains("[" + variableName + "]");
		}

		public virtual bool HasRegexVariable(string variableName)
		{
			return _fragmentContent.Contains("[" + variableName + "(");
		}

		public virtual bool HasCondition(string conditionName)
		{
			return _fragmentContent.Contains(" " + conditionName + "]");
		}

		// Render() could be heavy, so better to get inner "_content" field instead.
		private static string GetFragmentPrivateContent(Fragment fragmentItem)
		{
			FieldInfo field = _contentFieldInfo ?? (_contentFieldInfo = typeof(Fragment).GetField("_content", BindingFlags.Instance | BindingFlags.NonPublic));

			if (field != null)
			{
				return (string)field.GetValue(fragmentItem);
			}

			return fragmentItem.Render();
		}
	}
}
