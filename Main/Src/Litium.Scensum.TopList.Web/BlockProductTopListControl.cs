﻿using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.TopList.Web
{
	public class BlockProductTopListControl : BlockControlBase<IBlockTopList>
	{
		private readonly IBlockTopListService _blockTopListService;
		private readonly ITopListProductService _topListProductService;
		private IPagingControl _pagingControl;
		private ProductCollection _products;

		public BlockProductTopListControl(
			ITemplateFactory templateFactory,
			IBlockTopListService blockTopListService,
			ITopListProductService topListProductService)
			: base(templateFactory)
		{
			_blockTopListService = blockTopListService;
			_topListProductService = topListProductService;
		}

		protected override IBlockTopList GetBlockById(int blockId)
		{
			return _blockTopListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			_pagingControl.TotalCount = _products.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Block.Title", Block.Title, VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			var productIds = RenderProductIdsArrayParam();
			string head = RenderFragment("Head", productIds, pagingContent);
			string footer = RenderFragment("Footer", productIds, pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private void Initialize()
		{
			_pagingControl = CreatePagingControl();
			_products = _topListProductService.GetAllByBlock(UserContext.Current, Block, _pagingControl.SelectedPage, _pagingControl.PageSize);
		}

		private string RenderFragment(string fragmentName, string productIds, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", productIds);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};
			return grid.Render();
		}

		private IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}
		private string RenderProductIdsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			for (int i = 0; i < _products.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _products[i].Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", _products[i].Id));
				}
			}
			return itemBuilder.ToString();
		}
	}
}