﻿namespace Litium.Lekmer.Oculus
{
	public interface IProductSimilarSecureService
	{
		IProductSimilar Save(IProductSimilar productSimilar);
	}
}