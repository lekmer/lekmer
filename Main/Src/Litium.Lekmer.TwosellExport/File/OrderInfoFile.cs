﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.TwosellExport.Contract;

namespace Litium.Lekmer.TwosellExport
{
	public class OrderInfoFile
	{
		private readonly string _priceFormat = "0.00";

		private ITwosellExportSetting _setting;
		private string _delimiter;
		private Collection<IOrderInfo> _orderInfoCollection;

		private bool _isInitialized;

		public void Initialize(ITwosellExportSetting setting, Collection<IOrderInfo> orderInfoCollection)
		{
			if (setting == null)
			{
				throw new ArgumentNullException("setting");
			}

			_setting = setting;
			_delimiter = _setting.OrderInfoFileDelimiter;
			_orderInfoCollection = orderInfoCollection;

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) and should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap);

			// write field titles.
			writer.WriteLine(AddHeaderRow());

			foreach (var orderInfo in _orderInfoCollection)
			{
				writer.WriteLine(AddRow(orderInfo));
			}

			writer.Flush();
			writer.Close();
		}

		protected void AddValue(ref string row, string value)
		{
			row += string.Format("{0}{1}", value, _delimiter);
		}

		protected string AddHeaderRow()
		{
			string headerRow = string.Empty;

			foreach (FieldNames type in Enum.GetValues(typeof(FieldNames)))
			{
				AddValue(ref headerRow, type.GetEnumDescription());
			}

			headerRow = RemoveLastDelimiter(headerRow);

			return headerRow;
		}

		protected string AddRow(IOrderInfo orderInfo)
		{
			string row = string.Empty;

			// Date
			AddValue(ref row, orderInfo.Date.ToString("yyyy-MM-dd HH:mm:ss"));
			// OrderId
			AddValue(ref row, orderInfo.OrderId.ToString(CultureInfo.InvariantCulture));
			// ProductId
			AddValue(ref row, orderInfo.ProductId.ToString(CultureInfo.InvariantCulture));
			// ErpId
			AddValue(ref row, orderInfo.ErpId);
			// Price
			AddValue(ref row, orderInfo.Price.ToString(_priceFormat, CultureInfo.InvariantCulture));
			// CustomerIp
			AddValue(ref row, orderInfo.CustomerIp);
			// Email
			AddValue(ref row, orderInfo.Email);
			// SessionId
			AddValue(ref row, orderInfo.LekmerId.Trim());

			row = RemoveLastDelimiter(row);

			return row;
		}

		private string RemoveLastDelimiter(string row)
		{
			int lastDelimiterPosition = row.LastIndexOf(_delimiter);
			if (lastDelimiterPosition > 0)
			{
				row = row.Substring(0, lastDelimiterPosition);
			}
			return row;
		}
	}
}