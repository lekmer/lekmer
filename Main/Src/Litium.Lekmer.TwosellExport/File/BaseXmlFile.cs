﻿using System.Globalization;
using System.Xml;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.TwosellExport
{
	public class BaseXmlFile
	{
		protected virtual XmlDocument CreateXmlDocument()
		{
			var doc = new XmlDocument();

			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);

			return doc;
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, int elementValue)
		{
			return AddNodeRaw(xmlDocument, parentNode, elementName, elementValue.ToString(CultureInfo.InvariantCulture));
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName)
		{
			XmlNode node = xmlDocument.CreateElement(elementName);
			parentNode.AppendChild(node);
			return node;
		}

		protected virtual XmlNode AddNodeRaw(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlNode node = xmlDocument.CreateElement(elementName);
			node.AppendChild(xmlDocument.CreateTextNode(elementValue));
			parentNode.AppendChild(node);
			return node;
		}

		protected virtual XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			return AddNodeRaw(xmlDocument, parentNode, elementName, elementValue.StripIllegalXmlCharacters());
		}

		protected virtual void AddAttribute(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlAttribute attribute = xmlDocument.CreateAttribute(elementName);
			attribute.Value = elementValue;
			if (parentNode.Attributes != null)
			{
				parentNode.Attributes.Append(attribute);
			}
		}
	}
}
