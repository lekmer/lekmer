﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using log4net;

namespace Litium.Lekmer.TwosellExport
{
	public class TwosellExportService : ITwosellExportService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private ICategoryTree _categoryTree;
		private Dictionary<int, Dictionary<int, IBrand>> _brands; // brand_id -> channel_id -> brand
		private IProductTagHash _productTagHash;

		protected ITwosellExportSetting Setting { get; private set; }
		protected TwosellExportRepository Repository { get; private set; }
		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected IProductImageGroupService ProductImageGroupService { get; private set; }
		protected IImageSizeSecureService ImageSizeService { get; private set; }
		protected ITagGroupSecureService TagGroupSecureService { get; private set; }
		protected ICampaignSecureService CampaignService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }
		protected IBrandService BrandService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected ITagService TagService { get; private set; }
		protected IProductTagService ProductTagService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected IContentNodeService ContentNodeService { get; private set; }

		public TwosellExportService(ITwosellExportSetting setting, TwosellExportRepository repository, ILekmerChannelService channelService,
			IProductImageGroupService productImageGroupService, IImageSizeSecureService imageSizeService, ITagGroupSecureService tagGroupSecureService,
			ICampaignSecureService campaignService, ICategoryService categoryService, ILekmerProductService productService, IBrandService brandService,
			ITagGroupService tagGroupService, ITagService tagService, IProductTagService productTagService, IProductSizeService productSizeService,
			IContentNodeService contentNodeService)
		{
			Setting = setting;
			Repository = repository;
			LekmerChannelService = channelService;
			ProductImageGroupService = productImageGroupService;
			ImageSizeService = imageSizeService;
			TagGroupSecureService = tagGroupSecureService;
			CampaignService = campaignService;
			CategoryService = categoryService;
			LekmerProductService = productService;
			BrandService = brandService;
			TagGroupService = tagGroupService;
			TagService = tagService;
			ProductTagService = productTagService;
			ProductSizeService = productSizeService;
			ContentNodeService = contentNodeService;
		}

		public string GetOrderInfoFilePath()
		{
			string dirr = Setting.DestinationDirectory;
			string fileName = string.Format(Setting.OrderInfoFileName, DateTime.Now.AddDays(-Setting.DaysAfterPurchase).ToString("yyyy-MM-dd-HHTmm"));
			return Path.Combine(dirr, fileName);
		}
		public Collection<IOrderInfo> GetOrderInfoCollectionToExport()
		{
			return Repository.GetAllOrderInfoByDate(Setting.DaysAfterPurchase);
		}

		public virtual string GetProductInfoFilePath(IUserContext context)
		{
			string dirr = Setting.DestinationDirectory;
			string name = string.Format(Setting.ProductInfoFileName.Replace("[ChannelName]", Setting.GetChannelNameReplacement(context.Channel.CommonName)), DateTime.Now.ToString("yyyy-MM-dd-HHTmm"));
			return Path.Combine(dirr, name);
		}
		public virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}
		public virtual Collection<IProductImageGroup> GetAllowedImageGroups()
		{
			ICollection<IProductImageGroup> allGroups = ProductImageGroupService.GetAll();
			if (allGroups == null)
			{
				return null;
			}

			ICollection<string> groups = Setting.ImageGroups;
			if (groups == null)
			{
				return null;
			}

			var allowedGroups = new Collection<IProductImageGroup>();
			var allowedGroupsDic = allowedGroups.ToDictionary(s => s.CommonName);
			var allGroupsDic = allGroups.ToDictionary(s => s.CommonName);

			foreach (string groupCommonName in groups)
			{
				if (!allowedGroupsDic.ContainsKey(groupCommonName))
				{
					if (allGroupsDic.ContainsKey(groupCommonName))
					{
						IProductImageGroup imageGroup = allGroupsDic[groupCommonName];
						if (imageGroup != null)
						{
							allowedGroups.Add(imageGroup);
							allowedGroupsDic[groupCommonName] = imageGroup;
						}
					}
				}
			}

			return allowedGroups;
		}
		public virtual Collection<IImageSize> GetAllowedImageSizes()
		{
			ICollection<IImageSize> allImageSizes = ImageSizeService.GetAll();
			if (allImageSizes == null)
			{
				return null;
			}

			ICollection<string> sizes = Setting.ImageSizes;
			if (sizes == null)
			{
				return null;
			}

			var allowedSizes = new Collection<IImageSize>();
			var allowedSizesDic = allowedSizes.ToDictionary(s => s.CommonName);
			var allImageSizesDic = allImageSizes.ToDictionary(s => s.CommonName);

			foreach (string sizeCommonName in sizes)
			{
				if (!allowedSizesDic.ContainsKey(sizeCommonName))
				{
					if (allImageSizesDic.ContainsKey(sizeCommonName))
					{
						IImageSize imageSize = allImageSizesDic[sizeCommonName];
						if (imageSize != null)
						{
							allowedSizes.Add(imageSize);
							allowedSizesDic[sizeCommonName] = imageSize;
						}
					}
				}
			}

			return allowedSizes;
		}
		public virtual Collection<ITagGroup> GetAllowedTagGroups()
		{
			ICollection<ITagGroup> allTagGroups = TagGroupSecureService.GetAll();
			if (allTagGroups == null)
			{
				return null;
			}

			ICollection<string> tagGroups = Setting.ColorTagGroup;
			if (tagGroups == null)
			{
				return null;
			}

			var allowedGroups = new Collection<ITagGroup>();
			var allowedGroupsDic = allowedGroups.ToDictionary(s => s.CommonName);
			var allGroupsDic = allTagGroups.ToDictionary(s => s.CommonName);

			foreach (string groupCommonName in tagGroups)
			{
				if (!allowedGroupsDic.ContainsKey(groupCommonName))
				{
					if (allGroupsDic.ContainsKey(groupCommonName))
					{
						ITagGroup tagGroup = allGroupsDic[groupCommonName];
						if (tagGroup != null)
						{
							allowedGroups.Add(tagGroup);
							allowedGroupsDic[groupCommonName] = tagGroup;
						}
					}
				}
			}

			return allowedGroups;
		}
		public virtual Dictionary<string, string> GetAllowedCategoryLevelUrls()
		{
			ICollection<string> categoryLevelUrls = Setting.CategoryLevelUrl;
			return categoryLevelUrls == null ? null : categoryLevelUrls.ToDictionary(c => c);
		}
		public virtual Collection<ICampaign> GetCampaigns()
		{
			return CampaignService.GetAllProductCampaigns();
		}
		public virtual Collection<IProductInfo> GetProductInfoCollectionToExport(IUserContext context)
		{
			_log.InfoFormat("Reading product data started. //{0}", context.Channel.CommonName);

			_categoryTree = CategoryService.GetAllAsTree(context);

			var productInfoCollection = new Collection<IProductInfo>();

			var pageInfo = IoC.Resolve<IPageTracker>().Initialize(Setting.PageSize, 1);

			_log.Info("Reading product data  - all / all.");

			while (pageInfo.HasNext)
			{
				ProductCollection products = LekmerProductService.GetViewAllForExport(context, pageInfo.Current, pageInfo.PageSize);

				Collection<IProductInfo> productsExportInfo = PopulateProductsInfo(context, products);

				AddToCollection(productInfoCollection, productsExportInfo);

				pageInfo.MoveToNext(products.TotalCount);

				_log.InfoFormat("Reading product data  - {0} / {1}.", pageInfo.ItemsLeft, pageInfo.ItemsTotalCount);
			}

			_log.InfoFormat("Reading product data done. //{0}", context.Channel.CommonName);

			return productInfoCollection;
		}

		protected virtual Collection<IProductInfo> PopulateProductsInfo(IUserContext context, ProductCollection productsBatch)
		{
			var products = new Collection<IProductInfo>();

			PreloadAllBrands(context);
			PreloadAllTags(context);

			foreach (ILekmerProductView product in productsBatch)
			{
				var productExportInfo = IoC.Resolve<IProductInfo>();

				productExportInfo.Product = product;

				// Brand
				SupplementWithBrandInfo(context, productExportInfo);
				// Categories
				SupplementWithCategoryInfo(context, productExportInfo);
				// Images
				productExportInfo.ImageGroups = ProductImageGroupService.GetAllFullByProduct(product.Id);
				// Tags
				SupplementWithTagsInfo(context, product);

				products.Add(productExportInfo);
			}

			//Sizes
			SupplementWithSizeInfo(productsBatch);

			return products;
		}
		protected virtual void PreloadAllBrands(IUserContext userContext)
		{
			if (_brands == null)
			{
				_brands = new Dictionary<int, Dictionary<int, IBrand>>();
			}

			if (_brands.ContainsKey(userContext.Channel.Id))
			{
				return;
			}

			BrandCollection brandCollection = BrandService.GetAll(userContext);

			var channelBrands = new Dictionary<int, IBrand>();
			_brands[userContext.Channel.Id] = channelBrands;

			foreach (IBrand brand in brandCollection)
			{
				channelBrands[brand.Id] = brand;
			}
		}
		protected virtual void PreloadAllTags(IUserContext userContext)
		{
			if (_productTagHash == null)
			{
				_productTagHash = IoC.Resolve<IProductTagHash>();
			}

			if (!_productTagHash.ContainProductTags())
			{
				Collection<IProductTag> productTags = ProductTagService.GetAll();
				_productTagHash.Add(productTags);
			}

			if (!_productTagHash.ContainChannel(userContext.Channel.Id))
			{
				Collection<ITagGroup> tagGroups = TagGroupService.GetAll(userContext);
				_productTagHash.Add(userContext.Channel.Id, tagGroups);
			}
		}
		protected virtual void SupplementWithBrandInfo(IUserContext userContext, IProductInfo productInfo)
		{
			if (!productInfo.Product.BrandId.HasValue) return;

			productInfo.Product.Brand = FindBrand(userContext.Channel.Id, productInfo.Product.BrandId.Value);
			productInfo.BrandUrl = GetBrandUrl(userContext, productInfo);
		}
		protected virtual void SupplementWithCategoryInfo(IUserContext context, IProductInfo productInfo)
		{
			ICategoryTreeItem categoryTreeItem = _categoryTree.FindItemById(productInfo.Product.CategoryId);
			if (categoryTreeItem == null) return;

			productInfo.ThirdCategoryLevelUrl = GetCategoryUrl(context, categoryTreeItem, ResolveProductUrl(context, productInfo.Product.LekmerUrl));

			ICategory category = categoryTreeItem.Category;
			if (category != null)
			{
				productInfo.CategoryTitle = category.Title;
			}

			categoryTreeItem = categoryTreeItem.Parent;
			if (categoryTreeItem != null)
			{
				productInfo.SecondCategoryLevelUrl = GetCategoryUrl(context, categoryTreeItem, productInfo.ThirdCategoryLevelUrl);

				ICategory parentCategory = categoryTreeItem.Category;
				if (parentCategory != null)
				{
					productInfo.ParentCategoryTitle = parentCategory.Title;
				}

				categoryTreeItem = categoryTreeItem.Parent;
				if (categoryTreeItem != null)
				{
					productInfo.FirstCategoryLevelUrl = GetCategoryUrl(context, categoryTreeItem, productInfo.SecondCategoryLevelUrl);

					ICategory mainCategory = categoryTreeItem.Category;
					if (mainCategory != null)
					{
						productInfo.MainCategoryTitle = mainCategory.Title;
					}
				}
			}
		}
		protected virtual void SupplementWithTagsInfo(IUserContext userContext, ILekmerProductView product)
		{
			product.TagGroups = _productTagHash.GetProductTags(userContext.Channel.Id, product.Id);
		}
		protected virtual void SupplementWithSizeInfo(ProductCollection products)
		{
			var productIds = new ProductIdCollection(products.Select(p => p.Id).ToArray());
			Collection<IProductSize> productSizes = ProductSizeService.GetAllByProductIdList(productIds);
			Dictionary<int, Collection<IProductSize>> productSizesByProduct = productSizes.GroupBy(ps => ps.ProductId).ToDictionary(ps => ps.Key, ps => new Collection<IProductSize>(ps.ToArray()));

			foreach (ILekmerProductView product in products)
			{
				if (!productSizesByProduct.ContainsKey(product.Id)) continue;

				Collection<IProductSize> sizes = productSizesByProduct[product.Id];
				product.ProductSizes = sizes;
			}
		}
		protected virtual IBrand FindBrand(int channelId, int brandId)
		{
			if (!_brands.ContainsKey(channelId)) return null;

			return _brands[channelId].ContainsKey(brandId) ? _brands[channelId][brandId] : null;
		}
		protected virtual void AddToCollection(Collection<IProductInfo> products, Collection<IProductInfo> productsToAdd)
		{
			foreach (IProductInfo product in productsToAdd)
			{
				products.Add(product);
			}
		}

		protected virtual string GetCategoryUrl(IUserContext context, ICategoryTreeItem categoryTreeItem, string previousObjectUrl)
		{
			int? contentNodeId = GetCategoryContentNodeId(context, categoryTreeItem);

			if (contentNodeId.HasValue)
			{
				string categoryUrl = GetContentNodeUrl(context, contentNodeId.Value);
				if (categoryUrl != null)
				{
					return ResolveUrlHttp(context, categoryUrl);
				}
			}

			if (string.IsNullOrEmpty(previousObjectUrl))
			{
				return string.Empty;
			}

			previousObjectUrl = RemoveLastSlash(previousObjectUrl);
			string appUrl = RemoveLastSlash(GetApplicationUrl(context));

			if (previousObjectUrl == appUrl)
			{
				return string.Empty;
			}

			string url = string.Empty;

			int position = previousObjectUrl.LastIndexOf('/');
			if (position > 0)
			{
				url = previousObjectUrl.Substring(0, position + 1);
			}

			return url;
		}
		protected virtual int? GetCategoryContentNodeId(IUserContext context, ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null || categoryTreeItem.Category == null)
			{
				return null;
			}

			if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
			{
				return categoryTreeItem.Category.ProductParentContentNodeId.Value;
			}

			foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
			{
				if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
				{
					return parentItem.Category.ProductParentContentNodeId.Value;
				}
			}

			return null;
		}
		protected virtual string GetBrandUrl(IUserContext context, IProductInfo productInfo)
		{
			if (productInfo.Product.Brand != null)
			{
				if (productInfo.Product.Brand.ContentNodeId.HasValue)
				{
					string brandUrl = GetContentNodeUrl(context, productInfo.Product.Brand.ContentNodeId.Value);
					if (brandUrl != null)
					{
						return ResolveUrlHttp(context, brandUrl);
					}
				}
			}

			return string.Empty;
		}
		protected virtual string GetContentNodeUrl(IUserContext context, int contentNodeId)
		{
			var treeItem = ContentNodeService.GetTreeItemById(context, contentNodeId);
			if (treeItem == null || treeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return treeItem.Url;
		}
		protected virtual string ResolveUrlHttp(IUserContext context, string relativeUrl)
		{
			if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

			if (!relativeUrl.StartsWith("~/", StringComparison.Ordinal))
			{
				return relativeUrl;
			}

			return GetApplicationUrl(context) + relativeUrl.Substring(1);
		}
		protected virtual string ResolveProductUrl(IUserContext context, string productUrl)
		{
			return GetApplicationUrl(context) + '/' + productUrl;
		}
		protected virtual string GetApplicationUrl(IUserContext context)
		{
			return "http://" + context.Channel.ApplicationName;
		}
		protected virtual string RemoveLastSlash(string url)
		{
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.Substring(0, url.Length - 1);
			}

			return url;
		}
	}
}