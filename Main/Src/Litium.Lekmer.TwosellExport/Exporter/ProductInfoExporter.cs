﻿using System.Diagnostics;
using System.IO;
using System.Reflection;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Core;
using log4net;

namespace Litium.Lekmer.TwosellExport
{
	public class ProductInfoExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected ITwosellExportSetting Setting { get; private set; }
		protected ITwosellExportService TwosellExportService { get; private set; }

		public ProductInfoExporter(ITwosellExportSetting setting, ITwosellExportService twosellExportService)
		{
			Setting = setting;
			TwosellExportService = twosellExportService;
		}

		public virtual void Execute()
		{
			_log.Info("Execution started.");

			ExecuteProductInfoExport();

			_log.Info("Execution complited.");
		}

		protected virtual void ExecuteProductInfoExport()
		{
			_log.Info("Export product list started.");

			var allowedImageGroups = TwosellExportService.GetAllowedImageGroups();
			var allowedImageSizes = TwosellExportService.GetAllowedImageSizes();
			var allowedTagGroups = TwosellExportService.GetAllowedTagGroups();
			var allowedCategoryLevelUrls = TwosellExportService.GetAllowedCategoryLevelUrls();
			var campaigns = TwosellExportService.GetCampaigns();

			var userContexts = TwosellExportService.GetUserContextForAllChannels();
			foreach (IUserContext context in userContexts)
			{
					if (!Setting.GetChannelNameReplacementExists(context.Channel.CommonName))
					{
						_log.Info("Skipping channel: " + context.Channel.CommonName);
						continue;
					}

					Stopwatch stopwatch = Stopwatch.StartNew();

					string filePath = TwosellExportService.GetProductInfoFilePath(context);
					if (File.Exists(filePath))
					{
						File.Delete(filePath);
					}

					_log.InfoFormat("File preparing... //{0}", filePath);

					_log.Info("Get products info...");
					var products = TwosellExportService.GetProductInfoCollectionToExport(context);

					using (Stream stream = File.OpenWrite(filePath))
					{
						var productInfoXmlFile = new ProductInfoXmlFile();

						productInfoXmlFile.InitializeImages(allowedImageGroups, allowedImageSizes);
						productInfoXmlFile.InitializeTagGroups(allowedTagGroups);
						productInfoXmlFile.InitializeCategoryLevels(allowedCategoryLevelUrls);
						productInfoXmlFile.InitializeCampaigns(campaigns);

						productInfoXmlFile.Save(stream, context, products);
					}

					_log.InfoFormat("File saved... //{0}", filePath);
					stopwatch.Stop();
					_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}

			_log.Info("Export product list completed.");
		}
	}
}
