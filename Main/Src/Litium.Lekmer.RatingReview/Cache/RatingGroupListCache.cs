﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingGroupListCache : ScensumCacheBase<RatingGroupListKey, Collection<IRatingGroup>>
	{
		private static readonly RatingGroupListCache _instance = new RatingGroupListCache();

		private RatingGroupListCache()
		{
		}

		public static RatingGroupListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int productId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new RatingGroupListKey(channel.Id, productId));
			}
		}
	}

	public class RatingGroupListKey : ICacheKey
	{
		public RatingGroupListKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public int ChannelId { get; set; }
		public int ProductId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + ProductId; }
		}
	}
}