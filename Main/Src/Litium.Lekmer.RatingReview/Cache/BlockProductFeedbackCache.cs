﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class BlockProductFeedbackCache : ScensumCacheBase<BlockProductFeedbackKey, IBlockProductFeedback>
	{
		private static readonly BlockProductFeedbackCache _instance = new BlockProductFeedbackCache();

		private BlockProductFeedbackCache()
		{
		}

		public static BlockProductFeedbackCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockProductFeedbackKey(channel.Id, blockId));
			}
		}
	}

	public class BlockProductFeedbackKey : ICacheKey
	{
		public BlockProductFeedbackKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}