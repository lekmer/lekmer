﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class ReviewCache : ScensumCacheBase<ReviewKey, IReview>
	{
		private static readonly ReviewCache _instance = new ReviewCache();

		private ReviewCache()
		{
		}

		public static ReviewCache Instance
		{
			get { return _instance; }
		}
	}

	public class ReviewKey : ICacheKey
	{
		public ReviewKey(int ratingReviewFeedbackId)
		{
			RatingReviewFeedbackId = ratingReviewFeedbackId;
		}

		public int RatingReviewFeedbackId { get; set; }

		public string Key
		{
			get { return "Review_RatingReviewFeedbackId_" + RatingReviewFeedbackId; }
		}
	}
}