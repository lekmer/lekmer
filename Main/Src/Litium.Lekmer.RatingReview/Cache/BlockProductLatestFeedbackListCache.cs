﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class BlockProductLatestFeedbackListCache : ScensumCacheBase<BlockProductLatestFeedbackListKey, IBlockProductLatestFeedbackList>
	{
		private static readonly BlockProductLatestFeedbackListCache _instance = new BlockProductLatestFeedbackListCache();

		private BlockProductLatestFeedbackListCache()
		{
		}

		public static BlockProductLatestFeedbackListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockProductLatestFeedbackListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockProductLatestFeedbackListKey : ICacheKey
	{
		public BlockProductLatestFeedbackListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}