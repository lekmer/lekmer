﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.NewsletterSubscriber;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public class ReviewOrderedProductMessengerHelper
	{
		private int _daysAfterPurchase;
		private string _reviewOrderedProductUrl;
		private Collection<IOrderFull> _orders;
		private Collection<IChannel> _channels;

		private ILekmerOrderService _orderService;
		private ILekmerOrderItemService _orderItemService;
		private IProductService _productService;
		private IRatingGroupService _ratingGroupService;
		private IRatingService _ratingService;
		private ILekmerChannelService _channelService;
		private ICustomerService _customerService;
		private INewsletterUnsubscriberService  _newsletterUnsubscriberService;

		public ReviewOrderedProductMessengerHelper(int daysAfterPurchase, string reviewOrderedProductUrl)
		{
			if (daysAfterPurchase > 0 )
			{
				_daysAfterPurchase = daysAfterPurchase * (-1);
			}
			else
			{
				_daysAfterPurchase = daysAfterPurchase;
			}

			_reviewOrderedProductUrl = reviewOrderedProductUrl;

			_orderService = (ILekmerOrderService)IoC.Resolve<IOrderService>();
			_orderItemService = (ILekmerOrderItemService)IoC.Resolve<IOrderItemService>();
			_productService = IoC.Resolve<IProductService>();
			_ratingGroupService = IoC.Resolve<IRatingGroupService>();
			_ratingService = IoC.Resolve<IRatingService>();
			_channelService = (ILekmerChannelService)IoC.Resolve<IChannelService>();
			_customerService = IoC.Resolve<ICustomerService>();
			_newsletterUnsubscriberService = IoC.Resolve<INewsletterUnsubscriberService>();

			_channels = _channelService.GetAll();
		}

		public void ProcessFeedbackMessages()
		{
			var ordersToRate = new Collection<int>();

			DateTime currentDate = DateTime.Now.Date;
			DateTime dateForOrders = currentDate.AddDays(_daysAfterPurchase);

			_orders = _orderService.GetLatest(dateForOrders);
			foreach (var order in _orders)
			{
				var userContext = CreateUserContext(order.ChannelId);

				// check if customer unsubscribed from review emails
				var newsletterUnsubscriber = _newsletterUnsubscriberService.GetByEmail(userContext, order.Email);
				if (newsletterUnsubscriber != null)
				{
					if (newsletterUnsubscriber.UnsubscriberOptions.Any(o => o.NewsletterTypeId == (int)NewsletterType.OrderReview)) continue;
				}

				foreach (var orderItem in _orderItemService.GetAllByOrderWithoutBrandAndTags(order.Id))
				{
					if (orderItem.IsDeleted) continue;

					var product = GetProduct(userContext, orderItem.ProductId);
					if (product == null)
					{
						product = GetProductOffline(userContext, orderItem.ProductId);
						if (product == null) continue;
					}

					var ratingGroups = GetProductRatingGroups(userContext, product);
					if (ratingGroups == null || ratingGroups.Count <= 0) continue;

					bool hasRatings = false;
					foreach (var ratingGroup in ratingGroups)
					{
						var productRatings = GetProductRatings(userContext, ratingGroup.Id);
						if (productRatings == null || productRatings.Count <= 0) continue;

						hasRatings = true;
						break;
					}

					if (hasRatings)
					{
						order.AddOrderItem(orderItem);

						if (!ordersToRate.Contains(order.Id))
						{
							ordersToRate.Add(order.Id);
						}
					}
				}
			}

			SendMessage(ordersToRate);
		}

		protected IUserContext CreateUserContext(int channelId)
		{
			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = _channels.FirstOrDefault(c => c.Id == channelId);

			return userContext;
		}

		protected IProduct GetProduct(IUserContext userContext, int productId)
		{
			return _productService.GetById(userContext, productId);
		}

		protected IProduct GetProductOffline(IUserContext userContext, int productId)
		{
			return ((ILekmerProductService)_productService).GetByIdWithoutStatusFilter(userContext, productId);
		}

		protected Collection<IRatingGroup> GetProductRatingGroups(IUserContext userContext, IProduct product)
		{
			return _ratingGroupService.GetAllByProduct(userContext, product.Id, product.CategoryId);
		}

		protected Collection<IRating> GetProductRatings(IUserContext userContext, int ratingGroupId)
		{
			return _ratingService.GetAllByGroup(userContext, ratingGroupId);
		}

		protected void SendMessage(Collection<int> ordersToRate)
		{
			foreach (int orderId in ordersToRate)
			{
				var order = (ILekmerOrderFull)_orders.FirstOrDefault(o => o.Id == orderId);
				if (order == null) continue;

				var channel = _channels.FirstOrDefault(c => c.Id == order.ChannelId);
				if (channel != null && !string.IsNullOrEmpty(_reviewOrderedProductUrl))
				{
					Guid feedbackToken = Guid.NewGuid();
					order.FeedbackToken = feedbackToken;

					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					order.Customer = _customerService.GetById(context, order.CustomerId);

					var messageArgs = new ReviewOrderedProductMessageArgs(channel, order, _reviewOrderedProductUrl);
					var messenger = new ReviewOrderedProductMessenger();
					messenger.Send(messageArgs);

					_orderService.SetFeedbackToken(order.Id, feedbackToken);
				}
			}
		}
	}
}