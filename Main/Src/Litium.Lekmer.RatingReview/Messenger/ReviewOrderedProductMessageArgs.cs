﻿using Litium.Lekmer.Order;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class ReviewOrderedProductMessageArgs
	{
		public IChannel Channel { get; set; }
		public ILekmerOrderFull Order { get; set; }
		public string PageUrl { get; set; }

		public ReviewOrderedProductMessageArgs(
			IChannel channel,
			ILekmerOrderFull order,
			string pageUrl)
		{
			Channel = channel;
			Order = order;
			PageUrl = pageUrl;
		}
	}
}
