﻿using System;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingItemSummary : IRatingItemSummary
	{
		public int RatingItemId { get; set; }
		public int TotalHitCount { get; set; }
		public decimal PercentageHitValue { get; set; }
	}
}