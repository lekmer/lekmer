﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockLatestFeedbackList : BlockBase, IBlockLatestFeedbackList
	{
		private int? _categoryId;
		private int _numberOfItems;
		private Collection<IBlockLatestFeedbackListBrand> _blockLatestFeedbackListBrands;

		public int? CategoryId
		{
			get
			{
				return _categoryId;
			}
			set
			{
				CheckChanged(_categoryId, value);
				_categoryId = value;
			}
		}

		public int NumberOfItems
		{
			get
			{
				return _numberOfItems;
			}
			set
			{
				CheckChanged(_numberOfItems, value);
				_numberOfItems = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockLatestFeedbackListBrand> BlockLatestFeedbackListBrands
		{
			get
			{
				return _blockLatestFeedbackListBrands;
			}
			set
			{
				CheckChanged(_blockLatestFeedbackListBrands, value);
				_blockLatestFeedbackListBrands = value;
			}
		}
	}
}