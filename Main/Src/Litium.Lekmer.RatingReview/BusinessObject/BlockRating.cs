﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockRating : BusinessObjectBase, IBlockRating
	{
		private int _blockId;
		private int _ratingId;

		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public int RatingId
		{
			get { return _ratingId;
; }
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}
	}
}