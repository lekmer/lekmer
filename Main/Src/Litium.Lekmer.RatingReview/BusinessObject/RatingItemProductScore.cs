﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingItemProductScore : BusinessObjectBase, IRatingItemProductScore
	{
		private int _channelId;
		private int _productId;
		private int _ratingId;
		private int _ratingItemId;
		private int _hitCount;

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int RatingId
		{
			get { return _ratingId; }
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}

		public int RatingItemId
		{
			get { return _ratingItemId; }
			set
			{
				CheckChanged(_ratingItemId, value);
				_ratingItemId = value;
			}
		}

		public int HitCount
		{
			get { return _hitCount; }
			set
			{
				CheckChanged(_hitCount, value);
				_hitCount = value;
			}
		}
	}
}