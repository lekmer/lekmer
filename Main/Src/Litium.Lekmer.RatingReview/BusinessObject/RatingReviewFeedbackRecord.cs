﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingReviewFeedbackRecord : RatingReviewFeedback, IRatingReviewFeedbackRecord
	{
		private string _productTitle;
		private string _productErpId;
		private string _userName;
		private IReview _review;
		private Collection<IRatingItemProductVote> _ratingItemProductVotes;

		public string ProductTitle
		{
			get { return _productTitle; }
			set
			{
				CheckChanged(_productTitle, value);
				_productTitle = value;
			}
		}

		public string ProductErpId
		{
			get { return _productErpId; }
			set
			{
				CheckChanged(_productErpId, value);
				_productErpId = value;
			}
		}

		public string UserName
		{
			get { return _userName; }
			set
			{
				CheckChanged(_userName, value);
				_userName = value;
			}
		}

		public IReview Review
		{
			get { return _review; }
			set
			{
				CheckChanged(_review, value);
				_review = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IRatingItemProductVote> RatingItemProductVotes
		{
			get { return _ratingItemProductVotes; }
			set
			{
				CheckChanged(_ratingItemProductVotes, value);
				_ratingItemProductVotes = value;
			}
		}
	}
}