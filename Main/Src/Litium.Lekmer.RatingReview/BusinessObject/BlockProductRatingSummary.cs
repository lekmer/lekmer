﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockProductRatingSummary : BlockBase, IBlockProductRatingSummary
	{
		private Collection<IBlockRating> _blockRatings;
		private Collection<IBlockRatingGroup> _blockRatingGroups;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockRating> BlockRatings
		{
			get { return _blockRatings; }
			set
			{
				CheckChanged(_blockRatings, value);
				_blockRatings = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockRatingGroup> BlockRatingGroups
		{
			get { return _blockRatingGroups; }
			set
			{
				CheckChanged(_blockRatingGroups, value);
				_blockRatingGroups = value;
			}
		}
	}
}