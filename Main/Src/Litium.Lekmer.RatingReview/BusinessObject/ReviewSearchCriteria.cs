using System;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class ReviewSearchCriteria : IReviewSearchCriteria
	{
		private string _author;
		private string _message;
		private string _statusId;
		private string _createdFrom;
		private string _createdTo;
		private string _productId;
		private string _productTitle;
		private string _orderId;
		private string _channelId;
		private string _ratingId;
		private string _inappropriateContent;
		private string _sortBy;
		private string _sortByDescending;

		public string Author
		{
			get
			{
				return string.IsNullOrEmpty(_author) ? null : _author;
			}
			set
			{
				_author = value;
			}
		}

		public string Message
		{
			get
			{
				return string.IsNullOrEmpty(_message) ? null : _message;
			}
			set
			{
				_message = value;
			}
		}

		public string StatusId
		{
			get
			{
				return string.IsNullOrEmpty(_statusId) ? null : _statusId;
			}
			set
			{
				_statusId = value;
			}
		}

		public string CreatedFrom
		{
			get
			{
				return string.IsNullOrEmpty(_createdFrom) ? null : _createdFrom;
			}
			set
			{
				_createdFrom = value;
			}
		}

		public string CreatedTo
		{
			get
			{
				return string.IsNullOrEmpty(_createdTo) ? null : _createdTo;
			}
			set
			{
				_createdTo = value;
			}
		}

		public string ProductId
		{
			get
			{
				return string.IsNullOrEmpty(_productId) ? null : _productId;
			}
			set
			{
				_productId = value;
			}
		}

		public string ProductTitle
		{
			get
			{
				return string.IsNullOrEmpty(_productTitle) ? null : _productTitle;
			}
			set
			{
				_productTitle = value;
			}
		}

		public string OrderId
		{
			get
			{
				return string.IsNullOrEmpty(_orderId) ? null : _orderId;
			}
			set
			{
				_orderId = value;
			}
		}

		public string ChannelId
		{
			get
			{
				return string.IsNullOrEmpty(_channelId) ? null : _channelId;
			}
			set
			{
				_channelId = value;
			}
		}

		public string RatingId
		{
			get
			{
				return string.IsNullOrEmpty(_ratingId) ? null : _ratingId;
			}
			set
			{
				_ratingId = value;
			}
		}

		public string InappropriateContent
		{
			get
			{
				return string.IsNullOrEmpty(_inappropriateContent) ? null : _inappropriateContent;
			}
			set
			{
				_inappropriateContent = value;
			}
		}

		public string SortBy
		{
			get
			{
				return string.IsNullOrEmpty(_sortBy) ? null : _sortBy;
			}
			set
			{
				_sortBy = value;
			}
		}

		public string SortByDescending
		{
			get
			{
				return string.IsNullOrEmpty(_sortByDescending) ? null : _sortByDescending;
			}
			set
			{
				_sortByDescending = value;
			}
		}
	}
}