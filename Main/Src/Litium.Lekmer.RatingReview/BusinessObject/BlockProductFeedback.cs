﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class BlockProductFeedback : BlockBase, IBlockProductFeedback
	{
		private bool _allowReview;

		public bool AllowReview
		{
			get { return _allowReview; }
			set
			{
				CheckChanged(_allowReview, value);
				_allowReview = value;
			}
		}
	}
}