﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class BlockRatingItemService : IBlockRatingItemService
	{
		protected BlockRatingItemRepository Repository { get; private set; }

		public BlockRatingItemService(BlockRatingItemRepository blockRatingItemRepository)
		{
			Repository = blockRatingItemRepository;
		}

		public virtual Collection<IBlockRatingItem> GetAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByBlock(blockId);
		}
	}
}