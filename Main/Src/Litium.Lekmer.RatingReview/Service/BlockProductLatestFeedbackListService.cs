﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductLatestFeedbackListService : IBlockProductLatestFeedbackListService
	{
		protected BlockProductLatestFeedbackListRepository Repository { get; private set; }

		public BlockProductLatestFeedbackListService(BlockProductLatestFeedbackListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductLatestFeedbackList GetById(IUserContext context, int blockId)
		{
			Repository.EnsureNotNull();

			return BlockProductLatestFeedbackListCache.Instance.TryGetItem(
				new BlockProductLatestFeedbackListKey(context.Channel.Id, blockId),
				() => GetByIdCore(context, blockId));
		}

		protected virtual IBlockProductLatestFeedbackList GetByIdCore(IUserContext context, int blockId)
		{
			return Repository.GetById(context.Channel, blockId);
		}
	}
}