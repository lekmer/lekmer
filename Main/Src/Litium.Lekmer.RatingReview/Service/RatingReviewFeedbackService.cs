﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewFeedbackService : IRatingReviewFeedbackService
	{
		private const char DELIMITER = ',';

		protected RatingReviewFeedbackRepository Repository { get; private set; }
		protected IRatingReviewStatusService RatingReviewStatusService { get; private set; }
		protected IReviewService ReviewService { get; private set; }
		protected IRatingItemProductVoteService RatingItemProductVoteService { get; private set; }
		protected IRatingItemProductScoreService RatingItemProductScoreService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }

		public RatingReviewFeedbackService(
			RatingReviewFeedbackRepository repository,
			IRatingReviewStatusService ratingReviewStatusService,
			IReviewService reviewService,
			IRatingItemProductVoteService ratingItemProductVoteService,
			IRatingItemProductScoreService ratingItemProductScoreService,
			ILekmerProductService lekmerProductService
			)
		{
			Repository = repository;
			RatingReviewStatusService = ratingReviewStatusService;
			ReviewService = reviewService;
			RatingItemProductVoteService = ratingItemProductVoteService;
			RatingItemProductScoreService = ratingItemProductScoreService;
			LekmerProductService = lekmerProductService;
		}

		public virtual IRatingReviewFeedbackRecord CreateRecord(IUserContext context)
		{
			RatingReviewStatusService.EnsureNotNull();

			var ratingReviewStatus = RatingReviewStatusService.GetByCommonName(RatingReviewStatusType.Pending.ToString());
			if (ratingReviewStatus == null)
			{
				throw new BusinessObjectNotExistsException("Could not find a IRatingReviewStatus with common name 'Pending'.");
			}

			var feedback = IoC.Resolve<IRatingReviewFeedbackRecord>();
			feedback.RatingReviewStatusId = ratingReviewStatus.Id;

			feedback.ChannelId = context.Channel.Id;
			feedback.CreatedDate = DateTime.Now;

			return feedback;
		}

		public virtual IRatingReviewFeedbackRecord Insert(IUserContext context, IRatingReviewFeedbackRecord feedbackRecord)
		{
			if (feedbackRecord == null)
			{
				throw new ArgumentNullException("feedbackRecord");
			}

			Repository.EnsureNotNull();
			ReviewService.EnsureNotNull();
			RatingItemProductVoteService.EnsureNotNull();

			using (var transaction = new TransactedOperation())
			{
				// Insert Feedback
				int feedbackId = Repository.Save(feedbackRecord);

				feedbackRecord.Id = feedbackId;
				feedbackRecord.Review.RatingReviewFeedbackId = feedbackId;
				foreach (IRatingItemProductVote productVote in feedbackRecord.RatingItemProductVotes)
				{
					productVote.RatingReviewFeedbackId = feedbackId;
				}

				// Save Review
				ReviewService.Save(feedbackRecord.Review);

				// Insert Rating Votes
				foreach (IRatingItemProductVote productVote in feedbackRecord.RatingItemProductVotes)
				{
					RatingItemProductVoteService.Insert(productVote);
				}

				transaction.Complete();
			}

			feedbackRecord.SetUntouched();

			return feedbackRecord;
		}

		public void InsertInappropriateFlag(int ratingReviewFeedbackId)
		{
			RatingItemProductScoreService.Delete(ratingReviewFeedbackId);

			Repository.EnsureNotNull();
			Repository.UpdateInappropriateFlag(ratingReviewFeedbackId, true);

			ReviewCache.Instance.Remove(new ReviewKey(ratingReviewFeedbackId));
			RatingItemProductVoteListCache.Instance.Remove(new RatingItemProductVoteListKey(ratingReviewFeedbackId));
			LatestRatingReviewFeedbackListCache.Instance.Flush();
		}

		public virtual IRatingReviewFeedback GetMostHelpful(IUserContext context, int productId, int ratingId, Collection<int> ratingItemIds, int ratingReviewStatusId, bool isInappropriate)
		{
			Repository.EnsureNotNull();

			ProductIdCollection productIdCollection = LekmerProductService.GetVariantIdAllByProduct(productId);

			if (productIdCollection != null && productIdCollection.Count > 0)
			{
				productIdCollection = new ProductIdCollection(productIdCollection) {productId};

				return Repository.GetMostHelpful(
					context.Channel.Id,
					Scensum.Foundation.Convert.ToStringIdentifierList(productIdCollection, DELIMITER),
					ratingId,
					Scensum.Foundation.Convert.ToStringIdentifierList(ratingItemIds, DELIMITER),
					DELIMITER,
					ratingReviewStatusId,
					isInappropriate,
					context.Customer != null ? (int?)context.Customer.Id : null);
			}

			return Repository.GetMostHelpful(
				context.Channel.Id,
				productId,
				ratingId,
				Scensum.Foundation.Convert.ToStringIdentifierList(ratingItemIds, DELIMITER),
				DELIMITER,
				ratingReviewStatusId,
				isInappropriate);
		}

		public IRatingReviewFeedback GetByProductAndOrder(IUserContext context, int productId, int orderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetByProductAndOrder(context.Channel.Id, productId, orderId);
		}

		public Collection<IRatingReviewFeedback> GetLatestByBlock(IUserContext context, IBlockLatestFeedbackList blockLatestFeedbackList)
		{
			Repository.EnsureNotNull();

			return LatestRatingReviewFeedbackListCache.Instance.TryGetItem(
				new LatestRatingReviewFeedbackListKey(context.Channel.Id, blockLatestFeedbackList.Id),
				() => GetLatestByBlockCore(context, blockLatestFeedbackList));
		}

		public Collection<IRatingReviewFeedback> GetLatestByProduct(IUserContext context, int productId, int numberOfItems, int ratingReviewStatusId, bool isInappropriate)
		{
			Repository.EnsureNotNull();

			ProductIdCollection productIdCollection = LekmerProductService.GetVariantIdAllByProduct(productId);

			productIdCollection = new ProductIdCollection(productIdCollection) {productId};

			return Repository.GetLatestByProduct(
				context.Channel.Id,
				Scensum.Foundation.Convert.ToStringIdentifierList(productIdCollection, DELIMITER),
				DELIMITER,
				numberOfItems,
				ratingReviewStatusId,
				isInappropriate,
				context.Customer != null ? (int?)context.Customer.Id : null);
		}


		protected virtual Collection<IRatingReviewFeedback> GetLatestByBlockCore(IUserContext context, IBlockLatestFeedbackList blockLatestFeedbackList)
		{
			var ratingReviewStatus = RatingReviewStatusService.GetByCommonName(RatingReviewStatusType.Approved.ToString());
			if (ratingReviewStatus == null)
			{
				return new Collection<IRatingReviewFeedback>();
			}

			var categoryIds = new Collection<int>();
			if (blockLatestFeedbackList.CategoryId.HasValue)
			{
				var categoryUtility = new CategoryUtility();
				categoryIds = categoryUtility.ResolveCategoryWithChildren(context, blockLatestFeedbackList.CategoryId.Value);
			}

			var brandIds = new Collection<int>();
			if (blockLatestFeedbackList.BlockLatestFeedbackListBrands != null && blockLatestFeedbackList.BlockLatestFeedbackListBrands.Count > 0)
			{
				brandIds = new Collection<int>(blockLatestFeedbackList.BlockLatestFeedbackListBrands.Select(b => b.Brand.Id).ToList());
			}

			return Repository.GetLatest(
				context.Channel.Id,
				Scensum.Foundation.Convert.ToStringIdentifierList(categoryIds, DELIMITER),
				Scensum.Foundation.Convert.ToStringIdentifierList(brandIds, DELIMITER),
				DELIMITER,
				blockLatestFeedbackList.NumberOfItems,
				ratingReviewStatus.Id,
				false, //isInappropriate,
				context.Customer != null ? (int?)context.Customer.Id : null);
		}
	}
}