﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingItemService : IRatingItemService
	{
		protected RatingItemRepository Repository { get; private set; }

		public RatingItemService(RatingItemRepository ratingItemRepository)
		{
			Repository = ratingItemRepository;
		}

		public virtual Collection<IRatingItem> GetAllByRating(IUserContext context, int ratingId)
		{
			Repository.EnsureNotNull();

			return RatingItemListCache.Instance.TryGetItem(
				new RatingItemListKey(context.Channel.Id, ratingId),
				() => Repository.GetAllByRating(context.Channel.Id, ratingId));
		}
	}
}