﻿using System.Collections.ObjectModel;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockLatestFeedbackListBrandService : IBlockLatestFeedbackListBrandService
	{
		protected BlockLatestFeedbackListBrandRepository Repository { get; private set; }

		public BlockLatestFeedbackListBrandService(BlockLatestFeedbackListBrandRepository repository)
		{
			Repository = repository;
		}

		public Collection<IBlockLatestFeedbackListBrand> GetAllByBlock(IUserContext context, int blockId)
		{
			return Repository.GetAllByBlock(context.Channel.Id, blockId);
		}
	}
}