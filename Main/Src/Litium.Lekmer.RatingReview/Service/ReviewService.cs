﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class ReviewService : IReviewService
	{
		protected ReviewRepository Repository { get; private set; }

		public ReviewService(ReviewRepository repository)
		{
			Repository = repository;
		}

		public virtual IReview Save(IReview review)
		{
			if (review == null)
			{
				throw new ArgumentNullException("review");
			}

			Repository.EnsureNotNull();

			review.Id = Repository.Save(review);

			return review;
		}

		public virtual IReview GetByFeedback(int ratingReviewFeedbackId)
		{
			Repository.EnsureNotNull();

			return ReviewCache.Instance.TryGetItem(
				new ReviewKey(ratingReviewFeedbackId), 
				() => Repository.GetByFeedback(ratingReviewFeedbackId));
		}
	}
}