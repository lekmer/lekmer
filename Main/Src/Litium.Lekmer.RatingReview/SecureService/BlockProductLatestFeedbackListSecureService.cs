using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockProductLatestFeedbackListSecureService : IBlockProductLatestFeedbackListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductLatestFeedbackListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }
		

		public BlockProductLatestFeedbackListSecureService(
			IAccessValidator accessValidator,
			BlockProductLatestFeedbackListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IChannelSecureService channelSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			ChannelSecureService = channelSecureService;
		}

		public virtual IBlockProductLatestFeedbackList Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockProductLatestFeedbackList = IoC.Resolve<IBlockProductLatestFeedbackList>();

			blockProductLatestFeedbackList.AccessId = AccessSecureService.GetByCommonName("All").Id;

			blockProductLatestFeedbackList.Status = BusinessObjectStatus.New;

			return blockProductLatestFeedbackList;
		}

		public virtual IBlockProductLatestFeedbackList GetById(int blockId)
		{
			Repository.EnsureNotNull();

			var blockProductLatestFeedbackList = Repository.GetByIdSecure(blockId);

			return blockProductLatestFeedbackList;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockProductLatestFeedbackList BlockProductLatestFeedbackList = Create();

			BlockProductLatestFeedbackList.ContentNodeId = contentNodeId;
			BlockProductLatestFeedbackList.ContentAreaId = contentAreaId;
			BlockProductLatestFeedbackList.BlockTypeId = blockTypeId;
			BlockProductLatestFeedbackList.BlockStatusId = (int) BlockStatusInfo.Offline;
			BlockProductLatestFeedbackList.NumberOfItems = 1;
			BlockProductLatestFeedbackList.Title = title;
			BlockProductLatestFeedbackList.TemplateId = null;
			BlockProductLatestFeedbackList.Id = Save(systemUserFull, BlockProductLatestFeedbackList);

			return BlockProductLatestFeedbackList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductLatestFeedbackList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			Repository.EnsureNotNull();
			
			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				transactedOperation.Complete();
			}

			BlockProductLatestFeedbackListCache.Instance.Remove(block.Id, ChannelSecureService.GetAll());

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockProductLatestFeedbackListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}
	}
}