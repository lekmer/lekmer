﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockRatingGroupSecureService : IBlockRatingGroupSecureService
	{
		protected BlockRatingGroupRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public BlockRatingGroupSecureService(BlockRatingGroupRepository blockRatingGroupRepository, IAccessValidator accessValidator)
		{
			Repository = blockRatingGroupRepository;
			AccessValidator = accessValidator;
		}

		public virtual void SaveAll(ISystemUserFull systemUserFull, int blockId, Collection<IBlockRatingGroup> blockRatingGroups)
		{
			if (blockRatingGroups == null)
			{
				throw new ArgumentNullException("blockRatingGroups");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(blockId);

				foreach (var blockRatingGroup in blockRatingGroups)
				{
					Repository.Save(blockId, blockRatingGroup.RatingGroupId);
				}

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(blockId);

				transaction.Complete();
			}
		}

		public virtual Collection<IBlockRatingGroup> GetAllByBlock(int blockId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByBlock(blockId);
		}
	}
}