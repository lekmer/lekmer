﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.RatingReview
{
	public class RatingFolderSecureService : IRatingFolderSecureService
	{
		protected RatingFolderRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IRatingSecureService RatingSecureService { get; private set; }

		public RatingFolderSecureService(RatingFolderRepository ratingFolderRepository, IAccessValidator accessValidator, IRatingSecureService ratingSecureService)
		{
			Repository = ratingFolderRepository;
			AccessValidator = accessValidator;
			RatingSecureService = ratingSecureService;
		}

		public virtual IRatingFolder Create()
		{
			var ratingFolder = IoC.Resolve<IRatingFolder>();
			ratingFolder.Status = BusinessObjectStatus.New;
			return ratingFolder;
		}

		public virtual IRatingFolder Save(ISystemUserFull systemUserFull, IRatingFolder ratingFolder)
		{
			if (ratingFolder == null)
			{
				throw new ArgumentNullException("ratingFolder");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				ratingFolder.Id = Repository.Save(ratingFolder);

				transaction.Complete();
			}

			return ratingFolder;
		}

		public virtual bool TryDelete(ISystemUserFull systemUserFull, int ratingFolderId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			RatingSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				if (!TryDeleteCore(ratingFolderId))
				{
					return false;
				}

				transaction.Complete();

				return true;
			}
		}

		public virtual IRatingFolder GetById(int ratingFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetById(ratingFolderId);
		}

		public virtual Collection<IRatingFolder> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}

		public virtual Collection<IRatingFolder> GetAllByParent(int parentRatingFolderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByParent(parentRatingFolderId);
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			Repository.EnsureNotNull();

			return Repository.GetTree(selectedId);
		}


		protected virtual bool TryDeleteCore(int ratingFolderId)
		{
			if (RatingSecureService.GetAllByFolder(ratingFolderId).Count > 0)
			{
				return false;
			}

			if (!TryDeleteChildren(ratingFolderId))
			{
				return false;
			}

			Repository.Delete(ratingFolderId);

			return true;
		}

		protected virtual bool TryDeleteChildren(int parentRatingFolderId)
		{
			foreach (IRatingFolder ratingFolder in Repository.GetAllByParent(parentRatingFolderId))
			{
				if (!TryDeleteCore(ratingFolder.Id))
				{
					return false;
				}
			}

			return true;
		}
	}
}