﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupProductSecureService : IRatingGroupProductSecureService
	{
		protected RatingGroupProductRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public RatingGroupProductSecureService(RatingGroupProductRepository ratingGroupProductRepository, IAccessValidator accessValidator, IChannelSecureService channelSecureService)
		{
			Repository = ratingGroupProductRepository;
			AccessValidator = accessValidator;
			ChannelSecureService = channelSecureService;
		}

		public virtual void Save(ISystemUserFull systemUserFull, IRatingGroupProduct ratingGroupProduct)
		{
			if (ratingGroupProduct == null)
			{
				throw new ArgumentNullException("ratingGroupProduct");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Save(ratingGroupProduct);

				transaction.Complete();
			}

			RatingGroupProductListCache.Instance.Remove(new RatingGroupProductListKey(ratingGroupProduct.ProductId));
			RatingGroupListCache.Instance.Remove(ratingGroupProduct.ProductId, ChannelSecureService.GetAll());
		}

		public virtual void SaveAll(ISystemUserFull systemUserFull, int productId, Collection<IRatingGroupProduct> ratingGroupProducts)
		{
			if (ratingGroupProducts == null)
			{
				throw new ArgumentNullException("ratingGroupProducts");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			ChannelSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			Collection<IRatingGroupProduct> existedCollection = Repository.GetAllByProduct(productId);

			using (var transaction = new TransactedOperation())
			{
				// Remove existed if necessary
				foreach (var existedItem in existedCollection)
				{
					IRatingGroupProduct ratingGroupProduct = ratingGroupProducts.FirstOrDefault(r => r.ProductId == productId && r.RatingGroupId == existedItem.RatingGroupId);

					if (ratingGroupProduct == null)
					{
						Repository.Delete(existedItem.RatingGroupId, productId);
					}
				}

				// Save new if necessary
				foreach (var item in ratingGroupProducts)
				{
					IRatingGroupProduct ratingGroupProduct = existedCollection.FirstOrDefault(r => r.ProductId == productId && r.RatingGroupId == item.RatingGroupId);

					if (ratingGroupProduct == null)
					{
						Repository.Save(item);
					}
				}

				transaction.Complete();
			}

			RatingGroupProductListCache.Instance.Remove(new RatingGroupProductListKey(productId));
			RatingGroupListCache.Instance.Remove(productId, ChannelSecureService.GetAll());
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int ratingGroupId, int productId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(ratingGroupId, productId);

				transaction.Complete();
			}

			RatingGroupProductListCache.Instance.Remove(new RatingGroupProductListKey(productId));
			RatingGroupListCache.Instance.Remove(productId, ChannelSecureService.GetAll());
		}

		public virtual Collection<IRatingGroupProduct> GetAllByGroup(int ratingGroupId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByGroup(ratingGroupId);
		}

		public virtual Collection<IRatingGroupProduct> GetAllByProduct(int productId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByProduct(productId);
		}
	}
}