using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewStatusSecureService : IRatingReviewStatusSecureService
	{
		protected RatingReviewStatusRepository Repository { get; private set; }

		public RatingReviewStatusSecureService(RatingReviewStatusRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IRatingReviewStatus> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}
	}
}