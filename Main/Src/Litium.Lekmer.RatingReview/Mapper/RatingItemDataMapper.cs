﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingItemDataMapper : DataMapperBase<IRatingItem>
	{
		public RatingItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingItem Create()
		{
			var ratingItem = IoC.Resolve<IRatingItem>();

			ratingItem.Id = MapValue<int>("RatingItem.RatingItemId");
			ratingItem.RatingId = MapValue<int>("RatingItem.RatingId");
			ratingItem.Title = MapValue<string>("RatingItem.Title");
			ratingItem.Description = MapValue<string>("RatingItem.Description");
			ratingItem.Score = MapValue<int>("RatingItem.Score");

			ratingItem.SetUntouched();

			return ratingItem;
		}
	}
}