﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingItemProductScoreSummaryDataMapper : DataMapperBase<IRatingItemProductScoreSummary>
	{
		public RatingItemProductScoreSummaryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingItemProductScoreSummary Create()
		{
			var itemProductScoreSummary = IoC.Resolve<IRatingItemProductScoreSummary>();

			itemProductScoreSummary.RatingId = MapValue<int>("RatingItemProductScoreSummary.RatingId");
			itemProductScoreSummary.RatingItemId = MapValue<int>("RatingItemProductScoreSummary.RatingItemId");
			itemProductScoreSummary.RatingItemScore = MapValue<int>("RatingItemProductScoreSummary.RatingItemScore");
			itemProductScoreSummary.TotalHitCount = MapValue<int>("RatingItemProductScoreSummary.TotalHitCount");

			itemProductScoreSummary.SetUntouched();

			return itemProductScoreSummary;
		}
	}
}