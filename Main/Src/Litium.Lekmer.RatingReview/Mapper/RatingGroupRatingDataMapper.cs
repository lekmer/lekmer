﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingGroupRatingDataMapper : DataMapperBase<IRatingGroupRating>
	{
		public RatingGroupRatingDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingGroupRating Create()
		{
			var ratingGroupRating = IoC.Resolve<IRatingGroupRating>();

			ratingGroupRating.RatingGroupId = MapValue<int>("RatingGroupRating.RatingGroupId");
			ratingGroupRating.RatingId = MapValue<int>("RatingGroupRating.RatingId");
			ratingGroupRating.Ordinal = MapValue<int>("RatingGroupRating.Ordinal");

			ratingGroupRating.SetUntouched();

			return ratingGroupRating;
		}
	}
}