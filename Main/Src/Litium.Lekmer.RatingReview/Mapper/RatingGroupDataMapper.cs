﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingGroupDataMapper : DataMapperBase<IRatingGroup>
	{
		public RatingGroupDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingGroup Create()
		{
			var ratingGroup = IoC.Resolve<IRatingGroup>();

			ratingGroup.Id = MapValue<int>("RatingGroup.RatingGroupId");
			ratingGroup.RatingGroupFolderId = MapValue<int>("RatingGroup.RatingGroupFolderId");
			ratingGroup.CommonName = MapValue<string>("RatingGroup.CommonName");
			ratingGroup.Title = MapValue<string>("RatingGroup.Title");
			ratingGroup.Description = MapValue<string>("RatingGroup.Description");

			ratingGroup.SetUntouched();

			return ratingGroup;
		}
	}
}