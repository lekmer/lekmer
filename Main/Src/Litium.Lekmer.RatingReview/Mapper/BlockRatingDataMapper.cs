﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockRatingDataMapper : DataMapperBase<IBlockRating>
	{
		public BlockRatingDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBlockRating Create()
		{
			var blockRating = IoC.Resolve<IBlockRating>();

			blockRating.BlockId = MapValue<int>("BlockRating.BlockId");
			blockRating.RatingId = MapValue<int>("BlockRating.RatingId");

			blockRating.SetUntouched();

			return blockRating;
		}
	}
}