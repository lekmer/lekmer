﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingRegistryDataMapper : DataMapperBase<IRatingRegistry>
	{
		public RatingRegistryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingRegistry Create()
		{
			var ratingRegistry = IoC.Resolve<IRatingRegistry>();

			ratingRegistry.Id = MapValue<int>("RatingRegistry.RatingRegistryId");
			ratingRegistry.CommonName = MapValue<string>("RatingRegistry.CommonName");
			ratingRegistry.Title = MapValue<string>("RatingRegistry.Title");

			return ratingRegistry;
		}
	}
}