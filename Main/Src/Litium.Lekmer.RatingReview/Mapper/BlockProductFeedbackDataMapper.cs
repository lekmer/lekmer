﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockProductFeedbackDataMapper : DataMapperBase<IBlockProductFeedback>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockProductFeedbackDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockProductFeedback Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockProductFeedback = IoC.Resolve<IBlockProductFeedback>();

			block.ConvertTo(blockProductFeedback);

			blockProductFeedback.AllowReview = MapValue<bool>("BlockProductFeedback.AllowReview");

			blockProductFeedback.SetUntouched();

			return blockProductFeedback;
		}
	}
}