﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingItemProductVoteDataMapper : DataMapperBase<IRatingItemProductVote>
	{
		private DataMapperBase<IRating> _rating;
		private DataMapperBase<IRatingItem> _ratingItem;

		public RatingItemProductVoteDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_rating = DataMapperResolver.Resolve<IRating>(DataReader);
			_ratingItem = DataMapperResolver.Resolve<IRatingItem>(DataReader);
		}

		protected override IRatingItemProductVote Create()
		{
			var ratingItemProductVote = IoC.Resolve<IRatingItemProductVote>();

			ratingItemProductVote.RatingReviewFeedbackId = MapValue<int>("RatingItemProductVote.RatingReviewFeedbackId");
			ratingItemProductVote.RatingId = MapValue<int>("RatingItemProductVote.RatingId");
			ratingItemProductVote.RatingItemId = MapValue<int>("RatingItemProductVote.RatingItemId");
			ratingItemProductVote.Rating = _rating.MapRow();
			ratingItemProductVote.RatingItem = _ratingItem.MapRow();

			ratingItemProductVote.SetUntouched();

			return ratingItemProductVote;
		}
	}
}