﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewMessengerSetting : SettingBase
	{
		private const string _storageName = "RatingReviewMessagingService";

		protected override string StorageName
		{
			get { return _storageName; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public static string GetStorageName()
		{
			return _storageName;
		}

		private const string GROUP_NAME = "RatingReviewMessenger";

		public int TimerInterval()
		{
			return GetInt32(GROUP_NAME, "TimerInterval", 60 * 24);
		}

		public int DaysAfterPurchase()
		{
			return GetInt32(GROUP_NAME, "DaysAfterPurchase", 7);
		}

		public string ReviewOrderedProductUrl()
		{
			return GetString(GROUP_NAME, "ReviewOrderedProductUrl");
		}
	}
}