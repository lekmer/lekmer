﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockBestRatedProductListRepository
	{
		protected virtual DataMapperBase<IBlockBestRatedProductList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBestRatedProductList>(dataReader);
		}

		public virtual void Save(IBlockBestRatedProductList blockBestRatedProductList)
		{
			if (blockBestRatedProductList == null)
			{
				throw new ArgumentNullException("blockBestRatedProductList");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockBestRatedProductList.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", blockBestRatedProductList.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", blockBestRatedProductList.RatingId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockBestRatedProductListRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockBestRatedProductListSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBestRatedProductListRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockBestRatedProductListDelete]", parameters, dbSettings);
		}

		public virtual IBlockBestRatedProductList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBestRatedProductListRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockBestRatedProductListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockBestRatedProductList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockBestRatedProductListRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockBestRatedProductListGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}