﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingGroupCategoryRepository
	{
		protected virtual DataMapperBase<IRatingGroupCategory> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingGroupCategory>(dataReader);
		}

		public virtual void Save(IRatingGroupCategory ratingGroupCategory)
		{
			if (ratingGroupCategory == null)
			{
				throw new ArgumentNullException("ratingGroupCategory");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupCategory.RatingGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", ratingGroupCategory.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubCategories", ratingGroupCategory.IncludeSubcategories, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("RatingGroupCategoryRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupCategorySave]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingGroupId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupCategoryRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupCategoryDelete]", parameters, dbSettings);
		}

		public virtual Collection<IRatingGroupCategory> GetAllByGroup(int ratingGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupCategoryRepository.GetAllByGroup");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupCategoryGetAllByGroup]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRatingGroupCategory> GetAllByCategory(int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupCategoryRepository.GetAllByCategory");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupCategoryGetAllByCategory]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}