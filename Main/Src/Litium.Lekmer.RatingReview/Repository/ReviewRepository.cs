﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class ReviewRepository
	{
		protected virtual DataMapperBase<IReview> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IReview>(dataReader);
		}

		public virtual int Save(IReview review)
		{
			if (review == null)
			{
				throw new ArgumentNullException("review");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ReviewId", review.Id > 0 ? (object) review.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", review.RatingReviewFeedbackId, SqlDbType.Int),
					ParameterHelper.CreateParameter("AuthorName", review.AuthorName, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Title", review.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Message", review.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Email", review.Email, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("ReviewRepository.Insert");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pReviewSave]", parameters, dbSettings);
		}

		public virtual void Update(string feedbackIdList, string authorNameList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("FeedbackIds", feedbackIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("AuthorNames", authorNameList, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("ReviewRepository.Update");
			new DataHandler().ExecuteCommand("[review].[pReviewUpdate]", parameters, dbSettings);
		}

		public virtual IReview GetByFeedback(int ratingReviewFeedbackId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ReviewRepository.GetByFeedback");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pReviewGetByFeedback]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		public virtual void ReviewHistoryInsert(int reviewId, int authorId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ReviewId", reviewId, SqlDbType.Int),
					ParameterHelper.CreateParameter("AuthorId", authorId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ReviewRepository.ReviewHistoryInsert");

			new DataHandler().ExecuteCommand("[review].[pReviewHistoryInsert]", parameters, dbSettings);
		}
	}
}