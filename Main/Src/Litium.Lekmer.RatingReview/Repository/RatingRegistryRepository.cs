﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingRegistryRepository
	{
		protected virtual DataMapperBase<IRatingRegistry> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingRegistry>(dataReader);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IRatingRegistry> GetAll()
		{
			var dbSettings = new DatabaseSetting("RatingRegistryRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingRegistryGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}