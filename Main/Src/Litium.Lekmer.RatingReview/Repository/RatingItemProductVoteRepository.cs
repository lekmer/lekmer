﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingItemProductVoteRepository
	{
		protected virtual DataMapperBase<IRatingItemProductVote> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingItemProductVote>(dataReader);
		}

		public virtual void Insert(IRatingItemProductVote ratingItemProductVote)
		{
			if (ratingItemProductVote == null)
			{
				throw new ArgumentNullException("ratingItemProductVote");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingItemProductVote.RatingReviewFeedbackId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingItemProductVote.RatingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingItemId", ratingItemProductVote.RatingItemId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemProductVoteRepository.Insert");

			new DataHandler().ExecuteCommand("[review].[pRatingItemProductVoteInsert]", parameters, dbSettings);
		}

		public virtual Collection<IRatingItemProductVote> GetAllByFeedback(int ratingReviewFeedbackId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingReviewFeedbackId", ratingReviewFeedbackId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemProductVoteRepository.GetAllByFeedback");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingItemProductVoteGetAllByFeedback]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}