﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingRepository
	{
		protected virtual DataMapperBase<IRating> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRating>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual int Save(IRating rating)
		{
			if (rating == null)
			{
				throw new ArgumentNullException("rating");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", rating.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingFolderId", rating.RatingFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingRegistryId", rating.RatingRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CommonName", rating.CommonName, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Title", rating.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", rating.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CommonForVariants", rating.CommonForVariants, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRating_Save]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRating_Delete]", parameters, dbSettings);
		}

		public virtual IRating GetById(int channelId, int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRating_GetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IRating GetByIdSecure(int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRating_GetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IRating> GetAllByGroup(int channelId, int groupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", groupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.GetAllByGroup");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRating_GetAllByGroup]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRating> GetAllByGroupSecure(int groupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", groupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.GetAllByGroupSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRating_GetAllByGroupSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRating> GetAllByFolder(int folderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingFolderId", folderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.GetAllByFolder");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRating_GetAllByFolder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRating> Search(string searchCriteria)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchCriteria", searchCriteria, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.Search");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRating_Search]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Translations.

		public virtual void SaveTranslations(int ratingId, int languageId, string title, string description)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", description, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.SaveTranslations");

			new DataHandler().ExecuteCommand("[review].[pRatingTranslationSave]", parameters, dbSettings);
		}

		public virtual Collection<ITranslationGeneric> GetAllTitleTranslations(int ratingId)
		{
			return GetAllTranslations(ratingId, "[review].[pRatingTranslationTitleGetAll]");
		}

		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int ratingId)
		{
			return GetAllTranslations(ratingId, "[review].[pRatingTranslationDescriptionGetAll]");
		}
		
		protected virtual Collection<ITranslationGeneric> GetAllTranslations(int ratingId, string storedProcedureName)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingRepository.GetAllTranslations");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(storedProcedureName, parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}