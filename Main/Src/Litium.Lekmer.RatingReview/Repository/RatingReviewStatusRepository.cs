﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingReviewStatusRepository
	{
		protected virtual DataMapperBase<IRatingReviewStatus> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingReviewStatus>(dataReader);
		}

		public virtual IRatingReviewStatus GetByCommonName(string commonName)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CommonName", commonName, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("RatingReviewStatusRepository.GetByCommonName");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewStatusGetByCommonName]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IRatingReviewStatus> GetAll()
		{
			var dbSettings = new DatabaseSetting("RatingReviewStatusRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingReviewStatusGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}