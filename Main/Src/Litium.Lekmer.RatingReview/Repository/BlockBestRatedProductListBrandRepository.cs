﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockBestRatedProductListBrandRepository
	{
		protected virtual DataMapperBase<IBlockBestRatedProductListBrand> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBestRatedProductListBrand>(dataReader);
		}

		public virtual void Save(IBlockBestRatedProductListBrand blockBestRatedProductListBrand)
		{
			if (blockBestRatedProductListBrand == null)
			{
				throw new ArgumentNullException("blockBestRatedProductListBrand");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockBestRatedProductListBrand.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", blockBestRatedProductListBrand.Brand.Id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBestRatedProductListBrandRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockBestRatedProductListBrandSave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBestRatedProductListBrandRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[review].[pBlockBestRatedProductListBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockBestRatedProductListBrand> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBestRatedProductListBrandRepository.GetAllByBlockSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockBestRatedProductListBrandGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockBestRatedProductListBrand> GetAllByBlock(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBestRatedProductListBrandRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockBestRatedProductListBrandGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}