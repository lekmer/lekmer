﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockLatestFeedbackListBrandRepository
	{
		protected virtual DataMapperBase<IBlockLatestFeedbackListBrand> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockLatestFeedbackListBrand>(dataReader);
		}

		public virtual void Save(IBlockLatestFeedbackListBrand blockLatestFeedbackListBrand)
		{
			if (blockLatestFeedbackListBrand == null)
			{
				throw new ArgumentNullException("blockLatestFeedbackListBrand");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockLatestFeedbackListBrand.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", blockLatestFeedbackListBrand.Brand.Id, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockLatestFeedbackListBrandRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockLatestFeedbackListBrandSave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockLatestFeedbackListBrandRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[review].[pBlockLatestFeedbackListBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockLatestFeedbackListBrand> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockLatestFeedbackListBrandRepository.GetAllByBlockSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockLatestFeedbackListBrandGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockLatestFeedbackListBrand> GetAllByBlock(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockLatestFeedbackListBrandRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockLatestFeedbackListBrandGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}