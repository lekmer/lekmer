﻿using System;

namespace Litium.Lekmer.Customer.Web
{
	public class RegisterMessageArgs
	{
		public string UserName { get; set; }
		public string Name { get; set; }
		public DateTime CreatedDate { get; set; }
		public bool IsAlternateLogin { get; set; }
		public bool IsLekmerAccount { get; set; }

		public RegisterMessageArgs(
			string userName,
			string name,
			DateTime createdDate,
			bool isAlternateLogin,
			bool isLekmerAccount)
		{
			UserName = userName;
			Name = name;
			CreatedDate = createdDate;
			IsAlternateLogin = isAlternateLogin;
			IsLekmerAccount = isLekmerAccount;
		}
	}
}