﻿using System;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class BlockNewPasswordControl : BlockControlBase
	{
		private readonly ICustomerService _customerService;
		private readonly ICustomerForgotPasswordService _customerForgotPasswordService;

		private NewPasswordForm _newPasswordForm;
		private NewPasswordForm NewPasswordForm
		{
			get { return _newPasswordForm ?? (_newPasswordForm = new NewPasswordForm(ResolveUrl(ContentNodeTreeItem.Url))); }
		}

		private bool WasPasswordChanged { get; set; }


		public BlockNewPasswordControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICustomerService customerService,
			ICustomerForgotPasswordService customerForgotPasswordService)
			: base(templateFactory, blockService)
		{
			_customerService = customerService;
			_customerForgotPasswordService = customerForgotPasswordService;
		}


		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult;
			if (NewPasswordForm.IsFormPostBack)
			{
				NewPasswordForm.MapFromRequest();
				ICustomer customer;
				validationResult = NewPasswordForm.Validate(out customer);
				if (validationResult.IsValid && customer != null)
				{
					customer.User.ClearTextPassword = NewPasswordForm.Password;
					_customerService.Save(UserContext.Current, customer);
					_customerForgotPasswordService.Delete(customer.Id);
					WasPasswordChanged = true;
				}
			}
			else
			{
				validationResult = new ValidationResult();
				var customerId = ValidatePageAuthorization(validationResult);
				if (!validationResult.IsValid)
				{
					var validationControl = new ValidationControl(validationResult.Errors);
					return new BlockContent(string.Empty, validationControl.Render(), string.Empty);
				}

				NewPasswordForm.Clear();
				NewPasswordForm.CustomerId = customerId;
			}

			return RenderNewPasswordForm(validationResult);
		}

		private BlockContent RenderNewPasswordForm(ValidationResult validationResult)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			NewPasswordForm.MapFieldNamesToFragment(fragmentContent);
			NewPasswordForm.MapFieldValuesToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", RenderValidationErrors(validationResult), VariableEncoding.None);
			fragmentContent.AddCondition("NewPassword.WasChanged", WasPasswordChanged);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			NewPasswordForm.MapFieldNamesToFragment(fragment);
			NewPasswordForm.MapFieldValuesToFragment(fragment);
			return fragment.Render();
		}

		private string RenderValidationErrors(ValidationResult validationResult)
		{
			if (validationResult == null || validationResult.IsValid) 
				return null;

			var validationControl = new ValidationControl(validationResult.Errors);
			return validationControl.Render();
		}

		private int ValidatePageAuthorization(ValidationResult validationResult)
		{
			Guid guid;
			ICustomerForgotPassword customerForgotPassword = null;
			var id = Request.QueryString["id"];

			if (string.IsNullOrEmpty(id) || !TryParseGuid(id, out guid) || !IsUrlValid(guid, out customerForgotPassword))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.GuidIncorrect"));
			}
			else if (customerForgotPassword.ValidToDate < DateTime.Now)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.Expired"));
			}

			return customerForgotPassword != null ? customerForgotPassword.CustomerId : -1;
		}

		private bool TryParseGuid(string id, out Guid guid)
		{
			try
			{
				guid = new Guid(id);
				return true;
			}
			catch
			{
				guid = Guid.Empty;
				return false;
			}
		}

		private bool IsUrlValid(Guid guid, out ICustomerForgotPassword customerForgotPassword)
		{
			customerForgotPassword = _customerForgotPasswordService.GetByGuid(guid);
			return customerForgotPassword != null;
		}
	}
}