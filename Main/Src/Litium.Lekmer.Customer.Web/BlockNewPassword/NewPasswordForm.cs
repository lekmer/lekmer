﻿using System.Collections.Specialized;
using System.Globalization;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class NewPasswordForm : ControlBase
	{
		private readonly string _postUrl;

		public string PostUrl
		{
			get { return _postUrl; }
		}
		private static string PostModeValue
		{
			get { return "newpassword"; }
		}
		private static string PasswordFormName
		{
			get { return "newpassword-password"; }
		}
		private static string PasswordVerifyFormName
		{
			get { return "newpassword-passwordverify"; }
		}
		private static string CustomerIdFormName
		{
			get { return "newpassword-customerid"; }
		}
		public string Password { get; set; }
		public string PasswordVerify { get; set; }
		public int CustomerId { get; set; }
		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}


		public NewPasswordForm(string postUrl)
		{
			_postUrl = postUrl;
		}


		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Password.Name", PasswordFormName);
			fragment.AddVariable("Form.PasswordVerify.Name", PasswordVerifyFormName);
			fragment.AddVariable("Form.CustomerId.Name", CustomerIdFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Password.Value", Password);
			fragment.AddVariable("Form.PasswordVerify.Value", PasswordVerify);
			fragment.AddVariable("Form.CustomerId.Value", CustomerId.ToString(CultureInfo.InvariantCulture));
		}

		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			Password = form[PasswordFormName];
			PasswordVerify = form[PasswordVerifyFormName];
			int customerId;
			CustomerId = int.TryParse(form[CustomerIdFormName], out customerId) ? customerId : 0;
		}

		public void Clear()
		{
			Password = string.Empty;
			PasswordVerify = string.Empty;
			CustomerId = 0;
		}

		public ValidationResult Validate(out ICustomer customer)
		{
			customer = null;
			var validationResult = new ValidationResult();

			if (CustomerId < 1 || !GetCustomer(out customer))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.CustomerNotFound"));
			}
			else
			{
				validationResult.Errors.Append(CustomerValidationUtil.ValidatePassword(Password, PasswordVerify, "NewPassword").Errors);
			}

			return validationResult;
		}

		private bool GetCustomer(out ICustomer customer)
		{
			customer = IoC.Resolve<ICustomerService>().GetById(UserContext.Current, CustomerId);
			return customer != null;
		}
	}
}