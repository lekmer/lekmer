﻿using System.Collections.Specialized;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class SignInForm : ControlBase
	{
		private readonly string _signInPostUrl;

		public virtual string PostUrl
		{
			get { return _signInPostUrl; }
		}
		public virtual string PostModeValue
		{
			get { return "signin"; }
		}
		public virtual string UserNameFormName
		{
			get { return "signin-username"; }
		}
		public virtual string PasswordFormName
		{
			get { return "signin-password"; }
		}
		public virtual string SignInActionName
		{
			get { return "signin-action"; }
		}
		public virtual string FacebookSignInActionName
		{
			get { return "facebook-signin-action"; }
		}
		public virtual string UserName { get; set; }
		public virtual string Password { get; set; }
		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}
		public virtual bool SignInActionPost
		{
			get { return Request.Form[SignInActionName] != null; }
		}
		public virtual bool FacebookSignInActionPost
		{
			get { return Request.Form[FacebookSignInActionName] != null; }
		}


		public SignInForm(string signInPostUrl)
		{
			_signInPostUrl = signInPostUrl;
		}


		/// <summary>
		/// Adds field name variables to a certain fragment.
		/// </summary>
		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.UserName.Name", UserNameFormName);
			fragment.AddVariable("Form.Password.Name", PasswordFormName);
			fragment.AddVariable("Form.SignIn.Action.Name", SignInActionName);
			fragment.AddVariable("Form.FacebookSignIn.Action.Name", FacebookSignInActionName);
		}

		/// <summary>
		/// Adds field value variables to a certain fragment.
		/// </summary>
		public virtual void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.UserName.Value", UserName);
			fragment.AddVariable("Form.Password.Value", Password);
		}

		/// <summary>
		/// Map fields from http request form.
		/// </summary>
		public virtual void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			UserName = form[UserNameFormName];
			Password = form[PasswordFormName];
		}

		/// <summary>
		/// Validates the form.
		/// </summary>
		public virtual ValidationResult Validate(out ICustomer customer)
		{
			customer = null;
			var validationResult = new ValidationResult();

			if (UserName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.SignIn.Validation.UserNameNotProvided"));
			}
			else if (Password.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.SignIn.Validation.PasswordNotProvided"));
			}
			else
			{
				var customerService = IoC.Resolve<ICustomerService>();
				bool isValid = customerService.Validate(UserContext.Current, UserName, Password, out customer);
				if (isValid) return validationResult;

				if (customer == null || customer.User == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.SignIn.Validation.InvalidUserName"));
				}
				else
				{
					var customerStatus = (CustomerStatusInfo) customer.CustomerStatusId;
					if (customerStatus == CustomerStatusInfo.Offline || customerStatus == CustomerStatusInfo.Blocked)
					{
						validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.SignIn.Validation.CustomerIsBlockedOrOffline"));
					}
					else
					{
						validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.SignIn.Validation.InvalidPassword"));
					}
				}
			}

			return validationResult;
		}
	}
}
