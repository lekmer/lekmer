using System;
using System.Collections.Specialized;
using System.Globalization;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Customer detail form.
	/// </summary>
	public class CustomerDetailForm : ControlBase
	{
		private readonly string _customerDetailPostUrl;
		private LekmerInformationForm _customerInformationForm;
		private AddressForm _billingAddressForm;
		private AddressForm _deliveryAddressForm;
		private FacebookUtility _facebookUtility;
		private ICustomerService _customerService;
		private IUserService _userService;
		private IAddressService _addressService;

		public virtual string PostUrl
		{
			get { return _customerDetailPostUrl; }
		}
		public LekmerInformationForm CustomerInformationForm
		{
			get { return _customerInformationForm ?? (_customerInformationForm = new LekmerInformationForm()); }
		}
		public AddressForm BillingAddressForm
		{
			get { return _billingAddressForm ?? (_billingAddressForm = new LekmerAddressForm("BillingAddress", "billingaddress")); }
		}
		public AddressForm DeliveryAddressForm
		{
			get { return _deliveryAddressForm ?? (_deliveryAddressForm = new LekmerAddressForm("DeliveryAddress", "deliveryaddress")); }
		}
		protected FacebookUtility FacebookUtility
		{
			get
			{
				return _facebookUtility ?? (_facebookUtility = new FacebookUtility(IoC.Resolve<IMyAccountSetting>()));
			}
		}
		public ICustomerService CustomerService
		{
			get { return _customerService ?? (_customerService = IoC.Resolve<ICustomerService>()); }
		}
		public IUserService UserService
		{
			get { return _userService ?? (_userService = IoC.Resolve<IUserService>()); }
		}
		public IAddressService AddressService
		{
			get { return _addressService ?? (_addressService = IoC.Resolve<IAddressService>()); }
		}

		public virtual string PostModeValue
		{
			get { return "customerdetail"; }
		}
		public virtual string CustomerIdFormName
		{
			get { return "customer-id"; }
		}
		public virtual string UserNameFormName
		{
			get { return "customer-username"; }
		}
		
		public virtual string OldPasswordFormName
		{
			get { return "customer-oldpassword"; }
		}
		public virtual string NewPasswordFormName
		{
			get { return "customer-newpassword"; }
		}
		public virtual string NewPasswordVerifyFormName
		{
			get { return "customer-newpasswordverify"; }
		}
		public virtual string UseBillingAddressAsDeliveryAddressFormName
		{
			get { return "customer-deliveryaddress-usebillingaddress"; }
		}
		public virtual string ChangeUserNameActionName
		{
			get { return "customer-changeusername-action"; }
		}
		public virtual string ChangePasswordActionName
		{
			get { return "customer-changepassword-action"; }
		}
		public virtual string UpdateCustomerInformationActionName
		{
			get { return "customer-update-action"; }
		}
		public virtual string UserName { get; set; }
		public virtual string CustomerId { get; set; }
		public virtual string OldPassword { get; set; }
		public virtual string NewPassword { get; set; }
		public virtual string NewPasswordVerify { get; set; }
		public virtual IFacebookUser FacebookUser { get; set; }
		public virtual bool IsFacebookUser { get; set; }
		public virtual bool UseBillingAddressAsDeliveryAddress { get; set; }
		public virtual bool IsChangePasswordActionPost
		{
			get { return Request.Form[ChangePasswordActionName] != null; }
		}
		public virtual bool IsChangeUsernameActionPost
		{
			get { return Request.Form[ChangeUserNameActionName] != null; }
		}
		public virtual bool IsUpdateCustomerInformationActionPost
		{
			get { return Request.Form[UpdateCustomerInformationActionName] != null; }
		}

		public virtual string AlternateUserNameFormName
		{
			get { return "customer-alternate-username"; }
		}
		public virtual string AlternatePasswordFormName
		{
			get { return "register-alternate-password"; }
		}
		public virtual string AlternatePasswordVerifyFormName
		{
			get { return "register-alternate-passwordverify"; }
		}
		public virtual string AlternateAcceptTermsFormName
		{
			get { return "register-alternate-accept-terms"; }
		}
		public virtual string AlternateUserName { get; set; }
		public virtual string AlternatePassword { get; set; }
		public virtual string AlternatePasswordVerify { get; set; }
		public virtual string AlternateAcceptTermsFormValue
		{
			get { return "agree"; }
		}
		public virtual bool AlternateAcceptTerms { get; set; }
		public virtual bool HasAlternateLogin { get; set; }
		public virtual bool IsAlternateLogin { get; set; }
		public virtual string AddAlternateLoginActionName
		{
			get { return "customer-addalternatelogin-action"; }
		}
		public virtual bool IsAddAlternateLoginActionPost
		{
			get { return Request.Form[AddAlternateLoginActionName] != null; }
		}
		public virtual string AddAlternateFacebookLoginActionName
		{
			get { return "customer-addalternatefacebooklogin-action"; }
		}
		public virtual bool IsAddAlternateFacebookLoginActionPost
		{
			get { return Request.Form[AddAlternateFacebookLoginActionName] != null; }
		}
		public virtual string DeleteAlternateLoginActionName
		{
			get { return "customer-deletealternatelogin-action"; }
		}
		public virtual bool IsDeleteAlternateLoginActionPost
		{
			get { return Request.Form[DeleteAlternateLoginActionName] != null; }
		}

		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public CustomerDetailForm(string customerDetailPostUrl)
		{
			_customerDetailPostUrl = customerDetailPostUrl;
		}

		public virtual void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.User.CustomerId.Name", CustomerIdFormName);
			fragment.AddVariable("Form.User.UserName.Name", UserNameFormName);
			fragment.AddVariable("Form.User.OldPassword.Name", OldPasswordFormName);
			fragment.AddVariable("Form.User.NewPassword.Name", NewPasswordFormName);
			fragment.AddVariable("Form.User.NewPasswordVerify.Name", NewPasswordVerifyFormName);
			fragment.AddVariable("Form.User.ChangeUserName.Action.Name", ChangeUserNameActionName);
			fragment.AddVariable("Form.User.ChangePassword.Action.Name", ChangePasswordActionName);
			fragment.AddVariable("Form.User.UpdateCustomerInformation.Action.Name", UpdateCustomerInformationActionName);
			fragment.AddVariable("Form.UseBillingAddressAsDeliveryAddress.Name", UseBillingAddressAsDeliveryAddressFormName);

			CustomerInformationForm.MapFieldNamesToFragment(fragment);
			BillingAddressForm.MapFieldNamesToFragment(fragment);
			DeliveryAddressForm.MapFieldNamesToFragment(fragment);

			fragment.AddVariable("Form.User.AlternateUserName.Name", AlternateUserNameFormName);
			fragment.AddVariable("Form.User.AlternatePassword.Name", AlternatePasswordFormName);
			fragment.AddVariable("Form.User.AlternatePasswordVerify.Name", AlternatePasswordVerifyFormName);
			fragment.AddVariable("Form.User.AlternateAcceptTerms.Name", AlternateAcceptTermsFormName);
			fragment.AddVariable("Form.User.AddAlternateLogin.Action.Name", AddAlternateLoginActionName);
			fragment.AddVariable("Form.User.AddAlternateFacebookLogin.Action.Name", AddAlternateFacebookLoginActionName);
			fragment.AddVariable("Form.User.DeleteAlternateLogin.Action.Name", DeleteAlternateLoginActionName);
		}

		public virtual void MapFieldValuesToFragment(Fragment fragment, bool needExtendedInfo)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.User.CustomerId.Value", CustomerId);
			fragment.AddVariable("Form.User.UserName.Value", UserName);
			fragment.AddCondition("Form.User.IsFacebookUser", IsFacebookUser);
			fragment.AddCondition("Form.UseBillingAddressAsDeliveryAddress.Checked", UseBillingAddressAsDeliveryAddress);

			CustomerInformationForm.MapFieldValuesToFragment(fragment);
			BillingAddressForm.MapFieldValuesToFragment(fragment);
			DeliveryAddressForm.MapFieldValuesToFragment(fragment);

			if (needExtendedInfo)
			{
				FacebookUtility.AddUserPictureParameters(fragment, IsFacebookUser ? FacebookUser : null);
			}

			fragment.AddVariable("Form.User.AlternateUserName.Value", AlternateUserName);
			fragment.AddVariable("Form.User.AlternatePassword.Value", AlternatePassword);
			fragment.AddVariable("Form.User.AlternatePasswordVerify.Value", AlternatePasswordVerify);
			fragment.AddVariable("Form.User.AlternateAcceptTerms.Value", AlternateAcceptTermsFormValue);
			fragment.AddCondition("Form.User.AlternateAcceptTerms.Checked", AlternateAcceptTerms);
			fragment.AddCondition("Form.User.HasAlternateLogin", HasAlternateLogin);
			fragment.AddCondition("Form.User.IsAlternateLogin", IsAlternateLogin);
		}

		public virtual void MapToForm(ICustomer customer)
		{
			if (customer == null) throw new ArgumentNullException("customer");

			var lekmerCustomer = (ILekmerCustomer) customer;
			if (customer.User == null && lekmerCustomer.FacebookUser == null) throw new NullReferenceException("User or FacebookUser should exist");

			var user = (ILekmerUser)customer.User;
			FacebookUser = lekmerCustomer.FacebookUser;
			IsFacebookUser = lekmerCustomer.LoginType == CustomerLoginType.FacebookAccount;
			CustomerId = customer.Id.ToString(CultureInfo.InvariantCulture);

			if (IsFacebookUser)
			{
				UserName = FacebookUser.Name;
				IsAlternateLogin = FacebookUser.IsAlternateLogin;

				if (user != null && user.IsAlternateLogin)
				{
					HasAlternateLogin = true;
					AlternateUserName = user.UserName;
				}
			}
			else
			{
				UserName = user.UserName;
				IsAlternateLogin = user.IsAlternateLogin;

				if (FacebookUser != null && FacebookUser.IsAlternateLogin)
				{
					HasAlternateLogin = true;
					AlternateUserName = FacebookUser.Name;
				}
			}

			MapToForm((ILekmerCustomerInformation) customer.CustomerInformation);
		}
		protected virtual void MapToForm(ILekmerCustomerInformation customerInformation)
		{
			if (customerInformation == null) return;

			CustomerInformationForm.MapFromCustomerInformationToForm(customerInformation);

			var billingAddress = customerInformation.DefaultBillingAddress;
			if (billingAddress != null)
			{
				BillingAddressForm.MapFromAddressToForm(billingAddress);
			}

			var deliveryAddress = customerInformation.DefaultDeliveryAddress;
			if (deliveryAddress != null)
			{
				DeliveryAddressForm.MapFromAddressToForm(deliveryAddress);
			}

			UseBillingAddressAsDeliveryAddress = deliveryAddress == null || customerInformation.DefaultBillingAddressId == customerInformation.DefaultDeliveryAddressId;
		}

		public virtual void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			UserName = form[UserNameFormName];
			OldPassword = form[OldPasswordFormName];
			NewPassword = form[NewPasswordFormName];
			NewPasswordVerify = form[NewPasswordVerifyFormName];

			CustomerInformationForm.MapFromRequestToForm();
			BillingAddressForm.MapFromRequestToForm();
			DeliveryAddressForm.MapFromRequestToForm();

			bool useBillingAddressAsDeliveryAddress;
			Boolean.TryParse(form[UseBillingAddressAsDeliveryAddressFormName], out useBillingAddressAsDeliveryAddress);
			UseBillingAddressAsDeliveryAddress = useBillingAddressAsDeliveryAddress;

			AlternateUserName = form[AlternateUserNameFormName];
			AlternatePassword = form[AlternatePasswordFormName];
			AlternatePasswordVerify = form[AlternatePasswordVerifyFormName];
			var accept = Request.Form[AlternateAcceptTermsFormName];
			AlternateAcceptTerms = accept.HasValue() && (accept.ToLower() == "on" || accept.ToLower() == AlternateAcceptTermsFormValue);
		}

		public virtual void MapToCustomer(ICustomer customer)
		{
			if (IsChangeUsernameActionPost)
			{
				customer.User.UserName = UserName;
			}
			else if (IsChangePasswordActionPost)
			{
				customer.User.ClearTextPassword = NewPassword;
			}
			else if (IsUpdateCustomerInformationActionPost)
			{
				MapToCustomerInformation(customer);
				MapToCustomerAddress(customer);
			}
		}
		protected virtual void MapToCustomerInformation(ICustomer customer)
		{
			if (customer.CustomerInformation == null)
			{
				var customerInformationService = IoC.Resolve<ICustomerInformationService>();
				customer.CustomerInformation = customerInformationService.Create(UserContext.Current);
			}

			CustomerInformationForm.MapFromFormToCustomerInformation(customer.CustomerInformation);
		}
		protected virtual void MapToCustomerAddress(ICustomer customer)
		{
			var customerInformation = customer.CustomerInformation;
			customerInformation.DefaultBillingAddress = MapToCustomerAddress(customerInformation.DefaultBillingAddress, BillingAddressForm);

			if (UseBillingAddressAsDeliveryAddress)
			{
				customerInformation.DefaultDeliveryAddress = customerInformation.DefaultBillingAddress;
				customerInformation.DefaultDeliveryAddressId = customerInformation.DefaultBillingAddressId;
			}
			else
			{
				if (customerInformation.DefaultDeliveryAddress != null && customerInformation.DefaultDeliveryAddress.Id == customerInformation.DefaultBillingAddress.Id)
				{
					customerInformation.DefaultDeliveryAddress = null;
				}
				customerInformation.DefaultDeliveryAddress = MapToCustomerAddress(customerInformation.DefaultDeliveryAddress, DeliveryAddressForm);
			}
		}
		protected virtual IAddress MapToCustomerAddress(IAddress address, AddressForm addressForm)
		{
			if (address == null)
			{
				address = AddressService.Create(UserContext.Current);
				address.CountryId = Channel.Current.Country.Id;
			}
			addressForm.MapFromFormToAddress(address);
			return address;
		}

		public virtual void MapToUser(ILekmerUser user, int customerid)
		{
			user.Id = customerid;
			user.UserName = AlternateUserName;
			user.ClearTextPassword = AlternatePassword;
			user.IsAlternateLogin = true;
		}

		public virtual ValidationResult ValidateUserName(int customerId, string userName, string type)
		{
			var validationResult = CustomerValidationUtil.ValidateUserName(userName, type);
			if (validationResult.IsValid && UserNameExists(userName, customerId))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.UserNameAlreadyExists", type)));
			}
			return validationResult;
		}
		protected virtual bool UserNameExists(string userName, int customerId)
		{
			var customer = CustomerService.GetByUserName(UserContext.Current, userName);
			return customer != null && customer.Id != customerId;
		}

		public virtual ValidationResult ValidatePassword(ICustomer customer)
		{
			var validationResult = new ValidationResult();
			ValidateOldPassword(validationResult, customer);
			ValidateNewPassword(validationResult);
			return validationResult;
		}
		protected virtual void ValidateOldPassword(ValidationResult validationResult, ICustomer customer)
		{
			if (OldPassword.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.CustomerDetail.Validation.OldPasswordNotProvided"));
			}
			else
			{
				bool oldPasswordIsValid = UserService.Validate(UserContext.Current, customer.User, OldPassword);
				if (!oldPasswordIsValid)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.CustomerDetail.Validation.InvalidOldPassword"));
				}
			}
		}
		protected virtual void ValidateNewPassword(ValidationResult validationResult)
		{
			validationResult.Errors.Append(CustomerValidationUtil.ValidatePassword(NewPassword, NewPasswordVerify, "CustomerDetail").Errors);
		}

		public virtual ValidationResult ValidateCustomerInformation()
		{
			var validationResult = new ValidationResult();
			validationResult.Errors.Append(CustomerInformationForm.CivicNumber.IsNullOrTrimmedEmpty()
				? CustomerInformationForm.Validate().Errors
				: CustomerInformationForm.Validate(true).Errors);
			validationResult.Errors.Append(((LekmerAddressForm)BillingAddressForm).CustomerDetailAddressValidate().Errors);
			if (!UseBillingAddressAsDeliveryAddress)
			{
				validationResult.Errors.Append(((LekmerAddressForm)DeliveryAddressForm).CustomerDetailAddressValidate().Errors);
			}
			return validationResult;
		}

		public virtual ValidationResult ValidateAlternateLogin(ICustomer customer, string userName, string type)
		{
			var validationResult = ValidateUserName(customer.Id, userName, type);
			validationResult.Errors.Append(CustomerValidationUtil.ValidatePassword(AlternatePassword, AlternatePasswordVerify, type).Errors);
			if (!AlternateAcceptTerms)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue(string.Format("Customer.{0}.Validation.TermsNotAccepted", type)));
			}
			return validationResult;
		}
	}
}