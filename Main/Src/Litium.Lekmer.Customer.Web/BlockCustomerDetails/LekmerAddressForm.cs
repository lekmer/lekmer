using System.Collections.Specialized;
using System.Globalization;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerAddressForm : AddressForm
	{
		private ILekmerCartSetting _cartSetting;
		protected ILekmerCartSetting CartSetting
		{
			get { return _cartSetting ?? (_cartSetting = IoC.Resolve<ILekmerCartSetting>()); }
		}

		public LekmerAddressForm(string formName, string fieldPrefix)
			: base(formName, fieldPrefix)
		{
		}

		public virtual string HouseNumberFormName
		{
			get { return FieldPrefix + "-housenumber"; }
		}

		public virtual string HouseExtensionFormName
		{
			get { return FieldPrefix + "-houseextension"; }
		}

		public virtual string DoorCodeFormName
		{
			get { return FieldPrefix + "-doorcode"; }
		}

		public virtual int DoorCodeMaxLength
		{
			get { return CartSetting.MaxDoorCodeSymbols; }
		}

		public virtual string HouseNumber { get; set; }

		public virtual string HouseExtension { get; set; }

		public virtual string DoorCode { get; set; }
		
		public virtual bool DoorCodeIsAvailable { get; set; }

		public override void MapFromAddressToForm(IAddress address)
		{
			base.MapFromAddressToForm(address);

			var lekmerAddress = address as ILekmerAddress;
			if (lekmerAddress != null)
			{
				HouseNumber = lekmerAddress.HouseNumber;
				HouseExtension = lekmerAddress.HouseExtension;
				DoorCode = lekmerAddress.DoorCode;
			}
			else
			{
				HouseNumber = string.Empty;
				HouseExtension = string.Empty;
				DoorCode = string.Empty;
			}
		}

		public virtual void MapFromProfileToForm(IProfileAddress address)
		{
			Addressee = address.Addressee;
			StreetAddress = address.StreetAddress;
			StreetAddress2 = address.StreetAddress2;
			PostalCode = address.PostalCode;
			City = address.City;
			PhoneNumber = address.PhoneNumber;

			HouseNumber = address.HouseNumber;
			HouseExtension = address.HouseExtension;

			if (DoorCodeIsAvailable)
			{
				DoorCode = address.DoorCode;
			}
		}

		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();

			NameValueCollection form = Request.Form;

			HouseNumber = form[HouseNumberFormName].NullWhenTrimmedEmpty();
			HouseExtension = form[HouseExtensionFormName].NullWhenTrimmedEmpty();
			DoorCode = form[DoorCodeFormName].NullWhenTrimmedEmpty();
		}

		public override void MapFromFormToAddress(IAddress address)
		{
			base.MapFromFormToAddress(address);

			var lekmerAddress = address as ILekmerAddress;
			if (lekmerAddress != null)
			{
				lekmerAddress.HouseNumber = HouseNumber;
				lekmerAddress.HouseExtension = HouseExtension;
				lekmerAddress.DoorCode = DoorCode;
			}
		}

		public virtual void MapFromFormToProfile(IProfileAddress address)
		{
			address.Addressee = Addressee;
			address.StreetAddress = StreetAddress;
			address.StreetAddress2 = StreetAddress2;
			address.PostalCode = PostalCode;
			address.City = City;
			address.PhoneNumber = PhoneNumber;

			address.HouseNumber = HouseNumber;
			address.HouseExtension = HouseExtension;

			if (DoorCodeIsAvailable)
			{
				address.DoorCode = DoorCode;
			}
		}

		public override void MapFieldNamesToFragment(Fragment fragment)
		{
			base.MapFieldNamesToFragment(fragment);

			fragment.AddVariable("Form." + FormName + ".HouseNumber.Name", HouseNumberFormName);
			fragment.AddVariable("Form." + FormName + ".HouseExtension.Name", HouseExtensionFormName);

			fragment.AddCondition("Form." + FormName + ".HouseDetails.IsRequired", ValidationUtil.IsHouseDetailsRequired(UserContext.Current.Channel.Country.Iso));

			fragment.AddVariable("Form." + FormName + ".DoorCode.Name", DoorCodeFormName);
			fragment.AddVariable("Form." + FormName + ".DoorCode.MaxLength", DoorCodeMaxLength.ToString(CultureInfo.InvariantCulture));

			fragment.AddCondition(FormName + ".DoorCode.IsAvailable", DoorCodeIsAvailable);
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			fragment.AddVariable("Form." + FormName + ".HouseNumber.Value", HouseNumber);
			fragment.AddVariable("Form." + FormName + ".HouseExtension.Value", HouseExtension);
			fragment.AddVariable("Form." + FormName + ".DoorCode.Value", DoorCode);
		}

		public override ValidationResult Validate()
		{
			var validationResult = base.Validate();

			ValidateHouseNumber(validationResult);
			ValidateHouseExtension(validationResult);
			if (CartSetting.IsDoorCodeValidate)
			{
				ValidateDoorCode(validationResult);
			}

			return validationResult;
		}
		public virtual ValidationResult CustomerDetailAddressValidate()
		{
			var validationResult = Validate();

			ValidateCellPhone(validationResult);

			return validationResult;
		}

		protected override void ValidatePostalCode(ValidationResult validationResult)
		{
			if (PostalCode.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.PostalCodeNotProvided"));
			}
			else if (!ValidationUtil.IsValidPostalCode(UserContext.Current.Channel.Country.Iso, PostalCode))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.PostalCodeIncorrectFormat"));
			}
		}

		protected virtual void ValidateCellPhone(ValidationResult validationResult)
		{
			if (PhoneNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.CellPhoneNumberNotProvided"));
			}
		}

		protected virtual void ValidateHouseNumber(ValidationResult validationResult)
		{
			if (HouseNumber.IsNullOrTrimmedEmpty())
			{
				if (ValidationUtil.IsHouseNumberRequired(UserContext.Current.Channel.Country.Iso))
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.HouseExtensionNotProvided"));
				}
			}
		}

		protected virtual void ValidateHouseExtension(ValidationResult validationResult)
		{
			if (HouseExtension.IsNullOrTrimmedEmpty())
			{
				if (ValidationUtil.IsHouseExtensionRequired(UserContext.Current.Channel.Country.Iso))
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.HouseExtensionNotProvided"));
				}
			}
		}

		protected virtual void ValidateDoorCode(ValidationResult validationResult)
		{
			if (!DoorCode.HasValue()) return;

			if (DoorCode.Length > DoorCodeMaxLength)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.DoorCodeTooLong"));
			}
			else if (!ValidationUtil.IsValidDoorCode(UserContext.Current.Channel.Country.Iso, DoorCode, CartSetting.GetChannelsThatApplyStarSymbol()))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout." + FormName + ".Validation.DoorCodeIncorrectCharacters"));
			}
		}
	}
}