using System;
using System.Collections.Specialized;
using System.Globalization;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerInformationForm : InformationForm
	{
		public virtual string GenderTypeFormName
		{
			get { return "customer-gender"; }
		}

		public virtual string GenderTypeManFormName
		{
			get { return "customer-gender-man"; }
		}

		public virtual string GenderTypeWomanFormName
		{
			get { return "customer-gender-woman"; }
		}

		public virtual int GenderTypeId { get; set; }

		public override void MapFromCustomerInformationToForm(ICustomerInformation customerInformation)
		{
			base.MapFromCustomerInformationToForm(customerInformation);

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				GenderTypeId = lekmerCustomerInformation.GenderTypeId;
			}
		}
		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();

			NameValueCollection form = Request.Form;

			int genderTypeId;
			GenderTypeId = int.TryParse(form[GenderTypeFormName].NullWhenTrimmedEmpty(), out genderTypeId)
				? genderTypeId
				: (int)GenderTypeInfo.Unknown;
		}
		public override void MapFromFormToCustomerInformation(ICustomerInformation customerInformation)
		{
			if (!Validate().IsValid) throw new ValidationException("Couldn't validate the customer information.");

			customerInformation.FirstName = FirstName;
			customerInformation.LastName = LastName;
			customerInformation.CivicNumber = CivicNumber.HasValue() ? CivicNumber : customerInformation.CivicNumber;
			customerInformation.PhoneNumber = PhoneNumber;
			customerInformation.CellPhoneNumber = CellPhoneNumber;
			customerInformation.Email = Email;
			customerInformation.CreatedDate = DateTime.Now;

			var lekmerCustomerInformation = customerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				lekmerCustomerInformation.GenderTypeId = GenderTypeId;
			}
		}
		public override void MapFieldNamesToFragment(Fragment fragment)
		{
			base.MapFieldNamesToFragment(fragment);

			fragment.AddVariable("Form.CustomerInformation.GenderType.Name", GenderTypeFormName);
			fragment.AddVariable("Form.CustomerInformation.GenderType.ManName", GenderTypeManFormName);
			fragment.AddVariable("Form.CustomerInformation.GenderType.WomanName", GenderTypeWomanFormName);
		}
		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			fragment.AddVariable("Form.CustomerInformation.GenderType.SelectedValue", GenderTypeId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Form.CustomerInformation.GenderType.ManValue", ((int)GenderTypeInfo.Man).ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Form.CustomerInformation.GenderType.WomanValue", ((int)GenderTypeInfo.Woman).ToString(CultureInfo.InvariantCulture));

			fragment.AddCondition("Form.CustomerInformation.GenderType.ManValueIsSelected", GenderTypeId == (int)GenderTypeInfo.Man);
			fragment.AddCondition("Form.CustomerInformation.GenderType.WomanValueIsSelected", GenderTypeId == (int)GenderTypeInfo.Woman);
		}

		public override ValidationResult Validate()
		{
			var validationResult = base.Validate();

			ValidateCellPhone(validationResult);
			ValidateGenderType(validationResult);

			return validationResult;
		}
		public virtual ValidationResult Validate(bool isCivicNumberRequired)
		{
			var validationResult = base.Validate();

			ValidateCellPhone(validationResult);
			ValidateGenderType(validationResult);

			if (isCivicNumberRequired)
			{
				ValidateCivicNumber(validationResult);
			}
			return validationResult;
		}

		protected override void ValidateEmail(ValidationResult validationResult)
		{
			if (Email.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.EmailNotProvided"));
			}
			else if (Email.Length > 320)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.EmailTooLong"));
			}
			else if (!ValidationUtil.IsValidEmail(Email))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.EmailIncorrect"));
			}
		}

		protected virtual void ValidateCellPhone(ValidationResult validationResult)
		{
			if (CellPhoneNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CellPhoneNumberNotProvided"));
			}
		}

		protected virtual void ValidateGenderType(ValidationResult validationResult)
		{
			// - Required, Valid, Not unknown
			// + Required, Valid, Known
			// - Required, Not Valid

			// + Not required, Valid
			// - Not required, Not Valid

			bool isRequired = ValidationUtil.IsGenderTypeRequired(UserContext.Current.Channel.Country.Iso);
			bool isValid = ValidationUtil.IsValidGenderType(GenderTypeId);
			bool isKnown = GenderTypeId != (int)GenderTypeInfo.Unknown;
			bool notProvided = false;

			if (isRequired)
			{
				if (isValid)
				{
					if (!isKnown)
					{
						notProvided = true;
					}
				}
				else
				{
					notProvided = true;
				}
			}
			else
			{
				if (!isValid)
				{
					notProvided = true;
				}
			}

			if (notProvided)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.GenderTypeNotProvided"));
			}
		}

		protected virtual void ValidateCivicNumber(ValidationResult validationResult)
		{
			if (CivicNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CivicNumberNotProvided"));
			}
			else
			{
				var iso = (CountryIso)Enum.Parse(typeof(CountryIso), UserContext.Current.Channel.Country.Iso, true);
				if (!ValidationUtil.IsValidCivicNumber(CivicNumber, iso))
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CivicNumberNotValid"));
				}
			}
		}
	}
}