﻿using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	/// <summary>
	/// Control for rendering products for one or several categories.
	/// </summary>
	public class BlockCategoryProductListControl : BlockControlBase<IBlockCategoryProductList>
	{
		private readonly IBlockCategoryProductListService _blockCategoryProductListService;
		private readonly ILekmerProductService _productService;

		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods"),
		 SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ProductCollection Products { get; set; }

		protected IPagingControl PagingControl { get; set; }

		/// <summary>
		/// Initializes the <see cref="BlockCategoryProductListControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="blockCategoryProductListService"></param>
		/// <param name="productService"></param>
		public BlockCategoryProductListControl(ITemplateFactory templateFactory, IBlockCategoryProductListService blockCategoryProductListService, IProductService productService)
			: base(templateFactory)
		{
			_blockCategoryProductListService = blockCategoryProductListService;
			_productService = (ILekmerProductService) productService;
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		/// <param name="blockId">Id of the block.</param>
		/// <returns>An instance of <see cref="IBlockCategoryProductList"/>.</returns>
		protected override IBlockCategoryProductList GetBlockById(int blockId)
		{
			return _blockCategoryProductListService.GetById(UserContext.Current, blockId);
		}

		protected virtual void Initialize()
		{
			PagingControl = CreatePagingControl();
			Products = GetProducts();
		}

		/// <summary>
		/// Renders the control content.
		/// </summary>
		/// <returns>Rendered <see cref="BlockContent"/>.</returns>
		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		/// <summary>
		/// Get products to list.
		/// </summary>
		/// <returns>Collection of products.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ProductCollection GetProducts()
		{
			return _productService.GetAllByBlock(UserContext.Current, Block, PagingControl.SelectedPage, PagingControl.PageSize);
		}

		/// <summary>
		/// Initializes the paging control.
		/// </summary>
		/// <returns>Instance of <see cref="IPagingControl"/>.</returns>
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Product url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		/// <summary>
		/// Renders the list of products.
		/// </summary>
		/// <returns>Rendered products.</returns>
		protected virtual string RenderProductList()
		{
			return CreateGridControl().Render();
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual GridControl<IProduct> CreateGridControl()
		{
			return new GridControl<IProduct>
			{
				Items = Products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};
		}
	}
}
