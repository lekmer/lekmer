﻿using System.Globalization;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockProductSearchResultEntityMapper : BlockEntityMapper<IBlockProductSearchResult>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductSearchResult item)
		{
			base.AddEntityVariables(fragment, item);
			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}
