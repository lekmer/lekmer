using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockPackageProductListEntityMapper : LekmerBlockEntityMapper<IBlockPackageProductList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockPackageProductList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}