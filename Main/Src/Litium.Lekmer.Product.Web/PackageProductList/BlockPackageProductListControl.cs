﻿using System.Linq;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Product.Web
{
	public class BlockPackageProductListControl : BlockProductPageControlBase<IBlockPackageProductList>
	{
		private readonly IBlockPackageProductListService _blockPackageProductListService;
		private readonly ILekmerProductService _productService;
		private IPagingControl _pagingControl;
		private ProductCollection _products;

		public BlockPackageProductListControl(ITemplateFactory templateFactory, IBlockPackageProductListService blockPackageProductListService, IProductService productService)
			: base(templateFactory)
		{
			_blockPackageProductListService = blockPackageProductListService;
			_productService = (ILekmerProductService) productService;
		}

		protected override IBlockPackageProductList GetBlockById(int blockId)
		{
			return _blockPackageProductListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render package product list control on this page ]");
			}

			if (Product.IsPackage() == false)
			{
				return new BlockContent();
			}

			Initialize();

			if (_products.Count <= 0)
			{
				return new BlockContent();
			}

			_pagingControl.TotalCount = _products.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Block.Title", Block.Title, VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}


		protected virtual void Initialize()
		{
			_pagingControl = CreatePagingControl();

			_products = GetPackageProducts();
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();

			pagingControl.PageBaseUrl = ResolveUrl("~" + System.Web.HttpContext.Current.Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;

			pagingControl.Initialize();

			return pagingControl;
		}

		protected virtual ProductCollection GetPackageProducts()
		{
			ProductIdCollection productIds = _productService.GetIdAllByPackageMasterProduct(UserContext.Current, Product.Id);
			var filteredProductIds = productIds.Skip((_pagingControl.SelectedPage - 1) * _pagingControl.PageSize).Take(_pagingControl.PageSize);
			ProductCollection products = _productService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, new ProductIdCollection(filteredProductIds));
			products.TotalCount = productIds.Count();

			return products;
		}
	}
}