﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockWishListSenderReceiver
{
	public class BlockWishListSenderMultiReceiverControlForm : ControlBase
	{
		private readonly string _postUrl;

		public BlockWishListSenderMultiReceiverControlForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string SenderEmailFormName
		{
			get { return "senderemail"; }
		}
		public string SenderNameFormName
		{
			get { return "sendername"; }
		}

		public string ReceiverFormName
		{
			get { return "receiver"; }
		}
		public string SenderFormName
		{
			get { return "sender"; }
		}
		public string MessageFormName
		{
			get { return "message"; }
		}

		public string MailToMyselfFormName
		{
			get { return "mailtomyself"; }
		}
		public string WishListPageUrlFormName
		{
			get { return "wishlistpageurl"; }
		}
		public string PostUrl
		{
			get { return _postUrl; }
		}
		public string PostModeValue
		{
			get { return "sendwishlist"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string WishListPageUrl { get; set; }
		public Collection<string> Receivers = new Collection<string>();
		public string Message { get; set; }
		public string Sender { get; set; }
		public bool MailToMyself { get; set; }
		public string SenderEmail { get; set; }
		public string SenderName { get; set; }

		public Guid WishListKey { get; set; }

		public void MapFromRequest()
		{
			var receivers = Request.Form[ReceiverFormName];
			if (!string.IsNullOrEmpty(receivers))
			{
				Receivers = new Collection<string>(receivers.Split(','));
			}

			Sender = Request.Form[SenderFormName];
			Message = Request.Form[MessageFormName];
			SenderEmail = Request.Form[SenderEmailFormName];
			SenderName = Request.Form[SenderNameFormName];
			var mailToMyself = Request.Form.GetBooleanOrNull(MailToMyselfFormName);
			if (mailToMyself.HasValue)
			{
				MailToMyself = mailToMyself.Value;
			}
			WishListPageUrl = Request.Form[WishListPageUrlFormName];
		}

		public void ClearFrom()
		{
			Receivers = new Collection<string>();
			Sender = string.Empty;
			Message = string.Empty;
			MailToMyself = false;
		}

		public void MapFieldsToFragment(Template template, Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Receiver.Name", ReceiverFormName);
			fragment.AddVariable("Form.Sender.Name", SenderFormName);
			fragment.AddVariable("Form.Message.Name", MessageFormName);
			fragment.AddVariable("Form.MailToMyself.Name", MailToMyselfFormName);
		}
		public void MapFieldsValueToFragment(Template template, Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Iterate:Receiver", RenderReceivers(template), VariableEncoding.None);
			fragment.AddVariable("Form.Sender.Value", Sender);
			fragment.AddVariable("Form.Message.Value", Message);
			fragment.AddCondition("MailToMyself", MailToMyself);
			fragment.AddVariable("WishListKey", WishListKey.ToString());
			fragment.AddCondition("Form.IsPostBack", IsFormPostBack);
		}

		private string RenderReceivers(Template template)
		{
			var optionBuilder = new StringBuilder();
			foreach (var receiver in Receivers)
			{
				var fragment = template.GetFragment("Option");
				fragment.AddVariable("Form.Receiver.Value", receiver);
				optionBuilder.AppendLine(fragment.Render());
			}
			return optionBuilder.ToString();
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();
			ValidateSenderName(validationResult);
			ValidateEmail(validationResult);
			return validationResult;
		}

		private void ValidateEmail(ValidationResult validationResult)
		{
			if (Message.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Message.Validation.EmailEmpty"));
			}

			if (Receivers == null || Receivers.Count == 0)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Receiver.Validation.EmailEmpty"));
			}
			else
			{
				foreach (var receiver in Receivers)
				{
					if (!ValidationUtil.IsValidEmail(receiver))
					{
						validationResult.Errors.Add(
							AliasHelper.GetAliasValue("WishList.Receiver.Validation.EmailIncorrectForReceiver") + receiver);
					}
				}
			}

			if (SenderEmail.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Sender.Validation.EmailEmpty"));
			}
			else if (!ValidationUtil.IsValidEmail(SenderEmail))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Sender.Validation.EmailIncorrect"));
			}
		}


		public void ValidateSenderName(ValidationResult validationResult)
		{
			if (SenderName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("WishList.Sender.Validation.SenderEmpty"));
			}
		}
	}
}