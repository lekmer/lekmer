﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Media;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockProductCampaignList
{
	public class ProductCampaignListControl : ControlBase, IProductCampaignListControl
	{
		protected IFormatter Formatter { get; private set; }
		protected IContentNodeService ContentNodeService { get; private set; }
		protected ILekmerProductCampaignService ProductCampaignService { get; private set; }
		protected IImageService ImageService { get; private set; }
		protected IMediaUrlService MediaUrlService { get; private set; }
		protected ICampaignLandingPageService CampaignLandingPageService { get; private set; }

		public ILekmerProduct Product { get; set; }
		public Template Template { get; set; }
		public Fragment FragmentContent { get; set; }

		public ProductCampaignListControl(
			IFormatter formatter,
			IContentNodeService contentNodeService,
			IProductCampaignService productCampaignService,
			IImageService imageService,
			IMediaUrlService mediaUrlService,
			ICampaignLandingPageService campaignLandingPageService)
		{
			Formatter = formatter;
			ContentNodeService = contentNodeService;
			ProductCampaignService = (ILekmerProductCampaignService) productCampaignService;
			ImageService = imageService;
			MediaUrlService = mediaUrlService;
			CampaignLandingPageService = campaignLandingPageService;
		}

		public virtual void Render()
		{
			if (Template == null)
			{
				throw new InvalidOperationException("Template must be set.");
			}
			if (FragmentContent == null)
			{
				throw new InvalidOperationException("FragmentContent must be set.");
			}
			if (Product == null)
			{
				FragmentContent.AddVariable("CampaignList", "[ Can't render product campaign list (Product == NULL) ]");
				return;
			}

			var campaigns = ProductCampaignService.GetDistinctCampaigns(UserContext.Current, ((ILekmerProductCampaignInfo) Product.CampaignInfo).CampaignActionsApplied);
			FragmentContent.AddVariable("CampaignList", RenderCampaignList(campaigns), VariableEncoding.None);
		}

		protected virtual string RenderCampaignList(IEnumerable<IProductCampaign> campaignList)
		{
			Fragment fragmentCampaignList = Template.GetFragment("CampaignList");

			var itemBuilder = new StringBuilder();

			foreach (ILekmerProductCampaign campaign in campaignList)
			{
				Fragment fragmentCampaignItem = Template.GetFragment("CampaignItem");

				fragmentCampaignItem.AddEntity(campaign as IProductCampaign);
				fragmentCampaignItem.AddVariable("Campaign.Id", campaign.Id.ToString(CultureInfo.InvariantCulture));
				fragmentCampaignItem.AddCondition("Campaign.HasEndDate", campaign.EndDate.HasValue);

				if (campaign.EndDate.HasValue)
				{
					TimeSpan span = (DateTime)campaign.EndDate - DateTime.Now;
					fragmentCampaignItem.AddVariable("Campaign.EndDate", Formatter.FormatShortDate(UserContext.Current.Channel, campaign.EndDate.Value));
					fragmentCampaignItem.AddVariable("Campaign.EndDateTime", Formatter.FormatDateTime(UserContext.Current.Channel, campaign.EndDate.Value), VariableEncoding.None);
					fragmentCampaignItem.AddVariable("Campaign.DaysRemaining", Math.Floor(span.TotalDays).ToString(CultureInfo.InvariantCulture));
				}
				else
				{
					fragmentCampaignItem.AddVariable("Campaign.EndDate", string.Empty);
					fragmentCampaignItem.AddVariable("Campaign.EndDateTime", string.Empty);
					fragmentCampaignItem.AddVariable("Campaign.DaysRemaining", string.Empty);
				}

				// LandingPage.
				AddLandingPageVariables(fragmentCampaignItem, campaign.Id, campaign.UseLandingPage);

				itemBuilder.Append(fragmentCampaignItem.Render());
			}

			fragmentCampaignList.AddVariable("CampaignItems", itemBuilder.ToString, VariableEncoding.None);

			return fragmentCampaignList.Render();
		}

		protected virtual void AddLandingPageVariables(Fragment fragment, int campaignId, bool campaignUseLandingPage)
		{
			var landingPage = CampaignLandingPageService.GetByCampaignId(UserContext.Current, campaignId);
			if (landingPage != null && landingPage.RegistryDataList != null && landingPage.RegistryDataList.Count > 0)
			{
				var registryData = landingPage.RegistryDataList[0];

				fragment.AddVariable("Campaign.WebTitle", landingPage.WebTitle);
				fragment.AddVariable("Campaign.Description", landingPage.Description);
				fragment.AddCondition("Campaign.HasLandingPage", true);

				var contentNode = ContentNodeService.GetTreeItemById(UserContext.Current, registryData.ContentNodeId);
				if (contentNode != null && contentNode.ContentNode.ContentNodeStatusId == (int)ContentNodeStatusInfo.Online)
				{
					fragment.AddVariable("Campaign.LandingPageUrl", ResolveUrl(contentNode.Url));
				}
				else
				{
					fragment.AddVariable("Campaign.LandingPageUrl", string.Empty);
				}

				var hasIcon = registryData.IconMediaId.HasValue && registryData.IconMediaId > 0;
				fragment.AddCondition("Campaign.HasIcon", hasIcon);
				IImage image = null;
				if (hasIcon)
				{
					image = ImageService.GetById(UserContext.Current, registryData.IconMediaId.Value);
				}

				string originalSizeImageUrl = null;
				string alternativeText = null;

				if (image != null)
				{
					alternativeText = image.AlternativeText;
					string mediaUrl = MediaUrlService.ResolveMediaArchiveUrl(Channel.Current, image);
					originalSizeImageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(mediaUrl, image, landingPage.WebTitle);
				}

				fragment.AddVariable("Campaign.IconUrl", originalSizeImageUrl);
				fragment.AddCondition("Campaign.IconHasAlternativeText", alternativeText.HasValue());
				fragment.AddVariable("Campaign.IconAlternativeText", alternativeText);
			}
			else
			{
				fragment.AddVariable("Campaign.WebTitle", string.Empty);
				fragment.AddVariable("Campaign.Description", string.Empty);
				fragment.AddCondition("Campaign.HasLandingPage", false);
				fragment.AddVariable("Campaign.LandingPageUrl", string.Empty);

				fragment.AddCondition("Campaign.HasIcon", false);
				fragment.AddVariable("Campaign.IconUrl", string.Empty);
				fragment.AddCondition("Campaign.IconHasAlternativeText", false);
				fragment.AddVariable("Campaign.IconAlternativeText", string.Empty);
			}
		}
	}
}