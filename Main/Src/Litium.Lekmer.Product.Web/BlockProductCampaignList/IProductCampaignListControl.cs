﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockProductCampaignList
{
	public interface IProductCampaignListControl
	{
		ILekmerProduct Product { get; set; }
		Template Template { get; set; }
		Fragment FragmentContent { get; set; }

		/// <summary>
		/// Render the component's body into FragmentContent.
		/// </summary>
		void Render();
	}
}
