﻿using Litium.Lekmer.Payment.Klarna;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Web.Helper
{
	public class PaymentHelper
	{
		/// <summary>
		/// Gets the lowest monthly part Payment cost (To be used with "pay from xx amount)
		/// </summary>
		public static double GetLowestPartPayment(IChannel channel, decimal totalOrderCost)
		{
			return KlarnaFactory.CreateKlarnaClient(channel.CommonName).CalculateCheapestMonthlyCost((double)totalOrderCost, true);
		}

		/// <summary>
		/// Checks if Klarna payment provider supported.
		/// </summary>
		public static bool IsKlarnaSupported(IChannel channel)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "nl":
				case "dk":
				case "fi":
				case "no":
				case "se":
					return true;
				default:
					return false;
			}
		}
	}
}