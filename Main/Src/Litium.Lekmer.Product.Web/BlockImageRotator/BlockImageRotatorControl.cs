﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using Litium.Lekmer.Product.Web.BlockImageRotator;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Lekmer.Product.Web
{
	public class BlockImageRotatorControl : BlockControlBase<IBlockImageRotator>
	{
		private readonly IImageService _imageService;
		private readonly IBlockImageRotatorService _blockImageRotatorService;
		private readonly IImageRotatorGroupService _imageRotatorGroupService;
		private readonly IContentNodeService _contentNodeService;
		private bool _useSecondaryTemplate;
		private bool _hasImageRotatorGroups;

		private readonly ITemplateFactory _templateFactory;
		public BlockImageRotatorControl(
			ITemplateFactory templateFactory, IBlockImageRotatorService blockImageRotatorService, IImageRotatorGroupService imageRotatorGroupService, IImageService imageService,
			IContentNodeService contentNodeService)
			: base(templateFactory)
		{
			_templateFactory = templateFactory;
			_blockImageRotatorService = blockImageRotatorService;
			_imageRotatorGroupService = imageRotatorGroupService;
			_imageService = imageService;
			_contentNodeService = contentNodeService;
		}

		protected override IBlockImageRotator GetBlockById(int blockId)
		{
			IBlockImageRotator blockProductFilter = _blockImageRotatorService.GetById(UserContext.Current, blockId);
			return blockProductFilter;
		}

		protected override BlockContent RenderCore()
		{
			Collection<IImageRotatorGroup> imageRotatorGroups = _imageRotatorGroupService.GetByBlockId(UserContext.Current, Block.Id);
			_hasImageRotatorGroups = imageRotatorGroups.Count > 0;

			PopulateFromQuery();

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ImageRotatorGroupList", RenderImageRotatorGroupList(imageRotatorGroups), VariableEncoding.None);
			fragmentContent.AddVariable("ImageRotatorCaptions", RenderImageRotatorCaptions(imageRotatorGroups), VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:ImageRotatorThumbnailGroup", RenderImageRotatorThumbNails(imageRotatorGroups), VariableEncoding.None);
			fragmentContent.AddEntity(Block);
			fragmentContent.AddCondition("HasImageRotatorGroup", _hasImageRotatorGroups);
			fragmentContent.AddCondition("HasSecondaryTemplate", Block.SecondaryTemplateId.HasValue);
			fragmentContent.AddCondition("UseSecondaryTemplate", _useSecondaryTemplate);
			AddTemplateLinks(fragmentContent);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render();
		}

		private void AddTemplateLinks(Fragment fragment)
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			queryBuilder.Remove("usesecondarytemplate");
			string primaryQuery = queryBuilder.ToString();

			queryBuilder.Add("usesecondarytemplate", "true");
			string secondaryQuery = queryBuilder.ToString();

			string baseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());

			fragment.AddVariable("PrimaryTemplateLink", baseUrl + primaryQuery);
			fragment.AddVariable("SecondaryTemplateLink", baseUrl + secondaryQuery);
		}
		private string RenderImageRotatorCaptions(Collection<IImageRotatorGroup> imageRotatorGroups)
		{
			var itemBuilder = new StringBuilder();
			foreach (ImageRotatorGroup imageRotatorGroup in imageRotatorGroups)
			{
				if (!CheckOnValidTime(imageRotatorGroup))
					continue;
				itemBuilder.Append(RenderImageRotatorCaption(imageRotatorGroup));
			}
			return itemBuilder.ToString();
		}
		private string RenderImageRotatorThumbNails(Collection<IImageRotatorGroup> imageRotatorGroups)
		{
			var itemBuilder = new StringBuilder();
			foreach (ImageRotatorGroup imageRotatorGroup in imageRotatorGroups)
			{
				if (!CheckOnValidTime(imageRotatorGroup))
					continue;
				itemBuilder.Append(RenderImageRotatorThumbNail(imageRotatorGroup));
			}
			return itemBuilder.ToString();
		}
		private string RenderImageRotatorGroupList(Collection<IImageRotatorGroup> imageRotatorGroups)
		{
			Fragment fragment = Template.GetFragment("ImageRotatorGroupList");

			var itemBuilder = new StringBuilder();
			foreach (ImageRotatorGroup imageRotatorGroup in imageRotatorGroups)
			{
				if (!CheckOnValidTime(imageRotatorGroup))
					continue;

				itemBuilder.Append(RenderImageRotatorGroup(imageRotatorGroup));
			}
			fragment.AddVariable("Iterate:ImageRotatorGroup", itemBuilder.ToString(), VariableEncoding.None);
			return fragment.Render();
		}

		private bool CheckOnValidTime(ImageRotatorGroup imageRotatorGroup)
		{
			var now = DateTime.Now;
			if (imageRotatorGroup.StartDateTime.HasValue && now < imageRotatorGroup.StartDateTime) return false;
			if (imageRotatorGroup.EndDateTime.HasValue && now > imageRotatorGroup.EndDateTime) return false;
			return true;
		}
		private string RenderImageRotatorGroup(ImageRotatorGroup imageRotatorGroup)
		{
			Fragment fragment = Template.GetFragment("ImageRotatorGroup");
			IImage mainImage = null;
			if (imageRotatorGroup.MainImageId.HasValue)
				mainImage = _imageService.GetById(UserContext.Current, imageRotatorGroup.MainImageId.Value);
			ImageHelper.AddMainImageVariables(fragment, mainImage);

			IImage thumbnailImage = null;
			if (imageRotatorGroup.ThumbnailImageId.HasValue)
				thumbnailImage = _imageService.GetById(UserContext.Current, imageRotatorGroup.ThumbnailImageId.Value);
			ImageHelper.AddThumbnailImageVariables(fragment, thumbnailImage);

			fragment.AddCondition("IsMovie", imageRotatorGroup.IsMovie);
			if (imageRotatorGroup.InternalLinkContentNodeId.HasValue)
			{
				fragment.AddVariable("InternalLink", GetNavigationNodeUrl(imageRotatorGroup.InternalLinkContentNodeId.Value));
				fragment.AddCondition("ExistInternalLink", true);
			}
			else
			{
				fragment.AddCondition("ExistInternalLink", false);
			}

			fragment.AddCondition("OverrideInternalLink", !string.IsNullOrEmpty(imageRotatorGroup.ExternalLink));
			fragment.AddCondition("ExistExternalLink", !string.IsNullOrEmpty(imageRotatorGroup.ExternalLink));
			fragment.AddVariable("ExternalLink", imageRotatorGroup.ExternalLink);

			fragment.AddCondition("IsMovie2", imageRotatorGroup.IsMovie2);
			if (imageRotatorGroup.InternalLinkContentNodeId2.HasValue)
			{
				fragment.AddVariable("InternalLink2", GetNavigationNodeUrl(imageRotatorGroup.InternalLinkContentNodeId2.Value));
				fragment.AddCondition("ExistInternalLink2", true);
			}
			else
			{
				fragment.AddCondition("ExistInternalLink2", false);
			}

			fragment.AddCondition("OverrideInternalLink2", !string.IsNullOrEmpty(imageRotatorGroup.ExternalLink2));
			fragment.AddCondition("ExistExternalLink2", !string.IsNullOrEmpty(imageRotatorGroup.ExternalLink2));
			fragment.AddVariable("ExternalLink2", imageRotatorGroup.ExternalLink2);

			fragment.AddCondition("IsMovie3", imageRotatorGroup.IsMovie3);
			if (imageRotatorGroup.InternalLinkContentNodeId3.HasValue)
			{
				fragment.AddVariable("InternalLink3", GetNavigationNodeUrl(imageRotatorGroup.InternalLinkContentNodeId3.Value));
				fragment.AddCondition("ExistInternalLink3", true);
			}
			else
			{
				fragment.AddCondition("ExistInternalLink3", false);
			}

			fragment.AddCondition("OverrideInternalLink3", !string.IsNullOrEmpty(imageRotatorGroup.ExternalLink3));
			fragment.AddCondition("ExistExternalLink3", !string.IsNullOrEmpty(imageRotatorGroup.ExternalLink3));
			fragment.AddVariable("ExternalLink3", imageRotatorGroup.ExternalLink3);

			fragment.AddVariable("Title", imageRotatorGroup.Title);
			fragment.AddVariable("HtmalMarkup", imageRotatorGroup.HtmalMarkup, VariableEncoding.None);
			return fragment.Render();
		}
		private string RenderImageRotatorCaption(ImageRotatorGroup imageRotatorGroup)
		{

			Fragment fragment = Template.GetFragment("ImageRotatorCaptions");

			IImage mainImage = null;
			if (imageRotatorGroup.MainImageId.HasValue)
				mainImage = _imageService.GetById(UserContext.Current, imageRotatorGroup.MainImageId.Value);
			ImageHelper.AddMainImageVariables(fragment, mainImage);

			fragment.AddVariable("Title", imageRotatorGroup.Title);
			fragment.AddVariable("HtmalMarkup", imageRotatorGroup.HtmalMarkup, VariableEncoding.None);
			return fragment.Render();
		}
		private string RenderImageRotatorThumbNail(ImageRotatorGroup imageRotatorGroup)
		{
			Fragment fragment = Template.GetFragment("ImageRotatorThumbNails");
			IImage thumbnailImage = null;
			if (imageRotatorGroup.ThumbnailImageId.HasValue)
				thumbnailImage = _imageService.GetById(UserContext.Current, imageRotatorGroup.ThumbnailImageId.Value);
			ImageHelper.AddThumbnailImageVariables(fragment, thumbnailImage);
			return fragment.Render();
		}
		private string GetNavigationNodeUrl(int nodeId)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemById(UserContext.Current, nodeId);
			if (contentNodeTreeItem == null || contentNodeTreeItem.Url.IsNullOrEmpty())
			{
				return null;
			}
			return UrlHelper.ResolveUrlHttp(contentNodeTreeItem.Url);
		}

		protected override Template CreateTemplate()
		{
			if (_useSecondaryTemplate && Block.SecondaryTemplateId.HasValue)
			{
				return _templateFactory.Create(Block.SecondaryTemplateId.Value);
			}

			return base.CreateTemplate();
		}

		private void PopulateFromQuery()
		{
			bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
			_useSecondaryTemplate = useSecondaryTemplate ?? false;
		}
	}
}