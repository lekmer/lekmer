﻿using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockBrandList
{
	public class BlockBrandListEntityMapper : LekmerBlockEntityMapper<IBlockBrandList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockBrandList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));

			var linkUrl = string.Empty;
			if (item.LinkContentNodeId.HasValue)
			{
				var contentNode = IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current, item.LinkContentNodeId.Value);
				if (contentNode != null && !string.IsNullOrEmpty(contentNode.Url))
				{
					linkUrl = contentNode.Url;
				}
			}
			fragment.AddCondition("Block.HasNavigationLink", item.LinkContentNodeId.HasValue);
			fragment.AddVariable("Block.NavigationLinkUrl", UrlHelper.ResolveUrl(linkUrl));
		}
	}
}