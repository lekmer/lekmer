﻿using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockMonitorProduct
{
	public class MonitorProductControl : ControlBase
	{
		private readonly ILekmerProduct _product;
		private readonly Fragment _fragment;

		private MonitorProductForm _monitorProductForm;
		private bool _isRegistered;
		private bool _isRegistrationFailed;

		public MonitorProductControl(ILekmerProduct product, Fragment fragment)
		{
			_product = product;
			_fragment = fragment;
		}

		protected MonitorProductForm MonitorProductForm
		{
			get
			{
				if (_product == null || string.IsNullOrEmpty(_product.LekmerUrl))
					return null;

				return _monitorProductForm ??
				       (_monitorProductForm = new MonitorProductForm(UrlHelper.ResolveUrlHttp("~/" + _product.LekmerUrl)));
			}
		}

		public void Render()
		{
			if (MonitorProductForm == null)
				return;

			ValidationResult validationResult = null;

			if (MonitorProductForm.IsFormPostBack)
			{
				MonitorProductForm.MapFromRequest();
				validationResult = MonitorProductForm.Validate();
				if (validationResult.IsValid)
				{
					RegisterMonitorProduct();
				}
			}
			else
			{
				MonitorProductForm.ClearFrom();
			}
			RenderMonitorProductForm(validationResult);
		}

		private void RenderMonitorProductForm(ValidationResult validationResult)
		{
			MonitorProductForm.MapFieldsToFragment(_fragment);
			RenderValidationResult(_fragment, validationResult);
			RenderRegisteredNotification(_fragment);
		}

		private void RenderValidationResult(Fragment fragment, ValidationResult validationResult)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragment.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		private void RenderRegisteredNotification(Fragment fragment)
		{
			var message = string.Empty;
			if (_isRegistered)
			{
				message = AliasHelper.GetAliasValue("Product.MonitorProduct.RegisteredMessage");
			}
			else if (_isRegistrationFailed)
			{
				message = AliasHelper.GetAliasValue("Product.MonitorProduct.RegistrationFailedMessage");
			}
			fragment.AddVariable("RegisteredMessage", message, VariableEncoding.None);
			fragment.AddCondition("WasMonitorProductRegistered", _isRegistered);
		}

		private void RegisterMonitorProduct()
		{
			var monitorProductService = IoC.Resolve<IMonitorProductService>();
			var monitorProduct = monitorProductService.Create(UserContext.Current, _product.Id, MonitorProductForm.Email,
			                                                  MonitorProductForm.SizeId);
			var id = monitorProductService.Save(UserContext.Current, monitorProduct);
			if (id > 0)
			{
				_isRegistered = true;
			}
			else
			{
				_isRegistrationFailed = true;
			}
		}
	}
}