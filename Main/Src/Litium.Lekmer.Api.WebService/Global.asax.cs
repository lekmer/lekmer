﻿using System;
using System.ServiceModel.Activation;
using System.Web.Routing;
using Litium.Lekmer.Api.WebService.TwoSell;

namespace Litium.Lekmer.Api.WebService
{
	public class Global : System.Web.HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			RouteTable.Routes.Add(new ServiceRoute("Suggestions", new WebServiceHostFactory(), typeof(Suggestions)));
		}

		protected void Session_Start(object sender, EventArgs e)
		{

		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{

		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{

		}

		protected void Application_Error(object sender, EventArgs e)
		{

		}

		protected void Session_End(object sender, EventArgs e)
		{

		}

		protected void Application_End(object sender, EventArgs e)
		{

		}
	}
}