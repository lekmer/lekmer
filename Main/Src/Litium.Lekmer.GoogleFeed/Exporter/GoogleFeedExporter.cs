﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using ICSharpCode.SharpZipLib.Zip;
using Litium.Lekmer.Core;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.GoogleFeed
{
	public class GoogleFeedExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IEnumerable<IUserContext> _userContexts;
		private Collection<ICampaign> _productCampaigns;

		protected IGoogleFeedSetting GoogleFeedSetting { get; private set; }
		protected IGoogleFeedService GoogleFeedService { get; private set; }
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ICampaignSecureService CampaignSecureService { get; private set; }

		public GoogleFeedExporter(
			IGoogleFeedSetting googleFeedSetting,
			IGoogleFeedService googleFeedService,
			ILekmerChannelService channelService,
			ICampaignSecureService campaignSecureService)
		{
			GoogleFeedSetting = googleFeedSetting;
			ChannelService = channelService;
			GoogleFeedService = googleFeedService;
			CampaignSecureService = campaignSecureService;
		}

		public virtual void Execute()
		{
			_log.Info("Execution is started.");

			_userContexts = GetUserContextForAllChannels();
			_productCampaigns = CampaignSecureService.GetAllProductCampaigns();

			ExecuteGoogleFeedForChannels();

			_log.Info("Execution is finished.");
		}

		protected virtual void ExecuteGoogleFeedForChannels()
		{
			foreach (IUserContext context in _userContexts)
			{
				var channel = context.Channel.CommonName;
				if (!GoogleFeedSetting.GetChannelNameReplacementExists(channel))
				{
					_log.Info("Skipping channel: " + channel);
					continue;
				}

				Stopwatch stopwatch = Stopwatch.StartNew();

				Collection<IGoogleFeedInfo> googleFeedInfoList = GoogleFeedService.GetGoogleFeedInfoList(context);

				string filePath = GetFilePath(channel);
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}

				_log.InfoFormat("File saving... //{0}", filePath);
				using (Stream stream = File.OpenWrite(filePath))
				{
					var googleFeedXmlFile = new GoogleFeedXmlFile();
					googleFeedXmlFile.Initialize(GoogleFeedSetting, _productCampaigns);
					googleFeedXmlFile.Save(stream, context, googleFeedInfoList);
				}

				ZipFile(filePath);

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", channel, stopwatch.Elapsed);
			}
		}

		protected virtual void ZipFile(string filePath)
		{
			try
			{
				var zipPath = GetArchivePath(filePath);
				if (File.Exists(zipPath))
				{
					File.Delete(zipPath);
				}

				_log.InfoFormat("Zip xml file... {0}", zipPath);
				using (var stream = new ZipOutputStream(File.Create(zipPath)))
				{
					var entry = new ZipEntry(Path.GetFileName(filePath))
					{
						DateTime = DateTime.Now
					};

					stream.PutNextEntry(entry);
					stream.SetLevel(9); // 0 - store only to 9 - means best compression

					using (var fileStream = File.OpenRead(filePath))
					{
						var buffer = new byte[4096];
						int sourceBytes;
						do
						{
							sourceBytes = fileStream.Read(buffer, 0, buffer.Length);
							stream.Write(buffer, 0, sourceBytes);
						} while (sourceBytes > 0);
					}

					stream.Finish();
					stream.Close();
				}

				_log.InfoFormat("File {0} zipped successfully... {0}", zipPath);

				File.Delete(filePath);
			}
			catch (Exception ex)
			{
				_log.ErrorFormat("Exception during zip process... //{0}", ex);
			}
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = ChannelService.GetAll();
			return channels.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
				);
		}

		protected virtual string GetFilePath(string channel)
		{
			string channelName = GoogleFeedSetting.GetChannelNameReplacement(channel);

			string dirr = GoogleFeedSetting.DestinationDirectoryPath;
			string name = GoogleFeedSetting.XmlFileName.Replace("[ChannelName]", channelName);
			return Path.Combine(dirr, name);
		}
		protected virtual string GetArchivePath(string filePath)
		{
			string dirr = GoogleFeedSetting.DestinationDirectoryPath;
			string name = Path.GetFileName(filePath);
			return Path.Combine(dirr, Path.ChangeExtension(name, GoogleFeedSetting.ZipFileExtension));
		}
	}
}