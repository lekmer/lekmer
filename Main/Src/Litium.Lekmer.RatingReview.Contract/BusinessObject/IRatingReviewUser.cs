﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewUser : IBusinessObjectBase
	{
		int Id { get; set; }
		int? CustomerId { get; set; }
		string Token { get; set; }
		DateTime CreatedDate { get; set; }
	}
}