﻿using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockLatestFeedbackListBrand : IBusinessObjectBase
	{
		int BlockId { get; set; }
		IBrand Brand { get; set; }
	}
}