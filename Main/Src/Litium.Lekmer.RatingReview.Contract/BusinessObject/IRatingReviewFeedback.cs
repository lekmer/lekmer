﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewFeedback : IBusinessObjectBase
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		int ProductId { get; set; }
		int? OrderId { get; set; }
		int RatingReviewStatusId { get; set; }
		int LikeHit { get; set; }
		bool Inappropriate { get; set; }
		string IPAddress { get; set; }
		DateTime CreatedDate { get; set; }
		int RatingReviewUserId { get; set; }
	}
}