﻿using Litium.Lekmer.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupFolder : IBusinessObjectBase, IFolder
	{
		int? ParentRatingGroupFolderId { get; set; }
	}
}