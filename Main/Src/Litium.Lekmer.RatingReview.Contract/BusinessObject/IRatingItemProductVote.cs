﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductVote : IBusinessObjectBase
	{
		int RatingReviewFeedbackId { get; set; }
		int RatingId { get; set; }
		int RatingItemId { get; set; }
		IRating Rating { get; set; }
		IRatingItem RatingItem { get; set; }
	}
}