﻿namespace Litium.Lekmer.RatingReview
{
	public enum RatingReviewStatusType
	{
		Pending,
		Approved
	}
}