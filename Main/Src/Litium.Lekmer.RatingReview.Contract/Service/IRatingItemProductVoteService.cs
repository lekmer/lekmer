﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemProductVoteService
	{
		IRatingItemProductVote Insert(IRatingItemProductVote ratingItemProductVote);

		Collection<IRatingItemProductVote> GetAllByFeedback(int ratingReviewFeedbackId);
	}
}