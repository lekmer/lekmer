using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingGroupService
	{
		Collection<IBlockRatingGroup> GetAllByBlock(int blockId);
	}
}