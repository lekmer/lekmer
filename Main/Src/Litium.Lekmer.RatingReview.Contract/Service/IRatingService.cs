﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingService
	{
		IRating GetById(IUserContext context, int ratingId);

		Collection<IRating> GetAllByGroup(IUserContext context, int groupId);
	}
}