﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingSecureService
	{
		IRating Save(ISystemUserFull systemUserFull, IRating rating, Collection<ITranslationGeneric> titleTranslations, Collection<ITranslationGeneric> descriptionTranslations);

		void Delete(ISystemUserFull systemUserFull, int ratingId);

		IRating GetById(int ratingId);

		Collection<IRating> GetAllByGroup(int groupId);

		Collection<IRating> GetAllByFolder(int folderId);

		Collection<IRating> Search(string searchCriteria);

		// Translations.

		void SaveTranslations(ISystemUserFull systemUserFull, int ratingId, int languageId, string title, string description);

		Collection<ITranslationGeneric> GetAllTitleTranslations(int ratingId);

		Collection<ITranslationGeneric> GetAllDescriptionTranslations(int ratingId);
	}
}