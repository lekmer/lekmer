﻿using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IReviewSecureService
	{
		IReview Save(ISystemUserFull systemUserFull, IReview review);
		void Update(ISystemUserFull systemUserFull, IEnumerable<int> feedbackIdList, IEnumerable<string> authorNameList);

		void ReviewHistoryInsert(ISystemUserFull systemUserFull, int reviewId);
	}
}