using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductFeedbackSecureService
	{
		IBlockProductFeedback Create();

		IBlockProductFeedback GetById(int id);

		int Save(ISystemUserFull systemUserFull, IBlockProductFeedback block);
	}
}