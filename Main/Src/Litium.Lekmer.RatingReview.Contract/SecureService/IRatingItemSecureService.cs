﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItemSecureService
	{
		IRatingItem Save(ISystemUserFull systemUserFull, IRatingItem ratingItem);

		void Delete(ISystemUserFull systemUserFull, int ratingItemId);

		void DeleteAll(ISystemUserFull systemUserFull, int ratingId);

		Collection<IRatingItem> GetAllByRating(int ratingId);

		// Translations.

		void SaveTranslations(ISystemUserFull systemUserFull, int ratingItemId, int languageId, string title, string description);

		Collection<ITranslationGeneric> GetAllTitleTranslations(int ratingItemId);

		Collection<ITranslationGeneric> GetAllDescriptionTranslations(int ratingItemId);
	}
}