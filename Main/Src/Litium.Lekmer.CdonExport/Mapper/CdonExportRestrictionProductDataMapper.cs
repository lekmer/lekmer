﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Mapper
{
	public class CdonExportRestrictionProductDataMapper : DataMapperBase<ICdonExportRestrictionProduct>
	{
		public CdonExportRestrictionProductDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICdonExportRestrictionProduct Create()
		{
			var cdonExportRestrictionProduct = IoC.Resolve<ICdonExportRestrictionProduct>();

			cdonExportRestrictionProduct.ProductRegistryId = MapValue<int>("ProductRegistryId");
			cdonExportRestrictionProduct.ProductId = MapValue<int>("ProductId");
			cdonExportRestrictionProduct.RestrictionReason = MapNullableValue<string>("RestrictionReason");
			cdonExportRestrictionProduct.UserId = MapNullableValue<int?>("UserId");
			cdonExportRestrictionProduct.CreatedDate = MapValue<DateTime>("CreatedDate");
			cdonExportRestrictionProduct.ChannelId = MapValue<int>("ChannelId");

			return cdonExportRestrictionProduct;
		}
	}
}