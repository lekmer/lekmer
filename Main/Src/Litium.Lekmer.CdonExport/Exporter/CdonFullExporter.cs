﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.CdonExport
{
	public class CdonFullExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected ICdonExportSetting CdonExportSetting { get; private set; }
		protected ICdonExportService CdonExportService { get; private set; }

		public CdonFullExporter(ICdonExportSetting cdonExportSetting, ICdonExportService cdonExportService)
		{
			CdonExportSetting = cdonExportSetting;
			CdonExportService = cdonExportService;

			CdonExportService.Log = _log;
		}

		public void Execute()
		{
			_log.Info("Execution FULL is started.");

			ExecuteCdonExportInfo();

			_log.Info("Execution FULL is complited.");
		}

		protected virtual void ExecuteCdonExportInfo()
		{
			string filePath = GetFilePath();
			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			int cdonExportId;
			using (Stream stream = File.OpenWrite(filePath))
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.Info("Reading data from database.");

				var cdonExportXmlFile = new CdonExportXmlFile();
				cdonExportXmlFile.Initialize(CdonExportSetting, CdonExportService, CdonExportType.FULL);

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time to read data: {0}", stopwatch.Elapsed);

				_log.InfoFormat("File saving... //{0}", filePath);
				cdonExportId = cdonExportXmlFile.Save(stream);
				_log.Info("File saved");
			}

			bool isExportFinished = true;
			if (CdonExportSetting.NeedToValidate)
			{
				_log.InfoFormat("Xml file will be validated by xsd... //{0}", CdonExportSetting.XsdFilePath);
				var xmlFileValidator = new XmlFileValidator();
				bool valid = xmlFileValidator.Validate(filePath, CdonExportSetting.XsdFilePath);
				if (!valid)
				{
					isExportFinished = false;
					CdonExportService.Update(cdonExportId, CdonExportStatus.InvalidXml);
					_log.ErrorFormat("Xml file is not valid: {0}", xmlFileValidator.GetError());
				}
				else
				{
					_log.InfoFormat("Xml file is valid... //{0}", filePath);
				}
			}

			if (isExportFinished)
			{
				CdonExportService.Update(cdonExportId, CdonExportStatus.Finished);

				try
				{
					var zipName = GetZipPath(filePath);

					_log.InfoFormat("Zip xml file... {0}", zipName);

					var zipHelper = new ZipHelper();
					zipHelper.ZipFile(filePath, zipName);

					_log.InfoFormat("File {0} zipped successfully... {0}", zipName);

					File.Delete(filePath);
				}
				catch (Exception ex)
				{
					_log.ErrorFormat("Exception during zip process... //{0}", ex);
				}
			}
		}

		protected virtual string GetFilePath()
		{
			var dateTime = DateTime.Now;

			string dirr = CdonExportSetting.DestinationDirectory;
			string name = string.Format(CdonExportSetting.FileNameFullExport, dateTime.ToString("yyyyMMdd-HHmmss"));

			return Path.Combine(dirr, name);
		}

		protected virtual string GetZipPath(string filePath)
		{
			string dirr = CdonExportSetting.ZipFileDirectory;
			string fileName = Path.GetFileName(filePath);

			return Path.Combine(dirr, Path.ChangeExtension(fileName, CdonExportSetting.ZipFileExtension));
		}
	}
}