﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionCategorySecureService : ICdonExportRestrictionCategorySecureService
	{
		private const char DELIMITER = ',';

		protected CdonExportRestrictionCategoryRepository Repository { get; private set; }

		public CdonExportRestrictionCategorySecureService(CdonExportRestrictionCategoryRepository repository)
		{
			Repository = repository;
		}

		public void InsertIncludeCategory(ICdonExportRestrictionItem category, List<int> productChannelIds)
		{
			Repository.InsertIncludeCategory(category, Scensum.Foundation.Convert.ToStringIdentifierList(productChannelIds), DELIMITER);
		}

		public void InsertRestrictionCategory(ICdonExportRestrictionItem category, List<int> productChannelIds)
		{
			Repository.InsertRestrictionCategory(category, Scensum.Foundation.Convert.ToStringIdentifierList(productChannelIds), DELIMITER);
		}

		public void DeleteIncludeCategories(List<int> categoryIds)
		{
			Repository.DeleteIncludeCategories(Scensum.Foundation.Convert.ToStringIdentifierList(categoryIds), DELIMITER);
		}

		public void DeleteRestrictionCategories(List<int> categoryIds)
		{
			Repository.DeleteRestrictionCategories(Scensum.Foundation.Convert.ToStringIdentifierList(categoryIds), DELIMITER);
		}

		public Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			return Repository.GetIncludeAllSecure();
		}

		public Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			return Repository.GetRestrictionAllSecure();
		}
	}
}