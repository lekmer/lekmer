using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonProductInfo : ICdonProductInfo
	{
		private readonly Dictionary<int, int> _channelDict = new Dictionary<int, int>();
		private readonly Collection<ICdonChannelProductInfo> _channelProductInfoCollection = new Collection<ICdonChannelProductInfo>();
		private readonly Collection<IProductImageGroupFull> _productImagesCollection = new Collection<IProductImageGroupFull>();
		private readonly Collection<ICdonIcon> _productIconsCollection = new Collection<ICdonIcon>();
		private readonly Collection<IProductSize> _productSizesCollection = new Collection<IProductSize>();
		private readonly Dictionary<int, ICdonProductInfo> _productVariantsCollection = new Dictionary<int, ICdonProductInfo>();

		public int ProductId { get; set; }
		public string VariantErpId { get; set; }

		public bool Available
		{
			get { return _channelProductInfoCollection.Count > 0; }
		}

		public ICdonChannelProductInfo DefaultProductInfo
		{
			get { return _channelProductInfoCollection[0]; }
		}

		public Collection<ICdonChannelProductInfo> ChannelProductInfoCollection
		{
			get { return _channelProductInfoCollection; }
		}

		public Collection<IProductImageGroupFull> ProductImagesCollection
		{
			get { return _productImagesCollection; }
		}

		public Collection<ICdonIcon> ProductIconsCollection
		{
			get { return _productIconsCollection; }
		}

		public Collection<IProductSize> ProductSizesCollection
		{
			get { return _productSizesCollection; }
		}

		public Dictionary<int, ICdonProductInfo> ProductVariantsCollection
		{
			get { return _productVariantsCollection; }
		}

		public void Add(IChannel channel, ILekmerProductView product)
		{
			if (!_channelDict.ContainsKey(channel.Id))
			{
				var cdonChannelProductInfo = IoC.Resolve<ICdonChannelProductInfo>();

				cdonChannelProductInfo.Channel = channel;
				cdonChannelProductInfo.Product = product;

				_channelDict[channel.Id] = channel.Id;
				_channelProductInfoCollection.Add(cdonChannelProductInfo);
			}
		}
	}
}