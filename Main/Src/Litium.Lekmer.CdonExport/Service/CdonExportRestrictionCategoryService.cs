﻿using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRestrictionCategoryService : ICdonExportRestrictionCategoryService
	{
		protected CdonExportRestrictionCategoryRepository Repository { get; private set; }

		public CdonExportRestrictionCategoryService(CdonExportRestrictionCategoryRepository repository)
		{
			Repository = repository;
		}

		public Collection<ICdonExportRestrictionItem> GetIncludeAll()
		{
			return Repository.GetIncludeAll();
		}

		public Collection<ICdonExportRestrictionItem> GetRestrictionAll()
		{
			return Repository.GetRestrictionAll();
		}
	}
}