﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro.Repository
{
	public class QliroTransactionRepository
	{
		public virtual int CreateGetAddress(string clientRef, QliroTransactionType transactionType, string civicNumber, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.CreateGetAddress");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ClientRef", clientRef, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CivicNumber", civicNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pQliroTransactionCreateGetAddress]", parameters, dbSettings);
		}

		public virtual int CreateGetPaymentTypes(string clientRef, QliroTransactionType transactionType, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.CreateGetPaymentTypes");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ClientRef", clientRef, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pQliroTransactionCreatePaymentTypes]", parameters, dbSettings);
		}

		public virtual int CreateReservation(string clientRef, int mode, QliroTransactionType transactionType, string civicNumber, int orderId, decimal amount, string currencyCode, string paymentType, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.CreateReservation");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ClientRef", clientRef, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Mode", mode, SqlDbType.Int),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("CivicNumber", civicNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Amount", amount, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("CurrencyCode", currencyCode, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("PaymentType", paymentType, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pQliroTransactionCreateReservation]", parameters, dbSettings);
		}

		public virtual int CreateCheckOrderStatus(string clientRef, QliroTransactionType transactionType, int orderId, string reservationNumber, DateTime createdDate)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.CreateCheckOrderStatus");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ClientRef", clientRef, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("TransactionTypeId", transactionType, SqlDbType.Int),
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReservationNumber", reservationNumber, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", createdDate, SqlDbType.DateTime)
				};

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pQliroTransactionCreateCheckOrderStatus]", parameters, dbSettings);
		}

		public virtual void SaveResult(IQliroResult qliroResult)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.SaveResult");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", qliroResult.TransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusCode", qliroResult.StatusCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReturnCodeId", qliroResult.ReturnCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("Message", qliroResult.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CustomerMessage", qliroResult.CustomerMessage, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", qliroResult.Duration, SqlDbType.BigInt),
					ParameterHelper.CreateParameter("ResponseContent", qliroResult.ResponseContent, SqlDbType.NVarChar)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pQliroTransactionSaveResult]", parameters, dbSettings);
		}

		public virtual void SaveReservationResponse(IQliroReservationResult qliroReservationResult)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.SaveReservationResponse");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", qliroReservationResult.TransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusCode", qliroReservationResult.StatusCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReservationNumber", qliroReservationResult.ReservationId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ReturnCodeId", qliroReservationResult.ReturnCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("InvoiceStatus", qliroReservationResult.InvoiceStatus, SqlDbType.Int),
					ParameterHelper.CreateParameter("Message", qliroReservationResult.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CustomerMessage", qliroReservationResult.CustomerMessage, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", qliroReservationResult.Duration, SqlDbType.BigInt)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pQliroTransactionSaveReservationResponse]", parameters, dbSettings);
		}

		public virtual void SaveCheckOrderStatusResponse(IQliroGetStatusResult qliroGetStatusResult)
		{
			var dbSettings = new DatabaseSetting("QliroTransactionRepository.SaveCheckOrderStatusResponse");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", qliroGetStatusResult.TransactionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusCode", qliroGetStatusResult.StatusCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReturnCodeId", qliroGetStatusResult.ReturnCode, SqlDbType.Int),
					ParameterHelper.CreateParameter("InvoiceStatus", qliroGetStatusResult.InvoiceStatus, SqlDbType.Int),
					ParameterHelper.CreateParameter("Message", qliroGetStatusResult.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Duration", qliroGetStatusResult.Duration, SqlDbType.BigInt)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pQliroTransactionSaveCheckOrderStatusResponse]", parameters, dbSettings);
		}
	}
}
