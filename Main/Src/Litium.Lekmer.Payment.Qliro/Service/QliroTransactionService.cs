﻿using System;
using System.Reflection;
using Litium.Lekmer.Payment.Qliro.Repository;
using log4net;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroTransactionService : IQliroTransactionService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected QliroTransactionRepository Repository { get; private set; }

		public QliroTransactionService(QliroTransactionRepository repository)
		{
			Repository = repository;
		}


		public virtual int CreateGetAddress(string clientRef, string civicNumber)
		{
			try
			{
				return Repository.CreateGetAddress(clientRef, QliroTransactionType.GetAddress, civicNumber, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateGetPaymentTypes(string clientRef)
		{
			try
			{
				return Repository.CreateGetPaymentTypes(clientRef, QliroTransactionType.GetPaymentTypes, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateReservation(string clientRef, int mode, string civicNumber, int orderId, decimal amount, string currencyCode, string paymentType)
		{
			try
			{
				return Repository.CreateReservation(clientRef, mode, QliroTransactionType.ReserveAmount, civicNumber, orderId, amount, currencyCode, paymentType, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateReservationTimeout(string clientRef, int mode, string civicNumber, int orderId, decimal amount, string currencyCode, string paymentType)
		{
			try
			{
				return Repository.CreateReservation(clientRef, mode, QliroTransactionType.ReserveAmountTimeout, civicNumber, orderId, amount, currencyCode, paymentType, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateCheckOrderStatus(string clientRef, int orderId, string reservationNumber)
		{
			try
			{
				return Repository.CreateCheckOrderStatus(clientRef, QliroTransactionType.CheckOrderStatus, orderId, reservationNumber, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}


		public virtual void SaveResult(IQliroResult qliroResult)
		{
			try
			{
				if (qliroResult.TransactionId > 0)
				{
					Repository.SaveResult(qliroResult);
				}
				else
				{
					_log.Error("SaveResult: QliroTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		public virtual void SaveReservationResponse(IQliroReservationResult qliroReservationResult)
		{
			try
			{
				if (qliroReservationResult.TransactionId > 0)
				{
					Repository.SaveReservationResponse(qliroReservationResult);
				}
				else
				{
					_log.Error("SaveReservationResponse: QliroTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		public virtual void SaveCheckOrderStatusResponse(IQliroGetStatusResult qliroGetStatusResult)
		{
			try
			{
				if (qliroGetStatusResult.TransactionId > 0)
				{
					Repository.SaveCheckOrderStatusResponse(qliroGetStatusResult);
				}
				else
				{
					_log.Error("SaveCheckOrderStatusResponse: QliroTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}
	}
}
