﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Payment.Qliro.Cache;
using Litium.Lekmer.Payment.Qliro.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroPaymentTypeService : IQliroPaymentTypeService
	{
		protected QliroPaymentTypeRepository Repository { get; private set; }

		public QliroPaymentTypeService(QliroPaymentTypeRepository repository)
		{
			Repository = repository;
		}

		public Collection<IQliroPaymentType> GetAllByChannel(IChannel channel)
		{
			if (channel == null)
			{
				throw new ArgumentNullException("channel");
			}

			return QliroPaymentTypesCache.Instance.TryGetItem(
				new QliroPaymentTypeKey(channel.Id),
				() => Repository.GetAllByChannel(channel.Id));
		}
	}
}
