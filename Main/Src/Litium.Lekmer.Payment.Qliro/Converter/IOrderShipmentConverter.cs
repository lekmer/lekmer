﻿using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IOrderShipmentConverter
	{
		invoiceItemType Convert(ILekmerOrderFull orderFull, bool taxFreeZone, decimal vatPercentage);
	}
}
