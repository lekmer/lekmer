﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class OrderPackageConverter : IOrderPackageConverter
	{
		protected IPackageOrderItemConverter PackageOrderItemConverter { get; set; }

		public OrderPackageConverter(IPackageOrderItemConverter packageOrderItemConverter)
		{
			PackageOrderItemConverter = packageOrderItemConverter;
		}

		public Collection<invoiceItemType> Convert(ILekmerOrderItem orderItem, bool taxFreeZone)
		{
			if (orderItem.PackageOrderItems == null)
			{
				throw new InvalidOperationException("lekmerOrderItem.PackageOrderItems can't be null when it is package");
			}

			var invoiceItems = new Collection<invoiceItemType>();

			string rowDescription = orderItem.Title;
			decimal vatPercent = orderItem.GetOriginalVatPercent();
			decimal packageDiscountIncludingVat = orderItem.ActualPrice.IncludingVat;
			decimal packageDiscountExcludingVat = orderItem.ActualPrice.ExcludingVat;

			foreach (IPackageOrderItem packageOrderItem in orderItem.PackageOrderItems)
			{
				invoiceItemType invoiceItem = PackageOrderItemConverter.Convert(packageOrderItem, taxFreeZone);
				invoiceItems.Add(invoiceItem);

				packageDiscountIncludingVat -= packageOrderItem.OriginalPrice.IncludingVat;
				packageDiscountExcludingVat -= packageOrderItem.OriginalPrice.ExcludingVat;
			}

			if (taxFreeZone) // ExclVat
			{
				vatPercent = 0m;
				packageDiscountIncludingVat = packageDiscountExcludingVat;
			}

			string erpId = orderItem.OrderItemSize.SizeId.HasValue ? orderItem.OrderItemSize.ErpId : orderItem.ErpId + "-000";

			var invoiceItemDiscount = new invoiceItemType
			{
				itemNo = erpId,
				description = rowDescription,
				itemPrice = packageDiscountExcludingVat, // item price (excl. VAT)
				grossAmount = packageDiscountIncludingVat * orderItem.Quantity, // item price (incl. VAT) * item count
				netAmount = packageDiscountExcludingVat * orderItem.Quantity, // item price (excl. VAT) * item count
				vatPercent = vatPercent,
				quantity = orderItem.Quantity,
				unit = null,
				discountAmount = 0.0m,
				feeType = debtFeeType.None
			};

			invoiceItems.Add(invoiceItemDiscount);

			return invoiceItems;
		}
	}
}
