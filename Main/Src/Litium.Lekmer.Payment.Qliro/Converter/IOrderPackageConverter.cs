﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IOrderPackageConverter
	{
		Collection<invoiceItemType> Convert(ILekmerOrderItem orderItem, bool taxFreeZone);
	}
}
