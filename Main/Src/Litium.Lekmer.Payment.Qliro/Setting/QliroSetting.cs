﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Qliro.Setting
{
	public class QliroSetting : SettingBase, IQliroSetting
	{
		private const string _storageName = "Qliro";

		private string _groupName;

		protected override string StorageName
		{
			get { return _storageName; }
		}

		protected virtual string GroupName
		{
			get { return _groupName; }
		}


		public string Url
		{
			get { return GetString(GroupName, "Url"); }
		}

		public string UserName
		{
			get { return GetString(GroupName, "UserName"); }
		}

		public string Password
		{
			get { return GetString(GroupName, "Password"); }
		}

		public string ClientRef
		{
			get { return GetString(GroupName, "ClientRef"); }
		}

		public bool IgnoreCertificateValidation
		{
			get { return GetBoolean(GroupName, "IgnoreCertificateValidation"); }
		}

		public string ProductionStatus
		{
			get { return GetString(GroupName, "ProductionStatus"); }
		}

		public string CommunicationChannel
		{
			get { return GetString(GroupName, "CommunicationChannel"); }
		}

		public string SpecialPaymentTypeCode
		{
			get { return GetString(GroupName, "SpecialPaymentTypeCode"); }
		}

		public bool Use7SecRule
		{
			get { return GetBoolean(GroupName, "Use7SecRule"); }
		}

		public int DefaultResponseTimeout
		{
			get { return GetInt32(GroupName, "DefaultResponseTimeout"); }
		}

		public int Customer7SecResponseTimeout
		{
			get { return GetInt32(GroupName, "Customer7SecResponseTimeout"); }
		}

		public int Service7SecResponseTimeout
		{
			get { return GetInt32(GroupName, "Service7SecResponseTimeout"); }
		}


		public virtual void Initialize(string channelCommonName)
		{
			_groupName = channelCommonName;
		}
	}
}