using Litium.Scensum.Customer;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroAddress : Address, IQliroAddress
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public bool IsCompany { get; set; }
		public string CompanyName { get; set; }
	}
}