﻿namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroReservationResult : QliroResult, IQliroReservationResult
	{
		public string ReservationId { get; set; }
		public QliroInvoiceStatus InvoiceStatus { get; set; }
	}
}