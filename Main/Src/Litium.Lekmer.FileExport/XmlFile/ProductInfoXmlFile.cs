﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport
{
	public class ProductInfoXmlFile: BaseXmlFile
	{
		private IUserContext _context;

		private Dictionary<string, IProductImageGroup> _imageGroups;
		private Collection<IImageSize> _imageSizes;
		private Dictionary<string, ITagGroup> _tagGroups;
		private Dictionary<string, string> _categoryLevels;
		private Dictionary<int, ICampaign> _campaigns;
		private bool _imagesInitialized;
		private bool _isHeppoAplication;
		private bool _isLekmerAplication;
		private bool _applicationTypeInitialized;
		private bool _tagGroupsInitialized;
		private bool _categoryLevelsInitialized;
		private bool _campaignsInitialized;
		private string _mediaUrl;


		public void Save(Stream savedSiteMap, IUserContext context, IEnumerable<IProductExportInfo> products)
		{
			if (!_imagesInitialized)
			{
				throw new InvalidOperationException("InitializeImages() should be called first.");
			}

			if (!_applicationTypeInitialized)
			{
				throw new InvalidOperationException("InitializeApplicationType() should be called first.");
			}

			if (!_tagGroupsInitialized)
			{
				throw new InvalidOperationException("InitializeTagGrpups() should be called first.");
			}

			if (!_categoryLevelsInitialized)
			{
				throw new InvalidOperationException("InitializeCategoryLevels() should be called first.");
			}

			if (!_campaignsInitialized)
			{
				throw new InvalidOperationException("InitializeCampaigns() should be called first.");
			}
			
			_context = context;
			_mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveExternalUrl(_context.Channel);

			var doc = CreateXmlDocument();

			XmlNode productsNode = AddProductsNode(doc);
			XmlNode countryNode = AddCountryNodes(doc, productsNode);

			foreach (IProductExportInfo productExportInfo in products)
			{
				AddProductNodes(doc, countryNode, productExportInfo);
			}

			doc.Save(savedSiteMap);
		}

		public void InitializeImages(Collection<IProductImageGroup> imageGroups, Collection<IImageSize> imageSizes)
		{
			if (imageGroups == null)
			{
				throw new ArgumentNullException("imageGroups");
			}
			if (imageSizes == null)
			{
				throw new ArgumentNullException("imageSizes");
			}

			_imageGroups = imageGroups.ToDictionary(g => g.CommonName);
			_imageSizes = imageSizes;
			_imagesInitialized = true;
		}

		public void InitializeTagGroups(Collection<ITagGroup> tagGroups)
		{
			if (tagGroups == null)
			{
				throw new ArgumentNullException("tagGroups");
			}

			_tagGroups = tagGroups.ToDictionary(g => g.CommonName);
			_tagGroupsInitialized = true;
		}

		public void InitializeCategoryLevels(Dictionary<string, string> categoryLevels)
		{
			if (categoryLevels == null)
			{
				throw new ArgumentNullException("categoryLevels");
			}

			_categoryLevels = categoryLevels;
			_categoryLevelsInitialized = true;
		}

		public void InitializeCampaigns(Collection<ICampaign> campaigns)
		{
			if (campaigns == null)
			{
				throw new ArgumentNullException("campaigns");
			}

			_campaigns = campaigns.ToDictionary(c => c.Id);
			_campaignsInitialized = true;
		}

		public void InitializeApplicationType(string applicationName)
		{
			_isHeppoAplication = applicationName == ExporterConstants.HeppoApplication;
			_isLekmerAplication = applicationName == ExporterConstants.LekmerApplication;
			
			_applicationTypeInitialized = true;
		}


		protected XmlNode AddProductsNode(XmlDocument xmlDocument)
		{
			// Products
			XmlNode productsNode = xmlDocument.CreateElement(ExporterConstants.NodeProducts);

			// Products:export_date
			XmlAttribute atributeExportDate = xmlDocument.CreateAttribute(ExporterConstants.NodeExportDate);
			atributeExportDate.Value = DateTime.Now.ToString(CultureInfo.InvariantCulture);
			if (productsNode.Attributes != null)
			{
				productsNode.Attributes.Append(atributeExportDate);
			}

			xmlDocument.AppendChild(productsNode);

			return productsNode;
		}

		protected XmlNode AddCountryNodes(XmlDocument xmlDocument, XmlNode parentNode)
		{
			// Countries
			XmlNode countries = xmlDocument.CreateElement(ExporterConstants.NodeCountries);
			parentNode.AppendChild(countries);

			// Country
			XmlNode country = xmlDocument.CreateElement(ExporterConstants.NodeCountry);

			// Country:Iso
			XmlAttribute attributeIso = xmlDocument.CreateAttribute(ExporterConstants.NodeIso);
			attributeIso.Value = _context.Channel.Country.Iso;
			if (country.Attributes != null)
			{
				country.Attributes.Append(attributeIso);
			}

			countries.AppendChild(country);

			return country;
		}

		protected void AddProductNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Product
			XmlNode productNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeProduct);
			// Product:Type
			AddAttribute(xmlDocument, productNode, ExporterConstants.NodeType, productExportInfo.CategoryTitle);

			// ID
			AddNode(xmlDocument, productNode, ExporterConstants.NodeId, productExportInfo.Product.ErpId);

			// Product-Id
			AddNode(xmlDocument, productNode, ExporterConstants.NodeProductId, productExportInfo.Product.Id.ToString(CultureInfo.InvariantCulture));

			// EAN
			AddEanNode(xmlDocument, productNode, productExportInfo);

			// Brand
			string brandTitle = ExporterConstants.ValueMissing;
			if (productExportInfo.Product.Brand != null && productExportInfo.Product.Brand.Title.HasValue())
			{
				brandTitle = productExportInfo.Product.Brand.Title;
			}
			AddNode(xmlDocument, productNode, ExporterConstants.NodeBrand, brandTitle);

			// Title
			AddNode(xmlDocument, productNode, ExporterConstants.NodeTitle, productExportInfo.Product.DisplayTitle);

			// Gender
			AddGenderNode(xmlDocument, productNode, productExportInfo);

			// Supplier
			AddSupplierNode(xmlDocument, productNode, productExportInfo);

			// Price
			// Modify price depending on active campaign
			decimal priceIncludingVat = productExportInfo.Product.IsPriceAffectedByCampaign
				? productExportInfo.Product.CampaignInfo.Price.IncludingVat
				: productExportInfo.Product.Price.PriceIncludingVat;
			string priceText = priceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);

			XmlNode productPriceNode = AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodePrice, priceText);
			// Price:Currency
			AddAttribute(xmlDocument, productPriceNode, ExporterConstants.NodeCurrency, _context.Channel.Currency.Iso);

			// Original Price
			AddOriginalPriceNode(xmlDocument, productNode, productExportInfo);

			// Link
			string link = "http://" + _context.Channel.ApplicationName + "/" + productExportInfo.Product.LekmerUrl;
			AddNode(xmlDocument, productNode, ExporterConstants.NodeLink, link);

			// In stock
			string inStock = productExportInfo.Product.TotalNumberInStock > 0 ? "yes" : "no";
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeInStock, inStock);

			// Freight
			AddFreightNode(xmlDocument, productNode, productExportInfo, priceIncludingVat);
			
			// Delivery time
			string deliveryTimeDays = _context.Channel.Currency.Iso.Equals("SEK") ? "1-3" : "3-5";
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeDeliveryTimeDays, deliveryTimeDays);

			// Description
			AddNode(xmlDocument, productNode, ExporterConstants.NodeDescription, productExportInfo.Product.Description);
						
			// ColorNode
			AddProductColorNode(xmlDocument, productNode, productExportInfo);
			
			// OnPromotion
			AddOnPromotionNode(xmlDocument, productNode, productExportInfo);
			
			// PromotionName - CampaignNameNode
			AddCampaignNode(xmlDocument, productNode, productExportInfo);

			// Main Category
			AddMainCategoryNode(xmlDocument, productNode, productExportInfo);

			// Category
			AddCategoryNode(xmlDocument, productNode, productExportInfo);

			// SubCategory eg Sneakers
			AddSubCategoryNode(xmlDocument, productNode, productExportInfo);
			
			// CategoryUrlsNode
			AddCategoryUrlsNode(xmlDocument, productNode, productExportInfo);

			// Images
			AddProductImagesNodes(xmlDocument, productNode, productExportInfo);
		}

		private void AddSubCategoryNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			if (_isLekmerAplication)
			{
				AddNode(xmlDocument, productNode, ExporterConstants.NodeSubCategory, productExportInfo.CategoryTitle);
			}
		}

		private void AddCategoryNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			if (_isLekmerAplication)
			{
				AddNode(xmlDocument, productNode, ExporterConstants.NodeCategory, productExportInfo.ParentCategoryTitle);
			}
		}

		private void AddMainCategoryNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			if (_isLekmerAplication)
			{
				AddNode(xmlDocument, productNode, ExporterConstants.NodeMainCategory, productExportInfo.MainCategoryTitle);
			}
		}

		private void AddOnPromotionNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			if (_isLekmerAplication)
			{
				string promotion = productExportInfo.Product.CampaignInfo != null ? "true" : "false";
				AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeOnPromotion, promotion);
			}
		}

		protected virtual void AddEanNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Heppo has only few EAN codes, so do not need it for Heppo
			if (_isLekmerAplication)
			{
				string eanCode = ExporterConstants.ValueMissing;
				if (!productExportInfo.Product.EanCode.IsEmpty())
				{
					eanCode = productExportInfo.Product.EanCode;
				}
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeEan, eanCode);
			}
		}

		protected virtual void AddGenderNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			if (_isHeppoAplication)
			{
				//TODO: Check if it is ok to use parent category title?
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeGender, productExportInfo.ParentCategoryTitle);
			}
		}

		protected virtual void AddSupplierNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			if (_isLekmerAplication)
			{
				string supplierNumber = productExportInfo.Product.LekmerErpId;
				if (supplierNumber.IsEmpty())
				{
					supplierNumber = ExporterConstants.ValueMissing;
				}
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeArtNo, supplierNumber.Trim().Replace(Environment.NewLine, string.Empty));
			}
		}

		protected virtual void AddFreightNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo, decimal priceIncludingVat)
		{
			if (_isLekmerAplication)
			{
				string cost = priceIncludingVat > 1000 ? "0.00" : "49.00";

				//if (_context.Channel.Currency.Iso.Equals("SEK"))
				//{
				//	cost = priceIncludingVat > 1000 ? "0.00" : "39.00"; //Schenker
				//}

				if (_context.Channel.Country.Iso.Equals("FI"))
				{
					cost = priceIncludingVat > 100 ? "0.00" : "4.90";
				}

				if (_context.Channel.Country.Iso.Equals("NL"))
				{
					cost = priceIncludingVat > 100 ? "0.00" : "2.90";
				}

				XmlNode productFreightNode = AddNodeRaw(xmlDocument, parentNode, ExporterConstants.NodeFreightCost, cost);
				AddAttribute(xmlDocument, productFreightNode, ExporterConstants.NodeCurrency, _context.Channel.Currency.Iso);
			}
		}

		protected virtual void AddOriginalPriceNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			// Price
			string priceText = productExportInfo.Product.Price.PriceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);

			XmlNode productPriceNode = AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeOriginalPrice, priceText);
			// Price:Currency
			AddAttribute(xmlDocument, productPriceNode, ExporterConstants.NodeCurrency, _context.Channel.Currency.Iso);
			
		}

		protected virtual void AddProductImagesNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Images
			XmlNode productImagesNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeImages);

			// Default Image
			if (productExportInfo.Product.Image != null)
			{
				XmlNode imageNode = AddNode(xmlDocument, productImagesNode, ExporterConstants.NodeDefaultImage);

				AddProductImageNodes(xmlDocument, imageNode, productExportInfo.Product.Image);
			}

			// Images from groups
			if (productExportInfo.ImageGroups != null)
			{
				foreach (IProductImageGroupFull imageGroup in productExportInfo.ImageGroups)
				{
					if (_imageGroups.ContainsKey(imageGroup.CommonName))
					{
						if (imageGroup.ProductImages != null && imageGroup.ProductImages.Count > 0)
						{
							XmlNode groupNode = AddNode(xmlDocument, productImagesNode, ExporterConstants.NodeGroup);
							AddAttribute(xmlDocument, groupNode, ExporterConstants.NodeCommonName, imageGroup.CommonName);

							foreach (IProductImage productImage in imageGroup.ProductImages)
							{
								XmlNode imageNode = AddNode(xmlDocument, groupNode, ExporterConstants.NodeImage);

								AddProductImageNodes(xmlDocument, imageNode, productImage.Image);
							}
						}
					}
				}
			}
			
			/*
			<images>
				<default>
					<image CommonName="" Url="">
						<origin>Url<>
						<size CommonName="">Url<>
					<>
				<>
				<group CommonName="">
					<image CommonName="" Url="">
						<origin>Url<>
						<size CommonName=""><>
					<>
				<>
			<>
			*/
		}

		protected virtual void AddProductImageNodes(XmlDocument xmlDocument, XmlNode parentNode, IImage image)
		{
			string imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, image);

			AddNode(xmlDocument, parentNode, ExporterConstants.NodeOriginal, imageUrl);

			foreach (IImageSize imageSize in _imageSizes)
			{
				string imageSizeUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(_mediaUrl, image, imageSize.CommonName);

				XmlNode sizeNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeSize, imageSizeUrl);
				AddAttribute(xmlDocument, sizeNode, ExporterConstants.NodeCommonName, imageSize.CommonName);
			}

			/*
			<image CommonName="" Url="">
				<origin>Url<>
				<size CommonName=""><>
			<>
			*/
		}


		protected virtual void AddProductColorNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			string color = "none";

			// tag from groups
			if (productExportInfo.Product != null && productExportInfo.Product.TagGroups != null)
			{
				foreach (ITagGroup tagGroup in productExportInfo.Product.TagGroups)
				{
					if (_tagGroups.ContainsKey(tagGroup.CommonName))
					{
						if (tagGroup.Tags != null && tagGroup.Tags.Count > 0)
						{
							color = tagGroup.Tags[0].Value;
						}
					}
				}
			}

			// Color
			if (!color.Equals("none"))
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeColor, color);
		}

		protected virtual void AddCampaignNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			if (_isLekmerAplication)
			{
				string campaignName = string.Empty;

				var campaignInfo = (ILekmerProductCampaignInfo) productExportInfo.Product.CampaignInfo;

				if (campaignInfo != null && campaignInfo.CampaignActionsApplied != null && campaignInfo.CampaignActionsApplied.Count > 0)
				{
					var campaignIds = campaignInfo.CampaignActionsApplied.GroupBy(a => a.ProductCampaignId).Select(c => c.First().ProductCampaignId);

					foreach (int campaignId in campaignIds)
					{
						if (_campaigns.ContainsKey(campaignId))
						{
							campaignName += _campaigns[campaignId].Title + ", ";
						}
					}
				}

				int lastCommaPosition = campaignName.LastIndexOf(",", StringComparison.Ordinal);
				if (lastCommaPosition > 0)
				{
					campaignName = campaignName.Substring(0, lastCommaPosition);
				}

				// CampaignName
				if (!string.IsNullOrEmpty(campaignName))
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.NodePromotionName, campaignName);
				}
			}
		}

		protected virtual void AddCategoryUrlsNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			if (_categoryLevels.ContainsKey(ExporterConstants.AllCategoryLevel))
			{
				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CustomUrl1, productExportInfo.FirstCategoryLevelUrl); // customUrl1
				}

				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CategoryUrl, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}

				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.SubCategoryUrl, productExportInfo.ThirdCategoryLevelUrl); //subCategoryUrl
				}

				return;
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.FirstCategoryLevel))
			{
				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CustomUrl1, productExportInfo.FirstCategoryLevelUrl); // customUrl1
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.SecondCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CategoryUrl, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.ThirdCategoryLevel))
			{
				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.SubCategoryUrl, productExportInfo.ThirdCategoryLevelUrl); //subCategoryUrl
				}
			}
		}
	}
}