using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Template;

namespace Litium.Lekmer.Template.Cache
{
	public sealed class IncludeCache : ScensumCacheBase<IncludeKey, IInclude>
	{
		private static readonly IncludeCache _instance = new IncludeCache();

		private IncludeCache()
		{
		}

		public static IncludeCache Instance
		{
			get { return _instance; }
		}
	}

	public class IncludeKey : ICacheKey
	{
		public IncludeKey(string commonName)
		{
			CommonName = commonName;
		}

		public string CommonName { get; set; }

		public string Key
		{
			get { return "cn_" + CommonName; }
		}
	}
}