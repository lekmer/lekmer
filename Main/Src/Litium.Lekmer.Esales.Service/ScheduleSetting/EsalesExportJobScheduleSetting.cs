﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Esales.Service
{
	public class EsalesExportJobScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "Esales"; }
		}

		protected override string GroupName
		{
			get { return "EsalesExportJob"; }
		}
	}
}