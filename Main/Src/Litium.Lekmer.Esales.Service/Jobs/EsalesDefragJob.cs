﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Esales.Connector;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Service
{
	public class EsalesDefragJob : BaseJob
	{
		public override string Name
		{
			get { return "EsalesDefragmentation"; }
		}

		protected override void ExecuteAction()
		{
			var esalesConnector = IoC.Resolve<IEsalesConnector>();
			esalesConnector.Defragmentation();
		}
	}
}
