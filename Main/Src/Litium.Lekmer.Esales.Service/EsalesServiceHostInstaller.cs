﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;


namespace Litium.Lekmer.Esales.Service
{
	[RunInstaller(true)]
	public partial class EsalesServiceHostInstaller : System.Configuration.Install.Installer
	{
		public EsalesServiceHostInstaller()
		{
			InitializeComponent();
		}
	}
}
