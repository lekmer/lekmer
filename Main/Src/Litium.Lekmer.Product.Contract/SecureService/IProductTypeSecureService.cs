﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductTypeSecureService
	{
		Collection<IProductType> GetAll();

		Collection<int> GetIdAllByBlock(int blockId);
		void SaveByBlock(ISystemUserFull systemUserFull, int blockId, Collection<int> productTypeIds);
		void DeleteByBlock(ISystemUserFull systemUserFull, int blockId);
	}
}
