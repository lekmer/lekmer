﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IBatteryTypeSecureService
	{
		Collection<IBatteryType> GetAll();
	}
}
