﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using System.Collections.Generic;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProductSecureService
	{
		void Save(ISystemUserFull systemUserFull,
			IProductRecord product,
			Collection<IRelationList> relationLists,
			IProductSeoSetting seoSetting,
			Collection<IProductImageGroupFull> imageGroups,
			IEnumerable<IStoreProduct> storeProductItems,
			Collection<ITranslationGeneric> webShopTitleTranslations,
			Collection<ITranslationGeneric> shortDescriptionTranslations,
			Collection<ITranslationGeneric> descriptionTranslations,
			Collection<ITranslationGeneric> seoTitleTranslations,
			Collection<ITranslationGeneric> seoDescriptionTranslations,
			Collection<ITranslationGeneric> seoKeywordsTranslations,
			Collection<ITranslationGeneric> measurementTranslations,
			Collection<int> icons,
			Collection<int> tagsToRemove,
			Collection<int> tagsToAdd,
			IEnumerable<IProductSize> productSizes,
			IEnumerable<IProductUrl> productUrls,
			Collection<IProductRegistryRestrictionItem> registryRestrictions,
			Collection<IRecommendedPrice> recommendedPrices);

		void UpdatePopularity(Collection<IChannel> channels, DateTime dateTimeFrom, DateTime dateTimeTo);

		ProductIdCollection GetVariantIdAllByProduct(int productId);

		ProductCollection GetAllByPackage(int channelId, int packageId);

		ProductIdCollection GetIdAllByCampaign(int campaignId);

		Collection<IProductRecord> GetProductRecordAllByIdList(IEnumerable<int> productIds);

		Dictionary<string, int> GetProductIdsByHyIdList(IEnumerable<string> productHyIds);

		Dictionary<string, decimal> GetPriceByChannelAndCampaign(int currencyId, decimal priceLimit, IFixedPriceAction action);

		bool IsProductDeleted(int productId);


		#region Import Product Info

		string UpdateProductInfo(IImportProductInfo importProductInfo, int channelId, int languageId, string userName);
		void RemoveOldProductInfoImportLogs(int days);

		#endregion
	}
}