﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IAgeSecureService
	{
		Collection<IAge> GetAll();
	}
}
