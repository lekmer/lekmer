using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface ICategoryTagGroupSecureService
	{
		void SaveAll(int categoryId, IEnumerable<int> tagGroupIds);

		void DeleteAll(int categoryId);

		Collection<ICategoryTagGroup> GetAllByCategory(int categoryId);
	}
}