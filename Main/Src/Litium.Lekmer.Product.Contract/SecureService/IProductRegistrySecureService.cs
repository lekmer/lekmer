﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistrySecureService
	{
		Collection<IProductRegistry> GetAll();
		/// <summary>
		/// Gets all product registries except ignored.
		/// </summary>
		/// <param name="productRegistryIdsToIgnore">Comma separated list of ids to ignore.</param>
		/// <returns></returns>
		Collection<IProductRegistry> GetAll(string productRegistryIdsToIgnore);
	}
}