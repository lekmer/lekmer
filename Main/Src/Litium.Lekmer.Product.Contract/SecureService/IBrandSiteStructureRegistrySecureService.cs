using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBrandSiteStructureRegistrySecureService
	{
		IBrandSiteStructureRegistry Create();
		Collection<IBrandSiteStructureRegistry> GetAllByBrand(int brandId);
		Collection<IBrandSiteStructureRegistry> GetAll();
		void Save(ISystemUserFull systemUserFull, IBrandSiteStructureRegistry registry);
		void DeleteByContentNode(ISystemUserFull systemUserFull, int contentNodeId);
		void DeleteByBrand(ISystemUserFull systemUserFull, int contentNodeId);
		
	}
}