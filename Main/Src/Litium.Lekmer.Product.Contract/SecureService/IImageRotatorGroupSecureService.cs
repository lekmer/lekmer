﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IImageRotatorGroupSecureService
	{
		IImageRotatorGroup Create();
		IImageRotatorGroup GetById(ISystemUserFull systemUserFull, int id);
		Collection<IImageRotatorGroup> GetByBlockId(ISystemUserFull systemUserFull, int blockId);
		void Save(ISystemUserFull systemUserFull, int imageRotatorGroupId, Collection<IImageRotatorGroup> imageRotatorGroups);

		void Save(IImageRotatorGroup imageRotatorGroup);
	}
}
