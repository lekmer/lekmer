﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistryRestrictionBrandSecureService
	{
		IProductRegistryRestrictionItem Create();

		void Save(ISystemUserFull systemUserFull, int brandId, Collection<IProductRegistryRestrictionItem> productRegistryRestrictionBrandList);

		void Delete(ISystemUserFull systemUserFull, int brandId);

		Collection<IProductRegistryRestrictionItem> GetAllByBrand(int brandId);

		Collection<IProductRegistryRestrictionItem> GetAllWithChannel();
	}
}