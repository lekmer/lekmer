using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IPriceIntervalBand : IBusinessObjectBase
	{
		decimal? From { get; set; }
		decimal? To { get; set; }
	}
}