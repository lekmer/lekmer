﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerCategoryTree : ICategoryTree
	{
		ICategoryTreeItem FindItemByTitle(string title);
	}
}