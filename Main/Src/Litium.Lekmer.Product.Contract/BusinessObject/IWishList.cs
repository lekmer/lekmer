﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IWishList : IBusinessObjectBase
	{
		int Id { get; set; }
		Guid Key { get; set; }
		string Title { get; set; }

		//Methods

		/// <summary>
		/// Return sorted (by Ordinal ascending) not deleted (IsDeleted = false) wish list items.
		/// </summary>
		IEnumerable<IWishListItem> GetItems();

		/// <summary>
		/// Add new wish list item
		/// </summary>
		bool AddItem(IWishListItem wishListItem);

		/// <summary>
		/// Set IsDeleted to true for wish list item
		/// </summary>
		void DeleteItem(int itemId);

		/// <summary>
		/// Set IsDeleted to true for wish list item
		/// </summary>
		void DeleteItem(int productId, int? sizeId, int? packageItemsHashCode);

		/// <summary>
		/// Change the Ordinal property for the wish list item and re-sort all items
		/// </summary>
		void SetItemOrdinal(int itemId, int ordinal);

		/// <summary>
		/// Change the Ordinal property for the wish list item and re-sort all items
		/// </summary>
		void SetItemOrdinal(int productId, int? sizeId, int? packageItemsHashCode, int ordinal);

		/// <summary>
		/// Update product size of wish list item.
		/// </summary>
		bool UpdateProductSize(int itemId, int productId, int sizeId);

		/// <summary>
		/// Update package items sizes of wish list item.
		/// </summary>
		bool UpdatePackageSize(int itemId, List<string> productIds, List<string> sizeIds, out IWishListItem wishListItem);

		//Extra fields for internal analysis
		DateTime? CreatedDate { get; set; }
		DateTime? UpdatedDate { get; set; }
		string IpAddress { get; set; }
		int? NumberOfRemovedItems { get; set; }
	}
}