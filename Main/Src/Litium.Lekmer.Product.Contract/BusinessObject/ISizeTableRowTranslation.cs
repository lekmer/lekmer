﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableRowTranslation : IBusinessObjectBase
	{
		int SizeTableRowId { get; set; }
		int LanguageId { get; set; }
		string Column1Value { get; set; }
		string Column2Value { get; set; }
	}
}