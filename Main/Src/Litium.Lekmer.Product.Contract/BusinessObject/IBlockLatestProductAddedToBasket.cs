﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockLatestProductAddedToBasket : IBlock
	{
		bool RedirectToStartPage { get; set; }
		bool RedirectToMainCategory { get; set; }
		bool RedirectToCategory { get; set; }
	}
}
