using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductTagHash
	{
		void Add(Collection<IProductTag> productTags);
		void Add(int channelId, Collection<ITagGroup> tagGroups);

		Collection<ITagGroup> GetProductTags(int channelId, int productId);
		Dictionary<int, Collection<ITag>> GetChannelTags();

		bool ContainProductTags();
		bool ContainChannel(int channelId);
	}
}