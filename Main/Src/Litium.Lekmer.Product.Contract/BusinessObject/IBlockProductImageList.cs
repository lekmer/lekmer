﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductImageList : IBlock
	{
		int? ProductImageGroupId { get; set; }
		IBlockSetting Setting { get; set; }
	}
}
