namespace Litium.Lekmer.Product
{
	public interface IProductTag
	{
		int TagId { get; set; }
		int ProductId { get; set; }
	}
}