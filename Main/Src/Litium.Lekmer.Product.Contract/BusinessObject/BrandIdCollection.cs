﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BrandIdCollection : Collection<int>
	{
		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// 
		/// </summary>
		public int TotalCount { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Litium.Lekmer.Product.Contract.BrandIdCollection"/> class that is empty.
		/// </summary>
		public BrandIdCollection()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Litium.Lekmer.Product.Contract.BrandIdCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param><exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public BrandIdCollection(IList<int> list)
			: base(list)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Litium.Lekmer.Product.Contract.BrandIdCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param><exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public BrandIdCollection(IEnumerable<int> list)
			: base(new List<int>(list))
		{
		}
	}
}