﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductMedia : IBusinessObjectBase
	{
		int ProductId { get; set; }
		int MediaId { get; set; }
		int GroupId { get; set; }
		int Ordinal { get; set; }
		int FormatId { get; set; }
		string Title { get; set; }
		string FileName { get; set; }
		string Extension { get; set; }

	}
}