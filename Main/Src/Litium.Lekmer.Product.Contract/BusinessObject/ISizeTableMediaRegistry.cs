﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableMediaRegistry : IBusinessObjectBase
	{
		int Id { get; set; }
		int SizeTableMediaId { get; set; }
		int ProductRegistryId { get; set; }
	}
}