﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductChangeEvent : IBusinessObjectBase
	{
		int Id { get; set; }
		int ProductId { get; set; }
		int EventStatusId { get; set; }
		int CdonExportEventStatusId { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime ActionAppliedDate { get; set; }
		string Reference { get; set; }
	}
}
