﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductSearchResultService
	{
		IBlockProductSearchResult GetById(IUserContext context, int id);
	}
}
