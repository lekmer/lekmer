using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ISearchFacet
	{
		// Brands.
		BrandCollection AllBrandsWithoutFilter { get; set; }
		Dictionary<int, int> BrandsWithFilter { get; set; }

		// Categories.
		CategoryCollection AllMainCategoriesWithoutFilter { get; set; }
		Dictionary<int, int> MainCategoriesWithFilter { get; set; }
		CategoryCollection AllParentCategoriesWithoutFilter { get; set; }
		Dictionary<int, int> ParentCategoriesWithFilter { get; set; }
		CategoryCollection AllCategoriesWithoutFilter { get; set; }
		Dictionary<int, int> CategoriesWithFilter { get; set; }

		// Age & Price.
		IAgeIntervalBand AgeInterval { get; set; }
		IPriceIntervalBand PriceInterval { get; set; }

		// Sizes.
		Collection<ISize> AllSizesWithoutFilter { get; set; }
		Dictionary<int, int> SizesWithFilter { get; set; }

		// Tags.
		Dictionary<int, int> TagsWithFilter { get; set; }
	}
}