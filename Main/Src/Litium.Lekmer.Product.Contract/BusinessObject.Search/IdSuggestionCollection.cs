﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class IdSuggestionCollection : Collection<IIdSuggestionInfo>
	{
		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// 
		/// </summary>
		public int TotalCount { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Litium.Lekmer.Product.IdCollection"/> class that is empty.
		/// </summary>
		public IdSuggestionCollection()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Litium.Lekmer.Product.IdCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param><exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public IdSuggestionCollection(IList<IIdSuggestionInfo> list)
			: base(list)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Litium.Lekmer.Product.IdCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param><exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public IdSuggestionCollection(IEnumerable<IIdSuggestionInfo> list)
			: base(new List<IIdSuggestionInfo>(list))
		{
		}
	}
}