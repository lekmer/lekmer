namespace Litium.Lekmer.Product
{
	public interface IBrandSuggestionInfo: IIdSuggestionInfo
	{
		IBrand Brand { get; set; }
	}
}