using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ITagService
	{
		Collection<ITag> GetAll(IUserContext context);
		Collection<ITag> GetAllByTagGroup(IUserContext context, int tagGroupId);
		Collection<ITag> GetAllByProduct(IUserContext context, int productId);
		Collection<ITag> GetAllByTagGroupAndProduct(IUserContext context, int tagGroupId, int productId);

		IdCollection GetAllIdsByCategory(int categoryId);

		void SaveProductTag(int productId, int tagId, int objectId);
		Collection<int> GetAllProductTagIdsByCampaign(int campaignId);
		void DeleteProductTagsUsage(int objectId);
		void DeleteProductTagsByIds(Collection<int> productTagIdList);
	}
}