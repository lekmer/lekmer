using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IProductImageGroupService
	{
		Collection<IProductImageGroup> GetAll();
		Collection<IProductImageGroupFull> GetAllFullByProduct(int productId);

		TProductImageGroup Clone<TProductImageGroup>(TProductImageGroup sourceProductImageGroup) where TProductImageGroup : IProductImageGroup;
		Collection<TProductImageGroup> Clone<TProductImageGroup>(Collection<TProductImageGroup> sourceProductImageGroups) where TProductImageGroup : IProductImageGroup;
	}
}