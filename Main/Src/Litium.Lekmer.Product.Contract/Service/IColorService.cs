using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IColorService
	{
		Collection<IColor> GetAll(IUserContext context);
	}
}