﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductStoreStockService
	{
		Collection<IProductStoreStock> GetAllByProduct(int productId);
	}
}
