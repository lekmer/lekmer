﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IWishListItemService
	{
		IWishListItem Create();
		Collection<IWishListItem> GetAllByWishList(IUserContext context, int wishListId);
		int Save(IUserContext context, IWishListItem item);
		void Delete(IUserContext context, IWishListItem item);
	}
}