﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using Litium.Lekmer.Common.Job;
using log4net;

namespace Litium.Lekmer.Cache.Cleaner.Service
{
	public partial class CacheCleanerHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private Collection<IJob> _jobs = new Collection<IJob>();

		public CacheCleanerHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			_log.Info("Service stopped.");
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var job = new CacheCleanerJob { ScheduleSetting = new CacheCleanerScheduleSetting(), IntervalCalculator = intervalCalculator };
			_jobs.Add(job);
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}
