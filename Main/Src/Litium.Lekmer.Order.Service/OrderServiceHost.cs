﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using System.ServiceProcess;
using Litium.Lekmer.Common.Job;
using log4net;

namespace Litium.Lekmer.Order.Service
{
	public partial class OrderServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private object _syncToken = new object();
		private Collection<IJob> _jobs = new Collection<IJob>();

		public OrderServiceHost()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			try
			{
				_log.Info("Starting service.");

				InitializeJobs();

				foreach (IJob job in _jobs)
				{
					job.StartJob();
				}

				_log.Info("Service started successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Failed to start service.", ex);
				Stop();
			}
		}

		protected override void OnStop()
		{
			try
			{
				foreach (IJob job in _jobs)
				{
					job.StopJob();
				}

				_log.Info("Service stopped successfuly.");
			}
			catch (Exception ex)
			{
				_log.Error("Error occurred while stopping service.", ex);
			}
		}

		private void InitializeJobs()
		{
			IIntervalCalculator intervalCalculator = new IntervalCalculator();

			var klarnaPendingOrderJob = new KlarnaPendingOrderJob { ScheduleSetting = new KlarnaPendingOrderScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(klarnaPendingOrderJob);

			var maksuturvaPendingOrderJob = new MaksuturvaPendingOrderJob { ScheduleSetting = new MaksuturvaPendingOrderScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(maksuturvaPendingOrderJob);

			var qliroPendingOrderJob = new QliroPendingOrderJob { ScheduleSetting = new QliroPendingOrderScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(qliroPendingOrderJob);

			var qliroTimeoutOrderJob = new QliroTimeoutOrderJob { ScheduleSetting = new QliroTimeoutOrderScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(qliroTimeoutOrderJob);

			var collectorTimeoutOrderJob = new CollectorTimeoutOrderJob { ScheduleSetting = new CollectorTimeoutOrderScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(collectorTimeoutOrderJob);

			var klarnaDeletedOrderJob = new KlarnaDeletedOrderJob { ScheduleSetting = new KlarnaDeletedOrderScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(klarnaDeletedOrderJob);

			var topListProductJob = new TopListProductJob { ScheduleSetting = new TopListProductScheduleSetting(), IntervalCalculator = intervalCalculator, SyncToken = _syncToken };
			_jobs.Add(topListProductJob);
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}