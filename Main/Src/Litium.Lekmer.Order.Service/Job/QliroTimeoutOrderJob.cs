﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Service
{
	public class QliroTimeoutOrderJob : BaseJob
	{
		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "QliroTimeoutOrderJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<IQliroTimeoutOrderService>().ManageQliroTimeoutOrders();
		}
	}
}
