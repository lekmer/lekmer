﻿using System.Reflection;
using Litium.Lekmer.Common.Job;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Order.Service
{
	public class KlarnaDeletedOrderJob : BaseJob
	{
		private ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private KlarnaDeletedOrderScheduleSetting _klarnaDeletedOrderScheduleSetting;
		protected KlarnaDeletedOrderScheduleSetting KlarnaDeletedOrderScheduleSetting
		{
			get { return _klarnaDeletedOrderScheduleSetting ?? (_klarnaDeletedOrderScheduleSetting = new KlarnaDeletedOrderScheduleSetting()); }
		}

		private readonly ISystemUserSecureService _systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
		private readonly ILekmerOrderSecureService _orderSecureService = (ILekmerOrderSecureService)IoC.Resolve<IOrderSecureService>();

		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "KlarnaDeletedOrderJob"; }
		}

		protected override void ExecuteAction()
		{
			var systemUser = _systemUserSecureService.GetFullByUserName(KlarnaDeletedOrderScheduleSetting.SystemUserName);
			if (systemUser == null)
			{
				_log.ErrorFormat("Cannot get system user '{0}' from  database", KlarnaDeletedOrderScheduleSetting.SystemUserName);
				return;
			}

			var klarnaDeletedOrders = _orderSecureService.GetKlarnaCheckoutOrders(KlarnaDeletedOrderScheduleSetting.DeletedOrderLifeCycleInDays, "KCODeleted");
			foreach (var order in klarnaDeletedOrders)
			{
				_log.InfoFormat("Delete order with orderId = {0}", order.Id);
				_orderSecureService.Delete(systemUser, order);
			}

			var klarnaPendingOrders = _orderSecureService.GetKlarnaCheckoutOrders(KlarnaDeletedOrderScheduleSetting.PendingOrderLifeCycleInDays, "KCOPaymentPending");
			foreach (var order in klarnaPendingOrders)
			{
				_log.InfoFormat("Delete order with orderId = {0}", order.Id);
				_orderSecureService.Delete(systemUser, order);
			}
		}
	}
}