﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Cart.Service
{
	public class SavedCartReminderScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "CartService"; }
		}

		protected override string GroupName
		{
			get { return "SavedCartReminderJob"; }
		}

		public int SavedCartReminderInDays
		{
			get { return GetInt32(GroupName, "SavedCartReminderInDays", 10); }
		}
	}
}