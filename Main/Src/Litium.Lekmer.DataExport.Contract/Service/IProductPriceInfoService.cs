using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.DataExport
{
	public interface IProductPriceInfoService
	{
		Collection<IProductPriceInfo> GetAll(IUserContext context);
		void Save(IUserContext context, Collection<IProductPriceInfo> productPriceInfoList);
	}
}