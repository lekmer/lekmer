﻿using System.Collections;
using System.Globalization;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Skrill
{
	public class SkrillPaymentProvider : ISkrillPaymentProvider
	{
		protected IChannelService ChannelService { get; private set; }
		protected ICountryService CountryService { get; private set; }

		public SkrillPaymentProvider(IChannelService channelService, ICountryService countryService)
		{
			ChannelService = channelService;
			CountryService = countryService;
		}

		public string CreateRequest(IOrderFull order, ICustomerInformation customer, ICurrency currency)
		{
			string requestString = string.Empty;

			var channel = ChannelService.GetById(order.ChannelId);
			var setting = new SkrillSetting(channel.CommonName);

			// Prepare request data.
			var parameters = PrepareParameters(order, customer, currency, channel, setting);
			byte[] data = BuildRequestData(parameters);

			ServicePointManager.CertificatePolicy = new CertificatesPolicy();

			// Open the request url for the Web Payment Frontend
			var request = GetRequest(setting.WpfUrl);

			// Send the request data
			Stream input = request.GetRequestStream();
			input.Write(data, 0, data.Length);
			input.Close();

			// Get the answer from the frontend server
			IDictionary responseProperties = GetResponse(request);

			if (responseProperties["POST.VALIDATION"].Equals("ACK"))
			{
				requestString = GetRedirectUrl(responseProperties);
			}

			return requestString;
		}

		protected virtual IDictionary PrepareParameters(IOrderFull order, ICustomerInformation customer, ICurrency currency, IChannel channel, SkrillSetting setting)
		{
			var country = CountryService.GetById(customer.DefaultBillingAddress.CountryId);
			var countryCode = country.Iso;
			var langCode = GetLanguageCode(channel.CommonName);
			var currencyCode = currency.Iso;

			IDictionary parameters = new Hashtable();

			parameters["REQUEST.VERSION"] = "1.0";
			parameters["SECURITY.SENDER"] = setting.SecuritySender;
			parameters["USER.LOGIN"] = setting.UserLogin;
			parameters["USER.PWD"] = setting.UserPwd;
			parameters["TRANSACTION.CHANNEL"] = setting.TransactionChannel;
			parameters["TRANSACTION.MODE"] = setting.TransactionMode;
			parameters["TRANSACTION.RESPONSE"] = "SYNC";
			parameters["IDENTIFICATION.TRANSACTIONID"] = order.Id;

			parameters["FRONTEND.MODE"] = "DEFAULT";
			parameters["FRONTEND.ENABLED"] = "true";
			parameters["FRONTEND.POPUP"] = "false";
			parameters["FRONTEND.LANGUAGE"] = langCode;
			parameters["FRONTEND.LANGUAGE_SELECTOR"] = "true";
			parameters["FRONTEND.REDIRECT_TIME"] = "0";
			parameters["FRONTEND.REDIRECT_TIME_OK"] = "0";
			parameters["FRONTEND.RESPONSE_URL"] = setting.ResponseUrl;
			parameters["FRONTEND.CSS_PATH"] = setting.CssPath;
			parameters["FRONTEND.JSCRIPT_PATH"] = setting.JsPath;

			parameters["NAME.GIVEN"] = customer.FirstName;
			parameters["NAME.FAMILY"] = customer.LastName;
			parameters["CONTACT.EMAIL"] = customer.Email;
			parameters["CONTACT.PHONE"] = customer.PhoneNumber;
			parameters["CONTACT.MOBILE"] = customer.CellPhoneNumber;
			parameters["ADDRESS.STREET"] = customer.DefaultBillingAddress.StreetAddress;
			parameters["ADDRESS.ZIP"] = customer.DefaultBillingAddress.PostalCode;
			parameters["ADDRESS.CITY"] = customer.DefaultBillingAddress.City;
			parameters["ADDRESS.COUNTRY"] = countryCode;
			parameters["ACCOUNT.HOLDER"] = customer.FirstName + " " + customer.LastName;

			// Payment code.
			switch (channel.CommonName)
			{
				case "France":
					parameters["PAYMENT.CODE"] = setting.PaymentCode;
					parameters["ACCOUNT.BRAND"] = setting.AccountBrand;
					AddPaymentMethodsWithSubtypes(parameters, setting);
					break;
				case "Netherlands":
					parameters["PAYMENT.CODE"] = setting.PaymentCode;
					parameters["ACCOUNT.BRAND"] = setting.AccountBrand;
					AddPaymentMethodsWithSubtypes(parameters, setting);
					parameters["FRONTEND.COLLECT_DATA"] = "false";
					parameters["CRITERION.MONEYBOOKERS_recipient_description"] = setting.RecipientDescription;
					parameters["CRITERION.MONEYBOOKERS_cancel_url"] = setting.CancelUrl;
					parameters["CRITERION.MONEYBOOKERS_hide_login"] = "1";
					parameters["CRITERION.MONEYBOOKERS_payment_methods"] = setting.MoneybookersPaymentMethods;
					parameters["CRITERION.MONEYBOOKERS_language"] = langCode;
					parameters["CRITERION.MONEYBOOKERS_country"] = countryCode;
					break;
				default:
					parameters["PAYMENT.CODE"] = "CC.DB";
					parameters["ACCOUNT.BRAND"] = "VISA";
					parameters["FRONTEND.PM.DEFAULT_DISABLE_ALL"] = true;
					parameters["FRONTEND.PM.1.METHOD"] = "CC";
					parameters["FRONTEND.PM.1.ENABLED"] = true;
					parameters["FRONTEND.PM.1.SUBTYPES_DISABLED"] = true;
					parameters["FRONTEND.PM.1.SUBTYPES"] = "VISA";
					break;
			}

			parameters["PRESENTATION.AMOUNT"] = GetOrderPrice(order).ToString(CultureInfo.InvariantCulture).Replace(",", ".");
			parameters["PRESENTATION.CURRENCY"] =currencyCode;
			parameters["PRESENTATION.USAGE"] = order.Id;

			parameters["CRITERION.Channel.CommonName"] = channel.CommonName;

			return parameters;
		}

		protected virtual byte[] BuildRequestData(IDictionary parameters)
		{
			var stringBuilder = new StringBuilder();
			foreach (DictionaryEntry entry in parameters)
			{
				stringBuilder.AppendFormat(CultureInfo.CurrentCulture, "{0}={1}&", entry.Key, entry.Value);
			}

			string requestData = stringBuilder.ToString();
			requestData = requestData.Substring(0, requestData.Length - 1);

			return Encoding.UTF8.GetBytes(requestData);
		}

		protected virtual HttpWebRequest GetRequest(string url)
		{
			var request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			return request;
		}

		protected virtual IDictionary GetResponse(HttpWebRequest request)
		{
			IDictionary responseProperties = new Hashtable();

			var response = (HttpWebResponse)request.GetResponse();
			var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
			string initialResponse = reader.ReadToEnd();
			reader.Close();
			response.Close();

			string[] initialResponseParts = initialResponse.Split('&');
			foreach (string responsePart in initialResponseParts)
			{
				string[] keyValue = responsePart.Split('=');
				responseProperties[keyValue[0]] = keyValue[1];
			}

			return responseProperties;
		}

		protected virtual string GetRedirectUrl(IDictionary responseProperties)
		{
			var redirectUrl = (string)responseProperties["FRONTEND.REDIRECT_URL"];

			redirectUrl = redirectUrl.Replace("%2F", "/");
			redirectUrl = redirectUrl.Replace("%3A", ":");
			redirectUrl = redirectUrl.Replace("%3B", ";");
			redirectUrl = redirectUrl.Replace("%3D", "=");

			return redirectUrl;
		}

		protected virtual string GetLanguageCode(string channelCommonName)
		{
			string langCode;
			switch (channelCommonName)
			{
				case "Sweden":
					langCode = "se";
					break;
				case "Denmark":
					langCode = "dk";
					break;
				case "Norway":
					langCode = "no";
					break;
				case "Finland":
					langCode = "fi";
					break;
				case "France":
					langCode = "fr";
					break;
				case "Netherlands":
					langCode = "nl";
					break;
				default:
					langCode = "en";
					break;
			}
			return langCode;
		}

		protected virtual decimal GetOrderPrice(IOrderFull order)
		{
			Price price = order.GetActualPriceSummary();
			decimal priceFreight = ((ILekmerOrderFull)order).GetTotalFreightCost();
			var priceIncludingVat = price.IncludingVat + priceFreight;
			return priceIncludingVat;
		}

		protected virtual void AddPaymentMethodsWithSubtypes(IDictionary parameters, SkrillSetting setting)
		{
			string methods = setting.AvaliablePaymentMethods;
			if (!string.IsNullOrEmpty(methods))
			{
				string[] avaliableMethods = methods.Split(':');
				if (avaliableMethods.Length > 0)
				{
					const string method = "FRONTEND.PM.{0}.METHOD";
					const string enabled = "FRONTEND.PM.{0}.ENABLED";
					const string subtypes = "FRONTEND.PM.{0}.SUBTYPES";

					string[] avaliableSubtypes = setting.AvaliablePaymentMethodSubtypes.Split(':');

					parameters["FRONTEND.PM.DEFAULT_DISABLE_ALL"] = true;

					for (int i = 0; i < avaliableMethods.Length; i++)
					{
						if (!string.IsNullOrEmpty(avaliableMethods[i]))
						{
							parameters[string.Format(method, i + 1)] = avaliableMethods[i];
							parameters[string.Format(enabled, i + 1)] = true;
						}

						if (!string.IsNullOrEmpty(avaliableSubtypes[i]))
						{
							parameters[string.Format(subtypes, i + 1)] = avaliableSubtypes[i];
						}
					}
				}
			}
		}
	}

	class CertificatesPolicy : ICertificatePolicy
	{
		public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest request, int problem)
		{
			return true;
		}
	}
}