﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Skrill
{
	public class SkrillSetting : SettingBase
	{
		private readonly string _groupName;


		public SkrillSetting(string channelCommonName)
		{
			_groupName = channelCommonName;
		}


		protected override string StorageName
		{
			get { return "Skrill"; }
		}

		public string WpfUrl
		{
			get { return GetString(_groupName, "WPFURL", null); }
		}

		public string SecuritySender
		{
			get { return GetString(_groupName, "SecuritySender", null); }
		}

		public string UserLogin
		{
			get { return GetString(_groupName, "UserLogin", null); }
		}

		public string UserPwd
		{
			get { return GetString(_groupName, "UserPwd", null); }
		}

		public string TransactionChannel
		{
			get { return GetString(_groupName, "TransactionChannel", null); }
		}

		public string TransactionMode
		{
			get { return GetString(_groupName, "TransactionMode", null); }
		}

		public string ResponseUrl
		{
			get { return GetString(_groupName, "ResponseUrl", null); }
		}

		public string CancelUrl
		{
			get { return GetString(_groupName, "CancelUrl", null); }
		}

		public string SecretWord
		{
			get { return GetString(_groupName, "SecretWord", null); }
		}

		public string JsPath
		{
			get { return GetString(_groupName, "js_path", null); }
		}

		public string CssPath
		{
			get { return GetString(_groupName, "css_path", null); }
		}

		public string RecipientDescription
		{
			get { return GetString(_groupName, "RecipientDescription", null); }
		}

		public string PaymentCode
		{
			get { return GetString(_groupName, "PaymentCode", null); }
		}

		public string AccountBrand
		{
			get { return GetString(_groupName, "AccountBrand", null); }
		}

		public string AvaliablePaymentMethods
		{
			get { return GetString(_groupName, "AvaliablePaymentMethods", null); }
		}

		public string AvaliablePaymentMethodSubtypes
		{
			get { return GetString(_groupName, "AvaliablePaymentMethodSubtypes", null); }
		}

		public string MoneybookersPaymentMethods
		{
			get { return GetString(_groupName, "MoneybookersPaymentMethods", null); }
		}
	}
}