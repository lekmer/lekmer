﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Litium.Lekmer.FtpServer.Service
{
	[RunInstaller(true)]
	public partial class FtpServiceInstaller : System.Configuration.Install.Installer
	{
		public FtpServiceInstaller()
		{
			InitializeComponent();
		}
	}
}
