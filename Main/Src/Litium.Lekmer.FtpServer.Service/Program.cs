﻿using System.ServiceProcess;

namespace Litium.Lekmer.FtpServer.Service
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			ServiceBase[] ServicesToRun;
			ServicesToRun = new ServiceBase[]
			{
				new FtpService()
			};
			ServiceBase.Run(ServicesToRun);
		}
	}
}
