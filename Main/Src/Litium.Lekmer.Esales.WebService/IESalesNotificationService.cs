﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace Litium.Lekmer.Esales.WebService
{
	[ServiceContract]
	public interface IESalesNotificationService
	{
		[OperationContract]
		[WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Xml)]
		string Notify(string type, string ticket, string productKey, string rating, string id);
	}
}
