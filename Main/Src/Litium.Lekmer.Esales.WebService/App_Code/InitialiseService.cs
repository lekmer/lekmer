﻿using log4net;

namespace Litium.Lekmer.Esales.WebService.App_Code
{
	public class InitialiseService
	{
		/// <summary>
		/// Application initialisation method.
		/// </summary>
		public static void AppInitialize()
		{
			GlobalContext.Properties["SiteName"] = System.Web.Hosting.HostingEnvironment.SiteName;
		}
	}
}