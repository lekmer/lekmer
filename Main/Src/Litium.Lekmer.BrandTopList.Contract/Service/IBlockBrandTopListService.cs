using Litium.Scensum.Core;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListService
	{
		IBlockBrandTopList GetById(IUserContext context, int id);
	}
}