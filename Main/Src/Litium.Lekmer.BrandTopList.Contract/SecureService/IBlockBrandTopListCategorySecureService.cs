﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListCategorySecureService
	{
		Collection<IBlockBrandTopListCategory> GetAllByBlock(int blockId);

		void Save(int blockId, Collection<IBlockBrandTopListCategory> blockCategories);
	}
}