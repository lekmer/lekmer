using System.Collections.ObjectModel;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListBrandSecureService
	{
		Collection<IBlockBrandTopListBrand> GetAllByBlock(int blockId);

		void Save(int blockId, Collection<IBlockBrandTopListBrand> blockBrands);
	}
}