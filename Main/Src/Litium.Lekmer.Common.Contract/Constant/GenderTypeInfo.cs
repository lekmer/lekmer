﻿namespace Litium.Lekmer.Common
{
	public enum GenderTypeInfo
	{
		None = 0,
		Unknown = 1,
		Man = 2,
		Woman = 3
	}
}