﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Common
{
	public interface IBlockContactUsSecureService
	{
		IBlockContactUs Create();
		IBlockContactUs GetById(int id);
		int Save(ISystemUserFull systemUserFull, IBlockContactUs block);
	}
}
