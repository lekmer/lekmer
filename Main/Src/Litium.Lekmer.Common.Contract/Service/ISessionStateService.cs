﻿namespace Litium.Lekmer.Common
{
	public interface ISessionStateService
	{
		bool Available { get; }
		bool Contains(string name);
		void Remove(string name);
		object this[string name] { get; set; }
		string SessionId { get; }
	}
}