﻿namespace Litium.Lekmer.Common
{
	public interface ICrawlerDetectorService
	{
		bool IsMatch(string userAgent);
	}
}