﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockSetting : IBusinessObjectBase
	{
		int BlockId { get; set; }
		int ColumnCount { get; set; }
		int? ColumnCountMobile { get; set; }
		int RowCount { get; set; }
		int? RowCountMobile { get; set; }
		int TotalItemCount { get; set; }
		int? TotalItemCountMobile { get; set; }

		int ActualColumnCount { get; }
		int ActualRowCount { get; }
		int ActualTotalItemCount { get; }

		void SetValues(IBlockSetting blockSetting);
	}
}
