﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Litium.Framework.Cache;

namespace Litium.Scensum.Web.Diagnostics
{
	public partial class CacheDiagnostics : System.Web.UI.Page
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores")]
		protected void Page_Load(object sender, EventArgs e)
		{
			VerifyAccess();

			PopulateCacheManagers(CacheManagerGrid, CacheProviderContainer.GetAllProviders());
		}

		protected virtual void VerifyAccess()
		{
			var cacheDiagnostics = CacheDiagnosticsSection.GetConfig();

			if (!cacheDiagnostics.Enabled)
			{
				throw new HttpException(403, "Diagnostics not enabled.");
			}

			if (cacheDiagnostics.LocalOnly && !Request.IsLocal)
			{
				throw new HttpException(403, "Diagnostics not available from remote client.");
			}
		}

		protected virtual void PopulateCacheManagers(BaseDataBoundControl cacheManagerGrid, IEnumerable<ICacheProvider> cacheProviders)
		{
			IOrderedEnumerable<CacheManagerInfo> cacheManagers = cacheProviders
				.Select(p => new CacheManagerInfo
				{
					ManagerName = p.ManagerName,
					Count = p.Count
				})
				.OrderBy(m => m.ManagerName);

			cacheManagerGrid.DataSource = cacheManagers;
			cacheManagerGrid.DataBind();
		}
	}
}