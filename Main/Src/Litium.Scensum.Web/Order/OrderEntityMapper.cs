using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Order
{
	public class OrderEntityMapper : Scensum.Order.Web.OrderEntityMapper
	{
		public OrderEntityMapper(IFormatter formatter) : base(formatter)
		{
		}

		public override void AddEntityVariables(Fragment fragment, IOrder item)
		{
			base.AddEntityVariables(fragment, item);

			string orderUrl = null;
			string basePath = OrderUrlHelper.BasePath;
			if (!string.IsNullOrEmpty(basePath))
			{
				orderUrl = UrlHelper.ResolveUrlHttps("~/" + basePath + item.Id);
			}

			fragment.AddVariable("Order.Url", orderUrl);
		}
	}
}