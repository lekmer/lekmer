using Litium.Lekmer.Order.Web.OrderHistory;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Web.Customer;

namespace Litium.Scensum.Web.Order
{
	public class OrderDetailPage : LekmerOrderDetailPageHandler
	{
		protected override IOrderFull GetOrderCore()
		{
			return Items["Order"] as IOrderFull;
		}

		protected override string OrderUrl
		{
			get
			{
				string basePath = OrderUrlHelper.BasePath;
				if (string.IsNullOrEmpty(basePath)) return null;

				return UrlHelper.ResolveUrlHttps("~/" + basePath + Order.Id);
			}
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetSignInUrl());
		}

		protected override void AccessDeniedToSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetMyPagesUrl());
		}

		protected override Core.Web.Master CreateMaster()
		{
			return new Master();
		}
	}
}