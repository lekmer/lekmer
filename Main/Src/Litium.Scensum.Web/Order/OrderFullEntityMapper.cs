using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Order
{
	public class OrderFullEntityMapper : Scensum.Order.Web.OrderFullEntityMapper
	{
		public OrderFullEntityMapper(IFormatter formatter, ICountryService countryService) : base(formatter, countryService)
		{
		}

		public override void AddEntityVariables(Fragment fragment, IOrderFull item)
		{
			base.AddEntityVariables(fragment, item);

			string orderUrl = null;
			string basePath = OrderUrlHelper.BasePath;
			if (!string.IsNullOrEmpty(basePath))
			{
				orderUrl = UrlHelper.ResolveUrlHttps("~/" + basePath + item.Id);
			}

			fragment.AddVariable("Order.Url", orderUrl);
		}
	}
}