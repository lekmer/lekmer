using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using CartHelper = Litium.Scensum.Core.Web.CartHelper;

namespace Litium.Scensum.Web.Order
{
	public class AutoCheckout : HandlerBase
	{
		public int? ActiveCartId { get; set; }

		[SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider",
			MessageId = "System.String.Format(System.String,System.Object)"),
		 SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
		protected override void ProcessRequest()
		{
			string[] productIds = Request.QueryString["productid"].Split(',');

			foreach (string id in productIds)
			{
				int productId;
				if (!int.TryParse(id, out productId))
				{
					throw new Exception(string.Format("'{0}' is not an integer.", id));
				}

				AddItemToCart(productId, 1);
			}
			CartHelper.RefreshSessionCart();
			PlaceOrder();

			Response.Write("Order placed! " + DateTime.Now);
		}

		private void PlaceOrder()
		{
			var orderService = IoC.Resolve<IOrderService>();
			var order = (ILekmerOrderFull)orderService.CreateFull(UserContext.Current);

			order.MergeWith(GetDeliveryMethod());
			order.MergeWith(UserContext.Current, GetOrderPayments());
			order.MergeWith(UserContext.Current, GetExistingCart());
			order.MergeWith(GetCustomer());
			order.MergeWith(false);

			order.Number = Guid.NewGuid().ToString("N", CultureInfo.InvariantCulture);
			var orderStatusService = IoC.Resolve<IOrderStatusService>();
			order.OrderStatus = orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);
			var orderItemStatusService = IoC.Resolve<IOrderItemStatusService>();
			var orderItemStatus = orderItemStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);
			order.SetAllOrderItemsStatus(orderItemStatus);
			order.IP = "127.0.0.1";

			orderService.SaveFull(UserContext.Current, order);
		}

		private static ICustomer GetCustomer()
		{
			IAddress address = IoC.Resolve<IAddressService>().Create(UserContext.Current);
			address.CountryId = Channel.Current.Country.Id;
			address.StreetAddress = "Evergreen Terrace 742";
			address.PostalCode = "97477";
			address.City = "Springfield";
			address.Addressee = "Homer Simpson";

			ICustomerInformation info = IoC.Resolve<ICustomerInformationService>().Create(UserContext.Current);
			info.FirstName = "Homer";
			info.LastName = "Simpson";
			info.Email = "chunkylover53@aol.com";
			info.PhoneNumber = "555-6832";
			info.CivicNumber = "568-47-0008";
			info.CreatedDate = DateTime.Now;
			info.DefaultBillingAddress = address;
			info.DefaultDeliveryAddress = address;

			ICustomer customer = IoC.Resolve<ICustomerService>().Create(UserContext.Current);
			customer.CustomerInformation = info;
			customer.User = null;

			return customer;
		}

		private static Collection<IOrderPayment> GetOrderPayments()
		{
			return new Collection<IOrderPayment>();
		}

		private static IDeliveryMethod GetDeliveryMethod()
		{
            return IoC.Resolve<IDeliveryMethodService>().GetById(UserContext.Current, 1);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		private ICartFull GetExistingCart()
		{
			int? cartId = ActiveCartId;
			if (!cartId.HasValue) return null;

			var cartService = IoC.Resolve<ICartService>();
			return cartService.GetById(UserContext.Current, cartId.Value);
		}

		private void AddItemToCart(int productId, int quantity)
		{
			ICartFull cart = GetCart();
			AddItemToCart(productId, quantity, cart);
		}

		private void AddItemToCart(int productId, int quantity, ICartFull cart)
		{
			if (cart == null) throw new ArgumentNullException("cart");

			IProduct product = IoC.Resolve<IProductService>().GetById(UserContext.Current, productId);
			if (product != null)
			{
				bool cartIsNew = cart.IsNew;
				cart.ChangeItemQuantity(product, quantity);
				cart.Id = IoC.Resolve<ICartService>().Save(UserContext.Current, cart);
				if (cartIsNew)
				{
					ActiveCartId = cart.Id;
				}
			}
		}

		private ICartFull GetCart()
		{
			int? cartId = ActiveCartId;

			if (!cartId.HasValue)
			{
				return CreateCart();
			}

			return IoC.Resolve<ICartService>().GetById(UserContext.Current, cartId.Value);
		}

		private ICartFull CreateCart()
		{
			return ((ILekmerCartService) IoC.Resolve<ICartService>()).Create(UserContext.Current, Request.UserHostAddress);
		}
	}
}