﻿using System.Collections.ObjectModel;

namespace Litium.Scensum.Web
{
	public static class CollectionExtension
	{
		public static Collection<T> AddRange<T>(this Collection<T> collection, Collection<T> addCollection)
		{
			foreach (T block in addCollection)
			{
				collection.Add(block);
			}
			return collection;
		}
	}
}
