using System;
using Litium.Lekmer.Common;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Cart;

namespace Litium.Scensum.Web.Lekmer
{
	public class CartDelete : CartDeleteHandler
	{
		private readonly ICartService _cartService = IoC.Resolve<ICartService>();
		private CartDeleteHelper _cartDeleteHelper;

		protected override void ProcessRequest()
		{
			var crawlerDetector = IoC.Resolve<ICrawlerDetectorService>();
			if (Request.Browser.Crawler || crawlerDetector.IsMatch(Request.UserAgent))
			{
				Response.Write(@"<!-- CD -->");
				Response.DisablePageCaching();
				return;
			}

			_cartDeleteHelper = new CartDeleteHelper(Request.QueryString);

			int? productId = GetProductId();
			decimal? campaignInfoPrice = GetProductCampaignInfoPrice();
			if (productId.HasValue && campaignInfoPrice.HasValue)
			{
				ICartFull cart = IoC.Resolve<ICartSession>().Cart;
				if (cart != null)
				{
					DeleteItemFromCart(cart, productId.Value, campaignInfoPrice.Value, GetQuantity());
				}
			}

			if (AjaxHelper.IsXmlHttpRequest(Request))
			{
				Response.Write(AjaxHelper.RenderCart());
			}
			else
			{
				Response.Redirect(GetRedirectUrl());
			}
		}

		protected override void DeleteItemFromCart(ICartFull cart, int productId, decimal campaignInfoPrice, int quantity)
		{
			if (cart == null) throw new ArgumentNullException("cart");

			((ILekmerCartFull)cart).DeleteItem(productId, campaignInfoPrice, quantity, _cartDeleteHelper.GetSizeId(), _cartDeleteHelper.GetCartItemPackageElementsHashCode());
			_cartService.Save(UserContext.Current, cart);
			RefreshCart();
		}


		protected override string GetRedirectUrl()
		{
			return _cartDeleteHelper.GetRedirectUrl();
		}

		protected override int? GetProductId()
		{
			return _cartDeleteHelper.GetProductId();
		}

		protected override int GetQuantity()
		{
			return _cartDeleteHelper.GetQuantity();
		}

		protected override decimal? GetProductCampaignInfoPrice()
		{
			return _cartDeleteHelper.GetProductCampaignInfoPrice();
		}
	}
}