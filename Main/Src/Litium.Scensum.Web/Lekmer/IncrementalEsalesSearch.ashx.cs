﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Channel=Litium.Scensum.Core.Web.Channel;
using Encoder=Litium.Scensum.Foundation.Utilities.Encoder;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;
using UserContext=Litium.Scensum.Core.Web.UserContext;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Scensum.Web.Lekmer
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Esales")]
	public class IncrementalEsalesSearch : HandlerBase
	{
		private const string _imageSizeCommonName = "minithumbnail";
		private const int _defaultNumberOfItemsToShow = 10;
		private const int _maximumNumberOfItemsToShow = 100;

		private IContentNodeService _contentNodeService;
		private IMediaUrlService _mediaUrlService;
		private IProductTypeService _productTypeService;

		protected int NumberOfItemsToShow
		{
			get
			{
				return Math.Min(Request.QueryString.GetInt32OrNull("show-items") ?? _defaultNumberOfItemsToShow, _maximumNumberOfItemsToShow);
			}
		}

		protected string SearchQuery
		{
			get
			{
				return Request.QueryString["q"];
			}
		}

		protected IContentNodeService ContentNodeService
		{
			get
			{
				return _contentNodeService ?? (_contentNodeService = IoC.Resolve<IContentNodeService>());
			}
		}

		protected IMediaUrlService MediaUrlService
		{
			get
			{
				return _mediaUrlService ?? (_mediaUrlService = IoC.Resolve<IMediaUrlService>());
			}
		}

		protected IProductTypeService ProductTypeService
		{
			get
			{
				return _productTypeService ?? (_productTypeService = IoC.Resolve<IProductTypeService>());
			}
		}


		protected override void ProcessRequest()
		{
			int showItems = NumberOfItemsToShow;
			string searchQuery = SearchQuery;

			ISearchSuggestions suggestions = searchQuery.HasValue() ? SearchProducts(searchQuery, showItems) : new SearchSuggestions();

			Response.ContentType = "application/json";
			Response.Write(BuildJsonResponse(suggestions));
		}

		protected virtual string BuildJsonResponse(ISearchSuggestions suggestions)
		{
			if (suggestions == null)
			{
				throw new ArgumentNullException("suggestions");
			}

			var json = new StringBuilder();

			json.Append("{");

			json.Append(BuildSuggestionsJsonResponse(suggestions));

			json.Append(", ");

			json.Append(BuildProductsJsonResponse(suggestions.Products));

			json.Append(", ");

			json.Append(BuildCategoryTagsJsonResponse(suggestions.CategoryTags));

			json.Append(", ");

			json.Append(BuildBrandsJsonResponse(suggestions.Brands));

			json.Append("}");

			return json.ToString();
		}

		protected virtual string BuildSuggestionsJsonResponse(ISearchSuggestions suggestions)
		{
			var json = new StringBuilder();

			json.Append("\"suggestions\": [");

			if (suggestions.Suggestions != null)
			{
				foreach (ISearchSuggestionInfo suggestionInfo in suggestions.Suggestions)
				{
					json.Append(string.Concat("{\"text\": \"", Encoder.EncodeJavaScript(suggestionInfo.Text), "\", \"ticket\": \"", Encoder.EncodeJavaScript(suggestionInfo.Ticket), "\"}, "));
				}

				if (suggestions.Suggestions.Count > 0)
				{
					json.Remove(json.Length - 2, 2);
				}
			}

			json.Append("]");

			json.Append(", \"corrections\": [");

			if (suggestions.Corrections != null)
			{
				foreach (ISearchSuggestionInfo correctionInfo in suggestions.Corrections)
				{
					json.Append(string.Concat("{\"text\": \"", Encoder.EncodeJavaScript(correctionInfo.Text), "\", \"ticket\": \"", Encoder.EncodeJavaScript(correctionInfo.Ticket), "\"}, "));
				}

				if (suggestions.Corrections.Count > 0)
				{
					json.Remove(json.Length - 2, 2);
				}
			}

			json.Append("]");

			return json.ToString();
		}

		protected virtual string BuildProductsJsonResponse(ProductCollection products)
		{
			if (products == null)
			{
				return "\"productResults\": [], \"totalCount\": 0";
			}

			var formatter = IoC.Resolve<IFormatter>();

			const string jsonProductFormat = "\"name\": \"{0}\", \"price\": \"{1}\", \"image\": \"{2}\", \"productUrl\": \"{3}\", \"campaignPrice\": \"{4}\", \"ticket\": \"{5}\", \"type\": \"{6}\"";

			var json = new StringBuilder();

			json.Append("\"productResults\": [");
			foreach (IProduct product in products)
			{
				var lekmerProduct = (ILekmerProduct) product;

				string price = formatter.FormatPrice(Channel.Current, product.Price.PriceIncludingVat);
				
				string campaignPrice = price;
				if (product.CampaignInfo != null)
				{
					campaignPrice = formatter.FormatPrice(Channel.Current, product.CampaignInfo.Price.IncludingVat);
				}

				string imageUrl = GetImageUrl(product) ?? string.Empty;
				string productUrl = UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl);

				string ticket = lekmerProduct.EsalesTicket;

				string productType = ProductTypeService.GetById(UserContext.Current, lekmerProduct.ProductTypeId).CommonName;

				json.Append(
					"{" +
					string.Format(
						CultureInfo.CurrentCulture, jsonProductFormat,
						Encoder.EncodeJavaScript(product.DisplayTitle),
						Encoder.EncodeJavaScript(price),
						Encoder.EncodeJavaScript(imageUrl),
						Encoder.EncodeJavaScript(productUrl),
						Encoder.EncodeJavaScript(campaignPrice),
						Encoder.EncodeJavaScript(ticket),
						Encoder.EncodeJavaScript(productType)
						)
					+ "}, ");
			}

			if (products.Count > 0)
			{
				json.Remove(json.Length - 2, 2);
			}
			json.Append("]");
			json.AppendFormat(", \"totalCount\": {0}", products.TotalCount);

			return json.ToString();
		}

		protected virtual string BuildCategoryTagsJsonResponse(Collection<ICategoryTagSearchSuggestion> categoryTagSuggestions)
		{
			if (categoryTagSuggestions == null)
			{
				return "\"categoryTagResults\": []";
			}

			const string jsonFormat = "\"name\": \"{0}\", \"url\": \"{1}\"";
			const string jsonFormatWithTicket = "\"name\": \"{0}\", \"url\": \"{1}\", \"ticket\": \"{2}\"";

			var json = new StringBuilder();

			json.Append("\"categoryTagResults\": [");
			foreach (ICategoryTagSearchSuggestion categoryTagSuggestion in categoryTagSuggestions)
			{
				json.Append("{ ");

				// Parent categories
				json.Append("\"parentCategories\": [");

				foreach (ICategory category in categoryTagSuggestion.ParentCategories)
				{
					string prentCategoryUrl = GetCategoryUrl(category);

					json.Append(
						"{" +
						string.Format(
							CultureInfo.CurrentCulture, jsonFormat,
							Encoder.EncodeJavaScript(category.Title),
							Encoder.EncodeJavaScript(prentCategoryUrl))
						+ "}, ");
				}

				if (categoryTagSuggestion.ParentCategories.Count > 0)
				{
					json.Remove(json.Length - 2, 2);
				}

				json.Append("], ");

				// Category
				json.Append("\"category\": {");

				string categoryUrl = GetCategoryUrl(categoryTagSuggestion.Category);

				json.Append(
					string.Format(
						CultureInfo.CurrentCulture, jsonFormatWithTicket,
						Encoder.EncodeJavaScript(categoryTagSuggestion.Category.Title),
						Encoder.EncodeJavaScript(categoryUrl),
						Encoder.EncodeJavaScript(categoryTagSuggestion.CategoryTicket))
					);
				json.Append("}, ");

				// Tags
				json.Append("\"tags\": [");

				if (categoryTagSuggestion.Tags != null)
				{
					foreach (ITag tag in categoryTagSuggestion.Tags)
					{
						string tagUrl = GetTagUrl(tag, categoryUrl);

						json.Append(
							"{" +
							string.Format(
								CultureInfo.CurrentCulture, jsonFormat,
								Encoder.EncodeJavaScript(tag.Value),
								Encoder.EncodeJavaScript(tagUrl))
							+ "}, ");
					}

					if (categoryTagSuggestion.Tags.Count > 0)
					{
						json.Remove(json.Length - 2, 2);
					}
				}

				json.Append("]");

				json.Append("}, ");
			}

			if (categoryTagSuggestions.Count > 0)
			{
				json.Remove(json.Length - 2, 2);
			}
			json.Append("]");

			return json.ToString();
		}

		protected virtual string BuildBrandsJsonResponse(Collection<IBrandSuggestionInfo> brands)
		{
			if (brands == null)
			{
				return "\"brandResults\": []";
			}

			const string jsonBrandFormat = "\"name\": \"{0}\", \"image\": \"{1}\", \"url\": \"{2}\", \"ticket\": \"{3}\"";

			var json = new StringBuilder();

			json.Append("\"brandResults\": [");
			foreach (IBrandSuggestionInfo brandSuggestion in brands)
			{
				IBrand brand = brandSuggestion.Brand;
				
				string imageUrl = GetImageUrl(brand) ?? string.Empty;
				string brandUrl = GetBrandUrl(brand);

				json.Append(
					"{" +
					string.Format(
						CultureInfo.CurrentCulture, jsonBrandFormat,
						Encoder.EncodeJavaScript(brand.Title),
						Encoder.EncodeJavaScript(imageUrl),
						Encoder.EncodeJavaScript(brandUrl),
						Encoder.EncodeJavaScript(brandSuggestion.Ticket))
					+ "}, ");
			}

			if (brands.Count > 0)
			{
				json.Remove(json.Length - 2, 2);
			}
			json.Append("]");

			return json.ToString();
		}

		protected virtual string GetImageUrl(IProduct product)
		{
			if (product == null)
			{
				throw new ArgumentNullException("product");
			}

			string imageUrl = null;
			if (product.Image != null)
			{
				string mediaUrl = MediaUrlService.ResolveMediaArchiveUrl(Channel.Current, product.Image);
				imageUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, product.Image, product.DisplayTitle, _imageSizeCommonName);
			}
			return imageUrl;
		}

		protected virtual string GetImageUrl(IBrand brand)
		{
			if (brand == null)
			{
				throw new ArgumentNullException("brand");
			}

			string imageUrl = null;
			if (brand.Image != null)
			{
				string mediaUrl = MediaUrlService.ResolveMediaArchiveUrl(Channel.Current, brand.Image);
				imageUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, brand.Image, brand.Title, _imageSizeCommonName);
			}
			return imageUrl;
		}

		protected virtual string GetCategoryUrl(ICategory category)
		{
			var categoryView = (ILekmerCategoryView)category;

			if (categoryView.ProductParentContentNodeId.HasValue)
			{
				var treeItem = ContentNodeService.GetTreeItemById(UserContext.Current, categoryView.ProductParentContentNodeId.Value);

				if (treeItem.ContentNode != null && treeItem.Url.HasValue())
				{
					return UrlHelper.ResolveUrlHttp(treeItem.Url);
				}
			}

			return string.Empty;
		}

		protected virtual string GetTagUrl(ITag tag, string genericFilterPageUrl)
		{
			if (genericFilterPageUrl.IsNullOrEmpty())
			{
				return string.Empty;
			}

			var query = new QueryBuilder();
			query.Add("mode", "filter");
			query.Add("taggroup" + tag.TagGroupId + "-tag-id", tag.Id.ToString(CultureInfo.InvariantCulture));
			return genericFilterPageUrl + query;
		}

		protected virtual string GetBrandUrl(IBrand brand)
		{
			if (brand == null)
			{
				throw new ArgumentNullException("brand");
			}

			if (brand.ContentNodeId.HasValue)
			{
				var treeItem = ContentNodeService.GetTreeItemById(UserContext.Current, brand.ContentNodeId.Value);

				if (treeItem.ContentNode != null && treeItem.Url.HasValue())
				{
					return UrlHelper.ResolveUrlHttp(treeItem.Url);
				}
			}

			return string.Empty;
		}

		protected virtual ISearchSuggestions SearchProducts(string searchQuery, int showItems)
		{
			var incrementalSearchService = IoC.Resolve<IEsalesIncrementalSearchService>();

			ISearchSuggestions resultSuggestions = new SearchSuggestions();

			ISearchSuggestions searchSuggestions = incrementalSearchService.SearchSuggestions(UserContext.Current, searchQuery, 1, showItems);

			resultSuggestions.Suggestions = searchSuggestions.Suggestions;
			resultSuggestions.Corrections = searchSuggestions.Corrections;

			ISearchSuggestions searchProducts = incrementalSearchService.SearchProducts(UserContext.Current, searchQuery, 1, showItems);
			if (searchProducts.Products.Count <= 0)
			{
				searchProducts = incrementalSearchService.SearchProductsWithFilter(UserContext.Current, searchQuery, 1, showItems);
			}

			if (searchProducts != null)
			{
				resultSuggestions.Products = searchProducts.Products;
				resultSuggestions.CategoryTags = searchProducts.CategoryTags;
				resultSuggestions.Brands = searchProducts.Brands;
			}

			return resultSuggestions;
		}
	}
}