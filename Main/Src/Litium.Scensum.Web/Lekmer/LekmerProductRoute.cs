using System;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using ProductStatus = Litium.Lekmer.Product.ProductStatus;

namespace Litium.Scensum.Web.Lekmer
{
	public class LekmerProductRoute : Route
	{
		public LekmerProductRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public LekmerProductRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
			: base(url, defaults, routeHandler)
		{
		}

		public LekmerProductRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, IRouteHandler routeHandler)
			: base(url, defaults, constraints, routeHandler)
		{
		}

		public LekmerProductRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints, RouteValueDictionary dataTokens, IRouteHandler routeHandler)
			: base(url, defaults, constraints, dataTokens, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null)
			{
				throw new ArgumentNullException("httpContext");
			}

			RouteData data = base.GetRouteData(httpContext);
			if (data == null)
			{
				return null;
			}

			string url = (data.Values["url"] ?? string.Empty).ToString();
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				return null;
			}

			string urlTitle;
			string parentPageUrl;

			int slashIndex = url.LastIndexOf('/');
			if (slashIndex != -1)
			{
				urlTitle = url.Substring(slashIndex + 1);
				parentPageUrl = url.Substring(0, slashIndex + 1);
			}
			else
			{
				urlTitle = url;
				parentPageUrl = null;
			}

			urlTitle = urlTitle.Replace('-', ' ');

			var productUrlService = IoC.Resolve<IProductUrlService>();
			int? productId = productUrlService.GetIdByUrlTitle(UserContext.Current, urlTitle);
			if (!productId.HasValue)
			{
				return null;
			}

			var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
			var product = productService.GetById(UserContext.Current, productId.Value, true);
			if (product != null)
			{
				httpContext.Items["UrlProductComplete"] = RelativeUrl(httpContext.Request);
				httpContext.Items["UrlProductTitle"] = urlTitle;
				httpContext.Items["UrlProductId"] = productId.Value;
				httpContext.Items["UrlParentPageUrl"] = parentPageUrl;
				httpContext.Items["UrlSiteStructureCommonName"] = httpContext.Request.QueryString["page-common-name"];

				return data;
			}

			product = productService.GetByIdWithoutStatusFilter(UserContext.Current, productId.Value, true, true);
			if (product != null)
			{
				string parentUrl = ((LekmerProduct)product).ParentPageUrl;
				httpContext.Response.PermanentRedirect(parentUrl.HasValue() ? parentUrl : "~/");
			}

			return null;
		}

		public static string RelativeUrl(HttpRequestBase request)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}

			// Get full url.
			string url = request.ServerVariables["HTTP_URL"];

			// Take url and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath;
			string relativeUrl = url.Substring(appPath.Length);
			if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
			{
				relativeUrl = "/" + relativeUrl;
			}

			int questionMarkIndex = relativeUrl.IndexOf('?');
			if (questionMarkIndex != -1)
			{
				relativeUrl = relativeUrl.Substring(0, questionMarkIndex);
			}

			// Return relative url.
			return relativeUrl;
		}
	}
}