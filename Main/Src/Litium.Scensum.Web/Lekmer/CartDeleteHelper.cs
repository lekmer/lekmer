﻿using System.Collections.Specialized;

namespace Litium.Scensum.Web.Lekmer
{
	public class CartDeleteHelper
	{
		private const string _idFieldName = "id";
		private const string _quantityFieldName = "quantity";
		private const string _priceFieldName = "price";
		private const string _referrerFieldName = "referrer";
		private const string _sizeIdFieldName = "sizeid";
		private const string _hashCodeFieldName = "packageproducthashcode";

		private readonly NameValueCollection _query;

		public CartDeleteHelper(NameValueCollection query)
		{
			_query = query;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int? GetProductId()
		{
			int productId;
			if (!int.TryParse(_query[_idFieldName], out productId))
			{
				return null;
			}
			return productId;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int GetQuantity()
		{
			int quantity;
			if (!int.TryParse(_query[_quantityFieldName], out quantity))
			{
				quantity = 1;
			}
			return quantity;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public decimal? GetProductCampaignInfoPrice()
		{
			decimal campaignInfoPrice;
			if (!decimal.TryParse(_query[_priceFieldName], out campaignInfoPrice))
			{
				return null;
			}
			return campaignInfoPrice;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public string GetRedirectUrl()
		{string referrer = _query[_referrerFieldName];

			if (string.IsNullOrEmpty(referrer))
			{
				return "~/";
			}
			return "~" + referrer;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int? GetSizeId()
		{
			int sizeId;
			if (!int.TryParse(_query[_sizeIdFieldName], out sizeId))
			{
				return null;
			}
			return sizeId;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public int? GetCartItemPackageElementsHashCode()
		{
			int hashCode;
			if (!int.TryParse(_query[_hashCodeFieldName], out hashCode))
			{
				return null;
			}
			return hashCode;
		}
	}
}