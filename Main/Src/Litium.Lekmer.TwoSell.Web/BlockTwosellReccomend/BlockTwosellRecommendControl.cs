﻿using System;
using Litium.Lekmer.Avail.Web;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.TwoSell.Web
{
	public class BlockTwosellRecommendControl : BlockProductPageControlBase<IBlockList>
	{
		private readonly IBlockListService _blockListService;
		private readonly IRecommenderProductService _recommenderProductService;
		private readonly ICustomerSession _customerSession;

		private string _customerKey;
		private int _productId;
		private int _itemsToReturn;

		private ProductCollection _products;

		public BlockTwosellRecommendControl(
			ITemplateFactory templateFactory,
			IBlockListService blockListService,
			IRecommenderProductService recommenderProductService,
			ICustomerSession customerSession
			)
			: base(templateFactory)
		{
			_blockListService = blockListService;
			_recommenderProductService = recommenderProductService;
			_customerSession = customerSession;
		}

		protected override IBlockList GetBlockById(int blockId)
		{
			return _blockListService.GetById(blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Block Twosell Recommend works only on product page ]");
			}

			Initialize();

			if (_products.Count <= 0)
			{
				return RenderNoResultsContent();
			}

			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return fragment.Render() ?? string.Empty;
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		protected virtual BlockContent RenderNoResultsContent()
		{
			var fragment = Template.GetFragment("Empty");

			fragment.AddEntity(Block);

			return new BlockContent(string.Empty, fragment.Render(), string.Empty);
		}


		private void Initialize()
		{
			InitializeCustomerKey();
			InitializeProductId();
			InitializeItemsToReturn();

			_products = _recommenderProductService.GetProductsBySessionAndProduct(UserContext.Current, _customerKey, _productId, _itemsToReturn);
		}

		private void InitializeCustomerKey()
		{
			_customerKey = CustomerIdentificationCookie.GetCustomerIdentificationId(_customerSession.SignedInCustomer);
		}

		private void InitializeProductId()
		{
			IProductView product = Product;
			if (product != null)
			{
				_productId = product.Id;
			}
		}

		private void InitializeItemsToReturn()
		{
			_itemsToReturn = Math.Max(Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount, Block.Setting.ActualTotalItemCount);
		}
	}
}
