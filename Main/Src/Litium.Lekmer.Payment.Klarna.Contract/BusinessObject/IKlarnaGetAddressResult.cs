﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaGetAddressResult : IKlarnaResult
	{
		Collection<IKlarnaAddress> KlarnaAddresses { get; set; }
	}
}