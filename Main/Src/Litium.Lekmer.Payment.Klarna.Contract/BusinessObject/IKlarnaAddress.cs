using Litium.Scensum.Customer;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaAddress : IAddress
	{
		string FirstName { get; set; }
		string LastName { get; set; }
		bool IsCompany { get; set; }
		string CompanyName { get; set; }
	}
}