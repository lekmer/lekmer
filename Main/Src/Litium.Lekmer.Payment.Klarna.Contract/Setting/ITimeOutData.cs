﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public interface ITimeOutData
	{
		int OrderValue { get; set; }
		int TimeOutValue { get; set; }
	}
}