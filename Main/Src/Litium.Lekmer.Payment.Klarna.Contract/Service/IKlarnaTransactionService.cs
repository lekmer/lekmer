﻿namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaTransactionService
	{
		int CreateGetAddress(int shopId, int mode, string civicNumber);
		int CreateReservation(int shopId, int mode, int transactionType, string civicNumber, int orderId, decimal amount, int currencyId, int pClass, int yearlySalary);
		int CreateCancelReservation(int shopId, int mode, int transactionType, int orderId, string reservationNumber);
		int CreateCheckOrderStatus(int shopId, int mode, int transactionType, int orderId, string reservationNumber);

		void SaveResult(IKlarnaResult klarnaResult);
		void SaveReservationResponse(IKlarnaReservationResult klarnaReservationResult);
	}
}
