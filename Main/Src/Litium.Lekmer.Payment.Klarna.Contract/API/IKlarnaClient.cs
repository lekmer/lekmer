﻿using System.Collections.Generic;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaClient
	{
		IKlarnaSetting KlarnaSetting { get; set; }

		void FetchPClasses();
		List<object> GetPClasses();

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Norway, Sweden and Denmark.
		/// </summary>
		IKlarnaGetAddressResult GetAddress(IChannel channel, string civicNumber, string ip);

		/// <summary>
		/// Reserves an amount at Klarna. Can only be used in Sweden, Denmark, Norway, Finland and Holland.
		/// </summary>
		IKlarnaReservationResult ReserveAmount(IChannel channel, ICustomer customer, IOrderFull order, int partPaymentId, decimal totalAmount, double? yearlySalary);

		IKlarnaResult CancelReservation(int orderId, string reservationNumber);

		IKlarnaResult CheckOrderStatus(int orderId, string reservationNumber);

		double CalculateCheapestMonthlyFee();
		double CalculateCheapestMonthlyCost(double sum, bool isCheckOut);
		double CalculateMonthlyCost(double sum, object pClass, bool isCheckOut);

		void Initialize(string channelCommonName);
	}
}