using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class OrderedProductFeedbackForm : ControlBase
	{
		private const int REVIEW_MESSAGE_MAXLENGTH = 3000;


		private readonly string _postUrl;


		public int PostBackProductId { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, OrderedProductFeedback> Feedbacks { get; set; }

		protected NameValueCollection RequestForm
		{
			get { return Request.Form; }
		}

		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals("feedback"); }
		}

		public OrderedProductFeedbackForm(string postUrl)
		{
			_postUrl = postUrl;
		}


		public virtual void SetDefaultValues(int productId, ICustomer customer)
		{
			if (customer != null && customer.CustomerInformation != null)
			{
				Feedbacks[productId].ReviewAuthorFirstName = customer.CustomerInformation.FirstName;
				Feedbacks[productId].ReviewAuthorLastName = customer.CustomerInformation.LastName;
			}
		}


		public virtual ValidationResult Validate(int productId)
		{
			var validationResult = new ValidationResult();

			ValidateReview(productId, validationResult);
			ValidateRatings(productId, validationResult);

			return validationResult;
		}

		protected virtual void ValidateReview(int productId, ValidationResult validationResult)
		{
			var feedback = Feedbacks[productId];
			if (feedback.ReviewAuthorName.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.AuthorNotProvided"));
			}

			if (feedback.ReviewTitle.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.TitleNotProvided"));
			}

			if (feedback.ReviewMessage.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.MessageNotProvided"));
			}

			if (feedback.ReviewMessage != null && feedback.ReviewMessage.Length > REVIEW_MESSAGE_MAXLENGTH)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.MessageTooLong"));
			}
		}

		protected virtual void ValidateRatings(int productId, ValidationResult validationResult)
		{
			var feedback = Feedbacks[productId];
			bool ratingsWithoutSelection = false;
			bool ratingsWithInvalidSelection = false;

			foreach (IRating rating in feedback.Ratings.Values)
			{
				if (feedback.SelectedRatingItems.ContainsKey(rating.Id))
				{
					int ratingItemId = feedback.SelectedRatingItems[rating.Id];

					IRatingItem ratingItem = rating.RatingItems.FirstOrDefault(ri => ri.Id == ratingItemId);

					if (ratingItem == null)
					{
						ratingsWithInvalidSelection = true;
						break;
					}
				}
				else
				{
					ratingsWithoutSelection = true;
					break;
				}
			}

			if (ratingsWithoutSelection || ratingsWithInvalidSelection)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.RatingNotProvided"));
			}
		}


		public virtual void MapFromRequest(int productId)
		{
			NameValueCollection form = RequestForm;

			var feedback = Feedbacks[productId];

			if (string.IsNullOrEmpty(feedback.ReviewAuthorName))
			{
				feedback.ReviewAuthorName = form[feedback.ReviewAuthorFormName];
			}
			if (string.IsNullOrEmpty(feedback.ReviewAuthorFirstName))
			{
				feedback.ReviewAuthorFirstName = form[feedback.ReviewAuthorFirstNameFormName];
			}
			if (string.IsNullOrEmpty(feedback.ReviewAuthorLastName))
			{
				feedback.ReviewAuthorLastName = form[feedback.ReviewAuthorLastNameFormName];
			}

			feedback.ReviewTitle = form[feedback.ReviewTitleFormName];
			feedback.ReviewMessage = form[feedback.ReviewMessageFormName];

			feedback.SelectedRatingItems = new Dictionary<int, int>();
			foreach (IRating rating in feedback.Ratings.Values)
			{
				var ratingFormName = feedback.GetRatingFormName(rating.Id);

				int? ratingItemId = form.GetInt32OrNull(ratingFormName);

				if (ratingItemId.HasValue)
				{
					feedback.SelectedRatingItems[rating.Id] = ratingItemId.Value;
				}
			}

			if (form.Get(feedback.ProductId.ToString(CultureInfo.InvariantCulture)) != null)
			{
				PostBackProductId = feedback.ProductId;
			}
		}

		public virtual void MapToFeedback(int productId, IRatingReviewFeedbackRecord ratingReviewFeedbackRecord)
		{
			if (!Validate(productId).IsValid)
			{
				throw new ValidationException("Couldn't validate the review.");
			}

			var feedback = Feedbacks[productId];

			var review = IoC.Resolve<IReview>();
			review.AuthorName = feedback.ReviewAuthorName;
			review.Title = feedback.ReviewTitle;
			review.Message = feedback.ReviewMessage;

			ratingReviewFeedbackRecord.Review = review;

			ratingReviewFeedbackRecord.RatingItemProductVotes = new Collection<IRatingItemProductVote>();
			foreach (var selectedRatingItem in feedback.SelectedRatingItems)
			{
				var productVote = IoC.Resolve<IRatingItemProductVote>();
				productVote.RatingId = selectedRatingItem.Key;
				productVote.RatingItemId = selectedRatingItem.Value;

				ratingReviewFeedbackRecord.RatingItemProductVotes.Add(productVote);
			}
		}

		public virtual void MapToForm(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", _postUrl);
		}

		public virtual void MapToFragmentFeedbackForm(int productId, Fragment fragment)
		{
			fragment.AddVariable("Form-Id", productId.ToString(CultureInfo.InvariantCulture));
		}

		public virtual void MapToFragmentReview(int productId, Fragment fragment)
		{
			fragment.AddVariable("Form.Review.Author.Name", Feedbacks[productId].ReviewAuthorFormName);
			fragment.AddVariable("Form.Review.Author.FirstName.Name", Feedbacks[productId].ReviewAuthorFirstNameFormName);
			fragment.AddVariable("Form.Review.Author.LastName.Name", Feedbacks[productId].ReviewAuthorLastNameFormName);
			fragment.AddVariable("Form.Review.Title.Name", Feedbacks[productId].ReviewTitleFormName);
			fragment.AddVariable("Form.Review.Message.Name", Feedbacks[productId].ReviewMessageFormName);

			fragment.AddVariable("Form.Review.Author.Value", Feedbacks[productId].ReviewAuthorName);
			fragment.AddVariable("Form.Review.Author.FirstName.Value", Feedbacks[productId].ReviewAuthorFirstName);
			fragment.AddVariable("Form.Review.Author.LastName.Value", Feedbacks[productId].ReviewAuthorLastName);
			fragment.AddVariable("Form.Review.Title.Value", Feedbacks[productId].ReviewTitle);
			fragment.AddVariable("Form.Review.Message.Value", Feedbacks[productId].ReviewMessage);
		}

		public virtual void MapToFragmentRatingItem(int productId, Fragment fragment, IRatingItem ratingItem)
		{
			bool itemSelected = false;

			if (Feedbacks[productId].SelectedRatingItems != null && Feedbacks[productId].SelectedRatingItems.ContainsKey(ratingItem.RatingId))
			{
				itemSelected = Feedbacks[productId].SelectedRatingItems[ratingItem.RatingId] == ratingItem.Id;
			}

			fragment.AddVariable("Form.Review.RatingItem.Name", Feedbacks[productId].GetRatingFormName(ratingItem.RatingId));

			fragment.AddCondition("RatingItem.Selected", itemSelected);
		}
	}

	public class OrderedProductFeedback
	{
		public int ProductId { get; set; }
		public string ReviewAuthorName { get; set; }
		public string ReviewAuthorFirstName { get; set; }
		public string ReviewAuthorLastName { get; set; }
		public string ReviewTitle { get; set; }
		public string ReviewMessage { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, IRating> Ratings { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, IRatingItem> RatingItems { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> SelectedRatingItems { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public string ReviewAuthorFormName
		{
			get { return "productId-id-" + ProductId + "-review-author"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public string ReviewAuthorFirstNameFormName
		{
			get { return "productId-id-" + ProductId + "-review-author-first-name"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public string ReviewAuthorLastNameFormName
		{
			get { return "productId-id-" + ProductId + "-review-author-last-name"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public string ReviewTitleFormName
		{
			get { return "productId-id-" + ProductId + "-review-title"; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		public string ReviewMessageFormName
		{
			get { return "productId-id-" + ProductId + "-review-message"; }
		}

		public string GetRatingFormName(int ratingId)
		{
			return "productId-id-" + ProductId + "-rating-" + ratingId;
		}

		public OrderedProductFeedback(
			int productId,
			string reviewAuthorName,
			string reviewAuthorFirstName,
			string reviewAuthorLastName,
			string reviewTitle,
			string reviewMessage,
			Dictionary<int, IRating> ratings,
			Dictionary<int, IRatingItem> ratingItems)
		{
			ProductId = productId;
			ReviewAuthorName = reviewAuthorName;
			ReviewAuthorFirstName = reviewAuthorFirstName;
			ReviewAuthorLastName = reviewAuthorLastName;
			ReviewTitle = reviewTitle;
			ReviewMessage = reviewMessage;
			Ratings = ratings;
			RatingItems = ratingItems;
		}
	}
}