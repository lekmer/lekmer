using System.Globalization;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class ReviewEntityMapper : EntityMapper<IReview>
	{
		public override void AddEntityVariables(Fragment fragment, IReview item)
		{
			fragment.AddVariable("Review.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Review.AuthorName", item.AuthorName);
			fragment.AddVariable("Review.Title", item.Title);

			string message = Encoder.EncodeHtml(item.Message).Replace("[", "&#91;").Replace("]", "&#93;");
			message = Encoder.EncodeMultiline(message);
			fragment.AddVariable("Review.Message", message, VariableEncoding.None);

			fragment.AddVariable("Review.Email", item.Email);
		}
	}
}