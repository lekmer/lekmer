using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockLatestFeedbackListEntityMapper : LekmerBlockEntityMapper<IBlockLatestFeedbackList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockLatestFeedbackList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.NumberOfItems", item.NumberOfItems.ToString(CultureInfo.InvariantCulture));
		}
	}
}