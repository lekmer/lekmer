﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class FacebookUserDataMapper : DataMapperBase<IFacebookUser>
	{
		public FacebookUserDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IFacebookUser Create()
		{
			var user = IoC.Resolve<IFacebookUser>();
			user.CustomerId = MapValue<int>("FacebookCustomerUser.CustomerId");
			user.CustomerRegistryId = MapValue<int>("FacebookCustomerUser.CustomerRegistryId");
			user.FacebookId = MapValue<string>("FacebookCustomerUser.FacebookId");
			user.Name = MapValue<string>("FacebookCustomerUser.Name");
			user.Email = MapNullableValue<string>("FacebookCustomerUser.Email");
			user.CreatedDate = MapValue<DateTime>("FacebookCustomerUser.CreatedDate");
			user.IsAlternateLogin = MapValue<bool>("FacebookCustomerUser.IsAlternateLogin");
			user.Status = BusinessObjectStatus.Untouched;
			return user;
		}
	}
}