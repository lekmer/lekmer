﻿using System.Data;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer.Mapper
{
	public class LekmerBillingAddressDataMapper : LekmerAddressDataMapper<IBillingAddress>
	{
		public LekmerBillingAddressDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override string Prefix
		{
			get { return "Billing"; }
		}
	}
}
