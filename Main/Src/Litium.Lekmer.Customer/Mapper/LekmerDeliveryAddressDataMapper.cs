﻿using System.Data;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer.Mapper
{
	public class LekmerDeliveryAddressDataMapper : LekmerAddressDataMapper<IDeliveryAddress>
	{
		public LekmerDeliveryAddressDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override string Prefix
		{
			get { return "Delivery"; }
		}
	}
}
