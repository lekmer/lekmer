﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class LekmerUserRepository : UserRepository
	{
		public override int Save(IUser user)
		{
			var lekmerUser = (ILekmerUser) user;
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", user.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerRegistryId", user.CustomerRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("UserName", user.UserName, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Password", user.Password, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("PasswordSalt", user.PasswordSalt, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CreatedDate", user.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("IsAlternateLogin", lekmerUser .IsAlternateLogin, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("UserRepository.Save");
			user.Id = new DataHandler().ExecuteCommandWithReturnValue("[customer].[pUserSave]", parameters, dbSettings);
			return user.Id;
		}
		public virtual void DeleteAlternateAccount(IUser user)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", user.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("UserName", user.UserName, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("UserRepository.DeleteAlternateLogin");
			new DataHandler().ExecuteCommand("[customerlek].[pCustomerUserDeleteAlternateLogin]", parameters, dbSettings);
		}
	}
}