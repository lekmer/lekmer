﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class LekmerCustomerInformationRepository : CustomerInformationRepository
	{
		public override int Save(ICustomerInformation customerInformation)
		{
			var lekmerCustomerInformation = (ILekmerCustomerInformation)customerInformation;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerInformation.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("FirstName", customerInformation.FirstName, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("LastName", customerInformation.LastName, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CivicNumber", customerInformation.CivicNumber, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("PhoneNumber", customerInformation.PhoneNumber, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CellPhoneNumber", customerInformation.CellPhoneNumber, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Email", customerInformation.Email, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CreatedDate", customerInformation.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("DefaultBillingAddressId", customerInformation.DefaultBillingAddressId, SqlDbType.Int),
					ParameterHelper.CreateParameter("DefaultDeliveryAddressId", customerInformation.DefaultDeliveryAddressId, SqlDbType.Int),
					ParameterHelper.CreateParameter("GenderTypeId", lekmerCustomerInformation.GenderTypeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IsCompany", lekmerCustomerInformation.IsCompany, SqlDbType.Bit),
					ParameterHelper.CreateParameter("AlternateAddressId", lekmerCustomerInformation.AlternateAddressId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CustomerInformationRepository.Save");

			customerInformation.Id = new DataHandler().ExecuteCommandWithReturnValue("[customerlek].[pCustomerInformationSave]", parameters, dbSettings);

			return customerInformation.Id;
		}

		public virtual void DeleteByCustomer(int customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerCustomerInformationRepository.DeleteByCustomer");
			new DataHandler().ExecuteCommand("[customerlek].[pCustomerInformationDeleteByCustomer]", parameters, dbSettings);
		}
	}
}