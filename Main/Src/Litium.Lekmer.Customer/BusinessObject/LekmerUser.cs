﻿using System;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class LekmerUser : User, ILekmerUser
	{
		private bool _isAlternateLogin;
		public bool IsAlternateLogin
		{
			get
			{ return _isAlternateLogin; }
			set
			{
				CheckChanged(_isAlternateLogin, value);
				_isAlternateLogin = value;
			}
		}
	}
}
