﻿using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerUserService : UserService, ILekmerUserService
	{
		protected LekmerUserRepository LekmerRepository { get; private set; }

		public LekmerUserService(UserRepository userRepository, ICustomerModuleChannelService customerModuleChannelService)
			: base(userRepository, customerModuleChannelService)
		{
			LekmerRepository = (LekmerUserRepository) userRepository;
		}

		public void DeleteAlternateAccount(IUser user)
		{
			LekmerRepository.DeleteAlternateAccount(user);
		}
	}
}