﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerService : CustomerService, ILekmerCustomerService
	{
		protected LekmerCustomerRepository LekmerRepository { get; private set; }
		protected IFacebookUserService FacebookUserService { get; private set; }

		public LekmerCustomerService(
			CustomerRepository customerRepository,
			ICustomerInformationService customerInformationService,
			IUserService userService,
			ICustomerModuleChannelService customerModuleChannelService,
			IFacebookUserService facebookUserService)
			: base(customerRepository, customerInformationService, userService, customerModuleChannelService)
		{
			LekmerRepository = (LekmerCustomerRepository)customerRepository;
			FacebookUserService = facebookUserService;
		}

		public virtual ILekmerCustomer Create(IUserContext context, bool hasInformation, bool isUser, bool isFacebookUser)
		{
			var customer = (ILekmerCustomer) base.Create(context, hasInformation, isUser);

			if (isFacebookUser)
			{
				if (FacebookUserService == null) throw new InvalidOperationException("FacebookUserService must be set before calling Create.");
				customer.FacebookUser = FacebookUserService.Create();
			}
			return customer;
		}

		public override int Save(IUserContext context, ICustomer customer)
		{
			if (customer == null) throw new ArgumentNullException("customer");
			if (UserService == null) throw new InvalidOperationException("UserService must be set before calling Save.");
			if (CustomerModuleChannelService == null) throw new InvalidOperationException("CustomerModuleChannelService must be set before calling Save.");
			if (CustomerInformationService == null) throw new InvalidOperationException("CustomerInformationService must be set before calling Save.");
			if (FacebookUserService == null) throw new InvalidOperationException("FacebookUserService must be set before calling Create.");

			ICustomerModuleChannel byId = CustomerModuleChannelService.GetById(context);
			if (byId == null) throw new InvalidOperationException("Current channel does not have the customer registry assigned.");

			customer.CustomerRegistryId = byId.CustomerRegistryId;
			using (var transactedOperation = new TransactedOperation())
			{
				customer.Id = Repository.Save(customer);
				if (customer.CustomerInformation != null)
				{
					customer.CustomerInformation.Id = customer.Id;
					CustomerInformationService.Save(context, customer.CustomerInformation);
				}
				if (customer.User != null)
				{
					customer.User.Id = customer.Id;
					UserService.Save(context, customer.User);
				}
				var lekmerCustomer = (ILekmerCustomer) customer;
				if (lekmerCustomer.FacebookUser != null)
				{
					lekmerCustomer.FacebookUser.CustomerId = customer.Id;
					lekmerCustomer.FacebookUser.CustomerRegistryId = customer.CustomerRegistryId;
					FacebookUserService.Save(lekmerCustomer.FacebookUser);
				}
				transactedOperation.Complete();
			}
			return customer.Id;
		}

		public virtual ICustomer GetByFacebookId(IUserContext context, string facebookId)
		{
			return LekmerRepository.GetByFacebookId(context.Channel.Id, facebookId);
		}
	}
}