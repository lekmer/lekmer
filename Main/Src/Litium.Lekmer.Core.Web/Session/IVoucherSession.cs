using Litium.Lekmer.Voucher;

namespace Litium.Lekmer.Core.Web
{
	public interface IVoucherSession
	{
		IVoucherCheckResult Voucher { get; set; }
		bool WasUsed { get; set; }
		string VoucherCode { get; set; }
	}
}