using Litium.Scensum.Core.Web;

namespace Litium.Lekmer.Core.Web
{
	/// <summary>
	/// Control for rendering paging.
	/// </summary>
	public interface ILekmerPagingControl : IPagingControl
	{
		int PageCount { get; }
		bool ViewAllModeAllowed { get; set; }
	}
}