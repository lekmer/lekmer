﻿using System.ComponentModel;

namespace Litium.Lekmer.InsuranceInfo.Contract
{
	public enum FieldNames
	{
		[Description("Förnamn")]
		FirstName = 1,
		[Description("Efternamn")]
		LastName = 2,
		[Description("Personnummer")]
		CivicNumber = 3,
		[Description("Adress")]
		Adress = 4,
		[Description("Postnummer")]
		PostalCode = 5,
		[Description("Ort")]
		City = 6,
		[Description("Telefon")]
		PhoneNumber = 7,
		[Description("Köpdatum")]
		PurchaseDate = 8,
		[Description("Säljare")]
		Säljare = 9,
		[Description("Category name")]
		Category = 10,
		[Description("SubCategory name")]
		SubCategory = 11,
		[Description("SubCategory id")]
		SubCategoryId = 12,
		[Description("Märke")]
		Brand = 13,
		[Description("Modell")]
		Modell = 14,
		[Description("Serienummer")]
		Serienummer = 15,
		[Description("Köpbelopp")]
		PurchaseAmount = 16,
		[Description("Premie")]
		Premie = 17,
		[Description("Försäkringsnummer")]
		Försäkringsnummer = 18,
		[Description("Löptid")]
		Löptid = 19,
		[Description("Försäkringsprodukt")]
		Försäkringsprodukt = 20,
		[Description("Antal")]
		Antal = 21,
		[Description("Netto premie")]
		NettoPremie = 22,
		[Description("Inköpsorder")]
		Inköpsorder = 23,
		[Description("Försäljningorder")]
		Försäljningorder = 24,
		[Description("Email")]
		Email = 25
	}
}