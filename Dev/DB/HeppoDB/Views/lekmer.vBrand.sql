SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vBrand]
AS
SELECT     
	B.BrandId as "Brand.BrandId",
    COALESCE (bt.[BrandTranslation.Title], B.Title) as "Brand.Title",
    COALESCE (bt.[BrandTranslation.Description], B.Description) as "Brand.Description",
    B.ExternalUrl as "Brand.ExternalUrl",
    B.MediaId as "Brand.MediaId",
    B.ErpId as  "Brand.ErpId",
    Ch.ChannelId,
    I.*,
    Bssr.ContentNodeId AS 'Brand.ContentNodeId'
FROM
	lekmer.tBrand AS B
	CROSS JOIN core.tChannel AS Ch
	LEFT JOIN media.vCustomImage AS I
		ON I.[Image.MediaId] = B.MediaId
		AND I.[Image.LanguageId] = Ch.LanguageId
	LEFT JOIN lekmer.vBrandTranslation AS bt 
		ON bt.[BrandTranslation.BrandId] = B.BrandId 
		AND bt.[BrandTranslation.LanguageId] = Ch.LanguageId
	LEFT JOIN sitestructure.tSiteStructureModuleChannel Ssmc
		ON Ssmc.ChannelId = Ch.ChannelId
	LEFT JOIN lekmer.tBrandSiteStructureRegistry Bssr
		ON Bssr.BrandId = B.BrandId AND Bssr.SiteStructureRegistryId = Ssmc.SiteStructureRegistryId
GO
