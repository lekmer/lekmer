SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomCategorySecure]
as
	select
		*
	from
		[product].[vCategorySecure]
GO
