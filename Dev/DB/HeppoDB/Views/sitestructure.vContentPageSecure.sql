SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentPageSecure]
AS
SELECT     
	cp.TemplateId AS 'ContentPage.TemplateId', 
	cp.MasterTemplateId AS 'ContentPage.MasterTemplateId', 
	cp.ContentPageTypeId AS 'ContentPage.ContentPageTypeId', 
	cp.Title AS 'ContentPage.PageTitle', 
	cp.UrlTitle AS 'ContentPage.UrlTitle',
	cp.MasterPageId AS 'ContentPage.MasterPageId',
	cp.IsMaster AS 'ContentPage.IsMaster', 
	vn.*, 
    pt.Title AS 'ContentPageType.Title', 
    pt.CommonName AS 'ContentPageType.CommonName'
FROM         
	sitestructure.tContentPage AS cp
	INNER JOIN sitestructure.vCustomContentNodeSecure AS vn ON vn.[ContentNode.ContentNodeId] = cp.ContentNodeId
	LEFT OUTER JOIN sitestructure.tContentPageType AS pt ON cp.ContentPageTypeId = pt.ContentPageTypeId

GO
