SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomPriceListFolder]
as
	select
		*
	from
		[product].[vPriceListFolder]
GO
