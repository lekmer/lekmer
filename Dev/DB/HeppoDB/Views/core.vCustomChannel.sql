SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [core].[vCustomChannel]
as
	select
		*
	from
		[core].[vChannel]

GO
