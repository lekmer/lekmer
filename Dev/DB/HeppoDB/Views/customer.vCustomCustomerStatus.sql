SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomerStatus]
as
	select
		*
	from
		[customer].[vCustomerStatus]
GO
