SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [order].[vCustomCartItem]
as
	select
		c.*,
		l.SizeId as [LekmerCartItem.SizeId],
		l.ErpId as [LekmerCartItem.ErpId]
	from
		[order].[vCartItem] c
		inner join [lekmer].tLekmerCartItem l on c.[CartItem.CartItemId] = l.CartItemId
GO
