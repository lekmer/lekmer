SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomAccess]
as
	select
		*
	from
		[sitestructure].[vAccess]
GO
