SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockProductImageList]
as
	select
		*
	from
		[product].[vBlockProductImageList]
GO
