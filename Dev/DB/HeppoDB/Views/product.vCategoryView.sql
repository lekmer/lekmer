SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vCategoryView]
AS
	SELECT     
		c.*,
		ch.[Channel.Id],
		r.[CategoryRegistry.ProductParentContentNodeId],
		r.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		[product].[vCustomCategory] AS c
		CROSS JOIN core.vCustomChannel ch
		LEFT JOIN sitestructure.tSiteStructureModuleChannel mch ON mch.ChannelId = ch.[Channel.Id]
		LEFT JOIN product.vCustomCategorySiteStructureRegistry r
			ON mch.SiteStructureRegistryId = r.[CategoryRegistry.SiteStructureRegistryId]
			AND c.[Category.Id] = r.[CategoryRegistry.CategoryId]
GO
