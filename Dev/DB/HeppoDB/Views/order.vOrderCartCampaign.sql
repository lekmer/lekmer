SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vOrderCartCampaign]
AS
SELECT 
	tcc.OrderCartCampaignId AS 'OrderCampaign.Id',
	tcc.CampaignId AS 'OrderCampaign.CampaignId', 
	tcc.OrderId AS 'OrderCampaign.OrderId', 
	tcc.Title AS 'OrderCampaign.Title'
FROM 
	[order].tOrderCartCampaign tcc






GO
