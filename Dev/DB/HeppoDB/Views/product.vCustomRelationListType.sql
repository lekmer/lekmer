SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomRelationListType]
as
	select
		*
	from
		[product].[vRelationListType]
GO
