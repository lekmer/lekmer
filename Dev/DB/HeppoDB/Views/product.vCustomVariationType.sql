SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomVariationType]
as
	select
		*
	from
		[product].[vVariationType]
GO
