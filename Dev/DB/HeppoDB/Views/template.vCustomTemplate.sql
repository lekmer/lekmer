SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomTemplate]
as
	select
		*
	from
		[template].[vTemplate]
GO
