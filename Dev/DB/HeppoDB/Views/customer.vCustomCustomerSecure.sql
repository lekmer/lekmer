SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomerSecure]
as
	select
		*
	from
		[customer].[vCustomerSecure]
GO
