SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [media].[vMediaFormatGroup]
AS
SELECT 
	 [MediaFormatGroupId] 'MediaFormatGroup.Id',
	 [Title] 'MediaFormatGroup.Title',
	 [CommonName] 'MediaFormatGroup.CommonName'
FROM [media].[tMediaFormatGroup]
GO
