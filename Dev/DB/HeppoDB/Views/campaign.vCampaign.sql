SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCampaign]
as
	select
		CampaignId as 'Campaign.Id',
		CampaignRegistryId as 'Campaign.CampaignRegistryId',
		FolderId as 'Campaign.FolderId',
		Title as 'Campaign.Title',
		CampaignStatusId as 'Campaign.CampaignStatusId',
		StartDate as 'Campaign.StartDate',
		EndDate as 'Campaign.EndDate',
		Priority as 'Campaign.Priority',
		Exclusive as 'Campaign.Exclusive',
		s.*
	from
		campaign.tCampaign c
		inner join campaign.vCustomCampaignStatus s on c.CampaignStatusId = s.[CampaignStatus.Id]

GO
