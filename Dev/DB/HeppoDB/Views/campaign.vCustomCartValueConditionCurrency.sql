SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCartValueConditionCurrency]
as
	select
		*
	from
		[campaign].[vCartValueConditionCurrency]
GO
