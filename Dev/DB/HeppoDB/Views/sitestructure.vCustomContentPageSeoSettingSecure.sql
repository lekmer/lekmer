SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentPageSeoSettingSecure]
as
	select
		*
	from
		[sitestructure].[vContentPageSeoSettingSecure]
GO
