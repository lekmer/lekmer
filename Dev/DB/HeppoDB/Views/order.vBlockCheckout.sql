SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vBlockCheckout]
AS
SELECT 
		 [BlockId] AS 'BlockCheckout.CheckoutBlockId'
		,[RedirectContentNodeId] AS 'BlockCheckout.RedirectContentNodeId'
FROM 
		[order].[tBlockCheckout]
GO
