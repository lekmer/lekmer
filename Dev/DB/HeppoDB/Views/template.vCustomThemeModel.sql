SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomThemeModel]
as
	select
		*
	from
		[template].[vThemeModel]
GO
