SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vPercentagePriceDiscountAction]
as
	select
		DiscountAmount as 'PercentagePriceDiscountAction.DiscountAmount', 
		p.IncludeAllProducts AS 'PercentagePriceDiscountAction.IncludeAllProducts',
		a.*
	from
		campaign.tPercentagePriceDiscountAction p
		inner join campaign.vCustomProductAction a on p.ProductActionId = a.[ProductAction.Id]

GO
