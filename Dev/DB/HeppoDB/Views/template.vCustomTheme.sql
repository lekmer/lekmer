SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomTheme]
as
	select
		*
	from
		[template].[vTheme]
GO
