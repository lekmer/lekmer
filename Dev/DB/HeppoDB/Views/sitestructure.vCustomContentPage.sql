SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentPage]
as
	select
		*
	from
		[sitestructure].[vContentPage]
GO
