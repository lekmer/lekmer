SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlockSecure]
as
	select
		*
	from
		[sitestructure].[vBlockSecure]
GO
