SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view [lekmer].[vProductDiscountAction]
as
	select
		p.CurrencyId as 'ProductDiscountAction.CurrencyId', 
		a.*
	from
		lekmer.tProductDiscountAction p
		inner join campaign.vCustomProductAction a on p.ProductActionId = a.[ProductAction.Id]


GO
