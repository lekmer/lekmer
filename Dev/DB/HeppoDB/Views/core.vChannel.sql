SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [core].[vChannel]
AS
SELECT
	CHA.ChannelId AS [Channel.Id],
	CHA.Title AS [Channel.Title],
	CHA.CommonName AS [Channel.CommonName],
	CHA.ApplicationName AS [Channel.ApplicationName], 
	CHA.AlternateLayoutRatio AS [Channel.AlternateLayoutRatio],
	COU.[Country.Id],
	COU.[Country.Title],
	COU.[Country.ISO],
	COU.[Country.IsInEU], 
	COU.[Country.PhonePrefix],
	CUR.[Currency.Id],
	CUR.[Currency.ISO],
	CUR.[Currency.Title],
	CUR.[Currency.PriceFormat],
	CUR.[Currency.NumberOfDecimals], 
	LAN.[Language.Id],
	LAN.[Language.Title],
	LAN.[Language.ISO],
	CUL.[Culture.Id],
	CUL.[Culture.CommonName],
	CUL.[Culture.Title]
FROM
	core.tChannel AS CHA INNER JOIN
	core.vCustomCountry AS COU ON COU.[Country.Id] = CHA.CountryId INNER JOIN
	core.vCustomCurrency AS CUR ON CUR.[Currency.Id] = CHA.CurrencyId INNER JOIN
	core.vCustomLanguage AS LAN ON LAN.[Language.Id] = CHA.LanguageId LEFT OUTER JOIN
	core.vCustomCulture AS CUL ON CUL.[Culture.Id] = CHA.CultureId


GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CHA"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 123
               Right = 230
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COU"
            Begin Extent = 
               Top = 6
               Left = 268
               Bottom = 123
               Right = 458
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CUR"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 243
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "LAN"
            Begin Extent = 
               Top = 126
               Left = 303
               Bottom = 228
               Right = 463
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CUL"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 348
               Right = 234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3600
         Alias = 900
         Table = 1170
      ', 'SCHEMA', N'core', 'VIEW', N'vChannel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'   Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'core', 'VIEW', N'vChannel', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'core', 'VIEW', N'vChannel', NULL, NULL
GO
