SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomCategory]
as
	select
		*
	from
		[product].[vCategory]
GO
