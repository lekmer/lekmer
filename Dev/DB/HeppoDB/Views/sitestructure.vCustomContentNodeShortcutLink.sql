SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeShortcutLink]
as
	select
		*
	from
		[sitestructure].[vContentNodeShortcutLink]
GO
