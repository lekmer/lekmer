SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [lekmer].[vPriceIntervalSecure]
AS
SELECT     
	p.PriceIntervalId AS 'PriceInterval.PriceIntervalId',
	p.[From] AS 'PriceInterval.From',
	p.[To] AS 'PriceInterval.To',
	p.[Title] AS 'PriceInterval.Title',
	c.*,
	NULL AS 'PriceInterval.ContentNodeId'
FROM
	[lekmer].[tPriceInterval] p
	inner join [core].[vCustomCurrency] c on p.CurrencyId = c.[Currency.Id]
GO
