SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vCustomProductView]
AS
SELECT     p.[Product.Id], p.[Product.ItemsInPackage], p.[Product.ErpId], p.[Product.EanCode], p.[Product.NumberInStock], p.[Product.CategoryId], p.[Product.WebShopTitle], 
                      p.[Product.Title], p.[Product.ProductStatusId], p.[Product.Description], p.[Product.ShortDescription], p.[Product.ChannelId], p.[Product.CurrencyId], 
                      p.[Product.PriceListRegistryId], p.[Image.Width], p.[Image.Height], p.[Image.AlternativeText], p.[Image.MediaId], p.[Media.Id], p.[Media.FormatId], p.[Media.FolderId], 
                      p.[LekmerImage.Link],p.[LekmerImage.Parameter],p.[LekmerImage.TumbnailImageUrl],p.[LekmerImage.HasImage],
                      p.[Media.Title], p.[Media.FileName], p.[MediaFormat.Extension], p.[MediaFormat.CommonName], p.[Image.LanguageId], p.[Product.ParentContentNodeId], 
                      p.[Product.TemplateContentNodeId], lp.[Lekmer.BrandId], lp.[Lekmer.IsBookable], lp.[Lekmer.AgeFromMonth], lp.[Lekmer.AgeToMonth], lp.[Lekmer.IsNewFrom], 
                      lp.[Lekmer.IsNewTo], COALESCE (lpt.[Product.Measurement], lp.[Lekmer.Measurement]) AS [Lekmer.Measurement], lp.[Lekmer.BatteryTypeId], 
                      lp.[Lekmer.NumberOfBatteries], lp.[Lekmer.IsBatteryIncluded], lp.[Lekmer.ExpectedBackInStock], lp.[Lekmer.CreatedDate], lp.[Lekmer.SizeDeviationId], 
                      lp.[Lekmer.HasSizes], bt.[BatteryType.Id], bt.[BatteryType.Title], sd.[SizeDeviation.Id], sd.[SizeDeviation.Title], sd.[SizeDeviation.CommonName], 
                      pu.[ProductUrl.UrlTitle] AS [Lekmer.UrlTitle], lp.[Lekmer.LekmerErpId]
FROM         product.vProductView AS p INNER JOIN
                      lekmer.vLekmerProduct AS lp ON p.[Product.Id] = lp.[Lekmer.ProductId] INNER JOIN
                      core.vCustomChannel AS c ON p.[Product.ChannelId] = c.[Channel.Id] INNER JOIN
                      lekmer.vProductUrl AS pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId] LEFT OUTER JOIN
                      lekmer.vLekmerProductTranslation AS lpt ON lp.[Lekmer.ProductId] = lpt.[Product.Id] AND c.[Language.Id] = lpt.[Language.Id] LEFT OUTER JOIN
                      lekmer.vBatteryType AS bt ON lp.[Lekmer.BatteryTypeId] = bt.[BatteryType.Id] LEFT OUTER JOIN
                      lekmer.vSizeDeviation AS sd ON lp.[Lekmer.SizeDeviationId] = sd.[SizeDeviation.Id]

GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "p"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 286
            End
            DisplayFlags = 280
            TopColumn = 24
         End
         Begin Table = "lp"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 245
               Right = 268
            End
            DisplayFlags = 280
            TopColumn = 12
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 365
               Right = 273
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "pu"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 470
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "lpt"
            Begin Extent = 
               Top = 366
               Left = 276
               Bottom = 485
               Right = 471
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "bt"
            Begin Extent = 
               Top = 474
               Left = 38
               Bottom = 563
               Right = 211
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "sd"
            Begin Extent = 
               Top = 486
               Left = 249
               Bottom = 590
               Right = 474
            End
            DisplayFlags = 280
            TopColumn = 0
      ', 'SCHEMA', N'product', 'VIEW', N'vCustomProductView', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'   End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2520
         Alias = 3810
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'product', 'VIEW', N'vCustomProductView', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', 2, 'SCHEMA', N'product', 'VIEW', N'vCustomProductView', NULL, NULL
GO
