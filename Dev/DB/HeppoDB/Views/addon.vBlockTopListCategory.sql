SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view addon.vBlockTopListCategory
as
	select
		b.BlockId as 'BlockTopListCategory.BlockId',
		b.CategoryId as 'BlockTopListCategory.CategoryId',
		b.IncludeSubcategories as 'BlockTopListCategory.IncludeSubcategories',
		c.*
	from
		addon.tBlockTopListCategory b
		inner join product.vCustomCategory c on b.CategoryId = c.[Category.Id]

GO
