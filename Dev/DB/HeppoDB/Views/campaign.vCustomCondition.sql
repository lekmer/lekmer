SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCondition]
as
	select
		*
	from
		[campaign].[vCondition]
GO
