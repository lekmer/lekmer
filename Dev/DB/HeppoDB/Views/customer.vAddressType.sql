SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [customer].[vAddressType]
AS
select
		AddressTypeId 'AddressType.AddressTypeId',
		Title 'AddressType.Title',
		CommonName 'AddressType.CommonName'
from
		customer.tAddressType
GO
