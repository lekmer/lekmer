SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [addon].[vCustomReviewStatus]
as
         select
                   *
         from
                   [addon].[vReviewStatus]

GO
