SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vLekmerProductTranslation]
AS
SELECT
	pt.[ProductId] AS 'Product.Id',
	pt.[Measurement] AS 'Product.Measurement',
	l.[LanguageId] AS 'Language.Id',
	l.[Title] AS 'Language.Title',
	l.[ISO] AS 'Language.ISO'
FROM
	lekmer.[tLekmerProductTranslation] AS pt
	INNER JOIN [core].[tLanguage] l ON pt.LanguageId = l.LanguageId

GO
