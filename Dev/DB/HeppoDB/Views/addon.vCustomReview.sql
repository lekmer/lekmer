SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [addon].[vCustomReview]
as
         select
                   *
         from
                   [addon].[vReview]

GO
