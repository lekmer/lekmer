SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




create VIEW [lekmer].[vProductPopularity]
AS
SELECT 
	pp.[ProductId] AS 'ProductPopularity.ProductId',
	pp.[ChannelId] AS 'ProductPopularity.ChannelId',
	pp.[Popularity] AS 'ProductPopularity.Popularity'
FROM 
	lekmer.[tProductPopularity] AS pp


GO
