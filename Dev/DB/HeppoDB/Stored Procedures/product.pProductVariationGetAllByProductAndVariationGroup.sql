SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductVariationGetAllByProductAndVariationGroup]
	@ProductId			int,
	@VariationGroupId	int	
as
begin
	SET NOCOUNT ON;

	SELECT 
		*
	FROM 
		[product].[vCustomProductVariation] pv
	where
		pv.[ProductVariation.ProductId] = @ProductId
		AND pv.[ProductVariation.VariationGroupId] = @VariationGroupId
end

GO
