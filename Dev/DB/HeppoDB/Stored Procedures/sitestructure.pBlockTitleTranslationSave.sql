SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [sitestructure].[pBlockTitleTranslationSave]
	@BlockId	INT,
	@LanguageId	INT,
	@Title		NVARCHAR(MAX)
AS
begin
	set nocount on
	
	update
		[sitestructure].[tBlockTranslation]
	set
		[Title] = @Title	
	where
		[BlockId] = @BlockId AND
		[LanguageId] = @LanguageId
	if  @@ROWCOUNT = 0
	begin		
		insert into [sitestructure].[tBlockTranslation]
		(
			[BlockId],
			[LanguageId],
			[Title]				
		)
		values
		(
			@BlockId,
			@LanguageId,
			@Title
		)
	end
end	
GO
