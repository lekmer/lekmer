SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE procedure messaging.pBatchDeleteWithoutMessagesAndRecipientsAndWithEndDate
as
begin

	delete from 
		[messaging].[tBatch]
	where
		BatchId not in (
			select BatchId from [messaging].[tMessage]
			where BatchId is not null) and
		BatchId not in (
			select BatchId from [messaging].[tRecipient]
			where BatchId is not null) and
		EndDate is not null

end


GO
