SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pConditionSave]
	@ConditionId		int,
	@CampaignId			int,
	@ConditionTypeId	int
AS 
BEGIN
	UPDATE
		campaign.tCondition
	SET 
		CampaignId = @CampaignId,
		ConditionTypeId = @ConditionTypeId
	WHERE 
		ConditionId = @ConditionId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			campaign.tCondition
		( 
			CampaignId,
			ConditionTypeId
		)
		VALUES 
		(
			@CampaignId,
			@ConditionTypeId
		)
		SET @ConditionId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @ConditionId
END

GO
