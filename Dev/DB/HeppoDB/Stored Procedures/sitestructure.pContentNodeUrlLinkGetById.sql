SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeUrlLinkGetById]
	@ContentNodeId	int
as
begin
	select
		cnu.*
	from 
		[sitestructure].[vCustomContentNodeUrlLinkSecure] as cnu
	where
		cnu.[ContentNode.ContentNodeId] = @ContentNodeId
end

GO
