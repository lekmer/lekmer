SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [order].[pOrderProductCampaignSave]
	@Id int,
	@CampaignId int,
	@OrderItemId int,
	@Title nvarchar(500)
as
begin
	update
		[order].tOrderProductCampaign
	set
		CampaignId = @CampaignId,
		OrderItemId = @OrderItemId,
		Title = @Title
	where
		OrderProductCampaignId = @Id
	
	if @@rowcount = 0
	begin
		insert
			[order].tOrderProductCampaign
		(
			CampaignId,
			OrderItemId,
			Title
		)
		values
		(
			@CampaignId,
			@OrderItemId,
			@Title
		)
	end
end
GO
