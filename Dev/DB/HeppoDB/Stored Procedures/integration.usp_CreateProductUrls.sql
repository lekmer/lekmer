SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_CreateProductUrls]
	@ProductUrlXml		xml

AS
begin
 --			<products>
--		        <product id="103024" title="100124-0014" languageid="1"/>
--			    <product id="103025" title="100124-0014" languageid="1"/>
--               <product id="103026" title="100124-0014" languageid="1"/>
--	        </products>'
	set nocount on


	if object_id('tempdb..#tProductUrls') is not null
		drop table #tProductUrls

	create table #tProductUrls
	(
		ProductId int not null,
		Title nvarchar (250) COLLATE Finnish_Swedish_CI_AS not null,
		LanguageId int not null
		constraint PK_#tProductUrls primary key(ProductId, LanguageId)
	)
	
	begin try
		begin transaction
		-- Populate temp table
		insert #tProductUrls(ProductId, Title, LanguageId)		
		select 
		T.c.value('@id[1]', 'int'),
		T.c.value('@title[1]', 'nvarchar(250)'),
		T.c.value('@languageid[1]', 'int')
		--T.c.value('@id[1]', 'int'),
		--T.c.value('@height[1]', 'int'),
		--T.c.value('@width[1]', 'int'),
		--T.c.value('@extension[1]', 'nvarchar(10)')
		from @ProductUrlXml.nodes('/products/product') T(c)
		
		--select * from #tProductUrls
			
		delete 
			p
		from
			lekmer.tProductUrl p
			inner join #tProductUrls purl
				on p.ProductId = purl.ProductId
				and p.LanguageId = purl.LanguageId	
			
		insert lekmer.tProductUrl(ProductId, LanguageId, UrlTitle)
		select
			ProductId,
			LanguageId,
			Title
		from
			#tProductUrls
		
		
			
	commit transaction
	--return @Return
	end try
	begin catch

		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
		--return @Return
	end catch	
		
end
GO
