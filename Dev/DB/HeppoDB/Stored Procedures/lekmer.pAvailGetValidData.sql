SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [lekmer].[pAvailGetValidData] 
AS
BEGIN
	SET NOCOUNT ON;
	--SELECT ProductId FROM product.tProduct WHERE (ProductStatusId = 0)
	SELECT     P.ProductId
	FROM       product.tProduct AS P 
	WHERE     (P.ProductStatusId = 0) AND NumberInStock>0 AND P.ProductId IN
			  (SELECT distinct PS.ProductId
			   FROM lekmer.tProductSize AS PS
			   WHERE (SELECT SUM(lekmer.tProductSize.NumberInStock) 
					  FROM lekmer.tProductSize
					  WHERE lekmer.tProductSize.ProductId=PS.ProductId)>0)  
END



GO
