SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionSave]
	@ActionId	int,
	@CurrencyId int
AS 
BEGIN 
	UPDATE
		lekmer.tProductDiscountAction
	SET
		CurrencyId = @CurrencyId
	WHERE
		ProductActionId = @ActionId
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT 
			lekmer.tProductDiscountAction
		(
			ProductActionId,
			CurrencyId
		)
		VALUES
		(
			@ActionId,
			@CurrencyId
		)
	END
END
GO
