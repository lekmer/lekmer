SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportHeppoFailedOrders]

AS
begin
	set nocount on
	begin try
		--begin transaction
		
		select OrderId, CreatedDate, Email, case ChannelId when 1 then 'Sweden'
											when 4 then 'Norway'
											when 657 then 'Denmark'
											when 1000003 then 'Finland'
											end as Channel
		from [order].torder
		where --Createddate > DATEADD(day, -1, Convert(varchar, GETDATE(), 112))
		--and CreatedDate < GETDATE()
		--and 
			(
				OrderStatusId = 5 
				or OrderStatusId = 6
			)
	

	--commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
