SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [security].[pRoleRemovePrivileges]
	@RoleId int
as
begin
	set nocount on

	delete
		[security].[tRolePrivilege]
	where
		[RoleId] = @RoleId
end
GO
