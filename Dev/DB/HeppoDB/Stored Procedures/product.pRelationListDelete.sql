SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- Stored Procedure
/***********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 15:00
Description: delete try - cath  block and error logging 

***********************  Version 3  **********************
User: Oleh K.		Date: 09/02/2009		Time: 16:05
Description: Deleted check using table 
				product.tVariationRelationList

**********************************************************/
CREATE procedure [product].[pRelationListDelete]
	@RelationListId int	
as
begin
	delete
		product.tProductRelationList
	where
		RelationListId = @RelationListId

    delete
		product.tRelationListProduct
	where
		RelationListId = @RelationListId

	delete
		product.tRelationList
	where
		RelationListId = @RelationListId

	return 0
end







GO
