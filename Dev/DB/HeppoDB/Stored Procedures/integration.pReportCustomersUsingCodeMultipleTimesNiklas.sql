SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportCustomersUsingCodeMultipleTimesNiklas]
	@RepetedOrders	int,
	@LastxDays		int	

AS
begin
	set nocount on
		
	begin try
		
		select
			v.OrderId,
			v.Email,
			v.CreatedDate,
			c.CommonName
		from
			(select 
				o.Email
			from 
				[order].tOrder o				
				inner join Voucher.product.tVoucherLog v
					on v.OrderId = o.OrderId
				inner join Voucher.product.tVoucher vo
					on vo.VoucherId = v.VoucherId
				inner join Voucher.product.tVoucherInfo vi
					on vi.VoucherInfoId = vo.VoucherInfoId
			where
			o.OrderStatusId = 4 
			and (vi.VoucherInfoId = 312 or vi.VoucherInfoId = 313 or vi.VoucherInfoId = 315
										or vi.VoucherInfoId = 314)
			and o.CreatedDate > DATEADD(DAY, -@LastxDays, Convert(varchar, GETDATE(), 112))	
			group by 
				o.Email
			having
				count(o.Email) > @RepetedOrders
			) z
			
			inner join [order].tOrder v
				on v.Email = z.Email
			inner join core.tChannel c
				on c.ChannelId = v.ChannelId
		order by
			c.CommonName
					
			
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
