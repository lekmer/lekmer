SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemGetAllSecure]
	@ActionId		int
AS
BEGIN
	SELECT 
		A.ProductId,
		A.DiscountPrice
	FROM 
		lekmer.tProductDiscountActionItem A INNER JOIN 
		product.tProduct P ON A.ProductId = P.ProductId
	WHERE 
		A.ProductActionId = @ActionId AND 
		P.IsDeleted = 0
END
GO
