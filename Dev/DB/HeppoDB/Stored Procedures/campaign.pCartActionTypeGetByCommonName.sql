SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartActionTypeGetByCommonName]
	@CartActionTypeCommonName	varchar(50)
AS 
BEGIN 
	SELECT 
		*
	FROM 
		campaign.vCustomCartActionType
	WHERE 
		[CartActionType.CommonName] = @CartActionTypeCommonName
END

GO
