SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceActionExcludeProductGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		p.ProductId
	FROM 
		[addon].tCartItemPriceActionExcludeProduct A INNER JOIN 
		product.tProduct p ON A.ProductId = p.ProductId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
