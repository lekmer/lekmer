SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE 	[lekmer].[pVoucherConditionGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		lekmer.tVoucherCondition VC
		inner join campaign.vCustomCondition c on VC.ConditionId = c.[Condition.Id]
	WHERE
		[Condition.Id] = @Id
end
GO
