SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pConditionTypeGetById]
	@ConditionTypeId	int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomConditionType
	WHERE 
		[ConditionType.Id] = @ConditionTypeId
end

GO
