SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockProductImageListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM 
		product.tBlockProductImageList
	WHERE
		BlockId = @BlockId
END
GO
