SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_CreateImageReferences]
	@ProductXml		xml

	-- Returns.....: 0 on Valid, 1 on failure
AS
begin
--'<products>
--		<product id="103024" hyid="100124-0014">
--			<picture id="1" height="360" width="480" extension="jpg"/>
--			<picture id="2" height="360" width="480" extension="jpg"/>
--			<picture id="3" height="360" width="480" extension="jpg"/>
--			<picture id="4" height="360" width="480" extension="jpg"/>
--			<picture id="5" height="360" width="480" extension="jpg"/>
--			<picture id="6" height="360" width="480" extension="jpg"/>
--			<picture id="7" height="360" width="480" extension="jpg"/>
--			<picture id="8" height="360" width="480" extension="jpg"/>
--		</product>
--	</products>'
	set nocount on
	
	
	-- HÅRDKODAT, KOLLA ÖVER SEN
	declare @MEDIAFOLDERPARENTID int,
			@MEDIAFORMATID int
	
	set @MEDIAFOLDERPARENTID = 1000007-- 1000008
	set @MEDIAFORMATID = 2
	
	if object_id('tempdb..#tProductPicture') is not null
		drop table #tProductPicture
		
-- create table #UniqueErpId(BrandId nvarchar(200) COLLATE Finnish_Swedish_CI_AS not null, BrandTitle nvarchar(2500) COLLATE Finnish_Swedish_CI_AS not null);
	
	create table #tProductPicture
	(
		ProductId int not null,
		HYErpId nvarchar (50) COLLATE Finnish_Swedish_CI_AS null,
		PictureId int not null,
		PictureHeight int not null,
		PictureWidth int not null,
		Extension nvarchar (10) COLLATE Finnish_Swedish_CI_AS not null,
		MediaFolderId int null,
		MediaId int null,
		FolderTitle as (cast(ProductId - ProductId % 100 as nvarchar(150))) COLLATE Finnish_Swedish_CI_AS,
		constraint PK_#tProductPicture primary key(ProductId, PictureId)
	)
	
	begin try
		begin transaction
		-- Populate temp table
		insert #tProductPicture(ProductId, HYErpId, PictureId, PictureHeight, PictureWidth, Extension)		
		select 
		T.c.value('../@id[1]', 'int'),
		T.c.value('../@hyid[1]', 'nvarchar(50)'),
		T.c.value('@id[1]', 'int'),
		T.c.value('@height[1]', 'int'),
		T.c.value('@width[1]', 'int'),
		T.c.value('@extension[1]', 'nvarchar(10)')
	from @ProductXml.nodes('/products/product/picture') T(c)
		

		
		-- insert into tMediaFolder - fel, ska skapas fler underkategorier...
		insert media.tMediaFolder(MediaFolderParentId, Title)
		select distinct
			@MEDIAFOLDERPARENTID,
			p.FolderTitle
		from
			#tProductPicture p
		where not exists(select 1
						from media.tMediaFolder mf
						where mf.Title = p.FolderTitle
							and mf.MediaFolderParentId = @MEDIAFOLDERPARENTID)
		
		-- update temp table
		update p
			set MediaFolderId = mf.MediaFolderId
		from #tProductPicture p
			inner join media.tMediaFolder mf
				on mf.Title = p.FolderTitle
				and mf.MediaFolderParentId = @MEDIAFOLDERPARENTID
		
	
	
		-- INSERT INTO tMEDIA -fel lägger in även om det finns!	
		INSERT INTO media.tMedia(MediaFormatId, MediaFolderId, Title, [FileName])
		select
			(select MediaFormatId from media.tMediaFormat where Extension = t.Extension), --3
			t.MediaFolderId,
			isnull(b.Title, ' ') + '_' + pp.Title + '_' + cast(t.ProductId as varchar(10)),
			cast(t.ProductId as varchar(10)) + '_' + cast(t.PictureId as varchar)
		from
			#tProductPicture t
			inner join lekmer.tlekmerProduct l
				on t.ProductId = l.ProductId
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			inner join product.tproduct pp
				on pp.ProductId = t.ProductId
		where
			not exists (select 1 from
						media.tMedia 
						where [FileName] = cast(t.ProductId as varchar(10)) + '_' + cast(t.PictureId as varchar))
			
			
		--select * from media.tMedia
		
		update pp
		set MediaId = t.MediaId
		from #tProductPicture pp
			inner join (select MediaId,
							   [FileName],
							   row_number() over(partition by [FileName] order by MediaId) RowNo
						  from media.tMedia) t
					on t.[FileName] = cast(pp.ProductId as varchar(10)) + '_' + cast(pp.PictureId as varchar)
					and t.RowNo = 1
					
		
		--select * from #tProductPicture
		
		-- INSERT INTO tIMAGE
		INSERT INTO media.tImage(MediaId, Height, Width, AlternativeText)
		select
			pp.MediaId,
			pp.PictureHeight,
			pp.PictureWidth,
			cast(pp.ProductId as varchar(10)) + '_' + cast(pp.PictureId as varchar)
		from
			#tProductPicture pp
		where 
			not exists (select 1
						from media.tImage im
						inner join #tProductPicture ppp
							on im.MediaId = ppp.MediaId)
		
		
		/*
		select * 
		from 
			#tProductPicture pp
			inner join media.tImage i
				on i.MediaId = pp.MediaId
		*/
		
		
		-- INSERT INTO tPRODUCT
		UPDATE 
			k
		set
			k.MediaId = pp.MediaId
		from
				#tProductPicture pp	
				inner join product.tProduct k
					on k.ProductId = pp.ProductId
		where 
			pp.PictureId = 1

/*	
select p.MediaId 
from lekmer.tLekmerProduct l inner join product.tProduct p on l.ProductId = p.ProductId
where 
	l.HYErpId = '100124-0014'
	
*/
	
	-- INSERT INTO tPRODUCTIMAGE
	INSERT INTO product.tProductImage(ProductId, MediaId, ProductImageGroupId, Ordinal)
	select
			pp.ProductId,
			pp.MediaId,
			/*(select ProductImageGroupId 
					from product.tProductImageGroup 
					where pp.PictureId),*/
			(case when pp.PictureId = 1 then 1 else 4 end),
			pp.PictureId--1 -- ORDINAL (Sorting)
		from
			#tProductPicture pp
		where 
			not exists (select 1 
				from product.tProductImage i
				where i.ProductId = pp.ProductId
				AND i.MediaId = pp.MediaId)

	
	commit transaction
	--return @Return
	end try
	begin catch

		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [integation].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
					
		INSERT INTO [integration].[tImageImportLog](Data, [Message])
					values(ERROR_PROCEDURE(), ERROR_MESSAGE())
	end catch	
	
	-- SKICKA TILLBAK ETT RESULTSET MED MEDIA IDS
	-- kör en insert till en permanent tabell som alltid finns, rensa den innan SPn körs	
		
		select *  
		from product.tProductImage	i inner join #tProductPicture pp on i.MediaId = pp.MediaId	
end
GO
