SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionItemInsert]
	@ActionId			int,
	@ProductId			int,
	@DiscountPrice		decimal(16,2)
AS 
BEGIN 
	INSERT
		lekmer.tProductDiscountActionItem
	(
		ProductActionId,
		ProductId,
		DiscountPrice
	)
	VALUES
	(
		@ActionId,
		@ProductId,
		@DiscountPrice
	)
END 




GO
