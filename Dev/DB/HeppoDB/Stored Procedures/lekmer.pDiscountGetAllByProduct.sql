SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pDiscountGetAllByProduct]
	@ProductId	int
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[lekmer].[vProductPriceDiscount]
	WHERE
		[Discount.ProductId] = @ProductId
END
GO
