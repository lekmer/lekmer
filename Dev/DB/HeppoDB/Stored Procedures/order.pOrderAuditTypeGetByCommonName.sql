SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderAuditTypeGetByCommonName]
	@CommonName		varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrderAuditType]
	WHERE
		[OrderAuditType.CommonName] = @CommonName

END
GO
