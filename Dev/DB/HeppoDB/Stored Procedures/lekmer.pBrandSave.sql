SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pBrandSave]
	@BrandId INT,
	@Title	VARCHAR(500),
	@MediaId Int = null,
	@Description VARCHAR(MAX) = null,
	@ExternalUrl varchar(500) = null
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tBrand WHERE Title = @Title AND BrandId <> @BrandId)
		RETURN -1

	UPDATE
		[lekmer].[tBrand]
	SET
		[Title] = @Title,	
		[MediaId] = @MediaId,
		[Description] = @Description,
		[ExternalUrl] = @ExternalUrl 
	WHERE
		[BrandId] = @BrandId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tBrand]
		( 
			[Title],
			[MediaId],
			[Description],
			[ExternalUrl]
		)
		VALUES
		(
			@Title,
			@MediaId ,
			@Description,
			@ExternalUrl 
		)
		SET @BrandId = scope_identity()						
	END 
	RETURN @BrandId
END


GO
