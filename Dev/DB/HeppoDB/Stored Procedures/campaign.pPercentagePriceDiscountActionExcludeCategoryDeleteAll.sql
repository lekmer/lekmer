SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pPercentagePriceDiscountActionExcludeCategoryDeleteAll]
	@ProductActionId int
as
begin
	DELETE
		campaign.tPercentagePriceDiscountActionExcludeCategory
	WHERE
		ProductActionId = @ProductActionId
end



GO
