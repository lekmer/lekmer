SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-26
-- Description:	Gets all MediaFormat
-- =============================================
CREATE PROCEDURE [media].[pMediaFormatGetAll]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomMediaFormat]	
END

GO
