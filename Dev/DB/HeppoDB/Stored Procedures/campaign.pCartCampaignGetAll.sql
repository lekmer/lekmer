SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartCampaignGetAll]
	@ChannelId int
as
begin
	SELECT
		c.*
	FROM
		campaign.vCustomCartCampaign c
		inner join campaign.tCampaignModuleChannel m on m.CampaignRegistryId = c.[Campaign.CampaignRegistryId]
	ORDER BY
		c.[Campaign.Priority] ASC
end

GO
