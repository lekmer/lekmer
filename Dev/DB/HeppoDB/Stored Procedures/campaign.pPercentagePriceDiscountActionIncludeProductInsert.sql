SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionIncludeProductInsert]
	@ProductActionId int,
	@ProductId int
as
begin
	INSERT
		campaign.tPercentagePriceDiscountActionIncludeProduct
	(
		ProductActionId,
		ProductId
	)
	VALUES
	(
		@ProductActionId,
		@ProductId
	)
end



GO
