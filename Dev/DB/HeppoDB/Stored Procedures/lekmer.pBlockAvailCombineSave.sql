SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure  [lekmer].[pBlockAvailCombineSave]
	@BlockId int,
	@UseCartPredictions bit,
	@UseClickStreamPredictions bit,
	@UseLogPurchase bit,
	@UsePersonalPredictions bit,
	@UseProductSearchPredictions bit,
	@UseProductsPredictions bit,
	@UseProductsPredictionsFromClicksCategory bit,
	@UseProductsPredictionsFromClicksProduct bit,
	@UseLogClickedOn bit,
	@ColumnCount	int,
	@RowCount		int
as
begin
	update lekmer.tBlockAvailCombine
	set
		UseCartPredictions = @UseCartPredictions,
		UseClickStreamPredictions = @UseClickStreamPredictions,
		UseLogPurchase = @UseLogPurchase,
		UsePersonalPredictions = @UsePersonalPredictions,
		UseProductSearchPredictions = @UseProductSearchPredictions,
		UseProductsPredictions = @UseProductsPredictions,
		UseProductsPredictionsFromClicksCategory = @UseProductsPredictionsFromClicksCategory,
		UseProductsPredictionsFromClicksProduct = @UseProductsPredictionsFromClicksProduct,
		UseLogClickedOn=@UseLogClickedOn,
		[ColumnCount]	= @ColumnCount,
		[RowCount]		= @RowCount
	where
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert lekmer.tBlockAvailCombine
	(
		BlockId,
		UseCartPredictions,
		UseClickStreamPredictions,
		UseLogPurchase,
		UsePersonalPredictions,
		UseProductSearchPredictions,
		UseProductsPredictions,
		UseProductsPredictionsFromClicksCategory,
		UseProductsPredictionsFromClicksProduct,
		UseLogClickedOn,
				[ColumnCount],
				[RowCount]
	)
	values
	(
		@BlockId,
		@UseCartPredictions,
		@UseClickStreamPredictions,
		@UseLogPurchase,
		@UsePersonalPredictions,
		@UseProductSearchPredictions,
		@UseProductsPredictions,
		@UseProductsPredictionsFromClicksCategory,
		@UseProductsPredictionsFromClicksProduct,
		@UseLogClickedOn,
		1,
		1
	)
end

GO
