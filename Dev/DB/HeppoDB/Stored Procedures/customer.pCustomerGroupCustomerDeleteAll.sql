SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerGroupCustomerDeleteAll]
@CustomerGroupId	int
AS
BEGIN
	DELETE FROM 
		customer.tCustomerGroupCustomer
	WHERE 
		CustomerGroupId = @CustomerGroupId
END













GO
