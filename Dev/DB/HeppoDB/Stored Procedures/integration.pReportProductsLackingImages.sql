SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductsLackingImages]

AS
begin
	--set nocount on
					
			with UniqueErpId
				as
				(
					select
						l.HYErpId,
						s.Erpid,
						psp.Lagerplats,
						psp.Lagerplatstyp,
						psp.LagerplatstypBenamning,
						row_number() over(partition by HYErpId
										  order by HYErpId) RowNo,
						--s.NumberInStock,
						p.Title,
						l.BrandId
						--s.ProductId
						-- select *
					from 
						lekmer.tlekmerproduct l
						inner join product.tproduct p
							on p.productid = l.productid
						left join lekmer.tproductsize s
							on p.productid = s.productid
							--and s.numberinstock > 0
						left join integration.tProductStockPosition psp
							on isnull(s.ErpId, l.HYErpId + '-251') = psp.HYarticleId
					where 
						p.mediaid is null
						and (p.NumberInStock > 0 and s.NumberInStock is null
							or (s.NumberInStock > 0))
						and l.HYErpId not in
											('000001-0014',
											'000002-0005',
											'000004-0023')
				)
				
				
				select
					HYErpId,
					Lagerplats,
					Lagerplatstyp,
					LagerplatstypBenamning,
					u.Title as Title,
					b.Title as BrandTitle
				from 
					UniqueErpId u
					inner join lekmer.tBrand b
						on u.BrandId = b.BrandId
				where RowNo = 1 
				--and Lagerplats is not null

end
GO
