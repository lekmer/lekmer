SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderStatusGetAll]
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM
		[order].[vCustomOrderStatus]
END


GO
