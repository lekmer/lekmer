SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityFunctionGetTree]
@ModelFragmentId	int,
@SelectedId			int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	
	INSERT INTO @tTree
		SELECT
			v.[Entity.Id] * (-1),
			NULL,
			v.[Entity.Title]
		FROM template.vCustomEntity AS v INNER JOIN template.tModelFragmentEntity tfe
		ON v.[Entity.Id] = tfe.EntityId
		WHERE tfe.ModelFragmentId = @ModelFragmentId
		
	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[EntityFunction.Id],
					v.[EntityFunction.EntityId] * (-1),
					v.[EntityFunction.CommonName]
				FROM template.vCustomEntityFunction AS v
				WHERE v.[EntityFunction.EntityId] = @SelectedId
		END

		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN ParentId IS NOT NULL THEN 0 ELSE (CASE WHEN EXISTS (SELECT 1 FROM template.vCustomEntityFunction AS v
				WHERE v.[EntityFunction.EntityId] = -1 * Id) THEN 1 ELSE 0 END) END) AS bit) AS 'HasChildren'
		FROM @tTree
END

GO
