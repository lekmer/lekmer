SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionIncludeProductInsert]
	@ConditionId	int,
	@ProductId		int
AS 
BEGIN 
	INSERT
		[addon].tCartContainsConditionIncludeProduct
	(
		ConditionId,
		ProductId
	)
	VALUES
	(
		@ConditionId,
		@ProductId
	)
END 



GO
