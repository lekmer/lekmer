SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pIncludeFolderGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[IncludeFolder.Id],
			v.[IncludeFolder.ParentIncludeFolderId],
			v.[IncludeFolder.Title]
		FROM template.vCustomIncludeFolder AS v
		WHERE v.[IncludeFolder.ParentIncludeFolderId] IS NULL
	
	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[IncludeFolder.Id],
					v.[IncludeFolder.ParentIncludeFolderId],
					v.[IncludeFolder.Title]
				FROM template.vCustomIncludeFolder AS v
				WHERE v.[IncludeFolder.ParentIncludeFolderId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[IncludeFolder.ParentIncludeFolderId]
									 FROM template.vCustomIncludeFolder AS v
									 WHERE v.[IncludeFolder.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT
							v.[IncludeFolder.Id],
							v.[IncludeFolder.ParentIncludeFolderId],
							v.[IncludeFolder.Title]
						FROM template.vCustomIncludeFolder AS v
						WHERE v.[IncludeFolder.ParentIncludeFolderId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM template.vCustomIncludeFolder AS v
									WHERE v.[IncludeFolder.ParentIncludeFolderId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY ParentId ASC
END

GO
