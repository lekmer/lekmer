SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure lekmer.pProductDecreaseNumberInStock
	@ProductId int,
	@Quantity int
as
begin
	update
		product.tProduct
	set
		NumberInStock = (case when NumberInStock - @Quantity < 0 then 0 else NumberInStock - @Quantity end)
	where
		ProductId = @ProductId
end
GO
