SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 12:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentAreaGetById]
@ContentAreaId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentArea]
	where
		[ContentArea.Id] = @ContentAreaId
end

GO
