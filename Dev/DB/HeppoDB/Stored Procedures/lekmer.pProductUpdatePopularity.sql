SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUpdatePopularity]
	@ChannelId int,
	@DateTimeFrom datetime,
	@DateTimeTo datetime
as
begin
	declare @ProductIds table(ProductId int, Quantity int)
	
	insert
		@ProductIds
	(
		ProductId,
		Quantity
	)
	select 
		p.ProductId,
		sum(oi.Quantity)
	from
		product.tProduct p
		inner join [order].tOrderItemProduct oip WITH(NOLOCK) on p.ProductId = oip.ProductId
		inner join [order].tOrderItem oi WITH(NOLOCK) on oi.OrderItemId = oip.OrderItemId
		inner join [order].tOrder o WITH(NOLOCK) on oi.OrderId = o.OrderId
	where
		o.ChannelId = @ChannelId
		and o.CreatedDate between isnull(@DateTimeFrom, o.CreatedDate) and isnull(@DateTimeTo, o.CreatedDate)
	group by
		p.ProductId
	order by
		sum(oi.Quantity) desc
		
		
	Delete [lekmer].[tProductPopularity]
	where 	@ChannelId=	ChannelId

INSERT INTO [lekmer].[tProductPopularity](ChannelId,ProductId,Popularity)
	select
		@ChannelId,
		p.[Product.Id],		
		i.Quantity 
	from
		@ProductIds as i 
		inner join [product].[vCustomProduct] as p on p.[Product.Id] = i.ProductId
		inner join [product].[vCustomPriceListItem] as pli
			on pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[Product.ChannelId] = @ChannelId
	order by
		i.Quantity DESC
end
GO
