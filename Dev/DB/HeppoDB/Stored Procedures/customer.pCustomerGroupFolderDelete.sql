SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerGroupFolderDelete]
@CustomerGroupFolderId	int
AS
BEGIN
	DELETE FROM 
		customer.tCustomerGroupFolder
	WHERE 
		CustomerGroupFolderId = @CustomerGroupFolderId
END
GO
