SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductSortOrderGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[product].[vCustomProductSortOrder]
END

GO
