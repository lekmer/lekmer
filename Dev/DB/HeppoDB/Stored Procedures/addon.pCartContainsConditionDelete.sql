SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionDelete]
	@ConditionId int
as
begin
	delete addon.tCartContainsConditionIncludeProduct
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsConditionExcludeProduct
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsConditionIncludeCategory
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsConditionExcludeCategory
	where ConditionId = @ConditionId
	
	delete addon.tCartContainsCondition
	where ConditionId = @ConditionId
end
GO
