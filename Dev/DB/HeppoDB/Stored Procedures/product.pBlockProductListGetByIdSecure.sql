SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductListGetByIdSecure]
	@BlockId	int
as
begin
	select 
		ba.*,
		b.*
	from 
		[product].[vCustomBlockProductList] as ba
		inner join [sitestructure].[vCustomBlockSecure] as b on ba.[BlockProductList.BlockId] = b.[Block.BlockId]
	where
		ba.[BlockProductList.BlockId] = @BlockId
end

GO
