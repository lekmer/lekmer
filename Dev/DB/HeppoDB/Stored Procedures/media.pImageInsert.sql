SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-23
-- Description:	Saves an image
-- =============================================
create PROCEDURE [media].[pImageInsert]
	@MediaId INT,
	@Width INT,
	@Height INT,
	@AlternativeText NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON
	
	    INSERT [media].[tImage]
	      (
	      	[MediaId],
	        [Width],
	        [Height],
	        [AlternativeText]
	      )
	    VALUES
	      (
	      	@MediaId,
	        @Width,
	        @Height,
	        @AlternativeText
	      ) 
	RETURN @MediaId
END
GO
