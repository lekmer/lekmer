SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateAllSeoSettings]

AS
begin
	set nocount on
	begin try
		-- NOTE
		-- Använda like 'a%' för att inte lägga till a då det slutar på a
	
	
		-- SE Seo General
		exec [integration].[pGenerateSeoSettingsSE]
		
		-- SE Seo
		exec [integration].[pGenerateProductSeoShoesSE]
		exec [integration].[pGenerateProductSeoShoesAccessoriesSE]
		
		-- NO Seo
		exec [integration].[pGenerateProductSeoShoesNO]
		exec [integration].[pGenerateProductSeoShoesAccessoriesNO]
		exec [integration].[pGenerateBrandSeoNO]
		exec [integration].[pGenerateSitestructureSeoNO]
		
		-- DK Seo
		exec [integration].[pGenerateProductSeoShoesDK]
		exec [integration].[pGenerateProductSeoShoesAccessoriesDK]
		exec [integration].[pGenerateBrandSeoDK]
		exec [integration].[pGenerateSitestructureSeoDK]
		
		-- FI Seo
		exec [integration].[pGenerateProductSeoShoesFI]
		exec [integration].[pGenerateProductSeoShoesAccessoriesFI]
		exec [integration].[pGenerateBrandSeoFI]
		exec [integration].[pGenerateSitestructureSeoFI]
		
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
