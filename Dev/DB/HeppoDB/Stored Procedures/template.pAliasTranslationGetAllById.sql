SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pAliasTranslationGetAllById]
	@AliasId	int
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomAliasTranslation]
	where
		[Alias.Id] = @AliasId
end

GO
