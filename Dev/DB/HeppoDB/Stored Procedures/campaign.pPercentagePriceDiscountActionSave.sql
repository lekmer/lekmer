SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionSave]
	@ProductActionId int,
	@DiscountAmount decimal(16, 2),
	@IncludeAllProducts bit
as
begin
	UPDATE
		campaign.tPercentagePriceDiscountAction
	SET
		DiscountAmount = @DiscountAmount,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ProductActionId = @ProductActionId
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			campaign.tPercentagePriceDiscountAction
		(
			ProductActionId,
			DiscountAmount,
			IncludeAllProducts
		)
		VALUES
		(
			@ProductActionId,
			@DiscountAmount,
			@IncludeAllProducts
		)
	END
end



GO
