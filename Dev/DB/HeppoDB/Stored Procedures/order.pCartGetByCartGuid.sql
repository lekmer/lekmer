SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pCartGetByCartGuid] 
@CartGuid	uniqueidentifier
as
begin
	set nocount on
	select	
		*
	from
		[order].vCustomCart
	where
		[Cart.CartGuid] = @CartGuid
end

GO
