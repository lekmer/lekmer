SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 15:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentNodeShortcutLinkDelete]
@ContentNodeId	int
as
begin
	delete
		[sitestructure].[tContentNodeShortcutLink]
	where
		[ContentNodeId] = @ContentNodeId
end
GO
