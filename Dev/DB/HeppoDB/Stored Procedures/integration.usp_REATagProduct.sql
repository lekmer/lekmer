SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_REATagProduct]

AS
BEGIN	
	begin try
	begin transaction

		-- 1. untag all products as "rea (Sale)"
		-- 2. get all active campaigns 
		-- 3. get all actions related to those active campaigns
		-- 4. tag all products assosiated with these Actions/campaigns with Rea Tag
		declare @TagId int
		set @TagId = (select TagId from lekmer.tTag where Value = 'Sale')
		
		delete from lekmer.tProductTag
		where TagId = @TagId
			
		insert into lekmer.tProductTag(ProductId, TagId)
		select distinct
			z.ProductId,
			@TagId
		from								-- lekmer.tProductDiscountActionItem innehåller alla discout priser
			(select coalesce(d.ProductId , b.ProductId) ProductId --,c.ProductActionId
				from campaign.tProductAction c
					left join lekmer.tProductDiscountActionItem b
					on c.ProductActionId = b.ProductActionId
					left join campaign.tPercentagePriceDiscountActionIncludeProduct d -- only include product!
					on c.ProductActionId = d.ProductActionId 
				where CampaignId in (
									select CampaignId from campaign.tCampaign
									where EndDate > GETDATE()
									and CampaignStatusId = 0
									)
								) z
		where
			z.ProductId is not null
		
		--rollback
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
END

GO
