SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pProductSearchAdvanced]
	@ChannelId	INT,
	@SearchCriteriaInclusiveList XML,
	@SearchCriteriaExclusiveList XML,
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = 'ErpId',
	@SortDescending BIT = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @ParsedBrandId INT
	DECLARE @ParsedCategoryId INT
	DECLARE @ParsedTitle NVARCHAR(256)
	DECLARE @ParsedPriceFrom DECIMAL(16,2)
	DECLARE @ParsedPriceTo DECIMAL(16,2)
	DECLARE @ParsedErpId VARCHAR(max)
	DECLARE @ParsedStatusId INT
	DECLARE @ParsedEanCode VARCHAR(20)
	
	DECLARE CriteriaInclusiveCursor CURSOR
		FOR SELECT 
					T.a.value('(@brandId)', 'INT') AS 'brandId',
					T.a.value('(@categoryId)', 'INT') AS 'categoryId',
					T.a.value('(@title)', 'NVARCHAR(256)') AS 'title',
					T.a.value('(@priceFrom)', 'DECIMAL(16,2)') AS 'priceFrom',
					T.a.value('(@priceTo)', 'DECIMAL(16,2)') AS 'priceTo',
					T.a.value('(@erpId)', 'VARCHAR(max)') AS 'erpId',
					T.a.value('(@statusId)', 'INT') AS 'statusId',
					T.a.value('(@eanCode)', 'VARCHAR(20)') AS 'eanCode'
			FROM    @SearchCriteriaInclusiveList.nodes('criterias/criteria') AS T ( a )
	
	OPEN CriteriaInclusiveCursor
	FETCH NEXT FROM CriteriaInclusiveCursor INTO @ParsedBrandId, @ParsedCategoryId, @ParsedTitle, @ParsedPriceFrom, @ParsedPriceTo, @ParsedERPId, @ParsedStatusId, @ParsedEANCode
	CLOSE CriteriaInclusiveCursor
	DEALLOCATE CriteriaInclusiveCursor

	IF ( @@FETCH_STATUS = 0 )
	BEGIN
		exec [product].[pProductSearch]
			@BrandId = @ParsedBrandId,
			@CategoryId = @ParsedCategoryId,
			@Title = @ParsedTitle,
			@PriceFrom = @ParsedPriceFrom,
			@PriceTo = @ParsedPriceTo,
			@ErpId = @ParsedERPId,
			@StatusId  = @ParsedStatusId,
			@EanCode  = @ParsedEANCode,
			@ChannelId	 = @ChannelId,
			@Page = @Page,
			@PageSize  = @PageSize,
			@SortBy = @SortBy,
			@SortDescending = @SortDescending
	END
END
GO
