SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pUpdateProductNumberInStock]

AS
BEGIN
		BEGIN TRANSACTION
		BEGIN TRY
		
			-- Update numberInStock tProductSize
			UPDATE 
				ps
			SET
				ps.NumberInStock = m.NumberInStock
				-- select ps.numberinstock, m.numberinstock, ps.erpid, m.hyerpidsize
			FROM
				import.tProduct m
				INNER JOIN lekmer.tProductSize ps
					ON m.HYErpIdSize = ps.ErpId
			WHERE
				ps.NumberInStock <> m.NumberInStock
				and m.ChannelId = 1
			
		
		
			-- Update numberInStock tProduct
			UPDATE 
				p
			SET 
				p.NumberInStock = m.NumberInStock
				-- select p.numberinstock, m.numberinstock, l.hyerpid, m.channelid, m.HYErpIdSize
			FROM
				import.tProduct m
				INNER JOIN lekmer.tlekmerProduct l
					ON m.HYErpId = l.HYErpId
				INNER JOIN product.tProduct p
					ON p.ProductId = l.ProductId
				LEFT JOIN lekmer.tProductSize s
					ON p.ProductId = s.ProductId
			WHERE
				p.NumberInStock <> m.NumberInStock
				AND m.ChannelId = 1
				AND s.ProductId IS NULL

			COMMIT
		END TRY		
		BEGIN CATCH
			if @@TRANCOUNT > 0 rollback

				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				values('Update NumberInStock', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

		END CATCH
	
END
GO
