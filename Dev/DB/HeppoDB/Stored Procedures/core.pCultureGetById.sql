SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pCultureGetById]
@CultureId	int
as
begin
	set nocount on
	
	select
		*
	from
		[core].[vCustomCulture]
	WHERE [Culture.Id] = @CultureId
end

GO
