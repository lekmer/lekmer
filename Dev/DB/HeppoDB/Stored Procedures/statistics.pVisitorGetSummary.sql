SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [statistics].[pVisitorGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@Alternate		bit
AS 
BEGIN
	SELECT 
		isnull(sum([VisitCount]), 0)
	FROM
		[statistics].[tHourlyVisitor] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [Alternate] = @Alternate
		AND [statistics].[fnGetDateTimeKey]([Year], [Month], [Day], [Hour], 'Day')
			BETWEEN [statistics].[fnGetDateTimeKeyDirectly](@DateTimeFrom, 'Day') 
			AND @DateTimeTo
END
GO
