SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetAllByVariationGroup]
	@VariationGroupId	int,
	@ChannelId			int
as
begin
	SET NOCOUNT ON;
	
DECLARE @CurrencyId int,@ProductRegistryId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	SELECT
		P.*,
		pli.*
	FROM 
		[product].[vCustomProductSecure] AS P
		INNER JOIN [product].[tVariationGroupProduct] AS VGP on P.[Product.Id] = VGP.[ProductId] AND VGP.VariationGroupId = @VariationGroupId    
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = P.[Product.Id] AND PRP.ProductRegistryId = @ProductRegistryId
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, P.[Product.Id], PMC.PriceListRegistryId, NULL)
end


GO
