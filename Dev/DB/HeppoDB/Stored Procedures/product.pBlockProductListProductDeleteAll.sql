SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
*****************  Version 1  *****************
User: Yura P.	Date: 22.12.2008	Time: 11:00
Description:	Created
*/

CREATE procedure [product].[pBlockProductListProductDeleteAll]
@BlockId	int
as
begin
	delete
		[product].[tBlockProductListProduct]
	where
		[BlockId] = @BlockId
end

GO
