SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure test.pRelationListAddRandomProducts
	@RelationListId int,
	@ProductsAmount int
as
begin
	insert
		product.tRelationListProduct
	(
		RelationListId,
		ProductId
	)
	select top (@ProductsAmount)
		@RelationListId,
		ProductId
	from
		product.tProduct
	order by
		newid()
end
GO
