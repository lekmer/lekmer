SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Temp lösning
CREATE PROCEDURE [integration].[usp_BrandRestrictionsFok]
	
AS
begin
	set nocount on
	
	begin try
		begin transaction
		
		delete from product.tProductRegistryProduct
		where ProductId in (select ProductId 
								from lekmer.tLekmerProduct -- 20 rows (60)
								where BrandId = 1000139) --lloyd
		-- får endast säljas i sverige
		and ProductRegistryId != 1

		print 'Deleted ' + CONVERT(varchar, @@ROWCOUNT) + ' records from table product.tProductRegistryProduct - Brand Lloyd' 

		delete from product.tProductRegistryProduct
		where ProductId in (select ProductId 
								from lekmer.tLekmerProduct -- 20 rows (60)
								where BrandId = 1000218) --FitFlop
		-- får endast säljas i sverige
		and ProductRegistryId != 1
		
		
		----- 0-1 age intervall
		update lekmer.tLekmerProduct
		set AgeFromMonth = 0,
		AgeToMonth = 1
		where
			AgeToMonth = 0
		
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
