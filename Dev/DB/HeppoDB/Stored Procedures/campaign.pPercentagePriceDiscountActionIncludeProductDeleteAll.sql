SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionIncludeProductDeleteAll]
	@ProductActionId int
as
begin
	DELETE
		campaign.tPercentagePriceDiscountActionIncludeProduct
	WHERE
		ProductActionId = @ProductActionId
end



GO
