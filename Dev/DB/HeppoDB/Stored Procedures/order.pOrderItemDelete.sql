SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].pOrderItemDelete
	@OrderItemId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	delete
		[order].tOrderProductCampaign
	WHERE
		OrderItemId = @OrderItemId
	
	delete
		[order].[tOrderItemProduct]
	WHERE
		OrderItemId = @OrderItemId
	
	delete
		[order].[tOrderItem]
	WHERE
		OrderItemId = @OrderItemId
END


GO
