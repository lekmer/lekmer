SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pMonitorProductGetNextPortion]
@PreviousMaxId	int,
@Quantity		int
AS 
BEGIN 
	SELECT TOP (@Quantity)
		*
	FROM 
		[lekmer].[tMonitorProduct]
	WHERE
		MonitorProductId > @PreviousMaxId
	ORDER BY 
		MonitorProductId ASC
END
GO
