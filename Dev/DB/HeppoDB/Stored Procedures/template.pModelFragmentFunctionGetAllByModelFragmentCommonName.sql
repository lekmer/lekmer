SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionGetAllByModelFragmentCommonName]
	@ModelFragmentCommonName	varchar(50)
AS
begin
	set nocount on
	select
		F.*
	from
		[template].[vCustomModelFragmentFunction] F
		INNER JOIN [template].tModelFragment M ON M.ModelFragmentId = F.[ModelFragmentFunction.ModelFragmentId]
	where
		M.CommonName = @ModelFragmentCommonName
	ORDER BY F.[ModelFragmentFunction.CommonName]
end

GO
