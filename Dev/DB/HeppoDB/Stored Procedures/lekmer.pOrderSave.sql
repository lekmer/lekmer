SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pOrderSave]
	@OrderId int,
	@PaymentCost decimal(16,2),
	@VoucherDiscount decimal(16,2),
	@CustomerIdentificationKey nchar(50)
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		PaymentCost = @PaymentCost,
		VoucherDiscount = @VoucherDiscount,
		CustomerIdentificationKey= @CustomerIdentificationKey
	WHERE 
		OrderId = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			OrderId,
			PaymentCost,
			VoucherDiscount,
			CustomerIdentificationKey
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@VoucherDiscount,
			@CustomerIdentificationKey
		)
	END
END 
GO
