SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeUrlLinkGetAll]
@ChannelId INT
AS
BEGIN
	SELECT 		
		cnu.*
	FROM 
		[sitestructure].[vCustomContentNodeUrlLink] AS cnu
	WHERE 
		cnu.[ContentNode.ChannelId] = @ChannelId
END

GO
