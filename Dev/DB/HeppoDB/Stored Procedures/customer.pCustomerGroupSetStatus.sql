SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [customer].[pCustomerGroupSetStatus]
@CustomerGroupId	int,
@StatusId			int
AS
BEGIN
	UPDATE 
		customer.tCustomerGroup
	SET
		StatusId = @StatusId
	WHERE 
		CustomerGroupId = @CustomerGroupId
END
GO
