SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerProductIconSave]
	@ProductId	int,
	@IconId		int
AS 
BEGIN 
	INSERT [lekmer].[tLekmerProductIcon]
	VALUES(@ProductId, @IconId)
END
GO
