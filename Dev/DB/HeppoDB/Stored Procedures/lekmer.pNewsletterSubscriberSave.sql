SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberSave]
	@ChannelId		INT,
	@Email			VARCHAR(320),
	@CreatedDate	DATETIME
AS
BEGIN
	DECLARE @Id INT
	UPDATE
		[lekmer].[tNewsletterSubscriber]
	SET
		[CreatedDate] = @CreatedDate,
		@Id = [SubscriberId]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tNewsletterSubscriber]
		( 
			[Email], 
			[CreatedDate],
			[ChannelId] 
		)
		VALUES
		(
			@Email, 
			@CreatedDate,
			@ChannelId 
		)
		SET @Id = scope_identity()
	END
	RETURN @Id
END
GO
