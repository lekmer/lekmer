SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductRelationListGetByIdSecure]
 @BlockId		int
as
begin
	select 
		bprl.*,
		b.*
	from 
		[product].[vCustomBlockProductRelationList] as bprl
		inner join [sitestructure].[vCustomBlockSecure] as b on bprl.[BlockProductRelationList.BlockId] = b.[Block.BlockId]
	where
		bprl.[BlockProductRelationList.BlockId] = @BlockId
end

GO
