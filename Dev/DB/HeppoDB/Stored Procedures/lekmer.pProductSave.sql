SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pProductSave]
	@ProductId				int,
	@ItemsInPackage			int,
	@ErpId					varchar(50),
	@EANCode				varchar(20),
	@Title					nvarchar(256),
	@WebShopTitle			nvarchar(256),
	@ProductStatusId		int,
	@Description			nvarchar(max),
	@ShortDescription		nvarchar(max),
	@CategoryId				int,
	@MediaId				int,
	@BrandId				int,
	@AgeFromMonth			int,
	@AgeToMonth				int,
	@IsBookable				bit,
	@IsNewFrom				datetime,
	@IsNewTo				datetime,
	@Measurement			nvarchar(max),
	@BatteryTypeId			int,
	@NumberOfBatteries		int,
	@IsBatteryIncluded		bit,
	@ExpectedBackInStock	datetime,
	@CreatedDate			datetime,
	@SizeDeviationId		int
as
begin
	set nocount on

	update
		[product].[tProduct]
	set
		[ItemsInPackage]		= @ItemsInPackage,
		[ErpId]					= @ERPId,
		[EANCode]				= @EANCode,
		[Title]					= @Title,
		[WebShopTitle]			= @WebShopTitle,
		[Description]			= @Description,
		[ShortDescription]		= @ShortDescription,
		[CategoryId]			= @CategoryId,
		[MediaId]				= @MediaId,
		[ProductStatusId]		= @ProductStatusId
	where
		[ProductId] = @ProductId
		
	update
		[lekmer].[tLekmerProduct]
	set
		[BrandId]				= @BrandId,
		[AgeFromMonth]			= @AgeFromMonth,
		[AgeToMonth]			= @AgeToMonth,
		[IsBookable]			= @IsBookable,
		[IsNewFrom]				= @IsNewFrom,
		[IsNewTo]				= @IsNewTo,
		[Measurement]			= @Measurement,
		[BatteryTypeId]			= @BatteryTypeId,
		[NumberOfBatteries]		= @NumberOfBatteries,
		[IsBatteryIncluded]		= @IsBatteryIncluded,
		[ExpectedBackInStock]	= @ExpectedBackInStock,
		[CreatedDate]			= @CreatedDate,
		[SizeDeviationId]		= @SizeDeviationId
	where
		[ProductId] = @ProductId
end









GO
