SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [product].[pProductSearch]
	@BrandId int,
	@CategoryId int,
	@Title nvarchar(256),
	@PriceFrom DECIMAL(16,2),
	@PriceTo DECIMAL(16,2),
	@ErpId varchar(max),
	@StatusId int,
	@EanCode varchar(20),
	@ChannelId	int,
	@Page int = NULL,
	@PageSize int,
	@SortBy varchar(20) = NULL,
	@SortDescending bit = NULL
AS
BEGIN
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sqlGeneral nvarchar(max)
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)
	DECLARE @CurrencyId int,@ProductRegistryId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId

	SET @sqlFragment = '	
				SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					p.ProductId,
					pli.*
				FROM 
					product.tProduct AS p
					-- get price for current channel
					LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[ProductId] AND PRP.ProductRegistryId = ' + CAST(@ProductRegistryId AS varchar(10)) + '
					LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
					LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[ProductId]
						AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', p.[ProductId], PMC.PriceListRegistryId, NULL)
						
					inner join lekmer.tLekmerProduct as LP on p.ProductId= LP.ProductId
				WHERE
					p.IsDeleted = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND p.[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS(p.*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND pli.[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND p.[ErpId] IN (SELECT * FROM generic.fnConvertIDListToTableString(@ErpId,'',''))' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND p.[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@EanCode IS NOT NULL) THEN + '
					AND p.[EanCode] = @EanCode' ELSE '' END
		+ CASE WHEN (@BrandId IS NOT NULL) THEN + '
					AND LP.[BrandId] = @BrandId' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN product.vCustomProductSecure vp
				on p.ProductId = vp.[Product.Id]'

print @sql


	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	
			@BrandId int,
			@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@StatusId int,
			@EanCode varchar(20)',
			@BrandId,
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode

	EXEC sp_executesql @sql, 
		N'	
			@BrandId int,
			@ChannelId	int,
			@CategoryId int,
			@Title nvarchar(256),
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@StatusId int,
			@EanCode varchar(20)',
			@BrandId,
			@ChannelId,
			@CategoryId,
			@Title,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@StatusId,
			@EanCode
END



GO
