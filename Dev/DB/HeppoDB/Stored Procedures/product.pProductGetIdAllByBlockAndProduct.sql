SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductGetIdAllByBlockAndProduct]
		@ChannelId int,
		@CustomerId int,
		@BlockId int,
		@ProductId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(50) = NULL,
		@SortDescending bit = NULL
AS
BEGIN
	DECLARE @maxCount char(4)
	SET @maxCount = '2000'

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	DECLARE @CustomerIdString VARCHAR(10)
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))
		
	DECLARE @sql nvarchar(max)
	
	SET @sql = '
	declare @tResult table (ProductId int, SortBy nvarchar(max))	
	insert @tResult	(ProductId, SortBy)
	select distinct TOP ' + @maxCount + ' p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	from
		[product].[tProductRelationList] prl
		INNER JOIN [product].[tRelationList] rl ON prl.RelationListId = rl.RelationListId
		INNER JOIN [product].[tRelationListProduct] rlp ON rl.RelationListId = rlp.RelationListId
		INNER JOIN [product].[vCustomProduct] p ON rlp.ProductId = [p].[Product.Id]
		INNER JOIN [product].[tBlockProductRelationListItem] bprli ON rl.RelationListTypeId = bprli.RelationListTypeId
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				' + @CustomerIdString + '
			)
	WHERE
		prl.ProductId = ' + cast(@ProductId as varchar) + '
		AND p.[Product.ChannelId] = ' + cast(@ChannelId as varchar) + '
		AND bprli.BlockId = ' + cast(@BlockId as varchar) + '
		
	
	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + CAST(@Page * @PageSize AS varchar(10)) + ' *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
		AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + @maxCount + ' ProductId FROM @tResult AS SearchResult'
	END
			
	EXEC (@sql)
END

GO
