SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [sitestructure].[pContentNodeDelete]
	@ContentNodeId	int
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM sitestructure.tContentNodeTranslation
	WHERE ContentNodeId = @ContentNodeId

	DELETE FROM sitestructure.tContentNode
	WHERE ContentNodeId = @ContentNodeId
END

GO
