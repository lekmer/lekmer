SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeSave]
@ProductId				int,
@SizeId					int,
@MillimeterDeviation	int,
@OverrideEU				decimal(3, 1)
AS 
BEGIN 
	UPDATE 
		[lekmer].[tProductSize]
	SET
		MillimeterDeviation = @MillimeterDeviation,
		OverrideEU			= @OverrideEU
	WHERE 
		ProductId = @ProductId AND
		SizeId = @SizeId
END
GO
