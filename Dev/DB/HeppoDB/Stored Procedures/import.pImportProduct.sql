SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pImportProduct]

AS
BEGIN

		exec [import].[pImportCategory]
		exec [import].[pImportBrand]
		exec [integration].[usp_UpdateBrandHeppoCreationDate] 
		exec [import].[pUpdateProduct]
		exec [import].[pUpdateProductNumberInStock]
		
	
		declare @HYErpId nvarchar(50),
				@HYErpIdSize nvarchar(50),
				@Channel int,								
				@NumberInStock int,
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				@SizeId nvarchar(25), -- for taging socks
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				@VarugruppTitel nvarchar(50),
				@VarukodTitel nvarchar(50),
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25.0					
		
		declare cur_product cursor fast_forward for
			select
				tp.HYErpId,
				tp.HYErpIdSize,
				tp.ChannelId,				
				tp.NumberInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId,
				tp.SizeId,
				tp.ArticleGroupTitle,
				tp.ArticleCodeTitle
			FROM 
				[import].tProduct tp
			WHERE NOT EXISTS(SELECT 1
								FROM 
									lekmer.tLekmerProduct lp
									INNER JOIN product.tProductRegistryProduct prp
										on lp.ProductId = prp.ProductId
								where 
									lp.HYErpId = tp.HYErpId AND
									prp.ProductRegistryId = tp.ChannelId)												
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYErpId,
				 @HYErpIdSize,
				 @Channel,			 
				 @NumberInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId,
				 @SizeId,
				 @VarugruppTitel,
				 @VarukodTitel
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
				
				IF NOT EXISTS (SELECT 1 
								FROM lekmer.tLekmerProduct lp
								WHERE lp.HYErpId = @HYErpId)
				BEGIN 
				
				-- Data that was sent into insert
				SET @Data = 'NEW: HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel as varchar(10)) + 'HY-Cat ' + 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId 
				
					INSERT INTO [product].tProduct
						(
							ErpId,  
							IsDeleted, 
							NumberInStock, 
							CategoryId, 
							Title, 
							[Description], 
							ProductStatusId
						)
					SELECT 
						@HYErpId,
						0,
						@NumberInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), 
						@ArticleTitle, 
						'',
						6 --case when @NoInStock > 0 then 0 else 1 end (Ändrat alla produkter sätts som offline när de sätts in)
												
					SET @ProductId = SCOPE_IDENTITY()
					
					
		
												
					INSERT INTO lekmer.tLekmerProduct
						(
							ProductId, 
							HYErpId, 
							BrandId, 
							IsBookable, 
							AgeFromMonth, 
							AgeToMonth, 
							IsNewFrom, 
							IsNewTo, 
							IsBatteryIncluded, 
							ExpectedBackInStock
						)
					VALUES
						(
							@ProductId, 
							@HYErpId,
							(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
							0, 
							0, 
							0, 
							Convert(varchar, GETDATE(), 112),
							DATEADD(WK, 1, Convert(varchar, GETDATE(), 112)), 
							0, 
							Null
						)
					
					---- 
					-- TradeDoublerMappning på produktnivår
					
					DECLARE @TradeDoublerProductGroup nvarchar (50) 
					SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId 
														FROM integration.tTradeDoublerProductGroupMapping
															WHERE HYArticleClassId = 'C_'+@VaruklassId+'-'+@VarugruppId
															AND ChannelId = @Channel)
					
					DECLARE @tFok INT
					SET @tFok = (SELECT CASE WHEN @Channel = 1 THEN 1
								WHEN @Channel = 2 THEN 4
								WHEN @Channel = 3 THEN 657
								WHEN @Channel = 4 THEN 1000003
								end)
					
					IF @TradeDoublerProductGroup IS NOT NULL AND @tFok > 0
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping 
							(
								ProductId, 
								ProductGroupId, 
								ChannelId
							)
						SELECT
							@ProductId,
							@TradeDoublerProductGroup,
							@tFok
						WHERE
							NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping m
										WHERE m.ProductId = @ProductId AND m.ChannelId = @tFok)													
					END 

					-- Flagga så att produkten är och har varit nyhet
					insert into integration.[tProductNews](ProductId, ErpId)
					select
						@ProductId,
						@HYErpId
					where
						@ProductId not in (select productId from integration.[tProductNews])
					
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem 
						(
							PriceListId, 
							ProductId, 
							PriceIncludingVat, 
							PriceExcludingVat, 
							VatPercentage
						)
					select
							@Channel, 
							@ProductId, 
							@Price, 
							((@Price) / (1.0+(@Vat/100.0))), -- KOLLA OM DETTA STÄMMER
							@Vat
					where
						not exists (select 1 from product.tPriceListItem
										where ProductId = @ProductId
										and PriceListId = @Channel)
					--values(@Fok, @ProductId, @Price, ((@Price) / (1.0+(@Vat/100.0))), @Vat)
							
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct 
						(
							ProductId, 
							ProductRegistryId
						)
					select
						@ProductId,
						@Channel
					where
						not exists (select 1 from product.tProductRegistryProduct
										where ProductId = @ProductId
										and ProductRegistryId = @Channel)					
					
					-- tagga med categoryLevel3 (sneaker känga osv)
					if @VarukodTitel in 
					(select Value from lekmer.tTag where TagGroupId = 1000008) -- CategoryLevel3
						begin
							-- add tags
							insert into lekmer.tProductTag(ProductId,TagId)
							select
								@ProductId,
								(select TagId from lekmer.tTag where Value = @VarukodTitel
								and TagGroupId = 1000008)
							where
								not exists (select 1
											from lekmer.tProductTag
											where productId = @ProductId
											and Tagid = (select TagId from lekmer.tTag where Value = @VarukodTitel
															and TagGroupId = 1000008))
						end
					
					-- Tag man/kvinna/pojke/flocka/unisex
					if @VarugruppTitel in 
					('Kvinna', 'Man', 'Flickskor', 'Pojkskor')
						begin
							declare @TagId int
							set @TagId = (select TagId from lekmer.tTag where Value = @VarugruppTitel
							and TagGroupId = 1000002) -- taggroup kön
							-- add tags
							insert into lekmer.tProductTag(ProductId,TagId)
							select
								@ProductId,
								@TagId
							where
								not exists (select 1
													from lekmer.tProductTag
													where productId = @ProductId
													and Tagid = @TagId)
							-- Just in case
							set @TagId = 0
						end
					else if @VarugruppTitel in ('Unisex barn')
						begin
							declare @TagIdUnisexBoy int,
									@TagIdUnisexGirl int
								set @TagIdUnisexBoy = 1000013 -- pojkskor
								set @TagIdUnisexGirl = 1000012 -- flickskor
								-- add tags
								insert into lekmer.tProductTag(ProductId,TagId)
								select
									@ProductId,
									@TagIdUnisexBoy
								where
									not exists (select 1
														from lekmer.tProductTag
														where productId = @ProductId
														and Tagid = @TagIdUnisexBoy)
														
								insert into lekmer.tProductTag(ProductId,TagId)
								select
									@ProductId,
									@TagIdUnisexGirl
								where
									not exists (select 1
														from lekmer.tProductTag
														where productId = @ProductId
														and Tagid = @TagIdUnisexGirl)
														
								-- Just in case
								set @TagIdUnisexBoy = 0
								set @TagIdUnisexGirl = 0
						end
					else if @VarugruppTitel = ('Unisex kvinna/man')
						begin
							declare @TagIdUnisexMan int,
									@TagIdUnisexWomen int
								set @TagIdUnisexMan = 1000009 -- Man
								set @TagIdUnisexWomen = 1000010 -- Kvinna
								-- add tags
								insert into lekmer.tProductTag(ProductId,TagId)
								select
									@ProductId,
									@TagIdUnisexMan
								where
									not exists (select 1
														from lekmer.tProductTag
														where productId = @ProductId
														and Tagid = @TagIdUnisexMan)
								
								insert into lekmer.tProductTag(ProductId,TagId)
								select
									@ProductId,
									@TagIdUnisexWomen
								where
									not exists (select 1
														from lekmer.tProductTag
														where productId = @ProductId
														and Tagid = @TagIdUnisexWomen)
								-- Just in case
								set @TagIdUnisexMan = 0
								set @TagIdUnisexWomen = 0
						end					
				end	 
				ELSE IF not exists (select 1
										from lekmer.tLekmerProduct lp
										inner join product.tPriceListItem prp
											on lp.ProductId = prp.ProductId
										where 
											lp.HYErpId = @HYErpId
											and prp.PriceListId = @Channel)
				begin
					set @ProductId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId)
					set @Data = 'EXISTING: @HYErpId ' + @HYErpId + ' @Fok ' + CAST(@Channel as varchar(10)) + ' ProductId' + CAST(@ProductId as varchar(10))
						
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct 
						(
							ProductId, 
							ProductRegistryId
						)
					select
						@ProductId,
						@Channel
					where
						not exists (select 1 from product.tProductRegistryProduct
										where ProductId = @ProductId
										and ProductRegistryId = @Channel)

						
					-- tPriceListItem
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					select
						@Channel,
						@ProductId,
						@Price,
						(@Price / (1.0+(@Vat/100.0))), 
						@Vat
					where
						not exists (select 1 from product.tPriceListItem
										where ProductId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId)
										and PriceListId = @Channel)
					
					
					-- TradeDoublerMappning på produktnivår
					
					DECLARE @TradeDoublerProductGroupp nvarchar (50) 
					SET @TradeDoublerProductGroupp = (SELECT TOP 1 ProductGroupId 
														FROM integration.tTradeDoublerProductGroupMapping
															WHERE HYArticleClassId = 'C_'+@VaruklassId+'-'+@VarugruppId
															AND ChannelId = CAST(@Channel AS INT))
					
					DECLARE @tFokk INT
					SET @tFokk = (SELECT CASE WHEN @Channel = 1 THEN 1
								WHEN @Channel = 2 THEN 4
								WHEN @Channel = 3 THEN 657
								WHEN @Channel = 4 THEN 1000003
								end)
					
					IF @TradeDoublerProductGroupp IS NOT NULL AND @tFokk > 0
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping 
							(
								ProductId, 
								ProductGroupId, 
								ChannelId
							)
						SELECT
							@ProductId,
							@TradeDoublerProductGroupp,
							@tFokk
						WHERE
							NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping m
										WHERE m.ProductId = @ProductId AND m.ChannelId = @tFokk)													
					END 
					
				end

				commit	
			end try			
			begin catch
				if @@TRANCOUNT > 0 rollback

					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into 
				 @HYErpId,
				 @HYErpIdSize,
				 @Channel,			 
				 @NumberInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId,
				 @SizeId,
				 @VarugruppTitel,
				 @VarukodTitel
		end
		
		close cur_product
		deallocate cur_product
		

		exec [import].[pImportProductSize]
		exec [import].[pUpdateProductBrand]
		
		exec [import].[pAddProductColorTags]
		-- temp lösning
		exec [integration].[usp_BrandRestrictionsFok]	
		exec [integration].[usp_REATagProduct]	
END
GO
