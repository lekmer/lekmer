SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGetProductInformationToCreateProductUrls]
	@Channel 	int
AS
BEGIN

		begin
			begin try
				
				
				declare @LanguageId int
				set @LanguageId = (select LanguageId from core.tChannel where ChannelId = @Channel) --
				
				select 
					t.ProductId, 
					t.Title, 
					t.WebShopTitle, 
					t.LanguageId, 
					l.HYErpId, 
					pu.UrlTitle,
					coalesce(b.Title, '') as BrandTitle
				from
					(select
						p.ProductId,
						coalesce(pt.Title, p.Title) as Title,
						coalesce(pt.WebShopTitle, p.WebShopTitle) as WebShopTitle,
						@LanguageId as LanguageId
					from
						product.tProduct p
						left outer join product.tProductTranslation pt
							on p.ProductId = pt.ProductId
							and pt.LanguageId = @LanguageId) t
							
				inner join lekmer.tLekmerProduct l
					on t.ProductId = l.ProductId
			
				left outer join lekmer.tProductUrl pu
					on t.ProductId = pu.ProductId
					and t.LanguageId = pu.LanguageId
				left join lekmer.tBrand b
					on l.BrandId = b.BrandId
					
				
			end try	
			begin catch
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
						values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch			
		end		
END
GO
