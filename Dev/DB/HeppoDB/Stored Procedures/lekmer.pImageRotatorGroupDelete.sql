SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pImageRotatorGroupDelete]
	@BlockId	int
as
begin

	delete
		[lekmer].[tImageRotatorGroup]
	where
		[BlockId] = @BlockId
		
end
GO
