SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionIncludeProductGetIdAllSecure]
	@ActionId		int
AS
BEGIN
	SELECT 
		P.ProductId
	FROM 
		campaign.tPercentagePriceDiscountActionIncludeProduct A INNER JOIN 
		product.tProduct P ON A.ProductId = P.ProductId
	WHERE 
		A.ProductActionId = @ActionId AND 
		P.IsDeleted = 0
END
GO
