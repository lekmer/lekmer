SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByTagGroupAndProduct]
	@LanguageId int,
	@TagGroupId int,
	@ProductId	int
AS
BEGIN 
	SET nocount ON 

	SELECT 
		t.*
	FROM 
		lekmer.[vTag] AS t
		INNER JOIN lekmer.tProductTag AS pt ON t.[Tag.TagId] = pt.TagId
	WHERE 
		t.[Tag.TagGroupId] = @TagGroupId
		AND pt.ProductID = @ProductId
		AND t.[Tag.LanguageId] = @LanguageId
END 

GO
