SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [media].[pMediaItemUpdate]
	@MediaId INT,
	@MediaFolderId INT,
	@Title NVARCHAR(150)
AS
BEGIN
	SET NOCOUNT ON	
	
	    UPDATE [media].[tMedia]
	     SET   [MediaFolderId]	= @MediaFolderId,
	           [Title]			= @Title
	    WHERE  [MediaId]= @MediaId
	
	RETURN @MediaId
END
GO
