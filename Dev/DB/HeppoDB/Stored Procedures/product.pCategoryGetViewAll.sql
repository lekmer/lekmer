SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategoryGetViewAll]
	@ChannelId	int,
	@LanguageId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Channel.Id] = @ChannelId
		and c.[Category.LanguageId] = @LanguageId
end

GO
