SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 24.01.2009
Description: Created
*/

CREATE procedure [template].[pAliasFolderDelete]
	@AliasFolderId	int
AS
begin
	set nocount on

	DELETE FROM
		template.tAliasFolder
	WHERE
		AliasFolderId = @AliasFolderId

END





GO
