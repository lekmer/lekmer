SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockCategoryProductListGetByIdSecure]
	@BlockId	int
as
begin
	select 
		ba.*,
		b.*,
		ps.*
	from 
		[product].[vCustomBlockCategoryProductList] as ba
		inner join [sitestructure].[vCustomBlockSecure] as b on ba.[BlockCategoryProductList.BlockId] = b.[Block.BlockId]
		inner join product.vCustomProductSortOrder as ps on ps.[ProductSortOrder.Id] = ba.[BlockCategoryProductList.ProductSortOrderId]
	where
		ba.[BlockCategoryProductList.BlockId] = @BlockId
end

GO
