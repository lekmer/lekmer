SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [sitestructure].[pSiteStructureRegistrySave]
	@SiteStructureRegistryId	int,
	@StartContentNodeId			int
as
begin
	UPDATE 
		sitestructure.tSiteStructureRegistry
	SET 
		StartContentNodeId = @StartContentNodeId
	WHERE 
		SiteStructureRegistryId = @SiteStructureRegistryId
end
GO
