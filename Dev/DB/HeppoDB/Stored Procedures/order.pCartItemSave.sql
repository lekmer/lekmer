SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pCartItemSave]
	@CartItemId int,
    @CartId INT,
    @ProductId INT,
    @Quantity INT,
    @CreatedDate datetime
AS 
BEGIN
      SET NOCOUNT ON

      UPDATE
            [order].tCartItem
      SET
            Quantity = @Quantity
      WHERE
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO [order].tCartItem
            (
                  CartId,
                  ProductId,
                  Quantity,
                  CreatedDate            
            )
            VALUES
            (
                  @CartId,
                  @ProductId,
                  @Quantity,
                  @CreatedDate
            )
      set @CartItemId = scope_identity()
	end
	return @CartItemId
END
GO
