SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetAllMaster]
@SiteStructureRegistryId int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageSecure]
	WHERE 
	[ContentPage.IsMaster] = 1
	AND [ContentNode.SiteStructureRegistryId] = @SiteStructureRegistryId
end

GO
