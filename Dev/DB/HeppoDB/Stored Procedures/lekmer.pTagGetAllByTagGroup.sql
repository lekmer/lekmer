SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByTagGroup]
	@LanguageId int,
	@TagGroupId int
AS
begin
	set nocount on

	select
		*
	from
		lekmer.[vTag]
	where
		[Tag.TagGroupId] = @TagGroupId
		and [Tag.LanguageId] = @LanguageId
end

GO
