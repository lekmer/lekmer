SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockBrandProductListGetByIdSecure]
	@BlockId	int
as
begin
	select 
		ba.*,
		b.*,
		ps.*
	from 
		[lekmer].[tBlockbrandProductList] as ba
		inner join [sitestructure].[vCustomBlockSecure] as b on ba.[BlockId] = b.[Block.BlockId]
		inner join product.vCustomProductSortOrder as ps on ps.[ProductSortOrder.Id] = ba.[ProductSortOrderId]
	where
		ba.[BlockId] = @BlockId
end


GO
