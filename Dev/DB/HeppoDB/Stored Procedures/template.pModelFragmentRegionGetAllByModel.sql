SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentRegionGetAllByModel]
	@ModelId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT *
	FROM   [template].[vCustomModelFragmentRegion]
	WHERE  [ModelFragmentRegion.ModelId] = @ModelId
	ORDER BY
	       [ModelFragmentRegion.Ordinal]
END

GO
