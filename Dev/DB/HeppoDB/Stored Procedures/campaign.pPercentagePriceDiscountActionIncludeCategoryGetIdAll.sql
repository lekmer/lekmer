SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionIncludeCategoryGetIdAll]
	@LanguageId		int,
	@ActionId		int
AS
BEGIN
	SELECT
		c.[Category.Id]
	FROM
		campaign.tPercentagePriceDiscountActionIncludeCategory A
		INNER JOIN product.vCustomCategory c ON A.CategoryId = c.[Category.Id]
	WHERE
		A.ProductActionId = @ActionId
		and c.[Category.LanguageId] = @LanguageId
END

GO
