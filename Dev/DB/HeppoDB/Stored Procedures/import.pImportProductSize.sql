SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pImportProductSize]

AS
BEGIN

		declare @HYErpId nvarchar(50),
				@HYErpIdSize nvarchar(50),
				@Channel int,
				@NumberInStock int,
				@HYSizeId nvarchar (50),	
				@Data nvarchar(4000),
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10)

		
				----Variabler för FoK (Split)-------
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				DECLARE @SizeChar NVARCHAR (20)
				DECLARE @SizeNo NVARCHAR (20)
				------------------------------------
				DECLARE @tmpProductId int
		
		declare cur_productHeppo cursor fast_forward for
			select
				tp.HYErpId,
				tp.HYErpIdSize,
				tp.ChannelId,				
				tp.NumberInStock,				
				tp.SizeId,
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId
			from
				[import].tProduct tp
			where not exists(select 1
						from lekmer.tProductSize ps
						where ps.ErpId = HYErpIdSize)
			and tp.SizeId != 'Onesize' 
			and tp.ChannelId = 1 
			
			
		--print 'Open Cursor'
		open cur_productHeppo
		fetch next from cur_productHeppo
			into @HYErpId,
				 @HYErpIdSize,
				 @Channel, 			 
				 @NumberInStock,
				 @HYSizeId,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
						
				-- splitta sizeid på semikolon
				SET @Delimiter = '-'
				SET @String = @HYSizeId -- EU-45-
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeChar = substring(@String,1,@Pos - 1)
				--print 'End @SizeChar = ' + @Sizechar
			
				--print 'Start @SizeNo = ' + @SizeNo
				SET @String = @HYSizeId
				SET @String = @String --+ @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeNo = substring(@String,(@Pos+1),(len(@String)- (@pos)))
				
				-- om den specifika sizen av produkten inte finns i tProductSize
				if @SizeChar not in ('XS','S','M','L','XL','Onesize', 'Storlekslös')
				and @HYSizeId not in (select SocksHYText from integration.tSocks) -- NY
				begin
				
					-- Handel the @sizeChar so when it is USM it presents as USMale
					SET @SizeChar = dbo.fx_SizeStandard(@SizeChar)
					
					
					-- Prepare the query that retrives SizeId from tSize
					-- executes a string and returns sizeId
					DECLARE @tSizeIdResult int
					DECLARE @SizeId as int,@sqlQ as nvarchar(200)
					SET @sqlQ=N'select @SizeId = SizeId FROM lekmer.tSize WHERE ' + @SizeChar + ' = ' + REPLACE(@SizeNo, ',', '.')
					EXEC sp_executesql @sqlQ, N'@SizeId int OUTPUT', @SizeId= @SizeId OUTPUT

					SET @tSizeIdResult = @SizeId
					
					set @Data = 'NEW tProductSIZE Shoes: @HYErpIdSize ' + @HYErpIdSize + ' @Fok ' + CAST(@Channel as varchar(10)) + ' @SizeChar ' + @SizeChar + ' @SizeNo ' + @SizeNo
					set @tmpProductId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId)
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					SELECT 
						@tmpProductId,
						@tSizeIdResult, 
						@HYErpIdSize,
						@NumberInStock
					WHERE
						not exists (select 1
									from [lekmer].tProductSize j
									where j.ProductId = @tmpProductId and j.SizeId = @tSizeIdResult)
								
				end
				
				else if @SizeChar not in ('Onesize', 'Storlekslös')
				and @HYSizeId not in (select SocksHYText from integration.tSocks) -- ny
				begin
					
					set @Data = 'NEW tProductSIZE Accessories: @HYErpIdSize ' + @HYErpIdSize + ' @Fok ' + CAST(@Channel as varchar(10)) + ' @SizeChar ' + @SizeChar
					set @tmpProductId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId)
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					SELECT 
						@tmpProductId,
						(select AccessoriesSizeId from integration.tAccessoriesSize where AccessoriesSize = @SizeChar), 
						@HYErpIdSize,
						@NumberInStock
					WHERE
						not exists (select 1
									from [lekmer].tProductSize j
									where j.ProductId = @tmpProductId and j.SizeId = @tSizeIdResult)
				end
				
				else if @HYSizeId in (select SocksHYText from integration.tSocks)
				begin
					-- Tag socks with the appropriet tags
					-- if it is in socks category
					if (SELECT CategoryId FROM product.tCategory 
						WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId) = 1000209
						or
						(SELECT CategoryId FROM product.tCategory 
						WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId) = 1000148
						begin
							-- add tags

							insert into lekmer.tProductTag(ProductId,TagId)
							select
								(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId),
								st.TagId
							
							from
								integration.tSocks s
								inner join integration.tSocksTagHYIdMapping st
									on s.SocksHYId = st.SocksHYId
							where
								s.SocksHYText = @HYSizeId
								and st.TagId not in
												(select TagId 
													from lekmer.tProductTag
													where productId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId)
												)																				
									
							----  nytt endast strumpor
							INSERT INTO 
								[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
								SELECT 
									(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYErpId),
									(select SizeId from lekmer.tSize where EU =
														(select AccessoriesSizeId 
														from integration.tAccessoriesSize 
														where AccessoriesSize = @HYSizeId)), 
									@HYErpIdSize,
									@NumberInStock
						end
				end
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_productHeppo into
				 @HYErpId,
				 @HYErpIdSize,
				 @Channel, 			 
				 @NumberInStock,
				 @HYSizeId,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId
	
		end
		
		close cur_productHeppo
		deallocate cur_productHeppo

END
GO
