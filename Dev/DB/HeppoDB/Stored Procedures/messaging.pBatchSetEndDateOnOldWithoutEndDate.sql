SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE procedure [messaging].[pBatchSetEndDateOnOldWithoutEndDate]
@MaxStartDate	datetime,
@NewEndDate		datetime
as
begin

	update
		[messaging].[tBatch]
	set
		EndDate = @NewEndDate
	where
		EndDate is null and
		StartDate <= @MaxStartDate

end



GO
