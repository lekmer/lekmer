SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Stored Procedure

CREATE PROCEDURE [template].[pModelFragmentEntitySave]
	@ModelFragmentId INT,
	@EntityId INT
AS
begin
	SET NOCOUNT ON
	
	INSERT  [template].[tModelFragmentEntity] 
	(
		[ModelFragmentId],
		[EntityId]
	)
	VALUES
	( 
		@ModelFragmentId,
		@EntityId
	)
	
    RETURN @ModelFragmentId
end	





GO
