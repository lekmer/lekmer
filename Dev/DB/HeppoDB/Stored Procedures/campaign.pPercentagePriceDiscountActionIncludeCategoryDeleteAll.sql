SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionIncludeCategoryDeleteAll]
	@ProductActionId int
as
begin
	DELETE
		campaign.tPercentagePriceDiscountActionIncludeCategory
	WHERE
		ProductActionId = @ProductActionId
end



GO
