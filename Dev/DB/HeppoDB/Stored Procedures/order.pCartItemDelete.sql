SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [order].[pCartItemDelete] 
	@CartItemId int
as
begin
	set nocount on
	
	DELETE FROM 
		[order].tCartItem
	WHERE
		CartItemId = @CartItemId
end
GO
