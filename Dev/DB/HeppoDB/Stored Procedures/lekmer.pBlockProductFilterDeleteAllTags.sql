SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pBlockProductFilterDeleteAllTags]
	@BlockId int
as
begin
	delete
		lekmer.tBlockProductFilterTag
	where
		BlockId = @BlockId
end
GO
