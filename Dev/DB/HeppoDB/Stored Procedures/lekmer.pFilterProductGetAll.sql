SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pFilterProductGetAll]
	@CustomerId int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;
	
	select
		*
	from
		lekmer.vFilterProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[FilterProduct.ProductId]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[FilterProduct.CurrencyId],
				p.[FilterProduct.ProductId],
				p.[FilterProduct.PriceListRegistryId],
				@CustomerId
			)
	where
		[FilterProduct.ChannelId] = @ChannelId
	order by
		[FilterProduct.Title]
end
GO
