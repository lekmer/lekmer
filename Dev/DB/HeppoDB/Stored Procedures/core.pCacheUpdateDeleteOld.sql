SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 13.01.2009
Description:
			Created 
*/

CREATE PROCEDURE [core].[pCacheUpdateDeleteOld]
	@ToDate		datetime
AS
BEGIN
	SET NOCOUNT ON

	delete
	from
		[core].[tCacheUpdate]
	where
		[CreationDate] < @ToDate

END
GO
