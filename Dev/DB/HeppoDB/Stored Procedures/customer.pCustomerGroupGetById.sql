SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupGetById]
@CustomerGroupId	int
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerGroup]
	WHERE
		[CustomerGroup.CustomerGroupId] = @CustomerGroupId
END

GO
