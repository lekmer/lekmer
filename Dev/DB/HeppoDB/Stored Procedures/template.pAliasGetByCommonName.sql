SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 19.09.2008		Time: 11:20
Description:
			Created 
*****************  Version 2  *****************
User: Victor E.		Date: 23.01.2009
Description: Add Path
*****************  Version 3  *****************
User: Yuriy P.		Date: 26.01.2009
Description: Remove Path
*/
CREATE PROCEDURE [template].[pAliasGetByCommonName]
	@LanguageId	int,
	@CommonName	varchar(100)
AS
begin
	set nocount on

	select
		a.*
	from
		[template].[vCustomAlias] a
	where
		a.[Alias.CommonName] = @CommonName
		AND a.[Alias.LanguageId] = @LanguageId
end

GO
