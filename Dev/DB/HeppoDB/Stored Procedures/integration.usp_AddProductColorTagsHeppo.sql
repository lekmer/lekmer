SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_AddProductColorTagsHeppo]

AS
BEGIN

		begin
			begin try
				begin transaction
					
				insert into lekmer.tProductTag
				select distinct
					ProductId = p.ProductId,
					TagId = isnull((select TagId from lekmer.tTag where Value = tp.ColorId), 251)
							
				--select distinct p.ProductId, lp.HYerpid

					-- select *				 
				FROM 
				[integration].tempProduct tp
				inner join [lekmer].tLekmerProduct lp
						on lp.HYErpId = substring(tp.HYarticleId, 5,11)	
				inner join product.tProduct p
						on p.ProductId = lp.ProductId

				where
					 substring(tp.HYarticleId, 3,1) = 1
					 and p.productid not in (select distinct productid
													from lekmer.tproductTag pt
													inner join lekmer.tTag t
														on pt.TagId = t.TagId
													inner join lekmer.tTagGroup tg
														on t.TagGroupId = tg.TagGroupId
													where 
														tg.TagGroupId = 30)
					 
					 
					
				--rollback
				commit
			end try		
			begin catch
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
						values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			

		end		
END
GO
