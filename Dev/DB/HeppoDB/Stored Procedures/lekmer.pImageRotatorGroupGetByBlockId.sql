SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pImageRotatorGroupGetByBlockId]
	@BlockId	int
as	
begin
	set nocount on
	select
		IR.*
	from
		[lekmer].[tImageRotatorGroup] as IR
	where
		IR.[BlockId] = @BlockId
		order by Ordinal
end
GO
