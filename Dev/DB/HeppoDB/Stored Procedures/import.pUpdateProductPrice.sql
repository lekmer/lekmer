SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [import].[pUpdateProductPrice]

AS
BEGIN
	
	if object_id('tempdb..#tProductPriceIncludingVatLess') is not null
		drop table #tProductPriceIncludingVatLess
		
	if object_id('tempdb..#tProductPriceIncludingVatMore') is not null
		drop table #tProductPriceIncludingVatMore
		
	create table #tProductPriceIncludingVatLess
	(
		ProductId int not null,
		PriceListId int not null,
		PriceIncludingVat decimal(16,2) not null,
		PriceExcludingVat decimal(16,2) not null,
		VatPercentage decimal(16,2) not null
		constraint PK_#tProductPriceIncludingVatLess primary key(ProductId, PricelistId)
	)
	
	create table #tProductPriceIncludingVatMore
	(
		ProductId int not null,
		PriceListId int not null,
		PriceIncludingVat decimal(16,2) not null,
		PriceExcludingVat decimal(16,2) not null,
		VatPercentage decimal(16,2) not null
		constraint PK_#tProductPriceIncludingVatMore primary key(ProductId, PricelistId)
	)
	
	begin try
	begin transaction

	declare @Vat decimal
		set @Vat = 25.0
		
		insert #tProductPriceIncludingVatLess(ProductId, PriceListId, PriceIncludingVat,
		PriceExcludingVat, VatPercentage)		
		select distinct
			pli.productid,
			pli.pricelistid,
			t.Price/100.0, -- PriceIncludingVat
			((t.Price/100.0) / (1.0+(25.0/100.0))), -- PRiceExclusingvat
			25.0--@Vat
		from 
		[import].tProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYErpIdSize, 0,12)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = t.ChannelId
		WHERE
			pli.PriceIncludingVat <> (t.Price/100.0)
			and (t.Price/100.0) < pli.PriceIncludingVat
		
		-- UPDATE 
		Update
			ppli
		set
			PriceExcludingVat = t.PriceExcludingVat
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatLess t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid

			
	
		-- UPDATE  (REAL)
		Update
			ppli
		set
			ppli.PriceIncludingVat = t.PriceIncludingVat,
			--ppli.PriceExcludingVat = t.PriceExcludingVat, 
			ppli.VatPercentage = t.VatPercentage
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatLess t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid
			
			
		--- DEL 2  om nya priser är mer än nuvarande priset
		insert #tProductPriceIncludingVatMore(ProductId, PriceListId, PriceIncludingVat,
		PriceExcludingVat, VatPercentage)		
		select distinct
			pli.productid,
			pli.pricelistid,
			(t.Price/100.0), -- PriceIncludingVat
			((t.Price/100.0) / (1.0+(25.0/100.0))), -- PRiceExclusingvat
			@Vat
		from 
		[import].tProductPrice t
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(t.HYErpIdSize, 0,12)
			inner join [product].tPriceListItem pli
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = t.ChannelId
		WHERE
			pli.PriceIncludingVat <> (t.Price/100.0)
			and (t.Price/100.0 ) > pli.PriceIncludingVat
		
		
		-- UPDATE 
		Update
			ppli
		set
			ppli.PriceIncludingVat = t.PriceIncludingVat
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatMore t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid

			
		-- UPDATE  (REAL)
		Update
			ppli
		set
			ppli.PriceExcludingVat = t.PriceExcludingVat, 
			ppli.VatPercentage = t.VatPercentage
		from
			product.tpricelistitem ppli
				inner join #tProductPriceIncludingVatMore t
					on ppli.Productid = t.productid
					and ppli.pricelistid = t.pricelistid
		
		
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
       
END

GO
