SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityFunctionGetByCommonName]
	@EntityFunctionCommonName	varchar(500)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomEntityFunction] 
	where
		[EntityFunction.CommonName] = @EntityFunctionCommonName
end

GO
