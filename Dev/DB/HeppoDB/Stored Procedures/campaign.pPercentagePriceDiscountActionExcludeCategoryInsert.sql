SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pPercentagePriceDiscountActionExcludeCategoryInsert]
	@ProductActionId int,
	@CategoryId int
as
begin
	INSERT
		campaign.tPercentagePriceDiscountActionExcludeCategory
	(
		ProductActionId,
		CategoryId
	)
	VALUES
	(
		@ProductActionId,
		@CategoryId
	)
end



GO
