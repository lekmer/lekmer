SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCampaignSave]
	@CampaignId int,
	@Title varchar(500),
	@CampaignStatusId int,
	@CampaignRegistryId int,
	@FolderId int,
	@StartDate datetime,
	@EndDate datetime,
	@Exclusive bit,
	@Priority int
as
begin
	update
		campaign.tCampaign
	set
		Title = @Title,
		CampaignStatusId = @CampaignStatusId,
		CampaignRegistryId = @CampaignRegistryId,
		FolderId = @FolderId,
		StartDate = @StartDate,
		EndDate = @EndDate,
		Exclusive = @Exclusive,
		Priority = @Priority
	where
		CampaignId = @CampaignId
		
	IF  @@ROWCOUNT = 0
	begin
		set @Priority = (select max(Priority) from campaign.tCampaign) + 1
		if @Priority is null
			begin
				set @Priority = 1
			end
		insert
			campaign.tCampaign
		(
			Title,
			CampaignStatusId,
			CampaignRegistryId,
			FolderId,
			StartDate,
			EndDate,
			Exclusive,
			Priority
		)
		values
		(
			@Title,
			@CampaignStatusId,
			@CampaignRegistryId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority
		)
		set @CampaignId = cast(scope_identity() as int)
	end
	return @CampaignId
end
GO
