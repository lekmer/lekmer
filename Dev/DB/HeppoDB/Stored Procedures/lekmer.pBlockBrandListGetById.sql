SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListGetById]
@LanguageId INT,
@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		bc.*,
		b.*
	FROM
		[lekmer].[vBlockBrandList] AS bc
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bc.[BlockBrandList.BlockId] = b.[Block.BlockId]
	WHERE
		bc.[BlockBrandList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId 
END

GO
