SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Volodymyr Y.   20.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelFragmentGetAllByModelFragmentRegion]
    @ModelFragmentRegionId INT
AS 
BEGIN
    SET NOCOUNT ON
    
    SELECT  *
    FROM    [template].[vCustomModelFragment]
    WHERE   [ModelFragment.RegionId] = @ModelFragmentRegionId
END

GO
