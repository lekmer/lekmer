SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [product].[pProductAttributeGetByProduct]
	@ProductId int,
	@LanguageId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    select
		*
	from
		product.vCustomProductAttribute a
	where
		a.[ProductAttribute.ProductId] = @ProductId and
		a.[ProductAttribute.LanguageId] = @LanguageId
	order by
		a.[ProductAttribute.Ordinal]
END

GO
