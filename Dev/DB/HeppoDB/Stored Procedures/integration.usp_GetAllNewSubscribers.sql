SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_GetAllNewSubscribers]

AS
begin
	set nocount on
	begin try
		begin transaction
		
		Select 
			SubscriberId,
			Email,
			ChannelId
		from 
			lekmer.tNewsletterSubscriber
		where
			SentStatus = 0
			or SentStatus is null -- TRUE is converted to 1 and FALSE is converted to 0. 
		
	commit transaction
	end try
	begin catch
		if @@trancount > 0 rollback transaction
	end catch		 
end
GO
