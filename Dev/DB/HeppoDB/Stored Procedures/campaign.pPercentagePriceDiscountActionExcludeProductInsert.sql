SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pPercentagePriceDiscountActionExcludeProductInsert]
	@ProductActionId int,
	@ProductId int
as
begin
	INSERT
		campaign.tPercentagePriceDiscountActionExcludeProduct
	(
		ProductActionId,
		ProductId
	)
	VALUES
	(
		@ProductActionId,
		@ProductId
	)
end



GO
