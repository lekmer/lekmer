SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pBlockTopListProductGetAllByBlock]
	@ChannelId int,
	@BlockId int
as
begin
	select
		p.*,
		pli.*
	from
		addon.vCustomBlockTopListProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				null
			)
	where
		p.[BlockTopListProduct.BlockId] = @BlockId
		and p.[Product.ChannelId] = @ChannelId
	order by
		[BlockTopListProduct.Position]
end

GO
