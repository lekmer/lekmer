SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockSave]
	@BlockId		int,
	@ContentNodeId	int,
	@BlockTypeId	int,
	@BlockStatusId	int,
	@ContentAreaId	int,
	@Title			nvarchar(200),
	@Ordinal		int,
	@AccessId		int,
	@TemplateId		int
as
begin

	if exists (
				select 
					1 
				from 
					[sitestructure].[tBlock] 
				where 
					[ContentNodeId] = @ContentNodeId 
					AND [ContentAreaId] = @ContentAreaId 
					AND [Title]= @Title 
					AND [BlockId] <> @BlockId
				)
	BEGIN
		--Y 27.11.2009 bug8409: row below was commented since there were no statement in code to catch and process this error, -1 value is returned to notify about title duplicate.
		--raiserror (N'Block with this name for this Page and ContentArea exist !', 16, 1)
		return -1
	end
	
	update
		[sitestructure].[tBlock]
	set
		[BlockStatusId]	= @BlockStatusId,
		[ContentAreaId] = @ContentAreaId,
		[Ordinal]		= @Ordinal,
		[Title]			= @Title,
		[AccessId]		= @AccessId,
		[TemplateId]	= @TemplateId
	where
		[BlockId] = @BlockId
		
	if @@ROWCOUNT = 0
		begin
			declare @NewOrdinal int
			set @NewOrdinal = (
				select
					isnull(max(Ordinal), 0)
				from
					[sitestructure].[tBlock]
				where
					(@ContentNodeId = ContentNodeId AND @ContentAreaId = ContentAreaId)
				) + 1
			insert
				[sitestructure].[tBlock]
			(
				[ContentNodeId],
				[BlockTypeId],
				[BlockStatusId],
				[ContentAreaId],
				[Title],
				[Ordinal],
				[AccessId],
				[TemplateId]
			)
			values
			(
				@ContentNodeId,
				@BlockTypeId,
				@BlockStatusId,
				@ContentAreaId,
				@Title,
				@NewOrdinal,
				@AccessId,
				@TemplateId
			)
			set @BlockId = scope_identity()
		end
	return @BlockId
end

GO
