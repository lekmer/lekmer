SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE PROCEDURE [lekmer].[pProductDeletePopularity]
@ChannelId		int
AS	
BEGIN
	SET NOCOUNT ON	
	
	DELETE  [lekmer].[tProductPopularity]
	where ChannelId = @ChannelId

END

GO
