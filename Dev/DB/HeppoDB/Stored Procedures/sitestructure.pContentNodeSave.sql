SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  PROCEDURE [sitestructure].[pContentNodeSave]
@ContentNodeId				int,
@ParentContentNodeId		int,
@ContentNodeTypeId			int,
@ContentNodeStatusId		int,
@Title						nvarchar(200),
@CommonName					varchar(100),
@AccessId					int,
@SiteStructureRegistryId	int
as
begin
	if exists (
				select 
					1 
				from 
					[sitestructure].[tContentNode] 
				where 
					CommonName = @CommonName 
					and ContentNodeId <> @ContentNodeId
					and SiteStructureRegistryId = @SiteStructureRegistryId
				)
	return -1
	
	update
		[sitestructure].[tContentNode]
	set
		[ContentNodeStatusId]	= @ContentNodeStatusId,
		[Title]					= @Title,
		[CommonName]			= @CommonName,
		[AccessId]				= @AccessId
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		declare @Ordinal int
		set @Ordinal = (
							select
								isnull(max(Ordinal), 0)
							from
								[sitestructure].[tContentNode]
							where
								(@ParentContentNodeId IS NULL 
								AND [ParentContentNodeId] IS NULL)
								OR (
									@ParentContentNodeId IS NOT NULL 
									AND [ParentContentNodeId] = @ParentContentNodeId
									)
						) + 1
		insert
			[sitestructure].[tContentNode]
			(
				[ParentContentNodeId],
				[ContentNodeTypeId],
				[ContentNodeStatusId],
				[Title],
				[Ordinal],
				[CommonName],
				[AccessId],
				[SiteStructureRegistryId]
			)
		values
			(
				@ParentContentNodeId,
				@ContentNodeTypeId,
				@ContentNodeStatusId,
				@Title,
				@Ordinal,
				@CommonName,
				@AccessId,
				@SiteStructureRegistryId
			)
		set @ContentNodeId = scope_identity()
	end	
	return @ContentNodeId
end




GO
