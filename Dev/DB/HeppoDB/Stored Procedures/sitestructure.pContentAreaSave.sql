SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 15:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentAreaSave]
	@ContentAreaId	int,
	@TemplateId		int,
	@Title			nvarchar(50),
	@CommonName		varchar(50)
as
BEGIN
	update
		[sitestructure].[tContentArea]
	set
		[Title]			= @Title,
		[CommonName]	= @CommonName
	where
		[ContentAreaId] = @ContentAreaId
		
	if @@ROWCOUNT = 0
	begin
		insert
			[sitestructure].[tContentArea]
		(
			[TemplateId],
			[Title],
			[CommonName]
		)
		values
		(
			@TemplateId,
			@Title,
			@CommonName
		)
		set @ContentAreaId = scope_identity()
	end
	return @ContentAreaId
end


GO
