SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pAddProductColorTags]

AS
BEGIN

		begin
			begin try
				begin transaction
					
				insert into lekmer.tProductTag
				select distinct
					ProductId = p.ProductId,
					TagId = isnull((select TagId from lekmer.tTag where Value = m.ConvertToTagColor), 251)
					--TagId = isnull((select TagId from lekmer.tTag where Value = tp.ColorId), 251)
					-- select p.productid, lp.Hyerpid, tp.ColorId, x.Value, m.HYColor			 
				FROM 
				[integration].tempProduct tp
				inner join [lekmer].tLekmerProduct lp
						on lp.HYErpId = substring(tp.HYarticleId, 5,11)	
				inner join product.tProduct p
						on p.ProductId = lp.ProductId
				inner join integration.tProductColorMapping m
						on m.HYColor = tp.ColorId
				---------------------
				left join (select distinct productid, t.Value
							from lekmer.tproductTag pt
							inner join lekmer.tTag t
								on pt.TagId = t.TagId
							inner join lekmer.tTagGroup tg
								on t.TagGroupId = tg.TagGroupId
							where 
								tg.TagGroupId = 30) x
						on x.ProductId = p.ProductId

				where
					 substring(tp.HYarticleId, 3,1) = 1
					 and x.ProductId is null
					 --and p.productid not in (select distinct productid
						--							from lekmer.tproductTag pt
						--							inner join lekmer.tTag t
						--								on pt.TagId = t.TagId
						--							inner join lekmer.tTagGroup tg
						--								on t.TagGroupId = tg.TagGroupId
						--							where 
						--								tg.TagGroupId = 30)
					 
					 
					
			
				commit
			end try		
			begin catch
				if @@trancount > 0 rollback transaction
				
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
						values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			

		end		
END
GO
