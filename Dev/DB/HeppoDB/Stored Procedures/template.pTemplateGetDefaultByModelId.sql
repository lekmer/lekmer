SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****************  Version 1  *****************
User: Roman A.		Date: 26.01.2009
Description:
		created
*/

CREATE PROCEDURE [template].[pTemplateGetDefaultByModelId]
	@Id	int
AS
BEGIN
	SELECT
		T.*
	FROM
		[template].[vCustomModel] AS M
		INNER JOIN [template].[vCustomTemplate] AS T ON M.[Model.DefaultTemplateId] = T.[Template.Id]
	WHERE
        M.[Model.Id] = @Id
END

GO
