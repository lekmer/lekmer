SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductGetIdAllByBrandIdList]
	@ChannelId		int,
	@CustomerId		int,
	@BrandIds	varchar(max),
	@Delimiter		char(1),
	@Page			int = NULL,
	@PageSize		int = 1,
	@SortBy			varchar(50) = NULL,
	@SortDescending bit = NULL
AS
BEGIN
	DECLARE @maxCount char(4)
	SET @maxCount = '2000'
	
	DECLARE @sql nvarchar(max)
	
	DECLARE @CustomerIdString VARCHAR(10)
	IF (@CustomerId IS NULL)
		SET @CustomerIdString = 'null'
	ELSE
		SET @CustomerIdString = CAST(@CustomerId AS VARCHAR(10))

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	SET @sql = '
	declare @tResult table (ProductId int, SortBy nvarchar(max))	
	insert @tResult	(ProductId, SortBy)
	select  TOP ' + @maxCount + ' p.[Product.Id], ' + COALESCE(@SortBy, 'p.[Product.Title]') + '
	from [product].[vCustomProduct] p
	INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				' + @CustomerIdString + '
			)
	WHERE
		p.[Product.ChannelId] = ' +  CAST(@ChannelId AS varchar(10)) + ' AND
		EXISTS (
			SELECT * FROM [generic].[fnConvertIDListToTableWithOrdinal](''' + @BrandIds + ''', ''' + @Delimiter+ ''') AS pl 
			WHERE pl.Id = p.[Lekmer.BrandId]
		)

	
	SELECT COUNT(1) FROM @tResult AS SearchResultsCount
	'
	
	IF (@Page != 0 AND @Page IS NOT NULL)
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + CAST(@Page * @PageSize AS varchar(10)) + ' *
		FROM (
			SELECT ROW_NUMBER() OVER (ORDER BY SortBy ' + CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
				ProductId
			FROM
				@tResult
		) AS SearchResult
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
		AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	ELSE
	BEGIN
		SET @sql = @sql + '
		SELECT TOP ' + @maxCount + ' ProductId FROM @tResult AS SearchResult'
	END
			
	EXEC (@sql)
END


GO
