SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 13.01.2009
Description:
			Created 
*/

CREATE PROCEDURE [core].[pCacheUpdateSave]
	@ManagerName	varchar(100),
	@UpdateType		int,
	@Key			varchar(500),
	@CreationDate	datetime
AS
BEGIN
	SET NOCOUNT ON

	INSERT [core].[tCacheUpdate]
	(
		[ManagerName],
		[UpdateType],
		[Key],
		[CreationDate]
	)
	VALUES
	(
		@ManagerName,
		@UpdateType,
		@Key,
		@CreationDate
	)
END
GO
