SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create procedure [lekmer].[pVoucherConditionDeleteAll]
	@ConditionId int
AS 
BEGIN
	
	DELETE FROM lekmer.tVoucherBatches 
	WHERE ConditionId = @ConditionId
END
GO
