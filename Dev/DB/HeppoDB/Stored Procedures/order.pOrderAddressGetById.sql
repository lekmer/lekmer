SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderAddressGetById]
	@OrderAddressId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomOrderAddress] 
	WHERE
		[OrderAddress.OrderAddressId] = @OrderAddressId
END

GO
