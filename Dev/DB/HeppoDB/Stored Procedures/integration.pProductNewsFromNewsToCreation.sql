SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pProductNewsFromNewsToCreation]

AS
begin
	set nocount on
	begin try
		begin transaction
			
			DELETE FROM integration.tempProductNumberInStockDayBeforeNews
			
			
			if object_id('tempdb..#ProductNumberInStockBeforeUpdateSize') is not null
			drop table #ProductNumberInStockBeforeUpdateSize
		
			if object_id('tempdb..#ProductNumberInStockAFTERUpdateSize') is not null
			drop table #ProductNumberInStockAFTERUpdateSize
			
			create table #ProductNumberInStockBeforeUpdateSize
			(
				ProductId int not null,
				NumberInStock int not null
				constraint PK_#ProductNumberInStockBeforeUpdateSize primary key(ProductId)
			)
		
			create table #ProductNumberInStockAFTERUpdateSize
			(
				ProductId int not null,
				NumberInStock int not null
				constraint PK_#ProductNumberInStockAFTERUpdateSize primary key(ProductId)
			)
				
			insert into #ProductNumberInStockBeforeUpdateSize (ProductId, NumberInStock)
			select 
				x.ProductId, 
				sum(x.NoInStock) AS NumberInStock
			from 
				integration.tProductNumberInStockDayBeforeNews x
			group by
				 x.ProductId	
			
			
			
			insert into #ProductNumberInStockAFTERUpdateSize (ProductId, NumberInStock)
			select 
				x.ProductId, 
				sum(x.NumberInStock)
			from 
				(select 
					substring(ErpId, 0,12) as ErpId,
					NumberInStock,
					ProductId
				from
					lekmer.tProductSize) x
			group by
				 x.ProductId			 				
		
		
			-- Make a product "news" if numberInStock has incresed by 6 or more.
			update
				l2
			set
				l2.IsNewFrom = DATEADD(WK, 0, Convert(varchar, GETDATE(), 112)),
				l2.IsNewTo = DATEADD(WK, 2, Convert(varchar, GETDATE(), 112))
			from 
				lekmer.tLekmerProduct l2
				inner join #ProductNumberInStockBeforeUpdateSize pc2
					on l2.ProductId = pc2.ProductId
				inner join #ProductNumberInStockAFTERUpdateSize a2
					on a2.ProductId = pc2.ProductId											
			where 
				-- IsNewTo måste vara mer än dagens datum för att jag vill inte förnya om den är en nyhet
				l2.IsNewTo < DATEADD(WK, 0, Convert(varchar, GETDATE(), 112))
				and 
				(pc2.NumberInStock + 6) <= a2.NumberInStock 
				
			
			-- insert result into tmp Table
			INSERT INTO integration.tempProductNumberInStockDayBeforeNews
					(
						ProductId,
						NumberInStockDayBefore,
						NumberInStock
					)
			SELECT	
				b.ProductId,
				b.NumberInStock,
				a.NumberInStock
			FROM 
				#ProductNumberInStockBeforeUpdateSize b
				INNER JOIN #ProductNumberInStockAFTERUpdateSize a
					ON b.ProductId = a.ProductId
					
					
			--SELECT * FROM integration.tempProductNumberInStockDayBeforeNews
			--WHERE  (NumberInStockDayBefore + 6) <= NumberInStock
		commit transaction
	end try
	begin catch

		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	
	end catch		 
end
GO
