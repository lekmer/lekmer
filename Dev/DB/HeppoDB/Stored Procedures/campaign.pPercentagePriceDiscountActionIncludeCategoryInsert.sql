SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionIncludeCategoryInsert]
	@ProductActionId int,
	@CategoryId int
as
begin
	INSERT
		campaign.tPercentagePriceDiscountActionIncludeCategory
	(
		ProductActionId,
		CategoryId
	)
	VALUES
	(
		@ProductActionId,
		@CategoryId
	)
end



GO
