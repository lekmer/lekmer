SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderSearch]
@FirstName		nvarchar(64),
@LastName		nvarchar(64),
@CustomerEmail  nvarchar(64),
@CivicNumber	varchar(50),
@Page           int = NULL,
@PageSize       int,
@SortBy			varchar(20) = NULL,
@SortAcsending	bit = 0

AS
BEGIN

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN;
	END
		
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

    SET @sqlFragment = 'SELECT ROW_NUMBER() OVER (ORDER BY ' 
		+ COALESCE(@SortBy, 'vO.[Order.CreatedDate]')
		+ CASE WHEN (@SortAcsending = 0) THEN ' DESC' ELSE ' ASC' END + ') AS _Number,
		vO.*
		FROM [order].[vCustomOrder] vO 
		INNER JOIN [customer].[tCustomerInformation] AS CCI ON vO.[Order.CustomerId] = CCI.CustomerId 
        WHERE vO.[OrderStatus.CommonName] <> ''RejectedByPaymentProvider'''
			+ CASE WHEN (@FirstName IS NOT NULL) THEN + '
			AND CCI.FirstName = @FirstName' ELSE '' END 
			
			+ CASE WHEN (@LastName IS NOT NULL) THEN + '
			AND CCI.LastName = @LastName' ELSE '' END 
			
			+ CASE WHEN (@CivicNumber IS NOT NULL) THEN + '
			AND CCI.CivicNumber = @CivicNumber' ELSE '' END 
			
			+ CASE WHEN (@CustomerEmail IS NOT NULL) THEN + '
			AND CCI.Email = @CustomerEmail' ELSE '' END 

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE _Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND _Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@FirstName		nvarchar(64),
			@LastName		nvarchar(64),
			@CustomerEmail  nvarchar(64),
			@CivicNumber	varchar(50)',
			@FirstName,
			@LastName,
			@CustomerEmail,
			@CivicNumber

	EXEC sp_executesql @sql, 
		N'	@FirstName		nvarchar(64),
			@LastName		nvarchar(64),
			@CustomerEmail  nvarchar(64),
			@CivicNumber	varchar(50)',
			@FirstName,
			@LastName,
			@CustomerEmail,
			@CivicNumber
END

GO
