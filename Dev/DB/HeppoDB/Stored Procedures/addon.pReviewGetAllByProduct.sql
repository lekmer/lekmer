SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pReviewGetAllByProduct]
         @ChannelId         int,
         @ProductId int,
         @Page                  int,
         @PageSize   int
AS
BEGIN
         SET NOCOUNT ON; 

    DECLARE @sql nvarchar(max)
         DECLARE @sqlCount nvarchar(max)
         DECLARE @sqlFragment nvarchar(max)
         
         SET @sqlFragment = '
                   SELECT ROW_NUMBER() OVER (ORDER BY r.[Review.CreatedDate] DESC) AS Number,
                            r.*
                  FROM 
                            [addon].[vCustomReview] AS r
                   WHERE 
                            r.[Review.ProductId] = @ProductId AND r.[Review.ChannelId] = @ChannelId AND r.[Review.ReviewStatusId] = 2' /*Approved status id*/

         SET @sql = 
                   'SELECT * FROM
                   (' + @sqlFragment + '
                   )
                   AS SearchResult'

         IF @Page != 0 AND @Page IS NOT NULL 
         BEGIN
                   SET @sql = @sql + '
         WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
         END
         
         SET @sqlCount = 'SELECT COUNT(1) FROM
                   (
                   ' + @sqlFragment + '
                   )
                   AS CountResults'
                   print @sqlCount
                   print @sql
         EXEC sp_executesql @sqlCount,
                   N'      @ChannelId         INT,
                            @ProductId INT',
                            @ChannelId,
                            @ProductId
                                 
         EXEC sp_executesql @sql, 
                   N'      @ChannelId         INT,
                            @ProductId INT',
                            @ChannelId,
                            @ProductId
END

GO
