SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderSave]
	@OrderId INT,
	@Number NVARCHAR(30),
	@CustomerId INT,
	@BillingAddressId INT,
	@DeliveryAddressId INT,
	@CreatedDate DATETIME,
	@FreightCost DECIMAL(16,2),
	@Email VARCHAR(320),
	@DeliveryMethodId INT,
	@ChannelId INT,
	@OrderStatusId int = 1,
	@UsedAlternate bit = 0,
	@IP varchar(15),
	@DeliveryTrackingId varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	update
		[order].[tOrder]
	set
		Number = @Number,            
		CustomerId = @CustomerId,
		BillingAddressId = @BillingAddressId,
		DeliveryAddressId = @DeliveryAddressId,
		CreatedDate = @CreatedDate,
		FreightCost = @FreightCost,
		Email = @Email,
		DeliveryMethodId = @DeliveryMethodId,
		ChannelId = @ChannelId,
		OrderStatusId = @OrderStatusId,
		UsedAlternate = @UsedAlternate,
		IP = @IP,
		DeliveryTrackingId = @DeliveryTrackingId
	where
		OrderId = @OrderId

	if @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [order].[tOrder]
		(
			Number,
			CustomerId,
			BillingAddressId,
			DeliveryAddressId,
			CreatedDate,
			FreightCost,
			Email,
			DeliveryMethodId,
			ChannelId,
			OrderStatusId,
			UsedAlternate,
			IP,
			DeliveryTrackingId
		)
		VALUES
		(
			@Number,
			@CustomerId,
			@BillingAddressId,
			@DeliveryAddressId,
			@CreatedDate,
			@FreightCost,
			@Email,
			@DeliveryMethodId,
			@ChannelId,
			@OrderStatusId,
			@UsedAlternate,
			@IP,
			@DeliveryTrackingId
		)
		SET @OrderId = SCOPE_IDENTITY();
	END
	RETURN @OrderId
END


GO
