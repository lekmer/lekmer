CREATE TABLE [integration].[tSocksTagHYIdMapping]
(
[SocksHYId] [int] NOT NULL,
[TagId] [int] NOT NULL,
[Value] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tSocksTagHYIdMapping] ADD CONSTRAINT [PK__tSocksTa__A0A8A40D14495DAB] PRIMARY KEY CLUSTERED  ([SocksHYId], [TagId]) ON [PRIMARY]
GO
ALTER TABLE [integration].[tSocksTagHYIdMapping] ADD CONSTRAINT [fk_socksHYId] FOREIGN KEY ([SocksHYId]) REFERENCES [integration].[tSocks] ([SocksHYId])
GO
