CREATE TABLE [product].[tPriceListFolder]
(
[PriceListFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentPriceListFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceListFolder] ADD CONSTRAINT [PK_tPriceListFolder] PRIMARY KEY CLUSTERED  ([PriceListFolderId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPriceListFolder_ParentPriceListFolder_Title] ON [product].[tPriceListFolder] ([ParentPriceListFolderId], [Title]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [product].[tPriceListFolder] ADD CONSTRAINT [FK_tPriceListFolder_tPriceListFolder] FOREIGN KEY ([ParentPriceListFolderId]) REFERENCES [product].[tPriceListFolder] ([PriceListFolderId])
GO
