CREATE TABLE [lekmer].[tBlockReviewMyWishList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockReviewMyWishList] ADD CONSTRAINT [PK_tBlockReviewMyWishList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockReviewMyWishList] ADD CONSTRAINT [FK_tBlockReviewMyWishList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
