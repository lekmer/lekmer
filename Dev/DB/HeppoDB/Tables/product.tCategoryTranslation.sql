CREATE TABLE [product].[tCategoryTranslation]
(
[CategoryId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tCategoryTranslation] ADD CONSTRAINT [PK_tCategoryTranslation] PRIMARY KEY CLUSTERED  ([CategoryId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tCategoryTranslation] ADD CONSTRAINT [FK_tCategoryTranslation_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [product].[tCategoryTranslation] ADD CONSTRAINT [FK_tCategoryTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
