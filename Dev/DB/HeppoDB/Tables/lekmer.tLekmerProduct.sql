CREATE TABLE [lekmer].[tLekmerProduct]
(
[ProductId] [int] NOT NULL,
[BrandId] [int] NULL,
[IsBookable] [bit] NOT NULL,
[AgeFromMonth] [int] NOT NULL,
[AgeToMonth] [int] NOT NULL,
[IsNewFrom] [datetime] NULL,
[IsNewTo] [datetime] NULL,
[Measurement] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[BatteryTypeId] [int] NULL,
[NumberOfBatteries] [int] NULL,
[IsBatteryIncluded] [bit] NOT NULL,
[ExpectedBackInStock] [datetime] NULL,
[OldUrl] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[HYErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_tLekmerProduct_CreatedDate] DEFAULT (getdate()),
[SizeDeviationId] [int] NULL,
[LekmerErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [PK_tLekmerProduct] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tLekmerProduct] ON [lekmer].[tLekmerProduct] ([HYErpId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBatteryType] FOREIGN KEY ([BatteryTypeId]) REFERENCES [lekmer].[tBatteryType] ([BatteryTypeId]) ON DELETE SET NULL
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [lekmer].[tLekmerProduct] ADD CONSTRAINT [FK_tLekmerProduct_tSizeDeviation] FOREIGN KEY ([SizeDeviationId]) REFERENCES [lekmer].[tSizeDeviation] ([SizeDeviationId])
GO
