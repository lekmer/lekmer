CREATE TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] ADD CONSTRAINT [PK_tPercentagePriceDiscountActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeProduct_tPercentagePriceDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tPercentagePriceDiscountAction] ([ProductActionId])
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeProduct] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
