CREATE TABLE [campaign].[tCampaignStatus]
(
[CampaignStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignStatus] ADD CONSTRAINT [PK_tCampaignStatus] PRIMARY KEY CLUSTERED  ([CampaignStatusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignStatus_CommonName] ON [campaign].[tCampaignStatus] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignStatus_Title] ON [campaign].[tCampaignStatus] ([Title]) ON [PRIMARY]
GO
