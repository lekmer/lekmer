CREATE TABLE [addon].[tBlockTopListCategory]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopListCategory] ADD CONSTRAINT [PK_tBlockTopListCategory] PRIMARY KEY CLUSTERED  ([BlockId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockTopListCategory] ADD CONSTRAINT [FK_tBlockTopListCategory_tBlockTopList] FOREIGN KEY ([BlockId]) REFERENCES [addon].[tBlockTopList] ([BlockId])
GO
ALTER TABLE [addon].[tBlockTopListCategory] ADD CONSTRAINT [FK_tBlockTopListCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
