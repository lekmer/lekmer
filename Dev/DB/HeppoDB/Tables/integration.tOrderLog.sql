CREATE TABLE [integration].[tOrderLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[OrderXmlIn] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[OrderXmlOut] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tOrderLog] ADD CONSTRAINT [PK__tOrderLo__3214EC075358458B] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
