CREATE TABLE [integration].[tProductColorMapping]
(
[HYColor] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ConvertToTagColor] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductColorMapping] ADD CONSTRAINT [PK__tProduct__51A01F1922D672D4] PRIMARY KEY CLUSTERED  ([HYColor]) ON [PRIMARY]
GO
