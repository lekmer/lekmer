CREATE TABLE [integration].[tempProductStockPosition]
(
[HYarticleId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Lagerplatstyp] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[LagerplatstypBenamning] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[Lagerplats] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tempProductStockPosition] ADD CONSTRAINT [PK__tempProd__3FA33FFA2DA90702] PRIMARY KEY CLUSTERED  ([HYarticleId], [Lagerplats]) ON [PRIMARY]
GO
