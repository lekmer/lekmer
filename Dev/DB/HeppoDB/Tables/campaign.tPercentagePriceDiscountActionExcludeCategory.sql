CREATE TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tPercentagePriceDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionExcludeCategory_tPercentagePriceDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tPercentagePriceDiscountAction] ([ProductActionId])
GO
