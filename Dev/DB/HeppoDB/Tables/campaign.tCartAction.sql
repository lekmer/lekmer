CREATE TABLE [campaign].[tCartAction]
(
[CartActionId] [int] NOT NULL IDENTITY(1, 1),
[CartActionTypeId] [int] NOT NULL,
[CampaignId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartAction] ADD CONSTRAINT [PK_tCartAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCartAction_CampaignId] ON [campaign].[tCartAction] ([CampaignId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCartAction_CartActionTypeId] ON [campaign].[tCartAction] ([CartActionTypeId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCartAction] ADD CONSTRAINT [FK_tCartAction_tCartCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCartCampaign] ([CampaignId])
GO
ALTER TABLE [campaign].[tCartAction] WITH NOCHECK ADD CONSTRAINT [FK_tCartAction_tCartActionType] FOREIGN KEY ([CartActionTypeId]) REFERENCES [campaign].[tCartActionType] ([CartActionTypeId])
GO
