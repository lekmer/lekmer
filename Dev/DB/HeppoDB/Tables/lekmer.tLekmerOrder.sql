CREATE TABLE [lekmer].[tLekmerOrder]
(
[OrderId] [int] NOT NULL,
[PaymentCost] [decimal] (16, 2) NOT NULL,
[VoucherDiscount] [decimal] (16, 2) NULL,
[CustomerIdentificationKey] [nchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [PK_tLekmerOrder] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [FK_tLekmerOrder_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
