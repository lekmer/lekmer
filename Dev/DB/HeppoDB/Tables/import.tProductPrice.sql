CREATE TABLE [import].[tProductPrice]
(
[HYErpIdSize] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ChannelId] [int] NOT NULL,
[Price] [decimal] (18, 0) NULL
) ON [PRIMARY]
GO
ALTER TABLE [import].[tProductPrice] ADD CONSTRAINT [PK__tProduct__3DCBFF4F0EE3B93A] PRIMARY KEY CLUSTERED  ([ChannelId], [HYErpIdSize]) ON [PRIMARY]
GO
