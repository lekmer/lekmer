CREATE TABLE [lekmer].[tBlockProductFilterBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductFilterBrand] ADD CONSTRAINT [PK_tBlockProductFilterBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductFilterBrand] ADD CONSTRAINT [FK_tBlockProductFilterBrand_tBlockProductFilter] FOREIGN KEY ([BlockId]) REFERENCES [lekmer].[tBlockProductFilter] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockProductFilterBrand] ADD CONSTRAINT [FK_tBlockProductFilterBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
