CREATE TABLE [product].[tBlockProductImageList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[ProductImageGroupId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductImageList] ADD CONSTRAINT [PK_tBlockProductImageList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductImageList_ProductImageGroupId] ON [product].[tBlockProductImageList] ([ProductImageGroupId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductImageList] ADD CONSTRAINT [FK_tBlockProductImageList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [product].[tBlockProductImageList] ADD CONSTRAINT [FK_tBlockProductImageList_tProductImageGroup] FOREIGN KEY ([ProductImageGroupId]) REFERENCES [product].[tProductImageGroup] ([ProductImageGroupId])
GO
