CREATE TABLE [lekmer].[tSize]
(
[SizeId] [int] NOT NULL IDENTITY(1, 1),
[EU] [decimal] (4, 1) NOT NULL,
[USMale] [decimal] (3, 1) NOT NULL,
[USFemale] [decimal] (3, 1) NOT NULL,
[UKMale] [decimal] (3, 1) NOT NULL,
[UKFemale] [decimal] (3, 1) NOT NULL,
[Millimeter] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSize] ADD CONSTRAINT [PK_tSize] PRIMARY KEY CLUSTERED  ([SizeId]) ON [PRIMARY]
GO
