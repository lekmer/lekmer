CREATE TABLE [product].[tProductAttribute]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductAttribute_LanguageId] ON [product].[tProductAttribute] ([LanguageId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductAttribute_ProductId] ON [product].[tProductAttribute] ([ProductId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductAttribute] ADD CONSTRAINT [FK_tProductAttribute_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [product].[tProductAttribute] ADD CONSTRAINT [FK_tProductAttribute_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
