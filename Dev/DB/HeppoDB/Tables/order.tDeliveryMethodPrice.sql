CREATE TABLE [order].[tDeliveryMethodPrice]
(
[CountryId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL,
[FreightCost] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tDeliveryMethodPrice] ADD CONSTRAINT [PK_order.tDeliveryMethodPrice] PRIMARY KEY CLUSTERED  ([CountryId], [CurrencyId], [DeliveryMethodId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tDeliveryMethodPrice] ADD CONSTRAINT [FK_tDeliveryMethodPrice_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
GO
ALTER TABLE [order].[tDeliveryMethodPrice] ADD CONSTRAINT [FK_tDeliveryMethodPrice_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
ALTER TABLE [order].[tDeliveryMethodPrice] ADD CONSTRAINT [FK_tDeliveryMethodPrice_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
