CREATE TABLE [template].[tAliasFolder]
(
[AliasFolderId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ParentAliasFolderId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tAliasFolder] ADD CONSTRAINT [PK_tAliasFolder] PRIMARY KEY CLUSTERED  ([AliasFolderId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tAliasFolder_ParentAliasFolderId_Title] ON [template].[tAliasFolder] ([ParentAliasFolderId], [Title]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [template].[tAliasFolder] ADD CONSTRAINT [FK_tAliasFolder_tAliasFolder] FOREIGN KEY ([ParentAliasFolderId]) REFERENCES [template].[tAliasFolder] ([AliasFolderId])
GO
