CREATE TABLE [import].[tProduct]
(
[HYErpId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[HYErpIdSize] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ChannelId] [int] NOT NULL,
[ArticleTitle] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[SizeId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ColorId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Price] [int] NULL,
[NumberInStock] [int] NULL,
[ArticleGroupId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleGroupTitle] [nvarchar] (40) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleClassId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleClassTitle] [nvarchar] (40) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleCodeId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleCodeTitle] [nvarchar] (40) COLLATE Finnish_Swedish_CI_AS NULL,
[BrandId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[BrandTitle] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[SupplierSize] [nvarchar] (15) COLLATE Finnish_Swedish_CI_AS NULL,
[Period] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [import].[tProduct] ADD CONSTRAINT [PK__tProduct__3DCBFF4F0B132856] PRIMARY KEY CLUSTERED  ([ChannelId], [HYErpIdSize]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProduct_HYErpIdSize] ON [import].[tProduct] ([HYErpIdSize]) ON [PRIMARY]
GO
