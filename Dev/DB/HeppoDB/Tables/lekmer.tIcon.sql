CREATE TABLE [lekmer].[tIcon]
(
[IconId] [int] NOT NULL IDENTITY(1, 1),
[Title] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[MediaId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tIcon] ADD CONSTRAINT [PK_tIcon] PRIMARY KEY CLUSTERED  ([IconId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tIcon] ADD CONSTRAINT [FK_tIcon_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId])
GO
