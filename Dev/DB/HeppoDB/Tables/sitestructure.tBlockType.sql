CREATE TABLE [sitestructure].[tBlockType]
(
[BlockTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL,
[AvailableForAllPageTypes] [bit] NOT NULL CONSTRAINT [DF_tBlockType_AvailableForAllPageTypes] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockType] ADD CONSTRAINT [PK_tBlockType] PRIMARY KEY CLUSTERED  ([BlockTypeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockType] ADD CONSTRAINT [UQ_tBlockType_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
