CREATE TABLE [integration].[tOrderQue]
(
[OrderId] [int] NOT NULL,
[NumberOfFailedAttempts] [int] NOT NULL,
[LastFailedAttempt] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tOrderQue] ADD CONSTRAINT [PK__tOrderQu__C3905BCF5AF96753] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
