CREATE TABLE [media].[tMediaFormat]
(
[MediaFormatId] [int] NOT NULL IDENTITY(1, 1),
[MediaFormatGroupId] [int] NULL,
[Title] [nvarchar] (150) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Mime] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Extension] [nvarchar] (8) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [media].[tMediaFormat] ADD CONSTRAINT [PK_tMediaFormat] PRIMARY KEY CLUSTERED  ([MediaFormatId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tMediaFormat_CommonName] ON [media].[tMediaFormat] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tMediaFormat_Extension] ON [media].[tMediaFormat] ([Extension]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tMediaFormat_MediaFormatGroupId] ON [media].[tMediaFormat] ([MediaFormatGroupId]) ON [PRIMARY]
GO
ALTER TABLE [media].[tMediaFormat] ADD CONSTRAINT [FK_tMediaFormat_tMediaFormatGroup] FOREIGN KEY ([MediaFormatGroupId]) REFERENCES [media].[tMediaFormatGroup] ([MediaFormatGroupId])
GO
