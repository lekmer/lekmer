CREATE TABLE [product].[tBlockProductSearchResult]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NULL,
[RowCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductSearchResult] ADD CONSTRAINT [PK_tBlockProductSearchResult] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tBlockProductSearchResult] ADD CONSTRAINT [FK_tBlockProductSearchResult_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
