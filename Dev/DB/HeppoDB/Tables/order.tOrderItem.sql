CREATE TABLE [order].[tOrderItem]
(
[OrderItemId] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[Quantity] [int] NOT NULL,
[ActualPriceIncludingVat] [decimal] (16, 2) NOT NULL,
[OriginalPriceIncludingVat] [decimal] (16, 2) NOT NULL,
[VAT] [decimal] (16, 2) NOT NULL,
[OrderItemStatusId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderItem] ADD CONSTRAINT [CK_tOrderItem_Quantity] CHECK (([Quantity]>(0)))
GO
ALTER TABLE [order].[tOrderItem] ADD CONSTRAINT [PK_tOrderItem] PRIMARY KEY CLUSTERED  ([OrderItemId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItem_OrderId] ON [order].[tOrderItem] ([OrderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderItem_OrderItemStatusId] ON [order].[tOrderItem] ([OrderItemStatusId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderItem] ADD CONSTRAINT [FK_tOrderItem_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
ALTER TABLE [order].[tOrderItem] ADD CONSTRAINT [FK_tOrderItem_tOrderItemStatus] FOREIGN KEY ([OrderItemStatusId]) REFERENCES [order].[tOrderItemStatus] ([OrderItemStatusId])
GO
