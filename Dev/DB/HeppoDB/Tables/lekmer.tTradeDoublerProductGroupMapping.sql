CREATE TABLE [lekmer].[tTradeDoublerProductGroupMapping]
(
[ProductId] [int] NOT NULL,
[ProductGroupId] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ChannelId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTradeDoublerProductGroupMapping] ADD CONSTRAINT [PK__tTradeDo__E780F84C4349CAB9] PRIMARY KEY CLUSTERED  ([ProductId], [ChannelId]) ON [PRIMARY]
GO
