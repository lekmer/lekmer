CREATE TABLE [order].[tOrderItemProduct]
(
[OrderItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [varchar] (20) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderItemProduct] ADD CONSTRAINT [PK_tOrderItemProduct] PRIMARY KEY CLUSTERED  ([OrderItemId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderItemProduct] ADD CONSTRAINT [FK_tOrderItemProduct_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
GO
