CREATE TABLE [lekmer].[tProductUrl]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[UrlTitle] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductUrl] ADD CONSTRAINT [PK_tProductUrl] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tProductUrl_LanguageId_UrlTitle] ON [lekmer].[tProductUrl] ([LanguageId], [UrlTitle]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductUrl] ADD CONSTRAINT [FK_tProductUrl_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [lekmer].[tProductUrl] ADD CONSTRAINT [FK_tProductUrl_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
