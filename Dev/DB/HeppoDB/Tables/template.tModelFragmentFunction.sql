CREATE TABLE [template].[tModelFragmentFunction]
(
[FunctionId] [int] NOT NULL IDENTITY(1, 1),
[ModelFragmentId] [int] NOT NULL,
[FunctionTypeId] [int] NOT NULL,
[CommonName] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [PK_tModelFragmentFunction] PRIMARY KEY CLUSTERED  ([FunctionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tModelFragmentFunction_FunctionTypeId] ON [template].[tModelFragmentFunction] ([FunctionTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tModelFragmentFunction_ModelFragnentId] ON [template].[tModelFragmentFunction] ([ModelFragmentId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tFunctionType] FOREIGN KEY ([FunctionTypeId]) REFERENCES [template].[tFunctionType] ([FunctionTypeId])
GO
ALTER TABLE [template].[tModelFragmentFunction] ADD CONSTRAINT [FK_tModelFragmentFunction_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
GO
