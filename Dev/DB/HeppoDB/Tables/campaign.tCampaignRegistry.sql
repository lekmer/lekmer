CREATE TABLE [campaign].[tCampaignRegistry]
(
[CampaignRegistryId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tCampaignRegistry] ADD CONSTRAINT [PK_tCampaignRegistry] PRIMARY KEY CLUSTERED  ([CampaignRegistryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignRegistry_CommonName] ON [campaign].[tCampaignRegistry] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCampaignRegistry_Title] ON [campaign].[tCampaignRegistry] ([Title]) ON [PRIMARY]
GO
