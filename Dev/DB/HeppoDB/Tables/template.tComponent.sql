CREATE TABLE [template].[tComponent]
(
[ComponentId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tComponent] ADD CONSTRAINT [PK_tComponent] PRIMARY KEY CLUSTERED  ([ComponentId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tComponent_CommonName] ON [template].[tComponent] ([CommonName]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
