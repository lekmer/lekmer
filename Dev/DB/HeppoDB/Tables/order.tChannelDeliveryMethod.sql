CREATE TABLE [order].[tChannelDeliveryMethod]
(
[ChannelId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tChannelDeliveryMethod] ADD CONSTRAINT [PK_ChannelDeliveryMethod] PRIMARY KEY CLUSTERED  ([ChannelId], [DeliveryMethodId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tChannelDeliveryMethod] ADD CONSTRAINT [FK_tChannelDeliveryMethod_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [order].[tChannelDeliveryMethod] ADD CONSTRAINT [FK_tChannelDeliveryMethod_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
