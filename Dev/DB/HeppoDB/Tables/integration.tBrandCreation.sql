CREATE TABLE [integration].[tBrandCreation]
(
[BrandId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tBrandCreation] ADD CONSTRAINT [PK__tBrandCr__DAD4F05E102EE5CE] PRIMARY KEY CLUSTERED  ([BrandId]) ON [PRIMARY]
GO
