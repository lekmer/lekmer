CREATE TABLE [lekmer].[tLekmerCartItem]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[CartItemId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [PK_tLekmerCartItem] PRIMARY KEY CLUSTERED  ([CartItemId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [FK_tLekmerCartItem_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId])
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [FK_tLekmerCartItem_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
