SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fGetImageUrl]
(
	@ProductId int,
	@LanguageId int
)
RETURNS varchar(1000)
AS
BEGIN

declare @mediaid int

select @mediaid = MediaId from (
select top 1 MediaId from product.tProductImage where ProductImageGroupId = 1 and ProductId = @ProductId order by ordinal) t

return (select 'http://' + ApplicationName + '/mediaarchive/' + cast(@mediaid as varchar) + '/' + cast(@mediaid as varchar) + '.jpg'  from core.tChannel where LanguageId = @LanguageId and not ChannelId = 1000005)



END
GO
