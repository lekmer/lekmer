

ALTER VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO

ALTER view [product].[vCustomProductSecure]
as
	select
		*
	from
		[product].[vProductSecure]

GO


ALTER VIEW [product].[vProductRelationList]
AS
SELECT      
       prl.RelationListId 'ProductRelationList.RelationListId',
       p.*
       
FROM       
       [product].[tProductRelationList] prl       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = prl.ProductId


GO

ALTER VIEW [product].[vRelationListProduct]
AS
SELECT      
       rlp.RelationListId 'RelationListProduct.RelationListId',
       p.*
       
FROM       
       [product].[tRelationListProduct] rlp       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = rlp.ProductId


GO

exec dev.pRefreshAllViews
go

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping [product].[pProductGetAllUsingRelationList ]'
GO
DROP PROCEDURE [product].[pProductGetAllUsingRelationList ]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListSearch]'
GO

ALTER PROCEDURE [product].[pPriceListSearch]
	@SearchParameter NVARCHAR(50),
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = NULL,
	@SortDescending	BIT = NULL
AS
BEGIN
	
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY pl.[' 
			+ COALESCE(@SortBy, 'PriceList.Id')
			+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			pl.*
		FROM
			[product].[vCustomPriceList] pl
		WHERE
			(
				pl.[PriceList.Title] LIKE ''%'' + @SearchParameter + ''%''
				OR EXISTS (
					SELECT
						1
					FROM
						product.tProduct p
						inner join product.tPriceListItem pli on pli.PriceListId = pl.[PriceList.Id] and pli.ProductId = p.ProductId
					WHERE
						p.ErpId = @SearchParameter
				)
			)
		'

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@SearchParameter nvarchar(50)',
			@SearchParameter

	EXEC sp_executesql @sql, 
		N'	@SearchParameter nvarchar(50)',
			@SearchParameter
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListGetAllByFolderWithPaging]'
GO

ALTER PROCEDURE [product].[pPriceListGetAllByFolderWithPaging]
	@PriceListFolderId NVARCHAR(50),
	@Page INT = NULL,
	@PageSize INT,
	@SortBy VARCHAR(20) = NULL,
	@SortDescending	BIT = NULL
AS
BEGIN
	
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY pl.[' 
			+ COALESCE(@SortBy, 'PriceList.Id')
			+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			pl.*
		FROM
			[product].[vCustomPriceList] pl
		WHERE
		   	pl.[PriceList.FolderId] = @PriceListFolderId'

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
			WHERE Number > ' + CAST((@Page - 1) * @PageSize AS VARCHAR(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS VARCHAR(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	
	EXEC sp_executesql @sqlCount,
		N'	@PriceListFolderId nvarchar(50)',
			@PriceListFolderId

	EXEC sp_executesql @sql, 
		N'	@PriceListFolderId nvarchar(50)',
			@PriceListFolderId
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[fnGetCurrencyAndPriceListRegistry]'
GO
CREATE function [product].[fnGetCurrencyAndPriceListRegistry](
	@ProductId int,
	@ChannelId int
)
RETURNS @tPriceListData TABLE (CurrencyId int, PriceListRegistryId int)
AS 
BEGIN
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	INSERT INTO @tPriceListData (CurrencyId, PriceListRegistryId)
	SELECT @CurrencyId, PMC.PriceListRegistryId 
	FROM product.tProductRegistryProduct AS PRP INNER JOIN product.tProductModuleChannel AS PMC ON 
			PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId AND PRP.ProductId = @ProductId	
	RETURN 
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListGetAllByFolder]'
GO
ALTER PROCEDURE [product].[pPriceListGetAllByFolder]
	@PriceListFolderId int
AS
BEGIN
	SELECT
		pl.*
	FROM
		[product].[vCustomPriceList] pl
	WHERE
		pl.[PriceList.FolderId] = @PriceListFolderId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pPriceListItemGetAllByProduct]'
GO

ALTER PROCEDURE [product].[pPriceListItemGetAllByProduct]
	@ProductId	int
as
begin
	SET NOCOUNT ON;

	SELECT 
		v.*
	FROM 
		product.vCustomPriceListItem v
	where
		v.[Price.ProductId] = @ProductId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [product].[pProductGetAllUsingRelationList]'
GO
CREATE PROCEDURE [product].[pProductGetAllUsingRelationList]
		@ChannelId int,
		@RelationListId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY prl.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		prl.*,
		[pli].*
		FROM [product].[vCustomProductRelationList] prl
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = prl.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = prl.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', prl.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			prl.[ProductRelationList.RelationListId] = '+  CAST(@RelationListId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END
	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetAllByCategory]'
GO
ALTER PROCEDURE [product].[pProductGetAllByCategory]
		@ChannelId int,
		@CategoryId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END
	
DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY p.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		[p].*,
		[pli].*
		FROM [product].[vCustomProductSecure] p
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = p.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', p.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			p.[Product.CategoryId] = '+  CAST(@CategoryId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END	

	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetAllByRelationList]'
GO
ALTER PROCEDURE [product].[pProductGetAllByRelationList]
		@ChannelId int,
		@RelationListId int,
		@Page int = NULL,
		@PageSize int,
		@SortBy varchar(20) = NULL,
		@SortDescending bit = NULL
AS
BEGIN

IF (generic.fnValidateColumnName(@SortBy) = 0) 
BEGIN
	RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
	RETURN
END

DECLARE @sql nvarchar(max)
DECLARE @sqlCount nvarchar(max)
DECLARE @sqlFilter nvarchar(max)
DECLARE @CurrencyId int
SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId

SET @sqlFilter = '
	(
		SELECT ROW_NUMBER() OVER (ORDER BY rlp.[' 
		+ COALESCE(@SortBy, 'Product.ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
		rlp.*,
		[pli].*
		FROM [product].[vCustomRelationListProduct]  rlp
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = rlp.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = ' + CAST(@ChannelId AS varchar(10)) + '
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = rlp.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(' + CAST(@CurrencyId AS varchar(10)) + ', rlp.[Product.Id], PMC.PriceListRegistryId, NULL)
		WHERE
			rlp.[RelationListProduct.RelationListId] = '+  CAST(@RelationListId AS varchar(10)) +'
	)'

SET @sql = '
	SELECT * FROM
	' + @sqlFilter + '
	AS SearchResult'
SET @sqlCount = '
	SELECT COUNT(1) FROM
	' + @sqlFilter + '
	AS SearchResultsCount'
IF (@Page != 0 AND @Page IS NOT NULL)
BEGIN
SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + '
	AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
END
	EXEC (@sqlCount)
	EXEC (@sql)
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vProductRecord]'
GO


ALTER VIEW [product].[vProductRecord]
AS
SELECT     
	P.ProductId 'Product.Id',
	P.ItemsInPackage 'Product.ItemsInPackage',
	P.ErpId 'Product.ErpId',
	P.EanCode 'Product.EanCode',
	P.NumberInStock 'Product.NumberInStock',
    P.CategoryId 'Product.CategoryId',
    P.WebShopTitle 'Product.WebShopTitle',
    P.Title 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.Description 'Product.Description',
    P.ShortDescription 'Product.ShortDescription',
    P.IsDeleted 'Product.IsDeleted',
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vProductSecure]'
GO

ALTER VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO


ALTER VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO

ALTER view [product].[vCustomProductSecure]
as
	select
		*
	from
		[product].[vProductSecure]

GO

ALTER VIEW [product].[vRelationListProduct]
AS
SELECT      
       rlp.RelationListId 'RelationListProduct.RelationListId',
       p.*
       
FROM       
       [product].[tRelationListProduct] rlp       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = rlp.ProductId


GO

exec dev.pRefreshAllViews
go

PRINT N'Altering [product].[pProductGetRecordById]'
GO
ALTER PROCEDURE [product].[pProductGetRecordById]
	@ProductId	int,
	@ChannelId	int
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @CurrencyId int, @PriceListRegistryId int, @PriceListId int
	SELECT @CurrencyId = CurrencyId, @PriceListRegistryId = PriceListRegistryId
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT *		
	FROM 
		[product].[vCustomProductRecord] as p
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = @PriceListId
	WHERE
		p.[Product.Id] = @ProductId
	
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[vCategoryView]'
GO

ALTER VIEW [product].[vCategoryView]
AS
	SELECT     
		c.*,
		ch.[Channel.Id],
		r.[CategoryRegistry.ProductParentContentNodeId],
		r.[CategoryRegistry.ProductTemplateContentNodeId]
	FROM
		[product].[vCustomCategory] AS c
		CROSS JOIN core.vCustomChannel ch
		LEFT JOIN sitestructure.tSiteStructureModuleChannel mch ON mch.ChannelId = ch.[Channel.Id]
		LEFT JOIN product.vCustomCategorySiteStructureRegistry r
			ON mch.SiteStructureRegistryId = r.[CategoryRegistry.SiteStructureRegistryId]
			AND c.[Category.Id] = r.[CategoryRegistry.CategoryId]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO

exec dev.pRefreshAllViews
go

PRINT N'Altering [product].[pProductGetByIdSecure]'
GO
ALTER PROCEDURE [product].[pProductGetByIdSecure]
	@ProductId	int,
	@ChannelId	int
as
begin
	SET NOCOUNT ON;	
	
	DECLARE @CurrencyId int, @PriceListRegistryId int, @PriceListId int
	SELECT @CurrencyId = CurrencyId, @PriceListRegistryId = PriceListRegistryId
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT *		
	FROM 
		[product].[vCustomProductSecure] as p
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = @PriceListId
	WHERE
		p.[Product.Id] = @ProductId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewById]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewById]
	@CategoryId	int,
	@ChannelId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Category.Id] = @CategoryId
		and c.[Channel.Id] = @ChannelId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pCategoryGetViewAll]'
GO
ALTER PROCEDURE [product].[pCategoryGetViewAll]
	@ChannelId	int,
	@LanguageId	int
as
begin
	select
		c.*
	from
		product.vCustomCategoryView as c
	where
		c.[Channel.Id] = @ChannelId
		and c.[Category.LanguageId] = @LanguageId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pProductGetAllByVariationGroup]'
GO
ALTER PROCEDURE [product].[pProductGetAllByVariationGroup]
	@VariationGroupId	int,
	@ChannelId			int
as
begin
	SET NOCOUNT ON;
	
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	SELECT
		P.*,
		pli.*
	FROM 
		[product].[vCustomProductSecure] AS P
		INNER JOIN [product].[tVariationGroupProduct] AS VGP on P.[Product.Id] = VGP.[ProductId] AND VGP.VariationGroupId = @VariationGroupId    
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = P.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, P.[Product.Id], PMC.PriceListRegistryId, NULL)
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [product].[pBlockProductListProductGetAllByBlockSecure]'
GO

ALTER PROCEDURE [product].[pBlockProductListProductGetAllByBlockSecure]
	@ChannelId int,
	@BlockId int
as
begin
	
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	SELECT 
		bp.*,
		p.*,
		pli.*
	from 
		[product].[vCustomBlockProductListProduct] as bp
		inner join [product].[vCustomProductSecure] as p on bp.[BlockProductListProduct.ProductId] = p.[Product.Id] AND bp.[BlockProductListProduct.BlockId] = @BlockId
		-- get price for current channel
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = P.[Product.Id] 
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, P.[Product.Id], PMC.PriceListRegistryId, NULL)
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO