SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [template].[tModelFragmentFunction]'
GO
ALTER TABLE [template].[tModelFragmentFunction] ALTER COLUMN [CommonName] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pModelFragmentFunctionSave]'
GO
ALTER PROCEDURE [template].[pModelFragmentFunctionSave] 
	@FunctionId INT,
	@ModelFragmentId INT,
	@FunctionTypeId INT,
	@CommonName NVARCHAR(500),
	@Description NVARCHAR(MAX)
AS 
BEGIN
    SET NOCOUNT ON
    	
	UPDATE  
		[template].[tModelFragmentFunction]
	SET     
		[ModelFragmentId] = @ModelFragmentId,
		[FunctionTypeId] = @FunctionTypeId,
		[CommonName] = @CommonName,
		[Description] = @Description
	WHERE
		[FunctionId] = @FunctionId
		
	IF  @@ROWCOUNT = 0 
	BEGIN
		INSERT  [template].[tModelFragmentFunction] 
        (
			[ModelFragmentId],
			[FunctionTypeId],
			[CommonName],
			[Description]
        )
        VALUES
        (
        	@ModelFragmentId,
			@FunctionTypeId, 
			@CommonName,
			@Description
		)

        SET @FunctionId = SCOPE_IDENTITY()
   END
   RETURN @FunctionId
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pModelFragmentFunctionGetByCommonName]'
GO
ALTER PROCEDURE [template].[pModelFragmentFunctionGetByCommonName]
	@CommonName	varchar(500)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomModelFragmentFunction]
	where
		[ModelFragmentFunction.CommonName] = @CommonName
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[tEntityFunction]'
GO
ALTER TABLE [template].[tEntityFunction] ALTER COLUMN [CommonName] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [template].[pEntityFunctionGetByCommonName]'
GO
ALTER PROCEDURE [template].[pEntityFunctionGetByCommonName]
	@EntityFunctionCommonName	varchar(500)
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomEntityFunction] 
	where
		[EntityFunction.CommonName] = @EntityFunctionCommonName
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO