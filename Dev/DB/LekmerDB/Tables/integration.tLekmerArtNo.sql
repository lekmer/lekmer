CREATE TABLE [integration].[tLekmerArtNo]
(
[HYarticleId] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[LakmerArtNo] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tLekmerArtNo] ADD CONSTRAINT [PK__tLekmerA__ED0627FB6A1E19B5] PRIMARY KEY CLUSTERED  ([HYarticleId]) ON [PRIMARY]
GO
