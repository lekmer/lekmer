CREATE TABLE [product].[tProductImage]
(
[ProductId] [int] NOT NULL,
[MediaId] [int] NOT NULL,
[ProductImageGroupId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductImage] ADD CONSTRAINT [PK_tProductImage] PRIMARY KEY CLUSTERED  ([ProductId], [MediaId], [ProductImageGroupId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductImage] ADD CONSTRAINT [FK_tProductImage_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId]) ON DELETE CASCADE
GO
ALTER TABLE [product].[tProductImage] ADD CONSTRAINT [FK_tProductImage_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tProductImage] ADD CONSTRAINT [FK_tProductImage_tProductImageGroup] FOREIGN KEY ([ProductImageGroupId]) REFERENCES [product].[tProductImageGroup] ([ProductImageGroupId])
GO
