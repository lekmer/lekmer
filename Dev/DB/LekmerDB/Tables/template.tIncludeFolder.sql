CREATE TABLE [template].[tIncludeFolder]
(
[IncludeFolderId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ParentIncludeFolderId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tIncludeFolder] ADD CONSTRAINT [PK_tIncludeFolder] PRIMARY KEY CLUSTERED  ([IncludeFolderId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tIncludeFolder_ParentIncludeFolderId_Title] ON [template].[tIncludeFolder] ([ParentIncludeFolderId], [Title]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [template].[tIncludeFolder] ADD CONSTRAINT [FK_tIncludeFolder_ParentIncludeFolder] FOREIGN KEY ([ParentIncludeFolderId]) REFERENCES [template].[tIncludeFolder] ([IncludeFolderId])
GO
