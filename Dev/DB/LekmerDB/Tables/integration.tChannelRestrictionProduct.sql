CREATE TABLE [integration].[tChannelRestrictionProduct]
(
[ProductId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductRegistryId] [int] NOT NULL,
[State] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tChannelRestrictionProduct] ADD CONSTRAINT [PK__tChannel__0F50561A14145381] PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId]) ON [PRIMARY]
GO
