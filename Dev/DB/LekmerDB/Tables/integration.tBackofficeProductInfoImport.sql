CREATE TABLE [integration].[tBackofficeProductInfoImport]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[HYErpId] [nvarchar] (20) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ChannelNameISO] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UserName] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[InsertedDate] [smalldatetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tBackofficeProductInfoImport] ADD CONSTRAINT [PK_tBackofficeProductInfoImport] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
