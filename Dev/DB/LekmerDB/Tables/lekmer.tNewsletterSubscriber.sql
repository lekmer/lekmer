CREATE TABLE [lekmer].[tNewsletterSubscriber]
(
[SubscriberId] [int] NOT NULL IDENTITY(1, 1),
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ChannelId] [int] NOT NULL,
[SentStatus] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD CONSTRAINT [PK_tNewsletterSubscriber] PRIMARY KEY CLUSTERED  ([SubscriberId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_ChannelId] ON [lekmer].[tNewsletterSubscriber] ([ChannelId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD CONSTRAINT [FK_tNewsletterSubscriber_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
