CREATE TABLE [integration].[tProductsTranslatedInHY]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductsTranslatedInHY] ADD CONSTRAINT [PK__tProduct__1F9F439771BF3B7D] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
