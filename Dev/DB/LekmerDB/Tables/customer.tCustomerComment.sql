CREATE TABLE [customer].[tCustomerComment]
(
[CustomerCommentId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NOT NULL,
[Comment] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreationDate] [datetime] NOT NULL,
[AuthorId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerComment] ADD CONSTRAINT [PK_tCustomerComment] PRIMARY KEY CLUSTERED  ([CustomerCommentId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerComment] ADD CONSTRAINT [FK_tCustomerComment_tSystemUser] FOREIGN KEY ([AuthorId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
ALTER TABLE [customer].[tCustomerComment] WITH NOCHECK ADD CONSTRAINT [FK_tCustomerComment_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
