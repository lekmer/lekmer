CREATE TABLE [lekmer].[tBlockLatestProductAddedToBasket]
(
[BlockId] [int] NOT NULL,
[RedirectToStartPage] [bit] NULL,
[RedirectToMainCategory] [bit] NULL,
[RedirectToCategory] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockLatestProductAddedToBasket] ADD CONSTRAINT [PK_tBlockLatestProductAddedToBasket] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockLatestProductAddedToBasket] ADD CONSTRAINT [FK_tBlockLatestProductAddedToBasket_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
