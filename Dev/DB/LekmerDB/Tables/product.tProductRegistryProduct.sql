CREATE TABLE [product].[tProductRegistryProduct]
(
[ProductRegistryId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductRegistryProduct] ADD CONSTRAINT [PK_tProductRegistryProduct] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [ProductId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductRegistryProduct] ON [product].[tProductRegistryProduct] ([ProductId]) INCLUDE ([ProductRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductRegistryProduct] ADD CONSTRAINT [FK_tProductRegistryProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tProductRegistryProduct] ADD CONSTRAINT [FK_tProductRegistryProduct_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
