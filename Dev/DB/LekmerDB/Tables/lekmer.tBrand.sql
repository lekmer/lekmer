CREATE TABLE [lekmer].[tBrand]
(
[BrandId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[MediaId] [int] NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ExternalUrl] [varchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrand] ADD CONSTRAINT [PK_tBrand] PRIMARY KEY CLUSTERED  ([BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBrand] ADD CONSTRAINT [FK_tBrand_tImage] FOREIGN KEY ([MediaId]) REFERENCES [media].[tImage] ([MediaId]) ON DELETE SET NULL
GO
