CREATE TABLE [integration].[tBrandCreation]
(
[BrandId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tBrandCreation] ADD CONSTRAINT [PK__tBrandCr__DAD4F05E4F3417FF] PRIMARY KEY CLUSTERED  ([BrandId]) ON [PRIMARY]
GO
