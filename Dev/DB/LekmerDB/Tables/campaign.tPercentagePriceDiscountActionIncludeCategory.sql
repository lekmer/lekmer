CREATE TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory]
(
[ProductActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] ADD CONSTRAINT [PK_tPercentagePriceDiscountActionCategory] PRIMARY KEY CLUSTERED  ([ProductActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeCategory] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionCategory_tPercentagePriceDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tPercentagePriceDiscountAction] ([ProductActionId])
GO
