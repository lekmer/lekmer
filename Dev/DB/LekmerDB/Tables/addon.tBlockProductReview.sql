CREATE TABLE [addon].[tBlockProductReview]
(
[BlockId] [int] NOT NULL,
[RowCount] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockProductReview] ADD CONSTRAINT [PK_tBlockProductReview] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tBlockProductReview] ADD CONSTRAINT [FK_tBlockProductReview_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId]) ON DELETE CASCADE
GO
