CREATE TABLE [campaign].[tPercentagePriceDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tPercentagePriceDiscountAction_IncludeAllProducts] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [PK_tPercentagePriceDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
