CREATE TABLE [integration].[tDealOfTheDay]
(
[BlockId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Type] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Rownumber] [nchar] (10) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tDealOfTheDay] ADD CONSTRAINT [PK__tDealOfT__47CE2B702E29C09D] PRIMARY KEY CLUSTERED  ([BlockId], [ChannelId]) ON [PRIMARY]
GO
