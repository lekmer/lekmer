CREATE TABLE [product].[tVariationTranslation]
(
[VariationId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationTranslation] ADD CONSTRAINT [PK_tVariationTranslation] PRIMARY KEY CLUSTERED  ([VariationId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationTranslation] ADD CONSTRAINT [FK_tVariationTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [product].[tVariationTranslation] ADD CONSTRAINT [FK_tVariationTranslation_tVariation] FOREIGN KEY ([VariationId]) REFERENCES [product].[tVariation] ([VariationId])
GO
