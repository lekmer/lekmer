CREATE TABLE [campaign].[tConditionType]
(
[ConditionTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tConditionType] ADD CONSTRAINT [PK_tConditionType] PRIMARY KEY CLUSTERED  ([ConditionTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tConditionType_CommonName] ON [campaign].[tConditionType] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tConditionType_Title] ON [campaign].[tConditionType] ([Title]) ON [PRIMARY]
GO
