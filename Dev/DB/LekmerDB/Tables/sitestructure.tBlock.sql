CREATE TABLE [sitestructure].[tBlock]
(
[BlockId] [int] NOT NULL IDENTITY(1, 1),
[ContentNodeId] [int] NOT NULL,
[BlockTypeId] [int] NOT NULL,
[BlockStatusId] [int] NOT NULL,
[ContentAreaId] [int] NOT NULL,
[Title] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL,
[AccessId] [int] NOT NULL CONSTRAINT [DF_tBlock_AccessId] DEFAULT ((1)),
[TemplateId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [PK_tBlock] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlock_AccessId] ON [sitestructure].[tBlock] ([AccessId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlock_tBlockStatus] ON [sitestructure].[tBlock] ([BlockStatusId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlock_tBlockType] ON [sitestructure].[tBlock] ([BlockTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlock_tContentArea] ON [sitestructure].[tBlock] ([ContentAreaId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlock_tContentPage] ON [sitestructure].[tBlock] ([ContentNodeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Block_ContentNodeId_ContentAreaId_Title] ON [sitestructure].[tBlock] ([ContentNodeId], [ContentAreaId], [Title]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [FK_tBlock_tAccess] FOREIGN KEY ([AccessId]) REFERENCES [sitestructure].[tAccess] ([AccessId])
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [FK_tBlock_tBlockStatus] FOREIGN KEY ([BlockStatusId]) REFERENCES [sitestructure].[tBlockStatus] ([BlockStatusId])
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [FK_tBlock_tBlockType] FOREIGN KEY ([BlockTypeId]) REFERENCES [sitestructure].[tBlockType] ([BlockTypeId])
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [FK_tBlock_tContentArea] FOREIGN KEY ([ContentAreaId]) REFERENCES [sitestructure].[tContentArea] ([ContentAreaId])
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [FK_tBlock_tContentPage] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
ALTER TABLE [sitestructure].[tBlock] ADD CONSTRAINT [FK_tBlock_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
