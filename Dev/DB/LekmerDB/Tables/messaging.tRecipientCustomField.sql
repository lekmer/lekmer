CREATE TABLE [messaging].[tRecipientCustomField]
(
[RecipientId] [uniqueidentifier] NOT NULL,
[Name] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Value] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [messaging].[tRecipientCustomField] ADD CONSTRAINT [PK_tRecipientCustomField] PRIMARY KEY NONCLUSTERED  ([RecipientId], [Name]) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tRecipientCustomField] ADD CONSTRAINT [FK_tRecipientCustomField_tRecipient] FOREIGN KEY ([RecipientId]) REFERENCES [messaging].[tRecipient] ([RecipientId])
GO
