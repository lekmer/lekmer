CREATE TABLE [integration].[tLekmerOldUrl]
(
[HYErpId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[OldUrl] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
