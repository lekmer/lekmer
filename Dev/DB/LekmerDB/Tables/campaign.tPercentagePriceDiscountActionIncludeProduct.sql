CREATE TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] ADD CONSTRAINT [PK_tPercentagePriceDiscountActionProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionProduct_tPercentagePriceDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tPercentagePriceDiscountAction] ([ProductActionId])
GO
ALTER TABLE [campaign].[tPercentagePriceDiscountActionIncludeProduct] ADD CONSTRAINT [FK_tPercentagePriceDiscountActionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
