CREATE TABLE [lekmer].[tProductDiscountActionItem]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[DiscountPrice] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD CONSTRAINT [PK_tProductDiscountActionItem] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD CONSTRAINT [FK_tProductDiscountActionItem_tProductDiscountAction] FOREIGN KEY ([ProductActionId]) REFERENCES [lekmer].[tProductDiscountAction] ([ProductActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tProductDiscountActionItem] ADD CONSTRAINT [FK_tProductDiscountActionItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
