CREATE TABLE [lekmer].[tVoucherBatches]
(
[ConditionId] [int] NOT NULL,
[Batchid] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherBatches] ADD CONSTRAINT [PK_tVoucherBatches_1] PRIMARY KEY CLUSTERED  ([ConditionId], [Batchid]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherBatches] ADD CONSTRAINT [FK_tVoucherBatches_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
