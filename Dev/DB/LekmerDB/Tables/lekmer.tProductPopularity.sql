CREATE TABLE [lekmer].[tProductPopularity]
(
[ChannelId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Popularity] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [PK_tProductPopularity] PRIMARY KEY CLUSTERED  ([ChannelId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [FK_tProductPopularity_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [lekmer].[tProductPopularity] ADD CONSTRAINT [FK_tProductPopularity_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
