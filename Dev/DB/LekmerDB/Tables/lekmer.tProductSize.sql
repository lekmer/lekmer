CREATE TABLE [lekmer].[tProductSize]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NumberInStock] [int] NOT NULL,
[MillimeterDeviation] [int] NULL,
[OverrideEU] [decimal] (3, 1) NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [PK_tProductSize] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId])
GO
ALTER TABLE [lekmer].[tProductSize] ADD CONSTRAINT [FK_tProductSize_tSize] FOREIGN KEY ([SizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
