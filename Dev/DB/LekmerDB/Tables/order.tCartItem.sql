CREATE TABLE [order].[tCartItem]
(
[CartId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_tCartItem_Quantity] DEFAULT ((1)),
[CreatedDate] [datetime] NOT NULL,
[CartItemId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [PK_tCartItem] PRIMARY KEY CLUSTERED  ([CartItemId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCartItem] ON [order].[tCartItem] ([CartId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [FK_tCartItem_tCart] FOREIGN KEY ([CartId]) REFERENCES [order].[tCart] ([CartId])
GO
ALTER TABLE [order].[tCartItem] ADD CONSTRAINT [FK_tCartItem_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'The active cart created in a user session.', 'SCHEMA', N'order', 'TABLE', N'tCartItem', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', N'Number of products of this item.', 'SCHEMA', N'order', 'TABLE', N'tCartItem', 'COLUMN', N'Quantity'
GO
