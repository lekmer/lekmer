CREATE TABLE [integration].[tProductRegistryRestrictions]
(
[ProductId] [int] NOT NULL,
[ProductRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductRegistryRestrictions] ADD CONSTRAINT [PK__tProduct__0F50561A46D4DD78] PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId]) ON [PRIMARY]
GO
