CREATE TABLE [product].[tVariationGroupProduct]
(
[VariationGroupId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationGroupProduct] ADD CONSTRAINT [PK_tVariationGroupProduct] PRIMARY KEY CLUSTERED  ([VariationGroupId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationGroupProduct] ADD CONSTRAINT [FK_tVariationGroupProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tVariationGroupProduct] ADD CONSTRAINT [FK_tVariationGroupProduct_tVariationGroup] FOREIGN KEY ([VariationGroupId]) REFERENCES [product].[tVariationGroup] ([VariationGroupId])
GO
