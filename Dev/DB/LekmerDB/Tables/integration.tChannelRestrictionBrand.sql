CREATE TABLE [integration].[tChannelRestrictionBrand]
(
[BrandId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tChannelRestrictionBrand] ADD CONSTRAINT [PK__tChannel__618860890C7331B9] PRIMARY KEY CLUSTERED  ([BrandId], [ProductRegistryId]) ON [PRIMARY]
GO
