CREATE TABLE [integration].[tAccessoriesSize]
(
[AccessoriesSizeId] [int] NOT NULL,
[AccessoriesSize] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AccessoriesSizeErpId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tAccessoriesSize] ADD CONSTRAINT [PK__tAccesso__B0A650D318D9089E] PRIMARY KEY CLUSTERED  ([AccessoriesSizeId]) ON [PRIMARY]
GO
