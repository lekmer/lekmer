CREATE TABLE [lekmer].[tImageRotatorGroup]
(
[ImageGroupId] [int] NOT NULL IDENTITY(1, 1),
[BlockId] [int] NOT NULL,
[MainImageId] [int] NULL,
[ThumbnailImageId] [int] NULL,
[InternalLinkContentNodeId] [int] NULL,
[ExternalLink] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[IsMovie] [bit] NULL,
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[HtmalMarkup] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[InternalLinkContentNodeId2] [int] NULL,
[ExternalLink2] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[IsMovie2] [bit] NULL,
[InternalLinkContentNodeId3] [int] NULL,
[ExternalLink3] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[IsMovie3] [bit] NULL,
[Ordinal] [int] NULL,
[StartDateTime] [datetime] NULL,
[EndDateTime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tImageRotatorGroup] ADD CONSTRAINT [PK_tImageRotatorGroup] PRIMARY KEY CLUSTERED  ([ImageGroupId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tImageRotatorGroup] ADD CONSTRAINT [FK_tImageRotatorGroup_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
