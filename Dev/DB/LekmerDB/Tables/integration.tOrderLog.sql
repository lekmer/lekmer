CREATE TABLE [integration].[tOrderLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[OrderId] [int] NOT NULL,
[OrderXmlIn] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[OrderXmlOut] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL,
[CustomerId] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[tOrderLog] ADD CONSTRAINT [PK__tOrderLo__3214EC0723F08164] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
