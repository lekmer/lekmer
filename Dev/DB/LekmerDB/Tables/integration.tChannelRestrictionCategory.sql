CREATE TABLE [integration].[tChannelRestrictionCategory]
(
[CategoryId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tChannelRestrictionCategory] ADD CONSTRAINT [PK__tChannel__A255AADC1043C29D] PRIMARY KEY CLUSTERED  ([CategoryId], [ProductRegistryId]) ON [PRIMARY]
GO
