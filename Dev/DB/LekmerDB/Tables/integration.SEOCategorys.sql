CREATE TABLE [integration].[SEOCategorys]
(
[ContentNodeId] [int] NOT NULL,
[SeoTitle] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[SeoDescription] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[SEOCategorys] ADD CONSTRAINT [PK__SEOCateg__BE27E945533AB45D] PRIMARY KEY CLUSTERED  ([ContentNodeId]) ON [PRIMARY]
GO
