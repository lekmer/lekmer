CREATE TABLE [security].[tSystemUser]
(
[SystemUserId] [int] NOT NULL IDENTITY(1, 1),
[Username] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Password] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[FailedSigninCounter] [int] NOT NULL CONSTRAINT [DF_tSystemUser_FailedLoginCounter] DEFAULT ((0)),
[ExpirationDate] [smalldatetime] NULL CONSTRAINT [DF_tSystemUser_PasswordExpirationDate] DEFAULT (dateadd(day,(30),getdate())),
[ActivationDate] [smalldatetime] NULL CONSTRAINT [DF_tSystemUser_AccountCreateDateTime] DEFAULT (getdate()),
[StatusId] [int] NOT NULL CONSTRAINT [DF_tSystemUser_SystemUserStatusId] DEFAULT ((1)),
[Name] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CultureId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [security].[tSystemUser] ADD CONSTRAINT [PK_tSystemUser] PRIMARY KEY CLUSTERED  ([SystemUserId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tSystemUser_CultureId] ON [security].[tSystemUser] ([CultureId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tSystemUser] ON [security].[tSystemUser] ([Username]) ON [PRIMARY]
GO
ALTER TABLE [security].[tSystemUser] ADD CONSTRAINT [FK_tSystemUser_tCulture] FOREIGN KEY ([CultureId]) REFERENCES [core].[tCulture] ([CultureId])
GO
ALTER TABLE [security].[tSystemUser] ADD CONSTRAINT [FK_tSystemUser_tSystemUserStatus] FOREIGN KEY ([StatusId]) REFERENCES [security].[tSystemUserStatus] ([SystemUserStatusId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Expiration date of this system user account. Defaults to creation date + 30 days.', 'SCHEMA', N'security', 'TABLE', N'tSystemUser', 'COLUMN', N'ExpirationDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Counts every failed login for the user. Sets to zero at successful login.', 'SCHEMA', N'security', 'TABLE', N'tSystemUser', 'COLUMN', N'FailedSigninCounter'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Oneway hash encrypted by applikation layer. MD5(PasswordRandom + Password)', 'SCHEMA', N'security', 'TABLE', N'tSystemUser', 'COLUMN', N'Password'
GO
EXEC sp_addextendedproperty N'MS_Description', N'The status of the system user.', 'SCHEMA', N'security', 'TABLE', N'tSystemUser', 'COLUMN', N'StatusId'
GO
