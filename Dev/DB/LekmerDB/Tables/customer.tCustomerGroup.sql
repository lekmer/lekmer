CREATE TABLE [customer].[tCustomerGroup]
(
[CustomerGroupId] [int] NOT NULL IDENTITY(1, 1),
[CustomerGroupFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StatusId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroup] ADD CONSTRAINT [PK_tCustomerGroup] PRIMARY KEY CLUSTERED  ([CustomerGroupId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomerGroup_FolderId] ON [customer].[tCustomerGroup] ([CustomerGroupFolderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tCustomerGroup_StatusId] ON [customer].[tCustomerGroup] ([StatusId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroup] ADD CONSTRAINT [FK_tCustomerGroup_tCustomerGroupFolder] FOREIGN KEY ([CustomerGroupFolderId]) REFERENCES [customer].[tCustomerGroupFolder] ([CustomerGroupFolderId])
GO
ALTER TABLE [customer].[tCustomerGroup] ADD CONSTRAINT [FK_tCustomerGroup_tCustomerGroupStatus] FOREIGN KEY ([StatusId]) REFERENCES [customer].[tCustomerGroupStatus] ([CustomerGroupStatusId])
GO
