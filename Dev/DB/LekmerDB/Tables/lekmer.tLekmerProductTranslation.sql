CREATE TABLE [lekmer].[tLekmerProductTranslation]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Measurement] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] ADD CONSTRAINT [PK_tLekmerProductTranslation] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] ADD CONSTRAINT [FK_tLekmerProductTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tLekmerProductTranslation] ADD CONSTRAINT [FK_tLekmerProductTranslation_tLekmerProduct] FOREIGN KEY ([ProductId]) REFERENCES [lekmer].[tLekmerProduct] ([ProductId]) ON DELETE CASCADE
GO
