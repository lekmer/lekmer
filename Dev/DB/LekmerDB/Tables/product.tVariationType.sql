CREATE TABLE [product].[tVariationType]
(
[VariationGroupId] [int] NOT NULL,
[VariationTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationType] ADD CONSTRAINT [PK_tVariationType] PRIMARY KEY CLUSTERED  ([VariationGroupId], [VariationTypeId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationType] ADD CONSTRAINT [UQ_tVariationType] UNIQUE NONCLUSTERED  ([VariationTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tVariationType_VariationGroupId_Title] ON [product].[tVariationType] ([VariationGroupId], [Title]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariationType] ADD CONSTRAINT [FK_tVariationType_tVariationGroup] FOREIGN KEY ([VariationGroupId]) REFERENCES [product].[tVariationGroup] ([VariationGroupId])
GO
