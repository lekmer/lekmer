CREATE TABLE [integration].[OrderLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[CustomerNo] [int] NOT NULL,
[OrderNo] [int] NOT NULL,
[OrderXmlIn] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[OrderXmlOut] [text] COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL,
[Messagge] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[OrderLog] ADD CONSTRAINT [PK__OrderLog__3214EC073337F91F] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
