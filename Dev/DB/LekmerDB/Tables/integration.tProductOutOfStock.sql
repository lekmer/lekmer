CREATE TABLE [integration].[tProductOutOfStock]
(
[ProductId] [int] NOT NULL,
[SizeId] [int] NOT NULL,
[OutOfStockDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tProductOutOfStock] ADD CONSTRAINT [PK__tProduct__0C37165A635B0C3D] PRIMARY KEY CLUSTERED  ([ProductId], [SizeId]) ON [PRIMARY]
GO
