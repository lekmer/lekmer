CREATE TABLE [product].[tProductTranslation]
(
[ProductId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[WebShopTitle] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ShortDescription] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [product].[tProductTranslation] ADD CONSTRAINT [PK_tProductTranslation] PRIMARY KEY CLUSTERED  ([ProductId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductTranslation] ADD CONSTRAINT [FK_tProductTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
ALTER TABLE [product].[tProductTranslation] ADD CONSTRAINT [FK_tProductTranslation_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
