CREATE TABLE [integration].[tProductTranslationDescriptionDK]
(
[HYId] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
