CREATE TABLE [lekmer].[tTag]
(
[TagId] [int] NOT NULL IDENTITY(1, 1),
[TagGroupId] [int] NOT NULL,
[Value] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTag] ADD CONSTRAINT [PK_tTag] PRIMARY KEY CLUSTERED  ([TagId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tTag_TagGroupId] ON [lekmer].[tTag] ([TagGroupId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTag] ADD CONSTRAINT [FK_tTag_tTagGroup] FOREIGN KEY ([TagGroupId]) REFERENCES [lekmer].[tTagGroup] ([TagGroupId])
GO
