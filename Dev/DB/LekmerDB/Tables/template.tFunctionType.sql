CREATE TABLE [template].[tFunctionType]
(
[FunctionTypeId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tFunctionType] ADD CONSTRAINT [PK_tFunctionType] PRIMARY KEY CLUSTERED  ([FunctionTypeId]) ON [PRIMARY]
GO
