CREATE TABLE [customer].[tCustomerGroupCustomer]
(
[CustomerGroupId] [int] NOT NULL,
[CustomerId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] ADD CONSTRAINT [PK_tCustomerGroupCustomer] PRIMARY KEY CLUSTERED  ([CustomerGroupId], [CustomerId]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] ADD CONSTRAINT [FK_tCustomerGroupCustomer_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId])
GO
ALTER TABLE [customer].[tCustomerGroupCustomer] WITH NOCHECK ADD CONSTRAINT [FK_tCustomerGroupCustomer_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
