CREATE TABLE [lekmer].[tTagGroup]
(
[TagGroupId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tTagGroup] ADD CONSTRAINT [PK_tTagGroup] PRIMARY KEY CLUSTERED  ([TagGroupId]) ON [PRIMARY]
GO
