CREATE TABLE [template].[tTemplate]
(
[TemplateId] [int] NOT NULL IDENTITY(1, 1),
[ModelId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[UseAlternate] [bit] NOT NULL,
[ImageFileName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplate] ADD CONSTRAINT [PK_tTemplate] PRIMARY KEY CLUSTERED  ([TemplateId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tTemplate_ModelId_Title] ON [template].[tTemplate] ([ModelId], [Title]) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplate] ADD CONSTRAINT [FK_tTemplate_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
