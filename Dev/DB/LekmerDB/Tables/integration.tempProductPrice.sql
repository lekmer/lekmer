CREATE TABLE [integration].[tempProductPrice]
(
[HYarticleId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Price] [decimal] (18, 0) NULL,
[PriceText] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceGroup] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tempProductPrice] ADD CONSTRAINT [PK__tempProd__ED0627FB3AD91AE7] PRIMARY KEY CLUSTERED  ([HYarticleId]) ON [PRIMARY]
GO
