CREATE TABLE [template].[tModelSetting]
(
[ModelSettingId] [int] NOT NULL IDENTITY(1, 1),
[ModelId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ModelSettingTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelSetting] ADD CONSTRAINT [PK_tModelSetting] PRIMARY KEY CLUSTERED  ([ModelSettingId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModelSetting_ModelId_CommonName] ON [template].[tModelSetting] ([ModelId], [CommonName]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModelSetting_ModelId_Title] ON [template].[tModelSetting] ([ModelId], [Title]) WITH (ALLOW_PAGE_LOCKS=OFF) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tModelSetting_ModelSettingTypeId] ON [template].[tModelSetting] ([ModelSettingTypeId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelSetting] ADD CONSTRAINT [FK_tModelSetting_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
ALTER TABLE [template].[tModelSetting] ADD CONSTRAINT [FK_tModelSetting_tModelSettingType] FOREIGN KEY ([ModelSettingTypeId]) REFERENCES [template].[tModelSettingType] ([ModelSettingTypeId])
GO
