CREATE TABLE [sitestructure].[tContentPageSeoSetting]
(
[ContentNodeId] [int] NOT NULL,
[Title] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[Description] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[Keywords] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ADD CONSTRAINT [PK_tContentPageSeoSettings] PRIMARY KEY CLUSTERED  ([ContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentPageSeoSetting] ADD CONSTRAINT [FK_tContentPageSeoSettings_tContentPage] FOREIGN KEY ([ContentNodeId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
