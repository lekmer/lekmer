CREATE TABLE [core].[tCurrency]
(
[CurrencyId] [int] NOT NULL IDENTITY(1, 1),
[ISO] [char] (3) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (64) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[PriceFormat] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NumberOfDecimals] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [core].[tCurrency] ADD CONSTRAINT [PK_tCurrency] PRIMARY KEY CLUSTERED  ([CurrencyId]) ON [PRIMARY]
GO
