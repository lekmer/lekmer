CREATE TABLE [order].[tOrder]
(
[OrderId] [int] NOT NULL IDENTITY(1, 1),
[Number] [nvarchar] (30) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CustomerId] [int] NOT NULL,
[BillingAddressId] [int] NOT NULL,
[DeliveryAddressId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[FreightCost] [decimal] (16, 2) NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NULL,
[DeliveryMethodId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[OrderStatusId] [int] NOT NULL,
[UsedAlternate] [bit] NOT NULL,
[IP] [varchar] (15) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DeliveryTrackingId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [CK_tOrder_FreightCost] CHECK (([FreightCost]>=(0)))
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [PK_tOrder] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_BillingAddressId] ON [order].[tOrder] ([BillingAddressId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_ChannelId] ON [order].[tOrder] ([ChannelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_CustomerId] ON [order].[tOrder] ([CustomerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_DeliveryAddressId] ON [order].[tOrder] ([DeliveryAddressId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_DeliveryMethodId] ON [order].[tOrder] ([DeliveryMethodId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tOrder_Number] ON [order].[tOrder] ([Number]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrder_OrderStatusId] ON [order].[tOrder] ([OrderStatusId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [FK_tOrder_tOrderBillingAddress] FOREIGN KEY ([BillingAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId])
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [FK_tOrder_tChannel1] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [FK_tOrder_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomerInformation] ([CustomerId])
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [FK_tOrder_tOrderDeliveryAddress] FOREIGN KEY ([DeliveryAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId])
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [FK_tOrder_DeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
ALTER TABLE [order].[tOrder] ADD CONSTRAINT [FK_tOrder_tOrderStatus] FOREIGN KEY ([OrderStatusId]) REFERENCES [order].[tOrderStatus] ([OrderStatusId])
GO
