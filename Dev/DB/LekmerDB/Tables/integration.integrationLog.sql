CREATE TABLE [integration].[integrationLog]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Data] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Message] [varchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[Date] [datetime] NOT NULL,
[OcuredInProcedure] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [integration].[integrationLog] ADD CONSTRAINT [PK__integrat__3214EC0737088A03] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
