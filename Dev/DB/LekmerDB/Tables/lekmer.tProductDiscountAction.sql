CREATE TABLE [lekmer].[tProductDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductDiscountAction] ADD CONSTRAINT [PK_tProductDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductDiscountAction] ADD CONSTRAINT [FK_tProductDiscountAction_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tProductDiscountAction] ADD CONSTRAINT [FK_tProductDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId]) ON DELETE CASCADE
GO
