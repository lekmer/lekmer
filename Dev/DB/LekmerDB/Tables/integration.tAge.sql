CREATE TABLE [integration].[tAge]
(
[HyId] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AgeFromMonth] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL,
[AgeToMonth] [nvarchar] (250) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tAge] ADD CONSTRAINT [PK__tAge__67B311717D30EE29] PRIMARY KEY CLUSTERED  ([HyId]) ON [PRIMARY]
GO
