CREATE TABLE [sitestructure].[tContentNodeStatus]
(
[ContentNodeStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeStatus] ADD CONSTRAINT [PK_tContentNodeStatus] PRIMARY KEY CLUSTERED  ([ContentNodeStatusId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tContentNodeStatus] ADD CONSTRAINT [UQ_tContentNodeStatus_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
