SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductTranslationsDiffFI]

AS
begin
	set nocount on
					
		select
			l.HYErpId, 
			p.Title as 'SE Title', 
			p.[Description] as 'SE Description', 
			pt.Title as 'FI Title', 
			pt.[Description] as 'FI Description'
		from 
			lekmer.tLekmerProduct l with (nolock)
			inner join product.tProduct p with (nolock)
				on l.ProductId = p.ProductId
			left join product.tProductTranslation pt with (nolock)
				on p.ProductId = pt.ProductId
			left join integration.tChannelRestrictionProduct i with (nolock)
				on l.ProductId = i.ProductId
				and i.ProductRegistryId = 4
				and i.[State] = 'Manual'
		where 
			p.Title != ''
			and p.Description != ''
			and pt.LanguageId = 1000003
			and (pt.Title is null
					or  pt.Description is null)
			and i.ProductId is null
		order by
			l.HYErpId					 
end
GO
