SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionIncludeCategoryGetIdAllSecure]
	@ActionId		int
AS
BEGIN
	SELECT 
		c.CategoryId
	FROM 
		campaign.tPercentagePriceDiscountActionIncludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.ProductActionId = @ActionId
END
GO
