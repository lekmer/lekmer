SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportOnlineProducts]

AS
begin
	set nocount on

	select
		x.SizeErpId,
		x.Title,
		x.NumberInStock,
		(case when sum(x.SE) > 0 then 'x' else '' end) as 'SE',
		(case when sum(x.[NO]) > 0 then 'x' else '' end) as 'NO',
		(case when sum(x.DK) > 0 then 'x' else '' end) as 'DK',
		(case when sum(x.FI) > 0 then 'x' else '' end) as 'FI'
	from
		(
		select
			isnull(s.ErpId, l.HYErpId + '-000') as SizeErpId,
			p.Title,
			coalesce(s.NumberInStock, p.NumberInStock) as NumberInStock,
			case when r.ProductRegistryId = 1 then 1 end as 'SE',
			case when r.ProductRegistryId = 2 then 1 end as 'NO', 
			case when r.ProductRegistryId = 3 then 1 end as 'DK',
			case when r.ProductRegistryId = 4 then 1  end as 'FI' 
		from 
			lekmer.tLekmerProduct l with (nolock)
			inner join product.tProduct p with (nolock)
				on l.ProductId = p.ProductId
			left join lekmer.tProductSize s with (nolock)
				on s.ProductId = p.ProductId 
			inner join product.tProductRegistryProduct r with (nolock)
				on r.ProductId = p.ProductId
		where
			p.ProductStatusId = 0
			and (p.NumberInStock > 0 and s.NumberInStock is null
									or (s.NumberInStock > 0))
		) x
	group by
	    x.SizeErpId,
		x.Title,
		x.NumberInStock
	order by
		x.Title,
	    x.SizeErpId,		
		x.NumberInStock
	
end
GO
