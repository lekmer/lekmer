SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_FokRestrictionsProducts]

AS
begin
	set nocount on
	begin try
		begin transaction
		-------------------------
		--- Lekmer NO
		-------------------------
		-- Delete all from NO
		-- Insert all new articles into the translation table
		insert into product.tProductTranslation(ProductId, LanguageId)
		select
			p.productid,
			1000001
		from	
			product.tProduct p
		where
			p.ProductId not in
								(
									select productid from product.tProductTranslation
									where LanguageId = 1000001
								)
		-- Delete all restrictions (on products) for NO, change later to only delete changes!!!				
		delete from integration.tProductRegistryRestrictions
		where ProductRegistryId = 2 or ProductRegistryId = 3 or ProductRegistryId =4
		
		-- Insert all products (NO) that are not translated and therefor not shown on NO site
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			ProductId,
			2
		from	
			product.tProductTranslation
		where 
			LanguageId = 1000001 -- NO
			and Title is null
		
		-- Build all the productRegistrys first!
		insert into product.tProductRegistryProduct(ProductId, ProductRegistryId)
		select
			pt.productid,
			2
		from 
			product.tProduct pt
		where
			pt.ProductId not in
								(
									select ProductId
									from product.tProductRegistryProduct 
									where ProductRegistryId = 2	
								)						
		
		
		
		-------------------------
		--- Lekmer DK
		-------------------------	
		insert into product.tProductTranslation(ProductId, LanguageId)
		select
			p.productid,
			1000002
		from	
			product.tProduct p
		where
			p.ProductId not in
								(
									select productid from product.tProductTranslation
									where LanguageId = 1000002
								)
								
		-- Insert all products (DK) that are not translated and therefor not shown on FI site
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			ProductId,
			3
		from	
			product.tProductTranslation
		where 
			LanguageId = 1000002 -- DK
			and Title is null
			
		-- Build all the productRegistrys first!
		insert into product.tProductRegistryProduct(ProductId, ProductRegistryId)
		select
			pt.productid,
			3
		from 
			product.tProduct pt
		where
			pt.ProductId not in
								(
									select ProductId
									from product.tProductRegistryProduct 
									where ProductRegistryId = 3	
								)
			
		
		-------------------------
		--- Lekmer FI
		-------------------------	
		-- Insert all new articles into the translation table
		insert into product.tProductTranslation(ProductId, LanguageId)
		select
			p.productid,
			1000003
		from	
			product.tProduct p
		where
			p.ProductId not in
								(
									select productid from product.tProductTranslation
									where LanguageId = 1000003
								)
		
		-- Insert all products (FI) that are not translated and therefor not shown on FI site
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			ProductId,
			4
		from	
			product.tProductTranslation
		where 
			LanguageId = 1000003 -- FI
			and Title is null

		-- Build all the productRegistrys first!
		insert into product.tProductRegistryProduct(ProductId, ProductRegistryId)
		select
			pt.productid,
			4
		from 
			product.tProduct pt
		where
			pt.ProductId not in
								(
									select ProductId
									from product.tProductRegistryProduct 
									where ProductRegistryId = 4	
								)
								
		
		------------ BÖCKER ALLA KANALER (NO, DK, FI) ------------
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			2
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						select CategoryId from product.tCategory
						where ParentCategoryId = 1000842  --böcker och tidningar  -- C_30-119  -- 1000842
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 2
							)
			
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			3
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						select CategoryId from product.tCategory
						where ParentCategoryId = 1000842  --böcker och tidningar  -- C_30-119  -- 1000842
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 3
							)
		
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			4
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						select CategoryId from product.tCategory
						where ParentCategoryId = 1000842  --böcker och tidningar  -- C_30-119  -- 1000842
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 4
							)
		
		
		------------ FILMER ALLA KANALER (NO, DK, FI) ------------
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			2
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						select CategoryId from product.tCategory
						where ParentCategoryId = 1000985  --böcker och tidningar  -- C_30-124  -- 1000985
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 2
							)
			
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			3
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						select CategoryId from product.tCategory
						where ParentCategoryId = 1000985  --böcker och tidningar  -- C_30-124  -- 1000985
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 3
							)
		
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			4
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						select CategoryId from product.tCategory
						where ParentCategoryId = 1000985  --böcker och tidningar  -- C_30-124  -- 1000985
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 4
							)
					
		------------ BRANDS ALLA KANALER (NO, DK, FI) ------------
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			2
		from	
			(select productid
			from product.tProduct 
			where ProductId in
					(
						select productid from lekmer.tLekmerProduct
						where BrandId = 1000232 -- 'Maxi-Cosi'
						or BrandId = 1000235 -- 'Quinny'
						or BrandId = 1000347 -- 'Quinny'
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 2
							)
			
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			3
		from	
			(select productid
			from product.tProduct 
			where ProductId in
					(
						select productid from lekmer.tLekmerProduct
						where BrandId = 1000232 -- 'Maxi-Cosi'
						or BrandId = 1000235 -- 'Quinny'
						or BrandId = 1000347 -- 'Quinny'
						or BrandId = 1000357 -- 'Flexibath'
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 3
							)
		
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			4
		from	
			(select productid
			from product.tProduct 
			where ProductId in
					(
						select productid from lekmer.tLekmerProduct
						where BrandId = 1000232 -- 'Maxi-Cosi'
						or BrandId = 1000235 -- 'Quinny'
						or BrandId = 1000347 -- 'Quinny'
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 4
							)		
		
		
		------------ SPEL/SÄLLSKAPSPEL/BARNSPEL ALLA KANALER (NO, DK, FI) ------------
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			2
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						'1000590',	--Barnspel
						'1000989',	--Vuxenspel
						'1000996',	--Familjespel
						'1000997',	--Tv-spel och datorspel
						'1000999',	--Utomhusspel
						'1001000',	--Strategispel
						'1001001',	--Pocketspel
						'1001002',	--Klassiker
						--'1001003',	--Kortspel
						--'1001004',	--Hockeyspel
						'1001005',	--Resespel
						'1001006',	--Memoryspel
						'1001008',	--Brädspel
						'1001009',	--Barnpussel
						'1001010',	--Startegispel
						'1001014'	--Strategispel
						--'1001094'	--Fotboll
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 2
							)

		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			3
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						'1000590',	--Barnspel
						'1000989',	--Vuxenspel
						'1000996',	--Familjespel
						'1000997',	--Tv-spel och datorspel
						'1000999',	--Utomhusspel
						'1001000',	--Strategispel
						'1001001',	--Pocketspel
						'1001002',	--Klassiker
						--'1001003',	--Kortspel
						--'1001004',	--Hockeyspel
						'1001005',	--Resespel
						'1001006',	--Memoryspel
						'1001008',	--Brädspel
						'1001009',	--Barnpussel
						'1001010',	--Startegispel
						'1001014'	--Strategispel
						--'1001094'	--Fotboll
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 3
							)
		
		insert into	integration.[tProductRegistryRestrictions](ProductId, ProductRegistryId)
		select
			t.ProductId,
			4
		from	
			(select productid
			from product.tProduct 
			where CategoryId in
					(
						'1000590',	--Barnspel
						'1000989',	--Vuxenspel
						'1000996',	--Familjespel
						'1000997',	--Tv-spel och datorspel
						'1000999',	--Utomhusspel
						'1001000',	--Strategispel
						'1001001',	--Pocketspel
						'1001002',	--Klassiker
						--'1001003',	--Kortspel
						--'1001004',	--Hockeyspel
						'1001005',	--Resespel
						'1001006',	--Memoryspel
						'1001008',	--Brädspel
						'1001009',	--Barnpussel
						'1001010',	--Startegispel
						'1001014'	--Strategispel
						--'1001094'	--Fotboll
					)) t
		where 
			t.ProductId not in
							(
								select ProductId from integration.tProductRegistryRestrictions
								where ProductRegistryId = 4
							)	
		
																
		---------------------------------------------
		-- Delete from productRegistry for NO,DK,FI
		delete 
			prp
		from 
			product.tProductRegistryProduct prp 
			inner join integration.tProductRegistryRestrictions i
				on prp.ProductId = i.ProductId
				and prp.ProductRegistryId = i.ProductRegistryId
	
				
	commit transaction
	end try
	begin catch

		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

	end catch			
end
GO
