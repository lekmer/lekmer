SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartActionSave]
	@CartActionId		int,
	@CartActionTypeId	int,
	@CampaignId			int,
	@Ordinal			int
AS
BEGIN
	UPDATE
		campaign.tCartAction
	SET 
		CartActionTypeId = @CartActionTypeId,
		CampaignId = @CampaignId,
		Ordinal = @Ordinal
	WHERE 
		CartActionId = @CartActionId
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			campaign.tCartAction
		( 
			CartActionTypeId, 
			CampaignId, 
			Ordinal
		)
		VALUES 
		(
			@CartActionTypeId,
			@CampaignId,
			@Ordinal
		)
		SET @CartActionId = cast(scope_identity() AS int)
	END
	RETURN @CartActionId
END 
GO
