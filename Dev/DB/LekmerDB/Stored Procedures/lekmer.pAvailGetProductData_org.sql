SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [lekmer].[pAvailGetProductData_org] 
AS
BEGIN
	SET NOCOUNT ON;

SELECT     ProductId, 
		   product.fGetPriceByPriceListCommonName(ProductId,'Sweden') as PriceSE,
		   product.fGetPriceByPriceListCommonName(ProductId,'Denmark') as PriceDK,
		   product.fGetPriceByPriceListCommonName(ProductId,'Norway') as PriceNO,
		   product.fGetPriceByPriceListCommonName(ProductId,'Finland') as PriceFI,
		   NumberInStock
FROM       product.tProduct

END



GO
