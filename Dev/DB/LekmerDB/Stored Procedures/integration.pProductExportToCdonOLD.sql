SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductExportToCdonOLD]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set transaction isolation level read uncommitted
	
	declare @Tree table (
		CategoryId int primary key with (ignore_dup_key = on),
		Depth int,
		HasChildren bit
	)
	
	declare @Channel table (
		ChannelId int primary key
	)
	
	insert @Channel(ChannelId)
   values(1), (2), (3), (4)
		
   select ch.ChannelId
         ,ch.Title as Name
         ,replace(ch.Title, 'Lekmer.', '') as Tld
         ,cu.ISO as CurrencyCode
         ,cu.CurrencyId
     from @Channel c
          inner join core.tChannel ch
                  on ch.ChannelId = c.ChannelId
          inner join core.tCurrency cu
                  on cu.CurrencyId = ch.CurrencyId

	
	insert into @Tree values (1000533, 0, 1)	-- Root id for toys.
	
	declare @depth int
	set @depth = 0
	while (@@ROWCOUNT > 0)
	begin
		set @depth = @depth + 1
		
		insert into @Tree
		select c.CategoryId, @depth, 1
		from @Tree t 
			inner join product.tCategory c on c.ParentCategoryId = t.CategoryId
	end
	
	
	---- HACK TO ASSURE WE CAN IMPORT Ryggsäckar
	--insert into @Tree (CategoryId, Depth, HasChildren)
	--select c.CategoryId, 2, 1
	--from product.tCategory c
	--where c.CategoryId in (
	--   1000539,
 --     1000540,
 --     1000546,
 --     1000549,
 --     1000552,
 --     1000557,
 --     1000558,
 --     1000563,
 --     1000564,
 --     1000565,
 --     1000566,
 --     1000840,
 --     1000916)
 --     and c.ParentCategoryId in (1000538, 1000548)
	
	--insert into @Tree (CategoryId, Depth, HasChildren)
	--select c.CategoryId, 1, 0
	--from product.tCategory c
	--where c.CategoryId in (1000538, 1000548)
	---- HACK
	
	update t set HasChildren = 0
	from @Tree t
		left outer join product.tCategory c on c.ParentCategoryId = t.CategoryId
	where c.CategoryId is null
	
	-- Category
	select c.CategoryId, 
	   
	   c.ParentCategoryId,
	   ---- HACK TO ASSURE WE CAN IMPORT Ryggsäckar
	   --case when c.CategoryId in (1000538, 1000548) then 1000533 else c.ParentCategoryId end ParentCategoryId, 
	   
	   c.Title, c.ErpId, t.Depth
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		
	-- Category Translation
	select ct.CategoryId, ct.LanguageId, case when (ct.LanguageId = 1) then c.Title else ct.Title end as Title
	from @Tree t
		inner join product.tCategory c on c.CategoryId = t.CategoryId
		inner join product.tCategoryTranslation ct on ct.CategoryId = t.CategoryId
	where ct.Title is not null
	
	-- Product to examine
	declare @CategoryLeaves table
	(
		CategoryId int primary key
	)
	insert into @CategoryLeaves
	select CategoryId
	from @Tree
	where Depth = 2 and HasChildren = 0
	
	declare @Product table
	(
		ProductId int primary key
	)
	insert into @Product
	select p.ProductId
	from product.tProduct p
		inner join @CategoryLeaves cl on cl.CategoryId = p.CategoryId
	where p.IsDeleted = 0
	
	delete from @Product where ProductId in (
		select ProductId
		from product.tProductImage
		group by ProductId, MediaId
		having count(*) > 1
	)
	
	-- Product
   select p.ProductId
         ,lp.HyErpId as ErpId
         ,p.EanCode
         ,p.IsDeleted
         ,p.CategoryId
         ,p.Title
         ,p.ProductStatusId
         ,lp.AgeFromMonth
         ,lp.AgeToMonth
		   ,lp.LekmerErpId as SupplierArticleNumber
		   ,p.[Description]
		   ,lp.BrandId
		   ,p.NumberInStock
     from @Product t
          inner join product.tProduct p
                  on p.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp
                  on t.ProductId = lp.ProductId
			
---- Discounts
--declare @PriceListDiscounts table
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2)
--)
--insert into @PriceListDiscounts ( PriceListId, ProductId, DiscountPrice )
--select 
--   case 
--      when (c.CampaignRegistryId = 1) then 1
--      when (c.CampaignRegistryId = 2) then 2
--      when (c.CampaignRegistryId = 3) then 3
--      when (c.CampaignRegistryId = 4) then 4
--      when (c.CampaignRegistryId = 5) then 5
--   else
--      c.CampaignRegistryId
--   end as PriceListId,
--   pdai.ProductId, pdai.DiscountPrice, pda.ProductActionId
--from campaign.tCampaign c
--   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId   
--   inner join lekmer.tProductDiscountAction pda on pda.ProductActionId = pa.ProductActionId
--   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = pda.ProductActionId   
--where c.CampaignStatusId = 0
--   and (c.StartDate is null or c.StartDate <= getdate())
--   and (c.EndDate is null or getdate() <= c.EndDate)
--   and pdai.ProductId = 7364


--create table #PriceListDiscount
--(
--   PriceListId int,
--   ProductId int,
--   DiscountPrice decimal(18, 2) primary key (PriceListId, ProductId)
--)

--insert into #PriceListDiscount
--select PriceListId, ProductId, min(DiscountPrice)
--from @PriceListDiscounts
--group by PriceListId, ProductId

if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId

insert into #CampaignProduct ( CampaignId, ProductId, Price )
select c.CampaignId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   Price decimal(16, 2)
)
insert into #BestCampaignPrice (ProductId, PriceListId, Price)
select cp.ProductId, c.PriceListId, min(cp.Price) as Price
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
group by cp.ProductId, c.PriceListId

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from @Channel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title

	-- Product Channel
	select pr.ProductId, pr.ProductRegistryId as ChannelId
	from @Channel c
		inner join product.tProductRegistryProduct pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tProduct p on p.ProductId = pr.ProductId
		inner join @Product t on t.ProductId = p.ProductId
		inner join @PriceList pl on pl.ChannelId = c.ChannelId
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		   and pli.ProductId = p.ProductId
	
	select pl.ChannelId, pli.ProductId, 
	   pli.PriceIncludingVat as Price, pli.PriceIncludingVat - pli.PriceExcludingVat as Vat, 
	   pld.Price as DiscountPrice, case when (pld.Price is null) then null else pld.Price * pli.VatPercentage / 100 end as DiscountPriceVat
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join @Product t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice pld on pld.PriceListId = pl.PriceListId
		   and pld.ProductId = t.ProductId
	
   drop table #BestCampaignPrice
		
	--drop table #PriceListDiscount
	
	-- Product Media
	declare @AllProductImages table (
	   ProductId int,
	   MediaId int,
	   ProductImageGroupId int
	   primary key (ProductId, MediaId, ProductImageGroupId) with (ignore_dup_key = on)
	)

   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
         ,p.MediaId
         ,1 as ProductImageGroupId
   from @Product t
         inner join product.tProduct p on p.ProductId = t.ProductId
   where p.MediaId is not null
	
   insert into @AllProductImages ( ProductId, MediaId, ProductImageGroupId )
   select p.ProductId
      ,p.MediaId
      ,p.ProductImageGroupId
   from @Product t            
       inner join product.tProductImage p on p.ProductId = t.ProductId
   
   select pi.ProductId 
		  ,lp.HyErpId AS ErpId
		  ,pi.MediaId
		  ,pi.ProductImageGroupId
		  ,mf.Extension
	 from @AllProductImages pi
	      inner join lekmer.tLekmerProduct lp on lp.ProductId = pi.ProductId
	      inner join media.tMedia m on pi.MediaId = m.MediaId
		   inner join media.tMediaFormat mf on m.MediaFormatId = mf.MediaFormatId
   
   -- Icons				   
   select lpi.IconId
         ,p.ProductId
     from @Product p
          inner join lekmer.tLekmerProductIcon lpi
                  on lpi.ProductId = p.ProductId
      
   -- Product translations (title & description)            
   select pt.ProductId
         ,lp.HyErpId AS ErpId
         ,pt.LanguageId
         ,pt.Title
         ,pt.[Description]
     from @product t 
         inner join product.tProductTranslation pt on pt.ProductId = t.ProductId
          inner join lekmer.tLekmerProduct lp on lp.ProductId = pt.ProductId
    where Title is not null or Description is not null
    
    -- Brands  
   select b.BrandId
         ,b.Title
         ,b.ExternalUrl
         ,b.MediaId
         ,mf.Extension
     from lekmer.tBrand b 
          inner join media.tMedia m on m.MediaId = b.MediaId
          inner join media.tMediaFormat mf on mf.MediaFormatId = m.MediaFormatId
END

GO
