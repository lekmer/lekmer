SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSettingTypeDelete]
    @ModelSettingTypeId INT
AS 
BEGIN
    SET NOCOUNT ON
    
    IF EXISTS ( SELECT  *
                FROM    [template].[tModelSetting]
                WHERE   [ModelSettingTypeId] = @ModelSettingTypeId ) 
        RETURN 1
    
    DELETE  [template].[tModelSettingType]
    WHERE   [ModelSettingTypeId] = @ModelSettingTypeId

    RETURN 0

END
GO
