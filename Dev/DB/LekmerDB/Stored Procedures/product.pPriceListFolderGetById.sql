SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListFolderGetById]
@PriceListFolderId	int
as
begin
	select
		*
	from
		[product].[vCustomPriceListFolder] plf
    where
		plf.[PriceListFolder.Id] = @PriceListFolderId
end

GO
