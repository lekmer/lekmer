SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Stored Procedure

CREATE procedure [product].[pRelationListSave]
	@RelationListId int,
	@Title nvarchar(50),
	@RelationListTypeId int
as
BEGIN
	update
		product.tRelationList
	set
		Title			= @Title,
		RelationListTypeId	=@RelationListTypeId
	where
		RelationListId = @RelationListId
		
	if  @@ROWCOUNT = 0 
	begin
		insert 
			product.tRelationList
		(
			Title, 
			RelationListTypeId
		)
		values
		(
			@Title, 
			@RelationListTypeId
		)
		set @RelationListId = scope_identity()						
	end
	return @RelationListId
end











GO
