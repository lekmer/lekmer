SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionGetById]
	@Id int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomPercentagePriceDiscountAction
	WHERE
		[ProductAction.Id] = @Id
end

GO
