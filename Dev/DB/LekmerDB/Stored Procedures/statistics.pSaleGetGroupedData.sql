SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [statistics].[pSaleGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime,
	@StatusIds		varchar(max)
AS 
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		 
	DECLARE @tSale TABLE ([DateTime] smalldatetime, Amount decimal(16, 2))
	INSERT INTO 
		@tSale
	(
		[DateTime],
		Amount
	)
	SELECT
		[statistics].[fnGetDateTimeKeyDirectly](o.[CreatedDate], @Mode) AS 'DateTime',
		oi.[ActualPriceIncludingVat] * oi.[Quantity] AS 'Amount'
	FROM
		[order].tOrderItem oi WITH(NOLOCK)
		INNER JOIN [order].tOrder o WITH(NOLOCK) ON oi.OrderId = o.OrderId
	WHERE
		o.[ChannelId] = @ChannelId
		AND o.[CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND o.OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
			
	SELECT 
		d.[DateTime], 
		isnull(sum(s.Amount), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tSale s ON s.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
