SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberDelete]
	@ChannelId	INT,
	@Email		VARCHAR(320)
AS
BEGIN
	DELETE
		[lekmer].[tNewsletterSubscriber]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
	RETURN @@ROWCOUNT
END
GO
