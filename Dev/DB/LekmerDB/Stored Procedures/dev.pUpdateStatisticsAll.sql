SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create procedure [dev].[pUpdateStatisticsAll]
as
begin
	declare @UpdateScript varchar(max)
	set @UpdateScript = ''

	select @UpdateScript = @UpdateScript + '
	update statistics ['+table_schema+'].['+table_name+'] WITH FULLSCAN'
	from information_schema.tables
	where TABLE_TYPE='BASE TABLE'

	exec (@UpdateScript)
end
GO
