SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsDK]

AS
begin
	set nocount on
	begin try
		begin transaction				
		
		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n --3
		where 
			n.SiteStructureRegistryId = 3
			and n.ContentNodeTypeId = 3 -- detta är contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Legetøj online fra Lekmer.dk – legetøj på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Vi tilbyder et stort udvalg af legetøj fra '
			+ 'kendte varemærker leverer indenfor 3-5 dage - Lekmer.dk din legetøjsbutik.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 3
			and cn.ParentContentNodeId = 1005996
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Legetøj online fra Lekmer.dk – legetøj på nettet.',
			
			cps.[Description] = 'Køb ' + x.Title + ' ' + cn.Title + ' på nettet. Vi tilbyder et stort udvalg af legetøj fra '
			+ 'kende varemærker leverer indenfor 3-5 dage - Lekmer.dk din legetøjsbutik.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1005996) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 3
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		
		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
										
		UPDATE
			cps
		SET
			cps.Title = 'Barn og Baby ' + cn.Title + ' produkter online fra Lekmer.dk – Køb børneartikler på nettet.',
			
			cps.[Description] = 'Køb Barn og Baby produkter på nettet. Få dine barn- og babyprodukter leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 3
			and cn.ParentContentNodeId = 1005875
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barn og Baby produkter online fra Lekmer.dk – Køb børneartikler på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title  + ' Barn og Baby produkter på nettet. Få dine barn- og babyprodukter leveret '
			+ 'indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1005875) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 3
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
			
		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Børnetøj online fra Lekmer.dk – Børn og Babytøj på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Stort udvalg af cool tøj til børn og babyer – '
			+ 'handle online dit børnetøj og få dem leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 3
			and cn.ParentContentNodeId = 1007349
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Børnetøj online fra Lekmer.dk – Barn og babytøj på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dit børnetøj leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1007349) x 
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 3	
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Indretning online fra Lekmer.dk Børneværelses indretning på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Få dit børneværelse leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 3
			and cn.ParentContentNodeId = 1005944
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Indretning online från Lekmer.dk – Børneværelse på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dit børneværelse leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1005944) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 3			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------							
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' og Underholdning og børnespil online fra Lekmer.dk – Tv-spil på nettet.',
			
			cps.[Description] = 'Køb ' + cn.Title + ' på nettet. Få dine tv-spil leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 3
			and cn.ParentContentNodeId = 1006393
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' ',
			
			cps.[Description] = 'Køb ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dine tv-spil leveret indenfor 3-5 dage fra Lekmer.dk.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1006393) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 3	
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
						
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end

GO
