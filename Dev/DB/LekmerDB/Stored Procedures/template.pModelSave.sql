SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSave]
    @ModelId INT,
    @ModelFolderId INT,
    @Title NVARCHAR(50),
    @CommonName NVARCHAR(50),
    @DefaultTemplateId INT
AS 
BEGIN
    SET NOCOUNT ON

    UPDATE 
		[template].[tModel]
    SET
		[ModelFolderId]	= @ModelFolderId,
		[Title]	= @Title,
		[CommonName] = @CommonName,
		[DefaultTemplateId] = 
			CASE 
				WHEN @DefaultTemplateId=-1 THEN NULL 
				ELSE @DefaultTemplateId END
    WHERE   [ModelId] = @ModelId
            
    IF  @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tModel]
        (
          [ModelFolderId],
          [Title],
          [CommonName],
          [DefaultTemplateId]
	    )
        VALUES  
        (
          @ModelFolderId,
          @Title,
          @CommonName,
          CASE 
			WHEN @DefaultTemplateId=-1 THEN NULL
			ELSE @DefaultTemplateId END
        )

        SET @ModelId = SCOPE_IDENTITY()
    END
    
    RETURN @ModelId

END
GO
