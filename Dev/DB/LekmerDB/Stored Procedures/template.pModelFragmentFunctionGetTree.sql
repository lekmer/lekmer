SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionGetTree]
@ModelFragmentId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))		
	IF (@ModelFragmentId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[ModelFragmentFunction.Id],
					NULL,
					v.[ModelFragmentFunction.CommonName]
				FROM template.vCustomModelFragmentFunction AS v
				WHERE v.[ModelFragmentFunction.ModelFragmentId] = @ModelFragmentId
		END

		SELECT 
			Id,	ParentId, Title, CAST(0 AS bit) AS 'HasChildren'
		FROM @tTree
END

GO
