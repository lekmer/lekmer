SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignGetAllByFolder]
@FolderId	int
AS
BEGIN
	SELECT 
		*
	FROM 
		[campaign].[vCustomCampaign]
	WHERE 
		[Campaign.FolderId] = @FolderId
	ORDER BY 
		[Campaign.Priority] ASC
END

GO
