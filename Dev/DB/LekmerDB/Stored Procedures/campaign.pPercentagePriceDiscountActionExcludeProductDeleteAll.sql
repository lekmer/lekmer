SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pPercentagePriceDiscountActionExcludeProductDeleteAll]
	@ProductActionId int
as
begin
	DELETE
		campaign.tPercentagePriceDiscountActionExcludeProduct
	WHERE
		ProductActionId = @ProductActionId
end



GO
