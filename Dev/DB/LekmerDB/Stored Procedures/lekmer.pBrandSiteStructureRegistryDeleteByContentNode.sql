SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandSiteStructureRegistryDeleteByContentNode]
	@ContentNodeId	INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE lekmer.tBrandSiteStructureRegistry
	WHERE ContentNodeId = @ContentNodeId
END

GO
