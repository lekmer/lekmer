SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 09.10.2008		Time: 12:00
Description:
			Created 
*/

CREATE PROCEDURE [template].[pModelGetAll]
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomModel]
	order by
		[Model.Title]
end

GO
