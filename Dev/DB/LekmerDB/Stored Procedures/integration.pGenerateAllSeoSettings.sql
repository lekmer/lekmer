SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateAllSeoSettings]

AS
begin
	set nocount on
	begin try
		
		-- SE Seo
		exec [integration].[pGenerateProductSeoSE]
		exec [integration].[pGenerateSeoContentPageSettingsSE]
		
		-- NO Seo
		exec [integration].[pGenerateProductSeoNO]
		exec [integration].[pGenerateSeoContentPageSettingsNO]
		
		-- DK Seo
		exec [integration].[pGenerateProductSeoDK]
		exec [integration].[pGenerateSeoContentPageSettingsDK]
		
		-- FI Seo
		exec [integration].[pGenerateProductSeoFI]
		exec [integration].[pGenerateSeoContentPageSettingsFI]

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
