SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Dovhun Roman
-- Create date: 2009-03-24
-- Description:	created
-- =============================================
CREATE PROCEDURE [media].[pImageSizeGetByCommonName]
	@CommonName VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomImageSize] AS tis
	WHERE  tis.[ImageSize.CommonName] = @CommonName
END

GO
