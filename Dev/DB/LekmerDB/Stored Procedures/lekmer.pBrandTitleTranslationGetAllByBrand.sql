SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandTitleTranslationGetAllByBrand]
@BrandId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.Title] AS 'Value'
	FROM
	    lekmer.vBrandTranslation where [BrandTranslation.BrandId] = @BrandId
END




GO
