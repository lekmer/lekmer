SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeGetAllByProduct]
@ProductId	int
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[lekmer].[vProductSize]
	WHERE 
		[ProductSize.ProductId] = @ProductId
	ORDER BY
		[Size.EU]
END
GO
