SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductTranslationsDiffNO]

AS
begin
	set nocount on
					
		select
			l.HYErpId, 
			p.Title as 'SE Title', 
			p.[Description] as 'SE Description', 
			pt.Title as 'NO Title', 
			pt.[Description] as 'NO Description'
		from 
			lekmer.tLekmerProduct l with (nolock)
			inner join product.tProduct p with (nolock)
				on l.ProductId = p.ProductId
			left join product.tProductTranslation pt with (nolock)
				on p.ProductId = pt.ProductId
			left join integration.tChannelRestrictionProduct i with (nolock)
				on l.ProductId = i.ProductId
				and i.ProductRegistryId = 2
				and i.[State] = 'Manual'
		where 
			p.Title != ''
			and p.Description != ''
			and pt.LanguageId = 1000001
			and (pt.Title is null
					or  pt.Description is null)
			and i.ProductId is null
		order by
			l.HYErpId					 
end
GO
