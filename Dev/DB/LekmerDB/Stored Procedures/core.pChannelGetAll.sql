SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pChannelGetAll]
as
begin
	set nocount on

	select
		*
	from
		[core].[vCustomChannel]
end

GO
