SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pOrderStatusUpdate]
	@OrderId int,
	@OrderStatusId int
AS
begin
	--set nocount on
	begin transaction
	begin try
		
		update
			[order].tOrder
		set
			OrderStatusId = @OrderStatusId
		where
			OrderId = @OrderId
			and OrderStatusId <> @OrderStatusId
		
	commit
	end try
	begin catch
		if @@trancount > 0 rollback transaction

		insert into [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
