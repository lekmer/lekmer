SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationTypeGetAllByVariationGroup]
	@VariationGroupId int
as
begin
	--set nocount off
	SELECT 
		*
	FROM 
		[product].[vCustomVariationTypeSecure] vt
	WHERE
		vt.[VariationType.VariationGroupId] = @VariationGroupId
	--order by [Ordinal]
end

GO
