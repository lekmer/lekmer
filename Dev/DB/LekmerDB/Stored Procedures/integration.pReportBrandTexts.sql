SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportBrandTexts]

AS
begin
	set nocount on
	
		
	select
		--b.BrandId,
		b.ErpId,
		b.Title as BrandTitle,		
		b.[Description] as SE,
		nob.Description as 'NO',
		dkb.Description as 'DK',
		fib.Description as 'FI'
	from 
		lekmer.tBrand b with (nolock)
		left join 		
		(select brandId, [Description] from lekmer.tBrandTranslation bt with (nolock)
			where LanguageId = 1000001) nob 
			on nob.BrandId = b.BrandId
		left join	
		(select brandId, [Description] from lekmer.tBrandTranslation bt with (nolock)
			where LanguageId = 1000002) dkb
			on dkb.BrandId = b.BrandId
			
		left join	
		(select brandId, [Description] from lekmer.tBrandTranslation bt with (nolock)
			where LanguageId = 1000003) fib
			on fib.BrandId = b.BrandId
		
end


GO
