SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupFolderGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	--insert root nodes
	INSERT INTO @tTree
		SELECT 
			[CustomerGroupFolder.Id],
			[CustomerGroupFolder.ParentId], 
			[CustomerGroupFolder.Title]
		FROM customer.vCustomCustomerGroupFolder
		WHERE [CustomerGroupFolder.ParentId] IS NULL
		
	IF (@SelectedId IS NOT NULL)
		BEGIN
			--insert selected node's children
			INSERT INTO @tTree
				SELECT 
					[CustomerGroupFolder.Id],
					[CustomerGroupFolder.ParentId], 
					[CustomerGroupFolder.Title]
				FROM customer.vCustomCustomerGroupFolder
				WHERE [CustomerGroupFolder.ParentId] = @SelectedId
			
			--insert siblings and parents with their siblings
			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT [CustomerGroupFolder.ParentId]
									 FROM customer.vCustomCustomerGroupFolder
									 WHERE [CustomerGroupFolder.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT 
							[CustomerGroupFolder.Id],
							[CustomerGroupFolder.ParentId], 
							[CustomerGroupFolder.Title]
						FROM 
							customer.vCustomCustomerGroupFolder
						WHERE
							[CustomerGroupFolder.ParentId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM customer.vCustomCustomerGroupFolder 
									WHERE [CustomerGroupFolder.ParentId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY ParentId ASC
END

GO
