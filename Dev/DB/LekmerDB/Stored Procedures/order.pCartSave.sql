SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [order].[pCartSave]
	@CartGuid		UNIQUEIDENTIFIER,
	@CreatedDate	DATETIME
as
begin
	SET NOCOUNT ON

	INSERT INTO [order].tCart
	(
		CartGuid,
		CreatedDate
	)
	VALUES
	(
		@CartGuid,
		@CreatedDate
	)
	RETURN SCOPE_IDENTITY()
end
GO
