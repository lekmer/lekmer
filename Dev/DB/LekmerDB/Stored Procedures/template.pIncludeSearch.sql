SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pIncludeSearch]
	@SearchTitle	varchar(MAX)
AS
begin
	set nocount ON
	SET @SearchTitle = [generic].[fPrepareSearchParameter](@SearchTitle)
	select
		*
	from
		[template].[vCustomInclude]
	where
		[Include.CommonName] like @SearchTitle ESCAPE '\'
		or [Include.Description] like @SearchTitle ESCAPE '\'
		OR [Include.Content] LIKE @SearchTitle ESCAPE '\'
end

GO
