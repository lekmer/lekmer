SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pCategoryTranslationSave]
@CategoryId		int,
@LanguageId		int,
@Title			nvarchar(50)
AS
BEGIN
	SET nocount ON
	
	UPDATE
		product.tCategoryTranslation
	SET
		Title = @Title 
	WHERE
		CategoryId = @CategoryId AND
		LanguageId = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO product.tCategoryTranslation
		(
			CategoryId,
			LanguageId,
			Title				
		)
		VALUES
		(
			@CategoryId,
			@LanguageId,
			@Title
		)
	END
END




GO
