SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pPriceListItemGetAllByProduct]
	@ProductId	int
as
begin
	SET NOCOUNT ON;

	SELECT 
		v.*
	FROM 
		product.vCustomPriceListItem v
	where
		v.[Price.ProductId] = @ProductId
end

GO
