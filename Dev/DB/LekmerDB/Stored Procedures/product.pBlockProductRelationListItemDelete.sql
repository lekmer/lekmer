SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockProductRelationListItemDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM 
		product.tBlockProductRelationListItem
	WHERE
		BlockId = @BlockId
END
GO
