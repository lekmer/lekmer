SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pSizeGetAll]
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[lekmer].[vSize]
END
GO
