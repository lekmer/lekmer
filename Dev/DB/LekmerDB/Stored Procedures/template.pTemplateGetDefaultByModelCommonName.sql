SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*****************  Version 1  *****************
User: Yuriy P.		Date: 22.01.2009
Description:
			created
*/

CREATE PROCEDURE [template].[pTemplateGetDefaultByModelCommonName]
	@CommonName	varchar(50)
AS
BEGIN
	SELECT
		T.*
	FROM
		[template].[vCustomModel] AS M
		INNER JOIN [template].[vCustomTemplate] AS T ON M.[Model.DefaultTemplateId] = T.[Template.Id]
	WHERE
        M.[Model.CommonName] = @CommonName
END

GO
