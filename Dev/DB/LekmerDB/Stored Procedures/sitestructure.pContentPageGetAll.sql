SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetAll]
@ChannelId int
as
begin
	select 
		v.*
	from 
		[sitestructure].[vCustomContentPage] AS v
	WHERE v.[ContentNode.ChannelId] = @ChannelId
end

GO
