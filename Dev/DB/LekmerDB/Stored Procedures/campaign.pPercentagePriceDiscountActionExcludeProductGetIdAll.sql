SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeProductGetIdAll]
	@ChannelId		int,
	@CustomerId		int,
	@ActionId		int
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		campaign.tPercentagePriceDiscountActionExcludeProduct A
		INNER JOIN product.vCustomProduct P ON A.ProductId = P.[Product.Id]
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				P.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		A.ProductActionId = @ActionId
		and P.[Product.ChannelId] = @ChannelId
END

GO
