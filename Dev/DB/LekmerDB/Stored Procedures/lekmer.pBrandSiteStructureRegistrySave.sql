SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pBrandSiteStructureRegistrySave]
	@BrandId					int,
	@SiteStructureRegistryId	int,
	@ContentNodeId				int
AS
BEGIN
	UPDATE 
		lekmer.tBrandSiteStructureRegistry
	SET 
		ContentNodeId = @ContentNodeId
	WHERE 
		BrandId = @BrandId
		AND SiteStructureRegistryId = @SiteStructureRegistryId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT
			lekmer.tBrandSiteStructureRegistry
			(
				[BrandId],
				[SiteStructureRegistryId],
				[ContentNodeId]
			)
		VALUES
			(
				@BrandId,
				@SiteStructureRegistryId,
				@ContentNodeId
			)
	END
END
GO
