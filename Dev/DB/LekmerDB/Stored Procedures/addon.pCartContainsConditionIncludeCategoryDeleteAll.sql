SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionIncludeCategoryDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].tCartContainsConditionIncludeCategory
	WHERE
		ConditionId = @ConditionId
END 



GO
