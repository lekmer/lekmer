SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartCampaignGetIdAll]
	@ChannelId int
as
begin
	SELECT
		c.[Campaign.Id]
	FROM
		campaign.vCustomCartCampaign c
		inner join campaign.tCampaignModuleChannel m on m.CampaignRegistryId = c.[Campaign.CampaignRegistryId]
	WHERE
		m.ChannelId = @ChannelId
		AND c.[Campaign.CampaignStatusId] = 0
	ORDER BY
		c.[Campaign.Priority]
end

GO
