SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [dev].[pCustomViewDeleteAll]
as
begin
	declare @scripts TABLE(script nvarchar(max))

	insert
		@scripts
	(
		script
	)
	select
		'drop view [' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'
	from
		INFORMATION_SCHEMA.Views
	where
		TABLE_NAME LIKE 'vCustom%'
		and TABLE_NAME NOT LIKE 'vCustomer%'
		and TABLE_NAME NOT LIKE 'vCustomField%'

	declare scripts_cursor cursor forward_only fast_forward for select script from @scripts
	declare @drop_script nvarchar(max)
	open scripts_cursor
	fetch next from scripts_cursor INTO @drop_script
	while @@fetch_status = 0
	begin
		exec(@drop_script)
		fetch next from scripts_cursor INTO @drop_script
	end
	close scripts_cursor
	deallocate scripts_cursor
end
GO
