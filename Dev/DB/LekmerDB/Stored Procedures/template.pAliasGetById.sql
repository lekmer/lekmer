SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 19.09.2008		Time: 10:50
Description:
			Created 

*****************  Version 2  *****************
User: Victor E.		Date: 23.01.2009
Description: Add Path

*****************  Version 3  *****************
User: Yuriy P.		Date: 26.01.2009
Description: Remove Path
*/

CREATE PROCEDURE [template].[pAliasGetById]
	@AliasId	int
AS
begin
	set nocount on

	SELECT	*
	from
		[template].[vCustomAliasSecure] a
	where
	    a.[Alias.AliasId] = @AliasId
end

GO
