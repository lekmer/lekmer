SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pProductSizeDecreaseNumberInStock]
	@ProductId int,
	@SizeId int,
	@Quantity int
as
begin
	update
		lekmer.tProductSize
	set
		NumberInStock = (case when NumberInStock - @Quantity < 0 then 0 else NumberInStock - @Quantity end)
	where
		ProductId = @ProductId
		and SizeId = @SizeId
end
GO
