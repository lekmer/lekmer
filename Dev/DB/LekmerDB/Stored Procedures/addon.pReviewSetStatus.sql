SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pReviewSetStatus]
         @ReviewId  int,
         @StatusId   int
AS    
BEGIN
         UPDATE 
                   addon.tReview
         SET 
                   ReviewStatusId = @StatusId
         WHERE 
                   ReviewId = @ReviewId
END
GO
