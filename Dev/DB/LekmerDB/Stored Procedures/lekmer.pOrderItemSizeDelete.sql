SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE lekmer.pOrderItemSizeDelete
	@OrderItemId INT
AS
BEGIN
	SET NOCOUNT ON;

	delete
		lekmer.tOrderItemSize
	WHERE
		OrderItemId = @OrderItemId
END
GO
