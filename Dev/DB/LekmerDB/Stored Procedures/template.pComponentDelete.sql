SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [template].[pComponentDelete]
	@ComponentId	INT
AS
BEGIN
	DELETE
		[template].[tComponent]
	WHERE
		[ComponentId]  = @ComponentId
END	


GO
