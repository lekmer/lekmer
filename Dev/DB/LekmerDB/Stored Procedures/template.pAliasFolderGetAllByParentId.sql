SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pAliasFolderGetAllByParentId]
	@AliasFolderId	int
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomAliasFolder]
	where
		[AliasFolder.ParentAliasFolderId] = @AliasFolderId
end

GO
