SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ImportUpdateLekmerProduct]

AS
BEGIN
		-- Update tLekmerProduct
		UPDATE 
			lp
		SET 
			lp.ExpectedBackInStock =  NULL
		--select *
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,17)	
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)
		WHERE 
			lp.ExpectedBackInStock <> LEN(lp.ExpectedBackInStock)
			AND tp.NoInStock > 0

		
		-- Update tLekmerProduct
		UPDATE 
			lp
		SET 
			lp.LekmerErpId = tp.LekmerArtNo
		--select lp.HYerpid, tp.HYarticleId, tp.LekmerArtNo, lp.LekmerErpId
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,17)	
			
		WHERE
			substring(tp.HYarticleId, 3,1) = 1
			AND
				(
					lp.LekmerErpId IS NULL
					OR lp.LekmerErpId <> tp.LekmerArtNo
				)
			
END
GO
