
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_UpdateTCategoryLekmer]
AS
BEGIN
	DECLARE 
		@Seperator NVARCHAR(1),	
		@ErpCategoryPrefix NVARCHAR(2),
		@ErpClassId NVARCHAR(50),
		@ErpClassGroupId NVARCHAR(50),
		@ErpClassGroupCodeId NVARCHAR(50),
		@ClassGroupCodeId NVARCHAR(50),
		@HYErpId NVARCHAR(50),
		@ArticleClassId NVARCHAR(10),
		@ArticleClassTitle NVARCHAR(40),
		@ArticleGroupId NVARCHAR(10),
		@ArticleGroupTitle NVARCHAR(40),
		@ArticleCodeId NVARCHAR(10),
		@ArticleCodeTitle NVARCHAR(40)
	
	SET @Seperator = '-'
	SET @ErpCategoryPrefix = 'C_'
		
	DECLARE cur_product CURSOR FAST_FORWARD FOR
		SELECT DISTINCT
			tp.HYErpId,
			tp.ArticleClassId ,
			tp.ArticleClassTitle,
			tp.ArticleGroupId,
			tp.ArticleGroupTitle,
			tp.ArticleCodeId,
			tp.ArticleCodeTitle
		FROM
			[import].tProduct tp
			LEFT JOIN lekmer.tLekmerProduct lp ON lp.HYErpId = tp.HYErpId 
			LEFT JOIN product.tProduct p ON p.ProductId = lp.ProductId
			LEFT JOIN product.tCategory c ON c.CategoryId = p.CategoryId
		WHERE
			@ErpCategoryPrefix 
				+ ISNULL(tp.ArticleClassId, '') + @Seperator 
				+ ISNULL(tp.ArticleGroupId, '') + @Seperator 
				+ ISNULL(tp.ArticleCodeId, '') <> c.ErpId
			AND tp.ChannelId = 1
		
	OPEN cur_product
	FETCH NEXT FROM cur_product
		INTO 
			@HYErpId,
			@ArticleClassId,
			@ArticleClassTitle,
			@ArticleGroupId,
			@ArticleGroupTitle,
			@ArticleCodeId,
			@ArticleCodeTitle
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			SET @ClassGroupCodeId = @ArticleClassId + @Seperator + @ArticleGroupId + @Seperator + @ArticleCodeId
			
			BEGIN TRY
				BEGIN TRANSACTION
					SET @ErpClassId = @ErpCategoryPrefix + @ArticleClassId
					SET @ErpClassGroupId = @ErpClassId + @Seperator + @ArticleGroupId
					SET @ErpClassGroupCodeId = @ErpClassGroupId + @Seperator + @ArticleCodeId
				
					-- varuklass
					IF not exists (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassId)
						INSERT INTO 
							product.tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							null,
							@ArticleClassTitle,
							@ErpClassId

					-- Varugrupp
					IF NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupId)
						INSERT INTO 
							[product].tCategory (ParentCategoryId, Title, ErpId)
						SELECT 	
							(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassId),
							@ArticleGroupTitle,		
							@ErpClassGroupId
					
					-- Varukod
					IF (NOT EXISTS (SELECT 1 FROM product.tCategory tc WHERE tc.ErpId = @ErpClassGroupCodeId)
						AND @ArticleCodeTitle <> '***Missing***' )
							INSERT INTO 
								[product].tCategory (ParentCategoryId, Title, ErpId)
							SELECT 	
								(SELECT CategoryId FROM product.tCategory WHERE ErpId = @ErpClassGroupId),
								@ArticleCodeTitle,		
								@ErpClassGroupCodeId

					-- ändra referensen på categoryId om den bytat
					UPDATE 
						tp
					SET 
						tp.CategoryId = (SELECT categoryId FROM product.tCategory 
										 WHERE ErpId = @ErpClassGroupCodeId)
					FROM 
						[lekmer].tLekmerProduct lp						
						INNER JOIN product.tProduct tp ON tp.ProductId = lp.ProductId				
					WHERE 
						lp.HYErpId = @HYErpId 
						AND tp.CategoryId <> (SELECT categoryId FROM product.tCategory 
											  WHERE ErpId = @ErpClassGroupCodeId)
					
				COMMIT
			END TRY
			BEGIN CATCH
				IF @@TRANCOUNT > 0 ROLLBACK
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				VALUES(@ClassGroupCodeId, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

				CLOSE cur_product
				DEALLOCATE cur_product 
			END CATCH
		
			FETCH NEXT FROM cur_product 
				INTO 
					@HYErpId,
					@ArticleClassId,
					@ArticleClassTitle,
					@ArticleGroupId,
					@ArticleGroupTitle,
					@ArticleCodeId,
					@ArticleCodeTitle
		END
	CLOSE cur_product
	DEALLOCATE cur_product	              
END
GO
