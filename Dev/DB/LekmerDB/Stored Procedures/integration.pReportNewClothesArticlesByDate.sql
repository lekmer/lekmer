SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportNewClothesArticlesByDate]
	@ValidFrom		datetime = NULL,
	@ValidTo		datetime = NULL	
	--@ChannelGroupId	int = NULL,
AS
begin
	set nocount on
		
	begin try

		-- @gör ValidTo i tid 00:00:00  sen lägg till 23h 59min och 59 sec  i sekunder
		set @ValidFrom = replace(cast(convert(varchar, @ValidFrom, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = replace(cast(convert(varchar, @ValidTo, 102) as varchar), '.', '-') + ' 00:00:00'
		set @ValidTo = DATEADD(SS,86399,@ValidTo)
		
		select 
			HYErpId,
			p.Title,
			coalesce(ps.NumberInStock, p.NumberInStock),
			CreatedDate,
			cast(s.EU as int) as Centilong
		from
			lekmer.tLekmerProduct l
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join lekmer.tProductSize ps
				on ps.ProductId = p.ProductId
			left join lekmer.tSize s
				on s.SizeId = ps.SizeId
		where
			p.CategoryId in
			(
				select CategoryId from product.tCategory
				where ParentCategoryId in 
					(
					select CategoryId from product.tCategory
					where ParentCategoryId in 
										(
											select CategoryId from product.tCategory
											where Title = 'barnkläder'
										)
									))
			and
			(@ValidFrom is null or CreatedDate > @ValidFrom)
			and
			(@ValidTo is null or CreatedDate < @ValidTo)

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
