SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionSave]
	@ConditionId		int,
	@MinQuantity		int,
	@AllowDuplicates	bit,
	@IncludeAllProducts	bit
AS 
BEGIN 
	UPDATE
		addon.tCartContainsCondition
	SET
		MinQuantity = @MinQuantity,
		AllowDuplicates = @AllowDuplicates,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ConditionId = @ConditionId
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			addon.tCartContainsCondition
		(
			ConditionId,
			MinQuantity,
			AllowDuplicates,
			IncludeAllProducts
		)
		VALUES
		(
			@ConditionId,
			@MinQuantity,
			@AllowDuplicates,
			@IncludeAllProducts
		)
	END
END 
GO
