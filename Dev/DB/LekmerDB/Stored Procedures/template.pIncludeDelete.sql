SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 19.09.2008		Time: 17:00
Description:
			Created 
*/
CREATE procedure [template].[pIncludeDelete]
	@IncludeId	int
AS
begin
	set nocount on

	delete
		[template].[tInclude] 
		where
		[IncludeId] = @IncludeId
end	


GO
