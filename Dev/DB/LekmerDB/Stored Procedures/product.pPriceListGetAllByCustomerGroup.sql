SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListGetAllByCustomerGroup]
@CustomerGroupId int
AS
BEGIN
	SELECT
		P.*
	FROM
		[product].[vCustomPriceList] AS P
		INNER JOIN product.tPriceListCustomerGroup AS PG ON P.[PriceList.Id] = PG.PriceListId
	WHERE 
		PG.CustomerGroupId = @CustomerGroupId
END

GO
