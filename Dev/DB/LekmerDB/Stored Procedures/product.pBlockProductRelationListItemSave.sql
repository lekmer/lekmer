SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockProductRelationListItemSave]
	@BlockId			INT,
	@RelationListTypeId	INT
AS
BEGIN
	INSERT INTO 
		product.tBlockProductRelationListItem 
	VALUES
		(
			@BlockId,
			@RelationListTypeId
		) 
END
GO
