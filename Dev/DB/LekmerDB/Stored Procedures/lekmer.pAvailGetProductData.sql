SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pAvailGetProductData] 
AS
BEGIN
	SET NOCOUNT ON;

SELECT     p.ProductId, 
		   product.fGetPriceByPriceListCommonName(p.ProductId,'Sweden') as PriceSE,
		   product.fGetPriceByPriceListCommonName(p.ProductId,'Denmark') as PriceDK,
		   product.fGetPriceByPriceListCommonName(p.ProductId,'Norway') as PriceNO,
		   product.fGetPriceByPriceListCommonName(p.ProductId,'Finland') as PriceFI,
		   generic.fGetCategoryUrl(p.CategoryId, 1, p.ProductId) as CategoryUrlSE,
		   generic.fGetCategoryUrl(p.CategoryId, 1000001, p.ProductId) as CategoryUrlNO,
		   generic.fGetCategoryUrl(p.CategoryId, 1000002, p.ProductId) as CategoryUrlDA,
		   generic.fGetCategoryUrl(p.CategoryId, 1000003, p.ProductId) as CategoryUrlFI,
		   generic.fGetImageUrl(p.ProductId, 1) as ImageUrlSE,
		   generic.fGetImageUrl(p.ProductId, 1000001) as ImageUrlNO,
		   generic.fGetImageUrl(p.ProductId, 1000002) as ImageUrlDA,
		   generic.fGetImageUrl(p.ProductId, 1000003) as ImageUrlFI, 
		   coalesce(tSE.Title,p.Title) as TitleSE,
		   coalesce(tNO.Title,p.Title) as TitleNO,
		   coalesce(tDA.Title,p.Title) as TitleDA,
		   coalesce(tFI.Title,p.Title) as TitleFI,
		   p.NumberInStock
FROM       product.tProduct p
left outer join product.tProductTranslation tSE on tSE.ProductId = p.ProductId and tSE.LanguageId = 1
left outer join product.tProductTranslation tNO on tNO.ProductId = p.ProductId and tNO.LanguageId = 1000001
left outer join product.tProductTranslation tDA on tDA.ProductId = p.ProductId and tDA.LanguageId = 1000002
left outer join product.tProductTranslation tFI on tFI.ProductId = p.ProductId and tFI.LanguageId = 1000003
END
GO
