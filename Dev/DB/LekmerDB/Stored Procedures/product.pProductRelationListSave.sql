SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/**********************  Version 2  **********************
User: Roman D.		Date: 16/01/2009		Time: 15:10
Description: deleted try - cath  block and error logging 
**********************************************************
***********************  Version 1  **********************
User: Roman G.		Date: 12/29/2008		Time: 11:33:20
Description: Created 
*/

CREATE PROCEDURE [product].[pProductRelationListSave]
	@ProductId		INT,
	@RelationListId INT
	
AS
BEGIN
	INSERT [product].[tProductRelationList]
	(				
		[ProductId],
		[RelationListId]
	)
	VALUES
	(
		@ProductId,
		@RelationListId
	)
END


GO
