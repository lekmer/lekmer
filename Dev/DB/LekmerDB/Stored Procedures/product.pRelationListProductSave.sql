SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



/*
*****************  Version 2  *****************
User: Yuriy P.		Date: 30.12.2008
Description:
			Edited (Remove ProductId list)
*/

CREATE procedure [product].[pRelationListProductSave]
	@RelationListId int,
	@ProductId		int
as
begin		
	insert 
		product.tRelationListProduct
	(
		RelationListId,
		ProductId
	)				
	values
	(
		@RelationListId,
		@ProductId
	)
end











GO
