SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagSave]
	@TagId		int,
	@TagGroupId int,
	@Value		nvarchar(255)
AS
BEGIN
	IF EXISTS (SELECT 1 FROM lekmer.tTag WHERE Value = @Value AND TagGroupId = @TagGroupId AND TagId <> @TagId)
		RETURN -1
	 
	UPDATE
		[lekmer].[tTag]
	SET
		[Value] = @Value
	WHERE
		[TagId] = @TagId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tTag]
		( 
			[TagGroupId],
			[Value]
		)
		VALUES
		(
			@TagGroupId,
			@Value
		)
		SET @TagId = scope_identity()						
	END 
	RETURN @TagId
END 

GO
