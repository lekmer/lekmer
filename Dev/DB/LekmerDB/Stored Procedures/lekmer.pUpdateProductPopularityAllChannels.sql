SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pUpdateProductPopularityAllChannels]

as
begin

declare @dateTo as datetime;
declare @dateFrom as datetime;
declare @channelId as int;
declare @channelTitle as varchar(80);

set @dateTo=GETDATE();
set @dateFrom = DATEADD(d,-30,@dateTo);

declare @cursor cursor
set @cursor = cursor for SELECT  [ChannelId], [Title]
  FROM [core].[tChannel];
  
  open @cursor

FETCH NEXT FROM @cursor 
INTO @channelId, @channelTitle;

While @@FETCH_STATUS = 0
begin
  
  
DECLARE	@return_value int

EXEC	@return_value = [lekmer].[pProductUpdatePopularity]
		@ChannelId = @channelId,
		@DateTimeFrom = @dateFrom,
		@DateTimeTo = @dateTo;

FETCH NEXT FROM @cursor 
INTO @channelId, @channelTitle;
	
END

End


GO
