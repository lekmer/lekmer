SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [test].[pCategoryInsert]
	@ParentCategoryId int,
	@Title nvarchar(50),
	@ErpId varchar(50)
as
begin
	insert into product.tCategory
	(
		ParentCategoryId,
		Title,
		ErpId
	)
	values
	(
		@ParentCategoryId,
		@Title,
		@ErpId
	)
	return scope_identity()
end
GO
