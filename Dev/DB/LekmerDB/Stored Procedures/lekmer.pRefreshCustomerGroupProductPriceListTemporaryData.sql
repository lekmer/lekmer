SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [lekmer].[pRefreshCustomerGroupProductPriceListTemporaryData] 
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM lekmer.tCustomerGroupProductPriceList
	
	INSERT	INTO lekmer.tCustomerGroupProductPriceList
				 (PriceListId,PriceListRegistryId,CurrencyId,ProductId,CustomerGroupId,PriceIncludingVat)
	SELECT PLI.PriceListId,
					PL.PriceListRegistryId,
					PL.CurrencyId,
					PLI.ProductId,
					CGP.CustomerGroupId,
					PLI.PriceIncludingVat
	FROM   product.tPriceListItem AS PLI
	       INNER JOIN product.tPriceList AS PL ON PLI.PriceListId = PL.PriceListId 
		   LEFT JOIN product.tPriceListCustomerGroup AS CGP ON PL.PriceListId = CGP.PriceListId
	WHERE  PL.PriceListStatusId = 0 AND
	       getdate() between coalesce(pl.StartDateTime, getdate()) and coalesce(pl.EndDateTime, getdate())
END

GO
