SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pVoucherConditionSave]
	@ConditionId int
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM lekmer.tVoucherCondition WHERE ConditionId = @ConditionId)
			RETURN
		
		INSERT lekmer.tVoucherCondition
		( 
			ConditionId
		)
		VALUES 
		(
			@ConditionId
		)
END
GO
