SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentNodeUrlLinkSave]
	@ContentNodeId	int,
	@LinkUrl		varchar(1000)
as
BEGIN
	update
		[sitestructure].[tContentNodeUrlLink]
	set
		[LinkUrl] = @LinkUrl
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		insert
			[sitestructure].[tContentNodeUrlLink]
		(
			[ContentNodeId],
			[LinkUrl]
		)
		values
		(
			@ContentNodeId,
			@LinkUrl
		)
	end
end
GO
