SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 15:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentPageDelete]
@ContentNodeId	int
as
BEGIN
	UPDATE product.tProductModuleChannel
	SET ProductTemplateContentNodeId = NULL 
	where ProductTemplateContentNodeId = @ContentNodeId
	
	UPDATE [order].tOrderModuleChannel
	SET OrderTemplateContentNodeId = NULL 
	where OrderTemplateContentNodeId = @ContentNodeId
	
	UPDATE sitestructure.tSiteStructureRegistry
	SET StartContentNodeId = NULL
	where StartContentNodeId = @ContentNodeId
		
	UPDATE product.tCategorySiteStructureRegistry
	SET ProductParentContentNodeId = NULL 
	where ProductParentContentNodeId = @ContentNodeId
	
	UPDATE product.tCategorySiteStructureRegistry
	SET ProductTemplateContentNodeId = NULL 
	where ProductTemplateContentNodeId = @ContentNodeId
	
	UPDATE product.tProductSiteStructureRegistry
	SET ParentContentNodeId = NULL 
	where ParentContentNodeId = @ContentNodeId
	
	UPDATE product.tProductSiteStructureRegistry
	SET TemplateContentNodeId = NULL 
	where TemplateContentNodeId = @ContentNodeId
	
	
	delete
		[sitestructure].[tContentPageTranslation]
	where
		[ContentNodeId] = @ContentNodeId

	delete
		[sitestructure].[tContentPage]
	where
		[ContentNodeId] = @ContentNodeId
end
GO
