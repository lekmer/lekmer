SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







/*
*****************  Version 1  *****************
User: Egorov V.		Date: 26.01.2009		Time: 21:11
Description:
			Created 

*/

CREATE procedure [template].[pIncludeFolderAdd]
	@ParentIncludeFolderId int,
	@Title				nvarchar(50)
	
as
begin
	set nocount on	

	if exists (select 1 from [template].[tIncludeFolder] where [Title] = @Title And ((@ParentIncludeFolderId IS NULL And ParentIncludeFolderId IS NULL) OR ParentIncludeFolderId = @ParentIncludeFolderId))
		return -1

	insert into [template].[tIncludeFolder]
	(
		[Title],
		[ParentIncludeFolderId]
	)
	values
	(
		@Title,
		@ParentIncludeFolderId
	)
	return scope_identity()
end

GO
