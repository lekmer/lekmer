SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Name
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [lekmer].[pRefreshProductListingTemporaryData] 
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE FROM lekmer.tAllProductTempTable
	
	INSERT	INTO lekmer.tAllProductTempTable
				 (ProductId,PriceListId,ChannelId,CurrencyId,PriceListRegistryId)
			SELECT p.[ProductId],pli.[PriceListId],Ch.ChannelId,Ch.CurrencyId,PMC.PriceListRegistryId
			FROM
				product.tProduct AS p
				INNER JOIN product.tProductRegistryProduct AS PRP ON P.ProductId = PRP.ProductId
				INNER JOIN product.tProductModuleChannel AS PMC	ON PRP.ProductRegistryId = PMC.ProductRegistryId
				INNER JOIN core.tChannel AS Ch ON PMC.ChannelId = Ch.ChannelId
				INNER JOIN [lekmer].[tLekmerProduct] lp ON p.[ProductId] = lp.[ProductId]
				INNER JOIN lekmer.[tProductUrl] AS pu ON lp.[ProductId] = pu.[ProductId] AND ch.[LanguageId] = pu.[LanguageId]
				INNER JOIN product.tPriceListItem AS pli
					ON pli.[ProductId] = P.ProductId
					AND pli.[PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(Ch.CurrencyId, P.ProductId, PMC.PriceListRegistryId, null)
			WHERE
				P.IsDeleted = 0
				AND P.ProductStatusId = 0
END

GO
