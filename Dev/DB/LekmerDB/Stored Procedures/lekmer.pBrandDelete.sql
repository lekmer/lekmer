SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandDelete]
	@BrandId	int
as
BEGIN
	DELETE FROM 
		lekmer.tBlockBrandListBrand
	WHERE 
		[BrandId] = @BrandId
	
	DELETE FROM 
		lekmer.tBlockBrandProductListBrand
	WHERE 
		[BrandId] = @BrandId
		
	DELETE FROM
		lekmer.tBlockProductFilterBrand
	WHERE 
		[BrandId] = @BrandId	
	
	Update lekmer.tLekmerProduct set BrandId = NULL 
	where BrandId = @BrandId
	
	DELETE FROM 
		lekmer.tBrandTranslation
	WHERE
		[BrandId] = @BrandId
			
	DELETE FROM 
		lekmer.tBrand
	WHERE 
		[BrandId] = @BrandId
END


GO
