SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeGetAll]
AS 
BEGIN 
	SELECT 
		ProductId,
		SizeId
	FROM 
		[lekmer].[tProductSize]
	WHERE
		NumberInStock > 0
END
GO
