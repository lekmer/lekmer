SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ImportUpdateProductLekmer]

AS
BEGIN
--SET NOCOUNT ON 
		--raiserror('errorMSG', 16, 1)
		-- tCategory proc kallas
		print 'Start tCategory'
		exec [integration].[usp_UpdateTCategoryLekmer] -- skapar om artiklar om det behövs
		print 'End tCategory'
		print 'Start tUpdateBrand'
		exec [integration].usp_UpdateBrandLekmer
		exec [integration].[usp_UpdateBrandLekmerCreationDate] -- adds the date the brand was created
		print 'End tUpdateBrand'

		
		-- tProduct
		print 'Start update product'
		UPDATE 
			p
		SET 
			--Title = tp.ArticleTitle, 
			--ItemsInPackage = null,
			EanCode = tp.EanCode, 
			--IsDeleted = 0, 
			--ColorId SAKNAS
			--SizeId SAKNAS
			NumberInStock = tp.NoInStock,
			-- Category kanske måste ändras om det skapar problem
			-- man ändrar den då till typ
			-- select catagoryId from tcategory where erpid = varukl+varugrupp+varukod 
			CategoryId = (select categoryId from product.tCategory where ErpId = 'C_'+tp.ArticleClassId+'-'+tp.ArticleGroupId+'-'+tp.ArticleCodeId)
			--CategoryId = tp.ArticleCodeId,	--varukodID det är den mest specifika!!
			--WebShopTitle = null,
			--[Description] = '',
			--ShortDescription = null,
			--MediaId = null, -- Måste sättas
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end		
		-- select lp.Hyerpid, tp.noinstock, p.numberinstock
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,12)	
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tPriceListItem pli  --[product].tProductRegistryProduct (i remove products from tRegistryProduct)
					on pli.ProductId = lp.ProductId and
					pli.PriceListId = 1--substring(tp.HYarticleId, 3,1)
		WHERE 
			substring(tp.HYarticleId, 3,1) = 1 
			and
			--(p.Title <> tp.ArticleTitle OR
			(p.EanCode <> tp.EanCode OR
			p.NumberInStock <> tp.NoInStock)
		 
		print 'End product update'
		
		---------------------------------------------
		print 'Start lekmer product update'
		exec integration.usp_ImportUpdateLekmerProduct
		print 'End lekmer product update'
	--------------------------------------		
		-- tPriceListItem - Update Prices from integration.tempProductPrice
		print 'Start price update'
		exec [integration].[usp_UpdateProductPRICELekmer]
		print 'End price update'
	--------------------------------------
	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@EanCode nvarchar(50),
				@NoInStock nvarchar(250),
				@ArticleTitle nvarchar(250),
				@ProductId int,
				@HYBrandId nvarchar(25),
				
				@IsBookable bit,
				@ExpectedBackInStock datetime,
				@IsDeleted bit,
				@CategoryId int,
				@Description nvarchar(250),
				@Price decimal,
				@VaruklassId nvarchar(10),
				@VarugruppId nvarchar(10),
				@VarukodId nvarchar(10),
				@LekmerErpId nvarchar(25), -- new
				
				@Data nvarchar(4000),
				@Vat decimal
				SET @Vat = 25.0
				SET @IsDeleted = 0
				SET @Description = ''
				SET @ExpectedBackInStock = null
				SET @IsBookable = 0
		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				------------------------------------
		
		declare cur_product cursor fast_forward for
			select
				tp.HYarticleId,
				tp.EanCode,
				tp.NoInStock,
				tp.ArticleTitle,
				(tp.Price/100),
				tp.ArticleClassId,
				tp.ArticleGroupId,
				tp.ArticleCodeId,
				tp.BrandId,
				tp.LekmerArtNo
				-- select *
			from
				[integration].tempProduct tp
			where not exists(select 1
						-- parar lekmerErpId med temp tabell erpId (klipper fok) sen parar
						-- tProductregistry på lekmerProductId på det ErpId sen parar också foken med productRegProduct
							from lekmer.tLekmerProduct lp, product.tPriceListItem prp
							where lp.HYErpId = substring(tp.HYarticleId, 5,12) and
							lp.ProductId = prp.ProductId and
							substring(tp.HYarticleId, 3,1) = prp.PriceListId)
							
			-- VIKTIGT ändrade till product.tPriceListItem från product.tProductRegistryProduct
			-- i och med att den mådas då HY används felaktigt
			
			
		--print 'Open Cursor'
		open cur_product
		fetch next from cur_product
			into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId,
				 @LekmerErpId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)
				SET @HYarticleSize = substring(@HYarticleId, 18,3)
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,12)			
				
				
				-- if, kolla om eprid utan fok finns, om det gör hämta dens produktid och lägg in de produkt idet + fok
				-- i tproductRegistryProduct	
				
				if not exists(select 1 -- om erpid inte finns i tlekmerProduct finns det inte i nån fok, lägg in den då, annars lägg bara in i pReg
							from lekmer.tLekmerProduct lp
							where lp.HYErpId = @HYarticleIdNoFokNoSize)
				begin	
					print 'insert into product.tproduct'
					INSERT INTO -- MediaId saknas
						[product].tProduct(ErpId, EanCode, IsDeleted, NumberInStock, CategoryId, Title, [Description], ProductStatusId)
					SELECT 
						@HYarticleIdNoFokNoSize, -- @HYArticleId
						@EanCode, 
						@IsDeleted, --0
						@NoInStock, 
						(SELECT CategoryId FROM product.tCategory WHERE ErpId = 'C_'+@VaruklassId+'-'+@VarugruppId+'-'+@VarukodId), -- koll här
						@ArticleTitle, 
						@Description, --''
						1--case when @NoInStock > 0 then 0 else 1 end --(Ändrat alla produkter sätts som offline när de sätts in)
						-- 2 = ready to translate
												
					set @ProductId = SCOPE_IDENTITY()
					
					-- Data that was sent into insert
					SET @Data = 'NEW: HYArticleId ' + @HYarticleId + ' EanCode ' + @EanCode + ' ProductId' + CAST(@ProductId as varchar(10))
		
					--IF @nrInStock > 0 -- om antal är mer än 0
					--begin
						--SET @IsBookable = 0
					--end
						
						
					insert into lekmer.tLekmerProduct(ProductId, HYErpId, BrandId, IsBookable, AgeFromMonth, AgeToMonth, 
								IsNewFrom, IsNewTo, IsBatteryIncluded, ExpectedBackInStock, LekmerErpId)
					values(
					@ProductId, 
					@HYarticleIdNoFokNoSize, --@HYarticleId 
					(Select b.BrandId from lekmer.tBrand b where b.ErpId = @HYBrandId),
					@IsBookable, 
					0, 
					0, 
					Convert(varchar, GETDATE(), 112),
					DATEADD(WK, 4, Convert(varchar, GETDATE(), 112)), 
					0, 
					@ExpectedBackInStock,
					@LekmerErpId)
					
					
					---- 
					-- TradeDoublerMappning på produktnivår
					
					DECLARE @TradeDoublerProductGroup nvarchar (50)
					DECLARE @tFok INT
					SET @tFok = CAST(@Fok AS INT)
					SET @TradeDoublerProductGroup = (SELECT TOP 1 ProductGroupId 
														FROM integration.tTradeDoublerProductGroupMapping
															WHERE HYArticleClassId = 'C_'+@VaruklassId
															AND ChannelId = @tFok)										
					
					IF @TradeDoublerProductGroup IS NOT NULL AND @tFok > 0
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping 
							(
								ProductId, 
								ProductGroupId, 
								ChannelId
							)
						SELECT
							@ProductId,
							@TradeDoublerProductGroup,
							@tFok
						WHERE
							NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping m
										WHERE m.ProductId = @ProductId AND m.ChannelId = @tFok)
							
						
					END 
					
					
					-- tPriceListItem
					-- pricelist ID är 1 ska ändras beroende på channel
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					select
						@Fok, 
						@ProductId, 
						@Price, 
						((@Price) / (1.0+(@Vat/100.0))), 
						@Vat
					where
						not exists (select 1 from product.tPriceListItem
										where ProductId = @ProductId
										and PriceListId = @Fok)
					--values(@Fok, @ProductId, @Price, ((@Price) / (1.0+(@Vat/100.0))), @Vat)
					
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					select
						@ProductId,
						@Fok
					where
						not exists (select 1 from product.tProductRegistryProduct
										where ProductId = @ProductId
										and ProductRegistryId = @Fok)
					--values(@ProductId, @Fok) -- Fok är färsäljningskanalen
		
					--commit
				end	
				else if not exists (select 1
							from lekmer.tLekmerProduct lp, product.tPriceListItem prp
							where lp.HYErpId = @HYarticleIdNoFokNoSize
							and lp.ProductId = prp.ProductId 
							and prp.PriceListId = @Fok)
							
						-- VIKTIGT ändrade till product.tPriceListItem från product.tProductRegistryProduct
						-- i och med att den mådas då HY används felaktigt
				begin
					select @ProductId = ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK
					set @Data = 'EXISTING: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' ProductId' + CAST(@ProductId as varchar(10))
			
					-- tProductRegistryProduct
					insert into product.tProductRegistryProduct (ProductId, ProductRegistryId)
					select
						@ProductId,
						@Fok
					where
						not exists (select 1 from product.tProductRegistryProduct
										where ProductId = @ProductId
										and ProductRegistryId = @Fok)
					--values(@ProductId, @Fok)
					
					-- tPriceListItem
					insert into product.tPriceListItem (PriceListId, ProductId, PriceIncludingVat, PriceExcludingVat, VatPercentage)
					select
						@Fok,
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						@Price,
						(@Price / (1.0+(@Vat/100.0))), 
						@Vat
					where
						not exists (select 1 from product.tPriceListItem
										where ProductId = (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK)
										and PriceListId = @Fok)
										
					--values(@Fok, (select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFoK),
					--@Price, (@Price / (1.0+(@Vat/100.0))), @Vat)
					
					--SET @Data = 'HYArticleId ' + @HYarticleId + 'EanCode ' + @EanCode + 'CategoryId ' +cast(@CategoryId as nvarchar)
					
					-- TradeDoublerMappning på produktnivår
					
					DECLARE @TradeDoublerProductGroupp nvarchar (50)
					DECLARE @tFokk INT
					SET @tFokk = CAST(@Fok AS INT)
					SET @TradeDoublerProductGroupp = (SELECT TOP 1 ProductGroupId 
														FROM integration.tTradeDoublerProductGroupMapping
															WHERE HYArticleClassId = 'C_'+@VaruklassId
															AND ChannelId = @tFokk)										
					
					IF @TradeDoublerProductGroupp IS NOT NULL AND @tFokk > 0
					BEGIN
						INSERT INTO lekmer.tTradeDoublerProductGroupMapping 
							(
								ProductId, 
								ProductGroupId, 
								ChannelId
							)
						SELECT
							@ProductId,
							@TradeDoublerProductGroupp,
							@tFokk
						WHERE
							NOT EXISTS (SELECT 1 FROM  lekmer.tTradeDoublerProductGroupMapping m
										WHERE m.ProductId = @ProductId AND m.ChannelId = @tFokk)
					END
					
				end
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_product into @HYArticleId,
				 @EanCode,			 
				 @NoInStock,
				 @ArticleTitle,
				 @Price,
				 @VaruklassId,
				 @VarugruppId,
				 @VarukodId,
				 @HYBrandId,
				 @LekmerErpId
		end
		
		close cur_product
		deallocate cur_product
		
		exec [integration].[usp_ImportUpdateProductSizesLekmer]
		exec [integration].[usp_UpdateProductBrandLekmer]
		--exec [integration].[usp_FokRestrictionsProducts] -- Outdated, soon to be removed, [usp_ChannelRestrictionsProducts] in its place
		exec [integration].[usp_ChannelRestrictionsProducts]
		exec [integration].[usp_REATagProduct]
	
END
GO
