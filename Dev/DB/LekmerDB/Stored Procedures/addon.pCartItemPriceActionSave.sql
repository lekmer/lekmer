SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceActionSave]
	@CartActionId		int,
	@MaxQuantity		int,
	@IncludeAllProducts	bit
AS 
BEGIN 
	UPDATE
		addon.tCartItemPriceAction
	SET
		MaxQuantity = @MaxQuantity,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			addon.tCartItemPriceAction
		(
			CartActionId,
			MaxQuantity,
			IncludeAllProducts
		)
		VALUES
		(
			@CartActionId,
			@MaxQuantity,
			@IncludeAllProducts
		)
	END
END 
GO
