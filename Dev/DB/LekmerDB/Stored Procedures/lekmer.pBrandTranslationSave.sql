SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandTranslationSave]
@BrandId		int,
@LanguageId		int,
@Title			nvarchar(500),
@Description	nvarchar(max)
AS
BEGIN
	SET nocount ON
	
	UPDATE
		lekmer.tBrandTranslation
	SET
		[Title] = @Title,
		[Description] = @Description
	WHERE
		[BrandId] = @BrandId AND
		[LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO lekmer.tBrandTranslation
		(
			[BrandId],
			[LanguageId],
			[Title],
			[Description]				
		)
		VALUES
		(
			@BrandId,
			@LanguageId,
			@Title,
			@Description
		)
	END
END




GO
