SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartValuesGetByConditionId]
	@ConditionId	int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCartValueConditionCurrency
	WHERE 
		[CartValueConditionCurrency.ConditionId] = @ConditionId
end

GO
