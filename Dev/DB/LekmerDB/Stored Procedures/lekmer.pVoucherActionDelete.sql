SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pVoucherActionDelete]
	@CartActionId int
AS 
BEGIN
	
	DELETE FROM [lekmer].tVoucherAction
	WHERE CartActionId = @CartActionId
END
GO
