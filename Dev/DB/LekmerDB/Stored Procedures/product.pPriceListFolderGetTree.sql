SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListFolderGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[PriceListFolder.Id],
			v.[PriceListFolder.ParentPriceListFolderId],
			v.[PriceListFolder.Title]
		FROM product.vCustomPriceListFolder AS v
		WHERE v.[PriceListFolder.ParentPriceListFolderId] IS NULL
		
	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[PriceListFolder.Id],
					v.[PriceListFolder.ParentPriceListFolderId],
					v.[PriceListFolder.Title]
				FROM product.vCustomPriceListFolder AS v
				WHERE v.[PriceListFolder.ParentPriceListFolderId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[PriceListFolder.ParentPriceListFolderId]
									 FROM product.vCustomPriceListFolder AS v
									 WHERE v.[PriceListFolder.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT
							v.[PriceListFolder.Id],
							v.[PriceListFolder.ParentPriceListFolderId],
							v.[PriceListFolder.Title]
						FROM product.vCustomPriceListFolder AS v
						WHERE v.[PriceListFolder.ParentPriceListFolderId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM product.vCustomPriceListFolder AS v
									WHERE v.[PriceListFolder.ParentPriceListFolderId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY ParentId ASC
END

GO
