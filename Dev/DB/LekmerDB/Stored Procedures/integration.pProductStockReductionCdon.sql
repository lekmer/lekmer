SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductStockReductionCdon]
	@ProductId int
AS
BEGIN			
	begin
		begin try
			begin transaction
							
				update
					p
				set
					p.NumberInStock = (p.NumberInStock - 1)
					-- select *
				from 
					product.tProduct p
					left join lekmer.tProductSize s
						on p.ProductId = s.ProductId
				where
					s.ProductId is null
					and p.ProductId = @ProductId
					
			commit		
		end try
		begin catch
			if @@TRANCOUNT > 0 rollback
			-- LOG here
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
			values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		end catch
		
	end
END

GO
