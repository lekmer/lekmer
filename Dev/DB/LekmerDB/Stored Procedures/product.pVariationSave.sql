SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pVariationSave]
	@VariationId		int,
	@VariationGroupId	int,
	@VariationTypeId	int,	
	@Value				nvarchar(50),
	@Ordinal			int
as
begin
	
	insert into product.[tVariation]
	(
		[VariationGroupId],
		[VariationTypeId],
		[Value],
		[Ordinal]
	)
	values
	(
		@VariationGroupId,
		@VariationTypeId,
		@Value,
		@Ordinal
	)
	return scope_identity();

end







GO
