SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [statistics].[pOrderGetSummary]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime,
	@Alternate		bit,
	@StatusIds		varchar(max)
AS 
BEGIN	
	SELECT 
		isnull(count([OrderId]), 0)
	FROM
		[order].[tOrder] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [UsedAlternate] = @Alternate
		AND [CreatedDate] BETWEEN @DateTimeFrom AND @DateTimeTo
		AND OrderStatusId IN (SELECT ID FROM generic.fnConvertIDListToTable(@StatusIds, ','))
END
GO
