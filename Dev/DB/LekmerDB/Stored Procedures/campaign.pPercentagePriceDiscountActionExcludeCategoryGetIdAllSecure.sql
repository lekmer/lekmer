SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pPercentagePriceDiscountActionExcludeCategoryGetIdAllSecure]
	@ActionId		int
AS
BEGIN
	SELECT 
		c.CategoryId
	FROM 
		campaign.tPercentagePriceDiscountActionExcludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.ProductActionId = @ActionId
END
GO
