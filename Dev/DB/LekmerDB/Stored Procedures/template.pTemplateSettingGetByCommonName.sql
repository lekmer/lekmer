SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 4  *****************
User: Roman D.		Date: 24.12.2008		Time: 15:00
Description:
			added inner join for field TypeName
************************************************
*****************  Version 3  *****************
User: Roman D.		Date: 23.12.2008		Time: 15:10
Description:
			added ModelSettingTypeId
************************************************
*****************  Version 2  *****************
User: Roman G.		Date: 22.12.2008		Time: 12:30
Description:
			Remove TemplateSettingId
************************************************

*****************  Version 1  *****************
User: Victor E.		Date: 19.12.2008		Time: 11:30
Description:
			Created 
*/

CREATE PROCEDURE [template].[pTemplateSettingGetByCommonName]
	@TemplateId int,
	@CommonName	varchar(50) 
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomTemplateSetting]
	where
		[TemplateSetting.TemplateId] = @TemplateId
		and	[ModelSetting.CommonName] = @CommonName
end

GO
