SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetByType]
@CommonName varchar(50)
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageSecure]
	WHERE 
	[ContentPageType.CommonName] = @CommonName
end

GO
