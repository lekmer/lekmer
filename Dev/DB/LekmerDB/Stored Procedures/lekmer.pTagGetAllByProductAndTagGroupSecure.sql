SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByProductAndTagGroupSecure]
	@ProductId	int,
	@TagGroupId int
AS
BEGIN 
	SET nocount ON

	SELECT 
		t.*
	FROM 
		lekmer.[vTagSecure] t INNER JOIN
		lekmer.[vProductTag] pt ON pt.[ProductTag.TagId] = t.[Tag.TagId] 
	WHERE
		pt.[ProductTag.ProductId] = @ProductId AND 
		t.[Tag.TagGroupId] = @TagGroupId
END 

GO
