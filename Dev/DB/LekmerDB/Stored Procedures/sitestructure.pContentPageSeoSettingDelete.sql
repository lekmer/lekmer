SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 15:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentPageSeoSettingDelete]
@ContentNodeId	int
as
begin
	delete [sitestructure].[tContentPageSeoSettingTranslation] where [ContentNodeId] = @ContentNodeId

	delete [sitestructure].[tContentPageSeoSetting] where [ContentNodeId] = @ContentNodeId
end
GO
