SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsNO]

AS
begin
	set nocount on
	begin try
		begin transaction				
		
		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n --3
		where 
			n.SiteStructureRegistryId = 2
			and n.ContentNodeTypeId = 3 -- detta är contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Leketøy online fra Lekmer.no – leketøy på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. Vi tilbyr et stort utvalg av barneleker fra kjente '
			+ 'varemerker og levererer dem på 3-5 dager - Lekmer.no din leketøysbutikk. '
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 2
			and cn.ParentContentNodeId = 1004533
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Leketøy online fra Lekmer.no – leketøy på nettet.',
			
			cps.[Description] = 'Kjøp ' + x.Title + ' ' + cn.Title + ' på nettet. Vi tilbyr et stort utvalg av barneleker '
			+ 'fra kjente varemerker og levererer dem på 3-5 dager - Lekmer.no din leketøysbutikk.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1004533) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 2		
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		
		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
										
		UPDATE
			cps
		SET
			cps.Title = 'Barn og Baby ' + cn.Title + ' produkter online fra Lekmer.no – Kjøp barnartikler på nettet.',
			
			cps.[Description] = 'Kjøp Barn og Baby produkter på nettet. Få dine barn- og babyprodukter levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 2
			and cn.ParentContentNodeId = 1004415
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barn og Baby produkter online fra Lekmer.no – Kjøp barneartikler på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title  + ' Barn og Baby produkter på nettet. Få dine barn- og '
			+ 'babyprodukter levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1004415) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 2		
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
			
		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Barneklær online fra Lekmer.no – Barn og Babyklær på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. Stort utvalg av kule klær til barn og baby – '
			+ 'handle dine barneklær online og få dem levert innen 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 2
			and cn.ParentContentNodeId = 1007422
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barneklær online fra Lekmer.no – Barn og babyklær på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dine barneklær levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1007422) x 
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 2		
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------			HÄR NUUU							
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Interiør online fra Lekmer.no Barneromsinnredning på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. Få ditt barnerom levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 2
			and cn.ParentContentNodeId = 1004480
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Interiør online fra Lekmer.no – Interiør på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' og ' + x.Title + ' på nettet. Få ditt barnerom levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1004480) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 2			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------							
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' og Underholdning og barnespill online fra Lekmer.no – Tv-spill på nettet.',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' på nettet. Få dine tv-spill levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 2
			and cn.ParentContentNodeId = 1004932
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' ',
			
			cps.[Description] = 'Kjøp ' + cn.Title + ' og ' + x.Title + ' på nettet. Få dine tv-spill levert på 3-5 dager fra Lekmer.no.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1004932) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 2		
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
						
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end

GO
