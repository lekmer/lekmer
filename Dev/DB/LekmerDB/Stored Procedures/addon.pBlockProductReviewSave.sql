SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [addon].[pBlockProductReviewSave]
         @BlockId              int,
         @RowCount                  int
AS
BEGIN
         UPDATE
                   [addon].[tBlockProductReview]
         SET
                   [RowCount] = @RowCount
         WHERE
                   [BlockId] = @BlockId
         IF @@ROWCOUNT = 0
         BEGIN
                   INSERT
                            [addon].[tBlockProductReview]
                            (
                                      [BlockId],
                                      [RowCount]
                            )
                   VALUES
                            (
                                      @BlockId,
                                      @RowCount
                            )
         END
END





GO
