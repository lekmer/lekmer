SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsFI]

AS
begin
	set nocount on
	begin try
		begin transaction				
		
		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n --3
		where 
			n.SiteStructureRegistryId = 4
			and n.ContentNodeTypeId = 3 -- detta är contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ja lelut netistä. Lekmer.fi – lelukauppa netissä.',
			
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Tarjoamme laajan leluvalikoiman ja tunnetut tuottemerkit. '
			+ 'Toimitusaika on 3-5 arkipäivää - Lekmer.fi sinun lelukauppasi.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 4
			and cn.ParentContentNodeId = 1005110
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Lelut netistä. Lekmer.fi – lelut netissä.',
			
			cps.[Description] = 'Osta ' + x.Title + ' ' + cn.Title + ' '
			+ 'Tarjoamme laajan leluvalikoiman ja tunnetut tuottemerkit. Toimitusaika on 3-5 arkipäivää - Lekmer.fi sinun lelukauppasi.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1005110) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 4
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		
		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
										
		UPDATE
			cps
		SET
			cps.Title = 'Kun tarvitset ' + cn.Title + ' lastasi, Lapsi ja vauva '
			+ 'Lekmer.fi - verkkokaupasta – Osta lastentuotteet netistä.',
			
			cps.[Description] = 'Osta ' + cn.Title + ' kaikki tarvikkeet netistä. Saa lasten- ja vauvantuotteet '
			+ 'toimitettua 3-5 arkipäivässä Lekmer.fi-verkkokaupasta. '
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 4
			and cn.ParentContentNodeId = 1004990
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ja muut tuotteet lapsen ' + cn.Title + ' ja istumiseen. '
			+ 'Kaikki Lapsi ja vauva netistä Lekmer.fi - verkkokaupasta - Osta lastentarvikkeet netistä.',
			
			cps.[Description] = 'Osta ' + x.Title + ' ' + cn.Title  + ' netistä. Tarjoamme laajan leluvalikoiman ja tunnetut tuottemerkit. Toimitusaika on 3-5 arkipäivää - Lekmer.fi sinun lelukauppasi.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1004990) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 4
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
			
		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ja lastenvaatteet Lekmer.fi - verkkokaupasta – Lasten - ja vauvanvaatteet netistä.',
			
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Laaja valikoima makeita lasten- ja vauvanvaatteita – '
			+ 'osta lastenvaatteet netistä, Lekmer.fi-verkkokaupasta ja saa ne toimitettua 3-5 arkipäivän sisällä. '
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 4
			and cn.ParentContentNodeId = 1007276 
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Sisustus netistä. Lekmer.fi - Lasten- ja vauvanvaatteet netistä.',
			
			cps.[Description] = 'Osta ' + x.Title + ' ja ' + cn.Title + ' '
			+ 'netistä. Saa Lekmer.fi - verkkokaupasta lastenvaatteet toimitettua 3-5 arkipäivässä.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1007276) x 
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 4
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Lastenhuone netistä. Lekmer.fi Lastenhuoneen sisustustuotteet netistä.',
			
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Saa kaikki lastenhuoneen tarvikkeet toimitettua Lekmer.fi-verkkokaupasta 3-5 arkipäivässä'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 4
			and cn.ParentContentNodeId = 1005059
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Lastenhuone netistä. Lekmer.fi – Lastenhuoneen tarvikkeet netistä.',
			
			cps.[Description] = 'Osta ' + x.Title + ' ja ' + cn.Title + ' '
			+ 'netistä. Saa lastenhuoneen tarvikkeet toimitettua Lekmer.fi - verkkokaupasta 3-5 arkipäivässä.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1005059) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 4		
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------							
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' peli ja viihde sekä lastenpelit netistä. Lekmer.fi – Tv-pelit netistä',
			
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Saa tv-pelit toimitettua Lekmer.fi-verkkokaupasta 3-5 arkipäivässä.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 4
			and cn.ParentContentNodeId = 1005510
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' netistä. Lekmer.fi- Lautapelit netistä',
			
			cps.[Description] = 'Osta ' + cn.Title + ' ja ' + x.Title + ' netistä. Saa pelit toimitettua Lekmer.fi-verkkokaupasta 3-5 arkipäivässä.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1005510) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 4
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
						
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end

GO
