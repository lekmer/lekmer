SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBrandGetAllByBlock]
	@ChannelId		int,
	@BlockId		int,
	@Page			int = NULL,
	@PageSize		int
AS
BEGIN
	SET NOCOUNT ON
		
	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)
	
	DECLARE @blockIncludeAllBrands bit
	SELECT 
		@blockIncludeAllBrands = IncludeAllBrands 
	FROM 
		lekmer.tBlockBrandList
	WHERE 
		BlockId = @BlockId
	
	IF (@blockIncludeAllBrands = 1)
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY cast(lower([b].[Brand.Title]) AS binary) ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[vBrand] b
				WHERE 
					[b].[ChannelId] = ' +  CAST(@ChannelId AS varchar(10)) + '
			)'
		END
	ELSE
		BEGIN
			SET @sqlFragment = '
			(
				SELECT 
					ROW_NUMBER() OVER (ORDER BY [bb].[Ordinal] ASC) AS Number,
					[b].*
				FROM 
					[lekmer].[tBlockBrandListBrand] bb
					INNER JOIN [lekmer].[vBrand] b ON [bb].[BrandId] = [b].[Brand.BrandId]
				WHERE
					[bb].[BlockId] = '+  CAST(@BlockId AS varchar(10)) +'
					AND [b].[ChannelId] = ' +  CAST(@ChannelId AS varchar(10)) + '
			)'
		END

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	EXEC sp_executesql @sqlCount,
		N'	@ChannelId				INT, 
			@BlockId				INT',
			@ChannelId,
			@BlockId
			     
	EXEC sp_executesql @sql, 
		N'	@ChannelId				INT, 
			@BlockId				INT',
			@ChannelId,
			@BlockId
END
GO
