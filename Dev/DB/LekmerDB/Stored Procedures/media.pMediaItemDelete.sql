SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-25
-- Description:	Deletes MediaItem by Id
-- =============================================
CREATE PROCEDURE [media].[pMediaItemDelete]
	@MediaId INT
AS
BEGIN
	SET NOCOUNT ON
	DELETE 
	FROM   [media].[tMedia]
	WHERE  [MediaId] = @MediaId
END
GO
