SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pBlockProductImageListSave]
	@BlockId				int,
	@ColumnCount			int,
	@RowCount				int,
	@ProductImageGroupId	int
as
begin
	update
		[product].[tBlockProductImageList]
	set
		[ColumnCount]			= @ColumnCount,
		[RowCount]				= @RowCount,
		[ProductImageGroupId]	= @ProductImageGroupId
	where
		[BlockId]		= @BlockId
		
	if  @@ROWCOUNT = 0
		begin
			insert
				[product].[tBlockProductImageList]
				(
					[BlockId],
					[ColumnCount],
					[RowCount],
					[ProductImageGroupId]
				)
			values
				(
					@BlockId,
					@ColumnCount,
					@RowCount,
					@ProductImageGroupId
				)
		end
end
GO
