SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateProductSeoFI]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Produktnivå: Start Leksaker>
		------------------------------------------------------------
		
		-- DEFAULT --
		declare @LanguageId int
		set @LanguageId = 1000003 -- Finland
		
		INSERT INTO product.tProductSeoSettingTranslation(ProductId, LanguageId)
		SELECT
			p.ProductId,
			@languageId
		FROM
			product.tProduct p
		WHERE
			not exists (SELECT 1
							FROM product.tProductSeoSettingTranslation n
							WHERE n.ProductId = p.ProductId and
							n.LanguageId = @LanguageId)
							


		UPDATE
			pss
		SET
			pss.Title = 'Osta ' + isnull(pt.Title, p.Title) + ' – Lekmer.fi - verkkokaupasta' ,
			
			pss.[Description] = 'Osta ' + isnull(ct.Title, c.Title) 
			+ ' toimintahahmo netistä. Löydät myös muut lelut '
			+ 'ja lastentarvikkeet valmistajalta '+ isnull(bt.Title, b.Title) 
			+ ' Lekmer.fi - verkkokaupasta.' 
			--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
		FROM 
			product.tProductSeoSettingTranslation pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				left join product.tProductTranslation pt
					on p.ProductId = pt.ProductId
					and pt.LanguageId = @LanguageId							
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				left join lekmer.tBrandTranslation bt
					on bt.BrandId = b.BrandId
					and bt.LanguageId = @LanguageId	
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				inner join product.tCategoryTranslation ct
				on c.CategoryId = ct.CategoryId
				
				inner join product.tCategoryTranslation ct2
				on c2.CategoryId = ct2.CategoryId							
		WHERE
			c3.CategoryId = 1000533 -- leksaker
			and pss.LanguageId = @LanguageId
			and ct.LanguageId = @LanguageId
			and ct2.LanguageId = @LanguageId
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))
				
	
	------------------------------------------------------------
	-- <Produktnivå: Start Barn & Baby>
	------------------------------------------------------------
	UPDATE
		pss
	SET
		pss.Title = isnull(pt.Title, p.Title) + ' - Lekmer.fi – Osta lastentarvikkeet netistä' ,
		
		pss.[Description] = 'Osta netistä ' + isnull(pt.Title, p.Title) 
		+ ' ' + isnull(bt.Title, b.Title) + ' Löydät muut ' + isnull(bt.Title, b.Title)
		+ ' lasten- ja vauvantuotteet verkkokaupasta Lekmer.fi' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1000445 -- barn och baby
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	
	------------------------------------------------------------
	-- <Produktnivå: Start Barnkläder>  
	------------------------------------------------------------
	UPDATE
		pss
	SET   
		pss.Title = isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct.Title, c.Title) + ' ' 
		+ isnull(ct2.Title, c2.Title) 
		+ ' - Lekmer.fi – Lasten- ja vauvanvaatteet netistä. Lekmer.fi - Lasten- ja vauvanvaatteet netistä.',
		
		pss.[Description] = 'Osta ' + isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct2.Title, c2.Title) + ' ja ' 
		+ isnull(ct.Title, c.Title) + ' '
		+ 'netistä. Löydät myös Lekmer.fi- verkkopaupasta muita lastenvaatteita samalta tuottajalta, ' 
		+ isnull(bt.Title, b.Title)
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1001310 -- barnkläder
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
	
	
	
	------------------------------------------------------------
	-- <Produktnivå: Start Inredning>  
	------------------------------------------------------------
	UPDATE
		pss
	SET  
		pss.Title = isnull(pt.Title, p.Title) + ' netistä. Lekmer.fi - verkkokaupasta',
		
		pss.[Description] = 'Osta ' + isnull(pt.Title, p.Title) 
		+ ' netistä. Löydät myös muut ' + isnull(bt.Title, b.Title) + ' ' 
		+ 'sisustustuotteet ja lastenvaatteet Lekmer.fi - kaupasta.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId	
			inner join product.tCategoryTranslation ct3
			on c3.CategoryId = ct3.CategoryId			
	WHERE
		c3.CategoryId = 1000494 -- inredning
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	
	------------------------------------------------------------
	-- <Produktnivå: Start Underhållning>  
	------------------------------------------------------------
	UPDATE
		pss
	SET  
		pss.Title = isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct.Title, c.Title) + ' ' 
		+ isnull(bt.Title, b.Title) + ' Lekmer.fi - lautapelit netistä. '
		+ isnull(ct3.Title, c3.Title) ,
		
		pss.[Description] = 'Osta ' + isnull(pt.Title, p.Title) + ' ' 
		+ isnull(bt.Title, b.Title)
		+ '. Löydät myös muut Algan lastenpelit verkkokaupasta Lekmer.fi '  
		+ '  hos Lekmer.fi.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId	
			inner join product.tCategoryTranslation ct3
			on c3.CategoryId = ct3.CategoryId			
	WHERE
		c3.CategoryId = 1000856 -- inredning
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
