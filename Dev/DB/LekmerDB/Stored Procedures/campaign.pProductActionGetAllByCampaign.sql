SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductActionGetAllByCampaign]
	@CampaignId	int
as
begin
	SELECT
		*
	FROM
		campaign.vCustomProductAction
	WHERE 
		[ProductAction.CampaignId] = @CampaignId
	ORDER BY
		[ProductAction.Ordinal]
end

GO
