SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
**********************  Version 1  ************************
User: Roman G.		Date: 12/23/2008		Time: 13:16:33
Description: Created 
*/
CREATE PROCEDURE [template].[pTemplateFragmentSave]
	@TemplateId			int,
	@ModelFragmentId	int,
	@Content			nvarchar(max),
	@AlternateContent	nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[template].[tTemplateFragment]
	SET
		[Content] = @Content,
		[AlternateContent] = @AlternateContent
	WHERE 
		[TemplateId] = @TemplateId
		AND [ModelFragmentId] = @ModelFragmentId
		
	IF  @@ROWCOUNT = 0
		BEGIN
			INSERT 
			[template].[tTemplateFragment]
			(
				[TemplateId],
				[ModelFragmentId],
				[Content],
				[AlternateContent]
			)
			VALUES
			(
				@TemplateId,
				@ModelFragmentId,
				@Content,
				@AlternateContent
			)		
		END
END





GO
