SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [cdon].[pProductGetStockBalances] 
   @ArticleNumbers varchar(4000)
as
begin
   select lp.HyErpId as ArticleNumber, p.NumberInStock as CurrentStockBalance
     from generic.fStringToStringTable(@ArticleNumbers, ',', 1) a
         inner join lekmer.tLekmerProduct lp on a.SepString = lp.HyErpId
         inner join product.tProduct p on p.ProductId = lp.ProductId
end
GO
