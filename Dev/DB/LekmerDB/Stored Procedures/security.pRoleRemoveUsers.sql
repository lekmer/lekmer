SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [security].[pRoleRemoveUsers]
	@RoleId int
as
begin
	set nocount on

	delete
		[security].[tUserRoles]
	where
		[RoleId] = @RoleId
end
GO
