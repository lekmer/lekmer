SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconGetAllByProductSecure]
	@ProductId	int
AS 
BEGIN 
	SELECT 
		i.*
	FROM 
		[lekmer].[vIconSecure] i INNER JOIN 
		[lekmer].[tLekmerProductIcon] lpi ON lpi.IconId = i.[Icon.Id]
	WHERE
		lpi.ProductId = @ProductId
END
GO
