SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pPercentagePriceDiscountActionExcludeCategoryGetIdAll]
	@LanguageId		int,
	@ActionId		int
AS
BEGIN
	SELECT
		c.[Category.Id]
	FROM
		campaign.tPercentagePriceDiscountActionExcludeCategory A
		INNER JOIN product.vCustomCategory c ON A.CategoryId = c.[Category.Id]
	WHERE
		A.ProductActionId = @ActionId
		and c.[Category.LanguageId] = @LanguageId
END

GO
