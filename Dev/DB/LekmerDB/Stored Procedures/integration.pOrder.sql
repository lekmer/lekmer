SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pOrder]
	@OrderId int
AS
begin
	set nocount on
	set transaction isolation level read uncommitted
	begin try
		
		--set statistics io on
		--set statistics time on
		
		-- Order
		select 		
			o.OrderId,
            o.FreightCost, 
            o.ChannelId as Channel,   
            z.OrderPaymentId, 
            v.ErpId as PaymentMethod, 
            z.ReferenceId, 
            z.Price,
            u.VoucherDiscount as VoucherAmount
		from
			[order].tPaymentType v
			inner join [order].tOrderPayment z
				on z.PaymentTypeId = v.PaymentTypeId
			inner join [order].torder o
				on z.OrderId = o.OrderId
			left join lekmer.tlekmerOrder u
				on o.OrderId = u.OrderId		
			where
				o.OrderStatusId = 2 and
				o.OrderId = @OrderId
		
		-- OrderRow
		select
			o.OrderItemId as OrderRowId, 
			p.Title, 
			o.Quantity, 
			p.ProductId,
			o.ActualPriceIncludingVat as Price, 
			o.OriginalPriceIncludingVat as OrdinaryPrice, 
			coalesce(ois.ErpId, p.ErpId) as HyErpId --p.ErpId
		from
			[order].tOrderItem o
			left join [order].tOrderItemProduct p 
				on o.OrderItemId = p.OrderItemId
			left join lekmer.tOrderItemSize ois
				on ois.OrderItemId = o.OrderItemId
		where
			o.OrderId = @OrderId 
			
		-- Customer
		select 		
            c.CustomerId, 
			c.FirstName, 
			c.LastName,
            c.CivicNumber, 
            c.PhoneNumber,
            c.CellPhoneNumber as MobilePhoneNumber, 
            o.Email
		from	
			[order].torder o
			inner join [customer].tCustomerInformation c
				on c.CustomerId = o.CustomerId
			where
				o.OrderId = @OrderId
		
		-- BillingAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.BillingAddressId 
			where
				o.OrderId = @OrderId
		
		-- DeliveryAddress
		select
			a.OrderAddressId,
			a.Addressee, 		         
            a.StreetAddress as Adress, 
            a.StreetAddress2 as CoAdress, 
            a.City, 
            a.PostalCode, 
            l.ISO as CountryIso,
            a.PhoneNumber
		from	
			[order].torder o		
			inner join [core].tChannel h
				on h.ChannelId = o.ChannelId
			inner join [core].tCountry l
				on h.CountryId = l.CountryId
			inner join [order].tOrderAddress a
				on a.OrderAddressId = o.DeliveryAddressId
			where
				o.OrderId = @OrderId
		
	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
