SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pBackofficeProductImport]
	@ChannelNameISO	nvarchar(10) 
AS
begin
	set nocount on
	
	declare @LanguageId int
	set @LanguageId = (
						select l.LanguageId 
						from core.tLanguage l
							inner join core.tChannel h
							on l.LanguageId = h.LanguageId
							inner join core.tCountry c
							on h.CountryId = c.CountryId
						where
							c.ISO = @ChannelNameISO
						)

	begin try
		begin transaction				
			
		if @LanguageId != 1
		begin
		
				update pt
				set 
					pt.Title = i.Title
					--pt.[Description] = i.[Description]
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					pt.LanguageId = @LanguageId
					and (i.[Description] is null or i.[Description] = '')
				
				
				update pt
				set 
					pt.Title = i.Title,
					pt.[Description] = i.[Description]
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProductTranslation pt
						on l.ProductId = pt.ProductId	
				where
					pt.LanguageId = @LanguageId
					and (i.[Description] is not null and i.[Description] != '')
		end
		else
		begin

			update pt
				set 
					pt.Title = i.Title
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					(i.[Description] is null or i.[Description] = '')
					
					
			update pt
				set 
					pt.Title = i.Title,
					pt.[Description] = i.[Description]
				from 
					integration.tBackofficeProductImport i
					inner join lekmer.tLekmerProduct l
						on l.HYErpId = i.HYErpId
					inner join product.tProduct pt
						on l.ProductId = pt.ProductId	
				where
					(i.[Description] is not null and i.[Description] != '')	
		end

		commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
