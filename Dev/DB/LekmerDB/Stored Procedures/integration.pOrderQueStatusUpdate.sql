SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pOrderQueStatusUpdate]
	@OrderId int,
	@OrderStatusId	int,
	@MaxAllowedFailedAttempts int
AS
begin
	--set nocount on
	begin try
	begin transaction
	
		if(select 1 from [integration].[tOrderQue] where OrderId = @OrderId) = 1
		begin
		
			update
				[integration].[tOrderQue]
			set
				NumberOfFailedAttempts = NumberOfFailedAttempts + 1,
				LastFailedAttempt = GETDATE()
			where
				OrderId = @OrderId
				
				
			if (select NumberOfFailedAttempts from [integration].[tOrderQue] where OrderId = @OrderId) >= @MaxAllowedFailedAttempts
			begin
				exec [integration].[pOrderQueStatusDelete] @OrderId
				exec [integration].[pOrderStatusUpdate] @OrderId, @OrderstatusId
			end
			
		end
		else
		begin
			
			insert into [integration].[tOrderQue] 
			(
				OrderId, 
				NumberOfFailedAttempts, 
				LastFailedAttempt
			)
			values
			(
				@OrderId,
				1,
				GETDATE()
			)
			
		end
		
	commit transaction	
	end try
	begin catch
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
