SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ImportUpdateProductSizesLekmer]

AS
BEGIN

		UPDATE 
			ps
		SET 
			ps.NumberInStock = tp.NoInStock
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		--select tp.NoInStock, ps.numberinstock
		FROM
			integration.tempProduct tp
			inner join lekmer.tProductSize ps
				on ps.ErpId = substring(tp.HYarticleId, 5,17)
		WHERE 
			ps.NumberInStock <> tp.NoInStock
			AND
			substring(tp.HYarticleId, 3,1) = 1
			
		


	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@NoInStock nvarchar(250),
				@HYSizeId nvarchar (50),	
				@Data nvarchar(4000)

		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				DECLARE @SizeChar NVARCHAR (20)
				DECLARE @SizeNo NVARCHAR (20)
				------------------------------------
		
		declare cur_productHeppo cursor fast_forward for
			select
				tp.HYarticleId,
				tp.NoInStock,
				tp.SizeId
			from
				[integration].tempProduct tp
			where not exists(select 1
						from lekmer.tProductSize ps
						where ps.ErpId = substring(tp.HYarticleId, 5,17))
						AND tp.SizeId != 'Onesize' -- ny KOLLA UPP
						AND substring(tp.HYarticleId, 3,1) = 1 -- ny
			
			
		--print 'Open Cursor'
		open cur_productHeppo
		fetch next from cur_productHeppo
			into @HYArticleId,			 
				 @NoInStock,
				 @HYSizeId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1)
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,12)
				
				
				-- splitta sizeid på semi kolon
				-- behövs inte i detta fallet då lekmer storlekar bara e siffror
				/*
				SET @String = @HYSizeId -- EU-45-
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeChar = substring(@String,1,@Pos - 1)
				*/
			

				SET @String = @HYSizeId
				SET @String = @String --+ @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeNo = substring(@String,(@Pos+1),(len(@String)- (@pos)))
				
				SET @SizeChar = @SizeNo -- BARA LEKMER då de inte har - i size som heppo (EU-42)
				-- om den specifika sizen av produkten inte finns i tProductSize
				if @SizeChar not in ('XS','Small','Medium','Large','XL','Onesize', '**Onesize', 'Storlekslös', '**Storlekslös')

				begin
				
					-- Handel the @sizeChar so when it is USM it presents as USMale
					--SET @SizeChar = dbo.fx_SizeStandard(@SizeChar)
					
					
					-- Prepare the query that retrives SizeId from tSize
					-- executes a string and returns sizeId
					/*
					-- behövs inte på lekmer då storleken är en siffra rakt av
					DECLARE @tSizeIdResult int
					DECLARE @SizeId as int,@sqlQ as nvarchar(200)
					SET @sqlQ=N'select @SizeId = SizeId FROM lekmer.tSize WHERE ' + @SizeChar + ' = ' + REPLACE(@SizeNo, ',', '.')
					EXEC sp_executesql @sqlQ, N'@SizeId int OUTPUT', @SizeId= @SizeId OUTPUT
					--SELECT @SizeId
					SET @tSizeIdResult = @SizeId
					*/
					set @Data = 'IF tProductSIZE Shoes: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' @SizeChar ' + @SizeChar + ' @SizeNo ' + @SizeNo
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						(select sizeId from lekmer.tSize where EU = @SizeNo), --REPLACE(@SizeNo, ',', '.')),--@tSizeIdResult, 
						@HYarticleIdNoFoK,
						@NoInStock			
				end
				
				else if @SizeChar not in ('Onesize', '**Onesize', 'Storlekslös', '**Storlekslös')
				begin
					
					set @Data = 'ELSE IF tProductSIZE Accessories: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' @SizeChar ' + @SizeChar
					
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						(select SizeId from lekmer.tSize where EU = (
																		select AccessoriesSizeId
																		from integration .tAccessoriesSize
																		where AccessoriesSize = @SizeChar)),
																		--@tSizeIdResult
						@HYarticleIdNoFoK,
						@NoInStock
				end
			
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
				print 'Loging error'
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_productHeppo into
				 @HYArticleId,	 
				 @NoInStock,	
				 @HYSizeId
	
		end
		
		close cur_productHeppo
		deallocate cur_productHeppo

END
GO
