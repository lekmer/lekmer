SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsSE]

AS
begin
	set nocount on
	begin try
		begin transaction				
		
		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n --3
		where 
			n.SiteStructureRegistryId = 1
			and n.ContentNodeTypeId = 3 -- detta är contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Leksaker online från Lekmer.se – leksaker på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. Vi erbjuder ett stort utbud av barnleksaker från kända varumärken '
			+ 'och levererar dem på 1-3 dagar - Lekmer.se din leksaksbutik. '
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 1
			and cn.ParentContentNodeId = 1000923 			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Leksaker online från Lekmer.se – leksaker på nätet.',
			
			cps.[Description] = 'Köp ' + x.Title + ' ' + cn.Title + ' på nätet. Vi erbjuder ett stort utbud av barnleksaker '
			+ 'från kända varumärken och levererar dem på 1-3 dagar - Lekmer.se din leksaksbutik. '
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1000923) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 1			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		
		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
										
		UPDATE
			cps
		SET
			cps.Title = 'Barn och Baby ' + cn.Title + ' produkter online från Lekmer.se – Köp barnartiklar på nätet.',
			
			cps.[Description] = 'Köp Barn och Baby produkter på nätet. Få dina barn- och babyprodukter levererade på 1-3 dagar från Lekmer.se.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 1
			and cn.ParentContentNodeId = 1000835
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barn och Baby produkter online från Lekmer.se – Köp barnartiklar på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title  + ' Barn och Baby produkter på nätet. Få dina barn- och babyprodukter '
			+ 'levererade på 1-3 dagar från Lekmer.se. '
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1000835) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 1			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
			
		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Barnkläder online från Lekmer.se – Barn och Babykläder på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. Stort utbud av coola kläder till barn och bebisar – '
			+ 'handla online dina barnkläder och få dem levererade inom 1-3 dagar från Lekmer.se.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 1
			and cn.ParentContentNodeId = 1007116
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Barnkläder online från Lekmer.se – Barn och babykläder på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' och ' + x.Title + ' på nätet. Få dina barnkläder levererade på 1-3 dagar från Lekmer.se.'
			+ 'levererade på 1-3 dagar från Lekmer.se. '
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1007116) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 1			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' Inredning online från Lekmer.se Barnrumsinredning på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. Få ditt barnrum levererat på 1-3 dagar från Lekmer.se.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 1
			and cn.ParentContentNodeId = 1000884
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' Inredning online från Lekmer.se – Barnrum på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' och ' + x.Title + ' på nätet. Få ditt barnrum levererat på 1-3 dagar från Lekmer.se.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1000884) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 1			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
		
		
		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------										
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' och Underhållning och barnspel online från Lekmer.se – Tv-spel på nätet.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' på nätet. Få dina tv-spel levererade på 1-3 dagar från Lekmer.se.'
			-- select cps.title, cps.description, cn.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId									
		WHERE
			cn.SiteStructureRegistryId = 1
			and cn.ParentContentNodeId = 1001245
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
				
				
		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------	
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ' + x.Title + ' och underhållning online från Lekmer.se.',
			
			cps.[Description] = 'Köp ' + cn.Title + ' och ' + x.Title + ' på nätet. Få dina tv-spel levererade på 1-3 dagar från Lekmer.se.'
			-- select cps.title, cps.description, cn.title, x.title, cn.contentnodeid
		FROM
				sitestructure.tContentPageSeoSetting cps
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId		
				inner join	
				(select ContentNodeId, Title from sitestructure.tContentNode where ParentContentNodeId = 1001245) x
					on cn.ParentContentNodeId = x.ContentNodeId		
		WHERE
			cn.SiteStructureRegistryId = 1			
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))
						
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end

GO
