SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [lekmer].[pVoucherConditionBatcheInsert]
	@ConditionId int,
	@BatchId int
AS 
BEGIN 
	INSERT 
		lekmer.tVoucherBatches 
	( 
		ConditionId,
		BatchId
	)
	VALUES 
	(
		@ConditionId,
		@BatchId
	)
END
GO
