SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateProductSeoSE]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Produktnivå: Start Leksaker>
		------------------------------------------------------------
		
		insert into product.tProductSeoSetting (ProductId)
		select
			productid
		from product.tProduct
		where ProductId not in (select ProductId from product.tProductSeoSetting)

		-- DEFAULT --
		declare @LanguageId int
		set @LanguageId = 1 -- Sweden
		
		INSERT INTO product.tProductSeoSettingTranslation(ProductId, LanguageId)
		SELECT
			p.ProductId,
			@languageId
		FROM
			product.tProduct p
		WHERE
			not exists (SELECT 1
							FROM product.tProductSeoSettingTranslation n
							WHERE n.ProductId = p.ProductId and
							n.LanguageId = @LanguageId)
							


		UPDATE
			pss
		SET
			pss.Title = 'Köp ' + p.Title + ' - från Lekmer.se' ,
			
			pss.[Description] = 'Köp ' + p.Title + ' ' + c.Title + ' på nätet. Du hittar även andra leksaker och '
			+ 'babyprodukter från ' + b.Title + ' hos Lekmer.se' 
			--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
		FROM 
			product.tProductSeoSettingTranslation pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
			 		on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				inner join product.tCategoryTranslation ct
				on c.CategoryId = ct.CategoryId
				
				inner join product.tCategoryTranslation ct2
				on c2.CategoryId = ct2.CategoryId				
		WHERE
			c3.CategoryId = 1000533 -- leksaker
			and pss.LanguageId = @LanguageId
			and ct.LanguageId = @LanguageId
			and ct2.LanguageId = @LanguageId
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))
				
	
	------------------------------------------------------------
	-- <Produktnivå: Start Barn & Baby>
	------------------------------------------------------------
	UPDATE
		pss
	SET
		pss.Title = 'Köp ' + p.Title + ' - Lekmer.se – Köp barnartiklar online' ,
		
		pss.[Description] = 'Köp ' + p.Title + ' ' + b.Title + ' på nätet. Du hittar även andra Barn & Baby produkter från '
		+ b.Title + ' hos Lekmer.se.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1000445 -- barn och baby
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	
	------------------------------------------------------------
	-- <Produktnivå: Start Barnkläder>  
	------------------------------------------------------------
	UPDATE
		pss
	SET   
		pss.Title = p.Title + ' ' + c.Title + ' - Lekmer.se – '
		+ 'Baby och ' + c3.Title + ' på nätet.' ,
		
		pss.[Description] = 'Köp ' + p.Title + ' ' + c.Title + ' och ' + c2.Title + ' på nätet. '
		+ 'Du hittar även andra barnkläder från ' + b.Title + ' hos Lekmer.se.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1001310 -- barnkläder
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
	
	
	
	------------------------------------------------------------
	-- <Produktnivå: Start Inredning>  
	------------------------------------------------------------
	UPDATE
		pss
	SET  
		pss.Title = p.Title + ' ' + c.Title + ' ' + c2.Title + ' ' + c3.Title + ' på nätet hos Lekmer.se',
		
		pss.[Description] = 'Köp ' + p.Title + ' från ' + b.Title + ' på nätet. '
		+ 'Du hittar även andra ' + c.Title + ' från ' + b.Title + ' hos Lekmer.se.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1000494 -- inredning
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	
	------------------------------------------------------------
	-- <Produktnivå: Start Underhållning>  
	------------------------------------------------------------
	UPDATE
		pss
	SET  
		pss.Title = p.Title + ' ' +c.Title + ' från ' + b.Title + ' hos Lekmer.se '
		+ c3.Title + ' på nätet.' ,
		
		pss.[Description] = 'Köp ' + p.Title + ' från ' + b.Title + ' på nätet. Du hittar även andra barnspel från ' + b.Title 
		+ '  hos Lekmer.se.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1000856 -- inredning
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
