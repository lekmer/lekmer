SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure addon.pBlockTopListDelete
	@BlockId int
as
begin
	delete addon.tBlockTopListCategory
	where BlockId = @BlockId
	
	delete addon.tBlockTopListProduct
	where BlockId = @BlockId
	
	delete addon.tBlockTopList
	where BlockId = @BlockId
end
GO
