SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductsChildrenClothsMissingImages]

AS
begin
	--set nocount on
		
		with UniqueErpIdLekmer
				as
				(
					SELECT
						HYErpId,
						ps.ErpId,
						psp.Lagerplats,
						psp.Lagerplatstyp,
						psp.LagerplatstypBenamning,
						row_number() over(partition by HYErpId
										  order by HYErpId) RowNo,
						p.Title,
						l.BrandId,
						l.LekmerErpId										   
					from 
						product.tProduct p
						inner join lekmer.tLekmerProduct l
							on p.ProductId = l.ProductId
						inner join product.tCategory c 
							on c.CategoryId = p.CategoryId
						inner join product.tCategory c2
							on c.ParentCategoryId = c2.CategoryId
						inner join product.tCategory c3
							on c2.ParentCategoryId = c3.CategoryId
							
						left join lekmer.tProductSize ps -- <-- fel
							on ps.ProductId = p.ProductId
						left join integration.tProductStockPosition psp
							on COALESCE(ps.ErpId, l.HYErpId + '-000') = psp.HYarticleId
					where
						p.MediaId is null
						and (p.NumberInStock > 0 and ps.NumberInStock is null
								or ps.NumberInStock > 0)
						and c3.CategoryId = 1001310--'barnkläder'
				)				
		
		select
			HYErpId,
			Lagerplats,
			Lagerplatstyp,
			LagerplatstypBenamning,
			u.Title as Title,
			b.Title as BrandTitle,
			u.LekmerErpId as SupplierArticleNumber
		from 
			UniqueErpIdLekmer u
			inner join lekmer.tBrand b
				on u.BrandId = b.BrandId
		where 
			RowNo = 1 
			and Lagerplats is not null
		order by
			u.ErpId
end
GO
