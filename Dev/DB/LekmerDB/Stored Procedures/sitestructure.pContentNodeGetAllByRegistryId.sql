SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeGetAllByRegistryId]
@SiteStructureRegistryId int
as
BEGIN
	
	select 
		*
	from 
		[sitestructure].[vCustomContentNodeSecure]		
	where
		[ContentNode.SiteStructureRegistryId] = @SiteStructureRegistryId
	order by
		[ContentNode.Ordinal] asc
end

GO
