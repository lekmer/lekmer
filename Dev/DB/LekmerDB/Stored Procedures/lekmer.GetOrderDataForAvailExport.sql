SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Hamid
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [lekmer].[GetOrderDataForAvailExport] 
AS
BEGIN
	SET NOCOUNT ON;
	--SELECT tOrder.CustomerId, tProduct.ProductId , tOrder.OrderId 
	--FROM   [order].tOrder INNER JOIN
	--	   [order].tOrderItem ON tOrder.OrderId = tOrderItem.OrderId INNER JOIN
	--       [order].tOrderItemProduct ON tOrderItem.OrderItemId = tOrderItemProduct.OrderItemId INNER JOIN
	--       [product].tProduct ON tOrderItemProduct.ErpId = tProduct.ErpId
	SELECT     COALESCE(lekmer.tLekmerOrder.CustomerIdentificationKey,cast([order].tOrder.CustomerId as nchar(50))) as CustomerId, product.tProduct.ProductId, [order].tOrder.OrderId 
	FROM         [order].tOrder INNER JOIN
						  [order].tOrderItem ON [order].tOrder.OrderId = [order].tOrderItem.OrderId INNER JOIN
						  [order].tOrderItemProduct ON [order].tOrderItem.OrderItemId = [order].tOrderItemProduct.OrderItemId INNER JOIN
						  product.tProduct ON [order].tOrderItemProduct.ErpId = product.tProduct.ErpId INNER JOIN
						  lekmer.tLekmerOrder ON [order].tOrder.OrderId = lekmer.tLekmerOrder.OrderId
END




GO
