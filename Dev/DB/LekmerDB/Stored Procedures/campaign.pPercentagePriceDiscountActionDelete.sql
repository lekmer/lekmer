SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pPercentagePriceDiscountActionDelete]
	@ProductActionId int
AS 
BEGIN
	DELETE FROM campaign.tPercentagePriceDiscountActionIncludeProduct
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM campaign.tPercentagePriceDiscountActionExcludeProduct
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM campaign.tPercentagePriceDiscountActionIncludeCategory
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM campaign.tPercentagePriceDiscountActionExcludeCategory
	WHERE ProductActionId = @ProductActionId
	
	DELETE FROM campaign.tPercentagePriceDiscountAction
	WHERE ProductActionId = @ProductActionId
END
GO
