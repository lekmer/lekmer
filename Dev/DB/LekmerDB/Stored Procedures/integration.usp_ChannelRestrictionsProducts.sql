SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ChannelRestrictionsProducts]

AS
begin
	set nocount on
	begin try
		begin transaction
		-------------------------
		--- Update tProductTranslation with new products
		-------------------------
		-- NO
		insert into product.tProductTranslation(ProductId, LanguageId)
		select
			p.productid,
			1000001
		from	
			product.tProduct p
		where
			p.ProductId not in
								(
									select productid from product.tProductTranslation
									where LanguageId = 1000001
								)
		-- DK
		insert into product.tProductTranslation(ProductId, LanguageId)
		select
			p.productid,
			1000002
		from	
			product.tProduct p
		where
			p.ProductId not in
								(
									select productid from product.tProductTranslation
									where LanguageId = 1000002
								)
		-- FI		
		insert into product.tProductTranslation(ProductId, LanguageId)
		select
			p.productid,
			1000003
		from	
			product.tProduct p
		where
			p.ProductId not in
								(
									select productid from product.tProductTranslation
									where LanguageId = 1000003
								)
		
		
		-------------------------
		--- Populate tProductRegistryProduct with translated products
		-------------------------
		-- NO
		-- Build all the productRegistrys first!
		insert into product.tProductRegistryProduct(ProductId, ProductRegistryId)
		select
			pt.productid,
			2
		from 
			product.tProduct pt
		where
			pt.ProductId not in
								(
									select ProductId
									from product.tProductRegistryProduct 
									where ProductRegistryId = 2	
								)						
		
		-- DK
		insert into product.tProductRegistryProduct(ProductId, ProductRegistryId)
		select
			pt.productid,
			3
		from 
			product.tProduct pt
		where
			pt.ProductId not in
								(
									select ProductId
									from product.tProductRegistryProduct 
									where ProductRegistryId = 3	
								)
		
		-- FI
		insert into product.tProductRegistryProduct(ProductId, ProductRegistryId)
		select
			pt.productid,
			4
		from 
			product.tProduct pt
		where
			pt.ProductId not in
								(
									select ProductId
									from product.tProductRegistryProduct 
									where ProductRegistryId = 4	
								)
		
		
		
		-------------------------
		--- Delete automatic products from productRestrictionTable. Automatic means that the script
		--  handels the products and that they are removed and added depending on the translation.
		-------------------------
		delete from [integration].[tChannelRestrictionProduct]
		where 
		(ProductRegistryId = 2 or ProductRegistryId = 3 or ProductRegistryId = 4)
		and [State] = 'Automatic'
		
		
		-------------------------
		--- Insert not translated products in the productRestrictionTable
		-------------------------
		-- NO
		-- Insert all products that are not translated and therefor not shown on the site										
		insert into	[integration].[tChannelRestrictionProduct](ProductId, Title, ProductRegistryId, [State])
		select
			pt.ProductId,
			pt.Title,
			2,
			'Automatic'
		from	
			product.tProductTranslation pt
		where 
			LanguageId = 1000001 -- NO
			and Title is null
			and not exists (select 1
							from [integration].[tChannelRestrictionProduct] i
							where pt.ProductId = i.ProductId
							and i.ProductRegistryId = 2)
		
		-- DK
		insert into	[integration].[tChannelRestrictionProduct](ProductId, Title, ProductRegistryId, [State])
		select
			pt.ProductId,
			pt.Title,
			3,
			'Automatic'
		from	
			product.tProductTranslation pt
		where 
			LanguageId = 1000002 -- DK
			and Title is null
			and not exists (select 1
							from [integration].[tChannelRestrictionProduct] i
							where pt.ProductId = i.ProductId
							and i.ProductRegistryId = 3)
			
		-- FI
		insert into	[integration].[tChannelRestrictionProduct](ProductId, Title, ProductRegistryId, [State])
		select
			pt.ProductId,
			pt.Title,
			4,
			'Automatic'
		from	
			product.tProductTranslation pt
		where 
			LanguageId = 1000003 -- FI
			and Title is null
			and not exists (select 1
							from [integration].[tChannelRestrictionProduct] i
							where pt.ProductId = i.ProductId
							and i.ProductRegistryId = 4)

		
																						
		-------------------------
		--- Delete from tProductRegistryProduct
		-------------------------
		delete 
			prp		
			--select *
		from 
			product.tProductRegistryProduct prp 
			inner join 
					(select distinct * from 
							(
									(select p.ProductId, ProductRegistryId
									from integration.tchannelrestrictionCategory ic
									inner join product.tProduct p
										on p.CategoryId = ic.CategoryId)
										
									UNION
									
									(select ProductId, ProductRegistryId
									from integration.tchannelrestrictionBrand ib
									inner join lekmer.tLekmerProduct l
										on l.BrandId = ib.BrandId) 
										
									UNION 
									
									(select productId, ProductRegistryId
									from integration.tchannelrestrictionProduct)
									
							) x	
										
					) t
			on prp.ProductId = t.Productid
			and prp.ProductRegistryId = t.ProductRegistryId
					
				
	commit transaction
	end try
	begin catch

		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

	end catch			
end
GO
