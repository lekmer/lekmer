SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pProductPriceOnSite]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
if object_id('tempdb..#Campaign') is not null
    drop table #Campaign

create table #Campaign
(
   CampaignId int primary key,
   PriceListId int  
)

insert into #Campaign ( CampaignId, PriceListId )
select c.CampaignId, c.CampaignRegistryId
from campaign.tCampaign c
where c.CampaignStatusId = 0
   and (c.StartDate is null or c.StartDate <= getdate())
   and (c.EndDate is null or getdate() <= c.EndDate)



if object_id('tempdb..#CampaignAction') is not null
    drop table #CampaignAction

create table #CampaignAction (
   CampaignId int,
   SortIndex int,   
   ProductActionId int,
   Ordinal int
)

declare @Index int
set @Index = 1

insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
from #Campaign c
   inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
   left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
      and pa2.Ordinal < pa.Ordinal
where pa2.ProductActionId is null

while (@@ROWCOUNT > 0)
begin
   set @Index = @Index + 1
      
   insert into #CampaignAction ( CampaignId, SortIndex, ProductActionId, Ordinal )
   select pa.CampaignId, @index, pa.ProductActionId, pa.Ordinal
   from #Campaign c      
      inner join #CampaignAction tca on tca.CampaignId = c.CampaignId
         and tca.SortIndex = @Index - 1
      inner join campaign.tProductAction pa on pa.CampaignId = c.CampaignId
         and pa.Ordinal > tca.Ordinal
      left outer join campaign.tProductAction pa2 on pa2.CampaignId = pa.CampaignId
         and pa2.Ordinal > tca.Ordinal
         and pa2.Ordinal < pa.Ordinal
   where pa2.ProductActionId is null
   option (force order)
end

if object_id('tempdb..#CampaignProduct') is not null
    drop table #CampaignProduct

create table #CampaignProduct (
   CampaignId int,
   PriceListId int,
   ProductId int,
   Price decimal(16, 2) primary key (CampaignId, ProductId) with (ignore_dup_key = on)
)

insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, ip.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId

insert into #CampaignProduct ( CampaignId, PriceListId, ProductId, Price )
select c.CampaignId, c.PriceListId, pdai.ProductId, null
from #Campaign c
   inner join #CampaignAction ca on ca.CampaignId = c.CampaignId
   inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId

update cp set Price = pli.PriceIncludingVat
from #Campaign c
   inner join #CampaignProduct cp on cp.CampaignId = c.CampaignId
   inner join product.tPriceListItem pli on pli.PriceListId = c.PriceListId
      and pli.ProductId = cp.ProductId

declare @LastIndex int
set @Index = 1
select @LastIndex = max(SortIndex)
from #CampaignAction

while (@Index <= @LastIndex)
begin
   update cp set Price = cp.Price * (100 - ppda.DiscountAmount) / 100
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1
      inner join campaign.tPercentagePriceDiscountAction ppda on ppda.ProductActionId = pa.ProductActionId         
      inner join campaign.tPercentagePriceDiscountActionIncludeProduct ip on ip.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = ip.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
   
   update cp set Price = pdai.DiscountPrice
   -- select cp.Price, pdai.DiscountPrice
   from #CampaignAction ca
      inner join campaign.tProductAction pa on pa.ProductActionId = ca.ProductActionId
         and pa.ProductActionTypeId = 1000001
      inner join lekmer.tProductDiscountActionItem pdai on pdai.ProductActionId = ca.ProductActionId
      inner join #CampaignProduct cp on cp.ProductId = pdai.ProductId
         and cp.CampaignId = ca.CampaignId
   where ca.SortIndex = @Index
      and (cp.Price > pdai.DiscountPrice or cp.Price is null)

   set @Index = @Index + 1
end

delete cp
from #CampaignProduct cp 
where Price is null

update cp set price = case when (c.PriceListId = 4) then Price else round(Price, 0) end
from #CampaignProduct cp
   inner join #Campaign c on c.CampaignId = cp.CampaignId
   

if object_id('tempdb..#BestCampaignPrice') is not null
    drop table #BestCampaignPrice

create table #BestCampaignPrice (
   ProductId int,
   PriceListId int,
   CampaignId int,
   Price decimal(16, 2)
)

insert into #BestCampaignPrice (ProductId, PriceListId, CampaignId, Price)
select cp.ProductId, cp.PriceListId, cp.CampaignId, cp.Price
from #CampaignProduct cp
   left outer join #CampaignProduct cp2 on cp2.PriceListId = cp.PriceListId
      and cp2.ProductId = cp.ProductId
      and (cp2.Price < cp.Price or cp2.Price = cp.Price and cp2.CampaignId > cp.CampaignId)      
where cp2.ProductId is null

drop table #Campaign
drop table #CampaignAction
drop table #CampaignProduct

	-- Price List
	declare @PriceList table (
		PriceListId int primary key,
		ChannelId int		
	)
	
	insert into @PriceList
	select pl.PriceListId, c.ChannelId
	from core.tChannel c
		inner join product.tProductRegistry pr on pr.ProductRegistryId = c.ChannelId
		inner join product.tPriceList pl on pl.Title = pr.Title
	
	select hh.CommonName, ll.HYErpId, 
		--pli.ProductId, 
	   t.Title as Product,
	   pli.PriceIncludingVat as ListPrice, 
	   --c.CampaignId, 
	   c.Title as Campaign, bcp.Price as DiscountPrice, 
	   case when (bcp.Price is null or bcp.Price > pli.PriceIncludingVat) then pli.PriceIncludingVat else bcp.Price end as SitePrice,
	   cast(((1-(bcp.Price/pli.PriceIncludingVat)) * 100) as decimal (18,2)) as 'Total discount %'
	--into temp.CdonPriceNewCalc3
	from @PriceList pl
		inner join product.tPriceListItem pli on pli.PriceListId = pl.PriceListId
		inner join product.tProduct t on t.ProductId = pli.ProductId
		left outer join #BestCampaignPrice bcp on bcp.PriceListId = pl.PriceListId
		   and bcp.ProductId = t.ProductId
		left outer join campaign.tCampaign c on c.CampaignId = bcp.CampaignId
		inner join core.tChannel hh on hh.ChannelId = pl.ChannelId
		inner join lekmer.tLekmerProduct ll on ll.ProductId = pli.ProductId
	where
		c.CampaignId is not null --
	order by
		pl.ChannelId --
   drop table #BestCampaignPrice
END

GO
