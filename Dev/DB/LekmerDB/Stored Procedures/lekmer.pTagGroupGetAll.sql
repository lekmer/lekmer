SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pTagGroupGetAll]
AS
begin
	set nocount on

	select
		*
	from
		lekmer.[vTagGroup]
end

GO
