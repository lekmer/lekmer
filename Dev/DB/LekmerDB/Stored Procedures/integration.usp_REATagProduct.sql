SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_REATagProduct]

AS
BEGIN	
	begin try
	begin transaction

		-- 1. untag all products as "rea (Sale)"
		-- 2. get all active campaigns 
		-- 3. get all actions related to those active campaigns
		-- 4. tag all products assosiated with these Actions/campaigns with Rea Tag
		declare @TagId int
		set @TagId = (select TagId from lekmer.tTag where Value = 'Sale')
		
		delete from lekmer.tProductTag
		where TagId = @TagId
			
		insert into lekmer.tProductTag(ProductId, TagId)
		select distinct
			z.ProductId,
			@TagId
		from								-- lekmer.tProductDiscountActionItem innehåller alla discout priser
			(select coalesce(d.ProductId , b.ProductId) ProductId --,c.ProductActionId
				from campaign.tProductAction c
					left join lekmer.tProductDiscountActionItem b
					on c.ProductActionId = b.ProductActionId
					left join campaign.tPercentagePriceDiscountActionIncludeProduct d -- only include product!
					on c.ProductActionId = d.ProductActionId 
				where CampaignId in (
									select CampaignId from campaign.tCampaign
									where EndDate > GETDATE()
									and CampaignStatusId = 0
									and CampaignId not in (1000421, 1000418, 1000419, 1000420,
									 -- 'Varumärken (SE)'
									1000418,
									1000490,
									1000491,
									1000492,
									1000493,
									1000494,
									1000495,
									1000496,
									1000497,
									1000498,
									1000499,
									1000500,
									 -- 'Varumärken (NO)'
									1000419,
									1000501,
									1000502,
									1000503,
									1000504,
									1000505,
									1000506,
									1000507,
									1000508,
									1000509,
									1000510,
									1000511,
									 -- 'Varumärken (DK)'
									1000421,
									1000523,
									1000524,
									1000525,
									1000526,
									1000527,
									1000528,
									1000529,
									1000530,
									1000531,
									1000532,
									1000533,
									 -- 'Varumärken (FI)'
									1000420,
									1000512,
									1000513,
									1000514,
									1000515,
									1000516,
									1000517,
									1000518,
									1000519,
									1000520,
									1000521,
									1000522
									)
									--and (Title = 'Julrea'
									--or Title = 'Presale')--'REA'
									)
								) z
		where z.ProductId is not null
		
		--rollback
	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		-- LOG here
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
END

GO
