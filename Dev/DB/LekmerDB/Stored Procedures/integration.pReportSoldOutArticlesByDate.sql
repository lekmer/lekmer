SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportSoldOutArticlesByDate]
	@From		datetime,
	@To		datetime	

AS
begin
	set nocount on		
	begin try
		
		-- @gör ValidTo i tid 00:00:00  sen lägg till 23h 59min och 59 sec  i sekunder
		set @From = replace(cast(convert(varchar, @From, 102) as varchar), '.', '-') + ' 00:00:00'
		set @To = replace(cast(convert(varchar, @To, 102) as varchar), '.', '-') + ' 00:00:00'
		set @To = DATEADD(SS,86399,@To)
		
		select 
			HYErpId, 
			p.Title, 
			(select convert(varchar(10),i.OutOfStockDate, 101)) as OutOfStockDate, 
			LekmerErpId, 
			coalesce(ps.NumberInStock, p.NumberInStock) as NumberInstock,
			ps.ErpId
		from 
			integration.[tProductOutOfStock] i
			inner join lekmer.tLekmerProduct l
				on i.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join lekmer.tProductSize ps
				on ps.ProductId = p.ProductId
		where
			ProductStatusId = 0
			and
			coalesce(ps.NumberInStock, p.NumberInStock) = 0
			and
			(@From is null or i.OutOfStockDate > @From)
			and
			(@To is null or i.OutOfStockDate < @To)

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
