SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionExcludeCategoryInsert]
	@ConditionId			int,
	@CategoryId				int,
	@IncludeSubcategories	bit
AS 
BEGIN 
	INSERT
		addon.tCartContainsConditionExcludeCategory
	(
		ConditionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES
	(
		@ConditionId,
		@CategoryId,
		@IncludeSubcategories
	)
END 



GO
