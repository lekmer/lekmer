SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pBlockTopListSave]
	@BlockId int,
	@ColumnCount int,
	@RowCount int,
	@TotalProductCount int,
	@IncludeAllCategories bit,
	@OrderStatisticsDayCount int
as
begin
	update addon.tBlockTopList
	set
		ColumnCount = @ColumnCount,
		[RowCount] = @RowCount,
		TotalProductCount = @TotalProductCount,
		IncludeAllCategories = @IncludeAllCategories,
		OrderStatisticsDayCount = @OrderStatisticsDayCount
	where
		BlockId = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	insert addon.tBlockTopList
	(
		BlockId,
		ColumnCount,
		[RowCount],
		TotalProductCount,
		IncludeAllCategories,
		OrderStatisticsDayCount
	)
	values
	(
		@BlockId,
		@ColumnCount,
		@RowCount,
		@TotalProductCount,
		@IncludeAllCategories,
		@OrderStatisticsDayCount
	)
end
GO
