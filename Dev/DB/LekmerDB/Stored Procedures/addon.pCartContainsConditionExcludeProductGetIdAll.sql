SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionExcludeProductGetIdAll]
	@ConditionId	int
AS
BEGIN
	SELECT 
		P.ProductId
	FROM 
		[addon].tCartContainsConditionExcludeProduct A INNER JOIN 
		product.tProduct P ON A.ProductId = P.ProductId
	WHERE 
		A.ConditionId = @ConditionId AND 
		P.IsDeleted = 0
END
GO
