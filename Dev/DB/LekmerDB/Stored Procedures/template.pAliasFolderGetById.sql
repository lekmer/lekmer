SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 24.01.2009
Description: Created
*/

CREATE PROCEDURE [template].[pAliasFolderGetById]
	@AliasFolderId	int
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomAliasFolder]
	where
		[AliasFolder.Id] = @AliasFolderId
end

GO
