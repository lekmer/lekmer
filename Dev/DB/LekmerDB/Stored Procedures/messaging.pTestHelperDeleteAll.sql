SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pTestHelperDeleteAll]
as
begin

	delete from 
		[messaging].[tRecipientCustomField]

	delete from 
		[messaging].[tRecipient]

	delete from 
		[messaging].[tMessageCustomField]

	delete from 
		[messaging].[tMessage]

	delete from 
		[messaging].[tBatch]

end

GO
