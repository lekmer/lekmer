SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerProductIconDeleteAll]
	@ProductId	int
AS 
BEGIN 
	DELETE 
		[lekmer].[tLekmerProductIcon]
	WHERE
		ProductId = @ProductId
END
GO
