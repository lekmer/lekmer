SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionSave] 
	@FunctionId INT,
	@ModelFragmentId INT,
	@FunctionTypeId INT,
	@CommonName NVARCHAR(500),
	@Description NVARCHAR(MAX)
AS 
BEGIN
    SET NOCOUNT ON
    	
	UPDATE  
		[template].[tModelFragmentFunction]
	SET     
		[ModelFragmentId] = @ModelFragmentId,
		[FunctionTypeId] = @FunctionTypeId,
		[CommonName] = @CommonName,
		[Description] = @Description
	WHERE
		[FunctionId] = @FunctionId
		
	IF  @@ROWCOUNT = 0 
	BEGIN
		INSERT  [template].[tModelFragmentFunction] 
        (
			[ModelFragmentId],
			[FunctionTypeId],
			[CommonName],
			[Description]
        )
        VALUES
        (
        	@ModelFragmentId,
			@FunctionTypeId, 
			@CommonName,
			@Description
		)

        SET @FunctionId = SCOPE_IDENTITY()
   END
   RETURN @FunctionId
END

GO
