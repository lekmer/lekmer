SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [addon].[pTopListProductGetIdAllByStatisticsAndCategoryIdListExcludeForced]
	@ChannelId				int,
	@BlockId				int,
	@IncludeAllCategories	bit,
	@CategoryIds			varchar(max),
	@Delimiter				char(1),
	@CountOrdersFrom		datetime,
	@From					int,
	@To						int
as
begin

	declare @sql as nvarchar(max)
	declare @sqlCount as nvarchar(max)
	declare @sqlFragment as nvarchar(max)

	set @sqlFragment = '
		select
			row_number() over (order by sum(oi.[OrderItem.Quantity]) desc) as Number,
			p.[Product.Id] as ProductId
		from
			product.vCustomProduct p
			inner join
				[order].vCustomOrderItem oi with(nolock)
				on oi.[OrderItem.ProductId] = p.[Product.Id]
			inner join
				[order].vCustomOrder o with(nolock)
				on oi.[OrderItem.OrderId] = o.[Order.OrderId]
				and o.[Order.ChannelId] = @ChannelId
				and o.[Order.CreatedDate] >= @CountOrdersFrom
		where
			p.[Product.ChannelId] = @ChannelId
			and (
				@IncludeAllCategories = 1
				or (
					p.[Product.CategoryId] in (
						select
							Id
						from
							[generic].[fnConvertIDListToTableWithOrdinal](@CategoryIds, @Delimiter)
					)
				)
			)
			and not exists (
				select *
				from
					addon.vCustomBlockTopListProduct fp
				where
					fp.[BlockTopListProduct.BlockId] = @BlockId
					and fp.[BlockTopListProduct.ProductId] = p.[Product.Id]
			)
		group by
			p.[Product.Id]
	'

	set @sqlCount = '
		select count(*)
		from
			(' + @sqlFragment + ') as CountResults'

	set @sql = '
		select ProductId
		from
			(' + @sqlFragment + ') as SearchResult
		where
			Number between @From and @To'

	exec sp_executesql @sqlCount,
		N'	@ChannelId				int,
			@BlockId				int,
			@IncludeAllCategories	bit,
			@CategoryIds			varchar(max),
			@Delimiter				char(1),
			@CountOrdersFrom		datetime',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom
		     
	exec sp_executesql @sql, 
		N'	@ChannelId				int,
			@BlockId				int,
			@IncludeAllCategories	bit,
			@CategoryIds			varchar(max),
			@Delimiter				char(1),
			@CountOrdersFrom		datetime,
			@From					int,
			@To						int',
			@ChannelId,
			@BlockId,
			@IncludeAllCategories,
			@CategoryIds,
			@Delimiter,
			@CountOrdersFrom,
			@From,
			@To

end

GO
