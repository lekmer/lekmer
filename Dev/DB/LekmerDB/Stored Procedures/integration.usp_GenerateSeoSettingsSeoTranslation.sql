SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_GenerateSeoSettingsSeoTranslation]

AS
begin
	set nocount on
	begin try
		begin transaction
		
		declare @Data nvarchar (250)
				
		-- DEFAULT --
		INSERT INTO product.tProductSeoSetting(ProductId)
		SELECT
			p.ProductId
		FROM
			product.tProduct p
		WHERE
			p.ProductId not in (select ProductId 
									from product.tProductSeoSetting)
		
		UPDATE
			pss
		SET
			pss.Title = p.Title + ' - ' + c.Title + ' - ' +
			(select top 1(Title) from product.tCategory where CategoryId = c.ParentCategoryId) + ' - ' +
			
			(select title from product.tCategory where CategoryId =
				(select ParentCategoryid from product.tCategory where CategoryId = c.ParentCategoryId)) +	
			' - Lekmer.se',

			pss.[Description] = 'Köp ' + p.Title + ' från ' + b.Title +
			' hos Lekmer.se. Vi har även andra leksaker och babyprodukter från ' +
			b.Title + ' i vår stora säkra leksaksbutik.'
		FROM
			product.tProductSeoSetting pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId

				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
			WHERE
			(pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = '')
			
		
		-- SWEDEN --
		declare @languageIdSV int
		set @languageIdSV = (select LanguageId from core.tLanguage where ISO = 'SV')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdSV
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdSV)
		
		update
			psst
		set
			psst.Title = p.Title + ' - ' + c.Title + ' - ' +
			(select top 1(Title) from product.tCategory where CategoryId = c.ParentCategoryId) + ' - ' +
			
			(select title from product.tCategory where CategoryId =
				(select ParentCategoryid from product.tCategory where CategoryId = c.ParentCategoryId)) +	
			' - Lekmer.se',

			psst.[Description] = 'Köp ' + p.Title + ' från ' + b.Title +
			' hos Lekmer.se. Vi har även andra leksaker och babyprodukter från ' +
			b.Title + ' i vår stora säkra leksaksbutik.'
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'SV')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				)
		
		
		-- NORWAY --  NOT DONE
		declare @languageIdNO int
		set @languageIdNO = (select LanguageId from core.tLanguage where ISO = 'NO')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdNO
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdNO)
						
																											
		update
			psst
		set		
			psst.Title = 'Sko | ' + b.Title + ':' + p.Title  + ' | Heppo.no',
			psst.[Description] = 'Kjøp ' + b.Title + ' : ' + p.Title + ' hos en av Nordens største skobutikker på nettet. Gratis frakt og fri bytterett med 30 dagers åpent kjøp.',
			psst.Keywords = 'Sko, ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'NO')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				or (psst.Keywords is null or psst.Keywords = '')
				)
		
		
		-- DENMARK --
		declare @languageIdDA int
		set @languageIdDA = (select LanguageId from core.tLanguage where ISO = 'DA')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdDA
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdDA)
							
		update
			psst
		set
			psst.Title = p.Title + ' - ' + c.Title + ' - ' +
			(select top 1(Title) from product.tCategory where CategoryId = c.ParentCategoryId) + ' - ' +
			
			(select title from product.tCategory where CategoryId =
				(select ParentCategoryid from product.tCategory where CategoryId = c.ParentCategoryId)) +	
			' - Lekmer.se',

			psst.[Description] = 'Køb ' + p.Title + ' fra ' + b.Title +
			' hos Lekmer.dk. Vi har også andet legetøj og babyprodukter fra ' +
			b.Title + ' i vores store sikre legetøjsbutik.'
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'DA')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				)
		
		
		-- FINLAND NOT DONE --
		declare @languageIdFI int
		set @languageIdFI = (select LanguageId from core.tLanguage where ISO = 'FI')
		
		insert into product.tProductSeoSettingTranslation(ProductId, LanguageId)
		select
			p.ProductId,
			@languageIdFI
		from
			product.tProduct p
		WHERE
			not exists (select 1
							from product.tProductSeoSettingTranslation n
							where n.ProductId = p.ProductId and
							n.LanguageId = @languageIdFI)
		
		
		update
			psst
		set
			psst.Title = 'Kengät | ' + b.Title + ':' + p.Title  + ' | Heppo.fi',
			psst.[Description] = 'Osta ' + b.Title + ' : ' + p.Title + ' pohjoismaiden suurimmasta online-kenkäkaupasta. Ilmainen toimitus ja 30 päivän vaihto-oikeus.',
			psst.Keywords = 'Kengät ' + b.Title + ' ' + p.Title + ' ' + 
			(select top 1(Title) from product.tCategory where ParentCategoryId = c.ParentCategoryId) + ' ' 
			+ c.Title
		from
			product.tProductSeoSettingTranslation psst
				inner join lekmer.tLekmerProduct l
					on psst.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				inner join product.tCategory c
					on c.CategoryId = p.CategoryId
		where 
			LanguageId = (select LanguageId from core.tLanguage where ISO = 'FI')
			AND 
				(
				(psst.Title is null or psst.Title = '')
				or (psst.[Description] is null or psst.[Description] = '')
				or (psst.Keywords is null or psst.Keywords = '')
				)
					
					
		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
	end catch		 
end
GO
