SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pCategorySiteStructureRegistrySave]
	@CategoryId						int,
	@SiteStructureRegistryId		int,
	@ProductParentContentNodeId		int,
	@ProductTemplateContentNodeId	int
as
begin
	set nocount ON
	
	update
		[product].[tCategorySiteStructureRegistry]
	set
		ProductParentContentNodeId = @ProductParentContentNodeId,
		ProductTemplateContentNodeId = @ProductTemplateContentNodeId
	where
		CategoryId = @CategoryId and	
		SiteStructureRegistryId = @SiteStructureRegistryId
		
	if @@ROWCOUNT = 0
	begin
		insert into [product].[tCategorySiteStructureRegistry]
		values
		(
			@CategoryId,
			@SiteStructureRegistryId,
			@ProductParentContentNodeId,
			@ProductTemplateContentNodeId
		)
	end

end
GO
