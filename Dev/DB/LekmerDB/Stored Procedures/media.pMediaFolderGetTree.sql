SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pMediaFolderGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[MediaFolder.Id],
			v.[MediaFolder.ParentId],
			v.[MediaFolder.Title]
		FROM media.vCustomMediaFolder AS v
		WHERE v.[MediaFolder.ParentId] IS NULL
	
	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[MediaFolder.Id],
					v.[MediaFolder.ParentId],
					v.[MediaFolder.Title]
				FROM media.vCustomMediaFolder AS v
				WHERE v.[MediaFolder.ParentId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[MediaFolder.ParentId]
									 FROM media.vCustomMediaFolder AS v
									 WHERE v.[MediaFolder.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT
							v.[MediaFolder.Id],
							v.[MediaFolder.ParentId],
							v.[MediaFolder.Title]
						FROM media.vCustomMediaFolder AS v
						WHERE v.[MediaFolder.ParentId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END
		
		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM media.vCustomMediaFolder AS v
									WHERE v.[MediaFolder.ParentId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY ParentId ASC
END

GO
