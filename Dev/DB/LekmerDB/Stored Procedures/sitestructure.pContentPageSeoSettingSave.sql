SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 17:00
Description:	Created
*/

CREATE procedure [sitestructure].[pContentPageSeoSettingSave]
	@ContentNodeId	int,
	@Title			nvarchar(250),
	@Description	nvarchar(250),
	@Keywords		nvarchar(250)
as
BEGIN
	update
		[sitestructure].[tContentPageSeoSetting]
	set
		[Title]			= @Title,
		[Description]	= @Description,
		[Keywords]		= @Keywords
	where
		[ContentNodeId] = @ContentNodeId
		
	if @@ROWCOUNT = 0
	begin
		INSERT [sitestructure].[tContentPageSeoSetting]
		(
			[ContentNodeId],
			[Title],
			[Description],
			[Keywords]
		)
		values
		(
			@ContentNodeId,
			@Title,
			@Description,
			@Keywords
		)
	end
end
GO
