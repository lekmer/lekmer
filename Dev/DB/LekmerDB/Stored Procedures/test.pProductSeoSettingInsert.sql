SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [test].[pProductSeoSettingInsert]
	@ProductId int,
	@Title nvarchar(50),
	@Description nvarchar(160),
	@Keywords nvarchar(250)
as
begin
	insert into product.tProductSeoSetting
	(
		ProductId,
		Title,
		[Description],
		Keywords
	)
	values
	(
		@ProductId,
		@Title,
		@Description,
		@Keywords
	)

	insert into product.tProductSeoSettingTranslation
	(
		ProductId,
		LanguageId,
		Title,
		[Description],
		Keywords
	)
	select
		@ProductId,
		LanguageId,
		@Title,
		@Description,
		@Keywords
	from
		core.tLanguage
end
GO
