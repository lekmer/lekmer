SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pDeliveryMethodGetAll]
	@ChannelId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomDeliveryMethod]
	WHERE
		[DeliveryMethod.ChannelId] = @ChannelId
	ORDER BY
		[DeliveryMethod.Title]
END

GO
