SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [addon].[pReviewSave]
         @ProductId                    int,
         @ReviewStatusId           int,
         @CustomerId                          int,
         @Rating                                  tinyint,
         @Message                      nvarchar(500),
         @Author                                 nvarchar(50),
         @CreatedDate                datetime,
         @ChannelId                            int
AS
BEGIN
         INSERT INTO 
                   addon.tReview
         VALUES
         (
                   @ProductId,
                   @ReviewStatusId,
                   @CustomerId,
                   @Rating,
                   @Message,
                   @Author,
                   @CreatedDate,
                   @ChannelId
         )
         RETURN SCOPE_IDENTITY()
END
GO
