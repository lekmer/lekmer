SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconGetAllByProduct]
	@ProductId	int,
	@LanguageId	int
AS 
BEGIN 
	SELECT 
		i.*
	FROM 
		[lekmer].[vIcon] i INNER JOIN 
		[lekmer].[tLekmerProductIcon] lpi ON lpi.IconId = i.[Icon.Id]
	WHERE
		lpi.ProductId = @ProductId AND
		i.[Image.LanguageId] = @LanguageId
END
GO
