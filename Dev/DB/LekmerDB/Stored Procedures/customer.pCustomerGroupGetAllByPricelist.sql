SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupGetAllByPricelist]
@PricelistId	int
AS
BEGIN
	SELECT
		CG.*
	FROM
		[customer].[vCustomCustomerGroup] AS CG
		INNER JOIN [product].[tPriceListCustomerGroup] AS PG 
			ON PG.CustomerGroupId = CG.[CustomerGroup.CustomerGroupId]
	WHERE
		PG.PriceListId = @PricelistId
END

GO
