SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pBlockTopListProductGetAllByBlockSecure]
      @ChannelId int,
      @BlockId int
as
begin
DECLARE @CurrencyId int,@ProductRegistryId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	SELECT @ProductRegistryId = ProductRegistryId FROM product.tProductModuleChannel WHERE ChannelId = @ChannelId
	select
		*
	from
		addon.vCustomBlockTopListProductSecure as CBTLPS
		-- get price for current channel
		
		LEFT JOIN product.tProductRegistryProduct AS PRP ON PRP.ProductId = CBTLPS.[Product.Id] AND PRP.ProductRegistryId = @ProductRegistryId
		LEFT JOIN product.tProductModuleChannel AS PMC ON PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId
		LEFT JOIN product.vCustomPriceListItem AS pli ON pli.[Price.ProductId] = CBTLPS.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, CBTLPS.[Product.Id], PMC.PriceListRegistryId, NULL)

	where
		[BlockTopListProduct.BlockId] = @BlockId
	
	order by
		[BlockTopListProduct.Position]
end
GO
