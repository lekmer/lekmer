SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pOfflineProductScript]

AS
BEGIN	
		if object_id('tempdb..#tProductsToUpdateStatusIncrement') is not null
		drop table #tProductsToUpdateStatusIncrement
		
		create table #tProductsToUpdateStatusIncrement
		(
			ProductId int not null,
			constraint PK_#tProductsToUpdateStatusIncrement primary key(ProductId)
		)
		
		declare @Counter int
		declare @TotalAmountProducts int
		declare @HighestCurrentProductId int
		declare @RowSize int
		set @Counter = 1
		set @TotalAmountProducts = (select COUNT(ProductId) from product.tProduct)
		set @HighestCurrentProductId = 1
		set @RowSize = 200				
			
		while @counter <= @TotalAmountProducts
		begin
			begin try
				begin transaction
				
				-- Fetch @RowSize products at the time
				insert into #tProductsToUpdateStatusIncrement(ProductId)
				select top (@RowSize)
					ProductId
				from 
					product.tProduct
				where 
					ProductId > @HighestCurrentProductId
					--and isdeleted = false (0?)
				order by 
					ProductId
				
				-- Get highest productId from tmpTable
				set @HighestCurrentProductId = (select MAX(ProductId) from #tProductsToUpdateStatusIncrement)
				
				
				-- set products online that fulfill the criterias
				update
					p
				set
					ProductStatusId = 0
				from 
					product.tproduct p
					inner join #tProductsToUpdateStatusIncrement i
						on p.ProductId = i.ProductId
					left join lekmer.tproductsize s
						on p.productid = s.productid
				where 
					p.Mediaid is not null
					and p.ProductStatusId = 1
					and [Description] is not null
					and 
						((p.NumberInStock > 0 and s.ProductId is NULL)
							or 
						(s.NumberInStock > 0))
					
								
				
				update
					p
				set
					ProductStatusId = 1
				from 
					#tProductsToUpdateStatusIncrement i
					INNER JOIN product.tproduct p
							ON i.ProductId = p.ProductId					
					inner join product.tCategory c 
						on c.CategoryId = p.CategoryId
					inner join product.tCategory c2
						on c.ParentCategoryId = c2.CategoryId
					inner join product.tCategory c3
						on c2.ParentCategoryId = c3.CategoryId	
				where
					c3.CategoryId = 1001310 -- barnkläder	
					and p.ProductStatusId = 0
					AND p.NumberInStock = 0
					AND NOT EXISTS(SELECT 1
									FROM lekmer.tProductSize ps
									WHERE ps.ProductId = i.ProductId
										AND ps.NumberInStock > 0)
				
			
				set @Counter = @Counter + @RowSize
				commit
				
				-- Clear tmp table
				delete from #tProductsToUpdateStatusIncrement
				
			end try
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG here
				INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
				values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch													
		end
END


GO
