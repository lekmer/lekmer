SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateProductSeoNO]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Produktnivå: Start Leksaker>
		------------------------------------------------------------
		
		-- DEFAULT --
		declare @LanguageId int
		set @LanguageId = 1000001 -- Norway
		
		INSERT INTO product.tProductSeoSettingTranslation(ProductId, LanguageId)
		SELECT
			p.ProductId,
			@languageId
		FROM
			product.tProduct p
		WHERE
			not exists (SELECT 1
							FROM product.tProductSeoSettingTranslation n
							WHERE n.ProductId = p.ProductId and
							n.LanguageId = @LanguageId)
							


		UPDATE
			pss
		SET
			pss.Title = 'Kjøp ' + isnull(pt.Title, p.Title) + ' - fra Lekmer.no' ,
			
			pss.[Description] = 'Kjøp ' + isnull(pt.Title, p.Title) + ' ' 
			+ isnull(ct.Title, c.Title) + ' på nettet. Du finner også andre leker og '
			+ 'babyprodukter fra ' + isnull(bt.Title, b.Title) + ' hos Lekmer.no.' 
			--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
		FROM 
			product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId	
			inner join product.tCategoryTranslation ct3
			on c3.CategoryId = ct3.CategoryId				
		WHERE
			c3.CategoryId = 1000533 -- leksaker
			and pss.LanguageId = @LanguageId
			and ct.LanguageId = @LanguageId
			and ct2.LanguageId = @LanguageId
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))
				
	
	------------------------------------------------------------
	-- <Produktnivå: Start Barn & Baby>
	------------------------------------------------------------
	UPDATE
		pss
	SET
		pss.Title = isnull(pt.Title, p.Title) + ' - Lekmer.no – Kjøp barneartikler online' ,
		
		pss.[Description] = 'Kjøp ' + isnull(pt.Title, p.Title) + ' ' 
		+ isnull(bt.Title, b.Title) + ' på nettet. Du finner også andre Barn & Baby produkter fra '
		+ isnull(bt.Title, b.Title) + ' hos Lekmer.no.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId					
	WHERE
		c3.CategoryId = 1000445 -- barn och baby
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	
	------------------------------------------------------------
	-- <Produktnivå: Start Barnkläder>  
	------------------------------------------------------------
	UPDATE
		pss
	SET   
		pss.Title = isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct.Title, c.Title)  + ' - Lekmer.no –  '
		+ 'Baby og Barneklær på nettet',
		
		pss.[Description] = 'Kjøp ' + isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct.Title, c.Title)  + ' og ' 
		+ isnull(ct2.Title, c2.Title) + ' på nettet. '
		+ 'Du finner også andre barneklær fra ' 
		+ isnull(bt.Title, b.Title) + ' Lekmer.no.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId				
	WHERE
		c3.CategoryId = 1001310 -- barnkläder
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
	
	
	
	------------------------------------------------------------
	-- <Produktnivå: Start Inredning>  
	------------------------------------------------------------
	UPDATE
		pss
	SET  
		pss.Title = isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct.Title, c.Title) + ' ' 
		+ isnull(ct2.Title, c2.Title) + ' ' 
		+ isnull(ct3.Title, c3.Title) + ' hos Lekmer.no på nettet.',
		
		pss.[Description] = 'Kjøp ' + isnull(pt.Title, p.Title) + ' fra ' 
		+ isnull(bt.Title, b.Title) + ' på nettet. ' 
		+ 'Du finner også andre ' + isnull(ct.Title, c.Title) + ' fra ' 
		+ isnull(bt.Title, b.Title) + ' hos Lekmer.no.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId	
			inner join product.tCategoryTranslation ct3
			on c3.CategoryId = ct3.CategoryId				
	WHERE
		c3.CategoryId = 1000494 -- inredning
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	
	------------------------------------------------------------
	-- <Produktnivå: Start Underhållning>  
	------------------------------------------------------------
	UPDATE
		pss
	SET  
		pss.Title = isnull(pt.Title, p.Title) + ' ' 
		+ isnull(ct.Title, c.Title) + ' fra ' 
		+ isnull(bt.Title, b.Title) + ' hos Lekmer.no '
		+ isnull(ct3.Title, c3.Title) + ' på nettet.' ,
		
		pss.[Description] = 'Kjøp ' + isnull(pt.Title, p.Title) + ' fra ' 
		+ isnull(bt.Title, b.Title) + ' på nettet. Du finner også andre barnespill fra ' 
		+ isnull(bt.Title, b.Title) + '  hos Lekmer.no.' 
		--select pss.title, pss.description, c.Title, c2.Title, c3.Title, ct.title, ct2.title
	FROM 
		product.tProductSeoSettingTranslation pss
			inner join lekmer.tLekmerProduct l
				on pss.ProductId = l.ProductId
			inner join product.tProduct p
				on p.ProductId = l.ProductId
			left join product.tProductTranslation pt
				on p.ProductId = pt.ProductId
				and pt.LanguageId = @LanguageId							
			inner join lekmer.tBrand b
				on b.BrandId = l.BrandId
			left join lekmer.tBrandTranslation bt
				on bt.BrandId = b.BrandId
				and bt.LanguageId = @LanguageId	
			---------------------------------	
			inner join product.tCategory c 
				on c.CategoryId = p.CategoryId
			inner join product.tCategory c2
				on c.ParentCategoryId = c2.CategoryId
			inner join product.tCategory c3
				on c2.ParentCategoryId = c3.CategoryId
			---------------------------------
			inner join product.tCategoryTranslation ct
			on c.CategoryId = ct.CategoryId
			
			inner join product.tCategoryTranslation ct2
			on c2.CategoryId = ct2.CategoryId	
			inner join product.tCategoryTranslation ct3
			on c3.CategoryId = ct3.CategoryId						
	WHERE
		c3.CategoryId = 1000856 -- inredning
		and pss.LanguageId = @LanguageId
		and ct.LanguageId = @LanguageId
		and ct2.LanguageId = @LanguageId
		and
		((pss.Title is null or pss.Title = '')
			or (pss.[Description] is null or pss.[Description] = ''))
			
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
