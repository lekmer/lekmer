SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE 	[lekmer].[pVoucherConditionGetBatcheIdsByConditionId]
	@Id int
as
begin
	SELECT
		BatchId
	FROM
		lekmer.tVoucherBatches 
	WHERE
		[ConditionId] = @Id
end
GO
