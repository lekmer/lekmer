SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockRichTextGetByIdSecure]
	@BlockId	int
as
begin
	select 
		brt.*
	from 
		[sitestructure].[vCustomBlockRichTextSecure] as brt
	where
		brt.[Block.BlockId] = @BlockId
end

GO
