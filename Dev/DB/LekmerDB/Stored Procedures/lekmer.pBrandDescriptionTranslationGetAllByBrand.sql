SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandDescriptionTranslationGetAllByBrand]
@BrandId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [BrandTranslation.BrandId] AS 'Id',
		[BrandTranslation.LanguageId] AS 'LanguageId',
		[BrandTranslation.Description] AS 'Value'
	FROM
	    lekmer.vBrandTranslation where [BrandTranslation.BrandId] = @BrandId
END




GO
