SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pPaymentTypeGetAll]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomPaymentType]
	where
		[PaymentType.ChannelId] = @ChannelId
	Order by
		[PaymentType.PaymentTypeId]
END

GO
