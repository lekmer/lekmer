SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_UpdateTNewsletterSubscriber]

AS
BEGIN

		begin
			begin try
				begin transaction
					
				insert into
					lekmer.tNewsletterSubscriber(Email, CreatedDate, ChannelId, SentStatus)
				select distinct
					o.Email,
					GETDATE(),
					o.ChannelId,
					NULL					
				from
					[order].tOrder o				
				where 
					o.Email LIKE '%@%'
					and o.Email not in
										(select Email	
										from lekmer.tNewsletterSubscriber)
				--rollback			
				commit
			end try
			
			begin catch
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
			end catch
			

		end
		
END
GO
