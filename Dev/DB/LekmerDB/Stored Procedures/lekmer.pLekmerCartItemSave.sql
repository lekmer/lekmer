SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pLekmerCartItemSave]
	@CartItemId int,
	@ProductId int,
	@SizeId int,
	@ErpId varchar(50)
as
begin
      SET NOCOUNT ON

      UPDATE
            lekmer.tLekmerCartItem
      SET
            SizeId = @SizeId,
            ErpId = @ErpId
      WHERE            
            CartItemId = @CartItemId

      IF @@ROWCOUNT = 0 
      BEGIN
            INSERT INTO lekmer.tLekmerCartItem
            (
				  CartItemId,
                  ProductId,
                  SizeId,
                  ErpId
            )
            VALUES
            (
				  @CartItemId,
                  @ProductId,
                  @SizeId,
                  @ErpId
            )
      END
end
GO
