SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListStatusGetAll]
as
begin
	select
		*
	from
		[product].[vCustomPriceListStatus]
end

GO
