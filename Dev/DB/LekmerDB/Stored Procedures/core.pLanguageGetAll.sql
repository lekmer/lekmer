SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [core].[pLanguageGetAll]
as
begin
	set nocount on

	select
		*
	from
		[core].[vCustomLanguage]
end

GO
