SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pProductGetIdByOldUrl]
	@OldUrl		varchar(1000)
as
begin
	SET NOCOUNT ON;	
	
	select
		top 1
		ProductId
	from
		[lekmer].[tLekmerProduct]
	where
		OldUrl = @OldUrl
	order by
		ProductId

end

GO
