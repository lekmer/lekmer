SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [order].[vCustomPaymentType]
as
	select
		pt.*,
		ptp.PaymentCost AS 'PaymentType.PaymentCost'
	from
		[order].[vPaymentType] pt
		INNER JOIN [core].[tChannel] ch	
			ON ch.ChannelId = pt.[PaymentType.ChannelId]
		INNER JOIN [lekmer].[tPaymentTypePrice] ptp 
			ON ptp.[PaymentTypeId] = pt.[PaymentType.PaymentTypeId]
			AND ch.CountryId = ptp.[CountryId]
			AND ch.CurrencyId = ptp.[CurrencyId]
GO
