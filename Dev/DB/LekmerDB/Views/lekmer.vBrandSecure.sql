SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [lekmer].[vBrandSecure]
AS
SELECT     
	B.BrandId AS "Brand.BrandId",
    B.Title AS "Brand.Title",
    B.[Description] AS "Brand.Description",
    B.ExternalUrl AS "Brand.ExternalUrl",
    B.MediaId AS "Brand.MediaId",
    B.ErpId as  "Brand.ErpId",
    I.*,
    NULL AS 'Brand.ContentNodeId'
FROM
	lekmer.tBrand AS B
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = B.MediaId






GO
