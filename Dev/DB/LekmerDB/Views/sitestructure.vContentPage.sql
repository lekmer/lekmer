SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentPage]
AS
SELECT
	p.TemplateId AS 'ContentPage.TemplateId', 
	p.MasterTemplateId AS 'ContentPage.MasterTemplateId', 
	p.ContentPageTypeId AS 'ContentPage.ContentPageTypeId', 
	COALESCE (pt.Title, p.Title) AS 'ContentPage.PageTitle', 
	COALESCE (pt.UrlTitle, p.UrlTitle) AS 'ContentPage.UrlTitle',
	p.MasterPageId AS 'ContentPage.MasterPageId',
	p.IsMaster AS 'ContentPage.IsMaster',
	vn.*
FROM         
	sitestructure.tContentPage AS p 
	INNER JOIN sitestructure.vCustomContentNode AS vn ON vn.[ContentNode.ContentNodeId] = p.ContentNodeId
    LEFT JOIN core.tChannel AS ch ON vn.[ContentNode.ChannelId] = ch.ChannelId
    LEFT JOIN sitestructure.tContentPageTranslation AS pt
    ON pt.ContentNodeId = p.ContentNodeId AND ch.LanguageId = pt.LanguageId

GO
