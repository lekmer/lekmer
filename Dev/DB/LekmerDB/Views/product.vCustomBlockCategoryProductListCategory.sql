SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockCategoryProductListCategory]
as
	select
		*
	from
		[product].[vBlockCategoryProductListCategory]
GO
