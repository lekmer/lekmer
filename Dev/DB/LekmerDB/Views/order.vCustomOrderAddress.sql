SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomOrderAddress]
as
	select
		*
	from
		[order].[vOrderAddress]
GO
