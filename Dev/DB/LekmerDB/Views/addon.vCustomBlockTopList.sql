SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [addon].[vCustomBlockTopList]
as
	select
		*
	from
		addon.vBlockTopList

GO
