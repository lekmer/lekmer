SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vProductSecure]
AS
SELECT     
	P.ProductId 'Product.Id', 
	P.ItemsInPackage AS 'Product.ItemsInPackage', 
	P.ErpId AS 'Product.ErpId', 
	P.EanCode AS 'Product.EanCode', 
	P.NumberInStock AS 'Product.NumberInStock', 
    P.CategoryId AS 'Product.CategoryId', 
    P.WebShopTitle AS 'Product.WebShopTitle', 
    P.Title AS 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.ShortDescription AS 'Product.ShortDescription',    
    P.IsDeleted AS 'Product.IsDeleted', 
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)

GO
