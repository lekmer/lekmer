SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomAddress]
as
	select
		*
	from
		[customer].[vAddress]
GO
