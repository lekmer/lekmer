SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductAttribute]
as
	select
		*
	from
		[product].[vProductAttribute]
GO
