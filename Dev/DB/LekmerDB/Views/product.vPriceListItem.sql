SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vPriceListItem]
AS
SELECT
		pli.[PriceListId] 'Price.PriceListId',
		pli.[ProductId] 'Price.ProductId',
		pli.[PriceIncludingVat] 'Price.PriceIncludingVat',
		pli.[PriceExcludingVat] 'Price.PriceExcludingVat',
		pli.[VatPercentage] 'Price.VatPercentage'
FROM 
		product.tPriceListItem  AS pli
			
GO
