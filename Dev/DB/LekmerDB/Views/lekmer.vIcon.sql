SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vIcon]
AS
SELECT
	ic.IconId AS 'Icon.Id',
	COALESCE (it.Title, ic.Title) AS 'Icon.Title', 
	im.*
FROM
	lekmer.tIcon ic INNER JOIN
	media.vCustomImage im ON ic.MediaId = im.[Image.MediaId] LEFT JOIN 
	lekmer.tIconTranslation it ON it.IconId = ic.IconId AND it.LanguageId = im.[Image.LanguageId]
GO
