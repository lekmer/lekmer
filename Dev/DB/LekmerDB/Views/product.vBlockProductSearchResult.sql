SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vBlockProductSearchResult]
AS 
SELECT 
	 [BlockId] 'BlockProductSearchResult.BlockId'
	,[ColumnCount] 'BlockProductSearchResult.ColumnCount'
	,[RowCount] 'BlockProductSearchResult.RowCount'
FROM 
	[product].[tBlockProductSearchResult]
GO
