SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCartValueCondition]
as
	select
		c.*
	from
		campaign.tCartValueCondition cvc
		inner join campaign.vCustomCondition c on cvc.ConditionId = c.[Condition.Id]

GO
