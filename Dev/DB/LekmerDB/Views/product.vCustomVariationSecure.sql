SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomVariationSecure]
as
	select
		*
	from
		[product].[vVariationSecure]
GO
