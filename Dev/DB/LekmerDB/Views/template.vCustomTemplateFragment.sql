SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomTemplateFragment]
as
	select
		*
	from
		[template].[vTemplateFragment]
GO
