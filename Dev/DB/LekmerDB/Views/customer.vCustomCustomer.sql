SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [customer].[vCustomCustomer]
as
	select
		*
	from
		[customer].[vCustomer]
GO
