SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [statistics].[vDailySearch]
as
	select
		[ChannelId] as 'DailySearch.ChannelId',
		[Query] as 'DailySearch.Query',
		[Date] as 'DailySearch.Date',
		[HitCount] as 'DailySearch.HitCount',
		[SearchCount] as 'DailySearch.SearchCount'
	from
		[statistics].[tDailySearch]


GO
