SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vPriceListFolder]
AS
select
		PriceListFolderId 'PriceListFolder.Id',
		ParentPriceListFolderId 'PriceListFolder.ParentPriceListFolderId',
		Title 'PriceListFolder.Title'
from
		[product].[tPriceListFolder]
GO
