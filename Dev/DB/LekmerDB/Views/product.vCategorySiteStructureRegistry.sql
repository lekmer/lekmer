SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vCategorySiteStructureRegistry]
AS
SELECT     
	CR.CategoryId 'CategoryRegistry.CategoryId',
	CR.SiteStructureRegistryId 'CategoryRegistry.SiteStructureRegistryId',
	CR.ProductParentContentNodeId 'CategoryRegistry.ProductParentContentNodeId',
	CR.ProductTemplateContentNodeId 'CategoryRegistry.ProductTemplateContentNodeId'
FROM
	product.tCategorySiteStructureRegistry CR
GO
