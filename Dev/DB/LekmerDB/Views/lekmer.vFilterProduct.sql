SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE view [lekmer].[vFilterProduct]
as
	select
		p.ProductId 'FilterProduct.ProductId',
		p.CategoryId 'FilterProduct.CategoryId',
		COALESCE(pt.Title, p.Title) 'FilterProduct.Title',
		lp.BrandId 'FilterProduct.BrandId',
		lp.AgeFromMonth 'FilterProduct.AgeFromMonth',
		lp.AgeToMonth 'FilterProduct.AgeToMonth',
		lp.IsNewFrom 'FilterProduct.IsNewFrom',
		ch.ChannelId 'FilterProduct.ChannelId',
		ch.CurrencyId 'FilterProduct.CurrencyId',
		pmc.PriceListRegistryId 'FilterProduct.PriceListRegistryId',
		lpp.[ProductPopularity.Popularity] AS 'FilterProduct.Popularity'
	from
		product.tProduct p
		inner join lekmer.tLekmerProduct lp on p.ProductId = lp.ProductId

		INNER JOIN product.tProductRegistryProduct AS prp ON p.ProductId = prp.ProductId
		INNER JOIN product.tProductModuleChannel AS pmc ON prp.ProductRegistryId = pmc.ProductRegistryId
		INNER JOIN core.tChannel AS ch ON pmc.ChannelId = ch.ChannelId

		LEFT JOIN product.tProductTranslation AS pt
			ON p.ProductId = pt.ProductId
			AND ch.LanguageId = pt.LanguageId
		LEFT JOIN [lekmer].[vProductPopularity] lpp on pmc.[ChannelId] = lpp.[ProductPopularity.ChannelId] AND p.[ProductId] = lpp.[ProductPopularity.ProductId]

	WHERE
		p.IsDeleted = 0
		AND p.ProductStatusId = 0

GO
