SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [customer].[vCustomerModuleChannel]
AS
SELECT     
	CMC.ChannelId AS 'CustomerModuleChannel.ChannelId',
	CMC.CustomerRegistryId AS 'CustomerModuleChannel.CustomerRegistryId'
FROM
	[customer].[tCustomerModuleChannel] as CMC
GO
