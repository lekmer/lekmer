SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [media].[vMediaFormat]
AS
SELECT [MediaFormatId] 'MediaFormat.Id',
	   [MediaFormatGroupId] 'MediaFormat.GroupId',
	   [Title] 'MediaFormat.Title',
	   [CommonName] 'MediaFormat.CommonName',
	   [Mime] 'MediaFormat.Mime',
	   [Extension] 'MediaFormat.Extension'
FROM   [media].[tMediaFormat]
GO
