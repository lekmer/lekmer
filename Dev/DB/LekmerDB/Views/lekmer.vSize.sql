SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



create VIEW [lekmer].[vSize]
AS
SELECT
	[SizeId] AS 'Size.Id',
	[EU] AS 'Size.EU',
	[USMale] AS 'Size.USMale',
	[USFemale] AS 'Size.USFemale',
	[UKMale] AS 'Size.UKMale',
	[UKFemale] AS 'Size.UKFemale',
	[Millimeter] AS 'Size.Millimeter'
FROM
	[lekmer].[tSize]

GO
