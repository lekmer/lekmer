SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlock]
as
	select
		*
	from
		[sitestructure].[vBlock]
GO
