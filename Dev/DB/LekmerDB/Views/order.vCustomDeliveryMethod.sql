SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [order].[vCustomDeliveryMethod]
as
	select
		*
	from
		[order].[vDeliveryMethod]

GO
