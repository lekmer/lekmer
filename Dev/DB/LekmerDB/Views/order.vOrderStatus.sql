SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vOrderStatus]
AS 
SELECT 
	OrderStatusId AS 'OrderStatus.Id', 
	Title AS 'OrderStatus.Title', 
	CommonName AS 'OrderStatus.CommonName'
FROM 	  
	[order].[tOrderStatus]
GO
