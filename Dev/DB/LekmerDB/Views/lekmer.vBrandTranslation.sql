SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vBrandTranslation]
AS
SELECT 
	[BrandId] AS 'BrandTranslation.BrandId',
	[LanguageId] AS 'BrandTranslation.LanguageId',
	[Title] AS 'BrandTranslation.Title',
	[Description] AS 'BrandTranslation.Description'
FROM 
	[lekmer].[tBrandTranslation]
GO
