SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockProductSearchResult]
as
	select
		*
	from
		[product].[vBlockProductSearchResult]
GO
