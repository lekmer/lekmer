SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductImage]
as
	select
		*
	from
		[product].[vProductImage]
GO
