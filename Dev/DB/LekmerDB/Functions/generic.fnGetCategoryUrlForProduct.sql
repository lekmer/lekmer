SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [generic].[fnGetCategoryUrlForProduct](
	@CategoryId int,
	@LanguageId int
)
returns @categoryTable table(category varchar(1000))
as
begin

	declare @xml varchar(1000)

   ;with CategoryList (CategoryId, Lvl)
   as
   (
	select c.CategoryId, 0 from product.tCategory c
		where CategoryId = @CategoryId
	union all
	select cc.ParentCategoryId, Lvl+1 from product.tCategory cc
		inner join CategoryList cl on cc.CategoryId = cl.CategoryId
		where cc.ParentCategoryId is not null
   )

select @xml = (
				select generic.fCleanCategory(coalesce(ct.Title,cc.Title)) + '/' as 'data()' from CategoryList cl
					inner join product.tCategory cc on cc.CategoryId = cl.CategoryId
					left outer join product.tCategoryTranslation ct on ct.CategoryId = cc.CategoryId and ct.LanguageId = @LanguageId
					order by cl.lvl desc for xml path (''))
set @xml = replace(@xml, ' ','')

insert into @categoryTable values(@xml)

	return
end
GO
