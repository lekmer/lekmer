SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [product].[fnGetPriceListIdOfItemWithLowestPrice]
(
	@CurrencyId int,
	@ProductId int,
	@PriceListRegistryId int,
	@CustomerId int
)
returns int
as
begin
	declare @PriceListId int
	
	select top(1)
		@PriceListId = pli.PriceListId		
	from
		product.tPriceListItem as pli
		inner join product.tPriceList as pl ON pl.PriceListId = pli.PriceListId
		left join product.tPriceListCustomerGroup AS cgp ON cgp.PriceListId = pl.PriceListId
	where
		pl.PriceListStatusId = 0
		and pl.PriceListRegistryId = @PriceListRegistryId
		and pl.CurrencyId = @CurrencyId
		and getdate() between coalesce(pl.StartDateTime, getdate()) and coalesce(pl.EndDateTime, getdate())
		and pli.ProductId = @ProductId
		and ( -- if customer is not defined then should be available all pricelists that are not assigned to any customer group
			cgp.CustomerGroupId is null
			or
			(
				@CustomerId is not null
				and
				cgp.CustomerGroupId in (
					select
						cg.CustomerGroupId
					from
						customer.tCustomerGroup AS cg
						inner join customer.tCustomerGroupCustomer AS cgc ON cgc.CustomerGroupId = cg.CustomerGroupId
					where
						cg.StatusId = 0
						and cgc.CustomerId = @CustomerId
				)
			)
		)
	order by 
		pli.PriceIncludingVat

	return @PriceListId
end



GO
