SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create FUNCTION [generic].[fCleanCategory]
(
	@String	VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX)


--Change to lower case

Set @String = LOWER(@String)

SET @String = REPLACE(@String,'/','-')
SET @String = REPLACE(@String,' ','-')

SET @String = CASE
				WHEN Left(@String,1) = '-' THEN SUBSTRING(@String,2,LEN(@String)) ELSE @String
				END
SET @String = CASE				
				WHEN Right(@String,1) = '-' THEN SUBSTRING(@String,1,LEN(@String)-1) ELSE @String
				END

RETURN (SELECT @String)
END
GO
