SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fGetCategoryUrl]
(
	@CategoryId	int,
	@LanguageId int,
	@ProductId int
)
RETURNS VARCHAR(1000)
AS
BEGIN

	declare @path varchar(1000)
	declare @productUrlTitle varchar(1000)

select @path = generic.fCleanUrl(category) from generic.fnGetCategoryUrlForProduct(@CategoryId,@LanguageId)

select @productUrlTitle = UrlTitle from lekmer.tProductUrl where ProductId =@ProductId and LanguageId = @LanguageId

set @productUrlTitle = replace(@productUrlTitle, ' ','-')

return (select 'http://' + ApplicationName + '/' + @path + @productUrlTitle from core.tChannel where LanguageId = @LanguageId and not channelId = 1000005)

END

GO
