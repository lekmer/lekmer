﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core;
using NUnit.Framework;
using Rhino.Mocks;

namespace Lekmer.ProductFilter.UnitTest
{
	[TestFixture]
	public class FilterProcessorTest
	{
		private MockRepository _mocker;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
		}

		[Test]
		public void Finds_all()
		{
			var query = new FilterQuery();

			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(5, products.Count());
		}

		[Test]
		public void Finds_all_lego_products()
		{
			var query = new FilterQuery();
			query.BrandIds.Add(FilterProductHelper.LegoBrandId);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(2, products.Count());
		}

		[Test]
		public void Finds_all_summer_products()
		{
			var query = new FilterQuery();
			query.GroupedTagIds.Add(new[] {FilterProductHelper.SummerTagId});
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(3, products.Count());
		}

		[Test]
		public void Finds_all_summer_or_winter_products()
		{
			var query = new FilterQuery();
			query.GroupedTagIds.Add(new[] {FilterProductHelper.SummerTagId, FilterProductHelper.WinterTagId});
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(4, products.Count());
		}

		[Test]
		public void Finds_all_summer_and_winter_products()
		{
			var query = new FilterQuery();
			query.GroupedTagIds.Add(new[] {FilterProductHelper.SummerTagId});
			query.GroupedTagIds.Add(new[] {FilterProductHelper.WinterTagId});
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(1, products.Count());
		}

		[Test]
		public void Finds_all_sport_products()
		{
			var query = new FilterQuery();
			query.Level1CategoryIds.Add(FilterProductHelper.SportCategoryId);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(3, products.Count());
		}

		[Test]
		public void Finds_all_sport_football_products()
		{
			var query = new FilterQuery();
			query.Level1CategoryIds.Add(FilterProductHelper.SportCategoryId);
			query.Level2CategoryIds.Add(FilterProductHelper.FootballCategoryId);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(1, products.Count());
		}

		[Test]
		public void Finds_all_sport_or_book_products()
		{
			var query = new FilterQuery();
			query.Level1CategoryIds.Add(FilterProductHelper.SportCategoryId);
			query.Level1CategoryIds.Add(FilterProductHelper.BookCategoryId);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(4, products.Count());
		}

		[Test]
		public void Finds_all_price_0_to_100_products()
		{
			var query = new FilterQuery();
			query.PriceIntervals.Add(FilterProductHelper.PriceIntervals.First(pi => pi.From == 0 && pi.To == 100));
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(2, products.Count());
		}

		[Test]
		public void Finds_all_age_0_to_12_products()
		{
			var query = new FilterQuery();
			query.AgeIntervals.Add(FilterProductHelper.AgeIntervals.First(ai => ai.FromMonth == 0 && ai.ToMonth == 12));
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(3, products.Count());
		}

		[Test]
		public void Finds_all_size1_products()
		{
			var query = new FilterQuery();
			query.SizeIds.Add(FilterProductHelper.Size1Id);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(3, products.Count());
		}

		[Test]
		public void Finds_all_size2_products()
		{
			var query = new FilterQuery();
			query.SizeIds.Add(FilterProductHelper.Size2Id);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(2, products.Count());
		}

		[Test]
		public void Finds_all_size1_or_size2_products()
		{
			var query = new FilterQuery();
			query.SizeIds.Add(FilterProductHelper.Size1Id);
			query.SizeIds.Add(FilterProductHelper.Size2Id);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(4, products.Count());
		}

		[Test]
		public void Finds_all_not_existed_size_products()
		{
			var query = new FilterQuery();
			query.SizeIds.Add(-1);
			var products = FilterByQuery(FilterProductHelper.FiveProducts, query);

			Assert.AreEqual(0, products.Count());
		}

		//Erik: I give up!
		//[Test]
		//public void Filter_on_100000_products_takes_less_then_100_ms()
		//{
		//    IEnumerable<FilterProduct> products = FilterProductHelper.GenerateRandomProducts(100000);

		//    var query = new FilterQuery();
		//    query.BrandIds.Add(FilterProductHelper.LegoBrandId);
		//    query.GroupedTagIds.Add(new[] {FilterProductHelper.SummerTagId});

		//    TimeSpan? lowestTime = null;

		//    for (int i = 0; i < 3; i++)
		//    {
		//        TimeSpan processTime;
		//        FilterByQuery(products, query, out processTime);

		//        if (!lowestTime.HasValue || processTime < lowestTime.Value)
		//        {
		//            lowestTime = processTime;
		//        }

		//        if (lowestTime.Value.TotalMilliseconds <= 100) return;
		//    }

		//    if (lowestTime.HasValue && lowestTime.Value.TotalMilliseconds > 100)
		//    {
		//        Assert.Fail(string.Format("Processing took {0} ms.", lowestTime.Value.TotalMilliseconds));
		//    }
		//}

		private IEnumerable<FilterProduct> FilterByQuery(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			TimeSpan processTime;
			return FilterByQuery(products, query, out processTime);
		}

		private IEnumerable<FilterProduct> FilterByQuery(IEnumerable<FilterProduct> products, FilterQuery query,
		                                                 out TimeSpan processTime)
		{
			var context = new UserContext
			              	{
			              		Channel = new Channel {Id = 1}
			              	};

			var priceIntervalService = _mocker.StrictMock<IPriceIntervalService>();
			Expect.Call(priceIntervalService.GetAll(context))
				.Repeat.Once()
				.Return(FilterProductHelper.PriceIntervals);

			var ageIntervalService = _mocker.StrictMock<IAgeIntervalService>();
			Expect.Call(ageIntervalService.GetAll(context))
				.Repeat.Once()
				.Return(FilterProductHelper.AgeIntervals);

			_mocker.ReplayAll();

			var filterProcessor = new FilterProcessor(priceIntervalService, ageIntervalService);

			var stopwatch = Stopwatch.StartNew();
			FilterResult filterResult = filterProcessor.Process(context, products, query);
			filterResult.Products.Count(); //Just to be sure to trigger all linq searches.
			stopwatch.Stop();
			processTime = stopwatch.Elapsed;

			_mocker.VerifyAll();

			return filterResult.Products;
		}
	}
}