using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Litium.Lekmer.ProductFilter;
using NUnit.Framework;

namespace Lekmer.ProductFilter.UnitTest
{
	[TestFixture]
	public class FilteredProductRangeStatisticsTest
	{
		[Test]
		public void Calculates_statistics_for_five_products()
		{
			var products = FilterProductHelper.FiveProducts;

			var stats = new FilteredProductRangeStatistics();
			stats.LoadAgeIntervals(FilterProductHelper.AgeIntervals);
			stats.LoadPriceIntervals(FilterProductHelper.PriceIntervals);
			stats.LoadProducts(products);

			Assert.AreEqual(2, stats.BrandCount.Count, "BrandCount.Count");
			Assert.AreEqual(2, stats.BrandCount[FilterProductHelper.LegoBrandId], "Brand Lego");
			Assert.AreEqual(3, stats.BrandCount[FilterProductHelper.BrioBrandId], "Brand Brio");

			Assert.AreEqual(5, stats.CategoryCount.Count, "CategoryCount.Count");
			Assert.AreEqual(3, stats.CategoryCount[FilterProductHelper.SportCategoryId], "Category Sport");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.FootballCategoryId], "Category Football");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.FloorballCategoryId], "Category Floorball");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.BookCategoryId], "Category Book");
			Assert.AreEqual(1, stats.CategoryCount[FilterProductHelper.DollCategoryId], "Category Doll");

			Assert.AreEqual(2, stats.TagCount.Count, "TagCount.Count");
			Assert.AreEqual(3, stats.TagCount[FilterProductHelper.SummerTagId], "Tag Summer");
			Assert.AreEqual(2, stats.TagCount[FilterProductHelper.WinterTagId], "Tag Winter");

			Assert.AreEqual(2, stats.AgeIntervalCount.Count(ai => ai.Count > 0), "AgeIntervalCount.Count");
			Assert.AreEqual(3, stats.AgeIntervalCount.First(ai => ai.Id == 1).Count, "AgeInterval 1");
			Assert.AreEqual(3, stats.AgeIntervalCount.First(ai => ai.Id == 2).Count, "AgeInterval 2");

			Assert.AreEqual(3, stats.PriceIntervalCount.Count(pi => pi.Count > 0), "PriceIntervalCount.Count");
			Assert.AreEqual(2, stats.PriceIntervalCount.First(pi => pi.Id == 1).Count, "PriceInterval 1");
			Assert.AreEqual(2, stats.PriceIntervalCount.First(pi => pi.Id == 2).Count, "PriceInterval 2");
			Assert.AreEqual(1, stats.PriceIntervalCount.First(pi => pi.Id == 3).Count, "PriceInterval 3");

			Assert.AreEqual(2, stats.SizeCount.Count, "SizeCount.Count");
			Assert.AreEqual(3, stats.SizeCount[FilterProductHelper.Size1Id], "Size 1");
			Assert.AreEqual(2, stats.SizeCount[FilterProductHelper.Size2Id], "Size 2");
		}

		//Erik: I give up!
		//[Test]
		//public void Calculate_statistics_on_10000_products_takes_less_then_100_ms()
		//{
		//    IEnumerable<FilterProduct> products = FilterProductHelper.GenerateRandomProducts(10000);

		//    TimeSpan? lowestTime = null;

		//    for (int i = 0; i < 3; i++)
		//    {
		//        var stopwatch = Stopwatch.StartNew();

		//        var stats = new FilteredProductRangeStatistics();
		//        stats.LoadAgeIntervals(FilterProductHelper.AgeIntervals);
		//        stats.LoadPriceIntervals(FilterProductHelper.PriceIntervals);
		//        stats.LoadProducts(products);

		//        stopwatch.Stop();
		//        TimeSpan processTime = stopwatch.Elapsed;

		//        if (!lowestTime.HasValue || processTime < lowestTime.Value)
		//        {
		//            lowestTime = processTime;
		//        }

		//        if (lowestTime.Value.TotalMilliseconds <= 100) return;
		//    }

		//    if (lowestTime.HasValue && lowestTime.Value.TotalMilliseconds > 100)
		//    {
		//        Assert.Fail(string.Format("Calculating statistics took {0} ms.", lowestTime.Value.TotalMilliseconds));
		//    }
		//}
	}
}