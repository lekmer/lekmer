using System;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class VoucherDiscountActionTest
	{
		private MockRepository _mocker;

		private const int _swedishKronorCurrencyId = 1;

		public ICartItem MockCartItem()
		{
			ICartItem cartItem = new LekmerCartItem();
			cartItem.Status = BusinessObjectStatus.New;
			IProduct product = new LekmerProduct();
			cartItem.Product = product;
			cartItem.Product.Price = new LekmerPriceListItem();
			cartItem.Product.CampaignInfo = new ProductCampaignInfo {Price = new Price()};
			cartItem.CreatedDate = DateTime.Now;
			return cartItem;
		}

		[Test]
		public void Invalid_discount_is_not_applied()
		{
			_mocker = new MockRepository();

			var cartItemServiceMock = _mocker.StrictMock<ICartItemService>();
			Expect.Call(cartItemServiceMock.Create())
				.Repeat.Any()
				.Return(MockCartItem());

			_mocker.ReplayAll();

			var cart = new LekmerCartFull(cartItemServiceMock);
			cart.AddItem((IProduct)GetProduct(110, 100), 1, null, "1",1);
            
			// can not add second product to cart becaseu CartItemService.Create is mocked and thus one and the same product is always referenced.
			cart.CampaignInfo = new LekmerCartCampaignInfo();

			var swedishContext = new LekmerUserContext
			{
				Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } },
                Voucher = new VoucherCheckResult { DiscountValue = 10, IsValid = false, ValueType = VoucherValueType.Price },
				Cart = cart
			};

			var voucherDiscountAction = new VoucherDiscountAction();
			voucherDiscountAction.Apply(swedishContext, cart);

			_mocker.VerifyAll();
			Assert.AreEqual(110, cart.GetActualPriceSummary().IncludingVat);
		}

		[Test]
		public void Price_discount_is_applied()
		{
			_mocker = new MockRepository();

			var cartItemServiceMock = _mocker.StrictMock<ICartItemService>();
			Expect.Call(cartItemServiceMock.Create())
				.Repeat.Any()
				.Return(MockCartItem());

			_mocker.ReplayAll();

			var cart = new LekmerCartFull(cartItemServiceMock);
			cart.AddItem(GetProduct(110, 100), 1, null, "1",1);
			// can not add second product to cart becaseu CartItemService.Create is mocked and thus one and the same product is always referenced.
			cart.CampaignInfo = new LekmerCartCampaignInfo();

			var swedishContext = new LekmerUserContext
			{
				Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } },
                Voucher = new VoucherCheckResult { DiscountValue = 10, IsValid = true, ValueType = VoucherValueType.Price },
				Cart = cart
			};

			var voucherDiscountAction = new VoucherDiscountAction();
			voucherDiscountAction.Apply(swedishContext, cart);

			_mocker.VerifyAll();
			Assert.AreEqual(100, cart.GetActualPriceSummary().IncludingVat);
		}

		[Test]
		public void Percentage_discount_is_applied()
		{
			_mocker = new MockRepository();

			var cartItemServiceMock = _mocker.StrictMock<ICartItemService>();
			Expect.Call(cartItemServiceMock.Create())
				.Repeat.Any()
				.Return(MockCartItem());

			_mocker.ReplayAll();

			var cart = new LekmerCartFull(cartItemServiceMock);
			cart.AddItem(GetProduct(110, 100), 1, null, "1",1);
			// can not add second product to cart becaseu CartItemService.Create is mocked and thus one and the same product is always referenced.
			cart.CampaignInfo = new LekmerCartCampaignInfo();

			var swedishContext = new LekmerUserContext
			{
				Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } },
                Voucher = new VoucherCheckResult { DiscountValue = 10, IsValid = true, ValueType = VoucherValueType.Percentage },
				Cart = cart
			};

			var voucherDiscountAction = new VoucherDiscountAction();
			voucherDiscountAction.Apply(swedishContext, cart);

			_mocker.VerifyAll();

			Assert.AreEqual(99, cart.GetActualPriceSummary().IncludingVat);
		}

		private static LekmerProduct GetProduct(decimal priceIncludingVat, decimal priceExcludingVat)
		{
			var product = new LekmerProduct();
			product.Id = -1;
			product.Price = new PriceListItem { PriceIncludingVat = priceIncludingVat, PriceExcludingVat = priceExcludingVat, VatPercentage = .1m };
			product.CampaignInfo = new ProductCampaignInfo { Price = new Price(priceIncludingVat, priceExcludingVat) };
			return product;
		}
	}
}