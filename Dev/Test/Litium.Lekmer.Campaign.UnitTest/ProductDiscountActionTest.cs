﻿using System.Collections.Generic;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class ProductDiscountActionTest
	{
		private const int _swedishKronorCurrencyId = 1;
		private const int _americanDollarCurrencyId = 2;

		[Test]
		public void Dollar_campaign_is_not_applied_in_sweden()
		{
			var swedishContext = new UserContext
			                     	{Channel = new Channel {Currency = new Currency {Id = _swedishKronorCurrencyId}}};

			var product = new Scensum.Product.Product
			              	{
			              		Id = 1,
			              		CampaignInfo = new ProductCampaignInfo {Price = new Price(100, 80)},
			              		Price = new PriceListItem {VatPercentage = 25}
			              	};

			var action = new ProductDiscountAction
			             	{
			             		CurrencyId = _americanDollarCurrencyId,
			             		ProductDiscountPrices = new Dictionary<int, decimal>
			             		                        	{
			             		                        		{1, 9.99m},
			             		                        		{2, 11.99m}
			             		                        	}
			             	};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_when_product_doesnt_have_campaign_price()
		{
			var swedishContext = new UserContext
			                     	{Channel = new Channel {Currency = new Currency {Id = _swedishKronorCurrencyId}}};

			var product = new Scensum.Product.Product
			              	{
			              		Id = 1,
			              		CampaignInfo = new ProductCampaignInfo {Price = new Price(100, 80)},
			              		Price = new PriceListItem {VatPercentage = 25}
			              	};

			var action = new ProductDiscountAction
			             	{
			             		CurrencyId = _swedishKronorCurrencyId,
			             		ProductDiscountPrices = new Dictionary<int, decimal>
			             		                        	{
			             		                        		{2, 99.50m}
			             		                        	}
			             	};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_when_campaign_price_is_higher_then_product_price()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
			{
				Id = 1,
				CampaignInfo = new ProductCampaignInfo { Price = new Price(100, 80) },
				Price = new PriceListItem { VatPercentage = 25 }
			};

			var action = new ProductDiscountAction
			{
				CurrencyId = _swedishKronorCurrencyId,
				ProductDiscountPrices = new Dictionary<int, decimal>
			             		                        	{
			             		                        		{1, 120.00m},
			             		                        		{2, 99.50m}
			             		                        	}
			};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_when_campaign_price_equal_to_product_price()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new Scensum.Product.Product
			{
				Id = 1,
				CampaignInfo = new ProductCampaignInfo { Price = new Price(100, 80) },
				Price = new PriceListItem { VatPercentage = 25 }
			};

			var action = new ProductDiscountAction
			{
				CurrencyId = _swedishKronorCurrencyId,
				ProductDiscountPrices = new Dictionary<int, decimal>
			             		                        	{
			             		                        		{1, 100.00m},
			             		                        		{2, 99.50m}
			             		                        	}
			};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsFalse(applied, "Action applied");
			Assert.AreEqual(new Price(100, 80), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_applied_when_currency_matches_and_campaign_price_is_lower()
		{
			var swedishContext = new UserContext
			                     	{Channel = new Channel {Currency = new Currency {Id = _swedishKronorCurrencyId}}};

			var product = new Scensum.Product.Product
			              	{
			              		Id = 1,
			              		CampaignInfo = new ProductCampaignInfo {Price = new Price(100, 80)},
			              		Price = new PriceListItem {VatPercentage = 25}
			              	};

			var action = new ProductDiscountAction
			             	{
			             		CurrencyId = _swedishKronorCurrencyId,
			             		ProductDiscountPrices = new Dictionary<int, decimal>
			             		                        	{
			             		                        		{1, 50.00m},
			             		                        		{2, 99.50m}
			             		                        	}
			             	};
			bool applied = action.Apply(swedishContext, product);

			Assert.IsTrue(applied, "Action applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_not_applied_after_PercentageDiscountAction_when_campaign_price_is_higher()
		{
			var swedishContext = new UserContext
			                     	{Channel = new Channel {Currency = new Currency {Id = _swedishKronorCurrencyId}}};

			var product = new Scensum.Product.Product
			              	{
			              		Id = 1,
			              		CampaignInfo = new ProductCampaignInfo {Price = new Price(100, 80)},
			              		Price = new PriceListItem {VatPercentage = 25}
			              	};

			var percentagePriceDiscountAction = new PercentagePriceDiscountAction
			                                    	{
			                                    		DiscountAmount = 50,
			                                    		IncludeAllProducts = true,
			                                    		IncludeProducts = new ProductIdDictionary(),
			                                    		IncludeCategories = new CategoryIdDictionary(),
			                                    		ExcludeProducts = new ProductIdDictionary(),
			                                    		ExcludeCategories = new CategoryIdDictionary()
			                                    	};
			bool percentagePriceDiscountActionApplied = percentagePriceDiscountAction.Apply(swedishContext, product);

			Assert.IsTrue(percentagePriceDiscountActionApplied, "Percentage discount applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);

			var discountAction = new ProductDiscountAction
			                     	{
			                     		CurrencyId = _swedishKronorCurrencyId,
			                     		ProductDiscountPrices = new Dictionary<int, decimal>
			                     		                        	{
			                     		                        		{1, 60.00m},
			                     		                        		{2, 99.50m}
			                     		                        	}
			                     	};
			bool discountActionApplied = discountAction.Apply(swedishContext, product);

			Assert.IsFalse(discountActionApplied, "Discount action applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_applied_after_PercentageDiscountAction_when_campaign_price_is_lower()
		{
			var swedishContext = new UserContext
			                     	{Channel = new Channel {Currency = new Currency {Id = _swedishKronorCurrencyId}}};

			var product = new Scensum.Product.Product
			              	{
			              		Id = 1,
			              		CampaignInfo = new ProductCampaignInfo {Price = new Price(100, 80)},
			              		Price = new PriceListItem {VatPercentage = 25}
			              	};

			var percentagePriceDiscountAction = new PercentagePriceDiscountAction
			                                    	{
			                                    		DiscountAmount = 50,
			                                    		IncludeAllProducts = true,
			                                    		IncludeProducts = new ProductIdDictionary(),
			                                    		IncludeCategories = new CategoryIdDictionary(),
			                                    		ExcludeProducts = new ProductIdDictionary(),
			                                    		ExcludeCategories = new CategoryIdDictionary()
			                                    	};
			bool percentagePriceDiscountActionApplied = percentagePriceDiscountAction.Apply(swedishContext, product);

			Assert.IsTrue(percentagePriceDiscountActionApplied, "Percentage discount applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);

			var discountAction = new ProductDiscountAction
			                     	{
			                     		CurrencyId = _swedishKronorCurrencyId,
			                     		ProductDiscountPrices = new Dictionary<int, decimal>
			                     		                        	{
			                     		                        		{1, 10.00m},
			                     		                        		{2, 99.50m}
			                     		                        	}
			                     	};
			bool discountActionApplied = discountAction.Apply(swedishContext, product);

			Assert.IsTrue(discountActionApplied, "Discount action applied");
			Assert.AreEqual(new Price(10, 8), product.CampaignInfo.Price);
		}

		[Test]
		public void Campaign_is_applied_before_PercentageDiscountAction()
		{
			var swedishContext = new UserContext
			                     	{Channel = new Channel {Currency = new Currency {Id = _swedishKronorCurrencyId}}};

			var product = new Scensum.Product.Product
			              	{
			              		Id = 1,
			              		CampaignInfo = new ProductCampaignInfo {Price = new Price(100, 80)},
			              		Price = new PriceListItem {VatPercentage = 25}
			              	};

			var discountAction = new ProductDiscountAction
			                     	{
			                     		CurrencyId = _swedishKronorCurrencyId,
			                     		ProductDiscountPrices = new Dictionary<int, decimal>
			                     		                        	{
			                     		                        		{1, 50.00m},
			                     		                        		{2, 99.50m}
			                     		                        	}
			                     	};
			bool discountActionApplied = discountAction.Apply(swedishContext, product);

			Assert.IsTrue(discountActionApplied, "Discount action applied");
			Assert.AreEqual(new Price(50, 40), product.CampaignInfo.Price);

			var percentagePriceDiscountAction = new PercentagePriceDiscountAction
			                                    	{
			                                    		DiscountAmount = 50,
			                                    		IncludeAllProducts = true,
			                                    		IncludeProducts = new ProductIdDictionary(),
			                                    		IncludeCategories = new CategoryIdDictionary(),
			                                    		ExcludeProducts = new ProductIdDictionary(),
			                                    		ExcludeCategories = new CategoryIdDictionary()
			                                    	};
			bool percentagePriceDiscountActionApplied = percentagePriceDiscountAction.Apply(swedishContext, product);

			Assert.IsTrue(percentagePriceDiscountActionApplied, "Percentage discount applied");
			Assert.AreEqual(new Price(25, 20), product.CampaignInfo.Price);
		}
	}
}