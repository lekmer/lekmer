﻿using System;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.StatisticsUtil.Console
{
	class Program
	{
		static void Main()
		{
			//System.Console.WriteLine("Statistics Util Console");
			//System.Console.WriteLine("-----------------------");
			//System.Console.WriteLine("Press any key to start");
			var lekmerProductSecureService = (ILekmerProductSecureService)IoC.Resolve<IProductSecureService>();
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			lekmerProductSecureService.UpdatePopularity(channels, DateTime.Now.AddDays(-30), DateTime.Now); ;

			System.Console.WriteLine("Done!");
			System.Console.ReadLine();
		}
	}
}
