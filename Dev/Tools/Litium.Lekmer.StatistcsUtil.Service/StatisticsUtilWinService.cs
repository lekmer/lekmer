﻿using System;
using System.Configuration;
using System.Timers;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using ServiceBase = System.ServiceProcess.ServiceBase;
using Timer = System.Timers.Timer;

namespace Litium.Lekmer.StatisticsUtil.Service
{
	public partial class StatisticsUtilWinService : ServiceBase
	{
		private const string _timerIntervalSettingName = "TimerInterval";
		private const string _dateTimeFromName = "DateTimeFrom";
		private const string _startHourName = "StartHour";
		private const int _timerIntervalDefaultValue = 86400; /*seconds (24hours)*/
		private int _startFromHourDefaultValue = 0;
		private int _dateTimeFromValue = 30;
		private Timer _startFrom;
		TimeSpan _tsDelay;
		private readonly Timer _timer;
		private readonly ILekmerProductSecureService _lekmerProductSecureService;
		private readonly IChannelSecureService _channelSecureService;

		public StatisticsUtilWinService()
		{
			InitializeComponent();
			var timerIntervalInSeconds = GetSetting(_timerIntervalSettingName, _timerIntervalDefaultValue);
			_startFromHourDefaultValue = GetSetting(_startHourName, _startFromHourDefaultValue);
			_dateTimeFromValue = GetSetting(_dateTimeFromName, _dateTimeFromValue);

			//Set startup time
			SetTime();
			
			_timer = new Timer(timerIntervalInSeconds * 1000)
			{
				Enabled = false,
				AutoReset = true
			};

			_timer.Elapsed += TimerElapsed;
			_lekmerProductSecureService = (ILekmerProductSecureService)IoC.Resolve<IProductSecureService>();
			_channelSecureService = IoC.Resolve<IChannelSecureService>();
		}

		void StartFrom_Elapsed(object sender, ElapsedEventArgs e)
		{
			_timer.Start();
			UpdatePopularity();
		}

		protected override void OnStart(string[] args)
		{
			_startFrom = new Timer(_tsDelay.TotalMilliseconds)
			{
				Enabled = false,
				AutoReset = false
			};
			_startFrom.Elapsed += StartFrom_Elapsed;
			_startFrom.Start();
		}

		protected override void OnStop()
		{
			_startFrom.Stop();
			_timer.Stop();
		}

		private void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			UpdatePopularity();
		}

		private void UpdatePopularity()
		{
			var channels = _channelSecureService.GetAll();
			_lekmerProductSecureService.UpdatePopularity(channels, DateTime.Now.AddDays(-_dateTimeFromValue), DateTime.Now);
		}

		private static int GetSetting(string settingName, int defaultValue)
		{
			int value;
			var setting = ConfigurationManager.AppSettings[settingName];
			if (!string.IsNullOrEmpty(setting) && int.TryParse(setting, out value))
				return value;
			return defaultValue;
		}
		private void SetTime()
		{
			DateTime start = new DateTime();
			if (DateTime.Now.Hour < _startFromHourDefaultValue)
			{
				start = DateTime.Today.AddHours(_startFromHourDefaultValue);
				_tsDelay = start - DateTime.Now;
				return;
			}
			if (DateTime.Now.Hour > _startFromHourDefaultValue)
			{
				start = DateTime.Today.AddDays(1).AddHours(_startFromHourDefaultValue);
				_tsDelay = start - DateTime.Now;
				return;
			}

			//start immediately
			_tsDelay = new TimeSpan(0, 0, 1);
		}
	}
}
