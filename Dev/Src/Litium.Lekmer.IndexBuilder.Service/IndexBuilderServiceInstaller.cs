﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace Litium.Lekmer.IndexBuilder.Service
{
	[RunInstaller(true)]
	public partial class IndexBuilderServiceInstaller : Installer
	{
		public IndexBuilderServiceInstaller()
		{
			InitializeComponent();

			var serviceProcessInstaller = new ServiceProcessInstaller();
			var serviceInstaller = new ServiceInstaller();

			//# Service Account Information
			serviceProcessInstaller.Account = ServiceAccount.LocalSystem;
			serviceProcessInstaller.Username = null;
			serviceProcessInstaller.Password = null;

			//# Service Information
			serviceInstaller.DisplayName = "Litium.Lekmer.IndexBuilder.Service";
			serviceInstaller.StartType = ServiceStartMode.Manual;

			//# This must be identical to the WindowsService.ServiceBase name
			//# set in the constructor of WindowsService.cs
			serviceInstaller.ServiceName = "Litium.Lekmer.IndexBuilder.Service";

			Installers.Add(serviceProcessInstaller);
			Installers.Add(serviceInstaller);
		}
	}
}