﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;

namespace Litium.Lekmer.ProductFilter
{
	public class LekmerGridControl<TItem> : GridControl<TItem> where TItem : class
	{
		private static string GetGenericFilterPageUrl()
		{
			var item = IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, "filter");
			if (item == null || item.Url.IsNullOrEmpty())
			{
				return null;
			}
			return UrlHelper.ResolveUrlHttp(item.Url);
		}

		protected override string RenderItem(int columnIndex, TItem item)
		{
			string genericFilterPageUrl = GetGenericFilterPageUrl();
			ILekmerProductView lekmerProductView = (ILekmerProductView)item;
			Fragment fragmentItem = Template.GetFragment(ItemFragmentName);
			AddPositionCondition(fragmentItem, columnIndex, ColumnCount);
			fragmentItem.AddEntity((IProductView)lekmerProductView);
			fragmentItem.AddVariable("ImageList", RenderImageList(lekmerProductView), VariableEncoding.None);
			fragmentItem.AddVariable("Product.Description", lekmerProductView.Description, VariableEncoding.None);
			fragmentItem.AddVariable("Iterate:SizeOption", RenderSizeOptions(lekmerProductView), VariableEncoding.None);
			fragmentItem.AddRegexVariable(
@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;

					return RenderTagGroup(lekmerProductView, tagGroupCommonName, genericFilterPageUrl);
				},
					VariableEncoding.None);
			return fragmentItem.Render();
		}

		private string RenderSizeOptions(ILekmerProductView lekmerProductView)
		{
			var optionBuilder = new StringBuilder();
			foreach (var size in lekmerProductView.ProductSizes)
			{
				var fragmentOption = Template.GetFragment("ProductSizeOption");
				fragmentOption.AddEntity(size);
				optionBuilder.AppendLine(fragmentOption.Render());
			}
			return optionBuilder.ToString();
		}

		private string RenderTagGroup(ILekmerProductView lekmerProduct, string commonName, string genericFilterPageUrl)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var tagGroup = lekmerProduct.TagGroups
				.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}

			var fragmentTagGroup = Template.GetFragment("ProductTagGroup");
			fragmentTagGroup.AddCondition("CountIsZero", tagGroup.Tags.Count == 0);
			fragmentTagGroup.AddVariable("TagGroupName", AliasHelper.GetAliasValue("Product.Detail." + commonName), VariableEncoding.None);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup, genericFilterPageUrl), VariableEncoding.None);
			return fragmentTagGroup.Render();
		}

		private string RenderTags(ITagGroup tagGroup, string genericFilterPageUrl)
		{
			var tagBuilder = new StringBuilder();
			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				string tagUrl = GetTagUrl(tag, genericFilterPageUrl);

				var fragmentTag = Template.GetFragment("ProductTag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				fragmentTag.AddVariable("Tag.Url", tagUrl);
				fragmentTag.AddCondition("Tag.HasUrl", tagUrl != null);

				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}
			return tagBuilder.ToString();
		}

		private static string GetTagUrl(ITag tag, string genericFilterPageUrl)
		{
			if (genericFilterPageUrl.IsNullOrEmpty())
			{
				return null;
			}

			var query = new QueryBuilder();
			query.Add("mode", "filter");
			query.Add("taggroup" + tag.TagGroupId + "-tag-id", tag.Id.ToString(CultureInfo.InvariantCulture));
			return genericFilterPageUrl + query;
		}

		protected virtual string RenderImageList(ILekmerProductView lekmerProductView)
		{
			ImageCollection images = IoC.Resolve<IImageService>().GetAllByProductAndGroup(UserContext.Current,
																						  lekmerProductView.Id, 4, null,
																						  int.MaxValue);
			if (images.Count == 0) return null;
			Fragment fragmentList = Template.GetFragment("ImageList");
			fragmentList.AddVariable("Iterate:ImageRow", RenderImageRows(images), VariableEncoding.None);
			return fragmentList.Render();
		}

		protected virtual string RenderImageRows(ImageCollection images)
		{
			var imageBuilder = new StringBuilder();
			foreach (IImage image in images)
			{
				Fragment fragmentRow = Template.GetFragment("ImageRow");
				fragmentRow.AddVariable("Iterate:Image", RenderImage(image), VariableEncoding.None);
				imageBuilder.AppendLine(fragmentRow.Render());
			}
			return imageBuilder.ToString();
		}
		protected virtual string RenderImage(IImage image)
		{
			Fragment fragmentItem = Template.GetFragment("Image");
			fragmentItem.AddEntity(image);
			return fragmentItem.Render();
		}
	}
}
