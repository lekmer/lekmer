using System;
using System.Globalization;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter.Web
{
	public class BlockProductFilterEntityMapper : BlockEntityMapper<IBlockProductFilter>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductFilter item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}