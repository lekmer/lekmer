using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter.Web
{
	public class AvailableFilterOptions
	{
		public IEnumerable<IBrand> Brands { get; set; }

        public IEnumerable<IAgeInterval> AgeIntervals { get; set; }

        public IEnumerable<IPriceInterval> PriceIntervals { get; set; }

		public IEnumerable<ITagGroup> TagGroups { get; set; }

		public IEnumerable<ISize> Sizes { get; set; }

		public IEnumerable<ICategory> Level1Categories { get; set; }

		public IEnumerable<ICategory> Level2Categories { get; set; }

		public IEnumerable<ICategory> Level3Categories { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<SortOption, string> SortOptions { get; set; }

		public IEnumerable<int> PageSizeOptions { get; set; }

		public bool HasSecondaryTemplate { get; set; }
	}
}