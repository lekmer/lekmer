using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter.Web
{
    public class FilterFormControl : ControlBase
    {
        public Template Template { get; set; }

        public SelectedFilterOptions SelectedFilterOptions { get; set; }

        public AvailableFilterOptions AvailableFilterOptions { get; set; }

        public FilteredProductRangeStatistics FilterStatistics { get; set; }

        public string Render()
        {
            if (Template == null) throw new InvalidOperationException("Template is not set.");
            if (SelectedFilterOptions == null) throw new InvalidOperationException("SelectedFilterOptions is not set.");
            if (AvailableFilterOptions == null) throw new InvalidOperationException("AvailableFilterOptions is not set.");
            if (FilterStatistics == null) throw new InvalidOperationException("FilterStatistics is not set.");

            var fragmentFilterForm = Template.GetFragment("FilterForm");
            fragmentFilterForm.AddVariable("Iterate:BrandOption", RenderBrandOptions(), VariableEncoding.None);
            fragmentFilterForm.AddVariable("Iterate:AgeIntervalOption", RenderAgeIntervalOptions(), VariableEncoding.None);
            fragmentFilterForm.AddVariable("Iterate:PriceIntervalOption", RenderPriceIntervalOptions(), VariableEncoding.None);

            fragmentFilterForm.AddRegexVariable(@"\[TagGroup\(\""(.+)\""\)\]",
                delegate(Match match)
                {
                    string tagGroupCommonName = match.Groups[1].Value;

                    return RenderTagGroup(tagGroupCommonName);
                },
                    VariableEncoding.None);

            fragmentFilterForm.AddVariable("Iterate:Level1CategoryOption", RenderLevel1CategoryOptions(), VariableEncoding.None);

            bool hasLevel2Categories = AvailableFilterOptions.Level2Categories != null &&
                                       AvailableFilterOptions.Level2Categories.Count() > 0;
            fragmentFilterForm.AddCondition("HasLevel2CategoryOptions", hasLevel2Categories);
            if (hasLevel2Categories)
            {
                fragmentFilterForm.AddVariable("Iterate:Level2CategoryOption", RenderLevel2CategoryOptions(), VariableEncoding.None);
            }

            bool hasLevel3Categories = AvailableFilterOptions.Level3Categories != null &&
                                       AvailableFilterOptions.Level3Categories.Count() > 0;
            fragmentFilterForm.AddCondition("HasLevel3CategoryOptions", hasLevel3Categories);
            if (hasLevel3Categories)
            {
                fragmentFilterForm.AddVariable("Iterate:Level3CategoryOption", RenderLevel3CategoryOptions(), VariableEncoding.None);
            }

            fragmentFilterForm.AddVariable("Iterate:SizeOption", RenderSizeOptions(), VariableEncoding.None);

            fragmentFilterForm.AddVariable("Iterate:SortOption", RenderSortOptions(), VariableEncoding.None);
            fragmentFilterForm.AddVariable("Iterate:PageSizeOption", RenderPageSizeOptions(), VariableEncoding.None);
            fragmentFilterForm.AddCondition("HasSecondaryTemplate", AvailableFilterOptions.HasSecondaryTemplate);
            fragmentFilterForm.AddCondition("UseSecondaryTemplate", SelectedFilterOptions.UseSecondaryTemplate);
            AddTemplateLinks(fragmentFilterForm);

            return fragmentFilterForm.Render();
        }

        private void AddTemplateLinks(Fragment fragment)
        {
            var queryBuilder = new QueryBuilder(Request.QueryString);
            queryBuilder.Remove("usesecondarytemplate");
            string primaryQuery = queryBuilder.ToString();
            queryBuilder.Add("usesecondarytemplate", "true");
            string secondaryQuery = queryBuilder.ToString();
            string baseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
            fragment.AddVariable("PrimaryTemplateLink", baseUrl + primaryQuery);
            fragment.AddVariable("SecondaryTemplateLink", baseUrl + secondaryQuery);
        }

        private string RenderTagGroup(string commonName)
        {
            if (commonName == null) throw new ArgumentNullException("commonName");



            var tagGroup = AvailableFilterOptions.TagGroups
                .FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

            if (tagGroup == null)
            {
                return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
            }

            var fragmentTagGroup = Template.GetFragment("TagGroup");
            fragmentTagGroup.AddVariable("Iterate:TagOption", RenderTagOptions(tagGroup), VariableEncoding.None);
            fragmentTagGroup.AddVariable("TagGroup.Id", tagGroup.Id.ToString(CultureInfo.InvariantCulture));
            return fragmentTagGroup.Render();
        }

        private string RenderTagOptions(ITagGroup tagGroup)
        {
            var tagBuilder = new StringBuilder();
            foreach (ITag tag in tagGroup.Tags)
            {
                int tagId = tag.Id;
                int count;
                FilterStatistics.TagCount.TryGetValue(tagId, out count);
                bool isSelected = SelectedFilterOptions.GroupedTagIds
                    .Any(group => group != null && group.Any(id => id == tagId));
                tagBuilder.AppendLine(RenderOption("TagOption", tagId, tag.Value, isSelected, count));
            }
            return tagBuilder.ToString();
        }

        private string RenderSortOptions()
        {
            var sortOptionBuilder = new StringBuilder();
            foreach (var sortOption in AvailableFilterOptions.SortOptions)
            {
                var fragmentSortOption = Template.GetFragment("SortOption");
                fragmentSortOption.AddVariable("Name", sortOption.Value, VariableEncoding.None);
                fragmentSortOption.AddVariable("Value", sortOption.Key.ToString());
                fragmentSortOption.AddCondition("IsSelected", sortOption.Key == SelectedFilterOptions.SortOption);
                sortOptionBuilder.AppendLine(fragmentSortOption.Render());
            }
            return sortOptionBuilder.ToString();
        }

        private string RenderPageSizeOptions()
        {
            var pageSizeBuilder = new StringBuilder();
            foreach (var pageSize in AvailableFilterOptions.PageSizeOptions)
            {
                var fragmentPageSizeOption = Template.GetFragment("PageSizeOption");
                fragmentPageSizeOption.AddVariable("Name", pageSize.ToString(CultureInfo.InvariantCulture));
                fragmentPageSizeOption.AddVariable("Value", pageSize.ToString(CultureInfo.InvariantCulture));
                fragmentPageSizeOption.AddCondition("IsSelected", pageSize == SelectedFilterOptions.PageSize);
                pageSizeBuilder.AppendLine(fragmentPageSizeOption.Render());
            }
            return pageSizeBuilder.ToString();
        }

        private string RenderAgeIntervalOptions()
        {
            var ageIntervalBuilder = new StringBuilder();
            foreach (IAgeInterval ageInterval in AvailableFilterOptions.AgeIntervals)
            {
                int ageIntervalId = ageInterval.Id;

                var ageIntervalCount =
                    FilterStatistics.AgeIntervalCount.FirstOrDefault(intervalCount => intervalCount.Id == ageIntervalId);
                int count = ageIntervalCount != null ? ageIntervalCount.Count : 0;
                bool isSelected = SelectedFilterOptions.AgeIntervalIds.Any(id => id == ageIntervalId);
                ageIntervalBuilder.AppendLine(
                    RenderOption(
                        "AgeIntervalOption", ageIntervalId,
                        ageInterval.Title,
                        isSelected, count));
            }
            return ageIntervalBuilder.ToString();
        }

        private string RenderPriceIntervalOptions()
        {
            var priceIntervalBuilder = new StringBuilder();
            foreach (IPriceInterval priceInterval in AvailableFilterOptions.PriceIntervals)
            {
                int priceIntervalId = priceInterval.Id;

                var priceIntervalCount =
                    FilterStatistics.PriceIntervalCount.FirstOrDefault(intervalCount => intervalCount.Id == priceIntervalId);
                int count = priceIntervalCount != null ? priceIntervalCount.Count : 0;

                bool isSelected = SelectedFilterOptions.PriceIntervalIds.Any(id => id == priceIntervalId);

                priceIntervalBuilder.AppendLine(
                    RenderOption(
                        "PriceIntervalOption", priceIntervalId,
                        priceInterval.Title,
                        isSelected, count));
            }
            return priceIntervalBuilder.ToString();
        }

        private string RenderBrandOptions()
        {
            var brandBuilder = new StringBuilder();
            foreach (IBrand brand in AvailableFilterOptions.Brands)
            {
                int brandId = brand.Id;
                int count;
                FilterStatistics.BrandCount.TryGetValue(brandId, out count);
                bool isSelected = SelectedFilterOptions.BrandIds.Any(id => id == brandId);
                brandBuilder.AppendLine(RenderOption("BrandOption", brandId, brand.Title, isSelected, count));
            }
            return brandBuilder.ToString();
        }

        private string RenderLevel1CategoryOptions()
        {
            return RenderCategoryOptions("Level1CategoryOption", AvailableFilterOptions.Level1Categories, SelectedFilterOptions.Level1CategoryIds);
        }

        private string RenderLevel2CategoryOptions()
        {
            return RenderCategoryOptions("Level2CategoryOption", AvailableFilterOptions.Level2Categories, SelectedFilterOptions.Level2CategoryIds);
        }

        private string RenderLevel3CategoryOptions()
        {
            return RenderCategoryOptions("Level3CategoryOption", AvailableFilterOptions.Level3Categories, SelectedFilterOptions.Level3CategoryIds);
        }

        private string RenderCategoryOptions(string optionFragmentName, IEnumerable<ICategory> availableCategories, IEnumerable<int> selectedCategoryIds)
        {
            if (optionFragmentName == null) throw new ArgumentNullException("optionFragmentName");
            if (availableCategories == null) throw new ArgumentNullException("availableCategories");
            if (selectedCategoryIds == null) throw new ArgumentNullException("selectedCategoryIds");

            var categoryBuilder = new StringBuilder();

            foreach (ICategory category in availableCategories.OrderBy(product => product.Title))
            {
                int categoryId = category.Id;
                int count;
                FilterStatistics.CategoryCount.TryGetValue(categoryId, out count);
                bool isSelected = selectedCategoryIds.Any(id => id == categoryId);
                categoryBuilder.AppendLine(RenderOption(optionFragmentName, categoryId, category.Title, isSelected, count));
            }
            return categoryBuilder.ToString();
        }

        private string RenderSizeOptions()
        {
            var sizeBuilder = new StringBuilder();
            foreach (ISize size in AvailableFilterOptions.Sizes)
            {
                int sizeId = size.Id;
                int count;
                FilterStatistics.SizeCount.TryGetValue(sizeId, out count);
                bool isSelected = SelectedFilterOptions.SizeIds.Any(id => id == sizeId);
                sizeBuilder.AppendLine(RenderOption("SizeOption", sizeId, size.EU.ToString(CultureInfo.InvariantCulture), isSelected, count));
            }
            return sizeBuilder.ToString();
        }

        private string RenderOption(string optionFragmentName, int id, string name, bool isSelected, int count)
        {
            var fragmentOption = Template.GetFragment(optionFragmentName);
            fragmentOption.AddVariable("Name", name);
            fragmentOption.AddVariable("Value", id.ToString(CultureInfo.InvariantCulture));
            fragmentOption.AddCondition("IsSelected", isSelected);
            fragmentOption.AddVariable("Count", count.ToString(CultureInfo.InvariantCulture));
            fragmentOption.AddCondition("CountIsZero", count == 0);
            return fragmentOption.Render();
        }
    }
}

