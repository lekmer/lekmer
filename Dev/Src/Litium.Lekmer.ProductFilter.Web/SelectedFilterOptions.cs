using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Litium.Lekmer.ProductFilter.Web
{
    public class SelectedFilterOptions
    {
        private readonly Collection<int> _productIds = new Collection<int>();
        private readonly Collection<int> _brandIds = new Collection<int>();
        private readonly Collection<int> _ageIntervalIds = new Collection<int>();
        private readonly Collection<int> _priceIntervalIds = new Collection<int>();
        private readonly Collection<IEnumerable<int>> _groupedTagIds = new Collection<IEnumerable<int>>();
        private readonly Collection<int> _level1CategoryIds = new Collection<int>();
        private readonly Collection<int> _level2CategoryIds = new Collection<int>();
        private readonly Collection<int> _level3CategoryIds = new Collection<int>();
        private readonly Collection<int> _sizeIds = new Collection<int>();

        public Collection<int> ProductIds
        {
            get { return _productIds; }
        }
        public Collection<int> BrandIds
        {
            get { return _brandIds; }
        }

        public Collection<int> AgeIntervalIds
        {
            get { return _ageIntervalIds; }
        }

        public Collection<int> PriceIntervalIds
        {
            get { return _priceIntervalIds; }
        }

        [SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
        public Collection<IEnumerable<int>> GroupedTagIds
        {
            get { return _groupedTagIds; }
        }

        public Collection<int> Level1CategoryIds
        {
            get { return _level1CategoryIds; }
        }

        public Collection<int> Level2CategoryIds
        {
            get { return _level2CategoryIds; }
        }

        public Collection<int> Level3CategoryIds
        {
            get { return _level3CategoryIds; }
        }

        public Collection<int> SizeIds
        {
            get { return _sizeIds; }
        }

        public SortOption SortOption { get; set; }

        public bool UseSecondaryTemplate { get; set; }

        public int PageSize { get; set; }




        public void SetSortOption(string value)
        {
            if (value == null || !Enum.IsDefined(typeof(SortOption), value))
                return;

            SortOption = (SortOption)Enum.Parse(typeof(SortOption), value);
        }


        public FilterQuery BuildQuery(AvailableFilterOptions availableFilterOptions)
        {
            if (availableFilterOptions == null) throw new ArgumentNullException("availableFilterOptions");

            var query = new FilterQuery();
            PopulateCollection(query.BrandIds, BrandIds);
            PopulateCollection(
                query.AgeIntervals,
                AgeIntervalIds
                    .Select(id => availableFilterOptions.AgeIntervals.FirstOrDefault(interval => interval.Id == id))
                    .Where(interval => interval != null));
            PopulateCollection(
                query.PriceIntervals,
                PriceIntervalIds
                    .Select(id => availableFilterOptions.PriceIntervals.FirstOrDefault(interval => interval.Id == id))
                    .Where(interval => interval != null));
            PopulateCollection(query.GroupedTagIds, GroupedTagIds);
            PopulateCollection(query.Level1CategoryIds, Level1CategoryIds);
            PopulateCollection(query.Level2CategoryIds, Level2CategoryIds);
            PopulateCollection(query.Level3CategoryIds, Level3CategoryIds);
            PopulateCollection(query.SizeIds, SizeIds);
            PopulateCollection(query.ProductIds, ProductIds);
            query.SortOption = SortOption;
            return query;
        }

        private static void PopulateCollection<T>(ICollection<T> targetCollection, IEnumerable<T> source)
        {
            if (targetCollection == null) throw new ArgumentNullException("targetCollection");
            if (source == null) throw new ArgumentNullException("source");

            foreach (T item in source)
            {
                targetCollection.Add(item);
            }
        }
    }
}