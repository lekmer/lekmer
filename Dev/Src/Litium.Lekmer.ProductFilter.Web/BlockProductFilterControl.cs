﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter.Web
{
    public class BlockProductFilterControl : BlockControlBase<IBlockProductFilter>
    {
        private bool _isCookieMode;
        private readonly ITemplateFactory _templateFactory;
        private readonly IBlockProductFilterService _blockProductFilterService;
        private readonly IBrandService _brandService;
        private readonly IAgeIntervalService _ageIntervalService;
        private readonly IPriceIntervalService _priceIntervalService;
        private readonly IFilterProductService _filterProductService;
        private readonly ICategoryService _categoryService;
        private readonly ITagGroupService _tagGroupService;
        private readonly ISizeService _sizeService;

        private readonly SelectedFilterOptions _selectedFilterOptions = new SelectedFilterOptions();
        private readonly AvailableFilterOptions _availableFilterOptions = new AvailableFilterOptions();

        private ProductCollection _products;
        private FilteredProductRangeStatistics _filterStatistics;
        private IPagingControl _pagingControl;

        private readonly Regex _tagGroupParameterRegex =
            new Regex(@"^taggroup(\d+)-tag-id$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public BlockProductFilterControl(
            ITemplateFactory templateFactory, IBlockProductFilterService blockProductFilterService,
            IBrandService brandService, IAgeIntervalService ageIntervalService, IPriceIntervalService priceIntervalService,
            IFilterProductService filterProductService, ITagGroupService tagGroupService, ICategoryService categoryService,
            ISizeService sizeService)
            : base(templateFactory)
        {
            _templateFactory = templateFactory;
            _blockProductFilterService = blockProductFilterService;
            _brandService = brandService;
            _ageIntervalService = ageIntervalService;
            _priceIntervalService = priceIntervalService;
            _filterProductService = filterProductService;
            _tagGroupService = tagGroupService;
            _categoryService = categoryService;
            _sizeService = sizeService;
        }

        protected override IBlockProductFilter GetBlockById(int blockId)
        {
            IBlockProductFilter blockProductFilter = _blockProductFilterService.GetById(UserContext.Current, blockId);
            return blockProductFilter;
        }

        /// <summary>
        /// Renders the control content.
        /// </summary>
        /// <returns>Rendered <see cref="BlockContent"/>.</returns>
        protected override BlockContent RenderCore()
        {
            Initialize();

            _pagingControl.TotalCount = _products.TotalCount;
            PagingContent pagingContent = _pagingControl.Render();
            var filterFormControl = CreateFilterFormControl();
            Fragment fragmentContent = Template.GetFragment("Content");
            fragmentContent.AddVariable("FilterForm", filterFormControl.Render(), VariableEncoding.None);
            fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
            fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
            fragmentContent.AddVariable("ProductTotalCount", _products.TotalCount.ToString(CultureInfo.InvariantCulture));
            fragmentContent.AddVariable("ProductDisplayedCount", _products.Count.ToString(CultureInfo.InvariantCulture));
            fragmentContent.AddVariable("PageCount",
                                        ((int)Math.Ceiling((decimal)_products.TotalCount / _pagingControl.PageSize))
                                            .ToString(CultureInfo.InvariantCulture));
            fragmentContent.AddEntity(Block);

            var fragmentHead = Template.GetFragment("Head");
            fragmentHead.AddEntity(Block);
            fragmentHead.AddVariable("Param.ProductIds", RenderProductIdsArrayParam());
            string head =
                (fragmentHead.Render() ?? string.Empty) +
                (pagingContent.Head ?? string.Empty);

            return new BlockContent(
                head,
                fragmentContent.Render());
        }

        private string RenderProductList()
        {
            int columnCount;
            if (!int.TryParse(Template.GetSettingOrNull("ColumnCount"), out columnCount))
            {
                columnCount = 1;
            }

            if (_isCookieMode)
            {
                return new LekmerGridControl<IProduct>
                {
                    Items = _products,
                    ColumnCount = columnCount,
                    RowCount = (int)Math.Ceiling((decimal)_selectedFilterOptions.PageSize / columnCount),
                    Template = Template,
                    ListFragmentName = "ProductList",
                    RowFragmentName = "ProductRow",
                    ItemFragmentName = "Product",
                    EmptySpaceFragmentName = "EmptySpace"
                }.Render();
            }

            return new GridControl<IProduct>
            {
                Items = _products,
                ColumnCount = columnCount,
                RowCount = (int)Math.Ceiling((decimal)_selectedFilterOptions.PageSize / columnCount),
                Template = Template,
                ListFragmentName = "ProductList",
                RowFragmentName = "ProductRow",
                ItemFragmentName = "Product",
                EmptySpaceFragmentName = "EmptySpace"
            }.Render();
        }

        private void Initialize()
        {
            if (!string.IsNullOrEmpty(Block.ProductListCookie))
            {
                _isCookieMode = true;
            }
            PopulateSelectedFilterOptions();
            PopulateAvailableFilterOptions();
            _pagingControl = CreatePagingControl();
            _products = GetProducts();
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual ProductCollection GetProducts()
        {
            var query = _selectedFilterOptions.BuildQuery(_availableFilterOptions);

            if (_isCookieMode)
            {
                return _filterProductService.GetAllViewByQuery(
                    UserContext.Current, query, _pagingControl.SelectedPage, _pagingControl.PageSize, out _filterStatistics);
            }
            return _filterProductService.GetAllByQuery(
             UserContext.Current, query, _pagingControl.SelectedPage, _pagingControl.PageSize, out _filterStatistics);

        }

        private void PopulateSelectedFilterOptions()
        {
            bool? useSecondaryTemplate = Request.QueryString.GetBooleanOrNull("usesecondarytemplate");
            _selectedFilterOptions.UseSecondaryTemplate = useSecondaryTemplate ?? false;

            if (_isCookieMode)
                SetCookieProductList(_selectedFilterOptions.ProductIds);

            string mode = Request.QueryString["mode"];
            if (mode != null && mode.Equals("filter"))
            {
                SetSelectedFromQuery(_selectedFilterOptions.BrandIds, "brand-id");
                SetSelectedFromQuery(_selectedFilterOptions.AgeIntervalIds, "ageinterval-id");
                SetSelectedFromQuery(_selectedFilterOptions.PriceIntervalIds, "priceinterval-id");
                SetSelectedFromQuery(_selectedFilterOptions.Level1CategoryIds, "level1category-id");
                SetSelectedFromQuery(_selectedFilterOptions.Level2CategoryIds, "level2category-id");
                SetSelectedFromQuery(_selectedFilterOptions.Level3CategoryIds, "level3category-id");
                SetSelectedFromQuery(_selectedFilterOptions.SizeIds, "size-id");
                _selectedFilterOptions.SetSortOption(Request.QueryString["sort"]);
                SetSelectedTagIds();

                int? pageSize = Request.QueryString.GetInt32OrNull("pagesize");
                _selectedFilterOptions.PageSize = pageSize ?? GetDefaultPageSize();
            }
            else
            {
                foreach (int brandId in Block.DefaultBrandIds)
                {
                    _selectedFilterOptions.BrandIds.Add(brandId);
                }

                foreach (int tagId in Block.DefaultTagIds)
                {
                    _selectedFilterOptions.GroupedTagIds.Add(new[] { tagId });
                }

                if (Block.DefaultCategoryId.HasValue)
                {
                    SetDefaultCategoryIds(Block.DefaultCategoryId.Value);
                }

                if (Block.DefaultAgeIntervalId.HasValue)
                {
                    _selectedFilterOptions.AgeIntervalIds.Add(Block.DefaultAgeIntervalId.Value);
                }

                if (Block.DefaultPriceIntervalId.HasValue)
                {
                    _selectedFilterOptions.PriceIntervalIds.Add(Block.DefaultPriceIntervalId.Value);
                }

                if (Block.DefaultSizeId.HasValue)
                {
                    _selectedFilterOptions.SizeIds.Add(Block.DefaultSizeId.Value);
                }


                // Default values.
                _selectedFilterOptions.SetSortOption(string.IsNullOrEmpty(Block.DefaultSort) ? SortOption.Popularity.ToString() : Block.DefaultSort);

                _selectedFilterOptions.PageSize = GetDefaultPageSize();
            }
        }

        private void SetSelectedTagIds()
        {
            var parameters = Request.QueryString.AllKeys.Distinct();
            foreach (string parameter in parameters)
            {
                Match match = _tagGroupParameterRegex.Match(parameter);
                if (!match.Success) continue;

                int tagGroupId;
                if (!int.TryParse(match.Groups[1].Value, out tagGroupId)) continue;

                IEnumerable<int> tagIds = GetSelectedFromQuery(parameter);
                _selectedFilterOptions.GroupedTagIds.Add(tagIds);
            }
        }

        private void SetDefaultCategoryIds(int categoryId)
        {
            var categoryTree = _categoryService.GetAllAsTree(UserContext.Current);

            var categoryTrail = new List<int>();
            var treeItem = categoryTree.FindItemById(categoryId);
            if (treeItem == null)
            {
                return;
            }

            categoryTrail.Add(treeItem.Id);
            foreach (ICategoryTreeItem item in treeItem.GetAncestors())
            {
                if (item.Parent == null) break;

                categoryTrail.Add(item.Id);
            }

            categoryTrail.Reverse();

            if (categoryTrail.Count >= 1)
            {
                _selectedFilterOptions.Level1CategoryIds.Add(categoryTrail[0]);
            }
            if (categoryTrail.Count >= 2)
            {
                _selectedFilterOptions.Level2CategoryIds.Add(categoryTrail[1]);
            }
            if (categoryTrail.Count >= 3)
            {
                _selectedFilterOptions.Level3CategoryIds.Add(categoryTrail[2]);
            }
        }

        private void PopulateAvailableFilterOptions()
        {
            var context = UserContext.Current;

            var categoryTree = _categoryService.GetAllAsTree(context);

            _availableFilterOptions.Brands = _brandService.GetAll(context);
            _availableFilterOptions.AgeIntervals = _ageIntervalService.GetAll(context);
            _availableFilterOptions.PriceIntervals = _priceIntervalService.GetAll(context);
            _availableFilterOptions.TagGroups = _tagGroupService.GetAll(context);
            _availableFilterOptions.Sizes = _sizeService.GetAll(context);

            // Level 1 categories.
            _availableFilterOptions.Level1Categories = categoryTree.Root.Children
                .Select(item => (ICategory)item.Category);

            // Level 2 categories.
            if (_selectedFilterOptions.Level1CategoryIds.Count == 1)
            {
                ICategoryTreeItem level1TreeItem = categoryTree.FindItemById(_selectedFilterOptions.Level1CategoryIds.First());

                if (level1TreeItem != null)
                {
                    _availableFilterOptions.Level2Categories = level1TreeItem.Children
                        .Select(category => (ICategory)category.Category);
                }
            }

            // Level 3 categories.
            if (_selectedFilterOptions.Level2CategoryIds.Count == 1)
            {
                ICategoryTreeItem level2TreeItem = categoryTree.FindItemById(_selectedFilterOptions.Level2CategoryIds.First());

                if (level2TreeItem != null)
                {
                    _availableFilterOptions.Level3Categories = level2TreeItem.Children
                        .Select(category => (ICategory)category.Category);
                }
            }

            // Sort options.
            _availableFilterOptions.SortOptions =
                new Dictionary<SortOption, string>
					{
							{SortOption.Popularity, AliasHelper.GetAliasValue("ProductFilter.SortOption.Popularity")},	
							{SortOption.IsNewFrom, AliasHelper.GetAliasValue("ProductFilter.SortOption.IsNewFrom")},
			
						{SortOption.TitleAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleAsc")},
						{SortOption.TitleDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.TitleDesc")},
						{SortOption.PriceAsc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceAsc")}, 		
						{SortOption.PriceDesc, AliasHelper.GetAliasValue("ProductFilter.SortOption.PriceDesc")}
					};

            int? secondaryTemplateId = Block.SecondaryTemplateId;
            _availableFilterOptions.HasSecondaryTemplate = secondaryTemplateId.HasValue;

            // Page size options.
            _availableFilterOptions.PageSizeOptions = GetPageSizeOptions();
        }

        private IEnumerable<int> GetPageSizeOptions()
        {
            string settingPageSizeOptions = Template.GetSettingOrNull("PageSizeOptions") ?? string.Empty;
            IEnumerable<int> pageSizeOptions = settingPageSizeOptions
                .Split(new[] { ',' })
                .Select(pageSize =>
                {
                    int intPageSize;
                    return int.TryParse(pageSize, out intPageSize) ? intPageSize : 0;
                })
                .Where(pageSize => pageSize > 0);

            if (!pageSizeOptions.Any())
            {
                return new List<int> { GetDefaultPageSize() };
            }
            return pageSizeOptions;
        }

        private int GetDefaultPageSize()
        {
            string settingDefaultPageSize = Template.GetSettingOrNull("DefaultPageSize");
            int defaultPageSize;
            if (!int.TryParse(settingDefaultPageSize, out defaultPageSize))
            {
                defaultPageSize = 20;
            }
            return defaultPageSize;
        }

        private void SetSelectedFromQuery(ICollection<int> selectedItems, string parameter)
        {
            IEnumerable<int> items = GetSelectedFromQuery(parameter);
            foreach (int item in items)
            {
                selectedItems.Add(item);
            }
        }

        private void SetCookieProductList(ICollection<int> selectedItems)
        {
            if (string.IsNullOrEmpty(Block.ProductListCookie))
                return;
            IEnumerable<int> items = GetCookieProductList();

            foreach (int item in items)
            {
                selectedItems.Add(item);
            }
        }

        private IEnumerable<int> GetCookieProductList()
        {
            //test
            HttpCookie cookie = HttpContext.Current.Request.Cookies[Block.ProductListCookie];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value)) return new Collection<int> { 0 };

            string productTempIds = HttpUtility.UrlDecode(cookie.Value, Encoding.Default);
            IEnumerable<int> productIds = productTempIds
                .Split(new[] { ',' })
                .Select(value =>
                {
                    int intValue;
                    return int.TryParse(value, out intValue) ? intValue : 0;
                })
                .Where(value => value != 0);
            productIds = productIds.Distinct();
            if (productIds.Count() < 1)
            {
                return new Collection<int> { 0 };
            }
            return productIds;
        }

        private IEnumerable<int> GetSelectedFromQuery(string parameter)
        {
            if (Request.QueryString[parameter].IsNullOrTrimmedEmpty()) return new int[0];

            return Request.QueryString[parameter]
                .Split(new[] { ',' })
                .Select(value =>
                {
                    int intValue;
                    return int.TryParse(value, out intValue) ? intValue : 0;
                })
                .Where(value => value != 0).Distinct();
        }

        private IPagingControl CreatePagingControl()
        {
            var pagingControl = IoC.Resolve<IPagingControl>();
            pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
            pagingControl.PageQueryStringParameterName = BlockId + "-page";
            pagingControl.PageSize = _selectedFilterOptions.PageSize;
            pagingControl.Initialize();
            return pagingControl;
        }

        private FilterFormControl CreateFilterFormControl()
        {
            return new FilterFormControl
            {
                Template = Template,
                AvailableFilterOptions = _availableFilterOptions,
                FilterStatistics = _filterStatistics,
                SelectedFilterOptions = _selectedFilterOptions
            };
        }

        protected override Template CreateTemplate()
        {
            if (_selectedFilterOptions.UseSecondaryTemplate && Block.SecondaryTemplateId.HasValue)
            {
                return _templateFactory.Create(Block.SecondaryTemplateId.Value);
            }

            return base.CreateTemplate();
        }

        private string RenderProductIdsArrayParam()
        {
            var itemBuilder = new StringBuilder();

            for (int i = 0; i < _products.Count; i++)
            {
                if (i == 0)//first or the only item in array
                {
                    itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _products[i].Id));
                }
                else
                {
                    itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", _products[i].Id));
                }
            }

            return itemBuilder.ToString();

        }
    }
}

