﻿using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
    public class LekmerImageSecureService : ImageSecureService, ILekmerImageSecureService
    {
        protected LekmerImageRepository LekmerImageRepository { get; private set; }
        protected MediaItemRepository MediaItemRepository { get; private set; }
        protected ImageRepository ImageRepository { get; private set; }
        public LekmerImageSecureService(IAccessValidator accessValidator, ImageRepository repository, IMediaItemSecureService mediaItemSecureService, IMediaFormatSecureService mediaFormatSecureService, IMediaItemSharedService mediaItemSharedService, IImageSharedService imageSharedService,
            MediaItemRepository mediaItemRepository, ImageRepository imageRepository)
            : base(accessValidator, repository, mediaItemSecureService, mediaFormatSecureService, mediaItemSharedService, imageSharedService)
        {
            LekmerImageRepository = (LekmerImageRepository)repository;
            MediaItemRepository = mediaItemRepository;
            ImageRepository = imageRepository;
        }

        public void SaveMovie(ILekmerImage movie)
        {
            if (movie.Id <= 0)
            {
                MediaItemRepository.Insert(movie);
                ImageRepository.Insert(movie);
            }
            LekmerImageRepository.SaveMovie(movie);
        }
    }
}
