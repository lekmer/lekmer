﻿using System.Data;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Mapper;

namespace Litium.Lekmer.Media.Mapper
{
    public class LekmerImageDataMapper : ImageDataMapper
    {
        public LekmerImageDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }
        protected override IImage Create()
        {
            var cartitem = (ILekmerImage)base.Create();
            cartitem.Link = MapNullableValue<string>("LekmerImage.Link");
            cartitem.Parameter = MapNullableValue<string>("LekmerImage.Parameter");

            cartitem.TumbnailImageUrl = MapNullableValue<string>("LekmerImage.TumbnailImageUrl");
            cartitem.HasImage = MapNullableValue<bool>("LekmerImage.HasImage");
            return cartitem;
        }
    }
}
