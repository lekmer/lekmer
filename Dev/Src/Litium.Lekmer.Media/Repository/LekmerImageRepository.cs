﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media.Repository
{
    public class LekmerImageRepository : ImageRepository
    {
        protected virtual DataMapperBase<IImage> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IImage>(dataReader);
        }
        public void SaveMovie(ILekmerImage entity)
        {
            if (entity == null) throw new ArgumentNullException("entity");
            IDataParameter[] parameters =
                { 
                    ParameterHelper.CreateParameter("MediaId", entity.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("Link", entity.Link, SqlDbType.NVarChar),
                    ParameterHelper.CreateParameter("Parameter", entity.Parameter, SqlDbType.NVarChar),
                    ParameterHelper.CreateParameter("HasImage", entity.HasImage, SqlDbType.Bit),
                    ParameterHelper.CreateParameter("TumbnailImageUrl", entity.TumbnailImageUrl, SqlDbType.NVarChar)
                };

            DatabaseSetting dbSettings = new DatabaseSetting("ImageRepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pLekmerImageSaveMovie]", parameters, dbSettings);
        }
    }
}
