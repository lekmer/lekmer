﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Incordia.Enterprise.CustomerService.WebService;

namespace Litium.Lekmer.ServiceCenter.Contract.Service
{
    public interface IServiceCenterService
    {
        /// <summary>
        /// Gets a list of available public categories for current channel
        /// </summary>
        /// <param name="display">Display property. (Bitwise comparison)
        /// 1 - Hidden
        /// 2 - Private
        /// 4 - Public
        /// 7 - All</param>
        /// <param name="siteName">The common name for the application/channel</param>
        /// <returns>A List<CSMCategory> with categories/returns>
        /// <author>Johan Liljeros</author>
        /// <date>2007-11-26</date>
        List<CSMCategory> GetCSMCategories(int display, string siteName);

        ///Overloaded methods for save case

        #region Save case

        /// <summary>
        /// Saves a case
        /// </summary>
        /// <param name="category">The category selected by the visitor, required</param>
        /// <param name="firstName">First name entered by the visitor, required</param>
        /// <param name="lastName">Last name entered by the visitor, required</param>
        /// <param name="email">E-Mail entered by the visitor, required</param>
        /// <param name="customerId">Customer number entered by the visitor, optional</param>
        /// <param name="orderId">Order number entered by the visitor, optional</param>
        /// <param name="message">Message entered by the visitor, required</param>
        /// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
        /// <author>Johan Liljeros</author>
        /// <date>2007-11-26</date>
        string SaveCase(int? csmCaseId, int categoryId, string firstName, string lastName, string email, int? customerId,
                        int? orderId, string civicOcrNumber, string title, string message, int channelId, string siteName);

        string SaveCase(int? csmCaseId, int categoryId, string firstName, string lastName, string email, int? orderId,
                string civicOcrNumber, string title, string message, int channelId, string siteName);

        /// <summary>
        /// Saves a case
        /// </summary>
        /// <param name="category">The category selected by the visitor, required</param>
        /// <param name="firstName">First name entered by the visitor, required</param>
        /// <param name="lastName">Last name entered by the visitor, required</param>
        /// <param name="email">E-Mail entered by the visitor, required</param>
        /// <param name="customerId">Customer number entered by the visitor, optional</param>
        /// <param name="message">Message entered by the visitor, required</param>
        /// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
        /// <author>Johan Liljeros</author>
        /// <date>2007-11-26</date>
        string SaveCase(int categoryId, string firstName, string lastName, string email, int? customerId, string civicOcrNumber, string title, string message,
                        int channelId, string siteName);

        /// <summary>
        /// Saves a case
        /// </summary>
        /// <param name="category">The category selected by the visitor, required</param>
        /// <param name="firstName">First name entered by the visitor, required</param>
        /// <param name="lastName">Last name entered by the visitor, required</param>
        /// <param name="email">E-Mail entered by the visitor, required</param>
        /// <param name="message">Message entered by the visitor, required</param>
        /// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
        /// <author>Johan Liljeros</author>
        /// <date>2007-11-26</date>
        string SaveCase(int categoryId, string firstName, string lastName, string email, string civicOcrNumber, string title, string message, int channelId,
                        string siteName);

        #endregion

        /// <summary>
        /// Gets a specific
        /// </summary>
        /// <param name="ticketNumber">The ticket number for the specific case</param>
        /// <returns>The case as CustomerServiceWebService</returns>
        /// <author>Johan Liljeros</author>
        /// <date>2007-11-26</date>
        CSMCustomerCaseDetails GetCaseDetails(string ticketNumber, string email, string siteName);

        /// <summary>
        /// Gets a list of cases for a specific e-mail address
        /// </summary>
        /// <param name="email">The e-mail</param>
        /// <returns>A List<CustomerServiceWebService> of cases</returns>
        List<CSMCustomerCase> GetCasesByEMail(string email, string channelName);

        List<CSMCustomerCaseDetails> GetCases(string email, string channelName);

        CSMCustomerCaseDetails GetCase(string ticket, string siteName, string customerEmail);
        CSMCustomerCaseDetails GetCaseByGuid(string guid, string siteName);

        void AddMessageToCase(string ticket, string reply);
    }
}
