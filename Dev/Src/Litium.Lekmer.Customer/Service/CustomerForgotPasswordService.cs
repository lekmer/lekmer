﻿using System;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public class CustomerForgotPasswordService : ICustomerForgotPasswordService
	{
		protected CustomerForgotPasswordRepository Repository { get; private set; }

		public CustomerForgotPasswordService(CustomerForgotPasswordRepository repository)
		{
			Repository = repository;
		}

		public ICustomerForgotPassword Create(int customerId)
		{
			var customerForgotPassword = IoC.Resolve<ICustomerForgotPassword>();
			customerForgotPassword.CustomerId = customerId;
			customerForgotPassword.Guid = Guid.NewGuid();
			customerForgotPassword.CreatedDate = DateTime.Now;
			return customerForgotPassword;
		}

		public ICustomerForgotPassword GetById(Guid id)
		{
			return Repository.GetById(id);
		}

		public void Save(ICustomerForgotPassword customerForgotPassword)
		{
			if (customerForgotPassword == null) throw new ArgumentNullException("customerForgotPassword");
			Repository.Save(customerForgotPassword);
		}

		public void Delete(int customerId)
		{
			Repository.Delete(customerId);
		}
	}
}

