﻿using System;
using Litium.Framework.Transaction;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerInformationService : CustomerInformationService
	{
		public LekmerCustomerInformationService(CustomerInformationRepository customerInformationRepository, IAddressTypeService addressTypeService, IAddressService addressService)
			: base(customerInformationRepository, addressTypeService, addressService)
		{
		}
		public override void Save(IUserContext context, ICustomerInformation customerInformation)
		{
			if (customerInformation == null)
				throw new ArgumentNullException("customerInformation");

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				Repository.Save(customerInformation);
				customerInformation.DefaultBillingAddressId = SaveAddress(context, customerInformation, customerInformation.DefaultBillingAddress, "BillingAddress");

				if (customerInformation.DefaultDeliveryAddress != null && customerInformation.DefaultDeliveryAddress.Id != customerInformation.DefaultBillingAddress.Id)
				{
					customerInformation.DefaultDeliveryAddressId = SaveAddress(context, customerInformation, customerInformation.DefaultDeliveryAddress, "DeliveryAddress");
				}
				else
				{
					customerInformation.DefaultDeliveryAddress = customerInformation.DefaultBillingAddress;
					customerInformation.DefaultDeliveryAddressId = customerInformation.DefaultBillingAddressId;
				}

				// Saving customer information once again to save its addresses values.
				Repository.Save(customerInformation);

				transactedOperation.Complete();
			}
		}
	}
}
