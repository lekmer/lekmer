﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Mapper
{
	public class CustomerForgotPasswordDataMapper : DataMapperBase<ICustomerForgotPassword>
    {
        public CustomerForgotPasswordDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override ICustomerForgotPassword Create()
        {
            var customerForgotPassword = IoC.Resolve<ICustomerForgotPassword>();
            customerForgotPassword.CustomerId = MapValue<int>("ForgotPassword.CustomerId");
            customerForgotPassword.Guid = MapValue<Guid>("ForgotPassword.Guid");
            customerForgotPassword.CreatedDate = MapValue<DateTime>("ForgotPassword.CreatedDate");
            customerForgotPassword.Status = BusinessObjectStatus.Untouched;
            return customerForgotPassword;
        }
    }
}
