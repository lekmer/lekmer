namespace Litium.Lekmer.ProductFilter
{
	public enum SortOption
	{
		TitleAsc,
		TitleDesc,
		PriceAsc,
		PriceDesc,
		Popularity,
		IsNewFrom 
	}
}