using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public interface IBlockProductFilterService
	{
		IBlockProductFilter GetById(IUserContext context, int id);
	}
}