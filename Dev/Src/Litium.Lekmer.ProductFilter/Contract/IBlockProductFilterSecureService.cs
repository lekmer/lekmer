﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Core;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.ProductFilter
{
	public interface IBlockProductFilterSecureService
	{
		IBlockProductFilter GetByIdSecure(int blockId);
		void Delete(ISystemUserFull systemUserFull, int blockId);
		int Save(ISystemUserFull systemUserFull, IBlockProductFilter blockProductFilter);
	}
}
