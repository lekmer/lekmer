using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter.Mapper
{
	public class BlockProductFilterDataMapper : DataMapperBase<IBlockProductFilter>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockProductFilterDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}
		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}
		protected override IBlockProductFilter Create()
		{
			var block = _blockDataMapper.MapRow();
			var bockProductFilter = IoC.Resolve<IBlockProductFilter>();
			block.ConvertTo(bockProductFilter);
			bockProductFilter.DefaultCategoryId = MapNullableValue<int?>("BlockProductFilter.DefaultCategoryId");
            bockProductFilter.SecondaryTemplateId = MapNullableValue<int?>("BlockProductFilter.SecondaryTemplateId"); 
            bockProductFilter.DefaultPriceIntervalId = MapNullableValue<int?>("BlockProductFilter.DefaultPriceIntervalId");
            bockProductFilter.DefaultAgeIntervalId = MapNullableValue<int?>("BlockProductFilter.DefaultAgeIntervalId");
			bockProductFilter.DefaultSizeId = MapNullableValue<int?>("BlockProductFilter.DefaultSizeId");
			bockProductFilter.ProductListCookie = MapNullableValue<string>("BlockProductFilter.ProductListCookie");
			bockProductFilter.DefaultSort = MapNullableValue<string>("BlockProductFilter.DefaultSort");
			bockProductFilter.Status = BusinessObjectStatus.Untouched;
			return bockProductFilter;
		}
	}
}