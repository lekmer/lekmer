using System.Collections.Generic;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterResult
	{
		public IEnumerable<FilterProduct> Products { get; set; }

		public FilteredProductRangeStatistics Statistics { get; set; }
	}
}