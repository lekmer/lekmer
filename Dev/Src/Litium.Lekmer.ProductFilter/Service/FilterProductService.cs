using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter
{
    public class FilterProductService : IFilterProductService
    {
        protected FilterProductRepository Repository { get; private set; }

        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IProductTagService _productTagService;
        private readonly IProductSizeService _productSizeService;
        private readonly IFilterProcessor _filterProcessor;

        public FilterProductService(
            FilterProductRepository filterProductRepository,
            IProductService productService,
            ICategoryService categoryService,
            IProductTagService productTagService,
            IProductSizeService productSizeService,
            IFilterProcessor filterProcessor)
        {
            Repository = filterProductRepository;
            _productService = productService;
            _categoryService = categoryService;
            _productTagService = productTagService;
            _productSizeService = productSizeService;
            _filterProcessor = filterProcessor;
        }

        public Collection<FilterProduct> GetAll(IUserContext context)
        {
            return FilterProductStickyCache.GetFilterProducts(context);
        }

        public Collection<FilterProduct> GetAllNoCache(IUserContext context)
        {
            var filterProducts = Repository.GetAll(context.Channel.Id, context.Customer);
            CompleteProducts(context, filterProducts);
            return filterProducts;
        }

        /// <summary>
        /// CompleteProducts
        /// </summary>
        /// <param name="context"></param>
        /// <param name="products"></param>
        /// <remarks>Rewritten by Dino Miralem, CDON</remarks>
        private void CompleteProducts(IUserContext context, IEnumerable<FilterProduct> products)
        {
            if (products == null) return;

            var categoryTree = _categoryService.GetAllAsTree(context);
            var productTags = _productTagService.GetAll();
            var productSizes = _productSizeService.GetAll(context);


            Dictionary<int, List<IProductTag>> productTagsHash = new Dictionary<int, List<IProductTag>>(products.Count());
            Dictionary<int, List<ProductSizeRelation>> productSizesHash = new Dictionary<int, List<ProductSizeRelation>>(products.Count());

            foreach (IProductTag productTag in productTags)
            {
                if (!productTagsHash.ContainsKey(productTag.ProductId))
                {
                    productTagsHash[productTag.ProductId] = new List<IProductTag>();
                }
                productTagsHash[productTag.ProductId].Add(productTag);
            }

            foreach (ProductSizeRelation productSizeRelation in productSizes)
            {
                if (!productSizesHash.ContainsKey(productSizeRelation.ProductId))
                {
                    productSizesHash[productSizeRelation.ProductId] = new List<ProductSizeRelation>();
                }
                productSizesHash[productSizeRelation.ProductId].Add(productSizeRelation);
            }

            foreach (FilterProduct product in products)
            {
                product.AddCategory(product.CategoryId);
                var treeItem = categoryTree.FindItemById(product.CategoryId);
                foreach (ICategoryTreeItem parentTreeItem in treeItem.GetAncestors())
                {
                    if (parentTreeItem.Parent == null) break;
                    product.AddCategory(parentTreeItem.Id);
                }

                int productId = product.Id;
                if (productTagsHash.ContainsKey(productId))
                {
                    var currentProductTags = productTagsHash[productId];
                    foreach (IProductTag productTag in currentProductTags)
                    {
                        product.AddTag(productTag.TagId);
                    }
                }

                if (productSizesHash.ContainsKey(productId))
                {
                    var currentProductSizes = productSizesHash[productId];
                    foreach (ProductSizeRelation productSize in currentProductSizes)
                    {
                        product.AddSize(productSize.SizeId);
                    }
                }
            }
        }

        //Old CompleteProducts

        //private void CompleteProducts(IUserContext context, IEnumerable<FilterProduct> products)
        //{
        //    var categoryTree = _categoryService.GetAllAsTree(context);
        //    var productTags = _productTagService.GetAll();
        //    var productSizes = _productSizeService.GetAll(context);

        //    foreach (FilterProduct product in products)
        //    {
        //        product.AddCategory(product.CategoryId);
        //        var treeItem = categoryTree.FindItemById(product.CategoryId);
        //        foreach (ICategoryTreeItem parentTreeItem in treeItem.GetAncestors())
        //        {
        //            if (parentTreeItem.Parent == null) break;
        //            product.AddCategory(parentTreeItem.Id);
        //        }

        //        int productId = product.Id;
        //        var currentProductTags = productTags.Where(t => t.ProductId == productId);
        //        foreach (IProductTag productTag in currentProductTags)
        //        {
        //            product.AddTag(productTag.TagId);
        //        }

        //        var currentProductSizes = productSizes.Where(s => s.ProductId == productId);
        //        foreach (var productSize in currentProductSizes)
        //        {
        //            product.AddSize(productSize.SizeId);
        //        }
        //    }
        //}

        public ProductCollection GetAllByQuery(
            IUserContext context, FilterQuery query, int pageNumber, int pageSize,
            out FilteredProductRangeStatistics statistics)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (query == null) throw new ArgumentNullException("query");
            if (pageNumber < 1)
                throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "Page number can't be less then 0.");

            var allProducts = GetAll(context);

            var result = _filterProcessor.Process(context, allProducts, query);

            statistics = result.Statistics;

            // Paging.
            int totalCount = result.Products.Count();
            var productIds = result.Products
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .Select(product => product.Id);

            // Populate full IProducts.
            var productIdCollection = new ProductIdCollection(productIds);
            productIdCollection.TotalCount = totalCount;
            return _productService.PopulateProducts(context, productIdCollection);
        }


        public ProductCollection GetAllViewByQuery(
            IUserContext context, FilterQuery query, int pageNumber, int pageSize,
            out FilteredProductRangeStatistics statistics)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (query == null) throw new ArgumentNullException("query");
            if (pageNumber < 1)
                throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "Page number can't be less then 0.");

            var allProducts = GetAll(context);

            var result = _filterProcessor.Process(context, allProducts, query);

            statistics = result.Statistics;

            // Paging.
            int totalCount = result.Products.Count();
            var productIds = result.Products
                .Skip((pageNumber - 1) * pageSize)
                .Take(pageSize)
                .Select(product => product.Id);

            // Populate full IProducts.
            var productIdCollection = new ProductIdCollection(productIds);
            productIdCollection.TotalCount = totalCount;
            return ((ILekmerProductService)_productService).PopulateViewProducts(context, productIdCollection);
        }
    }
}