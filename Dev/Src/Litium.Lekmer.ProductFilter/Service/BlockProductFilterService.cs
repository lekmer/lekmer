using System;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Scensum.Core;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.ProductFilter
{
	public class BlockProductFilterService : IBlockProductFilterService
	{
		protected BlockProductFilterRepository Repository { get; private set; }

		public BlockProductFilterService(BlockProductFilterRepository repository)
		{
			Repository = repository;
		}

		public IBlockProductFilter GetById(IUserContext context, int id)
		{
			if (context == null) throw new ArgumentNullException("context");

			return BlockProductFilterCache.Instance.TryGetItem(
				new BlockProductFilterKey(context.Channel.Id, id),
				delegate { return Get(context.Channel, id); });
		}

		private IBlockProductFilter Get(IChannel channel, int id)
		{
			var blockProductFilter = Repository.GetById(channel, id);
			blockProductFilter.DefaultBrandIds = Repository.GetAllBrandsByBlock(id);
			blockProductFilter.DefaultTagIds = Repository.GetAllTagsByBlock(id);
			return blockProductFilter;
		}
	}
}