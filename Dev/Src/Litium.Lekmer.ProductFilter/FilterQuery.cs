﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterQuery
	{
		private readonly Collection<int> _brandIds = new Collection<int>();
		private readonly Collection<int> _level1CategoryIds = new Collection<int>();
		private readonly Collection<int> _level2CategoryIds = new Collection<int>();
		private readonly Collection<int> _level3CategoryIds = new Collection<int>();
		private readonly Collection<IEnumerable<int>> _groupedTagIds = new Collection<IEnumerable<int>>();
		private readonly Collection<IAgeInterval> _ageIntervals = new Collection<IAgeInterval>();
		private readonly Collection<IPriceInterval> _priceIntervals = new Collection<IPriceInterval>();
		private readonly Collection<int> _productIds = new Collection<int>();
		private readonly Collection<int> _sizeIds = new Collection<int>();

		public Collection<int> BrandIds
		{
			get { return _brandIds; }
		}

		public Collection<int> Level1CategoryIds
		{
			get { return _level1CategoryIds; }
		}

		public Collection<int> Level2CategoryIds
		{
			get { return _level2CategoryIds; }
		}

		public Collection<int> Level3CategoryIds
		{
			get { return _level3CategoryIds; }
		}

		[SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
		public Collection<IEnumerable<int>> GroupedTagIds
		{
			get { return _groupedTagIds; }
		}

		public Collection<IAgeInterval> AgeIntervals
		{
			get { return _ageIntervals; }
		}

		public Collection<IPriceInterval> PriceIntervals
		{
			get { return _priceIntervals; }
		}

		public Collection<int> ProductIds
		{
			get { return _productIds; }
		}

		public SortOption SortOption { get; set; }

		public Collection<int> SizeIds
		{
			get { return _sizeIds; }
		}
	}
}