using System;
using System.Collections.Generic;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterProduct
	{
		private readonly Dictionary<int, int> _tags = new Dictionary<int, int>();

		private readonly Dictionary<int, int> _categoryIds = new Dictionary<int, int>();

		private readonly Dictionary<int, int> _sizes = new Dictionary<int, int>();

		public int Id { get; set; }

		public int CategoryId { get; set; }

		public Dictionary<int, int> ParentCategoryIds
		{
			get { return _categoryIds; }
		}

		public int? BrandId { get; set; }

		public decimal Price { get; set; }

		public int? Popularity { get; set; }

		public DateTime? IsNewFrom { get; set; }

		public int AgeFrom { get; set; }

		public int AgeTo { get; set; }

		public Dictionary<int, int> Tags
		{
			get { return _tags; }
		}

		public Dictionary<int, int> Sizes
		{
			get { return _sizes; }
		}

		public void AddTag(int tagId)
		{
			_tags[tagId] = tagId;
		}

		public bool HasTag(int tagId)
		{
			return _tags.ContainsKey(tagId);
		}

		public bool HasCategory(int categoryId)
		{
			return _categoryIds.ContainsKey(categoryId);
		}

		public void AddCategory(int categoryId)
		{
			_categoryIds[categoryId] = categoryId;
		}

		public bool PriceIsBetween(decimal from, decimal to)
		{
			return Price >= from && Price <= to;
		}

		public bool AgeIntervalMatches(int from, int to)
		{
			return AgeFrom <= to && AgeTo >= from;
		}

		public void AddSize(int sizeId)
		{
			_sizes[sizeId] = sizeId;
		}

		public bool HasSize(int sizeId)
		{
			return _sizes.ContainsKey(sizeId);
		}

	}
}