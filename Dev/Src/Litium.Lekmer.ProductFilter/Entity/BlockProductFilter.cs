﻿using System;
using Litium.Scensum.SiteStructure;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.ProductFilter
{
	[Serializable]
	public class BlockProductFilter : BlockBase, IBlockProductFilter
	{
		public int? SecondaryTemplateId { get; set; }
        public int? DefaultCategoryId { get; set; }
        public int? DefaultPriceIntervalId { get; set; }
        public int? DefaultAgeIntervalId { get; set; }
		public int? DefaultSizeId { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<int> DefaultBrandIds { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<int> DefaultTagIds { get; set; }
		public string ProductListCookie { get; set; }
		public string DefaultSort { get; set; }
	}
}
