using System.Globalization;
using Litium.Framework.Cache;

namespace Litium.Lekmer.ProductFilter.Cache
{
	public class FilterProductKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public FilterProductKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}