using System;
using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter.Cache
{
	internal static class CacheRenewWorkerManager
	{
		private static readonly Dictionary<int, CacheRenewWorker> _startedWorkers = new Dictionary<int, CacheRenewWorker>();
		private static readonly object _addLock = new object();

		internal static void StartWorker(IUserContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			if (!_startedWorkers.ContainsKey(context.Channel.Id))
			{
				lock (_addLock)
				{
					if (!_startedWorkers.ContainsKey(context.Channel.Id))
					{
						var worker = new CacheRenewWorker {UserContext = context};
						worker.Start();

						_startedWorkers.Add(context.Channel.Id, worker);
					}
				}
			}
		}
	}
}