using System.Collections.ObjectModel;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.ProductFilter.Cache
{
	public sealed class FilterProductCollectionCache : ScensumCacheBase<FilterProductKey, Collection<FilterProduct>>
	{
		#region Singleton

		private static readonly FilterProductCollectionCache _instance = new FilterProductCollectionCache();

		private FilterProductCollectionCache()
		{
		}

		public static FilterProductCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}
}