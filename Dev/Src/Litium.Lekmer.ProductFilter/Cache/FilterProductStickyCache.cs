using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter.Cache
{
	public static class FilterProductStickyCache
	{
		private static readonly Dictionary<int, Collection<FilterProduct>> _cachedFilterProducts =
			new Dictionary<int, Collection<FilterProduct>>();

		private static readonly object _addLock = new object();
		private static readonly object _fetchLock = new object();

		private static Collection<FilterProduct> GetCachedFilterProducts(int channelId)
		{
			Collection<FilterProduct> filterProducts;
			if (!_cachedFilterProducts.TryGetValue(channelId, out filterProducts))
			{
				return null;
			}

			return filterProducts;
		}

		private static void AddCachedFilterProducts(int channelId, Collection<FilterProduct> filterProducts)
		{
			if (filterProducts == null) throw new ArgumentNullException("filterProducts");

			lock (_addLock)
			{
				_cachedFilterProducts[channelId] = filterProducts;
			}
		}

		public static Collection<FilterProduct> GetFilterProducts(IUserContext context)
		{
			Collection<FilterProduct> filterProducts = GetCachedFilterProducts(context.Channel.Id);

			if (filterProducts != null)
			{
				return filterProducts;
			}

			CacheRenewWorkerManager.StartWorker(context);
			FetchAndAddToCache(context);
			return GetCachedFilterProducts(context.Channel.Id);
		}

		private static void FetchAndAddToCache(IUserContext context)
		{
			var key = new FilterProductKey(context.Channel.Id);
			var filterProducts = FilterProductCollectionCache.Instance.GetData(key);

			if (filterProducts == null)
			{
				lock (_fetchLock)
				{
					filterProducts = FilterProductCollectionCache.Instance.GetData(key);
					if (filterProducts == null)
					{
						filterProducts = FetchFromStorage(context);
						FilterProductCollectionCache.Instance.Add(key, filterProducts);
					}
				}
			}

			AddCachedFilterProducts(context.Channel.Id, filterProducts);
		}

		private static Collection<FilterProduct> FetchFromStorage(IUserContext context)
		{
			var filterProductService = IoC.Resolve<IFilterProductService>();

			return filterProductService.GetAllNoCache(context);
		}

		public static void RenewCache(IUserContext context)
		{
			FetchAndAddToCache(context);
		}
	}
}