using System;
using System.ComponentModel;
using System.Timers;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter.Cache
{
	internal class CacheRenewWorker
	{
		private const int _refreshIntervalInSeconds = 60;
		private BackgroundWorker _worker;
		private Timer _timer;

		internal IUserContext UserContext { get; set; }

		private void OnRenew(object sender, DoWorkEventArgs e)
		{
			FilterProductStickyCache.RenewCache(UserContext);
		}

		#region Background worker

		internal void Start()
		{
			if (UserContext == null)
			{
				throw new InvalidOperationException("UserContext property was not set.");
			}

			if (_worker == null)
			{
				_worker = CreateBackgroundWorker();
			}
			if (_timer == null)
			{
				_timer = CreateTimer();
			}

			_timer.Start();
		}

		private BackgroundWorker CreateBackgroundWorker()
		{
			var worker = new BackgroundWorker();
			worker.DoWork += OnRenew;

			return worker;
		}

		private Timer CreateTimer()
		{
			var timer = new Timer
			            	{
			            		Interval = (_refreshIntervalInSeconds*1000)
			            	};
			timer.Stop();
			timer.Elapsed += OnTimerElapsed;
			return timer;
		}

		private void OnTimerElapsed(object sender, ElapsedEventArgs e)
		{
			if (!_worker.IsBusy)
			{
				_worker.RunWorkerAsync();
			}
		}

		#endregion
	}
}