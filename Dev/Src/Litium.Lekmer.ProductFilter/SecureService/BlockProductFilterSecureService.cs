﻿using Litium.Lekmer.ProductFilter.Cache;
using Litium.Scensum.Core;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Scensum.Foundation;
using Litium.Framework.Transaction;
using System;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.ProductFilter
{
	public class BlockProductFilterSecureService : IBlockProductFilterSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected BlockProductFilterRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }

		public BlockProductFilterSecureService(
			BlockProductFilterRepository repository,
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService)
		{
			Repository = repository;
			AccessValidator = accessValidator;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
		}

		public virtual IBlockProductFilter Create()
		{
			var blockProductFilter = IoC.Resolve<IBlockProductFilter>();
			blockProductFilter.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductFilter.DefaultBrandIds = new Collection<int>();
			blockProductFilter.DefaultTagIds = new Collection<int>();
			blockProductFilter.Status = BusinessObjectStatus.New;
			return blockProductFilter;
		}

		public virtual IBlockProductFilter GetByIdSecure(int blockId)
		{
			var blockProductFilter = Repository.GetByIdSecure(blockId);
			blockProductFilter.DefaultBrandIds = Repository.GetAllBrandsByBlock(blockId);
			blockProductFilter.DefaultTagIds = Repository.GetAllTagsByBlock(blockId);
			return blockProductFilter;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductFilter blockProductFilter)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");
			if (blockProductFilter == null) throw new ArgumentNullException("blockProductFilter");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				blockProductFilter.Id = BlockSecureService.Save(systemUserFull, blockProductFilter);
				if (blockProductFilter.Id == -1)
				{
					return blockProductFilter.Id;
				}

				Repository.Save(blockProductFilter);

				Repository.DeleteAllBrands(blockProductFilter.Id);
				foreach (int brandId in blockProductFilter.DefaultBrandIds)
				{
					Repository.SaveBrand(blockProductFilter.Id, brandId);
				}

				Repository.DeleteAllTags(blockProductFilter.Id);
				foreach (int tagId in blockProductFilter.DefaultTagIds)
				{
					Repository.SaveTag(blockProductFilter.Id, tagId);
				}

				transaction.Complete();
			}

			BlockProductFilterCache.Instance.Flush();

			return blockProductFilter.Id;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockProductFilter = Create();
			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = contentAreaId;
			blockProductFilter.BlockTypeId = blockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockProductFilter.Title = title;
			blockProductFilter.TemplateId = null;
			blockProductFilter.Id = Save(systemUserFull, blockProductFilter);
			return blockProductFilter;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAllBrands(blockId);
				Repository.DeleteAllTags(blockId);
				Repository.Delete(blockId);
				transaction.Complete();
			}

			BlockProductFilterCache.Instance.Flush();
		}
	}
}
