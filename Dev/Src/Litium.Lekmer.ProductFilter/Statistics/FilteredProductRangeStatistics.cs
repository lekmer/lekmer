using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.ProductFilter
{
	public class FilteredProductRangeStatistics
	{
		private readonly Dictionary<int, int> _categoryCount = new Dictionary<int, int>();
		private readonly Dictionary<int, int> _brandCount = new Dictionary<int, int>();
		private readonly Collection<AgeIntervalCount> _ageIntervalCount = new Collection<AgeIntervalCount>();
		private readonly Collection<PriceIntervalCount> _priceIntervalCount = new Collection<PriceIntervalCount>();
		private readonly Dictionary<int, int> _tagCount = new Dictionary<int, int>();
		private readonly Dictionary<int, int> _sizeCount = new Dictionary<int, int>();

		public Dictionary<int, int> CategoryCount
		{
			get { return _categoryCount; }
		}

		public Dictionary<int, int> BrandCount
		{
			get { return _brandCount; }
		}

		public Collection<AgeIntervalCount> AgeIntervalCount
		{
			get { return _ageIntervalCount; }
		}

		public Collection<PriceIntervalCount> PriceIntervalCount
		{
			get { return _priceIntervalCount; }
		}

		public Dictionary<int, int> TagCount
		{
			get { return _tagCount; }
		}

		public Dictionary<int, int> SizeCount
		{
			get { return _sizeCount; }
		}

		public void LoadAgeIntervals(IEnumerable<IAgeInterval> ageIntervals)
		{
			var ageIntervalCounts = ageIntervals
				.Select(ai => new AgeIntervalCount {Id = ai.Id, From = ai.FromMonth, To = ai.ToMonth});
			foreach (AgeIntervalCount ageIntervalCount in ageIntervalCounts)
			{
				AgeIntervalCount.Add(ageIntervalCount);
			}
		}

		public void LoadPriceIntervals(IEnumerable<IPriceInterval> priceIntervals)
		{
			var priceIntervalCounts = priceIntervals
				.Select(ai => new PriceIntervalCount {Id = ai.Id, From = ai.From, To = ai.To});
			foreach (PriceIntervalCount priceIntervalCount in priceIntervalCounts)
			{
				PriceIntervalCount.Add(priceIntervalCount);
			}
		}

		public void LoadProducts(IEnumerable<FilterProduct> products)
		{
			foreach (FilterProduct product in products)
			{
				LoadProduct(product);
			}
		}

		private void LoadProduct(FilterProduct product)
		{
			if (product.BrandId.HasValue)
			{
				IncrementItem(_brandCount, product.BrandId.Value);
			}

			foreach (KeyValuePair<int, int> categoryId in product.ParentCategoryIds)
			{
				IncrementItem(_categoryCount, categoryId.Key);
			}

			foreach (KeyValuePair<int, int> tag in product.Tags)
			{
				IncrementItem(_tagCount, tag.Key);
			}

			foreach (KeyValuePair<int, int> size in product.Sizes)
			{
				IncrementItem(_sizeCount, size.Key);
			}

			var matchingAges = AgeIntervalCount
				.Where(ai => product.AgeIntervalMatches(ai.From, ai.To));
			foreach (AgeIntervalCount age in matchingAges)
			{
				age.Count++;
			}

			var matchingPriceIntervals = PriceIntervalCount
				.Where(pi => product.PriceIsBetween(pi.From, pi.To));
			foreach (PriceIntervalCount priceInterval in matchingPriceIntervals)
			{
				priceInterval.Count++;
			}
		}

		private static void IncrementItem<TKey>(IDictionary<TKey, int> itemCount, TKey key)
		{
			int count;
			if (itemCount.TryGetValue(key, out count))
			{
				itemCount[key] = count + 1;
			}
			else
			{
				itemCount.Add(key, 1);
			}
		}
	}
}