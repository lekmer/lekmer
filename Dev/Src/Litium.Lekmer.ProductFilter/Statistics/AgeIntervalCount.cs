namespace Litium.Lekmer.ProductFilter
{
	public class AgeIntervalCount
	{
		public int Id { get; set; }

		public int From { get; set; }

		public int To { get; set; }

		public int Count { get; set; }
	}
}