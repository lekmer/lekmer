namespace Litium.Lekmer.ProductFilter
{
	public class PriceIntervalCount
	{
		public int Id { get; set; }

		public decimal From { get; set; }

		public decimal To { get; set; }

		public int Count { get; set; }
	}
}