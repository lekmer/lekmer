using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterProcessor : IFilterProcessor
	{
		private readonly IPriceIntervalService _priceIntervalService;
		private readonly IAgeIntervalService _ageIntervalService;

		public FilterProcessor(IPriceIntervalService priceIntervalService, IAgeIntervalService ageIntervalService)
		{
			_priceIntervalService = priceIntervalService;
			_ageIntervalService = ageIntervalService;
		}

		public FilterResult Process(IUserContext context, IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (products == null) throw new ArgumentNullException("products");
			if (query == null) throw new ArgumentNullException("query");

			var priceIntervals = _priceIntervalService.GetAll(context);
			var ageIntervals = _ageIntervalService.GetAll(context);

			products = FilterProducts(products, query);

			products = SortProducts(products, query);

			var statistics = new FilteredProductRangeStatistics();
			statistics.LoadAgeIntervals(ageIntervals);
			statistics.LoadPriceIntervals(priceIntervals);
			statistics.LoadProducts(products);

			return new FilterResult {Products = products, Statistics = statistics};
		}

		private static IEnumerable<FilterProduct> SortProducts(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			switch (query.SortOption)
			{
				case SortOption.PriceAsc:
					return products.OrderBy(product => product.Price);
				case SortOption.PriceDesc:
					return products.OrderByDescending(product => product.Price);
				case SortOption.TitleDesc:
					// Products are in title order, just need to reverse it.
					return products.Reverse();
				case SortOption.Popularity:
					return products.OrderByDescending(product => product.Popularity);
				case SortOption.IsNewFrom:
					return products.OrderByDescending(product => product.IsNewFrom);
				default:
					// Title order.
					return products;
			}
		}

		private static IEnumerable<FilterProduct> FilterProducts(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			products = FilterByProductIds(products, query);

			products = FilterByBrandIds(products, query);

			products = FilterByCategoryIds(products, query);

			products = FilterByTags(products, query);

			products = FilterByAgeInterval(products, query);

			products = FilterByPriceInterval(products, query);

			products = FilterBySizeIds(products, query);

			return products;
		}

		private static IEnumerable<FilterProduct> FilterByAgeInterval(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.AgeIntervals.Count == 0) return products;

			return products
				.Where(p =>
				       	{
				       		foreach (IAgeInterval ageInterval in query.AgeIntervals)
				       		{
				       			if (p.AgeIntervalMatches(ageInterval.FromMonth, ageInterval.ToMonth)) return true;
				       		}
				       		return false;
				       	});
		}

		private static IEnumerable<FilterProduct> FilterByPriceInterval(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.PriceIntervals.Count == 0) return products;

			return products
				.Where(p =>
				       	{
				       		foreach (IPriceInterval priceInterval in query.PriceIntervals)
				       		{
				       			if (p.PriceIsBetween(priceInterval.From, priceInterval.To)) return true;
				       		}
				       		return false;
				       	});
		}

		private static IEnumerable<FilterProduct> FilterByTags(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.GroupedTagIds.Count == 0) return products;

			foreach (IEnumerable<int> tagIds in query.GroupedTagIds)
			{
				if (tagIds == null) continue;

				products = FilterByTagIds(products, tagIds);
			}

			return products;
		}

		private static IEnumerable<FilterProduct> FilterByTagIds(IEnumerable<FilterProduct> products, IEnumerable<int> tagIds)
		{
			return products
				.Where(p =>
				       	{
				       		foreach (int tagId in tagIds)
				       		{
				       			if (p.HasTag(tagId)) return true;
				       		}
				       		return false;
				       	});
		}

		private static IEnumerable<FilterProduct> FilterByCategoryIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.Level1CategoryIds.Count == 0) return products;

			products = products
				.Where(p =>
				       	{
				       		foreach (int categoryId in query.Level1CategoryIds)
				       		{
				       			if (p.HasCategory(categoryId)) return true;
				       		}
				       		return false;
				       	});

			return FilterByLevel2And3CategoryIds(products, query);
		}

		private static IEnumerable<FilterProduct> FilterByLevel2And3CategoryIds(
			IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.Level1CategoryIds.Count != 1 || query.Level2CategoryIds.Count <= 0) return products;

			products = products
				.Where(p =>
				       	{
				       		foreach (int categoryId in query.Level2CategoryIds)
				       		{
				       			if (p.HasCategory(categoryId)) return true;
				       		}
				       		return false;
				       	});

			return FilterByLevel3CategoryIds(query, products);
		}

		private static IEnumerable<FilterProduct> FilterByLevel3CategoryIds(
			FilterQuery query, IEnumerable<FilterProduct> products)
		{
			if (query.Level2CategoryIds.Count != 1 || query.Level3CategoryIds.Count <= 0) return products;

			return products
				.Where(p =>
				       	{
				       		foreach (int categoryId in query.Level3CategoryIds)
				       		{
				       			if (p.HasCategory(categoryId)) return true;
				       		}
				       		return false;
				       	});
		}

		private static IEnumerable<FilterProduct> FilterByBrandIds(
			IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.BrandIds.Count == 0) return products;

			var brandIdDictionary = query.BrandIds
				.ToDictionary(id => id, id => id);

			return products
				.Where(p => p.BrandId.HasValue && brandIdDictionary.ContainsKey(p.BrandId.Value));
		}

		private static IEnumerable<FilterProduct> FilterByProductIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.ProductIds.Count == 0) return products;

			var productIdDictionary = query.ProductIds
				.ToDictionary(id => id, id => id);

			return products
				.Where(p => productIdDictionary.ContainsKey(p.Id));
		}

		private static IEnumerable<FilterProduct> FilterBySizeIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.SizeIds.Count == 0) return products;

			return products
				.Where(p =>
				{
					foreach (int sizeId in query.SizeIds)
					{
						if (p.HasSize(sizeId)) return true;
					}
					return false;
				});
		}
	}
}