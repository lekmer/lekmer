using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Voucher
{
    public class VoucherService : IVoucherService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(VoucherService));
        public IVoucherCheckResult VoucherCheck(string channelApplicationName, int customerId, string code)
        {
            var voucherWebService = new VoucherWebReference.Service1();
            customerId = 0;//temporary mocked as requested by Dino
            var voucherConsumeResult = voucherWebService.VoucherCheck(customerId, code, channelApplicationName);
            var voucherCheckResult = IoC.Resolve<IVoucherCheckResult>();
            voucherCheckResult.VoucherCode = code;
            voucherCheckResult.VoucherbatchId = voucherConsumeResult.VoucherbatchId;
            voucherCheckResult.IsValid = voucherConsumeResult.ValidCode;
            voucherCheckResult.DiscountValue = voucherConsumeResult.DiscountValue;
            if (voucherConsumeResult.ValueType != VoucherWebReference.VoucherValueType.Percentage)
            {
                voucherCheckResult.ValueType = VoucherValueType.Price;
            }
            else
                voucherCheckResult.ValueType = VoucherValueType.Percentage;


            /* Let's mock! */

            //var voucherCheckResult = IoC.Resolve<IVoucherCheckResult>();

            //voucherCheckResult.VoucherCode = code;

            //int voucherbatchId;
            //if (int.TryParse(voucherCheckResult.VoucherCode, out voucherbatchId))
            //{
            //    voucherCheckResult.VoucherbatchId = voucherbatchId;
            //    voucherCheckResult.IsValid = true;
            //}
            //else
            //{
            //    voucherCheckResult.VoucherbatchId = null;
            //    voucherCheckResult.IsValid = false;
            //}
            return voucherCheckResult;
        }

        public void VoucherConsume(string channelApplicationName, int customerId, string code, int orderId)
        {
            try
            {
                var voucherWebService = new VoucherWebReference.Service1();
                voucherWebService.VoucherConsume(customerId, code, orderId, channelApplicationName);
            }
            catch
            {
                log.ErrorFormat("Error occured while consuming voucher. Voucher code: {0}", code);
            }
        }

        public void VoucherRelease(string code)
        {
            try
            {
                var voucherWebService = new VoucherWebReference.Service1();
                voucherWebService.VoucherRelease(code);
            }
            catch
            {
                log.ErrorFormat("Error occured while releasing voucher. Voucher code: {0}", code);
            }
        }
        public void VoucherReserve(string code)
        {
            try
            {
                var voucherWebService = new VoucherWebReference.Service1();
                voucherWebService.VoucherReserve(code);
            }
            catch
            {
                log.ErrorFormat("Error occured while reserving voucher. Voucher code: {0}", code);
            }
        }
    }
}