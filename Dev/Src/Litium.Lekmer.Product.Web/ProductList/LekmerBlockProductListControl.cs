using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockProductListControl : BlockProductListControl
	{
		public LekmerBlockProductListControl(
				ITemplateFactory templateFactory,
				IBlockProductListService blockProductListService,
				IProductService productService)
			: base(templateFactory, blockProductListService, productService) { }


		private void Initialize()
		{
			PagingControl = CreatePagingControl();
			Products = GetProducts();
		}

		protected override IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = ResolveUrl(ContentNodeTreeItem.Url); //GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.ColumnCount * Block.RowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			var fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddEntity(Block);
			fragmentHead.AddVariable("Param.ProductIds", RenderProductIdsArrayParam());
			string head =
				(fragmentHead.Render() ?? string.Empty) +
				(pagingContent.Head ?? string.Empty);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			return new BlockContent(
				head,
				fragmentContent.Render());
		}
		private string RenderProductIdsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			for (int i = 0; i < Products.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", Products[i].Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", Products[i].Id));
				}
			}

			return itemBuilder.ToString();

		}
	}
}