﻿using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
    internal class BrandHelper
    {
        private const string _imageUrlMatchPattern = @"\[Brand.ImageUrl\(""(.*?)""\)\]";
        private const int _imageUrlMatchCommonNamePosition = 1;

        internal static void AddImageVariables(Fragment fragment, IImage image, string brandTitle)
        {
            string originalSizeImageUrl = null;
            string alternativeText = null;
            if (image != null)
            {
                alternativeText = image.AlternativeText;
                originalSizeImageUrl = string.Format(
                    CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
                    WebSetting.Instance.MediaUrl, image.Id, UrlCleaner.CleanUp(brandTitle), image.FormatExtension);
                originalSizeImageUrl = UrlHelper.ResolveUrl(originalSizeImageUrl);
            }

            fragment.AddCondition("Brand.HasImage", image != null);
            fragment.AddVariable("Brand.ImageUrl", originalSizeImageUrl);
            fragment.AddCondition("Brand.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
            fragment.AddVariable("Brand.ImageAlternativeText", alternativeText);

            fragment.AddRegexVariable(
                _imageUrlMatchPattern,
                delegate(Match match)
                    {
                        if (image == null) return null;

                        string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

                        string imageUrl = string.Format(
                            CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
                            WebSetting.Instance.MediaUrl, image.Id, commonName, UrlCleaner.CleanUp(brandTitle),
                            image.FormatExtension);

                        return UrlHelper.ResolveUrl(imageUrl);
                    },
                VariableEncoding.HtmlEncodeLight,
                RegexOptions.Compiled);
        }
    }
}