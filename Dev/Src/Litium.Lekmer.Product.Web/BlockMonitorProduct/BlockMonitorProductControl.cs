﻿using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockMonitorProduct
{
	public class BlockMonitorProductControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		private MonitorProductForm _monitorProductForm;
		private bool _isRegistered;
		private bool _isRegistrationFailed;

		public BlockMonitorProductControl(ITemplateFactory templateFactory, IBlockService blockService) : base(templateFactory)
		{
			_blockService = blockService;
		}

		public MonitorProductForm MonitorProductForm
		{
			get
			{
				var lekmerProduct = (ILekmerProduct) Product;
                if (lekmerProduct == null || string.IsNullOrEmpty(lekmerProduct.LekmerUrl))
                    return null;
				return _monitorProductForm ??
					   (_monitorProductForm = new MonitorProductForm(UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl)));
			}
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
            if (MonitorProductForm==null)
                return new BlockContent();
			if (MonitorProductForm.IsFormPostBack)
			{
				MonitorProductForm.MapFromRequest();
				validationResult = MonitorProductForm.Validate();
				if (validationResult.IsValid)
				{
					RegisterMonitorProduct();
				}
			}
			else
			{
				MonitorProductForm.ClearFrom();
			}
			return RenderMonitorProductForm(validationResult);
		}

		private BlockContent RenderMonitorProductForm(ValidationResult validationResult)
		{
			Fragment fragmentHead = Template.GetFragment("Head");

			Fragment fragmentContent = Template.GetFragment("Content");
			MonitorProductForm.MapFieldsToFragment(fragmentContent);
			RenderValidationResult(fragmentContent, validationResult);
			RenderRegisteredNotification(fragmentContent);
			fragmentContent.AddEntity(Block);

			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}

		private static void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		private void RenderRegisteredNotification(Fragment fragmentContent)
		{
			var message = string.Empty;
			if (_isRegistered)
			{
				message = AliasHelper.GetAliasValue("Product.MonitorProduct.RegisteredMessage");
			}
			else if (_isRegistrationFailed)
			{
				message = AliasHelper.GetAliasValue("Product.MonitorProduct.RegistrationFailedMessage");
			}
			fragmentContent.AddVariable("RegisteredMessage", message, VariableEncoding.None);
			fragmentContent.AddCondition("WasMonitorProductRegistered", _isRegistered);
		}

		private void RegisterMonitorProduct()
		{
			var monitorProductService = IoC.Resolve<IMonitorProductService>();
			var monitorProduct = monitorProductService.Create(UserContext.Current, Product.Id, MonitorProductForm.Email, MonitorProductForm.SizeId);
			var id = monitorProductService.Save(UserContext.Current, monitorProduct);
			if (id > 0)
			{
				_isRegistered = true;
			}
			else
			{
				_isRegistrationFailed = true;
			}
		}
	}
}