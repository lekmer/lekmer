﻿using System.Text.RegularExpressions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockMonitorProduct
{
	public class MonitorProductForm : ControlBase
	{
		private readonly string _postUrl;

		public MonitorProductForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string EmailFormName
		{
			get { return "monitorproduct-email"; }
		}

		public string SizeFormName
		{
			get { return "monitorproduct-size"; }
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "monitorproduct"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string Email { get; set; }

		public int? SizeId { get; set; }

		public void MapFromRequest()
		{
			Email = Request.Form[EmailFormName];
			int sizeId;
			SizeId = int.TryParse(Request.Form[SizeFormName], out sizeId) ? (int?)sizeId : null;
		}

		public void ClearFrom()
		{
			Email = string.Empty;
			SizeId = null;
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Email.Name", EmailFormName);
			fragment.AddVariable("Form.Size.Name", SizeFormName);

			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Email.Value", Email);
			fragment.AddVariable("Form.Size.Value", SizeId.ToString());
			fragment.AddCondition("Form.IsPostBack", IsFormPostBack);
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();       
			ValidateEmail(validationResult);
			return validationResult;
		}

		private void ValidateEmail(ValidationResult validationResult)
		{
			if (Email.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Product.MonitorProduct.Validation.EmailEmpty"));
			}
			else if (!MatchEmailPattern(Email))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Product.MonitorProduct.Validation.EmailIncorrect"));
			}
		}

		private static bool MatchEmailPattern(string email)
		{
			Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
			return regex.IsMatch(email);
		}
	}
}