﻿using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockImageRotator
{
    public class ImageHelper
    {
        private const string _mainImageUrlMatchPattern = @"\[MainImage.Url\(""(.*?)""\)\]";
        private const string _thumbnailImageUrlMatchPattern = @"\[ThumbnailImage.Url\(""(.*?)""\)\]";
        private const int _imageUrlMatchCommonNamePosition = 1;

        public static void AddMainImageVariables(Fragment fragment, IImage item)
        {
            fragment.AddCondition("MainImage.HasImage", item != null);
            if (item == null)
                return;

            fragment.AddVariable("MainImage.Id", item.Id.ToString(CultureInfo.InvariantCulture));
            fragment.AddVariable("MainImage.Title", item.Title);
            fragment.AddVariable("MainImage.AlternativeText", item.AlternativeText);

            fragment.AddVariable("MainImage.Url", UrlHelper.ResolveUrl(string.Format(
                                                                    CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
                                                                    WebSetting.Instance.MediaUrl, item.Id,
                                                                    UrlCleaner.CleanUp(item.Title), item.FormatExtension)));

            fragment.AddRegexVariable(
                _mainImageUrlMatchPattern,
                delegate(Match match)
                {
                    string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

                    string imageUrl = string.Format(
                        CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
                        WebSetting.Instance.MediaUrl, item.Id, commonName, UrlCleaner.CleanUp(item.Title), item.FormatExtension);

                    return UrlHelper.ResolveUrl(imageUrl);
                },
                VariableEncoding.HtmlEncodeLight,
                RegexOptions.Compiled);
        }

        public static void AddThumbnailImageVariables(Fragment fragment, IImage item)
        {
            fragment.AddCondition("ThumbnailImage.HasImage", item != null);
            if (item == null)
                return;

            fragment.AddVariable("ThumbnailImage.Id", item.Id.ToString(CultureInfo.InvariantCulture));
            fragment.AddVariable("ThumbnailImage.Title", item.Title);
            fragment.AddVariable("ThumbnailImage.AlternativeText", item.AlternativeText);
            fragment.AddVariable("ThumbnailImage.Url", UrlHelper.ResolveUrl(string.Format(
                                                                    CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
                                                                    WebSetting.Instance.MediaUrl, item.Id,
                                                                    UrlCleaner.CleanUp(item.Title), item.FormatExtension)));

            fragment.AddRegexVariable(
                _thumbnailImageUrlMatchPattern,
                delegate(Match match)
                {
                    string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

                    string imageUrl = string.Format(
                        CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
                        WebSetting.Instance.MediaUrl, item.Id, commonName, UrlCleaner.CleanUp(item.Title), item.FormatExtension);

                    return UrlHelper.ResolveUrl(imageUrl);
                },
                VariableEncoding.HtmlEncodeLight,
                RegexOptions.Compiled);
        }



    }
}
