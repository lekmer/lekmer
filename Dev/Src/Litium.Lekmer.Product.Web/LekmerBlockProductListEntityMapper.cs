using System.Globalization;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockProductListEntityMapper : BlockProductListEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}