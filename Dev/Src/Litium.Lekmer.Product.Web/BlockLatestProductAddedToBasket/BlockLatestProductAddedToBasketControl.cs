﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Lekmer.Product.Web
{
    public class BlockLatestProductAddedToBasketControl : BlockControlBase<IBlockLatestProductAddedToBasket>
    {
        private readonly IBlockLatestProductAddedToBasketService _blockLatestProductAddedToBasket;
        private readonly IContentNodeService _contentNodeService;
        private IProductView _product;

        public BlockLatestProductAddedToBasketControl(ITemplateFactory templateFactory, IContentNodeService contentNodeService, IBlockLatestProductAddedToBasketService blockLatestProductAddedToBasket)
            : base(templateFactory)
        {
            _contentNodeService = contentNodeService;
            _blockLatestProductAddedToBasket = blockLatestProductAddedToBasket;
        }

        protected override IBlockLatestProductAddedToBasket GetBlockById(int blockId)
        {
            IBlockLatestProductAddedToBasket blockLatestProductAddedToBasket = _blockLatestProductAddedToBasket.GetById(UserContext.Current, blockId);
            return blockLatestProductAddedToBasket;
        }

        protected override BlockContent RenderCore()
        {
            int productId;
            if (!int.TryParse(Request.QueryString["id"], out productId))
            {
                return new BlockContent(null, "[ Can't render product on this page ]");
            }

            Initialize(productId);

            Fragment fragmentContent = Template.GetFragment("Content");
            string continueShoppingUrl = string.Empty;

            if (Block.RedirectToCategory)
            {
                continueShoppingUrl = GetCategoryUrl();
                if (string.IsNullOrEmpty(continueShoppingUrl))
                {
                    Block.RedirectToCategory = false;
                    Block.RedirectToMainCategory = false;
                    Block.RedirectToStartPage = true;
                }
            }

            else if (Block.RedirectToMainCategory)
            {
                continueShoppingUrl = GetMainCategoryUrl();
                if (string.IsNullOrEmpty(continueShoppingUrl))
                {
                    Block.RedirectToCategory = false;
                    Block.RedirectToMainCategory = false;
                    Block.RedirectToStartPage = true;
                }
            }
            fragmentContent.AddCondition("RedirectToCategory", Block.RedirectToCategory);
            fragmentContent.AddCondition("RedirectToMainCategory", Block.RedirectToMainCategory);
            fragmentContent.AddCondition("RedirectToStartPage", Block.RedirectToStartPage);
            fragmentContent.AddEntity(Block);
            fragmentContent.AddVariable("Product", RenderProduct(), VariableEncoding.None);
            fragmentContent.AddVariable("ContinueShoppingUrl", string.IsNullOrEmpty(continueShoppingUrl) ? string.Empty : UrlHelper.ResolveUrl(continueShoppingUrl));

            return new BlockContent(RenderHead(),fragmentContent.Render());
        }

        private string RenderProduct()
        {
            string genericFilterPageUrl = GetGenericFilterPageUrl();
            Fragment fragmentProduct = Template.GetFragment("Product");
            fragmentProduct.AddEntity(_product);
            fragmentProduct.AddVariable("Product.Description", _product.Description, VariableEncoding.None);
            fragmentProduct.AddRegexVariable(@"\[TagGroup\(\""(.+)\""\)\]",
                   delegate(Match match)
                   {
                       string tagGroupCommonName = match.Groups[1].Value;

                       return RenderTagGroup((ILekmerProductView)_product, tagGroupCommonName, genericFilterPageUrl);
                   },
                       VariableEncoding.None);
            return fragmentProduct.Render();
        }

        private string GetGenericFilterPageUrl()
        {
            var item = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, "filter");
            if (item == null || item.Url.IsNullOrEmpty())
            {
                return null;
            }
            return UrlHelper.ResolveUrlHttp(item.Url);
        }

        private string GetCategoryUrl()
        {
            int? contentNodeId = Request.QueryString.GetInt32OrNull("nodeId");
            if (!contentNodeId.HasValue && _product.ParentContentNodeId.HasValue)
            {
                contentNodeId = _product.ParentContentNodeId.Value;
            }
            else if (!contentNodeId.HasValue)
            {
                contentNodeId = ResolveParentContentNodeIdForCategory(_product.CategoryId);
            }

            if (contentNodeId != null)
            {
                bool isSignedIn = IoC.Resolve<ICustomerSession>().IsSignedIn;
                IContentNodeTreeItem treeItem =
                IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current, contentNodeId.Value);

                if (treeItem.ContentNode != null && treeItem.ContentNode.ShowInNavigation(isSignedIn))
                {
                    return treeItem.Url;
                }
            }
            return null;
        }

        private string GetMainCategoryUrl()
        {
            int? contentNodeId = _product.ParentContentNodeId;
            
            if (!contentNodeId.HasValue)
            {
                contentNodeId = ResolveParentContentNodeIdForCategory(_product.CategoryId);
            }

            if (contentNodeId != null)
            {
                bool isSignedIn = IoC.Resolve<ICustomerSession>().IsSignedIn;
                IContentNodeTreeItem treeItem =
                IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current, contentNodeId.Value);
                var reverseItems = new Collection<string>();
                // This item.
                if (treeItem.ContentNode != null && treeItem.ContentNode.ShowInNavigation(isSignedIn))
                {
                    reverseItems.Add(treeItem.Url);
                }

                // Ancestors.
                foreach (IContentNodeTreeItem parentItem in treeItem.GetAncestors())
                {
                    if (parentItem.ContentNode != null && parentItem.ContentNode.ShowInNavigation(isSignedIn))
                    {
                        reverseItems.Add(parentItem.Url);
                    }
                }

                if (reverseItems.Count > 0)
                {
                    return reverseItems[reverseItems.Count - 1];
                }
            }
            return null;
        }

        private static int? ResolveParentContentNodeIdForCategory(int categoryId)
        {
            ICategoryTree categoryTree = IoC.Resolve<ICategoryService>().GetAllAsTree(UserContext.Current);

            ICategoryTreeItem treeItem = categoryTree.FindItemById(categoryId);
            if (treeItem == null) return null;

            if (treeItem.Category != null && treeItem.Category.ProductParentContentNodeId.HasValue)
            {
                return treeItem.Category.ProductParentContentNodeId.Value;
            }

            foreach (ICategoryTreeItem parentItem in treeItem.GetAncestors())
            {
                if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
                {
                    return parentItem.Category.ProductParentContentNodeId.Value;
                }
            }
            return null;
        }
        private string RenderHead()
        {
            Fragment fragmentHead = Template.GetFragment("Head");
            fragmentHead.AddEntity(_product);
            return fragmentHead.Render();
        }

        private string RenderTagGroup(ILekmerProductView lekmerProduct, string commonName, string genericFilterPageUrl)
        {
            if (commonName == null) throw new ArgumentNullException("commonName");

            var tagGroup = lekmerProduct.TagGroups
                .FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

            if (tagGroup == null)
            {
                return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
            }

            if (tagGroup.Tags.Count == 0)
            {
                return null;
            }

            var fragmentTagGroup = Template.GetFragment("ProductTagGroup");
            fragmentTagGroup.AddCondition("CountIsZero", tagGroup.Tags.Count == 0);
            fragmentTagGroup.AddVariable("TagGroupName", AliasHelper.GetAliasValue("Product.Detail." + commonName), VariableEncoding.None);
            fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup, genericFilterPageUrl), VariableEncoding.None);
            return fragmentTagGroup.Render();
        }

        private string RenderTags(ITagGroup tagGroup, string genericFilterPageUrl)
        {
            var tagBuilder = new StringBuilder();
            int i = 0;
            foreach (ITag tag in tagGroup.Tags)
            {
                string tagUrl = GetTagUrl(tag, genericFilterPageUrl);

                var fragmentTag = Template.GetFragment("ProductTag");
                fragmentTag.AddVariable("Tag.Value", tag.Value);
                fragmentTag.AddVariable("Tag.Url", tagUrl);
                fragmentTag.AddCondition("Tag.HasUrl", tagUrl != null);

                AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
                tagBuilder.AppendLine(fragmentTag.Render());
            }
            return tagBuilder.ToString();
        }

        private static string GetTagUrl(ITag tag, string genericFilterPageUrl)
        {
            if (genericFilterPageUrl.IsNullOrEmpty())
            {
                return null;
            }

            var query = new QueryBuilder();
            query.Add("mode", "filter");
            query.Add("taggroup" + tag.TagGroupId + "-tag-id", tag.Id.ToString(CultureInfo.InvariantCulture));
            return genericFilterPageUrl + query;
        }

        private static void AddPositionCondition(Fragment fragment, int index, int count)
        {
            fragment.AddCondition("IsFirst", index == 0);
            fragment.AddCondition("IsLast", index == count - 1);
        }

        private void Initialize(int productId)
        {
            _product = IoC.Resolve<IProductService>().GetViewById(UserContext.Current, productId);
        }
    }
}