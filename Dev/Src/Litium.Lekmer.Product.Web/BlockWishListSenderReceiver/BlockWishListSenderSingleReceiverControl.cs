﻿using System;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Lekmer.Product.Web.BlockWishListSenderReceiver;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
    public class BlockWishListSenderSingleReceiverControl : BlockControlBase<IBlock>
    {
        private readonly IBlockService _blockService;
        private BlockWishListSenderSingleReceiverForm _wishListSenderSingleReceiverForm;
        private bool _wasSent;
        private string _wishListKey;

        public BlockWishListSenderSingleReceiverControl(ITemplateFactory templateFactory, IBlockService blockService)
            : base(templateFactory)
        {
            _blockService = blockService;

             _wishListKey = Request.QueryString["wishlist"];
            if (string.IsNullOrEmpty(_wishListKey))
            {
                _wishListKey = WishListCookie.GetWishListKey().ToString();
            }
         

        }

        public BlockWishListSenderSingleReceiverForm WishListSenderSingleReceiverForm
        {
            get
            {
                return _wishListSenderSingleReceiverForm ??
                       (_wishListSenderSingleReceiverForm = new BlockWishListSenderSingleReceiverForm(ResolveUrl(ContentNodeTreeItem.Url)));
            }
        }
        /// <summary>
        /// Common name of the model for the template.
        /// </summary>
        protected override string ModelCommonName
        {
            get { return "BlockWishListSenderSingleReceiver"; }
        }

        protected override IBlock GetBlockById(int blockId)
        {
            return _blockService.GetById(UserContext.Current, blockId);
        }

        protected override BlockContent RenderCore()
        {
            ValidationResult validationResult = null;
            WishListSenderSingleReceiverForm.WishListKey =  new Guid(_wishListKey);

            if (WishListSenderSingleReceiverForm.IsFormPostBack)
            {
                WishListSenderSingleReceiverForm.MapFromRequest();
                validationResult = WishListSenderSingleReceiverForm.Validate();
                if (validationResult.IsValid)
                {
                    SendWishList(GetWishListMessageArgs());
                    _wasSent = true;
                }

                WishListSenderSingleReceiverForm.ClearFrom();
            }
            else
            {
                WishListSenderSingleReceiverForm.ClearFrom();
            }
            return RenderWishListSenderForm(validationResult);
        }

        private WishListMessageArgs GetWishListMessageArgs()
        {
            WishListMessageArgs messageArgs = new WishListMessageArgs();
            messageArgs.Channel = UserContext.Current.Channel;
            messageArgs.Senders = WishListSenderSingleReceiverForm.Sender;
            messageArgs.Message = WishListSenderSingleReceiverForm.Message;
            messageArgs.WishListPageUrl = WishListSenderSingleReceiverForm.WishListPageUrl;
            messageArgs.Receivers.Add(WishListSenderSingleReceiverForm.Receiver);
            messageArgs.SenderEmail = WishListSenderSingleReceiverForm.SenderEmail;
            messageArgs.SenderName = WishListSenderSingleReceiverForm.SenderName;
            messageArgs.MailToMysef = WishListSenderSingleReceiverForm.MailToMyself;
            messageArgs.WishListKey = WishListCookie.GetWishListKey().ToString();
            return messageArgs;
        }

        private BlockContent RenderWishListSenderForm(ValidationResult validationResult)
        {
            Fragment fragmentHead = Template.GetFragment("Head");
            Fragment fragmentContent = Template.GetFragment("Content");
            WishListSenderSingleReceiverForm.MapFieldsToFragment(fragmentContent);
            WishListSenderSingleReceiverForm.MapFieldsValueToFragment(fragmentContent);
            RenderValidationResult(fragmentContent, validationResult);
            fragmentContent.AddEntity(Block);
            fragmentContent.AddCondition("WasSent", _wasSent);
            WishListSenderSingleReceiverForm.MapFieldsToFragment(fragmentHead);

            return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
        }

        private static void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult)
        {
            string validationError = null;
            bool isValid = validationResult == null || validationResult.IsValid;
            if (!isValid)
            {
                var validationControl = new ValidationControl(validationResult.Errors);
                validationError = validationControl.Render();
            }
            fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
        }

        private static void SendWishList(WishListMessageArgs messageArgs)
        {
            var wishListService = IoC.Resolve<IWishListService>();
            wishListService.SendWishList(UserContext.Current, messageArgs);
        }
    }
}