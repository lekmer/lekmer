using System.Globalization;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockCategoryProductListEntityMapper : BlockCategoryProductListEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockCategoryProductList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}