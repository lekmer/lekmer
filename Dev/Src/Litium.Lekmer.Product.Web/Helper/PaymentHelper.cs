﻿using System.Text;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Web.Helper
{
 public	class PaymentHelper
	{ 
		/// <summary>
		/// Gets the lowest monthly part Payment cost (To be used with "pay from xx amount)
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="totalOrderCost"></param>
		/// <returns></returns>
		public static int GetLowestPartPayment(IChannel channel, int totalOrderCost)
		{
			var klarnaSetting = new KlarnaSetting(channel.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');
			var partPaymentBuilder = new StringBuilder();
			int min = int.MaxValue;
			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(channel.CommonName, temp);
				var minimumTotalCost = pclassSetting.MinimumTotalCost;
				var pclassId = pclassSetting.PClassId;

				if (totalOrderCost >= minimumTotalCost && pclassId > 0)
				{
					var monthlyCost = KlarnaClient.PopulatePartPayment(channel, pclassId, totalOrderCost, true) / 100;
					if (monthlyCost < min) min = monthlyCost;
				}
			}
			return min == int.MaxValue ? 0 : min;
		}
	}
}
