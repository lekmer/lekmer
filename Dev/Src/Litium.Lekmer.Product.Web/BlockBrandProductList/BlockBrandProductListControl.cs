﻿using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockBrandProductList
{
    public class BlockBrandProductListControl : BlockControlBase<IBlockBrandProductList>
    {
        private readonly IBlockBrandProductListService _blockBrandProductListService;
        private readonly ILekmerProductService _productService;

        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods"),
         SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        protected ProductCollection Products { get; set; }

        protected IPagingControl PagingControl { get; set; }

        /// <summary>
        /// Initializes the <see cref="BlockBrandProductListControl"/>.
        /// </summary>
        /// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
        /// <param name="blockBrandProductListService"></param>
        /// <param name="productService"></param>
        public BlockBrandProductListControl(ITemplateFactory templateFactory,
                                            IBlockBrandProductListService blockBrandProductListService,
                                            ILekmerProductService productService) : base(templateFactory)
        {
            _blockBrandProductListService = blockBrandProductListService;
            _productService = productService;
        }

        /// <summary>
        /// Gets the block by id.
        /// </summary>
        /// <param name="blockId">Id of the block.</param>
        /// <returns>An instance of <see cref="IBlockCategoryProductList"/>.</returns>
        protected override IBlockBrandProductList GetBlockById(int blockId)
        {
            return _blockBrandProductListService.GetById(UserContext.Current, blockId);
        }

        private void Initialize()
        {
            PagingControl = CreatePagingControl();
            Products = GetProducts();
        }

        /// <summary>
        /// Renders the control content.
        /// </summary>
        /// <returns>Rendered <see cref="BlockContent"/>.</returns>
        protected override BlockContent RenderCore()
        {
            Initialize();

            PagingControl.TotalCount = Products.TotalCount;
            PagingContent pagingContent = PagingControl.Render();

            string head =
                (Template.GetFragment("Head").Render() ?? string.Empty) +
                (pagingContent.Head ?? string.Empty);

            Fragment fragmentContent = Template.GetFragment("Content");
            fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
            fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
            fragmentContent.AddEntity(Block);

            return new BlockContent(
                head,
                fragmentContent.Render());
        }

        /// <summary>
        /// Get products to list.
        /// </summary>
        /// <returns>Collection of products.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual ProductCollection GetProducts()
        {
            return _productService.GetAllByBlock(
                UserContext.Current,
                Block,
                PagingControl.SelectedPage,
                PagingControl.PageSize,
				Block.ProductSortOrderId);
        }

        [Obsolete("Obsolete since 2.1. Use CreatePagingControl instead.")]
        protected virtual IPagingControl InitializePagingControl()
        {
            return null;
        }

        /// <summary>
        /// Initializes the paging control.
        /// </summary>
        /// <returns>Instance of <see cref="IPagingControl"/>.</returns>
        protected virtual IPagingControl CreatePagingControl()
        {
            var pagingControl = IoC.Resolve<IPagingControl>();
            pagingControl.PageBaseUrl = GetPagingBaseUrl();
            pagingControl.PageQueryStringParameterName = BlockId + "-page";
            pagingControl.PageSize = Block.ColumnCount*Block.RowCount;
            pagingControl.Initialize();
            return pagingControl;
        }

        /// <summary>
        /// Gets the base url for paging.
        /// </summary>
        /// <returns>Product url.</returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual string GetPagingBaseUrl()
        {
            return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
        }

        /// <summary>
        /// Renders the list of products.
        /// </summary>
        /// <returns>Rendered products.</returns>
        protected virtual string RenderProductList()
        {
            return CreateGridControl().Render();
        }

        /// <summary>
        /// Initializes the <see cref="GridControl{TItem}"/>.
        /// </summary>
        /// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
        protected virtual GridControl<IProduct> CreateGridControl()
        {
            return new GridControl<IProduct>
                       {
                           Items = Products,
                           ColumnCount = Block.ColumnCount,
                           RowCount = Block.RowCount,
                           Template = Template,
                           ListFragmentName = "ProductList",
                           RowFragmentName = "ProductRow",
                           ItemFragmentName = "Product",
                           EmptySpaceFragmentName = "EmptySpace"
                       };
        }
    }
}