﻿using System.Globalization;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockBrandProductList
{
	public class BlockBrandProductListEntityMapper : BlockEntityMapper<IBlockBrandProductList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockBrandProductList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}