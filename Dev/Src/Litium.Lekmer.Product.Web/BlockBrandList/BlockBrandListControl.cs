﻿using System;
using System.Globalization;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockBrandList
{
    public class BlockBrandListControl : BlockControlBase<IBlockBrandList>
    {
        private readonly IBlockBrandListService _blockBrandListService;
        private readonly IBrandService _brandService;

        protected IPagingControl PagingControl { get; set; }
        protected BrandCollection Brands { get; set; }

        public BlockBrandListControl(
            ITemplateFactory templateFactory,
            IBlockBrandListService blockBrandListService,
            IBrandService brandService) : base(templateFactory)
        {
            _blockBrandListService = blockBrandListService;
            _brandService = brandService;
        }

        protected override IBlockBrandList GetBlockById(int blockId)
        {
            return _blockBrandListService.GetById(UserContext.Current, blockId);
        }

        private void Initialize()
        {
            PagingControl = CreatePagingControl();
            Brands = _brandService.GetAllByBlock(UserContext.Current, Block, PagingControl.SelectedPage,
                                                 PagingControl.PageSize);
        }

        protected override BlockContent RenderCore()
        {
            Initialize();

            PagingControl.TotalCount = Brands.TotalCount;
            PagingContent pagingContent = PagingControl.Render();

            string head =
                (Template.GetFragment("Head").Render() ?? string.Empty) +
                (pagingContent.Head ?? string.Empty);

            Fragment fragmentContent = Template.GetFragment("Content");
            fragmentContent.AddVariable("BrandList", RenderBrandList(), VariableEncoding.None);
            fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("BrandTotalQuantity", GetBrandsTotalQuantity(), VariableEncoding.None);
			fragmentContent.AddVariable("BrandTotalCount", Brands.TotalCount.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("BrandDisplayedCount", Brands.Count.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageSize", (Block.ColumnCount * Block.RowCount).ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageCount",
										((int)Math.Ceiling((decimal)Brands.TotalCount / PagingControl.PageSize))
											.ToString(CultureInfo.InvariantCulture));
            fragmentContent.AddEntity(Block);

            return new BlockContent(head, fragmentContent.Render());
        }

        protected virtual string RenderBrandList()
        {
            var grid = new GridControl<IBrand>
                           {
                               Items = Brands,
                               ColumnCount = Block.ColumnCount,
                               RowCount = Block.RowCount,
                               Template = Template,
                               ListFragmentName = "BrandList",
                               RowFragmentName = "BrandRow",
                               ItemFragmentName = "Brand",
                               EmptySpaceFragmentName = "EmptySpace"
                           };
            return grid.Render();
        }

        private IPagingControl CreatePagingControl()
        {
            var pagingControl = IoC.Resolve<IPagingControl>();
            pagingControl.PageBaseUrl = ResolveUrl(ContentNodeTreeItem.Url);
            pagingControl.PageQueryStringParameterName = BlockId + "-page";
            pagingControl.PageSize = Block.ColumnCount*Block.RowCount;
            pagingControl.Initialize();
            return pagingControl;
        }

		private static string GetBrandsTotalQuantity()
		{
			var brandService = IoC.Resolve<IBrandService>();
			var allBrands = brandService.GetAll(UserContext.Current);
			return allBrands != null 
				? allBrands.Count.ToString(CultureInfo.InvariantCulture)
				: string.Empty;
		}
    }
}