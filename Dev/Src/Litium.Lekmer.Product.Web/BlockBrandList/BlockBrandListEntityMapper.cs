﻿using System.Globalization;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockBrandList
{
	public class BlockBrandListEntityMapper : BlockEntityMapper<IBlockBrandList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockBrandList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.ColumnCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.RowCount.ToString(CultureInfo.InvariantCulture));

			var linkUrl = string.Empty;
			if (item.LinkContentNodeId.HasValue)
			{
				var contentNode = IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current,
				                                                                     item.LinkContentNodeId.Value);
				if (contentNode != null && !string.IsNullOrEmpty(contentNode.Url))
				{
					linkUrl = contentNode.Url;
				}
			}
			fragment.AddCondition("Block.HasNavigationLink", item.LinkContentNodeId.HasValue);
			fragment.AddVariable("Block.NavigationLinkUrl", UrlHelper.ResolveUrl(linkUrl));
		}
	}
}