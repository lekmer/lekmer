﻿using System.Globalization;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class ProductSizeEntityMapper : EntityMapper<IProductSize>
	{
		public override void AddEntityVariables(Fragment fragment, IProductSize item)
		{
			fragment.AddVariable("Size.Id", item.SizeId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Size.EU", FormatSize(item.OverrideEu ?? item.Eu));
			fragment.AddVariable("Size.USMale", FormatSize(item.UsMale));
			fragment.AddVariable("Size.USFemale", FormatSize(item.UsFemale));
			fragment.AddVariable("Size.UKMale", FormatSize(item.UkMale));
			fragment.AddVariable("Size.UKFemale", FormatSize(item.UkFemale));
			fragment.AddVariable("Size.Millimeter", (item.Millimeter + (item.MillimeterDeviation ?? 0)).ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Size.NumberInStock", item.NumberInStock.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Size.ErpId", item.ErpId);
		}

		private static string FormatSize(decimal size)
		{
			var intSize = decimal.ToInt32(size);
			return size - intSize == 0 
				? intSize.ToString(CultureInfo.CurrentCulture)
				: size.ToString(CultureInfo.CurrentCulture);
		}
	}
}