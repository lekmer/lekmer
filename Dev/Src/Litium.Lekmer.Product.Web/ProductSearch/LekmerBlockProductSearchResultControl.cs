﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch;
using Litium.Scensum.ProductSearch.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.ProductSearch
{
	public class LekmerBlockProductSearchResultControl : BlockProductSearchResultControl
	{
		public LekmerBlockProductSearchResultControl(ITemplateFactory templateFactory, IBlockProductSearchResultService blockProductSearchResultService, IProductSearchService productSearchService) : base(templateFactory, blockProductSearchResultService, productSearchService)
		{
		}

		private void Initialize()
		{
			PagingControl = CreatePagingControl();

			string searchQuery = Request.QueryString["q"];
			searchQuery = Regex.Replace(searchQuery, @"[^a-z0-9åäö\s]", "", RegexOptions.IgnoreCase);
			
			Products = !string.IsNullOrEmpty(searchQuery) ? SearchProducts(searchQuery) : new ProductCollection();
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();
			string searchQuery = string.Empty;
			if (!string.IsNullOrEmpty(SearchQuery))
			{
				searchQuery = SearchQuery;
			}
		
			Fragment blockHeadFragment = Template.GetFragment("Head");
			blockHeadFragment.AddVariable("SearchQuery", searchQuery, VariableEncoding.JavaScriptStringEncode);
			string head =
				(blockHeadFragment.Render() ?? string.Empty) +
				(pagingContent.Head ?? string.Empty);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddVariable("ProductTotalCount", Products.TotalCount.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("ProductDisplayedCount", Products.Count.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageSize", (Block.ColumnCount * Block.RowCount).ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("PageCount",
										((int)Math.Ceiling((decimal)Products.TotalCount / PagingControl.PageSize))
											.ToString(CultureInfo.InvariantCulture));
			fragmentContent.AddEntity(Block);

			return new BlockContent(
				head,
				fragmentContent.Render());
		}
		private string SearchQuery
		{
			get
			{
				return Request.QueryString["q"];
			}
		}
	}
}
