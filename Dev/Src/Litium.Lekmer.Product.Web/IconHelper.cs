﻿using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
    internal class IconHelper
    {
        private const string _imageUrlMatchPattern = @"\[Icon.ImageUrl\(""(.*?)""\)\]";
        private const int _imageUrlMatchCommonNamePosition = 1;

        internal static void AddImageVariables(Fragment fragment, IImage image, string iconTitle)
        {
            string originalSizeImageUrl = null;
            string alternativeText = null;
            if (image != null)
            {
                alternativeText = image.AlternativeText;
                originalSizeImageUrl = string.Format(
                    CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
                    WebSetting.Instance.MediaUrl, image.Id, UrlCleaner.CleanUp(iconTitle), image.FormatExtension);
                originalSizeImageUrl = UrlHelper.ResolveUrl(originalSizeImageUrl);
            }

            fragment.AddCondition("Icon.HasImage", image != null);
            fragment.AddVariable("Icon.ImageUrl", originalSizeImageUrl);
            fragment.AddCondition("Icon.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
            fragment.AddVariable("Icon.ImageAlternativeText", alternativeText);

            fragment.AddRegexVariable(
                _imageUrlMatchPattern,
                delegate(Match match)
                {
                    if (image == null) return null;

                    string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

                    string imageUrl = string.Format(
                        CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
                        WebSetting.Instance.MediaUrl, image.Id, commonName, UrlCleaner.CleanUp(iconTitle),
                        image.FormatExtension);

                    return UrlHelper.ResolveUrl(imageUrl);
                },
                VariableEncoding.HtmlEncodeLight,
                RegexOptions.Compiled);
        }
    }
}