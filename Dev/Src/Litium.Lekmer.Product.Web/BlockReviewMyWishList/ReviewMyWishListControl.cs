﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.Product.Web.BlockReviewMyWishList;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
    public class ReviewMyWishListComponent : ComponentControl
    {
        private readonly IBlockReviewMyWishListService _blockReviewMyWishList;
        private readonly ILekmerProductService _productService;
        private readonly IWishListService _wishListService;
        private ProductCollection _allProducts;
        private Collection<WishProduct> _existWishProducts = new Collection<WishProduct>();
        private IPagingControl _pagingControl;
        private ProductCollection _products;
        private Guid _wishListKey;

        public ReviewMyWishListComponent(ITemplateFactory templateFactory, IFormatter formatter,
                                         IProductService productService, IWishListService wishListService)
            : base(templateFactory)
        {
            _productService = (ILekmerProductService) productService;
            _wishListService = wishListService;
            Formatter = formatter;
        }

        protected IPagingControl PagingControl { get; set; }
        protected IFormatter Formatter { get; private set; }

        /// <summary>
        /// Id of the active content node.
        /// </summary>
        protected virtual int? ActiveContentNodeId
        {
            get { return Master.DynamicProperties["ActiveContentNodeId"] as int?; }
        }

        protected override string ModelCommonName
        {
            get { return "ReviewMyWishListComponent"; }
        }

        /// <summary>
        /// 'StartFromId' parameter.
        /// </summary>
        protected virtual int ColumnCount
        {
            get { return Parameters.GetInt32("ColumnCount"); }
        }

        /// <summary>
        /// 'StartFromCommonName' parameter.
        /// </summary>
        protected virtual int RowCount
        {
            get { return Parameters.GetInt32("RowCount"); }
        }

        private void Initialize()
        {
            _wishListKey = WishListCookie.GetWishListKey();

            if (!string.IsNullOrEmpty(Request.QueryString["mode"]) &&
                Request.QueryString["mode"].Equals("deleteProduct") && _wishListKey != Guid.Empty)
            {
                int? productId = Request.QueryString.GetInt32OrNull("id");
                if (productId.HasValue)
                {
                    //Get products that is online 
                    GetProducts();

                    IWishList wishList = _wishListService.GetByKey(_wishListKey);
                    if (wishList != null && !string.IsNullOrEmpty(wishList.ProductWishList))
                    {
                        var wishProduct = new WishProduct();
                        wishProduct.ProductId = productId.Value;

                        int? sizeId = Request.QueryString.GetInt32OrNull("sizeid");

                        if (sizeId.HasValue)
                            wishProduct.SizeId = sizeId;
                        _existWishProducts.Remove(null);
                        //wish products that is online
                        _existWishProducts.Remove(_existWishProducts.FirstOrDefault(
                            wp => wp.ProductId == wishProduct.ProductId && wp.SizeId == wishProduct.SizeId));

                        var itemBuilder = new StringBuilder();
                        foreach (WishProduct item in _existWishProducts)
                        {
                            if (item.SizeId.HasValue)
                            {
                                itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}|{1},",
                                                                 item.ProductId, item.SizeId));
                            }
                            else
                            {
                                itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0},", item.ProductId));
                            }
                        }
                        if (itemBuilder.Length > 0)
                            itemBuilder.Remove(itemBuilder.Length - 1, 1);

                        wishList.ProductWishList = itemBuilder.ToString();
                        _wishListService.Save(wishList);
                    }
                }
            }
            _pagingControl = CreatePagingControl();
            _allProducts = GetProducts();
            int totalCount = _allProducts.Count;
            _products =
                new ProductCollection(_allProducts.Skip((PagingControl.SelectedPage - 1)*PagingControl.PageSize).Take(
                    PagingControl.PageSize));
            _products.TotalCount = totalCount;
        }

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual ProductCollection GetProducts()
        {
            if (_wishListKey == Guid.Empty)
                return new ProductCollection();

            var productIdCollection = new ProductIdCollection();
            var wishProducts = new Collection<WishProduct>();
            IWishList wishList = _wishListService.GetByKey(_wishListKey);
            if (wishList == null || string.IsNullOrEmpty(wishList.ProductWishList))
                return new ProductCollection();

            string[] productsIds = wishList.ProductWishList.Split(',');
            foreach (string productsId in productsIds)
            {
                string[] product = productsId.Split('|');
                int productId;
                if (int.TryParse(product[0], out productId))
                {
                    if (product.Length > 1)
                    {
                        int sizeId;
                        if (int.TryParse(product[1], out sizeId))
                        {
                            if (wishProducts.FirstOrDefault(id => id.ProductId == productId && id.SizeId == sizeId) ==
                                null)
                            {
                                wishProducts.Add(new WishProduct(productId, sizeId));
                                if (!productIdCollection.Contains(productId))
                                    productIdCollection.Add(productId);
                                continue;
                            }
                        }
                    }
                    else if (wishProducts.FirstOrDefault(id => id.ProductId == productId && !id.SizeId.HasValue) == null)
                    {
                        if (!productIdCollection.Contains(productId))
                            productIdCollection.Add(productId);
                        wishProducts.Add(new WishProduct(productId, null));
                    }
                }
            }

            if (productIdCollection.Count == 0)
            {
                WishListCookie.RemoveCookie(_wishListKey);
                return new ProductCollection();
            }

            return GetOnlineProducts(wishProducts, productIdCollection);
        }

        protected virtual ProductCollection GetOnlineProducts(Collection<WishProduct> wishProducts,
                                                              ProductIdCollection productIdCollection)
        {
            var productCollection = new ProductCollection();

            _existWishProducts = new Collection<WishProduct>();
            ProductCollection tempCollection = _productService.PopulateViewProducts(UserContext.Current,
                                                                                    productIdCollection);
            foreach (WishProduct wishProduct in wishProducts)
            {
                WishProduct product1 = wishProduct;
                IProduct lekmerProductView = tempCollection.FirstOrDefault(product => product.Id == product1.ProductId);
                if (lekmerProductView != null)
                {
                    var product = (ILekmerProductView) ObjectCopier.Clone(lekmerProductView);
                    if (wishProduct.SizeId.HasValue)
                    {
                        WishProduct tempWishProduct = wishProduct;
                        IProductSize productSize = ((ILekmerProductView) lekmerProductView).ProductSizes.FirstOrDefault(
                            size => size.SizeId == tempWishProduct.SizeId && size.NumberInStock > 0);
                        if (productSize != null)
                        {
                            product.DefaultProductSize = productSize;
                            productCollection.Add(product);
                            _existWishProducts.Add(wishProduct);
                        }
                    }
                    else
                    {
                        productCollection.Add(product);
                        _existWishProducts.Add(wishProduct);
                    }
                }
            }
            return productCollection;
        }

        protected IPagingControl CreatePagingControl()
        {
            IContentNodeTreeItem treeItem =
                IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current, ActiveContentNodeId.Value);
            PagingControl = IoC.Resolve<IPagingControl>();
            PagingControl.PageBaseUrl = ResolveUrl(treeItem.Url);
            PagingControl.PageQueryStringParameterName = "wishlist-page";
            PagingControl.PageSize = ColumnCount*RowCount;
            PagingControl.Initialize();
            return PagingControl;
        }

        public override ComponentContent Render()
        {
            Initialize();

            _pagingControl.TotalCount = _products.TotalCount;
            PagingContent pagingContent = _pagingControl.Render();
            Fragment fragmentContent = Template.GetFragment("Content");
            fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
            fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);

            Price price = GetActualPriceSummary();
            fragmentContent.AddVariable(
                "WishList.PriceIncludingVatSummary",
                Formatter.FormatPrice(Channel.Current, price.IncludingVat));
            fragmentContent.AddVariable(
                "WishList.PriceExcludingVatSummary",
                Formatter.FormatPrice(Channel.Current, price.ExcludingVat));
            fragmentContent.AddVariable(
                "WishList.TotalProductCount",
                Formatter.FormatNumber(Channel.Current, _allProducts.Count));

            fragmentContent.AddVariable("WishListKey", _wishListKey.ToString);

            Fragment fragmentHead = Template.GetFragment("Head");
            string head =
                (fragmentHead.Render() ?? string.Empty) +
                (pagingContent.Head ?? string.Empty);


            var content =
                new ComponentContent(
                    head,
                    fragmentContent.Render());
            return content;
        }

        public virtual Price GetActualPriceSummary()
        {
            var priceSummary = new Price();
            foreach (ILekmerProduct product in _allProducts)
            {
                priceSummary += GetActualPrice(product);
            }
            return priceSummary;
        }

        public virtual Price GetActualPrice(ILekmerProduct product)
        {
            decimal priceIncludingVat;
            decimal priceExcludingVat;
            if (product.CampaignInfo != null)
            {
                priceIncludingVat = product.CampaignInfo.Price.IncludingVat;
                priceExcludingVat = product.CampaignInfo.Price.ExcludingVat;
            }
            else
            {
                priceIncludingVat = product.Price.PriceIncludingVat;
                priceExcludingVat = product.Price.PriceExcludingVat;
            }
            var price = new Price(priceIncludingVat, priceExcludingVat);
            return price;
        }

        private string RenderProductList()
        {
            return new WishListGridControl<IProduct>
                       {
                           Items = _products,
                           ColumnCount = ColumnCount,
                           RowCount = RowCount,
                           Template = Template,
                           ListFragmentName = "ProductList",
                           RowFragmentName = "ProductRow",
                           ItemFragmentName = "Product",
                           EmptySpaceFragmentName = "EmptySpace",
                           EditMode = true
                       }.Render();
        }
    }
}