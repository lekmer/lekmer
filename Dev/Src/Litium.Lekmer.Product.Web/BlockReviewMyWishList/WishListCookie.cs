﻿using System;
using System.Web;
using Litium.Lekmer.Product.Web.BlockReviewMyWishList;

namespace Litium.Lekmer.Product.Web
{
    public static class WishListCookie
    {
        public static string WishListCookieKey = "LekmerWishList";
      

        public static Guid GetWishListKey()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[WishListCookieKey];
            if (cookie == null)
            {
              return new Guid( SetWishListKey());
            }
            return new Guid(  cookie.Value);
        }
        public static string SetWishListKey()
        {
            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(354, 50, 0, 0, 0);
            HttpCookie cookie = HttpContext.Current.Request.Cookies[WishListCookieKey];
            if (cookie == null)
            {
                cookie = new HttpCookie(WishListCookieKey);
                cookie.Value = Guid.Empty.ToString();
            }
            cookie.Expires = dtNow + tsMinute;
            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie.Value;
        }

        public static string CreateWithList()
        {
            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(354, 50, 0, 0, 0);
            HttpCookie cookie = new HttpCookie(WishListCookieKey);
            cookie.Value = Guid.NewGuid().ToString();
            cookie.Expires = dtNow + tsMinute;
            HttpContext.Current.Response.Cookies.Add(cookie);
            return cookie.Value;
        }


        public static void RemoveCookie(Guid wishListKey)
        {
            if (HttpContext.Current.Response.Cookies[WishListCookieKey] != null)
                HttpContext.Current.Response.Cookies[WishListCookieKey].Value= WishListCookieConstants.EmptyCookie;
        }
    }
}