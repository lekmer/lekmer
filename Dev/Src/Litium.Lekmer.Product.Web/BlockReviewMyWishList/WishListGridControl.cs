﻿using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockReviewMyWishList
{
    class WishListGridControl<TItem> : GridControl<TItem> where TItem : class
    {
        public bool EditMode { get; set; }

        protected override string RenderItem(int columnIndex, TItem item)
        {
            ILekmerProductView lekmerProductView = (ILekmerProductView)item;
            Fragment fragmentItem = Template.GetFragment(ItemFragmentName);
            AddPositionCondition(fragmentItem, columnIndex, ColumnCount);
            fragmentItem.AddEntity((IProductView)lekmerProductView);
            fragmentItem.AddVariable("Product.Description", lekmerProductView.Description, VariableEncoding.None);
            fragmentItem.AddCondition("Product.HasSize", lekmerProductView.DefaultProductSize != null);
            if (lekmerProductView.DefaultProductSize != null)
            {
                fragmentItem.AddEntity(lekmerProductView.DefaultProductSize);
            }
            fragmentItem.AddCondition("EditMode", EditMode);

            return fragmentItem.Render();
        }
    }
}
