﻿namespace Litium.Lekmer.Product.Web
{
    public class WishProduct
    {
        public WishProduct(int productId, int? sizeId)
        {
            ProductId = productId;
            SizeId = sizeId;
        }
        public WishProduct()
        {
        }
        public int ProductId;
        public int? SizeId;
    }
}
