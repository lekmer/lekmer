using System.Globalization;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web.ProductPage;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockProductImageListEntityMapper : BlockProductImageListEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductImageList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}