using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Lekmer.Product.Web.ProductPage.Cookie;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockProductDetailControl : BlockProductDetailControl
	{
		private readonly IContentNodeService _contentNodeService;
		private int _maxQuantity;
		private List<string> emptyTagGroups = new List<string>();
	    private int? _sizeId;

		public LekmerBlockProductDetailControl(ITemplateFactory templateFactory, IContentNodeService contentNodeService) : base(templateFactory)
		{
			_contentNodeService = contentNodeService;
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null) return new BlockContent(null, "[ Can't render product on this page ]");
			var lekmerProduct = (ILekmerProductView)Product;
          
		    Initialize();

			string genericFilterPageUrl = GetGenericFilterPageUrl();

			Fragment fragmentContent = Template.GetFragment("Content");
			string trackingCode = Request.QueryString["trackingcode"];
			fragmentContent.AddVariable("TrackingCode", trackingCode);
			RenderMonthlypartPaymentCost(fragmentContent);
			fragmentContent.AddCondition("Product.IsRecommended", !string.IsNullOrEmpty(trackingCode));
			fragmentContent.AddCondition("WasSearched", WasSearched);
			fragmentContent.AddEntity(Product);
			fragmentContent.AddEntity(Block);
			fragmentContent.AddVariable("Setting.MaxQuantity", _maxQuantity.ToString(CultureInfo.CurrentCulture), VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:QuantityOption", RenderQuantityOptions(lekmerProduct), VariableEncoding.None);
			fragmentContent.AddVariable("Iterate:SizeOption", RenderSizeOptions(), VariableEncoding.None);
			fragmentContent.AddCondition("Product.HasIcons", lekmerProduct.Icons.Count > 0);
			fragmentContent.AddVariable("Iterate:Icon", RenderIcons(lekmerProduct.Icons), VariableEncoding.None);
			fragmentContent.AddRegexVariable(
@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;

					return RenderTagGroup(tagGroupCommonName, genericFilterPageUrl);
				},
					VariableEncoding.None);

			return new BlockContent(
				 RenderHead(trackingCode),
				fragmentContent.Render());
		}

		private void RenderMonthlypartPaymentCost(Fragment fragmentContent)
		{
			var klarnaSum = (int)Math.Round(Product.CampaignInfo.Price.IncludingVat) * 100;
			var monthlyPayment = PaymentHelper.GetLowestPartPayment(Channel.Current, klarnaSum);
			fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", monthlyPayment > 0);
			fragmentContent.AddVariable("MonthlyPartPaymentCost", monthlyPayment.ToString(CultureInfo.InvariantCulture),
										VariableEncoding.None);
		}
		private string GetGenericFilterPageUrl()
		{
			var item = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, "filter");
			if (item == null || item.Url.IsNullOrEmpty())
			{
				return null;
			}

			return UrlHelper.ResolveUrlHttp(item.Url);
		}

		private string RenderHead(string trackingCode)
		{
			Fragment fragmentHead = Template.GetFragment("Head");

			if (WasSearched)
			{
				fragmentHead.AddVariable("SearchQuery", SearchQuery, VariableEncoding.JavaScriptStringEncode);
			}
			fragmentHead.AddCondition("WasSearched", WasSearched);
			fragmentHead.AddVariable("TrackingCode", trackingCode);
			fragmentHead.AddCondition("Product.IsRecommended", !string.IsNullOrEmpty(trackingCode));
			fragmentHead.AddEntity(Product);
			AddOrderItemsArrayParam(fragmentHead);
			return fragmentHead.Render();
		}

		private string RenderTagGroup(string commonName, string genericFilterPageUrl)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var lekmerProduct = (ILekmerProductView) Product;
			var tagGroup = lekmerProduct.TagGroups
				.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				emptyTagGroups.Add(commonName);
				return null;
			}

			var fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddCondition("CountIsZero", tagGroup.Tags.Count == 0);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup, genericFilterPageUrl), VariableEncoding.None);
			return fragmentTagGroup.Render();
		}

		private string RenderTags(ITagGroup tagGroup, string genericFilterPageUrl)
		{
			var tagBuilder = new StringBuilder();
			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				string tagUrl = GetTagUrl(tag, genericFilterPageUrl);

				var fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				fragmentTag.AddVariable("Tag.Url", tagUrl);
				fragmentTag.AddCondition("Tag.HasUrl", tagUrl != null);

				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}
			return tagBuilder.ToString();
		}

		private static string GetTagUrl(ITag tag, string genericFilterPageUrl)
		{
			if (genericFilterPageUrl.IsNullOrEmpty())
			{
				return null;
			}

			var query = new QueryBuilder();
			query.Add("mode", "filter");
			query.Add("taggroup" + tag.TagGroupId + "-tag-id", tag.Id.ToString(CultureInfo.InvariantCulture));
			return genericFilterPageUrl + query;
		}

		private static void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}

		private void Initialize()
		{
			if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
			{
				_maxQuantity = 10;
			}
            _sizeId = GetSizeId();
            if(!IsPostBack)
		    ClickHistoryCookie.AddProductToClickHistory(Product.Id);
		}

		private string RenderQuantityOptions(ILekmerProductView lekmerProduct)
		{
			var maxItems = 0;
			if (lekmerProduct.IsBuyable)
			{
				maxItems = Math.Min(_maxQuantity, Product.NumberInStock);
			}
			else if (lekmerProduct.IsBookable)
			{
				maxItems = _maxQuantity;
			}

			var optionBuilder = new StringBuilder();
			for (int i = 1; i <= maxItems; i++)
			{
				var fragmentOption = Template.GetFragment("QuantityOption");
				fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));
				optionBuilder.AppendLine(fragmentOption.Render());
			}
			return optionBuilder.ToString();
		}

		private string RenderSizeOptions()
		{
			var optionBuilder = new StringBuilder();
			foreach (var size in ((ILekmerProductView)Product).ProductSizes)
			{
				var fragmentOption = Template.GetFragment("SizeOption");
			    fragmentOption.AddCondition("selected", _sizeId.HasValue ? _sizeId.Value == size.SizeId : false);
				fragmentOption.AddEntity(size);
				optionBuilder.AppendLine(fragmentOption.Render());
			}
			return optionBuilder.ToString();
		}

		private string RenderIcons(IEnumerable<IIcon> icons)
		{
			var iconBuilder = new StringBuilder();
			foreach (IIcon icon in icons)
			{
				var fragmentIcon = Template.GetFragment("Icon");
				fragmentIcon.AddEntity(icon);
				iconBuilder.AppendLine(fragmentIcon.Render());
			}
			return iconBuilder.ToString();
		}
		private void AddOrderItemsArrayParam(Fragment fragment)
		{
			var itemBuilder = new StringBuilder();
			itemBuilder.Append("new Array(");

			for (int i = 0; i < emptyTagGroups.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", emptyTagGroups[i]));
						}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", emptyTagGroups[i]));
				}
			}

			itemBuilder.Append(")");
			fragment.AddVariable("RemoveTagGroups", itemBuilder.ToString);
		}
		private bool WasSearched
		{
			get
			{
				return Request.UrlReferrer == null ? false : Request.UrlReferrer.Query.Contains("q");
			}
		}

		private string SearchQuery
		{
			get
			{
				var urlString = Request.UrlReferrer.ToString();
				int startIndex = urlString.IndexOf("q=") + 2;
				int length = urlString.Substring(startIndex).IndexOf("&") == -1
								? urlString.Length - startIndex
								: urlString.Substring(startIndex).IndexOf("&");
				return urlString.Substring(startIndex, length);
			}
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected int? GetSizeId()
        {
            int sizeId;
            if (!int.TryParse(Request.QueryString["sizeid"], out sizeId))
            {
                return null;
            }
            return sizeId;
        }

	}
}