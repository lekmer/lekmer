﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web.ProductPage;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class LekmerBlockProductRelationListControl : BlockProductRelationListControl
	{
		public LekmerBlockProductRelationListControl(ITemplateFactory templateFactory, IBlockProductRelationListService blockProductRelationListService, IProductService productService) : base(templateFactory, blockProductRelationListService, productService)
		{
		}
		/// <summary>
		/// Renders the control content.
		/// </summary>
		/// <returns>Rendered <see cref="BlockContent"/>.</returns>
		protected override BlockContent RenderCore()
		{
			if (Product == null) return new BlockContent(null, "[ Can't render product relation list on this page ]");
			Initialize();

			if (Products.Count<1)
			{
				return new BlockContent();
			}

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			string head =
				(Template.GetFragment("Head").Render() ?? string.Empty) +
				(pagingContent.Head ?? string.Empty);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			return new BlockContent(
				head,
				fragmentContent.Render());
		}
		private void Initialize()
		{
			PagingControl = CreatePagingControl();
			Products = GetProducts();
		}
	}
}
