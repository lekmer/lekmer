using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class TopListProductService : ITopListProductService
	{
		protected TopListProductRepository Repository { get; private set; }
		protected IBlockTopListProductService BlockTopListProductService { get; private set; }
		protected IBlockTopListCategoryService BlockTopListCategoryService { get; private set; }
		protected IProductService ProductService { get; private set; }

		public TopListProductService(
			TopListProductRepository repository, IBlockTopListProductService blockTopListProductService,
			IBlockTopListCategoryService blockTopListCategoryService, IProductService productService)
		{
			Repository = repository;
			BlockTopListProductService = blockTopListProductService;
			BlockTopListCategoryService = blockTopListCategoryService;
			ProductService = productService;
		}

		/// <summary>
		/// Gets all top list products. Forced position is taken into account.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="block"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public ProductCollection GetAllByBlock(IUserContext context, IBlockTopList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			ProductIdCollection productIds = TopListProductCollectionCache.Instance.TryGetItem(
				new TopListProductCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize),
				delegate { return GetIdAllByBlock(context, block, pageNumber, pageSize); });

			return ProductService.PopulateProducts(context, productIds);
		}

		/// <summary>
		/// Gets ids of top list products. Forced position is taken into account.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="block"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public virtual ProductIdCollection GetIdAllByBlock(
			IUserContext context, IBlockTopList block, int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");
			if (pageNumber < 1)
				throw new ArgumentOutOfRangeException("pageNumber", pageNumber, "pageNumber must be 1 or higher.");
			if (pageSize < 1) throw new ArgumentOutOfRangeException("pageSize", pageSize, "pageSize must be 1 or higher.");

			// All the forced products. The ones out of range are filtered so that they are not included.
			var forcedProducts = BlockTopListProductService.GetAllByBlock(context, block)
				.Where(fp => fp.Position > 0 && fp.Position <= block.TotalProductCount);

			int displayFrom = ((pageNumber - 1)*pageSize) + 1;
			int displayTo = Math.Min(pageNumber*pageSize, block.TotalProductCount);

			int forcedBeforeRangeCount = forcedProducts
				.Count(fp => fp.Position < displayFrom);
			var forcedWithinRange = forcedProducts
				.Where(fp => fp.Position >= displayFrom && fp.Position <= displayTo);

			int fetchFrom = displayFrom - forcedBeforeRangeCount;
			int fetchTo = displayTo - (forcedBeforeRangeCount + forcedWithinRange.Count());

			// Get categories.
			ProductIdCollection statisticsProducts = GetIdAllByStatisticsExcludeForced(context, block, fetchFrom, fetchTo);

			bool thisPageHasStatisticsProducts = statisticsProducts.Count > 0;

			// Add forced products.
			// Forced products not added the page doesn't have statistics products. Those will be added in the next step.
			if (thisPageHasStatisticsProducts)
			{
				foreach (IBlockTopListProduct forcedProduct in forcedWithinRange)
				{
					int index = forcedProduct.Position - displayFrom;

					if (index < statisticsProducts.Count)
					{
						statisticsProducts.Insert(index, forcedProduct.Product.Id);
					}
					else
					{
						statisticsProducts.Add(forcedProduct.Product.Id);
					}
				}
			}
			else if (forcedWithinRange.Count() == pageSize)
			{
				foreach (IBlockTopListProduct forcedProduct in forcedWithinRange)
				{
					statisticsProducts.Add(forcedProduct.Product.Id);
				}
			}

			// Additional products that can be outside of the current display range.
			if (statisticsProducts.Count < pageSize && block.TotalProductCount > statisticsProducts.TotalCount)
			{
				int additionalForcedUsedBeforeThisPage = Math.Max((displayFrom - 1) - statisticsProducts.TotalCount, 0);
				int additionalForcedRequiredOnThisPage = (displayTo - (displayFrom - 1)) - statisticsProducts.Count;

				IEnumerable<IBlockTopListProduct> additionalForcedProducts;
				if (thisPageHasStatisticsProducts)
				{
					// Only add products from position outside of this range.
					additionalForcedProducts = forcedProducts
						.Where(fp => fp.Position > displayTo)
						.Take(additionalForcedRequiredOnThisPage);
				}
				else
				{
					// Add products from position in this range and outside of this range.
					additionalForcedProducts = forcedProducts
						.Skip(additionalForcedUsedBeforeThisPage)
						.Take(additionalForcedRequiredOnThisPage);
				}

				foreach (IBlockTopListProduct forcedProduct in additionalForcedProducts)
				{
					statisticsProducts.Add(forcedProduct.Product.Id);
				}
			}

			// Set the totalcount (never bigger then block.TotalProductCount).
			statisticsProducts.TotalCount =
				Math.Min(statisticsProducts.TotalCount + forcedProducts.Count(), block.TotalProductCount);

			return statisticsProducts;
		}

		[SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "To")]
		public virtual ProductIdCollection GetIdAllByStatisticsExcludeForced(
			IUserContext context, IBlockTopList block, int from, int to)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			Collection<ICategoryView> categories = null;
			if (!block.IncludeAllCategories)
			{
				categories = BlockTopListCategoryService.GetViewAllByBlock(context, block);
			}

			// Get products by categories/statistics.
			return GetIdAllByStatisticsAndCategoriesExcludeForced(
				context, block, block.IncludeAllCategories, categories, from, to);
		}

		[SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "To")]
		public virtual ProductIdCollection GetIdAllByStatisticsAndCategoriesExcludeForced(
			IUserContext context, IBlockTopList block, bool includeAllCategories, Collection<ICategoryView> categories, int from, int to)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			DateTime countOrdersFrom = DateTime.Now.AddDays(-block.OrderStatisticsDayCount);

			return Repository.GetIdAllByStatisticsAndCategoriesExcludeForced(
				context.Channel.Id, block.Id, includeAllCategories, categories, countOrdersFrom, from, to);
		}
	}
}