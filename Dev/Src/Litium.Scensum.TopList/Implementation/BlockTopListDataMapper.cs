﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.TopList.Mapper
{
	public class BlockTopListDataMapper : DataMapperBase<IBlockTopList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockTopListDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockTopList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockTopList = IoC.Resolve<IBlockTopList>();
			block.ConvertTo(blockTopList);
			blockTopList.ColumnCount = MapValue<int>("BlockTopList.ColumnCount");
			blockTopList.RowCount = MapValue<int>("BlockTopList.RowCount");
			blockTopList.TotalProductCount = MapValue<int>("BlockTopList.TotalProductCount");
			blockTopList.IncludeAllCategories = MapValue<bool>("BlockTopList.IncludeAllCategories");
			blockTopList.OrderStatisticsDayCount = MapValue<int>("BlockTopList.OrderStatisticsDayCount");
			blockTopList.SetUntouched();
			return blockTopList;
		}
	}
}