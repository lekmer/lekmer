﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.TopList.Repository
{
	public class BlockTopListRepository
	{
		protected virtual DataMapperBase<IBlockTopList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockTopList>(dataReader);
		}

		public virtual IBlockTopList GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect("[addon].[pBlockTopListGetByIdSecure]", parameters,
					                                new DatabaseSetting("BlockTopListRepository.GetByIdSecure")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockTopList GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect("[addon].[pBlockTopListGetById]", parameters,
					                                new DatabaseSetting("BlockTopListRepository.GetById")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockTopList block)
		{
			if (block == null) throw new ArgumentNullException("block");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ColumnCount", block.ColumnCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("RowCount", block.RowCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("TotalProductCount", block.TotalProductCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllCategories", block.IncludeAllCategories, SqlDbType.Bit),
					ParameterHelper.CreateParameter("OrderStatisticsDayCount", block.OrderStatisticsDayCount, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[addon].[pBlockTopListSave]", parameters,
			                                 new DatabaseSetting("BlockTopListRepository.Save"));
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pBlockTopListDelete]", parameters, dbSettings);
		}
	}
}