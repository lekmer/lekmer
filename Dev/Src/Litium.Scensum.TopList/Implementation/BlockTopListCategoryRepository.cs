﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.TopList.Repository
{
	public class BlockTopListCategoryRepository
	{
		protected virtual DataMapperBase<IBlockTopListCategory> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockTopListCategory>(dataReader);
		}

		public virtual void Save(IBlockTopListCategory blockCategory)
		{
			if (blockCategory == null) throw new ArgumentNullException("blockCategory");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockCategory.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", blockCategory.Category.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", blockCategory.IncludeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("BlockTopListCategoryRepository.Save");
			new DataHandler().ExecuteCommand("[addon].[pBlockTopListCategorySave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListCategoryRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pBlockTopListCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockTopListCategory> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListCategoryRepository.GetAllByBlock");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[addon].[pBlockTopListCategoryGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockTopListCategory> GetAllByBlock(int languageId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockTopListCategoryRepository.GetAllByBlock");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[addon].[pBlockTopListCategoryGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}