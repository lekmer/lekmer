﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.TopList
{
	[Serializable]
	public class BlockTopList : BlockBase, IBlockTopList
	{
		private int _columnCount;
		private int _rowCount;
		private int _totalProductCount;
		private bool _includeAllCategories;

		private int _orderStatisticsDayCount;

		public int ColumnCount
		{
			get { return _columnCount; }
			set
			{
				CheckChanged(_columnCount, value);
				_columnCount = value;
			}
		}

		public int RowCount
		{
			get { return _rowCount; }
			set
			{
				CheckChanged(_rowCount, value);
				_rowCount = value;
			}
		}

		public int TotalProductCount
		{
			get { return _totalProductCount; }
			set
			{
				CheckChanged(_totalProductCount, value);
				_totalProductCount = value;
			}
		}

		public bool IncludeAllCategories
		{
			get { return _includeAllCategories; }
			set
			{
				CheckChanged(_includeAllCategories, value);
				_includeAllCategories = value;
			}
		}

		public int OrderStatisticsDayCount
		{
			get { return _orderStatisticsDayCount; }
			set
			{
				CheckChanged(_orderStatisticsDayCount, value);
				_orderStatisticsDayCount = value;
			}
		}
	}
}