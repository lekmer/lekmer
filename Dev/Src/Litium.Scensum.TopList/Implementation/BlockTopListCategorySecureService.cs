using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Scensum.TopList.Repository;

namespace Litium.Scensum.TopList
{
	public class BlockTopListCategorySecureService : IBlockTopListCategorySecureService
	{
		protected BlockTopListCategoryRepository Repository { get; private set; }

		public BlockTopListCategorySecureService(BlockTopListCategoryRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IBlockTopListCategory> GetAllByBlock(int blockId)
		{
			return Repository.GetAllByBlockSecure(blockId);
		}

		public virtual void Save(int blockId, Collection<IBlockTopListCategory> blockCategories)
		{
			if (blockCategories == null) throw new ArgumentNullException("blockCategories");
			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAll(blockId);
				foreach (var blockCategory in blockCategories)
				{
					blockCategory.BlockId = blockId;
					Repository.Save(blockCategory);
				}
				transaction.Complete();
			}
		}
	}
}