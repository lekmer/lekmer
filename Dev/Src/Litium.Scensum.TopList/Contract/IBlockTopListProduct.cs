﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListProduct : IBusinessObjectBase
	{
		int BlockId { get; set; }
		IProduct Product { get; set; }
		int Position { get; set; }
	}
}