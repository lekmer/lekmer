﻿using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopList : IBlock
	{
		int ColumnCount { get; set; }
		int RowCount { get; set; }
		int TotalProductCount { get; set; }
		bool IncludeAllCategories { get; set; }
		int OrderStatisticsDayCount { get; set; }
	}
}