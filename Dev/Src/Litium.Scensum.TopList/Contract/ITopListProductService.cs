using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	public interface ITopListProductService
	{
		/// <summary>
		/// Gets all top list products. Forced position is taken into account.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="block"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		ProductCollection GetAllByBlock(IUserContext context, IBlockTopList block, int pageNumber, int pageSize);

		/// <summary>
		/// Gets ids of top list products. Forced position is taken into account.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="block"></param>
		/// <param name="pageNumber"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		ProductIdCollection GetIdAllByBlock(
			IUserContext context, IBlockTopList block, int pageNumber, int pageSize);
	}
}