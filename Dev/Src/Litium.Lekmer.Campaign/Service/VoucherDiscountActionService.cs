using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class VoucherDiscountActionService : ICartActionPluginService
	{
		protected VoucherDiscountActionRepository Repository { get; private set; }

		public VoucherDiscountActionService(VoucherDiscountActionRepository repository)
		{
			Repository = repository;
		}

		public ICartAction GetById(int cartActionId)
		{
			return Repository.GetById(cartActionId);
		}

		public ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			return GetById(cartActionId);
		}
	}
}