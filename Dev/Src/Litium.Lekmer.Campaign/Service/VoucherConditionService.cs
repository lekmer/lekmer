﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
    public class VoucherConditionService : IConditionPluginService
    {
        protected VoucherConditionRepository Repository { get; private set; }

        public VoucherConditionService(VoucherConditionRepository voucherConditionRepository)
        {
            Repository = voucherConditionRepository;
        }

        /// <summary>
        /// Gets cart value condition by Id
        /// </summary>
        /// <param name="conditionId">Condition Id</param>
        [Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int conditionId).", true)]
        public virtual ICondition GetById(int conditionId)
        {
            if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

            var condition = Repository.GetById(conditionId);
            if (condition == null) return null;
            condition.BatchIds = Repository.GetBatchesByConditionId(conditionId);
            return condition;
        }

        /// <summary>
        /// Gets cart value condition by Id
        /// </summary>
        /// <param name="userContext">User context</param>
        /// <param name="conditionId">Condition Id</param>
        public virtual ICondition GetById(IUserContext userContext, int conditionId)
        {
            if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

            var condition = Repository.GetById(conditionId);
            if (condition == null) return null;
            condition.BatchIds = Repository.GetBatchesByConditionId(conditionId);
            return condition;
        }
    }
}