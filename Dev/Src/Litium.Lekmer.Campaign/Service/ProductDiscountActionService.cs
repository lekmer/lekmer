﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class ProductDiscountActionService : IProductActionPluginService
	{
		protected ProductDiscountActionRepository Repository { get; private set; }

		public ProductDiscountActionService(ProductDiscountActionRepository cartCampaignRepository)
		{
			Repository = cartCampaignRepository;
		}

		public IProductAction GetById(IUserContext context, int productActionId)
		{
            if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var productDiscountAction = Repository.GetById(productActionId);
			if (productDiscountAction == null)
			{
				return null;
			}
			productDiscountAction.ProductDiscountPrices = Repository.GetItems(
				context.Channel.Id, context.Customer == null ? null : (int?)context.Customer.Id, productActionId);
			return productDiscountAction;
		}
	}
}