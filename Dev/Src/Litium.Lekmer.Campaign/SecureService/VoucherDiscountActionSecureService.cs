using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
    public class VoucherDiscountActionSecureService : ICartActionPluginSecureService
    {
        protected IAccessValidator AccessValidator { get; private set; }
        protected VoucherDiscountActionRepository Repository { get; private set; }
        protected ICartActionTypeSecureService CartActionTypeSecureService { get; set; }

        public VoucherDiscountActionSecureService(
            IAccessValidator accessValidator,
            VoucherDiscountActionRepository repository,
            ICartActionTypeSecureService cartActionTypeSecureService)
        {
            AccessValidator = accessValidator;
            Repository = repository;
            CartActionTypeSecureService = cartActionTypeSecureService;
        }

        public ICartAction Create()
        {
            if (CartActionTypeSecureService == null)
                throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");

            var action = IoC.Resolve<IVoucherDiscountAction>();
            action.ActionType = CartActionTypeSecureService.GetByCommonName("VoucherDiscountActionType");
            action.FixedDiscount = true;
            return action;
        }

        public void Delete(ISystemUserFull systemUserFull, int id)
        {if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
        }

        public ICartAction GetById(int id)
        {
            return Repository.GetById(id);
        }

        public void Save(ISystemUserFull user, ICartAction action)
        { 
            if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

            var voucherDiscountAction = action as IVoucherDiscountAction;
            if (voucherDiscountAction == null) throw new InvalidOperationException("action is not IVoucherDiscountAction type");

            AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

            Repository.Save(voucherDiscountAction);
        }
    }
}