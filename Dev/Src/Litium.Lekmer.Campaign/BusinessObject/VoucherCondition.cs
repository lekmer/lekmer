﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
    [Serializable]
    public class VoucherCondition : Condition, IVoucherCondition
    {
        private Collection<int> _batchIds;
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public Collection<int> BatchIds
        {
            get { return _batchIds; }
            set
            {
                CheckChanged(_batchIds, value);
                _batchIds = value;
            }
        }

        public override bool Fulfilled(IUserContext context)
        {
            var voucher = IoC.Resolve<IVoucherSession>().Voucher;
            if (voucher == null || !voucher.IsValid ||!voucher.VoucherbatchId.HasValue)
            {
                return false;
            }
            if (BatchIds.Contains(voucher.VoucherbatchId.Value))
            {
                return true;
            }
            return false;
        }
        public override object[] GetInfoArguments()
        {
            return new object[] { string.Empty };
        }
    }
}
