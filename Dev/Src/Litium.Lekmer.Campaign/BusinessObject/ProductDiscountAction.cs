﻿using System;
using System.Collections.Generic;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class ProductDiscountAction : ProductAction, IProductDiscountAction
	{
		private int _currencyId;
		private Dictionary<int, decimal> _productDiscountPrices;

		public int CurrencyId
		{
			get { return _currencyId; }
			set
			{
				CheckChanged(_currencyId, value);
				_currencyId = value;
			}
		}

		public Dictionary<int, decimal> ProductDiscountPrices
		{
			get { return _productDiscountPrices; }
			set
			{
				CheckChanged(_productDiscountPrices, value);
				_productDiscountPrices = value;
			}
		}

		public override bool Apply(IUserContext context, IProduct product)
		{
			if (context.Channel.Currency.Id != CurrencyId)
			{
				return false;
			}

			decimal productDiscountPrice;
			if (!ProductDiscountPrices.TryGetValue(product.Id, out productDiscountPrice))
			{
				return false;
			}

			if (productDiscountPrice >= product.CampaignInfo.Price.IncludingVat)
			{
				return false;
			}

			var priceIncludingVat = productDiscountPrice;
			var priceExcludingVat = priceIncludingVat / (1 + product.Price.VatPercentage / 100);
			product.CampaignInfo.Price = new Price(priceIncludingVat, priceExcludingVat);
			return true;
		}

		public override object[] GetInfoArguments()
		{
			return new object[]{};
		}
	}
}
