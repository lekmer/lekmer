using System;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerCartCampaignInfo : CartCampaignInfo, ILekmerCartCampaignInfo
	{
		public decimal? VoucherDiscount { get; set; }
		public VoucherValueType VoucherDiscountType { get; set; }
	}
}