using System;
using Litium.Lekmer.Core;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
    [Serializable]
    public class VoucherDiscountAction : CartAction, IVoucherDiscountAction
    {
        public override bool Apply(IUserContext context, ICartFull cart)
        {
            var lekmerUserContext = (ILekmerUserContext)context;
            var voucher = lekmerUserContext.Voucher;
            if (voucher != null && voucher.IsValid)
            {
                var campaignInfo = (ILekmerCartCampaignInfo)cart.CampaignInfo;
                campaignInfo.VoucherDiscount = voucher.DiscountValue;
                campaignInfo.VoucherDiscountType = voucher.ValueType;
                return true;
            }
            return false;  
        }

        public override object[] GetInfoArguments()
        {
            string discountType = FixedDiscount ? "fixed" : "percentage";
            return new object[] { discountType, DiscountValue };
        }

        public decimal DiscountValue { get; set; }
        public bool FixedDiscount { get; set; }
        public bool PercentageDiscount { get; set; }
    }
}