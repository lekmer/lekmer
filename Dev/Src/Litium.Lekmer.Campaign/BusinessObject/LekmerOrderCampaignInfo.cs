using System;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerOrderCampaignInfo : OrderCampaignInfo, ILekmerOrderCampaignInfo
	{
		public decimal? VoucherDiscount { get; set; }
	}
}