using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface IVoucherDiscountAction : ICartAction
	{
        bool FixedDiscount { get; set; }
        bool PercentageDiscount { get; set; }
        decimal DiscountValue { get; set; }
	}
}