﻿using System.Collections.Generic;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface IProductDiscountAction : IProductAction
	{
		int CurrencyId { get; set; }
		Dictionary<int, decimal> ProductDiscountPrices { get; set; }
	}
}