﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
    public class VoucherConditionRepository
    {
        protected virtual DataMapperBase<IVoucherCondition> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IVoucherCondition>(dataReader);
        }

        /// <summary>
        /// Gets cart value condition by Id
        /// </summary>
        /// <param name="id">Condition Id</param>
        /// <returns>IVoucherCondition object</returns>
        public virtual IVoucherCondition GetById(int id)
        {
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Id", id, SqlDbType.Int)
			};
            var dbSettings = new DatabaseSetting("VoucherConditionrepository.GetById");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherConditionGetById]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        /// <summary>
        /// Saves cart value condition
        /// </summary>
        public virtual void Save(IVoucherCondition condition)
        {
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", condition.Id, SqlDbType.Int),
			};
            var dbSettings = new DatabaseSetting("VoucherConditionrepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionSave]", parameters, dbSettings);
        }

        /// <summary>
        /// Deletes cart value condition
        /// </summary>
        public virtual void Delete(int id)
        {
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", id, SqlDbType.Int),
			};
            var dbSettings = new DatabaseSetting("VoucherConditionrepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionDelete]", parameters, dbSettings);
        }

        /// <summary>
        /// Saves cart value
        /// </summary>
        public virtual void BatchInsert(int conditionId, int batchId)
        {
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@BatchId",batchId, SqlDbType.Int)
			};
            var dbSettings = new DatabaseSetting("VoucherConditionrepository.BatchInsert");
            new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionBatcheInsert]", parameters, dbSettings);
        }

        public virtual Collection<int> GetBatchesByConditionId(int id)
        {
            var batchIds = new Collection<int>();
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@Id", id, SqlDbType.Int)
			};
            var dbSettings = new DatabaseSetting("VoucherConditionrepository.GetBatchesByConditionId");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherConditionGetBatcheIdsByConditionId]", parameters, dbSettings))
            {
                while (dataReader.Read())
                {
                    batchIds.Add(dataReader.GetInt32(0));
                }
            }
            return batchIds;
        }

        /// <summary>
        /// Deletes all cart values for appropriate condition
        /// </summary>
        public virtual void DeleteAllBatchIds(int id)
        {
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", id, SqlDbType.Int),
			};
            var dbSettings = new DatabaseSetting("VoucherConditionrepository.DeleteAllBatchIds");
            new DataHandler().ExecuteCommand("[lekmer].[pVoucherConditionDeleteAll]", parameters, dbSettings);
        }
    }
}