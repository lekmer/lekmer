﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class ProductDiscountActionRepository
	{
		protected DataMapperBase<IProductDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductDiscountAction>(dataReader);
		}

		public IProductDiscountAction GetById(int actionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public Dictionary<int, decimal> GetItemsSecure(int actionId)
		{
			var productDiscounts = new Dictionary<int, decimal>();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.GetItemsSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductDiscountActionItemGetAllSecure]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productDiscounts.Add(dataReader.GetInt32(0), dataReader.GetDecimal(1));
				}
			}
			return productDiscounts;
		}

		public Dictionary<int, decimal> GetItems(int channelId, int? customerId, int actionId)
		{
			var productDiscounts = new Dictionary<int, decimal>();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CustomerId", customerId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.GetItems");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductDiscountActionItemGetAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productDiscounts.Add(dataReader.GetInt32(0), dataReader.GetDecimal(1));
				}
			}
			return productDiscounts;
		}

		public void Save(IProductDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CurrencyId", action.CurrencyId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDiscountActionSave]", parameters, dbSettings);
		}

		public void DeleteItems(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.DeleteItems");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDiscountActionItemDeleteAll]", parameters, dbSettings);
		}

		public void InsertItem(int actionId, int productId, decimal discountPrice)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DiscountPrice", discountPrice, SqlDbType.Decimal)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.InsertItem");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pProductDiscountActionItemInsert]", parameters, dbSettings);
		}

		public void Delete(int actionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int),
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDiscountActionDelete]", parameters, dbSettings);
		}
	}
}
