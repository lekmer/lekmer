using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class VoucherDiscountActionRepository
	{
		protected DataMapperBase<IVoucherDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IVoucherDiscountAction>(dataReader);
		}

		public IVoucherDiscountAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
        public  void Save(IVoucherDiscountAction action)
        {
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", action.Id, SqlDbType.Int),
			        ParameterHelper.CreateParameter("@DiscountValue", action.DiscountValue, SqlDbType.Decimal),			
                    ParameterHelper.CreateParameter("@Fixed", action.FixedDiscount, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@Percentage", action.PercentageDiscount, SqlDbType.Bit)
				};
            var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pVoucherActionSave]", parameters, dbSettings);
        }

        public virtual void Delete(int id)
        {
            IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", id, SqlDbType.Int),
			};
            var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pVoucherActionDelete]", parameters, dbSettings);
        }
	}
}