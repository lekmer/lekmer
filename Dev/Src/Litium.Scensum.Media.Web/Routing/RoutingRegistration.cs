using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Media.Web
{
	public static class RoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			routes.Add(
				new ImageRoute(
					@"{id}/{sizecommonname}/{whatever}.{extension}", null,
					new RouteValueDictionary { { "id", @"\d+" } },
					new RouteHandler("~/Image.ashx")));
			routes.Add(
				new MediaRoute(
					@"{id}/{whatever}.{extension}", null,
					new RouteValueDictionary { { "id", @"\d+" } },
					new RouteHandler("~/MediaItem.ashx")));
		}
	}
}