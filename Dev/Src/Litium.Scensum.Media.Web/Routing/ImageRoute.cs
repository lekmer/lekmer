using System;
using System.Globalization;
using System.Web;
using System.Web.Routing;

namespace Litium.Scensum.Media.Web
{
	public class ImageRoute : Route
	{
		public ImageRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public ImageRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
			: base(url, defaults, routeHandler)
		{
		}

		public ImageRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
						  IRouteHandler routeHandler)
			: base(url, defaults, constraints, routeHandler)
		{
		}

		public ImageRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
						  RouteValueDictionary dataTokens, IRouteHandler routeHandler)
			: base(url, defaults, constraints, dataTokens, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			if (data == null) return null;

			int urlMediaId = int.Parse(data.Values["id"].ToString(), CultureInfo.InvariantCulture);
			string urlExtension = data.Values["extension"].ToString();
			string urlSizeCommonName = data.Values["sizecommonname"].ToString();

			httpContext.Items["MediaId"] = urlMediaId;
			httpContext.Items["Extension"] = urlExtension;
			httpContext.Items["ImageSizeCommonName"] = urlSizeCommonName;

			return data;
		}
	}
}