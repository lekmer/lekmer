using System;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Scensum.Media.Web
{
	public class MediaItem : IHttpHandler
	{
		private const int _bufferSizeBytes = 8 * 1024;

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var mediaId = (int) context.Items["MediaId"];
			var extension = (string) context.Items["Extension"];

			LoadMedia(context, mediaId, extension);
		}

		private static void LoadMedia(HttpContext context, int mediaId, string extension)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (string.IsNullOrEmpty(extension)) throw new ArgumentNullException("extension");

			string etag = mediaId.ToString(CultureInfo.InvariantCulture);

			string ifNoneMatchHeader = context.Request.Headers["If-None-Match"];
			if (ifNoneMatchHeader != null && ifNoneMatchHeader.Equals(etag))
			{
				context.Response.StatusCode = 304;
				context.Response.StatusDescription = "Not modified";
				return;
			}

			var mediaItemService = IoC.Resolve<IMediaItemService>();

			string mime;
			using (Stream mediaStream = mediaItemService.LoadMedia(mediaId, extension, out mime))
			{
				if (mediaStream == null)
				{
					throw new HttpException(404, "Page not found");
				}

				context.Response.AppendHeader("ETag", etag);
				context.Response.ContentType = mime;

				StreamAppender.AppendToStream(mediaStream, context.Response.OutputStream, _bufferSizeBytes);
			}
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}