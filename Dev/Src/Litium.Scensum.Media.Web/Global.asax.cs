using System;
using System.Diagnostics.CodeAnalysis;
using System.Web;
using System.Web.Routing;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;
using log4net.Config;

namespace Litium.Scensum.Media.Web
{
	public class Global : HttpApplication
	{
		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Start(object sender, EventArgs e)
		{
			RoutingRegistration.RegisterRoutes(RouteTable.Routes);

			CacheUpdateCleaner.CacheUpdateFeed = new CacheUpdateFeed();
			CacheUpdateCleaner.StartWorker();

			XmlConfigurator.Configure();

			Initializer.InitializeAll();
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_Start(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_BeginRequest(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Error(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_End(object sender, EventArgs e)
		{
		}

		[SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_End(object sender, EventArgs e)
		{
		}
	}
}