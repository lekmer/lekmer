using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Scensum.Media.Web
{
	[DebuggerStepThrough]
	public class Image : IHttpHandler
	{
		private const int _bufferSizeBytes = 8*1024;

		public void ProcessRequest(HttpContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var mediaId = (int) context.Items["MediaId"];
			var extension = (string) context.Items["Extension"];
			var imageSizeCommonName = (string) context.Items["ImageSizeCommonName"];

			LoadImage(context, mediaId, extension, imageSizeCommonName);
		}

		private static void LoadImage(HttpContext context, int mediaId, string extension, string imageSizeCommonName)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (string.IsNullOrEmpty(extension)) throw new ArgumentNullException("extension");
			if (string.IsNullOrEmpty(imageSizeCommonName)) throw new ArgumentNullException("imageSizeCommonName");

			string etag = mediaId.ToString(CultureInfo.InvariantCulture) + "-" + imageSizeCommonName;
			var lastModified = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);

			if (HasMatchingETag(context, etag))
			{
				SendNotModifiedHeader(context);
				return;
			}

			if (HasMatchingLastModified(context, lastModified))
			{
				SendNotModifiedHeader(context);
				return;
			}

			var imageService = IoC.Resolve<IImageService>();

			string mime;
			using (Stream imageStream = imageService.LoadImage(mediaId, extension, imageSizeCommonName, out mime))
			{
				if (imageStream == null)
				{
					throw new HttpException(404, "Page not found");
				}

				context.Response.Cache.SetLastModified(lastModified);
				context.Response.Cache.SetETag(etag);
				context.Response.ContentType = mime;

				StreamAppender.AppendToStream(imageStream, context.Response.OutputStream, _bufferSizeBytes);
			}
		}

		private static bool HasMatchingETag(HttpContext context, string etag)
		{
			string ifNoneMatchHeader = context.Request.Headers["If-None-Match"];
			return ifNoneMatchHeader != null && ifNoneMatchHeader.Equals(etag);
		}

		private static bool HasMatchingLastModified(HttpContext context, DateTime lastModified)
		{
			string lastModifiedHeader = context.Request.Headers["If-Modified-Since"];
			if (lastModifiedHeader == null) return false;

			DateTime clientLastModified;
			if (!DateTime.TryParse(lastModifiedHeader, CultureInfo.InvariantCulture, DateTimeStyles.None, out clientLastModified))
			{
				return false;
			}

			return clientLastModified.ToUniversalTime() == lastModified;
		}

		private static void SendNotModifiedHeader(HttpContext context)
		{
			context.Response.StatusCode = 304;
			context.Response.StatusDescription = "Not modified";
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}