using System.Globalization;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Review.Web
{
	public class BlockProductReviewEntityMapper : BlockEntityMapper<IBlockProductReview>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductReview item)
		{
			fragment.AddVariable("Block.RowCount", item.RowCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}