using Litium.Scensum.Core.Web;

namespace Litium.Scensum.Review.Web
{
	public interface IReviewPagingControl : IPagingControl
	{
		string ReviewAddedQueryStringParameterName { get; set; }
	}
}