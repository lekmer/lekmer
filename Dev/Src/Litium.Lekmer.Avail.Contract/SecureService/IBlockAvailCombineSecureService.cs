﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Avail
{
	public interface IBlockAvailCombineSecureService
	{
		IBlockAvailCombine Create();

		int Save(ISystemUserFull systemUserFull, IBlockAvailCombine block);

		IBlockAvailCombine GetById(int id);
	}
}
