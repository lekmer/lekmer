﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Avail
{
	public interface IBlockAvailCombine : IBlock
	{
		int ColumnCount { get; set; }
		int RowCount { get; set; }
		bool UseCartPredictions { get; set; }
		bool UseClickStreamPredictions { get; set; }
		bool UseLogPurchase { get; set; }
		bool UsePersonalPredictions { get; set; }
		bool UseProductSearchPredictions { get; set; }
		bool UseProductsPredictions { get; set; }
		bool UseProductsPredictionsFromClicksProduct { get; set; }
		bool UseProductsPredictionsFromClicksCategory { get; set; }
		bool UseLogClickedOn { get; set; }
        bool GetClickHistoryFromCookie { get; set; }    
	}
}
