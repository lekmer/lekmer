﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.ServiceCenter.Setting
{
    public class ServiceCenterSetting:SettingBase
    {
        #region Singleton

		private static readonly ServiceCenterSetting _instance = new ServiceCenterSetting();

        private ServiceCenterSetting()
		{
		}

		public static ServiceCenterSetting Instance
		{
			get { return _instance; }
		}

		#endregion

        private const string _groupName = "ServiceCenterSetting";

		protected override string StorageName
		{
            get { return "ServiceCenterSetting"; }
		}

		public string WebServiceUrl
		{
			get { return GetString(_groupName, "WebServiceUrl"); }
		}
    }
}
