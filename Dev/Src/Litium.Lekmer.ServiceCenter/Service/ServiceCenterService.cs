﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Lekmer.ServiceCenter.Contract.Service;
using Litium.Lekmer.ServiceCenter.ServiceCenter;
using Litium.Lekmer.ServiceCenter.Setting;
using log4net;
using CSMCategory = Incordia.Enterprise.CustomerService.WebService.CSMCategory;
using CSMCustomerCase = Incordia.Enterprise.CustomerService.WebService.CSMCustomerCase;
using CSMCustomerCaseDetails = Incordia.Enterprise.CustomerService.WebService.CSMCustomerCaseDetails;
using WebService = Incordia.Enterprise.CustomerService.WebService;

namespace Litium.Lekmer.ServiceCenter.Service
{
    public class ServiceCenterService : IServiceCenterService
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ServiceCenterService));

		/// <summary>
		/// The web service
		/// </summary>
		private readonly CustomerServiceWebService service;

        public ServiceCenterService()
		{
			service = new CustomerServiceWebService();
            service.Url = ServiceCenterSetting.Instance.WebServiceUrl;
		}

		public const int cHIDDEN = 1;
		public const int cPRIVATE = 2;
		public const int cPUBLIC = 4;

		/// <summary>
		/// Gets a list of available public categories for current channel
		/// </summary>
		/// <param name="display">Display property. (Bitwise comparison)
		/// 1 - Hidden
		/// 2 - Private
		/// 4 - Public
		/// 7 - All</param>
		/// <param name="siteName">The common name for the application/channel</param>
		/// <returns>A List<CSMCategory> with categories/returns>
		/// <author>Johan Liljeros</author>
		/// <date>2007-11-26</date>
		public List<CSMCategory> GetCSMCategories(int display, string siteName)
		{
			ServiceCenter.CSMCategory[] categories = service.ListCategoryBySiteName(display, siteName);

			List<CSMCategory> results = new List<CSMCategory>();

			foreach (ServiceCenter.CSMCategory category in categories)
			{
				CSMCategory cat = new CSMCategory();
				cat.Caption = category.Caption;
				cat.CategoryID = category.CategoryID;
				cat.Display = category.Display;
				cat.QueueID = category.QueueID;
				cat.ReadOnly = category.ReadOnly;
				cat.SiteID = category.SiteID;
				results.Add(cat);
			}
			return results;
		}

		///Overloaded methods for save case

		#region Save case

        /// <summary>
		/// Saves a case
		/// </summary>
		/// <param name="csmCaseId"></param>
		/// <param name="categoryId"></param>
		/// <param name="firstName">First name entered by the visitor, required</param>
		/// <param name="lastName">Last name entered by the visitor, required</param>
		/// <param name="email">E-Mail entered by the visitor, required</param>
		/// <param name="orderId">Order number entered by the visitor, optional</param>
		/// <param name="message">Message entered by the visitor, required</param>
		/// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
		/// <author>Johan Liljeros</author>
		/// <date>2007-11-26</date>
		/// <param name="channelId"></param>
		/// <param name="siteName"></param>
		public string SaveCase(int? csmCaseId, int categoryId, string firstName, string lastName, string email, int? orderId,
							   string civicOcrNumber, string title, string message, int channelId, string siteName)
        {
			return SaveCase(csmCaseId, categoryId, firstName, lastName, email, null, orderId, civicOcrNumber, title, message, channelId, siteName);
        }

		/// <summary>
		/// Saves a case
		/// </summary>
		/// <param name="csmCaseId"></param>
		/// <param name="categoryId"></param>
		/// <param name="firstName">First name entered by the visitor, required</param>
		/// <param name="lastName">Last name entered by the visitor, required</param>
		/// <param name="email">E-Mail entered by the visitor, required</param>
		/// <param name="orderId">Order number entered by the visitor, optional</param>
		/// <param name="message">Message entered by the visitor, required</param>
		/// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
		/// <author>Johan Liljeros</author>
		/// <date>2007-11-26</date>
		/// <param name="channelId"></param>
		/// <param name="siteName"></param>
        public string SaveCase(int? csmCaseId, int categoryId, string firstName, string lastName, string email, int? customerId, 
                               int? orderId, string civicOcrNumber, string title, string message, int channelId, string siteName)
		{
			CSMCase newCase = new CSMCase();
		    if (!String.IsNullOrEmpty(title))
				title = title.Trim();
			if (String.IsNullOrEmpty(title))
			{
				newCase.CaseTitle = message.Trim().Length > 36 ? message.Substring(0, 35) + "..." : message;
			}
			else
			{
				newCase.CaseTitle = title;
			}
			newCase.CaseCategoryId = categoryId;
			newCase.CaseMessage = message;
			newCase.SenderEmail = email;
			newCase.SenderFirstName = firstName;
			newCase.SenderLastName = lastName;
			newCase.SenderCustomerId = customerId;
            newCase.SenderOrderNo = orderId;
            //newCase.SiteId = channelId;   SiteId and ChannelId are not the same id
			newCase.CivicOCRNumber = civicOcrNumber;
			newCase.SiteName = siteName;
			try
			{
				return service.CreateServiceCase(newCase);
			}
			catch (Exception ex)
			{
				if (log.IsErrorEnabled) 
				{
					log.Error(
						string.Format(
							"Error occured while saving case; csmCaseId {0}, categoryId {1}, firstName {2}, lastName {3}, email {4}, orderId {5}, message {6}, channelId {7}, siteName {8}. Message: {9}",
							csmCaseId, categoryId, firstName, lastName, email, orderId, message, channelId, siteName, ex.Message), ex);
				}
				throw;
			}
		}

		/// <summary>
		/// Saves a case
		/// </summary>
		/// <param name="categoryId"></param>
		/// <param name="firstName">First name entered by the visitor, required</param>
		/// <param name="lastName">Last name entered by the visitor, required</param>
		/// <param name="email">E-Mail entered by the visitor, required</param>
		/// <param name="customerId">Customer number entered by the visitor, optional</param>
		/// <param name="message">Message entered by the visitor, required</param>
		/// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
		/// <author>Johan Liljeros</author>
		/// <date>2007-11-26</date>
		/// <param name="channelId"></param>
		/// <param name="siteName"></param>
		public string SaveCase(int categoryId, string firstName, string lastName, string email, int? customerId,
							   string civicOcrNumber, string title, string message, int channelId, string siteName)
		{
			return SaveCase(categoryId, firstName, lastName, email, customerId, civicOcrNumber, title, message, channelId, siteName);
		}

		/// <summary>
		/// Saves a case
		/// </summary>
		/// <param name="categoryId"></param>
		/// <param name="firstName">First name entered by the visitor, required</param>
		/// <param name="lastName">Last name entered by the visitor, required</param>
		/// <param name="email">E-Mail entered by the visitor, required</param>
		/// <param name="message">Message entered by the visitor, required</param>
		/// <returns>The ticket number generated at creation or the existing ticket number at update</returns>
		/// <author>Johan Liljeros</author>
		/// <date>2007-11-26</date>
		/// <param name="channelId"></param>
		/// <param name="siteName"></param>
		public string SaveCase(int categoryId, string firstName, string lastName, string email, string civicOcrNumber, string title, string message, int channelId,
		                       string siteName)
		{
			return SaveCase(categoryId, firstName, lastName, email, null, civicOcrNumber, title, message, channelId, siteName);
		}

		#endregion

		/// <summary>
		/// Gets a specific
		/// </summary>
		/// <param name="ticketNumber">The ticket number for the specific case</param>
		/// <returns>The case as CustomerServiceWebService</returns>
		/// <author>Johan Liljeros</author>
		/// <date>2007-11-26</date>
        public CSMCustomerCaseDetails GetCaseDetails(string ticketNumber, string email, string siteName)
		{
			ServiceCenter.CSMCustomerCaseDetails details = service.GetServiceCaseByTicket(ticketNumber, siteName, email);
			if (details != null)
			{
				CSMCustomerCaseDetails det = new CSMCustomerCaseDetails();
				det.CreationDate = details.CreationDate;
				det.CustomerID = details.CustomerID;

				foreach (CSMCaseMessage message in details.Messages)
				{
					WebService.CSMCaseMessage mess = new WebService.CSMCaseMessage();
					mess.CreationDate = message.CreationDate;
					mess.Message = message.Message;
					mess.ReasonCode = message.ReasonCode;
					mess.Type = message.Type;
					det.Messages.Add(mess);
				}

				det.OrderNumber = details.OrderNumber;
				det.Status = details.Status;
				det.Ticket = details.Ticket;

				return det;
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Gets a list of cases for a specific e-mail address
		/// </summary>
		/// <param name="email">The e-mail</param>
		/// <returns>A List<CustomerServiceWebService> of cases</returns>
		public List<CSMCustomerCase> GetCasesByEMail(string email, string siteName)
		{
			ServiceCenter.CSMCustomerCase[] csmCases = service.ListServiceCaseByCustomer(email, siteName);

			List<CSMCustomerCase> results = new List<CSMCustomerCase>();
			foreach (ServiceCenter.CSMCustomerCase _case in csmCases)
			{
				CSMCustomerCase c = new CSMCustomerCase();
				c.CaseName = _case.CaseName;
				c.CategoryID = _case.CategoryID;
				c.CreationDate = _case.CreationDate;
				c.Ticket = _case.Ticket;
				results.Add(c);
			}
			return results;
		}


		public List<CSMCustomerCaseDetails> GetCases(string email, string channelName)
		{
		    ServiceCenter.CSMCustomerCaseDetails[] details = service.ListServiceCaseByCustomer(email, channelName);
			List<CSMCustomerCaseDetails> result = new List<CSMCustomerCaseDetails>();

			foreach (ServiceCenter.CSMCustomerCaseDetails detail in details)
			{
				CSMCustomerCaseDetails det = new CSMCustomerCaseDetails();

				det.Ticket = detail.Ticket;
				det.CreationDate = detail.CreationDate;
				det.CustomerID = detail.CustomerID;
				det.OrderNumber = detail.OrderNumber;
				det.Status = detail.Status;
				det.IsUnread = detail.IsUnread;

				foreach (CSMCaseMessage m in detail.Messages)
				{
					WebService.CSMCaseMessage mess = new WebService.CSMCaseMessage();
					mess.CreationDate = m.CreationDate;
					mess.Message = m.Message;
					mess.ReasonCode = m.ReasonCode;
					mess.Type = m.Type;
					mess.Direction = m.Direction;

					det.Messages.Add(mess);
				}
				result.Add(det);
			}
			return result;
		}

		public CSMCustomerCaseDetails GetCase(string ticket, string customerEmail, string siteName)
		{
			//NOTE: The class returned from the webservice is not the same as the client classes as far as .NET thinks.
			// These classes needs to be read and converted to a class that can be used by the web layer.
			//Todo: The web service call should actually be refactored to return XML or to be accessed directly via remoting.
			ServiceCenter.CSMCustomerCaseDetails _details = service.GetServiceCaseByTicket(ticket, siteName, customerEmail);
			if (_details != null)
			{
				CSMCustomerCaseDetails det = new CSMCustomerCaseDetails();
				det.CreationDate = _details.CreationDate;
				det.CustomerID = _details.CustomerID;
				det.OrderNumber = _details.OrderNumber;
				det.Status = _details.Status;
				det.Ticket = _details.Ticket;

				foreach (CSMCaseMessage m in _details.Messages)
				{
					WebService.CSMCaseMessage mess = new WebService.CSMCaseMessage();
					mess.CreationDate = m.CreationDate;
					mess.Message = m.Message;
					mess.ReasonCode = m.ReasonCode;
					mess.Type = m.Type;
					mess.Direction = m.Direction;

					det.Messages.Add(mess);
				}
				return det;
			}
			else
			{
				return null;
			}
		}

        public CSMCustomerCaseDetails GetCaseByGuid(string guid, string siteName)
        {
            //NOTE: The class returned from the webservice is not the same as the client classes as far as .NET thinks.
            // These classes needs to be read and converted to a class that can be used by the web layer.
            //Todo: The web service call should actually be refactored to return XML or to be accessed directly via remoting.
            ServiceCenter.CSMCustomerCaseDetails _details = service.GetServiceCaseByGuid(guid, siteName);
            if (_details != null)
            {
                CSMCustomerCaseDetails det = new CSMCustomerCaseDetails();
                det.CreationDate = _details.CreationDate;
                det.CustomerID = _details.CustomerID;
                det.OrderNumber = _details.OrderNumber;  //CDON ordernumber always int
                det.Status = _details.Status;
                det.Ticket = _details.Ticket;

                foreach (CSMCaseMessage m in _details.Messages)
                {
                    WebService.CSMCaseMessage mess = new WebService.CSMCaseMessage();
                    mess.CreationDate = m.CreationDate;
                    mess.Message = m.Message;
                    mess.ReasonCode = m.ReasonCode;
                    mess.Type = m.Type;
                    mess.Direction = m.Direction;

                    det.Messages.Add(mess);
                }
                return det;
            }
            else
            {
                return null;
            }
        }

		public void AddMessageToCase(string ticket, string reply)
		{
			service.AddMessageToServiceCase(ticket, reply, true);
		}
    }
}
