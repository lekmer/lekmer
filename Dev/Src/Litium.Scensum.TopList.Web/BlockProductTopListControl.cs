﻿using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.TopList.Web
{
	public class BlockProductTopListControl : BlockControlBase<IBlockTopList>
	{
		private readonly IBlockTopListService _blockTopListService;
		private readonly ITopListProductService _topListProductService;
		private IPagingControl _pagingControl;
		private ProductCollection _products;

		public BlockProductTopListControl(
			ITemplateFactory templateFactory,
			IBlockTopListService blockTopListService,
			ITopListProductService topListProductService)
			: base(templateFactory)
		{
			_blockTopListService = blockTopListService;
			_topListProductService = topListProductService;
		}

		protected override IBlockTopList GetBlockById(int blockId)
		{
			return _blockTopListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			_pagingControl.TotalCount = _products.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			var fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddEntity(Block);
			fragmentHead.AddVariable("Param.ProductIds", RenderProductIdsArrayParam());
			string head =
				(fragmentHead.Render() ?? string.Empty) +
				(pagingContent.Head ?? string.Empty);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
            fragmentContent.AddVariable("Block.Title", Block.Title, VariableEncoding.None);
            fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			return new BlockContent(
				head,
				fragmentContent.Render());
		}

		private void Initialize()
		{
			_pagingControl = CreatePagingControl();
			_products = _topListProductService.GetAllByBlock(
				UserContext.Current, Block, _pagingControl.SelectedPage, _pagingControl.PageSize);
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			           	{
			           		Items = _products,
			           		ColumnCount = Block.ColumnCount,
			           		RowCount = Block.RowCount,
			           		Template = Template,
			           		ListFragmentName = "ProductList",
			           		RowFragmentName = "ProductRow",
			           		ItemFragmentName = "Product",
			           		EmptySpaceFragmentName = "EmptySpace"
			           	};
			return grid.Render();
		}

		private IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.ColumnCount*Block.RowCount;
			pagingControl.Initialize();
			return pagingControl;
		}
		private string RenderProductIdsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			for (int i = 0; i < _products.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", _products[i].Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", _products[i].Id));
				}
			}
			return itemBuilder.ToString();
		}
	}
}