using System.Globalization;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.TopList.Web
{
	public class BlockProductTopListEntityMapper : BlockEntityMapper<IBlockTopList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockTopList item)
		{
			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.RowCount", item.RowCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.ColumnCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}