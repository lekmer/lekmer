﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BlockBrandListBrandCollectionCache :
		ScensumCacheBase<BlockBrandListBrandCollectionKey, BrandCollection>
	{
		#region Singleton

		private static readonly BlockBrandListBrandCollectionCache _instance =
			new BlockBrandListBrandCollectionCache();

		private BlockBrandListBrandCollectionCache()
		{
		}

		public static BlockBrandListBrandCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class BlockBrandListBrandCollectionKey : ICacheKey
	{
		public BlockBrandListBrandCollectionKey(int channelId, int blockId, int pageNumber, int pageSize)
		{
			ChannelId = channelId;
			BlockId = blockId;
			PageNumber = pageNumber;
			PageSize = pageSize;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public int PageNumber { get; set; }

		public int PageSize { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture) + "-" +
					PageNumber.ToString(CultureInfo.InvariantCulture) + "-" +
					PageSize.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}