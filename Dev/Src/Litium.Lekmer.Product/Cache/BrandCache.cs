﻿using Litium.Framework.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
    public sealed class BrandCache : ScensumCacheBase<BrandKey, IBrand>
    {
        #region Singleton

        private static readonly BrandCache _instance = new BrandCache();

        private BrandCache()
        {
        }

        public static BrandCache Instance
        {
            get { return _instance; }
        }

        #endregion
    }

    public class BrandKey : ICacheKey
    {
        public BrandKey(int brandId)
        {
            BrandId = brandId;
        }

        public int BrandId { get; set; }

        public string Key
        {
            get { return string.Format("BrandId_{0}", BrandId); }
        }
    }
}