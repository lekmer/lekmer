﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
    public sealed class BlockImageRotatorCache : ScensumCacheBase<BlockImageRotatorKey, IBlockImageRotator>
	{
        private static readonly BlockImageRotatorCache _instance = new BlockImageRotatorCache();

        private BlockImageRotatorCache()
		{
		}

        public static BlockImageRotatorCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
                Remove(new BlockImageRotatorKey(channel.Id, blockId));
			}
		}
	}

	public class BlockImageRotatorKey : ICacheKey
	{
        public BlockImageRotatorKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}