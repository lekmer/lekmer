﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class BrandListCache : ScensumCacheBase<BrandListKey, BrandCollection>
	{
		private static readonly BrandListCache _brandCache = new BrandListCache();

		private BrandListCache()
		{
		}

		public static BrandListCache Instance
		{
			get { return _brandCache; }
		}
	}

	public class BrandListKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public BrandListKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}