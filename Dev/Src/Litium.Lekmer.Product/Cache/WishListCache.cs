﻿using System;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
    public sealed class WishListCache : ScensumCacheBase<WishListCacheKey, IWishList>
    {
        private static readonly WishListCache _instance = new WishListCache();

        private WishListCache()
        {
        }

        public static WishListCache Instance
        {
            get { return _instance; }
        }

        public void Remove(Guid wishListKey)
        {
            Remove(new WishListCacheKey(wishListKey));
        }
    }

    public class WishListCacheKey : ICacheKey
    {
        public WishListCacheKey(Guid wishListKey)
        {
            WishListKey = wishListKey;
        }

        public Guid WishListKey { get; set; }

        public string Key
        {
            get
            {
                return WishListKey.ToString();
            }
        }
    }
}