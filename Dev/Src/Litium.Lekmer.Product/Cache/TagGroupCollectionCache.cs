using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class TagGroupCollectionCache : ScensumCacheBase<TagGroupKey, Collection<ITagGroup>>
	{
		#region Singleton

		private static readonly TagGroupCollectionCache _instance = new TagGroupCollectionCache();

		private TagGroupCollectionCache()
		{
		}

		public static TagGroupCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class TagGroupKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public TagGroupKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}