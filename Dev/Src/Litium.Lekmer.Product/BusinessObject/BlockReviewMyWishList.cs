﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class BlockReviewMyWishList : BlockBase, IBlockReviewMyWishList
    {
        private int _columnCount;
        private int _rowCount;
        /// <summary>
        /// Number of columns with product in a block.
        /// Value must be greater then 0.
        /// </summary>
        public int ColumnCount
        {
            get { return _columnCount; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("ColumnCount can't be less then '1'.");
                }
                CheckChanged(_columnCount, value);
                _columnCount = value;
            }
        }

        /// <summary>
        /// Number of rows with product in a block.
        /// Value must be greater then 0.
        /// </summary>
        public int RowCount
        {
            get { return _rowCount; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("RowCount can't be less then '1'.");
                }
                CheckChanged(_rowCount, value);
                _rowCount = value;
            }
        }
    }
}
