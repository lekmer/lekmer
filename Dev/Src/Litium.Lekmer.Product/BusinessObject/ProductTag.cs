namespace Litium.Lekmer.Product
{
	public class ProductTag : IProductTag
	{
		public int TagId { get; set; }
		public int ProductId { get; set; }
	}
}