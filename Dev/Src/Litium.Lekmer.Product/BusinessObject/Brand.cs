﻿using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class Brand : BusinessObjectBase, IBrand
    {
        private int _id;
        private string _title;
        private string _description;
        private string _url;
        private int? _mediaId;
        private IImage _image;
        private string _erpId;
    	private int? _contentNodeId;

        public int? MediaId
        {
            get { return _mediaId; }
            set
            {
                CheckChanged(_mediaId, value);
                _mediaId = value;
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                CheckChanged(_description, value);
                _description = value;
            }
        }

        public string ExternalUrl
        {
            get { return _url; }
            set
            {
                CheckChanged(_url, value);
                _url = value;
            }
        }

        public int Id
        {
            get { return _id; }
            set
            {
                CheckChanged(_id, value);
                _id = value;
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                CheckChanged(_title, value);
                _title = value;
            }
        }

        public IImage Image
        {
            get { return _image; }
            set
            {
                CheckChanged(_image, value);
                _image = value;
            }
        }

        public string ErpId
        {
            get { return _erpId; }
            set
            {
                CheckChanged(_erpId, value);
                _erpId = value;
            }
        }

		public int? ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}
    }
}