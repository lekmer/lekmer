﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Contract;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class LekmerProductView : LekmerProduct, ILekmerProductView
    {
        private string _description;
        private int? _parentContentNodeId;
        private int? _templateContentNodeId;
    	private int _ageFromMonth;
		private int _ageToMonth;
        private string _measurement;
        private int? _numberOfBatteries;
        private bool _isBatteryIncluded;
        private DateTime? _expectedBackInStock;
    	private IBatteryType _batteryType;
		private ISizeDeviation _sizeDeviation;

        public string Description
        {
            get { return _description; }
            set
            {
                CheckChanged(_description, value);
                _description = value;
            }
        }

        public int? ParentContentNodeId
        {
            get { return _parentContentNodeId; }
            set
            {
                CheckChanged(_parentContentNodeId, value);
                _parentContentNodeId = value;
            }
        }

        public int? TemplateContentNodeId
        {
            get { return _templateContentNodeId; }
            set
            {
                CheckChanged(_templateContentNodeId, value);
                _templateContentNodeId = value;
            }
        }

		public int AgeFromMonth
		{
			get { return _ageFromMonth; }
			set
			{
				CheckChanged(_ageFromMonth, value);
				_ageFromMonth = value;
			}
		}

		public int AgeToMonth
		{
			get { return _ageToMonth; }
			set
			{
				CheckChanged(_ageToMonth, value);
				_ageToMonth = value;
			}
		}

        public string Measurement
        {
            get { return _measurement; }
            set
            {
                CheckChanged(_measurement, value);
                _measurement = value;
            }
        }

        public int? NumberOfBatteries
        {
            get { return _numberOfBatteries; }
            set
            {
                CheckChanged(_numberOfBatteries, value);
                _numberOfBatteries = value;
            }
        }

        public bool IsBatteryIncluded
        {
            get { return _isBatteryIncluded; }
            set
            {
                CheckChanged(_isBatteryIncluded, value);
                _isBatteryIncluded = value;
            }
        }

        public Collection<IIcon> Icons
        {
            get;
            set;
        }

        public DateTime? ExpectedBackInStock
        {
            get { return _expectedBackInStock; }
            set
            {
                CheckChanged(_expectedBackInStock, value);
                _expectedBackInStock = value;
            }
        }

    	public IBatteryType BatteryType
    	{
			get { return _batteryType; }
			set
			{
				CheckChanged(_batteryType, value);
				_batteryType = value;
			}
    	}

		public ISizeDeviation SizeDeviation
		{
			get { return _sizeDeviation; }
			set
			{
				CheckChanged(_sizeDeviation, value);
				_sizeDeviation = value;
			}
		}

		public Collection<IProductSize> ProductSizes
		{
			get;
			set;
		}
        public IProductSize DefaultProductSize
        {
            get;
            set;
        }

		public Collection<ITagGroup> TagGroups
		{
			get;
			set;
		}
    }
}