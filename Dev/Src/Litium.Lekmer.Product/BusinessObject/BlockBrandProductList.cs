﻿using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class BlockBrandProductList : BlockBase, IBlockBrandProductList
    {
        #region IBlockBrandProductList Members

        private int _columnCount;
        private int _rowCount;
        private int _productSorterOrderId;
        private IProductSortOrder _productSortOrder;

        public int ColumnCount
        {
            get { return _columnCount; }
            set
            {
                CheckChanged(_columnCount, value);
                _columnCount = value;
            }
        }

        public int RowCount
        {
            get { return _rowCount; }
            set
            {
                CheckChanged(_rowCount, value);
                _rowCount = value;
            }
        }

        public int ProductSortOrderId
        {
            get { return _productSorterOrderId; }
            set
            {
                CheckChanged(_productSorterOrderId, value);
                _productSorterOrderId = value;
            }
        }

        public IProductSortOrder ProductSortOrder
        {
            get { return _productSortOrder; }
            set
            {
                CheckChanged(_productSortOrder, value);
                _productSortOrder = value;
            }
        }

        #endregion
    }
}