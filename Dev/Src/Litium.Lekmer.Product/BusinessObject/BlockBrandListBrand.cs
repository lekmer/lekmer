﻿using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.BusinessObject
{
    [Serializable]
    public class BlockBrandListBrand : BusinessObjectBase, IBlockBrandListBrand
    {
        private int _blockId;
        private int _brandId;
        private int _ordinal;
        private string _title;

        public int BlockId
        {
            get { return _blockId; }
            set
            {
                CheckChanged(_blockId, value);

                _blockId = value;
            }
        }

        public int BrandId
        {
            get { return _brandId; }
            set
            {
                CheckChanged(_brandId, value);

                _brandId = value;
            }
        }

        public int Ordinal
        {
            get { return _ordinal; }
            set
            {
                CheckChanged(_ordinal, value);

                _ordinal = value;
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                CheckChanged(_title, value);

                _title = value;
            }
        }
    }
}