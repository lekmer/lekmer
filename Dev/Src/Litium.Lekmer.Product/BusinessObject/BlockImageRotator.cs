﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class BlockImageRotator : BlockBase, IBlockImageRotator
    {
        private int? _secondaryTemplateId;

        public int? SecondaryTemplateId
        {
            get { return _secondaryTemplateId; }
            set
            {
                CheckChanged(_secondaryTemplateId, value);
                _secondaryTemplateId = value;
            }
        }
    }
}