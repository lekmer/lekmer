﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class WishList : BusinessObjectBase, IWishList
    {
        private int _id;
        private Guid _wishListKey;
        private string _wishList;

        public int Id
        {
            get { return _id; }
            set
            {
                CheckChanged(_id, value);
                _id = value;
            }
        }
        public string ProductWishList
        {
            get { return _wishList; }
            set
            {
                CheckChanged(_wishList, value);
                _wishList = value;
            }
        }
        public Guid WishListKey
        {
            get { return _wishListKey; }
            set
            {
                CheckChanged(_wishListKey, value);
                _wishListKey = value;
            }
        }
    }
}