﻿using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.BusinessObject
{
    [Serializable]
    public class LekmerProductSearchCriteria : ProductSearchCriteria, ILekmerProductSearchCriteria
    {
        public string BrandId
        {
            get;
            set;
        }
    }
}