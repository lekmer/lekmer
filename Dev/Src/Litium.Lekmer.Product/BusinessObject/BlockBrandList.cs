﻿using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.BusinessObject
{
    [Serializable]
    public class BlockBrandList : BlockBase, IBlockBrandList
    {
        private int _columnCount;
        private int _rowCount;
        private bool _includeAllBrands;
        private int? _linkContentNodeId;

        public int ColumnCount
        {
            get { return _columnCount; }
            set
            {
                CheckChanged(_columnCount, value);
                _columnCount = value;
            }
        }

        public int RowCount
        {
            get { return _rowCount; }
            set
            {
                CheckChanged(_rowCount, value);
                _rowCount = value;
            }
        }

        public bool IncludeAllBrands
        {
            get { return _includeAllBrands; }
            set
            {
                CheckChanged(_includeAllBrands, value);
                _includeAllBrands = value;
            }
        }

        public int? LinkContentNodeId
        {
            get { return _linkContentNodeId; }
            set
            {
                CheckChanged(_linkContentNodeId, value);
                _linkContentNodeId = value;
            }
        }
    }
}