using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BrandSiteStructureRegistry : BusinessObjectBase, IBrandSiteStructureRegistry
	{
		private int _brandId;
		private int _siteStructureRegistryId;
		private int _contentNodeId;

		public int BrandId
		{
			get { return _brandId; }
			set
			{
				CheckChanged(_brandId, value);
				_brandId = value;
			}
		}
		public int SiteStructureRegistryId
		{
			get { return _siteStructureRegistryId; }
			set
			{
				CheckChanged(_siteStructureRegistryId, value);
				_siteStructureRegistryId = value;
			}
		}
		public int ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}
	}
}