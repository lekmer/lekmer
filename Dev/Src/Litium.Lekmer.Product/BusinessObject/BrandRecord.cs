using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Contract;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BrandRecord : Brand, IBrandRecord
	{
		public Collection<IBrandSiteStructureRegistry> BrandSiteStructureRegistries { get; set; }
	}
}