﻿using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    public class BlockBrandProductListBrand : BusinessObjectBase, IBlockBrandProductListBrand
    {
        private int _brandId;
        private int _blockId;

        public int BrandId
        {
            get { return _brandId; }
            set
            {
                CheckChanged(_brandId, value);
                _brandId = value;
            }
        }

        public int BlockId
        {
            get { return _blockId; }
            set
            {
                CheckChanged(_brandId, value);
                _blockId = value;
            }
        }
    }
}