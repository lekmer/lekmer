namespace Litium.Lekmer.Product
{
	public class Size : ISize
	{
		public int Id { get; set; }
		public decimal EU { get; set; }
		public decimal USMale { get; set; }
		public decimal USFemale { get; set; }
		public decimal UKMale { get; set; }
		public decimal UKFemale { get; set; }
		public int Millimeter { get; set; }
	}
}