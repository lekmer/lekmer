﻿using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerProduct : Scensum.Product.Product, ILekmerProduct
	{
		private IBrand _brand;
		private int? _brandId;
		private bool _isBookable;
		private DateTime? _isNewFrom;
		private DateTime? _isNewTo;
		private DateTime? _createdDate;
		private bool _hasSizes;
		private string _urlTitle;
		private int? _parentContentNodeId;
		private string _lekmerErpId;

		public IBrand Brand
		{
			get { return _brand; }
			set
			{
				CheckChanged(_brand, value);
				_brand = value;
			}
		}

		public int? BrandId
		{
			get { return _brandId; }
			set
			{
				CheckChanged(_brandId, value);
				_brandId = value;
			}
		}

		public DateTime? IsNewFrom
		{
			get { return _isNewFrom; }
			set
			{
				CheckChanged(_isNewFrom, value);
				_isNewFrom = value;
			}
		}

		public DateTime? IsNewTo
		{
			get { return _isNewTo; }
			set
			{
				CheckChanged(_isNewTo, value);
				_isNewTo = value;
			}
		}

		public DateTime? CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public new bool IsBuyable
		{
			get { return NumberInStock > 0; }
		}

		public bool IsBookable
		{
			get { return _isBookable; }
			set
			{
				CheckChanged(_isBookable, value);
				_isBookable = value;
			}
		}

		public bool IsMonitorable
		{
			get { return !IsBookable && NumberInStock == 0; }
		}

		public bool IsNewProduct
		{
			get
			{
				return
					(IsNewFrom.HasValue || IsNewTo.HasValue) &&
					(!IsNewFrom.HasValue || DateTime.Now >= IsNewFrom) &&
					(!IsNewTo.HasValue || DateTime.Now <= IsNewTo);
			}
		}

		public bool HasSizes
		{
			get { return _hasSizes; }
			set
			{
				CheckChanged(_hasSizes, value);
				_hasSizes = value;
			}
		}

		public string ParentPageUrl { get; set; }

		public string LekmerUrl
		{
			get
			{
				string url = Encoder.EncodeUrl(UrlTitle.ToLowerInvariant())
					.Replace('+', '-');

				string parentPageUrl = ParentPageUrl;
				if (!parentPageUrl.IsNullOrEmpty())
				{
					if (parentPageUrl.Length > 1)
					{
						parentPageUrl = parentPageUrl.Substring(2);
					}

					url = parentPageUrl + url;
				}

				return url;
			}
		}

		public string UrlTitle
		{
			get { return _urlTitle; }
			set
			{
				CheckChanged(_urlTitle, value);
				_urlTitle = value;
			}
		}

		public int? ParentContentNodeId
		{
			get { return _parentContentNodeId; }
			set
			{
				CheckChanged(_parentContentNodeId, value);
				_parentContentNodeId = value;
			}
		}
		public string LekmerErpId
		{
			get { return _lekmerErpId; }
			set
			{
				CheckChanged(_lekmerErpId, value);
				_lekmerErpId = value;
			}
		}
	}
}