﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductSize : BusinessObjectBase, IProductSize
	{
		private int _productId;
		private int _sizeId;
		private string _erpId;
		private int _numberInStock;
		private int? _millimeterDeviation;
		private decimal? _overrideEu;
		private decimal _eu;
		private decimal _usMale;
		private decimal _usFemale;
		private decimal _ukMale;
		private decimal _ukFemale;
		private int _millimeter;
		
		public int ProductId
		{
			get { return _productId; }
			set 
			{ 
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}

		public string ErpId
		{
			get { return _erpId; }
			set
			{
				CheckChanged(_erpId, value);
				_erpId = value;
			}
		}

		public int NumberInStock
		{
			get { return _numberInStock; }
			set
			{
				CheckChanged(_numberInStock, value);
				_numberInStock = value;
			}
		}

		public int? MillimeterDeviation
		{
			get { return _millimeterDeviation; }
			set
			{
				CheckChanged(_millimeterDeviation, value);
				_millimeterDeviation = value;
			}
		}

		public decimal? OverrideEu
		{
			get { return _overrideEu; }
			set
			{
				CheckChanged(_overrideEu, value);
				_overrideEu = value;
			}
		}

		public decimal Eu
		{
			get { return _eu; }
			set
			{
				CheckChanged(_eu, value);
				_eu = value;
			}
		}

		public decimal UsMale
		{
			get { return _usMale; }
			set
			{
				CheckChanged(_usMale, value);
				_usMale = value;
			}
		}

		public decimal UsFemale
		{
			get { return _usFemale; }
			set
			{
				CheckChanged(_usFemale, value);
				_usFemale = value;
			}
		}

		public decimal UkMale
		{
			get { return _ukMale; }
			set
			{
				CheckChanged(_ukMale, value);
				_ukMale = value;
			}
		}

		public decimal UkFemale
		{
			get { return _ukFemale; }
			set
			{
				CheckChanged(_ukFemale, value);
				_ukFemale = value;
			}
		}

		public int Millimeter
		{
			get { return _millimeter; }
			set
			{
				CheckChanged(_millimeter, value);
				_millimeter = value;
			}
		}

		public bool IsBuyable
		{
			get { return NumberInStock > 0; }
		}
	}
}
