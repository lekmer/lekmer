﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
    [Serializable]
    public class Icon : BusinessObjectBase, IIcon
    {
        private int _id;
        private string _title;
        private IImage _image;

        public int Id
        {
            get { return _id; }
            set
            {
                CheckChanged(_id, value);
                _id = value;
            }
        }

        public string Title
        {
            get { return _title; }
            set
            {
                CheckChanged(_title, value);
                _title = value;
            }
        }

        public IImage Image
        {
            get { return _image; }
            set
            {
                CheckChanged(_image, value);
                _image = value;
            }
        }
    }
}
