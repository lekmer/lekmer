﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Lekmer.Product.BusinessObject;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerProductRepository : ProductRepository
	{
		public virtual ProductIdCollection GetIdAllByBrandIdList(
			int channelId,
			ICustomer customer,
			string categoryIdList,
			char delimiter,
			int pageNumber,
			int pageSize,
			IProductSortOrder sortOrder)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?) customer.Id : null,
                                                    SqlDbType.Int),
                    ParameterHelper.CreateParameter("@BrandIds", categoryIdList, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1),
                    ParameterHelper.CreateParameter("@Page", pageNumber, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@SortBy", sortOrder.SortByColumn, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("@SortDescending", sortOrder.SortDescending, SqlDbType.Bit)
                };
			var dbSettings = new DatabaseSetting(
				"ProductRepository.GetIdAllByBrandIdList");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[lekmer].[pProductGetIdAllByBrandIdList]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}
			productIds.TotalCount = totalCount;
			return productIds;
		}

		public override int Save(IProductRecord product)
		{
			if (product == null) throw new ArgumentNullException("product");

			var lekmerProduct = product as ILekmerProductRecord;
			if (lekmerProduct == null) throw new ArgumentException("product should be of ILekmerProductRecord type");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", lekmerProduct.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ItemsInPackage", lekmerProduct.ItemsInPackage, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ErpId", lekmerProduct.ErpId, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@EanCode", lekmerProduct.EanCode, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Title", lekmerProduct.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@WebShopTitle", lekmerProduct.WebShopTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@ProductStatusId", lekmerProduct.ProductStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Description", lekmerProduct.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@ShortDescription", lekmerProduct.ShortDescription, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@CategoryId", lekmerProduct.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MediaId", lekmerProduct.MediaId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@BrandId", lekmerProduct.BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@AgeFromMonth", lekmerProduct.AgeFromMonth, SqlDbType.Int),
					ParameterHelper.CreateParameter("@AgeToMonth", lekmerProduct.AgeToMonth, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@IsBookable", lekmerProduct.IsBookable, SqlDbType.Bit),
                    ParameterHelper.CreateParameter("@IsNewFrom", lekmerProduct.IsNewFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@IsNewTo", lekmerProduct.IsNewTo, SqlDbType.DateTime),
                    ParameterHelper.CreateParameter("@Measurement", lekmerProduct.Measurement, SqlDbType.NVarChar),
                    ParameterHelper.CreateParameter("@BatteryTypeId", lekmerProduct.BatteryTypeId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@NumberOfBatteries", lekmerProduct.NumberOfBatteries, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@IsBatteryIncluded", lekmerProduct.IsBatteryIncluded, SqlDbType.Bit),
                    ParameterHelper.CreateParameter("@ExpectedBackInStock", lekmerProduct.ExpectedBackInStock, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@CreatedDate", lekmerProduct.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@SizeDeviationId", lekmerProduct.SizeDeviationId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ProductRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pProductSave]", parameters, dbSettings);
			return lekmerProduct.Id;
		}

		public virtual ProductIdCollection GetIdAll(
			IChannel channel, ICustomer customer, int page, int pageSize)
		{
			int totalCount = 0;
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channel.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[lekmer].[pProductGetIdAll]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						productIds.Add(dataReader.GetInt32(1));
					}
				}
			}
			productIds.TotalCount = totalCount;
			return productIds;
		}

		public virtual int? GetIdByOldUrl(string oldUrl)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OldUrl", oldUrl, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.GetIdByOldUrl");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[lekmer].[pProductGetIdByOldUrl]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					return dataReader.GetInt32(0);
				}
			}
			return null;
		}

		public void DeletePopularity(int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
				};
			var dbSettings = new DatabaseSetting("LekmerProductRepository.UpdatePopularity");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDeletePopularity]", parameters, dbSettings);
		}
		public void UpdatePopularity(int channelId, DateTime dateTimeFrom, DateTime dateTimeTo)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("DateTimeFrom", dateTimeFrom, SqlDbType.SmallDateTime),
					ParameterHelper.CreateParameter("DateTimeTo", dateTimeTo, SqlDbType.SmallDateTime)
				};

			var dbSettings = new DatabaseSetting("LekmerProductRepository.UpdatePopularity");
			new DataHandler().ExecuteCommand("[lekmer].[pProductUpdatePopularity]", parameters, dbSettings);
		}




		public virtual LekmerProductViewCollection GetAllViewByIdList(
			int channelId, ICustomer customer, string idList, char delimiter, int pageNumber, int pageSize)
		{
			int totalCount = 0;
			var products = new LekmerProductViewCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductIds", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Delimiter", delimiter, SqlDbType.Char, 1),
					ParameterHelper.CreateParameter("@Page", pageNumber, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.ProductGetAllByIdList");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[product].[pProductGetViewAllByIdList]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					totalCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					products = CreateViewDataMapper(dataReader).ReadMultipleRows<LekmerProductViewCollection>();
				}
			}
			products.TotalCount = totalCount;
			return products;
		}

        public void RefreshProductListingTemporaryData()
        {
            var parameters = new IDataParameter[0];
            var dbSettings = new DatabaseSetting("LekmerProductRepository.RefreshProductListingTemporaryData");
            new DataHandler().ExecuteCommand("[lekmer].[pRefreshProductListingTemporaryData]", parameters, dbSettings);

        }
        public void RefreshCustomerGroupProductPriceListTemporaryData()
        {
            var parameters = new IDataParameter[0];
            var dbSettings = new DatabaseSetting("LekmerProductRepository.RefreshCustomerGroupProductPriceListTemporaryData");
            new DataHandler().ExecuteCommand("[lekmer].[pRefreshCustomerGroupProductPriceListTemporaryData]", parameters, dbSettings);
        }
		public override ProductCollection Search(int channelId, IProductSearchCriteria searchCriteria, int page, int pageSize)
		{
			
			if (searchCriteria == null) throw new ArgumentNullException("searchCriteria");
			int itemsCount = 0;
			var products = new ProductCollection();
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BrandId", ( (ILekmerProductSearchCriteria)searchCriteria).BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CategoryId", searchCriteria.CategoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Title", searchCriteria.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@PriceFrom", searchCriteria.PriceFrom, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@PriceTo", searchCriteria.PriceTo, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@ErpId", searchCriteria.ErpId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@StatusId", searchCriteria.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@EanCode", searchCriteria.EanCode, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[product].[pProductSearch]", parameters, dbSettings)
				)
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					products = CreateDataMapper(dataReader).ReadMultipleRows<ProductCollection>();
				}
			}
			products.TotalCount = itemsCount;
			return products;
		}

	}
}