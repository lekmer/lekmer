﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductUrlRepository
	{
		protected DataMapperBase<IProductUrl> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductUrl>(dataReader);
		}

		public int? GetIdByUrlTitle(int languageId, string urlTitle)
		{
			int? id = null;
			var dbSettings = new DatabaseSetting("ProductUrlRepository.GetIdByUrlTitle");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@UrlTitle", urlTitle, SqlDbType.NVarChar)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductGetIdByUrlTitle]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					id = dataReader.GetInt32(0);
				}
			}
			return id;
		}

		public Collection<IProductUrl> GetAllByProduct(int productId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlRepository.GetAllByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductUrlGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public int Check(IProductUrl productUrl)
		{
			var dbSettings = new DatabaseSetting("ProductUrlRepository.Check");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productUrl.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", productUrl.LanguageId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@UrlTitle", productUrl.UrlTitle, SqlDbType.NVarChar)
				};
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pProductUrlCheck]", parameters, dbSettings);
		}

		public void Save(IProductUrl productUrl)
		{
			var dbSettings = new DatabaseSetting("ProductUrlRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productUrl.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@LanguageId", productUrl.LanguageId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@UrlTitle", productUrl.UrlTitle, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlSave]", parameters, dbSettings);
		}

		public void DeleteAllByProduct(int productId)
		{
			var dbSettings = new DatabaseSetting("ProductUrlRepository.DeleteAllByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pProductUrlDelete]", parameters, dbSettings);
		}
	}
}
