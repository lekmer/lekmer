﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class WishListRepository
    {
        protected virtual DataMapperBase<IWishList> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IWishList>(dataReader);
        }

        public virtual IWishList GetByKey(Guid key)
        {
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@WishListKey", key, SqlDbType.UniqueIdentifier)
				};
            DatabaseSetting dbSettings = new DatabaseSetting("WishListRepository.GetByKey");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pWishListGetByKey]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public virtual void Save(IWishList wishList)
        {
            if (wishList == null) throw new ArgumentNullException("wishList");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@WishListKey", wishList.WishListKey, SqlDbType.UniqueIdentifier),
					ParameterHelper.CreateParameter("@WishList", wishList.ProductWishList, SqlDbType.VarChar)
				
				};
            DatabaseSetting dbSettings = new DatabaseSetting("WishListRepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pWishListSave]", parameters, dbSettings);
        }

    }
}
