using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class TagRepository
	{
		protected virtual DataMapperBase<ITag> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITag>(dataReader);
		}

        protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
        }

		public Collection<ITag> GetAllByTagGroup(int languageId, int tagGroupId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByTagGroup");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@TagGroupId", tagGroupId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByTagGroup]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<ITag> GetAllByTagGroupAndProduct(int languageId, int tagGroupId, int productId)
		{
			var dbSettings = new DatabaseSetting("TagRepository.GetAllByTagGroupAndProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@TagGroupId", tagGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByTagGroupAndProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

        public Collection<ITag> GetAllByTagGroupSecure(int tagGroupId)
        {
            var dbSettings = new DatabaseSetting("TagRepository.GetAllByTagGroupSecure");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@TagGroupId", tagGroupId, SqlDbType.Int)
				};
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByTagGroupSecure]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public Collection<ITag> GetAllByProductAndTagGroupSecure(int productId, int tagGroupId)
        {
            var dbSettings = new DatabaseSetting("TagRepository.GetAllByProductAndTagGroupSecure");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@TagGroupId", tagGroupId, SqlDbType.Int)
				};
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGetAllByProductAndTagGroupSecure]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public int Save(ITag tag)
        {
            var dbSettings = new DatabaseSetting("TagRepository.Save");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@TagId", tag.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@TagGroupId", tag.TagGroupId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@Value", tag.Value, SqlDbType.NVarChar)
				};
            return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pTagSave]", parameters, dbSettings);
        }

        public void Delete(int tagId)
        {
            var dbSettings = new DatabaseSetting("TagRepository.Delete");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@TagId", tagId, SqlDbType.Int),
				};
            new DataHandler().ExecuteCommand("[lekmer].[pTagDelete]", parameters, dbSettings);
        }

        public void InsertProductTag(int productId, int tagId)
        {
            var dbSettings = new DatabaseSetting("TagRepository.InsertProductTag");
            IDataParameter[] parameters =
				{
                    ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@TagId", tagId, SqlDbType.Int)
				};
            new DataHandler().ExecuteCommand("[lekmer].[pProductTagSave]", parameters, dbSettings);
        }

        public void DeleteAllProductTag(int productId)
        {
            var dbSettings = new DatabaseSetting("TagRepository.DeleteAllProductTag");
            IDataParameter[] parameters =
				{
                    ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
            new DataHandler().ExecuteCommand("[lekmer].[pProductTagDeleteAll]", parameters, dbSettings);
        }

        public Collection<ITranslationGeneric> GetAllTranslationsByTag(int id)
        {
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TagId", id, SqlDbType.Int)
				};
            DatabaseSetting dbSettings = new DatabaseSetting("TagRepository.GetAllTranslationsByTag");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagTranslationGetAllByTag]", parameters, dbSettings))
            {
                return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public void SaveTranslation(ITranslationGeneric translation)
        {
            var dbSettings = new DatabaseSetting("TagRepository.SaveTranslation");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@TagId", translation.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@LanguageId", translation.LanguageId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@Value", translation.Value, SqlDbType.NVarChar)
				};
            new DataHandler().ExecuteCommand("[lekmer].[pTagTranslationSave]", parameters, dbSettings);
        }
	}
}