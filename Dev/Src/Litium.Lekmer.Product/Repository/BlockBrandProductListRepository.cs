﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class BlockBrandProductListRepository
    {
        protected DataMapperBase<IBlockBrandProductList> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IBlockBrandProductList>(dataReader);
        }

        public void Save(IBlockBrandProductList block)
        {
            if (block == null) throw new ArgumentNullException("block");
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ColumnCount", block.ColumnCount, SqlDbType.Int),
                    ParameterHelper.CreateParameter("RowCount", block.RowCount, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ProductSortOrderId", block.ProductSortOrderId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductList.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandProductListSave]", parameters, dbSettings);
        }

        public IBlockBrandProductList GetById(IChannel channel, int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductListRepository.GetById");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pBlockBrandProductListGetById]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public IBlockBrandProductList GetByIdSecure(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductListRepository.GetByIdSecure");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pBlockBrandProductListGetByIdSecure]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public void Delete(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockBrandProductListRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandProductListDelete]", parameters, dbSettings);
        }
    }
}