using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class AgeIntervalRepository
	{
		protected virtual DataMapperBase<IAgeInterval> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IAgeInterval>(dataReader);
		}

		public Collection<IAgeInterval> GetAll(int languageId)
		{
			var dbSettings = new DatabaseSetting("AgeIntervalRepository.GetAll");
			IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int)
                };

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pAgeIntervalGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<IAgeInterval> GetAllSecure()
		{
			var dbSettings = new DatabaseSetting("AgeIntervalRepository.GetAllSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pAgeIntervalGetAllSecure]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		} 
	}
}