﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class ImageRotatorGroupRepository
    {
        protected DataMapperBase<IImageRotatorGroup> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IImageRotatorGroup>(dataReader);
        }

        public void Save(IImageRotatorGroup imageRotatorGroup)
        {
            if (imageRotatorGroup == null) throw new ArgumentNullException("imageRotatorGroup");
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("ImageGroupId", imageRotatorGroup.ImageGroupId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", imageRotatorGroup.BlockId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("MainImageId", imageRotatorGroup.MainImageId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ThumbnailImageId", imageRotatorGroup.ThumbnailImageId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("InternalLinkContentNodeId", imageRotatorGroup.InternalLinkContentNodeId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ExternalLink", imageRotatorGroup.ExternalLink, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("IsMovie", imageRotatorGroup.IsMovie, SqlDbType.Int),
                    ParameterHelper.CreateParameter("Title", imageRotatorGroup.Title, SqlDbType.NVarChar),
                    ParameterHelper.CreateParameter("HtmalMarkup", imageRotatorGroup.HtmalMarkup, SqlDbType.NVarChar),

                    ParameterHelper.CreateParameter("InternalLinkContentNodeId2", imageRotatorGroup.InternalLinkContentNodeId2, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ExternalLink2", imageRotatorGroup.ExternalLink2, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("IsMovie2", imageRotatorGroup.IsMovie2, SqlDbType.Int),
                    ParameterHelper.CreateParameter("InternalLinkContentNodeId3", imageRotatorGroup.InternalLinkContentNodeId3, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ExternalLink3", imageRotatorGroup.ExternalLink3, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("IsMovie3", imageRotatorGroup.IsMovie3, SqlDbType.Int),

                    ParameterHelper.CreateParameter("Ordinal", imageRotatorGroup.Ordinal, SqlDbType.Int),
					ParameterHelper.CreateParameter("StartDateTime", imageRotatorGroup.StartDateTime, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("EndDateTime", imageRotatorGroup.EndDateTime, SqlDbType.DateTime),
                  
                };
            DatabaseSetting dbSettings = new DatabaseSetting("ImageRotatorGroupRepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pImageRotatorGroupSave]", parameters, dbSettings);
        }

        public IImageRotatorGroup GetById(int imageRotatorGroupId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("ImageRotatorGroupId", imageRotatorGroupId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("ImageRotatorGroupRepository.GetById");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pImageRotatorGroupGetById]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public ImageRotatorGroupCollection GetByBlockId(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("ImageRotatorGroupRepository.GetById");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pImageRotatorGroupGetByBlockId]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows<ImageRotatorGroupCollection>();
            }
        }


        public void DeleteAllByBlockImageRotator(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("ImageRotatorGroupRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pImageRotatorGroupDelete]", parameters, dbSettings);
        }
    }
}