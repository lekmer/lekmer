﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class BrandRepository
    {
        protected virtual DataMapperBase<IBrand> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IBrand>(dataReader);
        }

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

        public virtual BrandCollection GetAll(int page, int pageSize, string sortBy, bool? sortByDescending)
        {
            int numberOfItems = 0;
            var brandList = new BrandCollection();

            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("Page", page, SqlDbType.Int),
                    ParameterHelper.CreateParameter("PageSize", pageSize, SqlDbType.Int),
                    ParameterHelper.CreateParameter("SortBy", sortBy, SqlDbType.NVarChar),
                    ParameterHelper.CreateParameter("SortDescending", sortByDescending, SqlDbType.Bit)
                };
            var dbSettings = new DatabaseSetting("BrandRepository.GetAll");
            using (
                IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandGetAllSecureList]", parameters,
                                                                         dbSettings))
            {
                if (dataReader.Read())
                {
                    numberOfItems = dataReader.GetInt32(0);
                }
                if (dataReader.NextResult())
                {
                    brandList = CreateDataMapper(dataReader).ReadMultipleRows<BrandCollection>();
                }
            }
            brandList.TotalCount = numberOfItems;
            return brandList;
        }


        public BrandCollection GetAll(IUserContext context)
        {
            var dbSettings = new DatabaseSetting("BrandRepository.GetAll");
			IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("@ChannelId", context.Channel.Id, SqlDbType.Int)
                };

        	BrandCollection brands;
        	using (
                IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandGetAll]", parameters, dbSettings))
            {
            	brands = CreateDataMapper(dataReader).ReadMultipleRows<BrandCollection>();
            }
        	brands.TotalCount = brands.Count;
        	return brands;
        }  

        public Collection<IBrand> GetAllSecure()
        {
            var dbSettings = new DatabaseSetting("BrandRepository.GetAllSecure");

            using (
                IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandGetAllSecure]", dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public IBrand GetById(int channelId, int brandId)
        {
            var dbSettings = new DatabaseSetting("BrandRepository.GetById");
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@BrandId", brandId, SqlDbType.Int)
                };

            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandGetById]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

		public IBrand GetByIdSecure(int brandId)
		{
			var dbSettings = new DatabaseSetting("BrandRepository.GetByIdSecure");
			IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("@BrandId", brandId, SqlDbType.Int)
                };

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

        public int Save(IBrand brand)
        {
            if (brand == null) throw new ArgumentNullException("brand");
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BrandId", brand.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("Title", brand.Title, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("MediaId", brand.MediaId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("Description", brand.Description, SqlDbType.VarChar),
                    ParameterHelper.CreateParameter("ExternalUrl", brand.ExternalUrl, SqlDbType.VarChar)
                };
            var dbSettings = new DatabaseSetting("BrandRepository.Save");
            return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pBrandSave]", parameters, dbSettings);
        }


        public virtual void Delete(int brandId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
                };
            var dbSettings = new DatabaseSetting("BrandRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pBrandDelete]", parameters, dbSettings);
        }

        public BrandCollection GetAllByBlock(int channelId, int blockId, int page, int pageSize)
        {
            int itemsCount = 0;
            var brands = new BrandCollection();
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("Page", page, SqlDbType.Int),
                    ParameterHelper.CreateParameter("PageSize", pageSize, SqlDbType.Int)
                };
            var dbSettings = new DatabaseSetting("BrandRepository.GetAllByBlock");
            using (
                IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandGetAllByBlock]", parameters,
                                                                         dbSettings))
            {
                if (dataReader.Read())
                {
                    itemsCount = dataReader.GetInt32(0);
                }
                if (dataReader.NextResult())
                {
                    brands = CreateDataMapper(dataReader).ReadMultipleRows<BrandCollection>();
                }
            }
            brands.TotalCount = itemsCount;
            return brands;
        }

        public Collection<IBrand> GetAllByBlock(int channelId, int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
                };
            var dbSettings = new DatabaseSetting("BrandRepository.GetAllByBlock2");
            using (
                IDataReader dataReader =
                    new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandProductListBrandGetAllByBlock]", parameters,
                                                    dbSettings))
            {
                return DataMapperResolver.Resolve<IBrand>(dataReader).ReadMultipleRows();
            }
        }



		public Collection<ITranslationGeneric> GetAllTitleTranslationsByBrand(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BrandRepository.GetAllTitleTranslationsByBrand");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandTitleTranslationGetAllByBrand]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public Collection<ITranslationGeneric> GetAllDescriptionTranslationsByBrand(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BrandRepository.GetAllDescriptionTranslationsByBrand");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandDescriptionTranslationGetAllByBrand]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public void SaveTranslation(int brandId, int languageId, string title, string description)
		{
			var dbSettings = new DatabaseSetting("BrandRepository.SaveTranslation");
			IDataParameter[] parameters =
		        {
		            ParameterHelper.CreateParameter("@BrandId", brandId, SqlDbType.Int),
		            ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int),
		            ParameterHelper.CreateParameter("@Title", title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Description", description, SqlDbType.NVarChar)
		        };
			new DataHandler().ExecuteCommand("[lekmer].[pBrandTranslationSave]", parameters, dbSettings);
		}
    }
}
