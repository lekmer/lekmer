﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class BlockImageRotatorRepository
    {
        protected DataMapperBase<IBlockImageRotator> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IBlockImageRotator>(dataReader);
        }

        public IBlockImageRotator GetById(IChannel channel, int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockImageRotatorRepository.GetById");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pBlockImageRotatorGetById]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public IBlockImageRotator GetByIdSecure(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockImageRotatorRepository.GetByIdSecure");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pBlockImageRotatorGetByIdSecure]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public void Save(IBlockImageRotator blockBrand)
        {
            if (blockBrand == null) throw new ArgumentNullException("blockBrand");

            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockBrand.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("SecondaryTemplateId", blockBrand.SecondaryTemplateId, SqlDbType.Int)
                };
            var dbSettings = new DatabaseSetting("BlockImageRotatorRepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockImageRotatorSave]", parameters, dbSettings);
        }

        public void Delete(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            var dbSettings = new DatabaseSetting("BlockImageRotatorRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockImageRotatorDelete]", parameters, dbSettings);
        }
    }
}