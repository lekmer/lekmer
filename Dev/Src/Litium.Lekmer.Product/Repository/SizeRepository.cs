using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeRepository
	{
		protected virtual DataMapperBase<ISize> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISize>(dataReader);
		}

		public Collection<ISize> GetAll()
		{
			var dbSettings = new DatabaseSetting("SizeRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}