﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class BlockReviewMyWishListRepository
    {
        protected virtual DataMapperBase<IBlockReviewMyWishList> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IBlockReviewMyWishList>(dataReader);
        }

        public virtual IBlockReviewMyWishList GetById(IChannel channel, int blockId)
        {
            IDataParameter[] parameters =
				{
				 ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
            DatabaseSetting dbSettings = new DatabaseSetting("BlockReviewMyWishListRepository.GetById");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockReviewMyWishListGetById]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public IBlockReviewMyWishList GetByIdSecure(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            DatabaseSetting dbSettings = new DatabaseSetting("BlockReviewMyWishListRepository.GetByIdSecure");
            using (IDataReader dataReader = new DataHandler().ExecuteSelect(
                "[lekmer].[pBlockReviewMyWishListGetByIdSecure]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public virtual void Save(IBlockReviewMyWishList block)
        {
            if (block == null) throw new ArgumentNullException("block");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
                    	ParameterHelper.CreateParameter("ColumnCount", block.ColumnCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("RowCount", block.RowCount, SqlDbType.Int)
				
				};
            DatabaseSetting dbSettings = new DatabaseSetting("BlockReviewMyWishListRepository.Save");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockReviewMyWishListSave]", parameters, dbSettings);
        }

        public virtual void Delete(int blockId)
        {
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
            DatabaseSetting dbSettings = new DatabaseSetting("BlockReviewMyWishListRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockReviewMyWishListDelete]", parameters, dbSettings);
        }
    }
}