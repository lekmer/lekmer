﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class BlockBrandListRepository
    {
        protected DataMapperBase<IBlockBrandList> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IBlockBrandList>(dataReader);
        }

        public IBlockBrandList GetByIdSecure(int id)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
                };
            using (
                IDataReader dataReader =
                    new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandListGetByIdSecure]", parameters,
                                                    new DatabaseSetting("BlockBrandListRepository.GetByIdSecure")))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public IBlockBrandList GetById(IChannel channel, int id)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
                };
            using (
                IDataReader dataReader =
                    new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandListGetById]", parameters,
                                                    new DatabaseSetting("BlockBrandListRepository.GetById")))
            {
                return CreateDataMapper(dataReader).ReadRow();
            }
        }

        public void Save(IBlockBrandList block)
        {
            if (block == null) throw new ArgumentNullException("block");

            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("ColumnCount", block.ColumnCount, SqlDbType.Int),
                    ParameterHelper.CreateParameter("RowCount", block.RowCount, SqlDbType.Int),
                    ParameterHelper.CreateParameter("IncludeAllBrands", block.IncludeAllBrands, SqlDbType.Bit),
                    ParameterHelper.CreateParameter("LinkContentNodeId", block.LinkContentNodeId, SqlDbType.Int)
                };
            new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandListSave]", parameters,
                                             new DatabaseSetting("BlockBrandListRepository.Save"));
        }

        public void Delete(int blockId)
        {
            IDataParameter[] parameters =
                {
                    ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
                };
            var dbSettings = new DatabaseSetting("BlockBrandListRepository.Delete");
            new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandListDelete]", parameters, dbSettings);
        }
    }
}