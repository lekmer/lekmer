﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerCategoryRepository : CategoryRepository
	{
		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public Collection<ITranslationGeneric> GetAllTranslationsByCategory(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CategoryId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerCategoryRepository.GetAllTranslationsByCategory");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCategoryTranslationGetAllByCategory]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public void SaveTranslation(ITranslationGeneric translation)
		{
			var dbSettings = new DatabaseSetting("LekmerCategoryRepository.SaveTranslation");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CategoryId", translation.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@LanguageId", translation.LanguageId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@Title", translation.Value, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[lekmer].[pCategoryTranslationSave]", parameters, dbSettings);
		}
	}
}
