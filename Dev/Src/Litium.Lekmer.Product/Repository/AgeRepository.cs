﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class AgeRepository
	{
		protected virtual DataMapperBase<IAge> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IAge>(dataReader);
		}

		public Collection<IAge> GetAll()
		{
			var dbSettings = new DatabaseSetting("AgeRepository.GetAll");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pAgeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		} 
	}
}
