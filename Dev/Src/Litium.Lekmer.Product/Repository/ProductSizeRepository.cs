﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductSizeRepository
	{
		protected DataMapperBase<IProductSize> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductSize>(dataReader);
		}

		public Collection<IProductSize> GetAllByProduct(int productId)
		{
			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetAllByProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public void Save(IProductSize productSize)
		{
			var dbSettings = new DatabaseSetting("ProductSizeRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productSize.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SizeId", productSize.SizeId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@MillimeterDeviation", productSize.MillimeterDeviation, SqlDbType.Int),
					ParameterHelper.CreateParameter("@OverrideEU", productSize.OverrideEu, SqlDbType.Decimal)
				};
			new DataHandler().ExecuteSelect("[lekmer].[pProductSizeSave]", parameters, dbSettings);
		}

		public IProductSize GetById(int productId, int sizeId)
		{
			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetById");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SizeId", sizeId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public Collection<ProductSizeRelation> GetAll()
		{
			var productSizes = new Collection<ProductSizeRelation>();
			var dbSettings = new DatabaseSetting("ProductSizeRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductSizeGetAll]", dbSettings))
			{
				while (dataReader.Read())
				{
					productSizes.Add(new ProductSizeRelation(dataReader.GetInt32(0), dataReader.GetInt32(1)));
				}
			}
			return productSizes;
		}
	}
}
