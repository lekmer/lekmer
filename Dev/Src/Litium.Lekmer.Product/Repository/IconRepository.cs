﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
    public class IconRepository
    {
        protected virtual DataMapperBase<IIcon> CreateDataMapper(IDataReader dataReader)
        {
            return DataMapperResolver.Resolve<IIcon>(dataReader);
        }

        public Collection<IIcon> GetAll()
        {
            var dbSettings = new DatabaseSetting("IconRepository.GetAll");
            using (
                IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pIconGetAll]", dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public Collection<IIcon> GetAllByProduct(int productId, int languageId)
        {
            var dbSettings = new DatabaseSetting("IconRepository.GetAllByProduct");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@LanguageId", languageId, SqlDbType.Int)
				};
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pIconGetAllByProduct]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public Collection<IIcon> GetAllByProductSecure(int productId)
        {
            var dbSettings = new DatabaseSetting("IconRepository.GetAllByProductSecure");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
            using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pIconGetAllByProductSecure]", parameters, dbSettings))
            {
                return CreateDataMapper(dataReader).ReadMultipleRows();
            }
        }

        public void Insert(int productId, int iconId)
        {
            var dbSettings = new DatabaseSetting("IconRepository.Save");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@IconId", iconId, SqlDbType.Int)
				};
            new DataHandler().ExecuteSelect("[lekmer].[pLekmerProductIconSave]", parameters, dbSettings);
        }

        public void Delete(int productId)
        {
            var dbSettings = new DatabaseSetting("IconRepository.Delete");
            IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};
            new DataHandler().ExecuteSelect("[lekmer].[pLekmerProductIconDeleteAll]", parameters, dbSettings);
        }
    }
}
