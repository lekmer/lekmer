﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductDescriptionTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductDescriptionTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductDescriptionTranslationSave]";
			}
		}
	}
}
