using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class TagGroupRepository
	{
		protected virtual DataMapperBase<ITagGroup> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITagGroup>(dataReader);
		}

		public Collection<ITagGroup> GetAll()
		{
			var dbSettings = new DatabaseSetting("TagGroupRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pTagGroupGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}