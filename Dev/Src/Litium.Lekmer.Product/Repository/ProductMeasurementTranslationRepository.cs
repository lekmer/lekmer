﻿namespace Litium.Lekmer.Product.Repository
{
    public class ProductMeasurementTranslationRepository : ProductTranslationGenericRepository
    {
        public override string GetAllByProductStoredProcedureName
        {
            get
            {
                return "[lekmer].[pProductMeasurementTranslationGetAllByProduct]";
            }
        }
        public override string SaveStoredProcedureName
        {
            get
            {
                return "[lekmer].[pProductMeasurementTranslationSave]";
            }
        }
    }
}
