﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductSizeDataMapper : DataMapperBase<IProductSize>
	{
		public ProductSizeDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IProductSize Create()
		{
			var productSize = IoC.Resolve<IProductSize>();
			productSize.ProductId = MapValue<int>("ProductSize.ProductId");
			productSize.SizeId = MapValue<int>("ProductSize.SizeId");
			productSize.ErpId = MapValue<string>("ProductSize.ErpId");
			productSize.NumberInStock = MapValue<int>("ProductSize.NumberInStock");
			productSize.MillimeterDeviation = MapNullableValue<int?>("ProductSize.MillimeterDeviation");
			productSize.OverrideEu = MapNullableValue<decimal?>("ProductSize.OverrideEU");
			productSize.Eu = MapValue<decimal>("Size.EU");
			productSize.UsMale = MapValue<decimal>("Size.USMale");
			productSize.UsFemale = MapValue<decimal>("Size.USFemale");
			productSize.UkMale = MapValue<decimal>("Size.UKMale");
			productSize.UkFemale = MapValue<decimal>("Size.UKFemale");
			productSize.Millimeter = MapValue<int>("Size.Millimeter");
			productSize.SetUntouched();
			return productSize;
		}
	}
}
