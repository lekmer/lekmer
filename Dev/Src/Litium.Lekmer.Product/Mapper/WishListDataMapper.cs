﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
    public class WishListDataMapper : DataMapperBase<IWishList>
    {
        public WishListDataMapper(IDataReader dataReader)
            : base(dataReader)
        {
        }
        protected override IWishList Create()
        {
            var wishList = IoC.Resolve<IWishList>();
            wishList.WishListKey = MapValue<Guid>("WishListKey");
            wishList.ProductWishList = MapValue<string>("WishList");
            wishList.Status = BusinessObjectStatus.Untouched;
            return wishList;
        }
    }
}

