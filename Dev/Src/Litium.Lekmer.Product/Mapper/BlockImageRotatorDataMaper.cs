﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
    public class BlockImageRotatorDataMapper : DataMapperBase<IBlockImageRotator>
    {
        private DataMapperBase<IBlock> _blockDataMapper;
        public BlockImageRotatorDataMapper(IDataReader dataReader)
            : base(dataReader)
        {
        }
        protected override void Initialize()
        {
            base.Initialize();
            _blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
        }
        protected override IBlockImageRotator Create()
        {
            var block = _blockDataMapper.MapRow();
            var blockImageRotator = IoC.Resolve<IBlockImageRotator>();
            block.ConvertTo(blockImageRotator);
            blockImageRotator.SecondaryTemplateId =  MapNullableValue<int?>("SecondaryTemplateId");
            return blockImageRotator;
        }
    }
}