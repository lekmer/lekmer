﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
    public class BlockReviewMyWishListDataMapper : DataMapperBase<IBlockReviewMyWishList>
    {
        private DataMapperBase<IBlock> _blockDataMapper;
        public BlockReviewMyWishListDataMapper(IDataReader dataReader)
            : base(dataReader)
        {
        }
        protected override void Initialize()
        {
            base.Initialize();
            _blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
        }
        protected override IBlockReviewMyWishList Create()
        {
            var block = _blockDataMapper.MapRow();
            var blockReviewMyWishList = IoC.Resolve<IBlockReviewMyWishList>();
            block.ConvertTo(blockReviewMyWishList);
            blockReviewMyWishList.ColumnCount = MapValue<int>("ColumnCount");
            blockReviewMyWishList.RowCount = MapValue<int>("RowCount");
            blockReviewMyWishList.Status = BusinessObjectStatus.Untouched;
            return blockReviewMyWishList;
        }
    }
}

