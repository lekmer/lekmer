using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeDataMapper : DataMapperBase<ISize>
	{
		public SizeDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

		protected override ISize Create()
        {
			var size = IoC.Resolve<ISize>();
			size.Id = MapValue<int>("Size.Id");
			size.EU = MapValue<decimal>("Size.EU");
			size.USMale = MapValue<decimal>("Size.USMale");
			size.USFemale = MapValue<decimal>("Size.USFemale");
			size.UKMale = MapValue<decimal>("Size.UKMale");
			size.UKFemale = MapValue<decimal>("Size.UKFemale");
			size.Millimeter = MapValue<int>("Size.Millimeter");
			return size;
        }
	}
}