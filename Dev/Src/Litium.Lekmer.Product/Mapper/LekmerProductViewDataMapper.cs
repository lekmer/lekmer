﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
    internal class LekmerProductViewDataMapper : ProductViewDataMapper
    {
		private DataMapperBase<IBatteryType> _batteryTypeDataMapper;
    	private DataMapperBase<ISizeDeviation> _sizeDeviationDataMapper;

        public LekmerProductViewDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

		protected override void Initialize()
		{
			base.Initialize();
			_batteryTypeDataMapper = DataMapperResolver.Resolve<IBatteryType>(DataReader);
			_sizeDeviationDataMapper = DataMapperResolver.Resolve<ISizeDeviation>(DataReader);
		}

        protected override IProductView Create()
        {
            var product = (ILekmerProductView) base.Create();
			product.BrandId = MapNullableValue<int?>("Lekmer.BrandId");
			product.IsBookable = MapValue<bool>("Lekmer.IsBookable");
			product.AgeFromMonth = MapValue<int>("Lekmer.AgeFromMonth");
			product.AgeToMonth = MapValue<int>("Lekmer.AgeToMonth");
            product.IsNewFrom = MapNullableValue<DateTime?>("Lekmer.IsNewFrom");
            product.IsNewTo = MapNullableValue<DateTime?>("Lekmer.IsNewTo");
            product.Measurement = MapNullableValue<string>("Lekmer.Measurement");
            product.NumberOfBatteries = MapNullableValue<int?>("Lekmer.NumberOfBatteries");
            product.IsBatteryIncluded = MapValue<bool>("Lekmer.IsBatteryIncluded");
            product.ExpectedBackInStock = MapNullableValue<DateTime?>("Lekmer.ExpectedBackInStock");
			product.LekmerErpId = MapNullableValue<string>("Lekmer.LekmerErpId");
			var batteryTypeId = MapNullableValue<int?>("Lekmer.BatteryTypeId");
			if (batteryTypeId.HasValue)
			{
				product.BatteryType = _batteryTypeDataMapper.MapRow();
			}
			var sizeDeviationId = MapNullableValue<int?>("Lekmer.SizeDeviationId");
			if (sizeDeviationId.HasValue)
			{
				product.SizeDeviation = _sizeDeviationDataMapper.MapRow();
			}
			product.HasSizes = MapValue<bool>("Lekmer.HasSizes");
			product.CreatedDate = MapNullableValue<DateTime?>("Lekmer.CreatedDate");
			product.UrlTitle = MapValue<string>("Lekmer.UrlTitle");
        	return product;
        }
    }
}