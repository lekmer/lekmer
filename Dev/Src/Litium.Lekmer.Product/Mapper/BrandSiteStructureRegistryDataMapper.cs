using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class BrandSiteStructureRegistryDataMapper : DataMapperBase<IBrandSiteStructureRegistry>
	{
		private int _brandId;
		private int _siteStructureRegistryId;
		private int _contentNodeId;

		public BrandSiteStructureRegistryDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_brandId = OrdinalOf("BrandSiteStructureRegistry.BrandId");
			_siteStructureRegistryId = OrdinalOf("BrandSiteStructureRegistry.SiteStructureRegistryId");
			_contentNodeId = OrdinalOf("BrandSiteStructureRegistry.ContentNodeId");
		}

		protected override IBrandSiteStructureRegistry Create()
		{
			IBrandSiteStructureRegistry brandSiteStructureRegistry = IoC.Resolve<IBrandSiteStructureRegistry>();
			brandSiteStructureRegistry.BrandId = MapValue<int>(_brandId);
			brandSiteStructureRegistry.SiteStructureRegistryId = MapValue<int>(_siteStructureRegistryId);
			brandSiteStructureRegistry.ContentNodeId = MapValue<int>(_contentNodeId);
			brandSiteStructureRegistry.Status = BusinessObjectStatus.Untouched;
			return brandSiteStructureRegistry;
		}
	}
}