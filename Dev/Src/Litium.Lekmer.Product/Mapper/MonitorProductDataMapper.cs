﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class MonitorProductDataMapper : DataMapperBase<IMonitorProduct>
    {
		public MonitorProductDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override IMonitorProduct Create()
        {
			var monitorProduct = IoC.Resolve<IMonitorProduct>();
        	monitorProduct.Id = MapValue<int>("MonitorProductId");
			monitorProduct.ProductId = MapValue<int>("ProductId");
			monitorProduct.ChannelId = MapValue<int>("ChannelId");
			monitorProduct.Email = MapValue<string>("Email");
			monitorProduct.CreatedDate = MapValue<DateTime>("CreatedDate");
			monitorProduct.SizeId = MapNullableValue<int?>("SizeId");
			monitorProduct.SetUntouched();
			return monitorProduct;
        }
    }
}