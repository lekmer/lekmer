﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class AgeDataMapper : DataMapperBase<IAge>
	{
		public AgeDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

		protected override IAge Create()
        {
			var age = IoC.Resolve<IAge>();
			age.Id = MapValue<int>("Age.Id");
			age.Months = MapValue<int>("Age.Months");
			age.Title = MapValue<string>("Age.Title");
			age.SetUntouched();
			return age;
        }
	}
}