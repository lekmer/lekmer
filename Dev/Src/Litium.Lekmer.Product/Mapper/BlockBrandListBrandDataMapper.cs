﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
    public class BlockBrandListBrandDataMapper : DataMapperBase<IBlockBrandListBrand>
    {
        public BlockBrandListBrandDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override IBlockBrandListBrand Create()
        {
            var blockBrand = IoC.Resolve<IBlockBrandListBrand>();
            blockBrand.BlockId = MapValue<int>("BlockBrandListBrand.BlockId");
            blockBrand.BrandId = MapValue<int>("BlockBrandListBrand.BrandId");
            blockBrand.Ordinal = MapValue<int>("BlockBrandListBrand.Ordinal");
            blockBrand.Title = MapValue<string>("Brand.Title");
            blockBrand.SetUntouched();
            return blockBrand;
        }
    }
}