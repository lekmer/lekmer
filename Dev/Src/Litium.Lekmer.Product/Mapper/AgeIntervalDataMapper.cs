using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class AgeIntervalDataMapper : DataMapperBase<IAgeInterval>
	{
		public AgeIntervalDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

		protected override IAgeInterval Create()
        {
			var ageInterval = IoC.Resolve<IAgeInterval>();
			ageInterval.Id = MapValue<int>("AgeInterval.AgeIntervalId");
			ageInterval.Title = MapValue<string>("AgeInterval.Title");
			ageInterval.FromMonth = MapValue<int>("AgeInterval.FromMonth");
			ageInterval.ToMonth = MapValue<int>("AgeInterval.ToMonth");
			ageInterval.Ordinal = MapValue<int>("AgeInterval.Ordinal");
			ageInterval.ContentNodeId = MapNullableValue<int?>("AgeInterval.ContentNodeId");
            return ageInterval;
        }
	}
}