﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
    public class BlockBrandProductListDataMapper : DataMapperBase<IBlockBrandProductList>
    {
        private DataMapperBase<IBlock> _blockDataMapper;
        private DataMapperBase<IProductSortOrder> _productSortOrderDataMapper;
        private int _productSortOrderId;


        public BlockBrandProductListDataMapper(IDataReader dataReader)
            : base(dataReader)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            _blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
            _productSortOrderDataMapper = DataMapperResolver.Resolve<IProductSortOrder>(DataReader);
            _productSortOrderId = OrdinalOf("ProductSortOrderId");
        }

        protected override IBlockBrandProductList Create()
        {
            var block = _blockDataMapper.MapRow();
            var blockBrandProductList = IoC.Resolve<IBlockBrandProductList>();
            block.ConvertTo(blockBrandProductList);
            blockBrandProductList.ColumnCount = MapValue<int>("ColumnCount");
            blockBrandProductList.RowCount = MapValue<int>("RowCount");
            blockBrandProductList.ProductSortOrderId = MapValue<int>(_productSortOrderId);
            blockBrandProductList.ProductSortOrder = _productSortOrderDataMapper.MapRow();
            return blockBrandProductList;
        }
    }
}