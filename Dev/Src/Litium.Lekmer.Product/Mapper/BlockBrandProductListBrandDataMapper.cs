﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
    internal class BlockBrandProductListBrandDataMapper : DataMapperBase<IBlockBrandProductListBrand>
    {
        private DataMapperBase<IBlockBrandProductListBrand> _blockDataMapper;

        public BlockBrandProductListBrandDataMapper(IDataReader dataReader)
            : base(dataReader)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            _blockDataMapper = DataMapperResolver.Resolve<IBlockBrandProductListBrand>(DataReader);
        }

        protected override IBlockBrandProductListBrand Create()
        {
            var blockBrand = _blockDataMapper.MapRow();
            //var blockBrandProductListBrand = IoC.Resolve<IBlockBrandProductListBrand>();
            blockBrand.BrandId = MapValue<int>("BrandId");
            blockBrand.BlockId = MapValue<int>("BlockId");
            return blockBrand;
        }
    }
}