using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductTagDataMapper : DataMapperBase<IProductTag>
	{
		public ProductTagDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

		protected override IProductTag Create()
        {
			var productTag = IoC.Resolve<IProductTag>();
			productTag.TagId = MapValue<int>("ProductTag.TagId");
			productTag.ProductId = MapValue<int>("ProductTag.ProductId");
            return productTag;
        }
	}
}