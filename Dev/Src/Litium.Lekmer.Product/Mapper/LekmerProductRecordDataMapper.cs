﻿using System;
using System.Data;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	public class LekmerProductRecordDataMapper : ProductRecordDataMapper
    {
		public LekmerProductRecordDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override IProductRecord Create()
        {
            var product = (ILekmerProductRecord) base.Create();
            product.BrandId = MapNullableValue<int?>("Lekmer.BrandId");
			product.IsBookable = MapValue<bool>("Lekmer.IsBookable");
			product.AgeFromMonth = MapValue<int>("Lekmer.AgeFromMonth");
			product.AgeToMonth = MapValue<int>("Lekmer.AgeToMonth");
            product.IsNewFrom = MapNullableValue<DateTime?>("Lekmer.IsNewFrom");
            product.IsNewTo = MapNullableValue<DateTime?>("Lekmer.IsNewTo");
            product.Measurement = MapNullableValue<string>("Lekmer.Measurement");
            product.BatteryTypeId = MapNullableValue<int?>("Lekmer.BatteryTypeId");
            product.NumberOfBatteries = MapNullableValue<int?>("Lekmer.NumberOfBatteries");
            product.IsBatteryIncluded = MapValue<bool>("Lekmer.IsBatteryIncluded");
            product.ExpectedBackInStock = MapNullableValue<DateTime?>("Lekmer.ExpectedBackInStock");
			product.CreatedDate = MapNullableValue<DateTime?>("Lekmer.CreatedDate");
        	product.SizeDeviationId = MapNullableValue<int?>("Lekmer.SizeDeviationId");
			product.LekmerErpId = MapNullableValue<string>("Lekmer.LekmerErpId");
            return product;
        }
    }
}