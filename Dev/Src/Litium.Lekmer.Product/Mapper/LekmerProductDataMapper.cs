﻿using System;
using System.Data;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
    internal class LekmerProductDataMapper : ProductDataMapper<IProduct>
    {
        public LekmerProductDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override IProduct Create()
        {
            var product = (ILekmerProduct) base.Create();
			product.BrandId = MapNullableValue<int?>("Lekmer.BrandId");
			product.IsBookable = MapValue<bool>("Lekmer.IsBookable");
            product.IsNewFrom = MapNullableValue<DateTime?>("Lekmer.IsNewFrom");
            product.IsNewTo = MapNullableValue<DateTime?>("Lekmer.IsNewTo");
        	product.CreatedDate = MapNullableValue<DateTime?>("Lekmer.CreatedDate");
			product.HasSizes = MapValue<bool>("Lekmer.HasSizes");
			product.UrlTitle = MapValue<string>("Lekmer.UrlTitle");
			product.ParentContentNodeId = MapNullableValue<int?>("Product.ParentContentNodeId");
            return product;
        }
    }
}