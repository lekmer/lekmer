﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
    public class BlockBrandListDataMapper : DataMapperBase<IBlockBrandList>
    {
        private DataMapperBase<IBlock> _blockDataMapper;

        public BlockBrandListDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
            _blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
        }

        protected override IBlockBrandList Create()
        {
            var block = _blockDataMapper.MapRow();
            var blockBrandList = IoC.Resolve<IBlockBrandList>();
            block.ConvertTo(blockBrandList);
            blockBrandList.ColumnCount = MapValue<int>("BlockBrandList.ColumnCount");
            blockBrandList.RowCount = MapValue<int>("BlockBrandList.RowCount");
            blockBrandList.IncludeAllBrands = MapValue<bool>("BlockBrandList.IncludeAllBrands");
            blockBrandList.LinkContentNodeId = MapNullableValue<int?>("BlockBrandList.LinkContentNodeId");
            return blockBrandList;
        }
    }
}