﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;

namespace Litium.Lekmer.Product.Messenger
{
	public class MonitorProductMessenger : MessengerBase<MonitorProductMessageArgs>
	{
		protected MonitorProductEmailTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "MonitorProduct"; }
		}

		protected override Message CreateMessage(MonitorProductMessageArgs messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");

			Template = new MonitorProductEmailTemplate(messageArgs.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};
			message.Recipients.Add(new EmailRecipient
			{
				Name = messageArgs.MonitorProduct.Email,
				Address = messageArgs.MonitorProduct.Email
			});
			return message;
		}

		private string RenderMessage(MonitorProductMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("Message");
			htmlFragment.AddVariable("Product.Url", string.Format(CultureInfo.CurrentCulture, 
				"http://{0}/{1}", messageArgs.Channel.ApplicationName, messageArgs.Product.LekmerUrl));
			htmlFragment.AddVariable("Product.Title", messageArgs.Product.DisplayTitle);
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}