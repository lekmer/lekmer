﻿using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Framework.Messaging;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Messenger
{
    public class WishListMessenger : ScensumMessengerBase<WishListMessageArgs>
    {
        protected IFormatter Formatter { get; private set; }
        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        protected WishListEmailTemplate Template { get; set; }

        protected virtual WishListEmailTemplate GetTemplate(IChannel channel)
        {
            return new WishListEmailTemplate(channel);
        }

        protected override Message CreateMessage(WishListMessageArgs messageArgs)
        {
            if (messageArgs == null) throw new ArgumentNullException("messageArgs");

            var channelServise = IoC.Resolve<IChannelSharedService>();
            IChannel channel = channelServise.GetById(messageArgs.Channel.Id);
            Template = GetTemplate(channel);

            string body = RenderMessage(messageArgs);

            string subject = Template.GetFragment("Subject").Render();
            string fromName = string.IsNullOrEmpty(messageArgs.SenderName) ? messageArgs.SenderEmail : messageArgs.SenderName;
            string fromEmail = messageArgs.SenderEmail;
            var message = new EmailMessage
            {
                Body = body,
                ProviderName = "email",
                FromName = fromName,
                FromEmail = fromEmail,
                Subject = subject,
                Type = MessageType.Single
            };

            if (messageArgs.MailToMysef)
            {
                if (!messageArgs.Receivers.Contains(messageArgs.Senders))
                    messageArgs.Receivers.Add(messageArgs.Senders);
            }

            foreach (var receiver in messageArgs.Receivers)
            {
                message.Recipients.Add(new EmailRecipient
                {
                    Name = receiver,
                    Address = receiver
                });
            }
            return message;
        }

        protected virtual string RenderSubject(WishListMessageArgs messageArgs)
        {
            Fragment fragmentSubject = Template.GetFragment("Subject");
            fragmentSubject.AddVariable("Subject", messageArgs.Subject, VariableEncoding.None);
            return fragmentSubject.Render();
        }

        protected virtual string RenderMessage(WishListMessageArgs messageArgs)
        {
            Fragment fragmentContent = Template.GetFragment("Content");
            fragmentContent.AddVariable("Message", messageArgs.Message, VariableEncoding.None);
            fragmentContent.AddVariable("SenderName",  messageArgs.SenderName);
            fragmentContent.AddVariable("WishListPageUrl", messageArgs.WishListPageUrl, VariableEncoding.None);
            return fragmentContent.Render();
        }

        protected override string MessengerName
        {
            get { return "WishListMessenger"; }
        }
    }
}
