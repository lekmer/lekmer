﻿using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Messenger
{
	public class MonitorProductMessageArgs
	{
		public IChannel Channel { get; set; }
		public IMonitorProduct MonitorProduct { get; set; }
		public ILekmerProduct Product { get; set; }

		public MonitorProductMessageArgs(IChannel channel, IMonitorProduct monitorProduct, ILekmerProduct product)
		{
			Channel = channel;
			MonitorProduct = monitorProduct;
			Product = product;
		}
	}
}
