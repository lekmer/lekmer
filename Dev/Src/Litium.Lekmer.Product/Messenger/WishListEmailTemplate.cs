﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Messenger
{

    public class WishListEmailTemplate : Template
    {
        public WishListEmailTemplate(int templateId, IChannel channel)
            : base(templateId, channel, false)
        {
        }

        public WishListEmailTemplate(IChannel channel)
            : base("WishListEmail", channel, false)
        {
        }
    }
}
