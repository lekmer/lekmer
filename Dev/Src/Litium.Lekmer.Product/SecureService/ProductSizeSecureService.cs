﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductSizeSecureService : IProductSizeSecureService
	{
		protected ProductSizeRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductSizeSecureService(ProductSizeRepository repository, IAccessValidator accessValidator)
        {
            Repository = repository;
            AccessValidator = accessValidator;
        }

        public Collection<IProductSize> GetAllByProduct(int productId)
        {
            return Repository.GetAllByProduct(productId);
        }

		public void Save(ISystemUserFull systemUserFull, IEnumerable<IProductSize> productSizes)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				foreach (var productSize in productSizes)
				{
					Repository.Save(productSize);
				}
				transactedOperation.Complete();
			}
			ProductSizeCollectionCache.Instance.Flush();
			ProductSizeCache.Instance.Flush();
		}
	}
}
