﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class SizeSecureService : ISizeSecureService
	{
		protected SizeRepository Repository { get; private set; }

		public SizeSecureService(SizeRepository sizeRepository)
		{
			Repository = sizeRepository;
		}

		public Collection<ISize> GetAll()
		{
			return Repository.GetAll();
		}
	}
}
