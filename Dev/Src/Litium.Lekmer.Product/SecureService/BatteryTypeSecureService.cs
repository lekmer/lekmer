﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
    public class BatteryTypeSecureService : IBatteryTypeSecureService
	{
		protected BatteryTypeRepository Repository { get; private set; }

        public BatteryTypeSecureService(BatteryTypeRepository repository)
        {
            Repository = repository;
        }

        public Collection<IBatteryType> GetAll()
        {
            return Repository.GetAll();
        }
	}
}
