﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product.SecureService
{
    public class BlockBrandListBrandSecureService : IBlockBrandListBrandSecureService
    {
        protected BlockBrandListBrandRepository Repository { get; private set; }

        public BlockBrandListBrandSecureService(BlockBrandListBrandRepository repository)
        {
            Repository = repository;
        }

        public Collection<IBlockBrandListBrand> GetAllByBlock(int blockId)
        {
            return Repository.GetAllByBlockSecure(blockId);
        }

        public void Save(int blockId, Collection<IBlockBrandListBrand> blockBrands)
        {
            if (blockBrands == null) throw new ArgumentNullException("blockBrands");
            using (var transaction = new TransactedOperation())
            {
                Repository.DeleteAll(blockId);
                foreach (var blockBrand in blockBrands)
                {
                    blockBrand.BlockId = blockId;
                    Repository.Save(blockBrand);
                }
                transaction.Complete();
            }
        }
    }
}