﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class ProductUrlSecureService : IProductUrlSecureService
	{
		protected ProductUrlRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductUrlSecureService(ProductUrlRepository repository, IAccessValidator accessValidator)
        {
            Repository = repository;
            AccessValidator = accessValidator;
        }

        public Collection<IProductUrl> GetAllByProduct(int productId)
        {
            return Repository.GetAllByProduct(productId);
        }

		public IProductUrl Create(int productId, int languageId, string urlTitle)
		{
			var productUrl = IoC.Resolve<IProductUrl>();
			productUrl.ProductId = productId;
			productUrl.LanguageId = languageId;
			productUrl.UrlTitle = urlTitle;
			productUrl.SetNew();
			return productUrl;
		}

		public bool Check(IProductUrl productUrl)
		{
			return Repository.Check(productUrl) > 0;
		}

		public void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<IProductUrl> productUrls)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				Repository.DeleteAllByProduct(productId);
				foreach (var productUrl in productUrls)
				{
					Repository.Save(productUrl);
				}
				transactedOperation.Complete();
			}

			ProductUrlIdCache.Instance.Flush();
		}
	}
}
