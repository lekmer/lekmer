﻿using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Media;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Cache;
using Litium.Scensum.Product;
using System.Collections.Generic;
using Litium.Lekmer.Product.Repository;
using System;
using Litium.Framework.Transaction;
using Litium.Scensum.Product.Repository;
using Litium.Scensum.Statistics;

namespace Litium.Lekmer.Product
{
    public class LekmerProductSecureService : ProductSecureService, ILekmerProductSecureService
    {
        protected ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> ProductWebShopTitleTranslationSecureService { get; private set; }
        protected ProductTranslationGenericSecureService<ProductShortDescriptionTranslationRepository> ProductShortDescriptionTranslationSecureService { get; private set; }
        protected ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> ProductDescriptionTranslationSecureService { get; private set; }
        protected ProductTranslationGenericSecureService<ProductSeoSettingTitleTranslationRepository> ProductSeoSettingTitleTranslationSecureService { get; private set; }
        protected ProductTranslationGenericSecureService<ProductSeoSettingDescriptionTranslationRepository> ProductSeoSettingDescriptionTranslationSecureService { get; private set; }
        protected ProductTranslationGenericSecureService<ProductSeoSettingKeywordsTranslationRepository> ProductSeoSettingKeywordsTranslationSecureService { get; private set; }
        protected ProductTranslationGenericSecureService<ProductMeasurementTranslationRepository> ProductMeasurementTranslationSecureService { get; private set; }
        protected IIconSecureService IconSecureService { get; private set; }
        protected ITagSecureService TagSecureService { get; private set; }
        protected IProductSizeSecureService ProductSizeSecureService { get; private set; }
        protected IImageSecureService ImageSecureService { get; private set; }
        protected IProductUrlSecureService ProductUrlSecureService { get; private set; }

        public LekmerProductSecureService(
            IAccessValidator accessValidator,
            ProductRepository repository,
            IProductSeoSettingSecureService productSeoSettingSecureService,
            IProductSiteStructureRegistrySecureService productSiteStructureRegistrySecureService,
            IProductImageSecureService productImageSecureService,
            IStoreProductSecureService storeProductSecureService,
            ProductTranslationGenericSecureService<ProductWebShopTitleTranslationRepository> productWebShopTitleTranslationSecureService,
            ProductTranslationGenericSecureService<ProductShortDescriptionTranslationRepository> productShortDescriptionTranslationSecureService,
            ProductTranslationGenericSecureService<ProductDescriptionTranslationRepository> productDescriptionTranslationSecureService,
            ProductTranslationGenericSecureService<ProductSeoSettingTitleTranslationRepository> productSeoSettingTitleTranslationSecureService,
            ProductTranslationGenericSecureService<ProductSeoSettingDescriptionTranslationRepository> productSeoSettingDescriptionTranslationSecureService,
            ProductTranslationGenericSecureService<ProductSeoSettingKeywordsTranslationRepository> productSeoSettingKeywordsTranslationSecureService,
            ProductTranslationGenericSecureService<ProductMeasurementTranslationRepository> productMeasurementTranslationSecureService,
            IIconSecureService iconSecureService,
            ITagSecureService tagSecureService,
            IProductSizeSecureService productSizeSecureService,
            IProductUrlSecureService productUrlSecureService, IImageCacheCleaner imageCacheCleaner, IImageSecureService imageSecureService)
            : base(accessValidator, repository, productSeoSettingSecureService, productSiteStructureRegistrySecureService,
                productImageSecureService, storeProductSecureService, imageCacheCleaner)
        {
            ProductWebShopTitleTranslationSecureService = productWebShopTitleTranslationSecureService;
            ProductShortDescriptionTranslationSecureService = productShortDescriptionTranslationSecureService;
            ProductDescriptionTranslationSecureService = productDescriptionTranslationSecureService;
            ProductSeoSettingTitleTranslationSecureService = productSeoSettingTitleTranslationSecureService;
            ProductSeoSettingDescriptionTranslationSecureService = productSeoSettingDescriptionTranslationSecureService;
            ProductSeoSettingKeywordsTranslationSecureService = productSeoSettingKeywordsTranslationSecureService;
            ProductMeasurementTranslationSecureService = productMeasurementTranslationSecureService;
            IconSecureService = iconSecureService;
            TagSecureService = tagSecureService;
            ProductSizeSecureService = productSizeSecureService;
            ProductUrlSecureService = productUrlSecureService;
            ImageSecureService = imageSecureService;
        }

        public override IProductRecord GetRecordById(int channelId, int productId)
        {
            var product = base.GetRecordById(channelId, productId) as ILekmerProductRecord;
            if (product != null)
            {
                product.Icons = IconSecureService.GetAllByProduct(productId);
            }
            return product;
        }

        public void Save(ISystemUserFull systemUserFull,
            IProductRecord product,
            Collection<IRelationList> relationLists,
            IProductSeoSetting seoSetting,
            Collection<IProductImageGroupFull> imageGroups,
            IEnumerable<IStoreProduct> storeProductItems,
            Collection<ITranslationGeneric> webShopTitleTranslations,
            Collection<ITranslationGeneric> shortDescriptionTranslations,
            Collection<ITranslationGeneric> descriptionTranslations,
            Collection<ITranslationGeneric> seoTitleTranslations,
            Collection<ITranslationGeneric> seoDescriptionTranslations,
            Collection<ITranslationGeneric> seoKeywordsTranslations,
            Collection<ITranslationGeneric> measurementTranslations,
            Collection<int> icons,
            Collection<int> tags,
            IEnumerable<IProductSize> productSizes,
            IEnumerable<IProductUrl> productUrls)
        {
            if (product == null) throw new ArgumentNullException("product");
            if (relationLists == null) throw new ArgumentNullException("relationLists");
            if (seoSetting == null) throw new ArgumentNullException("seoSetting");
            if (icons == null) throw new ArgumentNullException("icons");
            if (tags == null) throw new ArgumentNullException("tags");
            if (productSizes == null) throw new ArgumentNullException("productSizes");
            if (productUrls == null) throw new ArgumentNullException("productUrls");

            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

            using (TransactedOperation transactedOperation = new TransactedOperation())
            {
                base.Save(systemUserFull, product, relationLists, seoSetting, imageGroups, storeProductItems);

                ProductWebShopTitleTranslationSecureService.Save(systemUserFull, webShopTitleTranslations);
                ProductShortDescriptionTranslationSecureService.Save(systemUserFull, shortDescriptionTranslations);
                ProductDescriptionTranslationSecureService.Save(systemUserFull, descriptionTranslations);

                ProductSeoSettingTitleTranslationSecureService.Save(systemUserFull, seoTitleTranslations);
                ProductSeoSettingDescriptionTranslationSecureService.Save(systemUserFull, seoDescriptionTranslations);
                ProductSeoSettingKeywordsTranslationSecureService.Save(systemUserFull, seoKeywordsTranslations);

                ProductMeasurementTranslationSecureService.Save(systemUserFull, measurementTranslations);
                IconSecureService.Save(systemUserFull, product.Id, icons);
                TagSecureService.Save(systemUserFull, product.Id, tags);
                ProductSizeSecureService.Save(systemUserFull, productSizes);
                ProductUrlSecureService.Save(systemUserFull, product.Id, productUrls);

                transactedOperation.Complete();
            }

            FilterProductCollectionCache.Instance.Flush();
        }
        public void UpdatePopularity(Collection<IChannel> channels, DateTime dateTimeFrom, DateTime dateTimeTo)
        {
            var repositoryLocal = (LekmerProductRepository)Repository;
            foreach (IChannel channel in channels)
            {
                repositoryLocal.UpdatePopularity(channel.Id, dateTimeFrom, dateTimeTo);
            }
            FilterProductCollectionCache.Instance.Flush();
        }

        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "full")]
        protected override void SaveImageGroups(int productId, Collection<IProductImageGroupFull> imageGroups)
        {
            ProductImageSecureService.DeleteAllByProduct(productId);
            foreach (var group in imageGroups)
            {
                foreach (IProductImage image in group.ProductImages)
                {
                    if (((ILekmerImage)image.Image).IsLink)
                    {
                        ((ILekmerImageSecureService)ImageSecureService).SaveMovie((ILekmerImage)image.Image);
                        image.MediaId = image.Image.Id;
                    }
                    ProductImageSecureService.Save(image);
                }
            }
        }
    }
}
