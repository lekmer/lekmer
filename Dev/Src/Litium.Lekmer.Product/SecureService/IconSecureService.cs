﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public class IconSecureService : IIconSecureService
    {
        protected IconRepository Repository { get; private set; }
        protected IAccessValidator AccessValidator { get; private set; }

        public IconSecureService(IconRepository repository, IAccessValidator accessValidator)
        {
            Repository = repository;
            AccessValidator = accessValidator;
        }

        public Collection<IIcon> GetAll()
        {
            return Repository.GetAll();
        }

        public Collection<IIcon> GetAllByProduct(int productId)
        {
            return Repository.GetAllByProductSecure(productId);
        }

        public void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<int> iconsId)
        {
            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
            using (TransactedOperation transactedOperation = new TransactedOperation())
            {
                Repository.Delete(productId);
                foreach (var iconId in iconsId)
                {
                    Repository.Insert(productId, iconId);
                }
                transactedOperation.Complete();
            }
        }
    }
}
