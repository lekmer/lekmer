﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class PriceIntervalSecureService : IPriceIntervalSecureService
	{
		protected PriceIntervalRepository Repository { get; private set; }

		public PriceIntervalSecureService(PriceIntervalRepository repository)
        {
            Repository = repository;
        }

		public virtual Collection<IPriceInterval> GetAll()
		{
			return Repository.GetAllSecure();
		}
	}
}
