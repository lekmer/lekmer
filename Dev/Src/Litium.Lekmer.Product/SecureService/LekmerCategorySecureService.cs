﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerCategorySecureService : CategorySecureService, ILekmerCategorySecureService
	{
		public LekmerCategorySecureService(IAccessValidator accessValidator, CategoryRepository repository, ICategorySiteStructureRegistrySecureService categorySiteStructureRegistrySecureService) : base(accessValidator, repository, categorySiteStructureRegistrySecureService)
		{
		}

		public Collection<ITranslationGeneric> GetAllTranslationsByCategory(int id)
		{
			return ((LekmerCategoryRepository) Repository).GetAllTranslationsByCategory(id);
		}

		public void Update(ISystemUserFull systemUserFull, ICategoryRecord category, IEnumerable<ITranslationGeneric> translations)
		{
			if (translations == null) throw new ArgumentNullException("translations");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentCategory);
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				base.Update(systemUserFull, category);
				foreach (var translation in translations)
				{
					((LekmerCategoryRepository) Repository).SaveTranslation(translation);
				}
				transactedOperation.Complete();
			}
		}
	}
}
