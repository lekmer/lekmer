﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    public class TagSecureService : ITagSecureService
	{
		protected TagRepository Repository { get; private set; }
        protected IAccessValidator AccessValidator { get; private set; }

        public TagSecureService(TagRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
            AccessValidator = accessValidator;
		}

        public Collection<ITag> GetAllByTagGroup(int tagGroupId)
        {
            return Repository.GetAllByTagGroupSecure(tagGroupId);
        }

        public Collection<ITag> GetAllByProductAndTagGroup(int productId, int tagGroupId)
        {
            return Repository.GetAllByProductAndTagGroupSecure(productId, tagGroupId);
        }

        public Collection<ITranslationGeneric> GetAllTranslationsByTag(int tagId)
        {
            return Repository.GetAllTranslationsByTag(tagId);
        }

        public ITag Create(int groupId, string value)
        {
            var tag = IoC.Resolve<ITag>();
            tag.TagGroupId = groupId;
            tag.Value = value;
            return tag;
        }

        public void Delete(ISystemUserFull systemUserFull, int tagId)
        {
            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
            using (var transactedOperation = new TransactedOperation())
            {
                Repository.Delete(tagId);
                transactedOperation.Complete();
            }
			ProductTagGroupCollectionCache.Instance.Flush();
        }

        public int Save(ISystemUserFull systemUserFull, ITag tag, IEnumerable<ITranslationGeneric> translations)
        {
            if (translations == null) throw new ArgumentNullException("translations");

            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
            using (var transactedOperation = new TransactedOperation())
            {
                tag.Id = Repository.Save(tag);
                if (tag.Id > 0)
                {
                    foreach (var translation in translations)
                    {
                        translation.Id = tag.Id;
                        Repository.SaveTranslation(translation);
                    }
                }
                transactedOperation.Complete();
            }
			ProductTagGroupCollectionCache.Instance.Flush();
            return tag.Id;
        }

        public void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<int> tagIds)
        {
            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
            using (var transactedOperation = new TransactedOperation())
            {
                Repository.DeleteAllProductTag(productId);
                foreach (var tagId in tagIds)
                {
                    Repository.InsertProductTag(productId, tagId);
                }
                transactedOperation.Complete();
            }
			ProductTagGroupCollectionCache.Instance.Remove(productId);
        }
	}
}