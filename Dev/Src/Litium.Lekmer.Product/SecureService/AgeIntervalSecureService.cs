﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class AgeIntervalSecureService : IAgeIntervalSecureService
	{
		protected AgeIntervalRepository Repository { get; private set; }

		public AgeIntervalSecureService(AgeIntervalRepository repository)
        {
            Repository = repository;
        }

		public virtual Collection<IAgeInterval> GetAll()
		{
			return Repository.GetAllSecure();
		}
	}
}
