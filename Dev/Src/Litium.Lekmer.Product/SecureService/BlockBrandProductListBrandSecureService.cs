﻿using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.SecureService
{
    public class BlockBrandProductListBrandSecureService : IBlockBrandProductListBrandSecureService
    {
        protected BlockBrandProductListBrandRepository Repository { get; private set; }


        public BlockBrandProductListBrandSecureService(
            BlockBrandProductListBrandRepository repository)
        {
            Repository = repository;
        }


        public virtual IBlockBrandProductListBrand Create()
        {
            var blockBrand = IoC.Resolve<IBlockBrandProductListBrand>();
            blockBrand.Status = BusinessObjectStatus.Untouched;
            return blockBrand;
        }

        public Collection<IBrand> GetAllByBlockInverted(int channelId, int blockId)
        {
            return Repository.GetAllByBlockInverted(channelId, blockId);
        }

        public Collection<IBrand> GetAllByBlock(int channelId, int blockId)
        {
            return Repository.GetAllByBlockSecure(channelId, blockId);
        }

        public void Save(int blockId, Collection<IBlockBrandProductListBrand> blockBrands)
        {
            using (TransactedOperation transaction = new TransactedOperation())
            {
            	Delete(blockId);
                foreach (var brand in blockBrands)
                {
                    brand.BlockId = blockId;
                    Repository.Save(brand);
                }
                transaction.Complete();
            }
        }

        public void Delete(int blockId)
        {
            using (TransactedOperation transaction = new TransactedOperation())
            {
                Repository.Delete(blockId);
                transaction.Complete();
            }
        }
    }
}