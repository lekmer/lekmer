﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.SecureService
{
    public class BlockBrandProductListSecureService : IBlockBrandProductListSecureService, IBlockCreateSecureService,
                                                      IBlockDeleteSecureService
    {
        protected IAccessValidator AccessValidator { get; private set; }
        protected BlockBrandProductListRepository Repository { get; private set; }
        protected IBlockBrandProductListBrandSecureService BrandProductListBrandSecureService { get; private set; }
        protected IAccessSecureService AccessService { get; private set; }
        protected IBlockSecureService BlockService { get; private set; }

        public BlockBrandProductListSecureService(
            IAccessValidator accessValidator,
            BlockBrandProductListRepository repository,
            IBlockBrandProductListBrandSecureService brandProductListBrandSecureService,
            IAccessSecureService accessSecureService,
            IBlockSecureService blockSecureService)
        {
            AccessValidator = accessValidator;
            Repository = repository;
            BrandProductListBrandSecureService = brandProductListBrandSecureService;
            AccessService = accessSecureService;
            BlockService = blockSecureService;
        }


        public IBlockBrandProductList Create()
        {
            if (AccessService == null)
                throw new InvalidOperationException("AccessService must be set before calling Create.");

            var block = IoC.Resolve<IBlockBrandProductList>();
            block.AccessId = AccessService.GetByCommonName("All").Id;
            block.Status = BusinessObjectStatus.New;
            return block;
        }

        public int Save(ISystemUserFull systemUserFull, IBlockBrandProductList block,
                        Collection<IBlockBrandProductListBrand> blockBrands)
        {
            if (block == null)
                throw new ArgumentNullException("block");
            if (BlockService == null)
                throw new InvalidOperationException("BlockService must be set before calling Save.");

            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

            using (TransactedOperation transaction = new TransactedOperation())
            {
				block.Id = BlockService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;
                Repository.Save(block);
                BrandProductListBrandSecureService.Save(block.Id, blockBrands);
                transaction.Complete();
            }
            //TODO 
            //cache on BlockBrandProductList
			//BlockBrandProductCollectionCache.Instance.Remove()
            return block.Id;
        }

        public IBlockBrandProductList GetById(int blockId)
        {
            return Repository.GetByIdSecure(blockId);
        }


        public IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId,
                              int blockTypeId, string title)
        {
            if (title == null) throw new ArgumentNullException("title");

            IBlockBrandProductList block = Create();
            block.ContentNodeId = contentNodeId;
            block.ContentAreaId = contentAreaId;
            block.BlockTypeId = blockTypeId;
            block.BlockStatusId = (int) BlockStatusInfo.Offline;
            block.Title = title;
            block.TemplateId = null;
            block.ColumnCount = 1;
            block.RowCount = 1;
            block.ProductSortOrderId = (int) ProductSortOrderConstant.TitleAscending;
            block.Id = Save(systemUserFull, block, new Collection<IBlockBrandProductListBrand>());
            return block;
        }


        public void Delete(ISystemUserFull systemUserFull, int blockId)
        {
            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
            using (TransactedOperation transaction = new TransactedOperation())
            {
                BrandProductListBrandSecureService.Delete(blockId);
                Repository.Delete(blockId);
                transaction.Complete();
            }
        }
    }
}