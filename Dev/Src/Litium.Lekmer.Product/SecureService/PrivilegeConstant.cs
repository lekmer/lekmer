﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Litium.Lekmer.Brand.SecureService
{
	[Serializable]
	public static class PrivilegeConstant
	{
        private static string _assortmentBrand = "Assortment.Brand";

		public static string AssortmentBrand { get { return _assortmentBrand; } }

	}
}