﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
    public class TagGroupSecureService : ITagGroupSecureService
	{
		protected TagGroupRepository Repository { get; private set; }
		protected ITagSecureService TagSecureService { get; private set; }

        public TagGroupSecureService(TagGroupRepository repository, ITagSecureService tagSecureService)
		{
			Repository = repository;
            TagSecureService = tagSecureService;
		}

		public Collection<ITagGroup> GetAll()
		{
            var tagGroups = Repository.GetAll();
            foreach (var tagGroup in tagGroups)
            {
                tagGroup.Tags = TagSecureService.GetAllByTagGroup(tagGroup.Id);
            }
            return tagGroups;
		}
	}
}