﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
    public class BlockLatestProductAddedToBasketSecureService : IBlockLatestProductAddedToBasketSecureService, IBlockCreateSecureService,
                                                IBlockDeleteSecureService
    {
        protected IAccessValidator AccessValidator { get; private set; }
        protected BlockLatestProductAddedToBasketRepository Repository { get; private set; }
        protected IAccessSecureService AccessSecureService { get; private set; }
        protected IBlockSecureService BlockSecureService { get; private set; }


        public BlockLatestProductAddedToBasketSecureService(
            IAccessValidator accessValidator,
            BlockLatestProductAddedToBasketRepository repository,
            IAccessSecureService accessSecureService,
            IBlockSecureService blockSecureService)
        {
            AccessValidator = accessValidator;
            Repository = repository;
            AccessSecureService = accessSecureService;
            BlockSecureService = blockSecureService;
        }

        public virtual IBlockLatestProductAddedToBasket Create()
        {
            if (AccessSecureService == null)
                throw new InvalidOperationException("AccessSecureService must be set before calling Create.");

            var blockTopList = IoC.Resolve<IBlockLatestProductAddedToBasket>();
            blockTopList.AccessId = AccessSecureService.GetByCommonName("All").Id;
            blockTopList.Status = BusinessObjectStatus.New;
            return blockTopList;
        }

        public virtual IBlockLatestProductAddedToBasket GetById(int id)
        {
            return Repository.GetByIdSecure((id));
        }

        public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
        {
            IBlockLatestProductAddedToBasket blockLatestProductAddedToBasket = Create();
            blockLatestProductAddedToBasket.ContentNodeId = contentNodeId;
            blockLatestProductAddedToBasket.ContentAreaId = contentAreaId;
            blockLatestProductAddedToBasket.BlockTypeId = blockTypeId;
            blockLatestProductAddedToBasket.BlockStatusId = (int)BlockStatusInfo.Offline;
            blockLatestProductAddedToBasket.Title = title;
            blockLatestProductAddedToBasket.TemplateId = null;
            blockLatestProductAddedToBasket.RedirectToCategory = false;
            blockLatestProductAddedToBasket.RedirectToMainCategory = false;
            blockLatestProductAddedToBasket.RedirectToStartPage = true;
            blockLatestProductAddedToBasket.Id = Save(systemUserFull, blockLatestProductAddedToBasket);
            return blockLatestProductAddedToBasket;
        }
        public virtual int Save(ISystemUserFull systemUserFull, IBlockLatestProductAddedToBasket block)
        {
            if (block == null) throw new ArgumentNullException("block");
            if (BlockSecureService == null)
                throw new InvalidOperationException("BlockSecureService must be set before calling Save.");

            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

            using (var transactedOperation = new TransactedOperation())
            {
                block.Id = BlockSecureService.Save(systemUserFull, block);
                if (block.Id == -1)
                    return block.Id;

                Repository.Save(block);

                transactedOperation.Complete();
            }
            return block.Id;
        }

        public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
        {
            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
            using (var transactedOperation = new TransactedOperation())
            {
                Repository.Delete(blockId);
                transactedOperation.Complete();
            }
        }
    }
}