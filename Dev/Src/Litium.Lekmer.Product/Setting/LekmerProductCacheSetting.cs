using System;
using Litium.Framework.Setting;

namespace Litium.Lekmer.Product.Setting
{
    public sealed class LekmerProductCacheSetting : LekmerProductCacheSettingBase
	{
		#region Singleton

		private static readonly LekmerProductCacheSetting _instance = new LekmerProductCacheSetting();

        private LekmerProductCacheSetting()
        {
        }

	    public static LekmerProductCacheSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "ProductCache";

		protected override string StorageName
		{
			get { return "LekmerCacheManagement"; }
		}

		public TimeSpan ReadPagePauseDuration
		{
			get { return TimeSpan.FromSeconds(GetInt32(_groupName, "ReadPagePauseDurationInSeconds", 10)); }
		}

		public TimeSpan RefreshIntervalTolerance
		{
			get { return TimeSpan.FromMinutes(GetInt32(_groupName, "RefreshIntervalToleranceInMinutes", 30)); }
		}

		public bool ProductCachePopulateWorkerClosed
		{
			get { return GetBoolean(_groupName, "ProductCachePopulateWorkerClosed", false); }
		}

	    public TimeSpan ExpirationInMinutes
	    {
            get { return TimeSpan.FromMinutes(GetInt32(_groupName, "ExpirationInMinutes", 60)); }   
	    }
	}
}