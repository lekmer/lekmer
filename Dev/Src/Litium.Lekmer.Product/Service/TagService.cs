using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class TagService : ITagService
	{
        protected TagRepository Repository { get; private set; }

		public TagService(TagRepository repository)
        {
            Repository = repository;
        }

		public Collection<ITag> GetAllByTagGroup(IUserContext context, int tagGroupId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return TagGroupTagCache.Instance.TryGetItem(
				new TagGroupTagKey(context.Channel.Id, tagGroupId),
				delegate { return Repository.GetAllByTagGroup(context.Channel.Language.Id, tagGroupId); });
		}

		public Collection<ITag> GetAllByTagGroupAndProduct(IUserContext context, int tagGroupId, int productId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return Repository.GetAllByTagGroupAndProduct(context.Channel.Language.Id, tagGroupId, productId);
		}
	}
}