﻿using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Service
{
    public class BlockBrandProductListService : IBlockBrandProductListService
    {
        #region IBlockBrandProductListService Members

        public IBlockBrandProductList GetById(IUserContext context, int blockId)
        {
            return Repository.GetById(context.Channel, blockId);
        }

        protected BlockBrandProductListRepository Repository { get; private set; }


        public BlockBrandProductListService(BlockBrandProductListRepository repository)
        {
            Repository = repository;
        }
    }

    #endregion
}