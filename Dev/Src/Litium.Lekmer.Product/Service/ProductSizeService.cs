﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductSizeService : IProductSizeService
	{
		protected ProductSizeRepository Repository { get; private set; }

		public ProductSizeService(ProductSizeRepository repository)
		{
			Repository = repository;
		}

		public Collection<IProductSize> GetAllByProduct(IUserContext context, int productId)
		{
			return ProductSizeCollectionCache.Instance.TryGetItem(
				new ProductSizeCollectionKey(productId),
				delegate { return Repository.GetAllByProduct(productId); });
		}

		public IProductSize GetById(int productId, int sizeId)
		{
			return ProductSizeCache.Instance.TryGetItem(
				   new ProductSizeKey(productId, sizeId),
				   delegate { return Repository.GetById(productId, sizeId); });
		}

		public Collection<ProductSizeRelation> GetAll(IUserContext userContext)
		{
			return Repository.GetAll();
		}
	}
}
