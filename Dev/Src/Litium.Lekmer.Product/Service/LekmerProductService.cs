﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.BusinessObject;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Cache;
using Litium.Scensum.Product.Repository;
using Litium.Scensum.SiteStructure;
using Convert=Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
    public class LekmerProductService : ProductService, ILekmerProductService
    {
        protected IBrandService BrandService { get; private set; }
        protected IIconService IconService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
    	protected IContentNodeService ContentNodeService { get; private set; }
		protected ITagGroupService TagGroupService { get; set; }

    	public LekmerProductService(
            ProductRepository repository, ICategoryService categoryService,
            IProductCampaignProcessor campaignProcessor, IBrandService brandService, IIconService iconService,
			IProductSizeService productSizeService, IContentNodeService contentNodeService, ITagGroupService tagGroupService)
            : base(repository, categoryService, campaignProcessor)
        {
            BrandService = brandService;
            IconService = iconService;
        	ProductSizeService = productSizeService;
        	ContentNodeService = contentNodeService;
    		TagGroupService = tagGroupService;
        }

		public ILekmerProduct Create()
		{
			return (ILekmerProduct)IoC.Resolve<IProduct>();
		}

		protected override IProduct GetByIdCore(IUserContext context, int productId)
		{
			IProduct product = base.GetByIdCore(context, productId);
			if (product == null) return null;

			var lekmerProduct = (ILekmerProduct)product;
			if (lekmerProduct.BrandId.HasValue)
			{
				lekmerProduct.Brand = BrandService.GetById(context, lekmerProduct.BrandId.Value);
			}

			AppendLekmerUrl(context, lekmerProduct);

			return lekmerProduct;
		}

		protected override IProductView GetViewByIdCore(IUserContext context, int productId)
		{
			IProductView product = base.GetViewByIdCore(context, productId);
			if (product == null) return null;

			var lekmerProduct = (ILekmerProductView)product;
			if (lekmerProduct.BrandId.HasValue)
			{
				lekmerProduct.Brand = BrandService.GetById(context, lekmerProduct.BrandId.Value);
			}
			lekmerProduct.Icons = IconService.GetAllByProduct(context, productId);
			lekmerProduct.ProductSizes = ProductSizeService.GetAllByProduct(context, productId);
			lekmerProduct.TagGroups = TagGroupService.GetAllByProduct(context, productId);
			
			AppendLekmerUrl(context, lekmerProduct);

			return lekmerProduct;
		}

		protected LekmerProductViewCollection GetAllViewByIds(IUserContext context, IEnumerable<int> productIds)
		{
			LekmerProductViewCollection productCollection= ((LekmerProductRepository)Repository).GetAllViewByIdList(
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(productIds),
				Convert.DefaultListSeparator,
				1,
				int.MaxValue);

			foreach (ILekmerProductView lekmerProductView in productCollection)
			{
				if (lekmerProductView.BrandId.HasValue)
				{
					lekmerProductView.Brand = BrandService.GetById(context, lekmerProductView.BrandId.Value);
				}
				lekmerProductView.Icons = IconService.GetAllByProduct(context, lekmerProductView.Id);
				lekmerProductView.ProductSizes = ProductSizeService.GetAllByProduct(context, lekmerProductView.Id);
				lekmerProductView.TagGroups = TagGroupService.GetAllByProduct(context, lekmerProductView.Id);

				AppendLekmerUrl(context, lekmerProductView);
			}

			return productCollection;
		}

		public virtual ProductCollection PopulateViewProductsForExport(IUserContext context, ProductIdCollection productIds)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");
			if (ProductCampaignProcessor == null)
				throw new InvalidOperationException("ProductCampaignProcessor cannot be null.");

			var productsPopulated = new Dictionary<int, IProductView>();

			LekmerProductViewCollection productCollection = ((LekmerProductRepository)Repository).GetAllViewByIdList(
			context.Channel.Id,
			context.Customer,
			Convert.ToStringIdentifierList(productIds),
			Convert.DefaultListSeparator,
			1,
			int.MaxValue);

			foreach (ILekmerProductView lekmerProductView in productCollection)
			{
				if (lekmerProductView.BrandId.HasValue)
				{
					lekmerProductView.Brand = BrandService.GetById(context, lekmerProductView.BrandId.Value);
				}

				AppendLekmerUrl(context, lekmerProductView);
				productsPopulated.Add(lekmerProductView.Id, lekmerProductView);
			}

			// Return populated products in the same order as they were passed to the method.
			var products = new ProductCollection();
			foreach (int productId in productIds)
			{
				IProductView product;
				if (productsPopulated.TryGetValue(productId, out product))
				{
					ProductCampaignProcessor.Process(context, product);
					products.Add(product);
				}
			}
			products.TotalCount = productIds.TotalCount;
			return products;
		}

        public virtual ProductCollection PopulateWishListProducts(IUserContext context, ProductIdCollection productIds)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (productIds == null) throw new ArgumentNullException("productIds");
            if (ProductCampaignProcessor == null)
                throw new InvalidOperationException("ProductCampaignProcessor cannot be null.");

            var productsPopulated = new Dictionary<int, IProductView>();
            var productsNotInCache = new List<int>();

            // No caching when signed in.
            if (context.Customer != null)
            {
                productsNotInCache = new List<int>(productIds);
            }
            else
            {
                // Tries to get products from cache, otherwise adds them to fetching queue.
                foreach (int productId in productIds)
                {
                    IProductView product = ProductViewCache.Instance.GetData(new ProductKey(context.Channel.Id, productId));

                    if (product != null)
                    {
                        productsPopulated.Add(productId, product);
                    }
                    else
                    {
                        productsNotInCache.Add(productId);
                    }
                }
            }

            // Fetch the products that wasn't in the cache.
            if (productsNotInCache.Count > 0)
            {
                Collection<IProductView> productsFetched = GetAllViewByIds(context, productsNotInCache);

                foreach (IProductView product in productsFetched)
                {
                    productsPopulated.Add(product.Id, product);

                    // No caching when signed in.
                    if (context.Customer != null)
                    {
                        continue;
                    }

                    ProductViewCache.Instance.Add(new ProductKey(context.Channel.Id, product.Id), product);
                }
            }

            // Return populated products in the same order as they were passed to the method.
            var products = new ProductCollection();
            foreach (int productId in productIds)
            {
                IProductView product;
                if (productsPopulated.TryGetValue(productId, out product))
                {
                    ProductCampaignProcessor.Process(context, product);
                    products.Add(product);
                }
            }

            products.TotalCount = productIds.TotalCount;
            return products;
        }
		public virtual ProductCollection PopulateViewProducts(IUserContext context, ProductIdCollection productIds)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (productIds == null) throw new ArgumentNullException("productIds");
			if (ProductCampaignProcessor == null)
				throw new InvalidOperationException("ProductCampaignProcessor cannot be null.");

			var productsPopulated = new Dictionary<int, IProductView>();
			var productsNotInCache = new List<int>();

			// No caching when signed in.
			if (context.Customer != null)
			{
				productsNotInCache = new List<int>(productIds);
			}
			else
			{
				// Tries to get products from cache, otherwise adds them to fetching queue.
				foreach (int productId in productIds)
				{
					IProductView product = ProductViewCache.Instance.GetData(new ProductKey(context.Channel.Id, productId));

					if (product != null)
					{
						productsPopulated.Add(productId, product);
					}
					else
					{
						productsNotInCache.Add(productId);
					}
				}
			}

			// Fetch the products that wasn't in the cache.
			if (productsNotInCache.Count > 0)
			{
				Collection<IProductView> productsFetched = GetAllViewByIds(context, productsNotInCache);

				foreach (IProductView product in productsFetched)
				{
					productsPopulated.Add(product.Id, product);

					// No caching when signed in.
					if (context.Customer != null)
					{
						continue;
					}

					ProductViewCache.Instance.Add(new ProductKey(context.Channel.Id, product.Id), product);
				}
			}

			// Return populated products in the same order as they were passed to the method.
			var products = new ProductCollection();
			foreach (int productId in productIds)
			{
				IProductView product;
				if (productsPopulated.TryGetValue(productId, out product))
				{
					ProductCampaignProcessor.Process(context, product);
					products.Add(product);
				}
			}

			products.TotalCount = productIds.TotalCount;
			return products;
		}

    	private void AppendLekmerUrl(IUserContext context, ILekmerProduct product)
		{
			var categoryTree = CategoryService.GetAllAsTree(context);
			var contentNodeTree = ContentNodeService.GetAllAsTree(context);

			AppendLekmerUrl(product, categoryTree, contentNodeTree);
    	}

    	protected override ProductCollection GetAllByIds(IUserContext context, IEnumerable<int> productIds)
		{
			var categoryTree = CategoryService.GetAllAsTree(context);
			var contentNodeTree = ContentNodeService.GetAllAsTree(context);

			var productCollection = base.GetAllByIds(context, productIds);

			foreach (var productItem in productCollection)
			{
				var lekmerProduct = (ILekmerProduct) productItem;
				if (lekmerProduct.BrandId.HasValue)
				{
					lekmerProduct.Brand = BrandService.GetById(context, lekmerProduct.BrandId.Value);
				}

				AppendLekmerUrl(lekmerProduct, categoryTree, contentNodeTree);
			}

			return productCollection;
		}


    	private static void AppendLekmerUrl(ILekmerProduct product, ICategoryTree categoryTree, IContentNodeTree contentNodeTree)
    	{
    		product.ParentPageUrl = GetParentPageUrl(product, categoryTree, contentNodeTree);
    	}

    	private static string GetParentPageUrl(ILekmerProduct product, ICategoryTree categoryTree, IContentNodeTree contentNodeTree)
    	{
    		int? parentContentNodeId = ResolveParentContentNodeId(product, categoryTree);

    		if (parentContentNodeId.HasValue)
    		{
    			var contentNodeTreeItem = contentNodeTree.FindItemById(parentContentNodeId.Value);
    			if (contentNodeTreeItem != null)
    			{
    				return contentNodeTreeItem.Url;
    			}
    		}

    		return null;
    	}

    	private static int? ResolveParentContentNodeId(ILekmerProduct product, ICategoryTree categoryTree)
    	{
			if (product.ParentContentNodeId.HasValue)
    		{
				return product.ParentContentNodeId;
    		}
    		
			var categoryTreeItem = categoryTree.FindItemById(product.CategoryId);
    		if (categoryTreeItem != null && categoryTreeItem.Category != null)
    		{
    			if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
    			{
    				return categoryTreeItem.Category.ProductParentContentNodeId.Value;
    			}
    			
				foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
    			{
    				if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
    				{
    					return parentItem.Category.ProductParentContentNodeId.Value;
    				}
    			}
    		}

    		return null;
    	}


    	public ProductCollection GetAllByBlock(IUserContext context, IBlockBrandProductList block, int pageNumber,
		                                       int pageSize, int productSortOrderId)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			ProductIdCollection productIds = BlockBrandProductCollectionCache.Instance.TryGetItem(
				new BlockBrandProductCollectionKey(context.Channel.Id, block.Id, pageNumber, pageSize, productSortOrderId),
				delegate { return GetIdAllByBlock(context, block, pageNumber, pageSize); });

			return PopulateProducts(context, productIds);
		}


		protected virtual ProductIdCollection GetIdAllByBlock(IUserContext context, IBlockBrandProductList block,
		                                                      int pageNumber, int pageSize)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (block == null) throw new ArgumentNullException("block");

			return GetIdAllByBrands(
				context,
				BrandService.GetAllByBlockBrandProductList(context, block),
				pageNumber,
				pageSize,
				block.ProductSortOrder);
		}


		protected virtual ProductIdCollection GetIdAllByBrands(IUserContext context, Collection<IBrand> brands,
		                                                       int pageNumber, int pageSize, IProductSortOrder sortOrder)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (brands == null) throw new ArgumentNullException("brands");
			if (sortOrder == null) throw new ArgumentNullException("sortOrder");

			const char delimiter = ',';

			var repositoryLocal = (LekmerProductRepository) Repository;

			return repositoryLocal.GetIdAllByBrandIdList(
				context.Channel.Id,
				context.Customer,
				Convert.ToStringIdentifierList(brands.Select(c => c.Id), delimiter),
				delimiter,
				pageNumber,
				pageSize,
				sortOrder);
		}


		public ProductCollection GetAll(IUserContext context, int page, int pageSize)
		{
			var productIds = GetIdAll(context, page, pageSize);

			return PopulateProducts(context, productIds);
		}
	
		public ProductCollection GetViewAllForExport(IUserContext context, int page, int pageSize)
		{
			var productIds = GetIdAll(context, page, pageSize);

			return PopulateViewProductsForExport(context, productIds);
		}

    	private ProductIdCollection GetIdAll(IUserContext context, int page, int pageSize)
    	{
			var lekmerRepository = (LekmerProductRepository) Repository;
			return lekmerRepository.GetIdAll(context.Channel, context.Customer, page, pageSize);
		}

		public virtual int? GetIdByOldUrl(string oldUrl)
		{
			if (oldUrl == null) throw new ArgumentNullException("oldUrl");

			return ((LekmerProductRepository) Repository).GetIdByOldUrl(oldUrl);
		}


        public void RefreshProductListingTemporaryData()
        {
            var repositoryLocal = (LekmerProductRepository)Repository;
            repositoryLocal.RefreshCustomerGroupProductPriceListTemporaryData();
            repositoryLocal.RefreshProductListingTemporaryData();
        }

    }
}
