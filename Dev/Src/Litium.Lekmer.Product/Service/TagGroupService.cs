using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class TagGroupService : ITagGroupService
	{
		protected TagGroupRepository Repository { get; private set; }
		protected ITagService TagService { get; private set; }

		public TagGroupService(TagGroupRepository repository, ITagService tagService)
		{
			Repository = repository;
			TagService = tagService;
		}

		public Collection<ITagGroup> GetAll(IUserContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			return TagGroupCollectionCache.Instance.TryGetItem(
				new TagGroupKey(context.Channel.Id),
				delegate { return GetAllCore(context); });
		}

		private Collection<ITagGroup> GetAllCore(IUserContext context)
		{
			Collection<ITagGroup> tagGroups = Repository.GetAll();

			foreach (ITagGroup tagGroup in tagGroups)
			{
				tagGroup.Tags = TagService.GetAllByTagGroup(context, tagGroup.Id);
			}

			return tagGroups;
		}

		public Collection<ITagGroup> GetAllByProduct(IUserContext context, int productId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return ProductTagGroupCollectionCache.Instance.TryGetItem(
				new ProductTagGroupCollectionKey(context.Channel.Id, productId),
				delegate { return GetAllByProductCore(context, productId); });
		}

		private Collection<ITagGroup> GetAllByProductCore(IUserContext context, int productId)
		{
			Collection<ITagGroup> tagGroups = Repository.GetAll();
			foreach (ITagGroup tagGroup in tagGroups)
			{
				tagGroup.Tags = TagService.GetAllByTagGroupAndProduct(context, tagGroup.Id, productId);
			}
			return tagGroups;
		}
	}
}