using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public class PriceIntervalService : IPriceIntervalService
    {
        protected PriceIntervalRepository Repository { get; private set; }

        public PriceIntervalService(PriceIntervalRepository repository)
        {
            Repository = repository;
        }
     
        public Collection<IPriceInterval> GetAll(IUserContext context)
        {
            return PriceIntervalListCache.Instance.TryGetItem(
                new PriceIntervalListKey(context.Channel.Id),
                delegate { return Repository.GetAll(context.Channel.Currency.Id, context.Channel.Language.Id); });
        }

        public IPriceInterval GetMatching(IUserContext context, decimal price)
        {
            return GetAll(context)
                .FirstOrDefault(interval => interval.From <= price && interval.To >= price);
        }
    }
}