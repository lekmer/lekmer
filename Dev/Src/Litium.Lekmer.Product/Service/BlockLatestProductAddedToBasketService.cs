﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public class BlockLatestProductAddedToBasketService : IBlockLatestProductAddedToBasketService
    {
        protected BlockLatestProductAddedToBasketRepository Repository { get; private set; }

        public BlockLatestProductAddedToBasketService(
            BlockLatestProductAddedToBasketRepository repository)
        {
            Repository = repository;
        }

        public IBlockLatestProductAddedToBasket GetById(IUserContext context, int id)
        {
            return Repository.GetById(context.Channel, id);
        }
    }
}