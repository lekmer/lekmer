﻿using System;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Lekmer.Product.Messenger;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    public class WishListService : IWishListService
    {
        protected WishListRepository Repository { get; private set; }

        public WishListService(WishListRepository repository)
        {
            Repository = repository;
        }

        public IWishList Create()
        {
            var wishList = IoC.Resolve<IWishList>();
            wishList.ProductWishList = string.Empty;
            wishList.Status = BusinessObjectStatus.New;
            return wishList;
        }

        public IWishList GetByKey(Guid key)
        {
            return WishListCache.Instance.TryGetItem(
                new WishListCacheKey(key),
                () => Repository.GetByKey(key));
        }

        public void Save(IWishList wishList)
        {
            Repository.Save(wishList);
            WishListCache.Instance.Remove(wishList.WishListKey);
        }

        public void SendWishList(IUserContext context, WishListMessageArgs wishListMessageArgs)
        {
            new WishListMessenger().Send(wishListMessageArgs);
        }
    }
}