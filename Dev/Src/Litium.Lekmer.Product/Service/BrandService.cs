﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Service
{
    public class BrandService : IBrandService
    {
        protected BrandRepository BrandRepository { get; private set; }

        protected IBlockBrandProductListBrandService BlockBrandProductListBrandService { get; private set; }

        public BrandService(BrandRepository repository)
        {
            BrandRepository = repository;
        }

        public IBrand GetById(IUserContext context, int brandId)
        {
            return BrandCache.Instance.TryGetItem(
                new BrandKey(brandId),
				delegate { return BrandRepository.GetById(context.Channel.Id, brandId); });
        }

        public virtual IBrand Create()
        {
            IBrand brand = IoC.Resolve<IBrand>();
            brand.Status = BusinessObjectStatus.New;
            return brand;
        }

        public BrandCollection GetAllByBlock(IUserContext context, IBlockBrandList block, int page, int pageSize)
        {
            return BlockBrandListBrandCollectionCache.Instance.TryGetItem(
                new BlockBrandListBrandCollectionKey(context.Channel.Id, block.Id, page, pageSize),
                delegate { return BrandRepository.GetAllByBlock(context.Channel.Id, block.Id, page, pageSize); });
        }

        public Collection<IBrand> GetAllByBlock(IUserContext context, IBlockBrandProductList block)
        {
            return BrandRepository.GetAllByBlock(context.Channel.Id, block.Id);
        }


        public Collection<IBrand> GetAllByBlockBrandProductList(IUserContext context, IBlockBrandProductList block)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (block == null) throw new ArgumentNullException("block");


            var resolvedBrands = GetAllByBlock(context, block);


            return resolvedBrands;
        }


		public int? GetIdByTitle(IUserContext context, string title)
		{
			IBrand brand = GetAll(context)
				.FirstOrDefault(b => b.Title.Equals(title, StringComparison.OrdinalIgnoreCase));
			return brand != null ? brand.Id : (int?) null;
		}

    	public BrandCollection GetAll(IUserContext context)
        {
            return BrandListCache.Instance.TryGetItem(
                new BrandListKey(context.Channel.Id),
                delegate { return BrandRepository.GetAll(context); });
        }
    }
}