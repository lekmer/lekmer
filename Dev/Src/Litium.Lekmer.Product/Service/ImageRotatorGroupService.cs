﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public class ImageRotatorGroupService : IImageRotatorGroupService
    {
        protected ImageRotatorGroupRepository Repository { get; private set; }

        public ImageRotatorGroupService(ImageRotatorGroupRepository repository)
        {
            Repository = repository;
        }

        public ImageRotatorGroupCollection GetByBlockId(IUserContext context, int blockId)
        {
            return BlockImageRotatorCollectionCache.Instance.TryGetItem(
                new BlockImageRotatorCollectionKey(context.Channel.Id, blockId),
                delegate { return Repository.GetByBlockId(blockId); });
        }
    }
}