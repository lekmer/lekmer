﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Service
{
    public class BlockBrandProductListBrandService : IBlockBrandProductListBrandService

    {
        protected BlockBrandProductListBrandRepository Repository { get; private set; }

        public BlockBrandProductListBrandService(
            BlockBrandProductListBrandRepository repository)
        {
            Repository = repository;
        }

        public virtual Collection<IBlockBrandProductListBrand> GetAllByBlock(
            IUserContext context, int blockId)
        {
            return Repository.GetAllByBlock(context.Channel, blockId);
        }
    }
}