﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public class BlockImageRotatorService : IBlockImageRotatorService
    {
        protected BlockImageRotatorRepository Repository { get; private set; }

        public BlockImageRotatorService(
            BlockImageRotatorRepository repository)
        {
            Repository = repository;
        }

        public IBlockImageRotator GetById(IUserContext context, int id)
        {
            return BlockImageRotatorCache.Instance.TryGetItem(
                new BlockImageRotatorKey(context.Channel.Id, id),
                delegate { return Repository.GetById(context.Channel, id); });
        }
    }
}