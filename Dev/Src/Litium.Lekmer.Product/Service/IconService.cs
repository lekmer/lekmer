﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public class IconService : IIconService
    {
        protected IconRepository Repository { get; private set; }

        public IconService(IconRepository repository)
        {
            Repository = repository;
        }

        public Collection<IIcon> GetAllByProduct(IUserContext context, int productId)
		{
            return Repository.GetAllByProduct(productId, context.Channel.Language.Id);
		}
    }
}
