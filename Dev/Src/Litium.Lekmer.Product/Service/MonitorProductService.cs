﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Lekmer.Product.Messenger;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Service
{
	public class MonitorProductService : IMonitorProductService
	{
		protected MonitorProductRepository Repository { get; private set; }
        protected IProductService ProductService { get; private set; }
        protected IChannelService ChannelService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }

        public MonitorProductService(MonitorProductRepository repository, IProductService productService, IChannelService channelService, IProductSizeService productSizeService)
		{
			Repository = repository;
		    ProductService = productService;
            ChannelService = channelService;
        	ProductSizeService = productSizeService;
		}

		public IMonitorProduct Create(IUserContext context, int productId, string email, int? sizeId)
		{
        	var monitorProduct = IoC.Resolve<IMonitorProduct>();
			monitorProduct.ProductId = productId;
			monitorProduct.ChannelId = context.Channel.Id;
			monitorProduct.Email = email;
			monitorProduct.CreatedDate = DateTime.Now;
			monitorProduct.SizeId = sizeId;
			monitorProduct.Status = BusinessObjectStatus.New;
			return monitorProduct;
		}

		public int Save(IUserContext context, IMonitorProduct monitorProduct)
		{
			if (monitorProduct == null) throw new ArgumentNullException("monitorProduct");
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				monitorProduct.Id = Repository.Save(monitorProduct);
				transactedOperation.Complete();
			}
			return monitorProduct.Id;
		}

		/// <summary>
		/// Monitors the products and sends notifications to customers.
		/// </summary>
		/// <param name="portionSize">Number of products in portion to be monitored between the breaks.</param>
		/// <param name="breakDuration">Number of seconds to wait before the next portion will be monitored.</param>
        public void Monitor(int portionSize, int breakDuration)
        {
            var channels = new Collection<IChannel>();
            var products = new Collection<ILekmerProduct>();
        	var maxId = 0;
			while (true)
        	{
				var monitors = GetNextPortion(maxId, portionSize);
				if (monitors.Count == 0) break;

        		maxId = monitors.Max(i => i.Id);
				foreach (var monitor in monitors)
				{
					var channel = GetChannel(channels, monitor.ChannelId);
					var product = GetProduct(products, channel, monitor.ProductId);
					if (product == null) continue;
					if (monitor.SizeId.HasValue)
					{
						var productSize = ProductSizeService.GetById(product.Id, monitor.SizeId.Value);
						if (productSize == null) continue;
						if (!productSize.IsBuyable && !product.IsBookable) continue;
					}
					else if (!product.IsBuyable && !product.IsBookable) continue;

					var messageArgs = new MonitorProductMessageArgs(channel, monitor, product);
					var messenger = new MonitorProductMessenger();
					messenger.Send(messageArgs);
					Delete(monitor.Id);
				}
				Thread.Sleep(breakDuration * 1000); //convert seconds to miliseconds
        	}
        }

		private Collection<IMonitorProduct> GetNextPortion(int previousMaxId, int quantity)
		{
			return Repository.GetNextPortion(previousMaxId, quantity);
		}

		private void Delete(int id)
		{
			Repository.Delete(id);
		}

        private ILekmerProduct GetProduct(ICollection<ILekmerProduct> storage, IChannel channel, int productId)
        {
            var product = storage.FirstOrDefault(p => p.Id == productId);
            if (product == null)
            {
                var context = IoC.Resolve<IUserContext>();
                context.Channel = channel;
                product = ProductService.GetById(context, productId) as ILekmerProduct;
                if (product == null) return null;
                storage.Add(product);
            }
            return product;
        }

        private IChannel GetChannel(ICollection<IChannel> storage, int channelId)
        {
            var channel = storage.FirstOrDefault(c => c.Id == channelId);
            if (channel == null)
            {
                channel = ChannelService.GetById(channelId);
                storage.Add(channel);
            }
            return channel;
        }
	}
}
