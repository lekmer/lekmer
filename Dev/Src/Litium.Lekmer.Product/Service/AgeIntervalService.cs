using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class AgeIntervalService : IAgeIntervalService
	{
        protected AgeIntervalRepository Repository { get; private set; }

		public AgeIntervalService(AgeIntervalRepository repository)
        {
            Repository = repository;
        }

	    public Collection<IAgeInterval> GetAll(IUserContext context)
		{
			return AgeIntervalListCache.Instance.TryGetItem(
				new AgeIntervalListKey(context.Channel.Id),
				delegate { return Repository.GetAll(context.Channel.Language.Id); });
		}

		public IEnumerable<IAgeInterval> GetAllMatching(IUserContext context, int ageFromMonth, int ageToMonth)
		{
			return GetAll(context)
				.Where(interval => interval.FromMonth <= ageToMonth && interval.ToMonth >= ageFromMonth);
		}
	}
}