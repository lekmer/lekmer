using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class ProductTagService : IProductTagService
	{
		protected ProductTagRepository Repository { get; private set; }

		public ProductTagService(ProductTagRepository repository)
        {
            Repository = repository;
        }

		public Collection<IProductTag> GetAll()
		{
			return Repository.GetAll();
		}
	}
}