using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Service
{
	public class PriceListItemService : IPriceListItemService
	{
		public IPriceListItem Create()
		{
			return IoC.Resolve<IPriceListItem>();
		}
	}
}