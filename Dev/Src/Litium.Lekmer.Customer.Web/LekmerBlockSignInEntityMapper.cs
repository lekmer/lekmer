using System.Globalization;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerBlockSignInEntityMapper : BlockSignInEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockSignIn item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}