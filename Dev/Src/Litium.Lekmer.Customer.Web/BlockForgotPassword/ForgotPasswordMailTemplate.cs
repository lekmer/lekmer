﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web.BlockForgotPassword
{
	public class ForgotPasswordMailTemplate : Template
	{
		public ForgotPasswordMailTemplate(int templateId, IChannel channel) : base(templateId, channel, false)
		{
		}

		public ForgotPasswordMailTemplate(IChannel channel) : base("ForgotPasswordMail", channel, false)
		{
		}
	}
}