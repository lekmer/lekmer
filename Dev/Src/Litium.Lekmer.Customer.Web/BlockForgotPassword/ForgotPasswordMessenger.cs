﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web.BlockForgotPassword
{
	public class ForgotPasswordMessenger : ScensumMessengerBase<ForgotPasswordForm>
	{
		protected ForgotPasswordMailTemplate Template { get; set; }

		protected override Message CreateMessage(ForgotPasswordForm messageArgs)
		{
			Template = new ForgotPasswordMailTemplate(UserContext.Current.Channel);
			
			var customer = messageArgs.Customer;

			var message = new EmailMessage
			{
				Body = RenderMessage(customer),
				FromEmail = GetContent("FromEmail"),
				FromName = GetContent("FromName"),
				Subject = GetContent("Subject"),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Address = customer.User.UserName,
				Name = customer.User.UserName
			});
			return message;
		}

		private string RenderMessage(ICustomer customer)
		{
			Fragment htmlFragment = Template.GetFragment("Message");
			htmlFragment.AddVariable("NewPassword.Url", GetConfirmPageUrl(customer));
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private static string GetConfirmPageUrl(ICustomer customer)
		{
			var guid = CreateCustomerForgotPassword(customer);
			var node = IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, "newpassword");
			return string.Format(CultureInfo.InvariantCulture, "{0}?id={1}", UrlHelper.ResolveUrlHttps(node.Url), guid);
		}

		private static Guid CreateCustomerForgotPassword(ICustomer customer)
		{
			var service = IoC.Resolve<ICustomerForgotPasswordService>();
			var customerForgotPassword = service.Create(customer.Id);
			service.Save(customerForgotPassword);
			return customerForgotPassword.Guid;
		}

		protected override string MessengerName
		{
			get { return "BlockForgotPasswordControl"; }
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}
