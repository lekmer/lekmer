﻿using System.Collections.Specialized;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web.BlockForgotPassword
{
	public class ForgotPasswordForm : ControlBase
    {
        private readonly string _forgotPasswordPostUrl;

        public ForgotPasswordForm(string forgotPasswordPostUrl)
        {
            _forgotPasswordPostUrl = forgotPasswordPostUrl;
        }

		public ICustomer Customer { get; set; }

		public string UserFormName
        {
            get { return "forgotpassword-user"; }
        }

        public string PostUrl
        {
            get { return _forgotPasswordPostUrl; }
        }

        public string PostModeValue
        {
            get { return "forgotpassword"; }
        }

        public bool IsFormPostBack
        {
            get { return IsPostBack && PostMode.Equals(PostModeValue); }
        }

        public string User { get; set; }

        public void MapFromRequest()
        {
            NameValueCollection form = Request.Form;
			User = form[UserFormName];
        }

        public void MapFieldsToFragment(Fragment fragment)
        {
            fragment.AddVariable("Form.PostUrl", PostUrl);
            fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.User.Name", UserFormName);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.User.Value", User);
        }

        public ValidationResult Validate()
        {
			var validationResult = new ValidationResult();
			if (string.IsNullOrEmpty(User))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.ForgotPassword.UserNameEmpty"));
				return validationResult;
			}

        	var customerService = IoC.Resolve<ICustomerService>();
			Customer = customerService.GetByUserName(UserContext.Current, User);
			if (Customer == null)
            {
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.ForgotPassword.CustomerNotFound"));
            }
            return validationResult;
        }
    }
}
