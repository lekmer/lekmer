﻿using System;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web.BlockNewPassword
{
	public class BlockNewPasswordControl : BlockControlBase
	{
		private NewPasswordForm _newPasswordForm;
		private NewPasswordForm NewPasswordForm
		{
			get
			{
				return _newPasswordForm ?? (_newPasswordForm = new NewPasswordForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		public BlockNewPasswordControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		private bool WasPasswordChanged { get; set; }

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = new ValidationResult();

			if (!NewPasswordForm.IsFormPostBack)
			{
				var customerId = ValidatePageAuthorization(validationResult);
				if (!validationResult.IsValid)
					return RenderNotAuthorizedForm(validationResult);
				
				NewPasswordForm.Clear();
				NewPasswordForm.CustomerId = customerId;
			}
			else
			{
				NewPasswordForm.MapFromRequest();
				validationResult = NewPasswordForm.Validate();
				if (validationResult.IsValid)
				{
					var customer = NewPasswordForm.Customer;
					customer.User.ClearTextPassword = NewPasswordForm.Password;
					IoC.Resolve<ICustomerService>().Save(UserContext.Current, customer);
					IoC.Resolve<ICustomerForgotPasswordService>().Delete(customer.Id);
					WasPasswordChanged = true;
				}
			}

			return RenderNewPasswordForm(validationResult);
		}

		private BlockContent RenderNewPasswordForm(ValidationResult validationResult)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			NewPasswordForm.MapFieldNamesToFragment(fragmentContent);
			NewPasswordForm.MapFieldValuesToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			fragmentContent.AddCondition("NewPassword.WasChanged", WasPasswordChanged);
			fragmentContent.AddEntity(Block);

			Fragment fragmentHead = Template.GetFragment("Head");
			NewPasswordForm.MapFieldNamesToFragment(fragmentHead);

			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}

		private static BlockContent RenderNotAuthorizedForm(ValidationResult validationResult)
		{
			var validationControl = new ValidationControl(validationResult.Errors);
			return new BlockContent(string.Empty, validationControl.Render());
		}

		private int ValidatePageAuthorization(ValidationResult validationResult)
		{
			var id = Request.QueryString["id"];
			Guid guid;
			ICustomerForgotPassword customerForgotPassword = null;

			if (string.IsNullOrEmpty(id) || !TryParseGuid(id, out guid) || !Exists(guid, out customerForgotPassword))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.GuidIncorrect"));
			}
			else if ((DateTime.Now - customerForgotPassword.CreatedDate).Days > 1)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.Expired"));
			}
			return customerForgotPassword != null ? customerForgotPassword.CustomerId : -1;
		}

		private static bool TryParseGuid(string id, out Guid guid)
		{
			try
			{
				guid = new Guid(id);
			}
			catch
			{
				guid = Guid.Empty;
				return false;
			}
			return true;
		}

		private static bool Exists(Guid guid, out ICustomerForgotPassword customerForgotPassword)
		{
			customerForgotPassword = IoC.Resolve<ICustomerForgotPasswordService>().GetById(guid);
			return customerForgotPassword != null;
		}
	}
}