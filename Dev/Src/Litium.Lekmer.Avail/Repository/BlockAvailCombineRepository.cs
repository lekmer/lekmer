﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Avail.Repository
{
	public class BlockAvailCombineRepository
	{
		protected virtual DataMapperBase<IBlockAvailCombine> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockAvailCombine>(dataReader);
		}

		public virtual IBlockAvailCombine GetById(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockAvailCombineRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockAvailCombineGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockAvailCombine block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("UseCartPredictions", block.UseCartPredictions, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseClickStreamPredictions", block.UseClickStreamPredictions, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseLogPurchase", block.UseLogPurchase, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UsePersonalPredictions", block.UsePersonalPredictions, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseProductSearchPredictions", block.UseProductSearchPredictions, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseProductsPredictions", block.UseProductsPredictions, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseProductsPredictionsFromClicksCategory", block.UseProductsPredictionsFromClicksCategory, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseProductsPredictionsFromClicksProduct", block.UseProductsPredictionsFromClicksProduct, SqlDbType.Bit),
					ParameterHelper.CreateParameter("UseLogClickedOn", block.UseLogClickedOn, SqlDbType.Bit),
                    ParameterHelper.CreateParameter("GetClickHistoryFromCookie", block.GetClickHistoryFromCookie, SqlDbType.Bit),
					ParameterHelper.CreateParameter("ColumnCount", block.ColumnCount, SqlDbType.Int),
					ParameterHelper.CreateParameter("RowCount", block.RowCount, SqlDbType.Int)
				
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockAvailCombineRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockAvailCombineSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockAvailCombineRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockAvailCombineDelete]", parameters, dbSettings);
		}
	}
}
