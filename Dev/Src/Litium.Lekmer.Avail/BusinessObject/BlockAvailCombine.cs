﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Avail
{
    [Serializable]
    public class BlockAvailCombine : BlockBase, IBlockAvailCombine
    {
        private int _columnCount;
        private int _rowCount;
        private bool _useCartPredictions;
        private bool _useClickStreamPredictions;
        private bool _useLogPurchase;
        private bool _usePersonalPredictions;
        private bool _useProductSearchPredictions;
        private bool _useProductsPredictions;
        private bool _useProductsPredictionsFromClicksProduct;
        private bool _useProductsPredictionsFromClicksCategory;
        private bool _useLogClickedOn;

        private bool _getClickHistoryFromCookie;

        public bool GetClickHistoryFromCookie
        {
            get { return _getClickHistoryFromCookie; }
            set
            {
                CheckChanged(_getClickHistoryFromCookie, value);
                _getClickHistoryFromCookie = value;
            }
        }

        public bool UseLogClickedOn
        {
            get { return _useLogClickedOn; }
            set
            {
                CheckChanged(_useLogClickedOn, value);
                _useLogClickedOn = value;
            }
        }

        public bool UseCartPredictions
        {
            get { return _useCartPredictions; }
            set
            {
                CheckChanged(_useCartPredictions, value);
                _useCartPredictions = value;
            }
        }

        public bool UseClickStreamPredictions
        {
            get { return _useClickStreamPredictions; }
            set
            {
                CheckChanged(_useClickStreamPredictions, value);
                _useClickStreamPredictions = value;
            }
        }

        public bool UseLogPurchase
        {
            get { return _useLogPurchase; }
            set
            {
                CheckChanged(_useLogPurchase, value);
                _useLogPurchase = value;
            }
        }

        public bool UsePersonalPredictions
        {
            get { return _usePersonalPredictions; }
            set
            {
                CheckChanged(_usePersonalPredictions, value);
                _usePersonalPredictions = value;
            }
        }

        public bool UseProductSearchPredictions
        {
            get { return _useProductSearchPredictions; }
            set
            {
                CheckChanged(_useProductSearchPredictions, value);
                _useProductSearchPredictions = value;
            }
        }

        public bool UseProductsPredictions
        {
            get { return _useProductsPredictions; }
            set
            {
                CheckChanged(_useProductsPredictions, value);
                _useProductsPredictions = value;
            }
        }

        public bool UseProductsPredictionsFromClicksProduct
        {
            get { return _useProductsPredictionsFromClicksProduct; }
            set
            {
                CheckChanged(_useProductsPredictionsFromClicksProduct, value);
                _useProductsPredictionsFromClicksProduct = value;
            }
        }

        public bool UseProductsPredictionsFromClicksCategory
        {
            get { return _useProductsPredictionsFromClicksCategory; }
            set
            {
                CheckChanged(_useProductsPredictionsFromClicksCategory, value);
                _useProductsPredictionsFromClicksCategory = value;
            }
        }

        /// <summary>
        /// Number of columns with product in a block.
        /// Value must be greater then 0.
        /// </summary>
        public int ColumnCount
        {
            get { return _columnCount; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("ColumnCount can't be less then '1'.");
                }
                CheckChanged(_columnCount, value);
                _columnCount = value;
            }
        }

        /// <summary>
        /// Number of rows with product in a block.
        /// Value must be greater then 0.
        /// </summary>
        public int RowCount
        {
            get { return _rowCount; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("RowCount can't be less then '1'.");
                }
                CheckChanged(_rowCount, value);
                _rowCount = value;
            }
        }
    }
}
