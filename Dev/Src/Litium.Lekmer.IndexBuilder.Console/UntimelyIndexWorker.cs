using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Framework.Search.Configuration;
using Litium.Framework.Search.Indexing;

namespace Litium.Lekmer.IndexBuilder
{
	public class UntimelyIndexWorker : IndexWorker
	{
		[SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Ms")]
		public UntimelyIndexWorker(
			IIndexingService indexingService,
			IConfigurationService configurationService,
			IIndexJobLocator indexJobLocator,
			int intervalMs)
			: base(indexingService, configurationService, indexJobLocator, intervalMs)
		{
		}

		public override void Start()
		{
			ConsumeJobs();
		}

		public override void Abort()
		{
		}

		protected override void ConsumeJobs()
		{
			var jobs = ReadJobConfigurations();
			foreach (var job in jobs)
			{
				job.Execute(IndexingService);
			}
		}

		protected override bool IsDue(TimeSpan clockTime, TimeSpan jobTime)
		{
			return true;
		}
	}
}