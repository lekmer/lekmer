﻿using System;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Avail.Web
{
	public static class CustomerIdentificationCookie
	{
		public static string CustomerIdentificationIdKey = "CustomerIdentificationId";

		private static string GetCustomerIdentificationId()
		{
			HttpCookie cookie = HttpContext.Current.Request.Cookies[CustomerIdentificationIdKey];
			if (cookie == null) return null;
			return cookie.Value;
		}

		public static string GetCustomerIdentificationId(ICustomer customer)
		{
			string identificationId = GetCustomerIdentificationId();
			if (string.IsNullOrEmpty(identificationId) && customer != null)
			{
				identificationId = customer.Id.ToString();
			}
			return identificationId;
		}

		public static string SetCustomerIdentificationId(ICustomerSession customerSession)
		{
			string identificationId = GetCustomerIdentificationId();
			if (string.IsNullOrEmpty(identificationId))
			{
				if (customerSession.IsSignedIn)
				{
					identificationId = customerSession.SignedInCustomer.Id.ToString();
				}
				else
				{
					identificationId = SetCustomerIdentificationId();
				}
			}
			return identificationId;
		}

		private static string SetCustomerIdentificationId()
		{
			DateTime dtNow = DateTime.Now;
			TimeSpan tsMinute = new TimeSpan(354, 50, 0, 0, 0);
			HttpCookie cookie = HttpContext.Current.Request.Cookies[CustomerIdentificationIdKey];
			if (cookie == null)
			{
				cookie = new HttpCookie(CustomerIdentificationIdKey);
				cookie.Value = Guid.NewGuid().ToString();
			}
			cookie.Expires = dtNow + tsMinute;
			HttpContext.Current.Response.Cookies.Add(cookie);
			return cookie.Value;
		}
	}
}