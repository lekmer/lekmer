﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Litium.Lekmer.Avail.Web.Cookie
{
    public static class AvailSessionCookie
    {
        public static string AvailSessionCookieKey = "__avail_session__";

        public static string GetAvailSession()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[AvailSessionCookieKey];
            if (cookie == null) return null;
            return cookie.Value;
        }
    }
}
