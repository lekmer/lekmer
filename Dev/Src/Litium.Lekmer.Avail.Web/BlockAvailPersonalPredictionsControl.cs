﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Avail.Web
{
	public class BlockAvailPersonalPredictionsControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IBlockService _blockService;


		public BlockAvailPersonalPredictionsControl(ITemplateFactory factory, IBlockService blockService, ICustomerSession customerSession)
			: base(factory)
		{
			_blockService = blockService;
			_customerSession = customerSession;
		}

		private readonly ICustomerSession _customerSession;

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			string identificationId = CustomerIdentificationCookie.SetCustomerIdentificationId(_customerSession);
			var fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddEntity(Block);
			fragmentHead.AddVariable("Customer.Id", identificationId);
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);
			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}
	}
}