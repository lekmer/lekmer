﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Avail.Web
{
	public class BlockAvailProductsPredictionsControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		public BlockAvailProductsPredictionsControl(ITemplateFactory factory, IBlockService blockService)
			: base(factory)
		{
			_blockService = blockService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddEntity(Product);
			fragmentHead.AddEntity(Block);
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);
			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}
	}
}
