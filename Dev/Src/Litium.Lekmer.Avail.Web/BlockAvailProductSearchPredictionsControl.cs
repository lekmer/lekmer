﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class BlockAvailProductSearchPredictionsControl : BlockControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		public BlockAvailProductSearchPredictionsControl(ITemplateFactory factory, IBlockService blockService)
			: base(factory)
		{
			_blockService = blockService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			string searchQuery = Request.QueryString["q"];

			if (string.IsNullOrEmpty(searchQuery))
			{
				return new BlockContent();
			}
			var fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddEntity(Block);
			fragmentHead.AddVariable("SearchQuery", searchQuery, VariableEncoding.JavaScriptStringEncode);
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);
			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}
	}
}
