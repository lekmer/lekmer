﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailProductSearchPredictionsControl
	{
		public void Render(Fragment fragmentHead, Fragment fragmentContent, string searchQuery)
		{
			if (string.IsNullOrEmpty(searchQuery))
			{
				fragmentHead.AddCondition("UseProductSearchPredictions", false);
				fragmentContent.AddCondition("UseProductSearchPredictions", false);
				return;
			}
			fragmentHead.AddVariable("SearchQuery", searchQuery, VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddCondition("UseProductSearchPredictions", true);
			fragmentContent.AddCondition("UseProductSearchPredictions", true);
		}
	}
}
