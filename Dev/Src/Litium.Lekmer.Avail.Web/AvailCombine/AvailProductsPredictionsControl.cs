﻿using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailProductsPredictionsControl
	{
		public void Render(Fragment fragmentHead, Fragment fragmentContent, IProduct product)
		{
			if (product==null)
			{
				fragmentHead.AddCondition("UseProductsPredictions", false);
				fragmentContent.AddCondition("UseProductsPredictions", false);
				return;
			}
			fragmentHead.AddCondition("UseProductsPredictions", true);
			fragmentContent.AddCondition("UseProductsPredictions", true);
		}
	}
}
