﻿using System.Globalization;
using Litium.Scensum.Core.Web;

namespace Litium.Lekmer.Avail.Web.AvailCombine
{
	public class AvailPagingControl : PagingControl
	{
		public AvailPagingControl(ITemplateFactory templateFactory) : base(templateFactory)
		{
		}
		/// <summary>
		/// Gets the url to a certain page number.
		/// </summary>
		/// <param name="page">The page number.</param>
		/// <returns>Url for one page.</returns>
		protected override string GetPageUrl(int page)
		{
			var queryBuilder = new QueryBuilder();
			if (page != 1)
			{
				queryBuilder.ReplaceOrAdd(PageQueryStringParameterName, page.ToString(CultureInfo.InvariantCulture));
			}
			else
			{
				queryBuilder.Remove(PageQueryStringParameterName);
			}

			return PageBaseUrl + queryBuilder;
		}
	}
}
