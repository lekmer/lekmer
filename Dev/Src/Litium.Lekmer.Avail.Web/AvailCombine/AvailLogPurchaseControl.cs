﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using Litium.Lekmer.Avail.Setting;
using Litium.Lekmer.Avail.Web.Cookie;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;
using Channel = Litium.Scensum.Core.Web.Channel;
using Encoder = Litium.Scensum.Foundation.Utilities.Encoder;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Avail.Web
{
    public class AvailLogPurchaseControl
    {
        private IOrderFull _order;
        private string _identificationId;
        private string _availSession;
        private string _availCustomerId;
        private ICustomerSession _customerSession;
        public void Render(Fragment fragmentHead, Fragment fragmentContent)
        {
            _customerSession = IoC.Resolve<ICustomerSession>();
            _identificationId = CustomerIdentificationCookie.GetCustomerIdentificationId(_customerSession.SignedInCustomer);
            _availSession = AvailSessionCookie.GetAvailSession();
            var availSetting = new AvailSetting("AvailSettings");
            _availCustomerId = availSetting.CustomerId();

            int? orderId = IoC.Resolve<IOrderSession>().OrderId;
            //if (!orderId.HasValue || !GetOrder(orderId.Value) || string.IsNullOrEmpty(_identificationId))
            //{
            //    fragmentHead.AddCondition("UseLogPurchase", false);
            //    fragmentContent.AddCondition("UseLogPurchase", false);
            //    return;
            //}
            //fragmentHead.AddCondition("UseLogPurchase", true);
            //fragmentContent.AddCondition("UseLogPurchase", true);

            //RenderHead(fragmentHead);
            if (!orderId.HasValue || !GetOrder(orderId.Value) || string.IsNullOrEmpty(_identificationId) || string.IsNullOrEmpty(_availSession))
            {
                return;
            }

            PostPurchaseToAvail(_order, _availSession, _availCustomerId, _identificationId);
        }
        private bool GetOrder(int orderId)
        {
            _order = IoC.Resolve<IOrderService>().GetFullById(UserContext.Current, orderId);
            return _order != null && _order.GetOrderItems().Count != 0;
        }

        private void RenderHead(Fragment fragmentHead)
        {
            fragmentHead.AddVariable("UserId", _identificationId);
            AddOrderItemsArrayParam(fragmentHead);
            fragmentHead.AddVariable("OrderId", _order.Id.ToString);
            fragmentHead.AddVariable("Currency", UserContext.Current.Channel.Currency.Iso);
        }

        private void AddOrderItemsArrayParam(Fragment fragment)
        {
            var itemBuilder = new StringBuilder();
            itemBuilder.Append("new Array(");
            var priceBuilder = new StringBuilder();
            priceBuilder.Append("new Array(");
            Collection<IOrderItem> orderItems = _order.GetOrderItems();
            foreach (var item in orderItems)
            {
                for (var i = 1; i <= item.Quantity; i++)
                {
                    itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}',", item.ProductId));
                    priceBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}',", item.ActualPrice.IncludingVat));
                }

            }
            if (itemBuilder.Length > 0)
                itemBuilder.Remove(itemBuilder.Length - 1, 1);
            if (priceBuilder.Length > 0)
                priceBuilder.Remove(priceBuilder.Length - 1, 1);

            itemBuilder.Append(")");
            priceBuilder.Append(")");

            fragment.AddVariable("Product.Ids", itemBuilder.ToString);
            fragment.AddVariable("Prices", priceBuilder.ToString);
        }

        private void PostPurchaseToAvail(IOrderFull order, string sessionId, string availCustomerId, string userId)
        {
            string postUri = string.Format("https://service.avail.net/2009-02-13/dynamic/{0}/services/jsonrpc-1.x/", availCustomerId);
            string postData = BuildJsonPostData(order, sessionId, userId);
            var request = (HttpWebRequest)WebRequest.Create(postUri);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = postData.Length;
            Stream dataStream = request.GetRequestStream();
            var encoding = new UTF8Encoding();
            byte[] bytes = encoding.GetBytes(postData);
            dataStream.Write(bytes, 0, bytes.Length);
            dataStream.Close();

            #region not neccessery code just to see the response result
            //WebResponse response = request.GetResponse();
            //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            //dataStream = response.GetResponseStream();
            //if (dataStream != null)
            //{
            //    var reader = new StreamReader(dataStream);
            //    string responseFromServer = reader.ReadToEnd();
            //    Console.WriteLine(responseFromServer);
            //    reader.Close();
            //    dataStream.Close();
            //}
            //response.Close();
            #endregion


        }


        private string BuildJsonPostData(IOrderFull order, string sessionId, string userId)
        {
            //example of rendered json output
            //{"version": "1.1", "params": {"SessionID": "sess321", "UserID": "user321", "ProductIDs": ["1400040361", "0312346069"], "Prices": ["15.00", "9.00"], "OrderID": "304711248", "Currency": "SEK"}, "method": "logPurchase", "id": "123"}

            var formatter = IoC.Resolve<IFormatter>();
            const string quote = @"""";
            const string jsonOrderFormatPrefix = @"""version"":""{0}"",""params"":";
            const string jsonOrderFormat = @"""SessionID"":""{0}"",""UserID"":""{1}"",""ProductIDs"":{2},""Prices"": {3},""OrderID"":""{4}"",""Currency"":""{5}""";
            const string jsonOrderFormatPostfix = @",""method"":""logPurchase"",""id"":""{0}""";
            Collection<IOrderItem> orderItems = order.GetOrderItems();
            var json = new StringBuilder();
            var productIds = new StringBuilder();
            var prices = new StringBuilder();
            int counter = 1;
            foreach (var item in orderItems)
            {
                string price = formatter.FormatPrice(Channel.Current, item.ActualPrice.IncludingVat);
                if (counter < orderItems.Count)
                {
                    productIds.Append(quote + item.ProductId + quote + ",");
                    prices.Append(quote + price + quote + ",");
                }
                else
                {
                    productIds.Append(quote + item.ProductId + quote);
                    prices.Append(quote + price + quote);
                }
                counter++;
            }
            json.Append(
                        "{" + string.Format(CultureInfo.CurrentCulture, jsonOrderFormatPrefix, Encoder.EncodeJavaScript("1.1"))
                            + "{"
                            + string.Format(
                                        CultureInfo.CurrentCulture, jsonOrderFormat,
                                        Encoder.EncodeJavaScript(sessionId),
                                        Encoder.EncodeJavaScript(userId),
                                        "[" + productIds + "]",
                                        "[" + prices + "]",
                                        Encoder.EncodeJavaScript(order.Id.ToString()),
                                        Encoder.EncodeJavaScript(Channel.Current.Currency.Iso))
                            + "}"
                            + string.Format(CultureInfo.CurrentCulture, jsonOrderFormatPostfix, Encoder.EncodeJavaScript(order.Id.ToString()))
                        + "},");


            return json.ToString();
        }




    }
}
