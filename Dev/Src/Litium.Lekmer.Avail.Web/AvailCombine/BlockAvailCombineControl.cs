﻿using Litium.Lekmer.Avail.Web.AvailCombine;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.Avail.Web
{
	public class BlockAvailCombineControl : BlockProductPageControlBase<IBlockAvailCombine>
	{
		private readonly IBlockAvailCombineService _blockService;
		public BlockAvailCombineControl(ITemplateFactory factory, IBlockAvailCombineService blockService)
			: base(factory)
		{
			_blockService = blockService;
		}



		protected override IBlockAvailCombine GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddVariable("NumberOfRows", Block.RowCount.ToString);
			fragmentHead.AddVariable("NumberOfColumns", Block.ColumnCount.ToString);

			fragmentHead.AddEntity(Block);
			var fragmentContent = Template.GetFragment("Content");
			if (Product != null)
			{
				fragmentHead.AddEntity(Product);
				fragmentContent.AddEntity(Product);
			}

			fragmentContent.AddEntity(Block);
			if (Block.UseCartPredictions)
			{
				AvailCartPredictionsControl cartPredictionsControl = new AvailCartPredictionsControl();
				cartPredictionsControl.Render(fragmentHead, fragmentContent);
			}
			else
			{
				fragmentHead.AddCondition("UseCartPredictions", false);
				fragmentContent.AddCondition("UseCartPredictions", false);
			}
			if (Block.UseClickStreamPredictions)
			{
				AvailClickStreamPredictionsControl cartPredictionsControl = new AvailClickStreamPredictionsControl();
				cartPredictionsControl.Render(fragmentHead, fragmentContent);
			}
			else
			{
				fragmentHead.AddCondition("UseClickStreamPredictions", false);
				fragmentContent.AddCondition("UseClickStreamPredictions", false);

			}
			if (Block.UseLogPurchase)
			{
				AvailLogPurchaseControl availLogPurchaseControl = new AvailLogPurchaseControl();
				availLogPurchaseControl.Render(fragmentHead, fragmentContent);
			}
			else
			{
				fragmentHead.AddCondition("UseLogPurchase", false);
				fragmentContent.AddCondition("UseLogPurchase", false);

			}

			if (Block.UsePersonalPredictions)
			{
				AvailPersonalPredictionsControl availPersonalPredictionsControl = new AvailPersonalPredictionsControl();
				availPersonalPredictionsControl.Render(fragmentHead, fragmentContent);
			}
			else
			{
				fragmentHead.AddCondition("UsePersonalPredictions", false);
				fragmentContent.AddCondition("UsePersonalPredictions", false);

			}
			if (Block.UseProductSearchPredictions)
			{
				string searchQuery = Request.QueryString["q"];
				AvailProductSearchPredictionsControl availProductSearchPredictionsControl = new AvailProductSearchPredictionsControl();
				availProductSearchPredictionsControl.Render(fragmentHead, fragmentContent, searchQuery);
			}
			else
			{
				fragmentHead.AddCondition("UseProductSearchPredictions", false);
				fragmentContent.AddCondition("UseProductSearchPredictions", false);

			}
			if (Block.UseProductsPredictions)
			{
				AvailProductsPredictionsControl availProductsPredictionsControl = new AvailProductsPredictionsControl();
				availProductsPredictionsControl.Render(fragmentHead, fragmentContent, Product);
			}
			else
			{
				fragmentHead.AddCondition("UseProductsPredictions", false);
				fragmentContent.AddCondition("UseProductsPredictions", false);
			}
			if (Block.UseProductsPredictionsFromClicksProduct)
			{
				AvailProductsPredictionsFromClicksControl availProductsPredictionsFromClicksControl = new AvailProductsPredictionsFromClicksControl();
				availProductsPredictionsFromClicksControl.Render(fragmentHead, fragmentContent, Product);
			}
			else
			{
				fragmentHead.AddCondition("UseProductsPredictionsFromClicksProduct", false);
				fragmentContent.AddCondition("UseProductsPredictionsFromClicksProduct", false);
			}
			if (Block.UseProductsPredictionsFromClicksCategory)
			{
				AvailProductsPredictionsFromClicksCategory availProductsPredictionsFromClicksCategory = new AvailProductsPredictionsFromClicksCategory();
				availProductsPredictionsFromClicksCategory.Render(fragmentHead, fragmentContent,Block.GetClickHistoryFromCookie);
			}
			else
			{
				fragmentHead.AddCondition("UseProductsPredictionsFromClicksCategory", false);
				fragmentContent.AddCondition("UseProductsPredictionsFromClicksCategory", false);
			}
			if (Block.UseLogClickedOn)
			{
				string trackingCode = Request.QueryString["trackingcode"];
				AvailLogClickedOnControl availLogClickedOnControl = new AvailLogClickedOnControl();
				availLogClickedOnControl.Render(fragmentHead, fragmentContent, Product, trackingCode);
			}
			else
			{
				fragmentHead.AddCondition("UseLogClickedOn", false);
				fragmentContent.AddCondition("UseLogClickedOn", false);
			}

			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}
	}
}