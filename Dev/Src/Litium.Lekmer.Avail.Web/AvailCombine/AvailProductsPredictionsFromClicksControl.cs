﻿using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailProductsPredictionsFromClicksControl
	{
		public void Render(Fragment fragmentHead, Fragment fragmentContent, IProduct product)
		{
			if (product == null)
			{
				fragmentHead.AddCondition("UseProductsPredictionsFromClicksProduct", false);
				fragmentContent.AddCondition("UseProductsPredictionsFromClicksProduct", false);
				return;
			}
			
			fragmentHead.AddCondition("UseProductsPredictionsFromClicksProduct", true);
			fragmentContent.AddCondition("UseProductsPredictionsFromClicksProduct", true);
		}
	}
}
