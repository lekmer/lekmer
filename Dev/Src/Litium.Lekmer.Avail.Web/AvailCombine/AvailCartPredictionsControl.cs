﻿using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailCartPredictionsControl
	{
		public void Render(Fragment fragmentHead, Fragment fragmentContent)
		{
			var cart = IoC.Resolve<ICartSession>().Cart;
			if (cart == null)
			{
				fragmentHead.AddCondition("UseCartPredictions", false);
				fragmentContent.AddCondition("UseCartPredictions", false);
				return;
			}
			var cartItems = IoC.Resolve<ICartSession>().Cart.GetCartItems();

			if (cartItems.Count <= 0)
			{
				fragmentHead.AddCondition("UseCartPredictions", false);
				fragmentContent.AddCondition("UseCartPredictions", false);
				return;
			}
			fragmentHead.AddCondition("UseCartPredictions", true);
			fragmentContent.AddCondition("UseCartPredictions", true);
			fragmentHead.AddVariable("Param.Cart.Items", RenderCartItemsArrayParam());
		}

		private static string RenderCartItemsArrayParam()
		{
			var itemBuilder = new StringBuilder();

			var cart = IoC.Resolve<ICartSession>().Cart;
			if (cart == null)
			{
				return null;
			}

			var cartItems = IoC.Resolve<ICartSession>().Cart.GetCartItems();

			itemBuilder.Append("new Array(");

			for (int i = 0; i < cartItems.Count; i++)
			{
				if (i == 0)//first or the only item in array
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", cartItems[i].Product.Id));
				}
				else
				{
					itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", cartItems[i].Product.Id));
				}
			}

			itemBuilder.Append(")");

			return itemBuilder.ToString();

		}
	}
}
