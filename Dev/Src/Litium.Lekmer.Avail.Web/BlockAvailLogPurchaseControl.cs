﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class BlockAvailLogPurchaseControl : BlockControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		private IOrderFull _order;
		private string _identificationId;
		public BlockAvailLogPurchaseControl(ITemplateFactory factory, IBlockService blockService, ICustomerSession customerSession)
			: base(factory)
		{
			_blockService = blockService;
			_customerSession = customerSession;
		}
		private readonly ICustomerSession _customerSession;
		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			_identificationId = CustomerIdentificationCookie.GetCustomerIdentificationId(_customerSession.SignedInCustomer);
			int? orderId = IoC.Resolve<IOrderSession>().OrderId;
			if (!orderId.HasValue || !GetOrder(orderId.Value) || string.IsNullOrEmpty(_identificationId))
			{
				return new BlockContent();
			}

			string head = RenderHead();
			var fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddEntity(Block);
			return new BlockContent(head, fragmentContent.Render());
		}
		private bool GetOrder(int orderId)
		{
			_order = IoC.Resolve<IOrderService>().GetFullById(UserContext.Current, orderId);
			return _order != null && _order.GetOrderItems().Count != 0;
		}

		private string RenderHead()
		{
			Fragment fragmentHead = Template.GetFragment("Head");
			fragmentHead.AddEntity(Block);
			fragmentHead.AddVariable("UserId", _identificationId);
			AddOrderItemsArrayParam(fragmentHead);
			fragmentHead.AddVariable("OrderId", _order.Id.ToString);
			fragmentHead.AddVariable("Currency", UserContext.Current.Channel.Currency.Iso);
			return fragmentHead.Render();
		}

		private void AddOrderItemsArrayParam(Fragment fragment)
		{
			var itemBuilder = new StringBuilder();
			itemBuilder.Append("new Array(");
			var priceBuilder = new StringBuilder();
			priceBuilder.Append("new Array(");
			Collection<IOrderItem> orderItems = _order.GetOrderItems();

            //for (int i = 0; i < orderItems.Count; i++)
            //{
            //    if (i == 0)//first or the only item in array
            //    {
            //        itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", orderItems[i].ProductId));
            //        priceBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}'", orderItems[i].ActualPrice.IncludingVat));
            //    }
            //    else
            //    {
            //        itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", orderItems[i].ProductId));
            //        priceBuilder.Append(string.Format(CultureInfo.InvariantCulture, ", '{0}'", orderItems[i].ActualPrice.IncludingVat));
            //    }
            //}
		    foreach (var item in orderItems)
		    {
                for (var i = 1; i <= item.Quantity; i++ )
                {
                    itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}',", item.ProductId));
                    priceBuilder.Append(string.Format(CultureInfo.InvariantCulture, "'{0}',", item.ActualPrice.IncludingVat));
                }
		        
		    }
            if (itemBuilder.Length>0)
    		    itemBuilder.Remove(itemBuilder.Length - 1, 1);
            if (priceBuilder.Length > 0)
                priceBuilder.Remove(priceBuilder.Length - 1, 1);

			itemBuilder.Append(")");
			priceBuilder.Append(")");

			fragment.AddVariable("Product.Ids", itemBuilder.ToString);
			fragment.AddVariable("Prices", priceBuilder.ToString);
		}
	}
}