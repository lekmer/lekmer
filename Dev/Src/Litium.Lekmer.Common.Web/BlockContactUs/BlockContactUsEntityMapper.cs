﻿using System.Globalization;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web.BlockContactUs
{
	public class BlockContactUsEntityMapper : BlockEntityMapper<IBlockContactUs>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockContactUs item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ReceiverEmail", item.Receiver, VariableEncoding.None);
		}
	}
}