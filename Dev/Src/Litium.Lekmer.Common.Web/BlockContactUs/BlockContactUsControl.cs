﻿using System;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Lekmer.Common.Contract.Service;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using IoC = Litium.Scensum.Foundation.IoC;
using ValidationResult = Litium.Scensum.Core.ValidationResult;

namespace Litium.Lekmer.Common.Web.BlockContactUs
{
	public class BlockContactUsControl : BlockControlBase<IBlockContactUs>
	{
		private readonly IBlockContactUsService _blockContactUsService = IoC.Resolve<IBlockContactUsService>();     
		private ContactUsForm _contactUsForm;

		private ContactUsForm ContactUsForm
		{
			get
			{
				return _contactUsForm 
					?? (_contactUsForm = new ContactUsForm(ResolveUrl(ContentNodeTreeItem.Url), Block.Receiver));
			}
		}

		public BlockContactUsControl(ITemplateFactory templateFactory) : base(templateFactory)
		{
		}

		protected override IBlockContactUs GetBlockById(int blockId)
		{
			return _blockContactUsService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			bool messageWasSent = false;

			if (ContactUsForm.IsFormPostBack)
			{
				ContactUsForm.MapFromRequest();
				validationResult = ContactUsForm.Validate();
				if (validationResult.IsValid)
				{
					SendMail(out messageWasSent);
					if (messageWasSent)
					{
						ContactUsForm.ClearFrom();
					}
				}
			}
			return RenderContactUsForm(validationResult, messageWasSent);
		}

		private BlockContent RenderContactUsForm(ValidationResult validationResult, bool messageWasSent)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			ContactUsForm.MapFieldNamesToFragment(fragmentContent);
			ContactUsForm.MapFieldValuesToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			fragmentContent.AddVariable("MailWasSentMessage", RenderMailMessageWasSentMessage(messageWasSent),
										VariableEncoding.None);
			fragmentContent.AddCondition("WasMailSent", messageWasSent);
			fragmentContent.AddEntity(Block);

			Fragment fragmentHead = Template.GetFragment("Head");
			ContactUsForm.MapFieldNamesToFragment(fragmentHead);

			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}

		private static string RenderMailMessageWasSentMessage(bool messageWasSent)
		{
			return messageWasSent
					   ? AliasHelper.GetAliasValue("Email.ContactUs.MailWasSent")
					   : string.Empty;
		}

		private void SendMail(out bool messageWasSent)
		{
			Guid id = new ContactUsMessager().Send(ContactUsForm);
			messageWasSent = !id.Equals(Guid.Empty);
		}
	}
}