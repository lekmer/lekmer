﻿using System.Collections.Specialized;
using System.Text.RegularExpressions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web.BlockNewsletterUnregistration
{
	public class NewsletterUnregistrationForm : ControlBase
	{
		private readonly string _postUrl;

		public NewsletterUnregistrationForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string EmailFormName
		{
			get { return "newsletterunregistration-email"; }
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "newsletterunregistration"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string Email { get; set; }

		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			Email = form[EmailFormName];
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Email.Name", EmailFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Email.Value", Email);
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();       
			ValidateEmail(validationResult);
			return validationResult;
		}

		private void ValidateEmail(ValidationResult validationResult)
		{
			if (Email.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.EmailEmpty"));
			}
			else if (!MatchEmailPattern(Email))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.EmailIncorrect"));
			}
		}

		private static bool MatchEmailPattern(string email)
		{
			Regex regex = new Regex(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
			return regex.IsMatch(email);
		}
	}
}