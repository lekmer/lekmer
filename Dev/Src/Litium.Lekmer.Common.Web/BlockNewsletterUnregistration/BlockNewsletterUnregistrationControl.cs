﻿using Litium.Lekmer.Common.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web.BlockNewsletterUnregistration
{
	public class BlockNewsletterUnregistrationControl : BlockControlBase
	{
		private NewsletterUnregistrationForm _newsletterUnregistrationForm;

		public BlockNewsletterUnregistrationControl(ITemplateFactory templateFactory, IBlockService blockService) : base(templateFactory, blockService)
		{
		}

		public NewsletterUnregistrationForm NewsletterUnregistrationForm
		{
			get
			{
				return _newsletterUnregistrationForm ??
					   (_newsletterUnregistrationForm = new NewsletterUnregistrationForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			bool unregistered = false;
			if (!NewsletterUnregistrationForm.IsFormPostBack)
			{
				NewsletterUnregistrationForm.Email = GetEmailParameter();
			}
			else
			{
				NewsletterUnregistrationForm.MapFromRequest();
				validationResult = NewsletterUnregistrationForm.Validate();
				if (validationResult.IsValid)
				{
					unregistered = UnregisterNewsletterSubscriber();
				}
			}
			return RenderNewsletterUnregistration(validationResult, unregistered);
		}

		private bool UnregisterNewsletterSubscriber()
		{
			var service = IoC.Resolve<INewsletterSubscriberService>();
			return service.Delete(UserContext.Current, NewsletterUnregistrationForm.Email);
		}

		private BlockContent RenderNewsletterUnregistration(ValidationResult validationResult, bool unregistered)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			NewsletterUnregistrationForm.MapFieldNamesToFragment(fragmentContent);
			NewsletterUnregistrationForm.MapFieldValuesToFragment(fragmentContent);
			RenderValidationResult(fragmentContent, validationResult, unregistered);
			fragmentContent.AddCondition("WasUnregistered", unregistered);
			fragmentContent.AddEntity(Block);

			Fragment fragmentHead = Template.GetFragment("Head");
			NewsletterUnregistrationForm.MapFieldNamesToFragment(fragmentHead);
			NewsletterUnregistrationForm.MapFieldValuesToFragment(fragmentHead);
			fragmentHead.AddEntity(Block);

			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}

		private static void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult, bool unregistered)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			else if (validationResult != null && validationResult.IsValid && !unregistered)
			{
				validationError = AliasHelper.GetAliasValue("Email.NewsletterRegistration.SubscriberNotFound");
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		private string GetEmailParameter()
		{
			return Request.QueryString["email"];
		}
	}
}
