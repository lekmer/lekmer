using System.Globalization;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web
{
	public class LekmerBlockRichTextEntityMapper : BlockRichTextEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockRichText item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}