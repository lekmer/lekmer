﻿using Litium.Lekmer.Common.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Common.Web.BlockNewsletterRegistration
{
	public class BlockNewsletterRegistrationControl : BlockControlBase
	{
		private NewsletterRegistrationForm _newsletterRegistrationForm;

		public BlockNewsletterRegistrationControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		public NewsletterRegistrationForm NewsletterRegistrationForm
		{
			get 
			{
				return _newsletterRegistrationForm ??
					   (_newsletterRegistrationForm = new NewsletterRegistrationForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			bool registered = false;
			if (NewsletterRegistrationForm.IsFormPostBack)
			{
				NewsletterRegistrationForm.MapFromRequest();
				validationResult = NewsletterRegistrationForm.Validate();
				if (validationResult.IsValid)
				{
					registered = RegisterNewsletterSubscriber();
					if (registered)
					{
						NewsletterRegistrationForm.ClearFrom();
					}
				}
			}
			else
			{
				NewsletterRegistrationForm.ClearFrom();
			}
			return RenderNewsletterRegistrationForm(validationResult, registered);
		}

		private bool RegisterNewsletterSubscriber()
		{
			var service = IoC.Resolve<INewsletterSubscriberService>();
			var subscriber = service.Create(UserContext.Current);
			subscriber.Email = NewsletterRegistrationForm.Email;
			int id = service.Save(UserContext.Current, subscriber);
			return id > 0;
		}

		private BlockContent RenderNewsletterRegistrationForm(ValidationResult validationResult, bool registered)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			NewsletterRegistrationForm.MapFieldNamesToFragment(fragmentContent);
			NewsletterRegistrationForm.MapFieldValuesToFragment(fragmentContent);
			RenderValidationResult(fragmentContent, validationResult);
			RenderRegisteredNotification(fragmentContent, registered);
			fragmentContent.AddEntity(Block);

			Fragment fragmentHead = Template.GetFragment("Head");
			NewsletterRegistrationForm.MapFieldNamesToFragment(fragmentHead);
			NewsletterRegistrationForm.MapFieldValuesToFragment(fragmentHead);
			fragmentHead.AddEntity(Block);

			return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
		}

		private static void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		private static void RenderRegisteredNotification(Fragment fragmentContent, bool registered)
		{
			string message = registered
				? AliasHelper.GetAliasValue("Email.NewsletterRegistration.Registered")
         		: string.Empty;
			fragmentContent.AddVariable("RegisteredMessage", message, VariableEncoding.None);
            fragmentContent.AddCondition("RegistationSuccess", registered);
		}
	}
}