﻿using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Common.Contract.SecureService
{
	public interface IBlockContactUsSecureService
	{
		IBlockContactUs Create();
		IBlockContactUs GetById(int id);
		int Save(ISystemUserFull systemUserFull, IBlockContactUs block);
	}
}
