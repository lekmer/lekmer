﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Common.Contract.BusinessObject
{
	public interface IBlockContactUs : IBlock
	{
		string Receiver { get; set; }
	}
}
