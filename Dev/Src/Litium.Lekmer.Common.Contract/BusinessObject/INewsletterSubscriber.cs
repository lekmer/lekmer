﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Common.Contract.BusinessObject
{
	public interface INewsletterSubscriber : IBusinessObjectBase
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		string Email { get; set; }
		DateTime CreatedDate { get; set; }
	}
}