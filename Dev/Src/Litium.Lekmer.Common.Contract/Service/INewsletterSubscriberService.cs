﻿using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Common.Contract.Service
{
	public interface INewsletterSubscriberService
	{
		INewsletterSubscriber Create(IUserContext context);
		int Save(IUserContext context, INewsletterSubscriber subscriber);
		bool Delete(IUserContext context, string email);
	}
}
