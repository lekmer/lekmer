﻿using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Common.Contract.Service
{
	public interface IBlockContactUsService
	{
		IBlockContactUs GetById(IUserContext context, int id);
	}
}
