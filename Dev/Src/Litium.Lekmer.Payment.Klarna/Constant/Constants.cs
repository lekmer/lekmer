﻿namespace Litium.Lekmer.Payment.Klarna
{
	internal static class Constants
	{
		#region Response codes

		/// <summary>
		/// Kreditor response error code
		/// </summary>
		internal const int KREDITOR_ERROR = 1;

		/// <summary>
		/// Kreditor response ok code
		/// </summary>
		internal const int KREDITOR_OK = 2;

		#endregion

		#region Civic number

		/// <summary>
		/// Kreditor civic number encoding for sweden
		/// </summary>
		internal const int KREDITOR_PNO_SE = 2;

		/// <summary>
		/// Kreditor civic number encoding for norway
		/// </summary>
		internal const int KREDITOR_PNO_NO = 3;

		/// <summary>
		/// Kreditor civic number encoding for finland
		/// </summary>
		internal const int KREDITOR_PNO_FI = 4;

		/// <summary>
		/// Kreditor civic number encoding for denmark
		/// </summary>
		internal const int KREDITOR_PNO_DK = 5;

		#endregion

		#region Country

		/// <summary>
		/// Kreditor country code for sweden
		/// </summary>
		internal const int KREDITOR_COUNTRY_CODE_SE = 209;

		/// <summary>
		/// Kreditor country code for norway
		/// </summary>
		internal const int KREDITOR_COUNTRY_CODE_NO = 164;

		/// <summary>
		/// Kreditor country code for finland
		/// </summary>
		internal const int KREDITOR_COUNTRY_CODE_FI = 73;

		/// <summary>
		/// Kreditor country code for denmark
		/// </summary>
		internal const int KREDITOR_COUNTRY_CODE_DK = 59;

		#endregion

		#region Currency

		/// <summary>
		/// Kreditor currency code for sweden
		/// </summary>
		internal const int KREDITOR_CURRENCY_SE = 0;

		/// <summary>
		/// Kreditor currency code for norway
		/// </summary>
		internal const int KREDITOR_CURRENCY_NO = 1;

		/// <summary>
		/// Kreditor currency code for europe (finland)
		/// </summary>
		internal const int KREDITOR_CURRENCY_FI = 2;

		/// <summary>
		/// Kreditor currency code for denmark
		/// </summary>
		internal const int KREDITOR_CURRENCY_DK = 3;

		#endregion

		#region Language

		internal const int KREDITOR_LANGUAGE_DK = 27;

		internal const int KREDITOR_LANGUAGE_SE = 138;

		internal const int KREDITOR_LANGUAGE_NO = 97;

		internal const int KREDITOR_LANGUAGE_FI = 37;

		#endregion

		#region Address

		/// <summary>
		/// Indicates if all firstnames should be returned with a GetCreditInformation call
		/// </summary>
		internal const int KREDITOR_GA_OLD = 1;

		/// <summary>
		/// Indicates if only the lastname should be returned with a GetCreditInformation call
		/// </summary>
		internal const int KREDITOR_GA_NEW = 2;

		#endregion
	}
}
