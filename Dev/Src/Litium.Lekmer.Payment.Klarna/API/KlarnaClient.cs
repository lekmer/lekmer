﻿using System;
using System.Collections.Generic;
using System.Text;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Klarna
{
	public static  class KlarnaClient
	{
		#region Methods wrapping Kreditor API

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Norway, Sweden and Denmark.
		/// </summary>
		/// <param name="civicNumber"></param>
		/// <param name="channel"></param>
		/// <param name="totalAmount"></param>
		/// <returns></returns>
		public static IKlarnaAddress GetAddress(string civicNumber, IChannel channel)
		{
			ValidateArgument(civicNumber);
			//ValidateChannelIdForGetAddress(channelId);

			int pnoEncoding = GetCivicNumberEncoding(channel);

			// Format civic so Kreditor will accept it.
			civicNumber = FormatCivicNumber(civicNumber, channel);

			IKreditorResponseItem response = new KlarnaApi(channel).GetAddress(civicNumber, pnoEncoding);

			if (response != null)
			{
				return ConvertResponseToAddress(response);
			}

			return null;
		}

		/*
		/// <summary>
		/// Reserves an amount at Kreditor for a company.
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="order"></param>
		/// <param name="externalPaymentClassId"></param>
		/// <param name="totalAmount"></param>
		/// <returns></returns>
		internal static string ReserveAmountCompany(ICustomer customer, IOrder order, int externalPaymentClassId,
		                                            int totalAmount)
		{
			ValidateArguments(customer, order);
			ValidateChannelIdForReserveAmount(Channel.Current.Id);

			if (customer.company == null)
			{
				throw new ArgumentNullException("customer", "customer.Company is null!");
			}

			// Channel specific variables.
			int currency;
			int country;
			int language;
			int pnoEncoding;

			// Sets channel specific variables.
			SetChannelSpecificVariables(customer.ChannelId, out currency, out country, out language, out pnoEncoding);

			// Make delivery address for kreditor api.
			Object deliveryAddress = MakeCompanyAddress(customer, order.DeliveryAddress);

			// Make billing address.
			Object billingAddress = null;
			if (order.BillingAddress != null)
			{
				billingAddress = MakeCompanyAddress(customer, order.BillingAddress);
			}

			// Make goods list (product list) for kreditor api.
			Object[] goodsList = MakeGoods(order.OrderRowList);

			return KlarnaApi.ReserveAmountCompany(customer.Company.OrganisationNumber, totalAmount,
			                                        GetReference(order.DeliveryAddress), order.OrderId, deliveryAddress,
			                                        billingAddress ?? deliveryAddress, customer.EMail,
			                                        string.IsNullOrEmpty(customer.PhoneMobile)
			                                        	? string.Empty
			                                        	: customer.PhoneMobile, order.IPNumber, currency, country, language,
			                                        pnoEncoding, externalPaymentClassId, goodsList, customer.ChannelId);
		}
        */

		/// <summary>
		/// Reserves an amount at Kreditor. Can only be used in Sweden, Denmark, Norway and Finland.
		/// Mattias added Bookplus
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="customer"></param>
		/// <param name="order"></param>
		/// <param name="partPaymentId"></param>
		/// <param name="totalAmount"></param>
		/// <param name="yearlySalary"></param>
		/// <returns></returns>
		public static string ReserveAmount(IChannel channel, ICustomer customer, IOrderFull order,
		                                     int partPaymentId, int totalAmount, int? yearlySalary)
		{
			ValidateArguments(customer, order);
			//ValidateChannelIdForReserveAmount(Channel.Current.Id);

			// Channel specific variables.
			int currency;
			int country;
			int language;
			int pnoEncoding;

			// Sets channel specific variables.
			SetChannelSpecificVariables(channel, out currency, out country, out language, out pnoEncoding);

			// Make delivery address for kreditor api.
			Object deliveryAddress = MakeAddress(channel, customer, order.DeliveryAddress);

			// Make billing address for kreditor api.
			Object billingAddress = MakeAddress(channel, customer, order.BillingAddress);

			string civicNumber = customer.CustomerInformation.CivicNumber;
			// Format civicnumber so kreditor will accept it.
			civicNumber = FormatCivicNumber(civicNumber, channel);

			// Make goods list (product list) for kreditor api.
            Object[] goodsList = MakeGoods(channel, order.GetOrderItems(), order.FreightCost);
            
			return new KlarnaApi(channel).ReserveAmount(civicNumber, totalAmount, order.Id, deliveryAddress, billingAddress,
			                               customer.CustomerInformation.Email,
			                               string.IsNullOrEmpty(customer.CustomerInformation.CellPhoneNumber)
			                               	? string.Empty
			                               	: customer.CustomerInformation.CellPhoneNumber,
			                               order.IP, currency, country, language, pnoEncoding, partPaymentId,
										   yearlySalary ?? 0, goodsList);
		}

		/// <summary>
		/// Checks if a specific civicnumber has an account with Kreditor. Can only be used in Sweden, Denmark,
		/// Norway and Finland. 
		/// </summary>
		/// <param name="civicNumber"></param>
		/// <param name="channel"></param>
		/// <returns></returns>
		internal static bool HasAccount(string civicNumber, IChannel channel)
		{
			ValidateArgument(civicNumber);
			//ValidateChannelIdForHasAccount(channelId);

			// Format civicnumber so kreditor will accept it.
			civicNumber = FormatCivicNumber(civicNumber, channel);

			// Get civic encoding type.
			int pnoEncoding = GetCivicNumberEncoding(channel);

			return new KlarnaApi(channel).HasAccount(civicNumber, pnoEncoding);
		}

		/*

		/// <summary>
		/// Gets company info (IsCreditEligible, IAddress, CompanyName) from a specific orgnumber. Can only be used in Sweden.
		/// </summary>
		/// <param name="orgNumber"></param>
		/// <param name="channelId"></param>
		/// <returns></returns>
		internal static ICreditInformationCompany GetCompanyInfo(string orgNumber, int channelId)
		{
			ValidateArgument(orgNumber);

			if (channelId != (int) Localization.Channel.Sweden)
			{
				throw new ArgumentException("channelId");
			}

			IKreditorResponseItem response = KlarnaApi.GetCreditInformation(orgNumber, Constants.KREDITOR_PNO_SE,
			                                                                  Constants.KREDITOR_GA_NEW, channelId);

			if (response != null)
			{
				// Convert response to ICreditInformationCompany and return it.
				return ConvertToCreditInformationCompany(response);
			}

			return null;
		}


		/// <summary>
		/// Gets customer info (IsCreditEligible, HasAccount and IAddress) from a specific civicnumber. Can only be used in Sweden.
		/// </summary>
		/// <param name="civicNumber"></param>
		/// <param name="channelId"></param>
		/// <returns></returns>
		internal static ICreditInformationCustomer GetCustomerInfo(string civicNumber, int channelId)
		{
			ValidateArgument(civicNumber);

			if (channelId != (int) Localization.Channel.Sweden)
			{
				throw new ArgumentException("channelId");
			}

			IKreditorResponseItem response = KlarnaApi.GetCreditInformation(civicNumber, Constants.KREDITOR_PNO_SE,
			                                                                  Constants.KREDITOR_GA_NEW, channelId);

			if (response != null)
			{
				// Convert response to ICreditInformationCustomer and return it.
				return ConvertToCreditInformationCustomer(response);
			}

			return null;
		}
		*/

		/// <summary>
		/// Populates a PartPayment.
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="pClassId"></param>
		/// <param name="sum"></param>
		/// <param name="isCheckOut"></param>
		/// <returns></returns>
		public static int PopulatePartPayment(IChannel channel,int pClassId,  int sum, bool isCheckOut)
		{
			var klarnaSetting = new KlarnaSetting(channel.CommonName);
			//var pClassId = 100; //klarnaSetting.PClassId;

			int currency;
			SetChannelSpecificCurrency(channel, out currency);

			var flags = isCheckOut ? 0 : 1; //http://integration.klarna.com/api-functions/calc-monthly-cost
			return new KlarnaApi(channel).CalculateMonthlyCosts(sum, pClassId, flags, currency);
		}

		/// <summary>
		/// Builds a object with address information for use with Kreditor API
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="customer"></param>
		/// <param name="deliveryAddress"></param>
		/// <returns>The address as a object</returns>
		private static Object MakeAddress(IChannel channel, ICustomer customer, IOrderAddress deliveryAddress)
		{
			return new KlarnaApi(channel).MakeAddress(customer.CustomerInformation.FirstName,
				customer.CustomerInformation.LastName,
			    deliveryAddress.StreetAddress,
				deliveryAddress.PostalCode,
			    deliveryAddress.City);
		}

		/*
		/// <summary>
		/// Builds a object with address information for use with Kreditor API
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="address"></param>
		/// <returns>The address as a object</returns>
		private static Object MakeCompanyAddress(ICustomer customer, IAddress address)
		{
			string country = GetCountryISO();

			return KlarnaApi.MakeAddress(string.Empty,
			                               customer.CustomerInformation. != null ? customer.Company.CompanyName : string.Empty,
			                               address.StreetAddress, address.PostalNumber,
			                               address.City, country);
		}
		*/

		/// <summary>
		/// Builds a object array with goods (products) for use with Kreditor API
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="orderRowList"></param>
		/// <returns>The product list as an array of objects</returns>
		private static Object[] MakeGoods(IChannel channel, ICollection<IOrderItem> orderRowList,decimal freightCost)
		{
			// Kreditor uses iso-8859-1 encoding for product titles.
			Encoding iso88591 = Encoding.GetEncoding("iso-8859-1");
			Object[] goodsList = new Object[orderRowList.Count+1];

			int i = 0;
			foreach (IOrderItem orderRow in orderRowList)
			{
				string rowDescription = orderRow.Title;
				if (!string.IsNullOrEmpty(rowDescription))
				{
					byte[] bytes = iso88591.GetBytes(rowDescription);
					rowDescription = iso88591.GetString(bytes);
				}
				else
				{
					rowDescription = string.Empty;
				}

                //the original code is comented , as you can see it is sending the discount value allways 0
                //the new call to MakeGoods() method is sending in the row discount value in procent
                //Klarna can accept the discount by procent for each order row


                //Object goods = new KlarnaApi(channel).MakeGoods(orderRow.Quantity, orderRow.ProductId.ToString(), rowDescription,
                //                                   (int) (orderRow.ActualPrice.ExcludingVat * 100),
                //                                    (int) ((orderRow.GetActualVatAmount()/orderRow.ActualPrice.ExcludingVat) * 100), 0);

                Object goods = new KlarnaApi(channel).MakeGoods(orderRow.Quantity, orderRow.ProductId.ToString(), rowDescription,
                                   (int)(orderRow.ActualPrice.ExcludingVat * 100),
                                    (int)((orderRow.GetActualVatAmount() / orderRow.ActualPrice.ExcludingVat) * 100),
                                    (int)((ILekmerOrderItem)orderRow).RowDiscountInProcent(false));
				goodsList[i] = goods;
				i++;
			}

		    goodsList[i] = new KlarnaApi(channel).MakeShipment(1, "", "Shipping", (int) freightCost * 100, 25, 0);
			return goodsList;
		}

		#endregion

		#region Validate

		/*
		private static void ValidateChannelIdForHasAccount(int channelId)
		{
			if (channelId != (int) Localization.Channel.Sweden &&
			    channelId != (int) Localization.Channel.Norway &&
			    channelId != (int) Localization.Channel.Denmark &&
			    channelId != (int) Localization.Channel.Finland &&
			    channelId != (int) Localization.Channel.BookPlus)
			{
				throw new ArgumentException("channelId");
			}
		}
		  */
		/*
		/// <summary>
		/// Validates ChannelId
		/// </summary>
		/// <param name="channelId"></param>
		private static void ValidateChannelIdForGetAddress(int channelId)
		{
			if (channelId != (int) Localization.Channel.Sweden &&
			    channelId != (int) Localization.Channel.Norway &&
			    channelId != (int) Localization.Channel.Denmark)
			{
				throw new ArgumentException("channelId");
			}
		}
		*/

		/// <summary>
		/// Validates if argument is null.
		/// </summary>
		/// <param name="argument"></param>
		private static void ValidateArgument(string argument)
		{
			if (argument == null)
			{
				throw new ArgumentNullException("argument");
			}
		}

		/// <summary>
		/// Validates if arguments are null.
		/// </summary>
		/// <param name="customer"></param>
		/// <param name="order"></param>
		private static void ValidateArguments(ICustomer customer, IOrder order)
		{
			if (customer == null)
			{
				throw new ArgumentNullException("customer");
			}

			if (order == null)
			{
				throw new ArgumentNullException("order");
			}
		}

		/*
		private static void ValidateChannelIdForReserveAmount(int channelId)
		{
			if (channelId != (int) Localization.Channel.Sweden &&
			    channelId != (int) Localization.Channel.Norway &&
			    channelId != (int) Localization.Channel.Denmark &&
			    channelId != (int) Localization.Channel.Finland &&
			    channelId != (int) Localization.Channel.BookPlus)
			{
				throw new ArgumentException("channelId");
			}
		}
		*/

		#endregion

		/// <summary>
		/// Formats a civicnumber so Kreditor can accept it. In Sweden they accept "-" signs and a year format of 1950. 
		/// In Denmark, Norway and Finland they dont accept year format of 1950, here we need to remove 19. Also "-" signs
		/// are not allowed.
		/// </summary>
		/// <param name="civicNumber"></param>
		/// <param name="channel"></param>
		/// <returns></returns>
		private static string FormatCivicNumber(string civicNumber, IChannel channel)
		{
			var countryIso = channel.Country.Iso.ToLower();
			if (string.Compare(countryIso, "se", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber;
			}

			if (string.Compare(countryIso, "dk", StringComparison.InvariantCultureIgnoreCase) == 0 ||
			    string.Compare(countryIso, "no", StringComparison.InvariantCultureIgnoreCase) == 0 ||
			    string.Compare(countryIso, "fi", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				// Kreditor doesn't seem to like - signs.
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber.Replace("-", "");
			}

			// If country isn't supported, return input string.
			return civicNumber;
		}

		/// <summary>
		/// Sets channel specific constants
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="currency"></param>
		/// <param name="country"></param>
		/// <param name="language"></param>
		/// <param name="pnoEncoding"></param>
		public static void SetChannelSpecificVariables(IChannel channel, out int currency, out int country, out int language, out int pnoEncoding)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "dk":
					currency = Constants.KREDITOR_CURRENCY_DK;
					country = Constants.KREDITOR_COUNTRY_CODE_DK;
					language = Constants.KREDITOR_LANGUAGE_DK;
					pnoEncoding = Constants.KREDITOR_PNO_DK;
					break;
				case "fi":
					currency = Constants.KREDITOR_CURRENCY_FI;
					country = Constants.KREDITOR_COUNTRY_CODE_FI;
					language = Constants.KREDITOR_LANGUAGE_FI;
					pnoEncoding = Constants.KREDITOR_PNO_FI;
					break;
				case "no":
					currency = Constants.KREDITOR_CURRENCY_NO;
					country = Constants.KREDITOR_COUNTRY_CODE_NO;
					language = Constants.KREDITOR_LANGUAGE_NO;
					pnoEncoding = Constants.KREDITOR_PNO_NO;
					break;
				case "se":
					currency = Constants.KREDITOR_CURRENCY_SE;
					country = Constants.KREDITOR_COUNTRY_CODE_SE;
					language = Constants.KREDITOR_LANGUAGE_SE;
					pnoEncoding = Constants.KREDITOR_PNO_SE;
					break;
				default:
					throw new ArgumentException(string.Format("Channel id: \"{0}\" not supported.", channel.Id));
			}
		}
		private static void SetChannelSpecificCurrency(IChannel channel, out int currency)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "dk":
					currency = Constants.KREDITOR_CURRENCY_DK;
					break;
				case "fi":
					currency = Constants.KREDITOR_CURRENCY_FI;
					break;
				case "no":
					currency = Constants.KREDITOR_CURRENCY_NO;
					break;
				case "se":
					currency = Constants.KREDITOR_CURRENCY_SE;
					break;
				default:
					throw new ArgumentException(string.Format("Channel id: \"{0}\" not supported.", channel.Id));
			}
		}

		private static int GetCivicNumberEncoding(IChannel channel)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "dk":
					return Constants.KREDITOR_PNO_DK;
				case "fi":
					return Constants.KREDITOR_PNO_FI;
				case "no":
					return Constants.KREDITOR_PNO_NO;
				case "se":
					return Constants.KREDITOR_PNO_SE;
				default:
					throw new ArgumentException(string.Format("CountryISO: \"{0}\" not supported.", channel.Country.Iso));
			}
		}

		/*
		/// <summary>
		/// Converts response to a ICreditInformationCustomer
		/// </summary>
		/// <param name="response"></param>
		/// <returns></returns>
		private static ICreditInformationCompany ConvertToCreditInformationCompany(IKreditorResponseItem response)
		{
			ICreditInformationCompany creditInformationCompany = new CreditInformationCompany
			                                                     	{IsCreditEligible = bool.Parse(response.OkCustomer.ToString())};

			if (response.Address != null && response.Address.Length >= 5)
			{
				creditInformationCompany.CompanyName = response.Address[1];
				creditInformationCompany.Address = new Address
				                                   	{
				                                   		StreetAddress = response.Address[2],
				                                   		PostalNumber = response.Address[3],
				                                   		City = response.Address[4]
				                                   	};
			}
			else
			{
				creditInformationCompany.Address = null;
			}

			return creditInformationCompany;
		}
		
		/// <summary>
		/// Converts respons to a ICreditInformationCustomer
		/// </summary>
		/// <param name="response"></param>
		/// <returns></returns>
		private static ICreditInformationCustomer ConvertToCreditInformationCustomer(IKreditorResponseItem response)
		{
			ICreditInformationCustomer creditInformationCustomer = new CreditInformationCustomer
			                                                       	{
			                                                       		HasAccount =
			                                                       			bool.Parse(response.HasAccount.ToString()),
			                                                       		IsCreditEligible =
			                                                       			bool.Parse(response.OkCustomer.ToString())
			                                                       	};

			if (response.Address != null && response.Address.Length >= 5)
			{
				creditInformationCustomer.Address = new Address
				                                    	{
				                                    		FirstName = response.Address[0],
				                                    		LastName = response.Address[1],
				                                    		StreetAddress = response.Address[2],
				                                    		PostalNumber = response.Address[3],
				                                    		City = response.Address[4]
				                                    	};
			}
			else
			{
				creditInformationCustomer.Address = null;
			}

			return creditInformationCustomer;
		}
		*/

		/// <summary>
		/// Converts a Kreditor response item to a IAddress
		/// </summary>
		/// <param name="response"></param>
		/// <returns></returns>
		private static IKlarnaAddress ConvertResponseToAddress(IKreditorResponseItem response)
		{
			IKlarnaAddress address = null;

			// Kreditor can return several addresses, we only need the first.
			if (response.Addresses != null)
			{
				for (int rows = 0; rows < response.Addresses.GetLength(0);)
				{
					address = new KlarnaAddress();

					for (int cols = 0; cols < response.Addresses.GetLength(1); cols++)
					{
						// Add different properties depending on where in the multidimensional array
						// we are.
						switch (cols)
						{	
							case 0:
								address.FirstName = response.Addresses[rows, cols];
								break;
							case 1:
								address.LastName = response.Addresses[rows, cols];
								break;
							case 2:
								address.StreetAddress = response.Addresses[rows, cols];
								break;
							case 3:
								address.PostalCode = response.Addresses[rows, cols];
								break;
							case 4:
								address.City = response.Addresses[rows, cols];
								break;
							default:
								break;
						}
					}
					break;
				}
			}

			return address;
		}
	}
}