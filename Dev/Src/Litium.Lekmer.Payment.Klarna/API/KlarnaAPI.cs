﻿using System;
using System.Collections.Generic;
using Kreditor;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using log4net;

namespace Litium.Lekmer.Payment.Klarna
{
    public class KlarnaApi
    {
        // Create a logger for this class.
        private static readonly ILog log = LogManager.GetLogger(typeof (KlarnaApi));

        // Create a logger for ADONet logging.
        private static readonly ILog logAdoNet = LogManager.GetLogger("KreditorAdoNetLogger");

        /// <summary>
        /// Holds the timeout value depending on channel and order value
        /// </summary>
        private static readonly List<TimeOutData> TimeOutDataList = new List<TimeOutData>();

        private readonly IChannel _channel;

        /// <summary>
        /// Kreditor api's with higher timeout value.
        /// </summary>
        private readonly IDictionary<int, client> _clientList = new Dictionary<int, client>();

        /// <summary>
        /// Kreditor api
        /// </summary>
        private client _client;

        #region Constructor

        public KlarnaApi(IChannel channel)
        {
            _channel = channel;
            // Initialize the timeout dictionary.
            //InitializeTimeoutCache();

            // Create seperate api's per channel for every entry in the timeout dictionary.
            // These clients will have a higher timeout value (used for higher order values).
            InitializeClient();
        }

        #endregion

        #region Methods mapping directly against Kreditor API

        /// <summary>
        /// Used for making a goods list which will be used in conjuction with the Reserve amount call
        /// </summary>
        /// <param name="quantity"></param>
        /// <param name="artNo"></param>
        /// <param name="title"></param>
        /// <param name="price"></param>
        /// <param name="vat"></param>
        /// <param name="discount"></param>
        internal Object MakeGoods(
            int quantity,
            string artNo,
            string title,
            int price,
            int vat,
            double discount)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug("Make goods");
            }
            return _client.mk_goods(
                quantity,
                artNo,
                title,
                price,
                vat,
                discount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="quantity"></param>
        /// <param name="artNo"></param>
        /// <param name="title"></param>
        /// <param name="price"></param>
        /// <param name="vat"></param>
        /// <param name="discount"></param>
        internal Object MakeShipment(
            int quantity,
            string artNo,
            string title,
            int price,
            int vat,
            int discount
            )
        {
            if (log.IsDebugEnabled)
            {
                log.Debug("Make goods/flag");
            }
            return _client.mk_goods_flags(quantity, artNo, title, price, vat, discount,
                                          _client.INCVAT + _client.ISSHIPMENT);
        }

        /// <summary>
        /// Makes a object with a customers address for use as parameter in conjuction with the Reserve amount call
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="street"></param>
        /// <param name="zip"></param>
        /// <param name="city"></param>
        /// <returns></returns>
        internal Object MakeAddress(
            string firstName,
            string lastName,
            string street,
            string zip,
            string city)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug("Make address");
            }

            switch (_channel.Country.Iso.ToLower())
            {
                case "dk":
                    return _client.mk_address_dk(
                        firstName,
                        lastName,
                        street,
                        zip,
                        city);
                case "fi":
                    return _client.mk_address_fi(
                        firstName,
                        lastName,
                        street,
                        zip,
                        city);
                case "no":
                    return _client.mk_address_no(
                        firstName,
                        lastName,
                        street,
                        zip,
                        city);
                case "se":
                    return _client.mk_address_se(
                        firstName,
                        lastName,
                        street,
                        zip,
                        city);
                default:
                    throw new ArgumentException(string.Format("MakeAddress: \"{0}\" not supported.",
                                                              _channel.Country.Iso));
            }
        }

        /// <summary>
        /// Reserves an amount for a company.
        /// </summary>
        /// <param name="orgNumber"></param>
        /// <param name="amount"></param>
        /// <param name="reference"></param>
        /// <param name="orderId"></param>
        /// <param name="deliveryAddress"></param>
        /// <param name="billingAddress"></param>
        /// <param name="email"></param>
        /// <param name="phoneMobile"></param>
        /// <param name="ip"></param>
        /// <param name="currency"></param>
        /// <param name="country"></param>
        /// <param name="language"></param>
        /// <param name="pnoEncoding"></param>
        /// <param name="pClass"></param>
        /// <param name="goodsList"></param>
        /// <returns></returns>
        internal string ReserveAmountCompany(
            string orgNumber,
            int amount,
            string reference,
            int orderId,
            object deliveryAddress,
            object billingAddress,
            string email,
            string phoneMobile,
            string ip,
            int currency,
            int country,
            int language,
            int pnoEncoding,
            int pClass,
            object[] goodsList)
        {
            log.Debug("Reserve amount company");

            try
            {
                var klarnaSetting = new KlarnaSetting(_channel.CommonName);
                _client.reserve_amount(orgNumber, amount, reference, "", orderId.ToString(), orderId.ToString(),
                                       deliveryAddress,
                                       billingAddress, email, "", phoneMobile, ip, klarnaSetting.Mode, currency, country,
                                       language,
                                       klarnaSetting.EID, klarnaSetting.SecretKey, pnoEncoding, pClass, 0, goodsList);

                log.InfoFormat("Kreditor reservation made!. OrderId: {0}. ReferenceNumber: {1}", orderId, _client.Value);

                return _client.Value;
            }
            catch (KreditorException kex)
            {
                //PaymentSolutionException.ErrorCode errorCode = GetErrorCode(kex.FaultString);


                log.ErrorFormat(
                    "Kreditor call returned exception on reserve amount. OrderId: {0}, Fault code: {1}, Fault string: {2}, Fault reason: {3}",
                    orderId, kex.FaultCode, kex.FaultString, kex.FaultReason);


                if (kex.FaultCode == -99)
                {
                    throw new TimeoutException("Kreditor Exception: Timed out on ReserveAmount call.");
                }

                throw new Exception(kex.FaultString);
            }
        }

        /// <summary>
        /// Reserves a purchased amount to a particular customer. The reservation is valid, by default, in 7 days.
        /// </summary>
        /// <param name="civicNumber"></param>
        /// <param name="amount"></param>
        /// <param name="billingAddress"></param>
        /// <param name="email"></param>
        /// <param name="phoneMobile"></param>
        /// <param name="ip"></param>
        /// <param name="currency"></param>
        /// <param name="country"></param>
        /// <param name="language"></param>
        /// <param name="pClass"></param>
        /// <param name="yearlySalary"></param>
        /// <param name="orderId"></param>
        /// <param name="deliveryAddress"></param>
        /// <param name="pnoEncoding"></param>
        /// <param name="goodsList"></param>
        /// <returns>Reservation number</returns>
        internal string ReserveAmount(
            string civicNumber,
            int amount,
            int orderId,
            object deliveryAddress,
            object billingAddress,
            string email,
            string phoneMobile,
            string ip,
            int currency,
            int country,
            int language,
            int pnoEncoding,
            int pClass,
            int yearlySalary,
            object[] goodsList)
        {
            log.Debug("Reserve amount");

            try
            {
                var klarnaSetting = new KlarnaSetting(_channel.CommonName);
                _client.reserve_amount(civicNumber, amount, "", "", orderId.ToString(), orderId.ToString(),
                                       deliveryAddress,
                                       billingAddress, email, "", phoneMobile, ip, klarnaSetting.Mode, currency, country,
                                       language,
                                       klarnaSetting.EID, klarnaSetting.SecretKey, pnoEncoding, pClass, yearlySalary*100,
                                       goodsList);

                log.InfoFormat("Kreditor reservation made!. OrderId: {0}. ReferenceNumber: {1}", orderId, _client.Value);

                // Return reservation number.
                return _client.Value;
            }
            catch (KreditorException kex)
            {
                log.ErrorFormat(
                    "Kreditor call returned exception on reserve amount. OrderId: {0}, Fault code: {1}, Fault string: {2}, Fault reason: {3}",
                    orderId, kex.FaultCode, kex.FaultString, kex.FaultReason);


                if (kex.FaultCode == -99)
                {
                    throw new TimeoutException("Kreditor Exception: Timed out on ReserveAmount call.");
                }

                throw new Exception(kex.FaultString);
            }
        }

        /// <summary>
        /// This method will be used to calculate the monthly cost for values below 1000 (currency depending on which country).
        /// </summary>
        /// <param name="sum"></param>
        /// <param name="rate"></param>
        /// <param name="months"></param>
        /// <param name="flag">Can have two values; 0 or 1. </param>
        /// <param name="monthFee"></param>
        /// <param name="currency"></param>
        /// <returns></returns>
        internal int MonthlyCost(int sum, double rate, int months, int flag, int monthFee, int currency)
        {
            log.Debug("Monthly cost");

            return _client.monthly_cost(sum,
                                        rate,
                                        months,
                                        monthFee,
                                        flag,
                                        currency);
        }

        internal int TotalCreditPurchaseCost(int sum, int pclass, int currency)
        {
            log.Debug("TotalCreditPurchaseCost");

            return _client.total_credit_purchase_cost(sum, pclass, currency);
        }

        /// <summary>
        /// Returns whether the customer already has an account with kreditor or not. Is supposed to be
        /// used in Norway, Denmark and Finland.
        /// </summary>
        /// <param name="civicNumber"></param>
        /// <param name="pnoOrOrgEncoding"></param>
        /// <returns>True or false depending on the result</returns>
        internal bool HasAccount(string civicNumber, int pnoOrOrgEncoding)
        {
            log.Debug("Has account");
            var klarnaSetting = new KlarnaSetting(_channel.CommonName);

            try
            {
                _client.has_account(
                    klarnaSetting.EID,
                    civicNumber,
                    klarnaSetting.SecretKey,
                    pnoOrOrgEncoding);

                // Parse the result of the XML-RPC call.
                bool value;
                bool.TryParse(_client.Value, out value);
                return value;
            }
            catch (KreditorException kex)
            {
                log.ErrorFormat(
                    "Kreditor call returned exception. Fault code: {0}, Fault string: {1}, Fault reason: {2}",
                    kex.FaultCode, kex.FaultString, kex.FaultReason);
                if (kex.FaultCode == -99)
                {
                    throw new TimeoutException("Kreditor Exception: Timed out on HasAccount call.");
                }

                throw new Exception(kex.FaultString);
            }
        }


        public IKreditorResponseItem Fetch_Pclasses()
        {

            int currency;
            int country;
            int language;
            int pnoEncoding;

            KlarnaClient.SetChannelSpecificVariables(_channel, out currency, out country, out language, out pnoEncoding);


            var klarnaSetting = new KlarnaSetting(_channel.CommonName);
            _client.fetch_pclasses(klarnaSetting.EID,
                                   currency,
                                   klarnaSetting.SecretKey,
                                   country,
                                   language);

            //_client.get_pclasses(klarnaSetting.EID, Constants.KREDITOR_CURRENCY_SE, klarnaSetting.SecretKey);

            return BuildKreditorResponseItem_pclass(_client.ObjArr);
        }

        private static IKreditorResponseItem BuildKreditorResponseItem_pclass(object[] arrar)
        {
            try
            {
                if (arrar != null && arrar.Length > 0)
                {
                    return new KreditorResponseItem {PClasses = arrar};
                }
            }
            catch (Exception)
            {
                log.ErrorFormat("Kredtior call returned an invalid format in BuildKreditorResponseItem_pclass.");
            }

            return null;
        }


        public IKreditorResponseItem GetAddress(string civicNumber, int pnoEncoding)
        {
            log.Debug("Get addresses");
            var klarnaSetting = new KlarnaSetting(_channel.CommonName);

            try
            {
                _client.get_addresses(
                    civicNumber,
                    klarnaSetting.EID,
                    klarnaSetting.SecretKey,
                    pnoEncoding,
                    Constants.KREDITOR_GA_OLD);

                return BuildKreditorResponseItem(_client.Arrarr);
            }
            catch (KreditorException kex)
            {
                log.ErrorFormat(
                    "Kreditor call returned exception. Fault code: {0}, Fault string: {1}, Fault reason: {2}",
                    kex.FaultCode, kex.FaultString, kex.FaultReason);
                if (kex.FaultCode == -99)
                {
                    throw new TimeoutException("Kreditor Exception: Timed out on GetAddresses call.");
                }

                throw new Exception(kex.FaultString);
            }
        }

        internal int CalculateMonthlyCosts(int sum, int pc, int flags, int currency)
        {
            return _client.calc_monthly_cost(sum, pc, flags, currency);
        }

        #endregion

        #region Helper methods

        private void InitializeClient()
        {
            if (TimeOutDataList == null)
            {
                return;
            }
            var klarnaSetting = new KlarnaSetting(_channel.CommonName);
            string klarnaUrl = klarnaSetting.Url;

            _client = new client {Url = klarnaUrl, Timeout = klarnaSetting.Timeout};
            foreach (TimeOutData timeOutData in TimeOutDataList)
            {
                _clientList.Add(timeOutData.ChannelId, new client {Url = klarnaUrl, Timeout = timeOutData.TimeOutValue});
            }
        }


        /// <summary>
        /// Builds a IKreditorResponseItem from a Kreditor call result.
        /// </summary>
        /// <param name="objArr"></param>
        /// <returns></returns>
        private static IKreditorResponseItem BuildKreditorResponseItem(object[] objArr)
        {
            try
            {
                if (objArr != null)
                {
                    if (objArr.Length == 3)
                    {
                        bool noAddress = ((object[]) objArr[2]).Length == 0;

                        return new KreditorResponseItem
                                   {
                                       HasAccount = objArr[0],
                                       OkCustomer = objArr[1],
                                       Address = noAddress ? null : (string[]) ((object[]) objArr[2])[0]
                                   };
                    }
                }
            }
            catch (Exception)
            {
                log.ErrorFormat("Kreditor call returned an invalid format in GetCreditInformation.");
            }

            return null;
        }

        /// <summary>
        /// Builds a IKreditorResponseItem from a Kreditor call result.
        /// </summary>
        /// <param name="arrar"></param>
        /// <returns></returns>
        private static IKreditorResponseItem BuildKreditorResponseItem(string[,] arrar)
        {
            try
            {
                if (arrar != null && arrar.Length > 0)
                {
                    return new KreditorResponseItem {Addresses = arrar};
                }
            }
            catch (Exception)
            {
                log.ErrorFormat("Kredtior call returned an invalid format in BuildKreditorResponseItem.");
            }

            return null;
        }

        #endregion

        #region Timeout

        private class TimeOutData
        {
            public int ChannelId;
            public int OrderValue;
            public int TimeOutValue;
        }

        #endregion
    }
}