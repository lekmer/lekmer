﻿namespace Litium.Lekmer.Payment.Klarna
{
	public class KreditorResponseItem : IKreditorResponseItem
	{
		public object HasAccount { get; set; }
		public string[] Address { get; set; }
		public string[,] Addresses { get; set; }
		public object OkCustomer { get; set; }
        public object[] PClasses { get; set; }
	}
}
