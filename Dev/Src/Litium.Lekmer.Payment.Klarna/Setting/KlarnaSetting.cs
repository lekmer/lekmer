﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaSetting : SettingBase
	{
        private const string _storagename = "Klarna";

		protected override string StorageName
		{
            get { return _storagename; }
		}

        public static string GetStorageName()
        {
            return _storagename;
        }

	    private readonly string _groupName;

		public KlarnaSetting(string channelCommonName)
		{
			_groupName = channelCommonName;
		}

		public string EncodingIso
		{
			get { return GetString(_groupName, "EncodingIso"); }
		}

		public string SecretKey
		{
			get { return GetString(_groupName, "SecretKey"); }
		}

		public string Url
		{
			get { return GetString(_groupName, "Url"); }
		}

		public int Mode
		{
			get { return GetInt32(_groupName, "Mode"); }
		}


		public int Timeout
		{
			get { return GetInt32(_groupName, "Timeout"); }
		}

		public int EID
		{
			get { return GetInt32(_groupName, "EID"); }
		}

		public int PClassId
		{
			get { return GetInt32(_groupName, "PClassId"); }
		}


		public string PartPaymentMonths
		{
			get { return GetString(_groupName, "PartPaymentMonths"); }
		}
	}
}