﻿using System;
using Litium.Framework.Setting;
namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaPClassSetting : SettingBase
	{
	    private const string _storagename = "KlarnaPClass";

		protected override string StorageName
		{
            get { return _storagename; }
		}
        /// <summary>
        /// Gets the xml filename for Fetch_PClasses
        /// </summary>
        /// <returns></returns>
        public static string GetStorageName()
        {
            return _storagename;
        }

		private readonly string _groupName;

		public KlarnaPClassSetting(string channelCommonName, int noOfMonths)
		{
			_groupName = noOfMonths + "_" + channelCommonName;
		}

	    public int PClassId
		{
			get { return GetInt32(_groupName, "PClassId"); }
		}

		public int MinimumTotalCost
		{
			get { return GetInt32(_groupName, "MinimumTotalCost"); }
		}

		public int MonthlyFee
		{
			get { return GetInt32(_groupName, "MonthlyFee"); }
		}

		public int OneTimeFee
		{
			get { return GetInt32(_groupName, "OneTimeFee"); }
		}

		public int Rate
		{
			get { return GetInt32(_groupName, "Rate"); }
		}

		public int NoOfMonths
		{
			get { return GetInt32(_groupName, "NoOfMonths"); }
		}


		public string Description
		{
			get { return GetString(_groupName, "Description"); }
		}


		public string EncodingIso
		{
			get { return GetString(_groupName, "EncodingIso"); }
		}

		public int Currency
		{
			get { return GetInt32(_groupName, "Currency"); }
		}

		public int Type
		{
			get {  return GetInt32(_groupName, "Type"); }
		}
	}
}
