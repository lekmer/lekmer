
using System;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core.Web;

namespace Kreditor
{
	public class pclass
	{
		public bool correct_currency(int pclass, int curr)
		{
			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return (pclassSetting.Currency == curr);
				}
			}

			throw new Exception("Fel pclass");


		}

		public static int get_months(int pclass)
		{

			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.NoOfMonths;
				}
			}

			throw new Exception("Fel pclass");

		}

		public static int get_month_fee(int pclass)
		{

			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.MonthlyFee;
				}
			}

			throw new Exception("Fel pclass");


		}

		public static int get_start_fee(int pclass)
		{

			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.OneTimeFee;
				}
			}

			throw new Exception("Fel pclass");


		}

		public static int get_rate(int pclass)
		{
			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.Rate;
				}
			}

			throw new Exception("Fel pclass");



		}

		public static int get_type(int pclass)
		{
			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.Type;
				}
			}

			throw new Exception("Fel pclass");



		}

		public static int get_min_sum(int pclass)
		{

			var klarnaSetting = new KlarnaSetting(Channel.Current.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(Channel.Current.CommonName, temp);

				if (pclass == pclassSetting.PClassId)
				{
					return pclassSetting.MinimumTotalCost;
				}
			}

			throw new Exception("Fel pclass");



		}

		public static int[] get_pclass_ids_se()
		{
			return new[] { 100, 101, 102, 103, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123 };
		}
	}
}



////heppo live accounts
//using System;
//namespace Kreditor
//{
//    public class pclass
//    {
//        public static bool correct_currency(int pclass, int curr)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return (0 == curr);
//                case 118:
//                    return (0 == curr);
//                case 186:
//                    return (0 == curr);
//                case 339:
//                    return (0 == curr);
//                case 471:
//                    return (0 == curr);
//                case 608:
//                    return (0 == curr);
//                case 467:
//                    return (2 == curr);
//                case 484:
//                    return (2 == curr);
//                case 485:
//                    return (2 == curr);
//                case 486:
//                    return (2 == curr);
//                case 721:
//                    return (1 == curr);
//                case 722:
//                    return (1 == curr);
//                case 723:
//                    return (1 == curr);
//                case 724:
//                    return (1 == curr);
//                case 466:
//                    return (3 == curr);
//                case 487:
//                    return (3 == curr);
//                case 488:
//                    return (3 == curr);
//                case 489:
//                    return (3 == curr);
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_months(int pclass)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return 6;
//                case 118:
//                    return 24;
//                case 186:
//                    return 12;
//                case 339:
//                    return 3;
//                case 471:
//                    return 36;
//                case 608:
//                    return 24;
//                case 467:
//                    return 24;
//                case 484:
//                    return 6;
//                case 485:
//                    return 12;
//                case 486:
//                    return 36;
//                case 721:
//                    return 24;
//                case 722:
//                    return 6;
//                case 723:
//                    return 12;
//                case 724:
//                    return 36;
//                case 466:
//                    return 24;
//                case 487:
//                    return 6;
//                case 488:
//                    return 12;
//                case 489:
//                    return 36;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_month_fee(int pclass)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return 2900;
//                case 118:
//                    return 2900;
//                case 186:
//                    return 2900;
//                case 339:
//                    return 2900;
//                case 471:
//                    return 2900;
//                case 608:
//                    return 2900;
//                case 467:
//                    return 395;
//                case 484:
//                    return 395;
//                case 485:
//                    return 395;
//                case 486:
//                    return 395;
//                case 721:
//                    return 4500;
//                case 722:
//                    return 4500;
//                case 723:
//                    return 4500;
//                case 724:
//                    return 4500;
//                case 466:
//                    return 3900;
//                case 487:
//                    return 3900;
//                case 488:
//                    return 3900;
//                case 489:
//                    return 3900;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_start_fee(int pclass)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return 29500;
//                case 118:
//                    return 29500;
//                case 186:
//                    return 39500;
//                case 339:
//                    return 9500;
//                case 471:
//                    return 39500;
//                case 608:
//                    return 0;
//                case 467:
//                    return 0;
//                case 484:
//                    return 2995;
//                case 485:
//                    return 3995;
//                case 486:
//                    return 3995;
//                case 721:
//                    return 0;
//                case 722:
//                    return 29500;
//                case 723:
//                    return 39500;
//                case 724:
//                    return 39500;
//                case 466:
//                    return 0;
//                case 487:
//                    return 39500;
//                case 488:
//                    return 49500;
//                case 489:
//                    return 49500;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_rate(int pclass)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return 0;
//                case 118:
//                    return 995;
//                case 186:
//                    return 0;
//                case 339:
//                    return 0;
//                case 471:
//                    return 995;
//                case 608:
//                    return 1950;
//                case 467:
//                    return 2200;
//                case 484:
//                    return 0;
//                case 485:
//                    return 0;
//                case 486:
//                    return 1495;
//                case 721:
//                    return 2200;
//                case 722:
//                    return 0;
//                case 723:
//                    return 0;
//                case 724:
//                    return 1495;
//                case 466:
//                    return 2160;
//                case 487:
//                    return 0;
//                case 488:
//                    return 0;
//                case 489:
//                    return 1495;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_type(int pclass)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return 0;
//                case 118:
//                    return 0;
//                case 186:
//                    return 0;
//                case 339:
//                    return 0;
//                case 471:
//                    return 0;
//                case 608:
//                    return 1;
//                case 467:
//                    return 1;
//                case 484:
//                    return 0;
//                case 485:
//                    return 0;
//                case 486:
//                    return 0;
//                case 721:
//                    return 1;
//                case 722:
//                    return 0;
//                case 723:
//                    return 0;
//                case 724:
//                    return 0;
//                case 466:
//                    return 1;
//                case 487:
//                    return 0;
//                case 488:
//                    return 0;
//                case 489:
//                    return 0;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_min_sum(int pclass)
//        {
//            switch (pclass)
//            {
//                case 106:
//                    return 100000;
//                case 118:
//                    return 100000;
//                case 186:
//                    return 100000;
//                case 339:
//                    return 100000;
//                case 471:
//                    return 100000;
//                case 608:
//                    return 100;
//                case 467:
//                    return 153;
//                case 484:
//                    return 1;
//                case 485:
//                    return 1;
//                case 486:
//                    return 1;
//                case 721:
//                    return 1;
//                case 722:
//                    return 1;
//                case 723:
//                    return 1;
//                case 724:
//                    return 1;
//                case 466:
//                    return 1;
//                case 487:
//                    return 1;
//                case 488:
//                    return 1;
//                case 489:
//                    return 1;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int[] get_pclass_ids()
//        {
//            return new int[] { 106, 118, 186, 339, 471, 608, 467, 484, 485, 486, 721, 722, 723, 724, 466, 487, 488, 489 };
//        }

//    }
//}











////heppo test
//using System;
//namespace Kreditor
//{
//    public class pclass
//    {
//        public static bool correct_currency(int pclass, int curr)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return (0 == curr);
//                case 101:
//                    return (0 == curr);
//                case 102:
//                    return (0 == curr);
//                case 103:
//                    return (0 == curr);
//                case 111:
//                    return (3 == curr);
//                case 112:
//                    return (3 == curr);
//                case 113:
//                    return (3 == curr);
//                case 114:
//                    return (3 == curr);
//                case 115:
//                    return (2 == curr);
//                case 116:
//                    return (2 == curr);
//                case 117:
//                    return (2 == curr);
//                case 118:
//                    return (2 == curr);
//                case 120:
//                    return (1 == curr);
//                case 119:
//                    return (0 == curr);
//                case 121:
//                    return (1 == curr);
//                case 122:
//                    return (1 == curr);
//                case 123:
//                    return (1 == curr);
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_months(int pclass)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return 3;
//                case 101:
//                    return 6;
//                case 102:
//                    return 12;
//                case 103:
//                    return 24;
//                case 111:
//                    return 24;
//                case 112:
//                    return 6;
//                case 113:
//                    return 12;
//                case 114:
//                    return 36;
//                case 115:
//                    return 24;
//                case 116:
//                    return 6;
//                case 117:
//                    return 12;
//                case 118:
//                    return 36;
//                case 120:
//                    return 24;
//                case 121:
//                    return 6;
//                case 122:
//                    return 12;
//                case 123:
//                    return 36;
//                case 119:
//                    return 24;
//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_month_fee(int pclass)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return 2900;
//                case 101:
//                    return 2900;
//                case 102:
//                    return 2900;
//                case 103:
//                    return 2900;
//                case 111:
//                    return 3900;
//                case 112:
//                    return 3900;
//                case 113:
//                    return 3900;
//                case 114:
//                    return 3900;
//                case 115:
//                    return 395;
//                case 116:
//                    return 395;
//                case 117:
//                    return 395;
//                case 118:
//                    return 395;
//                case 119:
//                    return 2900;
//                case 120:
//                    return 4500;
//                case 121:
//                    return 4500;
//                case 122:
//                    return 4500;
//                case 123:
//                    return 4500;


//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_start_fee(int pclass)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return 9500;
//                case 101:
//                    return 29500;
//                case 102:
//                    return 39500;
//                case 103:
//                    return 39500;
//                case 111:
//                    return 0;
//                case 112:
//                    return 39500;
//                case 113:
//                    return 49500;
//                case 114:
//                    return 49500;
//                case 115:
//                    return 0;
//                case 116:
//                    return 2995;
//                case 117:
//                    return 3995;
//                case 118:
//                    return 3995;
//                case 119:
//                    return 0;
//                case 120:
//                    return 0;
//                case 121:
//                    return 29500;
//                case 122:
//                    return 39500;
//                case 123:
//                    return 39500;


//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_rate(int pclass)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return 0;
//                case 101:
//                    return 0;
//                case 102:
//                    return 0;
//                case 103:
//                    return 995;
//                case 111:
//                    return 2200;
//                case 112:
//                    return 0;
//                case 113:
//                    return 0;
//                case 114:
//                    return 1995;
//                case 115:
//                    return 2200;
//                case 116:
//                    return 0;
//                case 117:
//                    return 0;
//                case 118:
//                    return 1495;
//                case 119:
//                    return 1950;
//                case 120:
//                    return 2200;
//                case 121:
//                    return 0;
//                case 122:
//                    return 0;
//                case 123:
//                    return 1495;


//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_type(int pclass)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return 0;
//                case 101:
//                    return 0;
//                case 102:
//                    return 0;
//                case 103:
//                    return 0;
//                case 111:
//                    return 1;
//                case 112:
//                    return 0;
//                case 113:
//                    return 0;
//                case 114:
//                    return 0;
//                case 115:
//                    return 1;
//                case 116:
//                    return 0;
//                case 117:
//                    return 0;
//                case 118:
//                    return 0;
//                case 119:
//                    return 1;
//                case 120:
//                    return 1;
//                case 121:
//                    return 0;
//                case 122:
//                    return 0;
//                case 123:
//                    return 0;


//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int get_min_sum(int pclass)
//        {
//            switch (pclass)
//            {
//                case 100:
//                    return 100000;
//                case 101:
//                    return 100000;
//                case 102:
//                    return 100000;
//                case 103:
//                    return 100000;
//                case 111:
//                    return 1;
//                case 112:
//                    return 1;
//                case 113:
//                    return 1;
//                case 114:
//                    return 1;
//                case 115:
//                    return 1;
//                case 116:
//                    return 1;
//                case 117:
//                    return 1;
//                case 118:
//                    return 1;
//                case 119:
//                    return 1;
//                case 120:
//                    return 1;
//                case 121:
//                    return 0;
//                case 122:
//                    return 1;
//                case 123:
//                    return 0;


//            }
//            throw new Exception("Fel pclass");
//        }

//        public static int[] get_pclass_ids_se()
//        {
//            return new int[] { 100, 101, 102, 103, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123 };
//        }

//    }
//}
