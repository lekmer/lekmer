﻿using System;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Monitor
{
    internal class Program
    {
		private const int _breakDuration = 1; /*seconds*/
		private const int _portionSize = 10; /*number*/

        private static void Main()
        {
            Console.WriteLine("Monitor Product Console");
            Console.WriteLine("-----------------------");
            Console.WriteLine("Press any key to start the products monitoring process.");
            Console.ReadLine();
            Console.WriteLine("Monitor is working, please wait...");

            var monitorService = IoC.Resolve<IMonitorProductService>();
			monitorService.Monitor(_portionSize, _breakDuration);
           
            Console.WriteLine("Done!");
            Console.ReadLine();
        }
    }
}
