﻿using System.Configuration;
using System.Timers;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Foundation;
using ServiceBase = System.ServiceProcess.ServiceBase;

namespace Litium.Lekmer.Product.MonitorService
{
	public partial class MonitorProductWinService : ServiceBase
	{
		private const string _timerIntervalSettingName = "TimerInterval";
		private const string _breakDurationSettingName = "BreakDuration";
		private const string _portionSizeSettingName = "PortionSize";
		private const int _timerIntervalDefaultValue = 86400; /*seconds (24hours)*/
		private const int _breakDurationDefaultValue = 10; /*seconds*/
		private const int _portionSizeDefaultValue = 100; /*number*/
		private readonly Timer _timer;
        private readonly IMonitorProductService _monitorProductService;
		private readonly int _breakDuration;
		private readonly int _portionSize;

		public MonitorProductWinService()
		{
			InitializeComponent();

			var timerIntervalInSeconds = GetSetting(_timerIntervalSettingName, _timerIntervalDefaultValue);
			_timer = new Timer(timerIntervalInSeconds * 1000)
			{
				Enabled = false,
				AutoReset = true
			};
			_timer.Elapsed += TimerElapsed;

		    _monitorProductService = IoC.Resolve<IMonitorProductService>();
			_breakDuration = GetSetting(_breakDurationSettingName, _breakDurationDefaultValue);
			_portionSize = GetSetting(_portionSizeSettingName, _portionSizeDefaultValue);
		}

		protected override void OnStart(string[] args)
		{
			_timer.Start();
		}

		protected override void OnStop()
		{
			_timer.Stop();
		}

		private void TimerElapsed(object sender, ElapsedEventArgs e)
		{
			_monitorProductService.Monitor(_portionSize, _breakDuration);
		}

		private static int GetSetting(string settingName, int defaultValue)
		{
			int value;
			var setting = ConfigurationManager.AppSettings[settingName];
			if (!string.IsNullOrEmpty(setting) && int.TryParse(setting, out value))
				return value;
			return defaultValue;
		}
	}
}
