using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Voucher
{
	public interface ILekmerOrderCampaignInfo : IOrderCampaignInfo
	{
		decimal? VoucherDiscount { get; set; }
	}
}