namespace Litium.Lekmer.Voucher
{
	public interface IVoucherCheckResult
	{
		string VoucherCode { get; set; }
		bool IsValid { get; set; }
        int? VoucherbatchId { get; set; }
        decimal DiscountValue { get; set; }
        VoucherValueType ValueType { get; set; }
	}
}