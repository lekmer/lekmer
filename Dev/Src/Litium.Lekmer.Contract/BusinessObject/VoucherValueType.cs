using System;

namespace Litium.Lekmer.Voucher
{
	[Serializable]
	public enum VoucherValueType
	{
		Percentage,
		Price
	}
}