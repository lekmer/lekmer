using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Voucher
{
	public interface ILekmerCartCampaignInfo : ICartCampaignInfo
	{
		decimal? VoucherDiscount { get; set; }
		VoucherValueType VoucherDiscountType { get; set; }
	}
}