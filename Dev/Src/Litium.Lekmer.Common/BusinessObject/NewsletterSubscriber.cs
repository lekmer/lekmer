﻿using System;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Common.BusinessObject
{
	[Serializable]
	public class NewsletterSubscriber : BusinessObjectBase, INewsletterSubscriber
	{
		private int _id;
		private int _channelId;
		private string _email;
		private DateTime _createdDate;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}
	}
}
