﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Common.Cache
{
	public sealed class BlockContactUsCache : ScensumCacheBase<BlockContactUsKey, IBlockContactUs>
	{
		private static readonly BlockContactUsCache _instance = new BlockContactUsCache();

		private BlockContactUsCache()
		{
		}

		public static BlockContactUsCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			foreach (IChannel channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockContactUsKey(channel.Id, blockId));
			}
		}
	}

	public class BlockContactUsKey : ICacheKey
	{
		public BlockContactUsKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}