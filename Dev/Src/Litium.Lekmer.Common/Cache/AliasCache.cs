﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Template;

namespace Litium.Lekmer.Common.Cache
{
	public sealed class AliasCache : ScensumCacheBase<AliasKey, IAlias>
	{
		private static readonly AliasCache _instance = new AliasCache();

		private AliasCache()
		{
		}

		public static AliasCache Instance
		{
			get { return _instance; }
		}
	}

	public class AliasKey : ICacheKey
	{
		public AliasKey(int channelId, string commonName)
		{
			ChannelId = channelId;
			CommonName = commonName;
		}

		public int ChannelId { get; set; }
		public string CommonName { get; set; }

		public string Key
		{
			get
			{
				return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + CommonName;
			}
		}
	}
}