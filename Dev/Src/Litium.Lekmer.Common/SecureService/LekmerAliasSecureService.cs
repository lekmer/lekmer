﻿using Litium.Lekmer.Common.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Repository;

namespace Litium.Lekmer.Common
{
	public class LekmerAliasSecureService : AliasSecureService
	{
		public LekmerAliasSecureService(IAccessValidator accessValidator, AliasRepository aliasRepository) : base(accessValidator, aliasRepository)
		{
		}

		public override int Save(ISystemUserFull systemUserFull, IAlias alias)
		{
			var result = base.Save(systemUserFull, alias);
			AliasCache.Instance.Flush();
			return result;
		}

		public override void Delete(ISystemUserFull systemUserFull, int aliasId)
		{
			base.Delete(systemUserFull, aliasId);
			AliasCache.Instance.Flush();
		}

		public override void DeleteAllByAliasFolder(ISystemUserFull systemUserFull, int folderId)
		{
			base.DeleteAllByAliasFolder(systemUserFull, folderId);
			AliasCache.Instance.Flush();
		}
	}
}
