﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Cache;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Lekmer.Common.Contract.SecureService;
using Litium.Lekmer.Common.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Common.SecureService
{
	public class BlockContactUsSecureService : IBlockContactUsSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
        protected BlockContactUsRepository Repository { get; private set; }
        protected IAccessSecureService AccessService { get; private set; }
        protected IBlockSecureService BlockService { get; private set; }

        public BlockContactUsSecureService(
			IAccessValidator accessValidator,
            BlockContactUsRepository repository, 
            IAccessSecureService accessSecureService,
            IBlockSecureService blockSecureService)
		{
			AccessValidator = accessValidator;
            Repository = repository;
            AccessService = accessSecureService;
            BlockService = blockSecureService;
        }

        public IBlockContactUs Create()
        {
            if (AccessService == null)
                throw new InvalidOperationException("AccessService must be set before calling Create.");
			
            var block = IoC.Resolve<IBlockContactUs>();
            block.AccessId = AccessService.GetByCommonName("All").Id;
            block.Status = BusinessObjectStatus.New;
            return block;
        }

		public IBlockContactUs GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
        {
            if (title == null) throw new ArgumentNullException("title");

            IBlockContactUs block = Create();
            block.ContentNodeId = contentNodeId;
            block.ContentAreaId = contentAreaId;
            block.BlockTypeId = blockTypeId;
            block.BlockStatusId = (int)BlockStatusInfo.Offline;
            block.Title = title;
            block.TemplateId = null;
			block.Receiver = string.Empty;
			block.Id = Save(systemUserFull, block);
        	return block;
        }

		public int Save(ISystemUserFull systemUserFull, IBlockContactUs block)
		{
			if (block == null)
				throw new ArgumentNullException("block");
			if (BlockService == null)
				throw new InvalidOperationException("BlockService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transaction = new TransactedOperation())
			{
				block.Id = BlockService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;
				Repository.Save(block);
				transaction.Complete();
			}
			BlockContactUsCache.Instance.Remove(block.Id);
			return block.Id;
		}

        public void Delete(ISystemUserFull systemUserFull, int blockId)
        {
            AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
            using (TransactedOperation transaction = new TransactedOperation())
            {
                Repository.Delete(blockId);
                transaction.Complete();
            }
			BlockContactUsCache.Instance.Remove(blockId);
        }
    }
}