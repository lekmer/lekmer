﻿using Litium.Lekmer.Common.Cache;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Lekmer.Common.Contract.Service;
using Litium.Lekmer.Common.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Common.Service
{
	public class BlockContactUsService : IBlockContactUsService
    {
        protected BlockContactUsRepository Repository { get; private set; }

		public BlockContactUsService(BlockContactUsRepository repository)
		{
			Repository = repository;
		}

        public IBlockContactUs GetById(IUserContext context, int id)
        {
			return BlockContactUsCache.Instance.TryGetItem(
				new BlockContactUsKey(context.Channel.Id, id),
				delegate { return Repository.GetById(context.Channel, id); });
        }
    }
}