﻿using System;
using Litium.Lekmer.Common.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Repository;

namespace Litium.Lekmer.Common
{
	public class LekmerAliasSharedService : AliasSharedService
	{
		public LekmerAliasSharedService(AliasRepository aliasRepository) : base(aliasRepository)
		{
		}

		public override IAlias GetByCommonName(IChannel channel, string commonName)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");
			return AliasCache.Instance.TryGetItem(new AliasKey(channel.Id, commonName),
				delegate { return GetByCommonNameCore(channel, commonName); }
				);
		}

		protected IAlias GetByCommonNameCore(IChannel channel, string commonName)
		{
			return Repository.GetByCommonName(channel.Language.Id, commonName);
		}
	}
}
