﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Lekmer.Common.Contract.Service;
using Litium.Lekmer.Common.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Common.Service
{
	public class NewsletterSubscriberService : INewsletterSubscriberService
	{
		protected NewsletterSubscriberRepository Repository { get; private set; }

		public NewsletterSubscriberService(NewsletterSubscriberRepository repository)
		{
			Repository = repository;
		}

        public INewsletterSubscriber Create(IUserContext context)
		{
        	INewsletterSubscriber subscriber = IoC.Resolve<INewsletterSubscriber>();
        	subscriber.CreatedDate = DateTime.Now;
        	subscriber.ChannelId = context.Channel.Id;
			subscriber.Status = BusinessObjectStatus.New;
        	return subscriber;
		}

		public int Save(IUserContext context, INewsletterSubscriber subscriber)
		{
			if (subscriber == null) throw new ArgumentNullException("subscriber");
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				subscriber.Id = Repository.Save(subscriber);
				transactedOperation.Complete();
			}
			return subscriber.Id;
		}

		public bool Delete(IUserContext context, string email)
		{
			int result;
			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				result = Repository.Delete(context.Channel.Id, email);
				transactedOperation.Complete();
			}
			return result > 0;
		}
	}
}
