﻿using System;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Cache;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.Common
{
	public class LekmerContentPageService : ContentPageService
	{
		public LekmerContentPageService(ContentPageRepository repository, IContentAreaService contentAreaService, IContentPageSeoSettingService contentPageSeoSettingService) : base(repository, contentAreaService, contentPageSeoSettingService)
		{
		}

		public override IContentPageFull GetFullById(IUserContext context, int id)
		{
			if (ContentAreaService == null)
				throw new InvalidOperationException("ContentAreaService must be set before calling GetFullById.");
			if (ContentPageSeoSettingService == null)
				throw new InvalidOperationException("ContentPageSeoSettingService must be set before calling GetFullById.");

			return ContentPageFullCache.Instance.TryGetItem(
				new ContentPageFullKey(context.Channel.Id, id),
				delegate { return GetFullByIdCore(context, id); });
		}

		protected override IContentPageFull GetFullByIdCore(IUserContext context, int id)
		{
			var contentPageFull = base.GetFullByIdCore(context, id);
			if (contentPageFull != null)
			{
				contentPageFull.ContentAreas = ContentAreaService.GetAllFullByTemplate(context, contentPageFull.TemplateId, id);
				contentPageFull.ContentPageSeoSetting = ContentPageSeoSettingService.GetById(context, id);

				AppendOfflineBlocks(contentPageFull);
			}
			return contentPageFull;
		}

		// This is a hack to work around the bug where master blocks placed last disappear.
		private static void AppendOfflineBlocks(IContentPageFull contentPage)
		{
			if (contentPage == null) throw new ArgumentNullException("contentPage");

			if (!contentPage.MasterPageId.HasValue) return;

			int id = 10000000;
			foreach (IContentAreaFull contentArea in contentPage.ContentAreas)
			{
				if (!contentArea.Blocks.Any()) continue;

				var block = IoC.Resolve<IBlock>();
				block.Id = id++;
				block.Ordinal = int.MaxValue;
				block.ContentNodeId = contentPage.Id;
				block.ContentAreaId = contentArea.Id;
				block.BlockTypeId = 10;
				block.BlockStatusId = (int)BlockStatusInfo.Offline;
				block.Title = "Dummy";

				contentArea.Blocks.Add(block);
			}
		}
	}
}
