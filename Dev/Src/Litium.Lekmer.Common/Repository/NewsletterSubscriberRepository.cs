﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Common.Contract.BusinessObject;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Common.Repository
{
	public class NewsletterSubscriberRepository
	{
		protected DataMapperBase<INewsletterSubscriber> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INewsletterSubscriber>(dataReader);
		}

		public int Save(INewsletterSubscriber subscriber)
		{
			if (subscriber == null) throw new ArgumentNullException("subscriber");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", subscriber.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", subscriber.Email, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@CreatedDate", subscriber.CreatedDate, SqlDbType.DateTime),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue(
				"[lekmer].[pNewsletterSubscriberSave]", parameters, dbSettings);
		}

		public int Delete(int channelId, string email)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", email, SqlDbType.VarChar)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("NewsletterSubscriberRepository.Delete");
			return new DataHandler().ExecuteCommandWithReturnValue(
				"[lekmer].[pNewsletterSubscriberDelete]", parameters, dbSettings);
		}
	}
}