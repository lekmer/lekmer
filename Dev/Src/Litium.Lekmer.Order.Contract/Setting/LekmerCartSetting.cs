﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Order.Setting
{
    public sealed class LekmerCartSetting : SettingBase
    {
        #region Singleton

        private static readonly LekmerCartSetting _instance = new LekmerCartSetting();

        private LekmerCartSetting()
        {
        }

        public static LekmerCartSetting Instance
        {
            get { return _instance; }
        }

        #endregion

        private const string _groupName = "LekmerCart";

        protected override string StorageName
        {
            get { return "LekmerCart"; }
        }

        public string ShoppingCardCookiename
        {
            get { return (GetString(_groupName, "ShoppingCardCookiename", "ShoppingCard")); }
        }

        public int LifeCycleValue
        {
            get { return GetInt32(_groupName, "LifeCycleValue", 30); }
        }
    }
}