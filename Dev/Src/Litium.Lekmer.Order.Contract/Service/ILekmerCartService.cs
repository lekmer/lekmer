﻿using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
  public interface ILekmerCartService : ICartService
  {
       void RetrivePreviousShoppingCard();
      void UpdateCreatedDate(ICartFull cart);
  }
}
