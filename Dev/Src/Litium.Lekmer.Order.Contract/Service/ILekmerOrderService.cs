using System.Collections.Generic;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Contract.Service
{
	public interface ILekmerOrderService : IOrderService
	{
		int SaveFullExtended(IUserContext context, IOrderFull order);
		void UpdateNumberInStock(IUserContext context, IOrderFull order);
		bool CheckNumberInStock(IUserContext context, IOrderFull order);
		bool CheckNumberInStock(IUserContext context, ICartFull cart);

		bool CheckNumberInStock(IUserContext context, ICartItem cartItem,
		                        IEnumerable<IGrouping<KeyValuePair<int, int?>, ICartItem>> groupedCartItems);
	}
}