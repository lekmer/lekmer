﻿using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderSecureService : IOrderSecureService
	{
		bool CanChangePaymentCost(IOrderFull order, decimal previousPaymentCost);
	}
}
