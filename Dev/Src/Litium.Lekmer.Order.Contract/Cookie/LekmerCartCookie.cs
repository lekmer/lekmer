﻿using System;
using System.Web;
using Litium.Lekmer.Order.Setting;

namespace Litium.Lekmer.Order.Cookie
{
    public static class LekmerCartCookie
    {
        public static Guid GetShoppingCardGuid()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[LekmerCartSetting.Instance.ShoppingCardCookiename];
            if (cookie == null) return Guid.Empty;
            try
            {
                var cardGuid = new Guid(cookie.Value);
                return cardGuid;
            }
            catch (FormatException)
            {
                return Guid.Empty;
            }
        }

        public static void SetShoppingCardGuid(Guid shoppingCardGuid)
        {
            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(354, 50, 0, 0, 0);
            HttpCookie cookie = HttpContext.Current.Request.Cookies[LekmerCartSetting.Instance.ShoppingCardCookiename];
            if (cookie == null)
            {
                cookie = new HttpCookie(LekmerCartSetting.Instance.ShoppingCardCookiename);
            }
            cookie.Expires = dtNow + tsMinute;
            cookie.Value = shoppingCardGuid.ToString();
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public static void DeleteShoppingCardGuid()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[LekmerCartSetting.Instance.ShoppingCardCookiename];
            if (cookie != null)
            {
                cookie.Value = Guid.Empty.ToString();
                cookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
    }
}