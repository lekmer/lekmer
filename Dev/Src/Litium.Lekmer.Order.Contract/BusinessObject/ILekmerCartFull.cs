﻿using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	public interface ILekmerCartFull : ICartFull
	{
		void ChangeItemQuantity(IProduct product, int quantity, int? sizeId, string erpId);
		void ChangeItemQuantityForBlockExtendedCartControl(IProduct product, int quantity, int? sizeId, string erpId);
		void AddItem(IProduct product, int quantity, int? sizeId, string erpId, int cartItemId);
	}
}
