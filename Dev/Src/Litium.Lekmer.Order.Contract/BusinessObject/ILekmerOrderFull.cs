using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderFull : IOrderFull
	{
		decimal PaymentCost { get; set; }
		string CustomerIdentificationKey { get; set; }
		decimal? VoucherDiscount { get; set; }

		decimal? GetActualVoucherDiscount();

		decimal GetActualFreightCostVat();
		
		decimal GetFreightCostVat();

		decimal GetPaymentCostVat();

		/// <summary>
		/// Gets the total price including order items price summary, freight cost and payment cost
		/// </summary>
		Price GetPriceTotal();

		/// <summary>
		/// Gets the total price including order items price summary, freight cost and payment cost (taking into account campaigns).
		/// </summary>
		Price GetActualPriceTotal();

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT
		/// </summary>
		decimal GetVatTotal();

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT (taking into account campaigns).
		/// </summary>
		decimal GetActualVatTotal();

		void MergeWith(IUserContext context, Collection<IOrderPayment> payments);
	}
}