﻿using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerPaymentType : IPaymentType
	{
		decimal Cost { get; set; }
	}
}
