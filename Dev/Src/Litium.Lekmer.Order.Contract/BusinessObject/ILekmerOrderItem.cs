using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Media;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderItem : IOrderItem
	{
		IOrderItemSize OrderItemSize { get; set; }
		IProductSize Size { get; set; }
		int? BrandId { get; set; }
		IBrand Brand { get; set;}
		IImage Image { get; set;}
		Collection<ITagGroup> TagGroups { get; set; }

		bool HasSize { get; }
		bool HasBrand { get; }
		bool HasImage { get; }
	    decimal RowDiscount(bool isDiscountIncludingVat);
	    double RowDiscountInProcent(bool isDiscountIncludingVat);
	}
}