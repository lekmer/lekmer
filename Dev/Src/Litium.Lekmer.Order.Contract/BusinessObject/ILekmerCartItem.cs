using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerCartItem : ICartItem
	{
		int? SizeId { get; set; }
		string ErpId { get; set; } 
	}
}