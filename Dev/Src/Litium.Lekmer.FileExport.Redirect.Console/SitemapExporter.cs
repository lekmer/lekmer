using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.FileExport
{
    public class SitemapExporter : IExporter
    {
        #region IExporter Members

        public void Execute()
        {
            foreach (IUserContext context in GetUserContextForAllChannels())
            {
                var sitemapItems = new Collection<SitemapItem>();

                AppendPages(context, sitemapItems);
                AppendProducts(context, sitemapItems);

                new ExportXml().ExportSitemap(sitemapItems, context.Channel.CommonName);
            }
        }

        #endregion

        // appends all pages to collection
        private static void AppendPages(IUserContext context, Collection<SitemapItem> sitemapItems)
        {
            var contentNodeService = IoC.Resolve<IContentNodeService>();
            IContentNodeTree tree = contentNodeService.GetAllAsTree(context);

            AppendPageChildren(context, sitemapItems, tree.Root);
        }

        // appends all children to pages
        private static void AppendPageChildren(IUserContext context, Collection<SitemapItem> sitemapItems,
                                               IContentNodeTreeItem parent)
        {
            foreach (IContentNodeTreeItem item in parent.Children)
            {
                IContentNode contentNode = item.ContentNode;
                if (contentNode == null) continue;

                var status = (ContentNodeStatusInfo) contentNode.ContentNodeStatusId;
                var nodeType = (ContentNodeTypeInfo) contentNode.ContentNodeTypeId;
                var access = (AccessInfo) contentNode.AccessId;
                bool isOnline = status == ContentNodeStatusInfo.Online;
                bool isPage = nodeType == ContentNodeTypeInfo.Page;
                bool isAccessibleForEveryone = access == AccessInfo.All;
                string url = item.Url;
                if (isOnline && isPage && isAccessibleForEveryone && !string.IsNullOrEmpty(url))
                {
                    url = url.Substring(1); //Remove ~ in the beginning.
                    if (contentNode.CommonName != null &&
                        contentNode.CommonName.Equals("start", StringComparison.OrdinalIgnoreCase))
                    {
                        url = "/";
                    }

                    var sitemapItem = new SitemapItem
                                          {
                                              Url = "http://" + context.Channel.ApplicationName + url,
                                              Priority = 0.5m,
                                              ChangeFreq = "daily",
                                              ContentnodeId = contentNode.Id
                                          };
                    sitemapItems.Add(sitemapItem);
                }

                AppendPageChildren(context, sitemapItems, item);
            }
        }

        // appends all products
        private static void AppendProducts(IUserContext context, Collection<SitemapItem> sitemapItems)
        {
            int page = 1; // page best�mmer om f�rsta 100 ska visas eller andra hundra osv.
            int pageSize; // pagesize best�mmer hur m�nga som ska h�mtas �t g�ngen

            if (!int.TryParse(ConfigurationManager.AppSettings["PageSize"], out pageSize))
            {
                pageSize = 100;
            }


            var lekmerProductService = (ILekmerProductService) IoC.Resolve<IProductService>();
            ProductCollection products = lekmerProductService.GetAll(context, page, pageSize);
            //int totalCount = Math.Min(products.TotalCount, 200);
            int totalCount = products.TotalCount;


            while (totalCount > 0)
            {
                products = lekmerProductService.GetAll(context, page, pageSize);

                foreach (IProduct product in products)
                {
                    var lekmerProduct = (ILekmerProduct) product;

                    var sitemapItem = new SitemapItem
                                          {
                                              Url = "http://" + context.Channel.ApplicationName + "/" + lekmerProduct.LekmerUrl,
                                              Priority = 1,
                                              ChangeFreq = "monthly",
                                              ProductId = lekmerProduct.Id
                                          };
                    sitemapItems.Add(sitemapItem);
                }

                totalCount = totalCount - pageSize;
                page++;
            }
        }

        private static IEnumerable<IUserContext> GetUserContextForAllChannels()
        {
            //generera 5 filer en per f�rs�ljningskanal + .com

            var channelService = (LekmerChannelService) IoC.Resolve<IChannelService>();
            Collection<IChannel> channels = channelService.GetAll();

            return channels
                .Select(channel =>
                            {
                                var context = IoC.Resolve<IUserContext>();
                                context.Channel = channel;
                                return context;
                            });
        }
    }
}
