﻿namespace Litium.Lekmer.FileExport
{
	public interface IExporter
	{
		void Execute();
	}
}