﻿using System;

namespace Litium.Lekmer.FileExport
{
	internal class Program
	{
		private static void Main()
		{
			var exporters = new IExporter[]
			                	{
			                		new SitemapExporter()
			                	};

			foreach (IExporter exporter in exporters)
			{
			    try
			    {
                    exporter.Execute();
			    }
			    catch (Exception e)
			    {
			       //throw;
                    Console.WriteLine(e.ToString());

			        //Console.Read();
			    }
				
			}
		}
	}
}