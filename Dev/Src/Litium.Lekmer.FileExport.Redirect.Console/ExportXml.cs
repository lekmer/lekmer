﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Media;
using System.Globalization;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Lekmer.FileExport
{
	internal class ExportXml
	{

        public void ExportSitemap(IEnumerable<SitemapItem> sitemapItems, string channelCommonName)
		{
            var doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode urlsetNode = doc.CreateElement("urlset");
            //XmlAttribute urlsetAtribute = doc.CreateAttribute("xmlns");
            //urlsetAtribute.Value = "http://www.sitemaps.org/schemas/sitemap/0.9";
            //urlsetNode.Attributes.Append(urlsetAtribute);
            //XmlAttribute urlsetAtribute2 = doc.CreateAttribute("export_date");
            //urlsetAtribute2.Value = DateTime.Now.ToString();
            //urlsetNode.Attributes.Append(urlsetAtribute2);
            doc.AppendChild(urlsetNode);


			foreach (SitemapItem sitemapItem in sitemapItems)
			{
                XmlNode urlNode = doc.CreateElement("url");
			    urlsetNode.AppendChild(urlNode);

                XmlNode locNode = doc.CreateElement("loc");
			    locNode.AppendChild(doc.CreateTextNode(sitemapItem.Url));
			    urlNode.AppendChild(locNode);

                //XmlNode lastModeNode
                if (sitemapItem.LastMod.HasValue)
                {
                    XmlNode lastModNode = doc.CreateElement("lastmod");
                    lastModNode.AppendChild(doc.CreateTextNode(sitemapItem.LastMod.Value.ToString()));
                    urlNode.AppendChild(lastModNode);

                    //sitemapItem.LastMod.Value.ToString();
                }


				
				 XmlNode changeFreqNode = doc.CreateElement("changefreq");
				 changeFreqNode.AppendChild(doc.CreateTextNode(sitemapItem.ChangeFreq));
				 urlNode.AppendChild(changeFreqNode);    

			     XmlNode priorityNode = doc.CreateElement("priority");
				 priorityNode.AppendChild(doc.CreateTextNode(sitemapItem.Priority.ToString()));
				 urlNode.AppendChild(priorityNode);

                 XmlNode contentNodeIdNode = doc.CreateElement("contentnodeid");
                 contentNodeIdNode.AppendChild(doc.CreateTextNode(sitemapItem.ContentnodeId.ToString()));
                 urlNode.AppendChild(contentNodeIdNode);

                 XmlNode productIdNode = doc.CreateElement("productid");
                 productIdNode.AppendChild(doc.CreateTextNode(sitemapItem.ProductId.ToString()));
                 urlNode.AppendChild(productIdNode);

				Console.WriteLine(sitemapItem.Url);

			}

		    try
		    {
                string dirr = ConfigurationManager.AppSettings["DestinationDirectorySitemap"];
                string name = ConfigurationManager.AppSettings["XmlFileNameSitemapExporter" + channelCommonName];
                string path = Path.Combine(dirr, name);
                //Console.WriteLine(path);
                doc.Save(path);
		    }
		    catch (Exception e)
		    {
		        
		        Console.WriteLine("in sitmap path "+ e);
		    }
            

		}
	}
}
