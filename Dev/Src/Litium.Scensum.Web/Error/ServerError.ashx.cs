﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Web.Customer;
using log4net;

namespace Litium.Scensum.Web.Error
{
	public class ServerError : ContentPageHandler
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		[SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
		protected override void ProcessRequest()
		{
			const string messageFormat = @"
Server error.
Url: {0}";

			Exception exception = Server.GetLastError();
			_log.Error(string.Format(CultureInfo.InvariantCulture, messageFormat, Request.RawUrl), exception);

			if (ErrorSetting.Instance.Send500StatusCode)
			{
				Response.StatusCode = 500;
				Response.StatusDescription = "Internal Server Error";
				Response.TrySkipIisCustomErrors = true;
			}

			try
			{
				base.ProcessRequest();
			}
			catch (Exception ex)
			{
				var httpException = ex as HttpException;
				if (httpException == null || httpException.GetHttpCode() != 404)
				{
					_log.Error("Error when trying to render error content page", ex);
				}

				Response.Clear();
				Response.StatusCode = 500;
				Response.StatusDescription = "Internal Server Error";
				Response.Write(RenderHardCodedErrorPage());
				Response.End();
			}
		}

		protected override void AppendMetaTags(Collection<MetaTag> metaTags)
		{
			base.AppendMetaTags(metaTags);

			metaTags.Add(new MetaTag("robots", "noindex, follow"));
		}

		private static string RenderHardCodedErrorPage()
		{
			var html = new StringBuilder();
			html.AppendLine("<html>");
			html.AppendLine("<head>");
			html.AppendLine("<title>Technical error</title>");
			html.AppendLine("<meta name=\"robots\" content=\"noindex, follow\">");
			html.AppendLine("</head>");
			html.AppendLine("<body>");
			html.AppendLine("<h1>Technical error</h1>");
			html.AppendLine("An error has occurred.");
			html.AppendLine("</body>");
			html.AppendLine("</html>");

			return html.ToString();
		}

		protected override IContentPageFull GetContentPageCore()
		{
			if (ContentNodeTreeItem == null) return null;

			var contentPageService = IoC.Resolve<IContentPageService>();
            return contentPageService.GetFullById(UserContext.Current, ContentNodeTreeItem.Id);
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
            return IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, "ServerError");
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetSignInUrl());
		}

		protected override void AccessDeniedToSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetMyPagesUrl());
		}

		protected override Core.Web.Master CreateMaster()
		{
			return new Master();
		}
	}
}