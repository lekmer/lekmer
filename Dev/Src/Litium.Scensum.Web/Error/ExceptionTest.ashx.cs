﻿using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core.Web;

namespace Litium.Scensum.Web.Error
{
	public class ExceptionTest : HandlerBase
	{
		[SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
		protected override void ProcessRequest()
		{
			throw new Exception("Exception test!");
		}
	}
}