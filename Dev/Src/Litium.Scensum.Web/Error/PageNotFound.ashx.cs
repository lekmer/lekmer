﻿using System.Globalization;
using System.Reflection;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Web.Customer;
using log4net;

namespace Litium.Scensum.Web.Error
{
	public class PageNotFound : ContentPageHandler
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected override void ProcessRequest()
		{
			const string messageFormat = @"
Page not found.
Url: {0}";
			_log.WarnFormat(CultureInfo.InvariantCulture, messageFormat, Request.RawUrl);

			if (ErrorSetting.Instance.Send404StatusCode)
			{
				Response.StatusCode = 404;
				Response.StatusDescription = "Not Found";
				Response.TrySkipIisCustomErrors = true;
			}

			base.ProcessRequest();
		}

		protected override void AppendMetaTags(System.Collections.ObjectModel.Collection<MetaTag> metaTags)
		{
			base.AppendMetaTags(metaTags);

			metaTags.Add(new MetaTag("robots", "noindex, follow"));
		}

		protected override IContentPageFull GetContentPageCore()
		{
			if (ContentNodeTreeItem == null) return null;

			var contentPageService = IoC.Resolve<IContentPageService>();
            return contentPageService.GetFullById(UserContext.Current, ContentNodeTreeItem.Id);
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
            return IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, "PageNotFound");
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetSignInUrl());
		}

		protected override void AccessDeniedToSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetMyPagesUrl());
		}

		protected override Core.Web.Master CreateMaster()
		{
			return new Master();
		}
	}
}