using System.Web.Routing;
using Litium.Scensum.Web.Avail;
using Litium.Scensum.Web.Error;
using Litium.Scensum.Web.Lekmer;
using Litium.Scensum.Web.Media;
using Litium.Scensum.Web.Order;
using Litium.Scensum.Web.Search;
using Litium.Scensum.Web.SiteStructure;

namespace Litium.Scensum.Web
{
	public static class RoutingRegistration
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.RouteExistingFiles = true;
			routes.Add(new RootRoute(@"", null));
			LekmerRoutingRegistration.RegisterRoutes(routes);
			MediaRoutingRegistration.RegisterRoutes(routes);
			//ProductRoutingRegistration.RegisterRoutes(routes);
			OrderRoutingRegistration.RegisterRoutes(routes);
			CustomerRoutingRegistration.RegisterRoutes(routes);
			SearchRoutingRegistration.RegisterRoutes(routes);
			SiteStructureRoutingRegistration.RegisterRoutes(routes);
			LekmerProductRoutingRegistration.RegisterRoutes(routes);
			SiteStructureRoutingRegistration.RegisterRedirectRoutes(routes);
			AvailRoutingRegistration.RegisterRoutes(routes);
            routes.Add(new Route("trace.axd", new StopRoutingHandler()));
            routes.Add(new Route(@"wishlist", new RouteHandler("~/Lekmer/WishList.ashx")));
			routes.Add(new Route("diagnostics/cachediagnostics.aspx", new StopRoutingHandler()));
			routes.Add(new Route("crossdomain.xml", new StopRoutingHandler()));
			LekmerOldUrlRoutingRegistration.RegisterRoutes(routes);
			ErrorRoutingRegistration.RegisterRoutes(routes);
		}
	}
}