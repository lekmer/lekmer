using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Customer
{
	public static class CustomerUrlHelper
	{
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate"),
		 SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
		public static string GetSignInUrl()
		{
			var contentNodeService = IoC.Resolve<IContentNodeService>();

            IContentNodeTreeItem signInTreeItem = contentNodeService.GetTreeItemByCommonName(UserContext.Current, "SignIn");

			if (signInTreeItem == null || string.IsNullOrEmpty(signInTreeItem.Url))
			{
				throw new ApplicationException("Url to 'sign in' could not be found.");
			}

			return signInTreeItem.Url;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate"),
		 SuppressMessage("Microsoft.Usage", "CA2201:DoNotRaiseReservedExceptionTypes")]
		public static string GetMyPagesUrl()
		{
			var contentNodeService = IoC.Resolve<IContentNodeService>();

            IContentNodeTreeItem myPagesTreeItem = contentNodeService.GetTreeItemByCommonName(UserContext.Current, "MyPages");

			if (myPagesTreeItem == null || string.IsNullOrEmpty(myPagesTreeItem.Url))
			{
				throw new ApplicationException("Url to 'my pages' could not be found.");
			}

			return myPagesTreeItem.Url;
		}
	}
}