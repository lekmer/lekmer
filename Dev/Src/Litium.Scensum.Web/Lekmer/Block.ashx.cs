using System.Collections.ObjectModel;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using System.Linq;

namespace Litium.Scensum.Web.Lekmer
{
	public class Block : ContentPageHandler
	{
		protected override IContentPageFull GetContentPageCore()
		{
			var contentPage = (IContentPage) Items["ContentPage"];

			var contentPageService = IoC.Resolve<IContentPageService>();
			IContentPageFull contentPageFull = contentPageService.GetFullById(UserContext.Current, contentPage.Id);

			return contentPageFull;
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
			return (IContentNodeTreeItem) Items["ContentNodeTreeItem"];
		}

		protected override Core.Web.Master CreateMaster()
		{
			return new EmptyMaster();
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			RenderPageNotFound();
		}

		protected override void AccessDeniedToSignedIn()
		{
			RenderPageNotFound();
		}

		protected override void ProcessRequest()
		{
			if (ContentPage == null || !ContentPage.ShowPage)
			{
				RenderPageNotFound();
				return;
			}

			bool isSignedIn = IoC.Resolve<ICustomerSession>().IsSignedIn;
			var access = (AccessInfo) ContentPage.AccessId;

			if (!isSignedIn && access == AccessInfo.SignedIn)
			{
				AccessDeniedToNotSignedIn();
				return;
			}

			if (isSignedIn && access == AccessInfo.NotSignedIn)
			{
				AccessDeniedToSignedIn();
				return;
			}

			var blockId = Items["BlockId"] as int?;
			if (!blockId.HasValue)
			{
				RenderPageNotFound();
				return;
			}

			var areaBlockPair = GetAreaBlockPair(blockId.Value);
			if (!areaBlockPair.HasValue)
			{
				RenderPageNotFound();
				return;
			}

			// Use a master to get variables such as [Channel.MediaUrl].
			var master = new EmptyMaster();

			master.DynamicProperties["ActiveContentNodeId"] = ActiveContentNodeId;
			AppendBreadcrumbItems(master.BreadcrumbItems);
			AppendMetaTags(master.Page.MetaTags);
			master.Page.Title = PageTitle();
			master.Page.Content = RenderBlock(areaBlockPair.Value.ContentArea, areaBlockPair.Value.Block).Body;

			Response.Write(master.Render());
		}

		private BlockContent RenderBlock(IContentArea contentArea, IBlock block)
		{
			var blockControl = IoC.Resolve<IBlockControl>(block.BlockType.CommonName);
			blockControl.ContentPage = ContentPage;
			blockControl.ContentNodeTreeItem = ContentNodeTreeItem;
			blockControl.ContentArea = contentArea;
			blockControl.ContentPageHandler = this;
			blockControl.BlockId = block.Id;

			HttpContext context = HttpContext.Current;
			if (!context.Trace.IsEnabled)
			{
				return blockControl.Render();
			}

			string category = "Block " + block.BlockType.CommonName + " Id=" + block.Id;
			context.Trace.Write(category, "Begin Render");

			BlockContent content = blockControl.Render();

			context.Trace.Write(category, "End Render");

			return content;
		}

		private AreaBlockPair? GetAreaBlockPair(int blockId)
		{
			var blocks = new Collection<IBlock>();

			foreach (IContentAreaFull area in ContentPage.ContentAreas)
			{
				blocks.AddRange(area.Blocks);
			}

			IContentPageFull masterContentPage = null;
			if (ContentPage.MasterPageId.HasValue)
			{
				masterContentPage = IoC.Resolve<IContentPageService>().GetFullById(UserContext.Current, ContentPage.MasterPageId.Value);
				foreach (IContentAreaFull area in masterContentPage.ContentAreas)
				{
					blocks.AddRange(area.Blocks);
				}
			}

			foreach (IBlock block in blocks)
			{
				if (block.Id == blockId)
				{
					var contentArea = ContentPage.ContentAreas.SingleOrDefault(a => a.Id == block.ContentAreaId);
					if (contentArea == default(IContentAreaFull) && masterContentPage != null)
					{
						contentArea = masterContentPage.ContentAreas.SingleOrDefault(a => a.Id == block.ContentAreaId);
					}
					return new AreaBlockPair(contentArea, block);
				}
			}

			return null;
		}
	}
}