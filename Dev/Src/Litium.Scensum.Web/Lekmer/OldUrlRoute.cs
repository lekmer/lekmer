using System;
using System.Web;
using System.Web.Routing;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Lekmer.OldUrl;

namespace Litium.Scensum.Web.Lekmer
{
	public class OldUrlRoute : Route
	{
		public OldUrlRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			if (data == null) return null;

			string relativeUrl = RelativeUrl(httpContext.Request);

			string[] url = relativeUrl.Split('?');
			string queryStr = "";
			string baseUrl = "";
			if (url.Length > 0)
			{

				baseUrl = url[0];

				if (baseUrl.EndsWith("/", StringComparison.OrdinalIgnoreCase))
				{
					baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
				}


				if (url.Length > 1)
				{
					queryStr = "?" + url[1];
				}
			}

			//relativeUrl = relativeUrl.Split('?');

			string redirectUrl = OldUrlService.GetRedirectUrl(baseUrl);

			if (!redirectUrl.IsNullOrEmpty())
			{

				httpContext.Response.PermanentRedirect(redirectUrl + queryStr);
			}

			return null;
		}

		public static string RelativeUrl(HttpRequestBase request)
		{
			if (request == null) throw new ArgumentNullException("request");

			// Get full url.
			string url = request.ServerVariables["HTTP_URL"];

			// Take url and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath;
			string relativeUrl = url.Substring(appPath.Length);
			if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
			{
				relativeUrl = "/" + relativeUrl;
			}

			// Return relative url.
			return relativeUrl;
		}
	}
}