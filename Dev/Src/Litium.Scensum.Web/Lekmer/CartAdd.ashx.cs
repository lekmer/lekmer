using System;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Scensum.Web.Lekmer
{
	public class CartAdd : Order.CartAdd
	{
		private readonly ICartService _cartService = IoC.Resolve<ICartService>();

		protected override void ProcessRequest()
		{
			ICartFull cart = GetCart();
			int? productId = GetProductId();

			if (productId.HasValue)
			{
				AddItemToCart(productId.Value, cart, GetQuantity());

				Session["CartItemAdded"] = productId.Value;
			}

            if (AjaxCartHelper.IsXmlHttpRequest(Request))
            {
                Response.Write(AjaxCartHelper.RenderCart());
            }
            else
            {
                Response.Redirect(GetRedirectUrl());
            }
		}

		protected override void AddItemToCart(int productId, ICartFull cart, int quantity)
		{
			if (cart == null) throw new ArgumentNullException("cart");

			var product = GetProduct(productId);
			if (product != null)
			{
				((ILekmerCartFull)cart).ChangeItemQuantity(product, quantity, GetSizeId(), GetSizeErpId());
				cart.Id = _cartService.Save(UserContext.Current, cart);
				RefreshCart();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected int? GetSizeId()
		{
			int sizeId;
			if (!int.TryParse(Request.QueryString["sizeid"], out sizeId))
			{
				return null;
			}
			return sizeId;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected string GetSizeErpId()
		{
			return Request.QueryString["sizeerpid"] ?? string.Empty;
		}
	}
}