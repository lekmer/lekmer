﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Web.Lekmer
{
    public class WishList : HandlerBase
    {
        protected override void ProcessRequest()
        {
            int? productId = GetProductId();
            if (productId.HasValue)
            {
                int? sizeId = GetSizeId();
                Guid wishListKey = WishListCookie.GetWishListKey();

                if (wishListKey==Guid.Empty)
                    wishListKey = new Guid(WishListCookie.CreateWithList());

                var wishListService = IoC.Resolve<IWishListService>();
                IWishList wishList = wishListService.GetByKey(wishListKey);
                if (wishList == null)
                {
                    wishList = IoC.Resolve<IWishListService>().Create();
                    wishList.WishListKey = wishListKey;
                }
                string productKey = productId.ToString();
                if (sizeId.HasValue)
                    productKey += "|" + sizeId;

                if (!wishList.ProductWishList.Split(',').Contains(productKey))
                {
                    if (wishList.ProductWishList.Length > 0)
                    {
                        wishList.ProductWishList += "," + productKey;
                    }
                    else
                    {
                        wishList.ProductWishList = productKey;
                    }
                    wishListService.Save(wishList);
                }

            }
            string url = Request.QueryString["referrer"];
            if (url.IsNullOrTrimmedEmpty())
            {
                url = "~/";
            }

            Response.PermanentRedirect(url);
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected int? GetProductId()
        {
            int productId;
            if (!int.TryParse(Request.QueryString["id"], out productId))
            {
                return null;
            }


            return productId;
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected int? GetSizeId()
        {
            int sizeId;
            if (!int.TryParse(Request.QueryString["sizeid"], out sizeId))
            {
                return null;
            }
            return sizeId;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected string GetSizeErpId()
        {
            return Request.QueryString["sizeerpid"] ?? string.Empty;
        }
    }
}