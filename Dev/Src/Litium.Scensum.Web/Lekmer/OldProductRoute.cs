using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.Web.Lekmer
{
	public class OldProductRoute : Route
	{
		private static readonly Regex _oldUrlRegex =
			new Regex(@"^\/leksaker\/([^\/]+\/[^\/]+)$",
			          RegexOptions.IgnoreCase | RegexOptions.Compiled);

		public OldProductRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			if (data == null) return null;

			string relativeUrl = RelativeUrl(httpContext.Request);

			Match oldUrlMatch = _oldUrlRegex.Match(relativeUrl);
			if (!oldUrlMatch.Success) return null;

			string oldUrl = oldUrlMatch.Groups[1].Value;

			var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
			int? productId = productService.GetIdByOldUrl(oldUrl);

			if (!productId.HasValue)
			{
				return null;
			}

			httpContext.Items["ProductId"] = productId.Value;

			return data;
		}

		public static string RelativeUrl(HttpRequestBase request)
		{
			if (request == null) throw new ArgumentNullException("request");

			// Get full url.
			string url = request.ServerVariables["HTTP_URL"];

			// Take url and remove application path.
			string appPath = HttpRuntime.AppDomainAppVirtualPath;
			string relativeUrl = url.Substring(appPath.Length);
			if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
			{
				relativeUrl = "/" + relativeUrl;
			}

			// Return relative url.
			return relativeUrl;
		}
	}
}