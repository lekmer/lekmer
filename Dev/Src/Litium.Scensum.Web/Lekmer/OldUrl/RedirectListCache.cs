using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Scensum.Web.Lekmer.OldUrl
{
	public sealed class RedirectListCache : ScensumCacheBase<RedirectListKey, IEnumerable<Redirect>>
	{
		private static readonly RedirectListCache _instance = new RedirectListCache();

		private RedirectListCache()
		{
		}

		public static RedirectListCache Instance
		{
			get { return _instance; }
		}

		public override string ManagerName
		{
			get { return "RedirectListCache"; }
		}
	}

	public class RedirectListKey : ICacheKey
	{
		public string Key
		{
			get { return "redirect"; }
		}
	}
}