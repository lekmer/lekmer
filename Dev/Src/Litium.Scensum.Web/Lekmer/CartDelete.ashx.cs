using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Scensum.Web.Lekmer
{
	public class CartDelete : Order.CartDelete
	{
		protected override void ProcessRequest()
		{
			int? productId = GetProductId();
			decimal? campaignInfoPrice = GetProductCampaignInfoPrice();
			if (productId.HasValue && campaignInfoPrice.HasValue)
			{
				ICartFull cart = IoC.Resolve<ICartSession>().Cart;
				if (cart != null)
				{
					DeleteItemFromCart(cart, productId.Value, campaignInfoPrice.Value, GetQuantity());
				}
			}

			if (AjaxCartHelper.IsXmlHttpRequest(Request))
			{
				Response.Write(AjaxCartHelper.RenderCart());
			}
			else
			{
				Response.Redirect(GetRedirectUrl());
			}
		}
		protected override int GetQuantity()
		{
			int quantity;
			if (!int.TryParse(Request.QueryString["quantity"], out quantity))
			{
				quantity = 1;
			}
			return quantity;
		}
	}
}