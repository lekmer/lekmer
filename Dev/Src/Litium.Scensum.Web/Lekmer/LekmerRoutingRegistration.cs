using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Lekmer
{
	public static class LekmerRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");
			routes.Add(new BlockRoute(@"{*url}", new RouteHandler("~/Lekmer/Block.ashx")));
			routes.Add(new Route(@"search/incremental", new RouteHandler("~/Lekmer/IncrementalSearch.ashx")));
			routes.Add(new OldProductRoute(@"leksaker/{category}/{title}", new RouteHandler("~/Lekmer/OldProductRedirect.ashx")));
			routes.Add(new Route(@"cart/add", new RouteHandler("~/Lekmer/CartAdd.ashx")));
			routes.Add(new Route(@"cart/delete", new RouteHandler("~/Lekmer/CartDelete.ashx")));
			routes.Add(new Route(@"affiliate/tradedoubler", new RouteHandler("~/Lekmer/TradeDoubler.ashx")));
			routes.Add(new Route(@"robots.txt", new StopRoutingHandler()));
			routes.Add(new Route(@"Lekmer/ProductCacheCleaner.asmx", new StopRoutingHandler()));
			routes.Add(new Route(@"Lekmer/ProductCacheCleaner.asmx/{*command}", new StopRoutingHandler()));

			routes.Add(new OldBrandRoute(@"varumarke/{brand}", new RouteHandler("~/Lekmer/OldBrandRedirect.ashx")));
		}
	}
}