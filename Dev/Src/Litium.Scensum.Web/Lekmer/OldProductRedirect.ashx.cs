﻿using System.Web;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.Web.Lekmer
{
	public class OldProductRedirect : HandlerBase
	{
		protected override void ProcessRequest()
		{
			var productId = Items["ProductId"] as int?;

			if (!productId.HasValue)
			{
				throw new HttpException(404, "Not found");
			}

			var productService = IoC.Resolve<IProductService>();
			IProduct product = productService.GetById(UserContext.Current, productId.Value);

			if (product == null)
			{
				throw new HttpException(404, "Not found");
			}

			var lekmerProduct = (ILekmerProduct) product;
			Response.PermanentRedirect("~/" + lekmerProduct.LekmerUrl);
		}
	}
}