using System;
using System.Web;

namespace Litium.Scensum.Web.Lekmer
{
	public static class AjaxCartHelper
	{
		public static bool IsXmlHttpRequest(HttpRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			string header = request.Headers["X-Requested-With"];
			if (header == null) return false;

			return header.Equals("XMLHttpRequest", StringComparison.OrdinalIgnoreCase);
		}

		public static string RenderCart()
		{
			// Use a master to get variables such as [Channel.MediaUrl].
			var master = new EmptyMaster();
			master.Page.Content = "[Component(\"Cart\")]";
			return master.Render();
		}
	}
}