﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Framework.Search;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch;
using Channel=Litium.Scensum.Core.Web.Channel;
using Encoder=Litium.Scensum.Foundation.Utilities.Encoder;
using UserContext=Litium.Scensum.Core.Web.UserContext;

namespace Litium.Scensum.Web.Lekmer
{
	public class IncrementalSearch : HandlerBase
	{
		private const string _imageSizeCommonName = "minithumbnail";
		private const int _defaultNumberOfItemsToShow = 10;
		private const int _maximumNumberOfItemsToShow = 100;

		protected override void ProcessRequest()
		{
			int showItems = Math.Min(
				Request.QueryString.GetInt32OrNull("show-items") ?? _defaultNumberOfItemsToShow, _maximumNumberOfItemsToShow);

			string query = Request.QueryString["q"];

			ProductCollection products = SearchProducts(query, showItems);

			Response.ContentType = "application/json";
			Response.Write(BuildJsonResponse(products));
		}

		private static string BuildJsonResponse(ProductCollection products)
		{
			if (products == null) throw new ArgumentNullException("products");

			var formatter = IoC.Resolve<IFormatter>();

			const string jsonProductFormat = "name: '{0}', price: '{1}', image: '{2}', productUrl: '{3}', campaignPrice: '{4}'";

			var json = new StringBuilder();
			json.Append("{");
			json.Append("productResults:[");
			foreach (IProduct product in products)
			{
				var lekmerProduct = (ILekmerProduct) product;

				string price = formatter.FormatPrice(Channel.Current, product.Price.PriceIncludingVat);
				
				string campaignPrice = price;
				if (product.CampaignInfo != null)
				{
					campaignPrice = formatter.FormatPrice(Channel.Current, product.CampaignInfo.Price.IncludingVat);
				}

				string imageUrl = GetImageUrl(product) ?? string.Empty;
				string productUrl = UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl);

				json.Append(
					"{" +
					string.Format(
						CultureInfo.CurrentCulture, jsonProductFormat,
						Encoder.EncodeJavaScript(product.DisplayTitle),
						Encoder.EncodeJavaScript(price),
						Encoder.EncodeJavaScript(imageUrl),
						Encoder.EncodeJavaScript(productUrl),
						Encoder.EncodeJavaScript(campaignPrice))
					+ "},");
			}

			if (products.TotalCount > 0)
			{
				json.Remove(json.Length - 1, 1);
			}
			json.Append("]");
			json.AppendFormat(",totalCount: {0}", products.TotalCount);
			json.Append("}");

			return json.ToString();
		}

		private static string GetImageUrl(IProduct product)
		{
			if (product == null) throw new ArgumentNullException("product");

			string imageUrl = null;
			if (product.Image != null)
			{
				imageUrl = string.Format(
					CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
					WebSetting.Instance.MediaUrl, product.Image.Id, _imageSizeCommonName, UrlCleaner.CleanUp(product.DisplayTitle),
					product.Image.FormatExtension);

				imageUrl = UrlHelper.ResolveUrl(imageUrl);
			}
			return imageUrl;
		}

		private static ProductCollection SearchProducts(string searchQuery, int showItems)
		{
			var productSearchService = (LekmerProductSearchService)IoC.Resolve<IProductSearchService>();
			ProductCollection productCollection = productSearchService.Search(UserContext.Current, searchQuery, 1, showItems);
			return productCollection;
		}
	}
}