using System.Linq;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;
using Channel = Litium.Scensum.Core.Web.Channel;

namespace Litium.Scensum.Web.Order
{
	public class LekmerOrderFullEntityMapper : OrderFullEntityMapper
	{
		public LekmerOrderFullEntityMapper(IFormatter formatter, ICountryService countryService) : base(formatter, countryService)
		{
		}

		public override void AddEntityVariables(Fragment fragment, IOrderFull item)
		{
			base.AddEntityVariables(fragment, item);

			var lekmerOrder = (ILekmerOrderFull)item;
			var formatter = IoC.Resolve<IFormatter>();
			fragment.AddVariable("Order.FreightVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetActualFreightCostVat()));
			fragment.AddVariable("Order.PaymentCost", formatter.FormatPrice(Channel.Current, lekmerOrder.PaymentCost));
			fragment.AddVariable("Order.PaymentVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetPaymentCostVat()));
			fragment.AddVariable("Order.TotalCostIncludingVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetActualPriceTotal().IncludingVat));
			fragment.AddVariable("Order.TotalCostExcludingVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetActualPriceTotal().ExcludingVat));
			fragment.AddVariable("Order.TotalVat",  formatter.FormatPrice(Channel.Current, lekmerOrder.GetActualVatTotal()));
			fragment.AddVariable("Order.TotalQuantity", formatter.FormatNumber(Channel.Current, lekmerOrder.GetOrderItems().Sum(i => i.Quantity)));
			fragment.AddVariable("Order.Status", AliasHelper.GetAliasValue("Order.Status." + lekmerOrder.OrderStatus.CommonName), VariableEncoding.None);
		}
	}
}