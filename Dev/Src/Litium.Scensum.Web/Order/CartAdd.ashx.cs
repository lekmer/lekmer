using Litium.Lekmer.Order.Web.Cart;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Web.Cart;
using Litium.Scensum.SiteStructure;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Scensum.Web.Order
{
	public class CartAdd : CartAddHandler
    {
        private int _productId;
        protected override int? GetProductId()
        {
            if (!int.TryParse(Request.QueryString["id"], out _productId))
            {
                return null;
            }
            ((LekmerCartSession)IoC.Resolve<ICartSession>()).AvailProductId = _productId;
            string trackingCode = Request.QueryString["trackingcode"];
            if (!string.IsNullOrEmpty(trackingCode))
            {
                ((LekmerCartSession)IoC.Resolve<ICartSession>()).TrackingCode = trackingCode;
            }

            return _productId;
        }

		protected override int GetQuantity()
		{
			int quantity;
			if (!int.TryParse(Request.QueryString["quantity"], out quantity))
			{
				quantity = 1;
			}
			return quantity;
		}

		protected override string GetRedirectUrl()
		{
            string addedtobasket = Request.QueryString["addedtobasket"];
            if (!string.IsNullOrEmpty(addedtobasket))
            {
                string contentNodeId = Request.QueryString["nodeId"];

                var node = IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, addedtobasket);
                if (node == null)
                {
                    return "~/";
                }
                return UrlHelper.ResolveUrlHttp(node.Url + "?id=" + _productId + "&nodeId=" + contentNodeId);
            }
                       
            string referrer = Request.QueryString["referrer"];

            if (string.IsNullOrEmpty(referrer)) return "~/";
            return "~" + referrer;
		}
	}
}