using Litium.Scensum.Order.Web.Cart;

namespace Litium.Scensum.Web.Order
{
	public class CartDelete : CartDeleteHandler
	{
		protected override int? GetProductId()
		{
			int productId;
			if (!int.TryParse(Request.QueryString["id"], out productId))
			{
				return null;
			}
			return productId;
		}

		protected override int GetQuantity()
		{
			int quantity;
			if (!int.TryParse(Request.QueryString["quantity"], out quantity))
			{
				quantity = 1;
			}
			return quantity;
		}

		protected override decimal? GetProductCampaignInfoPrice()
		{
			decimal campaignInfoPrice;
			if (!decimal.TryParse(Request.QueryString["price"], out campaignInfoPrice))
			{
				return null;
			}
			return campaignInfoPrice;
		}

		protected override string GetRedirectUrl()
		{
			string referrer = Request.QueryString["referrer"];

			if (string.IsNullOrEmpty(referrer)) return "~/";
			return "~" + referrer;
		}
	}
}