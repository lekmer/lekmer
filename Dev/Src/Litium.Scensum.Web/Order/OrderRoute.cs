using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;

namespace Litium.Scensum.Web.Order
{
	public class OrderRoute : Route
	{
		public OrderRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public OrderRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
			: base(url, defaults, routeHandler)
		{
		}

		public OrderRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
		                  IRouteHandler routeHandler)
			: base(url, defaults, constraints, routeHandler)
		{
		}

		public OrderRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
		                  RouteValueDictionary dataTokens, IRouteHandler routeHandler)
			: base(url, defaults, constraints, dataTokens, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			if (data == null) return null;

			object pathValue = data.Values["path"];
			if (pathValue == null) return null;
			string urlPath = pathValue.ToString();

			Match match = Regex.Match(urlPath, @"^(.+)\/(\d+)$");
			if (!match.Success) return null;

			string pagePath = match.Groups[1].Value;
			int orderId = int.Parse(match.Groups[2].Value, CultureInfo.InvariantCulture);

			if (!IsCorrectPagePath(pagePath)) return null;

			var orderService = IoC.Resolve<IOrderService>();
            IOrderFull order = orderService.GetFullById(UserContext.Current, orderId);
			if (order == null) return null;

			var customerSession = IoC.Resolve<ICustomerSession>();
			if (customerSession.IsSignedIn && order.CustomerId != customerSession.SignedInCustomer.Id)
			{
				return null;
			}

			httpContext.Items["Order"] = order;
			
			return data;
		}

		private static bool IsCorrectPagePath(string pagePath)
		{
			string correctBasePath = OrderUrlHelper.BasePath;
			return
				!string.IsNullOrEmpty(correctBasePath)
				&& correctBasePath.Equals(pagePath + "/", StringComparison.OrdinalIgnoreCase);
		}
	}
}