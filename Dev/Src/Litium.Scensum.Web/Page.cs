﻿using System.Globalization;
using Litium.Lekmer.Product.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web
{
    public class Page : Core.Web.Page
    {
        public virtual int? ActiveContentNodeId { get; set; }
        protected override void AddStateVariables(Fragment fragment)
        {
            base.AddStateVariables(fragment);
            fragment.AddVariable("ActiveContentNodeId", ActiveContentNodeId.HasValue ? ActiveContentNodeId.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
        }
    }
}
