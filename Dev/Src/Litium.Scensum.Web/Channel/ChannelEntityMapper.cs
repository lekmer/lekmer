using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web
{
	public class ChannelEntityMapper : Core.Web.ChannelEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IChannel item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Channel.MediaUrl", UrlHelper.ResolveUrl("~/media"), VariableEncoding.None);
		}
	}
}