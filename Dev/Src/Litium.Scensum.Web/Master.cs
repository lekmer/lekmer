using System;
using Litium.Scensum.Template.Engine;
using Litium.Scensum.Web.Customer;
using Litium.Scensum.Web.SiteStructure;

namespace Litium.Scensum.Web
{
    internal class Master : Core.Web.Master
    {
        protected override void AddVariablesPostInsertComponent(Fragment fragmentContent)
        {
            if (fragmentContent == null) throw new ArgumentNullException("fragmentContent");
            ((Page)Page).ActiveContentNodeId = DynamicProperties["ActiveContentNodeId"] as int?;
            base.AddVariablesPostInsertComponent(fragmentContent);

            CustomerMasterHelper.AddCustomerUserVariables(fragmentContent);
            SiteStructureMasterHelper.AddSiteStructureVariables(fragmentContent);
        }
        protected override Core.Web.Page CreatePageInstance()
        {
            return new Page();
        }
    }
}