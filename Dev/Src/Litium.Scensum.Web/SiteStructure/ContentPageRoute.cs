using System;
using System.Web;
using System.Web.Routing;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.SiteStructure
{
	public class ContentPageRoute : Route
	{
		public ContentPageRoute(string url, IRouteHandler routeHandler) : base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			string url = (data.Values["url"] ?? "").ToString();

			var contentNodeService = IoC.Resolve<IContentNodeService>();
            IContentNodeTree contentNodeTree = contentNodeService.GetAllAsTree(UserContext.Current);

			IContentNodeTreeItem item;
			if (string.IsNullOrEmpty(url))
			{
				var siteStructureRegistryService = IoC.Resolve<ISiteStructureRegistryService>();
				ISiteStructureRegistry siteStructureRegistry =
                    siteStructureRegistryService.GetByChannel(UserContext.Current);
				if (siteStructureRegistry == null || !siteStructureRegistry.StartContentNodeId.HasValue) return null;

				item = contentNodeTree.FindItemById(siteStructureRegistry.StartContentNodeId.Value);
			}
			else
			{
				item = contentNodeTree.FindItemByUrl(url);
			}

			if (item == null) return null;

			var contentPage = item.ContentNode as IContentPage;
			if (contentPage == null) return null;

			httpContext.Items["ContentPage"] = contentPage;
			httpContext.Items["ContentNodeTreeItem"] = item;

			return data;
		}
	}
}