﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Avail
{
    public class AvailHandler : HandlerBase
    {
        private Template.Engine.Template _template;
        private Collection<IProduct> _products = new Collection<IProduct>();

        protected override void ProcessRequest()
        {
            var query = Request.QueryString["ids"];
            var templateId = Request.QueryString.GetInt32("templateid");
            if (string.IsNullOrEmpty(query)) return;
            var trackingCode = Request.QueryString["trackingcode"];

            var productService = IoC.Resolve<IProductService>();

            string[] productIdsString = query.Split(',');

            List<int> productIdsInt = new List<int>();

            for (int i = 0; i < productIdsString.Length; i++)
            {
                int productrId;
                if (int.TryParse(productIdsString[i], out productrId))
                {
                    productIdsInt.Add(productrId);
                }
            }
            var productIds = new ProductIdCollection(productIdsInt);
            var products = productService.PopulateProducts(UserContext.Current, productIds);
            _products = new Collection<IProduct>(products.Where(product => product.NumberInStock > 0).ToList());

            _template = GetTemplate(templateId);

            Response.Write(RenderProductList(trackingCode));
        }

        private string RenderProductList(string trackingCode)
        {
            if (_products.Count == 0) return null;

            Fragment fragmentProductList = _template.GetFragment("ProductList");
            fragmentProductList.AddVariable("Iterate:ProductRow", RenderProducts(trackingCode), VariableEncoding.None);
            return fragmentProductList.Render();
        }

        private string RenderProducts(string trackingCode)
        {
            var itemBuilder = new StringBuilder();
            foreach (var product in _products)
            {
                itemBuilder.Append(RenderProductRow(product, trackingCode));
            }
            return itemBuilder.ToString();
        }

        private string RenderProductRow(IProduct product, string trackingCode)
        {
            Fragment fragmentProductRow = _template.GetFragment("ProductRow");
            fragmentProductRow.AddVariable("Iterate:Product", RenderProduct(product, trackingCode), VariableEncoding.None);
            return fragmentProductRow.Render();
        }

        private string RenderProduct(IProduct product, string trackingCode)
        {
            Fragment fragmentProduct = _template.GetFragment("Product");
            fragmentProduct.AddEntity(product);
            fragmentProduct.AddVariable("TrackingCode", trackingCode);
            fragmentProduct.AddCondition("Product.IsRecommended", !string.IsNullOrEmpty(trackingCode));
            fragmentProduct.AddCondition("WasSearched", WasSearched);
            fragmentProduct.AddEntity(Channel.Current);
            string relativeUrl = RelativeUrl(Request.UrlReferrer);
            fragmentProduct.AddVariable("Page.CurrentRelativeUrl", relativeUrl, VariableEncoding.None);
            fragmentProduct.AddVariable("Page.CurrentRelativeUrl.UrlEncode", relativeUrl, VariableEncoding.UrlEncode);
            fragmentProduct.AddCondition("Page.HttpsEnabled", Request.IsSecureConnection);

            return fragmentProduct.Render();
        }

        private static Template.Engine.Template GetTemplate(int templateId)
        {
            return IoC.Resolve<ITemplateFactory>().Create(templateId);
        }

        private static string RelativeUrl(Uri uri)
        {
            if (uri == null)
                return string.Empty;

            // Get full url.
            string url = uri.AbsolutePath;

            // Take url and remove application path.
            string appPath = HttpRuntime.AppDomainAppVirtualPath;
            string relativeUrl = url.Substring(appPath.Length);
            if (relativeUrl.Length == 0 || relativeUrl.Substring(0, 1) != "/")
            {
                relativeUrl = "/" + relativeUrl;
            }

            // Return relative url.
            return relativeUrl;
        }
        private bool WasSearched
        {
            get
            {
                return Request.UrlReferrer == null ? false : Request.UrlReferrer.Query.Contains("q");
            }
        }
    }
}

