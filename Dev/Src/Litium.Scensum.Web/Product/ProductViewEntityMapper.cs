using System;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;

namespace Litium.Scensum.Web.Product
{
	public class ProductViewEntityMapper : Scensum.Product.Web.ProductViewEntityMapper
	{
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IContentNodeService _contentNodeService;
		private readonly ICategoryService _categoryService;
		private readonly IPriceIntervalService _priceIntervalService;

		public ProductViewEntityMapper(IAgeIntervalService ageIntervalService, IContentNodeService contentNodeService,
									   ICategoryService categoryService, IPriceIntervalService priceIntervalService)
		{
			_ageIntervalService = ageIntervalService;
			_contentNodeService = contentNodeService;
			_categoryService = categoryService;
			_priceIntervalService = priceIntervalService;
		}

		public override void AddEntityVariables(Fragment fragment, IProductView item)
		{
			var lekmerProduct = (ILekmerProductView)item;
			fragment.AddVariable("Product.Url", UrlHelper.ResolveUrlHttp(lekmerProduct.LekmerUrl));
            fragment.AddVariable("Product.FullUrl", UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl));

			base.AddEntityVariables(fragment, item);

			ProductHelper.AddImageVariables(fragment, item.Image, item.DisplayTitle);

			IBrand brand = lekmerProduct.Brand;
			fragment.AddCondition("Product.HasBrand", brand != null);

			if (brand != null)
			{
				fragment.AddEntity(brand);
			}
            if (IsPriceAffectedByCampaign(item))
            {
                fragment.AddVariable("Product.DiscountInPercent", CalculateDiscountpercent(lekmerProduct.Price.PriceIncludingVat, lekmerProduct.CampaignInfo.Price.IncludingVat));
            }
			fragment.AddCondition("Product.IsInStock", lekmerProduct.NumberInStock > 0);
			fragment.AddCondition("Product.IsBuyable", lekmerProduct.IsBuyable);
			fragment.AddCondition("Product.IsBookable", lekmerProduct.IsBookable);
			fragment.AddCondition("Product.IsMonitorable", lekmerProduct.IsMonitorable);
			fragment.AddCondition("Product.IsNew", lekmerProduct.IsNewProduct);
			fragment.AddCondition("Product.HasMeasurement", !string.IsNullOrEmpty(lekmerProduct.Measurement));
			fragment.AddCondition("Product.HasBatteries", lekmerProduct.BatteryType != null);
			fragment.AddCondition("Product.HasBatteriesIncluded", lekmerProduct.IsBatteryIncluded);
			fragment.AddCondition("Product.HasStockDate", lekmerProduct.ExpectedBackInStock.HasValue);
			fragment.AddVariable("Product.BatteryType",
								 lekmerProduct.BatteryType != null ? lekmerProduct.BatteryType.Title : string.Empty);
			fragment.AddVariable("Product.Measurement", lekmerProduct.Measurement);
			fragment.AddVariable("Product.ExpectedBackInStock", lekmerProduct.ExpectedBackInStock.HasValue
																	? lekmerProduct.ExpectedBackInStock.Value.ToString(
																		CultureInfo.InvariantCulture)
																	: string.Empty);
			fragment.AddVariable("Product.AgeFromMonth", lekmerProduct.AgeFromMonth.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Product.AgeToMonth", lekmerProduct.AgeToMonth.ToString(CultureInfo.InvariantCulture));

			var ageFromYear = (int)Math.Floor((decimal)lekmerProduct.AgeFromMonth / 12);
			var ageToYear = (int)Math.Ceiling((decimal)lekmerProduct.AgeToMonth / 12);
			fragment.AddVariable("Product.AgeFromYear", ageFromYear.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Product.AgeToYear", ageToYear.ToString(CultureInfo.InvariantCulture));

			fragment.AddCondition("Product.AgeFromIsYear", lekmerProduct.AgeFromMonth >= 12);
			fragment.AddCondition("Product.AgeToIsYear", lekmerProduct.AgeToMonth > 12);

			fragment.AddCondition("Product.HasSizeDeviation", lekmerProduct.SizeDeviation != null);
			fragment.AddVariable(
				"Product.SizeDeviation",
				lekmerProduct.SizeDeviation != null
					? AliasHelper.GetAliasValue("Product.ProductDetail.SizeDeviation." + lekmerProduct.SizeDeviation.CommonName)
					: string.Empty,
				VariableEncoding.None);

			fragment.AddCondition("Product.HasSizes", lekmerProduct.HasSizes);
			fragment.AddVariable("Product.LekmerErpId", lekmerProduct.LekmerErpId);

			string categoryTitle;
			string categoryUrl;
			GetParentCategory(lekmerProduct, out categoryTitle, out categoryUrl);
			IPriceInterval priceInterval;
			string priceIntervalUrl;
			GetPriceInterval(lekmerProduct, out priceInterval, out priceIntervalUrl);
			string ageIntervalUrl = GetAgeIntervalUrl(lekmerProduct);

			fragment.AddCondition("Product.Category.HasUrl", categoryUrl != null);
			fragment.AddVariable("Product.Category.Url", categoryUrl);
			fragment.AddVariable("Product.Category.Title", categoryTitle);

			fragment.AddCondition("Product.HasPriceInterval", priceInterval != null);
			fragment.AddVariable("Product.PriceInterval.Title", priceInterval != null ? priceInterval.Title : null);
			fragment.AddCondition("Product.PriceInterval.HasUrl", priceIntervalUrl != null);
			fragment.AddVariable("Product.PriceInterval.Url", priceIntervalUrl);

			fragment.AddCondition("Product.AgeInterval.HasUrl", ageIntervalUrl != null);
			fragment.AddVariable("Product.AgeInterval.Url", ageIntervalUrl);

			fragment.AddVariable("Product.ErpId", lekmerProduct.ErpId);

			ProductHelper.AddCampaignAppliedConditions(fragment, item);
		}

        private static string CalculateDiscountpercent(decimal originalPrice, decimal campaignPrice)
        {
            var discountInPercent = (originalPrice - campaignPrice) * 100 / originalPrice;
            discountInPercent = Math.Round(discountInPercent, 0, MidpointRounding.AwayFromZero);
            return discountInPercent + "%";
        }
		private void GetPriceInterval(IProduct product, out IPriceInterval priceInterval, out string priceIntervalUrl)
		{
			priceInterval = _priceIntervalService.GetMatching(
				UserContext.Current, product.Price.PriceIncludingVat);

			if (priceInterval == null || !priceInterval.ContentNodeId.HasValue)
			{
				priceIntervalUrl = null;
				return;
			}

			priceIntervalUrl = GetContentNodeUrl(priceInterval.ContentNodeId.Value);
			if (priceIntervalUrl == null)
			{
				return;
			}

			priceIntervalUrl = UrlHelper.ResolveUrlHttp(priceIntervalUrl);
		}

		private void GetParentCategory(IProduct product, out string parentCategoryTitle, out string parentCategoryUrl)
		{
			parentCategoryTitle = null;
			parentCategoryUrl = null;

			ICategoryTree categoryTree = _categoryService.GetAllAsTree(UserContext.Current);
			var categoryTreeItem = categoryTree.FindItemById(product.CategoryId);
			if (categoryTreeItem == null || categoryTreeItem.Category == null)
			{
				return;
			}

			parentCategoryTitle = categoryTreeItem.Category.Title;

			int? contentNodeId = GetCategoryContentNodeId(categoryTreeItem);

			if (!contentNodeId.HasValue)
			{
				return;
			}

			string categoryUrl = GetContentNodeUrl(contentNodeId.Value);
			if (categoryUrl == null)
			{
				return;
			}

			parentCategoryUrl = UrlHelper.ResolveUrlHttp(categoryUrl);
		}

		private static int? GetCategoryContentNodeId(ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null || categoryTreeItem.Category == null)
			{
				return null;
			}

			if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
			{
				return categoryTreeItem.Category.ProductParentContentNodeId.Value;
			}

			foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
			{
				if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
				{
					return parentItem.Category.ProductParentContentNodeId.Value;
				}
			}

			return null;
		}

		private string GetAgeIntervalUrl(ILekmerProductView product)
		{
			var ageIntervals = _ageIntervalService.GetAllMatching(
				UserContext.Current, product.AgeFromMonth, product.AgeToMonth);
			var firstInterval = ageIntervals.FirstOrDefault();

			if (firstInterval == null || !firstInterval.ContentNodeId.HasValue)
			{
				return null;
			}

			string ageIntervalUrl = GetContentNodeUrl(firstInterval.ContentNodeId.Value);
			if (ageIntervalUrl == null)
			{
				return null;
			}

			ageIntervalUrl = UrlHelper.ResolveUrlHttp(ageIntervalUrl);

			if (ageIntervals.Count() == 1)
			{
				return ageIntervalUrl;
			}

			var query = new QueryBuilder();
			query.Add("mode", "filter");
			foreach (IAgeInterval ageInterval in ageIntervals)
			{
				query.Add("ageinterval-id", ageInterval.Id.ToString(CultureInfo.InvariantCulture));
			}

			return ageIntervalUrl + query;
		}

		private string GetContentNodeUrl(int contentNodeId)
		{
			var treeItem = _contentNodeService.GetTreeItemById(UserContext.Current, contentNodeId);
			if (treeItem == null || treeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return treeItem.Url;
		}
	}
}