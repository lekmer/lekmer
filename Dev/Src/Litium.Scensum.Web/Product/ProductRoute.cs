using System;
using System.Web;
using System.Web.Routing;
using System.Web.SessionState;

namespace Litium.Scensum.Web.Product
{
	public class ProductRoute : Route, IRequiresSessionState
	{
		public ProductRoute(string url, IRouteHandler routeHandler) : base(url, routeHandler)
		{
		}

		public ProductRoute(string url, RouteValueDictionary defaults, IRouteHandler routeHandler)
			: base(url, defaults, routeHandler)
		{
		}

		public ProductRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
		                    IRouteHandler routeHandler) : base(url, defaults, constraints, routeHandler)
		{
		}

		public ProductRoute(string url, RouteValueDictionary defaults, RouteValueDictionary constraints,
		                    RouteValueDictionary dataTokens, IRouteHandler routeHandler)
			: base(url, defaults, constraints, dataTokens, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			if (data == null) return null;

			int urlProductId;
			if (!int.TryParse(data.Values["id"].ToString(), out urlProductId))
			{
				return null;
			}
			string urlProductTitle = data.Values["title"].ToString();
			string urlSiteStructureCommonName = data.Values["sitestructurecommonname"] == null
													? null
													: data.Values["sitestructurecommonname"].ToString();

			httpContext.Items["UrlProductTitle"] = urlProductTitle;
			httpContext.Items["UrlProductId"] = urlProductId;
			httpContext.Items["UrlSiteStructureCommonName"] = urlSiteStructureCommonName;

			return data;
		}
	}
}