using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Product
{
	internal class ProductHelper
	{
		private const string _imageUrlMatchPattern = @"\[Product.ImageUrl\(""(.*?)""\)\]";
		private const int _imageUrlMatchCommonNamePosition = 1;

		internal static void AddImageVariables(Fragment fragment, IImage image, string productTitle)
		{
			string originalSizeImageUrl = null;
			string alternativeText = null;
			if (image != null)
			{
				alternativeText = image.AlternativeText;

				originalSizeImageUrl = string.Format(
					CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
					WebSetting.Instance.MediaUrl, image.Id, UrlCleaner.CleanUp(productTitle), image.FormatExtension);
				originalSizeImageUrl = UrlHelper.ResolveUrl(originalSizeImageUrl);
			}

			fragment.AddCondition("Product.HasImage", image != null);
			fragment.AddVariable("Product.ImageUrl", originalSizeImageUrl);
			fragment.AddCondition("Product.ImageHasAlternativeText", !string.IsNullOrEmpty(alternativeText));
			fragment.AddVariable("Product.ImageAlternativeText", alternativeText);

			fragment.AddRegexVariable(
				_imageUrlMatchPattern,
				delegate(Match match)
					{
						if (image == null) return null;

						string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

						string imageUrl = string.Format(
							CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
							WebSetting.Instance.MediaUrl, image.Id, commonName, UrlCleaner.CleanUp(productTitle), image.FormatExtension);

						return UrlHelper.ResolveUrl(imageUrl);
					},
				VariableEncoding.HtmlEncodeLight,
				RegexOptions.Compiled);
		}

		internal static void AddCampaignAppliedConditions(Fragment fragment, IProduct item)
		{
			fragment.AddCondition("Product.HasProductDiscountAction", HasCampaignActionApplied(item, "ProductDiscount"));
			fragment.AddCondition("Product.HasPercentagePriceDiscountAction", HasCampaignActionApplied(item, "PercentagePriceDiscount"));
		}

		private static bool HasCampaignActionApplied(IProduct item, string actionCommonName)
		{
			return item.CampaignInfo.CampaignActionsApplied.Any(action =>
				action.ActionType.CommonName.Equals(actionCommonName));
		}
	}
}