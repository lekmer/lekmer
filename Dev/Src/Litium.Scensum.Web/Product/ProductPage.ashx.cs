using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Customer;

namespace Litium.Scensum.Web.Product
{
	public class ProductPage : ProductPageHandler
	{
		protected string SiteStructureCommonName
		{
			get { return Items["UrlSiteStructureCommonName"] as string; }
		}

		protected override IProductView GetProductCore()
		{
			var productId = Items["UrlProductId"] as int?;
			if (!productId.HasValue) return null;

			var productService = IoC.Resolve<IProductService>();
			return productService.GetViewById(UserContext.Current, productId.Value);
		}

		protected override void RedirectToCorrectUrl()
		{
			string url = Items["UrlProductComplete"] as string;
			if (string.IsNullOrEmpty(url)) return;

			//Remove first /
			url = url.Substring(1);

			var lekmerProduct = (ILekmerProduct) Product;
			if (lekmerProduct.LekmerUrl != null && !lekmerProduct.LekmerUrl.Equals(url, StringComparison.OrdinalIgnoreCase))
			{
			    string correctUrl = "~/" + lekmerProduct.LekmerUrl;

			    Response.PermanentRedirect(correctUrl);
			}

			//string siteStructureCommonName = SiteStructureCommonName;

			//string productTitle = Items["UrlProductTitle"] as string;
			//if (productTitle == null) return;

			//var lekmerProduct = (ILekmerProduct) Product;

			//string url = productTitle + "-" + Product.Id;
			//if (lekmerProduct.LekmerUrl != null && !lekmerProduct.LekmerUrl.Equals(url, StringComparison.OrdinalIgnoreCase))
			//{
			//    string correctUrl = "~/" + lekmerProduct.LekmerUrl;
			//    if (!siteStructureCommonName.IsNullOrEmpty())
			//    {
			//        correctUrl += "/" + siteStructureCommonName;
			//    }
			//    Response.PermanentRedirect(correctUrl);
			//}
		}

		protected override void AccessDeniedToNotSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetSignInUrl());
		}

		protected override void AccessDeniedToSignedIn()
		{
			Response.Redirect(CustomerUrlHelper.GetMyPagesUrl());
		}

		protected override Core.Web.Master CreateMaster()
		{
			return new Master();
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
			if (!SiteStructureCommonName.IsNullOrEmpty())
			{
				return IoC.Resolve<IContentNodeService>().GetTreeItemByCommonName(UserContext.Current, SiteStructureCommonName);
			}

			return base.GetContentNodeTreeItemCore();
		}

		protected override IContentPageFull GetContentPageCore()
		{
			if (!SiteStructureCommonName.IsNullOrEmpty())
			{
				if (ContentNodeTreeItem == null)
				{
					return null;
				}

				var contentPageService = IoC.Resolve<IContentPageService>();
				return contentPageService.GetFullById(UserContext.Current, ContentNodeTreeItem.Id);
			}

			return base.GetContentPageCore();
		}
	}
}
