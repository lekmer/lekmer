using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Product
{
    public class ProductEntityMapper : Scensum.Product.Web.ProductEntityMapper
    {
        public override void AddEntityVariables(Fragment fragment, IProduct item)
        {
            var lekmerProduct = (ILekmerProduct)item;
            fragment.AddVariable("Product.Url", UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl));

            base.AddEntityVariables(fragment, item);

            ProductHelper.AddImageVariables(fragment, item.Image, item.DisplayTitle);

            IBrand brand = lekmerProduct.Brand;
            fragment.AddCondition("Product.HasBrand", brand != null);

            if (brand != null)
            {
                fragment.AddEntity(brand);
            }

            if (IsPriceAffectedByCampaign(item))
            {
                fragment.AddVariable("Product.DiscountInPercent", CalculateDiscountpercent(lekmerProduct.Price.PriceIncludingVat, lekmerProduct.CampaignInfo.Price.IncludingVat));
            }
            fragment.AddCondition("Product.IsBuyable", lekmerProduct.IsBuyable);
            fragment.AddCondition("Product.IsBookable", lekmerProduct.IsBookable);
            fragment.AddCondition("Product.IsMonitorable", lekmerProduct.IsMonitorable);
            fragment.AddCondition("Product.IsNew", lekmerProduct.IsNewProduct);
            fragment.AddCondition("Product.HasSizes", lekmerProduct.HasSizes);

            fragment.AddVariable("Product.ErpId", lekmerProduct.ErpId);

            ProductHelper.AddCampaignAppliedConditions(fragment, item);
        }

        private static string CalculateDiscountpercent(decimal originalPrice, decimal campaignPrice)
        {
            var discountInPercent = (originalPrice - campaignPrice) * 100 / originalPrice;
            discountInPercent = Math.Round(discountInPercent, 0, MidpointRounding.AwayFromZero);
            return discountInPercent + "%";
        }
    }
}