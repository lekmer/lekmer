using System.Globalization;
using System.Text.RegularExpressions;
using Litium.Lekmer.Media;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Media
{
    public class ImageEntityMapper : EntityMapper<IImage>
    {
        private const string _imageUrlMatchPattern = @"\[Image.Url\(""(.*?)""\)\]";
        private const int _imageUrlMatchCommonNamePosition = 1;

        public override void AddEntityVariables(Fragment fragment, IImage item)
        {
            fragment.AddVariable("Image.Link", ((ILekmerImage)item).Link);
            fragment.AddVariable("Image.Parameter", ((ILekmerImage)item).Parameter);
            fragment.AddCondition("Image.HasTumbnailImageUrl", !string.IsNullOrEmpty(((ILekmerImage)item).TumbnailImageUrl));
            fragment.AddVariable("Image.TumbnailImageUrl", ((ILekmerImage)item).TumbnailImageUrl);
            fragment.AddCondition("Image.IsLink", ((ILekmerImage)item).IsLink);
            fragment.AddCondition("Image.HasImage", ((ILekmerImage)item).HasImage);

            fragment.AddVariable("Image.Id", item.Id.ToString(CultureInfo.InvariantCulture));
            fragment.AddVariable("Image.Title", item.Title);
            fragment.AddVariable("Image.AlternativeText", item.AlternativeText);

            fragment.AddVariable("Image.Url", UrlHelper.ResolveUrl(string.Format(
                                                                    CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
                                                                    WebSetting.Instance.MediaUrl, item.Id,
                                                                    UrlCleaner.CleanUp(item.Title), item.FormatExtension)));

            fragment.AddRegexVariable(
                _imageUrlMatchPattern,
                delegate(Match match)
                {
                    string commonName = match.Groups[_imageUrlMatchCommonNamePosition].Value;

                    string imageUrl = string.Format(
                        CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
                        WebSetting.Instance.MediaUrl, item.Id, commonName, UrlCleaner.CleanUp(item.Title), item.FormatExtension);

                    return UrlHelper.ResolveUrl(imageUrl);
                },
                VariableEncoding.HtmlEncodeLight,
                RegexOptions.Compiled);
        }
    }
}