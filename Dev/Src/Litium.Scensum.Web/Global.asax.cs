using System;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Framework.Statistics;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Setting;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;
using Litium.Scensum.Product.Cache;
using Litium.Scensum.Statistics;
using Litium.Scensum.Statistics.TrackerEntity;
using Litium.Scensum.Web.Error;
using log4net.Config;

namespace Litium.Scensum.Web
{
    public class Global : HttpApplication
    {
        protected Global()
        {
            BeginRequest += delegate
            {
                Response.CacheControl = "no-cache";
                Response.Expires = -10000;
            };
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Application_Start(object sender, EventArgs e)
        {
            RoutingRegistration.RegisterRoutes(RouteTable.Routes);

            CacheUpdateCleaner.CacheUpdateFeed = new CacheUpdateFeed();
#if DEBUG
            CacheUpdateCleaner.WorkerCleanInterval = 1000; // Clean interval lowered in debug mode.
#endif
            CacheUpdateCleaner.StartWorker();

            StatisticsSetup.CompilerLocator = new CompilerLocator();
            StatisticsSetup.WorkerInterval = StatisticsSetting.Instance.WorkerInterval;

            XmlConfigurator.Configure();

            Initializer.InitializeAll();
        }

        private static void PopulateProductCache()
        {
            var cacheSetting = new CacheSetting(ProductCache.Instance.ManagerName);

            var populateCount = (int)(cacheSetting.MaximumElementsInCacheBeforeScavenging * 0.8m);
            TimeSpan waitTime = LekmerProductCacheSetting.Instance.ExpirationInMinutes;
            if (populateCount <= 0 || waitTime.Minutes <= 0) return;
            
            var random = new Random();
            TimeSpan additionalRandomTimeToWait =
                TimeSpan.FromMinutes(random.Next(0, (int)LekmerProductCacheSetting.Instance.RefreshIntervalTolerance.TotalMinutes));
            ProductCachePopulateWorker.StartIfNotStarted(
                UserContext.Current,
                populateCount,
                waitTime + additionalRandomTimeToWait,
                additionalRandomTimeToWait);
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Session_Start(object sender, EventArgs e)
        {
            PopulateProductCache();

            AlternateVersion.SetAlternate();
            if (!IsCrawler(Request))
            {
                var visitor = new Visitor
                {
                    ChannelId = Channel.Current.Id,
                    Date = DateTime.Now,
                    Alternate = AlternateVersion.UseAlternate,
                    Referrer = Request.UrlReferrer == null ? null : Request.UrlReferrer.ToString()
                };
                StatisticsTracker.Track(visitor);
            }
            ILekmerCartService lekmerCartService = (ILekmerCartService)IoC.Resolve<ICartService>();
            lekmerCartService.RetrivePreviousShoppingCard();
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext context = HttpContext.Current;

            // HttpExceptions with HttpCode 404 should render a PageNotFound.
            var httpException = context.Server.GetLastError() as HttpException;
            if (httpException != null && httpException.GetHttpCode() == 404)
            {
                context.Response.Clear();
                new PageNotFound().ProcessRequest(context);
                Server.ClearError();

                return;
            }

            if (ErrorSetting.Instance.ShowFriendlyError)
            {
                context.Response.Clear();
                new ServerError().ProcessRequest(context);
                Server.ClearError();
            }
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Session_End(object sender, EventArgs e)
        {
        }

        [SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
        protected void Application_End(object sender, EventArgs e)
        {
        }

        private static bool IsCrawler(HttpRequest request)
        {
            if (request.Browser.Crawler) return true;

            string userAgent = request.UserAgent;
            if (!string.IsNullOrEmpty(userAgent))
            {
                const string pattern =
                    @"slurp|ask|teoma|google|bot|yahoo|spider
					|archiver|curl|python|nambu|twitt|perl|sphere|PEAR|java
					|wordpress|radian|crawl|yandex|eventbox|monitor|mechanize|facebookexternal";
                return Regex.IsMatch(userAgent, pattern, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
            }

            return false;
        }
    }
}