using System.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace Litium.Scensum.Web.Diagnostics
{
	public class CacheDiagnosticsSection : ConfigurationSection
	{
		[ConfigurationProperty("enabled", DefaultValue = false, IsRequired = false)]
		public bool Enabled
		{
			get { return (bool) this["enabled"]; }
		}

		[ConfigurationProperty("localOnly", DefaultValue = true, IsRequired = false)]
		public bool LocalOnly
		{
			get { return (bool) this["localOnly"]; }
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public static CacheDiagnosticsSection GetConfig()
		{
			return (CacheDiagnosticsSection) ConfigurationManager.GetSection("cacheDiagnostics");
		}
	}
}