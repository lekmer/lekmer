namespace Litium.Scensum.Web.Diagnostics
{
	public class CacheManagerInfo
	{
		public string ManagerName { get; set; }

		public int Count { get; set; }
	}
}