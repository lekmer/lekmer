﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CacheDiagnostics.aspx.cs" Inherits="Litium.Scensum.Web.Diagnostics.CacheDiagnostics"

%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Cache diagnostics</title>
</head>
<body>
    <form id="form1" runat="server" enableviewstate=false>
		<h1>Cache diagnostics</h1>
		
		<asp:GridView ID="CacheManagerGrid" runat="server" EnableViewState=false />
    </form>
</body>
</html>