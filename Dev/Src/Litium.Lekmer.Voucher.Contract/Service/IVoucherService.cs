namespace Litium.Lekmer.Voucher
{
	public interface IVoucherService
	{
		IVoucherCheckResult VoucherCheck(string channelApplicationName, int customerId, string code);
		void VoucherConsume(string channelApplicationName, int customerId, string code, int orderId);
	    void VoucherRelease(string code);
	    void VoucherReserve(string code);
	}
}