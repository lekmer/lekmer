using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using IUserContextFactory = Litium.Scensum.Core.IUserContextFactory;

namespace Litium.Lekmer.Voucher.Web
{
	public class BlockVoucherDiscountControl : BlockControlBase
	{
		private VoucherDiscountForm _voucherDiscountForm;

		public VoucherDiscountForm VoucherDiscountForm
		{
			get
			{
				return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm());
			}
		}

		public BlockVoucherDiscountControl(ICustomerSession customerSession, ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
			CustomerSession = customerSession;
		}

		protected override BlockContent RenderCore()
		{
			IoC.Resolve<IVoucherSession>().VoucherCode = null;
			IoC.Resolve<IVoucherSession>().WasUsed = false;
			if (IsVoucherPost())
			{
				VoucherDiscountForm.MapFromRequest();

				var voucherCode = VoucherDiscountForm.VoucherCode;
				if (string.IsNullOrEmpty(voucherCode))
				{
					IoC.Resolve<IVoucherSession>().Voucher = null;
					IoC.Resolve<IUserContextFactory>().FlushCache();
				}
				else
				{
					ICustomer customer = GetCustomer();

					var voucherService = IoC.Resolve<IVoucherService>();
					var voucherCheckResult = voucherService.VoucherCheck(UserContext.Current.Channel.ApplicationName, customer.Id, voucherCode);
					if (voucherCheckResult.IsValid)
					{
						IoC.Resolve<IVoucherSession>().Voucher = voucherCheckResult;
						IoC.Resolve<IUserContextFactory>().FlushCache();
					}
					else
					{
						IoC.Resolve<IVoucherSession>().Voucher = null;
						IoC.Resolve<IUserContextFactory>().FlushCache();
					}
				}
			}
			else if (PostMode == "checkout" && Request.Form["GetAddress"].IsNullOrEmpty())
			{
				ICustomer customer = GetCustomer();
				CheckVoucher(customer);
			}
			return RenderForm();
		}

		protected void CheckVoucher(ICustomer customer)
		{
			IVoucherSession voucherSession = IoC.Resolve<IVoucherSession>();
			if (voucherSession.Voucher != null && voucherSession.Voucher.IsValid)
			{
				var voucherService = IoC.Resolve<IVoucherService>();
				var voucherCheckResult = voucherService.VoucherCheck(UserContext.Current.Channel.ApplicationName, customer.Id,
																	 voucherSession.Voucher.VoucherCode);

				if (!voucherCheckResult.IsValid)
				{
					IoC.Resolve<IVoucherSession>().WasUsed = true;
					IoC.Resolve<IVoucherSession>().VoucherCode = voucherSession.Voucher.VoucherCode;
					IoC.Resolve<IVoucherSession>().Voucher = null;
					IoC.Resolve<IUserContextFactory>().FlushCache();
					return;
				}
			}
		}

		private bool IsVoucherPost()
		{
			string useVoucherValue = Request.Form[VoucherDiscountForm.UseVoucherFormName];
			return useVoucherValue == VoucherDiscountForm.UseVoucherFormValue || useVoucherValue == VoucherDiscountForm.UseVoucherFormText;
		}

		private static BlockContent RenderForm()
		{
			return new BlockContent(string.Empty, string.Empty);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ICustomer GetCustomer()
		{
			var customerService = IoC.Resolve<ICustomerService>();
			ICustomer customer = null;
			if (SignedInCustomerId.HasValue)
			{
				customer = customerService.GetById(UserContext.Current, SignedInCustomerId.Value);
			}
			if (customer == null)
			{
				customer = customerService.Create(UserContext.Current, true, false);
			}

			if (customer.CustomerInformation == null)
			{
				customer.CustomerInformation = IoC.Resolve<ICustomerInformationService>().Create(UserContext.Current);
			}

			if (customer.CustomerInformation.DefaultBillingAddress == null)
			{
				customer.CustomerInformation.DefaultBillingAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
			}
			customer.CustomerInformation.DefaultBillingAddress.CountryId = Channel.Current.Country.Id;


			if (customer.CustomerInformation.DefaultDeliveryAddress == null)
			{
				customer.CustomerInformation.DefaultDeliveryAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
			}
			customer.CustomerInformation.DefaultDeliveryAddress.CountryId = Channel.Current.Country.Id;

			return customer;
		}

		public ICustomerSession CustomerSession { get; set; }

		protected int? SignedInCustomerId
		{
			get { return CustomerSession.IsSignedIn ? (int?)CustomerSession.SignedInCustomer.Id : null; }
		}
	}
}