using System;
using System.Collections.Specialized;
using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Voucher.Web
{
    public class VoucherDiscountForm : ControlBase
    {
        public virtual string VoucherCode { get; set; }

        public string VoucherCodeFormName
        {
            get { return "voucher-code"; }
        }
        public string UseVoucherFormName
        {
            get { return "action"; }
        }
        public string UseVoucherFormValue
        {
            get { return "voucher"; }
        }
        public string UseVoucherFormText
        {
            get { return AliasHelper.GetAliasValue("Order.Checkout.Voucher.UseVoucher.Button.Value"); }
        }

        public void MapFromRequest()
        {
            NameValueCollection form = Request.Form;
            VoucherCode = form[VoucherCodeFormName].NullWhenTrimmedEmpty();
        }

        public void MapFieldNamesToFragment(Fragment fragment)
        {
            fragment.AddVariable("Form.VoucherCodeTextBox.Name", VoucherCodeFormName);
            fragment.AddVariable("Form.UseVoucherButton.Name", UseVoucherFormName);
        }

        public void MapFieldValuesToFragment(Fragment fragment)
        {
            fragment.AddVariable("Form.UseVoucherButton.Value", UseVoucherFormValue);

            var voucher = IoC.Resolve<IVoucherSession>().Voucher;
            ICartFull cart = IoC.Resolve<ICartSession>().Cart;
            var campaignInfo = (ILekmerCartCampaignInfo)cart.CampaignInfo;
            if (voucher != null && campaignInfo.VoucherDiscount != null)
            {
                fragment.AddCondition("IsVoucherUsed", true);
                var voucherInfo = ComposeVoucherInfo(campaignInfo);
                fragment.AddVariable("UsedVoucherInfo", voucherInfo);
                fragment.AddVariable("Form.VoucherCodeTextBox.Value", voucher.VoucherCode);
            }
            else
            {
                fragment.AddCondition("IsVoucherUsed", false);
                fragment.AddVariable("Form.VoucherCodeTextBox.Value", string.Empty);
            }
        }

        private static string ComposeVoucherInfo(ILekmerCartCampaignInfo campaignInfo)
        {
            if (campaignInfo.VoucherDiscountType == VoucherValueType.Percentage)
            {
                return string.Format("- {0} %", campaignInfo.VoucherDiscount);
            }
            if (campaignInfo.VoucherDiscountType == VoucherValueType.Price)
            {
                return string.Format("- {0} {1}", campaignInfo.VoucherDiscount, UserContext.Current.Channel.Currency.Iso);
            }
            throw new NotSupportedException("VoucherValueType '" + campaignInfo.VoucherDiscountType + "' not supported.");
        }
    }
}