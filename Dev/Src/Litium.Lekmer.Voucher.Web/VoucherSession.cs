using System.Web;
using System.Web.SessionState;
using Litium.Lekmer.Core.Web;

namespace Litium.Lekmer.Voucher.Web
{
    public class VoucherSession : IVoucherSession
    {
        private static HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        public bool WasUsed
        {
            get
            {
                if (Session == null) return false;
                return (bool)Session["VoucherWasUsed"];
            }
            set
            {
                if (Session == null) return;
                Session["VoucherWasUsed"] = value;
            }
        }
        public string VoucherCode
        {
            get
            {
                if (Session == null) return null;
                return Session["VoucherCode"] as string;
            }
            set
            {
                if (Session == null) return;
                Session["VoucherCode"] = value;
            }
        }
        public IVoucherCheckResult Voucher
        {
            get
            {
                if (Session == null) return null;
                return Session["VoucherCheckResult"] as IVoucherCheckResult;
            }
            set
            {
                if (Session == null) return;
                Session["VoucherCheckResult"] = value;
            }
        }
    }
}