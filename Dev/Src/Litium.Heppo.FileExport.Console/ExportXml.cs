﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Media;
using System.Globalization;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Lekmer.FileExport
{
	internal class ExportXml
	{
      // when possible add Lekmer ArtNo (does not exist in the current system 
      public void ExportProductsPrice(IUserContext context, IEnumerable<IProduct> products, string channelCommonName)
		{
			var categoryService = IoC.Resolve<ICategoryService>();
			ICategoryTree categoryTree = categoryService.GetAllAsTree(context);
   
			var doc = new XmlDocument();
			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);
			XmlNode lekmerProductsNode = doc.CreateElement("products"); // <lekmer:products>
			//XmlAttribute productsAtribute = doc.CreateAttribute("type");
			//productsAtribute.Value = "toys"; // hårdkoda inte sen!
			//lekmerProductsNode.Attributes.Append(productsAtribute);
			XmlAttribute productsAtribute2 = doc.CreateAttribute("export_date");
			productsAtribute2.Value = DateTime.Now.ToString();
			lekmerProductsNode.Attributes.Append(productsAtribute2);
			doc.AppendChild(lekmerProductsNode);


			XmlNode countries = doc.CreateElement("countries"); 
			lekmerProductsNode.AppendChild(countries);
			XmlNode country = doc.CreateElement("country"); 
			XmlAttribute countryAtribute = doc.CreateAttribute("iso");
			countryAtribute.Value = context.Channel.Country.Iso;
			country.Attributes.Append(countryAtribute);
			countries.AppendChild(country);

			foreach (ILekmerProductView lekmerProduct in products)
			{
				ICategoryTreeItem categoryTreeItem = categoryTree.FindItemById(lekmerProduct.CategoryId);
                ICategory category = categoryTreeItem.Category;
                /*if(categoryTreeItem.Category == null)
			    {
			        continue;
			    }
			    else
			    {
			        category = categoryTreeItem.Category;
			    }*/

				// Xml product element
				XmlNode productNode = doc.CreateElement("product");
				XmlAttribute productAttribute = doc.CreateAttribute("type");
				productAttribute.Value = category.Title; 
				productNode.Attributes.Append(productAttribute);
				country.AppendChild(productNode);

				// Xml id element
                XmlNode productIdNode = doc.CreateElement("id"); // lekmerProduct.ErpId - lekmerProduct.Id.ToString()
                productIdNode.AppendChild(doc.CreateTextNode(lekmerProduct.ErpId));
				productNode.AppendChild(productIdNode);

				// Xml ean element
				XmlNode productEanNode = doc.CreateElement("ean");
				if (lekmerProduct.EanCode == null)
				{
					productEanNode.AppendChild(doc.CreateTextNode("missing"));
				}
				else
				{
					productEanNode.AppendChild(doc.CreateTextNode(lekmerProduct.EanCode));
				}
				productNode.AppendChild(productEanNode);

                
				// Xml brand element
				XmlNode productBrandNode = doc.CreateElement("brand");
				//if (lekmerProduct.Brand != null)
				if (lekmerProduct.Brand == null || lekmerProduct.Brand.Title.Equals(""))
				{
					productBrandNode.AppendChild(doc.CreateTextNode("missing"));
				}
				else
				{   
					productBrandNode.AppendChild(doc.CreateTextNode(lekmerProduct.Brand.Title));
				}
				productNode.AppendChild(productBrandNode);
                

				// Xml title element
				XmlNode productTitleNode = doc.CreateElement("title");
				productTitleNode.AppendChild(doc.CreateTextNode(lekmerProduct.DisplayTitle)); 
				productNode.AppendChild(productTitleNode);


                //string a = lekmerProduct.CategoryId.ToString();
			    //string c = category.ParentCategoryId.ToString(); // him her kids
                                
                // Xml gender element
                XmlNode productGenderNode = doc.CreateElement("gender");
                productGenderNode.AppendChild(doc.CreateTextNode(GetGender(category.ParentCategoryId.ToString())));
                productNode.AppendChild(productGenderNode);

			
                
                //decimal priceIncludingVat = lekmerProduct.CampaignInfo != null ? lekmerProduct.CampaignInfo.Price.IncludingVat : originalPriceIncludingVat;


				// Xml price element
				XmlNode productPriceNode = doc.CreateElement("price");
                //decimal originalPriceIncludingVat = product.Price.PriceIncludingVat;
			    decimal originalPriceIncludingVat = lekmerProduct.Price.PriceIncludingVat;
                // Modify price depending on active campaign
                decimal priceIncludingVat = lekmerProduct.CampaignInfo != null ? lekmerProduct.CampaignInfo.Price.IncludingVat : originalPriceIncludingVat;
                productPriceNode.AppendChild(doc.CreateTextNode(priceIncludingVat.ToString().Replace(",",".")));
				
                XmlAttribute productPriceNodeAttribute = doc.CreateAttribute("currency");
				productPriceNodeAttribute.Value = context.Channel.Currency.Iso;
				productPriceNode.Attributes.Append(productPriceNodeAttribute);
				productNode.AppendChild(productPriceNode);
                //decimal priceIncludingVat = item.CampaignInfo != null ? item.CampaignInfo.Price.IncludingVat : originalPriceIncludingVat;
				
               
               


				// Xml link element
				XmlNode productLinkNode = doc.CreateElement("link"); // URL fel?
				productLinkNode.AppendChild(doc.CreateTextNode("http://" + context.Channel.ApplicationName + "/" + lekmerProduct.LekmerUrl));
				productNode.AppendChild(productLinkNode);

				// Xml instock element
				XmlNode productInStockNode = doc.CreateElement("in_stock");
				if (lekmerProduct.NumberInStock > 0)
				{
					productInStockNode.AppendChild(doc.CreateTextNode("yes"));
                    productNode.AppendChild(productInStockNode);

                    /*
                    // Xml amount in stock element
                    XmlNode productAmountInStockNode = doc.CreateElement("number_in_stock");
                    productAmountInStockNode.AppendChild(doc.CreateTextNode(lekmerProduct.NumberInStock.ToString())); 
                    productNode.AppendChild(productAmountInStockNode);
                    */
				}
				else
				{
					productInStockNode.AppendChild(doc.CreateTextNode("no"));
                    productNode.AppendChild(productInStockNode);
				}
                

                // Xml deliverytime element
                XmlNode productDeliveryTimeNode = doc.CreateElement("delivery_time_days");
                string deliveryTimeDays = context.Channel.Currency.Iso.Equals("SEK") ? "1-3" : "3-5";
                productDeliveryTimeNode.AppendChild(doc.CreateTextNode(deliveryTimeDays)); 
                productNode.AppendChild(productDeliveryTimeNode);

				// Xml description element
				XmlNode productDescriptionNode = doc.CreateElement("description");
				productDescriptionNode.AppendChild(doc.CreateCDataSection(lekmerProduct.Description));
				productNode.AppendChild(productDescriptionNode);


				// Xml images element
				XmlNode productImagesNode = doc.CreateElement("images");
				productNode.AppendChild(productImagesNode);

                //http://heppo.se/mediaarchive/1008232/fred-perry-b6030-1000960.jpg
                //http://heppo.se/mediaarchive/1008232/main/fred-perry-b6030-1000960.jpg
                //http://heppo.se/mediaarchive/1008232/thumbnail/modell-b6030.jpg

				// Xml image element
			    string imageUrl = string.Empty;
                string imageUrlMain = string.Empty;
                string imageUrlThumbnail = string.Empty;
                if (lekmerProduct.Image != null)
                {
                    IImage image = lekmerProduct.Image;
                    imageUrl = string.Format(
                        CultureInfo.InvariantCulture, "http://{4}{0}{1}/{2}.{3}",
                        WebSetting.Instance.MediaUrl.Substring(1), image.Id,
                        UrlCleaner.CleanUp(image.Title), image.FormatExtension,
                        context.Channel.ApplicationName);

                    imageUrlMain = string.Format(
                        CultureInfo.InvariantCulture, "http://{4}{0}{1}/main/{2}.{3}",
                        WebSetting.Instance.MediaUrl.Substring(1), image.Id,
                        UrlCleaner.CleanUp(image.Title), image.FormatExtension,
                        context.Channel.ApplicationName);

                    imageUrlThumbnail = string.Format(
                        CultureInfo.InvariantCulture, "http://{4}{0}{1}/thumbnail/{2}.{3}",
                        WebSetting.Instance.MediaUrl.Substring(1), image.Id,
                        UrlCleaner.CleanUp(image.Title), image.FormatExtension,
                        context.Channel.ApplicationName);
                }
                else // ÄNDRA FÖR heppo
                {
                    imageUrl = "http://lekmer.se/media/images/lekmer/noimage250x250.png";
                    imageUrlMain = "http://lekmer.se/media/images/lekmer/noimage250x250.png";
                    imageUrlThumbnail = "http://lekmer.se/media/images/lekmer/noimage250x250.png";
                }

                XmlNode productImageNode = doc.CreateElement("Detailed"); 
                productImageNode.AppendChild(doc.CreateTextNode(imageUrl));
				productImagesNode.AppendChild(productImageNode);

                XmlNode productImageRegularNode = doc.CreateElement("Regular");
                productImageRegularNode.AppendChild(doc.CreateTextNode(imageUrlMain));
                productImagesNode.AppendChild(productImageRegularNode);

                XmlNode productImageThumbnailNode = doc.CreateElement("Thumbnail");
                productImageThumbnailNode.AppendChild(doc.CreateTextNode(imageUrlThumbnail));
                productImagesNode.AppendChild(productImageThumbnailNode);
			}

			//doc.Save(Console.Out);
          try
          {
              string dirr = ConfigurationManager.AppSettings["DestinationDirectoryPrice"];
              string name = ConfigurationManager.AppSettings["XmlFileNameProductExporter" + channelCommonName];
              string path = Path.Combine(dirr, name);
              Console.WriteLine(path);
              doc.Save(path);
              //doc.Save(@"C:\Users\Dino\Desktop\bilder\heppoGender.xml");
          }
          catch (Exception e)
          {
              Console.WriteLine("in sitmap path " + e);
          }
		}

		public void ExportSitemap(IEnumerable<SitemapItem> sitemapItems, string channelCommonName)
		{
            var doc = new XmlDocument();
            XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(docNode);

            XmlNode urlsetNode = doc.CreateElement("urlset");
            XmlAttribute urlsetAtribute = doc.CreateAttribute("xmlns");
            urlsetAtribute.Value = "http://www.sitemaps.org/schemas/sitemap/0.9"; // hårdkoda inte sen!
            urlsetNode.Attributes.Append(urlsetAtribute);
            XmlAttribute urlsetAtribute2 = doc.CreateAttribute("export_date");
            urlsetAtribute2.Value = DateTime.Now.ToString();
            urlsetNode.Attributes.Append(urlsetAtribute2);
            doc.AppendChild(urlsetNode);


			foreach (SitemapItem sitemapItem in sitemapItems)
			{
                XmlNode urlNode = doc.CreateElement("url");
			    urlsetNode.AppendChild(urlNode);

                XmlNode locNode = doc.CreateElement("loc");
			    locNode.AppendChild(doc.CreateTextNode(sitemapItem.Url));
			    urlNode.AppendChild(locNode);

                //XmlNode lastModeNode
                if (sitemapItem.LastMod.HasValue)
                {
                    XmlNode lastModNode = doc.CreateElement("lastmod");
                    lastModNode.AppendChild(doc.CreateTextNode(sitemapItem.LastMod.Value.ToString()));
                    urlNode.AppendChild(lastModNode);

                    //sitemapItem.LastMod.Value.ToString();
                }


				
				 XmlNode changeFreqNode = doc.CreateElement("changefreq");
				 changeFreqNode.AppendChild(doc.CreateTextNode(sitemapItem.ChangeFreq));
				 urlNode.AppendChild(changeFreqNode);    

			     XmlNode priorityNode = doc.CreateElement("priority");
				 priorityNode.AppendChild(doc.CreateTextNode(sitemapItem.Priority.ToString()));
				 urlNode.AppendChild(priorityNode);
				


				//Console.WriteLine(sitemapItem.Url);

			}

		    try
		    {
                string dirr = ConfigurationManager.AppSettings["DestinationDirectorySitemap"];
                string name = ConfigurationManager.AppSettings["XmlFileNameSitemapExporter" + channelCommonName];
                string path = Path.Combine(dirr, name);
                Console.WriteLine(path);
                doc.Save(path);
		    }
		    catch (Exception e)
		    {
		        
		        Console.WriteLine("in sitmap path "+ e);
		    }
            

		}

        ////////////////////////////////////////
        // Bad solution, exten API later instead
	    private Dictionary<int, string> d;

        private string GetGender(string parentCatoegoryId)
        {
            foreach (var category in d)
            {
                if (category.Key == int.Parse(parentCatoegoryId))
                {
                    return category.Value;
                }
            }

            return "missing";
        }

        public ExportXml()
        {
            d = new Dictionary<int, string>
                    {
                        {1000060, "male"},
                        {1000064, "female"},
                        {1000165, "unisex_adults"},
                        {1000182, "kids"},
                        {1000186, "kids"},
                        {1000188, "kids"},
                        {1000208, "unisex"}
                    };
        }
	}
}
