using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Threading;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport
{
	public class PriceExporter : IExporter
	{
        private readonly ProductCollection completeList = new ProductCollection();
        private readonly ExportXml ep = new ExportXml();

		public void Execute()
		{
            foreach (IUserContext context in GetUserContextForAllChannels())
            {

                //IUserContext context = GetUserContext();

                int page = 1; // page best�mmer om f�rsta 100 ska visas eller andra hundra osv.
                int pageSize; // pagesize best�mmer hur m�nga som ska h�mtas �t g�ngen

                if (!int.TryParse(ConfigurationManager.AppSettings["PageSize"], out pageSize))
                {
                    pageSize = 100;
                }

                var lekmerProductService = (ILekmerProductService)IoC.Resolve<IProductService>();
                ProductCollection products = lekmerProductService.GetViewAllForExport(context, page, pageSize);
                //int totalCount = Math.Min(products.TotalCount, 200);
                int totalCount = products.TotalCount;

                // Clear the list that will hold all products
                completeList.Clear();
                // loop aslong as there are any more products
                while (totalCount > 0)
                {
                    Console.WriteLine("Reading product data... product count: " + totalCount);
					products = lekmerProductService.GetViewAllForExport(context, page, pageSize);
                    Copy(products);
                    totalCount = totalCount - pageSize;
                    page++;
                }
                // calls the create xml method after all products have been read from the DB and
                // reside in the completeList
                ep.ExportProductsPrice(context, completeList, context.Channel.CommonName);
                //break;
			//throw new NotImplementedException();
            }
		}

        private static IEnumerable<IUserContext> GetUserContextForAllChannels()
        {
            //generera 5 filer en per f�rs�ljningskanal + .com

            var channelService = (LekmerChannelService)IoC.Resolve<IChannelService>();
            Collection<IChannel> channels = channelService.GetAll();

            return channels
                .Select(channel =>
                {
                    var context = IoC.Resolve<IUserContext>();
                    context.Channel = channel;
                    return context;
                });
        }

        /*
        private static IUserContext GetUserContext()
        {
            var channelService = IoC.Resolve<IChannelService>();
            IChannel channel = channelService.GetById(1); // kolla upp, tror det �r swe

            var context = IoC.Resolve<IUserContext>();
            context.Channel = channel;
            return context;
        }*/

        public void Copy(ProductCollection pro)
        {
            for (int i = 0; i < pro.Count; i++)
            {
                completeList.Add(pro[i]);
            }
        }
	}
}