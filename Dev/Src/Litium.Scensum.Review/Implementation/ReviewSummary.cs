namespace Litium.Scensum.Review
{
	public class ReviewSummary : IReviewSummary
	{
		public decimal? AverageRating { get; set; }

		public int ReviewCount { get; set; }
	}
}