﻿using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Repository
{
	public class ReviewStatusRepository
	{
		protected virtual DataMapperBase<IReviewStatus> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IReviewStatus>(dataReader);
		}

		public virtual IReviewStatus GetByCommonName(string commonName)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CommonName", commonName, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("ReviewStatusRepository.GetByCommonName");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pReviewStatusGetByCommonName]",
				                                                         parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IReviewStatus> GetAll()
		{
			var dbSettings = new DatabaseSetting("ReviewStatusRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pReviewStatusGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}