﻿using System;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review.Repository
{
	public class ReviewRepository
	{
		protected virtual DataMapperBase<IReview> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IReview>(dataReader);
		}

		protected virtual DataMapperBase<IReviewRecord> CreateRecordDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IReviewRecord>(dataReader);
		}

		[SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual ReviewCollection GetAllByProduct(int channelId, int productId, int pageNumber, int pageSize)
		{
			var itemsCount = 0;
			var images = new ReviewCollection();
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Page", pageNumber, SqlDbType.Int),
					ParameterHelper.CreateParameter("PageSize", pageSize, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ReviewRepository.GetAllByProduct");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pReviewGetAllByProduct]",
				                                                         parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					images = CreateDataMapper(dataReader).ReadMultipleRows<ReviewCollection>();
				}
			}
			images.TotalCount = itemsCount;
			return images;
		}

		public virtual ReviewRecordCollection Search(IReviewSearchCriteria searchCriteria, int page, int pageSize)
		{
			if (searchCriteria == null) throw new ArgumentNullException("searchCriteria");
			var itemsCount = 0;
			var reviews = new ReviewRecordCollection();
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductTitle", searchCriteria.ProductTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@ProductId", searchCriteria.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Message", searchCriteria.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Author", searchCriteria.Author, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@StatusId", searchCriteria.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CreatedFrom", searchCriteria.CreatedFrom, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@CreatedTo", searchCriteria.CreatedTo, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@Page", page, SqlDbType.Int),
					ParameterHelper.CreateParameter("@PageSize", pageSize, SqlDbType.Int),
					ParameterHelper.CreateParameter("@SortBy", searchCriteria.SortBy, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@SortDescending", searchCriteria.SortByDescending, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("ReviewRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pReviewSearch]", parameters, dbSettings)
				)
			{
				if (dataReader.Read())
				{
					itemsCount = dataReader.GetInt32(0);
				}
				if (dataReader.NextResult())
				{
					reviews = CreateRecordDataMapper(dataReader).ReadMultipleRows<ReviewRecordCollection>();
				}
			}
			reviews.TotalCount = itemsCount;
			return reviews;
		}

		public virtual void SetStatus(int reviewId, int statusId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ReviewId", reviewId, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusId", statusId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ReviewRepository.SetStatus");
			new DataHandler().ExecuteCommand("[addon].[pReviewSetStatus]", parameters, dbSettings);
		}

		public virtual void Delete(int reviewId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ReviewId", reviewId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ReviewRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pReviewDelete]", parameters, dbSettings);
		}

		public virtual int Save(IReview review)
		{
			if (review == null) throw new ArgumentNullException("review");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", review.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ReviewStatusId", review.ReviewStatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", review.CustomerId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Rating", review.Rating, SqlDbType.TinyInt),
					ParameterHelper.CreateParameter("Message", review.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Author", review.Author, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CreatedDate", review.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("ChannelId", review.ChannelId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ReviewRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[addon].[pReviewSave]", parameters, dbSettings);
		}
	}
}