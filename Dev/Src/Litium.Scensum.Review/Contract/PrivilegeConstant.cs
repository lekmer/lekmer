using System;

namespace Litium.Scensum.Review
{
	[Serializable]
	public static class PrivilegeConstant
	{
		private static string _assortmentReview = "Assortment.Review";

		public static string AssortmentReview
		{
			get { return _assortmentReview; }
		}
	}
}