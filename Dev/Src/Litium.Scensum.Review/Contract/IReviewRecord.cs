namespace Litium.Scensum.Review
{
	public interface IReviewRecord : IReview
	{
		string ProductTitle { get; set; }
        string UserName { get; set; }
	}
}