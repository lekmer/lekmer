﻿namespace Litium.Lekmer.Payment.Klarna
{
    public interface IKreditorResponseItem
    {
        object HasAccount { get; set; }
        string[] Address { get; set; }
        string[,] Addresses { get; set; } 
        object OkCustomer { get; set; }
        object[] PClasses { get; set; } 
    }
}
