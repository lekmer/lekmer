﻿using System;

namespace Litium.Lekmer.FileExport
{
	internal class Program
	{
		private static void Main()
		{
			var exporters = new IExporter[]
			                	{
			                		new SitemapExporter(),
                                    new PriceExporter()
			                	};

			foreach (IExporter exporter in exporters)
			{
			    try
			    {
                    exporter.Execute();
			    }
			    catch (Exception e)
			    {
			       //throw;
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    //var log = new Log();
                    //log.LogMessageToFile(string.Format("process failed:\nMessage: {0}\nStacktrace: {1}", e.Message, e.StackTrace));

			        //Console.ReadLine();
			    }
                //Console.ReadLine();
			}
		}
	}
}