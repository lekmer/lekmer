using System;

namespace Litium.Lekmer.FileExport
{
	public class SitemapItem
	{
		public string Url { get; set; }
		public decimal Priority { get; set; }
        public string ChangeFreq { get; set; }
        public DateTime? LastMod { get; set; }
	}
}