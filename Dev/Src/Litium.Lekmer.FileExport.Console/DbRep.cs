﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Litium.Lekmer.FileExport
{
    public class DbRep
    {
        public Dictionary<int, string> GetProductsXmltopList(int contentNodeId, int channelId)
        {
            string sqlDbConn = ConfigurationManager.ConnectionStrings["LekmerConsole"].ToString();
            var ppList = new Dictionary<int, string>();

            using (var cnn = new SqlConnection(sqlDbConn))
            {
                var cmd = new SqlCommand("[integration].[pGetProductsByContentNodeId]", cnn)
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "[integration].[pGetProductsByContentNodeId]",
                    CommandTimeout = 180
                };

                cmd.Parameters.Add("@ContentNodeId", SqlDbType.Int).Value = contentNodeId;
                cmd.Parameters.Add("@ChannelId", SqlDbType.Int).Value = channelId;

                try
                {
                    cnn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        ppList.Add((int)reader.GetValue(0), reader.GetValue(1).ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("process failed: {0}", e);
                }
            }

            return ppList;
        }
    }
}
