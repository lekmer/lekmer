using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Threading;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.FileExport
{
	public class PriceExporter : IExporter
	{
        private readonly ProductCollection completeList = new ProductCollection();
        private readonly ProductCollection xmlTopList = new ProductCollection();
        private readonly ExportXml ep = new ExportXml();
	    private Dictionary<int, string> xmlTopListProducts;

		public void Execute()
		{
            foreach (IUserContext context in GetUserContextForAllChannels())
            {
                //XmlTopListsTest();
                // olika contentNodes per kanal (d�rmed ocks� blockid)
                
                //xmlTopListProducts = db.GetProductsXmltopList((int.Parse(ConfigurationManager.AppSettings[context.Channel.Id.ToString()])),
                                                              //context.Channel.Id);

                //IUserContext context = GetUserContext();
                int page = 1; // page best�mmer om f�rsta 100 ska visas eller andra hundra osv.
                int pageSize; // pagesize best�mmer hur m�nga som ska h�mtas �t g�ngen

                if (!int.TryParse(ConfigurationManager.AppSettings["PageSize"], out pageSize))
                {
                    pageSize = 100;
                }

                var lekmerProductService = (ILekmerProductService)IoC.Resolve<IProductService>();
                ProductCollection products = lekmerProductService.GetViewAllForExport(context, page, pageSize);
                //int totalCount = Math.Min(products.TotalCount, 200);
                int totalCount = products.TotalCount;

                // Clear the list that will hold all products
                completeList.Clear();

                while (totalCount > 0)
                {
                    Console.WriteLine("Reading product data... product count: " + totalCount);
					products = lekmerProductService.GetViewAllForExport(context, page, pageSize);
                    Copy(products);
                    //XmltoplistProducts(products);
                    totalCount = totalCount - pageSize;
                    page++;
                }
                
                ep.ExportProductsPrice(context, completeList, context.Channel.CommonName);

                //Xml top list
                //CreateAllXmlTopLists(context, xmlTopList, context.Channel.CommonName);

			//throw new NotImplementedException();
            }
		}

        private static IEnumerable<IUserContext> GetUserContextForAllChannels()
        {
            //generera 5 filer en per f�rs�ljningskanal + .com

            var channelService = (LekmerChannelService)IoC.Resolve<IChannelService>();
            Collection<IChannel> channels = channelService.GetAll();

            return channels
                .Select(channel =>
                {
                    var context = IoC.Resolve<IUserContext>();
                    context.Channel = channel;
                    return context;
                });
        }

        /*
        private static IUserContext GetUserContext()
        {
            var channelService = IoC.Resolve<IChannelService>();
            IChannel channel = channelService.GetById(1); // kolla upp, tror det �r swe

            var context = IoC.Resolve<IUserContext>();
            context.Channel = channel;
            return context;
        }*/

        public void Copy(ProductCollection pro)
        {
            foreach (IProduct t in pro)
            {
                completeList.Add(t);
            }
        }

        public void XmltoplistProducts(ProductCollection pro)
        {
            foreach (IProduct t in pro)
            {
                if (xmlTopListProducts.ContainsKey(t.Id))
                {
                    xmlTopList.Add(t);
                }
            }
        }

        public void CreateAllXmlTopLists(IUserContext context, IEnumerable<IProduct> products, string channelCommonName)
        {
            var productCategoryCollectionToplistProducts = new ProductCollection();
            // List<string> keyList = new List<string>(birdDictionary.Keys);
            var numberOfCats = xmlTopListProducts.Values.Distinct().Count();
            Console.WriteLine("numberOfCategorys " + numberOfCats);
            Console.WriteLine("---------------------------------");
            
            // f�r varje unik underkategori g� igenom hela disc f�r att f� ut all pId f�r dessa underkategorier););
            foreach (var y in xmlTopListProducts.Values.Distinct())
            {
                string value = y;
                Console.WriteLine("CategoryValue " + value);
                Console.WriteLine("---------------------------------");

                // h�mta ut alla pid per kategori och s�tt dem i en temp lista som skickas f�r att skapa xml filen
                IEnumerable<KeyValuePair<int, string>> dictval = from x in xmlTopListProducts
                              where x.Value.Contains(value)  
                              select x;

                foreach (var keyValuePair in dictval)
                {
                    foreach (var product in products)
                    {
                        if(product.Id == keyValuePair.Key)
                        {
                            productCategoryCollectionToplistProducts.Add(product);
                            break;
                        }
                    }
                    //Console.WriteLine(keyValuePair.Key);
                }        

                

                ep.ExportXmlToplistProducts(context, productCategoryCollectionToplistProducts, context.Channel.CommonName, value);
                Console.ReadKey();
            }
        }
        /*
        public void XmlTopListsTest()
        {
            //ILekmerProductService lekmerProductService = (ILekmerProductService)IoC.Resolve<IProductService>();

            Collection<IBlock> blocks = (Collection<IBlock>)IoC.Resolve<IBlockSecureService>();
            

                //(int contentNodeId)
            Console.ReadKey();
            
        }*/
	}
}

