﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace Litium.Lekmer.FileExport
{
    class Log
    {
        public string GetTempPath()
        {
            return ConfigurationManager.AppSettings["LitiumLekmerFileExportConsole"];
        }

        public void LogMessageToFile(string msg)
        {
            var sw = File.AppendText(GetTempPath() + "LitiumLekmerFileExportConsoleLog.txt");
            try
            {
                string logLine = String.Format("{0:G}: {1}.", DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            finally
            {
                sw.Close();
            }
        }
    }
}
