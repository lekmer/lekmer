using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Lekmer.Product.SecureService;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.TopList;

namespace Litium.Lekmer.PageBuilder
{
	internal class BrandBuildJob : JobBase
	{
		private readonly IBrandSecureService _brandSecureService = IoC.Resolve<IBrandSecureService>();

		private readonly IBrandSiteStructureRegistrySecureService _brandSiteStructureRegistrySecureService =
			IoC.Resolve<IBrandSiteStructureRegistrySecureService>();

		private readonly IContentPageSecureService _contentPageSecureService =
			IoC.Resolve<IContentPageSecureService>();

		private readonly IBlockProductFilterSecureService _blockProductFilterSecureService =
			IoC.Resolve<IBlockProductFilterSecureService>();

		private readonly IBlockBrandListSecureService _blockBrandListSecureService =
			IoC.Resolve<IBlockBrandListSecureService>();

		private readonly IBlockSecureService _blockSecureService =
			IoC.Resolve<IBlockSecureService>();

		private readonly IBlockRichTextSecureService _blockRichTextSecureService =
			IoC.Resolve<IBlockRichTextSecureService>();

		private readonly IBlockTopListSecureService _blockTopListSecureService =
			IoC.Resolve<IBlockTopListSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _languageId;
		private readonly int _siteStructureRegistryId;
		private int _masterPageTemplateId;
		private int? _masterPageMasterTemplateId;

		public BrandBuildJob(Configuration configuration, ISystemUserFull systemUser, int languageId,
		                     int siteStructureRegistryId)
			: base(configuration)
		{
			_systemUser = systemUser;
			_languageId = languageId;
			_siteStructureRegistryId = siteStructureRegistryId;
		}

		internal override void Initialize()
		{
			if (Configuration.BrandConfiguration == null) return;

			IContentPage masterPage = _contentPageSecureService.GetById(Configuration.BrandConfiguration.MasterPageId);
			_masterPageTemplateId = masterPage.TemplateId;
			_masterPageMasterTemplateId = masterPage.MasterTemplateId;
		}

		protected override void ExecuteCore()
		{
			if (Configuration.BrandConfiguration == null) return;

			var brands = _brandSecureService.GetAll()
				.OrderBy(brand => brand.Title);

			int ordinal = 10;
			foreach (IBrand brand in brands)
			{
				AddBrandPage(brand, Configuration.BrandConfiguration.ParentContentNodeId, ordinal);
				ordinal += 10;
			}
		}

		private void AddBrandPage(IBrand brand, int parentContentNodeId, int ordinal)
		{
			string title = brand.Title;

			IContentPage contentPage = _contentPageSecureService.Create();
			contentPage.ParentContentNodeId = parentContentNodeId;
			contentPage.ContentNodeTypeId = (int) ContentNodeTypeInfo.Page;
			contentPage.ContentNodeStatusId = (int) ContentNodeStatusInfo.Online;
			contentPage.Title = title;
			contentPage.Ordinal = ordinal;
			contentPage.CommonName = null;
			contentPage.AccessId = (int) AccessInfo.All;
			contentPage.SiteStructureRegistryId = _siteStructureRegistryId;
			contentPage.TemplateId = _masterPageTemplateId;
			contentPage.MasterTemplateId = _masterPageMasterTemplateId;
			contentPage.UrlTitle = UrlCleaner.CleanUp(title);
			contentPage.ContentPageTypeId = 1;
			contentPage.IsMaster = false;
			contentPage.MasterPageId = Configuration.BrandConfiguration.MasterPageId;

			_contentPageSecureService.Save(_systemUser, contentPage);

			SaveBrandContentNodeConnection(brand, contentPage.Id);

			AddFilterBlockToPage(contentPage.Id, brand.Id);
			AddBrandListBlockToPage(contentPage.Id, brand.Id);
			AddRichTextBlockToPage(contentPage.Id);
			AddBrandListBlock2ToPage(contentPage.Id, brand.Id);
			AddMenuBlockToPage(contentPage.Id);
			AddTopListBlockToPage(contentPage.Id);

			if (Configuration.BrandConfiguration.SubPageConfigurations != null)
			{
				foreach (BrandSubPageConfiguration subPageConfiguration in Configuration.BrandConfiguration.SubPageConfigurations)
				{
					AddSubBrandPage(contentPage.Id, brand, subPageConfiguration);
				}
			}
		}

		private void AddSubBrandPage(int contentNodeId, IBrand brand, BrandSubPageConfiguration subPageConfiguration)
		{
			string title = string.Format(subPageConfiguration.Title, brand.Title);

			IContentPage contentPage = _contentPageSecureService.Create();
			contentPage.ParentContentNodeId = contentNodeId;
			contentPage.ContentNodeTypeId = (int)ContentNodeTypeInfo.Page;
			contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Online;
			contentPage.Title = title;
			contentPage.Ordinal = subPageConfiguration.Ordinal;
			contentPage.CommonName = null;
			contentPage.AccessId = (int)AccessInfo.All;
			contentPage.SiteStructureRegistryId = _siteStructureRegistryId;
			contentPage.TemplateId = subPageConfiguration.TemplateId;
			contentPage.MasterTemplateId = subPageConfiguration.MasterTemplateId;
			contentPage.UrlTitle = UrlCleaner.CleanUp(title);
			contentPage.ContentPageTypeId = 1;
			contentPage.IsMaster = false;
			contentPage.MasterPageId = null;

			_contentPageSecureService.Save(_systemUser, contentPage);

			AddFilterBlockToPage(contentPage.Id, brand.Id, subPageConfiguration.SubPageFilterBlockTemplate);
		}

		private void AddFilterBlockToPage(int contentNodeId, int brandId)
		{
			IBlockProductFilter blockTemplate = Configuration.BrandConfiguration.FilterBlockTemplate;
			if (blockTemplate == null) return;

			AddFilterBlockToPage(contentNodeId, brandId, blockTemplate);
		}

		private void AddFilterBlockToPage(int contentNodeId, int brandId, IBlockProductFilter blockTemplate)
		{
			var blockProductFilter = ((BlockProductFilterSecureService)_blockProductFilterSecureService).Create();
			blockProductFilter.Ordinal = blockTemplate.Ordinal;
			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = blockTemplate.ContentAreaId;
			blockProductFilter.BlockTypeId = blockTemplate.BlockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Online;
			blockProductFilter.Title = blockTemplate.Title;
			blockProductFilter.TemplateId = blockTemplate.TemplateId;
			blockProductFilter.SecondaryTemplateId = blockTemplate.SecondaryTemplateId;
			blockProductFilter.DefaultBrandIds.Add(brandId);
			blockProductFilter.DefaultCategoryId = blockTemplate.DefaultCategoryId;

			_blockProductFilterSecureService.Save(_systemUser, blockProductFilter);
		}

		private void AddBrandListBlockToPage(int contentNodeId, int brandId)
		{
			IBlockBrandList blockTemplate = Configuration.BrandConfiguration.BrandListBlockTemplate;
			if (blockTemplate == null) return;

			var blockProductFilter = ((BlockBrandListSecureService)_blockBrandListSecureService).Create();
			blockProductFilter.Ordinal = blockTemplate.Ordinal;
			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = blockTemplate.ContentAreaId;
			blockProductFilter.BlockTypeId = blockTemplate.BlockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Online;
			blockProductFilter.Title = blockTemplate.Title;
			blockProductFilter.TemplateId = blockTemplate.TemplateId;
			blockProductFilter.ColumnCount = blockTemplate.ColumnCount;
			blockProductFilter.RowCount = blockTemplate.RowCount;
			blockProductFilter.IncludeAllBrands = blockTemplate.IncludeAllBrands;

			var blockBrands = new Collection<IBlockBrandListBrand>();
			var blockBrand = IoC.Resolve<IBlockBrandListBrand>();
			blockBrand.BrandId = brandId;
			blockBrand.Ordinal = 10;
			blockBrands.Add(blockBrand);

			_blockBrandListSecureService.Save(_systemUser, blockProductFilter, blockBrands);
		}

		private void AddBrandListBlock2ToPage(int contentNodeId, int brandId)
		{
			IBlockBrandList blockTemplate = Configuration.BrandConfiguration.BrandListBlock2Template;
			if (blockTemplate == null) return;

			var blockProductFilter = ((BlockBrandListSecureService)_blockBrandListSecureService).Create();
			blockProductFilter.Ordinal = blockTemplate.Ordinal;
			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = blockTemplate.ContentAreaId;
			blockProductFilter.BlockTypeId = blockTemplate.BlockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Online;
			blockProductFilter.Title = blockTemplate.Title;
			blockProductFilter.TemplateId = blockTemplate.TemplateId;
			blockProductFilter.ColumnCount = blockTemplate.ColumnCount;
			blockProductFilter.RowCount = blockTemplate.RowCount;
			blockProductFilter.IncludeAllBrands = blockTemplate.IncludeAllBrands;

			var blockBrands = new Collection<IBlockBrandListBrand>();
			var blockBrand = IoC.Resolve<IBlockBrandListBrand>();
			blockBrand.BrandId = brandId;
			blockBrand.Ordinal = 10;
			blockBrands.Add(blockBrand);

			_blockBrandListSecureService.Save(_systemUser, blockProductFilter, blockBrands);
		}

		private void AddRichTextBlockToPage(int contentNodeId)
		{
			IBlockRichText blockTemplate = Configuration.BrandConfiguration.RichTextBlockTemplate;
			if (blockTemplate == null) return;

			var blockRichText = ((BlockRichTextSecureService)_blockRichTextSecureService).Create();
			blockRichText.Ordinal = blockTemplate.Ordinal;
			blockRichText.ContentNodeId = contentNodeId;
			blockRichText.ContentAreaId = blockTemplate.ContentAreaId;
			blockRichText.BlockTypeId = blockTemplate.BlockTypeId;
			blockRichText.BlockStatusId = (int)BlockStatusInfo.Online;
			blockRichText.Title = blockTemplate.Title;
			blockRichText.TemplateId = blockTemplate.TemplateId;
			blockRichText.Content = blockTemplate.Content;

			_blockRichTextSecureService.Save(_systemUser, blockRichText);
		}

		private void AddMenuBlockToPage(int contentNodeId)
		{
			IBlock blockTemplate = Configuration.BrandConfiguration.MenuBlockTemplate;
			if (blockTemplate == null) return;

			var blockMenu = ((BlockSecureService)_blockSecureService).Create();
			blockMenu.Ordinal = blockTemplate.Ordinal;
			blockMenu.ContentNodeId = contentNodeId;
			blockMenu.ContentAreaId = blockTemplate.ContentAreaId;
			blockMenu.BlockTypeId = blockTemplate.BlockTypeId;
			blockMenu.BlockStatusId = (int)BlockStatusInfo.Online;
			blockMenu.Title = blockTemplate.Title;
			blockMenu.TemplateId = blockTemplate.TemplateId;

			_blockSecureService.Save(_systemUser, blockMenu);
		}

		private void AddTopListBlockToPage(int contentNodeId)
		{
			IBlockTopList blockTemplate = Configuration.BrandConfiguration.TopListBlockTemplate;
			if (blockTemplate == null) return;

			var blockTopList = ((BlockTopListSecureService)_blockTopListSecureService).Create();
			blockTopList.Ordinal = blockTemplate.Ordinal;
			blockTopList.ContentNodeId = contentNodeId;
			blockTopList.ContentAreaId = blockTemplate.ContentAreaId;
			blockTopList.BlockTypeId = blockTemplate.BlockTypeId;
			blockTopList.BlockStatusId = (int)BlockStatusInfo.Online;
			blockTopList.Title = blockTemplate.Title;
			blockTopList.TemplateId = blockTemplate.TemplateId;
			blockTopList.ColumnCount = blockTemplate.ColumnCount;
			blockTopList.RowCount = blockTemplate.RowCount;
			blockTopList.IncludeAllCategories = blockTemplate.IncludeAllCategories;
			blockTopList.OrderStatisticsDayCount = blockTemplate.OrderStatisticsDayCount;
			blockTopList.TotalProductCount = blockTemplate.TotalProductCount;
			

			_blockTopListSecureService.Save(_systemUser, blockTopList);
		}

		private void SaveBrandContentNodeConnection(IBrand brand, int contentNodeId)
		{
			var brandSiteStructureRegistry = _brandSiteStructureRegistrySecureService.GetAllByBrand(brand.Id)
				.FirstOrDefault(creg => creg.SiteStructureRegistryId == _siteStructureRegistryId);

			if (brandSiteStructureRegistry == null)
			{
				brandSiteStructureRegistry = _brandSiteStructureRegistrySecureService.Create();
				brandSiteStructureRegistry.BrandId = brand.Id;
				brandSiteStructureRegistry.SiteStructureRegistryId = _siteStructureRegistryId;
			}

			brandSiteStructureRegistry.ContentNodeId = contentNodeId;

			_brandSiteStructureRegistrySecureService.Save(_systemUser, brandSiteStructureRegistry);
		}
	}
}