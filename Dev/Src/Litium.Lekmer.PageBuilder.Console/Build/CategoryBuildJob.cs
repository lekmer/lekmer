using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryBuildJob : JobBase
	{
		private readonly ILekmerCategorySecureService _categorySecureService =
			(ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();

		private readonly ICategorySiteStructureRegistrySecureService _categorySiteStructureRegistrySecureService =
			IoC.Resolve<ICategorySiteStructureRegistrySecureService>();

		private readonly IContentPageSecureService _contentPageSecureService =
			IoC.Resolve<IContentPageSecureService>();

		private readonly IBlockProductFilterSecureService _blockProductFilterSecureService =
			IoC.Resolve<IBlockProductFilterSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _languageId;
		private readonly int _siteStructureRegistryId;
		private int _masterPageTemplateId;

		public CategoryBuildJob(Configuration configuration, ISystemUserFull systemUser, int languageId,
		                        int siteStructureRegistryId) : base(configuration)
		{
			_systemUser = systemUser;
			_languageId = languageId;
			_siteStructureRegistryId = siteStructureRegistryId;
		}

		internal override void Initialize()
		{
			if (Configuration.CategoryConfiguration == null) return;

			if (Configuration.CategoryConfiguration.MasterPageId.HasValue)
			{
				IContentPage masterPage = _contentPageSecureService.GetById(Configuration.CategoryConfiguration.MasterPageId.Value);
				_masterPageTemplateId = masterPage.TemplateId;
			}
			else
			{
				_masterPageTemplateId = Configuration.CategoryConfiguration.ContentNodeTemplateId;
			}
		}

		protected override void ExecuteCore()
		{
			if (Configuration.CategoryConfiguration == null) return;

			var categories = GetCategoryChildren(Configuration.CategoryConfiguration.ParentCategoryId);

			int ordinal = 10;
			foreach (ICategoryRecord category in categories)
			{
				int contentNodeId = AddCategoryPage(category, Configuration.CategoryConfiguration.ParentContentNodeId, ordinal);
				ordinal += 10;

				var subCategories = GetCategoryChildren(category.Id);
				int subOrdinal = 10;
				foreach (ICategoryRecord subCategory in subCategories)
				{
					int subContentNodeId = AddCategoryPage(subCategory, contentNodeId, subOrdinal);
					subOrdinal += 10;

					var subsubCategories = GetCategoryChildren(subCategory.Id);
					int subsubOrdinal = 10;
					foreach (ICategoryRecord subsubCategory in subsubCategories)
					{
						AddCategoryPage(subsubCategory, subContentNodeId, subsubOrdinal);
						subsubOrdinal += 10;
					}
				}
			}
		}


		private IEnumerable<ICategoryRecord> GetCategoryChildren(int? parentCategoryId)
		{
			return _categorySecureService.GetTree(parentCategoryId)
				.Where(node => node.ParentId == parentCategoryId)
				.Select(node => _categorySecureService.GetCategoryRecordById(node.Id))
				.OrderBy(category => category.Title);
		}


		private int AddCategoryPage(ICategoryRecord category, int parentContentNodeId, int ordinal)
		{
			string title = category.Title;

			var translations = _categorySecureService.GetAllTranslationsByCategory(category.Id);
			var translation = translations.FirstOrDefault(t => t.LanguageId == _languageId);
			if (translation != null && !translation.Value.IsNullOrTrimmedEmpty())
			{
				title = translation.Value;
			}

			IContentPage contentPage = _contentPageSecureService.Create();
			contentPage.ParentContentNodeId = parentContentNodeId;
			contentPage.ContentNodeTypeId = (int) ContentNodeTypeInfo.Page;
			contentPage.ContentNodeStatusId = (int) ContentNodeStatusInfo.Online;
			contentPage.Title = title;
			contentPage.Ordinal = ordinal;
			contentPage.CommonName = null;
			contentPage.AccessId = (int) AccessInfo.All;
			contentPage.SiteStructureRegistryId = _siteStructureRegistryId;
			contentPage.TemplateId = _masterPageTemplateId;
			contentPage.UrlTitle = UrlCleaner.CleanUp(title);
			contentPage.ContentPageTypeId = 1;
			contentPage.IsMaster = false;
			contentPage.MasterPageId = Configuration.CategoryConfiguration.MasterPageId;

			_contentPageSecureService.Save(_systemUser, contentPage);

			SaveCategoryContentNodeConnection(category, contentPage.Id);

			AddFilterBlockToPage(contentPage.Id, category.Id);

			return contentPage.Id;
		}

		private void AddFilterBlockToPage(int contentNodeId, int categoryId)
		{
			var blockProductFilter = ((BlockProductFilterSecureService) _blockProductFilterSecureService).Create();
			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = Configuration.CategoryConfiguration.FilterBlockAreaId;
			blockProductFilter.BlockTypeId = Configuration.CategoryConfiguration.FilterBlockTypeId;
			blockProductFilter.BlockStatusId = (int) BlockStatusInfo.Online;
			blockProductFilter.Title = Configuration.CategoryConfiguration.FilterBlockTitle;
			blockProductFilter.TemplateId = Configuration.CategoryConfiguration.FilterBlockTemplateId;
			blockProductFilter.SecondaryTemplateId = Configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId;
			blockProductFilter.DefaultCategoryId = categoryId;

			_blockProductFilterSecureService.Save(_systemUser, blockProductFilter);
		}

		private void SaveCategoryContentNodeConnection(ICategoryRecord category, int contentNodeId)
		{
			var categorySiteStructureRegistry = category.CategorySiteStructureRegistries
				.Single(creg => creg.SiteStructureRegistryId == _siteStructureRegistryId);

			categorySiteStructureRegistry.ProductParentContentNodeId = contentNodeId;

			_categorySiteStructureRegistrySecureService.Save(_systemUser, categorySiteStructureRegistry);
		}
	}
}