﻿using System;
using System.Diagnostics;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.PageBuilder
{
	internal class Program
	{
		private static void Main()
		{
			Init();

			var sw = Stopwatch.StartNew();
			IoC.Resolve<IChannelSecureService>();
			sw.Stop();
			Console.WriteLine("IoC:" + sw.ElapsedMilliseconds);

			var configurations = ConfigurationFactory.GetConfigurations();
			Console.WriteLine("?");

			while (true)
			{
				string input = Console.ReadLine() ?? string.Empty;

				sw.Reset();
				sw.Start();

				if (input.Equals("build", StringComparison.OrdinalIgnoreCase))
				{
					foreach (Configuration configuration in configurations)
					{
						var buildJob = new BuildJob(configuration);
						buildJob.Execute();
					}
				}
				else if (input.Equals("restore", StringComparison.OrdinalIgnoreCase))
				{
					foreach (Configuration configuration in configurations)
					{
						var restoreJob = new RestoreJob(configuration);
						restoreJob.Execute();
					}
				}
				else if (input.Equals("producturl", StringComparison.OrdinalIgnoreCase))
				{
						var productUrlJob = new ProductUrlJob();
						productUrlJob.Execute();
				}
				else
				{
					break;
				}

				sw.Stop();

				Console.WriteLine("Done:" + sw.ElapsedMilliseconds);
			}
		}

		private static void Init()
		{

			CacheUpdateCleaner.CacheUpdateFeed = new CacheUpdateFeed();
#if DEBUG
			CacheUpdateCleaner.WorkerCleanInterval = 1000; // Clean interval lowered in debug mode.
#endif
			CacheUpdateCleaner.StartWorker();
		}
	}
}