using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.PageBuilder
{
	internal class RestoreJob : JobBase
	{
		private readonly ISystemUserSecureService _systemUserSecureService =
			IoC.Resolve<ISystemUserSecureService>();

		private ISystemUserFull _systemUser;


		public RestoreJob(Configuration configuration) : base(configuration)
		{
		}

		protected override void ExecuteCore()
		{
			var categoryRestoreJob = new CategoryRestoreJob(Configuration, _systemUser);
			categoryRestoreJob.Execute();

			var brandRestoreJob = new BrandRestoreJob(Configuration, _systemUser);
			brandRestoreJob.Execute();
		}

		internal override void Initialize()
		{
			_systemUser = _systemUserSecureService.GetFullByUserName("admin@litium.se");
		}
	}
}