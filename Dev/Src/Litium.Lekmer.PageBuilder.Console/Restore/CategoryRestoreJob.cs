using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryRestoreJob : JobBase
	{
		private readonly ISystemUserFull _systemUser;

		private readonly IContentNodeSecureService _contentNodeSecureService =
			IoC.Resolve<IContentNodeSecureService>();

		private readonly IContentPageSecureService _contentPageSecureService =
			IoC.Resolve<IContentPageSecureService>();

		public CategoryRestoreJob(Configuration configuration, ISystemUserFull systemUser) : base(configuration)
		{
			_systemUser = systemUser;
		}

		protected override void ExecuteCore()
		{
			if (Configuration.CategoryConfiguration == null) return;

			var contentNodes = _contentNodeSecureService.GetAllByParent(Configuration.CategoryConfiguration.ParentContentNodeId);

			foreach (IContentNode contentNode in contentNodes)
			{
				_contentPageSecureService.Delete(_systemUser, contentNode.Id);
			}
		}
	}
}