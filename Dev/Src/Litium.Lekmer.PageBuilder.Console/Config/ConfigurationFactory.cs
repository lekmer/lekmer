using System.Collections.Generic;
using Litium.Lekmer.Product.BusinessObject;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal static class ConfigurationFactory
	{
		internal static IEnumerable<Configuration> GetConfigurations()
		{
			return GetHeppoConfigurations();
		}

		private static IEnumerable<Configuration> GetHeppoConfigurations()
		{
			const string systemUserName = "admin@litium.se";

			int? parentCategoryId = 1000059;
			int? categoryMasterPageId = null;
			const int categoryContentNodeTemplateId = 1000024;
			const int categoryFilterBlockAreaId = 1000010;
			const int categoryFilterBlockOrdinal = 1;
			const int categoryFilterBlockTypeId = 1000001;
			const int categoryFilterBlockTemplateId = 1000058;
			const int categoryFilterBlockSecondaryTemplateId = 1000025;
			const string categoryFilterBlockTitle = "Lista";

			/* for rhodium */
//			int? parentCategoryId = 1000458;
//			int? categoryMasterPageId = null;
//			const int categoryContentNodeTemplateId = 1000024;
//			const int categoryFilterBlockAreaId = 1000010;
//			const int categoryFilterBlockOrdinal = 1;
//			const int categoryFilterBlockTypeId = 1000001;
//			const int categoryFilterBlockTemplateId = 1000025;
//			const int categoryFilterBlockSecondaryTemplateId = 1000025;
//			const string categoryFilterBlockTitle = "Lista";

			var brandListBlockTemplate = new BlockBrandList
			                             	{
			                             		ContentAreaId = 1000018,
			                             		Ordinal = 10,
			                             		BlockTypeId = 18,
			                             		Title = "Brand profile",
			                             		TemplateId = 1000049,
			                             		ColumnCount = 1,
			                             		RowCount = 1,
			                             		IncludeAllBrands = false
			                             	};
			var brandList2BlockTemplate = new BlockBrandList
			                              	{
			                              		ContentAreaId = 1000018,
			                              		Ordinal = 30,
			                              		BlockTypeId = 18,
			                              		Title = "Brand info",
			                              		TemplateId = 1000095,
			                              		ColumnCount = 1,
			                              		RowCount = 1,
			                              		IncludeAllBrands = false
			                              	};
			var richTextBlockTemplate = new BlockRichText
			                            	{
			                            		ContentAreaId = 1000018,
			                            		Ordinal = 20,
			                            		BlockTypeId = 1,
			                            		Title = "Image",
			                            		TemplateId = 1000096,
			                            		Content = string.Empty
			                            	};
			var femaleProductFilterBlockTemplate = new BlockProductFilter
			                                       	{
			                                       		ContentAreaId = 1000010,
			                                       		Ordinal = 1,
			                                       		BlockTypeId = 1000001,
			                                       		Title = "Brand/category filter",
			                                       		TemplateId = 1000058,
			                                       		SecondaryTemplateId = null,
			                                       		DefaultCategoryId = 1000064
			                                       	};
			var maleProductFilterBlockTemplate = new BlockProductFilter
			                                     	{
			                                     		ContentAreaId = 1000010,
			                                     		Ordinal = 1,
			                                     		BlockTypeId = 1000001,
			                                     		Title = "Brand/category filter",
			                                     		TemplateId = 1000058,
			                                     		SecondaryTemplateId = null,
			                                     		DefaultCategoryId = 1000060
			                                     	};
			var femaleSubPageConfiguration = new BrandSubPageConfiguration
			                                 	{
			                                 		Ordinal = 10,
			                                 		Title = "-",
			                                 		TemplateId = 1000024,
			                                 		MasterTemplateId = 1000050,
			                                 		SubPageFilterBlockTemplate = ObjectCopier.Clone(femaleProductFilterBlockTemplate)
			                                 	};
			var maleSubPageConfiguration = new BrandSubPageConfiguration
			                               	{
			                               		Ordinal = 10,
			                               		Title = "-",
			                               		TemplateId = 1000024,
			                               		MasterTemplateId = 1000050,
			                               		SubPageFilterBlockTemplate = ObjectCopier.Clone(maleProductFilterBlockTemplate)
			                               	};


			var swedishConfiguration =
				new Configuration
					{
						ChannelId = 1,
						SystemUserName = systemUserName,
						CategoryConfiguration = null,
						BrandConfiguration =
							new BrandConfiguration
								{
									ParentContentNodeId = 1001004,
									MasterPageId = 1000057,
									BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
									BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
									RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
									SubPageConfigurations =
										new[]
											{
												ObjectCopier.Clone(femaleSubPageConfiguration)
													.Set(block => block.Title = "{0} f�r kvinnor"),
												ObjectCopier.Clone(maleSubPageConfiguration)
													.Set(block => block.Title = "{0} f�r m�n"),
											}
								}
					};

			var norweiganConfiguration =
				new Configuration
				{
					ChannelId = 4,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryId = parentCategoryId,
						ParentContentNodeId = 1002684,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					},
					BrandConfiguration = null
//						new BrandConfiguration
//						{
//							ParentContentNodeId = 1001324,
//							MasterPageId = 1001323,
//							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
//							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
//							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
//							SubPageConfigurations =
//								new[]
//											{
//												ObjectCopier.Clone(femaleSubPageConfiguration)
//													.Set(block => block.Title = "{0} for kvinner"),
//												ObjectCopier.Clone(maleSubPageConfiguration)
//													.Set(block => block.Title = "{0} for menn"),
//											}
//						}
				};

			var danishConfiguration =
				new Configuration
				{
					ChannelId = 657,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryId = parentCategoryId,
						ParentContentNodeId = 1002685,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					},
					BrandConfiguration = null
//						new BrandConfiguration
//						{
//							ParentContentNodeId = 1001637,
//							MasterPageId = 1001322,
//							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
//							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
//							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
//							SubPageConfigurations =
//								new[]
//											{
//												ObjectCopier.Clone(femaleSubPageConfiguration)
//													.Set(block => block.Title = "{0} til kvinder"),
//												ObjectCopier.Clone(maleSubPageConfiguration)
//													.Set(block => block.Title = "{0} for m�nd"),
//											}
//						}
				};

			var finnishConfiguration =
				new Configuration
				{
					ChannelId = 1000003,
					SystemUserName = systemUserName,
					CategoryConfiguration = new CategoryConfiguration
					{
						ParentCategoryId = parentCategoryId,
						ParentContentNodeId = 1002686,
						MasterPageId = categoryMasterPageId,
						ContentNodeTemplateId = categoryContentNodeTemplateId,
						FilterBlockAreaId = categoryFilterBlockAreaId,
						FilterBlockOrdinal = categoryFilterBlockOrdinal,
						FilterBlockTypeId = categoryFilterBlockTypeId,
						FilterBlockTitle = categoryFilterBlockTitle,
						FilterBlockTemplateId = categoryFilterBlockTemplateId,
						FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
					},
					BrandConfiguration = null
//						new BrandConfiguration
//						{
//							ParentContentNodeId = 1001638,
//							MasterPageId = 1001321,
//							BrandListBlockTemplate = ObjectCopier.Clone(brandListBlockTemplate),
//							BrandListBlock2Template = ObjectCopier.Clone(brandList2BlockTemplate),
//							RichTextBlockTemplate = ObjectCopier.Clone(richTextBlockTemplate),
//							SubPageConfigurations =
//								new[]
//											{
//												ObjectCopier.Clone(femaleSubPageConfiguration)
//													.Set(block => block.Title = "{0} naisten keng�t"),
//												ObjectCopier.Clone(maleSubPageConfiguration)
//													.Set(block => block.Title = "{0} miesten keng�t"),
//											}
//						}
				};

			return
				new[]
					{
						//swedishConfiguration,
						norweiganConfiguration,
						danishConfiguration,
						finnishConfiguration
					};
		}

		//private static IEnumerable<Configuration> GetLekmerConfigurations()
		//{
		//    int? parentCategoryId = null;
		//    const string systemUserName = "admin@litium.se";

		//    const int categoryFilterBlockTemplateId = 1000022;
		//    const int categoryFilterBlockSecondaryTemplateId = 1000025;
		//    const string categoryFilterBlockTitle = "Category filter";

		//    const int brandFilterBlockTemplateId = 1000022;
		//    const int brandFilterBlockSecondaryTemplateId = 1000025;
		//    const string brandFilterBlockTitle = "Brand filter";

		//    const int brandListBlockTemplateId = 1000047;
		//    const string brandListBlockTitle = "Brand info";
		//    const int brandListColumnCount = 1;
		//    const int brandListRowCount = 1;
		//    const bool brandListIncludeAllBrands = false;

		//    const int menuBlockTemplateId = 1000048;
		//    const string menuBlockTitle = "Brand menu";

		//    const int topListBlockTemplateId = 1000039;
		//    const string topListBlockTitle = "Brand top list";
		//    const int topListColumnCount = 1;
		//    const int topListRowCount = 5;
		//    const bool topListIncludeAllCategories = false;
		//    const int topListOrderStatisticsDayCount = 100;
		//    const int topListTotalProductCount = 30;

		//    return
		//        new[]
		//            {
		//                new Configuration
		//                    {
		//                        ChannelId = 1,
		//                        SystemUserName = systemUserName,
		//                        CategoryConfiguration =
		//                            new CategoryConfiguration
		//                                {
		//                                    ParentCategoryId = parentCategoryId,
		//                                    ParentContentNodeId = 1000091,
		//                                    MasterPageId = 2252,
		//                                    FilterBlockAreaId = 1000001,
		//                                    FilterBlockOrdinal = 1,
		//                                    FilterBlockTypeId = 1000001,
		//                                    FilterBlockTitle = categoryFilterBlockTitle,
		//                                    FilterBlockTemplateId = categoryFilterBlockTemplateId,
		//                                    FilterBlockSecondaryTemplateId = categoryFilterBlockSecondaryTemplateId
		//                                },
		//                        BrandConfiguration =
		//                            new BrandConfiguration
		//                                {
		//                                    ParentContentNodeId = 1000540,
		//                                    MasterPageId = 1000541,
		//                                    FilterBlockTemplate = new BlockProductFilter
		//                                                            {
		//                                                                ContentAreaId = 1000011,
		//                                                                Ordinal = 1,
		//                                                                BlockTypeId = 1000001,
		//                                                                Title = brandFilterBlockTitle,
		//                                                                TemplateId = brandFilterBlockTemplateId,
		//                                                                SecondaryTemplateId = brandFilterBlockSecondaryTemplateId
		//                                                            },
		//                                    BrandListBlockTemplate = new BlockBrandList
		//                                                                {
		//                                                                    ContentAreaId = 1000013,
		//                                                                    Ordinal = 1,
		//                                                                    BlockTypeId = 18,
		//                                                                    Title = brandListBlockTitle,
		//                                                                    TemplateId = brandListBlockTemplateId,
		//                                                                    ColumnCount = brandListColumnCount,
		//                                                                    RowCount = brandListRowCount,
		//                                                                    IncludeAllBrands = brandListIncludeAllBrands
		//                                                                },
		//                                    MenuBlockTemplate = new Block
		//                                                            {
		//                                                                ContentAreaId = 1000013,
		//                                                                Ordinal = 2,
		//                                                                BlockTypeId = 10,
		//                                                                Title = menuBlockTitle,
		//                                                                TemplateId = menuBlockTemplateId
		//                                                            },
		//                                    TopListBlockTemplate = new BlockTopList
		//                                                            {
		//                                                                ContentAreaId = 1000013,
		//                                                                Ordinal = 1,
		//                                                                BlockTypeId = 1002,
		//                                                                Title = topListBlockTitle,
		//                                                                TemplateId = topListBlockTemplateId,
		//                                                                ColumnCount = topListColumnCount,
		//                                                                RowCount = topListRowCount,
		//                                                                IncludeAllCategories = topListIncludeAllCategories,
		//                                                                OrderStatisticsDayCount = topListOrderStatisticsDayCount,
		//                                                                TotalProductCount = topListTotalProductCount
		//                                                            }
		//                                }
		//                    }
		//            };
		//}
	}
}