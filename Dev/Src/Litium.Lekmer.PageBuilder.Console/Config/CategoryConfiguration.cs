namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryConfiguration
	{
		public int ParentContentNodeId { get; set; }
		public int ContentNodeTemplateId { get; set; }
		public int? MasterPageId { get; set; }
		public int? ParentCategoryId { get; set; }

		public int FilterBlockAreaId { get; set; }
		public int FilterBlockOrdinal { get; set; }
		public int FilterBlockTypeId { get; set; }
		public string FilterBlockTitle { get; set; }
		public int FilterBlockSecondaryTemplateId { get; set; }
		public int FilterBlockTemplateId { get; set; }
	}
}