namespace Litium.Lekmer.PageBuilder
{
	internal class Configuration
	{
		public int ChannelId { get; set; }
		public string SystemUserName { get; set; }
		public CategoryConfiguration CategoryConfiguration { get; set; }
		public BrandConfiguration BrandConfiguration { get; set; }
	}
}