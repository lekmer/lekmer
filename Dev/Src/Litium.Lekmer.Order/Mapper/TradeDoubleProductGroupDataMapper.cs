﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
    public class TradeDoubleProductGroupDataMapper : DataMapperBase<ITradeDoubleProductGroup>
    {
        public TradeDoubleProductGroupDataMapper(IDataReader dataReader)
            : base(dataReader)
        {
        }

        protected override ITradeDoubleProductGroup Create()
        {
            var tradeDoubleProductGroup = IoC.Resolve<ITradeDoubleProductGroup>();
            tradeDoubleProductGroup.ProductGroupId = MapNullableValue<string>("ProductGroupId");
            return tradeDoubleProductGroup;
        }
    }
}