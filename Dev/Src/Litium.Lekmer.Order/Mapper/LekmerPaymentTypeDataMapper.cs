﻿using System.Data;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerPaymentTypeDataMapper : PaymentTypeDataMapper
	{
		public LekmerPaymentTypeDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override Scensum.Order.IPaymentType Create()
		{
			var paymentType = (ILekmerPaymentType)base.Create();
			paymentType.Cost = MapValue<decimal>("PaymentType.PaymentCost");
			return paymentType;
		}
	}
}
