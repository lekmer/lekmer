﻿using System;
using System.Data;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerOrderFullDataMapper : OrderDataMapper<IOrderFull>
	{
		public LekmerOrderFullDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IOrderFull Create()
		{
			var order = (ILekmerOrderFull)base.Create();
			order.PaymentCost = MapValue<decimal>("Lekmer.PaymentCost");
			order.CustomerIdentificationKey = MapNullableValue<string>("Lekmer.CustomerIdentificationKey");
			order.VoucherDiscount = MapNullableValue<decimal?>("Lekmer.VoucherDiscount");
			return order;
		}
	}
}
