using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerOrderItemDataMapper : OrderItemDataMapper
    {
        private DataMapperBase<IOrderItemSize> _orderItemSizeDataMapper;
		private DataMapperBase<IProductSize> _sizeDataMapper;
		private DataMapperBase<IImage> _imageDataMapper;

		public LekmerOrderItemDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
			_orderItemSizeDataMapper = DataMapperResolver.Resolve<IOrderItemSize>(DataReader);
			_sizeDataMapper = DataMapperResolver.Resolve<IProductSize>(DataReader);
			_imageDataMapper = DataMapperResolver.Resolve<IImage>(DataReader);
        }

		protected override IOrderItem Create()
		{
			var orderItem = (ILekmerOrderItem)base.Create();
			orderItem.OrderItemSize = _orderItemSizeDataMapper.MapRow();
			var sizeId = MapNullableValue<int?>("Size.Id");
			if (sizeId.HasValue)
			{
				orderItem.Size = _sizeDataMapper.MapRow();
			}
			orderItem.BrandId = MapNullableValue<int?>("BrandId");
			var imageId = MapNullableValue<int?>("Image.MediaId");
			if (imageId.HasValue)
			{
				orderItem.Image = _imageDataMapper.MapRow();
			}
			return orderItem;
        }
    }
}