using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderItemService : OrderItemService
	{
		protected OrderItemSizeRepository OrderItemSizeRepository;
		protected IBrandService BrandService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }

		public LekmerOrderItemService(OrderItemRepository orderItemRepository, OrderItemSizeRepository orderItemSizeRepository, 
			IOrderProductCampaignService orderProductCampaignService, IBrandService brandService, 
			ITagGroupService tagGroupService)
			: base(orderItemRepository, orderProductCampaignService)
		{
			OrderItemSizeRepository = orderItemSizeRepository;
			BrandService = brandService;
			TagGroupService = tagGroupService;
		}

		public override int Save(IUserContext context, IOrderItem orderItem)
		{
			var orderItemId = base.Save(context, orderItem);
			var lekmerOrderItem = (ILekmerOrderItem)orderItem;
			if (lekmerOrderItem.OrderItemSize.SizeId.HasValue)
			{
				lekmerOrderItem.OrderItemSize.OrderItemId = orderItemId;
				OrderItemSizeRepository.Save(context, lekmerOrderItem.OrderItemSize);
			}
			return orderItemId;
		}

		public override Collection<IOrderItem> GetAllByOrder(IUserContext context, int orderId)
		{
			var orderItems = Repository.GetAllByOrder(orderId);
			foreach (ILekmerOrderItem orderItem in orderItems)
			{
				if (orderItem.BrandId.HasValue)
				{
					orderItem.Brand = BrandService.GetById(context, orderItem.BrandId.Value);
				}

				orderItem.TagGroups = TagGroupService.GetAllByProduct(context, orderItem.ProductId);
			}
			return orderItems;
		}

	}
}