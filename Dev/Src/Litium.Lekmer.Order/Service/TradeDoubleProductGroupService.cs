﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
    public class TradeDoubleProductGroupService : ITradeDoubleProductGroupService
    {
        protected TradeDoublerProductGroupRepository Repository { get; private set; }

        public TradeDoubleProductGroupService(TradeDoublerProductGroupRepository repository)
        {
            Repository = repository;
        }
        public ITradeDoubleProductGroup GetProductGroupMapping(IChannel channel, int productId)
        {
            ITradeDoubleProductGroup productGroup = Repository.GetProductGroupMapping(channel.Id, productId);

            if (productGroup == null)
                productGroup = GetDefaultProductGroup(channel.Title);
                return productGroup;
        }

        //This change is based on the mail conversation with Sinan Tuncyurek and Dino
        private static ITradeDoubleProductGroup GetDefaultProductGroup(string channelTitle)
        {
            ITradeDoubleProductGroup pg = new TradeDoubleProductGroup();
            switch (channelTitle.ToLower())
            {
                case "lekmer.se":
                    pg.ProductGroupId = "2464";
                    break;
                case "lekmer.dk":
                    pg.ProductGroupId = "2464";    
                    break;
                case "lekmer.no":
                    pg.ProductGroupId = "2464";      
                    break;
                case "lekmer.fi":
                    pg.ProductGroupId = "2464";      
                    break;
                case "heppo.se":
                    pg.ProductGroupId = "2465";      
                    break;
                case "heppo.dk":
                    pg.ProductGroupId = "2465";      
                    break;
                case "heppo.no":
                    pg.ProductGroupId = "2467";      
                    break;
                case "heppo.fi":
                    pg.ProductGroupId = "2466";      
                    break;
                default:
                    pg.ProductGroupId = "notavailable";   
                    break;
            }
            return pg;
        }
    }
}
