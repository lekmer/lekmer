using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerCartItemService : ICartItemService
	{
		protected LekmerCartItemRepository Repository { get; private set; }

		public LekmerCartItemService(LekmerCartItemRepository cartItemRepository)
		{
			Repository = cartItemRepository;
		}

		public ICartItem Create()
		{
			var cartItem = IoC.Resolve<ICartItem>();
			cartItem.Status = BusinessObjectStatus.New;
			cartItem.CreatedDate = DateTime.Now;

			var product = IoC.Resolve<IProduct>();
			cartItem.Product = product;

			var priceListItem = IoC.Resolve<IPriceListItem>();
			cartItem.Product.Price = priceListItem;

			return cartItem;
		}

		public void Save(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			using (var transactedOperation = new TransactedOperation())
			{
				BaseSave(context, cartItem);
				
				var lekmerCartItem = (ILekmerCartItem)cartItem;
				var lekmerCartItemRepository = (LekmerCartItemRepository)Repository;
				lekmerCartItemRepository.Save(lekmerCartItem);

				transactedOperation.Complete();
			}
		}

		public void Delete(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");
			
			using (var transactedOperation = new TransactedOperation())
			{
				var lekmerCartItemRepository = (LekmerCartItemRepository)Repository;
				lekmerCartItemRepository.DeleteLekmerCartItem(cartItem.Id);

				BaseDelete(context, cartItem);

				transactedOperation.Complete();
			}
        }

		public Collection<ICartItem> GetAllByCart(IUserContext context, int cartId)
		{
			Collection<ICartItem> cartItems = Repository.GetAllByCart(cartId, context.Customer, context.Channel.Id);
			return cartItems ?? new Collection<ICartItem>();
		}

        private void BaseSave(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			Repository.Save(cartItem);
		}

		private void BaseDelete(IUserContext context, ICartItem cartItem)
		{
			if (cartItem == null) throw new ArgumentNullException("cartItem");

			Repository.Delete(cartItem.Id);
		}
	}
}