﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Cache;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerDeliveryMethodService : DeliveryMethodService
	{
		protected LekmerDeliveryMethodRepository _lekmerRepository;

		public LekmerDeliveryMethodService(DeliveryMethodRepository repository)
			: base(repository)
		{
			_lekmerRepository = new LekmerDeliveryMethodRepository();
		}

		public override Collection<IDeliveryMethod> GetAllByPaymentType(IUserContext context, int paymentTypeId)
		{
			return _lekmerRepository.GetAllByPaymentType(context.Channel.Id, paymentTypeId);
		}

		public override IDeliveryMethod GetById(IUserContext context, int deliveryMethodId)
		{
			return DeliveryMethodCache.Instance.TryGetItem(
				new DeliveryMethodKey(context.Channel.Id, deliveryMethodId),
				delegate { return GetByIdCore(context, deliveryMethodId); }
				);
		}

		protected IDeliveryMethod GetByIdCore(IUserContext context, int deliveryMethodId)
		{
			return Repository.GetById(context.Channel.Id, deliveryMethodId);
		}
	}
}