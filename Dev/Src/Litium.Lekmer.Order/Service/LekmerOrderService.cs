﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Transaction;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order.Contract.Service;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;
using System.Linq;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Cache;

namespace Litium.Lekmer.Order
{
    public class LekmerOrderService : OrderService, ILekmerOrderService
    {
        protected LekmerOrderRepository _lekmerRepository;
        protected IProductService _productService;
        protected IProductSizeService _productSizeService;
        protected ILekmerOrderSharedService _lekmerOrderSharedService;
        protected IVoucherService _voucherService;

        public LekmerOrderService(
            OrderRepository repository,
            IChannelService channelService,
            ICustomerService customerService,
            IOrderAddressService orderAddressService,
            IOrderPaymentService orderPaymentService,
            IOrderItemService orderItemService,
            IOrderCartCampaignService campaignService)
            : base(
                repository,
                channelService,
                customerService,
                orderAddressService,
                orderPaymentService,
                orderItemService,
                campaignService)
        {
            _lekmerRepository = IoC.Resolve<LekmerOrderRepository>();
            _productService = IoC.Resolve<IProductService>();
            _productSizeService = IoC.Resolve<IProductSizeService>();
            _lekmerOrderSharedService = IoC.Resolve<ILekmerOrderSharedService>();
            _voucherService = IoC.Resolve<IVoucherService>();
        }

        public override int Save(IUserContext context, IOrderFull order)
        {
            using (var transactedOperation = new TransactedOperation())
            {
                order.Id = base.Save(context, order);
                _lekmerOrderSharedService.Save(order);
                UpdateOrderNumber(context, order);

                transactedOperation.Complete();
            }
            return order.Id;
        }

        private void UpdateOrderNumber(IUserContext context, IOrderFull order)
        {
            order.Number = order.Id.ToString(CultureInfo.InvariantCulture);
            base.Save(context, order);
        }

        public int SaveFullExtended(IUserContext context, IOrderFull order)
        {
            int orderId;
            using (var transactedOperation = new TransactedOperation())
            {
                orderId = SaveFull(context, order);

                transactedOperation.Complete();
            }

            var lekmerUserContext = (ILekmerUserContext)context;
            var campaignInfo = (ILekmerOrderCampaignInfo)order.CampaignInfo;
            if (lekmerUserContext.Voucher != null && campaignInfo != null && campaignInfo.VoucherDiscount != null)
            {
                _voucherService.VoucherReserve(lekmerUserContext.Voucher.VoucherCode);
            }
            return orderId;
        }

        public void UpdateNumberInStock(IUserContext context, IOrderFull order)
        {
            // grouping by pair: productId, sizeId. sizeId is nullable.
            var groupedOrderItems = order.GetOrderItems().GroupBy(item => new KeyValuePair<int, int?>(item.ProductId, ((ILekmerOrderItem)item).OrderItemSize.SizeId));
            foreach (IGrouping<KeyValuePair<int, int?>, IOrderItem> groupedOrderItem in groupedOrderItems)
            {
                var productId = groupedOrderItem.Key.Key;
                var sizeId = groupedOrderItem.Key.Value;
                var quantity = groupedOrderItem.Sum(item => item.Quantity);
                if (sizeId.HasValue)
                {
                    _lekmerRepository.UpdateNumberInStock(productId, sizeId.Value, quantity);
                }
                else
                {
                    _lekmerRepository.UpdateNumberInStock(productId, quantity);
                }

                RemoveFromCache(productId);
            }
        }

        private void RemoveFromCache(int productId)
        {
            var channels = ((LekmerChannelService)ChannelService).GetAll();
            foreach (var channel in channels)
            {
                ProductCache.Instance.Remove(new ProductKey(channel.Id, productId));
                ProductViewCache.Instance.Remove(new ProductKey(channel.Id, productId));
            }
        }

        public bool CheckNumberInStock(IUserContext context, IOrderFull order)
        {
            // grouping by pair: productId, sizeId. sizeId is nullable.
            var groupedOrderItems = order.GetOrderItems().GroupBy(item => new KeyValuePair<int, int?>(item.ProductId, ((ILekmerOrderItem)item).OrderItemSize.SizeId));
            foreach (IGrouping<KeyValuePair<int, int?>, IOrderItem> groupedOrderItem in groupedOrderItems)
            {
                var quantity = groupedOrderItem.Sum(item => item.Quantity);
                var productId = groupedOrderItem.Key.Key;
                var sizeId = groupedOrderItem.Key.Value;
                if (!CheckQuantity(context, productId, sizeId, quantity)) return false;
            }
            return true;
        }

        public bool CheckNumberInStock(IUserContext context, ICartFull cart)
        {
            // grouping by pair: productId, sizeId. sizeId is nullable.
            var groupedCartItems = cart.GetCartItems().GroupBy(item => new KeyValuePair<int, int?>(item.Product.Id, ((ILekmerCartItem)item).SizeId));
            foreach (IGrouping<KeyValuePair<int, int?>, ICartItem> groupedCartItem in groupedCartItems)
            {
                var quantity = groupedCartItem.Sum(item => item.Quantity);
                var productId = groupedCartItem.Key.Key;
                var sizeId = groupedCartItem.Key.Value;
                if (!CheckQuantity(context, productId, sizeId, quantity)) return false;
            }
            return true;
        }

        public bool CheckNumberInStock(IUserContext context, ICartItem cartItem, IEnumerable<IGrouping<KeyValuePair<int, int?>, ICartItem>> groupedCartItems)
        {
            foreach (IGrouping<KeyValuePair<int, int?>, ICartItem> groupedCartItem in groupedCartItems)
            {
                var productId = groupedCartItem.Key.Key;
                if (cartItem.Product.Id == productId)
                {
                    var quantity = groupedCartItem.Sum(item => item.Quantity);
                    var sizeId = groupedCartItem.Key.Value;
                    return CheckQuantity(context, productId, sizeId, quantity);
                }
            }
            return true;
        }

        private bool CheckQuantity(IUserContext context, int productId, int? sizeId, int quantity)
        {
            var product = (ILekmerProduct)_productService.GetById(context, productId);
            var numberInStock = sizeId.HasValue
                ? _productSizeService.GetById(productId, sizeId.Value).NumberInStock
                : product.NumberInStock;
            if (numberInStock == 0 && product.IsBookable) return true;
            return numberInStock >= quantity;
        }

        public override Collection<IOrder> GetAllByCustomer(IUserContext context, int customerId)
        {
            var ordersFull = Repository.GetAllByCustomer<IOrderFull>(customerId);
            var orders = new Collection<IOrder>();
            foreach (var orderFull in ordersFull)
            {
                FillOrder(context, orderFull);
                orders.Add(orderFull);
            }
            return orders;
        }

        private void FillOrder(IUserContext context, IOrderFull order)
        {
            if (OrderItemService == null)
                throw new InvalidOperationException("OrderItemService must be set before calling FillOrder.");
            if (OrderPaymentService == null)
                throw new InvalidOperationException("OrderPaymentService must be set before calling FillOrder.");
            if (OrderAddressService == null)
                throw new InvalidOperationException("OrderAddressService must be set before calling FillOrder.");

            foreach (IOrderItem orderItem in OrderItemService.GetAllByOrder(context, order.Id))
            {
                order.AddOrderItem(orderItem);
            }
            order.MergeWith(OrderPaymentService.GetAllByOrderId(context, order.Id));
            order.MergeWith(OrderAddressService.GetById(context, order.BillingAddressId),
                            OrderAddressService.GetById(context, order.DeliveryAddressId));
            order.Customer = CustomerService.GetById(context, order.CustomerId);
        }

        public override int SaveFull(IUserContext context, IOrderFull order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            if (CustomerService == null)
                throw new InvalidOperationException("CustomerService must be set before calling Save.");
            if (OrderItemService == null)
                throw new InvalidOperationException("OrderItemService must be set before calling Save.");
            if (ChannelService == null)
                throw new InvalidOperationException("ChannelService must be set before calling Save.");
            if (OrderAddressService == null)
                throw new InvalidOperationException("OrderAddressService must be set before calling Save.");
            if (OrderPaymentService == null)
                throw new InvalidOperationException("OrderPaymentService must be set before calling Save.");

            using (var transactedOperation = new TransactedOperation())
            {
                order.CustomerId = CustomerService.Save(context, order.Customer);

                order.BillingAddressId = OrderAddressService.Save(context, order.BillingAddress);

                if (order.DeliveryAddress != null)
                {
                    order.DeliveryAddressId = OrderAddressService.Save(context, order.DeliveryAddress);
                }
                else
                {
                    order.MergeWith(order.BillingAddress, order.BillingAddress);
                    order.DeliveryAddressId = order.BillingAddressId;
                }

                order.Id = Save(context, order);

                var orderItems = order.GetOrderItems();
                foreach (IOrderItem orderItem in orderItems)
                {
                    orderItem.OrderId = order.Id;
                    orderItem.Id = OrderItemService.Save(context, orderItem);
                }
                foreach (IOrderPayment payment in order.Payments)
                {
                    payment.OrderId = order.Id;
                    payment.Id = OrderPaymentService.Save(context, payment);
                }
                if (order.CampaignInfo != null)
                {
                    OrderCartCampaignService.Save(context, order.Id, order.CampaignInfo);
                }

                transactedOperation.Complete();
            }
            return order.Id;
        }
    }
}
