﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Cache;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerPaymentTypeService : PaymentTypeService
	{
		public LekmerPaymentTypeService(PaymentTypeRepository repository) : base(repository)
		{
		}

		public override Collection<IPaymentType> GetAll(Scensum.Core.IUserContext context)
		{
			return PaymentTypeCollectionCache.Instance.TryGetItem(
				new PaymentTypeCollectionKey(context.Channel.Id),
				delegate { return GetAllCore(context); }
				);
		}

		protected Collection<IPaymentType> GetAllCore(Scensum.Core.IUserContext context)
		{
			return Repository.GetAll(context.Channel.Id);
		}
	}
}
