﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Order.Setting;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;
using Litium.Scensum.Product;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Order
{
    public class LekmerCartService : CartService, ILekmerCartService
    {
        protected LekmerCartRepository Repository { get; private set; }


        public LekmerCartService(CartRepository cartRepository, ICartItemService cartItemService, ICartCampaignProcessor cartCampaignProcessor, IProductService productService)
            : base(cartRepository, cartItemService, cartCampaignProcessor, productService)
        {
            Repository = (LekmerCartRepository)cartRepository;
        }

        public override void ApplyProductCampaigns(IUserContext context, ICartFull cart)
        {
            if (cart == null) throw new ArgumentNullException("cart");
            if (ProductService == null) throw new InvalidOperationException("ProductService cannot be null.");
            var lekmerCart = (ILekmerCartFull)cart;
            var cartItems = lekmerCart.GetCartItems();
            lekmerCart.DeleteAllItems();
            foreach (ILekmerCartItem cartItem in cartItems)
            {
                for (int i = 0; i < cartItem.Quantity; i++)
                {
                    var product = cartItem.Product;
                    product = ProductService.GetById(context, product.Id);
                    lekmerCart.AddItem(product, 1, cartItem.SizeId, cartItem.ErpId, cartItem.Id);
                }
            }
        }
           
        public void RetrivePreviousShoppingCard()
        {
            Guid shoppingCardGiud = LekmerCartCookie.GetShoppingCardGuid();
            if (Guid.Empty != shoppingCardGiud)
            {
                var cart = GetByCartGuid(UserContext.Current, shoppingCardGiud);
                if (cart != null)
                {
                    DateTime startLifeCycleDate = DateTime.Now.AddDays(-LekmerCartSetting.Instance.LifeCycleValue);
                    if (startLifeCycleDate <= cart.CreatedDate)
                    {
                        var session = IoC.Resolve<ICartSession>();
                        if (session.Cart == null)
                        {
                            session.Cart = cart;
                            return;
                        }
                    }
                }
                LekmerCartCookie.DeleteShoppingCardGuid();
            }
        }

        public void UpdateCreatedDate(ICartFull cart)
        {
            Repository.UpdateCreatedDate(cart);
        }

        /// <summary>
        /// Saves a Cart and its CartItems to the database.
        /// </summary>
        public override int Save(IUserContext context, ICartFull cart)
        {
            if (context == null) throw new ArgumentNullException("context");
            if (cart == null) throw new ArgumentNullException("cart");
            if (CartItemService == null) throw new InvalidOperationException("CartItemService must be set before calling Save.");

            using (var transactedOperation = new TransactedOperation())
            {
                if (cart.IsNew)
                {
                    cart.Id = Repository.Insert(cart);
                }
                else
                {
                    UpdateCreatedDate(cart);
                }
                var deletedCartItems = cart.GetDeletedCartItems();
                foreach (ICartItem cartItem in deletedCartItems)
                {
                    CartItemService.Delete(context, cartItem);
                } 
                var cartItems = GetGroupedCartItemsById(cart);
                foreach (ICartItem cartItem in cartItems)
                {
                    cartItem.CartId = cart.Id;
                    CartItemService.Save(context, cartItem);
                }
                cart.SetUntouched();

                context.Cart = cart;
                ApplyCampaigns(context, cart);

                if (cartItems.Count == 0)
                {
                    LekmerCartCookie.DeleteShoppingCardGuid();
                }
                else
                {
                    Guid shoppingCardGiud = LekmerCartCookie.GetShoppingCardGuid();
                    if (Guid.Empty == shoppingCardGiud || shoppingCardGiud != cart.CartGuid)
                    {
                        LekmerCartCookie.SetShoppingCardGuid(cart.CartGuid);
                    }
                }
                transactedOperation.Complete();
            }

            return cart.Id;
        }
        private static Collection<ICartItem> GetGroupedCartItemsById(ICartFull cart)
        {
            var cartItems = ObjectCopier.Clone(cart.GetCartItems());
            var groupedCartItems = cartItems.GroupBy(c => c.Id);
            var bundledCartItems = new Collection<ICartItem>();
            foreach (var groupedCartItem in groupedCartItems)
            {
                var cartItem = groupedCartItem.First();
                cartItem.Quantity = groupedCartItem.Sum(c => c.Quantity);
                bundledCartItems.Add(cartItem);
            }
            return bundledCartItems;
        }
        /// <summary>
        /// Gets a Cart by cart guid
        /// </summary>
        public override ICartFull GetByCartGuid(IUserContext context, Guid cartGuid)
        {
            if (CartItemService == null)
                throw new InvalidOperationException("CartItemService must be set before calling GetByCartGuid.");

            ICartFull cart = Repository.GetByCartGuid(cartGuid);
            if (cart == null)
                return null;
            cart.SetItems(CartItemService.GetAllByCart(context, cart.Id));

            context.Cart = cart;
            ApplyCampaigns(context, cart);

            return cart;
        }
    }
}
