﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public class LekmerOrderCampaignInfoService : ILekmerOrderCampaignInfoService
	{
		protected IOrderCartCampaignService OrderCartCampaignSecureService { get; private set; }

		public LekmerOrderCampaignInfoService(IOrderCartCampaignService orderCartCampaignSecureService)
		{
			OrderCartCampaignSecureService = orderCartCampaignSecureService;
		}

		public virtual IOrderCampaignInfo GetByOrder(int orderId)
		{
			if (OrderCartCampaignSecureService == null) throw new InvalidOperationException("OrderCartCampaignService cannot be null.");

			var orderCampaignInfo = IoC.Resolve<IOrderCampaignInfo>();
			orderCampaignInfo.Campaigns = ((ILekmerOrderCartCampaignService)OrderCartCampaignSecureService).GetAllByOrder(orderId);
			return orderCampaignInfo;
		}
	}
}
