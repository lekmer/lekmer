﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerDeliveryMethodRepository
	{
		protected virtual DataMapperBase<IDeliveryMethod> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IDeliveryMethod>(dataReader);
		}

		public virtual Collection<IDeliveryMethod> GetAllByPaymentType(int channelId, int paymentTypeId)
		{
			IDataParameter[] parameters = {
			                              	ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
			                              	ParameterHelper.CreateParameter("@PaymentTypeId", paymentTypeId, SqlDbType.Int)
			                              };

			var dbSettings = new DatabaseSetting("DeliveryMethodRepository.GetAllByPaymentMethod");

			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[order].[pDeliveryMethodGetAllByPaymentType]", parameters,
				                                                         dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}