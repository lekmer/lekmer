using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Repository
{
	[Serializable]
	public class LekmerCartItemRepository
	{
		protected DataMapperBase<ICartItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItem>(dataReader);
		}

		public void Save(ILekmerCartItem lekmerCartItem)
		{
			IDataParameter[] parameters = {
				ParameterHelper.CreateParameter("@CartItemId", lekmerCartItem.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductId", lekmerCartItem.Product.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@SizeId", lekmerCartItem.SizeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ErpId", lekmerCartItem.ErpId, SqlDbType.VarChar, 50)
			};
			var dbSettings = new DatabaseSetting("LekmerCartItemRepository.Save");
			new DataHandler().ExecuteCommand("lekmer.pLekmerCartItemSave", parameters, dbSettings);
		}

		public void DeleteLekmerCartItem(int cartItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartItemId", cartItemId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("LekmerCartItemRepository.Delete");


			new DataHandler().ExecuteCommand("[lekmer].[pLekmerCartItemDelete]", parameters, dbSettings);
		}

		public Collection<ICartItem> GetAllByCart(int cartId, ICustomer customer, int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartId", cartId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("CartItemRepository.GetAllByCart");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[order].[pCartItemGetAllByCart]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public void Save(ICartItem cartItem)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartItemId", cartItem.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CartId", cartItem.CartId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", cartItem.Product.Id, SqlDbType.Int),
                    ParameterHelper.CreateParameter("@Quantity", cartItem.Quantity, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CreatedDate", cartItem.CreatedDate, SqlDbType.DateTime),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("CartItemRepository.Save");

			cartItem.Id =new DataHandler().ExecuteCommandWithReturnValue("[order].[pCartItemSave]", parameters, dbSettings);
		}

		public void Delete(int cartItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CartItemId", cartItemId, SqlDbType.Int),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("CartItemRepository.Delete");


			new DataHandler().ExecuteCommand("[order].[pCartItemDelete]", parameters, dbSettings);
		}
	}
}