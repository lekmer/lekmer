using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerOrderRepository
	{
		public void UpdateNumberInStock(int productId, int quantity)
		{
			IDataParameter[] parameters = {
				ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdateNumberInStock");
			new DataHandler().ExecuteCommand("lekmer.pProductDecreaseNumberInStock", parameters, dbSettings);
		}

		public void UpdateNumberInStock(int productId, int sizeId, int quantity)
		{
			IDataParameter[] parameters = {
				ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@SizeId", sizeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("LekmerOrderRepository.UpdateNumberInStock");
			new DataHandler().ExecuteCommand("lekmer.pProductSizeDecreaseNumberInStock", parameters, dbSettings);
		}

		public void Save(ILekmerOrderFull order)
		{
			IDataParameter[] parameters = {
				ParameterHelper.CreateParameter("@OrderId", order.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CustomerIdentificationKey", order.CustomerIdentificationKey, SqlDbType.NChar),
				ParameterHelper.CreateParameter("@PaymentCost", order.PaymentCost, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@VoucherDiscount", order.GetActualVoucherDiscount(), SqlDbType.Decimal)
			};
			var dbSettings = new DatabaseSetting("LekmerOrderRepository.Save");
			new DataHandler().ExecuteCommand("lekmer.pOrderSave", parameters, dbSettings);
		}
	}
}