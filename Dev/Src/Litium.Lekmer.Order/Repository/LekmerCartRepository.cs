﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
    [Serializable]
    public class LekmerCartRepository : CartRepository
    {
        public void UpdateCreatedDate(ICartFull cart)
        {
            IDataParameter[] parameters = {
			    ParameterHelper.CreateParameter("@CartGuid", cart.CartGuid, SqlDbType.UniqueIdentifier),
				ParameterHelper.CreateParameter("@CreatedDate", DateTime.Now, SqlDbType.DateTime),
			};
            var dbSettings = new DatabaseSetting("LekmerCartRepository.UpdateCreatedDate");
            new DataHandler().ExecuteCommand("lekmer.pLekmerCartUpdateCreatedDate", parameters, dbSettings);
        }

    }
}
