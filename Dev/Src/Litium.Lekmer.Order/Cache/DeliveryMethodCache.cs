﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Cache
{
	public sealed class DeliveryMethodCache : ScensumCacheBase<DeliveryMethodKey, IDeliveryMethod>
	{
		private static readonly DeliveryMethodCache _instance = new DeliveryMethodCache();

		private DeliveryMethodCache()
		{
		}

		public static DeliveryMethodCache Instance
		{
			get { return _instance; }
		}
	}

	public class DeliveryMethodKey : ICacheKey
	{
		public DeliveryMethodKey(int channelId, int deliveryMethodId)
		{
			ChannelId = channelId;
			DeliveryMethodId = deliveryMethodId;
		}

		public int ChannelId { get; set; }
		public int DeliveryMethodId { get; set; }

		public string Key
		{
			get
			{
				return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + DeliveryMethodId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}