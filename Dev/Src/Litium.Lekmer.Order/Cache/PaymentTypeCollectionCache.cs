﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.Cache
{
	public sealed class PaymentTypeCollectionCache : ScensumCacheBase<PaymentTypeCollectionKey, Collection<IPaymentType>>
	{
		private static readonly PaymentTypeCollectionCache _instance = new PaymentTypeCollectionCache();

		private PaymentTypeCollectionCache()
		{
		}

		public static PaymentTypeCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class PaymentTypeCollectionKey : ICacheKey
	{
		public PaymentTypeCollectionKey(int channelId)
		{
			ChannelId = channelId;
		}

		public int ChannelId { get; set; }

		public string Key
		{
			get
			{
				return ChannelId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}
