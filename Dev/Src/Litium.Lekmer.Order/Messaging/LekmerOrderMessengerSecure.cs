﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Media;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Template.Engine;
using Litium.Scensum.Template;

namespace Litium.Lekmer.Order
{
    public class LekmerOrderMessengerSecure : ScensumMessengerBase<IOrderFull>
	{
        protected IFormatter Formatter { get; private set; }

        protected override string MessengerName
        {
            get { return "OrderConfirm"; }
        }

        public LekmerOrderMessengerSecure(IFormatter formatter)
		{
			if (formatter == null) throw new ArgumentNullException("formatter");

			Formatter = formatter;
		}

        [SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
        protected OrderConfirmEmailTemplate Template { get; set; }

        protected virtual OrderConfirmEmailTemplate GetTemplate(IChannel channel)
        {
            return new OrderConfirmEmailTemplate(channel);
        }

		protected override Message CreateMessage(IOrderFull messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");
			if (messageArgs.Customer == null) throw new ArgumentException("messageArgs.Customer can't be null.", "messageArgs");
			if (messageArgs.Customer.CustomerInformation == null) throw new ArgumentException("messageArgs.Customer.CustomerInformation can't be null.", "messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.ChannelId);
			Template = GetTemplate(channel);

			string body = RenderOrder(channel, messageArgs);

			string subject = RenderSubject(messageArgs);

			string fromName = GetContent("FromName");
			string fromEmail = GetContent("FromEmail");

			var message = new EmailMessage
			{
				Body = body,
				ProviderName = "email",
				FromName = fromName,
				FromEmail = fromEmail,
				Subject = subject,
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name =
					messageArgs.Customer.CustomerInformation.FirstName + " " +
					messageArgs.Customer.CustomerInformation.LastName,
				Address = messageArgs.Customer.CustomerInformation.Email
			});

			return message;
		}


        protected string RenderOrder(IChannel channel, IOrderFull order)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			string paymentTypeTitle = string.Empty;
			if (order.Payments.Count > 0)
			{
				IPaymentType paymentType = IoC.Resolve<IPaymentTypeSecureService>().GetAll().FirstOrDefault(pt => pt.Id == order.Payments[0].PaymentTypeId);
				if (paymentType != null)
				{
					paymentTypeTitle = paymentType.Title;
				}
			}
			fragmentContent.AddVariable("Order.PaymentTypeTitle", paymentTypeTitle);
            IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodSecureService>().GetById(order.ChannelId, order.DeliveryMethodId);
			fragmentContent.AddVariable("Order.DeliveryMethodTitle", deliveryMethod != null ? deliveryMethod.Title : string.Empty);

			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(channel, order.GetOrderItems()), VariableEncoding.None);
			//fragmentContent.AddEntity(order);
            RenderOrder(channel, fragmentContent, order);
            var orderCampaignInfo = IoC.Resolve<IOrderCampaignInfoSecureService>().GetByOrder(order.Id);
			if (orderCampaignInfo != null && orderCampaignInfo.Campaigns.Count > 0)
			{
				fragmentContent.AddVariable("Iterate:CartCampaign", RenderCartCampaigns(orderCampaignInfo.Campaigns), VariableEncoding.None);
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", true);
			}
			else
			{
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", false);
			}

			return HttpUtility.HtmlDecode(fragmentContent.Render());
		}
		protected virtual string RenderCartCampaigns(Collection<IOrderCampaign> campaigns)
		{
			if (campaigns == null) return null;

			var campaignBuilder = new StringBuilder();

			foreach (IOrderCampaign campaign in campaigns)
			{
				Fragment fragmentCartCampaign = Template.GetFragment("CartCampaign");
				fragmentCartCampaign.AddVariable("Campaign.Title", campaign.Title);
				campaignBuilder.AppendLine(fragmentCartCampaign.Render());
			}
			return campaignBuilder.ToString();
		}

		protected string RenderOrderItemList(IChannel channel, IEnumerable<IOrderItem> orderItems)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in orderItems)
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				//fragmentOrderItem.AddEntity(orderItem);
                RenderOrderItem(channel,fragmentOrderItem, orderItem);
                fragmentOrderItem.AddVariable("Channel.BaseUrlHttp", GetBaseUrlHttp(channel), VariableEncoding.None);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

        //The reason we are rendering order items by this method is using the entity mapper will force us to add OrderItemEntityMapper and 
        //ChannelService as components to IOC to be used in a bo and even create a new channel for BO which we do not want
        protected void RenderOrderItem(IChannel channel,Fragment fragment , IOrderItem orderItem)
        {
            decimal priceSummaryIncludingVat = orderItem.ActualPrice.IncludingVat * orderItem.Quantity;
            decimal priceSummaryExcludingVat = orderItem.ActualPrice.ExcludingVat * orderItem.Quantity;

            //standard order item variables
            fragment.AddVariable("OrderItem.Id", orderItem.Id.ToString(CultureInfo.InvariantCulture));
            fragment.AddVariable("OrderItem.Title", orderItem.Title);
            fragment.AddVariable(
                "OrderItem.Quantity",
                Formatter.FormatNumber(channel, orderItem.Quantity));
            fragment.AddVariable(
                "OrderItem.Price",
                Formatter.FormatPrice(channel, orderItem.ActualPrice.IncludingVat));
            fragment.AddVariable(
                "OrderItem.PriceSummary",
                Formatter.FormatPrice(channel, priceSummaryIncludingVat));
            fragment.AddVariable(
                "OrderItem.Vat",
                Formatter.FormatPrice(channel, priceSummaryIncludingVat - priceSummaryExcludingVat));

            //Lekmer specefic order item variables
            fragment.AddVariable("OrderItem.ErpId", orderItem.ErpId);
            fragment.AddVariable("OrderItem.Status", GetAliasValue(channel,"Order.Item.Status." + orderItem.OrderItemStatus.CommonName), VariableEncoding.None);

            var lekmerItem = (ILekmerOrderItem)orderItem;
            fragment.AddCondition("OrderItem.HasSize", lekmerItem.HasSize);
            if (lekmerItem.HasSize)
            {
                fragment.AddEntity(lekmerItem.Size);
            }
            fragment.AddCondition("OrderItem.HasBrand", lekmerItem.HasBrand);
            if (lekmerItem.HasBrand)
            {
                fragment.AddEntity(lekmerItem.Brand);
            }
            fragment.AddCondition("OrderItem.HasImage", lekmerItem.HasImage);
            if (lekmerItem.HasImage)
            {
                RenderImage(fragment, lekmerItem.Image);
            }
        }

        protected void RenderOrder(IChannel channel,Fragment fragment , IOrderFull order)
        {
            fragment.AddVariable("Order.Id", order.Id.ToString(CultureInfo.InvariantCulture));
            fragment.AddVariable("Order.Number", order.Number);
            fragment.AddVariable("Order.Email", order.Email);
            fragment.AddVariable("Order.IP", order.IP);
            fragment.AddVariable("Order.CreatedDate", Formatter.FormatDateTime(channel, order.CreatedDate));

            var lekmerOrder = (ILekmerOrderFull)order;
            fragment.AddVariable("Order.FreightVat", Formatter.FormatPrice(channel, lekmerOrder.GetActualFreightCostVat()));
            fragment.AddVariable("Order.PaymentCost", Formatter.FormatPrice(channel, lekmerOrder.PaymentCost));
            fragment.AddVariable("Order.PaymentVat", Formatter.FormatPrice(channel, lekmerOrder.GetPaymentCostVat()));
            fragment.AddVariable("Order.TotalCostIncludingVat", Formatter.FormatPrice(channel, lekmerOrder.GetActualPriceTotal().IncludingVat));
            fragment.AddVariable("Order.TotalCostExcludingVat", Formatter.FormatPrice(channel, lekmerOrder.GetActualPriceTotal().ExcludingVat));
            fragment.AddVariable("Order.TotalVat", Formatter.FormatPrice(channel, lekmerOrder.GetActualVatTotal()));
            fragment.AddVariable("Order.TotalQuantity", Formatter.FormatNumber(channel, lekmerOrder.GetOrderItems().Sum(i => i.Quantity)));
            fragment.AddVariable("Order.Status", GetAliasValue(channel,"Order.Status." + lekmerOrder.OrderStatus.CommonName), VariableEncoding.None);

        }

        protected void RenderImage(Fragment fragment, IImage item)
        {
            const string imageUrlMatchPattern = @"\[Image.Url\(""(.*?)""\)\]";
            const int imageUrlMatchCommonNamePosition = 1;

            fragment.AddVariable("Image.Link", ((ILekmerImage)item).Link);
            fragment.AddVariable("Image.Parameter", ((ILekmerImage)item).Parameter);
            fragment.AddCondition("Image.HasTumbnailImageUrl", !string.IsNullOrEmpty(((ILekmerImage)item).TumbnailImageUrl));
            fragment.AddVariable("Image.TumbnailImageUrl", ((ILekmerImage)item).TumbnailImageUrl);
            fragment.AddCondition("Image.IsLink", ((ILekmerImage)item).IsLink);
            fragment.AddCondition("Image.HasImage", ((ILekmerImage)item).HasImage);

            fragment.AddVariable("Image.Id", item.Id.ToString(CultureInfo.InvariantCulture));
            fragment.AddVariable("Image.Title", item.Title);
            fragment.AddVariable("Image.AlternativeText", item.AlternativeText);

            fragment.AddVariable("Image.Url", UrlHelper.ResolveUrl(string.Format(
                                                                    CultureInfo.InvariantCulture, "{0}{1}/{2}.{3}",
                                                                    WebSetting.Instance.MediaUrl, item.Id,
                                                                    UrlCleaner.CleanUp(item.Title), item.FormatExtension)));

            fragment.AddRegexVariable(
                imageUrlMatchPattern,
                delegate(Match match)
                {
                    string commonName = match.Groups[imageUrlMatchCommonNamePosition].Value;

                    string imageUrl = string.Format(
                        CultureInfo.InvariantCulture, "{0}{1}/{2}/{3}.{4}",
                        WebSetting.Instance.MediaUrl, item.Id, commonName, UrlCleaner.CleanUp(item.Title), item.FormatExtension);

                    return UrlHelper.ResolveUrl(imageUrl);
                },
                VariableEncoding.HtmlEncodeLight,
                RegexOptions.Compiled);
        }


        private static string GetAliasValue(IChannel channel,string commonName)
        {
            if (commonName == null) throw new ArgumentNullException("commonName");

            var aliasService = IoC.Resolve<IAliasSharedService>();
            string value = aliasService.GetValueEncodedByCommonName(channel, commonName);
            return value ?? string.Format(CultureInfo.InvariantCulture, "[ Alias '{0}' could not be found. ]", commonName);
        }

        private static string GetBaseUrlHttp(IChannel channel)
        {
            return "http://" + channel.ApplicationName;
        }

        protected string RenderSubject(IOrderFull order)
		{
            Fragment fragmentSubject = Template.GetFragment("Subject");
            fragmentSubject.AddVariable("Order.Number", order.Number, VariableEncoding.None);
            return fragmentSubject.Render();
        }

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}
