﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.Order.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Template.Engine;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderMessenger : OrderMessenger
	{
		public LekmerOrderMessenger(IFormatter formatter, ICountryService countryService)
			: base(formatter, countryService)
		{
		}

		protected override Message CreateMessage(IOrderFull messageArgs)
		{
			if (messageArgs == null) throw new ArgumentNullException("messageArgs");
			if (messageArgs.Customer == null) throw new ArgumentException("messageArgs.Customer can't be null.", "messageArgs");
			if (messageArgs.Customer.CustomerInformation == null) throw new ArgumentException("messageArgs.Customer.CustomerInformation can't be null.", "messageArgs");

			var channelServise = IoC.Resolve<IChannelSharedService>();
			IChannel channel = channelServise.GetById(messageArgs.ChannelId);
			Template = GetTemplate(channel);

			string body = RenderOrder(channel, messageArgs);

			string subject = RenderSubject(messageArgs);

			string fromName = GetContent("FromName");
			string fromEmail = GetContent("FromEmail");

			var message = new EmailMessage
			{
				Body = body,
				ProviderName = "email",
				FromName = fromName,
				FromEmail = fromEmail,
				Subject = subject,
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name =
					messageArgs.Customer.CustomerInformation.FirstName + " " +
					messageArgs.Customer.CustomerInformation.LastName,
				Address = messageArgs.Customer.CustomerInformation.Email
			});

			return message;
		}

		protected override string RenderOrder(IChannel channel, IOrderFull order)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			string paymentTypeTitle = string.Empty;
			if (order.Payments.Count > 0)
			{
				IPaymentType paymentType = IoC.Resolve<IPaymentTypeService>().GetAll(UserContext.Current).FirstOrDefault(pt => pt.Id == order.Payments[0].PaymentTypeId);
				if (paymentType != null)
				{
					paymentTypeTitle = paymentType.Title;
				}
			}
			fragmentContent.AddVariable("Order.PaymentTypeTitle", paymentTypeTitle);
			IDeliveryMethod deliveryMethod = IoC.Resolve<IDeliveryMethodService>().GetById(UserContext.Current, order.DeliveryMethodId);
			fragmentContent.AddVariable("Order.DeliveryMethodTitle", deliveryMethod != null ? deliveryMethod.Title : string.Empty);

			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(channel, order.GetOrderItems()), VariableEncoding.None);
			fragmentContent.AddEntity(order);

			var orderCampaignInfo = IoC.Resolve<ILekmerOrderCampaignInfoService>().GetByOrder(order.Id);
			if (orderCampaignInfo != null && orderCampaignInfo.Campaigns.Count > 0)
			{
				fragmentContent.AddVariable("Iterate:CartCampaign", RenderCartCampaigns(orderCampaignInfo.Campaigns), VariableEncoding.None);
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", true);
			}
			else
			{
				fragmentContent.AddCondition("CartCampaignsHasBeenApplied", false);
			}

			return HttpUtility.HtmlDecode(fragmentContent.Render());
		}
		protected virtual string RenderCartCampaigns(Collection<IOrderCampaign> campaigns)
		{
			if (campaigns == null) return null;

			var campaignBuilder = new StringBuilder();

			foreach (IOrderCampaign campaign in campaigns)
			{
				Fragment fragmentCartCampaign = Template.GetFragment("CartCampaign");
				fragmentCartCampaign.AddVariable("Campaign.Title", campaign.Title);
				campaignBuilder.AppendLine(fragmentCartCampaign.Render());
			}
			return campaignBuilder.ToString();
		}

		protected override string RenderOrderItemList(IChannel channel, IEnumerable<IOrderItem> orderItems)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in orderItems)
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddEntity(orderItem);
				fragmentOrderItem.AddVariable("Channel.BaseUrlHttp", UrlHelper.BaseUrlHttp, VariableEncoding.None);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

		protected override string RenderSubject(IOrderFull order)
		{
			return HttpUtility.HtmlDecode(base.RenderSubject(order));
		}

		private string GetContent(string name)
		{
			return HttpUtility.HtmlDecode(Template.GetFragment(name).Render());
		}
	}
}
