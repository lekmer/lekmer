﻿using System;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerPaymentType : PaymentType, ILekmerPaymentType
	{
		private decimal _cost;

		public decimal Cost
		{
			get
			{
				return _cost;
			}
			set 
			{
				CheckChanged(_cost, value);
				_cost = value;
			}
		}
	}
}
