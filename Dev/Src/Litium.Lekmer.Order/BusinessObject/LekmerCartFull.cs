﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerCartFull : CartFull, ILekmerCartFull
	{
		private ICartItemService _cartItemService;

		public LekmerCartFull(ICartItemService cartItemService)
		{
			_cartItemService = cartItemService;
		}

		public override Price GetActualPriceSummary()
		{
			var priceSummary = base.GetActualPriceSummary();
			if (CampaignInfo != null)
			{
				var lekmerCartCampaignInfo = (LekmerCartCampaignInfo)CampaignInfo;
				priceSummary = ApplyVoucherDiscount(lekmerCartCampaignInfo, priceSummary);
			}
			return priceSummary;
		}

		private static Price ApplyVoucherDiscount(ILekmerCartCampaignInfo lekmerCartCampaignInfo, Price priceSummary)
		{
			var discount = lekmerCartCampaignInfo.VoucherDiscount;
			if (!discount.HasValue || priceSummary.ExcludingVat == 0)
			{
				return priceSummary;
			}
			if (lekmerCartCampaignInfo.VoucherDiscountType == VoucherValueType.Price)
			{
				decimal priceIncludingVat = priceSummary.IncludingVat;
				decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;
				priceIncludingVat -= discount.Value;
				if (priceIncludingVat < 0)
				{
					return new Price(0, 0);
				}
				var priceExcludingVat = priceIncludingVat / (1 + vatPercentage / 100);
				return new Price(priceIncludingVat, priceExcludingVat);
			}
			if (lekmerCartCampaignInfo.VoucherDiscountType == VoucherValueType.Percentage)
			{
				if (discount.Value >= 100)
				{
					return new Price(0, 0);
				}
				decimal priceIncludingVat = priceSummary.IncludingVat;
				decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;
				priceIncludingVat = priceIncludingVat * (1 - discount.Value / 100);
				var priceExcludingVat = priceIncludingVat / (1 + vatPercentage / 100);
				return new Price(priceIncludingVat, priceExcludingVat);
			}
			throw new NotSupportedException("VoucherValueType '" + lekmerCartCampaignInfo.VoucherDiscountType + "' not supported.");
		}

		/// <summary>
		/// Changes the quantity of the product in the cart (adds or subtracts 'quantity' from its current quantity).
		/// If the product does not exist in the cart yet, it will be added.
		/// If the product has new size and cart already has same product with old size,
		/// then cartItem with old size will be delete.
		/// </summary>
		/// <param name="product"></param>
		/// <param name="quantity"></param>
		/// <param name="sizeId"></param>
		/// <param name="erpId"></param>
		public void ChangeItemQuantity(IProduct product, int quantity, int? sizeId, string erpId)
		{
			if (product == null) throw new ArgumentNullException("product");
			if (quantity < 0) throw new ArgumentOutOfRangeException("quantity", "Quantity must be greater than or equal to zero.");

			//Restriction: Cart can has only one size for product 
			DeleteProductWithOldSize(product.Id, sizeId);

			var cartItem = FindCartItemByProduct(product.Id, product.CampaignInfo.Price.IncludingVat);
			if (cartItem != null)
			{
				cartItem.ChangeQuantity(quantity);
			}
			else if (quantity > 0)
			{
				AddItem(product, quantity, sizeId, erpId,0);
			}
		}
		/// <summary>
		/// Changes the quantity of the product in the cart (adds or subtracts 'quantity' from its current quantity).
		/// If the product does not exist in the cart yet, it will be added.
		/// </summary>
		/// <param name="product"></param>
		/// <param name="quantity"></param>
		/// <param name="sizeId"></param>
		/// <param name="erpId"></param>
		public void ChangeItemQuantityForBlockExtendedCartControl(IProduct product, int quantity, int? sizeId, string erpId)
		{
			if (product == null) throw new ArgumentNullException("product");

			var cartItem = FindCartItemByProduct(product.Id, product.CampaignInfo.Price.IncludingVat);
			if (cartItem != null)
			{
				cartItem.ChangeQuantity(quantity);
			}
			else if (quantity > 0)
			{
				AddItem(product, quantity, sizeId, erpId, 0);
			}
		}

		private void DeleteProductWithOldSize(int productId, int? sizeId)
		{
			var cartItems = GetCartItems();
		
				foreach (var cartItem in cartItems)
				{
					if(cartItem.Product.Id==productId &&((ILekmerCartItem)cartItem).SizeId != sizeId)
					{//Delete cart with old size
						cartItem.Delete();
					}
				}
		}

		public void AddItem(IProduct product, int quantity, int? sizeId, string erpId, int cartItemId)
		{
			if (product == null) throw new ArgumentNullException("product");
			if (quantity < 0) throw new ArgumentOutOfRangeException("quantity", "Quantity must be greater than or equal to zero.");

			var item = (ILekmerCartItem)_cartItemService.Create();
			item.CartId = Id;
			item.Product = product;
			item.Quantity = quantity;
			item.SizeId = sizeId;
			item.ErpId = erpId;
			item.Id = cartItemId;
			AddItem(item);
		}
	}
}
