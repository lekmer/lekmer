using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderFull : OrderFull, ILekmerOrderFull
	{
		private decimal _paymentCost;
		private decimal? _voucherDiscount;
              
		public decimal PaymentCost
		{
			get { return _paymentCost; }
			set
			{
				CheckChanged(_paymentCost, value);
				_paymentCost = value;
			}
		}

		private string _customerIdentificationKey;
       
		public string CustomerIdentificationKey
		{
			get { return _customerIdentificationKey; }
			set
			{
				CheckChanged(_customerIdentificationKey, value);
				_customerIdentificationKey = value;
			}
		}
		

		public decimal? VoucherDiscount
		{
			get { return _voucherDiscount; }
			set
			{
				CheckChanged(_voucherDiscount, value);
				_voucherDiscount = value;
			}
		}

		public decimal? GetActualVoucherDiscount()
		{
			if (CampaignInfo != null && ((LekmerOrderCampaignInfo)CampaignInfo).VoucherDiscount.HasValue)
			{
				return ((LekmerOrderCampaignInfo)CampaignInfo).VoucherDiscount.Value;
			}
			if (VoucherDiscount.HasValue)
			{
				return VoucherDiscount.Value;
			}
			return null;
		}

		public decimal GetActualFreightCostVat()
		{
			return GetActualFreightCost() * GetOrderItemsAverageVat();
		}

		public decimal GetFreightCostVat()
		{
			return FreightCost * GetOrderItemsAverageVat();
		}

		public decimal GetPaymentCostVat()
		{
			return PaymentCost * GetOrderItemsAverageVat();
		}

		public override Price GetActualPriceSummary()
		{
			var priceSummary = base.GetActualPriceSummary();
			priceSummary = ApplyVoucherDiscount(priceSummary);
			return priceSummary;
		}

		private Price ApplyVoucherDiscount(Price priceSummary)
		{
			decimal? voucherDiscount = GetActualVoucherDiscount();
			if (!voucherDiscount.HasValue || voucherDiscount == 0)
			{
				return priceSummary;
			}
			decimal priceIncludingVat = priceSummary.IncludingVat;
			priceIncludingVat -= voucherDiscount.Value;
			if (priceIncludingVat < 0)
			{
				return new Price(0, 0);
			}
			decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;
			var priceExcludingVat = priceIncludingVat / (1 + vatPercentage);
			return new Price(priceIncludingVat, priceExcludingVat);
		}

		/// <summary>
		/// Gets the total price including order items price summary, freight cost and payment cost.
		/// </summary>
		public Price GetPriceTotal()
		{
			var priceIncludingVat = GetActualPriceSummary().IncludingVat + FreightCost + PaymentCost;
			var priceExcludingVat = priceIncludingVat - GetVatTotal();
			return new Price(priceIncludingVat, priceExcludingVat);
		}

		/// <summary>
		/// Gets the total price including order items price summary, freight cost and payment cost (taking into account campaigns).
		/// </summary>
		public Price GetActualPriceTotal()
		{
			var priceIncludingVat = GetActualPriceSummary().IncludingVat + GetActualFreightCost() + PaymentCost;
			var priceExcludingVat = priceIncludingVat - GetActualVatTotal();
			return new Price(priceIncludingVat, priceExcludingVat);
		}

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT.
		/// </summary>
		public decimal GetVatTotal()
		{
			return GetActualVatSummary() + GetFreightCostVat() + GetPaymentCostVat();
		}

		/// <summary>
		/// Gets the total VAT including order items VAT summary, freight cost VAT and payment cost VAT (taking into account campaigns).
		/// </summary>
		public decimal GetActualVatTotal()
		{
			return GetActualVatSummary() + GetActualFreightCostVat() + GetPaymentCostVat();
		}

		public void MergeWith(IUserContext context, Collection<IOrderPayment> payments)
		{
			var paymentTypes = IoC.Resolve<IPaymentTypeService>().GetAll(context);
			foreach (IOrderPayment payment in payments)
			{
				Payments.Add(payment);
				
				var paymentTypeId = payment.PaymentTypeId;
				var paymentType = (ILekmerPaymentType)paymentTypes.First(t => t.Id == paymentTypeId);
				PaymentCost += paymentType.Cost;
			}
		}
		public override void MergeWith(IUserContext context, ICartCampaignInfo cartCampaignInfo)
		{
			base.MergeWith(context, cartCampaignInfo);

			var lekmerCartCampaignInfo = (ILekmerCartCampaignInfo)cartCampaignInfo;
			if (lekmerCartCampaignInfo.VoucherDiscount.HasValue)
			{
				var voucherDiscountValue = lekmerCartCampaignInfo.VoucherDiscount.Value;
				if (lekmerCartCampaignInfo.VoucherDiscountType == VoucherValueType.Price)
				{
					((LekmerOrderCampaignInfo)CampaignInfo).VoucherDiscount = voucherDiscountValue;
				}
				else if (lekmerCartCampaignInfo.VoucherDiscountType == VoucherValueType.Percentage)
				{
					var priceSummary = GetActualPriceSummary();
					decimal priceIncludingVat = priceSummary.IncludingVat;
					((LekmerOrderCampaignInfo)CampaignInfo).VoucherDiscount = priceIncludingVat * voucherDiscountValue / 100;
				}
				else
				{
					throw new NotSupportedException("VoucherValueType '" + lekmerCartCampaignInfo.VoucherDiscountType + "' not supported.");
				}
			}
		}

		public override IOrderItem CreateOrderItem()
		{
			var orderItem = base.CreateOrderItem();
			var lekmerOrderItem = (ILekmerOrderItem)orderItem;
			lekmerOrderItem.OrderItemSize = IoC.Resolve<IOrderItemSize>();
			lekmerOrderItem.OrderItemSize.OrderItemId = lekmerOrderItem.Id;
			return lekmerOrderItem;
		}

		private decimal GetOrderItemsAverageVat()
		{
			var orderItems = GetOrderItems();
			if (orderItems.Count == 0)
			{
				return 0;
			}
			var orderItemsPrice = orderItems.Sum(item => item.ActualPrice.IncludingVat);
			if (orderItemsPrice == 0)
			{
				return 0;
			}
			var orderItemsVatAmount = orderItems.Sum(item => item.GetActualVatAmount());
			return orderItemsVatAmount / orderItemsPrice;
		}
		
		public override void MergeWith(Scensum.Customer.ICustomer customer)
		{
			if (customer.CustomerInformation == null)
			{
				throw new ArgumentException("Argument is not valid: customer. No customer information.");
			}

			if (customer.CustomerInformation.DefaultBillingAddress == null)
			{
				throw new ArgumentException("Argument is not valid: DefaultBillingAddress. No default billing address.");
			}

			IOrderAddress deliveryAddress = null;
			IOrderAddress billingAddress = null;

			Customer = customer;

			if (customer.CustomerInformation.DefaultBillingAddress != null)
			{
				billingAddress = Convert(customer.CustomerInformation.DefaultBillingAddress);
			}

			if (customer.CustomerInformation.DefaultDeliveryAddress != null &&
				customer.CustomerInformation.DefaultDeliveryAddress != customer.CustomerInformation.DefaultBillingAddress)
			{
				deliveryAddress = Convert(customer.CustomerInformation.DefaultDeliveryAddress);
			}
			
			MergeWith(billingAddress, deliveryAddress);
			Email = customer.CustomerInformation.Email;
		}
	}
}