using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderItem : OrderItem, ILekmerOrderItem
	{
		public IOrderItemSize OrderItemSize { get; set; }
		public IProductSize Size { get; set; }
		public int? BrandId { get; set; }
		public IBrand Brand { get; set; }
		public IImage Image { get; set; }
		public Collection<ITagGroup> TagGroups { get; set; }

		public bool HasSize
		{
			get { return Size != null; }
		}

		public bool HasBrand
		{
			get { return Brand != null; }
		}

		public bool HasImage
		{
			get { return Image != null; }
		}

		public override void MergeWith(IUserContext context, ICartItem cartItem)
		{
			base.MergeWith(context, cartItem);
			var lekmerCartItem = (ILekmerCartItem)cartItem;
			OrderItemSize.ErpId = lekmerCartItem.ErpId;
			OrderItemSize.SizeId = lekmerCartItem.SizeId;
		}

        public decimal RowDiscount(bool isDiscountIncludingVat)
        {
            decimal discount;
            if (isDiscountIncludingVat)
            {
                discount = OriginalPrice.IncludingVat - ActualPrice.IncludingVat;
            }
            else
            {
                discount = OriginalPrice.ExcludingVat - ActualPrice.ExcludingVat;            
            }
            if (discount < 0)
                discount = 0;
            return discount;
        }

        public double RowDiscountInProcent(bool isDiscountIncludingVat)
        {
            double discount;
            
            double discountProcent;
            if (isDiscountIncludingVat)
            {
                discount = (double)(OriginalPrice.IncludingVat - ActualPrice.IncludingVat);
                if (discount < 0)
                discount = 0;
                discountProcent = (discount * 100) / (double)OriginalPrice.IncludingVat;
            }
            else
            {
                discount = (double)(OriginalPrice.ExcludingVat - ActualPrice.ExcludingVat);
                if (discount < 0)
                    discount = 0;
                discountProcent = (discount * 100) / (double)OriginalPrice.ExcludingVat;
            }
            return discountProcent;
        }
    }
}