using System;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerCartItem : CartItem, ILekmerCartItem
	{
		public int? SizeId { get; set; }
		public string ErpId { get; set; }
	}
}