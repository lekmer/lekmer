using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderSecureService : OrderSecureService, ILekmerOrderSecureService
	{
		protected LekmerOrderRepository _lekmerRepository;
		protected ILekmerOrderSharedService _lekmerOrderSharedService;

		public LekmerOrderSecureService(
			IAccessValidator accessValidator,
			OrderRepository repository,
			IOrderAddressSecureService orderAddressSecureService,
			IOrderItemSecureService orderItemSecureService,
			IOrderPaymentSecureService orderPaymentSecureService,
			IOrderCampaignInfoSecureService orderCampaignInfoSecureService,
			IOrderCommentSecureService orderCommentSecureService,
			IOrderAuditSecureService orderAuditSecureService,
			ICustomerSecureService customerSecureService,
			IOrderStatusSecureService orderStatusSecureService)
		: base(
			accessValidator,
			repository,
			orderAddressSecureService,
			orderItemSecureService,
			orderPaymentSecureService,
			orderCampaignInfoSecureService,
			orderCommentSecureService,
			orderAuditSecureService,
			customerSecureService,
			orderStatusSecureService)
		{
			_lekmerRepository = IoC.Resolve<LekmerOrderRepository>();
			_lekmerOrderSharedService = IoC.Resolve<ILekmerOrderSharedService>();
		}

		public override int Save(
			ISystemUserFull systemUserFull,
			IOrderFull order,
			Collection<IOrderComment> orderComments,
			IOrderAddress billingAddress,
			IOrderAddress deliveryAddress,
			Collection<IOrderAudit> orderAudits,
			Collection<IOrderPayment> orderPayments)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				order.Id = base.Save(systemUserFull, order, orderComments, billingAddress, deliveryAddress, orderAudits, orderPayments);
				_lekmerOrderSharedService.Save(order);
				UpdateOrderNumber(order);

				transactedOperation.Complete();
			}
			return order.Id;
		}

		private void UpdateOrderNumber(IOrderFull order)
		{
			order.Number = order.Id.ToString(CultureInfo.InvariantCulture);
			Repository.Save(order);
		}

		public bool CanChangePaymentCost(IOrderFull order, decimal previousPaymentCost)
		{
			return true;
		}

        public override void SendConfirm(IOrderFull order)
        {
            if (order == null) throw new ArgumentNullException("order");

            IoC.Resolve<LekmerOrderMessengerSecure>().Send(order);
        }

	}
}