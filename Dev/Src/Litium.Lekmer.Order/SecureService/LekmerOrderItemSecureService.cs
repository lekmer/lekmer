using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderItemSecureService : OrderItemSecureService
	{
		protected OrderItemSizeRepository _orderItemSizeRepository;

		public LekmerOrderItemSecureService(
			IAccessValidator accessValidator,
			OrderItemRepository repository,
			IOrderItemCampaignInfoSecureService orderItemCampaignInfoSecureService,
			IOrderProductCampaignSecureService orderProductCampaignSecureService)
		: base(
			accessValidator,
			repository,
			orderItemCampaignInfoSecureService,
			orderProductCampaignSecureService
		)
		{
			_orderItemSizeRepository = IoC.Resolve<OrderItemSizeRepository>();
		}

		public override void Delete(ISystemUserFull systemUserFull, IOrderItem orderItem)
		{
			if (orderItem == null) throw new ArgumentNullException("orderItem");

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				_orderItemSizeRepository.Delete(((ILekmerOrderItem) orderItem).OrderItemSize);
				base.Delete(systemUserFull, orderItem);

				transactedOperation.Complete();
			}
		}
	}
}