﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;


namespace Litium.Lekmer.Payment.Klarna{

    public class Program
    {
        private static readonly Dictionary<string, List<string>> _klarnaMonthDictionary = new Dictionary<string, List<string>>();
        private static readonly Dictionary<string, List<string>> _klarnaPclassDictionary = new Dictionary<string, List<string>>();

        //This enum contains all the Constants that will be handled by the file.
        private enum PClassConstants
        {
            PClassId = 0,
            Description = 1,
            NoOfMonths = 2,
            OneTimeFee = 3,
            MonthlyFee = 4,
            Rate = 5,
            MinimumTotalCost = 6,
            Country = 7,
            Type = 8,
            PClassExpiryDate = 9
        }

        static void Main(string[] args)
        {
            //Get xml paths
            var settingXmlFilePathTemplate = ConfigurationManager.AppSettings["SettingXmlFilePathTemplate"];
            var settingXmlBackupFilePathTemplate = ConfigurationManager.AppSettings["SettingXmlBackupFilePathTemplate"];

            var pClassesXmlPath = string.Format(settingXmlFilePathTemplate, KlarnaPClassSetting.GetStorageName());
            var klarnaXmlPath = string.Format(settingXmlFilePathTemplate, KlarnaSetting.GetStorageName());

            var pClassesXmlBackupPath = string.Format(settingXmlBackupFilePathTemplate, KlarnaPClassSetting.GetStorageName());
            var klarnaXmlBackupPath = string.Format(settingXmlBackupFilePathTemplate, KlarnaSetting.GetStorageName());


            var date = DateTime.Now.ToShortDateString().Replace("/", "");

            //backup current file
            File.Copy(pClassesXmlPath, pClassesXmlBackupPath + "_" + date + ".bak", true);
            File.Copy(klarnaXmlPath, klarnaXmlBackupPath + "_" + date + ".bak", true);

            try
            {
                var doc = new XmlDocument();

                var declaration = doc.CreateXmlDeclaration("1.0", "utf-8", string.Empty);
                doc.AppendChild(declaration);

                var element = doc.CreateElement("Root");
                doc.AppendChild(element);

                //Get the Pclasses
                var channelService = (LekmerChannelService) IoC.Resolve<IChannelService>();
                var channels = channelService.GetAll();
                foreach (var channel in channels)
                {
                    if (channel.CommonName == "Europe")
                        continue;

                    IKreditorResponseItem response = new KlarnaApi(channel).Fetch_Pclasses();
                    //Loop through the PClasses from Klarna and check if this Pclass exists in local xml
                    foreach (object[] item in response.PClasses)
                    {
                        //Does not exist, create
                        CreatePClassGroup(item, doc, channel);
                    }
                }
                doc.Save(pClassesXmlPath);

                //Update klarna.xml
                doc = new XmlDocument();
                doc.Load(klarnaXmlPath);
                foreach (var channel in channels)
                {
                    var pClassNode = doc.SelectSingleNode("/Root/Group[@Name='" + channel.CommonName + "']/Setting[@Name='PClassIds']");
                    var monthNode = doc.SelectSingleNode("/Root/Group[@Name='" + channel.CommonName + "']/Setting[@Name='PartPaymentMonths']");

                    if (pClassNode != null)
                        pClassNode.InnerText = string.Join(",", _klarnaPclassDictionary[channel.CommonName].ToArray());

                    if (monthNode !=null)
                        monthNode.InnerText = string.Join(",", _klarnaMonthDictionary[channel.CommonName].ToArray());
                }

                doc.Save(klarnaXmlPath);
            }
            catch (Exception e)
            {
                File.Copy(pClassesXmlBackupPath + "_" + date + ".bak",pClassesXmlPath, true);
                File.Copy(klarnaXmlBackupPath + "_" + date + ".bak",klarnaXmlPath, true);

                Console.WriteLine("Failed to update klarna - " + e.Message);
                Console.WriteLine("Press key to exit");
                Console.Read();
            }

        }


        private static void CreatePClassGroup(object[] items, XmlDocument doc, IChannel channel)
        {
            //create group
            XmlElement group = doc.CreateElement("Group");
            XmlAttribute nameAttr = doc.CreateAttribute("Name");
            nameAttr.Value = ((string) items[(int) PClassConstants.NoOfMonths]).Replace("-", "0") + "_" + channel.CommonName;
            group.Attributes.Append(nameAttr);

            AddToKlarnaMonthDictionary(((string)items[(int)PClassConstants.NoOfMonths]).Replace("-","0"), channel);
            AddToKlarnaPClassDictionary((string)items[(int)PClassConstants.PClassId], channel);

            //Set encoding
            XmlElement encodingSetting = doc.CreateElement("Setting");
            XmlAttribute encodingNameAttr = doc.CreateAttribute("Name");
            encodingNameAttr.Value = "EncodingIso";
            encodingSetting.InnerText = "ISO-8859-1";
            encodingSetting.Attributes.Append(encodingNameAttr);
            group.AppendChild(encodingSetting);

            //Set settings
            for (int i = 0; i < items.Length; i++)
            {
                XmlElement setting = doc.CreateElement("Setting");
                XmlAttribute settingNameAttr = doc.CreateAttribute("Name");
                settingNameAttr.Value = "" + (PClassConstants)i;
                setting.InnerText = "" + items[i];

                if ((PClassConstants)i == PClassConstants.NoOfMonths && items[i].ToString() == "-")
                    setting.InnerText = "0";

                setting.Attributes.Append(settingNameAttr);
                group.AppendChild(setting);

            }

            doc.DocumentElement.AppendChild(group);

        }

        private static void AddToKlarnaMonthDictionary(string months, IChannel channel)
        {
            if (!_klarnaMonthDictionary.ContainsKey(channel.CommonName))
            {
                _klarnaMonthDictionary.Add(channel.CommonName, new List<string>());
            }


            if (!_klarnaMonthDictionary[channel.CommonName].Contains(months))
                _klarnaMonthDictionary[channel.CommonName].Add(months);
        }

        private static void AddToKlarnaPClassDictionary(string pClass, IChannel channel)
        {
            if (!_klarnaPclassDictionary.ContainsKey(channel.CommonName))
            {
                _klarnaPclassDictionary.Add(channel.CommonName, new List<string>());
            }

            if (!_klarnaPclassDictionary[channel.CommonName].Contains(pClass))
            _klarnaPclassDictionary[channel.CommonName].Add(pClass);
        }

        //private static void UpdatePClassGroup(XmlNode node,object[] item)
        //{
        //    if (node.ParentNode == null) throw new Exception("Invalid PClasses xml configuration");

        //    //Loop through all settings for this PClass in xml
        //    foreach (XmlNode settingsNode in node.ChildNodes)
        //    {
        //        if (settingsNode.Attributes==null || settingsNode.Attributes["Name"]==null)
        //            throw new Exception("Invalid PClasses xml configuration");
                
        //        //Get the settings name
        //        var name = settingsNode.Attributes["Name"].Value;

        //        //If setting is used get value from Klarna response and set new value to node
        //        if (Enum.IsDefined(typeof(PClassConstants),name ))
        //        {
        //            var value = "" + item[(int)Enum.Parse(typeof(PClassConstants), name)];
        //            settingsNode.InnerText = value;
        //        }
        //    }
        //}
    }
}