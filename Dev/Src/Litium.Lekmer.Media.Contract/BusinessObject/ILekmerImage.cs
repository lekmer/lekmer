﻿using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
    public interface ILekmerImage : IImage
    {
        string Link { get; set; }
        string Parameter { get; set; }
        string TumbnailImageUrl { get; set; }
        bool IsLink { get; }
        bool HasImage { get; set; }
    }
}
