using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartItemPriceAction.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.CartItemPriceAction
{
	public class CartItemPriceActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartItemPriceActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; private set; }

		public CartItemPriceActionSecureService(
			IAccessValidator accessValidator,
			CartItemPriceActionRepository repository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");

			var action = IoC.Resolve<ICartItemPriceAction>();
			action.ActionType = CartActionTypeSecureService.GetByCommonName("CartItemPrice");
			action.Amounts = new CurrencyValueDictionary();
			action.IncludeProducts = new ProductIdDictionary();
			action.ExcludeProducts = new ProductIdDictionary();
			action.IncludeCategories = new CategoryIdDictionary();
			action.ExcludeCategories = new CategoryIdDictionary();
			action.Status = BusinessObjectStatus.New;
			return action;
		}

		public void Delete(ISystemUserFull systemUserFull, int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public ICartAction GetById(int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var action = Repository.GetById(id);
			if (action == null) return null;

			action.Amounts = Repository.GetCartItemPricesByAction(id);
			action.IncludeProducts = Repository.GetIncludeProducts(id);
			action.IncludeCategories = Repository.GetIncludeCategories(id);
			action.ExcludeProducts = Repository.GetExcludeProducts(id);
			action.ExcludeCategories = Repository.GetExcludeCategories(id);
			return action;
		}

		public void Save(ISystemUserFull user, ICartAction action)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var cartItemPriceAction = action as ICartItemPriceAction;
			if (cartItemPriceAction == null) throw new InvalidOperationException("action is not ICartItemPriceAction type");

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(cartItemPriceAction);
				foreach (var amount in cartItemPriceAction.Amounts)
				{
					Repository.SaveCartItemPrice(cartItemPriceAction.Id, amount.Value);
				}
				SaveIncludeProducts(cartItemPriceAction);
				SaveIncludeCategories(cartItemPriceAction);
				SaveExcludeProducts(cartItemPriceAction);
				SaveExcludeCategories(cartItemPriceAction);
				transactedOperation.Complete();
			}
		}

		protected virtual void SaveIncludeProducts(ICartItemPriceAction action)
		{
			Repository.DeleteIncludeProducts(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}
			ProductIdDictionary productIdDictionary = action.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertIncludeProduct(action.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveExcludeProducts(ICartItemPriceAction action)
		{
			Repository.DeleteExcludeProducts(action.Id);

			ProductIdDictionary productIdDictionary = action.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertExcludeProduct(action.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveIncludeCategories(ICartItemPriceAction action)
		{
			Repository.DeleteIncludeCategories(action.Id);

			if (action.IncludeAllProducts)
			{
				return;
			}
			CategoryIdDictionary categories = action.IncludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, int> category in categories)
			{
				Repository.InsertIncludeCategory(action.Id, category.Key);
			}
		}

		protected virtual void SaveExcludeCategories(ICartItemPriceAction action)
		{
			Repository.DeleteExcludeCategories(action.Id);

			CategoryIdDictionary categories = action.ExcludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, int> category in categories)
			{
				Repository.InsertExcludeCategory(action.Id, category.Key);
			}
		}
	}
}