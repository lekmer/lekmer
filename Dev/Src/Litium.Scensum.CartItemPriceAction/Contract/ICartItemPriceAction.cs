using Litium.Scensum.Campaign;

namespace Litium.Scensum.CartItemPriceAction
{
	public interface ICartItemPriceAction : ICartAction
	{
		int MaxQuantity { get; set; }
		bool IncludeAllProducts { get; set; }
		ProductIdDictionary IncludeProducts { get; set; }
		CategoryIdDictionary IncludeCategories { get; set; }
		ProductIdDictionary ExcludeProducts { get; set; }
		CategoryIdDictionary ExcludeCategories { get; set; }
		CurrencyValueDictionary Amounts { get; set; }
	}
}