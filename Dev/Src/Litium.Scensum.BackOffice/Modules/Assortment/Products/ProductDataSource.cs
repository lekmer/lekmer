using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Xml;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	public class ProductDataSource
	{
		private int _rowCount;
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "title"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "statusId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "priceTo"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "priceFrom"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "erpId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "eanCode"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "categoryId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "brandId")]
		public int SelectCount(int maximumRows, int startRowIndex, string categoryId, string erpId, string title, string statusId, string priceFrom, string priceTo, string eanCode, string brandId)
		{
			return _rowCount;
		}
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex, string categoryId, string erpId, string title, string statusId, string priceFrom, string priceTo, string eanCode, string brandId)
		{
			ILekmerProductSearchCriteria searchCriteria = (ILekmerProductSearchCriteria)IoC.Resolve<IProductSearchCriteria>();
			searchCriteria.CategoryId = categoryId;
			searchCriteria.Title = title;
			searchCriteria.PriceFrom = priceFrom;
			searchCriteria.PriceTo = priceTo;
			searchCriteria.ErpId = erpId;
			searchCriteria.StatusId = statusId;
			searchCriteria.EanCode = eanCode;
			searchCriteria.BrandId = brandId;

			IProductSecureService productSecureService = IoC.Resolve<IProductSecureService>();
			var products = productSecureService.Search(
				ChannelHelper.CurrentChannel.Id,
				searchCriteria, startRowIndex / maximumRows + 1,
				maximumRows);
			_rowCount = products.TotalCount;
			return products;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "searchCriteriaInclusive"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "searchCriteriaExclusive"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex, string searchCriteriaInclusive, string searchCriteriaExclusive)
		{
			return _rowCount;
		}
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex, string searchCriteriaInclusive, string searchCriteriaExclusive)
		{
			IProductSecureService productSecureService = IoC.Resolve<IProductSecureService>();
			var products = productSecureService.AdvancedSearch(
				ChannelHelper.CurrentChannel.Id,
				searchCriteriaInclusive,
				searchCriteriaExclusive, startRowIndex / maximumRows + 1,
				maximumRows);
			_rowCount = products.TotalCount;
			return products;
		}
        
		public static string ConvertToXml(IEnumerable<IProductSearchCriteria> productSearchCriteria)
		{
			StringWriter stringWriter = new StringWriter(CultureInfo.CurrentCulture);
			XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
			xmlWriter.WriteStartElement("criterias");
			foreach (IProductSearchCriteria searchCriteria in productSearchCriteria)
			{
				xmlWriter.WriteStartElement("criteria");


				if (!string.IsNullOrEmpty(((ILekmerProductSearchCriteria)searchCriteria).BrandId))
					xmlWriter.WriteAttributeString("brandId", ((ILekmerProductSearchCriteria)searchCriteria).BrandId);

				if (!string.IsNullOrEmpty(searchCriteria.CategoryId))
					xmlWriter.WriteAttributeString("categoryId", searchCriteria.CategoryId);

				if (!string.IsNullOrEmpty(searchCriteria.Title))
					xmlWriter.WriteAttributeString("title", searchCriteria.Title);

				if (!string.IsNullOrEmpty(searchCriteria.PriceFrom))
					xmlWriter.WriteAttributeString("priceFrom", searchCriteria.PriceFrom);

				if (!string.IsNullOrEmpty(searchCriteria.PriceTo))
					xmlWriter.WriteAttributeString("priceTo", searchCriteria.PriceTo);

				if (!string.IsNullOrEmpty(searchCriteria.ErpId))
					xmlWriter.WriteAttributeString("erpId", searchCriteria.ErpId);

				if (!string.IsNullOrEmpty(searchCriteria.StatusId))
					xmlWriter.WriteAttributeString("statusId", searchCriteria.StatusId);

				if (!string.IsNullOrEmpty(searchCriteria.EanCode))
					xmlWriter.WriteAttributeString("eanCode", searchCriteria.EanCode);

				xmlWriter.WriteEndElement();
			}
			xmlWriter.WriteEndElement();
			return stringWriter.ToString();
		}
	}
}
