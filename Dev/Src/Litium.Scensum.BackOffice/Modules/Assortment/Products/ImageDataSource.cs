using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	public class ImageDataSource
	{
		private int _rowCount;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "id")]
		public int SelectCount(int id)
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), 
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", 
		MessageId = "startRowIndex"), 
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", 
		MessageId = "maximumRows")]
		public ImageCollection GetMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public ImageCollection GetMethod(int maximumRows, int startRowIndex, int id)
		{
			IImageSecureService templateSecureService = IoC.Resolve<IImageSecureService>();
			var images = templateSecureService.GetAllByFolder(id, maximumRows == 0 ? 0 : startRowIndex / maximumRows + 1, maximumRows, null, null);
			_rowCount = images.TotalCount;
			return images;
		}
	}
}
