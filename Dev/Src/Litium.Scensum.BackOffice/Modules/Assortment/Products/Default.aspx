<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>
<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Products.Default" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Assortment/ProductSearchCriteria.ascx" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="advancedSearchTab" ContentPlaceHolderID="body" runat="server">
    <Scensum:ToolBoxPanel ID="ProductHeaderToolBoxPanel" Text="Products" runat="server" />
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
			<ContentTemplate>
				<div style="width:978px;">					
					<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgProductSearchCriteria" />
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="SearchButton">
	<div id="articles-search" class="left">
	    <div >
		    <label ID="SearchCaptionLabel" runat="server" class="assortment-header" ><%= Resources.General.Literal_Search %></label>
		</div>
		<div id="search-block-criteria" class="left clear">
			<uc:Search ID="SearchControl" ValidationGroup="vgProductSearchCriteria" runat="server" />
		</div>
		<asp:UpdatePanel id="SearchUpdatePanel" runat="server">
			<ContentTemplate>
				<div class="articles-search-block">
					<div class="buttons right">
						<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SearchButton" Text="<%$ Resources:General, Button_Search %>" ValidationGroup="vgProductSearchCriteria" runat="server" SkinID="DefaultButton" style="padding-right: 0px;" />	
					</div>
					
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	<div class="product-horizontal-separator" >&nbsp;</div>
	<div id="articles-search-results">
		<asp:UpdatePanel runat="server" ID="SearchResultUpdatePanel">
			<ContentTemplate><label id="SeadrchResultLabel" runat="server" class="assortment-header"  ><%= Resources.General.Literal_SearchResults%></label>
				<div id="search-result-wrapper" style="margin-top: 11px;">
				    <Scensum:Grid runat="server" ID="Articles" fixedColumns="Type|Title">
					    <sc:GridViewWithCustomPager
						    ID="SearchResultsGrid" 
						    SkinID="grid" 
						    runat="server" 
						    PageSize="<%$AppSettings:DefaultGridPageSize%>"
						    AllowPaging="true" 
						    AutoGenerateColumns="false"
						    DataSourceID="ProductObjectDataSource"
						    Width="100%">
						    <Columns>
							    <asp:BoundField HeaderText="<%$ Resources:General,Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="7%" />
							    <asp:TemplateField HeaderText="Title" ItemStyle-Width="45%">
								    <ItemTemplate>
									    <uc:HyperLinkEncoded runat="server" ID="EditLink" Text='<%# Eval("DisplayTitle")%>' NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Product.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' />
								    </ItemTemplate>
							    </asp:TemplateField>
							   <asp:TemplateField HeaderText="<%$ Resources:Product,Literal_Category %>" ItemStyle-Width="25%">
									<ItemTemplate>
										<%# GetCategory((int)Eval("CategoryId"))%>
									</ItemTemplate>
								</asp:TemplateField>
							    <asp:BoundField HeaderText="<%$ Resources:Product,Label_EanCode %>" DataField="EanCode" ItemStyle-Width="8%" />
							    <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="7%">
										<ItemTemplate>
											<asp:Label id="ProductStatusLabel" runat="server" />
										</ItemTemplate>
								</asp:TemplateField>
							    <asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" >
								    <ItemTemplate>
									    <asp:Label ID="PriceLabel"  Text ="" runat="server"/>
								    </ItemTemplate>
							    </asp:TemplateField>
						    </Columns>
					    </sc:GridViewWithCustomPager>		
				    </Scensum:Grid>
				</div>
				<asp:ObjectDataSource ID="ProductObjectDataSource"  CacheDuration="300" CacheKeyDependency="AssortmentCacheKey" EnableCaching="true" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />
				</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	</asp:Panel>
</asp:Content>
