using System;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	[Serializable]
	public class ProductState
	{
		public string TempMediaFileName { get; set; }
		public bool SetNewImage { get; set; }
		public int? MediaId { get; set; }
		public object TempRelationList { get; set; }
	}
}
