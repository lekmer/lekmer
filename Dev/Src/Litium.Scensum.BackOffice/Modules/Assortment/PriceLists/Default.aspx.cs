using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.PriceLists
{
	public partial class Default : PageController 
	{
		//TODO: Remove when add cache logic
		private Collection<IPriceListStatus> _statuses;
		//TODO: Remove when add cache logic
		private Collection<ICurrency> _currencies;

		protected virtual string GetPriceListStatus(int priceListStatusId)
		{
			//TODO: Remove when add cache logic
			if (_statuses == null)
				_statuses = IoC.Resolve<IPriceListStatusSecureService>().GetAll();
			return _statuses.FirstOrDefault(item => item.Id == priceListStatusId).Title;
		}
	
		protected virtual string GetCurrency(int currencyId)
		{
			//TODO: Remove when add cache logic
			if (_currencies == null)
				_currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
			return _currencies.FirstOrDefault(item => item.Id == currencyId).Iso;
		}
		protected virtual bool? IsSearchResult()
		{
			return Request.QueryString.GetBooleanOrNull("IsSearchResult");
		}
		protected override void SetEventHandlers()
		{
			PriceListGrid.RowDataBound += PriceListGridRowDataBound;
			PriceListGrid.PageIndexChanging += PriceListGridPageIndexChanging;
			SetStatusButton.Click += OnSave;
		}

		protected override void PopulateForm()
		{
			PopulateStatusList();
		}
		public void OnSave(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(PricelistStatusList.SelectedValue))
			{
				messager.Add(Resources.GeneralMessage.ChooseStatusValue);
				return;
			}
			IEnumerable<int> ids = PriceListGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				messager.Add(Resources.ProductMessage.NoPricelistSelected);
				messager.MessageType = InfoType.Warning;
				return;
			}
			int priceListStatusId = int.Parse(PricelistStatusList.SelectedValue, CultureInfo.CurrentCulture);
			var priceListSecureService = IoC.Resolve<IPriceListSecureService>();
			priceListSecureService.SetStatus(SignInHelper.SignedInSystemUser, ids, priceListStatusId);
			messager.Add(Resources.GeneralMessage.StatusesChangedSuccessfully);
			messager.MessageType = InfoType.Success;
		}
		protected virtual bool? ShowFolderContent()
		{
			return Request.QueryString.GetBooleanOrNull("ShowFolderContent");
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnLoad(e);
			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				PopulateSearch();
				return;
			}


			if (Request.UrlReferrer != null)
			{
				if (ShowFolderContent() != null)
				{
					if (ShowFolderContent() == true)
					{
						((PriceLists)Master).ShowFolderContent = true;
						PopulatePriceListGrid();
						return;
					}
				}
			}

			if (((PriceLists)Master).ShowFolderContent)
			{
				PopulatePriceListGrid();
			}
			else
			{
				((PriceLists)Master).DenySelection = true;
				AllSelectedDiv.Visible = false;
			}
		}

		protected virtual void PriceListGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", 
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + PriceListGrid.ClientID + @"'); ShowBulkUpdatePanel('"
					+ selectAllCheckBox.ID + "', '" + PriceListGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			if (row.RowType == DataControlRowType.DataRow)
			{
				HiddenField hiddenId = (HiddenField)e.Row.FindControl("IdHiddenField");
				HyperLink hlEdit = (HyperLink) e.Row.FindControl("PriceEditLink");
				if (IsSearchResult().HasValue && IsSearchResult().Value)
				{
					hlEdit.NavigateUrl = PathHelper.Assortment.Pricelist.GetEditUrlForSearch(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
				}
				else
				{
					hlEdit.NavigateUrl = PathHelper.Assortment.Pricelist.GetEditUrlForDefault(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
				}
			}

		}

		protected virtual void PriceListGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			PriceListGrid.PageIndex = e.NewPageIndex;
		}
		
		protected void PopulatePriceListGrid()
		{
			int? folderId = ((PriceLists)Master).SelectedFolderId;
			if(folderId.HasValue)
			{
				PriceListObjectDataSource.SelectParameters.Clear();
				PriceListObjectDataSource.SelectMethod = "GetAllByFolderMethod";
				PriceListObjectDataSource.SelectParameters.Add("maximumRows", PriceListGrid.PageSize.ToString(CultureInfo.CurrentCulture));
				PriceListObjectDataSource.SelectParameters.Add("startRowIndex", (PriceListGrid.PageIndex * PriceListGrid.PageSize).ToString(CultureInfo.CurrentCulture));
				PriceListObjectDataSource.SelectParameters.Add("folderId", folderId.Value.ToString(CultureInfo.CurrentCulture));

			}
			PriceListGrid.DataBind();
			AllSelectedDiv.Visible = PriceListGrid.Rows.Count > 0;
			SetClientFunction();
		}

		protected void SetClientFunction()
		{
			if (PriceListGrid.HeaderRow == null) return;
			var selectAllCheckBox = (CheckBox)PriceListGrid.HeaderRow.FindControl("SelectAllCheckBox");
			foreach (GridViewRow row in PriceListGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick", 
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
					cbSelect.ID + "', '" + PriceListGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected void PopulateStatusList()
		{
			PricelistStatusList.DataSource = IoC.Resolve<IPriceListStatusSecureService>().GetAll();
			PricelistStatusList.DataBind();
			PricelistStatusList.Items.Insert(0, new ListItem(Resources.General.Literal_Status + "...", string.Empty));
		}
		protected void PopulateSearch()
		{
			string searchCriteria = SearchCriteriaState<string>.Instance.Get(PathHelper.Assortment.Pricelist.GetSearchResultUrl());
			if (!string.IsNullOrEmpty(searchCriteria))
			{
				PriceListObjectDataSource.SelectParameters.Clear();
				PriceListObjectDataSource.SelectMethod = "SearchMethod";
				PriceListObjectDataSource.SelectParameters.Add("maximumRows", PriceListGrid.PageSize.ToString(CultureInfo.CurrentCulture));
				PriceListObjectDataSource.SelectParameters.Add("startRowIndex", (PriceListGrid.PageIndex * PriceListGrid.PageSize).ToString(CultureInfo.CurrentCulture));
				PriceListObjectDataSource.SelectParameters.Add("searchCriteria", searchCriteria);
			}	
			PriceListGrid.DataBind();
			AllSelectedDiv.Visible = PriceListGrid.Rows.Count > 0;
			SetClientFunction();

            PriceListSearchResultLabelPanel.Visible = true;
		}
	}
}
