using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.PriceLists
{
	public partial class PriceListEdit : StatePageController<PricelistState>, IEditor 
	{
		private Collection<ICustomerGroupStatus> _customerGroupStatuses;
		//TODO: Remove when add cache logic
		private Collection<IProductStatus> _statuses;
		//TODO: Remove when add cache logic
		private Collection<ICurrency> _currencies;

		protected string GetMask()
		{
			char dateSeperator = System.Convert.ToChar(CultureInfo.CurrentCulture.DateTimeFormat.DateSeparator, CultureInfo.CurrentCulture);
			string[] dateFormat = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.Split(dateSeperator);
			var mask = new StringBuilder();

			foreach (string s in dateFormat)
			{
				for (int i = 0; i < (s.Length < 2 ? 2 : s.Length); i++)
				{
					mask.Append("9");
				}
				mask.Append("/");
			}

			return mask.Replace('/', ' ', mask.Length - 1, 1).Append("99:99:99").ToString();
		}

		protected override void SetEventHandlers()
		{
			ProductGrid.RowDataBound += ProductGridRowDataBound;
			ProductGrid.PageIndexChanging += ProductGridPageIndexChanging;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			StartDateValidator.ServerValidate += ValidateDateTime;
			EndDateValidator.ServerValidate += ValidateDateTime;
			StartEndDateLimitValidator.ServerValidate += CompareDateTime;

			RemoveCustomerGroupsButton.Click += OnCustomerGroupsRemove;
			CustomerGroupSelector.SelectEvent += OnCustomerGroupsAdd;
			CustomerGroupsGrid.PageIndexChanging += OnCustomerGroupsPageIndexChanging;
			CustomerGroupsGrid.RowDataBound += OnCustomerGroupsRowDataBound;
			CustomerGroupsGrid.RowCommand += OnCustomerGroupsRowCommand;

			PlaceFolderNodeSelector.NodeCommand += OnNodeSelectorCommand;

		}

		protected virtual void OnNodeSelectorCommand(object sender, CommandEventArgs e)
		{
			PlaceFolderNodeSelector.DataSource = GetPriceListFolders(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			PlaceFolderNodeSelector.DataBind();
		}

		protected override void PopulateForm()
		{
			IPriceList priceList = IoC.Resolve<IPriceListSecureService>().GetById(GetId());

			PopulateGrid();
			PopulateTree(priceList.PriceListFolderId);
			PopulateData(priceList);
			PopulateCustomerGroups();
			UpdateMasterTree(priceList.PriceListFolderId);

			StartDateValidator.ErrorMessage += Constants.ExampleDate;
			EndDateValidator.ErrorMessage += Constants.ExampleDate;

			StartDateMaskedEdit.Mask = EndDateMaskedEdit.Mask = GetMask();
			StartDateMaskedEdit.AcceptAMPM =
				EndDateMaskedEdit.AcceptAMPM = !string.IsNullOrEmpty(CultureInfo.CurrentCulture.DateTimeFormat.AMDesignator);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!Page.IsValid)
				return;
			var service = IoC.Resolve<IPriceListSecureService>();
			IPriceList priceList = service.GetById(GetId());
			if (priceList == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}
			priceList.PriceListStatusId = int.Parse(StatusList.SelectedValue, CultureInfo.CurrentCulture);
			priceList.PriceListFolderId = PlaceFolderNodeSelector.SelectedNodeId ?? priceList.PriceListFolderId;
			priceList.StartDateTime = string.IsNullOrEmpty(StartDateTextBox.Text) ? null : (DateTime?)DateTime.Parse(StartDateTextBox.Text, CultureInfo.CurrentCulture);
			priceList.EndDateTime = string.IsNullOrEmpty(EndDateTextBox.Text) ? null : (DateTime?)DateTime.Parse(EndDateTextBox.Text, CultureInfo.CurrentCulture);
			service.Save(SignInHelper.SignedInSystemUser, priceList, State.CustomerGroups);
			((PriceLists)Master).UpdateSelection(priceList.PriceListFolderId);
			SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessPriceList);
			if (((PriceLists)Master).SelectedFolderId != priceList.PriceListFolderId)
			{
				((PriceLists)Master).DenySelection = false;
				((PriceLists)Master).SelectedFolderId = priceList.PriceListFolderId;
				((PriceLists)Master).PopulateTree(null);
			}
		}

		protected virtual void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.DataRow)
			{
				var priceListItem = (IPriceListItem)row.DataItem;
				IProduct product = IoC.Resolve<IProductSecureService>().GetById(ChannelHelper.CurrentChannel.Id, priceListItem.ProductId);
				((Label)row.FindControl("ProductArticleIdLabel")).Text = product.ErpId;
				((Label)row.FindControl("ProductTitleLabel")).Text = product.DisplayTitle;
				((Label)row.FindControl("ProductStatusLabel")).Text = GetProductStatus(product.ProductStatusId);
			}
		}

		protected virtual void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void OnCustomerGroupsAdd(object sender, UserControls.Customer.Events.CustomerGroupSelectEventArgs e)
		{
			foreach (var group in e.CustomerGroups)
			{
				var clone = group;
				if (State.CustomerGroups.FirstOrDefault(c => c.Id == clone.Id) == null)
				{
					State.CustomerGroups.Add(group);
				}
			}
			PopulateCustomerGroupsGrid();
		}

		protected virtual void OnCustomerGroupsRemove(object sender, EventArgs e)
		{
			IEnumerable<int> ids = CustomerGroupsGrid.GetSelectedIds();
			foreach (var id in ids)
			{
				var cloneId = id;
				var group = State.CustomerGroups.FirstOrDefault(c => c.Id == cloneId);
				if (group != null)
				{
					State.CustomerGroups.Remove(group);
				}
			}
			PopulateCustomerGroupsGrid();
		}

		protected virtual void OnCustomerGroupsRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnCustomerGroupsPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CustomerGroupsGrid.PageIndex = e.NewPageIndex;
			PopulateCustomerGroupsGrid();
		}

		protected virtual void OnCustomerGroupsRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteGroup")
			{
				var group = State.CustomerGroups.FirstOrDefault(c => c.Id == System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				if (group != null)
				{
					State.CustomerGroups.Remove(group);
					PopulateCustomerGroupsGrid();
				}
			}
		}

		protected static void ValidateDateTime(object sender, ServerValidateEventArgs args)
		{
			if (args.Value.Trim().Length <= 10)
			{
				args.IsValid = false;
				return;
			}

			DateTime dateTime;
			if (!DateTime.TryParse(args.Value, out dateTime))
			{
				args.IsValid = false;
				return;
			}

			DateTime minDate = DateTime.Parse(Constants.MinDate, CultureInfo.CurrentCulture);
			DateTime maxDate = DateTime.Parse(Constants.MaxDate, CultureInfo.CurrentCulture);
			if (dateTime < minDate || dateTime > maxDate)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}

		protected void CompareDateTime(object sender, ServerValidateEventArgs args)
		{
			if (string.IsNullOrEmpty(StartDateTextBox.Text) || !StartDateValidator.IsValid || !EndDateValidator.IsValid)
			{
				args.IsValid = true;
				return;
			}
			DateTime startDate = DateTime.Parse(StartDateTextBox.Text, CultureInfo.CurrentCulture);
			DateTime endDate = DateTime.Parse(args.Value, CultureInfo.CurrentCulture);
			if (startDate > endDate)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}

		protected void PopulateGrid()
		{
			PiceListItemsObjectDataSource.SelectParameters.Clear();
			PiceListItemsObjectDataSource.SelectParameters.Add("maximumRows", ProductGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			PiceListItemsObjectDataSource.SelectParameters.Add("startRowIndex", (ProductGrid.PageIndex * ProductGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			PiceListItemsObjectDataSource.SelectParameters.Add("priceListId", GetId().ToString(CultureInfo.CurrentCulture));
		}

		protected void PopulateData(IPriceList priceList)
		{
			TitleTextBox.Text = priceList.Title;
			CommonNameTextBox.Text = priceList.CommonName;
			CurrencyTextBox.Text = GetCurrency(priceList.CurrencyId);
			StartDateTextBox.Text = priceList.StartDateTime.HasValue ?
				priceList.StartDateTime.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;
			EndDateTextBox.Text = priceList.EndDateTime.HasValue ?
				priceList.EndDateTime.Value.ToString(CultureInfo.CurrentCulture) : string.Empty;

			StatusList.DataSource = IoC.Resolve<IPriceListStatusSecureService>().GetAll();
			StatusList.DataBind();
			StatusList.Items.FindByValue(priceList.PriceListStatusId.ToString(CultureInfo.CurrentCulture)).Selected = true;
		}

		protected void PopulateTree(int folderId)
		{
			PlaceFolderNodeSelector.SelectedNodeId = folderId;
			PlaceFolderNodeSelector.DataSource = GetPriceListFolders(folderId);
			PlaceFolderNodeSelector.DataBind();
			PlaceFolderNodeSelector.PopulatePath(folderId);
		}

		protected void PopulateCustomerGroups()
		{
			var service = IoC.Resolve<ICustomerGroupSecureService>();
			State = new PricelistState(service.GetAllByPricelist(GetId()));
			PopulateCustomerGroupsGrid();
		}

		protected virtual void PopulateCustomerGroupsGrid()
		{
			CustomerGroupsGrid.DataSource = State.CustomerGroups;
			CustomerGroupsGrid.DataBind();
			CustomerGroupsUpdatePanel.Update();
		}

		protected IEnumerable<INode> GetPriceListFolders(int? folderId)
		{
			return IoC.Resolve<IPriceListFolderSecureService>().GetTree(folderId).Where(n => n.Id != GetId());
		}

		protected string GetProductStatus(int productStatusId)
		{
			//TODO: Remove when add cache logic
			if (_statuses == null)
				_statuses = IoC.Resolve<IProductStatusSecureService>().GetAll();
			return _statuses.FirstOrDefault(item => item.Id == productStatusId).Title;
		}
		protected string GetCurrency(int currencyId)
		{
			//TODO: Remove when add cache logic
			if (_currencies == null)
				_currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
			return _currencies.FirstOrDefault(item => item.Id == currencyId).Iso;
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Assortment.Pricelist.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Assortment.Pricelist.GetDefaultUrl());
			}
		}

		protected virtual string GetCustomerGropEditUrl(object groupId)
		{
			return PathHelper.Customer.CustomerGroup.GetEditUrlForPriceListEdit(System.Convert.ToInt32(groupId, CultureInfo.CurrentCulture), GetId());
		}

		protected virtual string GetCustomerGropStatus(object statusId)
		{
			if (_customerGroupStatuses == null)
				_customerGroupStatuses = IoC.Resolve<ICustomerGroupStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _customerGroupStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", 
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" 
					+ selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", 
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" +
					cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		//Use for cases when come from customer group edit page.
		protected virtual void UpdateMasterTree(int folderId)
		{
			var master = Master as PriceLists;
			if (master == null) return;

			master.SelectedFolderId = folderId;
		}

		private ICurrency _currency;
		protected virtual string GetPrice(int priceListId, decimal price)
		{
			if (_currency == null)
			{
				var priceList = IoC.Resolve<IPriceListSecureService>().GetById(priceListId);
				_currency = IoC.Resolve<ICurrencySecureService>().GetById(priceList.CurrencyId);
			}
			return IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, _currency, price);
		}
	}

	[Serializable]
	public class PricelistState
	{
		private readonly Collection<ICustomerGroup> _customerGroups;
		public Collection<ICustomerGroup> CustomerGroups
		{
			get
			{
				return _customerGroups;
			}
		}

		public PricelistState(Collection<ICustomerGroup> customerGroups)
		{
			_customerGroups = customerGroups;
		}
	}
}
