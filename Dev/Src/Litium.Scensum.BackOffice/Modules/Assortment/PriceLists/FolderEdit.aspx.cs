using System;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.PriceLists
{
	public partial class FolderEdit : PageController,IEditor
	{
		protected virtual PriceLists MasterPage
		{
			get
			{
				return (PriceLists)Master;
			}
		}

		protected virtual bool? HasMessage()
		{
			return Request.QueryString.GetBooleanOrNull("HasMessage");
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateForm()
		{
		    int? folderId = GetIdOrNull();
		    IPriceListFolder folder = null;

            if (folderId != null)
			{
                folder = IoC.Resolve<IPriceListFolderSecureService>().GetById((int)folderId);
				FolderTitleTextBox.Text = folder.Title;
                PopulateTreeView(null);
			}
			else
			{
                folder = IoC.Resolve<IPriceListFolderSecureService>().Create();
                DestinationDiv.Visible = false;
			}

		    SetFolderBreadcrumbPath(folder);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (HasMessage().HasValue && HasMessage().Value && !SystemMessageContainer.HasMessages)
			{
				SystemMessageContainer.MessageType = InfoType.Success;
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessFolder);
			}
			System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", FolderTree.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", FolderTree.ClientID), true);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Assortment.Pricelist.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var service = IoC.Resolve<IPriceListFolderSecureService>();
			IPriceListFolder folder = GetIdOrNull() != null ? service.GetById(GetId()) : service.Create();
			folder.Title = FolderTitleTextBox.Text;
			if (GetIdOrNull() == null)
			{
				folder.ParentPriceListFolderId = GetParentIdOrNull() != MasterPage.RootNodeId ? GetParentIdOrNull() : null;
			}
			else if (FolderTree.SelectedNodeId.HasValue)
			{
				folder.ParentPriceListFolderId = FolderTree.SelectedNodeId.Value == FolderTree.RootNodeId ? null : FolderTree.SelectedNodeId;
			}
			folder.Id = service.Save(SignInHelper.SignedInSystemUser, folder);
			if (folder.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.FolderTitleExist);
			}
			else
			{
				((PriceLists)Master).PopulateTree(null);
				((PriceLists) Master).SelectedFolderId = folder.Id;
				Response.Redirect(PathHelper.Assortment.Pricelist.GetFolderEditWithMessageUrl(folder.Id));
			}

		    SetFolderBreadcrumbPath(folder);
		}

        private void SetFolderBreadcrumbPath(IPriceListFolder folder)
        {
            IPriceListFolderSecureService service = null;
            
            StringBuilder breadcrumb = new StringBuilder();
            breadcrumb.Append(folder.Id > 0 ? folder.Title : Resources.General.Literal_CreateFolder);

            var parentId = folder.ParentPriceListFolderId ?? MasterPage.SelectedFolderId;
			while (parentId != null && parentId != -1)
            {
                if (service == null)
                    service = IoC.Resolve<IPriceListFolderSecureService>();

                folder = service.GetById((int)parentId);
                breadcrumb.Insert(0, string.Format(CultureInfo.InvariantCulture, "{0} > ", folder.Title));

                parentId = folder.ParentPriceListFolderId;
            }

            breadcrumb.Insert(0, string.Format(CultureInfo.InvariantCulture, "{0} > ", Resources.Product.Literal_PriceLists));
            FolderEditLabel.Text = breadcrumb.ToString();
        }


		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var folders = MasterPage.GetNodes(folderId ?? MasterPage.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;
			var editedFolder = folders.FirstOrDefault(f => f.Id == MasterPage.SelectedFolderId);

			FolderTree.DataSource = editedFolder != null
				? FolderTree.GetFilteredSource(folders, editedFolder.Id)
				: folders;

			FolderTree.RootNodeTitle = Resources.Product.Literal_PriceLists;
			FolderTree.DataBind();
			FolderTree.SelectedNodeId = folderId ?? (editedFolder != null ? editedFolder.ParentId : FolderTree.RootNodeId) ?? FolderTree.RootNodeId;
		}

		protected virtual int? GetParentIdOrNull()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}
	}
}
