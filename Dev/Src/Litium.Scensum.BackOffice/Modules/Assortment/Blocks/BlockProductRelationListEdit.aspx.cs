using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductRelationListEdit : PageController, IEditor 
	{
		private Collection<IBlockProductRelationListItem> _items;

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			
            var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductRelationListEdit);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			RelationsGrid.RowDataBound += RelationsGridRowDataBound;
		}

		protected virtual void RelationsGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var cbAll = (CheckBox)row.FindControl("AllRelationsSelectCheckBox");
				cbAll.Checked = false;
				cbAll.Attributes.Add("onclick", "SelectAll1('" + cbAll.ClientID + @"','" + RelationsGrid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var relationListType = (IRelationListType) row.DataItem;
				var cbSelect = (CheckBox)row.FindControl("RelationSelectCheckBox");
				cbSelect.Checked = GetItems().FirstOrDefault(i => i.RelationListTypeId == relationListType.Id) != null;
			}
		}

		protected override void PopulateForm()
		{
			var blockService = IoC.Resolve<IBlockProductRelationListSecureService>();
			IBlockProductRelationList blockProductRelationList = blockService.GetById(GetBlockId());

			BlockTitleTextBox.Text = blockProductRelationList.Title;
			NumberOfColumnsTextBox.Text = blockProductRelationList.ColumnCount.ToString(CultureInfo.CurrentCulture);
			NumberOfRowsTextBox.Text = blockProductRelationList.RowCount.ToString(CultureInfo.CurrentCulture);

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockProductRelationList");
			TemplateList.DataBind();
			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);
			ListItem item = TemplateList.Items.FindByValue(blockProductRelationList.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			var productSortOrderSecureService = IoC.Resolve<IProductSortOrderSecureService>();
			SortList.DataSource = productSortOrderSecureService.GetAll();
			SortList.DataBind();
			ListItem listItem = SortList.Items.FindByValue(blockProductRelationList.ProductSortOrder.Id.ToString(CultureInfo.CurrentCulture));
			if (listItem != null)
				listItem.Selected = true;

			var relationListTypeSecureService = IoC.Resolve<IRelationListTypeSecureService>();
			RelationsGrid.DataSource = relationListTypeSecureService.GetAll();
			RelationsGrid.DataBind();

			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductRelationListSecureService>();
			IBlockProductRelationList blockProductRelationList = blockService.GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(
			    blockProductRelationList.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductRelationListSecureService>();
			IBlockProductRelationList blockProductRelationList = blockService.GetById(GetBlockId());

			if (blockProductRelationList == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockProductRelationList.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductRelationList.Title = BlockTitleTextBox.Text;
			blockProductRelationList.ColumnCount = int.Parse(NumberOfColumnsTextBox.Text, CultureInfo.CurrentCulture);
			blockProductRelationList.RowCount = int.Parse(NumberOfRowsTextBox.Text, CultureInfo.CurrentCulture);
			int productSortOrderId;
			if (int.TryParse(SortList.SelectedValue, out productSortOrderId))
			{
				blockProductRelationList.ProductSortOrder = IoC.Resolve<IProductSortOrderSecureService>().GetById(productSortOrderId);
			}
			blockService.Save(SignInHelper.SignedInSystemUser, blockProductRelationList, GetRelationListItemsFromGrid());
			if (blockProductRelationList.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductRelationList);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IBlockProductRelationListItem> GetItems()
		{
			if (_items == null)
			{
				_items = IoC.Resolve<IBlockProductRelationListItemSecureService>().GetAllByBlock(GetBlockId());
			}
			return _items;
		}

        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual Collection<IBlockProductRelationListItem> GetRelationListItemsFromGrid()
		{
			var items = new Collection<IBlockProductRelationListItem>();
			var service = IoC.Resolve<IBlockProductRelationListItemSecureService>();
			foreach (GridViewRow row in RelationsGrid.Rows)
			{
				var cbSelect = (CheckBox)row.FindControl("RelationSelectCheckBox");
				if (cbSelect.Checked)
				{
					IBlockProductRelationListItem item = service.Create();
					item.BlockId = GetBlockId();
					var hfId = (HiddenField)row.FindControl("RelationIdHiddenField");
					item.RelationListTypeId = int.Parse(hfId.Value, CultureInfo.CurrentCulture);
					items.Add(item);
				}
			}
			return items;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}