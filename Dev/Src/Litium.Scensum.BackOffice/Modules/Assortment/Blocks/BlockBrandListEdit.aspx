﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="BlockBrandListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockBrandListEdit" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="SiteStructureNodeSelector" Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" />
	<uc:ScensumValidationSummary runat="server" ID="ValdationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="BlockBrandListValidationGroup" />
</asp:Content>

<asp:Content runat="server" ID="BlockBrandListContent" ContentPlaceHolderID="SiteStructureForm" >
	<script type="text/javascript">	
		function confirmDeleteBrands() {
			return DeleteConfirmation("<%= Resources.Lekmer.Literal_BrandsLowerCase %>");
		}
	</script>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="block-assortment">
			<div class="input-box">
				<span><asp:Literal ID="Literal1" runat="server" Text= "<%$ Resources:General,Literal_Title%>"/></span>&nbsp
				<uc:GenericTranslator ID="Translator" runat="server" />&nbsp
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="BlockTitleValidator" 
					ControlToValidate="BlockTitleTextBox" 
					ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"  
					Display ="None" 
					ValidationGroup="BlockBrandListValidationGroup"  />
				<br />	
				<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" ></asp:TextBox>
			</div>
			<br />
			<div class="input-box">
				<span><asp:Literal ID="Literal2" runat="server" Text= "<%$ Resources:Lekmer,Literal_NavigationLink%>"/></span>
				<uc:SiteStructureNodeSelector ID="LinkNodeSelector" runat="server" />
			</div>
			<div class="input-box">
				<br class="clear" />
				<asp:UpdatePanel ID="BrandListUpdatePanel" runat="server" UpdateMode="Conditional">                                
					<ContentTemplate>
						<asp:RadioButton ID="AllBrandsRadio" runat="server" Font-Bold="true" AutoPostBack="true"
							Text="<%$ Resources:Lekmer,Literal_AllBrands%>" GroupName="BrandsRadioGroup" />
						<br />
						<asp:RadioButton ID="SelectedBrandsRadio" runat="server" Font-Bold="true" AutoPostBack="true"
							Text="<%$ Resources:Lekmer,Literal_SelectedBrands%>" GroupName="BrandsRadioGroup" />
						<br />
						<div id="BrandsDiv" runat="server">                                           
							 <sc:GridViewWithCustomPager 
									ID="BrandsGrid" 
									SkinID="grid" 
									runat="server" 
									PageSize="<%$AppSettings:DefaultGridPageSize%>"
									AllowPaging="true" 
									AutoGenerateColumns="false"
									Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<HeaderTemplate>
												<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox ID="SelectCheckBox" runat="server" />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("BrandId") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField HeaderText="<%$ Resources:General, Literal_Title %>" DataField="Title" ItemStyle-Width="80%" />
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
											<HeaderTemplate>
												<div class="inline">
													<%= Resources.General.Literal_SortOrder %>
													<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" ImageUrl="~/Media/Images/SiteStructure/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="OrdinalsValidationGroup" />
												</div>
											</HeaderTemplate>
											<ItemTemplate>
												<div class="inline">
													<asp:HiddenField ID="IdHidden" Value='<%#Eval("BrandId") %>' runat="server" />
													<asp:RequiredFieldValidator runat="server" ID="v" ControlToValidate="OrdinalBox" Text="*" ValidationGroup="OrdinalsValidationGroup" />
													<asp:RangeValidator runat="server" ID="rv" ControlToValidate="OrdinalBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="OrdinalsValidationGroup"></asp:RangeValidator>
													<asp:TextBox runat="server" ID="OrdinalBox" Text='<%#Eval("Ordinal") %>' style="width:20px;"/>
													<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("BrandId") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" />
													<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("BrandId") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" />
												</div>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteBrand" CommandArgument='<%# Eval("BrandId") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Lekmer.Literal_BrandLowerCase + "\");" %>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</sc:GridViewWithCustomPager>			
								<br style="clear:both;"/>
								<div id="AllSelectedDiv" runat="server" style="display: none;">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectedButton" Text="<%$Resources:General, Button_RemoveSelected %>" OnClientClick="return confirmDeleteBrands();" runat="server" SkinID="DefaultButton"/>
								 </div>
								 
								 <div id="BrandsAddDiv" runat="server" class="popup-brands-add-container" style="z-index: 10010; display: none;">
									<div id="popup-brands-add-header">
										<div id="popup-brands-add-header-left">
										</div>
										<div id="popup-brands-add-header-center">
											<span><%= Resources.Lekmer.Literal_ChooseBrand%></span>
											<input type="button" id="BrandsAddCloseButton" runat="server" value="x"/>
										</div>
										<div id="popup-brands-add-header-right">
										</div>
									</div>
									<div id="popup-brands-add-content">
										<asp:ListBox runat="server" ID="BrandsAddList" SelectionMode="Multiple" Rows="10" 
											DataTextField="Title" DataValueField="Id" Width="194px">
										</asp:ListBox>
										<br style="clear:both;"/>
										<div style="float:right; padding: 10px;">
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandsAddOkButton" runat="server" Text="<%$ Resources:General, Button_Ok %>" SkinID="DefaultButton" />
											<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandsAddCancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
										</div>
									 </div>
								</div>
								<div style="float:right;">
									<uc:ImageLinkButton ID="BrandsAddButton" runat="server" Text="<%$ REsources:General, Button_Add %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
									<asp:HiddenField runat="server" ID="BrandsAddTargetHidden" />
									<ajaxToolkit:ModalPopupExtender 
										ID="BrandsAddPopup" 
										runat="server" 
										TargetControlID="BrandsAddTargetHidden"
										PopupControlID="BrandsAddDiv"
										BackgroundCssClass="popup-background"
										CancelControlID="BrandsAddCloseButton"/>
								</div>
							 </div>
						 </ContentTemplate>
					</asp:UpdatePanel>
			   
			</div>
			<div class="content-box">				
				<br class="clear" />
				<div id="Settings" runat="server" class="content-box">
					<label class="block-header"><asp:Literal ID="Literal3"  runat="server" Text= "<%$ Resources:General,Label_Settings%>" /></label>
					<br class="clear" />
					<div class="column">
						<div class="input-box">
							<span><asp:Literal ID="Literal4" runat="server" Text= "<%$ Resources:General,Literal_ChooseTemplate%>" /></span><br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><asp:Literal ID="Literal5" runat="server" Text= "<%$ Resources:General,Label_NumberOfColumns%>" />&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="ColumnsCompareValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>"  
								Display ="None" 
								ValidationGroup="BlockBrandListValidationGroup"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="ColumnsRequiredValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage, ColumnsNumberEmpty %>" 
								Display ="None"
								ValidationGroup="BlockBrandListValidationGroup" />
							<br />
							<asp:TextBox ID="NumberOfColumnsTextBox" runat="server" /><br />							
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><asp:Literal ID="Literal6" runat="server" Text= "<%$ Resources:General,Label_NumberOfRows%>" />&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="RowsCompareValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberGreaterZero %>" 
								Display ="None" 
								ValidationGroup="BlockBrandListValidationGroup"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="RowsRequiredValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberEmpty %>" 
								Display ="None" 
								ValidationGroup="BlockBrandListValidationGroup" />
							<br />
							<asp:TextBox ID="NumberOfRowsTextBox" runat="server" /><br />							
						</div>
					</div>
					<br class="clear" />
				</div>
			</div>
		</div>
		<br />
		<div id="product-list-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="BlockBrandListValidationGroup"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
			</asp:Panel>
</asp:Content>
