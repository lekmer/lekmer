<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="BlockProductSearchResultEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductSearchResultEdit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgCount" />
	</asp:Content>
<asp:Content ID="EditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><%=Resources.General.Literal_Title%></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator runat="server" 
				ID="BlockTitleValidator" 
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$Resources:GeneralMessage, TitleEmpty %>" 
				Display ="None" 
				ValidationGroup="vgCount" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br />
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings %></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
						<span><%=Resources.General.Literal_ChooseTemplate%></span><br />
						<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><%=Resources.General.Label_NumberOfColumns%>&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="ColumnsCompareValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ValueToCompare="0" Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>"  
								Display ="None" ValidationGroup="vgCount"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="ColumnsRequiredValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberEmpty %>"  
								Display ="None" 
								ValidationGroup="vgCount" />
							<br />
							<asp:TextBox ID="NumberOfColumnsTextBox" runat="server" /><br />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><%=Resources.General.Label_NumberOfRows %>&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="RowsCompareValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage,RowsNumberGreaterZero %>"  
								Display ="None" 
								ValidationGroup="vgCount"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="RowsRequiredValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage,RowsNumberGreaterZero %>"  
								Display ="None" 
								ValidationGroup="vgCount" />
							<br />
							<asp:TextBox ID="NumberOfRowsTextBox" runat="server" /><br />
						</div>
					</div>
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgCount" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>