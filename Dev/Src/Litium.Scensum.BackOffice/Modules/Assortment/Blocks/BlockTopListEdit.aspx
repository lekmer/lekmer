﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockTopListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockTopListEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2"
	Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Src="~/UserControls/Tree/SelectTreeView.ascx" TagName="SelectTreeView" %>
<%@ Register TagPrefix="uc" TagName="ProductSearch" Src="~/UserControls/Assortment/ProductSearch.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

	<script type="text/javascript">
		function confirmDeleteCategories() {
			return DeleteConfirmation("<%= Resources.Campaign.Literal_CategoriesConfirmRemove %>");
		}
		function confirmDeleteProducts() {
			return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
		}
		function SelectAllBelow(mainCheckBoxId, gridId) {
			$("#" + gridId).find("input[id$='SelectCheckBox']").attr('checked', $("#" + mainCheckBoxId).attr('checked'));
		}
	</script>

	<link href="../../../Media/Css/TopList.css" rel="stylesheet" type="text/css" />
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
		runat="server" />
	<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
		ID="ValdationSummary" DisplayMode="List" ValidationGroup="vgBlockTopList" />
</asp:Content>
<asp:Content ID="BlockTopListContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="toplist-content">
			<div class="block-assortment">
				<div class="input-box">
					<span>
						<asp:Literal ID="TitleLiteral" runat="server" Text="<%$ Resources:General,Literal_Title%>" /></span>&nbsp
					<uc:GenericTranslator ID="Translator" runat="server" />
					&nbsp
					<asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" Display="None" ValidationGroup="vgBlockTopList" />
					<br />
					<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
				</div>
				<br />
				
				<div class="input-box">
					<span><asp:Literal ID="OrderStatisticsDayCountLiteral" runat="server" Text="<%$ Resources:TopList,Literal_OrderStatisticsDayCount%>" /></span>
					<asp:RangeValidator runat="server" ID="OrderStatisticsDayCountRangeValidator" ControlToValidate="OrderStatisticsDayCountTextBox"
						Type="Integer" MinimumValue="1" MaximumValue="10000" ErrorMessage="<%$ Resources:TopList, Message_OrderStatisticsDayCountWithinRange %>"
						Display="None" ValidationGroup="vgBlockTopList" />
					<asp:RequiredFieldValidator runat="server" ID="OrderStatisticsDayCountValidator" ControlToValidate="OrderStatisticsDayCountTextBox"
						ErrorMessage="<%$ Resources:TopList, Message_OrderStatisticsDayCountEmpty %>"
						Display="None" ValidationGroup="vgBlockTopList" />
					<br />
					<asp:TextBox ID="OrderStatisticsDayCountTextBox" runat="server" />
				</div>
				<br />
				
				<div class="toplist-categories">
					<asp:UpdatePanel ID="CategoriesUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:RadioButton ID="WholeRangeRadio" runat="server" Font-Bold="true" AutoPostBack="true"
								Text="<%$ Resources:TopList,Literal_WholeRange%>" GroupName="CategoriesRadioGroup" />
							<br />
							<asp:RadioButton ID="CategoriesRadio" runat="server" Font-Bold="true" AutoPostBack="true"
								Text="<%$ Resources:TopList,Literal_Categories%>" GroupName="CategoriesRadioGroup" />
							<br />
							<div id="CategoriesDiv" runat="server">
								<div>
									<sc:GridViewWithCustomPager ID="CategoriesGrid" SkinID="grid" runat="server" PageSize="<%$AppSettings:DefaultGridPageSize%>"
										AllowPaging="true" AutoGenerateColumns="false" Width="100%">
										<Columns>
											<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
												ItemStyle-Width="3%">
												<HeaderTemplate>
													<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox ID="SelectCheckBox" runat="server" />
													<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Category.Id") %>' runat="server" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="72%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Category.Title")%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderText="<%$ Resources:TopList, Literal_IncludeSubcategory %>"
												ItemStyle-HorizontalAlign="Center" ItemStyle-Width="22%">
												<ItemTemplate>
													<asp:CheckBox ID="IncludeSubcategoryCheckBox" runat="server" Checked='<%# Eval("IncludeSubcategories")%>'
														AutoPostBack="true" OnCheckedChanged="OnIncludeSubcategoriesChanged" />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
												ItemStyle-Width="3%">
												<ItemTemplate>
													<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteCategory" CommandArgument='<%# Eval("Category.Id") %>'
														UseSubmitBehaviour="true" ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
														OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_Category + "\");" %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</sc:GridViewWithCustomPager>
								</div>
								<div id="CategoriesAllSelectedDiv" runat="server" style="display: none;" class="toplist-apply-to-all-selected">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoriesRemoveSelectedButton"
										Text="<%$Resources:General, Button_RemoveSelected %>" OnClientClick="return confirmDeleteCategories();"
										runat="server" SkinID="DefaultButton" />
								</div>
								<br style="clear: both;" />
								<div class="right">
									<uc:ImageLinkButton ID="CategoriesShowPopupButton" UseSubmitBehaviour="true" runat="server"
										Text="<%$ Resources:General,Button_Add%>" SkinID="DefaultButton" CausesValidation="false" />
								</div>
								<br style="clear: both;" />
							</div>
							<ajaxToolkit:ModalPopupExtender ID="CategoriesPopup" runat="server" TargetControlID="CategoriesShowPopupButton"
								PopupControlID="CategoriesPopupPanel" CancelControlID="CategoriesCancelPopupButton"
								BackgroundCssClass="PopupBackground" />
							<div id="CategoriesPopupPanel" class="PopupContainer" style="display: none;" runat="server">
								<div class="PopupHeader">
									<span>
										<asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Product,Label_AddCategoriesToBlock%>" /></span>
									<asp:Button ID="CategoriesCancelPopupButton" CausesValidation="false" runat="server"
										Text="X" />
								</div>
								<div class="PopupBody">
									<uc:SelectTreeView runat="server" ID="CategoriesTree" />
									<br />
									<uc:ImageLinkButton ID="CategoriesOkPopupButton" UseSubmitBehaviour="true" runat="server"
										Text="Ok" SkinID="DefaultButton" CausesValidation="false" CssClass="PopupButton" />
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<br />
				<div class="toplist-products">
					<asp:UpdatePanel ID="ProductsUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="left">
								<asp:Label ID="ProductsPositionLabel" runat="server" Font-Bold="true" Text="<%$ Resources:TopList,Literal_ProductForcePosition%>"></asp:Label>
							</div>
							<br />
							<div>
								<sc:GridViewWithCustomPager ID="ProductsGrid" SkinID="grid" runat="server" PageSize="<%$AppSettings:DefaultGridPageSize%>"
									AllowPaging="true" AutoGenerateColumns="false" Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
											ItemStyle-Width="3%">
											<HeaderTemplate>
												<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox ID="SelectCheckBox" runat="server" CssClass="top-list-categories-selector" />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Product.Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="40%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Product.DisplayTitle")%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Category %>" ItemStyle-Width="36%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# Eval("Product.ShortDescription")%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:TopList, Literal_Position %>" ItemStyle-HorizontalAlign="Center"
											ItemStyle-Width="8%">
											<ItemTemplate>
												<asp:TextBox ID="PositionTextBox" runat="server" Text='<%# Eval("Position") %>' Width="20px"></asp:TextBox>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
											ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteProduct" CommandArgument='<%# Eval("Product.Id") %>'
													UseSubmitBehaviour="true" ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
													OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_product + "\");" %>' />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</sc:GridViewWithCustomPager>
							</div>
							<div id="ProductsAllSelectedDiv" runat="server" style="display: none;" class="toplist-apply-to-all-selected">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductsRemoveSelectedButton" Text="<%$Resources:General, Button_RemoveSelected %>"
									OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton" />
							</div>
							<br style="clear: both;" />
							<uc:ProductSearch runat="server" ID="ProductsSearch" />
							<br style="clear: both;" />
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<div class="content-box">
					<br class="clear" />
					<div id="Settings" runat="server" class="content-box">
						<label class="block-header">
							<asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:General,Label_Settings%>" /></label>
						<br class="clear" />
						<div class="column">
							<div class="input-box">
								<span>
									<asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate%>" /></span><br />
								<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span>
									<asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:General,Label_NumberOfColumns%>" />&nbsp;*</span>&nbsp
								<asp:RangeValidator runat="server" ID="ColumnsNumberRangeValidator" ControlToValidate="NumberOfColumnsTextBox"
									Type="Integer" MinimumValue="1" ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>"
									Display="None" ValidationGroup="vgBlockTopList"></asp:RangeValidator>
								<asp:RequiredFieldValidator runat="server" ID="ColumnsRequiredValidator" ControlToValidate="NumberOfColumnsTextBox"
									ErrorMessage="<%$ Resources:GeneralMessage, ColumnsNumberEmpty %>" Display="None"
									ValidationGroup="vgBlockTopList" />
								<br />
								<asp:TextBox ID="NumberOfColumnsTextBox" runat="server" /><br />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span>
									<asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:General,Label_NumberOfRows%>" />&nbsp;*</span>&nbsp
								<asp:RangeValidator runat="server" ID="RowsNumberRangeValidator" ControlToValidate="NumberOfRowsTextBox"
									Type="Integer" MinimumValue="1" ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberGreaterZero %>"
									Display="None" ValidationGroup="vgBlockTopList"></asp:RangeValidator>
								<asp:RequiredFieldValidator runat="server" ID="RowsRequiredValidator" ControlToValidate="NumberOfRowsTextBox"
									ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberEmpty %>" Display="None"
									ValidationGroup="vgBlockTopList" />
								<br />
								<asp:TextBox ID="NumberOfRowsTextBox" runat="server" /><br />
							</div>
						</div>
						<div class="column">
							<div class="input-box">
								<span>
									<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:TopList,Literal_TotalProductCount%>" />&nbsp;*</span>&nbsp
								<asp:RangeValidator runat="server" ID="TotalProductsRangeValidator" ControlToValidate="NumberOfProductsTextBox"
									Type="Integer" MinimumValue="0" ErrorMessage="<%$ Resources:TopList, Message_TotalProductCountNotNegative %>"
									Display="None" ValidationGroup="vgBlockTopList"></asp:RangeValidator>
								<asp:RequiredFieldValidator runat="server" ID="TotalProductsRequiredFieldValidator"
									ControlToValidate="NumberOfProductsTextBox" ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberEmpty %>"
									Display="None" ValidationGroup="vgBlockTopList" />
								<br />
								<asp:TextBox ID="NumberOfProductsTextBox" runat="server" /><br />
							</div>
						</div>
						<br class="clear" />
					</div>
				</div>
			</div>
			<br />
			<div class="right">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>"
					SkinID="DefaultButton" ValidationGroup="vgBlockTopList" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>"
					SkinID="DefaultButton" CausesValidation="false" />
			</div>
		</div>
	</asp:Panel>
</asp:Content>
