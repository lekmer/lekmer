﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockBrandProductListEdit : StatePageController<BlockBrandProductListState>, IEditor
	{
		protected override void OnInit(EventArgs e)
		{
			((Master.Start) (Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnInit(e);
		}


		protected virtual Collection<IBrand> GetSelectedBrandsState(int blockId)
		{
			if (State == null)
			{
				Collection<IBrand> brandsList = null;

				var ss = IoC.Resolve<IBlockBrandProductListBrandSecureService>();
				brandsList = ss.GetAllByBlock(ChannelHelper.CurrentChannel.Id, blockId);

				State = new BlockBrandProductListState(null, brandsList, State.Brands, State.CurrentContentNode);
			}
			return State.BlockSelectedBrands;
		}


		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			AddBrandsButton.Click += AddBrandsClick;
			RemoveBrandsButton.Click += RemoveBrandsClick;
		}


		private void AddBrandsClick(object sender, EventArgs e)
		{
			var brandsList = GetSelectedBrandsState(State.Block.Id);

			foreach (ListItem item in BrandsForSelectionListBox.Items)
			{
				if (item.Selected)
				{
					IBrand brand = IoC.Resolve<IBrandSecureService>().Create();
					brand.Id = int.Parse(item.Value, CultureInfo.CurrentCulture);
					brand.Title = item.Text;
					brandsList.Add(brand);
				}
			}
			State = new BlockBrandProductListState(State.Block, brandsList, State.Brands, State.CurrentContentNode);
			PopulateBrands();
		}

		private void RemoveBrandsClick(object sender, EventArgs e)
		{
			var brandsList = GetSelectedBrandsState(State.Block.Id);
			foreach (ListItem item in SelectedBrandsListBox.Items)
			{
				if (item.Selected)
				{
					ListItem listItem = item;
					IBrand selectedBrand =
						brandsList.FirstOrDefault(
							r => r.Id == int.Parse(listItem.Value, CultureInfo.CurrentCulture));
					brandsList.Remove(selectedBrand);
				}
			}

			State = new BlockBrandProductListState(State.Block, brandsList, State.Brands, State.CurrentContentNode);
			PopulateBrands();
		}


		private void PopulateBrands()
		{
			var allBrands = State.Brands;
			var blockBrands = GetSelectedBrandsState(State.Block.Id);
			var selectableBrands = new Collection<IBrand>();
			if (blockBrands.Count > 0)
			{
				foreach (var brand in allBrands)
				{
					IBrand brandLocal = brand;
					if (blockBrands.FirstOrDefault(r => r.Id == brandLocal.Id) == null)
					{
						selectableBrands.Add(brandLocal);
					}
				}
			}
			else
			{
				selectableBrands = allBrands;
			}

			SelectedBrandsListBox.DataSource = blockBrands;
			SelectedBrandsListBox.DataBind();
			BrandsForSelectionListBox.DataSource = selectableBrands;
			BrandsForSelectionListBox.DataBind();
		}


		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(
			                  	State.Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!base.Page.IsValid) return;

			State.Block.Title = BlockTitleTextBox.Text;

			State.Block.TemplateId = TemplateList.SelectedValue.Length.Equals(0)
			                         	? null
			                         	: (int?) int.Parse(
			                         	         	TemplateList.SelectedValue, CultureInfo.CurrentCulture);

			State.Block.ColumnCount = int.Parse(
				ColumnCountTextBox.Text, CultureInfo.CurrentCulture);

			State.Block.RowCount = int.Parse(
				RowCountTextBox.Text, CultureInfo.CurrentCulture);

			State.Block.ProductSortOrderId = int.Parse(
				ProductSortOrderIdList.SelectedValue, CultureInfo.CurrentCulture);


			var blockBrandProductListBrands =
				new Collection<IBlockBrandProductListBrand>();
			foreach (var item in State.BlockSelectedBrands)
			{
				var blockBrandProductListBrand = new BlockBrandProductListBrand();
				blockBrandProductListBrand.BlockId = State.Block.Id;
				blockBrandProductListBrand.BrandId = item.Id;
				blockBrandProductListBrands.Add(blockBrandProductListBrand);
			}
			int result = SaveBlock(State.Block, blockBrandProductListBrands);
			State = new BlockBrandProductListState(State.Block, State.BlockSelectedBrands, State.Brands, State.CurrentContentNode);
			PopulateBrands();
			if (result > 0)
			{
				SystemMessageContainer.Add(GeneralMessage.SaveSuccessBlockBrandProductList, InfoType.Success);
			}
		}


		protected override void PopulateForm()
		{
			var currentContentNode = GetContentNode();
			var block = GetBlock();
			var brands = GetBrands();
			var blockBrands = GetBlockBrands();

			if (currentContentNode == null)
			{
				throw new BusinessObjectNotExistsException("Cannot find content node.");
			}

			if (block == null)
			{
				throw new BusinessObjectNotExistsException("Cannot find block.");
			}

			BlockTitleTextBox.Text = block.Title;

			TemplateList.DataSource = GetTemplates();
			TemplateList.DataTextField = "Title";
			TemplateList.DataValueField = "Id";
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			TemplateList.SelectedValue = block.TemplateId == null
			                             	? ""
			                             	: block.TemplateId.Value.ToString(CultureInfo.CurrentCulture);

			ColumnCountTextBox.Text = block.ColumnCount.ToString(CultureInfo.CurrentCulture);

			RowCountTextBox.Text = block.RowCount.ToString(CultureInfo.CurrentCulture);

			ProductSortOrderIdList.Items.AddRange(GetProductSortOrders());
			ProductSortOrderIdList.SelectedValue =
				block.ProductSortOrderId.ToString(CultureInfo.CurrentCulture);

			State = new BlockBrandProductListState(
				block, blockBrands, brands, currentContentNode);

			PopulateBrands();
		}

		protected virtual int SaveBlock(
			IBlockBrandProductList block,
			Collection<IBlockBrandProductListBrand> blockBrands)
		{
			return IoC.Resolve<IBlockBrandProductListSecureService>().Save(
				SignInHelper.SignedInSystemUser, block, blockBrands);
		}


		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ICategorySiteStructureRegistry GetRegistry(
			int registryId, ICategoryRecord categoryRecord)
		{
			return categoryRecord.CategorySiteStructureRegistries.FirstOrDefault(
				r => r.SiteStructureRegistryId.Equals(registryId));
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IBrand GetBrand(int brandId)
		{
			return IoC.Resolve<IBrandSecureService>().GetById(brandId);
		}


		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetContentNodeId()
		{
			return Request.QueryString.GetInt32("Id");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IContentNode GetContentNode()
		{
			return IoC.Resolve<IContentNodeSecureService>().GetById(GetContentNodeId());
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IBlockBrandProductList GetBlock()
		{
			return IoC.Resolve<IBlockBrandProductListSecureService>().GetById(GetBlockId());
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IBrand> GetBlockBrands()
		{
			return IoC.Resolve<IBlockBrandProductListBrandSecureService>().GetAllByBlock(
				ChannelHelper.CurrentChannel.Id,
				GetBlockId());
		}


		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IBrand> GetBrands()
		{
			return
				IoC.Resolve<IBrandSecureService>().GetAll(
					);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ListItem[] GetProductSortOrders()
		{
			var values = Enum.GetValues(typeof (ProductSortOrderConstant));
			var items = new ListItem[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				var value = values.GetValue(i);
				items[i] = new ListItem(
					value.ToString(),
					((int) value).ToString(CultureInfo.CurrentCulture));
			}
			return items;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ITemplate> GetTemplates()
		{
			var templateService = IoC.Resolve<ITemplateSecureService>();
			return templateService.GetAllByModel("BlockBrandProductList");
		}
	}

	[Serializable]
	public class BlockBrandProductListState
	{
		private readonly IBlockBrandProductList _block;

		public IBlockBrandProductList Block
		{
			get { return _block; }
		}

		private readonly Collection<IBrand> _blockSelectedBrands =
			new Collection<IBrand>();

		public Collection<IBrand> BlockSelectedBrands
		{
			get { return _blockSelectedBrands; }
		}

		private readonly Collection<IBrand> _brands =
			new Collection<IBrand>();

		public Collection<IBrand> Brands
		{
			get { return _brands; }
		}


		private readonly IContentNode _currentContentNode;

		public IContentNode CurrentContentNode
		{
			get { return _currentContentNode; }
		}

		public BlockBrandProductListState(
			IBlockBrandProductList block,
			Collection<IBrand> blockBrands,
			Collection<IBrand> brands,
			IContentNode currentContentNode)
		{
			_block = block;
			_blockSelectedBrands = blockBrands;
			_brands = brands;
			_currentContentNode = currentContentNode;
		}
	}
}