﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="BlockAvailCombineEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockAvailCombineEdit" %>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgCount" />
	</asp:Content>
<asp:Content ID="cBPRL" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_Title %>"/></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator 
				runat="server" 
				ID="BlockTitleValidator"
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"  
				Display ="None" ValidationGroup="vgCount" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br /><br />
			<span><asp:Literal runat = "server" Text="<%$ Resources:Product,Literal_AvailTypes%>" /></span>
			<div class="input-box">
				<asp:GridView
					ID="RelationsGrid" 
					SkinID="grid" 
					runat="server" 
					Width="99%"
					AutoGenerateColumns="false">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
							<HeaderTemplate>
								<asp:CheckBox ID="AllRelationsSelectCheckBox" runat="server" ToolTip="<%$ Resources:General,Literal_SelectAll%>" />
							</HeaderTemplate>	
							<ItemTemplate>
								<asp:CheckBox ID="RelationSelectCheckBox" runat="server" />							
							</ItemTemplate>
						</asp:TemplateField>								
						
							<asp:TemplateField HeaderText="Type" ItemStyle-Width="95%">
							<HeaderTemplate>							
							</HeaderTemplate>	
							<ItemTemplate>
									<asp:Label id="AvailType" runat="server" />					
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
			<br />
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
						<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate %>" /></span><br />
						<asp:DropDownList ID="TemplateList"  runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" />
						</div>
					</div>
						<div class="column">
						<div class="input-box">
							<span><asp:Literal ID="Literal1" runat="server" Text= "<%$ Resources:General,Label_NumberOfColumns%>" />&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="ColumnsCompareValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>"  
								Display ="None" 
								ValidationGroup="vgBlockProductList"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="ColumnsRequiredValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage, ColumnsNumberEmpty %>" 
								Display ="None"
								ValidationGroup="vgBlockProductList" />
							<br />
							<asp:TextBox ID="NumberOfColumnsTextBox" runat="server" /><br />							
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><asp:Literal ID="Literal2" runat="server" Text= "<%$ Resources:General,Label_NumberOfRows%>" />&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="RowsCompareValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberGreaterZero %>" 
								Display ="None" 
								ValidationGroup="vgBlockProductList"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="RowsRequiredValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberEmpty %>" 
								Display ="None" 
								ValidationGroup="vgBlockProductList" />
							<br />
							<asp:TextBox ID="NumberOfRowsTextBox" runat="server" /><br />							
						</div>
					</div>
					
						<div class="column">
						<div class="input-box">
							<span><asp:Literal ID="Literal3" runat="server" Text= "<%$ Resources:General,Label_GetClickHistoryFromCookie%>" /></span>
							<br />	
							<asp:CheckBox ID="GetClickHistoryFromCookieCheckBox" runat="server" />	
							<br />						
						</div>
					</div>
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgCount" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>"  SkinID="DefaultButton" CausesValidation="false" />
		</div>
		</asp:Panel>
</asp:Content>
