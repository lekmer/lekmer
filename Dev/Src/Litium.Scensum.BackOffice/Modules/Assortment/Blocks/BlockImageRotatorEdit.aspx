﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="BlockImageRotatorEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockImageRotatorEdit" %>

<%@ Register TagPrefix="uc" TagName="ProductMedia" Src="~/UserControls/Assortment/ImageRotator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgCount" />
	</asp:Content>
<asp:Content ID="cBPRL" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><asp:Literal ID="Literal1" runat="server" Text= "<%$ Resources:General,Literal_Title %>"/></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator 
				runat="server" 
				ID="BlockTitleValidator"
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"  
				Display ="None" ValidationGroup="vgCount" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
			<br /><br />
			<div >
			
				<uc:ProductMedia ID="ProductMediaControl" runat="server" />
			</div>
			<br />
			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings%></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
				<div class="input-box">
					<div class="column">
						<div class="input-box">
						<span><asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate %>" /></span><br />
						<asp:DropDownList ID="TemplateList"  runat="server" DataTextField="Title" DataValueField="Id" style="margin-top: 5px;" Width="180" />
						</div>
					</div>
					
					<div class="column">
					<div class="input-box">
			            <span><%= Resources.Lekmer.Literal_SecondaryTemplate %></span><br />
			            <asp:DropDownList ID="SecondaryTemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="180" style="margin-top: 5px;" />
			            	</div>
		            </div>			
					</div>
				</div>
			</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgCount" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>"  SkinID="DefaultButton" CausesValidation="false" />
		</div>
		</asp:Panel>
</asp:Content>