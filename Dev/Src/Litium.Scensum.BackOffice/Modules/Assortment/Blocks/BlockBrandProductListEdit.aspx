﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" AutoEventWireup="true" CodeBehind="BlockBrandProductListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockBrandProductListEdit" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MessageContainer" runat="server">
		
	<uc:MessageContainer 
	    ID="SystemMessageContainer" 
	    MessageType="Failure" 
	    HideMessagesControlId="SaveButton" 
	    runat="server" 
	/>
	<uc:ScensumValidationSummary 
	    ForeColor="Black" 
	    runat="server" 
	    CssClass="advance-validation-summary" 
	    ID="ValdationSummary" 
	    DisplayMode="List" 
	    ValidationGroup="BlockBrandProductList" 
	/>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<script type="text/javascript">
        function confirmDelete() {
           return confirm("<%= Resources.ProductMessage.BrandConfirmDelete %>");
        }
    </script>
    <br /><br />
    <div>
        <span><asp:Literal ID="Literal1"  runat="server" Text= "<%$ Resources:General,Literal_Title%>"/></span>&nbsp
        <br />
        <asp:TextBox ID="BlockTitleTextBox" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator 
            ID="BlockTitleValidator"
            runat="server" 
            ControlToValidate="BlockTitleTextBox" 
            ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty%>" 
            Display ="None"
            ValidationGroup="BlockBrandProductList"  
        />
    </div>
	<br />
    <div style="float:left;">
        <span class="block-header"></span>
    </div>
   
    <br />
    <div>
    
    	<div class= "user-roles">
					<label><h5><asp:Literal ID="Literal2"  runat="server" Text="<%$ Resources:Product,Literal_Brands%>" /></h5></label>
						<div class = "user-roles-list-box">
						  <label><asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Product, Literal_AvailableBrands %>"/></label>
						  <br />
						  <asp:ListBox id="BrandsForSelectionListBox" DataTextField="Title" DataValueField="Id"
						   Rows="10"
						   Width="230px"
						   SelectionMode ="Multiple"
						   runat="server">
						   </asp:ListBox>
						</div>
						<div class = "user-roles-button">
						<uc:ImageLinkButton ID="AddBrandsButton" runat="server" Text=">>" SkinID="DefaultButton" />
						<br />
						<br />
						<uc:ImageLinkButton ID="RemoveBrandsButton" runat="server" Text="<<" SkinID="DefaultButton" />
						</div>
						<div class = "user-roles-list-box">
						  <label><asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Product, Literal_SelectedBrands %>" /></label>
						  <br />
						  <asp:ListBox id="SelectedBrandsListBox" DataTextField="Title" DataValueField="Id"
						   Rows="10"
						   Width="230px"
						   SelectionMode ="Multiple"
						   runat="server">
						   </asp:ListBox>
						</div>
					</div>
    
    
 
    </div>
    <br />
    <div style="clear: both"></div>
    <span class="block-header"><asp:Literal ID="Literal3"  runat="server" Text="<%$ Resources:General, Label_Settings%>" /></span>
    <br /><br />
    <div class="FormInput" style="float:left;">
        <span><asp:Literal ID="Literal4"  runat="server" Text="<%$ Resources:General, Label_Template%>" /></span>
        <br />
        <asp:DropDownList ID="TemplateList" runat="server"></asp:DropDownList>
    </div>
    <div class="FormInput" style="float:left;">

            <span><asp:Literal ID="Literal5"  runat="server" Text="<%$ Resources:General,Label_NumberOfColumns %>" />&nbsp;*</span>
            <br />
            <asp:TextBox ID="ColumnCountTextBox" runat="server"></asp:TextBox>
            <asp:CompareValidator 
                runat="server" 
                ID="ColumnCountCompareValidator" 
                ControlToValidate="ColumnCountTextBox" 
                ValueToCompare="0" 
                Type="Integer" 
                Operator="GreaterThan" 
                ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>" 
                Display ="None"
                ValidationGroup="BlockBrandProductList" 
            />
            <asp:RequiredFieldValidator 
                ID="ColumnCountValidator"
                runat="server" 
                ControlToValidate="ColumnCountTextBox" 
                ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberEmpty %>" 
                Display ="None"
                ValidationGroup="BlockBrandProductList"  
            />
            
    </div>
    <div class="FormInput" style="float:left;">
            <span><asp:Literal ID="Literal6"  runat="server" Text="<%$ Resources:General,Label_NumberOfRows %>" />&nbsp;*</span>
            <br />
            <asp:TextBox ID="RowCountTextBox" runat="server"></asp:TextBox>
            <asp:CompareValidator 
                runat="server" 
                ID="RowCountCompareValidator" 
                ControlToValidate="RowCountTextBox" 
                ValueToCompare="0" 
                Type="Integer" 
                Operator="GreaterThan" 
                ErrorMessage="<%$ Resources:GeneralMessage,RowsNumberGreaterZero %>" 
                Display ="None"
                ValidationGroup="BlockCategoryProductList" 
            />
            <asp:RequiredFieldValidator 
                ID="RowCountValidator"
                runat="server" 
                ControlToValidate="RowCountTextBox" 
                ErrorMessage="<%$ Resources:GeneralMessage,RowsNumberEmpty %>" 
                Display ="None"
                ValidationGroup="BlockCategoryProductList"  
            />
    </div>
    <div class="FormInput" style="float:left;">
            <span><asp:Literal ID="Literal7"  runat="server" Text="<%$ Resources:General,Label_SortBy %>" /></span>
            <br />
            <asp:DropDownList ID="ProductSortOrderIdList" runat="server"></asp:DropDownList>
    </div>
    <br />
    <br />
    <br />
    <br />
	<div style="float:right;">
		<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="BlockCategoryProductList"/>
		<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
	</div>
	

	
</asp:Content>

