﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" AutoEventWireup="true" CodeBehind="BlockCategoryProductListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockCategoryProductListEdit" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Src="~/UserControls/Tree/SelectTreeView.ascx" TagName="SelectTreeView" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MessageContainer" runat="server">
		
	<uc:MessageContainer 
	    ID="SystemMessageContainer" 
	    MessageType="Failure" 
	    HideMessagesControlId="SaveButton" 
	    runat="server" 
	/>
	<uc:ScensumValidationSummary 
	    ForeColor="Black" 
	    runat="server" 
	    CssClass="advance-validation-summary" 
	    ID="ValdationSummary" 
	    DisplayMode="List" 
	    ValidationGroup="BlockCategoryProductList" 
	/>

</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<script type="text/javascript">
        function confirmDelete() {
           return confirm("<%= Resources.ProductMessage.CategoryConfirmDelete %>");
        }
    </script>
    <br /><br />
    <div>
        <span><asp:Literal  runat="server" Text= "<%$ Resources:General,Literal_Title%>"/></span>&nbsp
        <uc:GenericTranslator ID="Translator" runat="server" />
        <br />
        <asp:TextBox ID="BlockTitleTextBox" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator 
            ID="BlockTitleValidator"
            runat="server" 
            ControlToValidate="BlockTitleTextBox" 
            ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty%>" 
            Display ="None"
            ValidationGroup="BlockCategoryProductList"  
        />
    </div>
	<br />
    <div style="float:left;">
        <span class="block-header"><asp:Literal  runat="server" Text="<%$ Resources:Product,Label_Categories%>" /></span>
    </div>
    <div style="float:right;">
		<uc:ImageLinkButton ID="ShowCategoryPopupButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Add%>" SkinID="DefaultButton" CausesValidation="false" />
    </div>
    <br /><br />
    <div>
        <asp:GridView 
            ID="CategoryGrid" 
            AutoGenerateColumns="false" 
            OnRowDataBound="CategoryGrid_RowDataBound" 
            OnRowCommand="CategoryGrid_RowCommand" 
            SkinID="grid"
            Width="100%"
            runat="server">
            <Columns>
                <asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title%> ">
                    <ItemTemplate>
                        <asp:HiddenField ID="CategoryIdHiddenField" runat="server" />
                        <asp:Literal ID="TitleLiteral" runat="server"></asp:Literal>                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Product,Label_ParentContentNode%> ">
                    <ItemTemplate>
                        <asp:Literal ID="ParentContentNodeLiteral" runat="server"></asp:Literal>                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Product,Label_SetParentContentNode%>">
                    <ItemTemplate>
                        <asp:CheckBox ID="SetNodeCheckBox" runat="server" AutoPostBack="true" OnCheckedChanged="SetNodeCheckBoxChanged" />                     
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:Product,Label_IncludeSubCategories%>">
                    <ItemTemplate>
                        <asp:CheckBox ID="IncludeSubcategoriesCheckBox" runat="server" />                     
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField 
                    HeaderStyle-HorizontalAlign="Center" 
                    ItemStyle-HorizontalAlign="Center"
				    ItemStyle-Width="5%">
				    <ItemTemplate>
					    <asp:ImageButton 
				            runat="server" 
				            ID="DeleteButton" 
				            CommandArgument='<%# Eval("CategoryId") %>' 
				            ImageUrl="~/Media/Images/Common/delete.gif" 
				            AlternateText="<%$ Resources:General, Button_Delete%>" 
				            OnClientClick="return confirmDelete();"  />
				    </ItemTemplate>
				</asp:TemplateField>
            </Columns>
            
        </asp:GridView>
    </div>
    <br />
    <span class="block-header"><asp:Literal  runat="server" Text="<%$ Resources:General, Label_Settings%>" /></span>
    <br /><br />
    <div class="FormInput" style="float:left;">
        <span><asp:Literal  runat="server" Text="<%$ Resources:General, Label_Template%>" /></span>
        <br />
        <asp:DropDownList ID="TemplateList" runat="server"></asp:DropDownList>
    </div>
    <div class="FormInput" style="float:left;">

            <span><asp:Literal  runat="server" Text="<%$ Resources:General,Label_NumberOfColumns %>" />&nbsp;*</span>
            <br />
            <asp:TextBox ID="ColumnCountTextBox" runat="server"></asp:TextBox>
            <asp:CompareValidator 
                runat="server" 
                ID="ColumnCountCompareValidator" 
                ControlToValidate="ColumnCountTextBox" 
                ValueToCompare="0" 
                Type="Integer" 
                Operator="GreaterThan" 
                ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>" 
                Display ="None"
                ValidationGroup="BlockCategoryProductList" 
            />
            <asp:RequiredFieldValidator 
                ID="ColumnCountValidator"
                runat="server" 
                ControlToValidate="ColumnCountTextBox" 
                ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberEmpty %>" 
                Display ="None"
                ValidationGroup="BlockCategoryProductList"  
            />
            
    </div>
    <div class="FormInput" style="float:left;">
            <span><asp:Literal  runat="server" Text="<%$ Resources:General,Label_NumberOfRows %>" />&nbsp;*</span>
            <br />
            <asp:TextBox ID="RowCountTextBox" runat="server"></asp:TextBox>
            <asp:CompareValidator 
                runat="server" 
                ID="RowCountCompareValidator" 
                ControlToValidate="RowCountTextBox" 
                ValueToCompare="0" 
                Type="Integer" 
                Operator="GreaterThan" 
                ErrorMessage="<%$ Resources:GeneralMessage,RowsNumberGreaterZero %>" 
                Display ="None"
                ValidationGroup="BlockCategoryProductList" 
            />
            <asp:RequiredFieldValidator 
                ID="RowCountValidator"
                runat="server" 
                ControlToValidate="RowCountTextBox" 
                ErrorMessage="<%$ Resources:GeneralMessage,RowsNumberEmpty %>" 
                Display ="None"
                ValidationGroup="BlockCategoryProductList"  
            />
    </div>
    <div class="FormInput" style="float:left;">
            <span><asp:Literal  runat="server" Text="<%$ Resources:General,Label_SortBy %>" /></span>
            <br />
            <asp:DropDownList ID="ProductSortOrderIdList" runat="server"></asp:DropDownList>
    </div>
    <br />
    <br />
    <br />
    <br />
	<div style="float:right;">
		<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="BlockCategoryProductList"/>
		<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
	</div>
	
	<ajaxToolkit:ModalPopupExtender 
		ID="CategoryPopup" 
		runat="server"
		TargetControlID="ShowCategoryPopupButton"
		PopupControlID="CategoryPopupPanel" 
		CancelControlID="CancelCategoryPopupButton"
		BackgroundCssClass="PopupBackground" />
		
	<div id="CategoryPopupPanel" class="PopupContainer" style="display:none;" runat="server">
	    <div class="PopupHeader">
	        <span><asp:Literal  runat="server" Text="<%$ Resources:Product,Label_AddCategoriesToBlock%>" /></span>
			<asp:Button ID="CancelCategoryPopupButton" CausesValidation="false" runat="server" Text="X" />
		</div>
		<div class="PopupBody">
			<div class="TreeContainer">
			<uc:SelectTreeView runat="server" ID="CategoryTree" />
			</div>
	        <br />
		    <uc:ImageLinkButton 
		        ID="OkCategoryPopupButton" 
		        UseSubmitBehaviour="true" 
		        runat="server" 
		        Text="Ok" 
		        SkinID="DefaultButton" 
		        CausesValidation="false" 
		        CssClass="PopupButton" />
		    <br style="clear:both;" />
		</div>
	</div>
	
</asp:Content>
