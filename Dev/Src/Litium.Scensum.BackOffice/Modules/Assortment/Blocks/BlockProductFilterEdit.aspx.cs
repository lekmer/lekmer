﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using System.Globalization;
using Litium.Scensum.Product;
using Litium.Scensum.Foundation;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Template;
using Litium.Scensum.Core;
using Litium.Scensum.BackOffice.UserControls;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Lekmer.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
    public partial class BlockProductFilterEdit : StatePageController<Collection<int>>, IEditor
    {
        private const string EmptyItemValue = "0";
        private const string SortOptionEmptyItemValue = "";

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual int GetBlockId()
        {
            return Request.QueryString.GetInt32("BlockId");
        }

        private IBlockProductFilter _block;
        private IBlockProductFilter GetBlock()
        {
            if (_block == null)
            {
                _block = IoC.Resolve<IBlockProductFilterSecureService>().GetByIdSecure(GetBlockId());
            }
            return _block;
        }

        protected override void OnInit(EventArgs e)
        {
            ((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
            base.OnInit(e);
        }
        protected override void SetEventHandlers()
        {
            CancelButton.Click += OnCancel;
            SaveButton.Click += OnSave;
            CategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
            AddBrandsButton.Click += AddBrandsClick;
            RemoveBrandsButton.Click += RemoveBrandsClick;
            TagRepeater.ItemDataBound += OnTagRepeaterItemDataBound;
        }

        protected override void PopulateForm()
        {
            var block = GetBlock();

            State = block.DefaultBrandIds;
            PopulateSortOptionDropDownList(SortList, Enum.GetValues(typeof(SortOption)).Cast<SortOption>(), block.DefaultSort);
            BlockTitleTextBox.Text = block.Title;
            ProductListCookieTextBox.Text = block.ProductListCookie;
            PopulateCategory(block.DefaultCategoryId);

            var templates = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockProductFilter");
            PopulateDropDownList(PrimaryTemplateList, templates, block.TemplateId);
            PopulateDropDownList(SecondaryTemplateList, templates, block.SecondaryTemplateId);

            var ageIntervals = IoC.Resolve<IAgeIntervalSecureService>().GetAll();
            PopulateDropDownList(AgeIntervalList, ageIntervals, block.DefaultAgeIntervalId);

            PopulatePriceIntervalList(block.DefaultPriceIntervalId);

            PopulateBrands(block.DefaultBrandIds);

            PopulateTags();

            var sizes = IoC.Resolve<ISizeSecureService>().GetAll();
            PopulateDropDownList(SizeList, sizes, block.DefaultSizeId);
        }
        private void PopulateCategory(int? id)
        {
            if (id != null)
            {
                CategoryNodeSelector.SelectedNodeId = id;
            }
            CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(id);
            CategoryNodeSelector.DataBind();
            CategoryNodeSelector.PopulatePath(id);
        }
        private static void PopulateDropDownList(DropDownList dropDownList, object list, int? selectedId)
        {
            dropDownList.DataSource = list;
            dropDownList.DataBind();
            var emptyItem = new ListItem(string.Empty, EmptyItemValue);
            dropDownList.Items.Insert(0, emptyItem);
            if (!selectedId.HasValue)
            {
                emptyItem.Selected = true;
            }
            else
            {
                var selectedItem = dropDownList.Items.FindByValue(selectedId.ToString());
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                }
            }
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private static void PopulateDropDownList(DropDownList dropDownList, object list, string selectedId)
        {
            dropDownList.DataSource = list;
            dropDownList.DataBind();
            var emptyItem = new ListItem(string.Empty, EmptyItemValue);
            dropDownList.Items.Insert(0, emptyItem);
            if (string.IsNullOrEmpty(selectedId))
            {
                emptyItem.Selected = true;
            }
            else
            {
                var selectedItem = dropDownList.Items.FindByValue(selectedId.ToString());
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                }
            }
        }
        private static void PopulateSortOptionDropDownList(DropDownList dropDownList, object list, string selectedId)
        {
            dropDownList.DataSource = list;
            dropDownList.DataBind();
            var emptyItem = new ListItem(string.Empty, SortOptionEmptyItemValue);
            dropDownList.Items.Insert(0, emptyItem);
            if (string.IsNullOrEmpty(selectedId))
            {
                emptyItem.Selected = true;
            }
            else
            {
                var selectedItem = dropDownList.Items.FindByValue(selectedId.ToString());
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                }
            }
        }
        private void PopulatePriceIntervalList(int? selectedId)
        {
            var priceIntervals = IoC.Resolve<IPriceIntervalSecureService>().GetAll();

            PriceIntervalExtendedList.DataSource = priceIntervals;
            PriceIntervalExtendedList.DataBind();
            var emptyItem = new ExtendedListItem(string.Empty, EmptyItemValue, ListItemGroupingType.None);
            PriceIntervalExtendedList.ExtendedItems.Insert(0, emptyItem);
            if (!selectedId.HasValue)
            {
                emptyItem.Selected = true;
            }
            else
            {
                var selectedItem = PriceIntervalExtendedList.Items.FindByValue(selectedId.ToString());
                if (selectedItem != null)
                {
                    selectedItem.Selected = true;
                }
            }
        }
        private void PopulateBrands(Collection<int> selectedBrandIds)
        {
            var brandSecureService = IoC.Resolve<IBrandSecureService>();
            var allBrands = brandSecureService.GetAll();

            var availableBrands = allBrands.Where(brand => !selectedBrandIds.Contains(brand.Id));
            AvailableBrandsListBox.DataSource = availableBrands;
            AvailableBrandsListBox.DataBind();

            var selectedBrands = allBrands.Where(brand => selectedBrandIds.Contains(brand.Id));
            SelectedBrandsListBox.DataSource = selectedBrands;
            SelectedBrandsListBox.DataBind();
        }

        private void PopulateTags()
        {
            var tagGroups = IoC.Resolve<ITagGroupSecureService>().GetAll();
            TagRepeater.DataSource = tagGroups;
            TagRepeater.DataBind();
        }

        private void OnTagRepeaterItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var tagGroup = (TagGroup)e.Item.DataItem;
            var tags = IoC.Resolve<ITagSecureService>().GetAllByTagGroup(tagGroup.Id);
            var taglist = (DropDownList)e.Item.FindControl("TagsList");
            taglist.DataSource = tags;
            taglist.DataBind();
            var emptyItem = new ListItem(string.Empty, EmptyItemValue);
            taglist.Items.Insert(0, emptyItem);

            var selectedTag = tags.SingleOrDefault(t => GetBlock().DefaultTagIds.Contains(t.Id));
            if (selectedTag == null)
            {
                emptyItem.Selected = true;
            }
            else
            {
                var selectedListItem = taglist.Items.FindByValue(selectedTag.Id.ToString(CultureInfo.InvariantCulture));
                if (selectedListItem != null)
                {
                    selectedListItem.Selected = true;
                }
            }
        }
        protected void OnCategoryNodeCommand(object sender, CommandEventArgs e)
        {
            var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
            CategoryNodeSelector.DataSource = IoC.Resolve<ICategorySecureService>().GetTree(nodeId);
            CategoryNodeSelector.DataBind();
        }
        private void AddBrandsClick(object sender, EventArgs e)
        {
            Collection<int> brandsList = State;
            foreach (ListItem item in AvailableBrandsListBox.Items)
            {
                if (item.Selected)
                {
                    var brandId = int.Parse(item.Value, CultureInfo.CurrentCulture);
                    brandsList.Add(brandId);
                }
            }
            State = brandsList;
            PopulateBrands(brandsList);
        }
        private void RemoveBrandsClick(object sender, EventArgs e)
        {
            Collection<int> brandsList = State;
            foreach (ListItem item in SelectedBrandsListBox.Items)
            {
                if (item.Selected)
                {
                    ListItem listItem = item;
                    var brandId = int.Parse(listItem.Value, CultureInfo.CurrentCulture);
                    brandsList.Remove(brandId);
                }
            }
            State = brandsList;
            PopulateBrands(brandsList);
        }

        public virtual void OnCancel(object sender, EventArgs e)
        {
            var blockProductFilterSecureService = IoC.Resolve<IBlockProductFilterSecureService>();
            var blockProductFilter = blockProductFilterSecureService.GetByIdSecure(GetBlockId());
            Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductFilter.ContentNodeId));
        }
        public virtual void OnSave(object sender, EventArgs e)
        {
            var blockProductFilterSecureService = IoC.Resolve<IBlockProductFilterSecureService>();
            var blockProductFilter = blockProductFilterSecureService.GetByIdSecure(GetBlockId());
            if (blockProductFilter == null)
            {
                throw new BusinessObjectNotExistsException(GetBlockId());
            }
            blockProductFilter.ProductListCookie = ProductListCookieTextBox.Text;
            blockProductFilter.DefaultSort = SortList.SelectedValue;

            blockProductFilter.Title = BlockTitleTextBox.Text;
            blockProductFilter.DefaultCategoryId = CategoryNodeSelector.SelectedNodeId;
            blockProductFilter.TemplateId = GetDropDownListValue(PrimaryTemplateList);
            blockProductFilter.SecondaryTemplateId = GetDropDownListValue(SecondaryTemplateList);
            blockProductFilter.DefaultAgeIntervalId = GetDropDownListValue(AgeIntervalList);
            blockProductFilter.DefaultPriceIntervalId = GetDropDownListValue(PriceIntervalExtendedList);
            blockProductFilter.DefaultBrandIds = State;
            blockProductFilter.DefaultTagIds = GetSelectedTags();
            blockProductFilter.DefaultSizeId = GetDropDownListValue(SizeList);

            blockProductFilter.Id = blockProductFilterSecureService.Save(SignInHelper.SignedInSystemUser, blockProductFilter);
            if (blockProductFilter.Id == -1)
            {
                SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
            }
            else
            {
                SystemMessageContainer.Add(Resources.LekmerMessage.SaveSuccessBlockProductFilter);
                SystemMessageContainer.MessageType = InfoType.Success;
            }
        }
        private static int? GetDropDownListValue(DropDownList dropDownList)
        {
            var selectedValue = dropDownList.SelectedValue;
            if (selectedValue == EmptyItemValue)
            {
                return null;
            }
            int value;
            if (int.TryParse(dropDownList.SelectedValue, out value))
            {
                return value;
            }
            return null;
        }

        private Collection<int> GetSelectedTags()
        {
            var tags = new Collection<int>();
            foreach (RepeaterItem repeaterItem in TagRepeater.Items)
            {
                var tagsList = (DropDownList)repeaterItem.FindControl("TagsList");
                var selectedValue = tagsList.SelectedValue;
                if (selectedValue != EmptyItemValue)
                {
                    tags.Add(int.Parse(selectedValue, CultureInfo.InvariantCulture));
                }
            }
            return tags;
        }
    }
}