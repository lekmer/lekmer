using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductListEdit : StatePageController<Collection<IBlockProductListProduct>>, IEditor 
	{
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			BlockTitleTextBox.Text = IoC.Resolve<IBlockProductListSecureService>().GetById(GetBlockId()).Title;
		}
		protected override void OnLoad(EventArgs e)
		{
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			base.OnLoad(e);
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}
		protected override void PopulateForm()
		{
			PopulateData();
			PopulateTranslation(GetBlockId());

			var blockService = IoC.Resolve<IBlockProductListProductSecureService>();
			BlockProductSeach.BlockProducts = blockService.GetAllByBlock(ChannelHelper.CurrentChannel.Id, GetBlockId());
			
			var master = Master as Pages;
			if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductListEdit);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockProductList blockProductList = IoC.Resolve<IBlockProductListSecureService>().GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductList.ContentNodeId));
		}
		
		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateCount() || !ValidateOrdinals())
			{
				SystemMessageContainer.Add(Resources.ProductMessage.ProductOrdinalsBlockRowsColumnsFailed);
				return;
			}
			IBlockProductList blockProductList = IoC.Resolve<IBlockProductListSecureService>().GetById(GetBlockId());
			if (blockProductList == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}
			blockProductList.ColumnCount = int.Parse(NumberOfColumnsTextBox.Text, CultureInfo.CurrentCulture);
			blockProductList.RowCount = int.Parse(NumberOfRowsTextBox.Text, CultureInfo.CurrentCulture);
			int templateId;
			blockProductList.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductList.Title = BlockTitleTextBox.Text;
			var blockProductListSecureService = IoC.Resolve<IBlockProductListSecureService>();
			blockProductListSecureService.Save(SignInHelper.SignedInSystemUser, blockProductList, BlockProductSeach.BlockProducts);
			if (blockProductList.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductList);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}
		
		protected void PopulateData()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockProductList");
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			IBlockProductList blockProductList = IoC.Resolve<IBlockProductListSecureService>().GetById(GetBlockId());
			ListItem item = TemplateList.Items.FindByValue(blockProductList.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			NumberOfColumnsTextBox.Text = blockProductList.ColumnCount.ToString(CultureInfo.CurrentCulture);
			NumberOfRowsTextBox.Text = blockProductList.RowCount.ToString(CultureInfo.CurrentCulture);
		}

		protected bool ValidateOrdinals()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}

		protected bool ValidateCount()
		{
			Page.Validate("vgCount");
			return Page.IsValid;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}
