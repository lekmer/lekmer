<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="BlockProductListEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductListEdit" %>
<%@ Register TagPrefix="uc" TagName="BlockProducts" Src="~/UserControls/Assortment/BlockProductListProducts.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer ID="SystemMessageContainer"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValdationSummary" DisplayMode="List"  ValidationGroup="vgBlockProductList" />
</asp:Content>
<asp:Content ID="ProductListBlockCreateContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="block-assortment">
			<div class="input-box">
				<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_Title%>"/></span>&nbsp
				<uc:GenericTranslator ID="Translator" runat="server" />&nbsp
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="BlockTitleValidator" 
					ControlToValidate="BlockTitleTextBox" 
					ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>"  
					Display ="None" 
					ValidationGroup="vgBlockProductList"  />
				<br />	
				<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" ></asp:TextBox>
			</div>
			<div class="input-box">
			<br class="clear" />
			    <label class="block-header"><asp:Literal  runat="server" Text= "<%$ Resources:Product,Literal_Products%>"/></label>
			    <uc:BlockProducts ID="BlockProductSeach" runat="server" />
			</div>
			<div class="content-box">				
				<br class="clear" />
				<div id="Settings" runat="server" class="content-box">
					<label class="block-header"><asp:Literal  runat="server" Text= "<%$ Resources:General,Label_Settings%>" /></label>
					<br class="clear" />
					<div class="column">
						<div class="input-box">
							<span><asp:Literal runat="server" Text= "<%$ Resources:General,Literal_ChooseTemplate%>" /></span><br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><asp:Literal runat="server" Text= "<%$ Resources:General,Label_NumberOfColumns%>" />&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="ColumnsCompareValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage,ColumnsNumberGreaterZero %>"  
								Display ="None" 
								ValidationGroup="vgBlockProductList"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="ColumnsRequiredValidator" 
								ControlToValidate="NumberOfColumnsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage, ColumnsNumberEmpty %>" 
								Display ="None"
								ValidationGroup="vgBlockProductList" />
							<br />
							<asp:TextBox ID="NumberOfColumnsTextBox" runat="server" /><br />							
						</div>
					</div>
					<div class="column">
						<div class="input-box">
							<span><asp:Literal runat="server" Text= "<%$ Resources:General,Label_NumberOfRows%>" />&nbsp;*</span>&nbsp
							<asp:CompareValidator runat="server" 
								ID="RowsCompareValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ValueToCompare="0" 
								Type="Integer" 
								Operator="GreaterThan" 
								ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberGreaterZero %>" 
								Display ="None" 
								ValidationGroup="vgBlockProductList"/>
							<asp:RequiredFieldValidator runat="server" 
								ID="RowsRequiredValidator" 
								ControlToValidate="NumberOfRowsTextBox" 
								ErrorMessage="<%$ Resources:GeneralMessage, RowsNumberEmpty %>" 
								Display ="None" 
								ValidationGroup="vgBlockProductList" />
							<br />
							<asp:TextBox ID="NumberOfRowsTextBox" runat="server" /><br />							
						</div>
					</div>
					<br class="clear" />
				</div>
			</div>
		</div>
		<br />
		<div id="product-list-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="vgBlockProductList"/>
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
			</asp:Panel>
</asp:Content>