﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" AutoEventWireup="true" CodeBehind="BlockProductFilterEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductFilterEdit" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="scensum" TagName="CategoryNodeSelector" Src="~/UserControls/Tree/CategoryNodeSelector.ascx" %>
<%@ Register TagPrefix="scensum" Namespace="Litium.Scensum.BackOffice.UserControls" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" />
	<uc:ScensumValidationSummary runat="server" ID="ValdationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="BlockBrandListValidationGroup" />
</asp:Content>

<asp:Content runat="server" ID="BlockProductFilterContent" ContentPlaceHolderID="SiteStructureForm">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		
		<br />
        <div class="column" style="width:223px;">
            <span><%= Resources.General.Literal_Title %></span>&nbsp;
            <asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="BlockProductFilterValidationGroup" />
            <br />	
            <asp:TextBox ID="BlockTitleTextBox" Width="180px" runat="server" ></asp:TextBox>
        </div>
		<br clear="all" /><br />
		
	   <div class="column" style="width:223px;">
            <span><%= Resources.Lekmer.Literal_ProductListCookie%></span>
                 <br />	
            <asp:TextBox ID="ProductListCookieTextBox" Width="180px" runat="server" ></asp:TextBox>
        </div>
		<br clear="all" /><br />
		<div class="column" style="width:223px;">
		
			<span><%= Resources.Lekmer.Literal_DefaultSort%></span><br />
			<asp:DropDownList ID="SortList" runat="server" Width="180" />
		</div>
		
		
		<div class="column" style="width:223px;">
			<span><%= Resources.Lekmer.Literal_PrimaryTemplate %></span><br />
			<asp:DropDownList ID="PrimaryTemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="180" />
		</div>
		
		<div class="column">
			<span><%= Resources.Lekmer.Literal_SecondaryTemplate %></span><br />
			<asp:DropDownList ID="SecondaryTemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="180" />
		</div>
		<br clear="all" /><br /><br /><br />
		
		<strong><%= Resources.Lekmer.Literal_DefaultFilterSettings %></strong>
		<br clear="all" /><br />
		
		<div class="column" style="width:223px;">
			<%= Resources.Product.Literal_Category %>
			<scensum:CategoryNodeSelector ID="CategoryNodeSelector" runat="server" Width="155" />
		</div>
		<div class="column" style="width:223px;">
			<span><%= Resources.Lekmer.Literal_AgeInterval %></span><br />
			<asp:DropDownList ID="AgeIntervalList" runat="server" DataTextField="Title" DataValueField="Id" Width="180" />
		</div>
		<div class="column">
			<span><%= Resources.Lekmer.Literal_PriceInterval %></span><br />			
			<scensum:PriceIntervalDropDownList runat="server" ID="PriceIntervalExtendedList" Width="180"></scensum:PriceIntervalDropDownList>
		</div>
		<br clear="all" /><br />
		
		<div class="column">
			<div class="user-roles-list-box" style="width:185px;">
				<label><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Lekmer, Literal_AvailableBrands %>"/></label>
				<br />
				<asp:ListBox id="AvailableBrandsListBox" DataTextField="Title" DataValueField="Id"
					Rows="10"
					Width="180px"
					SelectionMode="Multiple"
					runat="server">
				</asp:ListBox>
			</div>
			<div class="user-roles-button" style="width:38px;">
				<uc:ImageLinkButton ID="AddBrandsButton" runat="server" Text=">>" SkinID="DefaultButton" />
				<br />
				<br />
				<uc:ImageLinkButton ID="RemoveBrandsButton" runat="server" Text="<<" SkinID="DefaultButton" />
			</div>
			<div class="user-roles-list-box" style="width:220px;">
				<label><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Lekmer, Literal_SelectedBrands %>" /></label>
				<br />
				<asp:ListBox id="SelectedBrandsListBox" DataTextField="Title" DataValueField="Id"
					Rows="10"
					Width="180px"
					SelectionMode="Multiple"
					runat="server">
				</asp:ListBox>
			</div>
		</div>
		
		<div class="column">
			<span><%= Resources.Lekmer.Literal_Size %></span><br />
			<asp:DropDownList runat="server" ID="SizeList" DataValueField="Id" DataTextField="Eu" Width="180"></asp:DropDownList>
		</div>
		
		<br clear="all" /><br /><br />
		
		<span><%= Resources.Lekmer.Literal_TagGroups %></span><br /><br />
		<asp:Repeater runat="server" ID="TagRepeater">
		<ItemTemplate>
			<div class="column" style="width:223px;">
				<asp:Literal runat="server" ID="TagGroupLiteral" Text='<%# Eval("Title") %>' /><br />
				<asp:DropDownList runat="server" ID="TagsList" DataTextField="Value" DataValueField="Id" Width="180" />
			</div>
		</ItemTemplate>
		</asp:Repeater>
		<br clear="all" /><br />
		
		<br />
		<div id="product-list-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="BlockProductFilterValidationGroup"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
		</div>
	</asp:Panel>
</asp:Content>