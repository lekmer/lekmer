using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Variations
{
	public partial class VariationGroupCreate : PageController,IEditor 
	{
		private const string SelectedTabClass = "ui-tabs-selected";

		protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
			VariantsListControl.VariationTypes.Clear();
            VariantsListControl.VariationTypes.AddRange(VariationTypeListControl.GetVariationTypeWrappers());
        }
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			CreateVariationGroupButton.Click += CreateVariationGroupButtonClick;
            CriteriaTabButton.Click += CriteriaTabButtonClick;
			VariantsTabButton.Click += VariantsTabButtonClick;
		}
		protected override void PopulateForm()
		{
			PopulateStatusList();
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Assortment.Variation.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!VariationTypeListControl.IsValid())
			{
				return;
			}
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			IVariationGroup variationGroup = variationGroupSecureService.Create();
			variationGroup.VariationGroupStatusId = System.Convert.ToInt32(StatusList.SelectedItem.Value, CultureInfo.CurrentCulture);
			variationGroup.Title = TitleTextBox.Text;
			variationGroup.DefaultProductId = VariantsListControl.DefaultProductId;
			Collection<IVariationTypeFull> variations = VariationTypeListControl.GetVariationTypes();

			
			if (VariantsListControl.Products.Count == 0)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.AddProductToVariantsGroup);
				return;
			}

			Collection<IProductVariation> productVariations = VariantsListControl.ProductVariations;
			IProductVariation productVariation = productVariations.FirstOrDefault(variant => VariantsListControl.DefaultProductId == variant.ProductId);
			if (productVariation == null)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.SelectCriteriaForPproductOrChangeProduct);
				return;
			}

			int variationGroupSecureId = variationGroupSecureService.Save(SignInHelper.SignedInSystemUser, variationGroup, variations, productVariations);
			if (variationGroupSecureId == -1)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.VariationGroupTitleExists);
				return;
			}

			Response.Redirect(PathHelper.Assortment.Variation.VariationGroup.GetEditUrl(variationGroupSecureId, true));
		}

		protected virtual void CriteriaTabButtonClick(object sender, EventArgs e)
		{
			CriteriaDiv.Visible = true;
			VariantsDiv.Visible = false;

			SetTabsStyle(SelectedTabClass, "");
		}
		protected virtual void VariantsTabButtonClick(object sender, EventArgs e)
		{
			if (VariationTypeListControl.IsValid())
			{
				VariantsListControl.VariationTypes.Clear();
				VariantsListControl.VariationTypes.AddRange(VariationTypeListControl.GetVariationTypeWrappers());
				CriteriaDiv.Visible = false;
				VariantsDiv.Visible = true;
                SetTabsStyle("", SelectedTabClass);
				VariantsListControl.RestoreProductSearchCriteria();
			}
			else
			{
				SetTabsStyle(SelectedTabClass, "");
				SystemMessageContainer.Add(Resources.ProductMessage.VariationTypeNotCorrectValue);
			}
		}
		protected virtual void CreateVariationGroupButtonClick(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.Assortment.Variation.VariationGroup.GetCreateUrl());
        }

		protected void PopulateStatusList()
		{
			var productStatusSecureService = IoC.Resolve<IVariationGroupStatusSecureService>();
			StatusList.DataSource = productStatusSecureService.GetAll();
			StatusList.DataBind();
			ListItem item = StatusList.Items.FindByValue("1");
			if (item != null)
			{
				item.Selected = true;
			}
		}
		protected void SetTabsStyle(string criteriaTabClass, string variationTabClass)
		{
			CriteriaLi.Attributes.Add("class", criteriaTabClass);
			VariationLi.Attributes.Add("class", variationTabClass);
		}
	}
}
