using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Variations
{
	public partial class Default : PageController
	{
		private const string VariationGroupId = "VariationGroupId";
		private const string DefaultStatusValue = "Status...";

		protected override void SetEventHandlers()
		{
			VariationGroupGrid.RowDataBound += VariationGroupGridRowDataBound;
			VariationGroupGrid.RowCommand += VariationGroupGridRowCommand;
			VariationGroupGrid.PageIndexChanging += VariationGroupGridPageIndexChanging;
			CreateVariationGroupButton.Click += CreateVariationGroupButtonClick;
			SetStatusButton.Click += SetStatusButtonClick;
			DeleteVariationGroupButton.Click += DeleteVariationGroupButtonClick;
		}
        protected override void PopulateForm()
		{
			VariationGroupGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
			PopulateStatusList();
		}
       
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (VariationGroupGrid.Rows.Count <= 0)
			{
				VariationGroupGrid.DataBind();
			}
		}

		protected virtual void VariationGroupGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var chkAllConnected = (CheckBox)row.FindControl("AllConnectedCheckBox");
				chkAllConnected.Checked = false;
				chkAllConnected.Attributes.Add("onclick", 
					"SelectAll1('" + chkAllConnected.ClientID + @"','" + VariationGroupGrid.ClientID + @"'); ShowBulkUpdatePanel('" + chkAllConnected.ID + "', '" + VariationGroupGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
				return;
			}
			if (row.RowType != DataControlRowType.DataRow) return;

			var ddl = (DropDownList)row.FindControl("StatusList");
			var variationGroup = (IVariationGroup)row.DataItem;
			PopulateVariationGroupStatusList(ddl, variationGroup);

			var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
			var selectAllCheckBox = (CheckBox)VariationGroupGrid.HeaderRow.FindControl("AllConnectedCheckBox");
			if (cbSelect == null || selectAllCheckBox == null) return;
			cbSelect.Attributes.Add("onclick",
				"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + VariationGroupGrid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
		}
		protected virtual void VariationGroupGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "DeleteVariationGroup") return;
			DeleteVariationGroup(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			PopulateVariationGroupGrid();
		}
		protected virtual void VariationGroupGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			VariationGroupGrid.PageIndex = e.NewPageIndex;
		}
		protected virtual void CreateVariationGroupButtonClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Assortment.Variation.VariationGroup.GetCreateUrl());
		}
		protected virtual void SetStatusButtonClick(object sender, EventArgs e)
		{
			if (VariationGroupStatusList.SelectedItem.Text == DefaultStatusValue) 
			{
				return;
			}
			IEnumerable<int> ids = VariationGroupGrid.GetSelectedIds();
            if (new List<int>(ids).Count <= 0)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.VariationGroupNotSelected);
				SystemMessageContainer.MessageType = InfoType.Warning;
            	return;
            }
			int statusId = System.Convert.ToInt32(VariationGroupStatusList.SelectedValue, CultureInfo.CurrentCulture);
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
            variationGroupSecureService.SetStatus(SignInHelper.SignedInSystemUser, ids, statusId);
			PopulateVariationGroupGrid();
			SystemMessageContainer.Add(Resources.GeneralMessage.StatusesChangedSuccessfully);
			SystemMessageContainer.MessageType = InfoType.Success;
		}
		protected virtual void DeleteVariationGroupButtonClick(object sender, EventArgs e)
		{
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			IEnumerable<int> ids = VariationGroupGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				SystemMessageContainer.Add(Resources.ProductMessage.VariationGroupNotSelected);
				SystemMessageContainer.MessageType = InfoType.Warning;
				return;
			}
			variationGroupSecureService.Delete(SignInHelper.SignedInSystemUser, ids);
			PopulateVariationGroupGrid();
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);
			SystemMessageContainer.MessageType = InfoType.Success;
		}
		protected virtual void Ddl_SelectedIndexChanged(object sender, EventArgs e)
		{
			var ddl = (DropDownList)sender;
			int variationGroupId = int.Parse(ddl.Attributes[VariationGroupId], CultureInfo.CurrentCulture);
			int statusId = int.Parse(ddl.SelectedValue, CultureInfo.CurrentCulture);
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			variationGroupSecureService.SetStatus(SignInHelper.SignedInSystemUser, variationGroupId, statusId);
			SystemMessageContainer.Add(Resources.GeneralMessage.StatusesChangedSuccessfully);
		}

		protected void PopulateVariationGroupGrid()
		{
			VariationGroupGrid.DataBind();
		}
		protected void DeleteVariationGroup(int rlId)
		{
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			variationGroupSecureService.Delete(SignInHelper.SignedInSystemUser, rlId);
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);
		}
		protected static void PopulateVariationGroupStatusList(ListControl ddlStatus, IVariationGroup variationGroup)
		{
			ddlStatus.Attributes.Add(VariationGroupId, variationGroup.Id.ToString(CultureInfo.CurrentCulture));
			ddlStatus.DataSource = IoC.Resolve<IVariationGroupStatusSecureService>().GetAll();
			ddlStatus.DataBind();
			ListItem item = ddlStatus.Items.FindByValue(variationGroup.VariationGroupStatusId.ToString(CultureInfo.CurrentCulture));
			if (item != null)
			{
				item.Selected = true;
			}
		}
		protected void PopulateStatusList()
		{
			VariationGroupStatusList.DataSource = IoC.Resolve<IVariationGroupStatusSecureService>().GetAll();
			VariationGroupStatusList.DataBind();
			VariationGroupStatusList.Items.Insert(0, DefaultStatusValue);
		}
	}
}
