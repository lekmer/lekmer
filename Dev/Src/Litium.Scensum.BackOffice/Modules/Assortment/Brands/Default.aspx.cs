﻿using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Convert=System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
    public partial class Default : PageController
    {
        protected override void SetEventHandlers()
        {
            BrandGrid.PageIndexChanging += OnPageIndexChanging;
            BrandGrid.RowCommand += OnRowCommand;
        }

        protected override void PopulateForm()
        {
            PopulateGrid();
        }

        protected void OnRowCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName != "DeleteBrand") return;

            var service = IoC.Resolve<IBrandSecureService>();
            service.Delete(SignInHelper.SignedInSystemUser,
                           Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
            PopulateGrid();
        }


        protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BrandGrid.PageIndex = e.NewPageIndex;
        }

        protected virtual void PopulateGrid()
        {
            BrandObjectDataSource.SelectParameters.Clear();
            BrandObjectDataSource.SelectParameters.Add("maximumRows",
                                                       BrandGrid.PageSize.ToString(CultureInfo.CurrentCulture));
            BrandObjectDataSource.SelectParameters.Add("startRowIndex",
                                                       (BrandGrid.PageIndex*BrandGrid.PageSize).ToString(
                                                           CultureInfo.CurrentCulture));

            BrandGrid.DataBind();
        }
    }
}