using System;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
	[Serializable]
	public class BrandState
	{
		public bool SetNewImage { get; set; }
		public int? MediaId { get; set; }
	}
}