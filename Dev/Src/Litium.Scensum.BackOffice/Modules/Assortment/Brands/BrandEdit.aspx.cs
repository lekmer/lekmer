﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Resources;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands
{
	public partial class BrandEdit : StatePageController<BrandState>
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			BrandDescriptionTranslator.TriggerImageButton = BrandDescriptionTranslateButton;
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			BrandMediaControl.Selected += OnBrandMediaSelected;
			BrandMediaControl.DeleteImage += OnBrandMediaDeleted;
			SiteStructureRegistryGrid.RowDataBound += SiteStructureRegistryGridRowDataBound;
		}

		private void OnBrandMediaDeleted(object sender, EventArgs e)
		{
			SetMediaId(null);
		}

		private void OnBrandMediaSelected(object sender, ImageSelectEventArgs e)
		{
			SetMediaId(e.Image.Id);
		}

		private void SiteStructureRegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var siteStructureRegistry = (ISiteStructureRegistry)row.DataItem;
			var lblTitle = (Label)row.FindControl("TitleLabel");
			lblTitle.Text = siteStructureRegistry.Title;
			var contentNodeSelector = (LekmerContentPageSelector)row.FindControl("ContentNodeSelector");
			contentNodeSelector.SiteStructureRegistryId = siteStructureRegistry.Id;

			var brandId = GetIdOrNull();
			if (brandId.HasValue)
			{
				var brandSiteStructureRegistries = IoC.Resolve<IBrandSiteStructureRegistrySecureService>().GetAllByBrand(brandId.Value);
				var brandSiteStructureRegistry = brandSiteStructureRegistries.SingleOrDefault(r => r.SiteStructureRegistryId == siteStructureRegistry.Id);
				if (brandSiteStructureRegistry != null)
				{
					contentNodeSelector.SelectedNodeId = brandSiteStructureRegistry.ContentNodeId;
				}
			}

			contentNodeSelector.SetContentPageTitle();
		}

		protected void SetMediaId(int? id)
		{
			EnsureState();
			State.MediaId = id;
			State.SetNewImage = true;
		}

		protected void EnsureState()
		{
			if (State == null)
			{
				State = new BrandState();
			}
		}

		protected override void PopulateForm()
		{
			// If redirected after successful saving new theme -> display success message
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
				Messager.Add(GeneralMessage.SaveSuccessBrand, InfoType.Success);

			EnsureState();
			PopulateBrandInfo();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx"); //PathHelper.Assortment.Store.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			Page.Validate("BrandValidationGroup");
			if (!Page.IsValid)
				return;

			var brandId = GetIdOrNull();

			var service = IoC.Resolve<IBrandSecureService>();
			var brand = brandId.HasValue ? service.GetById(brandId.Value) : service.Create();

			if (brandId.HasValue && brand == null)
				throw new BusinessObjectNotExistsException(brandId.Value);

			brand.Title = BrandTitleBox.Text;
			brand.ExternalUrl = string.IsNullOrEmpty(BrandUrlBox.Text) ? null : BrandUrlBox.Text;
			brand.Description = string.IsNullOrEmpty(BrandDescriptionEditor.Value) ? null : BrandDescriptionEditor.Value;

			brand.MediaId = State.MediaId;

			var formBrandSiteStructureRegistries = GetSiteStructureRegistries(brand);

			brand.Id = service.Save(SignInHelper.SignedInSystemUser, brand, formBrandSiteStructureRegistries, BrandTitleTranslator.GetTranslations(), BrandDescriptionTranslator.GetTranslations());

			if (brand.Id < 0)
			{
				//Saving failed because brand with the same title already exists
				Messager.Add(ProductMessage.BrandTitleExists, InfoType.Failure);
			}
			else if (!brandId.HasValue)
			{
				var editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true",
				                            "~/Modules/Assortment/Brands/BrandEdit.aspx?id=" + brand.Id);
				Response.Redirect(editUrl);
			}
			else
			{
				//Brand was updated successfully
				Messager.Add(GeneralMessage.SaveSuccessBrand, InfoType.Success);
				PopulateFormTitle(brand);
			}
		}

		private Collection<IBrandSiteStructureRegistry> GetSiteStructureRegistries(IBrand brand)
		{
			var formBrandSiteStructureRegistries = new Collection<IBrandSiteStructureRegistry>();
			var brandSiteStructureRegistrySecureService = IoC.Resolve<IBrandSiteStructureRegistrySecureService>();
			foreach (GridViewRow row in SiteStructureRegistryGrid.Rows)
			{
				var contentNodeSelector = ((LekmerContentPageSelector)row.FindControl("ContentNodeSelector"));
				if (contentNodeSelector.SelectedNodeId.HasValue)
				{
					var brandSiteStructureRegistry = brandSiteStructureRegistrySecureService.Create();
					brandSiteStructureRegistry.SiteStructureRegistryId = contentNodeSelector.SiteStructureRegistryId;
					brandSiteStructureRegistry.BrandId = brand.Id;
					brandSiteStructureRegistry.ContentNodeId = contentNodeSelector.SelectedNodeId.Value;
					formBrandSiteStructureRegistries.Add(brandSiteStructureRegistry);
				}
			}
			return formBrandSiteStructureRegistries;
		}

		protected void PopulateBrandInfo()
		{
			var brandId = GetIdOrNull();

			//Create mode
			if (!brandId.HasValue)
			{
				BrandHeader.Text = Resources.Product.Literal_CreateBrand;
				//DeleteButton.Visible = false;
			}
			else
			{
				var brand = IoC.Resolve<IBrandSecureService>().GetById(brandId.Value);

				PopulateFormTitle(brand);

				BrandTitleBox.Text = brand.Title;
				BrandUrlBox.Text = brand.ExternalUrl;
				BrandDescriptionEditor.Value = brand.Description;
				SetMediaId(brand.MediaId);
				BrandMediaControl.ImageId = brand.MediaId;
			}
			PopulateSiteStructure();
			PopulateTranslations();
		}

		protected void PopulateFormTitle(IBrand brand)
		{
			BrandHeader.Text = string.Format(CultureInfo.CurrentCulture, "{0} \"{1}\"",
			                                 Resources.Product.Literal_EditBrand, brand.Title);
		}

		private void PopulateSiteStructure()
		{
			SiteStructureRegistryGrid.DataSource = IoC.Resolve<ISiteStructureRegistrySecureService>().GetAll();
			SiteStructureRegistryGrid.DataBind();
		}

		private void PopulateTranslations()
		{
			var brandId = GetIdOrNull();
			var service = IoC.Resolve<IBrandSecureService>();

			BrandTitleTranslator.DefaultValueControlClientId = BrandTitleBox.ClientID;
			BrandTitleTranslator.BusinessObjectId = brandId ?? 0;
			BrandTitleTranslator.DataSource = brandId.HasValue 
				? service.GetAllTitleTranslationsByBrand(brandId.Value)
				: new Collection<ITranslationGeneric>();
			BrandTitleTranslator.DataBind();

			BrandDescriptionTranslator.DefaultValueControlClientId = BrandDescriptionEditor.ClientID;
			BrandDescriptionTranslator.BusinessObjectId = brandId ?? 0;
			BrandDescriptionTranslator.DataSource = brandId.HasValue
				? service.GetAllDescriptionTranslationsByBrand(brandId.Value)
				: new Collection<ITranslationGeneric>();
			BrandDescriptionTranslator.DataBind();
		}

	}
}