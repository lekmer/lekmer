﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4200
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands {
    
    
    public partial class BrandEdit {
        
        /// <summary>
        /// Messager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.MessageContainer Messager;
        
        /// <summary>
        /// ValidatorSummary control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.ScensumValidationSummary ValidatorSummary;
        
        /// <summary>
        /// BrandEditPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel BrandEditPanel;
        
        /// <summary>
        /// BrandHeader control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Common.LiteralEncoded BrandHeader;
        
        /// <summary>
        /// BrandTitleTranslator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.Translation.GenericTranslator BrandTitleTranslator;
        
        /// <summary>
        /// BrandTitleValidator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.RequiredFieldValidator BrandTitleValidator;
        
        /// <summary>
        /// BrandTitleBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox BrandTitleBox;
        
        /// <summary>
        /// BrandUrlBox control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox BrandUrlBox;
        
        /// <summary>
        /// BrandMediaControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.Modules.Assortment.Brands.Controls.BrandMedia BrandMediaControl;
        
        /// <summary>
        /// BrandDescriptionTranslateButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ImageButton BrandDescriptionTranslateButton;
        
        /// <summary>
        /// BrandDescriptionEditor control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.TinyMceEditor BrandDescriptionEditor;
        
        /// <summary>
        /// SiteStructureRegistryGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.GridView SiteStructureRegistryGrid;
        
        /// <summary>
        /// SaveButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton SaveButton;
        
        /// <summary>
        /// CancelButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton CancelButton;
        
        /// <summary>
        /// BrandDescriptionTranslator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.Translation.GenericWysiwygTranslator BrandDescriptionTranslator;
    }
}
