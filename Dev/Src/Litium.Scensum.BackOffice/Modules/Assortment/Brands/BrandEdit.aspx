﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Assortment/Brands/BrandMaster.Master"
	CodeBehind="BrandEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Brands.BrandEdit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="BrandMedia" Src="~/Modules/Assortment/Brands/Controls/BrandMedia.ascx" %>
<%@ Register TagPrefix="uc" TagName="ContentNodeSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton"
			runat="server" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
			ID="ValidatorSummary" DisplayMode="List" ValidationGroup="BrandValidationGroup" />
	</div>
</asp:Content>
<asp:Content ID="BrandEditContent" ContentPlaceHolderID="BrandPlaceHolder" runat="server">
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>"
		type="text/javascript"></script>
    <link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		function submitForm() {
			tinyMCE.triggerSave();
		}

		function ConfirmBrandDelete() {
			return DeleteConfirmation('<%= Resources.Product.Literal_DeleteBrand %>');
		}
	</script>

	<asp:Panel ID="BrandEditPanel" runat="server" DefaultButton="SaveButton">
		<div class="brand-edit-container">
			<div class="brand-edit-header">
				<uc:LiteralEncoded ID="BrandHeader" runat="server"></uc:LiteralEncoded>
			</div>
			<br />
			<div class="column">
				<div class="column input-box">
					<span><%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp
		            <uc:GenericTranslator ID="BrandTitleTranslator" runat="server" />&nbsp;
					<asp:RequiredFieldValidator runat="server" ID="BrandTitleValidator" ControlToValidate="BrandTitleBox"
						ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="BrandValidationGroup" />
					<br />
					<asp:TextBox ID="BrandTitleBox" runat="server" MaxLength="100" Width="465px"></asp:TextBox>
				</div>
				<div class="column input-box">
					<span>
						<%= Resources.Product.Literal_BrandExternalUrl %></span><br />
					<asp:TextBox ID="BrandUrlBox" runat="server" Width="465px"></asp:TextBox>
				</div>
			</div>
			
			<div class="brand-edit-block">
			    <uc:BrandMedia runat="server" Id="BrandMediaControl" />
			</div>
			
			<div class="brand-edit-block">
				<span><%= Resources.General.Literal_Description %></span>&nbsp
				<asp:ImageButton runat="server" ID="BrandDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
				<br />
				<uc:TinyMceEditor ID="BrandDescriptionEditor" runat="server" SkinID="tinyMCE" />
			</div>
			<br class="clear" />
			<br />
			
			<div class="brand-edit-block">		
				<span><%= Resources.General.Literal_SiteStructureRegistryList %></span>
				<asp:GridView ID="SiteStructureRegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="470px">
					<Columns>
						<asp:TemplateField HeaderText="<%$ Resources:General,Literal_SiteStructureRegistry %>" ItemStyle-Width="40%">
							<ItemTemplate>
								<asp:HiddenField ID="RegistryIdHiddenField" runat="server" Value='<%#Eval("Id")%>' />
								<asp:Label ID="TitleLabel" runat="server"></asp:Label>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_ContentNode %>" ItemStyle-Width="60%">
							<ItemTemplate>
								<uc:ContentNodeSelector ID="ContentNodeSelector" runat="server" AllowClearSelection="true" NoSelectionText="<%$ Resources:Lekmer, ContentPageSelector_None %>" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
			<br class="clear" />

			<div id="model-buttons" class="buttons right">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
					SkinID="DefaultButton" ValidationGroup="BrandValidationGroup" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
					SkinID="DefaultButton" />
			</div>
		</div>
	</asp:Panel>
	
	<uc:GenericWysiwygTranslator ID="BrandDescriptionTranslator" runat="server" />
</asp:Content>
