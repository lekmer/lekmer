﻿using System;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Brands.Controls
{
	public partial class BrandMedia : UserControlController
	{
		public event EventHandler<ImageSelectEventArgs> Selected;
		public event EventHandler FileUploaded;
		public event EventHandler DeleteImage;

		public int? ImageId { get; set; }

		protected override void SetEventHandlers()
		{
			ImageSelectControl.Selected += ImageSelected;
			ImageSelectControl.FileUploaded += ImageUnloaded;
			ImageSelectControl.Canceled += ImageSelectCanceled;

			DeleteImageButton.Click += DeleteImageButtonClick;
		}

		protected override void PopulateControl()
		{
			PopulateImage(ImageId);
		}

		protected void PopulateImage(IImage image)
		{
			ImageTitleLiteral.Text =
				ImageAlternativeTextLiteral.Text = ImageFileExtensionLabel.Text = ImageDimensionsLabel.Text = string.Empty;
			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = false;


			ImageTitleLiteral.Text = image.Title;
			ImageAlternativeTextLiteral.Text = image.AlternativeText;
			ImageFileExtensionLabel.Text = image.FormatExtension;
			ImageDimensionsLabel.Text = image.Width + "px x " + image.Height + "px";

			string imageUrl = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(image.Id, image.FormatExtension));

			ZoomProductImageAnchor.HRef = imageUrl;

			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = DefaultImage.Visible = true;
			ZoomProductImageAnchor.Title = image.Title;

			DefaultImage.ImageUrl = DefaultImage.ImageUrl =
			                        ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
		}

		protected void PopulateImage(int? imageId)
		{
			ImageTitleLiteral.Text =
				ImageAlternativeTextLiteral.Text = ImageFileExtensionLabel.Text = ImageDimensionsLabel.Text = string.Empty;
			ZoomProductImageAnchor.Visible = ZoomProductImage.Visible = false;

			if (imageId.HasValue)
			{
				var imageService = IoC.Resolve<IImageSecureService>();
				IImage image = imageService.GetById((int) imageId);
				PopulateImage(image);
			}
		}

		private void ImageSelectCanceled(object sender, EventArgs e)
		{
			ImagesPopup.Hide();
		}

		private void ImageUnloaded(object sender, EventArgs e)
		{
			ImagesPopup.X = 227;
			ImagesPopup.Show();
			if (FileUploaded != null)
			{
				FileUploaded(this, EventArgs.Empty);
			}
		}

		private void ImageSelected(object sender, ImageSelectEventArgs e)
		{
			ImagesPopup.Hide();
			PopulateImage(e.Image);

			if (Selected != null)
			{
				Selected(this, e);
			}
		}

		protected virtual void DeleteImageButtonClick(object sender, EventArgs e)
		{
			PopulateImage((int?) null);
			if (DeleteImage != null)
			{
				DeleteImage(this, EventArgs.Empty);
			}
		}
	}
}