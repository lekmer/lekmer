using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls.Lekmer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Categories
{
	public partial class CategoryEdit : PageController,IEditor
	{       
        protected override void SetEventHandlers()
        {
            ProductGrid.PageIndexChanging += ProductGridPageIndexChanging;
			RegistryGrid.RowDataBound += RegistryGridRowDataBound;
            SaveButton.Click += OnSave;
            CancelButton.Click += OnCancel;
        }

		protected override void PopulateForm()
        {
			var categoryService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();
			var category = categoryService.GetCategoryRecordById(GetId());

            if (category == null)
            {
				throw new BusinessObjectNotExistsException(GetId());
            }

			CategoryTitleTextBox.Text = category.Title;
            CategoryErpIdTextBox.Text = category.ErpId;

            CategoryProductsObjectDataSource.SelectParameters.Clear();
			CategoryProductsObjectDataSource.SelectParameters.Add("maximumRows", ProductGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			CategoryProductsObjectDataSource.SelectParameters.Add("startRowIndex", (ProductGrid.PageIndex * ProductGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			CategoryProductsObjectDataSource.SelectParameters.Add("categoryId", GetId().ToString(CultureInfo.CurrentCulture));

			RegistryGrid.DataSource = category.CategorySiteStructureRegistries;
			RegistryGrid.DataBind();

            SetCategoryBreadcrumbPath(category);

			CategoryTitleTranslator.DefaultValueControlClientId = CategoryTitleTextBox.ClientID;
			CategoryTitleTranslator.BusinessObjectId = category.Id;
			CategoryTitleTranslator.DataSource = categoryService.GetAllTranslationsByCategory(category.Id);
			CategoryTitleTranslator.DataBind();
        }

		public virtual void OnCancel(object sender, EventArgs e)
        {
			Response.Redirect(Request.Url.PathAndQuery);
        }

		public virtual void OnSave(object sender, EventArgs e)
        {
			var categoryService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();
			var category = categoryService.GetCategoryRecordById(GetId());
            if (category == null)
            {
				throw new BusinessObjectNotExistsException(GetId());
            }
			bool needUpdateMasterTree = category.Title != CategoryTitleTextBox.Text;
			category.Title = CategoryTitleTextBox.Text;
			foreach (GridViewRow row in RegistryGrid.Rows)
			{
				var pcns = ((LekmerContentPageSelector)row.FindControl("ParentContentNodeSelector"));
				ICategorySiteStructureRegistry reg = category.CategorySiteStructureRegistries.First(r => r.SiteStructureRegistryId == pcns.SiteStructureRegistryId);
				reg.ProductParentContentNodeId = pcns.SelectedNodeId;
				reg.ProductTemplateContentNodeId = ((LekmerContentPageSelector)row.FindControl("TemplateContentNodeSelector")).SelectedNodeId;
			}
			categoryService.Update(SignInHelper.SignedInSystemUser, category, CategoryTitleTranslator.GetTranslations());
			SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCategory);
			
			if (needUpdateMasterTree)
			{
				UpdateMasterTree(category.Id);
			}

            SetCategoryBreadcrumbPath(category);
        }

        private void SetCategoryBreadcrumbPath(ICategory category)
        {
            var breadcrumb = new StringBuilder(category.Title);
            
            ICategorySecureService service = null;
            var parentId = category.ParentCategoryId;
            if (parentId != null)
                service = IoC.Resolve<ICategorySecureService>();

            while (parentId != null)
            {
                ICategory parentCategory = service.GetCategoryRecordById((int)parentId);
                breadcrumb.Insert(0, string.Format(CultureInfo.InvariantCulture, "{0} > ", parentCategory.Title));

                parentId = parentCategory.ParentCategoryId;
            }

            breadcrumb.Insert(0, string.Format(CultureInfo.InvariantCulture, "{0} > ", Resources.Product.Literal_Category));
            CategoryTitleLabel.Text = breadcrumb.ToString();
        }

		protected virtual void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductGrid.PageIndex = e.NewPageIndex;
		}

		protected void RegistryGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var reg = (ICategorySiteStructureRegistry)row.DataItem;
			var lblTitle = (Label)row.FindControl("RegistryTitleLabel");
			lblTitle.Text = IoC.Resolve<ISiteStructureRegistrySecureService>().GetById(reg.SiteStructureRegistryId).Title;
			var pcns = (LekmerContentPageSelector)row.FindControl("ParentContentNodeSelector");
			pcns.SiteStructureRegistryId = reg.SiteStructureRegistryId;
			pcns.SelectedNodeId = reg.ProductParentContentNodeId;
			pcns.SetContentPageTitle();
			var tcns = (LekmerContentPageSelector)row.FindControl("TemplateContentNodeSelector");
			tcns.SiteStructureRegistryId = reg.SiteStructureRegistryId;
			tcns.SelectedNodeId = reg.ProductTemplateContentNodeId;
			tcns.SetContentPageTitle();
		}

		private void UpdateMasterTree(int selectedNodeId)
		{
			var master = Master as Categories;
			if (master != null)
			{
				master.UpdateMasterTree(selectedNodeId);
			}
		}
	}
}
