<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/Categories/Categories.Master"
	AutoEventWireup="false" CodeBehind="CategoryEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Categories.CategoryEdit" %>

<%@ Import Namespace="Litium.Scensum.BackOffice" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree"
	TagPrefix="TemplatedTreeView" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary attention-message warning"
					ID="ValidationSummary" DisplayMode="List" ValidationGroup="CategoryValidationGroup">
				</uc:ScensumValidationSummary>
				<uc:MessageContainer ID="SystemMessageContainer" MessageType="Success" HideMessagesControlId="SaveButton"
					runat="server" />
			</ContentTemplate>
		</asp:UpdatePanel>


</asp:Content>
<asp:Content ID="categoryEditContent" ContentPlaceHolderID="CategoriesContentPlaceHolder"
	runat="server">

	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>

	<asp:Label ID="CategoryTitleLabel" runat="server" CssClass="assortment-header" />

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
	
	    <div id="categories-block">
		    <div class="headBlock">
			    <div class="headColumn">
				    <div class="input-box">
					    <span><%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp
						<asp:RequiredFieldValidator runat="server" ID="CategoryTitleValidator" ControlToValidate="CategoryTitleTextBox"
							Display="None" ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" ValidationGroup="CategoryValidationGroup" />
						<uc:GenericTranslator ID="CategoryTitleTranslator" runat="server" />
					    <br />
					    <asp:TextBox ID="CategoryTitleTextBox" runat="server" />
				    </div>
				    <div class="input-box">
					    <span>
						    <%= Resources.General.Literal_ErpId %></span><br />
					    <asp:TextBox ID="CategoryErpIdTextBox" runat="server" ReadOnly="true" />
				    </div>
			    </div>
		    </div>
		    <br class="clear" />
		    <br />
    					
		    <asp:UpdatePanel ID="CategoriesUpdatePanel" runat="server" UpdateMode="Conditional" >
			    <ContentTemplate>
					    <div id="category-parent-content-node">
						    <span class="assortment-header">
							    <%= Resources.General.Literal_SiteStructureRegistryList %></span>
						    <asp:GridView ID="RegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
							    Width="100%">
							    <Columns>
								    <asp:TemplateField HeaderText="<%$ Resources:General,Literal_SiteStructureRegistry %>"
									    ItemStyle-Width="30%">
									    <ItemTemplate>
										    <asp:HiddenField ID="RegistryIdHiddenField" runat="server" Value='<%#Eval("SiteStructureRegistryId")%>' />
										    <asp:Label ID="RegistryTitleLabel" runat="server"></asp:Label>
									    </ItemTemplate>
								    </asp:TemplateField>
								    <asp:TemplateField HeaderText="<%$ Resources:General,Literal_ParentContentNode %>"
									    ItemStyle-Width="35%">
									    <ItemTemplate>
										    <uc:ContentPageSelector ID="ParentContentNodeSelector" runat="server" AllowClearSelection="true" />
									    </ItemTemplate>
								    </asp:TemplateField>
								    <asp:TemplateField HeaderText="<%$ Resources:General,Literal_TemplateContentNode%>"
									    ItemStyle-Width="35%">
									    <ItemTemplate>
										    <uc:ContentPageSelector ID="TemplateContentNodeSelector" runat="server" OnlyProductPagesSelectable="true"
											    AllowClearSelection="true" />
									    </ItemTemplate>
								    </asp:TemplateField>
							    </Columns>
						    </asp:GridView>
					    </div>
			    </ContentTemplate>
		    </asp:UpdatePanel>
		    <br />
		    <br />

		    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" >
			    <ContentTemplate>
		            <div id="categories-poducts" class="clear left">
			            <span class="assortment-header">
				            <%= Resources.Product.Literal_Products %></span>
			            <sc:GridViewWithCustomPager ID="ProductGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
				            Width="100%" DataSourceID="CategoryProductsObjectDataSource" AllowPaging="true"
				            PageSize="<%$AppSettings:DefaultGridPageSize%>">
				            <Columns>
					            <asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId"
						            ItemStyle-Width="20%" />
					            <asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="75%">
						            <ItemTemplate>
							            <uc:HyperLinkEncoded runat="server" ID="ProductEditLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Product.GetEditUrlForCategoryEdit(int.Parse(Eval("Id").ToString()), GetId()) %>'
								            Text='<%# Eval("DisplayTitle")%>' />
						            </ItemTemplate>
					            </asp:TemplateField>
				            </Columns>
			            </sc:GridViewWithCustomPager>
			            <asp:ObjectDataSource ID="CategoryProductsObjectDataSource" runat="server" EnablePaging="true"
				            SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Categories.CategoryProductsDataSource">
			            </asp:ObjectDataSource>
		            </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            
		    <br clear="all" />
		    <br class="clear" />

		    <div id="categories-buttons" class="buttons right">
			    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
				    SkinID="DefaultButton" ValidationGroup="CategoryValidationGroup" />
			    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				    SkinID="DefaultButton" />
		    </div>
        </div>
        
	</asp:Panel>
</asp:Content>
