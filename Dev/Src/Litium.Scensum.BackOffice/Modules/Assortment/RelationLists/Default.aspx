<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>
<%@ Page Language="C#" MasterPageFile="~/Modules/Assortment/RelationLists/RelationLists.Master"  CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.RelationLists.Default" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="RelationListsTabContent" ContentPlaceHolderID="RelationListPlaceHolder" runat="server">
<asp:UpdatePanel ID="RelationUpdatePanel" runat="server" UpdateMode="Always">
	<ContentTemplate>
		<div class="rlSearch">
			<asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
			<div class="rlSearchHeader">
				<span><%= Resources.General.Literal_Search %></span>
			</div>
			<div class="rlSearchForm">
				<div class="column">
					<div class="input-box">
						<span><%= Resources.General.Literal_Title %></span>
						<br />
						<asp:TextBox ID="RelationListTitleTextBox" runat="server" CssClass="rlSearchName"></asp:TextBox>
					</div>
				</div>
				<div class="column">
					<div class="input-box">
						<span><%= Resources.General.Literal_Type %></span>
						<br />
						<asp:DropDownList ID="RelationListTypeList" runat="server" DataValueField="Id" DataTextField="CommonName" CssClass="rlSearchType"></asp:DropDownList>
					</div>
				</div>
				<div class="rlSearchButton">
					<uc:ImageLinkButton ID="SearchButton" runat="server" Text="<%$ Resources:General, Button_Search %>" SkinID="DefaultButton" />
				</div>
			</div>
			</asp:Panel>
			
		</div>
		<div class="rl-search-result">
		<sc:GridViewWithCustomPager
			ID="RelationListGrid"
			SkinID="grid"
			runat="server"
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			DataSourceID="RelationListsObjectDataSource"
			Width="100%"
			PagerSettings-Mode="NumericFirstLast">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="50%">
					<ItemTemplate>
						<uc:HyperLinkEncoded runat="server" ID="RelationListEditLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Relation.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' Text='<%# Eval("Title")%>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:General, Literal_Type %>" DataField="RelationListTypeName" ItemStyle-Width="46%" />
				<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="4%">
					<ItemTemplate>
						<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteRelationList" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="Delete" OnClientClick="return DeleteConfirmation('relation list');" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
		</div>
		<asp:ObjectDataSource ID="RelationListsObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.RelationLists.RelationListDataSource" />
	
	</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>
