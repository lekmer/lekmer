﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Security/Roles/Role.Master"   CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Security.Roles.Default" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">.
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="Messager" MessageType="Failure" runat="server" />
				</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="AdvancedSearchTabContent" ContentPlaceHolderID="RolePlaceHolder" runat="server">	
	<asp:UpdatePanel ID="SearchUpdatePanel" runat="server">
	<ContentTemplate>
		
	<div id="role-search-result">
		<sc:GridViewWithCustomPager 
			ID="SearchGrid" 
			SkinID="grid" 
			runat="server" 
			DataSourceID="RoleObjectDataSource" 
			Width="100%"				
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="95%">
					<ItemTemplate>
						<uc:HyperLinkEncoded runat="server" ID="EditLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Role.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' Text='<%# Eval("Title")%>' />
					</ItemTemplate >
				</asp:TemplateField>
				<asp:TemplateField ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
						<ItemTemplate>
							<asp:ImageButton runat="server" OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.User.Literal_role + "\");" %>' ID="DeleteRoleButton" CommandName="DeleteRole" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="Delete" />
						</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
		<asp:ObjectDataSource ID="RoleObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetRoles" TypeName="Litium.Scensum.BackOffice.Modules.Security.SecurityDataSource" />
			
	</div>
	</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>