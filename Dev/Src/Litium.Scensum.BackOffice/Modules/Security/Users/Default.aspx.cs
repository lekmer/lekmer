﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.User.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Security.Users
{
	public partial class Default : PageController
	{
		private readonly ISystemUserSecureService _userService = 
            IoC.Resolve<ISystemUserSecureService>();

		protected override void SetEventHandlers()
		{
			UserSearchCriteriaControl.SearchEvent += UserSearchCriteriaControl_SearchEvent;
			SearchGrid.RowCommand += GVSearch_RowCommand;
		}
		protected virtual  void GVSearch_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "DeleteUser")
			{
				_userService.Delete(SignInHelper.SignedInSystemUser, System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				if (SearchGrid.Rows.Count == 1 && SearchGrid.PageIndex > 0)
					SearchGrid.PageIndex -= 1;
				RestoreSearchFields();
			}
		}

		
		void UserSearchCriteriaControl_SearchEvent(object sender, UserSearchCriteriaEventArgs e)
		{
			SearchGrid.PageIndex = e.PageIndex != null ? (int)e.PageIndex : 0;
			SearchUsers(UserObjectDataSource, SearchGrid, e.SearchCriteria);
			SearchGrid.DataBind();
			SearchCriteriaState<IUserSearchCriteria>.Instance.Save(e.SearchCriteria);
		}
		protected void RestoreSearchFields()
		{
			IUserSearchCriteria userSearchCriteria = SearchCriteriaState<IUserSearchCriteria>.Instance.Get() ??
															 IoC.Resolve<IUserSearchCriteria>();
			UserSearchCriteriaControl.RestoreSearchCriteriaFields(userSearchCriteria);
			int pageIndex = SearchGrid.PageIndex;
			UserSearchCriteriaControl_SearchEvent(this, new UserSearchCriteriaEventArgs { SearchCriteria = userSearchCriteria, PageIndex = pageIndex });
		}
		protected override void PopulateForm()
		{
			RestoreSearchFields();
		}
		public static void SearchUsers(ObjectDataSource ods, GridView gv, IUserSearchCriteria searchCriteria)
		{
			ods.SelectParameters.Clear();
			ods.SelectParameters.Add("maximumRows", gv.PageSize.ToString(CultureInfo.CurrentCulture));
			ods.SelectParameters.Add("startRowIndex", (gv.PageIndex * gv.PageSize).ToString(CultureInfo.CurrentCulture));
			ods.SelectParameters.Add("name", searchCriteria.Name);
			ods.SelectParameters.Add("userName", searchCriteria.UserName);
			ods.SelectParameters.Add("actDate", searchCriteria.ActivationDate);
			ods.SelectParameters.Add("expDate", searchCriteria.ExpirationDate);
			ods.SelectParameters.Add("userStatusId", searchCriteria.UserStatusId);
		}
	}
}
