using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Security.Users
{
	public partial class UserEdit : StatePageController<Collection<IRole>>, IEditor
	{
		protected virtual Collection<IRole> GetUserRoleState(int? userId)
		{
			if (State == null)
			{
				Collection<IRole> rolesList = null;
				if (userId != null)
				{
					var systemRoleSecureService = IoC.Resolve<ISystemRoleSecureService>();
					rolesList = systemRoleSecureService.GetAllByUser((int)userId);
				}
				State = rolesList ?? new Collection<IRole>();
			}
			return State;
		}
		protected override void SetEventHandlers()
		{
			ResetLoginsAttemptButton.Click += ResetLoginsAttemptClick;
			AddRolesButton.Click += AddRolesClick;
			RemoveRolesButton.Click += RemoveRolesClick;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			ChangePasswordCheckBox.CheckedChanged += ChangePasswordCheckedChanged;
			DateCompareValidator.ServerValidate += CompareDateTime;
		    DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            IoC.Resolve<ISystemUserSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
            Response.Redirect(PathHelper.Role.GetDefaultUrl());
        }

	    protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (UserCredential.Visible)
			{
				ScriptManager.RegisterStartupScript(EditUpdatePanel, EditUpdatePanel.GetType(), "pswd",
				                                    string.Format(CultureInfo.CurrentCulture, "restorePassword();"), true);
			}
		}

		private void AddRolesClick(object sender, EventArgs e)
		{
			Collection<IRole> rolesList = GetUserRoleState(GetIdOrNull());

			foreach (ListItem item in RolesForSelectionListBox.Items)
			{
				if (item.Selected)
				{
					IRole role = IoC.Resolve<ISystemRoleSecureService>().Create();
					role.Id = int.Parse(item.Value, CultureInfo.CurrentCulture);
					role.Title = item.Text;
					rolesList.Add(role);
				}
			}
			State = rolesList;
			PopulateRoles();
		}
		private void RemoveRolesClick(object sender, EventArgs e)
		{
			Collection<IRole> rolesList = GetUserRoleState(GetIdOrNull());
			foreach (ListItem item in SelectedRolesListBox.Items)
			{
				if (item.Selected)
				{
					ListItem listItem = item;
					IRole selectedRole = rolesList.FirstOrDefault(r => r.Id == int.Parse(listItem.Value, CultureInfo.CurrentCulture));
					rolesList.Remove(selectedRole);
				}
			}
			State = rolesList;
			PopulateRoles();
		}
		private void ResetLoginsAttemptClick(object sender, EventArgs e)
		{
			InvalidLoginsTextBox.Text = "0";
		}

		private void ChangePasswordCheckedChanged(object sender, EventArgs e)
		{
			UserCredential.Visible = ChangePasswordCheckBox.Checked;
		}

		protected override void PopulateForm()
		{
			ActivationDateRangeValidator.MinimumValue = CommonItems.Constants.MinDate;
			ActivationDateRangeValidator.MaximumValue = CommonItems.Constants.MaxDate;
			ExpirationDateRangeValidator.MinimumValue = CommonItems.Constants.MinDate;
			ExpirationDateRangeValidator.MaximumValue = CommonItems.Constants.MaxDate;
			UserCredential.Visible = false;
			if (GetIdOrNull() == null)
			{
				UserEditCreateLabel.Text = Resources.User.Literal_CreateSystemUser;
				PopulateCreateUserFields();
			}
			else
			{
				UserEditCreateLabel.Text = Resources.User.Literal_EditSystemUser;
				PopulateEditUserFields();
			}
			
			if (Request.QueryString.GetBooleanOrNull("HasMessage") ?? false)
			{
				SaveMesagerContainer.MessageType = InfoType.Success;
				SaveMesagerContainer.Add(Resources.GeneralMessage.SaveSuccessUser);
			}

		    DeleteButton.Visible = GetIdOrNull().HasValue;
		}
		
		private void PopulateCreateUserFields()
		{
			PopulateUserStatus();
			ActivationDateTextBox.Text = DateTime.Now.ToShortDateString();
			ExpDateTextBox.Text = DateTime.Now.AddDays(SystemUserSetting.Instance.ExpirationPeriodDays).ToShortDateString();
			InvalidLoginsTextBox.Text = "0";
			ChangePasswordCheckBox.Visible = false;
			UserCredential.Visible = true;
			PopulateRoles();
			PopulateCultures(null);
		}

		private void PopulateEditUserFields()
		{
			ISystemUser user = IoC.Resolve<ISystemUserSecureService>().GetById(GetId());
			if (user == null)
				return;

			NameTextBox.Text = user.Name;
			PopulateUserStatus();
			StatusList.Items.FindByValue(user.UserStatus.CommonName.ToString(CultureInfo.CurrentCulture)).Selected = true;
			InvalidLoginsTextBox.Text = user.FailedSignInCounter.ToString(CultureInfo.CurrentCulture);
			ActivationDateTextBox.Text = user.ActivationDate.HasValue ? user.ActivationDate.Value.ToShortDateString() : string.Empty;
			ExpDateTextBox.Text = user.ExpirationDate.HasValue ? user.ExpirationDate.Value.ToShortDateString() : string.Empty;
			UserCredential.UserName = user.UserName;
			PopulateRoles();
			PopulateCultures(user);
		}
		private void PopulateUserStatus()
		{
			var systemUserStatusSecureService = IoC.Resolve<ISystemUserStatusSecureService>();
			StatusList.DataSource = systemUserStatusSecureService.GetAll();
			StatusList.DataTextField = "Title";
			StatusList.DataValueField = "CommonName";
			StatusList.DataBind();
			StatusList.Items.Insert(0, new ListItem(string.Empty, "0"));
		}

		private void PopulateRoles()
		{
			var allRoles = IoC.Resolve<ISystemRoleSecureService>().GetAll();
			var userRoles = GetUserRoleState(GetIdOrNull());
			var notUseRole = new Collection<IRole>();
			foreach (var role in allRoles)
			{
				IRole roleLocal = role;
				if (userRoles.FirstOrDefault(r => r.Id == roleLocal.Id) == null)
				{
					notUseRole.Add(role);
				}
			}
			SelectedRolesListBox.DataSource = userRoles;
			SelectedRolesListBox.DataBind();
			RolesForSelectionListBox.DataSource = notUseRole;
			RolesForSelectionListBox.DataBind();
		}

		protected virtual void PopulateCultures(ISystemUser user)
		{
			CultureList.DataSource = IoC.Resolve<ICultureSecureService>().GetAll();
			CultureList.DataBind();
			if (user == null) return;
			
			ListItem item = CultureList.Items.FindByValue(user.Culture.Id.ToString(CultureInfo.CurrentCulture));
			if (item != null)
				item.Selected = true;
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.User.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.User.GetDefaultUrl());
			}
		}

		public void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}

		public void OnSave(object sender, EventArgs e)
		{
			Page.Validate("vgUserEdit");
			if (!Page.IsValid)
			{
				return;
			}

			var userService = IoC.Resolve<ISystemUserSecureService>();
			int? userId = GetIdOrNull();
			ISystemUser user = userId != null ? userService.GetById((int)userId) : userService.Create();
			user.Name = NameTextBox.Text;
			user.UserName = UserCredential.UserName;
			
			var userStatusSecureService = IoC.Resolve<ISystemUserStatusSecureService>();
			user.UserStatus = userStatusSecureService.GetByCommonName(StatusList.SelectedValue);

			var systemUserStatusOnline = userStatusSecureService.GetByCommonName("Online");
			if (user.UserStatus.CommonName == "Offline" && user.UserStatus.Id == systemUserStatusOnline.Id)
			{
				InvalidLoginsTextBox.Text = "0";
				user.FailedSignInCounter = 0;
			}
			else
			{
				// The textbox value might be set by javascript logic of Reset number 
				user.FailedSignInCounter = int.Parse(InvalidLoginsTextBox.Text, CultureInfo.CurrentCulture);
			}
			user.ActivationDate = DateTime.Parse(ActivationDateTextBox.Text, CultureInfo.CurrentCulture);
			user.ExpirationDate = DateTime.Parse(ExpDateTextBox.Text, CultureInfo.CurrentCulture);

			bool changePasswordChosen = ChangePasswordCheckBox.Checked;
			string clearTextPassword = string.Empty;
			if (changePasswordChosen || user.Status == BusinessObjectStatus.New)
			{
				user.Password = UserCredential.GetPassword(out clearTextPassword);
			}

			int cultureId = int.Parse(CultureList.SelectedValue, CultureInfo.CurrentCulture);
			user.Culture = IoC.Resolve<ICultureSecureService>().GetById(cultureId);

			user.Id = userService.Save(SignInHelper.SignedInSystemUser, user, GetUserRoleState(userId));
			if(user.Id == -1)
			{
				SaveMesagerContainer.MessageType = InfoType.Failure;
				SaveMesagerContainer.Add(Resources.UserMessage.UserWithSameUserName);
				return;
			}

			if (user.IsNew)
			{
				user.Password = clearTextPassword;
				userService.SendCreationConfirm(user);

				Response.Redirect(PathHelper.User.GetEditUrlWithSuccesMessage(user.Id));
			}
			else if (changePasswordChosen)
			{
				user.Password = clearTextPassword;
				userService.SendChangePasswordConfirm(user);
			}

			if (!SaveMesagerContainer.HasMessages)
			{
				SaveMesagerContainer.MessageType = InfoType.Success;
				SaveMesagerContainer.Add(Resources.GeneralMessage.SaveSuccessUser);
			}
		}
		private void CompareDateTime(object sender, ServerValidateEventArgs args)
		{
			if (string.IsNullOrEmpty(ActivationDateTextBox.Text)
				|| string.IsNullOrEmpty(ExpDateTextBox.Text)
				|| !ActivationDateRangeValidator.IsValid
				|| !ExpirationDateRangeValidator.IsValid)
			{
				args.IsValid = true;
				return;
			}
			var createdFrom = DateTime.Parse(ActivationDateTextBox.Text, CultureInfo.CurrentCulture);
			var createdTo = DateTime.Parse(args.Value, CultureInfo.CurrentCulture);
			if (createdFrom > createdTo)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}
	}
}
