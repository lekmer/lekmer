using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.General.Channels
{
	public partial class Edit : PageController,IEditor 
	{
        protected override void SetEventHandlers()
        {
            SaveButton.Click += OnSave;
            CancleButton.Click += OnCancel;
			LayoutCustomValidator.ServerValidate += ValidateLayout;
        }

		protected void ValidateLayout(object source, ServerValidateEventArgs args)
		{
			decimal layoutRatio;
			args.IsValid = decimal.TryParse(LayoutRatioTextBox.Text, NumberStyles.Any, CultureInfo.CurrentCulture, out layoutRatio) 
				&& layoutRatio >= 0 
				&& layoutRatio <= 100;
		}

		protected override void OnLoad(EventArgs e)
		{
			Thread.CurrentThread.CurrentCulture = new CultureInfo(CultureInfo.CurrentCulture.Name, false);
			Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";

			base.OnLoad(e);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Channel.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			Page.Validate("channelValidation");
            if (!Page.IsValid) return;
            if (!SaveChannel()) return;

			SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessChannel);
			SystemMessageContainer.MessageType = InfoType.Success;
		}

        #region Populate

        protected override void PopulateForm()
        {
			IChannel channel = IoC.Resolve<IChannelSecureService>().GetById(GetId());

            if (channel == null)
            {
                throw new BusinessObjectNotExistsException("Channel not found.");
            }

			Collection<IContentPage> orderDetailContentPages = IoC.Resolve<IContentPageSecureService>().GetByType("OrderHistoryDetail");
			PopulateContentPagesDropDown(OrderDetailTemplateList, orderDetailContentPages);

			//PopulateFormatFields();

            // Controls Caption.
			ActionLabel.Text = "Edit Channel " + channel.Title;

            // Application Name.
            UrlTextBox.Text = channel.ApplicationName;

            // Main Fields.
			ChannelTitleTextBox.Text = channel.Title;
			CountryIdHiddenField.Value = channel.Country.Id.ToString(CultureInfo.CurrentCulture);
            CountryTextBox.Text = channel.Country.Title;
			CurrencyIdHiddenField.Value = channel.Currency.Id.ToString(CultureInfo.CurrentCulture);
            CurrencyTextBox.Text = channel.Currency.Iso;
			LanguageIdHiddenField.Value = channel.Language.Id.ToString(CultureInfo.CurrentCulture);
            LanguageTextBox.Text = channel.Language.Title;
            LayoutRatioTextBox.Text = channel.AlternateLayoutRatio.ToString(CultureInfo.CurrentCulture);
            
            // Product template
        	PopulateProductTemplateContentNodes();
        	
			//OrderDetail template page
			IOrderModuleChannel orderModuleChannel = IoC.Resolve<IOrderModuleChannelSecureService>().GetById(GetId());
        	OrderDetailTemplateList.SelectedValue =
        		(null == orderModuleChannel
        		 	? -1
					: (orderModuleChannel.OrderTemplateContentNodeId ?? -1)).ToString(CultureInfo.CurrentCulture);
			
			// Theme
        	PopulateThemes(channel.Id);

			// Culture
			PopulateCultures(channel);
        }

		protected virtual void PopulateProductTemplateContentNodes()
		{
			var siteStructureRegistry = IoC.Resolve<ISiteStructureRegistrySecureService>().GetByChannel(GetId());
			if (siteStructureRegistry == null) return;

			ProductTemplateContentNodeSelector.SiteStructureRegistryId = siteStructureRegistry.Id;
            
			var productModuleChannel = IoC.Resolve<IProductModuleChannelSecureService>().GetById(GetId());
			if (productModuleChannel == null || !productModuleChannel.ProductTemplateContentNodeId.HasValue) return;
			
			ProductTemplateContentNodeSelector.SelectedNodeId = productModuleChannel.ProductTemplateContentNodeId;
			ProductTemplateContentNodeSelector.SetContentPageTitle();
		}

		protected virtual void PopulateContentPagesDropDown(DropDownList ddl, IEnumerable<IContentPage> datasource)
		{
			ddl.DataSource = datasource.OrderBy(item => item.Title);
			ddl.DataBind();
			ddl.Items.Insert(0, new ListItem(string.Empty, "-1"));
		}

		protected virtual void PopulateThemes(int channelId)
		{
			ThemeList.DataSource = IoC.Resolve<IThemeSecureService>().GetAll();
			ThemeList.DataBind();
			ThemeList.Items.Insert(0, new ListItem(Resources.Interface.Literal_DefaultThemeName, string.Empty));

			ITemplateModuleChannel templateModuleChannel = IoC.Resolve<ITemplateModuleChannelSecureService>().GetById(channelId);
			if (templateModuleChannel == null) 
				return;
			ListItem item = ThemeList.Items.FindByValue(templateModuleChannel.ThemeId.ToString(CultureInfo.CurrentCulture));
			if (item != null) 
				item.Selected = true;
		}

		protected virtual void PopulateCultures(IChannel channel)
		{
			CultureList.DataSource = IoC.Resolve<ICultureSecureService>().GetAll();
			CultureList.DataBind();

			ListItem item = CultureList.Items.FindByValue(channel.Culture.Id.ToString(CultureInfo.CurrentCulture));
			if (item != null)
				item.Selected = true;
		}

		#endregion

		protected bool SaveChannel()
		{
			var channelSecureService = IoC.Resolve<IChannelSecureService>();
			// Create Channel.
			IChannel channel = channelSecureService.Create();
			channel.Id = GetId();
			channel.Title = ChannelTitleTextBox.Text;
			channel.ApplicationName = UrlTextBox.Text;
			channel.Country.Id = int.Parse(CountryIdHiddenField.Value, CultureInfo.CurrentCulture);
			channel.Currency.Id = int.Parse(CurrencyIdHiddenField.Value, CultureInfo.CurrentCulture);
			channel.Language.Id = int.Parse(LanguageIdHiddenField.Value, CultureInfo.CurrentCulture);
			channel.Culture.Id = int.Parse(CultureList.SelectedValue, CultureInfo.CurrentCulture);
			channel.AlternateLayoutRatio = decimal.Parse(LayoutRatioTextBox.Text, CultureInfo.CurrentCulture);
			//channel.StartPageId = ttvNodes.SelectedNodeId.Value;
			//channel.ProductTemplateContentNodeId = int.Parse(ProductTemplatesList.SelectedValue);

			// Save Channel.
			channel.Id = channelSecureService.Save(SignInHelper.SignedInSystemUser, channel);
			if (channel.Id == -1)
			{
				SystemMessageContainer.Add("Channel with this name exist !");
				return false;
			}

		    SaveProductTemplateContentNode(channel.Id);

			SaveOrderDetailPageTemplate(channel.Id);

			SaveTemplateModuleChannel(channel.Id);

			return true;
		}
        protected void SaveProductTemplateContentNode(int channelId)
        {
            var productModuleChannelSecureService = IoC.Resolve<IProductModuleChannelSecureService>();
            var productModuleChannel = productModuleChannelSecureService.Create();
            productModuleChannel.Id = channelId;
			productModuleChannel.ProductTemplateContentNodeId = ProductTemplateContentNodeSelector.SelectedNodeId;
            productModuleChannelSecureService.Save(SignInHelper.SignedInSystemUser, productModuleChannel);
        }
		protected void SaveOrderDetailPageTemplate(int channelId)
		{
			var orderModuleChannel = IoC.Resolve<IOrderModuleChannel>();
			orderModuleChannel.Id = channelId;
			int orderTemplateContentNodeId = int.Parse(OrderDetailTemplateList.SelectedValue, CultureInfo.CurrentCulture);
			orderModuleChannel.OrderTemplateContentNodeId = orderTemplateContentNodeId == -1 ? null : (int?)orderTemplateContentNodeId;
			var orderModuleChannelSecureService = IoC.Resolve<IOrderModuleChannelSecureService>();
			orderModuleChannelSecureService.Save(orderModuleChannel);
		}
		protected void SaveTemplateModuleChannel(int channelId)
		{
			var service = IoC.Resolve<ITemplateModuleChannelSecureService>();
			string selectedThemeId = ThemeList.SelectedValue;
			if (string.IsNullOrEmpty(selectedThemeId))
			{
				service.Delete(SignInHelper.SignedInSystemUser, channelId);
			}
			else
			{
				ITemplateModuleChannel templateModuleChannel = service.GetById(channelId) ?? service.Create(channelId);
				templateModuleChannel.ThemeId = int.Parse(selectedThemeId, CultureInfo.CurrentCulture);
				service.Save(SignInHelper.SignedInSystemUser, templateModuleChannel);
			}
		}
	}
}
