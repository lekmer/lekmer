<%@ Page 
	Language="C#" 
	MasterPageFile="~/Master/Main.Master" 
	CodeBehind="Edit.aspx.cs" 
	Inherits="Litium.Scensum.BackOffice.Modules.General.Channels.Edit" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree"
	TagPrefix="TemplatedTreeView" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>

<asp:Content ID="ChannelEditContent" ContentPlaceHolderID="body" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		  	<div style="width:984px;">  <asp:UpdatePanel id="MessageContainerUpdatePanel" UpdateMode="Always" runat="server">
				<ContentTemplate>
					<uc:ScensumValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" ForeColor="Black" CssClass="advance-validation-summary"  ValidationGroup="channelValidation"  />
					<uc:MessageContainer ID="SystemMessageContainer"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />	
				</ContentTemplate>
		</asp:UpdatePanel>
		</div>
	<div id="channel-edit">
		<asp:Label ID="ActionLabel" runat="server" CssClass="main-caption" />
		
		<div id="divMainFields" class="content-box">
			<asp:Label ID="MainLabel" Text="Main Fields" CssClass="main-caption" runat="server" /><br />
			
			<div id="divFirstMain" class="column">
				<div class="input-box">
					<asp:Label ID="CannelTitleLabel" Text="Channel Title" AssociatedControlID="ChannelTitleTextBox" runat="server" /> *
					<asp:RequiredFieldValidator ID="ChannelTitleValidator" runat="server" ControlToValidate="ChannelTitleTextBox" ValidationGroup="channelValidation" Display="None" ErrorMessage="Channel name is a Required fild"/>
					<br />
					<asp:TextBox ID="ChannelTitleTextBox" runat="server" MaxLength="50" /><br />
				</div>
				
				<div class="input-box">
					<asp:Label ID="CountryLabel" Text="Country" AssociatedControlID="CountryTextBox" runat="server" /> *
					<br />
					<asp:HiddenField ID="CountryIdHiddenField" runat="server" />
					<asp:TextBox ID="CountryTextBox" runat="server" ReadOnly="true" Enabled="false" /><br />
				</div>
				
				<div class="input-box">
					<asp:Label ID="CurrencyLabel" Text="Currency" AssociatedControlID="CurrencyTextBox" runat="server" /> *
					<br />
					<asp:HiddenField ID="CurrencyIdHiddenField" runat="server" />
					<asp:TextBox ID="CurrencyTextBox" runat="server" ReadOnly="true" Enabled="false" /><br />
				</div>
				
				<div class="input-box">
					<asp:Label ID="CultureLabel" Text="Culture" runat="server" />
					<br />
					<asp:DropDownList ID="CultureList" runat="server" DataValueField="Id" DataTextField="Title"></asp:DropDownList>
				</div>			
			</div>
	        
			<div id="divSecondMain" class="column">
				<div class="input-box">
					<asp:Label ID="UrlLabel" Text="URL" AssociatedControlID="UrlTextBox" runat="server" /> *
					<asp:RequiredFieldValidator ID="UrlrRequiredValidator" runat="server" ControlToValidate="UrlTextBox" ValidationGroup="channelValidation" Display="None" ErrorMessage="URL is a Required fild"/>
					<%--<asp:RegularExpressionValidator ID="UrlRegularValidator" runat="server" ControlToValidate="UrlTextBox" ValidationGroup="channelValidation" Display="Dynamic" ErrorMessage="URL must be as http://www.address.com or www.address.com or address.com" ValidationExpression="(([\w]+:)?//)?([\w-]+\.)?[\w-]+(/[\w- ./?%&=]*)?">*</asp:RegularExpressionValidator>--%>
					<br />
					<asp:TextBox ID="UrlTextBox" runat="server" MaxLength="50" ReadOnly="true" Enabled="false" /><br />
				</div>
				
				<div class="input-box">
					<asp:Label ID="LanguageLabel" Text="Language" AssociatedControlID="LanguageTextBox" runat="server" /> *
					<br />
					<asp:HiddenField ID="LanguageIdHiddenField" runat="server" />
					<asp:TextBox ID="LanguageTextBox" runat="server" ReadOnly="true" Enabled="false" /><br />
				</div>
				
				<div class="input-box">
					<asp:Label ID="LayoutRatioLabel" Text="Alternate Layout Ratio %" AssociatedControlID="LayoutRatioTextBox" runat="server" /> *
					<asp:RequiredFieldValidator ID="LayoutRatioRequiredValidator" runat="server" ControlToValidate="LayoutRatioTextBox" ValidationGroup="channelValidation" Display="None" ErrorMessage="Alternate Layout Ratio is a Required fild"/>
					<asp:CustomValidator ID="LayoutCustomValidator" runat="server" ControlToValidate="LayoutRatioTextBox" ErrorMessage="Alternate Layout Ratio must be in decimal format, between 0 and 100." Display="None" ValidationGroup="channelValidation" />
					<br />
					<asp:TextBox ID="LayoutRatioTextBox" runat="server" /><br />
				</div>
				
				<div class="input-box">
					<asp:Label ID="ThemeLabel" Text="Theme" AssociatedControlID="ThemeList" runat="server" />
					<br />
					<asp:DropDownList ID="ThemeList" runat="server" DataValueField="Id" DataTextField="Title"></asp:DropDownList>
					<br />
				</div>
			</div>
	        
			<div id="divThirdMain" class="column">
			</div>
	        
			<div id="divFourthMain" class="column">
			</div>
		</div>
		
		<div class="content-box">
		<div class="input-box-float-left">
			<asp:Label ID="StartNodeCaptionLabel" Text="Product"  CssClass="main-caption" runat="server" />
			<br /><br />
			<span><%=Resources.General.Literal_ProductPageContentNode %></span>
			<br />
			<uc:ContentPageSelector ID="ProductTemplateContentNodeSelector" runat="server" OnlyProductPagesSelectable="true" />
	    </div>
	    <br class="clear" /><br />
	    <div class="input-box-float-left">		    
			<label class="main-caption">Order</label>
			<br /><br />
			<span><%=Resources.General.Literal_OrderDetailPageContentNode %></span>
			<br />
			<asp:DropDownList runat="server" ID="OrderDetailTemplateList" DataTextField="Title" DataValueField="Id" >
			</asp:DropDownList>
	    </div>
	    </div>
	    </div>
	    
		<br />
	    <asp:UpdatePanel id="FooterUpdatePanel" runat="server">
				<ContentTemplate>
					<div id="divButtons" class="buttons-container buttons">
						<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" Text="Save" ValidationGroup="channelValidation" runat="server" OnClientClick="DisableFormWithValidation('channelValidation');" SkinID="DefaultButton" />
						<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancleButton" Text="Cancel" runat="server"  SkinID="DefaultButton" />
					</div>  
					</ContentTemplate>
		</asp:UpdatePanel>
		</div>
	</asp:Panel>
</asp:Content>
