﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
    public partial class CampaignEdit : StatePageController<CampaignState>, IEditor
    {
        protected const string ConditionControlId = "ConditionControl";
        protected const string ActionControlId = "ActionControl";

        protected virtual CampaignsMaster MasterPage
        {
            get
            {
                return (CampaignsMaster)Master;
            }
        }

        protected CampaignType CurrentCampaignType
        {
            get
            {
				return MasterPage.CurrentCampaignType;
            }
        }

    	private bool? _isCartCampaign;
    	protected bool IsCartCampaign
    	{
    		get
    		{
    			if (!_isCartCampaign.HasValue)
    			{
    				_isCartCampaign = CurrentCampaignType.Equals(CampaignType.CartCampaign);
    			}
    			return _isCartCampaign.Value;
    		}
    	}

    	private bool? _isProductCampaign;
		protected bool IsProductCampaign
    	{
    		get
    		{
				if (!_isProductCampaign.HasValue)
    			{
					_isProductCampaign = CurrentCampaignType.Equals(CampaignType.ProductCampaign);
    			}
				return _isProductCampaign.Value;
    		}
    	}
		

        #region State

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual Collection<ICondition> GetConditionsFromState()
        {
			if (IsCartCampaign)
            {
                return ((ICartCampaign)State.Campaign).Conditions;
			}
			if (IsProductCampaign)
			{
				return ((IProductCampaign)State.Campaign).Conditions;
			}
			throw new InvalidOperationException("Unknown campaign.");
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual Collection<IAction> GetActionsFromState()
        {
			if (IsCartCampaign)
            {
                return ConvertActions(((ICartCampaign) State.Campaign).Actions);
            }
			if (IsProductCampaign)
			{
				return ConvertActions(((IProductCampaign)State.Campaign).Actions);
			}
			throw new InvalidOperationException("Unknown campaign.");
        }

        #endregion

        #region Convert

        protected virtual Collection<IAction> ConvertActions<T>(Collection<T> actions) where T : IAction
        {
            var list = new Collection<IAction>();
            foreach (var action in actions)
            {
                list.Add(action);
            }
            return list;
        }

        protected virtual Collection<IActionType> ConvertActionTypes<T>(Collection<T> actionTypes) where T : IActionType
        {
            var list = new Collection<IActionType>();
            foreach (var action in actionTypes)
            {
                list.Add(action);
            }
            return list;
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            if (IsPostBack)
            {
                EnsureConditionControl();
                EnsureActionControl();
            }
            base.OnInit(e);
        }

        protected override void SetEventHandlers()
        {
            SaveButton.Click += OnSave;
            CancelButton.Click += OnCancel;

            OpenConditionPopupButton.Click += OnOpenEmptyConditionPopup;
            OpenActionPopupButton.Click += OnOpenEmptyActionPopup;

            ConditionListControl.ListItemEdit += OnOpenConditionPopup;
            ActionListControl.ListItemEdit += OnOpenActionPopup;

            ConditionListControl.ListItemRemove += OnConditionRemove;
            ActionListControl.ListItemRemove += OnActionRemove;

            ConditionPopupOkButton.Click += OnSaveCondition;
            ActionPopupOkButton.Click += OnSaveAction;

            ActionListControl.ListItemOrdinalChange += OnActionOrdinalChange;

            DeleteButton.Click += BtnDelete_Click;
        }

        protected void SetButtonDeleteVisibility(ICampaign campaign)
        {

            DeleteButton.Visible = campaign.Id > 0;
        }

        protected virtual void BtnDelete_Click(object sender, EventArgs e)
        {

            if (State.Campaign is ICartCampaign)
            {
                IoC.Resolve<ICartCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, State.Campaign.Id);
            }
            else if (State.Campaign is IProductCampaign)
            {
                IoC.Resolve<IProductCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, State.Campaign.Id);
            }
            else
            {
                throw new InvalidOperationException("Unknown campaign.");
            }


            string redirectUrl;
            switch (CurrentCampaignType)
            {
                case CampaignType.CartCampaign:
                    redirectUrl = PathHelper.Campaign.GetDefaultUrlForCartCampaigns();
                    break;
                case CampaignType.ProductCampaign:
                    redirectUrl = PathHelper.Campaign.GetDefaultUrlForProductCampaigns();
                    break;
                default:
                    redirectUrl = string.Empty;
                    break;
            }
            Response.Redirect(redirectUrl);
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterCollapsiblePanelScripts();
			MasterPage.SetupTabAndPanel();
        }

        public void OnOpenEmptyConditionPopup(object sender, EventArgs e)
        {
            if (ConditionTypeList.Items.Count == 0)
            {
                throw new InvalidOperationException("The list of available condition types is empty.");
            }
            if (ConditionTypeList.SelectedValue == null)
            {
                throw new InvalidOperationException("Condition type has not been selected.");
            }
            int conditionTypeId = int.Parse(ConditionTypeList.SelectedValue, CultureInfo.CurrentCulture);
            var conditionType = IoC.Resolve<IConditionTypeSecureService>().GetById(conditionTypeId);
            var condition = IoC.Resolve<IConditionSecureService>().Create(conditionType.CommonName);
            OpenConditionPopup(condition);
        }

        public void OnOpenEmptyActionPopup(object sender, EventArgs e)
        {
            if (ActionTypeList.Items.Count == 0)
            {
                throw new InvalidOperationException("The list of available action types is empty.");
            }
            if (ActionTypeList.SelectedValue == null)
            {
                throw new InvalidOperationException("Action type has not been selected.");
            }

            OpenActionPopup(CreateAction());
        }

        private IAction CreateAction()
        {
            int actionTypeId = int.Parse(ActionTypeList.SelectedValue, CultureInfo.CurrentCulture);

			if (IsCartCampaign)
            {
                var actionType = IoC.Resolve<ICartActionTypeSecureService>().GetById(actionTypeId);
                return IoC.Resolve<ICartActionSecureService>().Create(actionType.CommonName); 
            }
			if (IsProductCampaign)
            {
                var actionType = IoC.Resolve<IProductActionTypeSecureService>().GetById(actionTypeId);
                return IoC.Resolve<IProductActionSecureService>().Create(actionType.CommonName);
			}
			throw new InvalidOperationException("Unknown campaign.");
        }

        public void OnOpenConditionPopup(object sender, CommandEventArgs e)
        {
            if (e.CommandArgument == null)
            {
                throw new ArgumentException("CommandArgument does not contain condition identifier.");
            }

            var condition = GetConditionsFromState().FirstOrDefault(
                item => item.Guid.Equals(new Guid(e.CommandArgument.ToString())));

            OpenConditionPopup(condition);
        }

        public void OnOpenActionPopup(object sender, CommandEventArgs e)
        {
            if (e.CommandArgument == null)
            {
                throw new ArgumentException("CommandArgument does not contain action identifier.");
            }

            var action = GetActionsFromState().FirstOrDefault(
                item => item.Guid.Equals(new Guid(e.CommandArgument.ToString())));

            OpenActionPopup(action);
        }

        public void OnConditionRemove(object sender, CommandEventArgs e)
        {
            if (e.CommandArgument == null)
            {
                throw new ArgumentException("CommandArgument does not contain condition identifier.");
            }
            MarkConditionAsDeleted(new Guid(e.CommandArgument.ToString()));
            RenderConditionList(GetConditionsFromState());
        }

        public void OnActionRemove(object sender, CommandEventArgs e)
        {
            if (e.CommandArgument == null)
            {
                throw new ArgumentException("CommandArgument does not contain action identifier.");
            }
            MarkActionAsDeleted(new Guid(e.CommandArgument.ToString()));
            RenderActionList(GetActionsFromState());
        }

        public void OnActionOrdinalChange(object sender, ActionOrdinalEventArgs e)
        {
            foreach (var action in GetActionsFromState())
            {
                int ordinal;
                if (e.ActionOrdinals.TryGetValue(action.Guid, out ordinal))
                {
                    action.Ordinal = ordinal;
                }
            }
            RenderActionList(GetActionsFromState());
        }

        public void OnSaveCondition(object sender, EventArgs e)
        {
            var conditionControl = ConditionControlPlaceHolder.FindControl(ConditionControlId)
                as ICampaignPluginControl<ICondition>;

            if (conditionControl == null)
            {
                throw new InvalidOperationException(
                    "The operation requires a condition plugin control, " +
                    "but no such control was added to the ConditionControlPlaceHolder control collection.");
            }

            ICondition condition;
            if (conditionControl.TryGet(out condition))
            {
                PersistInState(condition);
                RenderConditionList(GetConditionsFromState());
            }
            else
			{
				ConditionPopupValidationMessage.AddRange(conditionControl.ValidationMessages);
                ConditionPopup.Show();
            }
        }

        public void OnSaveAction(object sender, EventArgs e)
        {
			if (IsCartCampaign)
            {
                OnSaveCartAction();
            }
			else if (IsProductCampaign)
            {
                OnSaveProductAction();
			}
			else
			{
				throw new InvalidOperationException("Unknown campaign.");
			}
        }

        protected virtual void OnSaveCartAction()
        {
            var actionControl = ActionControlPlaceHolder.FindControl(ActionControlId)
                as ICampaignPluginControl<ICartAction>;

            if (actionControl == null)
            {
                throw new InvalidOperationException(
                    "The operation requires an action plugin control, " +
                    "but no such control was registered to the ActionControlPlaceHolder control collection.");
            }

            ICartAction action;

            if (actionControl.TryGet(out action))
            {
                PersistInState(action);
                RenderActionList(GetActionsFromState());
            }
            else
            {
                ActionPopupValidationMessage.AddRange(actionControl.ValidationMessages);
                ActionPopup.Show();
            }
        }

        protected virtual void OnSaveProductAction()
        {
            var actionControl = ActionControlPlaceHolder.FindControl(ActionControlId)
                as ICampaignPluginControl<IProductAction>;

            if (actionControl == null)
            {
                throw new InvalidOperationException(
                    "The operation requires an action plugin control, " +
                    "but no such control was registered to the ActionControlPlaceHolder control collection.");
            }

            IProductAction action;

            if (actionControl.TryGet(out action))
            {
                PersistInState(action);
                RenderActionList(GetActionsFromState());
            }
            else
            {
                ActionPopupValidationMessage.AddRange(actionControl.ValidationMessages);
                ActionPopup.Show();
            }
        }

        public void OnCancel(object sender, EventArgs e)
        {
            MasterPage.RedirectToDefaultPage();
        }

        public void OnSave(object sender, EventArgs e)
        {
            if (State == null)
            {
                throw new InvalidOperationException("No campaign was persisted in State.");
            }

            ICampaign campaignGeneralInfo;
            if (CampaignGeneralControl.TryGet(out campaignGeneralInfo))
            {
                State.Campaign.Title = campaignGeneralInfo.Title;
                State.Campaign.CampaignStatus.Id = campaignGeneralInfo.CampaignStatus.Id;
                State.Campaign.StartDate = campaignGeneralInfo.StartDate;
                State.Campaign.EndDate = campaignGeneralInfo.EndDate;
                State.Campaign.RegistryId = campaignGeneralInfo.RegistryId;
                State.Campaign.Exclusive = campaignGeneralInfo.Exclusive;
                State.Campaign.FolderId = campaignGeneralInfo.FolderId;

                ICampaign campaign;
                if (State.Campaign is ICartCampaign)
                {
                    var campaignService = IoC.Resolve<ICartCampaignSecureService>();
                    int campaignId = campaignService.Save(SignInHelper.SignedInSystemUser, (ICartCampaign)State.Campaign);
                    campaign = campaignService.GetById(campaignId);
                }
                else if (State.Campaign is IProductCampaign)
                {
                    var campaignService = IoC.Resolve<IProductCampaignSecureService>();
                    int campaignId = campaignService.Save(SignInHelper.SignedInSystemUser, (IProductCampaign)State.Campaign);
                    campaign = campaignService.GetById(campaignId);
                }
                else
                {
					throw new InvalidOperationException("Unknown campaign.");
                }

                LoadForm(campaign);
                SetButtonDeleteVisibility(campaign);
                CampaignValidationMessage.Add(Resources.GeneralMessage.SaveSuccessCampaign);
                CampaignValidationMessage.MessageType = InfoType.Success;
            	UpdateMaster(campaign.FolderId);
            }
            else
            {
                CampaignValidationMessage.Add(CampaignGeneralControl.ValidationMessage);
                CampaignValidationMessage.MessageType = InfoType.Warning;
            }
        }

		protected virtual void PersistInState(ICondition condition)
        {
            if (condition == null) throw new ArgumentNullException("condition");

            var conditions = GetConditionsFromState();
            var conditionFromState = conditions.FirstOrDefault(
                item => item.Guid.Equals(condition.Guid));

            if (conditionFromState == null)
            {
                conditions.Add(condition);
            }
            else
            {
                conditions[conditions.IndexOf(conditionFromState)] = condition;
            }
        }

        protected virtual void PersistInState(ICartAction action)
        {
            if (action == null) throw new ArgumentNullException("action");

            var campaign = (ICartCampaign)State.Campaign;

            var actionFromState = campaign.Actions.FirstOrDefault(
                            item => item.Guid.Equals(action.Guid));

            if (actionFromState == null)
            {
                campaign.Actions.Add(action);
            }
            else
            {
                campaign.Actions[campaign.Actions.IndexOf(actionFromState)] = action;
            }
        }

        protected virtual void PersistInState(IProductAction action)
        {
            if (action == null) throw new ArgumentNullException("action");

            var campaign = (IProductCampaign) State.Campaign;

            var actionFromState = campaign.Actions.FirstOrDefault(
                            item => item.Guid.Equals(action.Guid));

            if (actionFromState == null)
            {
                campaign.Actions.Add(action);
            }
            else
            {
                campaign.Actions[campaign.Actions.IndexOf(actionFromState)] = action;
            }
        }

        protected virtual void MarkConditionAsDeleted(Guid id)
        {
            foreach (var condition in GetConditionsFromState())
            {
                if (condition.Guid.Equals(id))
                {
                    condition.Status = BusinessObjectStatus.Deleted;
                    return;
                }
            }
            throw new ArgumentException(
                string.Format(CultureInfo.CurrentCulture,
                "Condition with Guid {0} does not exist in State.", id));
        }

        protected virtual void MarkActionAsDeleted(Guid id)
        {
            foreach (var action in GetActionsFromState())
            {
                if (action.Guid.Equals(id))
                {
                    action.Status = BusinessObjectStatus.Deleted;
                    return;
                }
            }
            throw new ArgumentException(
                string.Format(CultureInfo.CurrentCulture,
                "Action with Guid {0} does not exist in State.", id));
        }

        protected virtual void OpenConditionPopup(ICondition condition)
        {
            if (condition == null) throw new ArgumentNullException("condition");

            var setting = new CampaignControlSetting(condition.ConditionType.CommonName);
            var virtualPath = setting.ControlVirtualPath;
            var control = CreateConditionControl(virtualPath);
            LoadConditionControl(control);
            RegisterConditionControl(virtualPath);
            ((ICampaignPluginControl<ICondition>)control).DataBind(condition);
            ConditionPopup.Show();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        protected virtual void OpenActionPopup(IAction action)
        {
            if (action is ICartAction)
            {
                var typedAction = (ICartAction)action;
                var setting = new CampaignControlSetting(typedAction.ActionType.CommonName);
                var virtualPath = setting.ControlVirtualPath;
                var control = CreateActionControl(virtualPath);
                LoadActionControl(control);
                RegisterActionControl(virtualPath);
                ((ICampaignPluginControl<ICartAction>)control).DataBind(typedAction);
			}
			else if (State.Campaign is IProductCampaign)
            {
                var typedAction = (IProductAction)action;
                var setting = new CampaignControlSetting(typedAction.ActionType.CommonName);
                var virtualPath = setting.ControlVirtualPath;
                var control = CreateActionControl(virtualPath);
                LoadActionControl(control);
                RegisterActionControl(virtualPath);
                ((ICampaignPluginControl<IProductAction>)control).DataBind(typedAction);
			}
			else
			{
				throw new InvalidOperationException("Unknown campaign.");
			}
            ActionPopup.Show();
        }

        protected virtual Control CreateConditionControl(string virtualPath)
        {
            if (virtualPath == null) throw new ArgumentNullException("virtualPath");

            var control = LoadControl(virtualPath);
            control.ID = ConditionControlId;
            return control;
        }

        protected virtual Control CreateActionControl(string virtualPath)
        {
            if (virtualPath == null) throw new ArgumentNullException("virtualPath");

            var control = LoadControl(virtualPath);
            control.ID = ActionControlId;
            return control;
        }

        protected virtual void LoadConditionControl(Control control)
        {
            if (control == null) throw new ArgumentNullException("control");

            ConditionControlPlaceHolder.Controls.Clear();
            ConditionControlPlaceHolder.Controls.Add(control);
        }

        protected virtual void LoadActionControl(Control control)
        {
            if (control == null) throw new ArgumentNullException("control");

            ActionControlPlaceHolder.Controls.Clear();
            ActionControlPlaceHolder.Controls.Add(control);
        }

        protected virtual void RegisterConditionControl(string virtualPath)
        {
            if (virtualPath == null) throw new ArgumentNullException("virtualPath");

			ConditionControlToInitializeHiddenField.Value = virtualPath;
        }

        protected virtual void RegisterActionControl(string virtualPath)
        {
            if (virtualPath == null) throw new ArgumentNullException("virtualPath");

            ActionControlToInitializeHiddenField.Value = virtualPath;
        }

        protected virtual void EnsureConditionControl()
        {
            var virtualPath = GetFormElementValue(ConditionControlToInitializeHiddenField.UniqueID);
            if (!virtualPath.IsNullOrEmpty())
            {
                LoadConditionControl(CreateConditionControl(virtualPath));
            }
        }

        protected virtual void EnsureActionControl()
        {
            var virtualPath = GetFormElementValue(ActionControlToInitializeHiddenField.UniqueID);
            if (!virtualPath.IsNullOrEmpty())
            {
                LoadActionControl(CreateActionControl(virtualPath));
            }
        }

        protected virtual string GetFormElementValue(string controlId)
        {
            NameValueCollection formElements = Request.Form;
            return formElements[controlId];
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual Collection<IConditionType> GetConditionTypes()
        {
            return IoC.Resolve<IConditionTypeSecureService>().GetAll();
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual Collection<IActionType> GetActionTypes()
        {
			if (IsCartCampaign)
			{
				return ConvertActionTypes(IoC.Resolve<ICartActionTypeSecureService>().GetAll());
			}
			if (IsProductCampaign)
			{
				return ConvertActionTypes(IoC.Resolve<IProductActionTypeSecureService>().GetAll());
			}
			throw new InvalidOperationException("Unknown campaign.");
        }

        protected virtual ICampaign GetCampaign(int? campaignId)
        {
			if (IsCartCampaign)
            {
                return GetCartCampaign(campaignId);
            }
			if (IsProductCampaign)
            {
                return GetProductCampaign(campaignId);
            }
            throw new InvalidOperationException();
        }

        protected virtual ICartCampaign GetCartCampaign(int? campaignId)
        {
            var service = IoC.Resolve<ICartCampaignSecureService>();
            if (!campaignId.HasValue)
            {
                var newCampaign = service.Create(SignInHelper.SignedInSystemUser);
                if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
                {
                    newCampaign.FolderId = MasterPage.SelectedFolderId.Value;
                }
                return newCampaign;
            }
            var campaign = service.GetById(campaignId.Value);
            if (campaign == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                    CultureInfo.CurrentCulture,
                    "Cart campaign with id {0} does not exist.", campaignId));
            }
            return campaign;
        }

        protected virtual IProductCampaign GetProductCampaign(int? campaignId)
        {
            var service = IoC.Resolve<IProductCampaignSecureService>();
            if (!campaignId.HasValue)
            {
                var newCampaign = service.Create(SignInHelper.SignedInSystemUser);
                if (MasterPage.SelectedFolderId.HasValue && MasterPage.SelectedFolderId != MasterPage.RootNodeId)
                {
                    newCampaign.FolderId = MasterPage.SelectedFolderId.Value;
                }
                return newCampaign;
            }
            var campaign = service.GetById(campaignId.Value);
            if (campaign == null)
            {
                throw new InvalidOperationException(
                    string.Format(
                    CultureInfo.CurrentCulture,
                    "Product campaign with id {0} does not exist.", campaignId));
            }
            return campaign;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected virtual int? GetCampaignId()
        {
            return Request.QueryString.GetInt32OrNull("CampaignId");
        }

        protected override void PopulateForm()
        {
            var campaign = GetCampaign(GetCampaignId());
            RenderConditionTypes(GetConditionTypes());
            RenderActionTypes(GetActionTypes());
            LoadForm(campaign);
            SetButtonDeleteVisibility(campaign);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        protected virtual void LoadForm(ICampaign campaign)
        {
            if (campaign == null) throw new ArgumentNullException("campaign");

            RenderGeneralInfo(campaign);

            if (campaign is ICartCampaign)
            {
                var typedCampaign = (ICartCampaign) campaign;
                RenderConditionList(typedCampaign.Conditions);
                RenderActionList(ConvertActions(typedCampaign.Actions));
            }
            else if (campaign is IProductCampaign)
            {
                var typedCampaign = (IProductCampaign)campaign;
                RenderConditionList(typedCampaign.Conditions);
                RenderActionList(ConvertActions(typedCampaign.Actions));
            }
            else
            {
				throw new InvalidOperationException("Unknown campaign.");
            }

            var campaignState = new CampaignState(campaign);
            State = campaignState;
        }

        protected virtual void RenderConditionTypes(Collection<IConditionType> conditions)
        {
            if (conditions == null) throw new ArgumentNullException("conditions");

            ConditionTypeList.DataSource = conditions;
            ConditionTypeList.DataValueField = "Id";
            ConditionTypeList.DataTextField = "Title";
            ConditionTypeList.DataBind();
        }

        protected virtual void RenderActionTypes(Collection<IActionType> actions)
        {
            if (actions == null) throw new ArgumentNullException("actions");

            ActionTypeList.DataSource = actions;
            ActionTypeList.DataValueField = "Id";
            ActionTypeList.DataTextField = "Title";
            ActionTypeList.DataBind();
        }

        protected virtual void RenderConditionList(Collection<ICondition> conditions)
        {
            if (conditions == null) throw new ArgumentNullException("conditions");

            ConditionListControl.DataBind(conditions);
        }

        protected virtual void RenderActionList(Collection<IAction> actions)
        {
            if (actions == null) throw new ArgumentNullException("actions");

            ActionListControl.DataBind(actions);
        }

        protected virtual void RenderGeneralInfo(ICampaign campaign)
        {
            if (campaign == null) throw new ArgumentNullException("campaign");

            CampaignGeneralControl.DataBind(campaign, MasterPage.CurrentCampaignType);
        }

        protected virtual void RegisterCollapsiblePanelScripts()
        {
			string actionCollapsiblescript = "OpenClose('" + ActionPanelDiv.ClientID + "','" + ActionPanelStateHidden.ClientID + "','" + ActionIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
            ActionCollapsibleDiv.Attributes.Add("onclick", actionCollapsiblescript);
            ActionCollapsibleCenterDiv.Attributes.Add("onclick", actionCollapsiblescript);

			string conditionCollapsiblescript = "OpenClose('" + ConditionPanelDiv.ClientID + "','" + ConditionPanelStateHidden.ClientID + "','" + ConditionIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
            ConditionCollapsibleDiv.Attributes.Add("onclick", conditionCollapsiblescript);
            ConditionCollapsibleCenterDiv.Attributes.Add("onclick", conditionCollapsiblescript);
        }

		protected virtual void UpdateMaster(int folderId)
		{
			MasterPage.SelectedFolderId = folderId;
			MasterPage.PopulateTree(folderId);
			MasterPage.UpdateBreadcrumbs(folderId);
		}
    }

    [Serializable]
    public class CampaignState
    {
        public ICampaign Campaign { get; set; }
        public CampaignState(){}
        public CampaignState(ICampaign campaign)
        {
            Campaign = campaign;
        }
    }
}
