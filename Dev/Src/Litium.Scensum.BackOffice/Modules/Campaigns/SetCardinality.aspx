﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Campaigns/CampaignsMaster.Master" CodeBehind="SetCardinality.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Campaigns.SetCardinality" MaintainScrollPositionOnPostback="true" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MessageContainer" runat="server">		
		<uc:MessageContainer ID="ErrorMessager"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
		<uc:MessageContainer ID="SuccessMessager"  MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
</asp:Content>
<asp:Content ID="CardinalityContent" ContentPlaceHolderID="ProductCampaignsPlaceHolder" runat="server">
	<script type="text/javascript">
        function confirmDelete() {
        	return DeleteConfirmation("<%= Resources.Campaign.Literal_campaign %>");
        }
	</script>
	<asp:Panel ID="SortPanel" runat="server" DefaultButton="SaveButton">
		<div class="pages-full-content">
			<div id="sort-content">
				<asp:GridView 
					ID="CampaignsGrid" 
					SkinID="grid" 
					runat="server"  
					AutoGenerateColumns="false"
					Width="100%">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server" />
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>	
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="34%">
							<ItemTemplate>
							<div style="width:190px; overflow:hidden">
								<uc:LinkButtonTrimmed ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' CommandName="NavigateCampaign" CommandArgument='<%#Eval("Id")%>' MaxLength="50"></uc:LinkButtonTrimmed>
							</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="8%" >
							<ItemTemplate>
								<uc:LiteralEncoded ID="TypeLiteral" runat="server" Text='<%# Convert.ToBoolean(Eval("Exclusive")) ? "Selective" : "Always" %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_StartDate %>" ItemStyle-Width="18%" >
							<ItemTemplate>
								<uc:LiteralEncoded ID="StartDateLiteral" runat="server" Text='<%# Eval("StartDate") != null ? Convert.ToDateTime(Eval("StartDate")).ToString() : string.Empty %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_EndDate %>" ItemStyle-Width="18%" >
							<ItemTemplate>
								<uc:LiteralEncoded ID="EndDateLiteral" runat="server"  Text='<%# Eval("EndDate") != null ? Convert.ToDateTime(Eval("EndDate")).ToString() : string.Empty %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="6%" >
							<ItemTemplate>
								<uc:LiteralEncoded ID="StatusLiteral" runat="server" Text='<%# Eval("CampaignStatus.Title") %>'></uc:LiteralEncoded>
							</ItemTemplate>
						</asp:TemplateField>								
						<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
							<HeaderTemplate>
								<div class="inline">
									<%= Resources.General.Literal_SortOrder %>
									<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" CommandArgument="0" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
								</div>
							</HeaderTemplate>
							<ItemTemplate>
								<div class="inline">
									<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
									<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
									<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%#Eval("Priority") %>' />
									<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
									<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
								</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<ItemTemplate>
								<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteCampaign"
									CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
									OnClientClick="return confirmDelete();" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
			<div id="AllSelectedDiv" class="product-campaigns-apply" runat="server">
				<div style="float:left">
					<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
					<asp:DropDownList ID="StatusList" DataTextField="Title" Width="100px" DataValueField="Id" runat="server" /> &nbsp;
					<asp:DropDownList ID="TypeList" Width="100px" runat="server" /> &nbsp;
				</div> 
				<div style="float:left">
					<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="SetButton" runat="server" Text="<%$ Resources:General,Button_Set %>" />
					<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="DeleteCampaignButton" runat="server" Text="<%$ Resources:General,Button_Delete %>" OnClientClick="return confirmDelete();" />
				</div> 
			</div>
		</div>
		<br /><br />
		<div class="buttons right">
			<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgOrdinals" SkinID="DefaultButton"/>
			<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>" CausesValidation="false" SkinID="DefaultButton" />
		</div>
	</asp:Panel>
</asp:Content>
