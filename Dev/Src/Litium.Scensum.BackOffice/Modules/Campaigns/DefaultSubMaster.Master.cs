﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public partial class DefaultSubMaster : MasterPage, IEditor
	{
		private Collection<ICampaignFolder> _allFolders;
		private Collection<ICampaignStatus> _statuses;
		private Collection<ContextMenuDataSourceItem> _contextMenuStatuses;

		public virtual CampaignsMaster MasterPage
		{
			get
			{
				return (CampaignsMaster)Master;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CampaignsGrid.RowCommand += OnRowCommand;
			CampaignsGrid.RowDataBound += OnRowDataBound;
			CampaignsGrid.PageIndexChanging += OnPageIndexChanging;
			SetButton.Click += OnSave;
			DeleteCampaignButton.Click += OnDelete;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack)
			{
				PopulateCampaigns();
			}
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
		}

		public virtual void OnDelete(object sender, EventArgs e)
		{
			IEnumerable<int> ids = CampaignsGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				Messager.Add(Resources.CampaignMessage.NoCampaignSelected);
				Messager.MessageType = InfoType.Warning;
				return;
			}
			DeleteCampaigns(ids);

			Messager.Add(Resources.GeneralMessage.DeleteSeccessful);
			Messager.MessageType = InfoType.Success;

			PopulateCampaigns();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(StatusList.SelectedValue) && string.IsNullOrEmpty(TypeList.SelectedValue))
			{
				Messager.Add(Resources.CampaignMessage.NoSetValue);
				return;
			}
			IEnumerable<int> ids = CampaignsGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				Messager.Add(Resources.CampaignMessage.NoCampaignSelected);
				Messager.MessageType = InfoType.Warning;
				return;
			}
			var service = IoC.Resolve<ICampaignSecureService>();
			int statusId;
			if (int.TryParse(StatusList.SelectedValue, out statusId))
			{
				service.SetStatus(SignInHelper.SignedInSystemUser, ids, statusId);
			}
			if (!string.IsNullOrEmpty(TypeList.SelectedValue))
			{
				service.SetType(SignInHelper.SignedInSystemUser, ids,
					TypeList.SelectedValue == Resources.Campaign.Literal_TypeSelective);
			}

			Messager.Add(Resources.GeneralMessage.SaveSuccessCampaigns);
			Messager.MessageType = InfoType.Success;

			PopulateCampaigns();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id)) return;

			var service = IoC.Resolve<ICampaignSecureService>();
			switch (e.CommandName)
			{
				case "MoveCampaign":
					MasterPage.RedirectWithType(PathHelper.Campaign.GetCampaignMoveUrl(id));
					break;
				case "DeleteCampaign":
					DeleteCampaign(id);
					Messager.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
					break;
				case "SetAlways":
					service.SetType(SignInHelper.SignedInSystemUser, id, false);
					Messager.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
					break;
				case "SetSelective":
					service.SetType(SignInHelper.SignedInSystemUser, id, true);
					Messager.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
					break;
			}
			PopulateCampaigns();
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);

			var row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var campaign = row.DataItem as ICampaign;
			if (campaign == null) return;

			var statusList = (ContextMenu3)row.FindControl("StatusList");
			statusList.DataSource = GetContextMenuStatuses();
			statusList.DataBind();
			statusList.SelectedValue = campaign.CampaignStatus.Id.ToString(CultureInfo.CurrentCulture);
		}

		protected void OnCampaignStatusChanged(object sender, ContextMenu3EventArgs e)
		{
			int statusId;
			if (!int.TryParse(e.SelectedValue, out statusId)) return;

			var idField = (HiddenField)((Control)sender).Parent.Parent.FindControl("IdHiddenField");
			int id;
			if (!int.TryParse(idField.Value, out id)) return;

			var service = IoC.Resolve<ICampaignSecureService>();
			service.SetStatus(SignInHelper.SignedInSystemUser, id, statusId);
			Messager.Add(Resources.GeneralMessage.SaveSuccessCampaign, InfoType.Success);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CampaignsGrid.PageIndex = e.NewPageIndex;
			PopulateCampaigns();
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected virtual void PopulateCampaigns()
		{
			if (IsSearchResult)
			{
				CampaignsGrid.DataSource = SearchCampaigns();
				CampaignsGrid.Columns[2].Visible = false;
				CampaignsGrid.Columns[3].Visible = false;
				CampaignsGrid.Columns[4].Visible = false;
				CampaignsGrid.Columns[5].Visible = false;
				CampaignsGrid.Columns[6].Visible = true;
			}
			else
			{
				CampaignsGrid.DataSource = RetrieveCampaigns();
				CampaignsGrid.Columns[2].Visible = true;
				CampaignsGrid.Columns[3].Visible = true;
				CampaignsGrid.Columns[4].Visible = true;
				CampaignsGrid.Columns[5].Visible = true;
				CampaignsGrid.Columns[6].Visible = false;
			}
			CampaignsGrid.DataBind();
			CampaignsGrid.Visible = CampaignsGrid.DataSource != null;

			bool resultExists = AllSelectedDiv.Visible = CampaignsGrid.Rows.Count > 0;
			if (resultExists)
			{
				PopulateStatuses();
				PopulateTypes();
			}
		}

		protected virtual Collection<ICampaign> RetrieveCampaigns()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return IoC.Resolve<ICampaignSecureService>().GetAllByFolder(folderId.Value);
			}
			return null;
		}

		protected virtual Collection<ICampaign> SearchCampaigns()
		{
			var searchUrl = MasterPage.GetSearchPageUrl();
			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);
			if (string.IsNullOrEmpty(searchCriteria)) return new Collection<ICampaign>();

			var service = IoC.Resolve<ICampaignSecureService>();
			return MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
				? service.SearchProductCampaigns(searchCriteria)
				: service.SearchCartCampaigns(searchCriteria);
		}

		protected virtual void PopulateStatuses()
		{
			StatusList.DataSource = RetrieveStatuses();
			StatusList.DataBind();
			StatusList.Items.Insert(0, new ListItem(Resources.Campaign.Literal_StatusEmpty, string.Empty) { Selected = true });
		}

		protected virtual void PopulateTypes()
		{
			TypeList.Items.Clear();
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeEmpty, string.Empty) { Selected = true });
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeAlways, Resources.Campaign.Literal_TypeAlways));
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeSelective, Resources.Campaign.Literal_TypeSelective));
		}

		protected virtual string GetEditUrl(object campaignId)
		{
			string url = PathHelper.Campaign.GetEditUrl(System.Convert.ToInt32(campaignId, CultureInfo.CurrentCulture));
			return MasterPage.GetUrlWithType(url);
		}

		protected virtual string GetPath(object folderId)
		{
			if(_allFolders == null)
			{
				var service = IoC.Resolve<ICampaignFolderSecureService>();
				_allFolders = MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
				              	? service.GetAllProductCampaignFolders()
				              	: service.GetAllCartCampaignFolders();
			}

			var folders = _allFolders;
			var folder = folders.FirstOrDefault(f => f.Id == System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
			if (folder == null) return string.Empty;

			var stack = new Stack<string>();
			stack.Push(folder.Title);
			while (folder.ParentFolderId.HasValue)
			{
				var child = folder;
				folder = folders.FirstOrDefault(f => f.Id == child.ParentFolderId.Value);
				stack.Push(folder.Title);
			}

			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString().TrimEnd(new[] { '\\' });
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", 
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual void DeleteCampaign(int campaignId)
		{
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				IoC.Resolve<IProductCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignId);
			}
			else
			{
				IoC.Resolve<ICartCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignId);
			}
		}

		protected virtual void DeleteCampaigns(IEnumerable<int> campaignIds)
		{
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				IoC.Resolve<IProductCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignIds);
			}
			else
			{
				IoC.Resolve<ICartCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignIds);
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ICampaignStatus> RetrieveStatuses()
		{
			return _statuses 
				?? (_statuses = IoC.Resolve<ICampaignStatusSecureService>().GetAll());
			
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ContextMenuDataSourceItem> GetContextMenuStatuses()
		{
			return _contextMenuStatuses 
				?? (_contextMenuStatuses = new Collection<ContextMenuDataSourceItem>(RetrieveStatuses().Select(
					item => new ContextMenuDataSourceItem
					{
						Text = item.Title,
						Value = item.Id.ToString(CultureInfo.CurrentCulture),
						ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture,
						"~/Media/Images/Common/{0}.gif", item.CommonName))
					}).ToList()));
		}
	}
}
