using System;
using System.Collections.Generic;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public partial class CampaignMove : PageController, IEditor
	{
		protected virtual CampaignsMaster MasterPage
		{
			get
			{
				return (CampaignsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			CampaignsTree.NodeCommand += OnNodeCommand;
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var campaign = IoC.Resolve<ICampaignSecureService>().GetById(GetId());
			PopulateTree(campaign.FolderId);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
		}
        
		public void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}

		public void OnSave(object sender, EventArgs e)
		{
			int? id = CampaignsTree.SelectedNodeId;
			if (id.HasValue)
			{
				IoC.Resolve<ICampaignSecureService>().SetFolder(SignInHelper.SignedInSystemUser, GetId(), id.Value);
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCampaign);
				MasterPage.PopulateTree(id);
				MasterPage.UpdateBreadcrumbs(id);
			}
		}

		public virtual void PopulateTree(int? folderId)
		{
			CampaignsTree.DataSource = GetNodes(folderId);
			CampaignsTree.RootNodeTitle = Resources.Campaign.Literal_ProductCampaigns;
			CampaignsTree.DataBind();
			CampaignsTree.SelectedNodeId = folderId;
		}

		public virtual IEnumerable<INode> GetNodes(int? folderId)
		{
			var service = IoC.Resolve<ICampaignFolderSecureService>();
			return MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
				? service.GetProductCampaignFolderTree(folderId)
				: service.GetCartCampaignFolderTree(folderId);
		}
	}
}