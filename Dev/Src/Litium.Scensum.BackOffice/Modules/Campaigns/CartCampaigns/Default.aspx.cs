﻿using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Campaigns.CartCampaigns
{
	public partial class Default : PageController
	{
		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateForm()
		{
			var topMaster = ((DefaultSubMaster)Master).MasterPage;
			if (!topMaster.IsCampaignTypeSupplyed())
			{
				topMaster.RedirectWithType(PathHelper.Campaign.GetDefaultUrlForCartCampaigns(), CampaignType.CartCampaign);
			}
		}
	}
}
