﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public partial class SetCardinality : StatePageController<CampaignCardinalityState>, IEditor 
	{
		private const int MoveUpStep = -15;
		private const int MoveDownStep = 15;
		private const int DifferenceStep = 10;
		private const int MinStep = 1;

		private Collection<ICampaignFolder> _allFolders;

		protected virtual CampaignsMaster MasterPage
		{
			get
			{
				return (CampaignsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			SetButton.Click += OnSet;
			DeleteCampaignButton.Click += OnDelete;
			CampaignsGrid.RowCommand += OnRowCommand;
			CampaignsGrid.RowDataBound += OnRowDataBound;
		}
		
		protected override void PopulateForm()
		{
			PopulateData();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			MasterPage.UpdateBreadcrumbs(Resources.Campaign.Literal_SetCampaignCardinality);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			MasterPage.RedirectToDefaultPage();
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
            RefreshPriority();
			var campaigns = Sort(State.Campaigns, MinStep);
			var service = IoC.Resolve<ICampaignSecureService>();
			service.SetPriority(SignInHelper.SignedInSystemUser, campaigns);
			SuccessMessager.Add(Resources.CampaignMessage.SetCardinalitySuccess);
		}

		public virtual void OnDelete(object sender, EventArgs e)
		{
			IEnumerable<int> ids = CampaignsGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				ErrorMessager.Add(Resources.CampaignMessage.NoCampaignSelected);
				return;
			}
			DeleteCampaigns(ids);

			SuccessMessager.Add(Resources.GeneralMessage.DeleteSeccessful);

			PopulateData();
		}

		public virtual void OnSet(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(StatusList.SelectedValue) && string.IsNullOrEmpty(TypeList.SelectedValue))
			{
				ErrorMessager.Add(Resources.CampaignMessage.NoSetValue);
				return;
			}
			IEnumerable<int> ids = CampaignsGrid.GetSelectedIds();
            if (ids.Count() <= 0)
			{
				ErrorMessager.Add(Resources.CampaignMessage.NoCampaignSelected);
				return;
			}
			var service = IoC.Resolve<ICampaignSecureService>();
			int statusId;
			if (int.TryParse(StatusList.SelectedValue, out statusId))
			{
				service.SetStatus(SignInHelper.SignedInSystemUser, ids, statusId);
			}
			if (!string.IsNullOrEmpty(TypeList.SelectedValue))
			{
				service.SetType(SignInHelper.SignedInSystemUser, ids,
				                TypeList.SelectedValue == Resources.Campaign.Literal_TypeSelective);
			}

			SuccessMessager.Add(Resources.GeneralMessage.SaveSuccessCampaign);

			PopulateData();
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnRowCommand(object sender, GridViewCommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id)) return;

			switch (e.CommandName)
			{
				case "UpOrdinal":
					Move(id, MoveUpStep);
					break;
				case "DownOrdinal":
					Move(id, MoveDownStep);
					break;
				case "RefreshOrdinal":
					RefreshPriority();
					break;
				case "DeleteCampaign":
					DeleteCampaign(id);
					SuccessMessager.Add(Resources.GeneralMessage.DeleteSeccessful);
					PopulateData();
					break;
				case "NavigateCampaign":
					var campaign = State.Campaigns.FirstOrDefault(c => c.Id == id);
					MasterPage.SelectedFolderId = campaign != null ? (int?)campaign.FolderId : null;
					MasterPage.RedirectWithType(PathHelper.Campaign.GetEditUrl(id));
					break;
				case "NavigatePath":
					MasterPage.SelectedFolderId = id;
					MasterPage.RedirectToDefaultPage();
					break;
			}
		}

		protected void Move(int elementId, int step)
		{
			var campaigns = State.Campaigns;
			var campaign = campaigns.FirstOrDefault(c => c.Id == elementId);
			if (campaign == null) return;
			campaign.Priority += step;
			State.Campaigns = Sort(campaigns, DifferenceStep);
			PopulateGrid();
		}
		
		protected void RefreshPriority()
		{
			if (!ValidatePriorities())
			{
				ErrorMessager.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return;
			}
			var campaigns = State.Campaigns;
			foreach (GridViewRow row in CampaignsGrid.Rows)
			{
				var campaignId = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var campaign = campaigns.FirstOrDefault(c => c.Id == campaignId);
				if (campaign == null) continue;
				campaign.Priority = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
			}
			State.Campaigns = Sort(campaigns, DifferenceStep);
			PopulateGrid();
		}
		
		protected void PopulateData()
		{
			var service = IoC.Resolve<ICampaignSecureService>();
			var campaigns = MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
				? service.GetAllProductCampaigns()
				: service.GetAllCartCampaigns();
			State = new CampaignCardinalityState(Sort(campaigns, DifferenceStep));
			PopulateGrid();
			bool resultExists = AllSelectedDiv.Visible = CampaignsGrid.Rows.Count > 0;
			if (resultExists)
			{
				PopulateStatuses();
				PopulateTypes();
			}
		}

		protected void PopulateGrid()
		{
			CampaignsGrid.DataSource = State.Campaigns;
			CampaignsGrid.DataBind();
		}

		protected virtual void PopulateStatuses()
		{
			StatusList.DataSource = IoC.Resolve<ICampaignStatusSecureService>().GetAll();
			StatusList.DataBind();
			StatusList.Items.Insert(0, new ListItem(Resources.Campaign.Literal_StatusEmpty, string.Empty) { Selected = true });
		}

		protected virtual void PopulateTypes()
		{
			TypeList.Items.Clear();
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeEmpty, string.Empty) { Selected = true });
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeAlways, Resources.Campaign.Literal_TypeAlways));
			TypeList.Items.Add(new ListItem(Resources.Campaign.Literal_TypeSelective, Resources.Campaign.Literal_TypeSelective));
		}
		
		protected bool ValidatePriorities()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}
		
		protected static Collection<ICampaign> Sort(IEnumerable<ICampaign> list, int step)
		{
			var campaigns = new List<ICampaign>(list);
			campaigns.Sort(delegate(ICampaign element1, ICampaign element2)
			               	{
			               		if (element1.Priority < element2.Priority)
			               			return -1;
			               		if (element1.Priority > element2.Priority)
			               			return 1;
			               		return 0;
			               	});
			for (int i = 0; i < list.Count(); i++)
			{
				campaigns[i].Priority = (i + 1) * step;
			}
			return new Collection<ICampaign>(campaigns);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<ICampaignFolderSecureService>();
				_allFolders = MasterPage.CurrentCampaignType == CampaignType.ProductCampaign
								? service.GetAllProductCampaignFolders()
								: service.GetAllCartCampaignFolders();
			}

			var folders = _allFolders;
			var folder = folders.FirstOrDefault(f => f.Id == System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
			if (folder == null) return string.Empty;

			var stack = new Stack<string>();
			stack.Push(folder.Title);
			while (folder.ParentFolderId.HasValue)
			{
				var child = folder;
				folder = folders.FirstOrDefault(f => f.Id == child.ParentFolderId.Value);
				stack.Push(folder.Title);
			}
			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString().TrimEnd(new[] {'\\'});
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}

		protected virtual void DeleteCampaign(int campaignId)
		{
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				IoC.Resolve<IProductCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignId);
			}
			else
			{
				IoC.Resolve<ICartCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignId);
			}
		}

		protected virtual void DeleteCampaigns(IEnumerable<int> campaignIds)
		{
			if (MasterPage.CurrentCampaignType == CampaignType.ProductCampaign)
			{
				IoC.Resolve<IProductCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignIds);
			}
			else
			{
				IoC.Resolve<ICartCampaignSecureService>().Delete(SignInHelper.SignedInSystemUser, campaignIds);
			}
		}
	}

	[Serializable]
	public sealed class CampaignCardinalityState
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ICampaign> Campaigns { get; set; }

		public CampaignCardinalityState(Collection<ICampaign> campaigns)
		{
			Campaigns = campaigns;
		}
	}
}