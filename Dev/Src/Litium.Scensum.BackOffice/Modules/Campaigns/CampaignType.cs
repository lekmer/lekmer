namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
    public enum CampaignType
    {
        Undefined = 0,
        CartCampaign = 1,
        ProductCampaign = 2
    }
}