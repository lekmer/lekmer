﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Campaigns/CampaignsMaster.Master" AutoEventWireup="true" CodeBehind="CampaignEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Campaigns.CampaignEdit" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="CampaignGeneral" Src="~/UserControls/Campaign/CampaignGeneral.ascx" %>
<%@ Register TagPrefix="uc" TagName="ConditionList" Src="~/UserControls/Campaign/ConditionList.ascx" %>
<%@ Register TagPrefix="uc" TagName="ActionList" Src="~/UserControls/Campaign/ActionList.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MessageContainer" runat="server">
		<uc:MessageContainer 
			ID="CampaignValidationMessage"  
			MessageType= "Failure" 
			HideMessagesControlId="SaveButton"  
			runat="server" />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ProductCampaignsPlaceHolder" runat="server">
	<uc:CampaignGeneral ID="CampaignGeneralControl" runat="server" />
	<br class="clear"/>
	<br />
	<div id="conditionToolbar">
		<div class="collapsible-items left">
			<div id="ConditionCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<span><%=Resources.Campaign.Literal_Conditions%></span>
				</div>
				<div id="ConditionCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Literal_Type%></span>
						&nbsp;
						<asp:DropDownList ID="ConditionTypeList" runat="server"></asp:DropDownList>
					</div>
					<div class="link-button">
						<uc:ImageLinkButton ID="OpenConditionPopupButton" runat="server" Text="<%$ Resources:General,Button_Add %>" SkinID="DefaultButton"/>
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ConditionPanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="ConditionIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
	</div>
	<br />
	<div id="ConditionPanelDiv" runat="server" class="left">
		<asp:Panel ID="ConditionPanel" runat="server">
			<uc:ConditionList ID="ConditionListControl" runat="server" />
		</asp:Panel>
	</div>
	<br class="clear"/>
	<br />
	<br />
	<div id="actionToolbar">
		<div class="collapsible-items left">
			<div id="ActionCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<span><%=Resources.Campaign.Literal_Actions%></span>
				</div>
				<div id="ActionCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Literal_Type%></span>
						&nbsp;
						<asp:DropDownList ID="ActionTypeList" runat="server"></asp:DropDownList>
					</div>
					<div class="link-button">
						<uc:ImageLinkButton ID="OpenActionPopupButton" Text="<%$Resources:General, Button_Add%>" SkinID="DefaultButton" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ActionPanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="ActionIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
	</div>
	<br />
	<div id="ActionPanelDiv" runat="server" class="left">
		<asp:Panel ID="ActionPanel" runat="server">
			<uc:ActionList ID="ActionListControl" runat="server" />
		</asp:Panel>
	</div>
	<br class="clear"/>
	<br />

								<br class="clear" />
				<div class="buttons left">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"  SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('campaign');" />
				</div>
				<div class="buttons right">
        <uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
        <uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
    </div>

	<!-- Condition Popup Start -->
	<div id="ConditionPopupDiv" runat="server" class="campaign-popup-container small-campaign-popup-container" style="z-index: 10010; display: none;">
		<asp:Panel ID="ConditionPopupPanel" runat="server" DefaultButton="ConditionPopupOkButton">
		<div class="campaign-popup-header">
			<div class="campaign-popup-header-left">
			</div>
			<div class="campaign-popup-header-center small-campaign-popup-header-center">
				<span><%= Resources.Campaign.Literal_ConfigureCondition%></span>
				<input type="button" id="ConditionPopupCloseButton" runat="server" value="x"/>
			</div>
			<div class="campaign-popup-header-right">
			</div>
		</div>
		<div class="campaign-popup-content small-campaign-popup-content">
			<asp:UpdatePanel ID="ConditionPopupUpdatePanel" UpdateMode="Conditional" runat="server">
				<ContentTemplate>				
					<uc:MessageContainer 
						ID="ConditionPopupValidationMessage"  
						MessageType="Failure"  
						HideMessagesControlId="ConditionPopupOkButton"  
						runat="server" />	
					<asp:PlaceHolder ID="ConditionControlPlaceHolder" runat="server"></asp:PlaceHolder>
				
				</ContentTemplate>
			</asp:UpdatePanel>
			<div class="campaign-popup-buttons">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ConditionPopupOkButton" runat="server" 
							Text="<%$Resources:General, Button_Ok %>" SkinID="DefaultButton" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ConditionPopupCancelButton" runat="server" 
							Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
			</div>
		</div>
		</asp:Panel>
	</div>
	
	<ajaxToolkit:ModalPopupExtender 
		ID="ConditionPopup" 
		runat="server" 
		TargetControlID="MockedConditionPopupTargetButton"
		PopupControlID="ConditionPopupDiv"
		CancelControlID="ConditionPopupCloseButton" 
		BackgroundCssClass="PopupBackground" />

	<div style="display: none;">
		<asp:Button ID="MockedConditionPopupTargetButton" Visible="true" runat="server" />
	</div>
	<asp:HiddenField ID="ConditionControlToInitializeHiddenField" runat="server" />
	<!-- Condition Popup End -->
	
	<!-- Action Popup Start -->
	<div id="ActionPopupDiv" runat="server" class="campaign-popup-container" style="z-index: 10010; display: none;">
		<div id="campaign-popup-header" class="campaign-popup-header">
			<div class="campaign-popup-header-left">
			</div>
			<div class="campaign-popup-header-center">
				<span><%= Resources.Campaign.Literal_ConfigureAction%></span>
				<input type="button" id="ActionPopupCloseButton" runat="server" value="x"/>
			</div>
			<div class="campaign-popup-header-right">
			</div>
		</div>
		<div class="campaign-popup-content-scrollable">
	        <asp:UpdatePanel ID="ActionPopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
					<asp:PlaceHolder ID="ActionControlPlaceHolder" runat="server"></asp:PlaceHolder>
					<br class="clear"/>
					<uc:MessageContainer 
						ID="ActionPopupValidationMessage"  
						MessageType="Warning"
						HideMessagesControlId="ActionPopupOkButton"  
						runat="server" />
				</ContentTemplate>
			</asp:UpdatePanel>
			<br />
			<div class="campaign-popup-buttons">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ActionPopupOkButton" runat="server" 
							Text="<%$Resources:General, Button_Ok %>" SkinID="DefaultButton" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ActionPopupCancelButton" runat="server" 
							Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
			</div>
		</div>
	</div>
	
	<ajaxToolkit:ModalPopupExtender 
		ID="ActionPopup" 
		runat="server" 
		TargetControlID="MockedActionPopupTargetButton"
		PopupControlID="ActionPopupDiv"
		CancelControlID="ActionPopupCloseButton" 
		BackgroundCssClass="PopupBackground" />

	<div style="display: none;">
		<asp:Button ID="MockedActionPopupTargetButton" Visible="true" runat="server" />
	</div>
	<asp:HiddenField ID="ActionControlToInitializeHiddenField" runat="server" />
	<!-- Action Popup End -->
	
	
		
</asp:Content>