﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Themes
{
	public partial class Default : PageController, IEditor
	{
		protected override void SetEventHandlers()
		{
			ThemeGrid.PageIndexChanging += OnPageIndexChanging;
			ThemeGrid.RowCommand += OnRowCommand;
		}

		protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ThemeGrid.PageIndex = e.NewPageIndex;
			PopulateThemes();
		}

		protected void OnRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DeleteTheme")
			{
				IThemeSecureService service = IoC.Resolve<IThemeSecureService>();
				service.Delete(SignInHelper.SignedInSystemUser, System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
				PopulateThemes();
			}
			else if (e.CommandName == "CopyTheme")
			{
				Response.Redirect(PathHelper.Template.Theme.GetCopyUrl(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture)));
			}
		}

		protected override void PopulateForm()
		{
			PopulateThemes();
		}

		public void OnCancel(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		public void OnSave(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		protected virtual void PopulateThemes()
		{
			IThemeSecureService service = IoC.Resolve<IThemeSecureService>();
			Collection<ITheme> themes = service.GetAll();
			ITheme defaultTheme = service.Create();
			defaultTheme.Id = 0;
			defaultTheme.Title = Resources.Interface.Literal_DefaultThemeName;
			themes.Insert(0, defaultTheme);
			ThemeGrid.DataSource = themes;
			ThemeGrid.DataBind();
		}
	}
}