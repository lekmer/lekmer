﻿<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Themes/Theme.Master" CodeBehind="ThemeEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Themes.ThemeEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="Messager" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
				<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValidatorSummary" DisplayMode="List" ValidationGroup="vgTheme"  />
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="ThemeEditContent" ContentPlaceHolderID="ThemeHolder" runat="server">
<script type="text/javascript">
	function ConfirmThemeDelete() {
		return DeleteConfirmation('<%= Resources.Interface.Literal_theme %>');
	}
</script>	
	<div class="theme">
	<asp:Panel ID="ThemeEditPanel" runat="server" DefaultButton="SaveButton">
		<asp:UpdatePanel ID="ThemeEditUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div class="input-box theme-bold">
					<div class="relation-header">
						<uc:LiteralEncoded ID="ThemeTitleLabel" runat="server"></uc:LiteralEncoded>
				    </div>
				    <br />
					<span><%= Resources.General.Literal_Title %>&nbsp; *</span>&nbsp;
					<asp:RequiredFieldValidator runat="server" ID="ThemeTitleValidator" ControlToValidate="ThemeTitleBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgTheme" />
					<br />
					<asp:TextBox ID="ThemeTitleBox" runat="server" MaxLength="50" />						
				</div>
				<br />
				<div class="theme-model">
					<asp:Repeater ID="ModelFolderRepeater" runat="server">
						<ItemTemplate>
							<div id="model-item">
								<div id="model-name">
									<asp:Label ID="ModelFolderTitleLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' />
									<hr />
								</div>
								<div id="model-content-wrapper">
									<asp:Repeater ID="ModelRepeater" runat="server" OnItemDataBound="ModelRepeaterBound">
										<ItemTemplate>
											<div id="model-content">
												<asp:HiddenField ID="ModelIdHidden" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Id")%>' />
												<asp:Label ID="ModelTitleLabel" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' />
												<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
											</div>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>
						</ItemTemplate>
					</asp:Repeater>
				</div>
				
				<br class="clear"/>
					<asp:UpdatePanel id="FooterUpdatePannel" runat="server">
						<ContentTemplate>
							<div id="model-buttons" class="buttons">							
								<div class="right">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General, Button_Delete %>" SkinID="DefaultButton" OnClientClick="return ConfirmThemeDelete();"/>
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgTheme" />
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
								</div>
							</div>
						</ContentTemplate>
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click" />
						</Triggers>
					</asp:UpdatePanel>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click" />
				<asp:AsyncPostBackTrigger ControlID="CancelButton" EventName="Click" />
			</Triggers>
		</asp:UpdatePanel>
	</asp:Panel>
	</div>
</asp:Content>