<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Includes/Include.Master"  CodeBehind="IncludeEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Includes.IncludeEdit" %>
<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="messager"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />					
				<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgInclude"  />
			</ContentTemplate>
			</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="IncludeEditContent" ContentPlaceHolderID="IncludeContent" runat="server">
	<asp:Panel ID="IncludeEditPanel" runat="server" DefaultButton="SaveButton">
	<div class="include-create-edit">
		<div id="create-column-first" class="column">
			<div class="input-box">
				<span><%= Resources.Interface.Literal_SystemName %>&nbsp; *</span>
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="SystemNameValidator" 
					ControlToValidate="SystemNameTextBox" 
					ErrorMessage="<%$ Resources:InterfaceMessage, SystemNameEmpty %>" 
					Display="None" 
					ValidationGroup="vgInclude"  />
				<asp:RegularExpressionValidator runat="server" ID="RegularValidator" ControlToValidate="SystemNameTextBox" 
					ValidationExpression="^[\w\s-\.]+$" ErrorMessage="<%$ Resources:GeneralMessage, SystemNameContainsSpecialSymbols %>" 
					Display="None" ValidationGroup="vgInclude"></asp:RegularExpressionValidator>
				<br />
				<asp:TextBox ID="SystemNameTextBox" runat="server" MaxLength="50" />
			</div>
			<div class="input-box">
				<span><%= Resources.General.Literal_Description %></span>
				<br />
				<asp:TextBox ID="DescriptionTextBox" runat="server" TextMode="MultiLine" Rows="3"/>				
			</div>
			<div class="input-box">
				<span><%= Resources.Interface.Literal_IncludeSourceCode %></span>
				<br />
				<asp:TextBox ID="ValueTextBox" runat="server" TextMode="MultiLine" Rows="15" />
			</div>
			<br />
                    <div class="buttons left">
                        <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('include');" />
                    </div>
                    <div class="buttons right">
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="SaveButton" runat="server" ValidationGroup="vgInclude" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" />					
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
				</div>
		</div>
		<asp:UpdatePanel ID="PutIncludeTreeUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>	
			<div id="create-column-last" class="column">
			<div class="input-box">
				<span><%= Resources.Interface.Literal_PutInto %></span>
				<CustomControls:TemplatedTreeView runat="server" ID="PutIncludeTreeControl"
					NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
					DisplayTextControl="lbTitle"  MainContainerCssClass="treeview-main-container"
					NodeChildContainerCssClass="treeview-node-child" NodeExpandCollapseControlCssClass="tree-icon"
					NodeMainContainerCssClass="treeview-node" NodeParentContainerCssClass="treeview-node-parent" 
					NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png" NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
					MenuCallerElementCssClass="tree-menu-caller" MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
					<HeaderTemplate>
					</HeaderTemplate>        
					<NodeTemplate>
						<div class="tree-item-cell-expand">
							<img src='<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>' alt="" class="tree-icon" />
							<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
						</div>
						<div class="tree-item-cell-main">
							<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img"/> 
							<asp:LinkButton runat="server" ID="lbTitle" CommandName="Navigate"></asp:LinkButton>
						</div>                 
						<br />
					</NodeTemplate>
			  </CustomControls:TemplatedTreeView>
			</div>
		</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	</asp:Panel>
</asp:Content>