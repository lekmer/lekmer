using System;
using System.Globalization;
using System.Linq;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Includes
{
    public partial class FolderEdit : PageController,IEditor 
    {
		protected virtual bool? IsSuccessMessage()
		{
			return Request.QueryString.GetBooleanOrNull("HasMessage");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			Include.RegisterStartupScript(this, GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", PutIncludeTreeControl.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", PutIncludeTreeControl.ClientID), true);
		}
    	
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
    		PutIncludeTreeControl.NodeCommand += OnNodeCommand;
		}
		
		protected override void PopulateForm()
		{
			var includeFolderSecureService = IoC.Resolve<IIncludeFolderSecureService>();

			IIncludeFolder includeFolder = includeFolderSecureService.GetById(((Include)Master).SelectedFolderId.Value);
			TitleTextBox.Text = includeFolder.Title;

			PopulateTreeView(null);

			if (IsSuccessMessage() == null || !((bool) IsSuccessMessage())) return;
			ShowSuccessMessage();
		}
		
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.Include.GetDefaultUrl());
		}
		
		public virtual void OnSave(object sender, EventArgs e)
		{
			var includeFolderSecureService = IoC.Resolve<IIncludeFolderSecureService>();

			IIncludeFolder includeFolder = includeFolderSecureService.GetById((int)((Include)Page.Master).SelectedFolderId);
			if (includeFolder == null)
			{
				throw new BusinessObjectNotExistsException((int)((Include)Page.Master).SelectedFolderId);
			}
			includeFolder.Title = TitleTextBox.Text.Trim();
			if (PutIncludeTreeControl.SelectedNodeId.HasValue)
			{
				includeFolder.ParentIncludeFolderId = PutIncludeTreeControl.SelectedNodeId.Value == PutIncludeTreeControl.RootNodeId ? null : PutIncludeTreeControl.SelectedNodeId;
			}

			var includeFolderService = IoC.Resolve<IIncludeFolderSecureService>();

			try
			{
				includeFolderService.Save(SignInHelper.SignedInSystemUser, includeFolder);
			}
			catch (ArgumentException exception)
			{
                messager.Add(exception.Message);
				return;
			}

			((Include)Page.Master).UpdateMasterTree();
            ShowSuccessMessage();
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

    	protected void PopulateTreeView(int? folderId)
        {
			var master = (Include)Page.Master;
			var folders = master.GetTreeDataSource(folderId ?? master.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;
			var editedFolder = folders.FirstOrDefault(f => f.Id == master.SelectedFolderId);

			PutIncludeTreeControl.DataSource = editedFolder != null
				? PutIncludeTreeControl.GetFilteredSource(folders, editedFolder.Id)
				: folders;
			PutIncludeTreeControl.RootNodeTitle = Resources.Interface.Literal_Includes;
			PutIncludeTreeControl.DataBind();
			PutIncludeTreeControl.SelectedNodeId = folderId ?? (editedFolder != null ? editedFolder.ParentId : PutIncludeTreeControl.RootNodeId) ?? PutIncludeTreeControl.RootNodeId;
        }

		private void ShowSuccessMessage()
		{
			messager.Add(Resources.GeneralMessage.SaveSuccessIncludeFolder);
			messager.MessageType = InfoType.Success;
		}
    }
}
