using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Aliases
{
    public partial class FolderCreate : PageController,IEditor 
    {
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			ParentAliasTree.NodeCommand += OnNodeCommand;
		}
		
		protected override void PopulateForm()
		{
			if (Master.SelectedFolderId == null)
			{
				Master.DenySelection = true;
				PopulateTreeView(null);
			}
			else
			{
				if (Master.SelectedFolderId == Master.RootNodeId)
				{
					Master.DenySelection = true;
					Master.IsMainNode = true;
				}
				ParentAliasTree.Enabled = false;
				PutAliasTreeDiv.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			Master.BreadcrumbAppend.Clear();
			Master.BreadcrumbAppend.Add(Resources.Interface.Literal_CreateFolder);
			if (Master.DenySelection)
			{
				Master.BuildBreadcrumbs();
			}
			Alias.RegisterStartupScript(TreeViewFoldersUpdatePanel, TreeViewFoldersUpdatePanel.GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", ParentAliasTree.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", ParentAliasTree.ClientID), true);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			if (Master.SelectedFolderId == Master.RootNodeId)
			{
				Response.Redirect(PathHelper.Template.Alias.GetDefaultUrl());
			}
			else
			{
				Response.Redirect(PathHelper.Template.Alias.GetDefaultUrlWithShowFolderContent());	
			}
		}
		
		public virtual void OnSave(object sender, EventArgs e)
		{
			var aliasFolderService = IoC.Resolve<IAliasFolderSecureService>();

			IAliasFolder aliasFolder = aliasFolderService.Create();
			aliasFolder.Title = TitleTextBox.Text;

			if (Master.SelectedFolderId == null)
			{
				if (ParentAliasTree.SelectedNodeId.HasValue)
				{
					aliasFolder.ParentAliasFolderId = ParentAliasTree.SelectedNodeId.Value == ParentAliasTree.RootNodeId
															? null
															: ParentAliasTree.SelectedNodeId;
				}
			}
			else
			{
				aliasFolder.ParentAliasFolderId = Master.SelectedFolderId == Master.RootNodeId
															? null
															: Master.SelectedFolderId;
			}

			ValidationResult result = aliasFolderService.Validate(aliasFolder);
			if (!result.IsValid)
			{
				errors.AddRange(result.Errors);
				return;
			}
			int folderId = aliasFolderService.Save(SignInHelper.SignedInSystemUser, aliasFolder);
			Master.SelectedFolderId = folderId;
			Master.UpdateMasterTree();

			Response.Redirect(PathHelper.Template.Alias.Folder.GetEditUrl(true));
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var folders = Master.GetTreeDataSource(folderId);
			if (folders == null || folders.Count <= 0) return;

			ParentAliasTree.DataSource = folders;
			ParentAliasTree.RootNodeTitle = Resources.Interface.Literal_Aliases;
			ParentAliasTree.DataBind();
			ParentAliasTree.SelectedNodeId = folderId ?? ParentAliasTree.RootNodeId;
		}
    }
}
