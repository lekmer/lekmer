<%@ Page Language="C#" MasterPageFile="~/Modules/Interface/Aliases/Alias.Master" CodeBehind="AliasEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Interface.Aliases.AliasEdit" %>
<%@ Import Namespace="Litium.Scensum.Template"%>
<%@ Register TagPrefix="tinymce" Namespace="Moxiecode.TinyMCE.Web" Assembly="Moxiecode.TinyMCE" %>
<%@ Register TagPrefix="CustomControls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="AliasTranslator" Src="~/UserControls/Translation/AliasTranslator.ascx" %>
<%@ Register TagPrefix="IABContros" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<asp:UpdatePanel ID="MessageContainerUpdatePanel" runat="server">
			<ContentTemplate>
					<uc:MessageContainer ID="messager"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />				
						<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgAlias" />	
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
<asp:Content ID="aliasEdition" ContentPlaceHolderID="AliasContent" runat="server">

<script type='text/javascript' language='javascript'>

    function AliasTypeChange(dropdown) {
        if (confirm("<%= Resources.InterfaceMessage.AliasValueConfirmCleare %>")) {
            return true;
        }
        else {
            dropdown.selectedIndex = document.getElementById('<%= SelectedAliasTypeIndexHiddenField.ClientID%>').value;
            return false;
        }
    }
    function submitForm() {
        tinyMCE.triggerSave();
    }
	</script>
	
	<asp:Panel ID="AliasPanel" runat="server" DefaultButton="SaveButton">
	<div class="alias-create-edit">
		<div id="create-column-first" class="column">
			<div class="input-box">
				<span><%= Resources.Interface.Literal_SystemName %>&nbsp; *</span>
				<asp:RequiredFieldValidator 
					runat="server" 
					ID="SystemNameValidator" 
					ControlToValidate="SystemNameTextBox" 
					ErrorMessage="<%$ Resources:InterfaceMessage, SystemNameEmpty %>" 
					Display="None" 
					ValidationGroup="vgAlias" />
				<asp:RegularExpressionValidator runat="server" ID="RegularValidator" ControlToValidate="SystemNameTextBox" 
					ValidationExpression="^[\w\s-\.]+$" ErrorMessage="<%$ Resources:GeneralMessage, SystemNameContainsSpecialSymbols %>" 
					Display="None" ValidationGroup="vgAlias"></asp:RegularExpressionValidator>
				<br />
				<asp:TextBox ID="SystemNameTextBox" runat="server" MaxLength="100"/>			
			</div>
			<div class="input-box">
				<span><%= Resources.General.Literal_Description %></span>
				<br />
				<asp:TextBox ID="AliasDescriptionTextBox" runat="server" TextMode="MultiLine" Rows="3"/>				
			</div>
			<div class="input-box">
				<span><%= Resources.Interface.Literal_ChooseType %></span>
				<br />
				<asp:DropDownList ID="AliasTypeList" DataTextField="Title" onchange='if (!AliasTypeChange(this)){return false;}' AutoPostBack="true" DataValueField="Id" runat="server">
				</asp:DropDownList>	
				<asp:HiddenField ID="SelectedAliasTypeIndexHiddenField" runat="server" />			
			</div>
			
			        <div class="input-box">
				        <span><%= Resources.Interface.Literal_Value %></span>
				        <!-- RA -->
						<uc:AliasTranslator ID="AliasTranslatorControl" runat="server" />
						<!-- RA -->
				        <br />					
						        <asp:TextBox ID="ValueTextBox" runat="server"/>
						        <asp:TextBox ID="AreaValueTextBox" runat="server" Width="100%" TextMode="MultiLine" />
						        <div id="TinyMceDiv" runat="server" >
						        <%--<IABContros:TinyMceEditor  ID="TinyMceValueTextArea" runat="server" SkinID="tinyMCE" Width="200px" />--%>
						        </div>
			        </div>
			<asp:UpdatePanel id="AliasUpdatePanel" runat="server">
				<ContentTemplate>
				<br />
                    <div class="buttons left">
                        <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('alias');" />
                    </div>
                    <div class="buttons right">
                        <uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
                            OnClientClick="submitForm();" SkinID="DefaultButton" ValidationGroup="vgAlias" />
                        <uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
                            SkinID="DefaultButton" />
                    </div>
				</ContentTemplate>
		</asp:UpdatePanel>
				
		</div>
		<asp:UpdatePanel ID="TreeViewFoldersUpdatePanel" runat="server">
			<ContentTemplate>
				<div id="create-column-last" class="column">
					<div class="input-box">
						<span><%= Resources.Interface.Literal_PutInto %></span>
						<CustomControls:TemplatedTreeView runat="server" ID="ParentAliasTree" DisplayTextControl="lbName"
							NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
							MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
							NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
							NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
							NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
							MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
							<HeaderTemplate>
							</HeaderTemplate>
							<NodeTemplate>
								<div class="tree-item-cell-expand">
									<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
									<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
								</div>
								<div class="tree-item-cell-main">
									<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img"/> 
									<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
								</div>
							</NodeTemplate>
						</CustomControls:TemplatedTreeView>
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	</asp:Panel>
</asp:Content>