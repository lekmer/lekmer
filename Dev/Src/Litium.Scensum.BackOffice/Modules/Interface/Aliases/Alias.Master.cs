using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Aliases
{
	public partial class Alias : MasterPage
	{
		private const string _selectedNodeKey = "SelectedNodeId";
		private const string BreadcrumbsSeparator = " > ";

		private Collection<IAliasFolder> _aliasFolders;

		#region Property

		private Collection<string> _breadcrumbAppend;
		
		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				if(_breadcrumbAppend == null)
				{
					_breadcrumbAppend = new Collection<string>();
				}
				return _breadcrumbAppend;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session[_selectedNodeKey];
			}
			set
			{
				Session[_selectedNodeKey] = value;
			}
		}
		
		public virtual bool DenySelection
		{
			get
			{
				return AliasTree.DenySelection;
			}
			set
			{
				AliasTree.DenySelection = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return AliasTree.RootNodeId;
			}
		}
		
		public virtual bool ShowFolderContent { get; set; }

		public virtual bool IsMainNode { get; set; }

		public static void RegisterStartupScript(Control control, Type type, string key, string script, bool addScriptTags)
		{
			ScriptManager.RegisterStartupScript(control, type, key, script, addScriptTags);
		}
		
		public virtual void UpdateMasterTree()
		{
			PopulateTreeView(null);
			LeftUpdatePanel.Update();
		}
		
		public virtual void UpdateSelectedFolder(int selectedFolderId)
		{
			AliasTree.SelectedNodeId = SelectedFolderId = selectedFolderId;
			PopulateTreeView(null);
			LeftUpdatePanel.Update();
		}

		#endregion

		#region Page Events

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			CreateFolderButton.Click += LbtnCreateFolder_ServerClick;
			SearchButton.Click += BtnSearch_Click;
			AliasTree.NodeCommand += OnNodeCommand;
		}
		protected override void OnPreRender(EventArgs e)
		{
			if (!IsPostBack)
			{
				PopulateTreeView(null);
				RestoreSearchFields();
			}

			RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", AliasTree.ClientID), true);

			if (IsMainNode)
			{
				BuildBreadcrumbs();
				return;
			}

			if (DenySelection) return;

			if (SelectedFolderId == null || AliasTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}

			BuildBreadcrumbs();
		}

		public virtual void BuildBreadcrumbs()
		{
			Collection<IAliasFolder> folders = IoC.Resolve<IAliasFolderSecureService>().GetAll();
			IAliasFolder folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
			string breadcrumbs = string.Empty;

			while (folder != null)
			{
				breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", BreadcrumbsSeparator, folder.Title, breadcrumbs);
				int? parentNodeId = folder.ParentAliasFolderId;
				folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, BreadcrumbsSeparator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", Resources.Interface.Literal_Aliases, breadcrumbs);

			Breadcrumbs.Text = breadcrumbs;
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			ShowFolderContent = true;
			PopulateTreeView(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					DenySelection = true;
					break;
				case "Navigate":
					SelectedFolderId = e.Id;
					Response.Redirect(PathHelper.Template.Alias.GetDefaultUrlWithShowFolderContent());
					break;
			}
		}

		protected virtual void LbtnEdit_Click(object sender, EventArgs e)
		{
			SelectedFolderId = AliasTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Template.Alias.Folder.GetEditUrl());
		}

		protected virtual void LbtnCreateFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = AliasTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Template.Alias.Folder.GetCreateUrl());
		}

		protected virtual void LbtnCreateAlias_Click(object sender, EventArgs e)
		{
			SelectedFolderId = AliasTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Template.Alias.GetEditUrl());
		}

		protected virtual void LbtnDelete_Click(object sender, EventArgs e)
		{
			SelectedFolderId = GetAllAliasFolders().FirstOrDefault(a => a.Id == AliasTree.MenuLastClickedNodeId.Value).ParentAliasFolderId;
			var aliasFolderService = IoC.Resolve<IAliasFolderSecureService>();
			aliasFolderService.Delete(SignInHelper.SignedInSystemUser, AliasTree.MenuLastClickedNodeId.Value);
			PopulateTreeView(null);
			if (SelectedFolderId == null)
			{
				Response.Redirect(PathHelper.Template.Alias.GetDefaultUrl());
			}
			else
			{
				Response.Redirect(PathHelper.Template.Alias.GetDefaultUrlWithShowFolderContent());
			}
		}

		protected virtual void LbtnCreateFolder_ServerClick(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			Response.Redirect(PathHelper.Template.Alias.Folder.GetCreateUrl());
		}

		protected virtual void BtnSearch_Click(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			SearchCriteriaState<string>.Instance.Save(PathHelper.Template.Alias.GetSearchResultUrl(), SearchTextBox.Text);
			Response.Redirect(PathHelper.Template.Alias.GetSearchResultUrl());
		}

		#endregion

		#region Methods

		protected void PopulateTreeView(int? folderId)
		{
			AliasTree.DataSource = GetTreeDataSource(folderId);
			AliasTree.RootNodeTitle=Resources.Interface.Literal_Aliases;
			AliasTree.DataBind();
			AliasTree.SelectedNodeId = folderId ?? SelectedFolderId ?? AliasTree.RootNodeId;
		}

		public virtual Collection<INode> GetTreeDataSource(int? folderId)
		{
			var id = folderId ?? SelectedFolderId;
			id = id != AliasTree.RootNodeId ? id : null;
			return IoC.Resolve<IAliasFolderSecureService>().GetTree(id);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IAliasFolder> GetAllAliasFolders()
		{
			return _aliasFolders ??
				   (_aliasFolders = (IoC.Resolve<IAliasFolderSecureService>()).GetAll());
		}

		protected void RestoreSearchFields()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Template.Alias.GetSearchResultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		#endregion
	}
}
