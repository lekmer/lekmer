using System;

namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
	[Serializable]
	public class TemplateState
	{
		public int Id { get; set; }
        public int? CopyOfId { get; set; }

	    public int? IdOrCopyOfId
	    {
            get { return Id > 0 ? Id : CopyOfId; }
	    }

		public int? ActiveModelFragmentId { get; set; }
		public bool IsSearchedTemplate { get; set; }
	}
}
