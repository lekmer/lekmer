using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
    public partial class Template : System.Web.UI.MasterPage
	{
    	private const string _modelFolderIdKey = "ModelFolderId";
		private const string _modelIdKey = "ModelId";
		private const string _defaultTemplateKey = "DefaultTemplateId";

		public virtual int? DefaultTemplateId
		{
			get
			{
				return (int?)Session[_defaultTemplateKey];
			}
			private set
			{
				Session[_defaultTemplateKey] = value;
			}
		}
		public virtual int? ModelFolderId
		{
			get
			{
				return (int?)Session[_modelFolderIdKey];
			}
			private set
			{
				Session[_modelFolderIdKey] = value;
			}
		}
		public virtual int? ModelId
		{
			get
			{
				return (int?)Session[_modelIdKey];
			}
			private set
			{
				Session[_modelIdKey] = value;
			}
		}
    	public virtual TemplatedTreeView ModelTree
    	{
			get { return ModelTreeViewControl; }
    	}

        public virtual string PageCaption { get; set; }

    	protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
			ModelTreeViewControl.NodeCommand += OnNodeCommand;
			ModelTreeViewControl.NodeDataBound += ModelTree_NodeDataBound;
    		SearchButton.Click += BtnSearch_Click;
        }

		protected void ModelTree_NodeDataBound(object sender, TemplatedTreeViewNodeEventArgs e)
		{
			var image = (Image)e.Node.FindControl("ModelIcon");
			image.ImageUrl = e.NodeId < 0
             	? ResolveUrl("~/Media/Images/Tree/folder.png")
				: ResolveUrl("~/Media/Images/Interface/folder_brick.png");
		}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
			if(!Page.IsPostBack)
			{
				RestoreSearch();
			}

            //Breadcrumbs
            IModelSecureService modelSecureService = IoC.Resolve<IModelSecureService>();

            IModel currentModel = modelSecureService.GetById(ModelId ?? 0);
            if (currentModel != null)
            {
                IModelFolderSecureService modelFolderSecureService = IoC.Resolve<IModelFolderSecureService>();
                IModelFolder modelFolder = modelFolderSecureService.GetById(currentModel.ModelFolderId);
                if (modelFolder != null)
                {
					BreadcrumbsLiteral.Text = string.Format(CultureInfo.CurrentCulture, "{0} > {1}", modelFolder.Title, currentModel.Title);
					if (!string.IsNullOrEmpty(PageCaption))
					{
						BreadcrumbsLiteral.Text += " > ";
					}
				}
            }

			if (!string.IsNullOrEmpty(PageCaption))
			{
				BreadcrumbsLiteral.Text += PageCaption;
			}

			if (ModelFolderId == null)
			{
				DeniedSelection(true);
			}
        }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e); 
			if (!Page.IsPostBack)
			{
				PopulateTreeView();
			}
        }

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			if (e.ParentId == null)
			{
				ModelFolderId = e.Id;
				PopulateTreeView();
				ModelTreeViewControl.SelectedNodeId = ModelFolderId;
				DeniedSelection(true);
				return;
			}

			ModelId = e.Id;
			ModelFolderId = e.ParentId;
			ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
			ITemplate template = templateSecureService.GetDefaultByModel(e.Id);
			DefaultTemplateId = template == null ? (int?)null : template.Id;

			Response.Redirect(PathHelper.Template.GetDefaultUrl());
		}

		protected void PopulateTreeView()
		{
			var source = GetTreeDataSource(ModelFolderId);
			ModelTreeViewControl.DataSource = source;
			ModelTreeViewControl.DataBind();

			if (source.FirstOrDefault(n => n.Id == ModelId) != null)
			{
				ModelTreeViewControl.SelectedNodeId = ModelId;
			}
			else
			{
				ModelTreeViewControl.SelectedNodeId = ModelFolderId;
				ModelTreeViewControl.DenySelection = true;
			}
		}

		public virtual Collection<INode> GetTreeDataSource(int? itemId)
		{
			var id = itemId < 0 ? -1 * itemId : itemId;
			return IoC.Resolve<IModelSecureService>().GetTree(id);
		}

		protected virtual void BtnSearch_Click(object sender, EventArgs e)
		{
			ModelFolderId = null;
			SearchCriteriaState<string>.Instance.Save(PathHelper.Template.GetTemplateSearchResultUrl(), SearchTextBox.Text);
			Response.Redirect(PathHelper.Template.GetTemplateSearchResultUrl());
		}

		protected void RestoreSearch()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Template.GetTemplateSearchResultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		public virtual void DeniedSelection(bool denySelection)
		{
			ModelTreeViewControl.DenySelection = denySelection;
		}
    }
}
