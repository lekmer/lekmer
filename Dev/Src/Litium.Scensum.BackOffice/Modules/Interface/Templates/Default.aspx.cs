using System;
using System.Globalization;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Interface.Templates
{
	public partial class Default : PageController
	{
		protected virtual int ParentModelId
		{
			get
			{
				return ((Template)Master).ModelId ?? 0;
			}
		}

		private int? TemplateDefaultId
		{
			get
			{
				return ((Template)Master).DefaultTemplateId;
			}
		}

		protected bool IsTemplateDefaultId(int templateId)
		{
			return TemplateDefaultId.HasValue ? TemplateDefaultId == templateId : false;
		}

		protected static bool IsTemplateWithContentPages(int templateId)
		{
			IContentPageSecureService contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			return contentPageSecureService.GetAllByTemplate(templateId).Count > 0;
		}

		protected bool CanDeleteTemplate(int templateId)
		{
			return !IsTemplateDefaultId(templateId) && !IsTemplateWithContentPages(templateId);
		}

		protected virtual bool? IsSearchResult()
		{
			return Request.QueryString.GetBooleanOrNull("IsSearchResult");
		}

		protected override void SetEventHandlers()
		{
			CreateTemplateButton.Click += BtnCreateTemplateClick;
			TemplateGrid.RowDataBound += GVTemplatesRowDataBound;
			TemplateGrid.RowCommand += GVTemplatesRowCommand;
			TemplateGrid.PageIndexChanging += GVTemplatesPageIndexChanging;

		}

		protected override void PopulateForm() { }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			/* ScriptManager.RegisterStartupScript(this, GetType(), "hide context menu", "HideContextMenu();", true);*/
			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				Search();
				CreateTemplateButton.Visible = false;
				return;
			}

			if (ParentModelId == 0)
			{
				CreateTemplateButton.Visible = false;
				return;
			}

			PopulateGrid();
			if (IsPostBack) return;

			int pageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
			if (pageIndex == 0) return;

			TemplateGrid.DataBind();
			TemplateGrid.PageIndex = pageIndex >= TemplateGrid.PageCount ? TemplateGrid.PageCount - 1 : pageIndex;
		}

		protected virtual void BtnCreateTemplateClick(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.GetCreateUrl(ParentModelId));
		}

		protected virtual void GVTemplatesRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			HiddenField hiddenId = (HiddenField)e.Row.FindControl("hfId");
			HyperLink hlEdit = (HyperLink)e.Row.FindControl("hlEdit");

			if (IsSearchResult().HasValue && IsSearchResult().Value)
			{
				hlEdit.NavigateUrl = PathHelper.Template.GetEditUrlForSearch(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
			}
			else
			{
				hlEdit.NavigateUrl = PathHelper.Template.GetEditUrlForTemplates(int.Parse(hiddenId.Value, CultureInfo.CurrentCulture));
			}

			int templateId;
			if (!int.TryParse(hiddenId.Value, out templateId)) return;

			ImageButton ibDelete = (ImageButton)e.Row.FindControl("DeleteButton");
			LinkButton templateMenuDeleteButton = (LinkButton)e.Row.FindControl("TemplateMenuDeleteButton");
			Image templateMenuDeleteImage = (Image)e.Row.FindControl("TemplateMenuDeleteImage");
			if (ibDelete == null) return;

			if (TemplateDefaultId == templateId)
			{
				DisableButton(ibDelete);
				ibDelete.ToolTip = Resources.InterfaceMessage.NotDeleteDefaultTemplate;

				DisableButton(templateMenuDeleteImage);
				templateMenuDeleteImage.ToolTip = Resources.InterfaceMessage.NotDeleteDefaultTemplate;
				templateMenuDeleteButton.Enabled = false;
				
				return;
			}

			IContentPageSecureService contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			if (contentPageSecureService.GetAllByTemplate(templateId).Count > 0)
			{
				DisableButton(ibDelete);
				ibDelete.ToolTip = Resources.InterfaceMessage.NotDeleteTemplateWithContentPages;

				DisableButton(templateMenuDeleteImage);
				templateMenuDeleteImage.ToolTip = Resources.InterfaceMessage.NotDeleteTemplateWithContentPages;
				templateMenuDeleteButton.Enabled = false;
				return;
			}

			IContentAreaSecureService contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
			ibDelete.Attributes.Add("onclick", contentAreaSecureService.GetAllByTemplate(templateId).Count > 0
													? @"javascript: return DeleteTemplateWithContentAreasConfirmation();"
													: @"javascript: return DeleteConfirmation('template');");
			templateMenuDeleteButton.Attributes.Add("onclick", contentAreaSecureService.GetAllByTemplate(templateId).Count > 0
													? @"javascript: return DeleteTemplateWithContentAreasConfirmation();"
													: @"javascript: return DeleteConfirmation('template');");
		}

		protected static void DisableButton(Image btn)
		{
			btn.Enabled = false;
			btn.ImageUrl = "~/Media/Images/Common/delete-disabled.gif";
		}

		protected virtual void GVTemplatesRowCommand(object sender, GridViewCommandEventArgs e)
		{
			//if (e.CommandName != "TemplateDelete") return;
			int templateId;

			if (!int.TryParse(e.CommandArgument.ToString(), out templateId))
			{
				templateId = 0;
			}
			switch (e.CommandName)
			{
				case "TemplateCopy":
					TemplateCopy(templateId);
					break;
				case "TemplateDelete":
					DeleteTemplate(templateId);
					if (TemplateGrid.Rows.Count == 1 && TemplateGrid.PageIndex == TemplateGrid.PageCount - 1 && TemplateGrid.PageIndex != 0)
					{
						TemplateGrid.PageIndex -= 1;
					}
					else
					{
						PopulateGrid();
					}
					break;
			}

		}

		private void TemplateCopy(int templateId)
		{
			Response.Redirect(PathHelper.Template.GetCreateCopyOfUrl(templateId));
		}

		protected virtual void GVTemplatesPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			TemplateGrid.PageIndex = e.NewPageIndex;
		}

		protected void PopulateGrid()
		{
			TemplateGrid.DataSourceID = "TemplateObjectDataSource";
			TemplateGrid.Columns[1].Visible = false;
			TemplateObjectDataSource.SelectParameters.Clear();
			TemplateObjectDataSource.SelectParameters.Add("maximumRows", TemplateGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			TemplateObjectDataSource.SelectParameters.Add("startRowIndex", (TemplateGrid.PageIndex * TemplateGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			TemplateObjectDataSource.SelectParameters.Add("id", ParentModelId.ToString(CultureInfo.CurrentCulture));
		}

		protected void Search()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Template.GetTemplateSearchResultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				TemplateGrid.DataSource = IoC.Resolve<ITemplateSecureService>().Search(searchName);
			}
			TemplateGrid.DataSourceID = string.Empty;
			TemplateGrid.DataBind();
		}

		protected void DeleteTemplate(int id)
		{
			if (id == TemplateDefaultId)
			{
				errors.Add(Resources.InterfaceMessage.NotDeleteDefaultTemplate);
				return;
			}

			if (CheckUsageByBlocks(id)) return;
            
			//TODO: move content area deleting inside template secure service delete transaction
			IContentAreaSecureService contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
			foreach (IContentArea contentArea in contentAreaSecureService.GetAllByTemplate(id))
			{
				contentAreaSecureService.Delete(SignInHelper.SignedInSystemUser, contentArea);
			}

			ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
			templateSecureService.Delete(SignInHelper.SignedInSystemUser, id);
		}

		protected bool CheckUsageByBlocks(int id)
		{
			var relatedBlocks = IoC.Resolve<IBlockSecureService>().GetAllByTemplate(id);
			if (relatedBlocks.Count > 0)
			{
				var titles = new StringBuilder(string.Empty);
				foreach (var block in relatedBlocks)
				{
					var settings = new SiteStructureBlockSetting(block.BlockType.CommonName);
					var link = PathHelper.SiteStructure.Page.GetBlockEditUrl(settings.EditPageName, block.ContentNodeId, block.Id);
					titles.Append("<a style=\"color:#004CD5; text-decoration:underline;\" href=\"" + ResolveUrl(link) + "\">" + block.Title + "</a>");
					titles.Append(", ");
				}
				errors.Add(string.Format(CultureInfo.CurrentCulture,
					Resources.InterfaceMessage.NotDeleteRelatedBlocks,
					titles.ToString().Substring(0, titles.Length - 2)), false);
				return true;
			}
			return false;
		}
        
		protected static string GetPath(int modelId)
		{
			IModelSecureService modelSecureService = IoC.Resolve<IModelSecureService>();

			IModel currentModel = modelSecureService.GetById(modelId);
			if (currentModel == null)
			{
				return string.Empty;
			}

			IModelFolderSecureService modelFolderSecureService = IoC.Resolve<IModelFolderSecureService>();
			IModelFolder modelFolder = modelFolderSecureService.GetById(currentModel.ModelFolderId);
			return modelFolder != null ? string.Format(CultureInfo.CurrentCulture, "{0}/{1}", modelFolder.Title, currentModel.Title) : string.Empty;
		}

	}
}
