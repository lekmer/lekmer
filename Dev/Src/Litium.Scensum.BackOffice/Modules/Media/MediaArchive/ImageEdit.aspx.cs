using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;
using MediaBackOfficeSetting = Litium.Scensum.BackOffice.Setting.MediaSetting;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class ImageEdit : PageController,IEditor 
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			Master.BreadcrumbAppend.Clear();
			Master.BreadcrumbAppend.Add(IoC.Resolve<IMediaItemSecureService>().GetById(GetId()).Title);
			DeleteButton.Attributes.Add("onclick", "return DeleteConfirmation(\"" + Resources.Media.Literal_mediaItem + "\");");
		}
	
		protected override void  SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			DeleteButton.Click += OnDelete;
			NodeSelectorControl.NodeCommand += OnNodeSelectorCommand;
		}

		protected virtual void OnNodeSelectorCommand(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			NodeSelectorControl.DataSource = GetModelFolders(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			NodeSelectorControl.DataBind();
		}

		protected override void  PopulateForm()
		{
			PopulateImageInfo();
			PopulateTree();
		}

		protected void PopulateImageInfo()
		{
			var media = IoC.Resolve<IMediaItemSecureService>().GetById(GetId());
			ImageTitleTextBox.Text = ZoomImageAnchor.Title = media.Title;
			FileNameLabel.Text = media.FileName;
			
			var mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetById(media.MediaFormatId);
			ImageTypeLabel.Text = mediaFormat.Title;
			MediaFileSize.Text = GetFileSize(media.Id, mediaFormat.Extension);
			MediaItemImage.Src = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(media.Id, mediaFormat.Extension));
			var fileUrl = ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(media.Id, mediaFormat.Extension));
			ZoomImageAnchor.HRef = fileUrl;

			var image = IoC.Resolve<IImageSecureService>().GetById(GetId());
			if (image != null)
			{
				ImageSizeLabel.Text = string.Format(CultureInfo.CurrentCulture, "{0}x{1} pixels", image.Width, image.Height);
				AlternateTitleTextBox.Text = image.AlternativeText;
				ImageDiv.Attributes.Add("class", "image-show-lightbox");
				DownloadDocumentDiv.Visible = false;
				ZoomImage.Src = "~/Media/Images/Common/zoom.png";
			}
			else
			{
				DocumentLink.NavigateUrl = fileUrl;
				ImageAlternateTextDiv.Visible = ImageSizeDiv.Visible = false;
				ZoomImage.Src = "~/Media/Images/Media/download.png";
			}
		}

		protected void PopulateTree()
		{
			var media = IoC.Resolve<IMediaItemSecureService>().GetById(GetId());
            NodeSelectorControl.SelectedNodeId = media.MediaFolderId;
			NodeSelectorControl.DataSource = GetModelFolders(media.MediaFolderId);
			NodeSelectorControl.DataBind();
			NodeSelectorControl.PopulatePath(media.MediaFolderId);
		}

		protected static IEnumerable<INode> GetModelFolders(int? folderId)
		{
			return IoC.Resolve<IMediaFolderSecureService>().GetTree(folderId);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var media = IoC.Resolve<IMediaItemSecureService>().GetById(GetId());
			RedirectBack(media.MediaFolderId);
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;

			var imageService = IoC.Resolve<IImageSecureService>();
			var image = imageService.GetById(GetId());
			if (image == null)
			{
				var mediaService = IoC.Resolve<IMediaItemSecureService>();
				var media = mediaService.GetById(GetId());
				if (media == null) throw new BusinessObjectNotExistsException(GetId());
				
				media.Title = ImageTitleTextBox.Text;
				media.MediaFolderId = NodeSelectorControl.SelectedNodeId ?? media.MediaFolderId;
				mediaService.Update(SignInHelper.SignedInSystemUser, media);
			}
			else
			{
				image.Title = ImageTitleTextBox.Text;
				image.AlternativeText = AlternateTitleTextBox.Text;
				image.MediaFolderId = NodeSelectorControl.SelectedNodeId ?? image.MediaFolderId;
				imageService.Update(SignInHelper.SignedInSystemUser, image);
			}
			
			messager.Add(Resources.GeneralMessage.SaveSuccessImage);
			Master.DenySelection = false;
			Master.PopulateTreeView(NodeSelectorControl.SelectedNodeId);
			Master.BreadcrumbAppend.Clear();
			Master.BreadcrumbAppend.Add(ImageTitleTextBox.Text);
			Master.UpdateBreadCrump(NodeSelectorControl.SelectedNodeId.HasValue ? NodeSelectorControl.SelectedNodeId.Value : 0);
		}

		protected void OnDelete(object sender, EventArgs e)
		{
			var mediaService = IoC.Resolve<IMediaItemSecureService>();
			var media = mediaService.GetById(GetId());
			mediaService.Delete(SignInHelper.SignedInSystemUser, media);
			RedirectBack(media.MediaFolderId);
		}

		protected void RedirectBack(int selectFolderId)
		{
			var referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Media.GetEditItemReferrerUrl(referrer, selectFolderId));
			}
			else
			{
				Response.Redirect(PathHelper.Media.GetDefaultUrl());
			}
		}

		protected virtual string GetFileSize(int mediaId, string extension)
		{
			var mediaFilePath = IoC.Resolve<IMediaItemSharedService>().GetMediaItemPath(mediaId, extension);
			var fileInfo = new FileInfo(mediaFilePath);
			var size = fileInfo.Exists ? fileInfo.Length : 0;

			const int byteConversion = 1024;
			var bytes = System.Convert.ToDouble(size);
			if (bytes >= Math.Pow(byteConversion, 3))
			{
				return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 3), 2), " GB");
			}
			if (bytes >= Math.Pow(byteConversion, 2))
			{
				return string.Concat(Math.Round(bytes / Math.Pow(byteConversion, 2), 2), " MB");
			}
			if (bytes >= byteConversion)
			{
				return string.Concat(Math.Round(bytes / byteConversion, 2), " KB");
			}
			return string.Concat(bytes, " Bytes");
		}
	}
}
