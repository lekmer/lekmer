using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class Upload : PageController 
	{
		protected override void SetEventHandlers()
		{
			UploadButton.Click += OnSave;
			NodeSelectorControl.SelectedFolderChangedEvent += NodeSelectorControlSelectedFolderChangedEvent;
			NodeSelectorControl.NodeCommand += OnNodeSelectorCommand;
		}

		protected override void PopulateForm()
		{
			PopulateTree();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Media)Master).BreadcrumbAppend.Clear();
			((Media)Master).BreadcrumbAppend.Add(Resources.Media.Literal_UploadMediaFiles);
		}
		
		public void OnSave(object sender, EventArgs e)
		{
			int? folderId = NodeSelectorControl.SelectedNodeId;
			HttpPostedFile file = FileUploadControl.PostedFile;
			if (file.ContentLength == 0)
			{
				messager.Add(Resources.MediaMessage.FileIsEmpty);
				return;
			}
			if (!folderId.HasValue)
			{
				messager.Add(Resources.MediaMessage.MediaFolderIsNotSelected);
				return;
			}

			int maxFileSize = HttpRuntimeSetting.MaxRequestLength;
			if (maxFileSize <= 0)
			{
				messager.Add(Resources.MediaMessage.NoConfirmMaxFileSize);
				return;
			}

			if (FileUploadControl.PostedFile.ContentLength / 1024 > maxFileSize)
			{
				messager.Add(Resources.MediaMessage.LargeZipFile);
				return;
			}

			var service = IoC.Resolve<IMediaItemSecureService>();
			try
			{
				if (ZipCheckBox.Checked)
				{
					if (Path.GetExtension(file.FileName) != ".zip")
					{
						throw new UploadException(UploadExceptionReason.IncorrectFormat);
					}
					service.UnzipAndInsert((SignInHelper.SignedInSystemUser), file.InputStream, folderId.Value);
				}
				else
				{
					var extension = Path.GetExtension(file.FileName).TrimStart(new[] {'.'});
					if (string.IsNullOrEmpty(extension))
					{
						throw new UploadException(UploadExceptionReason.IncorrectFormat);
					}
					IMediaFormat mediaFormat = IoC.Resolve<IMediaFormatSecureService>().GetByExtension(extension);
					if (mediaFormat == null)
					{
						throw new UploadException(UploadExceptionReason.IncorrectFormat);
					}
					service.Insert(SignInHelper.SignedInSystemUser, file.InputStream, folderId.Value, file.FileName);
				}
				file.InputStream.Close();

				messager.MessageType = InfoType.Success;
				messager.Add(Resources.GeneralMessage.SaveSuccessImages);
			}
			catch (UploadException ex)
			{
				if (ex.Reason == UploadExceptionReason.IncorrectFormat)
				{
					messager.Add(string.Format(CultureInfo.CurrentCulture, Resources.MediaMessage.IncorrectFileFormat));
				}
				else
				{
					messager.Add(string.Format(CultureInfo.CurrentCulture, "{0} {1} {2}", Resources.MediaMessage.UploadFailed, ex.Message, ex.InnerException.Message));
				}
			}
		}

		protected virtual void NodeSelectorControlSelectedFolderChangedEvent(object sender, SelectedFolderChangedEventArgs e)
		{
			if (!e.SelectedFolderId.HasValue)
			{
				return;
			}
			var media = (Media)Master;
			media.DenySelection = false;
			media.SelectedFolderId = e.SelectedFolderId;
			media.PopulateTreeView(e.SelectedFolderId);
		}

		protected virtual void OnNodeSelectorCommand(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			NodeSelectorControl.DataSource = GetFolders(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			NodeSelectorControl.DataBind();
		}

		protected void PopulateTree()
		{
			var id = GetIdOrNull();
			var source = GetFolders(GetIdOrNull());
			NodeSelectorControl.SelectedNodeId = id;
			NodeSelectorControl.DataSource = source;
			NodeSelectorControl.DataBind();
			if (id.HasValue)
				NodeSelectorControl.PopulatePath(id.Value);
		}

		protected virtual IEnumerable<INode> GetFolders(int? folderId)
		{
			return IoC.Resolve<IMediaFolderSecureService>().GetTree(folderId);
		}
	}
}
