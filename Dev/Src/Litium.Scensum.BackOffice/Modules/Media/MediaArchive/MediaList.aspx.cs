using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class MediaList : PageController
	{
		private const string DisplayModeKey = "currentmode";

		protected override void SetEventHandlers()
		{
			MediaListControl.Delete += MediaListControlDelete;
			MediaListControl.Navigate += MediaListControlNavigate;
			MediaListControl.DisplayModeChanged += MediaListControlDisplayModeChanged;
		}

		protected override void PopulateForm()
		{
			if (!IsPostBack && Session[DisplayModeKey] != null)
			{
				MediaListControl.DisplayMode = (DisplayMode)System.Convert.ToInt32(Session[DisplayModeKey], CultureInfo.CurrentCulture);
			}
		}

		
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			int? id = GetIdOrNull();
			if (id.HasValue)
			{
				PopulateMediaItems((int) id);
			}
			else
			{
				Response.Redirect(PathHelper.Media.GetDefaultUrl());
			}
		}
		protected void PopulateMediaItems(int id)
		{
			MediaListControl.FolderId = id;
		}

		protected virtual void MediaListControlDelete(object sender, MediaNavigationEventArgs e)
		{
			var mediaService = IoC.Resolve<IMediaItemSecureService>();
			var media = mediaService.GetById(e.MediaId);
			mediaService.Delete(SignInHelper.SignedInSystemUser, media);
			MediaListControl.DataBind();
		}

		protected virtual void MediaListControlNavigate(object sender, MediaNavigationEventArgs e)
		{
			if (e.IsFolder)
			{
				((Media)Master).SelectedFolderId = e.MediaId;
				Response.Redirect(string.Format(CultureInfo.CurrentCulture, PathHelper.Media.GetDefaultUrl()));
			}

			Session[DisplayModeKey] = System.Convert.ToInt32(MediaListControl.DisplayMode, CultureInfo.CurrentCulture);
			Response.Redirect(PathHelper.Media.GetEditItemUrlForMediaList(e.MediaId));

		}

		protected virtual void MediaListControlDisplayModeChanged(object sender, EventArgs e)
		{
			Session[DisplayModeKey] = System.Convert.ToInt32(MediaListControl.DisplayMode, CultureInfo.CurrentCulture);
		}
	}
}