using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Media;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class Media : MasterPage
	{
		private const string MediaFolderIdKey = "MediaFolderId";
		private const string BreadcrumbsSeparator = " > ";

		private Collection<string> _breadcrumbAppend;
		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				if(_breadcrumbAppend == null)
				{
					_breadcrumbAppend = new Collection<string>();
				}
				return _breadcrumbAppend;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session[MediaFolderIdKey];
			}
			protected internal set
			{
				Session[MediaFolderIdKey] = value;
			}
		}
		public virtual bool DenySelection
		{
			get
			{
				return MediaTree.DenySelection;
			}
			set
			{
				MediaTree.DenySelection = value;
			}
		}
        public virtual bool IsMainNode { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual TemplatedTreeView GetMediaTree()
		{
			return MediaTree;
		}

		public virtual bool ClearBreadcrumb { get; set; }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			CreateFolderButton.Click += CreateFolderButtonClick;
			UploadButton.Click += UploadButtonClick;
			MediaTree.NodeCommand += OnNodeCommand;
		}

		protected virtual void UploadButtonClick(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			Response.Redirect(PathHelper.Media.GetUploadUrl());
		}

		protected virtual void CreateFolderButtonClick(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			Response.Redirect(PathHelper.Media.GetCreateUrl());
		}
		
		protected override void OnPreRender(EventArgs e)
		{
			base.OnLoad(e);
			ScriptManager.RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", MediaTree.ClientID), string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}');HideRootExpander('{0}');", MediaTree.ClientID), true);
			if (!Page.IsPostBack)
			{
				PopulateTreeView(SelectedFolderId);
				UpdateBreadCrump();
			}

            if (IsMainNode)
			{
				UpdateBreadCrump();
			}

			if (!SelectedFolderId.HasValue || MediaTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}
		}
		
		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					DenySelection = true;
					break;
				case "Navigate":
					SelectedFolderId = e.Id;
					Response.Redirect(PathHelper.Media.GetMediaListUrl(e.Id));
					break;
			}
		}

		public virtual void PopulateTreeView(int? itemId)
		{
			IEnumerable<INode> dataSources = GetMediaFolders(itemId);
			MediaTree.DataSource = dataSources;
			MediaTree.RootNodeTitle = Resources.Media.Literal_Media;
			MediaTree.DataBind();
			MediaTree.SelectedNodeId = itemId ?? MediaTree.RootNodeId;
			LeftUpdatePanel.Update();
		}

		public virtual Collection<INode> GetMediaFolders(int? itemId)
		{
			return IoC.Resolve<IMediaFolderSecureService>().GetTree(itemId);
		}

		protected virtual void LbtnCreateFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = MediaTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Media.GetCreateUrl());
		}

		protected virtual void LbtnEditFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = MediaTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Media.GetEditUrl());
		}

		protected virtual void LbtnDeleteFolder_Click(object sender, EventArgs e)
		{
			DeleteFolder((int)MediaTree.MenuLastClickedNodeId);
		}

		public virtual void DeleteFolder(int folderId)
		{
			SelectedFolderId = GetMediaFolders(folderId).FirstOrDefault(inc => inc.Id == folderId).ParentId;
			var mediaFolderSecureService = IoC.Resolve<IMediaFolderSecureService>();
			mediaFolderSecureService.Delete(SignInHelper.SignedInSystemUser, folderId);
			PopulateTreeView(SelectedFolderId);
			Response.Redirect(PathHelper.Media.GetMediaListUrl(SelectedFolderId));
		}

		protected virtual void LbtnUploadFile_Click(object sender, EventArgs e)
		{
			SelectedFolderId = MediaTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Media.GetUploadUrl(MediaTree.MenuLastClickedNodeId.Value));
		}
		
		public static void RegisterStartupScript(Control control, Type type, string key, string script, bool addScriptTags)
		{
			ScriptManager.RegisterStartupScript(control, type, key, script, addScriptTags);
		}
		
		public virtual void DeniedSelection(bool denySelection)
		{
			MediaTree.DenySelection = denySelection;
		}
		
		public virtual void UpdateBreadCrump(int selectFolderId)
		{
			SelectedFolderId = selectFolderId;
			UpdateBreadCrump();
		}

		protected void UpdateBreadCrump()
		{
			if (ClearBreadcrumb)
			{
				BreadcrumbsLiteral.Text = string.Empty;
				return;
			}
			
			string breadcrumbs = string.Empty;
			var folders = GetMediaFolders(SelectedFolderId);
			var folder = folders.FirstOrDefault(n => n.Id == SelectedFolderId);

			while (folder != null)
			{
				breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", BreadcrumbsSeparator, folder.Title, breadcrumbs);
				int? parentNodeId = folder.ParentId;
				folder = folders.FirstOrDefault(item => item.Id == parentNodeId);
			}
			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, BreadcrumbsSeparator, crumb);
					}
				}
			}
			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", MediaTree.RootNodeTitle, breadcrumbs);
			BreadcrumbsLiteral.Text = breadcrumbs;
		}
	}
}
