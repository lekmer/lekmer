using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Media.MediaArchive
{
	public partial class Default : PageController
	{
		protected override void SetEventHandlers(){}

		protected override void PopulateForm()
		{
			var master = (Media) Master;
			master.DeniedSelection(true);
			master.ClearBreadcrumb = true;
		}
	}
}
