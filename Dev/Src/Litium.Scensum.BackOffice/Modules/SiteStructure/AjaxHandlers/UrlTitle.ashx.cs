using System;

using System.Text.RegularExpressions;
using System.Web;
using System.Text;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.AjaxHandlers
{
    /// <summary>
    /// Summary description for $codebehindclassname$
    /// </summary>
    public class UrlTitle : IHttpHandler
    {
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1308:NormalizeStringsToUppercase")]
		public void ProcessRequest(HttpContext context)
        {
            string title = context.Request.Params["title"];
            string urlTitle = context.Request.Params["urlTitle"];

            if (string.IsNullOrEmpty(urlTitle))
            {
                urlTitle = title;
            }

            if (!string.IsNullOrEmpty(urlTitle))
            {
                urlTitle = urlTitle.ToLowerInvariant();
                urlTitle = urlTitle.Normalize(NormalizationForm.FormD);

				const string find = @"æø&€\/$£* +=!?:;,.";
				const string repl = @"aooe--slx---------";
				if (find.Length != repl.Length) throw new ArgumentException("find");
                for (int i = 0; i < find.Length; i++)
                {
                    urlTitle = urlTitle.Replace(find[i], repl[i]);
                }

                urlTitle = Regex.Replace(urlTitle, @"[^a-z0-9-]", string.Empty);
                urlTitle = Regex.Replace(urlTitle, @"-{2,}", "-");
                urlTitle = Regex.Replace(urlTitle, @"^-", string.Empty);
                urlTitle = Regex.Replace(urlTitle, @"-$", string.Empty);
            }

            context.Response.Write(urlTitle);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
