using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Validator;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class FolderEdit : PageController,IEditor
	{
		protected virtual int? GetParentNodeId()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}

        protected virtual bool HasMessage()
        {
            bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
            return hasMessage.HasValue && hasMessage.Value;
        }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		    int? parentNodeId = GetParentNodeId();
            if ((parentNodeId == null || parentNodeId <= 0) && (GetIdOrNull() == null))
				Master.DenyTreeSelection();
		}
        
		protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HasMessage() && !messager.HasMessages)
            {
                messager.MessageType = InfoType.Success;
                messager.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);
            }
        }
		
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			ParentTreeViewControl.NodeCommand += OnParentTreeNodeCommand;
            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.SiteStructure.Page.GetDeleteUrl(GetId()));
        }

		protected override void PopulateForm()
		{
			ParentPanel.Visible = false;

            if (GetIdOrNull() == null)
            {
				Master.Breadcrumbs.Clear();
				Master.Breadcrumbs.Add(Resources.General.Literal_CreateFolder);

                if (!GetParentNodeId().HasValue)
                {
					ParentPanel.Visible = true;
					PopulateTreeView(Master.CurrentRootId);
                }
            }
            else
            {
                IContentNode contentNode = IoC.Resolve<IContentNodeSecureService>().GetById(GetId());
                if (contentNode != null)
                {
                    TitleTextBox.Text = contentNode.Title;
                    CommonNameTextBox.Text = contentNode.CommonName;
                    IAccess access = IoC.Resolve<IAccessSecureService>().GetById(contentNode.AccessId);
                    (access.CommonName == "All" ? AllRadioButton : access.CommonName == "SignedIn" ? SignedInRadioButton : NotSignedInRadioButton).Checked = true;
                }
            }

            DeleteButton.Visible = GetIdOrNull().HasValue;
		}
		
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}
		
		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateForm())
			{
                messager.Add(Resources.SiteStructureMessage.FormValidationFailed);
				return;
			}

			IContentNode contentNode;
			if (GetIdOrNull() == null)
			{
				contentNode = IoC.Resolve<IContentNodeSecureService>().Create();
				contentNode.ContentNodeTypeId = (int)ContentNodeTypeInfo.Folder;
				contentNode.ContentNodeStatusId = (int)ContentNodeStatusInfo.Online;
				contentNode.ParentContentNodeId = GetParentNodeId().HasValue
					? (GetParentNodeId() < 0 ? null : GetParentNodeId())
					: (ParentTreeViewControl.SelectedNodeId < 0 ? null : ParentTreeViewControl.SelectedNodeId);
				contentNode.Ordinal = 0;
				contentNode.SiteStructureRegistryId = contentNode.ParentContentNodeId.HasValue
					? IoC.Resolve<IContentNodeSecureService>().GetById(contentNode.ParentContentNodeId.Value).SiteStructureRegistryId
					: -1 * (GetParentNodeId().HasValue ? GetParentNodeId().Value : ParentTreeViewControl.SelectedNodeId.Value);
			}
			else
			{
				contentNode = IoC.Resolve<IContentNodeSecureService>().GetById(GetId());
			}
			if (contentNode == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}
			contentNode.Title = TitleTextBox.Text;
			contentNode.CommonName = CommonNameTextBox.Text;
			contentNode.AccessId = IoC.Resolve<IAccessSecureService>().GetByCommonName(AllRadioButton.Checked ? "All" : SignedInRadioButton.Checked ? "SignedIn" : "NotSignedIn").Id;
			var contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();

            if (!ContentNodeSaveValidator.IsCommonNameUnique(contentNode))
            {
                messager.Add(Resources.SiteStructureMessage.ContentNodeCommomNameExist);
                return;
            }
            
			int nodeId = contentNodeSecureService.Save(SignInHelper.SignedInSystemUser, contentNode);

            Response.Redirect(PathHelper.SiteStructure.Page.GetFolderEditWithMessageUrl(nodeId));
		}

		protected virtual void OnParentTreeNodeCommand(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			ParentTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			ParentTreeViewControl.DataBind();
		}

		protected void PopulateTreeView(int? id)
		{
			var nodeId = id ?? Master.CurrentRootId;
			ParentTreeViewControl.SelectedNodeId = nodeId;
			ParentTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			ParentTreeViewControl.DataBind();
			ParentTreeViewControl.PopulatePath(nodeId);
		}
		
		protected bool ValidateForm()
		{
			Page.Validate("vg");
			return Page.IsValid;
		}
	}
}
