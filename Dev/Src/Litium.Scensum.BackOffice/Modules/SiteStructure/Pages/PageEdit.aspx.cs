using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Common;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls.Button;
using Convert = Litium.Scensum.Foundation.Convert;
using Litium.Scensum.BackOffice.UserControls.ContextMenu;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class PageEdit : StatePageController<Dictionary<int, List<IBlock>>>, IEditor
	{
		private const int DecStepMove = 10;
		private const int BinStepMove = 1;
		private const int BlockMoveUpStep = (int)(-1.5 * DecStepMove);
		private const int BlockMoveDownStep = (int)(1.5 * DecStepMove);

		private bool? _isFromMaster;
		private readonly Dictionary<string, bool> masterBlocksState = new Dictionary<string, bool>();

		protected virtual bool HasMessage()
		{
			bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
			return hasMessage.HasValue && hasMessage.Value;
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			SaveBlockButton.Click += BtnSaveBlockClick;
			AreaGrid.ItemDataBound += RptAreaItemDataBound;
			SaveMoveBlockButton.Click += BtnMoveBlockClick;
            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.SiteStructure.Page.GetDeleteUrl(GetId()));
        }

		protected override void PopulateForm()
		{
			InitializeState();

			IContentPage contentPage = IoC.Resolve<IContentPageSecureService>().GetById(GetId());
			Collection<IContentAreaFull> areas = GetAreasFromDatabase();

            if (contentPage.IsMaster)
            {
                var childPages = IoC.Resolve<IContentPageSecureService>().GetAllByMaster(contentPage.Id);
                if (null != childPages && childPages.Count > 0)
                {
                    MasterPageChildren.DataSource = childPages;
                    MasterPageChildren.DataBind();
                }
            }

		    AreaGrid.DataSource = areas;
			AreaGrid.DataBind();

			SelectContainerList.DataSource = areas;
			SelectContainerList.DataBind();

			TemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(contentPage.TemplateId);

			var template = IoC.Resolve<ITemplateSecureService>().GetById(contentPage.TemplateId);
			TemplateTitleLabel.Text = template.Title;

			var blockTypeSecureService = IoC.Resolve<IBlockTypeSecureService>();

			BlockTypeList.DataTextField = "Title";
			BlockTypeList.DataValueField = "CommonName";

			BlockTypeList.DataSource = blockTypeSecureService.GetAllByContentPageType(contentPage.ContentPageTypeId);
			BlockTypeList.DataBind();

			if(contentPage.MasterPageId.HasValue)
				PopulateMasterLink(contentPage.MasterPageId.Value);

		    DeleteButton.Visible = GetIdOrNull().HasValue;
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			Collection<IContentAreaFull> areas = GetAreas();
			var blocks = new Collection<IBlock>();
			foreach (var area in areas)
			{
				if (area != null)
				{
					var sortedBlocks = GetIsFromMasterWithBlocks(area.Id) ? SortForSave(area.Blocks) : Sort(area.Blocks, BinStepMove);
					foreach (IBlock block in sortedBlocks)
					{
						blocks.Add(block);
					}
				}
			}
			IoC.Resolve<IBlockSecureService>().SaveAll(SignInHelper.SignedInSystemUser, blocks);

			SaveMesagerContainer.MessageType = InfoType.Success;
			SaveMesagerContainer.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);

			((Pages)Master).PopulateTreeView(GetIdOrNull());
			ShowBlockOptions();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (HasMessage() && !SaveMesagerContainer.HasMessages)
			{
				SaveMesagerContainer.MessageType = InfoType.Success;
				SaveMesagerContainer.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);
			}
			ConfigureSelectAll();
		}

		protected virtual void BtnMoveBlockClick(object sender, EventArgs e)
		{
			int areaFromId = int.Parse(AreaBlockFromHiddenField.Value, CultureInfo.CurrentCulture);
			int areaToId = int.Parse(SelectContainerList.SelectedValue, CultureInfo.CurrentCulture);
			if (areaFromId == areaToId)
			{
				return;
			}
			int blockId = int.Parse(MovedBlockIdHiddenField.Value, CultureInfo.CurrentCulture);
			IContentAreaFull areaFrom = GetFullAreaById(areaFromId);
			IContentAreaFull areaTo = GetFullAreaById(areaToId);
			IBlock block = areaFrom.Blocks.First(item => item.Id == blockId);
			if (areaTo.Blocks.Count > 0 && (areaTo.Blocks.FirstOrDefault(item => item.Title == block.Title) != null))
			{
				ShowError(ErrorMoveSameNameDiv, ErrorMoveSameNameContainer, MoveBlockPopup);
				return;
			}

			areaFrom.Blocks.Remove(block);
			int rptIndexFrom = GetRepeaterItemByAreaId(areaFromId);
			PopulateBlockGrid(rptIndexFrom, areaFrom.Blocks);

			block.ContentAreaId = areaToId;

			if (areaTo.Blocks.Count > 0)
			{
				block.Ordinal = areaTo.Blocks.Max(item => item.Ordinal) + DecStepMove;
			}
			areaTo.Blocks.Add(block);

			int rptIndexTo = GetRepeaterItemByAreaId(areaToId);
			RepeaterItem itemToOpen = AreaGrid.Items[rptIndexTo];
			PopulateBlockGrid(rptIndexTo, areaTo.Blocks);
			SetActiveArea(itemToOpen);
			BlockNameTextBox.Text = string.Empty;
		}
		protected virtual void BtnSaveBlockClick(object sender, EventArgs e)
		{
			int itemId = int.Parse(ItemIdHiddenField.Value, CultureInfo.CurrentCulture);
			RepeaterItem itemToOpen = AreaGrid.Items[itemId];
			int areaId = int.Parse(((HiddenField)itemToOpen.FindControl("AreaIdHiddenField")).Value, CultureInfo.CurrentCulture);

			string blockTypeCommonName = BlockTypeList.SelectedValue;

			int blockTypeId = IoC.Resolve<IBlockTypeSecureService>().GetByCommonName(blockTypeCommonName).Id;
			var service = IoC.Resolve<IBlockCreateSecureService>(blockTypeCommonName);

			string blockTitle = Convert.RemoveRedundantSpaces(BlockNameTextBox.Text);
			IContentAreaFull area = GetFullAreaById(areaId);
			if (area.Blocks.Count > 0 && (area.Blocks.FirstOrDefault(item => item.Title == blockTitle) != null))
			{
				ShowError(ErrorSameNameDiv, ErrorSameNameContainer, SaveDivPopup);
				return;
			}

			IBlock newBlock = service.SaveNew(SignInHelper.SignedInSystemUser, GetId(), areaId, blockTypeId, blockTitle);
			newBlock.BlockType = IoC.Resolve<IBlockTypeSecureService>().GetById(newBlock.BlockTypeId);

			if (area.Blocks.Count > 0)
			{
				newBlock.Ordinal = area.Blocks.Max(item => item.Ordinal) + DecStepMove;
			}
			area.Blocks.Add(newBlock);

			SetActiveArea(itemToOpen);
			BlockNameTextBox.Text = string.Empty;
			PopulateBlockGrid(itemId, area.Blocks);
		}
		protected virtual void RptAreaItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			var repeaterItem = e.Item;
			var area = repeaterItem.DataItem as IContentAreaFull;
			if (area == null)
			{
				return;
			}
			DataBindArea(repeaterItem, area);
			if (area.Blocks.Count > 0 || GetIsFromMasterWithBlocks(area.Id))
			{
				DataBindBlocks(repeaterItem, area);
			}

			DataBindStatusList(repeaterItem);
			DataBindAccessList(repeaterItem);
		}

		[SuppressMessage("Microsoft.Maintainability", "CA1500:VariableNamesShouldNotMatchFieldNames", MessageId = "BlockMenuMoveButton")]
		protected virtual void GVRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridView gridView = ((GridView)sender);
			GridViewRow row = e.Row;
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				var applyToAllSelectedDiv = (HtmlGenericControl)(gridView.Parent).FindControl("ApplyToAllSelectedDiv");
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + gridView.ClientID + @"');
					ShowBlockOption(this,'" + applyToAllSelectedDiv.ClientID + @"');");
				selectAllCheckBox.Checked = false;
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var block = (IBlock)row.DataItem;
                var editLink = (HyperLink)row.FindControl("EditLink");
                var ddlStatus = (ContextMenu3)row.FindControl("StatusList");
				var ddlAccess = (ContextMenu3)row.FindControl("AccessList");
				var typeLabel = (Label)row.FindControl("TypeLabel");
				var deleteButton = (ImageButton)row.FindControl("DeleteButton");
				var blockMenuMoveButton = (LinkButton)row.FindControl("BlockMenuMoveButton");
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				var contextMenuControl = row.FindControl("ContextMenuControl");
				var ordinalDiv = row.FindControl("OrdinalDiv");
				
				var accessList = IoC.Resolve<IAccessSecureService>().GetAll();
                var contextMenuAccessListDataSource = new Collection<ContextMenuDataSourceItem>(accessList.Select(
                        item =>
                        new ContextMenuDataSourceItem
                            {
                            Text = item.Title,
                            Value = item.Id.ToString(CultureInfo.InvariantCulture),
                            ImageSrc =
                                ResolveUrl(string.Format(CultureInfo.InvariantCulture, "~/Media/Images/Customer/Access/{0}.png", item.CommonName))
                        }).ToList());

				#region Master block
				if (block.Id <= 0)
				{
					ddlStatus.Visible = ddlAccess.Visible = deleteButton.Visible = selectCheckBox.Visible
					                                                               = contextMenuControl.Visible = false;
					editLink.NavigateUrl = string.Empty;
					editLink.CssClass = "link-to-lable";

					var grid = (Control)sender;
					bool showRow = masterBlocksState.TryGetValue(grid.ClientID, out showRow) ? showRow : true;
					if (block.Id == 0)
					{
						var masterCollapseButton = (ImageButton)row.FindControl("MasterCollapseButton");
						var masterShow = (CheckBox)row.FindControl("MasterShow");
						
						var script = "MasterBlockCollapseExpand('" 
							+ grid.ClientID + "', '"
							+ masterShow.ClientID + "', '" 
							+ masterCollapseButton.ClientID + "', '"
							+ ResolveClientUrl("~/Media/Images/Tree/tree-collapse.png") + "','" 
							+ ResolveClientUrl("~/Media/Images/Tree/tree-expand.png") + "'); "
							+ "return false;";
						masterCollapseButton.Attributes.Add("onclick", script);
						masterShow.Checked = showRow;
						masterCollapseButton.ImageUrl = showRow
															? "~/Media/Images/Tree/tree-collapse.png"
															: "~/Media/Images/Tree/tree-expand.png";
						masterCollapseButton.Visible = true;
						typeLabel.Visible = false;
						row.CssClass = "master-block-header-row";
					}
					else
					{
						var statusLabel = (Label)row.FindControl("StatusLabel");
						var accessLabel = (Label)row.FindControl("AccessLabel");
						statusLabel.Visible = accessLabel.Visible = true;
						ordinalDiv.Visible = false;
                        accessLabel.Text = accessList.FirstOrDefault(a => a.Id == block.AccessId).Title;
						statusLabel.Text = ((BlockStatusInfo)block.BlockStatusId).ToString();
						typeLabel.Text = block.BlockType.Title;

						row.CssClass = "master-block-data-row";
						row.Cells[1].CssClass = "master-block-second-cell";
						if (!showRow) row.Style.Add("display", "none");
					}
					return;
				}
				#endregion

				var settings = new SiteStructureBlockSetting(block.BlockType.CommonName);
				editLink.NavigateUrl = PathHelper.SiteStructure.Page.GetBlockEditUrl(settings.EditPageName, GetId(), block.Id);

                var statusDataSource = Enum.GetNames(typeof(BlockStatusInfo));
                var statusListContextMenuDataSource = new Collection<ContextMenuDataSourceItem>(statusDataSource.Select(
                            item =>
                            new ContextMenuDataSourceItem
                                {
                                Text = item,
                                Value = item,
                                ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture, "~/Media/Images/Common/{0}.gif", item))
                            }).ToList());
                ddlStatus.DataSource = statusListContextMenuDataSource;
				ddlStatus.DataBind();
                ddlStatus.SelectedValue = ((BlockStatusInfo) block.BlockStatusId).ToString();

                ddlAccess.DataSource = contextMenuAccessListDataSource;
				ddlAccess.DataBind();
                ddlAccess.SelectedValue = block.AccessId.ToString(CultureInfo.InvariantCulture);

				typeLabel.Text = block.BlockType.Title;

				deleteButton.Attributes.Add("onclick", "return DeleteConfirmation('block');");

				blockMenuMoveButton.OnClientClick = string.Format(CultureInfo.CurrentCulture, "BlockMenuMoveClick({0})", block.ContentAreaId);

				var applyToAllSelectedDiv = (HtmlGenericControl)(gridView.Parent).FindControl("ApplyToAllSelectedDiv");
				var selectAllCheckBox = (CheckBox)gridView.HeaderRow.FindControl("SelectAllCheckBox");
				selectCheckBox.Attributes.Add("onclick", "UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');
					ShowBlockOption(this,'" + applyToAllSelectedDiv.ClientID + @"');");
			}
		}
		protected virtual void GVRowCommand(object sender, CommandEventArgs e)
		{
			int itemId = FindParentRepeaterItem((GridView)sender).ItemIndex;
			int blockId;

			if (!int.TryParse(e.CommandArgument.ToString(), out blockId))
			{
				blockId = 0;
			}

			switch (e.CommandName)
			{
				case "UpOrdinal":
					MoveBlock(itemId, blockId, BlockMoveUpStep);
					break;
				case "DownOrdinal":
					MoveBlock(itemId, blockId, BlockMoveDownStep);
					break;
				case "RefreshBlockOrder":
					RefreshOrder(itemId);
					break;
				case "MoveBlock":
					MoveBlock(itemId, blockId);
					break;
				case "DeleteBlock":
					DeleteBlock(itemId, blockId);
					break;
			}
		}
		protected virtual void SetForSelectedClick(object sender, EventArgs e)
		{
			var imageLinkButton = (ImageLinkButton)sender;
			var repeaterItem = FindParentRepeaterItem(imageLinkButton);

			var gridView = (GridView)repeaterItem.FindControl("BlocksGrid");
			IEnumerable<int> ids = gridView.GetSelectedIds();

			var statusList = (ContextMenu3)repeaterItem.FindControl("StatusList");
			int? statusId = string.IsNullOrEmpty(statusList.SelectedValue) ? (int?)null : (int)(BlockStatusInfo)Enum.Parse(typeof(BlockStatusInfo), statusList.SelectedValue);

			var accessList = (ContextMenu3)repeaterItem.FindControl("AccessList");
			int? accessId = string.IsNullOrEmpty(accessList.SelectedValue) ? (int?)null : System.Convert.ToInt32(accessList.SelectedValue, CultureInfo.CurrentCulture);

			int areaId = Int32.Parse(imageLinkButton.CommandArgument, CultureInfo.CurrentCulture);
			IContentAreaFull areaFull = GetFullAreaById(areaId);

			foreach (int id in ids)
			{
				int i = id;// To prevent access to modified closure
				IBlock block = areaFull.Blocks.First(item => item.Id == i);
				block.BlockStatusId = statusId ?? block.BlockStatusId;
				block.AccessId = accessId ?? block.AccessId;
			}

			PopulateBlockGrid(repeaterItem.ItemIndex, areaFull.Blocks);
			statusList.SelectedValue = string.Empty;
			accessList.SelectedValue = string.Empty;
		}
		protected virtual void DeleteSelectedClick(object sender, EventArgs e)
		{
			var imageLinkButton = (ImageLinkButton)sender;
			var repeaterItem = FindParentRepeaterItem(imageLinkButton);

			var gridView = (GridView)repeaterItem.FindControl("BlocksGrid");
			IEnumerable<int> ids = gridView.GetSelectedIds();

			var blockSecureService = IoC.Resolve<IBlockSecureService>();
			blockSecureService.DeleteAll(SignInHelper.SignedInSystemUser, ids);

			int areaId = Int32.Parse(imageLinkButton.CommandArgument, CultureInfo.CurrentCulture);
			Collection<IBlock> blocks = blockSecureService.GetAllByContentNodeAndContentArea(GetId(), areaId);
			PopulateBlockGrid(repeaterItem.ItemIndex, blocks);

			SaveMesagerContainer.MessageType = InfoType.Success;
			SaveMesagerContainer.Add(Resources.SiteStructureMessage.BlocksDeletedSuccessfully);
		}

		protected virtual void DataBindArea(RepeaterItem repeaterItem, IContentAreaFull area)
		{
			var areaCollapsiblePanel = (CollapsiblePanel)repeaterItem.FindControl("AreaCollapsiblePanel");
			string divBlockGvId = repeaterItem.FindControl("BlockGridDiv").ClientID;
			areaCollapsiblePanel.ControlToExpand = divBlockGvId;
			areaCollapsiblePanel.ActionButtonClientClick = "CreateBlock('" + SaveDivPopup.BehaviorID + "','" + repeaterItem.ItemIndex + "','" + area.Id + "'); return false;";
		}
		protected virtual void DataBindBlocks(RepeaterItem repeaterItem, IContentAreaFull area)
		{
			var gridView = (GridView)repeaterItem.FindControl("BlockGridDiv").FindControl("BlocksGrid");
			Collection<IBlock> blocks = area.Blocks;

			gridView.DataSource = GetIsFromMasterWithBlocks(area.Id) ? AppendMasterBlocks(blocks, area.Id) : Sort(blocks, DecStepMove);
			gridView.DataBind();
		}

		protected virtual IEnumerable<IBlock> AppendMasterBlocks(Collection<IBlock> blocks, int areaId)
		{
			if (blocks.FirstOrDefault(b => b.Id == 0) == null)
			{
				var fakeMasterBlock = IoC.Resolve<IBlock>();
				fakeMasterBlock.Id = 0;
				fakeMasterBlock.Ordinal = 0;
				fakeMasterBlock.Title = "Master blocks";
				blocks.Add(fakeMasterBlock);
			}
            var source = Sort(blocks, DecStepMove);
            var masterBlocksSet = RetrieveMasterBlocks(areaId);
			var index = source.IndexOf(source.FirstOrDefault(b => b.Id == 0));
			foreach (var block in masterBlocksSet)
			{
				index++;
				source.Insert(index, block);
			}
			return source;
		}

		protected virtual IEnumerable<IBlock> RetrieveMasterBlocks(int areaId)
		{
			List<IBlock> masterBlocksSet;
			if (!State.TryGetValue(areaId, out masterBlocksSet))
			{
				var masterId = IoC.Resolve<IContentPageSecureService>().GetById(GetId()).MasterPageId.Value;
				var masterBlocks = IoC.Resolve<IBlockSecureService>().GetAllByContentNodeAndContentArea(masterId, areaId);
				masterBlocksSet = new List<IBlock>();
				foreach (var block in masterBlocks)
				{
					block.Id = -1 * block.Id;
					masterBlocksSet.Add(block);
				}
				Sort(masterBlocksSet);
				State.Add(areaId, masterBlocksSet);
			}
			return masterBlocksSet;
		}

		protected virtual void DataBindStatusList(RepeaterItem repeaterItem)
		{
            var statusDataSource = Enum.GetNames(typeof(BlockStatusInfo));
            var statusListContextMenuDataSource = new Collection<ContextMenuDataSourceItem>(statusDataSource.Select(
                        item =>
                        new ContextMenuDataSourceItem
                            {
                            Text = item,
                            Value = item,
                            ImageSrc = ResolveUrl(string.Format(CultureInfo.InvariantCulture, "~/Media/Images/Common/{0}.gif", item))
                        }).ToList());
			statusListContextMenuDataSource.Insert(0, new ContextMenuDataSourceItem { Value = string.Empty, Text = "Status..." });
			var statusList = (ContextMenu3)repeaterItem.FindControl("StatusList");
		    statusList.DataSource = statusListContextMenuDataSource;
			statusList.DataBind();
		}
        protected virtual void DataBindAccessList(RepeaterItem repeaterItem)
        {
            var ddlAccess = (ContextMenu3) repeaterItem.FindControl("AccessList");
            ddlAccess.DataSource = _accessDataSource;
            ddlAccess.DataBind();
        }

	    protected static void ShowError(Control divError, MessageContainer messager, ModalPopupExtender popupExtender)
		{
			divError.Visible = true;
			messager.Add(Resources.GeneralMessage.BlockTitleExist);
			popupExtender.Show();
		}
		protected static void SetActiveArea(Control itemToOpen)
		{
			var areaCollapsiblePanel = (CollapsiblePanel)itemToOpen.FindControl("AreaCollapsiblePanel");
			areaCollapsiblePanel.Open();
		}
		protected static RepeaterItem FindParentRepeaterItem(Control control)
		{
			var parentControl = control.Parent;
			if (parentControl == null)
			{
				return null;
			}
			var repeaterItem = parentControl as RepeaterItem;
			if (repeaterItem != null)
			{
				return repeaterItem;
			}
			return FindParentRepeaterItem(parentControl);
		}
		protected void MoveBlock(int itemId, int blockId)
		{
			AreaBlockFromHiddenField.Value = ((HiddenField)AreaGrid.Items[itemId].FindControl("AreaIdHiddenField")).Value;
			MovedBlockIdHiddenField.Value = blockId.ToString(CultureInfo.CurrentCulture);
			MoveBlockPopup.Show();
		}
		protected void PopulateBlockGrid(int itemId, Collection<IBlock> list)
		{
			var blockGrid = (GridView)AreaGrid.Items[itemId].FindControl("BlockGridDiv").FindControl("BlocksGrid");
			var areaId = int.Parse(((HiddenField)AreaGrid.Items[itemId].FindControl("AreaIdHiddenField")).Value, CultureInfo.CurrentCulture);
			blockGrid.DataSource = GetIsFromMasterWithBlocks(areaId) ? AppendMasterBlocks(list, areaId) : Sort(list, DecStepMove);
			blockGrid.DataBind();
			ShowBlockOptions();
		}
		protected void ShowBlockOptions()
		{
			foreach (RepeaterItem item in AreaGrid.Items)
			{
				bool selected = false;
				GridView gridView = (GridView)item.FindControl("BlockGridDiv").FindControl("BlocksGrid");
				foreach (GridViewRow row in gridView.Rows)
				{
					CheckBox selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
					if (selectCheckBox.Checked)
					{
						selected = true;
						break;
					}
				}
				var applyToAllSelectedDiv = (HtmlGenericControl)item.FindControl("ApplyToAllSelectedDiv");
				if (selected)
				{
					applyToAllSelectedDiv.Style.Add("display", "block");
				}
				else
				{
					applyToAllSelectedDiv.Style.Add("display", "none");
				}
			}
		}

		protected static Collection<IBlock> Sort(IEnumerable<IBlock> list, int ordinalsStep)
		{
			var blocks = new List<IBlock>(list);
			Sort(blocks);
			for (int i = 0; i < blocks.Count; i++)
			{
				blocks[i].Ordinal = (i + 1) * ordinalsStep;
			}
			return new Collection<IBlock>(blocks);
		}

		[SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
		protected static void Sort(List<IBlock> blocks)
		{
			blocks.Sort(delegate(IBlock block1, IBlock block2)
			            	{
			            		if (block1.Ordinal < block2.Ordinal)
			            			return -1;
			            		if (block1.Ordinal > block2.Ordinal)
			            			return 1;
			            		return 0;
			            	});
		}

		protected static IEnumerable<IBlock> SortForSave(IEnumerable<IBlock> list)
		{
			var blocks = new List<IBlock>(list);
			Sort(blocks);
			var masterHeader = blocks.FirstOrDefault(b => b.Id == 0);
			if (masterHeader == null) return blocks;

			var masterSetOrdinal = masterHeader.Ordinal;
			var blocksBeforeMaster = new List<IBlock>(blocks.Where(b => b.Ordinal < masterSetOrdinal));
			for (int i = 0; i < blocksBeforeMaster.Count; i++)
			{
				blocksBeforeMaster[i].Ordinal = i - blocksBeforeMaster.Count;
			}
			var blocksAfterMaster = new List<IBlock>(blocks.Where(b => b.Ordinal > masterSetOrdinal));
			for (int i = 0; i < blocksAfterMaster.Count; i++)
			{
				blocksAfterMaster[i].Ordinal = i + 1;
			}
			return blocksBeforeMaster.Union(blocksAfterMaster);
		}

		protected void RefreshOrder(int itemId)
		{
			Collection<IBlock> blocks = GetBlocksFromGrid(itemId);
			if (blocks != null)
			{
				PopulateBlockGrid(itemId, blocks);
			}
		}
		protected void MoveBlock(int itemId, int blockId, int step)
		{
			Collection<IBlock> blocks = GetBlocksFromGrid(itemId);
			if (blocks != null)
			{
				blocks.FirstOrDefault(b => b.Id == blockId).Ordinal += step;
				PopulateBlockGrid(itemId, blocks);
			}
		}
		protected void DeleteBlock(int itemId, int blockId)
		{
			var blockService = IoC.Resolve<IBlockSecureService>();
			blockService.Delete(SignInHelper.SignedInSystemUser, blockId);
			int areaId = int.Parse(((HiddenField)AreaGrid.Items[itemId].FindControl("AreaIdHiddenField")).Value, CultureInfo.CurrentCulture);
			Collection<IBlock> blocks = blockService.GetAllByContentNodeAndContentArea(GetId(), areaId);
			PopulateBlockGrid(itemId, blocks);
		}
		protected Collection<IBlock> GetBlocksFromGrid(int itemId)
		{
			if (!ValidateOrdinals())
			{
				errors.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return null;
			}
			var blockGrid = (GridView)AreaGrid.Items[itemId].FindControl("BlockGridDiv").FindControl("BlocksGrid");
			var blocks = new Collection<IBlock>();
			IBlock block;
			foreach (GridViewRow row in blockGrid.Rows)
			{
				var blockId = int.Parse(((HiddenField)row.FindControl("BlockIdHiddenField")).Value, CultureInfo.CurrentCulture);
				if (blockId < 0) continue;

				block = IoC.Resolve<IBlockSecureService>().Create();
				block.Id = blockId;
				block.ContentNodeId = GetId();
				int areaId = int.Parse(((HiddenField)AreaGrid.Items[itemId].FindControl("AreaIdHiddenField")).Value, CultureInfo.CurrentCulture);
				block.ContentAreaId = areaId;
				block.Title = Server.HtmlDecode(HttpUtility.HtmlDecode(((HyperLink)row.FindControl("EditLink")).Text));
				block.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
				
				if (blockId > 0)
				{
					block.BlockTypeId = int.Parse(((HiddenField)row.FindControl("BlockTypeIdHiddenField")).Value, CultureInfo.CurrentCulture);
					block.BlockStatusId = (int)(BlockStatusInfo)Enum.Parse(typeof(BlockStatusInfo), ((ContextMenu3)row.FindControl("StatusList")).SelectedValue);
					block.AccessId = int.Parse(((ContextMenu3)row.FindControl("AccessList")).SelectedValue, CultureInfo.CurrentCulture);
					block.BlockType = IoC.Resolve<IBlockTypeSecureService>().GetById(block.BlockTypeId);
					int templateId;
					block.TemplateId = int.TryParse(((HiddenField) row.FindControl("BlockTemplateIdHiddenField")).Value, out templateId)
					                   	? (int?)templateId
					                   	: null;
				}

				blocks.Add(block);

				if (blockId == 0)
				{
					var showMasterBlocks = ((CheckBox) row.FindControl("MasterShow")).Checked;
					masterBlocksState.Add(blockGrid.ClientID, showMasterBlocks);
				}
			}
			return blocks;
		}
		protected bool ValidateOrdinals()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected Collection<IContentAreaFull> GetAreasFromDatabase()
		{
			IContentPage contentPage = IoC.Resolve<IContentPageSecureService>().GetById(GetId());
			Collection<IContentAreaFull> areas = IoC.Resolve<IContentAreaSecureService>().GetAllFullByTemplate(contentPage.TemplateId, contentPage.Id);
			return areas;
		}
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected Collection<IContentAreaFull> GetAreas()
		{
			Collection<IContentAreaFull> areas = GetAreasFromDatabase();
			foreach (RepeaterItem item in AreaGrid.Items)
			{
				var hfAreaId = (HiddenField)item.FindControl("AreaIdHiddenField");
				int areaId = int.Parse(hfAreaId.Value, CultureInfo.CurrentCulture);
				GetAreaById(areas, areaId).Blocks = GetBlocksFromGrid(item.ItemIndex);
			}
			return areas;
		}
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected IContentAreaFull GetFullAreaById(int areaId)
		{
			IContentAreaFull areaFull = null;
			Collection<IContentAreaFull> areas = GetAreasFromDatabase();
			foreach (RepeaterItem item in AreaGrid.Items)
			{
				var hfAreaId = (HiddenField)item.FindControl("AreaIdHiddenField");
				int hidenAreaIdId = int.Parse(hfAreaId.Value, CultureInfo.CurrentCulture);
				if (hidenAreaIdId != areaId)
				{
					continue;
				}
				areaFull = GetAreaById(areas, areaId);
				areaFull.Blocks = GetBlocksFromGrid(item.ItemIndex);
			}
			return areaFull;
		}
		protected int GetRepeaterItemByAreaId(int areaId)
		{
			foreach (RepeaterItem item in AreaGrid.Items)
			{
				var hfAreaId = (HiddenField)item.FindControl("AreaIdHiddenField");
				int areaIdFromHiddenField = int.Parse(hfAreaId.Value, CultureInfo.CurrentCulture);
				if (areaId == areaIdFromHiddenField)
				{
					return item.ItemIndex;
				}
			}
			return -1;
		}
		protected static IContentAreaFull GetAreaById(Collection<IContentAreaFull> areas, int areaId)
		{
			return areas.First(item => item.Id == areaId);
		}

		protected virtual void PopulateMasterLink(int masterId)
		{
			const string separator = @"\";
			var service = IoC.Resolve<IContentNodeSecureService>();
			var node = service.GetById(masterId);
			var link = new StringBuilder(node.Title);
			while (node.ParentContentNodeId.HasValue)
			{
				node = service.GetById(node.ParentContentNodeId.Value);
				link.Insert(0, node.Title + separator);
			}
			link.Insert(0, separator);
			MasterLink.Text = link.ToString();
			MasterLink.NavigateUrl = PathHelper.SiteStructure.Page.GetPageEditUrl(masterId);
			MasterLinkDiv.Visible = MasterLinkSpace.Visible = true;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsFromMaster()
		{
			if (!_isFromMaster.HasValue)
			{
				_isFromMaster = IoC.Resolve<IContentPageSecureService>().GetById(GetId()).MasterPageId.HasValue;
			}
			return _isFromMaster.Value;
		}

        private Collection<ContextMenuDataSourceItem> _accessDataSource;
        protected override void OnInitComplete(EventArgs e)
        {
            base.OnInitComplete(e);

            _accessDataSource = new Collection<ContextMenuDataSourceItem>(IoC.Resolve<IAccessSecureService>().GetAll().Select(
                        item =>
                        new ContextMenuDataSourceItem
                            {
                            Text = item.Title,
                            Value = item.Id.ToString(CultureInfo.InvariantCulture),
                            ImageSrc =
                                ResolveUrl(string.Format(CultureInfo.InvariantCulture, "~/Media/Images/Customer/Access/{0}.png", item.CommonName))
                        }).ToList());
            ContextMenuDataSourceItem fakeAccess = new ContextMenuDataSourceItem();
            fakeAccess.Text = "Access...";
            fakeAccess.Value = string.Empty;
            _accessDataSource.Insert(0, fakeAccess);
        }

		protected virtual bool GetIsFromMasterWithBlocks(int areaId)
		{
			return GetIsFromMaster() && RetrieveMasterBlocks(areaId).Count() > 0;
		}

		protected virtual void InitializeState()
		{
			if (GetIsFromMaster() && State == null)
			{
				State = new Dictionary<int, List<IBlock>>();
			}
		}

		protected virtual void ConfigureSelectAll()
		{
			var script = @"
				var tables = $(""table[id*='BlocksGrid']"");
				for (var i = 0; i < tables.length; i++) {
					var checkBoxes = $(tables[i]).find(""td > input[type='checkbox']:visible"");
					if (checkBoxes.length == 0) {
						$(tables[i]).find(""th > input[type='checkbox']:visible"").hide();
					}
				};";
			if (!IsPostBack)
			{
				script = "$(function(){" + script + "});";
			}
			ScriptManager.RegisterClientScriptBlock(EditUpdatePanel, EditUpdatePanel.GetType(), Guid.NewGuid().ToString(),
													script, true);
		}
	}
}
