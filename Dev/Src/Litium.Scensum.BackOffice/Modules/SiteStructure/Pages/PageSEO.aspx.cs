using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class PageSeo : PageController,IEditor
	{
		private const int NewObjectId = 0;
		private const int MaxLength = 250;

		private IContentPageSeoSetting _contentPageSeoSetting;

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}
		protected override void PopulateForm()
		{
			if (GetId() != NewObjectId)
			{
				PopulateData();
			}

			var master = Master as Pages;
			if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.SiteStructure.Literal_SearchEngineOptimization);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateFields())
			{
				return;
			}
			if (GetContentPageSeoSetting() == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}
			GetContentPageSeoSetting().Title = string.IsNullOrEmpty(SeoTitleTextBox.Text) ? null : SeoTitleTextBox.Text;
			GetContentPageSeoSetting().Description = string.IsNullOrEmpty(SeoDescriptionTextBox.Text) ? null : SeoDescriptionTextBox.Text;
			GetContentPageSeoSetting().Keywords = string.IsNullOrEmpty(SeoKeywordsTextBox.Text) ? null : SeoKeywordsTextBox.Text;
			var contentPageSeoSettingSecureService = IoC.Resolve<IContentPageSeoSettingSecureService>();
			contentPageSeoSettingSecureService.Save(SignInHelper.SignedInSystemUser, GetContentPageSeoSetting());
			SaveMesageContainer.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);

			((Pages)Master).PopulateTreeView(GetIdOrNull());
		}

		protected IContentPageSeoSetting GetContentPageSeoSetting()
		{
			if (_contentPageSeoSetting == null)
			{
				var seoService = IoC.Resolve<IContentPageSeoSettingSecureService>();
				_contentPageSeoSetting = seoService.GetById(GetId());
				if (_contentPageSeoSetting == null)
				{
					_contentPageSeoSetting = seoService.Create();
					_contentPageSeoSetting.Id = GetId();
				}
			}
			return _contentPageSeoSetting;
		}

		protected void PopulateData()
		{
			var seoService = IoC.Resolve<IContentPageSeoSettingSecureService>();
			_contentPageSeoSetting = seoService.GetById(GetId());

			SeoTitleTextBox.Text = GetContentPageSeoSetting().Title;
			SeoDescriptionTextBox.Text = GetContentPageSeoSetting().Description;
			SeoKeywordsTextBox.Text = GetContentPageSeoSetting().Keywords;
		}

		protected bool ValidateFields()
		{
			var isValid = true;
			if (SeoTitleTextBox.Text.Length > MaxLength)
			{
				isValid = false;
				SaveMesageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.SeoTitleToLong, MaxLength), InfoType.Warning);
			}
			if (SeoDescriptionTextBox.Text.Length > MaxLength)
			{
				isValid = false;
				SaveMesageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.SeoDescriptionToLong, MaxLength), InfoType.Warning);
			}
			if (SeoKeywordsTextBox.Text.Length > MaxLength)
			{
				isValid = false;
				SaveMesageContainer.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.SeoKeywordsToLong, MaxLength), InfoType.Warning);
			}
			return isValid;
		}
	}
}
