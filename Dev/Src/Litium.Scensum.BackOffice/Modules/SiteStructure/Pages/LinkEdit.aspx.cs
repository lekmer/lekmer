using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Validator;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class LinkEdit : PageController,IEditor
	{
		protected virtual int? GetParentNodeId()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}

        protected virtual bool HasMessage()
        {
            bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
            return hasMessage.HasValue && hasMessage.Value;
        }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		    int? parentNodeId = GetParentNodeId();
            if ((parentNodeId == null || parentNodeId <= 0) && (GetIdOrNull() == null))
				Master.DenyTreeSelection();
		}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (HasMessage() && !messager.HasMessages)
            {
                messager.MessageType = InfoType.Success;
                messager.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);
            }
        }

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			ParentTreeViewControl.NodeCommand += OnParentTreeNodeCommand;
            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.SiteStructure.Page.GetDeleteUrl(GetId()));
        }

		protected override void PopulateForm()
		{
            ParentPanel.Visible = false;

            PopulateData();

            DeleteButton.Visible = GetIdOrNull().HasValue;
		}

	    protected void PopulateData()
	    {
	        if (GetIdOrNull() == null)
	        {
	            Master.Breadcrumbs.Clear();
	            Master.Breadcrumbs.Add(Resources.SiteStructure.Literal_CreateLink);
                
	            if (!GetParentNodeId().HasValue)
	            {
	                ParentPanel.Visible = true;
	                PopulateTreeView(Master.CurrentRootId);
	            }
	        }
	        else
	        {
	            IContentNodeUrlLink contentNodeUrlLink = IoC.Resolve<IContentNodeUrlLinkSecureService>().GetById(GetId());
	            TitleTextBox.Text = contentNodeUrlLink.Title;
	            CommonNameTextBox.Text = contentNodeUrlLink.CommonName;
	            LinkUrlTextBox.Text = contentNodeUrlLink.LinkUrl;
	            StatusDiv.Visible = false;
	            IAccess access = IoC.Resolve<IAccessSecureService>().GetById(contentNodeUrlLink.AccessId);
	            (access.CommonName == "All" ? AllRadioButton : access.CommonName == "SignedIn" ? SignedInRadioButton : NotSignedInRadioButton).Checked = true;
	        }
	    }

	    public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateForm())
			{
                messager.Add(Resources.SiteStructureMessage.FormValidationFailed);
				return;
			}

			IContentNodeUrlLink contentNodeUrlLink;

		    int? nodeId = GetIdOrNull();
            if (nodeId == null)
			{
			    int? parentNodeId = GetParentNodeId();
				contentNodeUrlLink = IoC.Resolve<IContentNodeUrlLinkSecureService>().Create();
                contentNodeUrlLink.ParentContentNodeId = parentNodeId.HasValue
                    ? (GetParentNodeId() < 0 ? null : parentNodeId)
					: (ParentTreeViewControl.SelectedNodeId < 0 ? null : ParentTreeViewControl.SelectedNodeId);
				contentNodeUrlLink.ContentNodeTypeId = (int)ContentNodeTypeInfo.UrlLink;
				contentNodeUrlLink.Ordinal = 0;
				contentNodeUrlLink.ContentNodeStatusId = OnLineRadioButton.Checked
													? (int)ContentNodeStatusInfo.Online
													: (int)ContentNodeStatusInfo.Offline;
				contentNodeUrlLink.SiteStructureRegistryId = contentNodeUrlLink.ParentContentNodeId.HasValue
					? IoC.Resolve<IContentNodeSecureService>().GetById(contentNodeUrlLink.ParentContentNodeId.Value).SiteStructureRegistryId
                    : -1 * (parentNodeId.HasValue ? parentNodeId.Value : ParentTreeViewControl.SelectedNodeId.Value);
			}
			else
			{
                contentNodeUrlLink = IoC.Resolve<IContentNodeUrlLinkSecureService>().GetById(nodeId.Value);
			}
			if (contentNodeUrlLink == null)
			{
                throw new BusinessObjectNotExistsException(nodeId.Value);
			}

			contentNodeUrlLink.Title = TitleTextBox.Text;
			contentNodeUrlLink.CommonName = CommonNameTextBox.Text;
			contentNodeUrlLink.LinkUrl = LinkUrlTextBox.Text;
			contentNodeUrlLink.AccessId = IoC.Resolve<IAccessSecureService>().GetByCommonName(AllRadioButton.Checked ? "All" : SignedInRadioButton.Checked ? "SignedIn" : "NotSignedIn").Id;

			var contentNodeUrlLinkSecureService = IoC.Resolve<IContentNodeUrlLinkSecureService>();
            if (!ContentNodeSaveValidator.IsCommonNameUnique(contentNodeUrlLink))
            {
                messager.Add(Resources.SiteStructureMessage.ContentNodeCommomNameExist);
                return;
            }

			int result = contentNodeUrlLinkSecureService.Save(SignInHelper.SignedInSystemUser, contentNodeUrlLink);
			
            Response.Redirect(PathHelper.SiteStructure.Page.GetLinkEditWithMessageUrl(result));
		}

		protected virtual void OnParentTreeNodeCommand(object sender, System.Web.UI.WebControls.CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			ParentTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			ParentTreeViewControl.DataBind();
		}

		protected void PopulateTreeView(int? id)
		{
			var nodeId = id ?? Master.CurrentRootId;
			ParentTreeViewControl.SelectedNodeId = nodeId;
			ParentTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			ParentTreeViewControl.DataBind();
			ParentTreeViewControl.PopulatePath(nodeId);
		}

		protected bool ValidateForm()
		{
			Page.Validate("vg");
			return Page.IsValid;
		}
	}
}
