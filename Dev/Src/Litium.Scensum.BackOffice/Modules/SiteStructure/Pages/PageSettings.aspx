<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="PageSettings.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.PageSettings" %>

<%@ Register Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" TagName="SiteStructureNodeSelector" TagPrefix="uc" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" TagName="ContentPageSelector" TagPrefix="uc" %>
<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
			<ContentTemplate>
						<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
					runat="server" />
				<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
					ID="ValidationSummary" DisplayMode="List" ValidationGroup="vg" />
			</ContentTemplate>
		</asp:UpdatePanel>


</asp:Content>
<asp:Content ID="PageCreateContent" ContentPlaceHolderID="SiteStructureForm" runat="server">

	<script type='text/javascript' language='javascript'>	
	    function SuggestAndValidateUrlTitle() {
	        jQuery.post('<%= ResolveUrl("~/Modules/SiteStructure/AjaxHandlers/UrlTitle.ashx") %>',
            {
                title: jQuery('#<%= TitleNavigationTextBox.ClientID %>')[0].value,
                urlTitle: jQuery('#<%= TitleUrlTextBox.ClientID %>')[0].value
            },
            function(urlTitle) {
                jQuery('#<%= TitleUrlTextBox.ClientID %>')[0].value = urlTitle;
            });
	    }

	    jQuery(document).ready(function() {
	        jQuery('#<%= TitleNavigationTextBox.ClientID %>').blur(SuggestAndValidateUrlTitle);
	        jQuery('#<%= TitleUrlTextBox.ClientID %>').blur(SuggestAndValidateUrlTitle);
	    });

	    function ChangeTemplateImage(i, c) {
	        document.getElementById(c).src = '../../../Modules/Interface/Templates/TemplateImageLoader.ashx?name=' + layouts[i];
	       };
	</script>
    
	<asp:Panel ID="PageSettingPanel" runat="server" DefaultButton="SaveButton">
	<asp:UpdatePanel ID="PageSettingUpdatePanel" runat="server" UpdateMode="Conditional"  >
		<ContentTemplate>
		<div class="page-settings">
		    <div class="content-box">
			    <div class="column">
				    <div class="input-box">
					    <span><%= Resources.SiteStructure.Literal_TitleNavigation %>&nbsp; *</span>
					    <asp:RequiredFieldValidator runat="server" ID="TitleNavigationValidator" ControlToValidate="TitleNavigationTextBox"  Display ="None"
							    ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" ValidationGroup="vg" />
						    <br />
						    <asp:TextBox ID="TitleNavigationTextBox" runat="server" />
				    </div>
				    <div class="input-box">
					    <span><%= Resources.SiteStructure.Literal_CommonNameNavigation %></span>						
					    <br />
					    <asp:TextBox ID="CommonNameTextBox" runat="server" />
				    </div>
				    <div class="input-box" runat="server" id="PageFromMasterDiv" visible="false">
					    <span><%= Resources.SiteStructure.Literal_MasterPage%>&nbsp; *</span>						
					    <br />
						<div style="width:300px">
							<uc:ContentPageSelector ID="MasterPageSelector" runat="server" AllowClearSelection="false" OnlyMasterPagesSelectable="true"/>
						</div>
				    </div>
				    <div class="input-box">
					    <span><%= Resources.SiteStructure.Literal_MasterTemplate %></span>
					    <br />
					    <asp:Label ID="MasterTemplateLabel" runat="server" Visible="false"></asp:Label>
					    <asp:DropDownList ID="MasterTemplateList" DataTextField="Title" DataValueField="Id" runat="server" />
				    </div> 				
				    <div class="input-box">
					    <span><%=Resources.SiteStructure.Literal_PageType %></span>
					    <br />
					    <asp:Label ID="TypeLabel" runat="server" Visible="false"></asp:Label>
					    <asp:DropDownList ID="TypeList" runat="server" />
				    </div>
				    <div class="input-box">
				        <span><%=Resources.SiteStructure.Literal_PageTemplate %></span>
					    <br />
					    <asp:Label ID="LayoutLabel" runat="server" Visible="false"></asp:Label>
					    <asp:DropDownList ID="LayoutList" runat="server" />
				    </div>
				    <div id="page-layout-column">
					    <div class="input-box">						   
							<asp:Image runat="server" ID="TemplateImage" Style="max-width: 150px; max-height: 150px;" />						   
					    </div>
				    </div>
			    </div>
    			
			    <div class="column">
					<div runat="server" id="NotMasterDiv" style="padding-bottom:20px;">
						<div class="input-box">
							<span><%=Resources.SiteStructure.Literal_PageTitle %></span>
							<br />
							<asp:TextBox ID="tbTitle" runat="server" />
						</div>
						<div class="input-box">
							<span><%=Resources.SiteStructure.Literal_URLTitle %>&nbsp; *</span>
							<asp:RequiredFieldValidator runat="server" ID="PageTitleValidator" ControlToValidate="TitleUrlTextBox" Display="None"
							    ErrorMessage="<%$ Resources:SiteStructureMessage, UrlTitleEmpty %>" ValidationGroup="vg" />
							<br />
							<asp:TextBox ID="TitleUrlTextBox" runat="server" />
						</div>
						<div class="input-box">					
								<div id="StatusDiv" runat="server">
									<Scensum:GroupBox ID="NodeStatusGroupBox" runat="server" Text="<%$ Resources:General, Literal_Status %>" Width="221">
										<asp:RadioButton ID="OnLineRadioButton" CssClass="margin-left-px" runat="server" GroupName="Status" /><%= Resources.SiteStructure.Literal_Online%>
										<asp:RadioButton ID="OffLineRadioButton" CssClass="margin-left-px" runat="server" GroupName="Status" Checked="true" /><%= Resources.SiteStructure.Literal_Offline%>
										<asp:RadioButton ID="HiddenRadioButton" CssClass="margin-left-px" runat="server" GroupName="Status" /><%= Resources.SiteStructure.Literal_Hidden%>
									</Scensum:GroupBox>
								</div>
								<div id="AccessDiv" runat="server">
									<Scensum:GroupBox ID="AccessGroupBox" runat="server" Text="<%$ Resources:SiteStructure,Literal_Access %>" Width="221">
										<asp:RadioButton ID="AllRadioButton" runat="server" GroupName="Access" Checked="true" /><%= Resources.SiteStructure.Literal_All%>
										<asp:RadioButton ID="SignedInRadioButton" runat="server" GroupName="Access" /><%= Resources.SiteStructure.Literal_SignedIn%>
										<asp:RadioButton ID="NotSignedInRadioButton" runat="server" GroupName="Access" /><%= Resources.SiteStructure.Literal_NotSignedIn %>
									</Scensum:GroupBox>
								</div>
						</div>
						<br class="clear" />
				    </div>
	    			<div class="input-box">				
	                    <div style="margin-top:-1px;">
		                    <asp:Panel ID="ParentPanel" runat="server">
			                    <label>
				                   <%= Resources.SiteStructure.Literal_SelectPlacement %></label><asp:Label runat="server" ID="PlasmentasterixLabel" ForeColor="Red"
					                    Visible="false">&nbsp*</asp:Label>
			                    <div id="link-placement-tree" class="site-structure-placement-tree">						
							<uc:SiteStructureNodeSelector ID="PlacementTreeViewControl" runat="server" AllowClearSelection="false" />						
						</div>
		                    </asp:Panel>
	                    </div>
                    </div>
			    </div>
		</div>
	</div>
		<div id="page-create">

		</div>
		<br class="clear" />
		<div class="buttons left">
                    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmationContentNode();" />
                </div>		
				<div class="buttons right">
					<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>"
						SkinID="DefaultButton" ValidationGroup="vg" />
					<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel%>"
						SkinID="DefaultButton" CausesValidation="false" />
				</div>
		</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>

