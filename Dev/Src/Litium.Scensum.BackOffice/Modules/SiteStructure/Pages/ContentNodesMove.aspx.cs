using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	///<summary>
	/// Use for content node moving.
	///</summary>
	public partial class ContentNodesMove : PageController,IEditor 
	{
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			DestinationTreeViewControl.NodeCommand += OnDestinationTreeNodeCommand;
		}

		protected virtual void OnDestinationTreeNodeCommand(object sender, Web.Controls.Tree.TemplatedTree.TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
		}

		protected override void PopulateForm()
		{
			var master = Master as Pages;
			if (master == null) return;

			var node = IoC.Resolve<IContentNodeSecureService>().GetById(GetId());
			PopulateTree(node.ParentContentNodeId ?? -1 * node.SiteStructureRegistryId);

			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add("Move");
		}
		
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}
		
		public virtual void OnSave(object sender, EventArgs e)
		{
			var nodeService = IoC.Resolve<IContentNodeSecureService>();
			nodeService.UpdateParent(
                GetId(), 
                DestinationTreeViewControl.SelectedNodeId < 0 ? null : DestinationTreeViewControl.SelectedNodeId);
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl(GetId()));
		}

		protected virtual void PopulateTree(int id)
		{
			var master = Master as Pages;
			if (master == null) return;

			DestinationTreeViewControl.DataSource = GetTreeViewDataSource(id);
			DestinationTreeViewControl.DataBind();
			DestinationTreeViewControl.SelectedNodeId = id;
		}

		protected IEnumerable<ISiteStructureNode> GetTreeViewDataSource(int id)
		{
			var master = Master as Pages;
			if (master == null) return null;

			var dataSource = master.GetTreeNodes(id, false);
			var filteredSource = DestinationTreeViewControl.GetFilteredSource(dataSource, GetId());
			var shortcutLinks = IoC.Resolve<IContentNodeShortcutLinkSecureService>().GetAllByLinkContentNode(GetId());
			foreach (var link in shortcutLinks)
			{
				if (filteredSource.FirstOrDefault(n => n.Id == link.Id) != null)
				{
					filteredSource = DestinationTreeViewControl.GetFilteredSource(filteredSource, link.Id);
				}
			}
			return filteredSource;
		}
	}
}
