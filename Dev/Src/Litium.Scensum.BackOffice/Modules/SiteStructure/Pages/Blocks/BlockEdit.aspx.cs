using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks
{
	public partial class BlockEdit : PageController, IEditor
	{
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}
		protected override void PopulateForm()
		{
			PopulateData();
			PopulateTranslation(GetBlockId());
			
			var master = Master as Pages;
			if (master == null) return;
			var block = IoC.Resolve<IBlockSecureService>().GetById(GetBlockId());
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(string.Format(CultureInfo.CurrentCulture, Resources.SiteStructureMessage.EditBlock, block.BlockType.Title.ToLower(CultureInfo.CurrentCulture)));
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			var block = IoC.Resolve<IBlockSecureService>().GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(block.ContentNodeId));
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			var service = IoC.Resolve<IBlockSecureService>();
			var block = service.GetById(GetBlockId());
			if (block == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}
			int templateId;
			block.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			block.Title = BlockTitleTextBox.Text;

			service.Save(SignInHelper.SignedInSystemUser, block);
			if (block.Id == -1)
			{
				errors.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				errors.Add(Resources.GeneralMessage.SaveSuccessBlock);
				errors.MessageType = InfoType.Success;
			}
		}
		protected void PopulateData()
		{
			var block = IoC.Resolve<IBlockSecureService>().GetById(GetBlockId());
			BlockTitleTextBox.Text = block.Title;

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("Block" + block.BlockType.CommonName);
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			ListItem item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
				item.Selected = true;
		}
		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}