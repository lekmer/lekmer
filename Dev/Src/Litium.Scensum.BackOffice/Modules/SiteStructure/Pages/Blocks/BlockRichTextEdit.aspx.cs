using System;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks
{
	public partial class BlockRichTextEdit : PageController, IEditor
	{
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
		}
		protected override void PopulateForm()
		{
			PopulateData();
			PopulateTranslation(GetBlockId());

			var master = Master as Pages;
			if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.SiteStructureMessage.BlockRichTextEdit);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockRichTextSecureService = IoC.Resolve<IBlockRichTextSecureService>();
			IBlockRichText blockRichText = blockRichTextSecureService.GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockRichText.ContentNodeId));
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockRichTextSecureService = IoC.Resolve<IBlockRichTextSecureService>();
			IBlockRichText blockRichText = blockRichTextSecureService.GetById(GetBlockId());
			if (blockRichText == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}
			blockRichText.Content = string.IsNullOrEmpty(BodyEditor.Value) ? null : BodyEditor.Value;
			int templateId;
			blockRichText.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockRichText.Title = BlockTitleTextBox.Text;

			blockRichText.Id = blockRichTextSecureService.Save(SignInHelper.SignedInSystemUser, blockRichText);
			if (blockRichText.Id == -1)
			{
				errors.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				errors.Add(Resources.GeneralMessage.SaveSuccessBlockRichText);
				errors.MessageType = InfoType.Success;
			}
		}

		protected void PopulateData()
		{
			var block = IoC.Resolve<IBlockRichTextSecureService>().GetById(GetBlockId());
			BlockTitleTextBox.Text = block.Title;
			BodyEditor.Value = block.Content ?? string.Empty;

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockRichText");
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			ListItem item = TemplateList.Items.FindByValue(block.TemplateId.ToString());
			if (item != null)
				item.Selected = true;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
	}
}
