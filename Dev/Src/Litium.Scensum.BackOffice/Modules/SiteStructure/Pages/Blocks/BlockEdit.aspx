﻿<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"CodeBehind="BlockEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks.BlockEdit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

	<uc:MessageContainer ID="errors" MessageType="Failure" HideMessagesControlId="SaveButton"
			runat="server" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"	ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgBlock"/>


</asp:Content>
<asp:Content ID="BlockEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
<asp:Panel ID="BlockEditPanel" runat="server" DefaultButton="SaveButton">
	<br clear="all" />
	<div class="content-box">
		<span><%= Resources.General.Literal_Title %></span>&nbsp
		<uc:GenericTranslator ID="Translator" runat="server" />&nbsp
		<asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox"
						ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgBlock" /><br />
		<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server"></asp:TextBox>
		<br />
		<div class="block-setting-container">
			<span class="bold"><%= Resources.General.Label_Settings %></span>
			<br />
			<div class="block-setting-content">
				<span><%= Resources.General.Literal_ChooseTemplate %></span><br />
				<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" Width="194px" />
			</div>
		</div>
		</div>
		<br clear="all" />
		<br clear="all" />
		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" runat="server" ID="SaveButton"  ValidationGroup="vgBlock" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
			<uc:ImageLinkButton  UseSubmitBehaviour="true" runat="server" ID="CancelButton" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
		</div>
		
	</asp:Panel>
</asp:Content>
