<%@ Page Language="C#" ValidateRequest="false" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="BlockRichTextEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks.BlockRichTextEdit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="errors"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
			<uc:ScensumValidationSummary  ForeColor= "Black" runat="server" CssClass="advance-validation-summary " ID="ValidationSummary" DisplayMode="List"  ValidationGroup="vgBlockRichText" />
</asp:Content>
<asp:Content ID="BlockRichTextEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
    <script>
        function submitForm() {
            tinyMCE.triggerSave();
           }
   </script>
   
	<asp:Panel ID="BlockRichTextEditPanel" runat="server" DefaultButton="SaveButton">
            <div class="pages-full-content">
	            <div class="input-box">
		            <span><%= Resources.General.Literal_Title %></span>&nbsp
		            <uc:GenericTranslator ID="Translator" runat="server" />&nbsp
		            <asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgBlockRichText"  />
		            <br />	
		            <asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" ></asp:TextBox>
	            </div>			
	            <div class="content-box">
		            <div class="column">
			            <div class="input-box" >
				            <span><%= Resources.SiteStructure.Literal_Content %></span><br />
				            <uc:TinyMceEditor  ID="BodyEditor" runat="server" SkinID="tinyMCE" />
			            </div>
			            <br />
			            <div class="block-setting-container">
				            <span class="bold"><%= Resources.General.Label_Settings %></span>
				            <br />
				            <div class="block-setting-content">
					            <span><%= Resources.General.Literal_ChooseTemplate %></span><br />
					            <asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
				            </div>
			            </div>
		            </div>
	            </div>
            </div>
            <div id="rich-text-action-buttons">
				<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>" OnClientClick="submitForm();" SkinID="DefaultButton" ValidationGroup="vgBlockRichText"/>
				<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
			</div>
				</asp:Panel>
	<script>
		 tinyMCE.settings['setup'] =
		 function(ed)
			{
				ed.onEvent.add(function(ed, e) {increaseMCE(ed, e);	});
 			}
		 
		 function increaseMCE(ed, e) {
 			var inviolableMin = 120;
 			var contentHeight = ed.getBody().scrollHeight;
 			if (contentHeight > inviolableMin) {
 				ed.resizeToContent();
 			}
 			else {
 				ed.theme.resizeTo(ed.getBody().offsetWidth + 2, inviolableMin + 55)
 			}
		 }      
	</script>
</asp:Content>