using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class SetStartPage : PageController,IEditor
	{
		protected override int GetId()
		{
			return -1 * base.GetId();
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			ISiteStructureRegistry reg = IoC.Resolve<ISiteStructureRegistrySecureService>().GetById(GetId());
			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Add(string.Format(CultureInfo.CurrentCulture, "[ {0} ]", reg.Title));
				master.Breadcrumbs.Add(Resources.SiteStructure.Literal_SetStartPage);
			}
			ConfigureTree(reg);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			ISiteStructureRegistry reg = IoC.Resolve<ISiteStructureRegistrySecureService>().GetById(GetId());
			reg.StartContentNodeId = ContentPageSelectorControl.SelectedNodeId;
			IoC.Resolve<ISiteStructureRegistrySecureService>().Save(SignInHelper.SignedInSystemUser, reg);
			messager.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);
		}

		private void ConfigureTree(ISiteStructureRegistry reg)
		{
			ContentPageSelectorControl.SelectedNodeId = reg.StartContentNodeId;
			ContentPageSelectorControl.SiteStructureRegistryId = GetId();
			if (reg.StartContentNodeId.HasValue)
			{
				ContentPageSelectorControl.SetContentPageTitle();
			}
		}
	}
}
