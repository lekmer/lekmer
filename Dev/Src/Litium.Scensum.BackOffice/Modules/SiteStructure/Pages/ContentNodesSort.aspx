<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"  CodeBehind="ContentNodesSort.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.ContentNodesSort" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">

		<asp:UpdatePanel ID="MessagesContainer" runat="server">
			<ContentTemplate>
				<uc:MessageContainer ID="errors"  MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
			<uc:MessageContainer ID="success"  MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />	
			</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
			
<asp:Content ID="ContentNodesSort" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="NodesSortPanel" runat="server" DefaultButton="SaveButton">
	<asp:UpdatePanel ID="SortNodesUpdatePanel" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<div class="pages-full-content">
				<div id="sort-content">
					<asp:GridView 
						ID="NodeGrid" 
						SkinID="grid" 
						runat="server"  
						AutoGenerateColumns="false"
						Width="100%">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="85%">
								<ItemTemplate>
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									<asp:HiddenField ID="TypeIdHiddenField" Value='<%#Eval("ContentNodeTypeId") %>' runat="server" />
									<asp:HiddenField ID="StatusIdHiddenField" Value='<%#Eval("ContentNodeStatusId") %>' runat="server" />
									<asp:Image ID="TypeImage" runat="server" />
									<Scensum:LiteralEncoded ID="TitleLiteral" runat="server" Text='<%#Eval("Title") %>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
								<HeaderTemplate>
									<div class="inline">
										<%= Resources.General.Literal_SortOrder %>
										<asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshOrdinal" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
									</div>
								</HeaderTemplate>
								<ItemTemplate>
									<div class="inline">
										<asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox" Text="*" ValidationGroup="vgOrdinals" />
										<asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals" />
										<asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%#Eval("Ordinal") %>' />
										<asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" ValidationGroup="vgOrdinals" />
										<asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" ValidationGroup="vgOrdinals" />
									</div>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</div>
			</div>
			<div class="buttons right">
				<uc:ImageLinkButton ID="SaveButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgOrdinals" SkinID="DefaultButton"/>
				
				<uc:ImageLinkButton ID="CancelButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General, Button_Cancel %>" CausesValidation="false" SkinID="DefaultButton" />
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>