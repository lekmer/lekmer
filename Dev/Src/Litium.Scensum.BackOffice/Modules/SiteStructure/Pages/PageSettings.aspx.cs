using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Validator;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using ITemplate = Litium.Scensum.Template.ITemplate;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class PageSettings : PageController, IEditor
	{
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetParentNodeId()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsMaster()
		{
			if (!GetIdOrNull().HasValue)
				return Request.QueryString.GetBooleanOrNull("isMaster") ?? false;
			return IoC.Resolve<IContentPageSecureService>().GetById(GetId()).IsMaster;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsFromMaster()
		{
			if (!GetIdOrNull().HasValue)
				return Request.QueryString.GetBooleanOrNull("fromMaster") ?? false;
			return IoC.Resolve<IContentPageSecureService>().GetById(GetId()).MasterPageId.HasValue;

		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			int? parentNodeId = GetParentNodeId();
			if (((parentNodeId == null || parentNodeId <= 0)) && !GetIdOrNull().HasValue)
				Master.DenyTreeSelection();
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			Master.StatusChanged += MasterStatusChanged;
			PlacementTreeViewControl.NodeCommand += OnParentTreeNodeCommand;
			MasterPageSelector.OnSelectedPageChanged += OnMasterChanged;
            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.SiteStructure.Page.GetDeleteUrl(GetId()));
        }

		protected override void PopulateForm()
		{
			PopulateData();

			Master.Breadcrumbs.Clear();
			if (GetIdOrNull().HasValue)
			{
				Master.Breadcrumbs.Add(Resources.SiteStructure.Literal_Settings);
			}
			else if (GetIsMaster())
			{
                Master.Breadcrumbs.Add(Resources.SiteStructure.Literal_CreateMasterPage);
			}
            else if (GetIsFromMaster())
            {
                Master.Breadcrumbs.Add(Resources.SiteStructure.Literal_CreatePageFromMaster);
            }
            else
            {
                Master.Breadcrumbs.Add(Resources.SiteStructure.Literal_CreatePage);
            }

            DeleteButton.Visible = GetIdOrNull().HasValue;
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateForm())
			{
				messager.Add(Resources.SiteStructureMessage.FormValidationFailed);
				return;
			}

			int? pageId = GetIdOrNull();
			if (GetIsFromMaster() && !pageId.HasValue && !MasterPageSelector.SelectedNodeId.HasValue)
			{
				messager.Add(Resources.SiteStructureMessage.NoneMasterSelected, InfoType.Failure);
				return;
			}

			IContentPage contentPage = GetContentPage(pageId);
			if (!ContentNodeSaveValidator.IsCommonNameUnique(contentPage))
			{
				messager.Add(Resources.SiteStructureMessage.ContentNodeCommomNameExist);
				return;
			}

			var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			int result = contentPageSecureService.Save(SignInHelper.SignedInSystemUser, contentPage);

			if (result < 0)
			{
				SetErrors(result);
			}
			else
			{
				if (pageId.HasValue)
				{
					Master.PopulateTreeView(pageId);
					messager.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);
					messager.MessageType = InfoType.Success;
				}
				else
				{
					Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrlWithMessage(result));
				}
			}
		}

		protected virtual void OnParentTreeNodeCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			PlacementTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			PlacementTreeViewControl.DataBind();
		}

		private IContentPage GetContentPage(int? pageId)
		{
			IContentPage contentPage;
			var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			int? parentNodeId = GetParentNodeId();
			if (pageId.HasValue)
			{
				contentPage = contentPageSecureService.GetById(pageId.Value);
				if (contentPage == null)
				{
					throw new BusinessObjectNotExistsException(GetId());
				}
			}
			else
			{
				contentPage = contentPageSecureService.Create();
				contentPage.Id = -1;

				if (parentNodeId.HasValue)
				{
					if (parentNodeId < 0)
					{
						contentPage.ParentContentNodeId = null;
						contentPage.SiteStructureRegistryId = parentNodeId.Value * -1;
					}
					else
					{
						contentPage.ParentContentNodeId = parentNodeId;
						contentPage.SiteStructureRegistryId = GetRegistryIdByContentNodeId(contentPage.ParentContentNodeId.Value);
					}
				}
				else
				{
					if (PlacementTreeViewControl.SelectedNodeId < 0)
					{
						contentPage.ParentContentNodeId = null;
						contentPage.SiteStructureRegistryId = PlacementTreeViewControl.SelectedNodeId.Value * -1;
					}
					else
					{
						contentPage.ParentContentNodeId = PlacementTreeViewControl.SelectedNodeId;
						contentPage.SiteStructureRegistryId = GetRegistryIdByContentNodeId(contentPage.ParentContentNodeId.Value);
					}
				}

				contentPage.ContentNodeTypeId = (int)ContentNodeTypeInfo.Page;

				if (GetIsFromMaster())
				{
					contentPage.MasterPageId = MasterPageSelector.SelectedNodeId;
					var masterPage = contentPageSecureService.GetById(contentPage.MasterPageId.Value);
					contentPage.ContentPageTypeId = masterPage.ContentPageTypeId;
					contentPage.TemplateId = masterPage.TemplateId;
					contentPage.MasterTemplateId = masterPage.MasterTemplateId;
				}
				else
				{
					contentPage.TemplateId = int.Parse(LayoutList.SelectedValue, CultureInfo.CurrentCulture);
					contentPage.ContentPageTypeId = int.Parse(TypeList.SelectedValue, CultureInfo.CurrentCulture);
				}
			}

			contentPage.Title = TitleNavigationTextBox.Text;
			contentPage.PageTitle = tbTitle.Text;
			contentPage.CommonName = CommonNameTextBox.Text;
			contentPage.UrlTitle = TitleUrlTextBox.Text;
			contentPage.Ordinal = 0;

			if (!GetIsFromMaster())
			{
				int masterTemplateId;
				contentPage.MasterTemplateId = int.TryParse(MasterTemplateList.SelectedValue, out masterTemplateId)
												? (int?)masterTemplateId
												: null;
			}

			if (GetIsMaster())
			{
				contentPage.IsMaster = true;
				contentPage.PageTitle = contentPage.UrlTitle = Guid.NewGuid().ToString();

				contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Online;
				contentPage.AccessId = IoC.Resolve<IAccessSecureService>().GetByCommonName("All").Id;
			}
			else
			{
				if (OnLineRadioButton.Checked)
					contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Online;
				else if (OffLineRadioButton.Checked)
					contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Offline;
				else if (HiddenRadioButton.Checked)
					contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Hidden;
				contentPage.AccessId = IoC.Resolve<IAccessSecureService>().GetByCommonName(AllRadioButton.Checked ? "All" : SignedInRadioButton.Checked ? "SignedIn" : "NotSignedIn").Id;
			}
			return contentPage;
		}
		private static int GetRegistryIdByContentNodeId(int contentNodeId)
		{
			return IoC.Resolve<IContentNodeSecureService>().GetById(contentNodeId).SiteStructureRegistryId;
		}

		protected void SetErrors(int errorCode)
		{
			var contentPageErrors = (ContentPageErrors)(Math.Abs(errorCode));
			if ((contentPageErrors & ContentPageErrors.SameUrlTitle) == ContentPageErrors.SameUrlTitle)
			{
				messager.Add(Resources.SiteStructureMessage.UrlTitleExists);
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			int? pageId = GetIdOrNull();

			if (pageId.HasValue)
			{
				IContentPage contentPage = IoC.Resolve<IContentPageSecureService>().GetById(pageId.Value);
				if (contentPage == null)
				{
					throw new BusinessObjectNotExistsException(GetId());
				}
				PopulateCheckBoxes(contentPage);
			}
			base.OnPreRender(e);
		}

		protected void PopulateTreeView(int? id)
		{
			var nodeId = id ?? Master.CurrentRootId;
			PlacementTreeViewControl.SelectedNodeId = nodeId;
			PlacementTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			PlacementTreeViewControl.DataBind();
			PlacementTreeViewControl.PopulatePath(nodeId);
		}

		protected void PopulateData()
		{
			var contentPageTypeSecureService = IoC.Resolve<IContentPageTypeSecureService>();
			Collection<IContentPageType> contentPageTypes = contentPageTypeSecureService.GetAll();

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			MasterTemplateList.DataSource = templateSecureService.GetAllByModel("Master");
			MasterTemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			MasterTemplateList.Items.Insert(0, listItem);

			Collection<ITemplate> templates = templateSecureService.GetAllByModel("ContentPage");

			int? pageId = GetIdOrNull();
			ParentPanel.Visible = false;
			NotMasterDiv.Style.Add("display", GetIsMaster() ? "none" : "block");

			if (pageId.HasValue)
			{
				IContentPage contentPage = IoC.Resolve<IContentPageSecureService>().GetById(pageId.Value);
				if (contentPage == null)
				{
					throw new BusinessObjectNotExistsException(pageId.Value);
				}
				ITemplate template = templates.First(item => item.Id == contentPage.TemplateId);
				IContentPageType type = contentPageTypes.First(item => item.Id == contentPage.ContentPageTypeId);
				PopulatePageSettings(contentPage, type, template);
			}
			else
			{
				PopulateCreatePage(templates, contentPageTypes);
			}
		}

		private void PopulateCreatePage(Collection<ITemplate> templates, Collection<IContentPageType> contentPageTypes)
		{
			if (!GetParentNodeId().HasValue)
			{
				ParentPanel.Visible = true;
				PopulateTreeView(null);
			}

			if (GetIsFromMaster())
			{
				TypeList.Visible = LayoutList.Visible = MasterTemplateList.Visible = false;
				TypeLabel.Visible = LayoutLabel.Visible = MasterTemplateLabel.Visible = true;
				TemplateImage.Style.Add("display", "none");
				PageFromMasterDiv.Visible = true;
				var registryId = !GetParentNodeId().HasValue 
					? -1 * Master.CurrentRootId 
					: GetParentNodeId() < 0
				                 	? -1 * GetParentNodeId().Value
				                 	: IoC.Resolve<IContentNodeSecureService>().GetById(GetParentNodeId().Value).SiteStructureRegistryId;
				if (registryId.HasValue)
					MasterPageSelector.SiteStructureRegistryId = registryId.Value;

				return;
			}

			TypeList.DataSource = contentPageTypes;
			TypeList.DataValueField = "Id";
			TypeList.DataTextField = "Title";
			TypeList.DataBind();

			LayoutList.DataSource = templates;
			LayoutList.DataValueField = "Id";
			LayoutList.DataTextField = "Title";
			LayoutList.DataBind();

			LayoutList.SelectedValue = templates[0].Id.ToString(CultureInfo.CurrentCulture);

			RegisterLayoutScript(templates);

			LayoutList.Attributes.Add("onchange", "ChangeTemplateImage(this.value,'" + TemplateImage.ClientID + "')");

			TemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(templates[0].ImageFileName);
		}

		private void PopulatePageSettings(IContentPage contentPage, IContentPageType type, ITemplate template)
		{
			tbTitle.Text = contentPage.PageTitle;
			TitleUrlTextBox.Text = contentPage.UrlTitle;
			TitleNavigationTextBox.Text = contentPage.Title;
			CommonNameTextBox.Text = contentPage.CommonName;

			TypeLabel.Text = type.Title;
			TypeLabel.Visible = true;
			TypeList.Visible = false;

			LayoutLabel.Text = template.Title;
			LayoutLabel.Visible = true;
			LayoutList.Visible = false;

			if (GetIsFromMaster())
			{
				MasterTemplateList.Visible = false;
				MasterTemplateLabel.Visible = true;
				MasterTemplateLabel.Text = contentPage.MasterTemplateId.HasValue
					? IoC.Resolve<ITemplateSecureService>().GetById(contentPage.MasterTemplateId.Value).Title
					: "-";
			}
			else
			{
				ListItem itemMaster = MasterTemplateList.Items.FindByValue(contentPage.MasterTemplateId.ToString());
				if (itemMaster != null)
				{
					itemMaster.Selected = true;
				}
			}

			TemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(template.ImageFileName);

			PopulateCheckBoxes(contentPage);
		}

		private void PopulateCheckBoxes(IContentPage contentPage)
		{
			AllRadioButton.Checked = false;
			SignedInRadioButton.Checked = false;
			NotSignedInRadioButton.Checked = false;
			IAccess access = IoC.Resolve<IAccessSecureService>().GetById(contentPage.AccessId);
			(access.CommonName == "All" ? AllRadioButton
				: access.CommonName == "SignedIn" ? SignedInRadioButton
					: NotSignedInRadioButton).Checked = true;

			OnLineRadioButton.Checked = false;
			OffLineRadioButton.Checked = false;
			HiddenRadioButton.Checked = false;

			(contentPage.ContentNodeStatusId == (int)ContentNodeStatusInfo.Online ? OnLineRadioButton
				: contentPage.ContentNodeStatusId == (int)ContentNodeStatusInfo.Offline ? OffLineRadioButton
					: HiddenRadioButton).Checked = true;
		}

		protected void RegisterLayoutScript(Collection<ITemplate> templates)
		{
			var scriptArray = new StringBuilder();
			scriptArray.Append(string.Format(CultureInfo.CurrentCulture, "var layouts = new Array({0});", templates.Count));
			for (int i = 0; i < templates.Count; ++i)
			{
				scriptArray.Append(string.Format(CultureInfo.CurrentCulture, "layouts[{0}]='{1}';", templates[i].Id, templates[i].ImageFileName));
			}

			ScriptManager.RegisterClientScriptBlock(this, GetType(), "layoutArray", scriptArray.ToString(), true);
		}
		protected bool ValidateForm()
		{
			Page.Validate("vg");
			return Page.IsValid;
		}

		private void MasterStatusChanged(object sender, EventArgs e)
		{
			PageSettingUpdatePanel.Update();
		}

		protected virtual void OnMasterChanged(object sender, CommandEventArgs e)
		{
			var masterPageId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			var masterPage = IoC.Resolve<IContentPageSecureService>().GetById(masterPageId);
			if (masterPage == null) return;

			var templateService = IoC.Resolve<ITemplateSecureService>();
			var template = templateService.GetById(masterPage.TemplateId);
			LayoutLabel.Text = template.Title;
			TemplateImage.Style.Remove("display");
			TemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(template.ImageFileName);
			MasterTemplateLabel.Text = masterPage.MasterTemplateId.HasValue
				? templateService.GetById(masterPage.MasterTemplateId.Value).Title
				: string.Empty;

			var type =
				IoC.Resolve<IContentPageTypeSecureService>().GetAll().FirstOrDefault(t => t.Id == masterPage.ContentPageTypeId);
			TypeLabel.Text = type != null ? type.Title : string.Empty;

			PageSettingUpdatePanel.Update();
		}
	}
}
