using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Common;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class ContentNodesSort : PageController,IEditor 
	{
		private const int BlockMoveUpStep = -15;
		private const int BlockMoveDownStep = 15;
        
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			NodeGrid.RowDataBound += NodeGrid_RowDataBound;
			NodeGrid.RowCommand += NodeGrid_RowCommand;
		}
		protected override void PopulateForm()
		{
			PopulateData();

			var master = Master as Pages;
			if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add("Sort");
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			Collection<IContentNode> nodes = Sort(GetNodesFromGrid(), 1);
			var contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
			contentNodeSecureService.ChangeOrder(SignInHelper.SignedInSystemUser, nodes);
			PopulateGrid(nodes);
			((Pages)Master).PopulateTreeView(GetIdOrNull());
			success.Add(Resources.SiteStructureMessage.CompletedAndTreeUpdated);
		}

		protected virtual void NodeGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var node = (IContentNode)row.DataItem;
			var imgType = (Image)e.Row.FindControl("TypeImage");
			imgType.ImageUrl = GetImageUrl(node);
		}
		protected virtual void NodeGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			int nodeId;
			if (!int.TryParse(e.CommandArgument.ToString(), out nodeId))
			{
				nodeId = 0;
			}

			switch (e.CommandName)
			{
				case "UpOrdinal":
					MoveBlock(nodeId, BlockMoveUpStep);
					break;
				case "DownOrdinal":
					MoveBlock(nodeId, BlockMoveDownStep);
					break;
				case "RefreshOrdinal":
					RefreshOrder();
					break;
			}
		}

		protected virtual string GetImageUrl(IContentNode node)
		{
			var url = new StringBuilder("~/Media/Images/SiteStructure/");
			switch (node.ContentNodeTypeId)
			{
				case (int)ContentNodeTypeInfo.ShortcutLink:
					url.Append("shortcut");
					break;
				case (int)ContentNodeTypeInfo.UrlLink:
					url.Append("link");
					break;
				case (int)ContentNodeTypeInfo.Page:
					url.Append(GetPageImageUrl(node.Id));
					break;
				case (int)ContentNodeTypeInfo.Folder:
					url.Append("folder");
					break;
			}
			switch (node.ContentNodeStatusId)
			{
				case (int)ContentNodeStatusInfo.Online:
					break;
				case (int)ContentNodeStatusInfo.Offline:
					url.Append("-off");
					break;
				case (int)ContentNodeStatusInfo.Hidden:
					url.Append("-hid");
					break;
			}
			return url.Append(".png").ToString();
		}
		protected virtual string GetPageImageUrl(int id)
		{
			var page = IoC.Resolve<IContentPageSecureService>().GetById(id);
			return page.IsMaster ? "master" : page.MasterPageId.HasValue ? "sub-page" : "page";
		}

		protected void MoveBlock(int elementId, int step)
		{
			Collection<IContentNode> nodes = GetNodesFromGrid();
			nodes.FirstOrDefault(n => n.Id == elementId).Ordinal += step;
			PopulateGrid(nodes);
		}
		protected void RefreshOrder()
		{
			Collection<IContentNode> nodes = GetNodesFromGrid();
			PopulateGrid(nodes);
		}
		protected void PopulateGrid(IEnumerable<IContentNode> nodes)
		{
			NodeGrid.DataSource = Sort(nodes, 10);
			NodeGrid.DataBind();
		}
		protected void PopulateData()
		{
			var contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
			int? parentId = GetIdOrNull() == null ? null : (int?)GetId();
			if (parentId == null) return;

			IEnumerable<IContentNode> nodes = parentId > 0 
                ? contentNodeSecureService.GetAllByParent(parentId) 
                : contentNodeSecureService.GetAllByRegistry(
                    (int)parentId * (-1)).Where(cn => !cn.ParentContentNodeId.HasValue);
			PopulateGrid(nodes);
		}
		protected bool ValidateOrdinals()
		{
			Page.Validate("vgOrdinals");
			return Page.IsValid;
		}
		protected static Collection<IContentNode> Sort(IEnumerable<IContentNode> list, int ordinalsStep)
		{
			var nodes = new List<IContentNode>(list);
			nodes.Sort(delegate(IContentNode element1, IContentNode element2)
			{
				if (element1.Ordinal < element2.Ordinal)
					return -1;
				if (element1.Ordinal > element2.Ordinal)
					return 1;
				return 0;
			});
			for (int i = 0; i < list.Count(); i++)
			{
				nodes[i].Ordinal = (i + 1) * ordinalsStep;
			}
			return new Collection<IContentNode>(nodes);
		}
		protected Collection<IContentNode> GetNodesFromGrid()
		{
			if (!ValidateOrdinals())
			{
				errors.Add(Resources.SiteStructureMessage.OrdinalsValidationFailed);
				return null;
			}
			var newList = new Collection<IContentNode>();
			var scnService = IoC.Resolve<IContentNodeSecureService>();
			foreach (GridViewRow row in NodeGrid.Rows)
			{
				IContentNode contentNode = scnService.Create();
				contentNode.Id = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				contentNode.ContentNodeTypeId = int.Parse(((HiddenField)row.FindControl("TypeIdHiddenField")).Value, CultureInfo.CurrentCulture);
				contentNode.ContentNodeStatusId = int.Parse(((HiddenField)row.FindControl("StatusIdHiddenField")).Value, CultureInfo.CurrentCulture);
				contentNode.Title = ((LiteralEncoded)row.FindControl("TitleLiteral")).Text;
				contentNode.Ordinal = int.Parse(((TextBox)row.FindControl("OrdinalTextBox")).Text, CultureInfo.CurrentCulture);
				newList.Add(contentNode);
			}
			return newList;
		}
	}
}
