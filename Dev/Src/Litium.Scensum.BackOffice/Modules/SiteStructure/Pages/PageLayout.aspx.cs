﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using ITemplate = Litium.Scensum.Template.ITemplate;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class PageLayout : StatePageController<Collection<IContentAreaFull>>, IEditor
	{
		private bool? _isMaster;
		private bool? _isFromMaster;
		private Collection<IContentArea> _destinationContentAreas;
		protected const int NotSelectedItemValue = 0;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsFromMaster()
		{
			if (!_isFromMaster.HasValue)
			{
				_isFromMaster = IoC.Resolve<IContentPageSecureService>().GetById(GetId()).MasterPageId.HasValue;
			}
			return _isFromMaster.Value;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsMaster()
		{
			if (!_isMaster.HasValue)
			{
				_isMaster = IoC.Resolve<IContentPageSecureService>().GetById(GetId()).IsMaster;
			}
			return _isMaster.Value;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsTypeChanged()
		{
			return Request.QueryString.GetBooleanOrNull("ChangeType") ?? false;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual bool GetIsMasterUsed()
		{
			return (GetIsFromMaster() && !GetIsTypeChanged()) || (!GetIsFromMaster() && GetIsTypeChanged());
		}

		protected override void SetEventHandlers()
		{
			AreasGrid.RowDataBound += AreasRowDataBound;
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			AvailableTemplatesList.SelectedIndexChanged += AvailableTemplatesSelectedIndexChanged;
			MasterPageSelector.OnSelectedPageChanged += OnMasterPageChanged;
			SubPagesGrid.PageIndexChanging += SubPagesGridOnPageIndexChanging;
		}

		protected void SubPagesGridOnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			SubPagesGrid.PageIndex = e.NewPageIndex;
			PopulateSubPages();
		}

		protected override void PopulateForm()
		{
			var contentPage = IoC.Resolve<IContentPageSecureService>().GetById(GetId());
            PopulateInfo(contentPage);
			PopulateNewChoiceVariants(contentPage);
            PopulateBreadcrumbs();
			PopulateAreas(contentPage);
		}

		protected virtual void PopulateInfo(IContentPage contentPage)
		{
			int templateId;
			if (GetIsFromMaster())
			{
				CurrentLabel.Text = Resources.SiteStructure.Literal_CurrentMasterPage;
				NewLabel.Text = GetIsTypeChanged() 
					? Resources.SiteStructure.Literal_NewPageTemplate
					: Resources.SiteStructure.Literal_NewMasterPage;

				var master = IoC.Resolve<IContentPageSecureService>().GetById(contentPage.MasterPageId.Value);
				TemplateTitleLabel.Text = master.Title;
				templateId = master.TemplateId;
			}
			else
			{
				CurrentLabel.Text = Resources.SiteStructure.Literal_CurrentPageTemplate;
				NewLabel.Text = GetIsTypeChanged()
					? Resources.SiteStructure.Literal_NewMasterPage
					: Resources.SiteStructure.Literal_NewPageTemplate;

				var template = IoC.Resolve<ITemplateSecureService>().GetById(contentPage.TemplateId);
				TemplateTitleLabel.Text = template.Title;
				templateId = template.Id;
			}
			TemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(templateId);

			if (GetIsMaster())
			{
				PopulateSubPages();
			}
		}

		protected virtual void PopulateNewChoiceVariants(IContentPage contentPage)
		{
			if (GetIsMasterUsed())
			{
				PopulateAvailableMasterPages(contentPage);
				AvailableTemplatesList.Visible = false;
			}
			else
			{
				PopulateAvailableTemplates(contentPage);
				MasterPageSelector.Visible = false;
			}
		}

		protected virtual void PopulateAvailableTemplates(IContentPage contentPage)
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			Collection<ITemplate> templates = templateSecureService.GetAllByModel("ContentPage");

			AvailableTemplatesList.DataSource = templates;
			AvailableTemplatesList.DataValueField = "Id";
			AvailableTemplatesList.DataTextField = "Title";
			AvailableTemplatesList.DataBind();
			var current = AvailableTemplatesList.Items.FindByValue(contentPage.TemplateId.ToString(CultureInfo.CurrentCulture));
			if (current != null)
			{
				current.Selected = true;
				NewTemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(contentPage.TemplateId);
			}
			else
			{
				NewTemplateImage.Style[HtmlTextWriterStyle.Display] = "none";
			}
		}

		protected virtual void PopulateAvailableMasterPages(IContentPage contentPage)
		{
			MasterPageSelector.SiteStructureRegistryId = contentPage.SiteStructureRegistryId;
			MasterPageSelector.SelectedNodeId = contentPage.MasterPageId;
			MasterPageSelector.SetContentPageTitle();
			if (MasterPageSelector.SelectedNodeId.HasValue)
			{
				var master = IoC.Resolve<IContentPageSecureService>().GetById(MasterPageSelector.SelectedNodeId.Value);
				NewTemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(master.TemplateId);
			}
			else
			{
				NewTemplateImage.Style[HtmlTextWriterStyle.Display] = "none";
			}
		}

		protected void PopulateBreadcrumbs()
		{
			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(GetIsMasterUsed() 
					? Resources.SiteStructure.Literal_ChangeMasterPage 
					: Resources.SiteStructure.Literal_ChangePageTemplate);
			}
		}

		protected void PopulateAreas(IContentPage contentPage)
		{
			Collection<IContentAreaFull> areas = IoC.Resolve<IContentAreaSecureService>().GetAllFullByTemplate(contentPage.TemplateId, contentPage.Id);
			State = areas;
			AreasGrid.DataSource = areas;
			AreasGrid.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected Collection<IContentArea> GetDestinationContentAreas()
		{
			if (_destinationContentAreas == null)
			{
				var id = GetNewTemplateId();
				if (id != NotSelectedItemValue)
				{
					var contentAreaSecureService = IoC.Resolve<IContentAreaSecureService>();
					_destinationContentAreas = contentAreaSecureService.GetAllByTemplate(id);
				}
			}
			return _destinationContentAreas;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetNewTemplateId()
		{
			int templateId;
			if (GetIsMasterUsed())
			{
				if (!MasterPageSelector.SelectedNodeId.HasValue) return NotSelectedItemValue;

				var masterId = MasterPageSelector.SelectedNodeId.Value;
				var master = IoC.Resolve<IContentPageSecureService>().GetById(masterId);
				templateId = master.TemplateId;
			}
			else
			{
				templateId = int.Parse(AvailableTemplatesList.SelectedValue, CultureInfo.CurrentCulture);
			}
			return templateId;
		}

		protected void AvailableTemplatesSelectedIndexChanged(object sender, EventArgs e)
		{
			var id = int.Parse(AvailableTemplatesList.SelectedValue, CultureInfo.CurrentCulture);
			if (id != NotSelectedItemValue)
			{
				ITemplate template = IoC.Resolve<ITemplateSecureService>().GetById(id);
				NewTemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(template.ImageFileName);
				NewTemplateImage.Style[HtmlTextWriterStyle.Display] = "block";
			}
			else
			{
				NewTemplateImage.Style[HtmlTextWriterStyle.Display] = "none";
			}

			var contentPage = IoC.Resolve<IContentPageSecureService>().GetById(GetId());
			PopulateAreas(contentPage);
		}

		protected virtual void OnMasterPageChanged(object sender, CommandEventArgs e)
		{
			var masterId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			var master = IoC.Resolve<IContentPageSecureService>().GetById(masterId);
			if (master == null) return;

			ITemplate template = IoC.Resolve<ITemplateSecureService>().GetById(master.TemplateId);
			NewTemplateImage.ImageUrl = PathHelper.Template.GetTemplateImageLoaderUrl(template.ImageFileName);
			NewTemplateImage.Style[HtmlTextWriterStyle.Display] = "block";

			var contentPage = IoC.Resolve<IContentPageSecureService>().GetById(GetId());
			PopulateAreas(contentPage);
		}
		
		protected void AreasRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var destinationAreas = (DropDownList)e.Row.FindControl("DestinationAreasList");

			int blocksCount = ((IContentAreaFull)row.DataItem).Blocks.Count;
			if (blocksCount > 0 || GetIsMaster())
			{
				destinationAreas.DataSource = GetDestinationContentAreas();
				destinationAreas.DataValueField = "Id";
				destinationAreas.DataTextField = "Title";
				destinationAreas.DataBind();
				destinationAreas.Items.Insert(0, new ListItem(string.Empty, NotSelectedItemValue.ToString(CultureInfo.CurrentCulture)));
			}
			else
			{
				destinationAreas.Enabled = false;
				e.Row.Enabled = false;
			}
		}

		public void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}
		
		public void OnSave(object sender, EventArgs e)
		{
			IContentPage contentPage = GetContentPage();
			Collection<IBlock> blocks = GetBlocks(State);

			if (contentPage.IsMaster)
			{
				ChangeLayoutForMaster(contentPage, blocks);
			}
			else
			{
				var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
				contentPageSecureService.ChangeLayout(SignInHelper.SignedInSystemUser, contentPage, blocks);
			}
			
			Messages.Add(Resources.SiteStructureMessage.PageLayoutChangedSuccessfully);
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrlWithMessage(contentPage.Id));
		}

		protected virtual void ChangeLayoutForMaster(IContentPage masterPage, Collection<IBlock> masterBlocks)
		{
			var service = IoC.Resolve<IContentPageSecureService>();
			var pageBlocksDictionary = new Dictionary<IContentPage, Collection<IBlock>>();
			var subPages = service.GetAllByMaster(GetId());
			foreach (var page in subPages)
			{
				var areas = IoC.Resolve<IContentAreaSecureService>().GetAllFullByTemplate(page.TemplateId, page.Id);
				page.TemplateId = GetNewTemplateId();
				pageBlocksDictionary.Add(page, GetBlocks(areas));
			}
			service.ChangeLayoutForMaster(SignInHelper.SignedInSystemUser, masterPage, masterBlocks, pageBlocksDictionary);
		}

		private IContentPage GetContentPage()
		{
			int id = GetId();
			var contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
			IContentPage contentPage = contentPageSecureService.GetById(id);
			if (contentPage == null)
			{
				throw new BusinessObjectNotExistsException(id);
			}

			contentPage.TemplateId = GetNewTemplateId();
			contentPage.MasterPageId = GetIsMasterUsed() 
				? MasterPageSelector.SelectedNodeId
				: null;
			return contentPage;
		}

		private Collection<IBlock> GetBlocks(IEnumerable<IContentAreaFull> originalAreas)
		{
			Dictionary<int, IEnumerable<IBlock>> areaMapper = CreateAreaMapper(originalAreas);
			return UpdateBlocks(areaMapper);
		}
		
		private Dictionary<int, IEnumerable<IBlock>> CreateAreaMapper(IEnumerable<IContentAreaFull> originalAreas)
		{
			var areaMapper = new Dictionary<int, IEnumerable<IBlock>>();
		    foreach (GridViewRow row in AreasGrid.Rows)
			{
				var destinationAreas = (DropDownList)row.FindControl("DestinationAreasList");
				if (!destinationAreas.Enabled)
				{
					continue;
				}

				int destinationAreaId = int.Parse(destinationAreas.SelectedValue, CultureInfo.CurrentCulture);

				var postedOriginAreaId = (HiddenField)row.FindControl("OriginAreaIdHiddenField");
				int originAreaId = int.Parse(postedOriginAreaId.Value, CultureInfo.CurrentCulture);
				IContentAreaFull originArea = originalAreas.First(item => item.Id == originAreaId);
				Collection<IBlock> blocks = originArea.Blocks;

				if (areaMapper.ContainsKey(destinationAreaId))
				{
					areaMapper[destinationAreaId] = areaMapper[destinationAreaId].Concat(blocks);
				}
				else
				{
					areaMapper.Add(destinationAreaId, blocks);
				}
			}
			return areaMapper;
		}
		
		private static Collection<IBlock> UpdateBlocks(Dictionary<int, IEnumerable<IBlock>> areaMapper)
		{
			var allBlocks = new Collection<IBlock>();
			foreach (KeyValuePair<int, IEnumerable<IBlock>> keyValuePair in areaMapper)
			{
				int contentAreaId = keyValuePair.Key;
				IEnumerable<IBlock> blocks = keyValuePair.Value;

				int ordinal = 1;
				foreach (IBlock block in blocks)
				{
					block.Ordinal = ordinal++;
					block.ContentAreaId = contentAreaId;
					allBlocks.Add(block);
				}
			}
			return allBlocks;
		}

		protected virtual void PopulateSubPages()
		{
			var subPages = IoC.Resolve<IContentPageSecureService>().GetAllByMaster(GetId());
			SubPagesGrid.DataSource = subPages;
			SubPagesGrid.DataBind();
			SubPagesDiv.Visible = true;
		}

		protected virtual string GetImageUrl(object id)
		{
			var statusId = System.Convert.ToInt32(id, CultureInfo.CurrentCulture);
			var url = new StringBuilder("~/Media/Images/SiteStructure/sub-page");
			switch (statusId)
			{
				case (int)ContentNodeStatusInfo.Online:
					break;
				case (int)ContentNodeStatusInfo.Offline:
					url.Append("-off");
					break;
				case (int)ContentNodeStatusInfo.Hidden:
					url.Append("-hid");
					break;
			}
			return url.Append(".png").ToString();
		}

		protected virtual string GetEditUrl(object id)
		{
			var pageId = System.Convert.ToInt32(id, CultureInfo.CurrentCulture);
			return PathHelper.SiteStructure.Page.GetPageEditUrl(pageId);
		}

		protected virtual string GetPath(object id)
		{
			const string separator = @"\";
			var pageId = System.Convert.ToInt32(id, CultureInfo.CurrentCulture);
			var service = IoC.Resolve<IContentNodeSecureService>();
			var node = service.GetById(pageId);
			var path = new StringBuilder(node.Title);
			while (node.ParentContentNodeId.HasValue)
			{
				node = service.GetById(node.ParentContentNodeId.Value);
				path.Insert(0, node.Title + separator);
			}
			path.Insert(0, separator);
			return path.ToString();
		}
	}
}
