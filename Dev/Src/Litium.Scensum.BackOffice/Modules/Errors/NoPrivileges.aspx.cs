﻿using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.Errors
{
	public partial class NoPrivileges : PageController
	{
		protected override void SetEventHandlers() { }
		protected override void PopulateForm() { }
	}
}
