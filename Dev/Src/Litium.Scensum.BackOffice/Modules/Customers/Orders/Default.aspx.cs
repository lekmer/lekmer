using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.Modules.Customers.Orders
{
	public partial class Default : PageController
	{
		protected override void SetEventHandlers()
		{
			SearchButton.Click += BtnSearch_Click;
			SearchResultsGrid.PageIndexChanging += OrderGridPageIndexChanging;
		}
		protected override void PopulateForm()
		{
			RestoreSearchData();
		}
		protected virtual void BtnSearch_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid) return;
			PopulateSearch();
			ResultsDiv.Visible = true;
		}
		protected virtual void OrderGridPageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
		{
			SearchResultsGrid.PageIndex = e.NewPageIndex;
		}

		public virtual void RestoreSearchData()
		{
			RestoreSearchCriteria();
			SearchResultsGrid.PageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
			PopulateSearch();
			ResultsDiv.Visible = true;
		}

		protected void PopulateSearch()
		{
			OrderObjectDataSource.SelectParameters.Clear();
			OrderObjectDataSource.SelectParameters.Add("maximumRows", SearchResultsGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			OrderObjectDataSource.SelectParameters.Add("startRowIndex", (SearchResultsGrid.PageIndex * SearchResultsGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			OrderObjectDataSource.SelectParameters.Add("customerFirstName", CustomerFirstNameTextBox.Text.Length != 0 ? CustomerFirstNameTextBox.Text : null);
			OrderObjectDataSource.SelectParameters.Add("customerLastName", CustomerLastNameTextBox.Text.Length != 0 ? CustomerLastNameTextBox.Text : null);
			OrderObjectDataSource.SelectParameters.Add("customerEmail", EmailTextBox.Text.Length != 0 ? EmailTextBox.Text : null);
			OrderObjectDataSource.SelectParameters.Add("socialSecurityNumber", CustomerCivicNumberTextBox.Text.Length != 0 ? CustomerCivicNumberTextBox.Text : null);
			SearchResultsGrid.DataBind();
		}
		protected void RestoreSearchCriteria()
		{
			ICustomerSearchCriteria customerSearchCriteria = SearchCriteriaState<ICustomerSearchCriteria>.Instance.Get(PathHelper.Order.GetDefaultUrl()) ?? IoC.Resolve<ICustomerSearchCriteria>();
			CustomerFirstNameTextBox.Text = customerSearchCriteria.FirstName;
			CustomerLastNameTextBox.Text = customerSearchCriteria.LastName;
			CustomerCivicNumberTextBox.Text = customerSearchCriteria.Number;
			EmailTextBox.Text = customerSearchCriteria.Email;
		}
	}
}