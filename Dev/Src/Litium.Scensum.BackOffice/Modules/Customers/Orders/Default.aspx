<%@ Import Namespace="Litium.Scensum.BackOffice" %>
<%@ Import Namespace="System.Collections.Generic" %>

<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Customers.Orders.Default" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="OrdersTabContent" ContentPlaceHolderID="body" runat="server">
	<asp:Panel ID="DefaultPanel" runat="server" DefaultButton="SearchButton">
		<Scensum:ToolBoxPanel ID="VariationPanelToolBoxPanel" runat="server" Text="<%$ Resources:Customer, Literal_OrderToolBoxPanel %>" ShowSeparator="false">
		</Scensum:ToolBoxPanel> 
		<div style="float: left; width:980px; margin-bottom:-20px">
        <uc:ScensumValidationSummary runat="server" ID="ValidationSummary" DisplayMode="BulletList"
								HeaderText="<%$ Resources:CustomerMessage, OrderSearchErrors %>" ValidationGroup="vgSearhOrder" /> </div>

		<div id="search-block" style="float: left ">
		
				<span class="order-header"><%= Resources.General.Literal_Search %></span>
			</div>
			<asp:UpdatePanel ID="SearchUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<div id="order-search-content" class="customer-content-box customer-caption">
						<label><%= Resources.Customer.Literal_CustomerInformation %></label>
						<br />
						<div class="left">
							<div class="column">
								<div class="input-box">
									<span><%= Resources.Customer.Literal_FirstName %></span><br />
									<asp:TextBox ID="CustomerFirstNameTextBox" runat="server" />
								</div>
							</div>
							<div class="column">
								<div class="input-box">
									<span><%= Resources.Customer.Literal_LastName %></span><br />
									<asp:TextBox ID="CustomerLastNameTextBox" runat="server" />
								</div>
							</div>
							<div class="column">
								<div class="input-box">
									<span><%= Resources.Customer.Literal_CivicNumber %></span>
									<asp:RegularExpressionValidator runat="server" ID="CustomerCivicNumberValidator" ControlToValidate="CustomerCivicNumberTextBox"
										ErrorMessage="<%$ Resources:CustomerMessage, CivicNumberRequirement %>" Text=" *"
										ValidationExpression="\d{6}-\d{4}" ValidationGroup="vgSearhOrder" />
									<br />
									<asp:TextBox ID="CustomerCivicNumberTextBox" runat="server" />
								</div>
							</div>
							<div class="column">
								<div class="input-box">
									<span><%= Resources.Customer.Literal_Email %></span>
									<asp:RegularExpressionValidator runat="server" ID="EmailValidator" ControlToValidate="EmailTextBox"
										ErrorMessage="<%$ Resources:CustomerMessage, EmailRequirement %>" Text=" *" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
										ValidationGroup="vgSearhOrder" />
									<br />
									<asp:TextBox ID="EmailTextBox" runat="server" />
								</div>
							</div>
						</div>
						<div class="right">
							<br />
							<div id="order-search-button" class="buttons right">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SearchButton" Text="<%$ Resources:General, Button_Search %>" runat="server"
									ValidationGroup="vgSearhOrder" SkinID="DefaultButton" />
							</div>
						</div>
					</div>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="SearchButton" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
			<br class="clear" />
			<br />			
			<div class="order-horizontal-separator"></div>
			<label class="order-header" style="float: left;padding-bottom: 14px;padding-top: 14px;">
				<%= Resources.General.Literal_SearchResults %></label>
			<div id = "order-search-result">
				<asp:UpdatePanel ID="SearchResultUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div id="ResultsDiv" runat="server" visible="false">
							<Scensum:Grid runat="server" ID="ArticlesGrid" FixedColumns="Order number|Customer">
								<sc:GridViewWithCustomPager ID="SearchResultsGrid" SkinID="grid" runat="server" AllowPaging="true"
									PageSize="<%$AppSettings:DefaultGridPageSize%>" AutoGenerateColumns="false" DataSourceID="OrderObjectDataSource"
									Width="100%">
									<Columns>
										<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_OrderNumber %>" ItemStyle-Width="40%">
											<ItemTemplate>
												<uc:HyperLinkEncoded runat="server" ID="OrderNumberLink" NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Order.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>'
													Text='<%# Eval("Number")%>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_Email %>" DataField="Email" ItemStyle-Width="40%" />
										<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>" DataField="CreatedDate" ItemStyle-Width="20%" />
									</Columns>
								</sc:GridViewWithCustomPager>
							</Scensum:Grid>
							<asp:ObjectDataSource ID="OrderObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount"
								SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Customers.Orders.OrderDataSource" />
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="SearchButton" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
