﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups
{
	public partial class Default : PageController, IEditor
	{
		private Collection<ICustomerGroupStatus> _customerGroupStatuses;
		private Collection<ICustomerGroupFolder> _allFolders;

		protected virtual CustomerGroupsMaster MasterPage
		{
			get
			{
				return (CustomerGroupsMaster)Master;
			}
		}

		protected override void SetEventHandlers()
		{
			GroupGrid.RowCommand += OnRowCommand;
			GroupGrid.RowDataBound += OnRowDataBound;
			GroupGrid.PageIndexChanging += OnPageIndexChanging;
			SetStatusButton.Click += OnSave;
			DeleteGroupButton.Click += OnDelete;
		}

		protected override void PopulateForm()
		{
			if(IsSearchResult)
			{
				MasterPage.SelectedFolderId = null;
			}

			PopulateCustomerGroups();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
		}

		public virtual void OnDelete(object sender, EventArgs e)
		{
			IEnumerable<int> ids = GroupGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				Messager.Add(Resources.CustomerMessage.NoCustomerGroupSelected);
				Messager.MessageType = InfoType.Warning;
				return;
			}
			var service = IoC.Resolve<ICustomerGroupSecureService>();
			service.Delete(SignInHelper.SignedInSystemUser, ids);
			
			Messager.Add(Resources.GeneralMessage.DeleteSeccessful);
			Messager.MessageType = InfoType.Success;

			PopulateCustomerGroups();
			MasterPage.PopulateTree(null);
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(StatusList.SelectedValue))
			{
				Messager.Add(Resources.GeneralMessage.ChooseStatusValue);
				return;
			}
			IEnumerable<int> ids = GroupGrid.GetSelectedIds();
			if (new List<int>(ids).Count <= 0)
			{
				Messager.Add(Resources.CustomerMessage.NoCustomerGroupSelected);
				Messager.MessageType = InfoType.Warning;
				return;
			}
			int statusId = int.Parse(StatusList.SelectedValue, CultureInfo.CurrentCulture);
			var service = IoC.Resolve<ICustomerGroupSecureService>();
			service.SetStatus(SignInHelper.SignedInSystemUser, ids, statusId);
			
			Messager.Add(Resources.GeneralMessage.StatusesChangedSuccessfully);
			Messager.MessageType = InfoType.Success;

			PopulateCustomerGroups();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DeleteGroup")
			{
				var service = IoC.Resolve<ICustomerGroupSecureService>();
				var groupId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				service.Delete(SignInHelper.SignedInSystemUser, groupId);

				PopulateCustomerGroups();
				MasterPage.PopulateTree(null);
			}
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GroupGrid.PageIndex = e.NewPageIndex;
			PopulateCustomerGroups();
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected virtual void PopulateCustomerGroups()
		{
			GroupGrid.DataSource = IsSearchResult ? SearchCustomerGroups() : RetrieveCustomerGroups();
			GroupGrid.DataBind();
			GroupGrid.Columns[4].Visible = IsSearchResult;

			bool resultExists = AllSelectedDiv.Visible = GroupGrid.Rows.Count > 0;
			if (resultExists)
			{
				PopulateStatuses();
			}
		}

		protected virtual Collection<ICustomerGroup> RetrieveCustomerGroups()
		{
			var folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId
			               	? MasterPage.SelectedFolderId
			               	: null;
			return IoC.Resolve<ICustomerGroupSecureService>().GetAllByFolder(folderId);
		}

		protected virtual Collection<ICustomerGroup> SearchCustomerGroups()
		{
			string searchCriteria = SearchCriteriaState<string>.Instance.Get(PathHelper.Customer.CustomerGroup.GetSearchResultUrl());
			return !string.IsNullOrEmpty(searchCriteria)
				? IoC.Resolve<ICustomerGroupSecureService>().Search(searchCriteria)
				: null;	
		}
        
		protected virtual void PopulateStatuses()
		{
			StatusList.DataSource = _customerGroupStatuses ?? IoC.Resolve<ICustomerGroupStatusSecureService>().GetAll();
			StatusList.DataBind();
		}

		protected virtual string GetCustomerGropStatus(object statusId)
		{
			if (_customerGroupStatuses == null)
				_customerGroupStatuses = IoC.Resolve<ICustomerGroupStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _customerGroupStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual string GetEditUrl(object groupId)
		{
			int id = System.Convert.ToInt32(groupId, CultureInfo.CurrentCulture);
			return IsSearchResult
		       	? PathHelper.Customer.CustomerGroup.GetEditUrlForSearch(id)
		       	: PathHelper.Customer.CustomerGroup.GetEditUrlForDefault(id);
		}
        
		protected virtual string GetPath(object folderId)
		{
			var folders = GetAllFolders();
			var folder = folders.FirstOrDefault(f => f.Id == System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
			if (folder == null) return string.Empty;

			var stack = new Stack<string>();
			stack.Push(folder.Title);
			while (folder.ParentId.HasValue)
			{
				var child = folder;
				folder = folders.FirstOrDefault(f => f.Id == child.ParentId.Value);
				stack.Push(folder.Title);
			}
			var sb = new StringBuilder();
			while (stack.Count > 0)
			{
				sb.Append(stack.Pop());
				sb.Append(@"\");
			}
			return sb.ToString();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IEnumerable<ICustomerGroupFolder> GetAllFolders()
		{
			return _allFolders ?? (_allFolders = IoC.Resolve<ICustomerGroupFolderSecureService>().GetAll());
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"'); ShowBulkUpdatePanel('" + selectAllCheckBox.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", 
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" + cbSelect.ID + "', '" + grid.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}
	}
}
