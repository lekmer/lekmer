﻿using System;
using System.Globalization;
using System.Linq;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups
{
	public partial class FolderEdit : PageController, IEditor
	{
		protected virtual CustomerGroupsMaster MasterPage
		{
			get
			{
				return (CustomerGroupsMaster)Master;
			}
		}

		protected virtual bool? HasMessage()
		{
			return Request.QueryString.GetBooleanOrNull("HasMessage");
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			FolderTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateForm()
		{
			if (GetIdOrNull() != null)
			{
				var folder = IoC.Resolve<ICustomerGroupFolderSecureService>().GetById(GetId());
				FolderTitleTextBox.Text = folder.Title;
				MasterPage.BreadcrumbAppend.Add(Resources.General.Literal_EditFolder);
				PopulateTreeView(null);
			}
			else
			{
				MasterPage.BreadcrumbAppend.Add(Resources.General.Literal_CreateFolder);
				DestinationDiv.Visible = false;
			}
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (HasMessage().HasValue && HasMessage().Value && !SystemMessageContainer.HasMessages)
			{
				SystemMessageContainer.MessageType = InfoType.Success;
				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCustomerGroupFolder);
			}
			System.Web.UI.ScriptManager.RegisterStartupScript(this, GetType(), string.Format(CultureInfo.CurrentCulture, "root menu {0}", FolderTree.ClientID), string.Format(CultureInfo.CurrentCulture, "HideRootExpander('{0}');", FolderTree.ClientID), true);
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Customer.CustomerGroup.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var service = IoC.Resolve<ICustomerGroupFolderSecureService>();
			var folder = GetIdOrNull() != null ? service.GetById(GetId()) : service.Create();
			folder.Title = FolderTitleTextBox.Text;
			if (GetIdOrNull() == null)
			{
				folder.ParentId = GetParentIdOrNull() != MasterPage.RootNodeId ? GetParentIdOrNull() : null;
			}
			else if (FolderTree.SelectedNodeId.HasValue)
			{
				folder.ParentId = FolderTree.SelectedNodeId.Value == FolderTree.RootNodeId ? null : FolderTree.SelectedNodeId;
			}
			folder.Id = service.Save(SignInHelper.SignedInSystemUser, folder);
			if (folder.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.FolderTitleExist);
			}
			else
			{
				MasterPage.PopulateTree(null);
				MasterPage.SelectedFolderId = folder.Id;
				Response.Redirect(PathHelper.Customer.CustomerGroup.GetFolderEditWithMessageUrl(folder.Id));
			}
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected void PopulateTreeView(int? folderId)
		{
			var folders = MasterPage.GetNodes(folderId ?? MasterPage.SelectedFolderId);
			if (folders == null || folders.Count <= 0) return;
			var editedFolder = folders.FirstOrDefault(f => f.Id == MasterPage.SelectedFolderId);

			FolderTree.DataSource = editedFolder != null
				? FolderTree.GetFilteredSource(folders, editedFolder.Id)
				: folders;
			FolderTree.RootNodeTitle = Resources.Customer.Label_CustomerGroups;
			FolderTree.DataBind();
			FolderTree.SelectedNodeId = folderId ?? (editedFolder != null ? editedFolder.ParentId : FolderTree.RootNodeId) ?? FolderTree.RootNodeId;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetParentIdOrNull()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}
	}
}