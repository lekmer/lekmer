using System;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using Convert=Litium.Scensum.Foundation.Convert;

namespace Litium.Scensum.BackOffice.Modules.Customers.Blocks
{
	public partial class BlockSignInEdit : PageController,IEditor 
	{
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
		}
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			BlockTitleTextBox.Text = IoC.Resolve<IBlockSignInSecureService>().GetById(GetBlockId()).Title;

		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			SiteStructureTree.NodeCommand += OnSiteStructureTreeNodeCommand;
		}
        protected virtual void OnSiteStructureTreeNodeCommand(object sender, Web.Controls.Tree.TemplatedTree.TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}
        protected override void PopulateForm()
		{
			PopulateData();
			PopulateTranslation(GetBlockId());
            
			var master = Master as Pages;
			if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.Customer.Literal_EditSignInBlock);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockSignIn blockSignIn = IoC.Resolve<IBlockSignInSecureService>().GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockSignIn.ContentNodeId));
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockSignIn blockSignIn = IoC.Resolve<IBlockSignInSecureService>().GetById(GetBlockId());
			if (blockSignIn == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}
			int templateId;
			blockSignIn.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockSignIn.RedirectContentNodeId = SiteStructureTree.SelectedNodeId <= 0 ? null: SiteStructureTree.SelectedNodeId ;
			blockSignIn.Title = BlockTitleTextBox.Text;

			var service = IoC.Resolve<IBlockSignInSecureService>();
			service.Save(SignInHelper.SignedInSystemUser, blockSignIn);
			if (blockSignIn.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockSignIn);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		protected void PopulateData()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockSignIn");
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);

			IBlockSignIn blockSignIn = IoC.Resolve<IBlockSignInSecureService>().GetById(GetBlockId());
			ListItem item = TemplateList.Items.FindByValue(blockSignIn.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}

			IContentNode node = IoC.Resolve<IContentNodeSecureService>().GetById(blockSignIn.ContentNodeId);
			PopulateTreeView(blockSignIn.RedirectContentNodeId ?? -1 * node.SiteStructureRegistryId);
		}

		protected void PopulateTreeView(int id)
		{
			var master = Master as Pages;
			if (master == null) return;

			SiteStructureTree.SelectedNodeId = id;
			SiteStructureTree.DataSource = master.GetTreeNodes(id, true);
			SiteStructureTree.DataBind();
			SiteStructureTree.DenySelection = SiteStructureTree.SelectedNodeId < 0;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}
