using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Customer.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch
{
	public partial class Default : PageController
    {
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			if (!IsPostBack) return;
			if (CustomersSearchGrid.Rows.Count != 0) return;
			CustomersSearchGrid.DataBind();
		}
		protected override void SetEventHandlers()
		{
			CustomersSearchGrid.PageIndexChanging += CustomerGridPageIndexChanging;
			CustomerSearchCriteriaControl.SearchEvent += CustomerSearchControl_SearchEvent;
		}
		protected override void PopulateForm()
		{
			RestoreSearchData();
		}
		protected virtual void CustomerGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CustomersSearchGrid.PageIndex = e.NewPageIndex;
		}
		protected virtual void CustomerSearchControl_SearchEvent(object sender, CustomerSearchCriteriaEventArgs e)
		{
			CustomersSearchGrid.PageIndex = e.PageIndex != null ? (int)e.PageIndex : 0;
			SearchCustomers(CustomerObjectDataSource, CustomersSearchGrid, e.SearchCriteria);
			CustomersSearchGrid.DataBind();
			SearchCriteriaState<ICustomerSearchCriteria>.Instance.Save(e.SearchCriteria);
		}

		protected void RestoreSearchData()
		{
			ICustomerSearchCriteria customerSearchCriteria = SearchCriteriaState<ICustomerSearchCriteria>.Instance.Get() ??
															 IoC.Resolve<ICustomerSearchCriteria>();
			CustomerSearchCriteriaControl.RestoreSearchFields(customerSearchCriteria);
			int pageIndex = PageInfoHelper.GetGridPageIndex() ?? 0;
			CustomerSearchControl_SearchEvent(this, new CustomerSearchCriteriaEventArgs{ SearchCriteria = customerSearchCriteria, PageIndex = pageIndex });
		}

		public static void SearchCustomers(ObjectDataSource ods, GridView gv, ICustomerSearchCriteria searchCriteria)
		{
			ods.SelectParameters.Clear();
			ods.SelectParameters.Add("maximumRows", gv.PageSize.ToString(CultureInfo.CurrentCulture));
			ods.SelectParameters.Add("startRowIndex", (gv.PageIndex * gv.PageSize).ToString(CultureInfo.CurrentCulture));
			ods.SelectParameters.Add("erpId", searchCriteria.ErpId);
			ods.SelectParameters.Add("firstName", searchCriteria.FirstName);
			ods.SelectParameters.Add("lastName", searchCriteria.LastName);
			ods.SelectParameters.Add("civicNumber", searchCriteria.Number);
			ods.SelectParameters.Add("address", searchCriteria.Address);
			ods.SelectParameters.Add("zip", searchCriteria.Zip);
			ods.SelectParameters.Add("city", searchCriteria.City);
			ods.SelectParameters.Add("email", searchCriteria.Email);
			ods.SelectParameters.Add("phoneHome", searchCriteria.PhoneHome);
			ods.SelectParameters.Add("phoneMobile", searchCriteria.PhoneMobile);
			ods.SelectParameters.Add("createdFrom", searchCriteria.CreatedFrom);
			ods.SelectParameters.Add("createdTo", searchCriteria.CreatedTo);
			ods.SelectParameters.Add("customerStatusId", searchCriteria.CustomerStatusId);
		}
    }
}