using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.BackOffice.UserControls.Customer;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Web.Controls.Common;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch
{
	public partial class CustomerEdit : PageController, IEditor
	{
		private Collection<ICustomerGroupStatus> _customerGroupStatuses;

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			OrdersGrid.RowDataBound += OrderGridRowDataBound;
			OrdersGrid.PageIndexChanging += OrderGridPageIndexChanging;
			CustomerAddresses.Changed += CustomerAddresses_Change;
			CustomerAddresses.CannotDo += CustomerAddresses_CannotDo;
			CustomerCommentControl.CannotAddComment += CustomerCommentControl_CannotAddComment;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private string GetOrderddress(IOrderFull order)
		{
			return order.DeliveryAddress.StreetAddress += ", " + order.DeliveryAddress.StreetAddress2;
		}

		private void ReBind()
		{
			CustomerUserControl.REBind();
		}

		protected void CustomerAddresses_CannotDo(object sender, CustomerCannotDoEventArgs e)
		{
			switch (e.CustomerCannot)
			{
				case CustomerCannotDoWhat.Add:
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotAddAddress);
					break;
				case CustomerCannotDoWhat.Edit:
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditAddress);
					break;
				case CustomerCannotDoWhat.Delete:
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotDeleteAddress);
					break;
			}
		}

		protected void CustomerCommentControl_CannotAddComment(object sender, EventArgs e)
		{
			SystemMessageContainer.Add(Resources.CustomerMessage.CannotAddComment);
		}

		protected void CustomerAddresses_Change(object sender, EventArgs e)
		{
			ICustomer customer = IoC.Resolve<ICustomerSecureService>().GetById(GetId());
			CustomerInformation.BindAddresses(customer, CustomerAddresses.GetAddresses().Where(item => !item.IsDeleted));
		}

		protected override void PopulateForm()
		{
			ICustomer customer = IoC.Resolve<ICustomerSecureService>().GetById(GetId());
			PopulateCustomerInfo(customer);
			PopulateOrderGrid(customer);
			PopulateCustomerGroups();
			PopulateCustomerAddresses(customer);
			PopulateCustomerInformation(customer);
			PopulateCustomerComments();
			PopulateCustomerUser(customer);
		}
		protected void PopulateCustomerUser(ICustomer customer)
		{
			CustomerUserControl.Customer = customer;
		}
		protected void PopulateCustomerInformation(ICustomer customer)
		{
			CustomerInformation.Customer = customer;
		}
		protected void PopulateCustomerComments()
		{
			CustomerCommentControl.CustomerId = GetId();
		}
		protected void PopulateCustomerAddresses(ICustomer customer)
		{
			CustomerAddresses.Customer = customer;
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			ICustomer customer = IoC.Resolve<ICustomerSecureService>().GetById(GetId());

			if (customer == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}
			customer.User = CustomerUserControl.GetCustomerUser();
			customer.CustomerInformation = CustomerInformation.GetCustomerInformation();
			customer.CustomerStatusId = System.Convert.ToInt32(StatusList.SelectedValue, CultureInfo.CurrentCulture);
			int previousRegistryId = customer.CustomerRegistryId;
			int newRegistryId = System.Convert.ToInt32(RegistryList.SelectedValue, CultureInfo.CurrentCulture);
			int previouRegistryId = customer.CustomerRegistryId;
			customer.CustomerRegistryId = newRegistryId;

			if (customer.CustomerInformation.IsEdited && !IoC.Resolve<ICustomerInformationSecureService>().CanEdit(customer))
			{
				SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditInformation);
				return;
			}

			if (customer.CustomerRegistryId != previousRegistryId && !IoC.Resolve<ICustomerSecureService>().CanChangeRegistry(customer, previouRegistryId))
			{
				SystemMessageContainer.Add(Resources.CustomerMessage.CannotChangeRegistry);
				return;
			}

			if (customer.User.IsEdited && !IoC.Resolve<IUserSecureService>().CanEdit(customer))
			{
				SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditUser);
				return;
			}

			if (!IoC.Resolve<IUserSecureService>().IsUserNameUnique(customer.User))
			{
				SystemMessageContainer.Add(Resources.CustomerMessage.UserWithSameUserName);
				return;
			}

			IoC.Resolve<ICustomerSecureService>().Save(SignInHelper.SignedInSystemUser, customer, CustomerAddresses.GetAddresses(), CustomerCommentControl.GetCustomerComments());

			SystemMessageContainer.MessageType = InfoType.Success;
			SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessCustomer);

			//Update page
			//This is because newly created addresses had id <= 0 and after save they have actual id and drop-downs for addresses have to be updated with actual selectedValue;
			CustomerAddresses.BindControlData();
			CustomerInformation.BindAddresses(customer, CustomerAddresses.GetAddresses().Where(item => !item.IsDeleted));

			CustomerNameLiteral.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
													 customer.CustomerInformation.FirstName,
													 customer.CustomerInformation.LastName);
			ReBind();
		}


		protected virtual void AddressGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var address = (IAddress)row.DataItem;

			var addressTypeLink = (LinkButton)e.Row.FindControl("AddressViewButton");
			if (addressTypeLink != null)
			{
				addressTypeLink.Text = IoC.Resolve<IAddressTypeSecureService>().GetById(address.AddressTypeId).Title;
			}

			var labelCountry = (Label)e.Row.FindControl("CountryLabel");
			if (labelCountry == null) return;
			labelCountry.Text = IoC.Resolve<ICountrySecureService>().GetById(address.CountryId).Title;
		}

		protected virtual void OrderGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;
			IOrderFull order = (IOrderFull) e.Row.DataItem;
			var lDeliverSstreetAddress = (Label)e.Row.FindControl("lDeliverSstreetAddress");
			lDeliverSstreetAddress.Text = EncodeHelper.HtmlEncode(GetOrderddress(order));
			var lOrderValue = (Label)e.Row.FindControl("lOrderValue");
			lOrderValue.Text = order.GetActualPriceSummary().IncludingVat.ToString(CultureInfo.InvariantCulture);
			int quantity = 0;
			foreach (IOrderItem item in order.GetOrderItems())
			{
				quantity += item.Quantity;
			}
			var lNoArticles = (Label)e.Row.FindControl("lNoArticles");
			lNoArticles.Text = quantity.ToString(CultureInfo.InvariantCulture);
			var hfId = (HiddenField)e.Row.FindControl("OrderIdHiddenField");
			var hlEdit = (HyperLink)e.Row.FindControl("EditLink");
			hlEdit.NavigateUrl = PathHelper.Order.GetEditUrlForCustomerEdit(int.Parse(hfId.Value, CultureInfo.CurrentCulture), GetId());
		}
		protected virtual void OrderGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			OrdersGrid.PageIndex = e.NewPageIndex;
		}

		protected void PopulateCustomerInfo(ICustomer customer)
		{
			ErpIdLiteral.Text = customer.ErpId;
			StatusList.DataSource = IoC.Resolve<ICustomerStatusSecureService>().GetAll();
			StatusList.DataBind();
			StatusList.Items.FindByValue(customer.CustomerStatusId.ToString(CultureInfo.CurrentCulture)).Selected = true;
			RegistryList.DataSource = IoC.Resolve<ICustomerRegistrySecureService>().GetAll();
			RegistryList.DataBind();
			RegistryList.Items.FindByValue(customer.CustomerRegistryId.ToString(CultureInfo.CurrentCulture)).Selected = true;
			CustomerNameLiteral.Text = string.Format(CultureInfo.CurrentCulture, "{0} {1}",
													 customer.CustomerInformation.FirstName,
													 customer.CustomerInformation.LastName);
		}

		protected void PopulateOrderGrid(ICustomer customer)
		{
			CustomerObjectDataSource.SelectParameters.Clear();
			CustomerObjectDataSource.SelectParameters.Add("customerId", customer.Id.ToString(CultureInfo.CurrentCulture));
		}

		protected void PopulateCustomerGroups()
		{
			Collection<ICustomerGroup> customerGroups= IoC.Resolve<ICustomerGroupSecureService>().GetAllByCustomer(GetId());
			if (customerGroups.Count == 0)
			{
				CustomerGroupsNotFoundLiteral.Text = Resources.Customer.Literal_CustomerGroupsNotFound;
				CustomerGroupsNotFoundDiv.Visible = true;
				return;
			}
			GroupGrid.DataSource = customerGroups;
			GroupGrid.DataBind();
		}

		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Customer.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Customer.GetDefaultUrl());
			}
		}

		protected string GetCustomerGroupEditUrl(object groupId)
		{
			return PathHelper.Customer.CustomerGroup.GetEditUrlForCustomerEdit(System.Convert.ToInt32(groupId, CultureInfo.CurrentCulture), GetId());
		}

		protected string GetCustomerGropStatus(object statusId)
		{
			if (_customerGroupStatuses == null)
				_customerGroupStatuses = IoC.Resolve<ICustomerGroupStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _customerGroupStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}
	}
}