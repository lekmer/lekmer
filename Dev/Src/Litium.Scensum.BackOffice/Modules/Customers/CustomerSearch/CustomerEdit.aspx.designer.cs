//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4200
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch {
    
    
    public partial class CustomerEdit {
        
        /// <summary>
        /// EditPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel EditPanel;
        
        /// <summary>
        /// MessagesContainerUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel MessagesContainerUpdatePanel;
        
        /// <summary>
        /// SystemMessageContainer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.MessageContainer SystemMessageContainer;
        
        /// <summary>
        /// ValidationSummary control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.ScensumValidationSummary ValidationSummary;
        
        /// <summary>
        /// CustomerNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal CustomerNameLiteral;
        
        /// <summary>
        /// CustomerInformation control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.Customer.CustomerInformationControl CustomerInformation;
        
        /// <summary>
        /// CustomerAddresses control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.Customer.CustomerAddresses CustomerAddresses;
        
        /// <summary>
        /// CustomerOrdersUpdatePanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.UpdatePanel CustomerOrdersUpdatePanel;
        
        /// <summary>
        /// OrdersGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.GridView2.GridViewWithCustomPager OrdersGrid;
        
        /// <summary>
        /// CustomerObjectDataSource control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.ObjectDataSource CustomerObjectDataSource;
        
        /// <summary>
        /// ErpIdLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal ErpIdLiteral;
        
        /// <summary>
        /// RegistryList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList RegistryList;
        
        /// <summary>
        /// StatusList control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList StatusList;
        
        /// <summary>
        /// CustomerUserControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.Customer.CustomerUserControl CustomerUserControl;
        
        /// <summary>
        /// CustomerGroupsNotFoundDiv control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlGenericControl CustomerGroupsNotFoundDiv;
        
        /// <summary>
        /// CustomerGroupsNotFoundLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal CustomerGroupsNotFoundLiteral;
        
        /// <summary>
        /// GroupGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Repeater GroupGrid;
        
        /// <summary>
        /// CustomerCommentControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.Customer.CustomerCommentControl CustomerCommentControl;
        
        /// <summary>
        /// SaveButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton SaveButton;
        
        /// <summary>
        /// CancelButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton CancelButton;
    }
}
