using System.Configuration;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.AddOns.Dashboard
{
	public partial class Default : PageController
	{
		protected override void SetEventHandlers() { }
		protected override void PopulateForm()
		{
			Frame1.Attributes.Add("src", ConfigurationManager.AppSettings["DashboardPath"]);
		}
	}
}
