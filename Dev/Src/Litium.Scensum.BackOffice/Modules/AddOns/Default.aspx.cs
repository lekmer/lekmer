using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.AddOns
{
	public partial class Default : PageController
	{
		protected override void SetEventHandlers() { }
		protected override void PopulateForm() { }
	}
}