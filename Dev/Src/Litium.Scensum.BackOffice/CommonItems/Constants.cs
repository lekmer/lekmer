using System;
using System.Globalization;

namespace Litium.Scensum.BackOffice.CommonItems
{
	public static class Constants
	{
		public static readonly string MinDate = new DateTime(1753, 1, 1).ToShortDateString();
		public static readonly string MaxDate = new DateTime(9999, 12, 31).ToShortDateString();
		public static readonly string MinSearchDate = new DateTime(2008, 1, 1).ToShortDateString();
		public static readonly string MaxSearchDate = new DateTime(2010, 12, 31).ToShortDateString();
		public static readonly string ExampleDate = new DateTime(2001, 1, 1, 12, 45, 30).ToString(CultureInfo.CurrentCulture);
	}
}