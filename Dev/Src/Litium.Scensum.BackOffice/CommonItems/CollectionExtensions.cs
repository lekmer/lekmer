using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Scensum.BackOffice.CommonItems
{
	public static class CollectionExtensions
	{
		public static void AddRange<T>(this Collection<T> collection, IEnumerable<T> values)
		{
			foreach (var item in values)
			{
				collection.Add(item);
			}
		}
	}
}
