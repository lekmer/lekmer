// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project. 
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc. 
//
// To add a suppression to this file, right-click the message in the 
// Error List, point to "Suppress Message(s)", and click 
// "In Project Suppression File". 
// You do not need to add suppressions to this file manually. 
using System.Diagnostics.CodeAnalysis;

[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Template.Fragment.#GetFragment()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.Modules.Media.MediaArchive.Media.#GetAllMediaFolders()")]

[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Template.Setting.#GetSetting()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Template.SettingRegion.#GetSettings()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Assortment.VariationTypeItem.#GetSource()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Assortment.VariationTypeList.#GetVariationTypes()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Assortment.VariationTypeList.#GetVariationTypeWrappers()")]
[assembly: 
	SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", 
		Target = "Litium.Scensum.Main.UserControls.Assortment.VariantsList.#ProductVariationWrappers")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.Templates")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.Template")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Master.Start")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.UserControls.Template.Setting")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.UserControls.Search.Search")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Assortment.RelationLists.RelationLists")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceLists")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Pages")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Media.MediaArchive.Media")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.UserControls.Search.CustomerSearchCriteria")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerSearchCriteria.Customers")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Assortment.Categories.Categories")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces", Scope = "type", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Aliases.Alias")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.Modules.Media.MediaArchive.Media.#GetAllMediaFolders()")]
[assembly: 
	SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Assortment.VariantsList.#ProductVariationWrappers")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Assortment.VariationTypeItem.#GetSource()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Assortment.VariationTypeList.#GetVariationTypes()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Assortment.VariationTypeList.#GetVariationTypeWrappers()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Template.Fragment.#GetFragment()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Template.Setting.#GetSetting()")]
[assembly: 
	SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", 
		Target = "Litium.Scensum.BackOffice.UserControls.Template.SettingRegion.#GetSettings()")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Interface", Scope = "namespace", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Alias", Scope = "namespace", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Aliases")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Interface", Scope = "namespace", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Aliases")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Interface", Scope = "namespace", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Includes")]
[assembly: 
	SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Interface", Scope = "namespace", 
		Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", Scope = "member", Target = "Litium.Scensum.BackOffice.UserControls.Media.MediaItem.#DataSource")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1014:MarkAssembliesWithClsCompliant")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1044:PropertiesShouldNotBeWriteOnly", Scope = "member", Target = "Litium.Scensum.BackOffice.UserControls.Template.FragmentRegion.#Fragments")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListEdit.#GetMask()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.Orders.OrderEdit.#PopulateOrder(Litium.Scensum.Order.IOrderFull)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductListEdit.#GetProductsState()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListEdit.#GetAllPriceListFolder()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListEdit.#GetMask()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductEdit.#GetAllMediaFolders()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductEdit.#GetProduct()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductEdit.#GetProductSeoSetting()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.RelationLists.RelationListEdit.#GetProductsState()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Aliases.AliasEdit.#GetAliasValue()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.Template.#GetAllModelFolders()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.Template.#GetAllModels()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.TemplateEdit.#GetTemplate()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Media.MediaArchive.ImageEdit.#GetAllModelFolders()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.ContentNodesMove.#GetTreeViewDataSource()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.ContentNodesSort.#GetNodesFromGrid()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.PageSeo.#GetContentPageSeoSetting()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductListEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductSearchResultEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.Categories.Categories.#GetIdOrOrNull()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.FolderEdit.#GetParentIdOrNull()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.Blocks.BlockSignInEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.Blocks.BlockRegisterEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.TemplateEdit.#GetModelId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Templates.TemplateEdit.#GetModelIdOrNull()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks.BlockRichTextEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.FolderEdit.#GetParentNodeId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.LinkEdit.#GetParentNodeId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.PageCreate.#GetParentIdOrOrNull()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Pages.#GetNodeId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Pages.#GetParentNodeId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.ShortcutEdit.#GetParentNodeId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.Orders.Block.BlockCheckoutEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Blocks.BlockEdit.#GetBlockId()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "WYSIWYG", Scope = "namespace", Target = "Litium.Scensum.BackOffice.UserControls.WYSIWYGEditor")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "tiny", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "plugins", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "mce", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "js", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "imagemanager", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "MCE", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Interface", Scope = "namespace", Target = "Litium.Scensum.BackOffice.Modules.Interface.Themes")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Themes.ThemeEdit.#CollectModelTemplateDictionary()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Themes.ThemeEdit.#SaveTheme(System.Nullable`1<System.Int32>,System.Collections.Generic.Dictionary`2<System.Int32,System.Nullable`1<System.Int32>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Interface.Themes.ThemeEdit.#SetDefaultTemplates(System.Collections.Generic.Dictionary`2<System.Int32,System.Nullable`1<System.Int32>>)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch.CustomerEdit.#GetCustomerGropStatuses()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.CustomerGroupEdit.#GetParentIdOrNull()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.CustomerGroupEdit.#GetAllCustomerGroups()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.CustomerGroupEdit.#GetCurrentUrlAsReferrer()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.CustomerGroupsMaster.#GetNodes()")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate", Scope = "member", Target = "Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups.Default.#GetAllGroupsWithRoot()")]
