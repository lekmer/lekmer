﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearch.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ProductSearch" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="Search" Src="~/UserControls/Assortment/ProductSearchCriteria.ascx" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<div id="divSearchProducts" runat="server" class="popup-product-search-container" style="z-index: 10010; display: none;">
    <div id="popup-product-search-header">
		<div id="popup-product-search-header-left">
		</div>
		<div id="popup-product-search-header-center">
			<span><%= Resources.General.Literal_Search %></span>
			<input type="button" id="btnCancel" runat="server" value="x"/>
		</div>
		<div id="popup-product-search-header-right">
		</div>
	</div>
	<div id="popup-product-search-content">
	    <div id="product-search-form">
				<span class="assortment-header"><%= Resources.General.Literal_Search %></span>
                <uc:Search ID="search" ShowSearch="true" runat="server" />
            </div>
	    <asp:UpdatePanel runat="server" ID="upMain" ChildrenAsTriggers="true">
		    <ContentTemplate>			
                <div class="product-search-result">
		                    <span class="assortment-header"><%= Resources.General.Literal_SearchResults %></span>
		                    <div class="relation-product-buttonsx">
							    <sc:GridViewWithCustomPager
								    ID="GVSearch" 
								    SkinID="grid" 
								    runat="server"
								    AllowPaging="true" 
								    PageSize="<%$AppSettings:DefaultGridPageSize%>" 
								    DataSourceID="productDataSource"
								    AutoGenerateColumns="false" 
								    Width="100%">
								    <Columns>
									    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
										    <HeaderTemplate>
											    <asp:CheckBox id="chkAllSearched" runat="server" />
										    </HeaderTemplate>
										    <ItemTemplate>
											    <asp:CheckBox ID="cbSelect" runat="server" />
											    <asp:HiddenField ID="hfId" Value='<%#Eval("Id") %>' runat="server" />
										    </ItemTemplate>
									    </asp:TemplateField>
										
							    <asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="10%" />
							    <asp:BoundField HeaderText="<%$ Resources:General, Literal_Title %>" DataField="DisplayTitle" ItemStyle-Width="60%" />
							    <asp:BoundField HeaderText="<%$ Resources:Product, Literal_Ean %>" DataField="EanCode" ItemStyle-Width="10%" />
							    <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="7%">
										    <ItemTemplate>
											    <asp:Label id="lblProductStatus" runat="server" />
										    </ItemTemplate>
									    </asp:TemplateField>
									    <asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
										    <ItemTemplate>
											    <asp:Label id="lblPrice" runat="server" />
										    </ItemTemplate>
									    </asp:TemplateField>
								    </Columns>
							    </sc:GridViewWithCustomPager>
							    <asp:ObjectDataSource ID="productDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />
		                    </div>
		                    <br style="clear:both;" />
		                    <div id="AllSelectedDiv" runat="server" class="left" style="display: none;">
		                        <uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnAdd" runat="server" Text="<%$ Resources:Product, Button_Select %>" SkinID="DefaultButton" />
		                    </div>
	                    </div>
		    </ContentTemplate>
		    <Triggers>
		        <asp:AsyncPostBackTrigger ControlID="search" EventName="SearchEvent" />
		    </Triggers>
	    </asp:UpdatePanel>
	    	    
     </div>
</div>
<div style="float:right;">
    <uc:ImageLinkButton ID="btnAddProducts" runat="server" Text="<%$ REsources:General, Button_Add %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
    <ajaxToolkit:ModalPopupExtender 
        ID="mpeSearchProducts" runat="server" 
        TargetControlID="btnAddProducts"
		PopupControlID="divSearchProducts"
		BackgroundCssClass="popup-background" Y="20"
		CancelControlID="btnCancel"/>
</div>