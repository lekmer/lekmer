﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class ProductSearchEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IProduct> Products
		{
			get;
			set;
		}
	}
}
