using System;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class ProductSearchCriteriaEventArgs : EventArgs
	{
		public ILekmerProductSearchCriteria SearchCriteria
		{
			get; set;
		}
	}
}
