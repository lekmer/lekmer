﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TagList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.TagList" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<asp:UpdatePanel ID="TagGroupUpdatePanel" runat="server" >
    <ContentTemplate>
        <div class="tags-list-container">
            <div class="left">  
                <asp:LinkButton ID="TagGroupLink" runat="server" ForeColor="Blue" Font-Underline="true"></asp:LinkButton>
            </div>
            <br />
            <div class="data-row">
                <div class="data-column-tags">
                    <asp:ListBox Id="AvailableTagsList" runat="server" DataValueField="Id" DataTextField="Value" Rows="10" CssClass="list-box-tags" SelectionMode="Multiple"></asp:ListBox>
                </div>
                <div class="data-column-tags-center">
                    <uc:ImageLinkButton ID="MoveToSelectedButton" runat="server" Text=">>" SkinID="DefaultButton" />
                    <br style="clear: both;"/>
                    <uc:ImageLinkButton ID="MoveToAvailableButton" runat="server" Text="<<" SkinID="DefaultButton" />
                </div>
                <div class="data-column-tags">
                    <asp:ListBox Id="SelectedTagsList" runat="server" DataValueField="Id" DataTextField="Value" Rows="10" CssClass="list-box-tags" SelectionMode="Multiple"></asp:ListBox>
                </div>
            </div>
        </div>
        
        <div id="TagEditForm" runat="server" class="popup-tags-container" style="z-index: 10010; display: none;">
			<div id="popup-tags-header">
				<div id="popup-tags-header-left">
				</div>
				<div id="popup-tags-header-center">
					<span><asp:Literal ID="TagGroupTitleLiteral" runat="server"></asp:Literal></span>
					<input type="button" id="TagPopupCloseButton" runat="server" value="x"/>
				</div>
				<div id="popup-tags-header-right">
				</div>
			</div>
			<div id="popup-tags-content">
			    <div id="messages">
			        <uc:MessageContainer runat="server" ID="TagPopupMessenger" MessageType="Failure" HideMessagesControlId="NewTagButton" />
			    </div>
			    <br />
				<div class="left">
				    <span><%= Resources.General.Literal_Value %></span>
				    <asp:TextBox ID="NewTagBox" runat="server" Width="205px"></asp:TextBox>
				</div>
				<div class="left" style="padding-left: 10px;">
				    <uc:ImageLinkButton ID="NewTagButton" runat="server" Text="<%$ Resources:General, Button_Add %>" SkinID="DefaultButton"/>
                </div>
				<br style="clear:both;"/><br style="clear:both;"/>
				<div class="left" style="width: 100%; max-height: 305px; overflow-y: scroll; overflow-x: hidden;">
				    <asp:GridView
				        ID="TagGrid" 
				        runat="server" 
				        SkinID="GridWithoutHeaderAndPaging" 
					    AutoGenerateColumns="false"
					    Width="95%">
				        <Columns>
					        <asp:TemplateField ItemStyle-Width="96%">
					            <ItemTemplate>
					                <div id="tag-grid-item">
					                    <asp:TextBox ID="TagValueBox" runat="server" Text='<%# Eval("Value") %>' BorderStyle="None" Width="87%" onfocus="$(this).next().focus();"></asp:TextBox>
					                    &nbsp;
					                    <uc:GenericTranslator ID="TagValueTranslator" runat="server" UseManualDataBinding="true" />
					                </div>
					            </ItemTemplate>
					        </asp:TemplateField>
					        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
						        <ItemTemplate>
							        <asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteTag" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" OnClientClick="return DeleteTagConfirmation();" />
						        </ItemTemplate>
					        </asp:TemplateField>
				        </Columns>
				    </asp:GridView>
				</div>
				<br style="clear:both;"/>
				<div style="float:right; padding: 10px;">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="TagPopupOkButton" runat="server" Text="<%$ Resources:General, Button_Ok %>" SkinID="DefaultButton" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="TagPopupCancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
				</div>
			 </div>
		</div>
        <asp:HiddenField runat="server" ID="FakePopupTarget" />
        
        <ajaxToolkit:ModalPopupExtender 
	        ID="TagPopupExtender" 
	        runat="server"
	        TargetControlID="FakePopupTarget"
	        PopupControlID="TagEditForm"
	        CancelControlID="TagPopupCloseButton"
	        BackgroundCssClass="popup-background"/>
    </ContentTemplate>
</asp:UpdatePanel>
