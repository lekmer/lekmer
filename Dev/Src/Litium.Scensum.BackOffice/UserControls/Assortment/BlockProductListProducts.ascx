﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlockProductListProducts.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.BlockProductListProducts" %>
<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ProductSearch" Src="~/UserControls/Assortment/ProductSearch.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type="text/javascript">	
    function confirmDeleteProducts() {
    	return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
    }
</script>

<asp:UpdatePanel ID="upRelationList" runat="server" UpdateMode="Conditional">                                
<ContentTemplate>                                           
        <Scensum:Grid runat="server" ID="Articles" fixedColumns="Type|Title">
            <sc:GridViewWithCustomPager 
	            ID="GVSave" 
	            SkinID="grid" 
	            runat="server" 
	            PageSize="<%$AppSettings:DefaultGridPageSize%>"
	            AllowPaging="true" 
	            AutoGenerateColumns="false"
	            Width="100%">
	            <Columns>
					<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
						<HeaderTemplate>
							<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:CheckBox ID="SelectCheckBox" runat="server" />
							<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
						</ItemTemplate>
					</asp:TemplateField>
		            <asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="8%" />
		            <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="27%">
			            <ItemTemplate>
				            <uc:HyperLinkEncoded runat="server" ID="hlEdit" Text='<%# Eval("DisplayTitle")%>' NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Product.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' />
			            </ItemTemplate>
		            </asp:TemplateField>
		           <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Category %>" ItemStyle-Width="20%">
				        <ItemTemplate>
					        <%# GetCategory((int)Eval("CategoryId"))%>
				        </ItemTemplate>
			        </asp:TemplateField>
		            <asp:BoundField HeaderText="<%$ Resources:Product, Literal_Ean %>" DataField="EanCode" ItemStyle-Width="10%" />
		            <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="7%">
					        <ItemTemplate>
						        <asp:Label id="lblProductStatus" runat="server" />
					        </ItemTemplate>
			        </asp:TemplateField>
		            <asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="8%" >
			            <ItemTemplate>
				            <asp:Label ID="lblPrice"  Text ="" runat="server"/>
			            </ItemTemplate>
		            </asp:TemplateField>
		            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="15%">
					    <HeaderTemplate>
						    <div class="inline">
							    <%= Resources.General.Literal_SortOrder %>
							    <asp:ImageButton ID="btnRefreshOrder" runat="server" CommandName="RefreshOrdinal" ImageUrl="~/Media/Images/Common/refresh.png" ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
						    </div>
					    </HeaderTemplate>
					    <ItemTemplate>
						    <div class="inline">
						        <asp:HiddenField ID="hfId" Value='<%#Eval("Id") %>' runat="server" />
							    <asp:RequiredFieldValidator runat="server" ID="v" ControlToValidate="tbOrdinal" Text="*" ValidationGroup="vgOrdinals" />
							    <asp:RangeValidator runat="server" ID="rv" ControlToValidate="tbOrdinal" Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>' Text="*" ValidationGroup="vgOrdinals"></asp:RangeValidator>
							    <asp:TextBox runat="server" ID="tbOrdinal" Text='<%#Eval("Ordinal") %>' style="width:20px;"/>
							    <asp:ImageButton runat="server" ID="ibtnUp" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="Up" />
							    <asp:ImageButton runat="server" ID="ibtnDown" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="Down" />
						    </div>
					    </ItemTemplate>
				    </asp:TemplateField>
		            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<ItemTemplate>
									<asp:ImageButton runat="server" ID="ibtnDelete" CommandName="DeleteProduct" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"   OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_product + "\");" %>' />
							</ItemTemplate>
					</asp:TemplateField>
	            </Columns>
            </sc:GridViewWithCustomPager>			
        </Scensum:Grid>
        <br style="clear:both;"/>
        <div id="AllSelectedDiv" runat="server" style="display: none;">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectedButton" Text="<%$Resources:General, Button_RemoveSelected %>" OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton"/>
		 </div>
        <uc:ProductSearch runat="server" ID="productSearch" />        
     </ContentTemplate>
</asp:UpdatePanel>