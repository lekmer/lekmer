﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Common;
using Litium.Scensum.Web.Controls.Exceptions;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
    public partial class ImageGroupCollapsiblePanel : StateUserControlController<ImageGroupCollapsiblePanelState>
    {
        public string CommandArgument
        {
            get
            {
                return State.CommandArgument;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.CommandArgument = value;
            }
        }
        public string Title
        {
            get
            {
                return State.Title;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.Title = value;
            }
        }
        public string ControlToExpand
        {
            get
            {
                return State.ControlToExpand;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.ControlToExpand = value;
            }
        }
        public bool HasAction
        {
            get
            {
                return State.HasAction;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.HasAction = value;
            }
        }
        public string ActionCaption { get; set; }
        public string ActionButtonCaption { get; set; }
        public string ActionButtonCommandName { get; set; }
        public string ActionButtonCommandArgument
        {
            get
            {
                return State.ActionButtonCommandArgument;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.ActionButtonCommandArgument = value;
            }
        }
        public string ActionButtonImageUrl { get; set; }
        public string ActionButtonClientClick { get; set; }


        public string MovieCommandArgument
        {
            get
            {
                return State.MovieCommandArgument;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.CommandArgument = value;
            }
        }
        public string MovieButtonCaption { get; set; }
        public string MovieButtonCommandName { get; set; }
        public string MovieButtonCommandArgument
        {
            get
            {
                return State.MovieButtonCommandArgument;
            }
            set
            {
                if (State == null)
                {
                    State = new ImageGroupCollapsiblePanelState();
                }
                State.MovieButtonCommandArgument = value;
            }
        }
        public string MovieButtonImageUrl { get; set; }

        protected override void SetEventHandlers() { }
        protected override void PopulateControl()
        {
            if (!HasAction)
            {
                ActionBlockDiv.Visible = false;
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!Visible)
            {
                return;
            }

            string areaAction = "OpenClose('" + ControlToExpand + "','" + IsOpenedHidden.ClientID + "','" + IndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
            CollapsibleItemDiv.Attributes.Add("onclick", areaAction);
            ActionBlockDiv.Attributes.Add("onclick", areaAction);

            TitleLiteral.Text = Title;
            ActionButton.CommandArgument = CommandArgument;
            CommandArgumentHidden.Value = CommandArgument;
            ActionCaptionLiteral.Text = ActionCaption;
            ActionButtonImage.ImageUrl = ActionButtonImageUrl;
            ActionButtonCaptionLiteral.Text = ActionButtonCaption;
            if (!string.IsNullOrEmpty(ActionButtonClientClick))
            {
                ActionButton.OnClientClick = ActionButtonClientClick;
            }
            ActionButton.CommandName = ActionButtonCommandName;
            ActionButton.CommandArgument = ActionButtonCommandArgument;

            MovieButton.CommandArgument = MovieButtonCommandArgument;
            MovieButtonCaptionLiteral.Text = MovieButtonCaption;
            MovieButtonImage.ImageUrl = MovieButtonImageUrl;
            MovieButtonCaptionLiteral.Text = MovieButtonCaption;
            MovieButton.CommandName = MovieButtonCommandName;
            MovieButton.CommandArgument = MovieButtonCommandArgument;
            PersistState();
        }

        private void PersistState()
        {
            if (string.IsNullOrEmpty(ControlToExpand))
            {
                throw new ControlIntegrationException("'ControlToExpand' should be set.");
            }
            var controlToExpand = FindControl(Page.Controls, ControlToExpand) as HtmlControl;
            if (controlToExpand == null)
            {
                throw new ControlIntegrationException("Control with Id '" + ControlToExpand + "' is not found.");
            }

            var isOpened = bool.Parse(IsOpenedHidden.Value);
            if (isOpened)
            {
                controlToExpand.Style.Add(HtmlTextWriterStyle.Display, "block");
                IndicatorImage.ImageUrl = "~/Media/Images/Common/up.gif";
            }
            else
            {
                controlToExpand.Style.Add(HtmlTextWriterStyle.Display, "none");
                IndicatorImage.ImageUrl = "~/Media/Images/Common/down.gif";
            }
        }
        private static Control FindControl(ControlCollection controls, string id)
        {
            foreach (Control control in controls)
            {
                if (control.ClientID == id)
                {
                    return control;
                }
                if (control.HasControls())
                {
                    var foundCountrol = FindControl(control.Controls, id);
                    if (foundCountrol != null)
                    {
                        return foundCountrol;
                    }
                }
                var repeater = control as Repeater;
                if (repeater != null)
                {
                    foreach (RepeaterItem item in repeater.Items)
                    {
                        var foundCountrol = FindControl(item.Controls, id);
                        if (foundCountrol != null)
                        {
                            return foundCountrol;
                        }
                    }
                }
            }
            return null;
        }

        public void Open()
        {
            IsOpenedHidden.Value = "true";
        }
    }

    [Serializable]
    public class ImageGroupCollapsiblePanelState
    {
        public string Title { get; set; }
        public string CommandArgument { get; set; }
        public string ActionButtonCommandArgument { get; set; }

        public string MovieCommandArgument { get; set; }
        public string MovieButtonCommandArgument { get; set; }
        public string ControlToExpand { get; set; }
        public bool HasAction { get; set; }
    }
}