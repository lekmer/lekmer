﻿using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
    public partial class ProductSearch : ProductSearchCore
	{
        protected override void OnSearch(ProductSearchEventArgs e)
        {
            base.OnSearch(e);
            mpeSearchProducts.Hide();
        }

        /// <summary>
		/// Performs DataBind method for category tree.
		/// </summary>
		public virtual void RestoreCategoryTree()
		{
			search.RestoreCategoryTree();
		}
	}
}