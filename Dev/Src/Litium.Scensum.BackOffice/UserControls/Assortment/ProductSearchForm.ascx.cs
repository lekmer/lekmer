using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
    public partial class ProductSearchForm : UserControlController
    {
        protected const int MaxValueLength = 10;
        protected bool IsValid;

        public virtual bool Validate()
        {
            cvPriceFrom.Validate();
            cvPriceTo.Validate();
            comperePrice.Validate();

            if (string.IsNullOrEmpty(tbPriceTo.Text))
            {
                comperePrice.IsValid = true;
            }

            IsValid = cvPriceFrom.IsValid && cvPriceTo.IsValid && comperePrice.IsValid;
            return IsValid;
        }

        /// <summary>
        /// Create SearchResult object.
        /// </summary>
        public virtual IProductSearchCriteria CreateSearchCriteria()
        {
            if(!IsValid)
            {
                throw new InvalidOperationException("Call Validate before calling CreateSearchCriteria, "+
                    "to ensure the form data was validated and a search criteria object could be created.");
            }

			var productSearchCriteria = (ILekmerProductSearchCriteria) IoC.Resolve<IProductSearchCriteria>();
            productSearchCriteria.CategoryId = CategoryNodeSelector.SelectedNodeId.ToString();
            productSearchCriteria.Title = tbTitle.Text;
            productSearchCriteria.PriceFrom = tbPriceFrom.Text;
            productSearchCriteria.PriceTo = tbPriceTo.Text;

            var erpIds = tbErpId.Text.Replace(" ", "").Replace(Environment.NewLine, ",").Replace(";", ",").Replace("\t", ",").Replace("\n", ",");

            productSearchCriteria.ErpId = erpIds;
            productSearchCriteria.StatusId = ddlStatus.SelectedValue;
            productSearchCriteria.EanCode = tbEanCode.Text;
			productSearchCriteria.BrandId = ddlBrand.SelectedValue;
            return productSearchCriteria;
        }

        /// <summary>
        /// Restores search fields.
        /// </summary>
        public virtual void RestoreSearchFields(IProductSearchCriteria searchCriteria, int searchBlockNumber)
        {
            PopulateStatusFields();
			PopulateBrandSelection();
            if (searchCriteria == null)
            {
                return;
            }

            int categoryId;
            if (!int.TryParse(searchCriteria.CategoryId, out categoryId))
            {
                categoryId = 0;
            }
            var selectedCategoryId = categoryId != 0 ? (int?)categoryId : null;
            CategoryNodeSelector.SelectedNodeId = selectedCategoryId;
            PopulateCategoryNodeSelectTree(selectedCategoryId);
            CategoryNodeSelector.PopulatePath(selectedCategoryId);

            tbTitle.Text = searchCriteria.Title;
            tbPriceFrom.Text = searchCriteria.PriceFrom;
            tbPriceTo.Text = searchCriteria.PriceTo;
            tbErpId.Text = searchCriteria.ErpId;
            ddlStatus.SelectedValue = searchCriteria.StatusId;
			tbEanCode.Text = searchCriteria.EanCode;
			ddlBrand.SelectedValue = ((ILekmerProductSearchCriteria)searchCriteria).BrandId;
            SetErrorMessage(searchBlockNumber);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            RegisterScript();
            if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator != ".")
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(CultureInfo.CurrentCulture.Name, false);
                Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            SetSettingsFieldsDefaultValues();
        }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			//Checking for the cases when control was loaded dynamically and PopulateControl hadn't been executed. 
			CheckSetup();
		}
        
        protected override void SetEventHandlers()
        {
            CategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
        }

        protected virtual void OnCategoryNodeCommand(object sender, CommandEventArgs e)
        {
			PopulateCategoryNodeSelectTree(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
        }

        protected override void PopulateControl()
        {
            PopulateCategoryNodeSelectTree(null);
			PopulateStatusFields();
			PopulateBrandSelection();
		}

		private void PopulateBrandSelection()
		{
			var brandService = IoC.Resolve<IBrandSecureService>();
			Collection<IBrand> brands = brandService.GetAll();
			ddlBrand.DataSource = brands;
			ddlBrand.DataBind();
			ddlBrand.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}

        protected virtual void PopulateCategoryNodeSelectTree(int? categoryId)
        {
            CategoryNodeSelector.DataSource = GetCategories(categoryId);
            CategoryNodeSelector.DataBind();
        }

        protected virtual IEnumerable<INode> GetCategories(int? categoryId)
        {
            return IoC.Resolve<ICategorySecureService>().GetTree(categoryId);
        }

        protected virtual void PopulateStatusFields()
        {
            var assortmentStatusSecureService = IoC.Resolve<IProductStatusSecureService>();

            ddlStatus.DataSource = assortmentStatusSecureService.GetAll();
            ddlStatus.DataBind();
            ddlStatus.Items.Insert(0, new ListItem(string.Empty, string.Empty));
        }

        protected virtual void SetErrorMessage(int blockNumber)
        {
            comperePrice.ErrorMessage = string.Format(CultureInfo.CurrentCulture, Resources.ProductMessage.SearchCriteriaBlock, blockNumber) + ":" + Resources.ProductMessage.SearchCriteriaPriceFromGreaterPriceTo;
            cvPriceFrom.ErrorMessage = string.Format(CultureInfo.CurrentCulture, Resources.ProductMessage.SearchCriteriaBlock, blockNumber) + ":" + Resources.ProductMessage.SearchCriteriaPriceFromFormat;
            cvPriceTo.ErrorMessage = string.Format(CultureInfo.CurrentCulture, Resources.ProductMessage.SearchCriteriaBlock, blockNumber) + ":" + Resources.ProductMessage.SearchCriteriaPriceToFormat;
        }

        protected virtual void SetErrorMessage()
        {
            comperePrice.ErrorMessage = Resources.ProductMessage.SearchCriteriaPriceFromGreaterPriceTo;
            cvPriceFrom.ErrorMessage = Resources.ProductMessage.SearchCriteriaPriceFromFormat;
            cvPriceTo.ErrorMessage = Resources.ProductMessage.SearchCriteriaPriceToFormat;
        }

        protected virtual void SetSettingsFieldsDefaultValues()
        {
            tbPriceFrom.MaxLength = MaxValueLength + 3;
            tbPriceTo.MaxLength = MaxValueLength + 3;
        }

        protected virtual void RegisterScript()
        {
            if (!Page.ClientScript.IsClientScriptBlockRegistered("SearchValidate"))
            {
                string script = @"<script type='text/javascript'>
				function ValidatePrice(val, args)
				{
					var minValue='0'
					var value = args.Value.trim(); 
					if (value.length == 0)
						return true;
					var rx = new RegExp( '\\d{1," + MaxValueLength + @"}(\\.\\d{1,2})?');
					var matches = rx.exec(value);
					if (matches == null || value != matches[0]) {
						return args.IsValid = false;
					}
					if (value < minValue) {
						return args.IsValid = false;
					}
					args.IsValid = true;
				} function CheckPrice(val, args) {

					ValidatePrice(val, args);
				}							
				</script>";
                Page.ClientScript.RegisterClientScriptBlock(GetType(), "SearchValidate", script);
            }
        }

        protected virtual void CheckSetup()
        {
            if (ddlStatus.Items.Count == 0)
            {
				PopulateStatusFields(); 
            }
			if (ddlBrand.Items.Count == 0)
			{
				PopulateBrandSelection();
			}

            if (CategoryNodeSelector.DataSource == null)
            {
                PopulateCategoryNodeSelectTree(null);
            }
        }
    }
}