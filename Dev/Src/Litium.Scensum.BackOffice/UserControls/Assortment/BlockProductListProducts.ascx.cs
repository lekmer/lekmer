﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class BlockProductListProducts : StateUserControlController<Collection<IBlockProductListProduct>>
	{
		private const int BlockMoveUpStep = -15;
		private const int BlockMoveDownStep = 15;
		private const int OrdinalsStep = 10;

		private Collection<ICategory> _categoriesState = new Collection<ICategory>();
		
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IBlockProductListProduct> BlockProducts
		{
			get
			{
				object productsStored = State;
				return (Collection<IBlockProductListProduct>)productsStored;
			}
			set
			{
				State = value;
			}
		}

		protected override void SetEventHandlers()
		{
			GVSave.RowDataBound += ProductGridRowDataBound;
			GVSave.RowCommand += ProductGridRowCommand;
			GVSave.PageIndexChanging += ProductGridPageIndexChanging;
			productSearch.SearchEvent += ProductSearchSearchEvent;
			RemoveSelectedButton.Click += RemoveSelectedClick;
		}

		protected override void PopulateControl()
		{
			DataBindProductGrid();
		}

		private void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			GVSave.PageIndex = e.NewPageIndex;
			DataBindProductGrid();
		}

		private void ProductSearchSearchEvent(object sender, ProductSearchEventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductListProductSecureService>();
			foreach (IProduct product in e.Products)
			{
			    int productId = product.Id;
				if (BlockProducts.FirstOrDefault(item => item.Id == productId) == null)
				{
					IBlockProductListProduct blockProductListProduct = blockService.Create();
					blockProductListProduct.Id = product.Id;
					blockProductListProduct.ErpId = product.ErpId;
					blockProductListProduct.Title = product.Title;
					blockProductListProduct.Price = product.Price;
					blockProductListProduct.ProductStatusId = product.ProductStatusId;
					blockProductListProduct.CategoryId = product.CategoryId;
					blockProductListProduct.EanCode = product.EanCode;
					blockProductListProduct.Ordinal = BlockProducts.Count > 0 ?
						BlockProducts.Max(item => item.Ordinal) + OrdinalsStep : OrdinalsStep;
					BlockProducts.Add(blockProductListProduct);
				}
			}

			DataBindProductGrid();
			upRelationList.Update();
		}

		private void DataBindProductGrid()
		{
			BlockProducts = Sort(BlockProducts);
			GVSave.DataSource = BlockProducts;
			GVSave.DataBind();
			SetClientFunction();
		}

		private void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var product = (IProduct)row.DataItem;
			var lblPrice = (Label)e.Row.FindControl("lblPrice");
			if (product.Price != null)
			{
				lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
			}
			var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
			var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
			var productStatus = productStatusSecureService.GetAll().First(item => item.Id == product.ProductStatusId).Title;
			lblProductStatus.Text = productStatus;
		}

		private void ProductGridRowCommand(object sender, CommandEventArgs e)
		{
			int productId;

			if (!int.TryParse(e.CommandArgument.ToString(), out productId))
			{
				productId = 0;
			}

			switch (e.CommandName)
			{
				case "UpOrdinal":
					MoveBlock(productId, BlockMoveUpStep);
					break;
				case "DownOrdinal":
					MoveBlock(productId, BlockMoveDownStep);
					break;
				case "DeleteProduct":
					IBlockProductListProduct product = BlockProducts.First(item => item.Id == productId);
					BlockProducts.Remove(product);
					break;
				case "RefreshOrdinal":
					RefreshOrder();
					break;
			}
			DataBindProductGrid();
		}

		protected void RefreshOrder()
		{
			IBlockProductListProduct product;
			foreach (GridViewRow row in GVSave.Rows)
			{
				int productId = int.Parse(((HiddenField)row.FindControl("hfId")).Value, CultureInfo.CurrentCulture);
				product = BlockProducts.FirstOrDefault(b => b.Id == productId);
				if (product == null) continue;
				product.Ordinal = int.Parse(((TextBox)row.FindControl("tbOrdinal")).Text, CultureInfo.CurrentCulture);
			}
			DataBindProductGrid();
		}

		protected void MoveBlock(int elementId, int step)
		{
			BlockProducts.FirstOrDefault(b => b.Id == elementId).Ordinal += step;
			DataBindProductGrid();
		}

		protected static Collection<IBlockProductListProduct> Sort(Collection<IBlockProductListProduct> list)
		{
			var products = new List<IBlockProductListProduct>(list);
			products.Sort(delegate(IBlockProductListProduct element1, IBlockProductListProduct element2)
			{
				if (element1.Ordinal < element2.Ordinal)
					return -1;
				if (element1.Ordinal > element2.Ordinal)
					return 1;
				return 0;
			});
			for (int i = 0; i < products.Count; i++)
			{
				products[i].Ordinal = (i + 1) * OrdinalsStep;
			}
			return new Collection<IBlockProductListProduct>(products);
		}

		protected string GetCategory(int categoryId)
		{
			var category = _categoriesState.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				category = categorySecureService.GetById(categoryId);
				_categoriesState.Add(category);
			}
			return category.Title;
		}

		protected virtual void RemoveSelectedClick(object sender, EventArgs e)
		{
			var ids = GVSave.GetSelectedIds();
			if (ids.Count() == 0) return;

			foreach (var id in ids)
			{
				var productId = id;
				var product = BlockProducts.First(item => item.Id == productId);
				if (product == null) continue;

				BlockProducts.Remove(product);
			}
			DataBindProductGrid();
		}

		protected virtual void SetClientFunction()
		{
			if (GVSave.HeaderRow == null) return;
			var selectAllCheckBox = (CheckBox)GVSave.HeaderRow.FindControl("SelectAllCheckBox");
			selectAllCheckBox.Checked = false;
			selectAllCheckBox.Attributes.Add("onclick",
				"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + GVSave.ClientID + @"'); ShowBulkUpdatePanel('"
				+ selectAllCheckBox.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			foreach (GridViewRow row in GVSave.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				if (cbSelect == null) continue;
				cbSelect.Attributes.Add("onclick",
					"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('"
					+ cbSelect.ID + "', '" + GVSave.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
			}
		}
	}
}
