using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Contract.SecureService;
using Litium.Lekmer.Product.Contract.Service;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class ProductSearchCriteria : UserControlController
	{
		private const int MaxValueLength = 10;
		private string _validationGroup = "vgProductSearchCriteria1";

		public event EventHandler<ProductSearchCriteriaEventArgs> SearchEvent;

		public bool ShowSearch { get; set; }
		
		public string ValidationGroup
		{
			get { return _validationGroup; }
			set { _validationGroup = value; }
		}

        protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			RegisterScript();
			if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator != ".")
			{
				Thread.CurrentThread.CurrentCulture = new CultureInfo(CultureInfo.CurrentCulture.Name, false);
				Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
			}
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			SetSettingsFieldsDefaultValues();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			//Checking for the cases when control was loaded dynamically and PopulateControl hadn't been executed. 
			CheckSetup();
		}

		protected override void SetEventHandlers()
		{
			btnSearch.Click += BtnSearchClick;
			CategoryNodeSelector.NodeCommand += OnCategoryNodeCommand;
		}

		protected virtual void OnCategoryNodeCommand(object sender, CommandEventArgs e)
		{
			PopulateCategoryNodeSelectTree(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
		}

		protected override void PopulateControl()
		{
			PopulateCategoryNodeSelectTree(null);
			PopulateStatusFields();
		    PopulateBrandSelection();
		}

	    private  void PopulateBrandSelection()
	    {
	        var brandService = IoC.Resolve<IBrandSecureService>();
	        Collection<IBrand> brands = brandService.GetAll();
	    	ddlBrand.DataSource = brands;
			ddlBrand.DataBind();
			ddlBrand.Items.Insert(0, new ListItem(string.Empty, string.Empty));
	    }

	    protected void PopulateCategoryNodeSelectTree(int? categoryId)
		{
			CategoryNodeSelector.DataSource = GetCategories(categoryId);
			CategoryNodeSelector.DataBind();
		}

		private static IEnumerable<INode> GetCategories(int? categoryId)
		{
			return IoC.Resolve<ICategorySecureService>().GetTree(categoryId);
		}

		protected void BtnSearchClick(object sender, EventArgs e)
		{
			SearchEvent(this, new ProductSearchCriteriaEventArgs{ SearchCriteria = CreateSearchCriteria() });
			PopulateStatusFields();
			PopulateBrandSelection();
		}

		protected void PopulateStatusFields()
		{
			var assortmentStatusSecureService = IoC.Resolve<IProductStatusSecureService>();

			ddlStatus.DataSource = assortmentStatusSecureService.GetAll();
			ddlStatus.DataBind();
			ddlStatus.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}

		/// <summary>
		/// Create SearchResult object.
		/// </summary>
		public ILekmerProductSearchCriteria CreateSearchCriteria()
		{
			if (string.IsNullOrEmpty(tbPriceTo.Text))
			{
				comperePrice.IsValid = true;
			}
            var productSearchCriteria = (ILekmerProductSearchCriteria) IoC.Resolve<IProductSearchCriteria>();
            productSearchCriteria.CategoryId = CategoryNodeSelector.SelectedNodeId.ToString();
			productSearchCriteria.Title = tbTitle.Text;
			productSearchCriteria.PriceFrom = tbPriceFrom.Text;
			productSearchCriteria.PriceTo = tbPriceTo.Text;
			productSearchCriteria.ErpId = tbErpId.Text;
			productSearchCriteria.StatusId = ddlStatus.SelectedValue;
			productSearchCriteria.EanCode = tbEanCode.Text;
			productSearchCriteria.BrandId = ddlBrand.SelectedValue;

			return productSearchCriteria;
		}

		/// <summary>
		/// Restores search fields.
		/// </summary>
		public void RestoreSearchFields(IProductSearchCriteria searchCriteria, int searchBlockNumber)
		{
			PopulateStatusFields();

			if (searchCriteria == null)
			{
				return;
			}

			int categoryId;
			if(!int.TryParse(searchCriteria.CategoryId, out categoryId))
			{
				categoryId = 0;
			}
			var selectedCategoryId = categoryId != 0 ? (int?)categoryId : null;
			CategoryNodeSelector.SelectedNodeId = selectedCategoryId;
			PopulateCategoryNodeSelectTree(selectedCategoryId);
			CategoryNodeSelector.PopulatePath(selectedCategoryId);

			tbTitle.Text = searchCriteria.Title;
			tbPriceFrom.Text = searchCriteria.PriceFrom;
			tbPriceTo.Text = searchCriteria.PriceTo;
			tbErpId.Text = searchCriteria.ErpId;
			ddlStatus.SelectedValue = searchCriteria.StatusId;
			ddlBrand.SelectedValue = ((ILekmerProductSearchCriteria)searchCriteria).BrandId;
			tbEanCode.Text = searchCriteria.EanCode;
			SetErrorMessage(searchBlockNumber);
		}

		private void SetErrorMessage(int blockNumber)
		{
			comperePrice.ErrorMessage = string.Format(CultureInfo.CurrentCulture,Resources.ProductMessage.SearchCriteriaBlock, blockNumber) + ":" + Resources.ProductMessage.SearchCriteriaPriceFromGreaterPriceTo;
			cvPriceFrom.ErrorMessage = string.Format(CultureInfo.CurrentCulture, Resources.ProductMessage.SearchCriteriaBlock, blockNumber) + ":" + Resources.ProductMessage.SearchCriteriaPriceFromFormat;
			cvPriceTo.ErrorMessage = string.Format(CultureInfo.CurrentCulture, Resources.ProductMessage.SearchCriteriaBlock, blockNumber) + ":" + Resources.ProductMessage.SearchCriteriaPriceToFormat;
		}
		private void SetErrorMessage()
		{
			comperePrice.ErrorMessage = Resources.ProductMessage.SearchCriteriaPriceFromGreaterPriceTo;
			cvPriceFrom.ErrorMessage = Resources.ProductMessage.SearchCriteriaPriceFromFormat;
			cvPriceTo.ErrorMessage = Resources.ProductMessage.SearchCriteriaPriceToFormat;
		}
		private void SetSettingsFieldsDefaultValues()
		{
			tbPriceFrom.MaxLength = MaxValueLength + 3;
			tbPriceTo.MaxLength = MaxValueLength + 3;
			cvPriceFrom.ValidationGroup = ValidationGroup;
			cvPriceTo.ValidationGroup = ValidationGroup;
			comperePrice.ValidationGroup = ValidationGroup;
			if (ShowSearch)
			{
				btnSearch.ValidationGroup = ValidationSummary.ValidationGroup = ValidationGroup;
				SetErrorMessage();
			}
			ValidationSummary.Enabled = btnSearch.Visible = ShowSearch;
		}
		
		private void RegisterScript()
		{
			if (!Page.ClientScript.IsClientScriptBlockRegistered("SearchValidate"))
			{
				string script = @"<script type='text/javascript'>
				function ValidatePrice(val, args)
				{
					var minValue='0'
					var value = args.Value.trim(); 
					if (value.length == 0)
						return true;
					var rx = new RegExp( '\\d{1," + MaxValueLength + @"}(\\.\\d{1,2})?');
					var matches = rx.exec(value);
					if (matches == null || value != matches[0]) {
						return args.IsValid = false;
					}
					if (value < minValue) {
						return args.IsValid = false;
					}
					args.IsValid = true;
				} function CheckPrice(val, args) {

					ValidatePrice(val, args);
				}							
				</script>";
				Page.ClientScript.RegisterClientScriptBlock(GetType(), "SearchValidate", script);
			}
		}

		protected virtual void CheckSetup()
		{
			if (ddlStatus.Items.Count == 0)
			{
				PopulateStatusFields();
			}
			
			if (CategoryNodeSelector.DataSource == null)
			{
				PopulateCategoryNodeSelectTree(null);
			}
		}
		
		/// <summary>
		/// Performs DataBind method for category tree.
		/// </summary>
		public virtual void RestoreCategoryTree()
		{
			CategoryNodeSelector.RestoreTree();
		}
	}
}
