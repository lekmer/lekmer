﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImageRotatorGroup.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ImageRotatorGroup" %>

<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Import Namespace="System.Collections.Generic"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>
<%@ Register TagPrefix="uc" TagName="ImageRotatorGroupCollapsiblePanel" Src="~/UserControls/Assortment/ImageRotatorGroupCollapsiblePanel.ascx" %>

<%@ Register TagPrefix="uc" TagName="SiteStructureNodeSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.BackOffice.UserControls.Assortment" %>
<script type="text/javascript">
	function ResetMessages(){
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
	}

    var divContainer = document.getElementById('tabs-main-container');
    divContainer.onscroll = function() {
    $('.image-rotator-context-menu-shadow').hide();
    $('.image-rotator-context-menu-container').hide(); 
    }    
</script>

<%--<asp:UpdatePanel ID="ImagesUpdatePanel" runat="server" UpdateMode="Always">
<ContentTemplate>--%>
<%--  <Scensum:TinyMceEditor  ID="CaptionsEditor" runat="server"  SkinID="tinyMCE"/>  --%> 
 <asp:Repeater ID="ImageGroupsRepeater" runat="server">
 <ItemTemplate>
    <div id="image-group">
			    <asp:Panel ID="ImageGroupsPanel" runat="server">
				<uc:ImageRotatorGroupCollapsiblePanel runat="server" Id="GroupCollapsiblePanel"
					HasAction="true"
					CommandArgument='<%# Eval("ImageGroupId") %>'
					Title='<%#"Group " +(((int) DataBinder.Eval(Container, "ItemIndex"))+1) %> '
					
					ActionButtonCaption="Remove group"
					ActionButtonImageUrl="./../../Media/Images/Assortment/add-product-image.png"
					ActionButtonCommandName="RemoveGroup"
					ActionButtonCommandArgument='<%# Eval("ImageGroupId") %>'
				/>
				<asp:HiddenField ID="ImageGroupIdValueHiddenField" runat="server" Value='<%# Eval("ImageGroupId") %>' />
			    <div id="ImageGroupGridViewDiv" runat="server" class="area-block-items left" style="table-layout:fixed;">
				    
			
				    <table class="imagerotator"  cellspacing="0" cellpadding="2" border="0" style="width:100%;border-collapse:collapse;" class="no-items" >
                         <tr align="left" valign="middle" style="color:White;background-color:#999999;font-size:11px;height:15px;">
                            <th class="grid-header">Type</th>
                            <th class="grid-header">Image</th>
                            <th class="grid-header"></th>
                            <th class="grid-header"></th>
                            <th class="grid-row-wo-padding">Delete</th>
                         </tr>
                         <tr align="left" valign="middle" style="height:15px">
                             <td  align="center" style="height:50px;width:10%;" class="grid-row-wo-padding">Main                             
                                    
                             </td>
                             <td style="width:40%;" class="grid-row" >
                             <div class="link-button">
				                <asp:LinkButton ID="AddMainImage" runat="server" CommandName="AddMainImage"
					                ><asp:Image runat="server" ID="ActionButtonImage" AlternateText="" ImageUrl="./../../Media/Images/Assortment/add-product-image.png"
					                /><asp:Literal runat="server" ID="ActionButtonCaptionLiteral" Text="Image"
				                /></asp:LinkButton>
			                    </div>
			                    <div style="width: 100%; overflow:hidden;">
							            <div style="float:left; max-width:230px; overflow:hidden;">
			                     <asp:Image id="MainImage" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
					              </div>
					            <div style="float:left;">
								            <uc:ContextMenu ID="MainImageContextMenu" runat="server" 
								            CallerImageSrc="~/Media/Images/Common/context-menu.png" 
								            CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png"
								            MenuContainerCssClass="image-rotator-context-menu-container"
								            MenuShadowCssClass="image-rotator-context-menu-shadow"
								            MenuCallerCssClass="image-rotator-context-menu-caller">  
	                                                    <div class="context-menu-header">
					                                        <%= Resources.General.Literal_Manage %>
				                                        </div>
				                                        <div class="menu-row-separator">
				                                        </div>
				                                        <div class="context-menu-row">
					                                        <img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
					                                        <asp:LinkButton ID="ImageDeleteButton" runat="server" OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_image + "\");" %>' CommandName="DeleteMainImage"  CommandArgument='<%# Eval("ImageGroupId") %>' Text = "<%$ Resources:General, Button_Delete %>"/>
				                                        </div>
				                                                                
	                                         </uc:ContextMenu>
								        </div>
					      </div>
			                  </td>
                              <td align="center" style="width:15%;" class="grid-row">
                                  <asp:Label ID="mainImageFormatExtension" runat="server" ></asp:Label>
                               </td>
                                    <td align="center"  style="width:15%;" class="grid-row">
                                      <asp:Label  ID="mainImageFormatSize" runat="server" ></asp:Label>
                                      </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">
                               <asp:ImageButton runat="server" 
											ID="DeleteMainImage" 
											CommandName="DeleteMainImage" 
											CommandArgument='<%# Eval("ImageGroupId") %>' 
											ImageUrl="~/Media/Images/Common/delete.gif" 
											OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_image + "\");" %>' 
											AlternateText="<%$ Resources:General, Button_Delete %>"  />
                             
                             </td>                                   
                          </tr>
                          
                         <tr align="left" valign="middle" style="height:15px" >
                              <td  align="center" style="height:50px;width:10%;" class="grid-row-wo-padding">Thumb</td>
                             <td style="width:40%;" class="grid-row" ><div class="link-button">
				                <asp:LinkButton ID="AddThumbImage" runat="server" CommandName="AddThumbImage"
					                ><asp:Image runat="server" ID="Image1" AlternateText="" ImageUrl="./../../Media/Images/Assortment/add-product-image.png"
					                /><asp:Literal runat="server" ID="Literal1" Text="Image"
				                /></asp:LinkButton>
				                 </div>
				                    <div style="width: 100%; overflow:hidden;">
							            <div style="float:left; max-width:230px; overflow:hidden;">
				                <asp:Image id="ThumbImage" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
					        </div>
					       
					          <div style="float:left;">
								            <uc:ContextMenu ID="ThumbImageContextMenu" runat="server" 
								            CallerImageSrc="~/Media/Images/Common/context-menu.png" 
								            CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png"
								            MenuContainerCssClass="image-rotator-context-menu-container"
								            MenuShadowCssClass="image-rotator-context-menu-shadow"
								            MenuCallerCssClass="image-rotator-context-menu-caller">   
	                                                    <div class="context-menu-header">
					                                        <%= Resources.General.Literal_Manage %>
				                                        </div>
				                                        <div class="menu-row-separator">
				                                        </div>
				                                       
                                        				
				                                        <div class="context-menu-row">
					                                        <img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
					                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_image + "\");" %>' CommandName="DeleteThumbImage"  CommandArgument='<%# Eval("ImageGroupId") %>' Text = "<%$ Resources:General, Button_Delete %>"/>
				                                        </div>
				                                                                
	                                         </uc:ContextMenu>
								     </div>
								    </div>
			                  
			                  </td>
                                  <td align="center" style="width:15%;" class="grid-row">
                                  <asp:Label ID="thumbImageFormatExtension" runat="server" ></asp:Label>
                               </td>
                                    <td align="center"  style="width:15%;" class="grid-row">
                                      <asp:Label  ID="thumbImageFormatSize" runat="server" ></asp:Label>
                                      </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">
                             
                              <asp:ImageButton runat="server" 
											ID="DeleteThumbImage" 
											CommandName="DeleteThumbImage" 
											CommandArgument='<%# Eval("ImageGroupId") %>' 
											ImageUrl="~/Media/Images/Common/delete.gif" 
											OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_image + "\");" %>' 
											AlternateText="<%$ Resources:General, Button_Delete %>"  />
                             
                             </td>                                   
                          </tr>
                         <tr align="left" valign="middle" style="height:15px" >
                            <td  align="center"style="height:50px;width:10%;" class="grid-row-wo-padding">Link</td>
                          <td style="width:40%;" class="grid-row" >
                             
                             <div id="link-placement-tree" class="site-structure-link-placement-tree">						
							<uc:SiteStructureNodeSelector ID="SiteStructureNodeSelector" runat="server" NotShowDefaultText="true" AllowClearSelection="true" />						
						</div>
                             
                             </td>
                                <td style="width:15%;" class="grid-row"> </td>
                                    <td style="width:15%;" class="grid-row">  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                         <tr align="left" valign="middle" style="height:15px">
                            <td  align="center"style="height:50px;width:15%;" class="grid-row-wo-padding">External link</td>
                            <td style="width:40%;" class="grid-row" >
                                 <asp:TextBox ID="ExternalLinkTextBox" runat="server"  Width="200px"></asp:TextBox>
                             </td>
                                 <td style="width:15%;" class="grid-row"> </td>
                                    <td align="center"  style="width:15%;" class="grid-row"> 
                                     <span>Is move</span><br/>
                                        <asp:CheckBox ID="IsMoveCheckBox" runat="server" />  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                          
                         <tr align="left" valign="middle" style="height:15px" >
                            <td  align="center"style="height:50px;width:10%;" class="grid-row-wo-padding">Link2</td>
                          <td style="width:40%;" class="grid-row" >
                             
                             <div id="Div1" class="site-structure-link-placement-tree">						
							<uc:SiteStructureNodeSelector ID="SiteStructureNodeSelector2" NotShowDefaultText="true" runat="server" AllowClearSelection="true" />						
						</div>
                             
                             </td>
                                <td style="width:15%;" class="grid-row"> </td>
                                    <td style="width:15%;" class="grid-row">  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                            <tr align="left" valign="middle" style="height:15px">
                            <td  align="center"style="height:50px;width:15%;" class="grid-row-wo-padding">External link2</td>
                            <td style="width:40%;" class="grid-row" >
                                 <asp:TextBox ID="ExternalLinkTextBox2" runat="server"  Width="200px"></asp:TextBox>
                             </td>
                                 <td style="width:15%;" class="grid-row"> </td>
                                    <td align="center"  style="width:15%;" class="grid-row"> 
                                     <span>Is move</span><br/>
                                        <asp:CheckBox ID="IsMoveCheckBox2" runat="server" />  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                          
                           <tr align="left" valign="middle" style="height:15px" >
                            <td  align="center"style="height:50px;width:10%;" class="grid-row-wo-padding">Link3</td>
                          <td style="width:40%;" class="grid-row" >
                             
                             <div id="Div2" class="site-structure-link-placement-tree">						
							<uc:SiteStructureNodeSelector ID="SiteStructureNodeSelector3" NotShowDefaultText="true" runat="server" AllowClearSelection="true"/>						
						</div>
                             
                             </td>
                                <td style="width:15%;" class="grid-row"> </td>
                                    <td style="width:15%;" class="grid-row">  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                         <tr align="left" valign="middle" style="height:15px">
                            <td  align="center"style="height:50px;width:15%;" class="grid-row-wo-padding">External link3</td>
                            <td style="width:40%;" class="grid-row" >
                                 <asp:TextBox ID="ExternalLinkTextBox3" runat="server"  Width="200px"></asp:TextBox>
                             </td>
                                 <td style="width:15%;" class="grid-row"> </td>
                                    <td align="center"  style="width:15%;" class="grid-row"> 
                                     <span>Is move</span><br/>
                                        <asp:CheckBox ID="IsMoveCheckBox3" runat="server" />  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                         <tr align="left" valign="middle" style="height:15px">
                             <td  align="center" style="height:50px;width:10%;" class="grid-row-wo-padding">Title</td>
                            <td style="width:40%;" class="grid-row" ><asp:TextBox ID="TitleTextBox" Width="200px" runat="server"></asp:TextBox></td>
                                <td style="width:15%;" class="grid-row"> </td>
                                    <td style="width:15%;" class="grid-row">  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                              <tr align="left" valign="middle" style="height:15px">
                             <td  align="center" style="height:50px;width:10%;" class="grid-row-wo-padding">Ordinal</td>
                            <td style="width:40%;" class="grid-row" ><asp:TextBox ID="OrdinalTextBox" Width="40px" runat="server"></asp:TextBox>
                            <asp:CustomValidator ID="OrdinalValidator" runat="server" ControlToValidate="OrdinalTextBox" 
						ErrorMessage="<%$ Resources:GeneralMessage, OrdinalShouldInteger %>" Text=" *" />
                            </td>
                                <td style="width:15%;" class="grid-row"> </td>
                                    <td style="width:15%;" class="grid-row">  </td>
                             <td align="center" style="width:4%;" class="grid-row-wo-padding">  
                             </td>                                   
                          </tr>
                            <tr align="left" valign="middle" style="height:50px">
                               <td style="width:40%;" colspan="5"  class="grid-row" >
                           
                           <div>
                           <div class="column">
					<span>
						<%= Resources.General.Literal_StartDate %></span>
					<asp:CustomValidator ID="StartDateValidator" runat="server" ControlToValidate="StartDateTextBox" Display="Static"
						ErrorMessage="<%$ Resources:GeneralMessage, StartDateType %>" Text=" *" ValidationGroup="vg"></asp:CustomValidator>
					<br />
					<asp:TextBox ID="StartDateTextBox" runat="server" CssClass="box" Width="125px" />
					<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Assortment/date.png"
						ImageAlign="AbsMiddle" CausesValidation="False" />
					
					<ajaxToolkit:CalendarExtender ID="StartDateCalendarExtender" runat="server" TargetControlID="StartDateTextBox"
						PopupButtonID="StartDateButton" />
				</div>
				<div class="column" style="padding-left:20px;">
					<span>
						<%= Resources.General.Literal_EndDate %></span>
					<asp:CustomValidator ID="EndDateValidator" runat="server" ControlToValidate="EndDateTextBox" Display="Static"
						ErrorMessage="<%$ Resources:GeneralMessage, EndDateType %>" Text=" *" />
					<asp:CustomValidator ID="StartEndDateLimitValidator" runat="server" ControlToValidate="EndDateTextBox" Display="Static"
						ErrorMessage="<%$ Resources:GeneralMessage, EndDateGreaterStartDate %>" Text=" *"
						ValidationGroup="vg" />
					<br />
					<asp:TextBox ID="EndDateTextBox" runat="server" CssClass="box" Width="125px" />
					<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Assortment/date.png"
						ImageAlign="AbsMiddle" CausesValidation="False" />
					<ajaxToolkit:CalendarExtender ID="EndDateCalendarExtender" runat="server" TargetControlID="EndDateTextBox"
						  PopupButtonID="EndDateButton" />
				</div>
                           </div>
                            </td>
                                                                  
                          </tr>
                         <tr align="left" valign="middle" style="height:15px" >
                           <td  align="center" style="height:50px;width:10%;" class="grid-row-wo-padding">Captions</td>
                        <td colspan="3" style="width:40%;" class="grid-row" >
                                   <Scensum:TinyMceEditor  ID="CaptionsEditor" runat="server"  SkinID="tinyMCE"/>                   
                             </td>
                              <td style="width:15%;" class="grid-row">  </td>
                             <td align="center" style="width:4%;">  
                             </td>                                   
                          </tr>
                    </table>
				    
				    <br />				
			    </div>
			    </asp:Panel>
			   			    	
    </div>
 </ItemTemplate>
 </asp:Repeater>
 
		<div style=" float:right; padding-top:20px;">
     <asp:Button ID="AddGroup" runat="server" Text="Add group" ></asp:Button>		                

</div>
  				                
				                 
<ajaxToolkit:ModalPopupExtender ID="ImageAddPopup" runat="server" BehaviorID="modalpopup-image"  TargetControlID="ImgBrowseButton"
        PopupControlID="ImagesDiv" BackgroundCssClass="popup-background" CancelControlID="_inpClose"
        Y="100" X="229" OnCancelScript="ResetMessages();">
</ajaxToolkit:ModalPopupExtender>


<div Style="display: none;">
     <asp:Button ID="ImgBrowseButton" runat="server"/>
</div>
	    <asp:HiddenField ID="ChosenImageHiddenField" runat="server"  />
 <asp:HiddenField ID="GroupIdValueHiddenField" runat="server" />
<uc:MessageContainer ID="SystemMessageContainer" MessageType="Warning" HideMessagesControlId="SaveButton"
	runat="server" /> 
<%--</ContentTemplate>
</asp:UpdatePanel>--%>

<div id="ImagesDiv" runat="server" class="product-popup-images-container">
    <div id="product-popup-images-header">
        <div id="product-popup-images-header-left">
				
        </div>
        <div id="product-popup-images-header-center">
            <span><%= Resources.Product.Literal_AddImages %></span>
            <input runat="server" type="button" id="_inpClose" class="_inpClose" value="x"  />
        </div>
        <div id="product-popup-images-header-right">
			
        </div>
    </div>
    <br clear="all" />
	<div class="product-popup-images-body">
		<asp:UpdatePanel id="ImageSelectUpdatePannel" runat="server" UpdateMode="Always">
		<ContentTemplate>
			<uc:MessageContainer ID="AddImageMessageContainer" MessageType="Failure" HideMessagesControlId="ImageSelectControl" runat="server" />
		</ContentTemplate>
		</asp:UpdatePanel> 
		<uc:ImageSelect id="ImageSelectControl" runat="server"/>
    </div>
</div>

	