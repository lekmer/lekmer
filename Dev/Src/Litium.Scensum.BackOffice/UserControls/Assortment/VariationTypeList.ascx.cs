using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class VariationTypeList : System.Web.UI.UserControl
	{
		private const string _variationTypeSourceStateKey = "VariationTypeSourceState";
		private Collection<VariationTypeWrapper> DataSource
		{
			get
			{
				object variationTypeSourceViewState = ViewState[_variationTypeSourceStateKey];
				if (variationTypeSourceViewState != null)
				{
					return (Collection<VariationTypeWrapper>)variationTypeSourceViewState;
				}

				variationTypeSourceViewState = ViewState[_variationTypeSourceStateKey] = new Collection<VariationTypeWrapper>();
				return (Collection<VariationTypeWrapper>)variationTypeSourceViewState;
			}
		}

		public string ValidationGroup { get; set; }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			btnVariationTypeAdd.Click += BtnVariationTypeAdd_Click;
			rptCriteria.ItemDataBound += RptCriteria_ItemDataBound;
			btnSaveVariation.Click += BtnSaveVariation_Click;
			SetScript();
		}
        
		private void BtnSaveVariation_Click(object sender, EventArgs e)
		{
            if (tbVariationValue != null
                && tbVariationValue.Text != null
                && !string.IsNullOrEmpty(tbVariationValue.Text.Trim()))
            {
				string criterieValue = Foundation.Convert.RemoveRedundantSpaces(tbVariationValue.Text);

                SaveSearchCriteria();
				int variationTypeOrdinal = System.Convert.ToInt32(hfOrdinal.Value, CultureInfo.CurrentCulture);
				int variationOrdinal = System.Convert.ToInt32(hvVariationOrdinal.Value, CultureInfo.CurrentCulture);
                foreach (VariationTypeWrapper wrapper in DataSource)
                {
                    if (wrapper.VariationType.Ordinal == variationTypeOrdinal)
                    {

                        IVariation variationTypeWrapper =
                            wrapper.VariationType.Variations.FirstOrDefault(
								v => v.Ordinal != variationOrdinal && v.Value == criterieValue);
                        if (variationTypeWrapper != null)
                        {
							errorSameName.Add(Resources.ProductMessage.SameValueExist);
                            MPE.Show();
                            return;
                        }
						wrapper.VariationType.Variations.FirstOrDefault(v => v.Ordinal == variationOrdinal).Value = criterieValue;
						break;
                    }
                }
                BindCriteria();
            }
            else
            {
                if (tbVariationValue != null)
                {
                    tbVariationValue.Text = string.Empty;
                }

				errorSameName.Add(Resources.ProductMessage.ValueEmpty);
                MPE.Show();
            }
		}

		private void RptCriteria_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
		{
			VariationTypeItem variationTypeItem = (VariationTypeItem)e.Item.FindControl("VariationType");
			VariationTypeWrapper variationTypeSource = (VariationTypeWrapper)e.Item.DataItem;
			variationTypeItem.ValidationGroup = ValidationGroup;
			variationTypeItem.VariationTypeGuid = variationTypeSource.Guid;
			variationTypeItem.Populate(variationTypeSource);
		}

		private void BtnVariationTypeAdd_Click(object sender, EventArgs e)
		{
            SaveSearchCriteria();

            if (IsValid())
            {
                IVariationTypeFullSecureService variationTypeSecureService =
                    IoC.Resolve<IVariationTypeFullSecureService>();

                IVariationTypeFull variationType = variationTypeSecureService.Create();
                variationType.Ordinal = DataSource.Count;
                VariationTypeWrapper variationTypeWraper = new VariationTypeWrapper {VariationType = variationType};
                DataSource.Add(variationTypeWraper);
            }

		    BindCriteria();
		}

		protected void VariationTitleChanged(object sender, VariationTitleChangedEventArgs e)
		{

			SaveSearchCriteria();

			VariationTypeWrapper variationTypeWrapper = DataSource.FirstOrDefault(v => v.VariationType.Title == e.Title && v.VariationType.Ordinal != e.Ordinal && !string.IsNullOrEmpty(e.Title));
			if (variationTypeWrapper != null)
			{
				DataSource.FirstOrDefault(v => v.VariationType.Ordinal == e.Ordinal).HasSameTitle = true;
			}

			foreach (VariationTypeWrapper wrapper in DataSource)
			{
				VariationTypeWrapper wrapper1 = wrapper;
				if (DataSource.FirstOrDefault(v => v.VariationType.Title == wrapper1.VariationType.Title && v.VariationType.Ordinal != wrapper1.VariationType.Ordinal) == null)
				{
					wrapper.HasSameTitle = false;
				}
			}
			BindCriteria();
		}

		protected void VariationValueChanged(object sender, VariationValueChangedEventArgs e)
		{
			tbVariationValue.Text = e.Value;
			hfOrdinal.Value = e.Ordinal.ToString(CultureInfo.CurrentCulture);
			hvVariationOrdinal.Value = e.ValueOrdinal.ToString(CultureInfo.CurrentCulture);
			if (e.HasError)
			{
				errorSameName.Add(Resources.ProductMessage.SameValueExist);
			}
			MPE.Show();
			return;
		}

		protected void VariationTypeAction(object sender, VariationTypeEventArgs e)
		{
			SaveSearchCriteria();
			switch (e.VariationTypeEventType)
			{
				case VariationTypeEventType.Up:

					for (int i = 1; i < DataSource.Count; i++)
					{
						if (DataSource[i].VariationType.Ordinal == e.Ordinal)
						{
							var higherItemId = DataSource[i - 1];
							DataSource[i - 1] = DataSource[i];
							DataSource[i] = higherItemId;
							break;
						}
					}
					break;
				case VariationTypeEventType.Down:
					for (int i = 0; i < DataSource.Count - 1; i++)
					{
						if (DataSource[i].VariationType.Ordinal == e.Ordinal)
						{
							var higherItemId = DataSource[i + 1];
							DataSource[i + 1] = DataSource[i];
							DataSource[i] = higherItemId;
							break;
						}
					}
					break;
				case VariationTypeEventType.Delete:
					DataSource.Remove(DataSource.FirstOrDefault(vts => vts.VariationType.Ordinal == e.Ordinal));
					break;
			}

			SetOrdinals();
			BindCriteria();
		}

		public Collection<IVariationTypeFull> GetVariationTypes()
		{
			SaveSearchCriteria();

			Collection<IVariationTypeFull> variationTypes = new Collection<IVariationTypeFull>();
		    foreach (VariationTypeWrapper variationTypeWrapper in DataSource)
		    {
				variationTypes.Add(variationTypeWrapper.VariationType);
		    }
		    return variationTypes;
		}

		public Collection<VariationTypeWrapper> GetVariationTypeWrappers()
		{
			SaveSearchCriteria();
			return DataSource;
		}

		public void SetDataSource(Collection<IVariationTypeFull> variationTypes)
		{
			foreach (IVariationTypeFull variationType in variationTypes)
			{
				VariationTypeWrapper variationTypeWrapper = new VariationTypeWrapper {VariationType = variationType};
			    DataSource.Add(variationTypeWrapper);
			}
		}

		public void BindCriteria()
		{
			rptCriteria.DataSource = DataSource;
			rptCriteria.DataBind();
		}

		public bool IsValid()
		{
			SaveSearchCriteria();
			foreach (VariationTypeWrapper variationTypeWrapper in DataSource)
			{
			    bool isValid = true;
				if (variationTypeWrapper.HasSameTitle)
				{
				    isValid = false;
					errorsVTList.Add(Resources.ProductMessage.VariationGroupMultipleTypeTitle);
				}

				if (string.IsNullOrEmpty(variationTypeWrapper.VariationType.Title))
                {
                    isValid = false;
					errorsVTList.Add(Resources.ProductMessage.VariationTypeTitleEmpty);
                }

                if (variationTypeWrapper.VariationType.Variations == null
                    || variationTypeWrapper.VariationType.Variations.Count < 1)
				{
					errorsVTList.Add(Resources.ProductMessage.NoVariationValueAddToType);
				    isValid = false;
				}

                if (!isValid)
                {
                    return false;
                }
			}

			return true;
		}
		
		private void SaveSearchCriteria()
		{
			VariationTypeItem variationTypeItem;
			for (int i = 0; i < rptCriteria.Items.Count; i++)
			{
				variationTypeItem = (VariationTypeItem)rptCriteria.Items[i].FindControl("VariationType");
				DataSource[i] = variationTypeItem.GetSource();
			}
		}
		private void SetScript()
		{
			if (!Page.ClientScript.IsClientScriptBlockRegistered("Variation"))
			{
			    const string script = @"<script type='text/javascript'>
				function HideVariationMessage(labelId)
				{
					var label = document.getElementById(labelId);
  					if (label != null) {
  						label.style.display = 'none'
					}
				}							
				</script>";
			    Page.ClientScript.RegisterClientScriptBlock(GetType(), "Variation", script);
			}
		}

		private void SetOrdinals()
		{
			for (int i = 0; i < DataSource.Count; i++)
			{
				DataSource[i].VariationType.Ordinal = i;
			}
		}
	}

	[Serializable]
	public class VariationTypeWrapper
	{
		private readonly Collection<Guid> variationGuids;
		
		public IVariationTypeFull VariationType { get; set; }
		public bool HasSameTitle { get; set; }
		public Guid Guid { get; set; }

		public Collection<Guid> VariationGuids
		{
			get { return variationGuids; }
		}
		
		public VariationTypeWrapper()
		{
			Guid = Guid.NewGuid();
			variationGuids = new Collection<Guid>();
		}
	}
}