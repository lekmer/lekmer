﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
    public partial class ProductSearchCore : UserControlController
    {
        public event EventHandler<ProductSearchEventArgs> SearchEvent;

        protected virtual void OnSearch(ProductSearchEventArgs e)
        {
            if (SearchEvent != null)
            {
                SearchEvent(this, e);
            }
        }

        protected override void SetEventHandlers()
        {
            GVSearch.PageIndexChanging += ProductGridPageIndexChanging;
            GVSearch.RowDataBound += ProductGridRowDataBound;

            search.SearchEvent += ProductSearchEvent;
            btnAdd.Click += BtnAddClick;
            btnCancel.ServerClick += BtnCancelClick;
        }

        protected override void PopulateControl()
        {
        }

        private void BtnCancelClick(object sender, EventArgs e)
        {
            productDataSource.SelectParameters.Clear();
        }

        private void ProductGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            GridViewRow row = e.Row;
            if (row.RowType == DataControlRowType.Header)
            {
				var chkAllSearched = (CheckBox)row.FindControl("chkAllSearched");
                chkAllSearched.Checked = false;
				chkAllSearched.Attributes.Add("onclick", 
					"SelectAll1('" + chkAllSearched.ClientID + @"','" + GVSearch.ClientID + @"'); ShowBulkUpdatePanel('"
					+ chkAllSearched.ID + "', '" + GVSearch.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
            }
            if (row.RowType != DataControlRowType.DataRow)
                return;
            var product = (IProduct)row.DataItem;

            var lblProductStatus = (Label)e.Row.FindControl("lblProductStatus");
            var productStatusSecureService = IoC.Resolve<IProductStatusSecureService>();
            var productStatus = productStatusSecureService.GetAll().First(item => item.Id == product.ProductStatusId).Title;
            lblProductStatus.Text = productStatus;

            var lblPrice = (Label)e.Row.FindControl("lblPrice");
            if (lblPrice != null && product.Price != null)
            {
            	lblPrice.Text = IoC.Resolve<IFormatter>().FormatPrice(CultureInfo.CurrentCulture, ChannelHelper.CurrentChannel.Currency, product.Price.PriceIncludingVat);
            }

			var selectAllCheckBox = (CheckBox)GVSearch.HeaderRow.FindControl("chkAllSearched");
			var cbSelect = (CheckBox)row.FindControl("cbSelect");
			cbSelect.Attributes.Add("onclick",
									"javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"'); ShowBulkUpdatePanel('" 
									+ cbSelect.ID + "', '" + GVSearch.ClientID + "', '" + AllSelectedDiv.ClientID + @"');");
        }

        private void ProductGridPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GVSearch.PageIndex = e.NewPageIndex;
        }

        private void ProductSearchEvent(object sender, ProductSearchCriteriaEventArgs e)
        {
            GVSearch.PageIndex = 0;
            SearchParamInit(e.SearchCriteria);
        }

        [SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        private void SearchParamInit(ILekmerProductSearchCriteria searchCriteria)
        {
            productDataSource.SelectParameters.Clear();
            productDataSource.SelectParameters.Add("maximumRows", GVSearch.PageSize.ToString(CultureInfo.CurrentCulture));
            productDataSource.SelectParameters.Add("startRowIndex", (GVSearch.PageIndex * GVSearch.PageSize).ToString(CultureInfo.CurrentCulture));
            productDataSource.SelectParameters.Add("categoryId", searchCriteria.CategoryId);
            productDataSource.SelectParameters.Add("erpId", searchCriteria.ErpId);
            productDataSource.SelectParameters.Add("title", searchCriteria.Title);
            productDataSource.SelectParameters.Add("statusId", searchCriteria.StatusId);
            productDataSource.SelectParameters.Add("priceFrom", searchCriteria.PriceFrom);
            productDataSource.SelectParameters.Add("priceTo", searchCriteria.PriceTo);
            productDataSource.SelectParameters.Add("eanCode", searchCriteria.EanCode);
            productDataSource.SelectParameters.Add("brandId", searchCriteria.BrandId);
        }

        private void BtnAddClick(object sender, EventArgs e)
        {
            var products = new Collection<IProduct>();
            bool isAnyRowSelected = false;
            if (GVSearch.Rows.Count > 0)
            {
                foreach (GridViewRow row in GVSearch.Rows)
                {
                    var selectCheckBox = (CheckBox)row.FindControl("cbSelect");
                    if (selectCheckBox.Checked)
                    {
                        isAnyRowSelected = true;
						int productId = System.Convert.ToInt32(((HiddenField)row.FindControl("hfId")).Value, CultureInfo.CurrentCulture);
                        if (products.FirstOrDefault(item => item.Id == productId) == null)
                        {
                            var productSecureService = IoC.Resolve<IProductSecureService>();
                            IProduct product = productSecureService.GetById(ChannelHelper.CurrentChannel.Id, productId);
                            products.Add(product);
                        }
                        selectCheckBox.Checked = false;
                    }
                }
                var chkAllSearched = (CheckBox)GVSearch.HeaderRow.FindControl("chkAllSearched");
                chkAllSearched.Checked = false;
            }

            if (isAnyRowSelected)
            {
                productDataSource.SelectParameters.Clear();
                OnSearch(new ProductSearchEventArgs { Products = products });
            }
        }
    }
}