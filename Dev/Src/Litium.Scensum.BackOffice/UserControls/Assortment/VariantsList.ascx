<%@ Import Namespace="Litium.Scensum.BackOffice.Modules.Assortment.Products"%>
<%@ Import Namespace="Litium.Scensum.BackOffice"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VariantsList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.VariantsList" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ProductSearch" Src="~/UserControls/Assortment/ProductSearch.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<div>
    <asp:UpdatePanel runat="server" ID="upVariationProducts" ChildrenAsTriggers="true" UpdateMode="Conditional">
		<ContentTemplate>
            <uc:MessageContainer Id="errors" MessageType="Warning" HideMessagesControlId="btnRemove" runat="server" />
	
			<asp:HiddenField ID="hfDefaultProduct" runat="server" />
            <Scensum:Grid runat="server" ID="gvVariants" FixedColumns="Def. Product">
                <sc:GridViewWithCustomPager 
		            ID="GV" 
		            SkinID="grid" 
		            runat="server" 
		            Width="100%"
		            AutoGenerateColumns="false"
		            PageSize="<%$AppSettings:DefaultGridPageSize%>"
		            AllowPaging="false"
		            EnableViewState="false">
		            <Columns>
		                <asp:TemplateField ItemStyle-Width="3%">
		                    <HeaderTemplate>
		                        <asp:CheckBox ID="cbSelectAll" runat="server" />
		                    </HeaderTemplate>
		                    <ItemTemplate>
		                        <input type="hidden" id="hdnProductId" runat="server" value='<%# Eval("Id") %>' />
		                        <asp:CheckBox ID="cbIsSelected" runat="server" />
		                    </ItemTemplate>
		                </asp:TemplateField>
		                <asp:TemplateField HeaderText="<%$ Resources:General, Literal_ArtNo %>" ItemStyle-Width="8%">
		                    <ItemTemplate>
		                        <asp:Label ID="lbArticleNumber" runat="server" Text='<%# Eval("ErpId") %>' />
		                    </ItemTemplate>
		                </asp:TemplateField>
			            <asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="30%">
				            <ItemTemplate>
					            <uc:HyperLinkEncoded ID="hlProductTitle" Text='<%# Eval("DisplayTitle")%>' NavigateUrl='<%# PathHelper.Assortment.Product.GetEditUrlForVariationList(int.Parse(Eval("Id").ToString()), VariationGroupId) %>' runat="server" />
				            </ItemTemplate>
			            </asp:TemplateField>
			            <asp:TemplateField HeaderText="<%$ Resources:Product, Literal_DefaultProduct %>" ItemStyle-Width="3%">
			                <ItemTemplate>
								<input type="hidden" id="hdnDefaultProductId" runat="server" />
			                    <asp:RadioButton ID="rbtnDefault" runat="server" GroupName="DefaultProduct" />
			                </ItemTemplate>
			            </asp:TemplateField>
			            <asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign=Center>
			                <ItemTemplate>
			                    <asp:ImageButton ID="btnDelete" runat="server" CommandName="DeleteProductVariation" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmProductDelete();" />
			                </ItemTemplate>
			            </asp:TemplateField>
		             </Columns>
	           </sc:GridViewWithCustomPager>
            </Scensum:Grid>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="AllSelectedDiv" runat="server" style="display: none;" class="apply-to-all-selected-2">
		<div id="cntMultipleActions" runat="server" class="variants-list-footer" style="float:left;">
			<div class="variatnts-list-footer-text"><%= Resources.General.Literal_ApplyToAllSelectedItems %>
			<span id="cntVariationTypes" runat="server" class="vertical-middle"></span>
			</div>
			<div style="display:inline;float: left;">
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnSet" runat="server" Text="<%$ Resources:General, Button_Set %>" CssClass="vertical-middle" SkinID="DefaultButton" />
			<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="btnRemove" runat="server" Text="<%$ Resources:General, Button_Delete %>" CssClass="vertical-middle" SkinID="DefaultButton" OnClientClick="return confirmProductDelete();" /></div>
		</div>
	</div>
    <div class="variants-list-footer">
        <uc:ProductSearch runat="server" ID="productSearch" />
    </div>
	<div>&nbsp;</div>
</div>