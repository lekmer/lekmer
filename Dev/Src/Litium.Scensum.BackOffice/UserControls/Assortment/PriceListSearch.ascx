﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PriceListSearch.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.PriceListSearch" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type='text/javascript' language='javascript'>
	function ResetSerchForm() {
		if (!confirm("<%= Resources.ProductMessage.SearchCriteriaClearConfirm %>")) return false;
		d = document;
		d.getElementById('<%= TitleTextBox.ClientID %>').value = '';
		d.getElementById('<%= StatusList.ClientID %>').value = '';
		d.getElementById('<%= CurrencyList.ClientID %>').value = '';
		return false;
	};

	function ClosePopup() {
		$("div#popup-customers-header-center input[name*='CancelButton']").click();
		return false;
	}
</script>

<div id="SearchPricelistsDiv" runat="server" class="popup-customers-container" style="z-index: 10010;
	display: none;">
	<div id="popup-customers-header">
		<div id="popup-customers-header-left">
		</div>
		<div id="popup-customers-header-center">
			<span>
				<%= Resources.Product.Literal_SelectPricelists %></span>
			<input type="button" id="CancelButton" runat="server" value="x" />
		</div>
		<div id="popup-customers-header-right">
		</div>
	</div>
	<div id="popup-customers-content" style="height: 690px;">
		<asp:Panel ID="Panel" runat="server" DefaultButton="SearchButton">
			<br />
			<div id="customer-search">
				<div id="customer-search-container">
					<label class="assortment-header">
						<%= Resources.General.Literal_Search %></label>
					<br />
					<div id="search-fields-wider">
						<div class="input-box">
							<span>
								<%= Resources.General.Literal_Title %></span><br />
							<asp:TextBox ID="TitleTextBox" runat="server" />
						</div>
						<div class="input-box">
							<span>
								<%= Resources.Product.Literal_Currency%></span><br />
							<asp:DropDownList runat="server" ID="CurrencyList" DataTextField="Iso" DataValueField="Id">
							</asp:DropDownList>
						</div>
						<div class="input-box">
							<span>
								<%= Resources.General.Literal_Status%></span><br />
							<asp:DropDownList runat="server" ID="StatusList" DataTextField="Title" DataValueField="Id">
							</asp:DropDownList>
						</div>
					</div>
				</div>
				<br />
				<div class="buttons-container">
					<uc:ImageLinkButton ID="SearchButton" runat="server" Text="<%$ Resources:General, Button_Search %>"
						SkinID="DefaultButton" />
					<uc:ImageLinkButton ID="ResetButton" runat="server" Text="<%$ Resources:Product, Button_Reset %>"
						OnClientClick='return ResetSerchForm();' SkinID="DefaultButton" />
				</div>
				<div class="product-horizontal-separator">
				</div>
			</div>
		</asp:Panel>
		<br />
		<asp:UpdatePanel runat="server" ID="MainUpdatePanel" ChildrenAsTriggers="true">
			<ContentTemplate>
				<div id="popup-customers-result" class="clear left">
					<label class="assortment-header">
						<%= Resources.General.Literal_SearchResults %></label>
					<br />
					<br />
					<Scensum:Grid ID="PricelistSearchScensumGrid" runat="server">
						<sc:GridViewWithCustomPager ID="PricelistSearchGrid" SkinID="gridWOImageForPager" runat="server"
							AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" DataSourceID="PricelistDataSource"
							AutoGenerateColumns="false" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
									ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:General, Literal_ErpId %>" DataField="ErpId"
									ItemStyle-Width="13%" />
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="30%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="TitleLiteral" runat="server" Text='<%# Eval("Title")%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Currency %>" ItemStyle-Width="9%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="CurrencyLiteral" runat="server" Text='<%# GetCurrency(Eval("CurrencyId"))%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="9%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="StatusLiteral" runat="server" Text='<%# GetPricelistStatus(Eval("PriceListStatusId"))%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_StartDate %>" ItemStyle-Width="18%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="StartDateLiteral" runat="server" Text='<%# Eval("StartDateTime")%>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_EndDate %>" ItemStyle-Width="18%">
									<ItemTemplate>
										<uc:LiteralEncoded ID="EndDateLiteral" runat="server" Text='<%# Eval("EndDateTime") %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
						<asp:ObjectDataSource ID="PricelistDataSource" runat="server" EnablePaging="true"
							SelectCountMethod="SelectCount" SelectMethod="AdvancedSearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.PriceLists.PriceListDataSource" />
					</Scensum:Grid>
					<br />
					<div class="clear right">
						<uc:ImageLinkButton UseSubmitBehaviour="true" ID="AddButton" runat="server" Text="<%$ Resources:Product, Button_Ok %>"
							Enabled="false" SkinID="DefaultButton" />
						<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelBottomButton" runat="server"
							Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" OnClientClick="ClosePopup();" />
					</div>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="SearchButton" EventName="Click" />
			</Triggers>
		</asp:UpdatePanel>
	</div>
</div>
<div style="float: left;">
	<uc:ImageLinkButton ID="AddPricelistsButton" runat="server" Text="<%$ Resources:Product, Button_AddPricelists %>"
		UseSubmitBehaviour="false" SkinID="DefaultButton" />
	<ajaxToolkit:ModalPopupExtender ID="PricelistsSearchPopup" runat="server" TargetControlID="AddPricelistsButton"
		PopupControlID="SearchPricelistsDiv" BackgroundCssClass="popup-background" Y="20"
		CancelControlID="CancelButton" />
</div>
