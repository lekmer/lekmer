<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MessageContainer.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.MessageContainer" %>


<div runat="server" id="divMessages" visible="false" >
    <h2><img runat="server" id="imgAlert" src="~/Media/Images/SystemMessage/icon-information.gif" alt="" />
		<asp:Label runat="Server" ID="lblMessage" />
		<!--[if IE]>
			<br/><div style="height:3px"></div>
		<![endif]-->
	</h2>	
	<ul class="message" >
	<asp:Repeater id="rptMessage" runat="server" EnableViewState="False">
		<ItemTemplate>
			<li  ><%# Container.DataItem %></li>
		</ItemTemplate>
	</asp:Repeater>
	</ul>
</div>
	