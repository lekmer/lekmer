using System;
using System.Collections.ObjectModel;
using System.Web;
using System.Web.UI;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.UserControls
{
	public partial class MessageContainer : UserControl
	{
		private readonly Collection<string> _messages = new Collection<string>();

		public string HideMessagesControlId;

		public bool HasMessages
		{
			get { return _messages.Count > 0; }
		}

		public InfoType MessageType { get; set; }

		public void Add(string message)
		{
			_messages.Add(HttpUtility.HtmlEncode(message));
            divMessages.Visible = true;
		}

		public void Add(string message, InfoType messageType)
		{
			Add(message);
			MessageType = messageType;
		}

		public void Add(string message, bool useHtmlEncode)
		{
			_messages.Add(useHtmlEncode ? HttpUtility.HtmlEncode(message) : message);
			divMessages.Visible = true;
		}

		public void AddRange(Collection<string> messages, bool useHtmlEncode)
		{
			if (messages == null || messages.Count == 0)
			{
				return;
			}
			foreach (string message in messages)
			{
				Add(message, useHtmlEncode);
			}
		}

		public void AddRange(Collection<string> messages)
		{
			if (messages == null || messages.Count == 0)
			{
				return;
			}
			foreach (string message in messages)
			{
				Add(message);
			}
		}

		public void AddRange(Collection<string> messages, InfoType messageType)
		{
			AddRange(messages);
			MessageType = messageType;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			if (!HasMessages)
			{
				divMessages.Visible = false;
				return;
			}
			rptMessage.DataSource = _messages;
			rptMessage.DataBind();
			SetValidationSeverity();

			if (!string.IsNullOrEmpty(HideMessagesControlId))
			{
				ImageLinkButton btn = GetHideMessagesControlId(Page.Controls);
				if (btn != null)
				{
					SetHideMessagesScript(btn);
				}
			}
		}

		private ImageLinkButton GetHideMessagesControlId(ControlCollection controls)
		{
			foreach (Control control in controls)
			{
				var btn = control.FindControl(HideMessagesControlId) as ImageLinkButton;
				if (btn != null)
					return btn;
				if (control.HasControls())
				{
					btn = GetHideMessagesControlId(control.Controls);
					if (btn != null)
						return btn;
				}
			}
			return null;
		}


		private void SetValidationSeverity()
		{
			string divCss = "message-container ";
			string imgAlertSrc = "";
			string messageText = "";
			switch (MessageType)
			{
				case InfoType.Success:
					divCss += "success";
					imgAlertSrc = "~/Media/Images/SystemMessage/icon-success.gif";
					ActivateTimeOutScript();
					break;
				case InfoType.Information:
					divCss += "information";
					imgAlertSrc = "~/Media/Images/SystemMessage/icon-information.gif";
					messageText = "Information!";
					break;
				case InfoType.Warning:
					divCss += "warning";
					imgAlertSrc = "~/Media/Images/SystemMessage/icon-warning.gif";
					messageText = "Warning!";
					break;
				case InfoType.Failure:
					divCss += "failure";
                    imgAlertSrc = "~/Media/Images/SystemMessage/icon-warning.gif";
					messageText = "Invalid input";
					break;
				default:
					break;
			}
			divMessages.Attributes.Add("class", divCss);
			imgAlert.Src = imgAlertSrc;
			lblMessage.Text = messageText;
		}

		private void SetHideMessagesScript(ImageLinkButton btn)
		{
			btn.OnClientClick += "HideMessages('" + divMessages.ClientID + "');";
		}
		private void ActivateTimeOutScript()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), new Guid().ToString(), "$(\"#" + divMessages.ClientID + "\").fadeIn(500).fadeTo(3000, 1).fadeOut(1000); window.setTimeout(function() {if($get('" + divMessages.ClientID + "')){$get('" + divMessages.ClientID + "').style.visibility = 'hidden'}}, 5000);", true);
		}
	}


	public enum InfoType
	{
		Success,
		Information,
		Warning,
		Failure
	}
}