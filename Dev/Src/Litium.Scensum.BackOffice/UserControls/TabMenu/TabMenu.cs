using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using Litium.Scensum.BackOffice.Controller.CommonItems;

namespace Litium.Scensum.BackOffice.UserControls.TabMenu
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
	public class TabMenu : Control
	{
		private const string Active = "active";
		private const string PathSuffix = "Default.aspx";
		private const string PathPrefix = "~/Modules/";
		private const string PathSeparator = "/";

		private HtmlGenericControl _tabsMain;
		private HtmlGenericControl _tabsChild;

		public string TabMenuXmlPath { get; set; }
		public string ActiveMainTab { get; set; }
		public string ActiveSubTab { get; set; }

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CreatePlaceholders();
			CreateMainTabs();
		}

		private static string CurrentPath
		{
			get
			{
				return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath;
			}
		}
		private void CreatePlaceholders()
		{
			HtmlGenericControl divMenuWrapper = CreateGenericControl("div", "menu-wrapper");
			_tabsMain = CreateGenericControl("div", "main-menu-list");
			_tabsChild = CreateGenericControl("div", "sub-menu-list");
			divMenuWrapper.Controls.Add(_tabsMain);
			divMenuWrapper.Controls.Add(_tabsChild);
			Controls.Add(divMenuWrapper);
		}
		private void CreateMainTab(Tab tab, HtmlGenericControl ul)
		{
			if (!PrivilegeHelper.CheckSecurity(tab))
			{
				return;
			}

			bool isActiveTab = IsActiveMainTab(tab.Name);
			if (isActiveTab)
			{
				// Hidden main tab might have visible subtabs
				CreateChildTabs(tab);
			}
			if (tab.Hidden)
			{
				return;
			}

			string cssListItemContainer;
			string cssLeftCorner;
			string cssMenuItem;
			string cssRightCorner;
			GetMainStyle(tab, isActiveTab, out cssListItemContainer, out cssLeftCorner, out cssMenuItem, out cssRightCorner);
			
			HtmlGenericControl li = CreateGenericControl("li", string.Empty);
			HtmlGenericControl divListItemContainer = CreateGenericControl("div", cssListItemContainer);
			HtmlGenericControl prefix = CreateGenericControl("div", cssLeftCorner);
			HtmlGenericControl label = CreateGenericControl("div", cssMenuItem);
			HtmlGenericControl suffix = CreateGenericControl("div", cssRightCorner);

			Tab defaultSubTab = tab.Children.FirstOrDefault(subTab => subTab.IsDefault && PrivilegeHelper.CheckSecurity(subTab));
			var a = new HtmlAnchor
			{
				HRef = BuildHref(tab.Name, defaultSubTab != null ? defaultSubTab.Name : string.Empty),
				InnerHtml = string.IsNullOrEmpty(tab.Caption) ? "&nbsp;" : tab.Caption
			};

			if (!string.IsNullOrEmpty(tab.ImagePath) && !string.IsNullOrEmpty(tab.ImageActivePath))
			{
				string imagePath = isActiveTab ? tab.ImageActivePath : tab.ImagePath;
				label.Style.Add("background-image", ResolveUrl(imagePath));
			}

			label.Controls.Add(a);
			divListItemContainer.Controls.Add(prefix);
			divListItemContainer.Controls.Add(label);
			divListItemContainer.Controls.Add(suffix);
			li.Controls.Add(divListItemContainer);
			ul.Controls.Add(li);
		}
		private void CreateMainTabs()
		{
			HtmlGenericControl ul = CreateGenericControl("ul", string.Empty);
			_tabsMain.Controls.Add(ul);

			NavigationHelper navHelper = new NavigationHelper(TabMenuXmlPath);
			foreach (Tab tab in navHelper.Tabs)
			{
				CreateMainTab(tab, ul);
			}
		}
		private void CreateChildTab(Tab mainTab, Tab tab, HtmlGenericControl ul, bool isLast)
		{
			var li = CreateGenericControl("li", GetChildStyle(tab.Name, isLast));
			var a = new HtmlAnchor
			{
				HRef = BuildHref(mainTab.Name, tab.Name),
				InnerText = tab.Caption
			};

			li.Controls.Add(a);
			ul.Controls.Add(li);
		}
		private void CreateChildTabs(Tab mainTab)
		{
			var ul = CreateGenericControl("ul", string.Empty);
			_tabsChild.Controls.Add(ul);

			foreach (Tab tab in mainTab.Children)
			{
				bool isLast = false;
				if (tab.Name == mainTab.Children.Last().Name)
				{
					isLast = true;
				}
				CreateChildTab(mainTab, tab, ul, isLast);
			}
		}
		private static HtmlGenericControl CreateGenericControl(string pControlName, string pClass)
		{
			HtmlGenericControl control = new HtmlGenericControl(pControlName);
			if (!string.IsNullOrEmpty(pClass))
			{
				control.Attributes.Add("class", pClass);
			}
			return control;
		}
		private string GetChildStyle(string tabName, bool isLast)
		{
			string style = ((Regex.IsMatch(CurrentPath, @"^~/Modules/\w+/" + tabName + @".+") && string.IsNullOrEmpty(ActiveSubTab)) || tabName == ActiveSubTab) ? Active : string.Empty;
			if (isLast)
			{
				style = string.IsNullOrEmpty(style) ? "last" : string.Format(CultureInfo.CurrentCulture, "{0}-last", style);
			}
			return style;
		}
		private static void GetMainStyle(Tab tab, bool isActiveTab, out string cssListItemContainer, out string cssLeftCorner, out string cssMenuItem, out string cssRightCorner)
		{
			cssListItemContainer = "list-item-container";
			cssLeftCorner = "left-corner";
			if (!string.IsNullOrEmpty(tab.ImagePath) && !string.IsNullOrEmpty(tab.ImageActivePath))
			{
				cssMenuItem = "menu-item-image";
			}
			else
			{
				cssMenuItem = "menu-item";
			}
			cssRightCorner = "right-corner";
			if (isActiveTab)
			{
				cssListItemContainer += "-active";
				cssLeftCorner += "-active";
				cssMenuItem += "-active";
				cssRightCorner += "-active";
			}
		}
		private static string BuildHref(string mainTabName, string childTabName)
		{
			StringBuilder href = new StringBuilder();
			href.Append(PathPrefix);
			href.Append(mainTabName);
			if (childTabName.Length > 0)
			{
				href.Append(PathSeparator);
				href.Append(childTabName);
			}
			href.Append(PathSeparator);
			href.Append(PathSuffix);
			return href.ToString();
		}

		private bool IsActiveMainTab(string tabName)
		{
			return ((Regex.IsMatch(CurrentPath, @"^~/Modules/" + tabName + @".+") && string.IsNullOrEmpty(ActiveMainTab)) || tabName == ActiveMainTab);
		}
	}
}
