using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class ContentPageSelector : UserControlController
	{
		private readonly string _noSelectionText = Resources.SiteStructure.Literal_InheritFromParent;

		public event EventHandler<CommandEventArgs> OnSelectedPageChanged;

		public bool ShowRegistryInPath { get; set; }
		public bool AllowClearSelection { get; set; }
		public bool OnlyProductPagesSelectable { get; set; }
		public bool OnlyMasterPagesSelectable { get; set; }
        public int SiteStructureRegistryId { get; set; }
		public int? SelectedNodeId { get; set; }

		protected override void SetEventHandlers()
		{
			SavePageButton.Click += BtnSavePage_Click;
			NodesTreeView.NodeDataBound += NodesTreeView_NodeDataBound;
			PageButton.Click += PageSelectButtonClick;
			NodesTreeView.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateControl()
		{
			if (OnlyProductPagesSelectable && OnlyMasterPagesSelectable) 
				throw new ConfigurationErrorsException("Only product pages and only master pages modes can not be used on the same time.");
		}
		
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CancelSavePageButton.OnClientClick = "document.getElementById('" + CancelPageButton.ClientID + "').click(); return false;";
			if (!AllowClearSelection)
			{
				ScriptManager.RegisterClientScriptBlock(TreeUpdatePanel, TreeUpdatePanel.GetType(), "DenyClearSelection", "$(\"div#clear-selection\").hide();", true);
			}
		}
		
		protected override object SaveViewState()
		{
			ViewState["SelectedContentPageId" + ClientID] = SelectedNodeId;
			ViewState["SiteStructureRegistryId" + ClientID] = SiteStructureRegistryId;
			return base.SaveViewState();
		}
		
		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			SelectedNodeId = (int?)ViewState["SelectedContentPageId" + ClientID];
			SiteStructureRegistryId = (int)ViewState["SiteStructureRegistryId" + ClientID];
		}
		
		private void PageSelectButtonClick(object sender, ImageClickEventArgs e)
		{
			PopulateTree(SelectedNodeId);
			TreeModalPopupExtender.Show();
		}
		
		protected void ClearSelectionButtonClick(object sender, EventArgs e)
		{
			NodesTreeView.DenySelection = true;
			NodesTreeView.SelectedNodeId = null;
		}

		private void NodesTreeView_NodeDataBound(object sender, TemplatedTreeViewNodeEventArgs e)
		{
			var node = TreeDataSource.FirstOrDefault(n => n.Id == e.NodeId);
			if (node != null && node.NodeType == ContentNodeTypeInfo.Page && Processor.IsAvailable(node))
			{
				return;
			}
			DisableNode(e.Node.FindControl("lbName") as LinkButton);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
			TreeModalPopupExtender.Show();
		}
		
		private void BtnSavePage_Click(object sender, EventArgs e)
		{
			if (IsAvailable(NodesTreeView.SelectedNodeId))
			{
				SelectedNodeId = NodesTreeView.SelectedNodeId;
			}
			SetContentPageTitle();
			TreeModalPopupExtender.Hide();
			if (OnSelectedPageChanged != null)
			{
				OnSelectedPageChanged(this, new CommandEventArgs(string.Empty, SelectedNodeId));
			}
		}

		private bool IsAvailable(int? id)
		{
			if (!id.HasValue && AllowClearSelection) return true;
			
			var node = TreeDataSource.FirstOrDefault(n => n.Id == id);
			if (node == null) return false;

			return (node.NodeType == ContentNodeTypeInfo.Page && Processor.IsAvailable(node));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
		private static void DisableNode(WebControl node)
		{
			if (node != null)
			{
				node.Enabled = false;
				node.ForeColor = Color.Gray;
				node.Font.Underline = false;
			}
		}

		public void SetContentPageTitle()
		{
			if (!SelectedNodeId.HasValue)
			{
				if (AllowClearSelection)
				{
					PageAllowClearSelectionLiteral.Text = _noSelectionText;
				    AllowClearSelectionSpan.Visible = true;
				    SelectionSpan.Visible = false;
				}
				return; 
			}
			PopulatePath();
			AllowClearSelectionSpan.Visible = false;
			SelectionSpan.Visible = true;
		}
		
		private void PopulatePath()
		{
			if (!SelectedNodeId.HasValue) return;

			var nodeId = SelectedNodeId.Value;
			var nodes = TreeDataSource;
			var node = nodes.FirstOrDefault(i => i.Id == nodeId);
			if (node == null)
			{
				return;
			}
			var foldersPath = new StringBuilder(node.Title);
			while (node.ParentId.HasValue)
			{
				var clone = node; // to avoid access to modified closure
				node = nodes.FirstOrDefault(f => f.Id == clone.ParentId.Value);
				foldersPath.Insert(0, @"\");
				foldersPath.Insert(0, node.Title);
			}
			PageSelectionLiteral.Text = foldersPath.ToString();
		}

		protected virtual void PopulateTree(int? id)
		{
			if (!OnlyProductPagesSelectable && !OnlyMasterPagesSelectable)
			{
				TreeDataSource = Processor.GetDataSource(id);
			}
			NodesTreeView.DataSource = TreeDataSource;
			NodesTreeView.DataBind();
			NodesTreeView.SelectedNodeId = id;
		}

		private ISelectorProcessor _processor;
		private ISelectorProcessor Processor
		{
			get
			{
				return _processor ?? (_processor = OnlyProductPagesSelectable 
					? new ProductPageProcessor(SiteStructureRegistryId) 
					: OnlyMasterPagesSelectable
						? new MasterPageProcessor(SiteStructureRegistryId) as ISelectorProcessor
						: new DefaultProcessor(SiteStructureRegistryId));
			}
		}

		protected IEnumerable<ISiteStructureNode> TreeDataSource
		{
			get
			{
				return ViewState["DataSource" + ClientID] as IEnumerable<ISiteStructureNode> ??
					(TreeDataSource = Processor.GetDataSource(SelectedNodeId));
			}
			set
			{
				ViewState["DataSource" + ClientID] = value;
			}
		}

		//[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		//protected virtual Collection<ISiteStructureNode> GetDataSourceFromProcessor()
		//{
		//    var contentNodes = Processor.GetDataSource();
		//    var source = new Collection<ISiteStructureNode>();
		//    var pageService = IoC.Resolve<IContentPageSecureService>();
		//    foreach (var item in contentNodes)
		//    {
		//        var node = IoC.Resolve<ISiteStructureNode>();
		//        node.Id = item.Id;
		//        node.ParentId = item.ParentContentNodeId;
		//        node.Title = item.Title;
		//        node.HasChildren = contentNodes.FirstOrDefault(n => n.ParentContentNodeId == node.Id) != null;
		//        node.NodeType = (ContentNodeTypeInfo) item.ContentNodeTypeId;
		//        node.NodeStatus = (ContentNodeStatusInfo)item.ContentNodeStatusId;
		//        if (node.NodeType == ContentNodeTypeInfo.Page)
		//        {
		//            var page = pageService.GetById(node.Id);
		//            node.IsMasterPage = page.IsMaster;
		//            node.HasMasterPage = page.MasterPageId.HasValue;
		//        }
		//        source.Add(node);
		//    }
		//    return source;
		//}
	}
}
