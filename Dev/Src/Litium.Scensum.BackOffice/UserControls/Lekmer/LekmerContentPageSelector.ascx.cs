﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.Lekmer
{
	public partial class LekmerContentPageSelector : UserControlController
	{
		private readonly string _noSelectionTextDefault = Resources.Lekmer.ContentPageSelector_Inherit;

		public event EventHandler<CommandEventArgs> OnSelectedPageChanged;

        public bool NotShowDefaultText{ get; set; }
		public bool ShowRegistryInPath { get; set; }
		public bool AllowClearSelection { get; set; }
		public bool OnlyProductPagesSelectable { get; set; }
		public bool OnlyMasterPagesSelectable { get; set; }
		public int SiteStructureRegistryId { get; set; }
		public int? SelectedNodeId { get; set; }
		public string NoSelectionText { get; set; }

		protected override void SetEventHandlers()
		{
			SavePageButton.Click += BtnSavePage_Click;
			NodesTreeView.NodeDataBound += NodesTreeView_NodeDataBound;
			PageButton.Click += PageSelectButtonClick;
			NodesTreeView.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateControl()
		{
			if (OnlyProductPagesSelectable && OnlyMasterPagesSelectable)
				throw new ConfigurationErrorsException("Only product pages and only master pages modes can not be used on the same time.");
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CancelSavePageButton.OnClientClick = "document.getElementById('" + CancelPageButton.ClientID + "').click(); return false;";
			if (!AllowClearSelection)
			{
				ScriptManager.RegisterClientScriptBlock(TreeUpdatePanel, TreeUpdatePanel.GetType(), "DenyClearSelection", "$(\"div#clear-selection\").hide();", true);
			}
		}

		private void PageSelectButtonClick(object sender, ImageClickEventArgs e)
		{
			PopulateTree(SelectedNodeId);
			TreeModalPopupExtender.Show();
		}

		protected void ClearSelectionButtonClick(object sender, EventArgs e)
		{
			NodesTreeView.DenySelection = true;
			NodesTreeView.SelectedNodeId = null;
		}

		private void NodesTreeView_NodeDataBound(object sender, TemplatedTreeViewNodeEventArgs e)
		{
			var node = TreeDataSource.FirstOrDefault(n => n.Id == e.NodeId);
			if (node != null && node.NodeType == ContentNodeTypeInfo.Page && LekmerProcessor.IsAvailable(node))
			{
				return;
			}
			DisableNode(e.Node.FindControl("lbName") as LinkButton);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
			TreeModalPopupExtender.Show();
		}

		private void BtnSavePage_Click(object sender, EventArgs e)
		{
			if (IsAvailable(NodesTreeView.SelectedNodeId))
			{
				SelectedNodeId = NodesTreeView.SelectedNodeId;
			}
			SetContentPageTitle();
			TreeModalPopupExtender.Hide();
			if (OnSelectedPageChanged != null)
			{
				OnSelectedPageChanged(this, new CommandEventArgs(string.Empty, SelectedNodeId));
			}
		}

		private bool IsAvailable(int? id)
		{
			if (!id.HasValue && AllowClearSelection) return true;

			var node = TreeDataSource.FirstOrDefault(n => n.Id == id);
			if (node == null) return false;

			return (node.NodeType == ContentNodeTypeInfo.Page && LekmerProcessor.IsAvailable(node));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
		private static void DisableNode(WebControl node)
		{
			if (node != null)
			{
				node.Enabled = false;
				node.ForeColor = Color.Gray;
				node.Font.Underline = false;
			}
		}

		public void SetContentPageTitle()
		{
			if (!SelectedNodeId.HasValue)
			{
				if (AllowClearSelection)
				{
					PageAllowClearSelectionLiteral.Text = !string.IsNullOrEmpty(NoSelectionText) ? NoSelectionText :NotShowDefaultText?string.Empty: _noSelectionTextDefault;
					AllowClearSelectionSpan.Visible = true;
					SelectionSpan.Visible = false;
				}
				return;
			}
			PopulatePath();
			AllowClearSelectionSpan.Visible = false;
			SelectionSpan.Visible = true;
		}

		private void PopulatePath()
		{
			int nodeId = SelectedNodeId.Value;
			var nodes = TreeDataSource;
			var node = nodes.FirstOrDefault(i => i.Id == nodeId);
			if (node == null)
			{
				return;
			}
			var foldersPath = new StringBuilder(node.Title);
			while (node.ParentId.HasValue)
			{
				var clone = node; // to avoid access to modified closure
				node = nodes.FirstOrDefault(f => f.Id == clone.ParentId.Value);
				foldersPath.Insert(0, @"\");
				foldersPath.Insert(0, node.Title);
			}
			PageSelectionLiteral.Text = foldersPath.ToString();
		}

		protected virtual void PopulateTree(int? id)
		{
			if (!OnlyProductPagesSelectable && !OnlyMasterPagesSelectable)
			{
				TreeDataSource = LekmerProcessor.GetDataSource(id);
			}
			NodesTreeView.DataSource = TreeDataSource;
			NodesTreeView.DataBind();
			NodesTreeView.SelectedNodeId = id;
		}

		private ILekmerSelectorProcessor _lekmerProcessor;
		private ILekmerSelectorProcessor LekmerProcessor
		{
			get
			{
				return _lekmerProcessor ?? (_lekmerProcessor = OnlyProductPagesSelectable
					? new LekmerProductPageProcessor(SiteStructureRegistryId)
					: OnlyMasterPagesSelectable
						? new LekmerMasterPageProcessor(SiteStructureRegistryId) as ILekmerSelectorProcessor
						: new LekmerDefaultProcessor(SiteStructureRegistryId));
			}
		}

		protected IEnumerable<ISiteStructureNode> TreeDataSource
		{
			get
			{
				return ViewState["LekmerDataSource" + ClientID] as IEnumerable<ISiteStructureNode> ??
					(TreeDataSource = LekmerProcessor.GetDataSource(SelectedNodeId));
			}
			set
			{
				ViewState["LekmerDataSource" + ClientID] = value;
			}
		}

		protected override object SaveViewState()
		{
			ViewState["LekmerSelectedId" + ClientID] = SelectedNodeId;
			ViewState["LekmerSiteStructureRegistryId" + ClientID] = SiteStructureRegistryId;
			return base.SaveViewState();
		}

		protected override void LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			SelectedNodeId = (int?)ViewState["LekmerSelectedId" + ClientID];
			SiteStructureRegistryId = (int)ViewState["LekmerSiteStructureRegistryId" + ClientID];
		}
	}
}