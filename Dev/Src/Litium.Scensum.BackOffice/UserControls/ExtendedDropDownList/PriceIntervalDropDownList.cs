﻿using Litium.Lekmer.Product;
using System.Collections.ObjectModel;
using System.Linq;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;

namespace Litium.Scensum.BackOffice.UserControls
{
	public class PriceIntervalDropDownList : ExtendedDropDownList
	{
		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods")]
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IPriceInterval> DataSource { get; set; }

		public override void DataBind()
		{
			base.DataBind();

			var orderedDataSource = DataSource.OrderBy(p => p.From).GroupBy(p => p.Currency.Id);
			foreach (IGrouping<int, IPriceInterval> groupItem in orderedDataSource)
			{
				var firstPriceInterval = groupItem.ElementAt(0);
				ExtendedItems.Add(new ExtendedListItem(firstPriceInterval.Title, firstPriceInterval.Id.ToString(CultureInfo.InvariantCulture), true, ListItemGroupingType.New, firstPriceInterval.Currency.Iso));
				var priceIntervals = groupItem.Skip(1);
				foreach (IPriceInterval priceInterval in priceIntervals)
				{
					ExtendedItems.Add(new ExtendedListItem(priceInterval.Title, priceInterval.Id.ToString(CultureInfo.InvariantCulture), ListItemGroupingType.Inherit));
				}
			}
		}
	}
}
