﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation.Tree;
using System.Text;

namespace Litium.Scensum.BackOffice.UserControls.Tree
{
	public partial class CategoryNodeSelector : StateUserControlController<CategoryNodeSelectorState>
	{
		public event EventHandler<SelectedCategoryChangedEventArgs> SelectedCategoryChangedEvent;
		public event EventHandler<CommandEventArgs> NodeCommand;

		public int? SelectedNodeId
		{
			get { return State.SelectedNodeId; }
			set
			{
				EnsureState();
				CategoriesTree.SelectedNodeId = State.SelectedNodeId = value;
			}
		}

		public IEnumerable<INode> DataSource
		{
			get { return State.DataSource; }
			set
			{
				EnsureState();
				CategoriesTree.DataSource = State.DataSource = value;
			}
		}

		public bool DenySelection
		{
			get { return CategoriesTree.DenySelection; }
			set { CategoriesTree.DenySelection = value; }
		}

		public Unit Width
		{
			get { return CategoryPathTextBox.Width; }
			set { CategoryPathTextBox.Width = value; }
		}

		protected void EnsureState()
		{
			if (State == null)
			{
				State = new CategoryNodeSelectorState();
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += SaveButton_Click;
			CancelSaveButton.Click += OnCancel;
			CancelButton.Click += OnCancel;
			CategoriesTree.NodeCommand += OnNodeCommand;
		}

		protected override void PopulateControl()
		{
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			if (NodeCommand != null)
			{
				NodeCommand(this, new CommandEventArgs(e.EventName, e.Id));
			}
		}

		protected void ClearSelectionButtonClick(object sender, EventArgs e)
		{
			DenySelection = true;
			SelectedNodeId = null;
		}

		private void SaveButton_Click(object sender, EventArgs e)
		{
			TreeModalPopupExtender.Hide();
			if (!CategoriesTree.SelectedNodeId.HasValue || DenySelection)
			{
				CategoryPathTextBox.Text = string.Empty;
				return;
			}

			int categoryId = CategoriesTree.SelectedNodeId.Value;
			SelectedNodeId = categoryId;
			PopulatePath(categoryId);
			if (SelectedCategoryChangedEvent != null)
			{
				SelectedCategoryChangedEvent(this, new SelectedCategoryChangedEventArgs { SelectedCategoryId = categoryId });
			}
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			CategoriesTree.SelectedNodeId = SelectedNodeId;
			DenySelection = DataSource.FirstOrDefault(n => n.Id == SelectedNodeId) == null;
		}

		public void PopulatePath(int? categoryId)
		{
			IEnumerable<INode> categories = DataSource;
			INode category = categories.FirstOrDefault(f => f.Id == categoryId);
			if (category == null)
			{
				return;
			}

			var categoriesPath = new StringBuilder(category.Title);

			while (category.ParentId.HasValue)
			{
				var categoryLocal = category; // to avoid access to modified closure
				category = categories.FirstOrDefault(f => f.Id == categoryLocal.ParentId);
				if (category == null || category.Id == categoryLocal.Id)
				{
					break;
				}

				categoriesPath.Insert(0, @"\");
				categoriesPath.Insert(0, category.Title);
			}
			CategoryPathTextBox.Text = categoriesPath.ToString();
		}


		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			CancelSaveButton.OnClientClick = "document.getElementById('" + CancelButton.ClientID + "').click();";
		}
	}

	[Serializable]
	public class CategoryNodeSelectorState
	{
		public int? SelectedNodeId { get; set; }
		public IEnumerable<INode> DataSource { get; set; }
	}

	public class SelectedCategoryChangedEventArgs : EventArgs
	{
		public int? SelectedCategoryId
		{
			get;
			set;
		}
	}
}