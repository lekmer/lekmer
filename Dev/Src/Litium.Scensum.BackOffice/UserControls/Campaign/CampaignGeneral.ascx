﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignGeneral.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CampaignGeneral" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>

<div class="campaign-general">
	<div class="campaign-general-row">
		<div class="input-box campaign-title">
			<span><%=Resources.General.Literal_Title%> *</span>
			<br />
			<asp:TextBox ID="TitleTextBox" runat="server"></asp:TextBox>
		</div>
		<div class="input-box">
			<span><%=Resources.General.Literal_Status%> *</span>
			<br />
			<asp:DropDownList ID="StatusList" runat="server"></asp:DropDownList>
		</div>
		<div class="input-box">
			<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
			<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
		</div>
	</div>
	<br />
	<div class="campaign-general-row">
		<div class="input-box">
			<span><%=Resources.General.Literal_StartDate%></span>
			<br />
			<asp:TextBox ID="StartDateTextBox" runat="server"></asp:TextBox>
			<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
				ImageAlign="AbsMiddle"/>
			<ajaxToolkit:CalendarExtender ID="StartDateCalendarControl" runat="server" TargetControlID="StartDateTextBox" 
				PopupButtonID="StartDateButton" />&nbsp;&nbsp;&nbsp;-&nbsp;
		</div>
		<div class="input-box">
			<span><%=Resources.General.Literal_EndDate%></span>
			<br />
			<asp:TextBox ID="EndDateTextBox" runat="server"></asp:TextBox>
			<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
				ImageAlign="AbsMiddle"/>
			<ajaxToolkit:CalendarExtender ID="EndDateCalendarControl" runat="server" TargetControlID="EndDateTextBox" 
				PopupButtonID="EndDateButton" />
		</div>
		<div class="input-box">
			<span><%=Resources.Campaign.Literal_ApplyToCampaignRegistry%> *</span>
			<br />
			<asp:DropDownList ID="CampaignRegistryList" runat="server"></asp:DropDownList>
		</div>
		<div class="input-box">
			<br />
			<asp:CheckBox ID="ExclusiveCheckBox" Text="<%$Resources:Campaign, Literal_Exclusive%>" TextAlign="Right" runat="server" />
		</div>
	</div>
</div>
