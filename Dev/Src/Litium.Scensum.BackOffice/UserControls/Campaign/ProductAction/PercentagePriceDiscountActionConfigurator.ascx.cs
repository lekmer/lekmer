﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
    public partial class PercentagePriceDiscountActionConfigurator : 
        ProductActionControl<IPercentagePriceDiscountAction>
    {
        protected override void SetEventHandlers()
        {
        	SetIncludeProductEventHandlers();
        	SetExcludeProductEventHandlers();
			SetIncludeCategoryEventHandlers();
			SetExcludeCategoryEventHandlers();

			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
        }

        protected override void PopulateControl() { }

        public override void DataBind(IProductAction dataSource)
        {
            if (dataSource == null) throw new ArgumentNullException("dataSource");

            var action = (IPercentagePriceDiscountAction) dataSource;

            DiscountPercentageTextBox.Text = action.DiscountAmount.ToString(CultureInfo.CurrentCulture);
        	DiscountTypeTextBox.Text = action.ActionType.Title;

            IncludeAllProductsCheckbox.Checked = action.IncludeAllProducts;

            DataBindIncludeProducts(action);
            DataBindExcludeProducts(action);

			DataBindIncludeCategories(action);
			DataBindExcludeCategories(action);

            State = action;
        }

		protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            IncludePanel.Visible = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0;
            RegisterClientScript();
            RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				DataBindProductIncludeGrid(new Collection<IProduct>());
				DataBindCategoryIncludeGrid(new Collection<ICategory>());
			}
			else
			{
				DataBindProductIncludeGrid(ResolveProducts(State.IncludeProducts));
				DataBindCategoryIncludeGrid(ResolveCategories(State.IncludeCategories));
			}
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row, HtmlGenericControl applyAllDiv)
		{
			string scriptShowBlockOption = @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');";
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					@"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');"
					+ scriptShowBlockOption);
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					@"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');"
					+ scriptShowBlockOption);
			}
		}

		protected virtual Collection<int> GetSelectedIdsFromGrid(System.Web.UI.WebControls.GridView gridView)
		{
			var ids = new Collection<int>();
			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					int id = System.Convert.ToInt32(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}
			return ids;
		}

    	public override bool TryGet(out IProductAction entity)
        {
            if (IsValid())
            {
                if (IncludeAllProductsCheckbox.Checked)
                {
                    State.IncludeProducts.Clear();
                    State.IncludeCategories.Clear();
                }

                var action = State;
                
                action.DiscountAmount = decimal.Parse(DiscountPercentageTextBox.Text, CultureInfo.CurrentCulture);
                action.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;

                entity = action;
                return true;
            }
            entity = null;
            return false;
        }

        protected virtual bool IsValid()
        {
            bool isValid = true;

            decimal discount;
            if(!decimal.TryParse(DiscountPercentageTextBox.Text, out discount))
            {
                isValid = false;
                ValidationMessages.Add(Resources.CampaignMessage.DiscountInvalid);
            }
            else if(discount < 0 || discount > 100)
            {
                isValid = false;
				ValidationMessages.Add(Resources.CampaignMessage.DiscountInvalidRange);
            }

            return isValid;
        }

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);

			const string script = "$(function() {" +
								  "ResizePopup('ActionPopupDiv', 0.8, 0.8,  0.06);" +
								  "ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								  "ResizePopup('ProductExcludePopupDiv', 0.8, 0.7, 0.07);" +
								  "ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
								  "ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
			                      "});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);
		}


		protected virtual Collection<IProduct> ResolveProducts(ProductIdDictionary productIds)
		{
			var products = new Collection<IProduct>();
			foreach (var id in productIds)
			{
				var product = IoC.Resolve<IProductSecureService>().GetById(ChannelHelper.CurrentChannel.Id, id.Value);
				if (product == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Product with ID {0} could not be found for channel with ID {1}.",
						id.Value, ChannelHelper.CurrentChannel.Id));
				}
				products.Add(product);
			}
			return products;
		}

		protected virtual Collection<ICategory> ResolveCategories(CategoryIdDictionary categoryIds)
		{
			var categories = new Collection<ICategory>();
			foreach (var id in categoryIds)
			{
				var category = IoC.Resolve<ICategorySecureService>().GetById(id.Value);
				if (category == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Category with ID {0} could not be found.",
						id));
				}
				categories.Add(category);
			}
			return categories;
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			ProductExcludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}
		private static bool HasAnySelectedItem(System.Web.UI.WebControls.GridView grid)
		{
			foreach (GridViewRow row in grid.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					return true;
				}
			}
			return false;
		}
    }
}
