﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction
{
	public partial class ProductDiscountActionConfigurator : ProductActionControl<IProductDiscountAction>
	{
		protected override void SetEventHandlers()
		{
			ProductIncludePopupSearchButton.Click += OnProductIncludeSearch;
			ProductIncludeGrid.RowDataBound += OnProductIncludeGridRowDataBound;
			ProductIncludeGrid.RowCommand += OnProductIncludeGridRowCommand;
			ProductIncludePopupOkButton.Click += OnIncludeProducts;
		    ProductIncludePopupAddAllButton.Click += OnIncludeAllProducts;
			RemoveSelectionFromProductIncludeGridButton.Click += OnRemoveSelectionFromProductIncludeGrid;
		}

	    private void OnIncludeAllProducts(object sender, EventArgs e)
	    {
            if (ProductIncludeSearchFormControl.Validate())
            {
                UpdateDiscounts();

                var productSecureService = IoC.Resolve<IProductSecureService>();


                var criteriaList = new List<IProductSearchCriteria>();
                var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
                criteriaList.Add(criteria);
                var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
                var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

                var products = productSecureService.AdvancedSearch(
                    ChannelHelper.CurrentChannel.Id,
                    searchCriteriaInclusive,
                    searchCriteriaExclusive, 1,
                    500);

                foreach (var product in products)
                {
                    if (State.ProductDiscountPrices.ContainsKey(product.Id)) continue;

                    State.ProductDiscountPrices.Add(product.Id, decimal.Zero);
                }
                DataBindProductIncludeGrid(State.ProductDiscountPrices);
            }
	    }

	    protected override void PopulateControl()
		{
		}

		public override void DataBind(IProductAction dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

            var action = (IProductDiscountAction) dataSource;
			DataBindCurrencyList(action.CurrencyId);
			DataBindProductIncludeGrid(action.ProductDiscountPrices);
			State = action;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();
			SetVisibility();
		}

		public override bool TryGet(out IProductAction entity)
		{
			var isValid = UpdateDiscounts();
			if (isValid)
			{
				State.CurrencyId = int.Parse(CurrencyList.SelectedValue, CultureInfo.CurrentCulture);
				entity = State;
				return true;
			}
			ValidationMessages.Add(Resources.LekmerMessage.Campaign_DiscountPriceIncorrect);
			entity = null;
			return false;
		}

		protected void OnProductIncludeSearch(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				ProductIncludeSearchResultControl.DataBind(criteria);
			}
		}

		protected void OnProductIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ProductIncludeApplyToAllSelectedDiv);
		}

		protected void OnProductIncludeGridRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				UpdateDiscounts();
				var id = int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture);
				State.ProductDiscountPrices.Remove(id);
				DataBindProductIncludeGrid(State.ProductDiscountPrices);
			}
		}

		protected void OnIncludeProducts(object sender, EventArgs e)
		{
			UpdateDiscounts();
			var products = ProductIncludeSearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (State.ProductDiscountPrices.ContainsKey(product.Id)) continue;
				
				State.ProductDiscountPrices.Add(product.Id, decimal.Zero);
			}
			DataBindProductIncludeGrid(State.ProductDiscountPrices);
		}

		protected void OnRemoveSelectionFromProductIncludeGrid(object sender, EventArgs e)
		{
			UpdateDiscounts();
			foreach (var productId in ProductIncludeGrid.GetSelectedIds())
			{
				State.ProductDiscountPrices.Remove(productId);
			}
			DataBindProductIncludeGrid(State.ProductDiscountPrices);
		}

		private static Dictionary<IProduct, decimal> ResolveProducts(Dictionary<int, decimal> productDiscountPrices)
		{
			var products = new Dictionary<IProduct, decimal>();
			foreach (var productDiscountPrice in productDiscountPrices)
			{
				var product = IoC.Resolve<IProductSecureService>().GetById(ChannelHelper.CurrentChannel.Id, productDiscountPrice.Key);
				if (product == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Product with ID {0} could not be found for channel with ID {1}.",
						productDiscountPrice.Key, ChannelHelper.CurrentChannel.Id));
				}
				products.Add(product, productDiscountPrice.Value);
			}
			return products;
		}

		private void DataBindProductIncludeGrid(Dictionary<int, decimal> data)
		{
			ProductIncludeGrid.DataSource = ResolveProducts(data).OrderBy(i => i.Key.ErpId);
			ProductIncludeGrid.DataBind();
			ProductIncludeGridUpdatePanel.Update();
		}

		private void DataBindCurrencyList(int selectedCurrencyId)
		{
			var currencyService = IoC.Resolve<ICurrencySecureService>();
			CurrencyList.DataSource = currencyService.GetAll(SignInHelper.SignedInSystemUser);
			CurrencyList.DataBind();
			var selectedItem = CurrencyList.Items.FindByValue(selectedCurrencyId.ToString(CultureInfo.CurrentCulture));
			if (selectedItem != null) selectedItem.Selected = true;
		}

		private bool UpdateDiscounts()
		{
			var isValid = true;
			foreach (GridViewRow row in ProductIncludeGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow) continue;

				var id = int.Parse(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
				var productDiscountPrice = State.ProductDiscountPrices.First(i => i.Key == id);
				var discountPriceString = ((TextBox)row.FindControl("DiscountPriceBox")).Text;
				decimal discountPrice;
				if (decimal.TryParse(discountPriceString, NumberStyles.Currency, CurrencyCulture, out discountPrice) && discountPrice >= 0)
				{
					State.ProductDiscountPrices.Remove(productDiscountPrice.Key);
					State.ProductDiscountPrices.Add(productDiscountPrice.Key, discountPrice);
				}
				else
				{
					isValid = false;
				}
			}
			return isValid;
		}

		private readonly Collection<ICategory> _categories = new Collection<ICategory>();
		protected string GetCategory(int categoryId)
		{
			var category = _categories.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				category = categorySecureService.GetById(SignInHelper.SignedInSystemUser, categoryId);
				_categories.Add(category);
			}
			return category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(int statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			var status = _productStatuses.FirstOrDefault(s => s.Id == statusId);
			return status != null ? status.Title : string.Empty;
		}

		private CultureInfo _currencyCulture;
		protected CultureInfo CurrencyCulture
		{
			get
			{
				if (_currencyCulture == null)
				{
					_currencyCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
					_currencyCulture.NumberFormat.CurrencySymbol = string.Empty;
					_currencyCulture.NumberFormat.NumberGroupSizes = _currencyCulture.NumberFormat.CurrencyGroupSizes;
					_currencyCulture.NumberFormat.NumberGroupSeparator = _currencyCulture.NumberFormat.CurrencyGroupSeparator;
					_currencyCulture.NumberFormat.NumberDecimalSeparator = _currencyCulture.NumberFormat.CurrencyDecimalSeparator;
					_currencyCulture.NumberFormat.NumberDecimalDigits = _currencyCulture.NumberFormat.CurrencyDecimalDigits;
				}
				return _currencyCulture;
			}
		}

		private static void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row, Control applyAllDiv)
		{
			string scriptShowBlockOption = @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');";
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					@"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');"
					+ scriptShowBlockOption);
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					@"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');"
					+ scriptShowBlockOption);
			}
		}

		private void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_pdac_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);

			const string script = "$(function() {" +
								  "ResizePopup('ActionPopupDiv', 0.8, 0.8,  0.06);" +
								  "ResizePopup('ProductIncludePopupDiv', 0.8, 0.6, 0.07);" +
								  "});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_pdac_popupResize", script, true);
		}

		private void RegisterCollapsiblePanelScripts()
		{
			var includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
		}

		private void SetVisibility()
		{
			if (ProductIncludeGrid.Rows.Count > 0)
			{
				IncludePanelDiv.Style.Remove("display");
			}
			else
			{
				IncludePanelDiv.Style.Add("display", "none");
			}

			ProductIncludeApplyToAllSelectedDiv.Style["display"] = ProductIncludeGrid.GetSelectedIds().Count() > 0 
				? "block" : "none";
		}
	}
}