﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConditionList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.ConditionList" %>
<div class="campaign-action-grid">
	<asp:GridView ID="ConditionsGrid" AutoGenerateColumns="false" OnRowDataBound="OnGridRowDataBound" runat="server" SkinID="campaign-actions-grid" Width="100%" BorderWidth="0" CellPadding="0">
		<Columns>
			<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="29%">
				<ItemTemplate>
					<asp:LinkButton ID="EditButton" CommandName="Configure" runat="server"></asp:LinkButton>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Description %>" ItemStyle-Width="55.5%">
				<ItemTemplate>
					<asp:Literal ID="DescriptionLiteral" runat="server"></asp:Literal>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField ItemStyle-Width="3.5%" ItemStyle-HorizontalAlign="Right">
				<ItemTemplate>
					<asp:ImageButton ID="RemoveButton" runat="server" CommandName="Remove" 
						ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmActionDelete();" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
	
	</asp:GridView>
</div>