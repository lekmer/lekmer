using System.Collections.ObjectModel;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
    public abstract class ProductActionControl<TProductAction> : StateUserControlController<TProductAction>, ICampaignPluginControl<IProductAction>
	{
		private Collection<string> _validationMessages;
    	public Collection<string> ValidationMessages
    	{
    		get
    		{
				if (_validationMessages == null)
				{
					_validationMessages = new Collection<string>();
				}
				return _validationMessages;
    		}
    	}

        public abstract void DataBind(IProductAction dataSource);
        public abstract bool TryGet(out IProductAction entity);
    }
}