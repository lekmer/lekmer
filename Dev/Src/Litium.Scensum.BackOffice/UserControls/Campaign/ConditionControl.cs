using System.Collections.ObjectModel;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
    public abstract class ConditionControl<TCondition> : StateUserControlController<TCondition>, ICampaignPluginControl<ICondition>
	{
		private Collection<string> _validationMessages;
		public Collection<string> ValidationMessages
		{
			get
			{
				if (_validationMessages == null)
				{
					_validationMessages = new Collection<string>();
				}
				return _validationMessages;
			}
		}

        public abstract void DataBind(ICondition dataSource);
        public abstract bool TryGet(out ICondition entity);
    }
}