﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreightValueActionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.FreightValueActionConfigurator" %>
<%@ Import Namespace="System.Globalization"%>
<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>

<br style="clear:both;" />
<scensum:CurrencyValueManager runat="server" id="AmountsManager" />