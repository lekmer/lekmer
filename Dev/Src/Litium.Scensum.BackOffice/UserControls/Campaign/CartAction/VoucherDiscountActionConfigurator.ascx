﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherDiscountActionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.VoucherDiscountActionConfigurator" %>

<br style="clear:both;" />


<div class="column">
	<span>	<%= Resources.Lekmer.Literal_DiscountType%> </span><br />
        <div class="column">
	        <asp:RadioButton ID="FixedRadioButton" GroupName="ValueType " runat="server"  />	
	        <span>	<%= Resources.Lekmer.Literal_FixedType%> </span>
	   
	        <br />
	    	
	<asp:RadioButton ID="PercentageRadioButton" runat="server" GroupName="ValueType " />
			<span>					<%= Resources.Lekmer.Literal_PercentageType%> </span>
			
      <br />
        <br />
    <span>	<%= Resources.Lekmer.Literal_DiscountValue%>* </span>
				<br />
	        	<asp:TextBox ID="DiscountValueTextBox"  runat="server" />	
	        
	 </div>
</div>
