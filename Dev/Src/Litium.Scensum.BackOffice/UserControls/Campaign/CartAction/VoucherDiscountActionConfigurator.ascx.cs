﻿using System;
using System.Globalization;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
    public partial class VoucherDiscountActionConfigurator :
        CartActionControl<IVoucherDiscountAction>
    {
        public override void DataBind(ICartAction dataSource)
        {
            var voucherDiscountAction = (IVoucherDiscountAction)dataSource;
            State = voucherDiscountAction;
            FixedRadioButton.Checked = State.FixedDiscount;
            PercentageRadioButton.Checked = State.PercentageDiscount;
            DiscountValueTextBox.Text = State.DiscountValue.ToString(CultureInfo.CurrentCulture);;
        }

        public override bool TryGet(out ICartAction entity)
        {
            decimal discountValue;
            State.FixedDiscount = FixedRadioButton.Checked;
            State.PercentageDiscount = PercentageRadioButton.Checked;
            entity = State;
            if (DiscountValueTextBox.Text.Trim().Length == 0)
            {
                ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.DefaultPriceFromValueShouldNotBeEmpty));
                return false;
            }
            if (!decimal.TryParse(DiscountValueTextBox.Text.Trim(), out discountValue))
            {
                ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.DiscountValueShouldBeDecimal));
                return false;
            }
            State.DiscountValue = discountValue;
            entity.Status = BusinessObjectStatus.Edited;
            return true;
        }

        protected override void PopulateControl()
        {
        }

        protected override void SetEventHandlers()
        {
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            const string script = "$(function() {" +
                "var popupDiv = $(\"div[id$='ActionPopupDiv']\");" +
                "popupDiv.attr('class','campaign-popup-container small-campaign-popup-container');" +
                "$(popupDiv).find(\"div[class*='campaign-popup-header-center']:first\").attr('class','campaign-popup-header-center small-campaign-popup-header-center');" +
                "$(popupDiv).find(\"div[class*='campaign-popup-content']:first\").attr('class','campaign-popup-content small-campaign-popup-content');" +
                "});";
            ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_fvac_smallPopup", script, true);
        }
    }
}