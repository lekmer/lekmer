﻿using System;
using System.Collections.ObjectModel;
using System.Web.UI;
using Litium.Scensum.Campaign;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
    public partial class FreightValueActionConfigurator : 
        CartActionControl<IFreightValueAction>
    {
        protected override void SetEventHandlers() { }
        protected override void PopulateControl() { }

        public override void DataBind(ICartAction dataSource)
        {
            var freightValueAction = (IFreightValueAction)dataSource;

        	AmountsManager.DataSource = freightValueAction.Amounts;
        	AmountsManager.DataBind();

            State = freightValueAction;
        }
        public override bool TryGet(out ICartAction entity)
        {
			if (IsValid())
			{
				var action = State;
				action.Amounts = AmountsManager.CollectCurrencyValues();
				entity = action;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			Collection<string> amountsValidationMessages;
			if (!AmountsManager.IsValid(out amountsValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in amountsValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			const string script = "$(function() {" +
				"var popupDiv = $(\"div[id$='ActionPopupDiv']\");" +
				"popupDiv.attr('class','campaign-popup-container small-campaign-popup-container');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-header-center']:first\").attr('class','campaign-popup-header-center small-campaign-popup-header-center');" +
				"$(popupDiv).find(\"div[class*='campaign-popup-content']:first\").attr('class','campaign-popup-content small-campaign-popup-content');" +
				"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_fvac_smallPopup", script, true);
		}
    }
}