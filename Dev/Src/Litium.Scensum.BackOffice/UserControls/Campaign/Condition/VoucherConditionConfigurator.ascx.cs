﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Resources;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
    public partial class VoucherConditionConfigurator : ConditionControl<IVoucherCondition>
    {
        protected override void SetEventHandlers()
        {
            BatchGrid.RowDataBound += OnBatchGridRowDataBound;
            BatchGrid.RowCommand += OnBatchGridRowCommand;
            OpenBatchSearchPopupButton.Click += OpenBatchSearchPopupButton_Click;

            RemoveSelectionFromBatchGridButton.Click += OnRemoveSelectionFromBatchGrid;
            BatchSaveButton.Click += BatchSaveButton_Click;
        }

        protected void BatchSaveButton_Click(object sender, EventArgs e)
        {
            int batchId = int.Parse(BatchIdTextBox.Text.Trim(), CultureInfo.CurrentCulture);
            foreach (int id in State.BatchIds)
            {
                if (id == batchId)
                {
                    BatchMessageContainer.Add(LekmerMessage.ExistBatchId);
                    BatchPopup.Show();
                    return;
                }
            }
            BatchIdTextBox.Text = string.Empty;
            State.BatchIds.Add(batchId);
            DataBindBatchGrid(State.BatchIds);
            BatchPopup.Hide();
        }

        protected void OpenBatchSearchPopupButton_Click(object sender, EventArgs e)
        {
            BatchUpdatePanel.Update();
            BatchPopup.Show();
        }

        protected void OnRemoveSelectionFromBatchGrid(object sender, EventArgs e)
        {
            foreach (var batchId in GetSelectedBatchsFromIncludeGrid())
            {
                State.BatchIds.Remove(batchId);
            }
            DataBindBatchGrid(State.BatchIds);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected Collection<int> GetSelectedBatchsFromIncludeGrid()
        {
            return GetSelectedIdsFromGrid(BatchGrid);
        }

        protected static Collection<int> GetSelectedIdsFromGrid(System.Web.UI.WebControls.GridView gridView)
        {
            var ids = new Collection<int>();
            foreach (GridViewRow row in gridView.Rows)
            {
                var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
                if (selectCheckBox.Checked)
                {
                    var batchIdHiddenField = (HiddenField)row.FindControl("BatchIdHiddenField");
                    int id = int.Parse(batchIdHiddenField.Value, CultureInfo.CurrentCulture);
                    ids.Add(id);
                }
            }
            return ids;
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected void OnBatchGridRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("RemoveBatch"))
            {
                RemoveBatch(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
            }
        }

        protected void RemoveBatch(int batchId)
        {
            State.BatchIds.Remove(batchId);
            DataBindBatchGrid(State.BatchIds);
        }

        protected void DataBindBatchGrid(Collection<int> batchIds)
        {
            BatchGrid.DataSource = batchIds;
            BatchGrid.DataBind();
            BatchGridUpdatePanel.Update();
        }
        protected void OnBatchGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
            SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, BatchApplyToAllSelectedDiv);
        }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected static void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row, HtmlGenericControl applyAllDiv)
        {
            string scriptShowBlockOption = @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');";
            if (row.RowType == DataControlRowType.Header)
            {
                var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
                selectAllCheckBox.Checked = false;
                selectAllCheckBox.Attributes.Add("onclick",
                    @"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');"
                    + scriptShowBlockOption);
            }
            else if (row.RowType == DataControlRowType.DataRow)
            {
                var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
                var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
                if (cbSelect == null || selectAllCheckBox == null) return;
                cbSelect.Attributes.Add("onclick",
                    @"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');"
                    + scriptShowBlockOption);

                var batchLiteral = (Label)row.FindControl("BatchIdLiteral");
                batchLiteral.Text = ((int)row.DataItem).ToString(CultureInfo.CurrentCulture);
                var batchIdHiddenField = (HiddenField)row.FindControl("BatchIdHiddenField");
                batchIdHiddenField.Value = ((int)row.DataItem).ToString(CultureInfo.CurrentCulture);
            }
        }

        protected override void PopulateControl()
        {
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterCollapsiblePanelScripts();

            SetVisibility();
        }
        private void SetVisibility()
        {
            BatchApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(BatchGrid) ? "block" : "none";
        }
        private static bool HasAnySelectedItem(System.Web.UI.WebControls.GridView grid)
        {
            foreach (GridViewRow row in grid.Rows)
            {
                var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
                if (selectCheckBox.Checked)
                {
                    return true;
                }
            }
            return false;
        }
        protected virtual void RegisterCollapsiblePanelScripts()
        {
            string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
            IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
            IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);
        }
        public override void DataBind(ICondition dataSource)
        {
            if (dataSource == null) throw new ArgumentNullException("dataSource");

            var condition = (IVoucherCondition)dataSource;
            DataBindBatchGrid(condition.BatchIds);
            State = condition;
        }

        public override bool TryGet(out ICondition entity)
        {
            var condition = State;
            entity = condition;
            return true;
        }
    }
}
