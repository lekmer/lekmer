﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartContainsConditionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.Condition.CartContainsConditionConfigurator" %>
<%@ Import Namespace="System.Globalization"%>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="uc" Src="~/UserControls/Tree/SelectTreeView.ascx" TagName="CategoryTree" %>

<script type="text/javascript">
	$(function() {
		$("div[id$='ConditionPopupDiv']").attr('class', 'campaign-popup-container');
		ResizePopup('ConditionPopupDiv', 0.75, 0.7, 0.065);
		$("div[class^='campaign-popup-content']").css('overflow-y', 'scroll');
	});	
    function confirmDeleteProducts() {
    	return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
    }
    function confirmDeleteCategories() {
    	return DeleteConfirmation("<%= Resources.Campaign.Literal_CategoriesConfirmRemove %>");
    }
</script>

<div class="campaign-full-left">
	<div class="input-box campaign-action-value">
		<asp:Label ID="MinQuantityLabel" Text="<%$Resources:CartContainsCondition, Literal_MinQuantity%>" runat="server"></asp:Label>
		<br />
		<asp:TextBox ID="MinQuantityTextBox" runat="server"></asp:TextBox>
	</div>
	<div class="input-box campaign-action-value">
		<asp:Label ID="AllowDuplicatesLabel" Text="<%$Resources:CartContainsCondition, Literal_AllowDuplicates%>" runat="server"></asp:Label>
		<br />
		<asp:CheckBox ID="AllowDuplicatesCheckBox" runat="server" />
	</div>
</div>
<br class="clear" />
<br />
<div class="campaign-full-left">

<!-- Include Toolbar Start -->

	<div id="includeToolbar">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenIncludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
						<asp:Button ID="OpenIncludeCategorySearchPopupButton" Text="<%$Resources:Product, Button_Category%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content">
			<asp:Label ID="IncludeAllProductsLabel" Text="<%$Resources:Campaign, Literal_IncludeAllProducts%>" runat="server" CssClass="text-bold"></asp:Label>
			<br />
			<asp:CheckBox ID="IncludeAllProductsCheckbox" AutoPostBack="true" runat="server" Text="<%$Resources:Campaign, Literal_AllProductsInRange%>" />
			<br />
			<br />
			<asp:Panel ID="IncludePanel" runat="server">
				<div style="padding-top:15px;clear:both;"></div>
				<asp:Label ID="LabelProducts" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="ProductIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="ProductIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="49%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Label_Categories %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price  %>" HeaderStyle-HorizontalAlign="Right"
									ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="PriceLiteral" Text='<%# Convert.ToDecimal(Eval("Price.PriceIncludingVat")).ToString("C", CultureInfo.CurrentCulture) %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmProductRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="ProductIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>

				<div style="padding-top:25px;clear:both;"></div>
				<asp:Label ID="Label1" Text="<%$Resources:Product, Label_Categories%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="CategoryIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="CategoryIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="30%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:CartContainsCondition, Literal_Path %>" ItemStyle-Width="48%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="PathLiteral" Text='<%# GetCategoryPath(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:CartContainsCondition, Literal_IncludeSubcategories %>" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="16%">
									<ItemTemplate>
										<asp:CheckBox ID="IncludeSubcategoryCheckBox" runat="server" Checked='<%# GetIncludeSubcategories(Eval("Id"))%>' AutoPostBack="true" OnCheckedChanged="OnIncludeSubcategoriesChanged" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCategory" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmCategoryRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="CategoryIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCategoryIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteCategories();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>
	
<!-- Include Toolbar End -->

	<br class="clear"/><br />

<!-- Exclude Toolbar Start -->

	<div id="excludeToolbar">
		<div class="collapsible-items left">
			<div id="ExcludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="ExcludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/exclude.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Exclude%></span>
				</div>
				<div id="ExcludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenExcludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
						<asp:Button ID="OpenExcludeCategorySearchPopupButton" Text="<%$Resources:Product, Button_Category%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="ExcludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="ExcludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<asp:UpdatePanel ID="ExcludeUpdatePanel" UpdateMode="Always" runat="server">
			<ContentTemplate>
				<div id="ExcludePanelDiv" runat="server" class="campaign-include-content">
					<asp:Panel ID="ExcludePanel" runat="server">
						<div style="padding-top:15px;clear:both;"></div>
						<asp:Label ID="Label2" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
						<br />
						<asp:UpdatePanel ID="ProductExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
							<ContentTemplate>
								<asp:GridView ID="ProductExcludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<HeaderTemplate>
												<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox ID="SelectCheckBox" runat="server" />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="10%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="49%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:Product, Label_Categories %>" ItemStyle-Width="15%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="10%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price  %>" HeaderStyle-HorizontalAlign="Right"
											ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="PriceLiteral" Text='<%# Convert.ToDecimal(Eval("Price.PriceIncludingVat")).ToString("C", CultureInfo.CurrentCulture) %>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
													ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
													OnClientClick="return ConfirmProductRemove();" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
								<div runat="server" id="ProductExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
									<div style="float:left;padding-top:6px;">
										<div class="apply-to-all">
											<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
										</div>
									</div>
									<div style="float:left">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton"/>
									</div>
								</div>
							</ContentTemplate>
						</asp:UpdatePanel>
						
						<div style="padding-top:25px;clear:both;"></div>
						<asp:Label ID="Label3" Text="<%$Resources:Product, Label_Categories%>" runat="server" CssClass="text-bold"></asp:Label>
						<br />
						<asp:UpdatePanel ID="CategoryExcludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
							<ContentTemplate>
								<asp:GridView ID="CategoryExcludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
									<Columns>
										<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<HeaderTemplate>
												<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
											</HeaderTemplate>
											<ItemTemplate>
												<asp:CheckBox ID="SelectCheckBox" runat="server" />
												<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="30%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:CartContainsCondition, Literal_Path %>" ItemStyle-Width="48%">
											<ItemTemplate>
												<uc:LiteralEncoded runat="server" ID="PathLiteral" Text='<%# GetCategoryPath(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="<%$ Resources:CartContainsCondition, Literal_IncludeSubcategories %>" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="16%">
											<ItemTemplate>
												<asp:CheckBox ID="IncludeSubcategoryCheckBox" runat="server" Checked='<%# GetExcludeSubcategories(Eval("Id"))%>' AutoPostBack="true" OnCheckedChanged="OnExcludeSubcategoriesChanged" />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
											<ItemTemplate>
												<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCategory" CommandArgument='<%# Eval("Id") %>'
													ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
													OnClientClick="return ConfirmCategoryRemove();" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
								<div runat="server" id="CategoryExcludeApplyToAllSelectedDiv" class="apply-to-all-selected">
									<div style="float:left;padding-top:6px;">
										<div class="apply-to-all">
											<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
										</div>
									</div>
									<div style="float:left">
										<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCategoryExcludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteCategories();" runat="server" SkinID="DefaultButton"/>
									</div>
								</div>
							</ContentTemplate>
						</asp:UpdatePanel>
					</asp:Panel>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
	</div>
	
<!-- Exclude Toolbar End -->

</div>
<br class="clear"/><br />

<!-- Category Search Popup Start -->
	<!-- Include Start -->
	
<div id="CategoryIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="CategoryIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="CategoryIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<uc:CategoryTree runat="server" ID="CategoryIncludePopupCategoryTree" />
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryIncludePopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="CategoryIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeCategorySearchPopupButton"
	PopupControlID="CategoryIncludePopupDiv" 
	CancelControlID="CategoryIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	
	<!-- Include End -->
	
	<!-- Exclude Start -->
	
<div id="CategoryExcludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="CategoryExcludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="CategoryExcludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<uc:CategoryTree runat="server" ID="CategoryExcludePopupCategoryTree" />
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryExcludePopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryExcludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>

<ajaxToolkit:ModalPopupExtender 
	ID="CategoryExcludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenExcludeCategorySearchPopupButton"
	PopupControlID="CategoryExcludePopupDiv" 
	CancelControlID="CategoryExcludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	
	<!-- Exclude End -->		
<!-- Category Search Popup End -->

<!-- Product Search Popup Start -->
	<!-- Include Start -->

<div id="ProductIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="ProductIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="ProductIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="campaign-product-search">
					<div class="content-box">
						<asp:Panel ID="SearchPanel" DefaultButton="ProductIncludePopupSearchButton" runat="server">
						<uc:ProductSearchForm ID="ProductIncludeSearchFormControl" runat="server" />
						<br style="clear:both;" />
						<br />
						<div class="campaign-popup-buttons no-padding">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
						</div>
						</asp:Panel>
						<br class="clear"/>
						<br />
						<uc:ProductSearchResult ID="ProductIncludeSearchResultControl" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
            <uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
		
<ajaxToolkit:ModalPopupExtender 
	ID="ProductIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeProductSearchPopupButton"
	PopupControlID="ProductIncludePopupDiv" 
	CancelControlID="ProductIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	
	<!-- Include End -->
	
	<!-- Exclude Start -->
	
<div id="ProductExcludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="ProductExcludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="ProductExcludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="campaign-product-search">
					<div class="content-box">
						<asp:Panel ID="Panel1" DefaultButton="ProductExcludePopupSearchButton" runat="server">
						<uc:ProductSearchForm ID="ProductExcludeSearchFormControl" runat="server" />
						<br style="clear:both;" />
						<br />
						<div class="campaign-popup-buttons no-padding">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
						</div>
						</asp:Panel>
						<br class="clear"/>
						<br />
						<uc:ProductSearchResult ID="ProductExcludeSearchResultControl" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
            <uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" Visible="false" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupOkButton" Text="<%$Resources:General, Button_Add %>" Visible="false" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductExcludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
		
<ajaxToolkit:ModalPopupExtender 
	ID="ProductExcludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenExcludeProductSearchPopupButton"
	PopupControlID="ProductExcludePopupDiv" 
	CancelControlID="ProductExcludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	
	<!-- Exclude End -->
<!-- Product Search Popup End -->

