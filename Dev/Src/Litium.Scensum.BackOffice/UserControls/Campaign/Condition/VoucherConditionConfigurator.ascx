﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoucherConditionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.Condition.VoucherConditionConfigurator" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<script type="text/javascript">
    $(function() {
        $("div[id$='ConditionPopupDiv']").attr('class', 'campaign-popup-container');
        ResizePopup('ConditionPopupDiv', 0.55, 0.7, 0.065);
        $("div[class^='campaign-popup-content']").css('overflow-y', 'scroll');
    });
    function CloseBatchSelect() {
        $('.BatchCancelButton').click();
    }

    function confirmDeleteBatchIds() {
        return DeleteConfirmation("<%= Resources.Lekmer.Literal_DeleteBatchIds  %>");
    }

</script>
<div class="campaign-full-left">

<!-- Include Toolbar Start -->
	<div id="includeToolbar">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenBatchSearchPopupButton" Text="<%$Resources:Lekmer, Literal_BatchId%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content">			
				<br />
			<asp:Panel ID="IncludePanel" runat="server">
			
				<asp:UpdatePanel ID="BatchGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="BatchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField runat="server" id="BatchIdHiddenField" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_BatchId %>" ItemStyle-Width="70%">
									<ItemTemplate>
										<asp:Label runat="server" ID="BatchIdLiteral" ></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>	
									<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveBatch" CommandArgument='<%# ((int)Container.DataItem).ToString() %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Lekmer.Literal_DeleteBatchId + "\");" %>' />
									</ItemTemplate>
								</asp:TemplateField>							
							</Columns>
						</asp:GridView>
						<div runat="server" id="BatchApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromBatchGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteBatchIds();" runat="server" SkinID="DefaultButton"/>
						
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
				
			</asp:Panel>
		</div>
	</div>
	


</div>
<asp:HiddenField runat="server" ID="FakeControl" />
<div id="BatchViewDiv" runat="server" class="customer-comment-mainDiv" style="z-index: 10;
	display: none;">
	<div id="customer-comment-header">
		<div id="customer-comment-header-left">
		</div>
		<div id="customer-comment-header-center">
			<span>
				<%= Resources.Lekmer.Label_AddBatch%></span>
			   <input runat="server" type="button" id="BatchCancelButton" class="BatchCancelButton"  value="x"  />
		</div>
		<div id="customer-comment-header-right">
		</div>
	</div>
	
<asp:UpdatePanel ID="BatchUpdatePanel" runat="server" UpdateMode="Conditional">
<ContentTemplate>
	<div id="customer-freight-subMainDiv">
	<asp:Panel ID="Panel1" runat="server" DefaultButton="BatchSaveButton">
		<uc:MessageContainer runat="server" ID="BatchMessageContainer" MessageType="Warning"  HideMessagesControlId="BatchSaveButton" />
		<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="BatchValidationGroup" />
		<div class="column1">
			<div class="input-box">
				<span>
					<%= Resources.Lekmer.Literal_BatchId%> *</span>
				<asp:RequiredFieldValidator ID="BatchValidator" runat="server" ControlToValidate="BatchIdTextBox"
					ErrorMessage="<%$ Resources:LekmerMessage,EmptyBatchId%>" Display="None" ValidationGroup="BatchValidationGroup" />
				<asp:RegularExpressionValidator ID="ValueValidator" runat="server" ValidationGroup="BatchValidationGroup" ControlToValidate="BatchIdTextBox" 
				ValidationExpression="\d*" Text="<%$ Resources:InterfaceMessage, OnlyNumericData  %>"/>
				<br />
				<asp:TextBox ID="BatchIdTextBox"  runat="server" />
			</div>				           
			<br clear="all" />
		</div>
		<br />
		<div class="customer-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="false"  ID="BatchSaveButton" runat="server" Text="<%$ Resources:General, Button_Next %>"
				SkinID="DefaultButton" ValidationGroup="BatchValidationGroup" />
				        <div class="button-container" onclick="CloseBatchSelect();" style="cursor: pointer">
                                <span class="button-left-border"></span><span class="button-middle"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:General, Button_Cancel%>"></asp:Literal></span>
                                <span class="button-right-border"></span>
                            </div>
		</div>
			</asp:Panel>
	</div>
	</ContentTemplate> 
</asp:UpdatePanel>
</div>

	<ajaxToolkit:ModalPopupExtender ID="BatchPopup" runat="server" TargetControlID="FakeControl"
		PopupControlID="BatchViewDiv" CancelControlID="BatchCancelButton"  />