using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.CartContainsCondition;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CartContainsConditionConfigurator
	{
		private void SetIncludeProductEventHandlers()
		{
			ProductIncludePopupSearchButton.Click += OnProductIncludeSearch;
			ProductIncludeGrid.RowDataBound += OnProductIncludeGridRowDataBound;
			ProductIncludeGrid.RowCommand += OnProductIncludeGridRowCommand;
			ProductIncludePopupOkButton.Click += OnIncludeProducts;
            ProductIncludePopupAddAllButton.Click += OnIncludeAllProducts;
			RemoveSelectionFromProductIncludeGridButton.Click += OnRemoveSelectionFromProductIncludeGrid;
		}

		private void DataBindIncludeProducts(ICartContainsCondition condition)
		{
			DataBindProductIncludeGrid(ResolveProducts(condition.IncludeProducts));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "criteria")]
		protected virtual void OnProductIncludeSearch(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				ProductIncludeSearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void OnProductIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ProductIncludeApplyToAllSelectedDiv);
		}

		protected virtual void OnProductIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				RemoveIncludedProduct(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveIncludedProduct(int productId)
		{
			State.IncludeProducts.Remove(productId);
			DataBindProductIncludeGrid(ResolveProducts(State.IncludeProducts));
		}

		protected virtual void OnIncludeProducts(object sender, EventArgs e)
		{
			var products = ProductIncludeSearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (!State.IncludeProducts.ContainsKey(product.Id))
				{
					State.IncludeProducts.Add(product.Id);
				}
			}
			DataBindProductIncludeGrid(ResolveProducts(State.IncludeProducts));
			if (products.Count > 0)
			{
				IncludeAllProductsCheckbox.Checked = false;
			}
		}

        private void OnIncludeAllProducts(object sender, EventArgs e)
        {

            if (ProductIncludeSearchFormControl.Validate())
            {

                var productSecureService = IoC.Resolve<IProductSecureService>();

                var criteriaList = new List<IProductSearchCriteria>();
                var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
                criteriaList.Add(criteria);
                var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
                var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

                var products = productSecureService.AdvancedSearch(
                    ChannelHelper.CurrentChannel.Id,
                    searchCriteriaInclusive,
                    searchCriteriaExclusive, 1,
                    500);


                foreach (var product in products)
                {
                    if (!State.IncludeProducts.ContainsKey(product.Id))
                    {
                        State.IncludeProducts.Add(product.Id);
                    }
                }
                DataBindProductIncludeGrid(ResolveProducts(State.IncludeProducts));
                if (products.Count > 0)
                {
                    IncludeAllProductsCheckbox.Checked = false;
                }
            }
        }

		protected virtual void OnRemoveSelectionFromProductIncludeGrid(object sender, EventArgs e)
		{
			foreach (var productId in GetSelectedProductsFromIncludeGrid())
			{
				State.IncludeProducts.Remove(productId);
			}
			DataBindProductIncludeGrid(ResolveProducts(State.IncludeProducts));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedProductsFromIncludeGrid()
		{
			return GetSelectedIdsFromGrid(ProductIncludeGrid);
		}

		protected virtual void DataBindProductIncludeGrid(Collection<IProduct> products)
		{
			ProductIncludeGrid.DataSource = products;
			ProductIncludeGrid.DataBind();
			ProductIncludeGridUpdatePanel.Update();
		}
	}
}