﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartContainsCondition;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CartContainsConditionConfigurator : ConditionControl<ICartContainsCondition>
	{
		protected override void SetEventHandlers()
		{
			SetIncludeProductEventHandlers();
			SetExcludeProductEventHandlers();
			SetIncludeCategoryEventHandlers();
			SetExcludeCategoryEventHandlers();

			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
		}

		protected override void PopulateControl() { }

		public override void DataBind(ICondition dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var condition = (ICartContainsCondition)dataSource;
			State = condition;

			MinQuantityTextBox.Text = condition.MinQuantity.ToString(CultureInfo.CurrentCulture);
			AllowDuplicatesCheckBox.Checked = condition.AllowDuplicates;
			IncludeAllProductsCheckbox.Checked = condition.IncludeAllProducts;

			DataBindIncludeProducts(condition);
			DataBindExcludeProducts(condition);

			DataBindIncludeCategories(condition);
			DataBindExcludeCategories(condition);
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			IncludePanel.Visible = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0;
			RegisterClientScript();
			RegisterCollapsiblePanelScripts();

			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				DataBindProductIncludeGrid(new Collection<IProduct>());
				DataBindCategoryIncludeGrid(new Collection<ICategory>());
			}
			else
			{
				DataBindProductIncludeGrid(ResolveProducts(State.IncludeProducts));
				DataBindCategoryIncludeGrid(ResolveCategories(State.IncludeCategories));
			}
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row, HtmlGenericControl applyAllDiv)
		{
			string scriptShowBlockOption = @"ShowBlockOption(this,'" + applyAllDiv.ClientID + @"');";
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick",
					@"SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');"
					+ scriptShowBlockOption);
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick",
					@"UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');"
					+ scriptShowBlockOption);
			}
		}

		protected virtual Collection<int> GetSelectedIdsFromGrid(System.Web.UI.WebControls.GridView gridView)
		{
			var ids = new Collection<int>();
			foreach (GridViewRow row in gridView.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					int id = System.Convert.ToInt32(((HiddenField)row.FindControl("IdHiddenField")).Value, CultureInfo.CurrentCulture);
					ids.Add(id);
				}
			}
			return ids;
		}

		public override bool TryGet(out ICondition entity)
		{
			if (IsValid())
			{
				if (IncludeAllProductsCheckbox.Checked)
				{
					State.IncludeProducts.Clear();
					State.IncludeCategories.Clear();
				}

				var condition = State;
                condition.MinQuantity = int.Parse(MinQuantityTextBox.Text, CultureInfo.CurrentCulture);
				condition.AllowDuplicates = AllowDuplicatesCheckBox.Checked;
				condition.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;
                entity = condition;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;
            int minQuantity;
			if (!int.TryParse(MinQuantityTextBox.Text, out minQuantity) || minQuantity < 0)
			{
				isValid = false;
				ValidationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.CartContainsCondition.Message_MinQuantityInvalid, 0, int.MaxValue));
			}
            return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);

			const string script = "$(function() {" +
								  "ResizePopup('ProductIncludePopupDiv', 0.8, 0.6, 0.07);" +
								  "ResizePopup('ProductExcludePopupDiv', 0.8, 0.6, 0.07);" +
								  "ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
								  "ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
								  "});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl("~/Media/Images/Common/down.gif") + "','" + ResolveClientUrl("~/Media/Images/Common/up.gif") + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);
		}


		protected virtual Collection<IProduct> ResolveProducts(ProductIdDictionary productIds)
		{
			var products = new Collection<IProduct>();
			foreach (var id in productIds)
			{
				var product = IoC.Resolve<IProductSecureService>().GetById(ChannelHelper.CurrentChannel.Id, id.Value);
				if (product == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Product with ID {0} could not be found for channel with ID {1}.",
						id.Value, ChannelHelper.CurrentChannel.Id));
				}
				products.Add(product);
			}
			return products;
		}

		protected virtual Collection<ICategory> ResolveCategories(CategoryDictionary categoryDictionary)
		{
			var categories = new Collection<ICategory>();
			foreach (var id in categoryDictionary)
			{
				var category = IoC.Resolve<ICategorySecureService>().GetById(SignInHelper.SignedInSystemUser, id.Key);
				if (category == null)
				{
					throw new InvalidOperationException(
						string.Format(
						CultureInfo.CurrentCulture,
						"Category with ID {0} could not be found.",
						id));
				}
				categories.Add(category);
			}
			return categories;
		}

		private ICategory _category;
		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(SignInHelper.SignedInSystemUser, categoryId);
			}
			return _category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			ProductExcludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			ExcludePanelDiv.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}
		private static bool HasAnySelectedItem(System.Web.UI.WebControls.GridView grid)
		{
			foreach (GridViewRow row in grid.Rows)
			{
				var selectCheckBox = (CheckBox)row.FindControl("SelectCheckBox");
				if (selectCheckBox.Checked)
				{
					return true;
				}
			}
			return false;
		}
	}
}
