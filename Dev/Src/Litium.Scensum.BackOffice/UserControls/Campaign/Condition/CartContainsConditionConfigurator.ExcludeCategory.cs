using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.CartContainsCondition;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CartContainsConditionConfigurator
	{
		private void SetExcludeCategoryEventHandlers()
		{
			CategoryExcludeGrid.RowDataBound += OnCategoryExcludeGridRowDataBound;
			CategoryExcludeGrid.RowCommand += OnCategoryExcludeGridRowCommand;
			CategoryExcludePopupOkButton.Click += OnExcludeCategories;
			RemoveSelectionFromCategoryExcludeGridButton.Click += OnRemoveSelectionFromCategoryExcludeGrid;
		}

		private void DataBindExcludeCategories(ICartContainsCondition condition)
		{
			CategoryExcludePopupCategoryTree.Selector = CategorySelector;
			CategoryExcludePopupCategoryTree.DataBind();
			DataBindCategoryExcludeGrid(ResolveCategories(condition.ExcludeCategories));
		}

		protected virtual void OnCategoryExcludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, CategoryExcludeApplyToAllSelectedDiv);
		}

		protected virtual void OnCategoryExcludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveCategory"))
			{
				RemoveExcludedCategory(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveExcludedCategory(int productId)
		{
			State.ExcludeCategories.Remove(productId);
			DataBindCategoryExcludeGrid(ResolveCategories(State.ExcludeCategories));
		}

		protected virtual void OnExcludeCategories(object sender, EventArgs e)
		{
			foreach (var categoryId in CategoryExcludePopupCategoryTree.SelectedIds)
			{
				if (!State.ExcludeCategories.ContainsKey(categoryId))
				{
					State.ExcludeCategories.Add(categoryId, false);
				}
			}
			DataBindCategoryExcludeGrid(ResolveCategories(State.ExcludeCategories));
			CategoryExcludePopupCategoryTree.SelectedIds.Clear();
		}

		protected virtual void OnRemoveSelectionFromCategoryExcludeGrid(object sender, EventArgs e)
		{
			foreach (var categoryId in GetSelectedCategoriesFromExcludeGrid())
			{
				State.ExcludeCategories.Remove(categoryId);
			}
			DataBindCategoryExcludeGrid(ResolveCategories(State.ExcludeCategories));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedCategoriesFromExcludeGrid()
		{
			return GetSelectedIdsFromGrid(CategoryExcludeGrid);
		}

		protected virtual void DataBindCategoryExcludeGrid(Collection<ICategory> categories)
		{
			CategoryExcludeGrid.DataSource = categories;
			CategoryExcludeGrid.DataBind();
			CategoryExcludeGridUpdatePanel.Update();
		}

		protected virtual bool GetExcludeSubcategories(object id)
		{
			var categoryId = Convert.ToInt32(id, CultureInfo.CurrentCulture);
			bool include;
			return State.ExcludeCategories.TryGetValue(categoryId, out include) ? include : false;
		}

		protected virtual void OnExcludeSubcategoriesChanged(object sender, EventArgs e)
		{
			var checkBox = (CheckBox)sender;
			var row = (GridViewRow)checkBox.Parent.Parent;
			var idHidden = (HiddenField)row.FindControl("IdHiddenField");
			var categoryId = int.Parse(idHidden.Value, CultureInfo.CurrentCulture);
			bool include;
			if (!State.ExcludeCategories.TryGetValue(categoryId, out include)) return;
			State.ExcludeCategories[categoryId] = checkBox.Checked;
			DataBindCategoryExcludeGrid(ResolveCategories(State.ExcludeCategories));
		}
	}
}