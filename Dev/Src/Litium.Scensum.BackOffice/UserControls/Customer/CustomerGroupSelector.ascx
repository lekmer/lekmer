﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerGroupSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Customer.CustomerGroupSelector" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" TagPrefix="CustomControls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type="text/javascript">
	function ClosePopup() {
		$("div#popup-customers-header-center input[name*='CancelButton']").click();
		return false;
	}
</script>

<div id="AddCustomerGroupDiv" runat="server" class="popup-customers-container" style="z-index: 10010; display: none;">
    <div id="popup-customers-header">
		<div id="popup-customers-header-left">
		</div>
		<div id="popup-customers-header-center">
			<span><%= Resources.Customer.Literal_SelectCustomerGroups %></span>
			<input type="button" id="CancelButton" runat="server" value="x"/>
		</div>
		<div id="popup-customers-header-right">
		</div>
	</div>
	<div id="popup-customers-content">
	    <asp:UpdatePanel runat="server" ID="MainUpdatePanel" UpdateMode="Conditional">
		    <ContentTemplate>
		    <br />
		    <div class="popup-customers-body">
				<div class="left">
					<CustomControls:TemplatedTreeView runat="server" ID="CustomerGroupTree" DisplayTextControl="TitleLabel"
						UseRootNode="true" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
						MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
						NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
						NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
						NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
						MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">
						<HeaderTemplate>
						</HeaderTemplate>
						<NodeTemplate>
							<div class="tree-item-cell-expand">
								<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
								<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden" />
							</div>
							<div class="tree-item-cell-main">
								<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
								<asp:LinkButton runat="server" ID="TitleLabel" CommandName="Navigate" ></asp:LinkButton>
							</div>
							<br />
						</NodeTemplate>
					</CustomControls:TemplatedTreeView>
					</div>
			    		
					<div class="popup-customer-group-grid">
						<sc:GridViewWithCustomPager ID="CustomerGroupGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox id="SelectAllCheckBox" runat="server"/>
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>	
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Title %>" ItemStyle-Width="64%" >
									<ItemTemplate>
										<uc:LiteralEncoded ID="GroupTitleLiteral" runat="server" Text='<%# Eval("Title") %>' ></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:Customer, ColumnHeader_ErpId %>" DataField="ErpId" ItemStyle-Width="15%" />
								<asp:TemplateField HeaderText="<%$ Resources:Customer, ColumnHeader_Status %>" ItemStyle-Width="15%" >
									<ItemTemplate>
										<uc:LiteralEncoded ID="GroupStatusLiteral" runat="server" Text='<%# GetCustomerGropStatus(Eval("StatusId")) %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
						<br />
					</div>
                </div>
                <br />
                <div class="popup-customer-group-buttons" style="vertical-align:bottom">
                    <uc:ImageLinkButton  UseSubmitBehaviour="true" ID="AddButton" runat="server" Text="<%$ Resources:Customer, Button_Ok %>" Enabled="false" SkinID="DefaultButton" />
                    <uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelBottomButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" OnClientClick="ClosePopup();" />
                </div>
		    </ContentTemplate>
	    </asp:UpdatePanel>
	    	    
     </div>
</div>
<div class="left">
    <uc:ImageLinkButton ID="AddCustomerGroupButton" runat="server" Text="<%$ Resources:Customer, Button_AddCustomerGroups %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
    <ajaxToolkit:ModalPopupExtender 
        ID="CustomerGroupSelectPopup" 
        runat="server" 
        TargetControlID="AddCustomerGroupButton"
		PopupControlID="AddCustomerGroupDiv"
		BackgroundCssClass="popup-background" Y="20"
		CancelControlID="CancelButton"/>
</div>