﻿using System;
using System.Globalization;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	public partial class CustomerUserControl : StateUserControlController<IUser>
	{
		public string ValidationGroup;
		private ICustomer _customer;

		public ICustomer Customer
		{
			set
			{
				ViewState["Customer"] = _customer = value;
			}
			get
			{
				if (_customer != null)
				{
					return _customer;
				}
				_customer = ViewState["Customer"] as ICustomer;
				return _customer;
			}
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re")]
		public void REBind()
		{
			PopulateControl();
		}

		protected override void SetEventHandlers()
		{

		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.DateTime.ToString(System.String)")]
		protected override void PopulateControl()
		{
			State = null;
			if (Customer != null)
			{
				if (Customer.User != null)
				{
					State = Customer.User;
					tbUserName.Text = Customer.User.UserName;
					DateCreatedLabel.Text = Customer.User.CreatedDate.ToString("yyyy-MM-dd HH:mm", CultureInfo.CurrentCulture);
				}
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public IUser GetCustomerUser()
		{
			if (State == null)
			{
				IUser user = IoC.Resolve<IUserSecureService>().Create();
				State = user;
				DateCreatedLabel.Text = user.CreatedDate.ToString("yyyy-MM-dd HH:mm", CultureInfo.CurrentCulture);
			}
			State.UserName = tbUserName.Text;
			return State;
		}
	}
}