﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerInformationControl.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Customer.CustomerInformationControl" %>
			<div class="customer-edit-container">
				<label class="assortment-header">
					<%= Resources.Customer.Label_CustomerInformation %></label>
				<br />				
				<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_CivicNumber %></span>
					<br />
					<asp:TextBox ID="CivicNumberTextBox" runat="server"  />
				</div>
				<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_PhoneNumber %></span>
					<br />
					<asp:TextBox ID="PhoneHomeTextBox" runat="server"  />
				</div>	
				<div class="customer-last-input-box  ">
					<span><%= Resources.Customer.Literal_DefaultDeliveryAddress%></span>
					<br />					
					<asp:DropDownList ID="DeliveryAddressList" DataTextField="StreetAddress" DataValueField="Id" runat="server">
					</asp:DropDownList>
				</div>
				<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_FirstName %>&nbsp; *</span>
					<asp:RequiredFieldValidator ID="rfvFirstName"  ControlToValidate="FirstNameTextBox" runat="server"  Display="None" ErrorMessage="<%$ Resources:GeneralMessage, FirstNameEmpty %>" />
					<br />
					<asp:TextBox ID="FirstNameTextBox" runat="server" />
				</div>
					<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_MobilePhone %></span>
					<br />
					<asp:TextBox ID="PhoneMobileTextBox" runat="server" />
				</div>
				<div class="customer-last-input-box">
					<span><%= Resources.Customer.Literal_DefaultBillingAddress%></span>
					<br />					
					<asp:DropDownList ID="BillingAddressList" DataTextField="StreetAddress" DataValueField="Id" runat="server">
					</asp:DropDownList>
				</div>
				<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_LastName %>&nbsp; *</span>
					<asp:RequiredFieldValidator ID="rfvLastName" ValidationGroup="vgCustomerEdit" ControlToValidate="LastNameTextBox" runat="server" Display="None"  ErrorMessage="<%$ Resources:GeneralMessage, LastNameEmpty %>" />
					<br />
					<asp:TextBox ID="LastNameTextBox" runat="server" />
				</div>		
				<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_Email %>&nbsp; *</span>
				    <asp:RequiredFieldValidator ID="rfvEmail" ValidationGroup="vgCustomerEdit" ControlToValidate="EmailTextBox" runat="server"  Display="None" ErrorMessage="<%$ Resources:GeneralMessage, EmailEmpty %>"/>
				    <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationGroup="vgCustomerEdit" ControlToValidate="EmailTextBox" Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"  ErrorMessage="<%$ Resources:GeneralMessage, InvalidEmail %>"></asp:RegularExpressionValidator>
					<br />
					<asp:TextBox ID="EmailTextBox" runat="server" />
				</div>
				<div class="customer-input-box input-box">
					<span><%= Resources.Customer.Literal_DateCreated%></span>
					<br />
					<asp:Literal ID="DateCreatedLabel" runat="server"></asp:Literal>
				</div>				
			</div>