﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CustomerUserControl.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Customer.CustomerUserControl" %>
						
<div class="control-container">
	<span><%= Resources.Customer.Label_Username%></span>
	<br />
	<asp:TextBox ID="tbUserName"  runat="server"  />
</div>
<div class="control-container">
	<span><%= Resources.Customer.Literal_DateCreated%></span>
	<br />
	<asp:Literal ID="DateCreatedLabel" runat="server"></asp:Literal>
</div>