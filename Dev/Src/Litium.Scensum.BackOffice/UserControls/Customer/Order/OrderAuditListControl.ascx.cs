﻿using System.Collections.ObjectModel;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Order;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Order
{
	public partial class OrderAuditListControl : StateUserControlController<Collection<IOrderAudit>>
	{
		public OrderAuditListControl()
		{
			State = new Collection<IOrderAudit>();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderAudit> OrderAudit
		{
			get
			{
				return State;
			}
			set
			{
				State = value;
			}
		}

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
			AuditListRepeater.DataSource = State;
			AuditListRepeater.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re")]
				public void REBind()
		{
			PopulateControl();
		}
	}
}