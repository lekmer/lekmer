﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Foundation;
using Convert=System.Convert;
using Litium.Scensum.BackOffice.CommonItems;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Order
{
	public partial class OrderPaymentsControl : StateUserControlController<Collection<IOrderPayment>>
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderPayment> OrderPayments
		{
			get
			{
				return State;
			}
			set
			{
				State = value;
			}
		}

		private IOrderFull _orderFull;
		public IOrderFull OrderFull
		{
			get
			{
				return _orderFull;
			}
			set
			{
				_orderFull = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.Object)")]
		private int CurrentEditingPaymentId
		{
			get
			{
				return Convert.ToInt32(ViewState["cpId"], CultureInfo.CurrentCulture);
			}
			set
			{
				ViewState["cpId"] = value;
			}
		}

		public bool EnableAdding { get; set; }
		public bool EnableEditing { get; set; }
		public bool EnableDeleting { get; set; }

		public event EventHandler<EventArgs> PaymentsChanged;
		public event EventHandler<CannotDoEventArgs> CannotDo;

		protected override void SetEventHandlers()
		{
			ValueTextBoxValidator.ServerValidate += DecimalValidate;
			VatTextBoxValidator.ServerValidate += DecimalValidate;
			
			OrderPaymentGrid.RowCommand += OrderPaymentGridOnRowCommand;
			OrderPaymentGrid.RowDataBound += OrderPaymentGrid_RowDataBound;
			OkButton.Click += OkButton_Click;
			AddPaymentLinkButton.Click += AddPaymentLinkButton_Click;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1820:TestForEmptyStringsUsingStringLength")]
		protected void DecimalValidate(object source, ServerValidateEventArgs args)
		{
			decimal result;
			args.IsValid = (!string.IsNullOrEmpty(args.Value)) && decimal.TryParse(args.Value, NumberStyles.Number, CultureInfo.CurrentCulture, out result) && (result > 0);
		}

		protected void AddPaymentLinkButton_Click(object sender, EventArgs e)
		{
			if (!IoC.Resolve<IOrderPaymentSecureService>().CanAdd(OrderFull))
			{
				if (null != CannotDo)
				{
					CannotDo(this, new CannotDoEventArgs(Cannot.Add));
				}
				return;
			}
			BindEditPopup(null);
			PaymentPopup.Show();
		}

		protected void OkButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				PaymentPopup.Show();
				return;
			}

			if (State.SingleOrDefault(item => item.Id == CurrentEditingPaymentId) != null)//we edit existing payment
			{
				if (!IoC.Resolve<IOrderPaymentSecureService>().CanEdit(OrderFull, State.SingleOrDefault(item => item.Id == CurrentEditingPaymentId)))
				{
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditPayment);
					PaymentPopup.Show();
					return;
				}

				IOrderPayment payment = State.Single(item => item.Id == CurrentEditingPaymentId);
				FillPayment(payment);
				
			}
			else//we create new payment
			{
				IOrderPayment payment = IoC.Resolve<IOrderPaymentSecureService>().Create();
				payment.Id = CurrentEditingPaymentId;
				FillPayment(payment);
				State.Add(payment);
			}

			PopulatePayment();

			if (PaymentsChanged != null)
			{
				PaymentsChanged(this, EventArgs.Empty);
			}
		}

		private void FillPayment(IOrderPayment payment)
		{
			payment.Price = ValueTextBox.Text.ToLocalDecimal();
			payment.PaymentTypeId = PaymentTypeDropDownList.SelectedValue.ToLocalInt32();
			payment.Vat = VatTextBox.Text.ToLocalDecimal();
			payment.ReferenceId = TransactionIdTextBox.Text;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
		protected void OrderPaymentGridOnRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DeletePayment")
			{
				
				int id = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);

				if (IoC.Resolve<IOrderPaymentSecureService>().CanDelete(OrderFull, State.SingleOrDefault(item => item.Id == id)))
				{

					State.Single(item => item.Id == id).Status = BusinessObjectStatus.Deleted;
					PopulateControl();

					if (PaymentsChanged != null)
					{
						PaymentsChanged(this, EventArgs.Empty);
					}
				}
				else
				{
					if (null != CannotDo)
					{
						CannotDo(this, new CannotDoEventArgs(Cannot.Delete));
					}
				}
			}
			else if (e.CommandName == "EditPayment")
			{
				int id = Convert.ToInt32(e.CommandArgument, CultureInfo.InvariantCulture);
				BindEditPopup(id);
				PaymentPopup.Show();
			}
			
		}

		private void OrderPaymentGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType == DataControlRowType.DataRow)
			{
				EnableFeatures(row);

				var paymentTypeSecureService = IoC.Resolve<IPaymentTypeSecureService>();
				var paymentTypeId = ((OrderPayment) e.Row.DataItem).PaymentTypeId;
				var paymentType = paymentTypeSecureService.GetById(paymentTypeId);
				var paymentTypeTitle = paymentType.Title;

				var paymentLinkButton = (LinkButton)row.FindControl("PaymentLinkButton");
				paymentLinkButton.Text = paymentTypeTitle;

				var paymentLiteral = (Literal)row.FindControl("PaymentLiteral");
				paymentLiteral.Text = paymentTypeTitle;
			}
		}

		private void BindEditPopup(int? id)
		{
			if (id.HasValue) //edit existing payment
			{
				CurrentEditingPaymentId = id.Value;

				IOrderPayment payment = State.Single(item => item.Id == id);
	

				ValueTextBox.Text = payment.Price.ToLocalStringAsCurrency();
				VatTextBox.Text = payment.Vat.ToLocalStringAsCurrency();
				TransactionIdTextBox.Text = payment.ReferenceId;

				PaymentTypeDropDownList.SelectedValue = payment.PaymentTypeId.ToLocalString();
			}
			else //will create new payment on ok
			{
				int newPaymentId;
				if (State.Any(item => item.Id < 0))
				{
					newPaymentId = State.Min(item => item.Id) - 1;
				}
				else
				{
					newPaymentId = -1;
				}

				CurrentEditingPaymentId = newPaymentId;

				ValueTextBox.Text = string.Empty;
				TransactionIdTextBox.Text = string.Empty;
				VatTextBox.Text = string.Empty;
				PaymentTypeDropDownList.SelectedIndex = 0;
			}
		}

		protected override void PopulateControl()
		{
			PopulatePayment();
			PopulatePaymentTypes();
			EnableFeatures();
		}

		private void PopulatePayment()
		{
			OrderPaymentGrid.DataSource = State.Where(item => item.Status != BusinessObjectStatus.Deleted);
			OrderPaymentGrid.DataBind();
		}

		private void PopulatePaymentTypes()
		{
			PaymentTypeDropDownList.DataSource = IoC.Resolve<IPaymentTypeSecureService>().GetAll();
			PaymentTypeDropDownList.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re")]
		public void REBind()
		{
			PopulateControl();
		}
		
		private void EnableFeatures()
		{
			AddPaymentLinkButton.Visible = EnableAdding;
			OrderPaymentGrid.Columns[2].Visible = EnableDeleting;
		}
		private void EnableFeatures(Control row)
		{
			var paymentLinkButton = (LinkButton)row.FindControl("PaymentLinkButton");
			paymentLinkButton.Visible = EnableEditing;

			var paymentLiteral = (Literal)row.FindControl("PaymentLiteral");
			paymentLiteral.Visible = !EnableEditing;
		}

		protected string FormatPrice(decimal value)
		{
			var order = IoC.Resolve<IOrderSecureService>().GetFullById(GetId());
			var orderChannel = IoC.Resolve<IChannelSecureService>().GetById(order.ChannelId);
			return IoC.Resolve<IFormatter>().FormatPrice(orderChannel, value);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected int GetId()
		{
			return Request.QueryString.GetInt32("Id");
		}
	}

	public class CannotDoEventArgs : EventArgs
	{
		public CannotDoEventArgs(Cannot cannotDo)
		{
			Cannot = cannotDo;
		}

		public Cannot Cannot { get; set; }
	}

	public enum Cannot
	{
		Add,
		Edit,
		Delete
	}
}
