﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderPaymentsControl.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Customer.Order.OrderPaymentsControl" %>
<%@ Import Namespace="Litium.Scensum.Order" %>
<%@ Import Namespace="Litium.Scensum.Foundation" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<div id="order-payment">
	<asp:GridView ID="OrderPaymentGrid" SkinID="grid" runat="server" Width="195" AutoGenerateColumns="false" >
		<Columns>
			<asp:TemplateField>
				<HeaderTemplate>
					<%= Resources.Customer.Label_OrderPaymentType%>
				</HeaderTemplate>
				<ItemTemplate>
					<asp:LinkButton CommandName="EditPayment" CommandArgument='<%# Eval("Id") %>' runat="server" ID="PaymentLinkButton"></asp:LinkButton>
					<asp:Literal runat="server" ID="PaymentLiteral"></asp:Literal>
					<br />
					<%# Eval("ReferenceId") == null ? string.Empty : EncodeHelper.HtmlEncode((string)Eval("ReferenceId")) %>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<HeaderTemplate>
					<%= Resources.Customer.Label_OrderPaymentAmount%>
				</HeaderTemplate>
				<ItemTemplate>
					<%# FormatPrice((decimal)Eval("Price"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField>
				<ItemTemplate>
					<asp:ImageButton runat="server" ID="DeletePaymentImageButton" OnClientClick='<%# "return DeleteConfirmation(\""+ Resources.Customer.Literal_OrderPaymentConfirmDelete+"\");"%>'
						CommandName="DeletePayment" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif"
						AlternateText="<%$ Resources:General, Button_Delete %>" />
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		
	</asp:GridView>
	<asp:LinkButton ID="AddPaymentLinkButton" runat="server"><%= Resources.Customer.Literal_OrderPaymentAdd %></asp:LinkButton>
</div>

<asp:HiddenField runat="server" ID="FakeControl" />
<div id="PaymentViewDiv" runat="server" class="order-address-mainDiv" style="z-index: 10010;
	display: none;">
	<div id="order-payment-edit-header">
		<div id="order-payment-edit-header-left">
		</div>
		<div id="order-payment-edit-header-center">
			<span>
				<%= Resources.Customer.Label_OrderPaymentType %></span>
			<asp:Button ID="PaymentViewCancelButton" runat="server" Text="X" UseSubmitBehavior="false" />
		</div>
		<div id="order-payment-edit-header-right">
		</div>
	</div>
	<div id="order-payment-edit-subMainDiv">
	<asp:Panel runat="server" DefaultButton="OkButton">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="OkButton" runat="server" />
	<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" runat="server"
			CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="PaymentValidationGroup" />
		<div class="input-box">
			<span>
				<%= Resources.Customer.Label_OrderPaymentType %></span><br />
			<asp:DropDownList runat="server" ID="PaymentTypeDropDownList" DataTextField="Title"
				DataValueField="Id">
			</asp:DropDownList>
		</div>
		<div class="input-box">
			<span>
				<%= Resources.Customer.Label_OrderPaymentAmount%></span>
			<br />
			<asp:TextBox ID="ValueTextBox" runat="server" />
			<asp:CustomValidator runat="server" ID="ValueTextBoxValidator" ControlToValidate="ValueTextBox" Display="None" ErrorMessage="<%$ Resources:GeneralMessage,VM_IncorrectPaymentValue%>" ValidationGroup="PaymentValidationGroup" ValidateEmptyText="true"></asp:CustomValidator>
		</div>
		<br class="clear" />
		<div class="input-box">
			<span>
				<%= Resources.Customer.Label_OrderPaymentVat%></span>
			<br />
			<asp:TextBox ID="VatTextBox" runat="server" />
			<asp:CustomValidator runat="server" ID="VatTextBoxValidator" ControlToValidate="VatTextBox" Display="None" ErrorMessage="<%$ Resources:GeneralMessage,VM_IncorrectPaymentVat%>" ValidationGroup="PaymentValidationGroup" ValidateEmptyText="true"></asp:CustomValidator>
		</div>
		<div class="input-box">
			<span>
				<%= Resources.Customer.Label_OrderPaymentTransactionId%></span>
			<br />
			<asp:TextBox ID="TransactionIdTextBox" runat="server" MaxLength="50" />
		</div>
		<br class="clear" />
		<br clear="all"/>
	<div class="customer-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="OkButton" runat="server" Text="<%$ Resources:General, Button_Ok %>"
				SkinID="DefaultButton" ValidationGroup="PaymentValidationGroup" />
			<uc:ImageLinkButton UseSubmitBehaviour="false" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				SkinID="DefaultButton" />
		</div>
		</asp:Panel>
	</div>	
	<ajaxToolkit:ModalPopupExtender ID="PaymentPopup" runat="server" TargetControlID="FakeControl"
		PopupControlID="PaymentViewDiv" CancelControlID="PaymentViewCancelButton" BackgroundCssClass="popup-background" />
</div>

