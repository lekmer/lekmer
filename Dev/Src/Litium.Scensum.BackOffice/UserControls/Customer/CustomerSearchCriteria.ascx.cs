using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.UserControls.Customer.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Customer;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	public partial class CustomerSearchCriteria : UserControl
	{
		#region Events

		public event EventHandler<CustomerSearchCriteriaEventArgs> SearchEvent;

		#endregion

		#region Protected Methods

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			btnSearch.Click += BtnSearch_Click;

			cvDateFrom.ServerValidate += ValidateDateTime;
			cvDateTo.ServerValidate += ValidateDateTime;
			cvDateCompare.ServerValidate += CompareDateTime;
		}

		protected void BtnSearch_Click(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				return;
			}
			SearchEvent(this, new CustomerSearchCriteriaEventArgs{SearchCriteria = CreateSearchCriteria()});
		}

		#endregion

		#region Private Methods

		public void PopulateStatusList()
		{
			ddlStatus.Items.Clear();
			var customerStatusSecureService = IoC.Resolve<ICustomerStatusSecureService>();
			ddlStatus.DataSource = customerStatusSecureService.GetAll();
			ddlStatus.DataBind();
			ddlStatus.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}

		private static void ValidateDateTime(object sender, ServerValidateEventArgs args)
		{
			DateTime dateTime;
			if (!DateTime.TryParse(args.Value, out dateTime))
			{
				args.IsValid = false;
				return;
			}

			DateTime minDate = DateTime.Parse(Constants.MinDate, CultureInfo.CurrentCulture);
			DateTime maxDate = DateTime.Parse(Constants.MaxDate, CultureInfo.CurrentCulture);
			if (dateTime < minDate || dateTime > maxDate)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}

		private void CompareDateTime(object sender, ServerValidateEventArgs args)
		{
			if (string.IsNullOrEmpty(tbCreatedFrom.Text) || !cvDateFrom.IsValid || !cvDateTo.IsValid)
			{
				args.IsValid = true;
				return;
			}
			DateTime createdFrom = DateTime.Parse(tbCreatedFrom.Text, CultureInfo.CurrentCulture);
			DateTime createdTo = DateTime.Parse(args.Value, CultureInfo.CurrentCulture);
			if (createdFrom > createdTo)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}
        
		#endregion

		#region Public Methods

		public ICustomerSearchCriteria CreateSearchCriteria()
		{
			string createFrom = string.IsNullOrEmpty(tbCreatedFrom.Text) ? string.Empty : tbCreatedFrom.Text.ToString(CultureInfo.CurrentCulture);
			string createTo = string.IsNullOrEmpty(tbCreatedTo.Text) ? string.Empty : tbCreatedTo.Text.ToString(CultureInfo.CurrentCulture);

			var searchCriteria = IoC.Resolve<ICustomerSearchCriteria>();

			searchCriteria.ErpId = tbErpId.Text;
			searchCriteria.FirstName = tbFirstName.Text;
			searchCriteria.Number = tbCivicNumber.Text;
			searchCriteria.Email = tbEmail.Text;
			searchCriteria.PhoneHome = tbPhoneHome.Text;
			searchCriteria.PhoneMobile = tbPhoneMobile.Text;
			searchCriteria.CreatedFrom = createFrom;
			searchCriteria.CreatedTo = createTo;
			searchCriteria.LastName = tbLastName.Text;
			searchCriteria.CustomerStatusId = ddlStatus.SelectedValue;

			return searchCriteria;
		}

		public void RestoreSearchFields(ICustomerSearchCriteria searchCriteria)
		{
			PopulateStatusList();

			if (searchCriteria == null)
			{
				return;
			}

			tbErpId.Text = searchCriteria.ErpId;
			tbFirstName.Text = searchCriteria.FirstName;
			tbCivicNumber.Text = searchCriteria.Number;
			tbEmail.Text = searchCriteria.Email;
			tbPhoneHome.Text = searchCriteria.PhoneHome;
			tbPhoneMobile.Text = searchCriteria.PhoneMobile;
			tbCreatedFrom.Text = searchCriteria.CreatedFrom;
			tbCreatedTo.Text = searchCriteria.CreatedTo;
			tbLastName.Text = searchCriteria.LastName;
			ddlStatus.SelectedValue = searchCriteria.CustomerStatusId;
		}

		private bool ValidateData()
		{
			Page.Validate("vgCustomerSearch");
			return Page.IsValid;
		}

		#endregion
	}
}