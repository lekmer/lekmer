﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Customer.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	[SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
	public partial class CustomerSearch : UserControlController
	{
		private Collection<ICustomerStatus> _customerStatuses;
		
		public event EventHandler<CustomerSelectEventArgs> SelectEvent;
        
		protected override void SetEventHandlers()
		{
			CustomersSearchGrid.PageIndexChanging += OnPageIndexChanging;
			CustomersSearchGrid.RowDataBound += OnGridRowDataBound;
			CustomersSearchGrid.DataBound += OnGridDataBound;

			SearchCriteria.SearchEvent += CustomerSearchEvent;
			AddButton.Click += OnAdd;
			CancelButton.ServerClick += OnCancel;
		}

		protected override void PopulateControl()
		{
			SearchCriteria.PopulateStatusList();
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			CustomersDataSource.SelectParameters.Clear();
		}

		protected virtual void OnGridDataBound(object sender, EventArgs e)
        {
			AddButton.Enabled = CustomersSearchGrid.Rows.Count > 0;
        }

		protected virtual void OnGridRowDataBound(object sender, GridViewRowEventArgs e)
        {
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row);
        }

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CustomersSearchGrid.PageIndex = e.NewPageIndex;
        }

		protected virtual void CustomerSearchEvent(object sender, CustomerSearchCriteriaEventArgs e)
        {
            CustomersSearchGrid.PageIndex = 0;
            SearchParamInit(e.SearchCriteria);
			SearchCriteria.PopulateStatusList();
        }

		[SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
		protected virtual void SearchParamInit(ICustomerSearchCriteria searchCriteria)
		{
			CustomersDataSource.SelectParameters.Clear();
			CustomersDataSource.SelectParameters.Add("maximumRows", CustomersSearchGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			CustomersDataSource.SelectParameters.Add("startRowIndex", (CustomersSearchGrid.PageIndex * CustomersSearchGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			CustomersDataSource.SelectParameters.Add("erpId", searchCriteria.ErpId);
			CustomersDataSource.SelectParameters.Add("firstName", searchCriteria.FirstName);
			CustomersDataSource.SelectParameters.Add("lastName", searchCriteria.LastName);
			CustomersDataSource.SelectParameters.Add("civicNumber", searchCriteria.Number);
			CustomersDataSource.SelectParameters.Add("address", searchCriteria.Address);
			CustomersDataSource.SelectParameters.Add("zip", searchCriteria.Zip);
			CustomersDataSource.SelectParameters.Add("city", searchCriteria.City);
			CustomersDataSource.SelectParameters.Add("email", searchCriteria.Email);
			CustomersDataSource.SelectParameters.Add("phoneHome", searchCriteria.PhoneHome);
			CustomersDataSource.SelectParameters.Add("phoneMobile", searchCriteria.PhoneMobile);
			CustomersDataSource.SelectParameters.Add("createdFrom", searchCriteria.CreatedFrom);
			CustomersDataSource.SelectParameters.Add("createdTo", searchCriteria.CreatedTo);
			CustomersDataSource.SelectParameters.Add("customerStatusId", searchCriteria.CustomerStatusId);
		}

		protected virtual void OnAdd(object sender, EventArgs e)
        {
			var ids = CustomersSearchGrid.GetSelectedIds();
			var customers = new Collection<ICustomer>();
			var service = IoC.Resolve<ICustomerSecureService>();
			foreach (var id in ids)
			{
				customers.Add(service.GetById(id));
			}

			if (customers.Count > 0)
			{
				CustomersDataSource.SelectParameters.Clear();
				SelectEvent(this, new CustomerSelectEventArgs { Customers = customers });
				CustomersSearchPopup.Hide();
			}
        }

		protected virtual string GetCustomerStatus(object statusId)
		{
			if (_customerStatuses == null)
				_customerStatuses = IoC.Resolve<ICustomerStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _customerStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"')");
			}
		}
	}
}