<%@ Control Language="C#" CodeBehind="Fragment.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Template.Fragment" %>
		
<div class="input-box">
    <asp:HiddenField ID="ModelFragmentIdHiddenField" runat="server" />
	<div class="left bold alternate-fragment-content">
		<asp:Label ID="HeaderLabel" runat="server" />
	</div>
	<div onmousedown="storeCaret(document.getElementById('<%=ContentTextBox.ClientID %>'));">
		<div id="AddComponentDiv" runat="server" class="right fragment-button">
			<%= Resources.Interface.Literal_Component %>
		</div>
		<div id="AddIncludeDiv" runat="server" class="right fragment-button">
			<%= Resources.Interface.Literal_Include %>
		</div>
		<div id="AddAliasDiv" runat="server" class="right fragment-button">
			<%= Resources.Interface.Literal_Alias %>
		</div>
		<div id="AddFunctionDiv" runat="server" class="right fragment-button-function" >
			<asp:Button ID="AddButton" Text="<%$ Resources:Interface,Literal_Function %>" runat="server" CssClass="functionAddButton" style="padding-bottom: 3px;" />
		</div>
	</div>
	<asp:TextBox ID="ContentTextBox" TextMode="MultiLine" Rows="12" runat="server"/>
	<div id="caret-storage">
		<asp:HiddenField ID="HiddenField1" runat="server"/>
	</div>
</div>
