<%@ Control Language="C#" CodeBehind="FragmentRegion.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Template.FragmentRegion" %>
<%@ Register TagPrefix="uc" TagName="Fragment" Src="~/UserControls/Template/Fragment.ascx" %>
<%@ Register TagPrefix="uc" TagName="CollapsiblePanel" Src="~/UserControls/Common/CollapsiblePanel.ascx" %>
<div style="width: 717px;">
	<uc:CollapsiblePanel runat="server" ID="RegionCollapsiblePanel" HasAction="false" />
	<div id="FragmentsDiv" runat="server" class="fragment-content left" style="display: none;">
		<asp:HiddenField ID="FragmentExpandHiddenField" Value="0" runat="server" />
		<label id="FragmentLabel" runat="server" class="settings-caption">
			<%= Resources.Interface.Literal_ATemplate %></label>
		<div id="FragmentDiv" runat="server">
			<asp:Repeater ID="FragmentRepeater" runat="server">
				<ItemTemplate>
					<uc:Fragment ID="ucFragment" runat="server" OnFunctionAdded="FragmentRegionFunctionAdded"
						IsAlternate="false" Name='<%# DataBinder.Eval(Container.DataItem, "Title")%>'
						Content='<%# DataBinder.Eval(Container.DataItem, "Content")%>' ModelFragmentId='<%# DataBinder.Eval(Container.DataItem, "ModelFragmentId")%>'
						Size='<%# DataBinder.Eval(Container.DataItem, "Size")%>' />
					</br>
				</ItemTemplate>
			</asp:Repeater>
		</div>
		<div id="AlternateFragmentsDiv" runat="server" class="alternate-fragment-content">
			<label class="settings-caption">
				<%= Resources.Interface.Literal_BTemplate %></label>
			<div id="AlternateFragmentDiv" runat="server" class="fragment-item-b">
				<asp:Repeater ID="AlternateFragmentRepeater" runat="server">
					<ItemTemplate>
						<uc:Fragment ID="ucAlternateFragment" runat="server" OnFunctionAdded="FragmentRegionFunctionAdded"
							IsAlternate="true" Name='<%# DataBinder.Eval(Container.DataItem, "Title")%>'
							Content='<%# DataBinder.Eval(Container.DataItem, "AlternateContent")%>' ModelFragmentId='<%# DataBinder.Eval(Container.DataItem, "ModelFragmentId")%>'
							Size='<%# DataBinder.Eval(Container.DataItem, "Size")%>' />
						</br>
					</ItemTemplate>
				</asp:Repeater>
			</div>
		</div>
	</div>
</div>
<br class="clear" />
