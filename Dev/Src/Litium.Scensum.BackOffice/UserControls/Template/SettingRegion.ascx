<%@ Control Language="C#"  CodeBehind="SettingRegion.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Template.SettingRegion" %>
<%@ Register TagPrefix="uc" TagName="Setting" Src="~/UserControls/Template/Setting.ascx"  %>
<%@ Register TagPrefix="uc" TagName="CollapsiblePanel" Src="~/UserControls/Common/CollapsiblePanel.ascx" %>
<div style="width: 717px;">
<uc:CollapsiblePanel runat="server" Id="SettingCollapsiblePanel"
	Title="Setting"
	HasAction="false"
/>
</div>
<div id="SettingsDiv" runat="server" class="fragment-content  left" style="display: none;">
<asp:HiddenField ID="SettingExpandHiddenField"  Value="0" runat="server" />
	<label id="SettingLabel" runat = "server" class="settings-caption"><%= Resources.Interface.Literal_ASetting %></label>
	<div id="SettingDiv" runat="server">
		<asp:Repeater ID="SettingRepeater" runat="server">
			<ItemTemplate>
				<uc:Setting ID="ucSetting" runat="server" IsAlternate="false" Header='<%# DataBinder.Eval(Container.DataItem, "Title")%>'
					Value='<%# DataBinder.Eval(Container.DataItem, "Value")%>' ModelSettingId='<%# DataBinder.Eval(Container.DataItem, "ModelSettingId")%>' 
					SettingType='<%# DataBinder.Eval(Container.DataItem, "SettingTypeId")%>' SettingTypeName = '<%# DataBinder.Eval(Container.DataItem, "SettingTypeName")%>'/>
			</ItemTemplate>
		</asp:Repeater>
	</div>
	<div id="AlternateFragmentsDiv" runat="server" class="alternate-fragment-content">
		<label class="settings-caption"><%= Resources.Interface.Literal_BSetting %></label>
		<div id="AlternateFragmentDiv" runat="server" class="fragment-item-b">
			<asp:Repeater ID="AlternateSettingRepeater" runat="server">
				<ItemTemplate>
					<uc:Setting ID="ucAlternateSetting" runat="server" IsAlternate="true" Header='<%# DataBinder.Eval(Container.DataItem, "Title")%>'
						Value='<%# DataBinder.Eval(Container.DataItem, "AlternateValue")%>' ModelSettingId='<%# DataBinder.Eval(Container.DataItem, "ModelSettingId")%>'
						SettingType='<%# DataBinder.Eval(Container.DataItem, "SettingTypeId")%>' SettingTypeName = '<%# DataBinder.Eval(Container.DataItem, "SettingTypeName")%>'/>
					</br>
				</ItemTemplate>
			</asp:Repeater>
		</div>
	</div>
</div>