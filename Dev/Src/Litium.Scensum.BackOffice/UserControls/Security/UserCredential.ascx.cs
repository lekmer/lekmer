﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Scensum.BackOffice.UserControls.Security
{
	public partial class UserCredential : UserControlController
	{
		private static readonly Random _random = new Random();

		public bool AllowChangeUserName { get; set; }

		public string UserName
		{
			get { return UserNameTextBox.Text; }
			set { UserNameTextBox.Text = value; }
		}

		protected override void SetEventHandlers()
		{
			ManualPasswordRadioButton.CheckedChanged += AutoPasswordChanged;
			AutoPasswordRadioButton.CheckedChanged += AutoPasswordChanged;
		}

		protected override void PopulateControl()
		{
			UserNameTextBox.ReadOnly = !AllowChangeUserName;
			PasswordTextBox.Attributes.Add("value", "TheFakePassword");
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			RegisterClientScripts();
		}

		private void AutoPasswordChanged(object sender, EventArgs e)
		{
			if (AutoPasswordRadioButton.Checked)
			{
				ConfirmPasswordDiv.Visible = false;
				PasswordTextBox.Enabled = false;
				PasswordTextBox.Attributes.Add("value", "TheFakePassword");
			}
			else
			{
				ConfirmPasswordDiv.Visible = true;
				PasswordTextBox.Enabled = true;
				PasswordTextBox.Attributes.Add("value", string.Empty);
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "0#")]
		public string GetPassword(out string clearTextPassword)
		{
			clearTextPassword = AutoPasswordRadioButton.Checked ? GeneratePassword(8) : PasswordTextBox.Text;
			return Sha1Hasher.HashIt(clearTextPassword);
		}

		private static string GeneratePassword(int characterCount)
		{
			const string chars = "abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ0123456789";

			var passwordBuilder = new StringBuilder();

			for (int i = 0; i < characterCount; i++)
			{
				int characterPosition = _random.Next(0, chars.Length);

				passwordBuilder.Append(chars[characterPosition]);
			}

			return passwordBuilder.ToString();
		}

		protected virtual void RegisterClientScripts()
		{
			ScriptManager.RegisterStartupScript(this, GetType(), "js_restoreCredentials",
			                                    "restorePassword('" + PasswordTextBox.ClientID + "', '" + PasswordHidden.ClientID +
			                                    "', '" + ConfirmTextBox.ClientID + "', '" + ConfirmHidden.ClientID + "');", true);
			PasswordTextBox.Attributes.Add("onblur",
			                               "javascript: clonePassword('" + PasswordTextBox.ClientID + "', '" +
			                               PasswordHidden.ClientID + "');");
			ConfirmTextBox.Attributes.Add("onblur",
			                              "javascript: cloneConfirm('" + ConfirmTextBox.ClientID + "', '" +
			                              ConfirmHidden.ClientID + "');");
		}
	}
}