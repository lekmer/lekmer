﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;

namespace Litium.Scensum.BackOffice.UserControls.ContextMenu
{

    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal), AspNetHostingPermission(SecurityAction.InheritanceDemand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class ContextMenu3 : Control, INamingContainer, IPostBackEventHandler
    {
        protected override void CreateChildControls()
        {
            Controls.Clear();
            if (null != DataSource && DataSource.Count() > 0)
            {
                ContainerMain.Controls.Clear();

                ContainerSelectedItem.Controls.Add(ControlSelectedItemValue);
				if (ShowSelectedIcon)
				{
					ContainerSelectedItem.Controls.Add(ControlSelectedItemImage);
				}
                ContainerSelectedItem.Controls.Add(ControlSelectedItemDisplayText);
                ContainerMain.Controls.Add(ContainerSelectedItem);
                ContainerMain.Controls.Add(ContainerSelector);
                ContainerSelector.Controls.Add(ControlImageExpand);
                ContainerSelector.Controls.Add(ContainerMenuShadow);
                ContainerSelector.Controls.Add(ContainerMenu);

                if (!string.IsNullOrEmpty(MenuHeaderText))
                    ContainerMenu.Controls.Add(CreateContainerMenuHeader(MenuHeaderText));

                foreach (var dataItem in DataSource)
                {
                    Control containerMenuItem = CreateContainerMenuItem();
                    if (!string.IsNullOrEmpty(dataItem.ImageSrc))
					{
						containerMenuItem.Controls.Add(CreateHtmlImage(dataItem.ImageSrc, dataItem.Text));
					}
                	containerMenuItem.Controls.Add(CreateHtmlInputHidden(dataItem.Value));
                    containerMenuItem.Controls.Add(CreateHtmlSpan(dataItem.Text));
                    ContainerMenu.Controls.Add(containerMenuItem);
                }

                Controls.Add(ContainerMain);

            }

            Page.ClientScript.RegisterClientScriptInclude(Page.GetType(), "scm-javascripts",
                                                          ResolveUrl("~/Media/Scripts/context-menu.js"));
        }

        protected override void Render(HtmlTextWriter writer)
        {
			var contextMenuDataSourceItem = DataSource
				.Where(item => item.Value == SelectedValue)
				.SingleOrDefault();
			if (contextMenuDataSourceItem != default(ContextMenuDataSourceItem))
			{
				ControlSelectedItemDisplayText.InnerText = contextMenuDataSourceItem.Text;
				ControlSelectedItemImage.Src = contextMenuDataSourceItem.ImageSrc;
				ControlSelectedItemImage.Alt = contextMenuDataSourceItem.Text;
			}

            base.Render(writer);

            ScriptManager.RegisterStartupScript(this, GetType(), "scm-startup-" + ClientID,
                                                string.Format(CultureInfo.InvariantCulture,
                                                              "__scmAttachEvents('div#{0}', '{1}', '{2}');", ContainerMain.ClientID, ResolveUrl(ImageExpandSrc), ResolveUrl(ImageExpandMouseOverSrc)), true);
        }

        private HtmlGenericControl _divContainerMain;
        protected virtual HtmlGenericControl ContainerMain
        {
            get
            {
                if (null == _divContainerMain)
                {
                    _divContainerMain = new HtmlGenericControl("div") { ID = "container" };
                    _divContainerMain.Attributes.Add("class", "scm-main");
                    return _divContainerMain;
                }
                return _divContainerMain;
            }
        }

        private HtmlGenericControl _divContainerSelector;
        protected virtual HtmlGenericControl ContainerSelector
        {
            get
            {
                if (null == _divContainerSelector)
                {
                    _divContainerSelector = new HtmlGenericControl("div");
                    _divContainerSelector.Attributes.Add("class", "scm-selector");
                    return _divContainerSelector;
                }
                return _divContainerSelector;
            }
        }

        private HtmlGenericControl _divContainerMenuShadow;
        protected virtual HtmlGenericControl ContainerMenuShadow
        {
            get
            {
                if (null == _divContainerMenuShadow)
                {
                    _divContainerMenuShadow = new HtmlGenericControl("div");
                    _divContainerMenuShadow.Attributes.Add("class", "scm-menu-shadow");
                    _divContainerMenuShadow.Attributes.Add("style", "display: none;");
                    _divContainerMenuShadow.EnableViewState = false;
                    return _divContainerMenuShadow;
                }
                return _divContainerMenuShadow;
            }
        }

        private HtmlGenericControl _divContainerMenu;
        protected virtual HtmlGenericControl ContainerMenu
        {
            get
            {
                if (null == _divContainerMenu)
                {
                    _divContainerMenu = new HtmlGenericControl("div");
                    _divContainerMenu.Attributes.Add("class", "scm-menu");
                    _divContainerMenu.Attributes.Add("style", "display: none;");
                    _divContainerMenu.EnableViewState = false;
                    return _divContainerMenu;
                }
                return _divContainerMenu;
            }
        }

        protected virtual Control CreateContainerMenuItem()
        {
            HtmlGenericControl containerMenuItem = new HtmlGenericControl("div");
            containerMenuItem.Attributes.Add("class", "scm-menu-item");
            return containerMenuItem;
        }

        protected virtual Control CreateContainerMenuHeader(string text)
        {
            HtmlGenericControl containerMenuHeader = new HtmlGenericControl("div");
            containerMenuHeader.Attributes.Add("class", "scm-menu-header");
            containerMenuHeader.InnerText = text;
            containerMenuHeader.EnableViewState = false;
            return containerMenuHeader;
        }

        private HtmlGenericControl _divContainerSelectedItem;
        protected virtual HtmlGenericControl ContainerSelectedItem
        {
            get
            {
                if (null == _divContainerSelectedItem)
                {
                    _divContainerSelectedItem = new HtmlGenericControl("div");
                    _divContainerSelectedItem.Attributes.Add("class", "scm-selected");
                    _divContainerSelectedItem.EnableViewState = false;
                    return _divContainerSelectedItem;
                }
                return _divContainerSelectedItem;
            }
        }

        private HtmlInputHidden _hidControlSelectedItemValue;
        protected virtual HtmlInputHidden ControlSelectedItemValue
        {
            get
            {
                //return _hidControlSelectedItemValue ?? (_hidControlSelectedItemValue = new HtmlInputHidden());
                if (null == _hidControlSelectedItemValue)
                {
                    _hidControlSelectedItemValue = new HtmlInputHidden();
                    if (null != SelectedValueChanged)
                    {
                        _hidControlSelectedItemValue.Attributes.Add("onchange", Page.ClientScript.GetPostBackEventReference(
                                                                        this, ID));
                    }
                }
                return _hidControlSelectedItemValue;
            }
        }

        private HtmlGenericControl _spanControlSelectedItemDisplayText;
        protected virtual HtmlGenericControl ControlSelectedItemDisplayText
        {
            get
            {
                if (null == _spanControlSelectedItemDisplayText)
                {
                    _spanControlSelectedItemDisplayText = new HtmlGenericControl("span")
                                                          	{
                                                          		ID = "text", 
																EnableViewState = true
                                                          	};
                }
                return _spanControlSelectedItemDisplayText;
            }
        }

		public virtual bool ShowSelectedIcon { get; set; }

		private HtmlImage _controlSelectedItemImage;
		protected virtual HtmlImage ControlSelectedItemImage
		{
			get
			{
				if (null == _controlSelectedItemImage)
				{
					_controlSelectedItemImage = new HtmlImage();
				}
				return _controlSelectedItemImage;
			}
		}

        private HtmlImage _imageControlImageExpand;
        protected virtual HtmlImage ControlImageExpand
        {
            get
            {
                if (null == _imageControlImageExpand)
                {

                    _imageControlImageExpand = new HtmlImage { Src = ImageExpandSrc };
                    _imageControlImageExpand.EnableViewState = false;
                    return _imageControlImageExpand;
                }
                return _imageControlImageExpand;
            }
        }

        protected virtual Control CreateHtmlInputHidden(string value)
        {
            return new HtmlInputHidden { Value = value, EnableViewState = false };
        }

        protected virtual Control CreateHtmlSpan(string text)
        {
            return new HtmlGenericControl("span") { InnerText = text, EnableViewState = false };
        }

        protected virtual Control CreateHtmlImage(string imageSrc, string altText)
        {
            return new HtmlImage { Src = imageSrc, EnableViewState = false, Alt = altText };
        }

        private IEnumerable<ContextMenuDataSourceItem> _dataSource;

        [
        Bindable(true),
        Browsable(false),
        Category("Data"),
        DefaultValue(null),
        Description("The data source used to build up the control."),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)
        ]
        public virtual IEnumerable<ContextMenuDataSourceItem> DataSource
        {
            get
            {
                return _dataSource ?? (_dataSource = (IEnumerable<ContextMenuDataSourceItem>)ViewState["dataSource"]);
            }
            set
            {
                ViewState["dataSource"] = _dataSource = value;
            }
        }

        public virtual string SelectedValue
        {
            get { return ControlSelectedItemValue.Value; }
            set { ControlSelectedItemValue.Value = value; }
        }

        public string ImageExpandSrc { get; set; }

        public string ImageExpandMouseOverSrc { get; set; }

        public string MenuHeaderText { get; set; }

        public void RaisePostBackEvent(string eventArgument)
        {
            if (null != SelectedValueChanged) SelectedValueChanged(this, new ContextMenu3EventArgs(SelectedValue));
        }

        public event EventHandler<ContextMenu3EventArgs> SelectedValueChanged;
    }

    public class ContextMenu3EventArgs : EventArgs
    {
        private readonly string _selectedValue;
        internal ContextMenu3EventArgs(string selectedValue)
        {
            _selectedValue = selectedValue;
        }

        public string SelectedValue
        {
            get
            {
                return _selectedValue;
            }
        }
    }

    [Serializable]
    public class ContextMenuDataSourceItem
    {
        public string Value { get; set; }

        public string Text { get; set; }

        public string ImageSrc { get; set; }
    }
}
