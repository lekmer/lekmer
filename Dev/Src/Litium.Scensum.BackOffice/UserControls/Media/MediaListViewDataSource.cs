

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	public class MediaListViewDataSource
	{
		public int Id { get; set; }
		public string DisplayText { get; set; }//title
		public string FormatExtension { get; set; }
		public string FileType { get; set; }
	}
}
