<%@ Import Namespace="Litium.Scensum.BackOffice.UserControls.Media" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MediaList.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Media.MediaList" %>
<%@ Register Src="MediaItem.ascx" TagName="MediaItem" TagPrefix="uc1" %>
<br clear="all" />

<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>

<script type="text/javascript">
	$(function() {
		media_url = '<%=ResolveUrl("~/Media") %>';
		$('div.light-box a.light-box').lightBox();
	});
</script>

<div class="media-catalog-container">
	<asp:ListView ID="lvMedia" runat="server" DataSourceID="ids">
		<LayoutTemplate>
			<div id="divSortMenu" class="media-sort-view-menu" runat="server">
				<div id="divMediaSort" class="media-sort">
					<asp:Label runat="server" Text="<%$ Resources:General, Literal_SortBy%>" />
					<asp:LinkButton ID="lbtnSortByName" ForeColor="#999999" runat="server" Enabled="false"><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Title%>" /></asp:LinkButton>
					|
					<asp:LinkButton ID="SortByTypeButon" ForeColor="#004CD5" runat="server"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Media, Literal_FileType%>" /></asp:LinkButton>
				</div>
				<div id="divMediaOrder" class="media-order">
					<asp:Label ID="Label2" runat="server" Text="<%$ Resources:General, Literal_Order%>" />:
					<asp:LinkButton ID="lbtnOrderByAsc" ForeColor="#999999" runat="server" Enabled="false"><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Ascending%>" /></asp:LinkButton>
					|
					<asp:LinkButton ID="lbtnOrderByDes" ForeColor="#004CD5" runat="server"><asp:Literal runat="server" Text="<%$ Resources:General, Literal_Descending%>" /></asp:LinkButton>
				</div>
				<div id="divMediaViewMode" class="media-view">
					<div style="float: left">
						<uc:ImageLinkButton ID="_lnkIconsView" runat="server" Text="" ImageUrl="~/Media/Images/Assortment/thumbnail-view.png"
							UseSubmitBehaviour="false" SkinID="HeaderPanelButton" Enabled="false"/>
					</div>
					<div style="padding-left: 3px; float: left">
						<uc:ImageLinkButton ID="_lnkListView" runat="server" Text="" ImageUrl="~/Media/Images/Assortment/list-view.png"
							UseSubmitBehaviour="false" SkinID="HeaderPanelButton" />
					</div>
				</div>
			</div>
			<div runat="server">
				<br />
				<% if (DisplayMode == DisplayMode.List)
	   { %>
				<div class="media-listview-header">
					<div class="media-header-thumbnail">
						&nbsp;</div>
					<div class="media-header-name">
						<asp:Literal runat="server" Text="<%$ Resources:General, Literal_Title%>" /></div>
					<div class="media-header-type">
						<asp:Literal runat="server" Text="<%$ Resources:General, Literal_Type%>" /></div>
					<div class="media-header-dimensions">
						<asp:Literal runat="server" Text="<%$ Resources:Media, Literal_FileInfo%>" /></div>
					<div class="media-header-delete">
						&nbsp;</div>
				</div>
				<%} %>
			</div>
			<asp:PlaceHolder runat="server" ID="itemPlaceholder" />
		</LayoutTemplate>
		<ItemTemplate>
			<% if (DisplayMode == DisplayMode.List){ %>
			<div class="media-listview-body">
			<%}%>
				<uc1:MediaItem ID="_ucImageItem" runat="server" DisplayMode="<%# DisplayMode %>" DataSource="<%# Container.DataItem %>"
					OnNavigate="UCMediaItem_Navigate" OnDelete="UCMediaItem_Delete" />
			<% if (DisplayMode == DisplayMode.List){ %>
			</div>
			<%}%>
		</ItemTemplate>
	</asp:ListView>
	<asp:ObjectDataSource ID="ids" runat="server" EnablePaging="true" SelectCountMethod="SelectCount"
		SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Media.MediaArchive.MediaDataSource">
	</asp:ObjectDataSource>
	<% if (DisplayMode == DisplayMode.Icons)
	{ %>
	<br class="clear" />
	<%} %>
	<div id="PagingDiv" runat="server">
		<div class="right">
			<asp:DataPager PageSize="12" ID="dataPager" PagedControlID="lvMedia" runat="server">
				<Fields>
					<asp:NextPreviousPagerField ButtonType="Image" ButtonCssClass="paging-left" FirstPageImageUrl="~/Media/Images/Grid/Paging/first.gif"
						PreviousPageImageUrl="~/Media/Images/Grid/Paging/prev.gif" ShowFirstPageButton="true"
						ShowPreviousPageButton="true" ShowLastPageButton="false" ShowNextPageButton="false" />
					<asp:NumericPagerField ButtonType="Link" ButtonCount="10" CurrentPageLabelCssClass="paging-selected"
						NumericButtonCssClass="paging-items" />
					<asp:NextPreviousPagerField ButtonType="Image" ButtonCssClass="paging-right" LastPageImageUrl="~/Media/Images/Grid/Paging/last.gif"
						NextPageImageUrl="~/Media/Images/Grid/Paging/next.gif" ShowFirstPageButton="false"
						ShowPreviousPageButton="false" ShowLastPageButton="true" ShowNextPageButton="true" />
				</Fields>
			</asp:DataPager>
		</div>
		<br class="clear" />
	</div>
</div>
