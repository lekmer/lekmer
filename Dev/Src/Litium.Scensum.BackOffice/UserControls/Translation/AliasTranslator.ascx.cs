﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using Litium.Scensum.Web.Controls;

namespace Litium.Scensum.BackOffice.UserControls.Translation
{
	public partial class AliasTranslator : UserControlController
	{
		public TranslationItemMode Mode { get; set; }
		public IAlias Alias { get; set; }
		public string DefaultValueControlClientId { get; set; }

		protected override void SetEventHandlers(){ }
		protected override void PopulateControl()
		{
			switch (Mode)
			{
				case TranslationItemMode.Simple:
					SetInvisible(new Collection<HtmlControl> { MultiLineDiv, WysiwygDiv });
					SetSimpleTranslations(GetTranslationItems());
					break;
				case TranslationItemMode.Multiline:
					SetInvisible(new Collection<HtmlControl> { SimpleDiv, WysiwygDiv });
					SetMultiTranslations(GetTranslationItems());
					break;
				case TranslationItemMode.Wysiwyg:
					SetInvisible(new Collection<HtmlControl> { SimpleDiv, MultiLineDiv });
					SetWysiwygTranslations(GetTranslationItems());
					break;
			}
		}

		protected static string GetExclamationVisibility(string value)
		{
			return string.IsNullOrEmpty(value) ? "display:inline-block" : "display:none";
		}

		public void ClearTranslations()
		{
			switch (Mode)
			{
				case TranslationItemMode.Simple:
					SetVisible(SimpleDiv);
					SetInvisible(new Collection<HtmlControl> { MultiLineDiv, WysiwygDiv });
					SetSimpleTranslations(GetEmptyTranslationItems());
					break;
				case TranslationItemMode.Multiline:
					SetVisible(MultiLineDiv);
					SetInvisible(new Collection<HtmlControl> { SimpleDiv, WysiwygDiv });
					SetMultiTranslations(GetEmptyTranslationItems());
					break;
				case TranslationItemMode.Wysiwyg:
					SetVisible(WysiwygDiv);
					SetInvisible(new Collection<HtmlControl> { SimpleDiv, MultiLineDiv });
					SetWysiwygTranslations(GetEmptyTranslationItems());
					break;
			}
		}
		public Collection<IAliasTranslation> GetTranslations(int id)
		{
			var languageSecureService = IoC.Resolve<ILanguageSecureService>();
            Collection<ILanguage> languages = languageSecureService.GetAll();
			var aliasTranslations = new Collection<IAliasTranslation>();
			switch (Mode)
			{
				case TranslationItemMode.Simple:
					aliasTranslations = GetTranslations(id, languages, SimpleTranslationRepeater, "tbTranslateValueSimple", "hfLangIdSimple");
					break;
				case TranslationItemMode.Multiline:
					aliasTranslations = GetTranslations(id, languages, MultiTranslationRepeater, "tbTranslateValueMulti", "hfLangIdMulti");
					break;
				case TranslationItemMode.Wysiwyg:
					aliasTranslations = GetTranslations(id, languages, WysiwygTranslationRepeater, "tmTranslateValueWysiwyg", "hfLangIdWysiwyg");
					break;
			}
			return aliasTranslations;
		}

		private void SetSimpleTranslations(Collection<TranslationItem> translationItems)
		{
			SimpleTranslationRepeater.DataSource = translationItems;
			SimpleTranslationRepeater.DataBind();
		}
		private void SetMultiTranslations(Collection<TranslationItem> translationItems)
		{
			MultiLanguageRepeater.DataSource = translationItems;
			MultiLanguageRepeater.DataBind();
			
			MultiTranslationRepeater.DataSource = translationItems;
			MultiTranslationRepeater.DataBind();
		}
		private void SetWysiwygTranslations(Collection<TranslationItem> translationItems)
		{
			WysiwygLanguageRepeater.DataSource = translationItems;
			WysiwygLanguageRepeater.DataBind();
			
			WysiwygTranslationRepeater.DataSource = translationItems;
			WysiwygTranslationRepeater.DataBind();
		}
		private static void SetVisible(HtmlControl control)
		{
			control.Attributes.Add("style", "display:block");
		}
		private static void SetInvisible(IEnumerable<HtmlControl> controls)
		{
			foreach (HtmlControl control in controls)
			{
				control.Attributes.Add("style", "display:none");
			}
		}

		private static Collection<TranslationItem> GetEmptyTranslationItems()
		{
			var languageSecureService = IoC.Resolve<ILanguageSecureService>();
			Collection<ILanguage> languages = languageSecureService.GetAll();

			var translationItems = new Collection<TranslationItem>();
			foreach (ILanguage language in languages)
			{
				ILanguage lang = language;
				var translationItem = new TranslationItem
				{
					Language = lang,
					Value = string.Empty
				};

				translationItems.Add(translationItem);
			}
			return translationItems;
		}
		private Collection<TranslationItem> GetTranslationItems()
		{
			var languageSecureService = IoC.Resolve<ILanguageSecureService>();
			Collection<ILanguage> languages = languageSecureService.GetAll();

			var aliasTranslationSecureService = IoC.Resolve<IAliasTranslationSecureService>();
			Collection<IAliasTranslation> aliasTranslations = Alias == null ? new Collection<IAliasTranslation>(): aliasTranslationSecureService.GetAllByAlias(Alias.Id);

			var translationItems = new Collection<TranslationItem>();
			foreach (ILanguage language in languages)
			{
				ILanguage lang = language;
				IAliasTranslation aliasTranslation = aliasTranslations.FirstOrDefault(item => item.Language.Id == lang.Id);
				var translationItem = new TranslationItem
              	{
					Language = lang,
              		Value = (aliasTranslation != null ? aliasTranslation.Value : string.Empty)
              	};

				translationItems.Add(translationItem);
			}
			return translationItems;
		}
        private Collection<IAliasTranslation> GetTranslations(int id, Collection<ILanguage> languages, Repeater rptTranslations, string tbTranslateValue, string hfLangId)
		{
			var aliasTranslations = new Collection<IAliasTranslation>();
            foreach (RepeaterItem item in rptTranslations.Items)
			{
			    IAliasTranslation aliasTranslation = GetTranslation(id, item, tbTranslateValue, hfLangId, languages);
			    aliasTranslations.Add(aliasTranslation);
			}
            return aliasTranslations;
		}
		private IAliasTranslation GetTranslation(int id, Control item, string tbTranslateValue, string hfLangId, IEnumerable<ILanguage> languages)
		{
			string value;
			int languageId;
			GetControlsValue(item, tbTranslateValue, hfLangId, out value, out languageId);
			IAliasTranslation aliasTranslation = IoC.Resolve<IAliasTranslationSecureService>().Create();
			aliasTranslation.AliasId = id;
			aliasTranslation.Language = languages.FirstOrDefault(language => language.Id == languageId);
			aliasTranslation.Value = string.IsNullOrEmpty(value) ? null : value;
			return aliasTranslation;
		}
		private void GetControlsValue(Control control, string tbTranslateValue, string hfLanguageId, out string value, out int languageId)
		{
			var hiddenField = control.FindControl(hfLanguageId) as HiddenField;
			if (hiddenField == null)
			{
				throw new ArgumentNullException(hfLanguageId);
			}
			languageId = Int32.Parse(hiddenField.Value, CultureInfo.CurrentCulture);

			if (Mode != TranslationItemMode.Wysiwyg)
			{
				var tbValue = control.FindControl(tbTranslateValue) as TextBox;
				if (tbValue == null)
				{
					throw new ArgumentNullException(tbTranslateValue);
				}
				value = tbValue.Text;
			}
			else
			{
				var tbValue = control.FindControl(tbTranslateValue) as TinyMceEditor;
				if (tbValue == null)
				{
					throw new ArgumentNullException(tbTranslateValue);
				}
				value = tbValue.Value;
			}
		}
	}
}
