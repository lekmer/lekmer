﻿namespace Litium.Scensum.BackOffice.UserControls.Translation
{
	public enum TranslationItemMode
	{
		Simple,
		Multiline,
		Wysiwyg
	}
}
