﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls;

namespace Litium.Scensum.BackOffice.UserControls.Translation
{
	public partial class GenericWysiwygTranslator : StateUserControlController<GenericTranslatorState>
	{
		public ImageButton TriggerImageButton { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ITranslationGeneric> DataSource
		{
			get;
			set;
		}

		public string DefaultValueControlClientId
		{
			get
			{
				return SafeState.DefaultValueControlClientId;
			}
			set
			{
				SafeState.DefaultValueControlClientId = value;
			}
		}

		public int BusinessObjectId
		{
			get
			{
				return SafeState.BusinessObjectId;
			}
			set
			{
				SafeState.BusinessObjectId = value;
			}
		}

		protected virtual GenericTranslatorState SafeState
		{
			get
			{
				return State ?? (State = new GenericTranslatorState());
			}
		}

		protected override void SetEventHandlers(){ }
		protected override void PopulateControl()
		{
			SetWysiwygTranslations(GetTranslationItems());
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			TranslatePopup.BehaviorID = ClientID + "_translatorPopup";
            TranslatePopup.TargetControlID = TriggerImageButton.ID;
		    TranslatePopup.PopupControlID = WysiwygTranslateDiv.ClientID;

            CloseTranslationButton.OnClientClick += "clearPopupWysiwyg" + ID + @"('" + ValueTextBox.ClientID + "', '" + ValueWysiwygHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + WysiwygTranslatinListDiv.ClientID + "'); return false;";
			SaveButton.OnClientClick += "changeDefaultAndTranslationsWysiwyg" + ID + @"('" + ValueTextBox.ClientID + "', '" + DefaultValueControlClientId + "', '" + ValueWysiwygHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + WysiwygTranslatinListDiv.ClientID + "', '" + WysiwygTabsDiv.ClientID + "'); return false;";
            CancelButton.OnClientClick += "clearPopupWysiwyg" + ID + @"('" + ValueTextBox.ClientID + "', '" + ValueWysiwygHiddenField.ClientID + "', '" + TranslatePopup.BehaviorID + "', '" + WysiwygTranslatinListDiv.ClientID + "'); return false;";
			TriggerImageButton.OnClientClick += "SetDefaultValueWysiwyg" + ID + @"('" + DefaultValueControlClientId + "', '" + ValueTextBox.ClientID + "', '" + ValueWysiwygHiddenField.ClientID + "');";

			var clientScript = Page.ClientScript;

			clientScript.RegisterClientScriptBlock(GetType(), "JQueryTabs" + ID, @"$(document).ready(function() { $('#" + WysiwygTabsDiv.ClientID + @"').tabs(); });", true);
			//if (!clientScript.IsClientScriptBlockRegistered("RemoveRedundantSpaces"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "RemoveRedundantSpaces" + ID, @"
<script>
					function RemoveRedundantSpaces" + ID + @"(str) {
						return str.replace(/(&nbsp;)+|\s+/g, "" "").trim();
					};
</script>
				", false);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("closePopup"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "closePopup" + ID, @"
<script>
					function closePopup" + ID + @"(b) {
						$find(b).hide();
                        return false;
					};
</script>
				", false);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("clearPopupWysiwyg"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "clearPopupWysiwyg" + ID, @"
<script>
					function clearPopupWysiwyg" + ID + @"(t,h,b,l) {
						tinyMCE.getInstanceById(t).setContent(document.getElementById(h).value);
						var rptTranslations = document.getElementById(l)
						var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
						for (var i = 0; i < rptTranslationValues.length; i++) {
							var translationValue = rptTranslationValues[i];
							if (translationValue.type === ""textarea"" && translationValue.nextSibling.nextSibling.nextSibling.type === ""hidden"") {
								tinyMCE.getInstanceById(translationValue.id).setContent(translationValue.nextSibling.nextSibling.nextSibling.value);
							}
						}
						closePopup" + ID + @"(b);
                        return false;
					};
</script>
				", false);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("changeDefaultAndTranslationsWysiwyg"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "changeDefaultAndTranslationsWysiwyg" + ID, @"
<script>
					function changeDefaultAndTranslationsWysiwyg" + ID + @"(t,d,h,b,l,ts) {
						var defValue = RemoveRedundantSpaces" + ID + @"(tinyMCE.getInstanceById(t).getContent());
						tinyMCE.getInstanceById(t).setContent(defValue);
						tinyMCE.getInstanceById(d).setContent(defValue);
						document.getElementById(h).value = defValue;
						var rptTranslations = document.getElementById(l)
						var rptTranslationValues = rptTranslations.getElementsByTagName('textarea');
						var rptLanguages = document.getElementById(ts)
						var rptTranslationImages = rptLanguages.getElementsByTagName('img');
						var imgCount = 0;
						for (var i = 0; i < rptTranslationValues.length; i++) {
							var translationValue = rptTranslationValues[i];
							if (translationValue.type === ""textarea"" && translationValue.nextSibling.nextSibling.nextSibling.type === ""hidden"") {
								var translationVal = RemoveRedundantSpaces" + ID + @"(tinyMCE.getInstanceById(translationValue.id).getContent());
								tinyMCE.getInstanceById(translationValue.id).setContent(translationVal);
								translationValue.nextSibling.nextSibling.nextSibling.value = translationVal;
								var translationImg = rptTranslationImages[imgCount];
								imgCount = imgCount + 1;
								if (tinyMCE.getInstanceById(translationValue.id).getContent() === """") {
									translationImg.style.display = 'inline-block';
								}
								else {
									translationImg.style.display = 'none';
								}
							}
						}
						closePopup" + ID + @"(b);
                        return false;
					};
</script>
				", false);
			}
			//if (!clientScript.IsClientScriptBlockRegistered("SetDefaultValueWysiwyg"))
			{
				clientScript.RegisterClientScriptBlock(GetType(), "SetDefaultValueWysiwyg" + ID, @"
<script>
					function SetDefaultValueWysiwyg" + ID + @"(d,t,h) {
						var defValue = RemoveRedundantSpaces" + ID + @"(tinyMCE.getInstanceById(d).getContent());
						tinyMCE.getInstanceById(d).setContent(defValue);
						tinyMCE.getInstanceById(t).setContent(defValue);
						document.getElementById(h).value = defValue;
					};
</script>
				", false);
			}
		}

		protected static string GetExclamationVisibility(string value)
		{
			return string.IsNullOrEmpty(value) ? "display:inline-block" : "display:none";
		}

		public Collection<ITranslationGeneric> GetTranslations()
		{
			return GetTranslations(WysiwygTranslationRepeater, "tmTranslateValueWysiwyg", "hfLangIdWysiwyg");
		}

		private void SetWysiwygTranslations(Collection<TranslationItem> translationItems)
		{
			WysiwygLanguageRepeater.DataSource = translationItems;
			WysiwygLanguageRepeater.DataBind();
			
			WysiwygTranslationRepeater.DataSource = translationItems;
			WysiwygTranslationRepeater.DataBind();
		}

		private Collection<TranslationItem> GetTranslationItems()
		{
			Collection<ILanguage> languages = IoC.Resolve<ILanguageSecureService>().GetAll(SignInHelper.SignedInSystemUser);

			var translationItems = new Collection<TranslationItem>();
			foreach (ILanguage language in languages)
			{
				ILanguage lang = language;
				ITranslationGeneric translation = DataSource.FirstOrDefault(item => item.LanguageId == lang.Id);
				var translationItem = new TranslationItem
              	{
					Language = lang,
					Value = (translation != null ? translation.Value : string.Empty)
              	};

				translationItems.Add(translationItem);
			}
			return translationItems;
		}
		private Collection<ITranslationGeneric> GetTranslations(Repeater rptTranslations, string tbTranslateValue, string hfLangId)
		{
			var translations = new Collection<ITranslationGeneric>();
            foreach (RepeaterItem item in rptTranslations.Items)
			{
				ITranslationGeneric translation = GetTranslation(item, tbTranslateValue, hfLangId);
				translations.Add(translation);
			}
			return translations;
		}
		private ITranslationGeneric GetTranslation(Control item, string tbTranslateValue, string hfLangId)
		{
			string value;
			int languageId;
			GetControlsValue(item, tbTranslateValue, hfLangId, out value, out languageId);
			var translation = IoC.Resolve<ITranslationGeneric>();
			translation.Id = BusinessObjectId;
			translation.LanguageId = languageId;
			translation.Value = string.IsNullOrEmpty(value) ? null : value;
			return translation;
		}
		private static void GetControlsValue(Control control, string tbTranslateValue, string hfLanguageId, out string value, out int languageId)
		{
			var hiddenField = control.FindControl(hfLanguageId) as HiddenField;
			if (hiddenField == null)
			{
				throw new ArgumentNullException(hfLanguageId);
			}
			languageId = Int32.Parse(hiddenField.Value, CultureInfo.CurrentCulture);

			var tbValue = control.FindControl(tbTranslateValue) as TinyMceEditor;
			if (tbValue == null)
			{
				throw new ArgumentNullException(tbTranslateValue);
			}
			value = tbValue.Value;
		}
	}
}
