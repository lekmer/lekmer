﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Web.Controls.Exceptions;

namespace Litium.Scensum.BackOffice.UserControls.Common
{
	public partial class CurrencyValueManager : UserControl
	{
		protected class CurrencyRepeaterDataSourceItem
		{
			public ICurrency Currency { get; set; }
			public string MonetaryValue { get; set; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public CurrencyValueDictionary DataSource { get; set; }

		public override void DataBind()
		{
			if (DataSource == null)
			{
				throw new ControlIntegrationException("DataSource shoule be initialized before calling DataBind().");
			}

			base.DataBind();

			
			var currencyValues = new Collection<CurrencyRepeaterDataSourceItem>();
			var currencySecureService = IoC.Resolve<ICurrencySecureService>();
			Collection<ICurrency> currencies = currencySecureService.GetAll();
			foreach (ICurrency currency in currencies)
			{
				CurrencyRepeaterDataSourceItem item;
				CurrencyValue currencyValue;
				if (!DataSource.TryGetValue(currency.Id, out currencyValue))
				{
					item = new CurrencyRepeaterDataSourceItem
					{
						Currency = currency,
						MonetaryValue = string.Empty
					};
				}
				else
				{
					item = new CurrencyRepeaterDataSourceItem
					{
						Currency = currencyValue.Currency,
						MonetaryValue = currencyValue.MonetaryValue.ToString("N", CultureInfo.CurrentCulture)
					};
				}
				currencyValues.Add(item);
			}
			CurrencyValueRepeater.DataSource = currencyValues;
			CurrencyValueRepeater.DataBind();
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "0#")]
		public bool IsValid(out Collection<string> validationMessages)
		{
			bool isValid = true;
			bool noneValueDefined = true;
			validationMessages = new Collection<string>();
			foreach (RepeaterItem repeaterItem in CurrencyValueRepeater.Items)
			{
				var monetaryValueText = ((TextBox)repeaterItem.FindControl("ValueTextBox")).Text;
				if (monetaryValueText.Trim().Length == 0)
				{
					continue;
				}
				decimal monetaryValue;
				if (!decimal.TryParse(monetaryValueText, out monetaryValue))
				{
					isValid = false;
					var currencyIsoText = ((Literal)repeaterItem.FindControl("CurrencyIsoLiteral")).Text;
					validationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyFormat, currencyIsoText));
				}
				else if (monetaryValue < 0)
				{
					isValid = false;
					var currencyIsoText = ((Literal)repeaterItem.FindControl("CurrencyIsoLiteral")).Text;
					validationMessages.Add(string.Format(CultureInfo.CurrentCulture, Resources.GeneralMessage.CurrencyValueNegative, currencyIsoText));
				}
				noneValueDefined = false;
			}

			if (noneValueDefined)
			{
				isValid = false;
				validationMessages.Add(Resources.CampaignMessage.NoCurrencyValueSet);
			}

			return isValid;
		}
		public CurrencyValueDictionary CollectCurrencyValues()
		{
			var currencyValueDictionary = new CurrencyValueDictionary();
			foreach (RepeaterItem repeaterItem in CurrencyValueRepeater.Items)
			{
				var monetaryValueText = ((TextBox)repeaterItem.FindControl("ValueTextBox")).Text;
				if (monetaryValueText.IsNullOrTrimmedEmpty())
				{
					continue;
				}
				decimal monetaryValue = decimal.Parse(monetaryValueText, CultureInfo.CurrentCulture);

				var currencyIdText = ((HiddenField)repeaterItem.FindControl("CurrencyIdHiddenField")).Value;
				var currencySecureService = IoC.Resolve<ICurrencySecureService>();
				int currencyId = int.Parse(currencyIdText, CultureInfo.CurrentCulture);
				ICurrency currency = currencySecureService.GetById(currencyId);

				var currencyValue = new CurrencyValue { Currency = currency, MonetaryValue = monetaryValue };
				currencyValueDictionary.Add(currencyValue);
			}
			return currencyValueDictionary;
		}
	}
}