﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CurrencyValueManager.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Common.CurrencyValueManager" %>

<div class="currencyvalue-wrapper">
<asp:Repeater runat="server" ID="CurrencyValueRepeater">
<HeaderTemplate>
	<div class="currencyvalue-header-wrapper">
		<span class="currencyvalue-header-left"><%=Resources.General.Literal_Value%></span><span><%=Resources.General.Literal_Currency%></span>
	</div>
</HeaderTemplate>
<ItemTemplate>
	<div class="currencyvalue-content">
		<asp:TextBox runat="server" ID="ValueTextBox" Text='<%# Eval("MonetaryValue") %>' Width="70px" />&nbsp;&nbsp;&nbsp;
		<asp:Literal runat="server" ID="CurrencyIsoLiteral" Text='<%# Eval("Currency.Iso") %>' />
		<asp:HiddenField runat="server" ID="CurrencyIdHiddenField" Value='<%# Eval("Currency.Id") %>' />
	</div>
</ItemTemplate>
</asp:Repeater>
</div>