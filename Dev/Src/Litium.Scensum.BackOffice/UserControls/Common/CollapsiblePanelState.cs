using System;

namespace Litium.Scensum.BackOffice.UserControls.Common
{
	[Serializable]
	public class CollapsiblePanelState
	{
		public string Title { get; set; }
		public string CommandArgument { get; set; }
		public string ActionButtonCommandArgument { get; set; }
		public string ControlToExpand { get; set; }
		public bool HasAction { get; set; }
	}
}