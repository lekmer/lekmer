//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.235
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "10.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Review {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Review() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Review", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear all.
        /// </summary>
        internal static string Button_ClearAll {
            get {
                return ResourceManager.GetString("Button_ClearAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Art No: .
        /// </summary>
        internal static string Literal_ArtNoDD {
            get {
                return ResourceManager.GetString("Literal_ArtNoDD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ascending.
        /// </summary>
        internal static string Literal_Ascending {
            get {
                return ResourceManager.GetString("Literal_Ascending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User name / Alias.
        /// </summary>
        internal static string Literal_Author {
            get {
                return ResourceManager.GetString("Literal_Author", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit product review block.
        /// </summary>
        internal static string Literal_BlockProductReviewEdit {
            get {
                return ResourceManager.GetString("Literal_BlockProductReviewEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created: {0} by &quot;{1}&quot; .
        /// </summary>
        internal static string Literal_Created {
            get {
                return ResourceManager.GetString("Literal_Created", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created date from.
        /// </summary>
        internal static string Literal_CreatedDateFrom {
            get {
                return ResourceManager.GetString("Literal_CreatedDateFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to to.
        /// </summary>
        internal static string Literal_CreatedDateTo {
            get {
                return ResourceManager.GetString("Literal_CreatedDateTo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to review.
        /// </summary>
        internal static string Literal_DeleteReview {
            get {
                return ResourceManager.GetString("Literal_DeleteReview", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to reviews.
        /// </summary>
        internal static string Literal_DeleteReviews {
            get {
                return ResourceManager.GetString("Literal_DeleteReviews", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Descending.
        /// </summary>
        internal static string Literal_Descending {
            get {
                return ResourceManager.GetString("Literal_Descending", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Message contains.
        /// </summary>
        internal static string Literal_MessageContains {
            get {
                return ResourceManager.GetString("Literal_MessageContains", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product.
        /// </summary>
        internal static string Literal_Product {
            get {
                return ResourceManager.GetString("Literal_Product", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product id.
        /// </summary>
        internal static string Literal_ProductId {
            get {
                return ResourceManager.GetString("Literal_ProductId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product title.
        /// </summary>
        internal static string Literal_ProductTitle {
            get {
                return ResourceManager.GetString("Literal_ProductTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rating: {0}.
        /// </summary>
        internal static string Literal_RatingDD {
            get {
                return ResourceManager.GetString("Literal_RatingDD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Review.
        /// </summary>
        internal static string Literal_Review {
            get {
                return ResourceManager.GetString("Literal_Review", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Review search.
        /// </summary>
        internal static string Literal_ReviewSearch {
            get {
                return ResourceManager.GetString("Literal_ReviewSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search Result - Displaying {0} of {1} items.
        /// </summary>
        internal static string Literal_SearchResult {
            get {
                return ResourceManager.GetString("Literal_SearchResult", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sort creation date: .
        /// </summary>
        internal static string Literal_SortCreationDateDD {
            get {
                return ResourceManager.GetString("Literal_SortCreationDateDD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status: .
        /// </summary>
        internal static string Literal_StatusDD {
            get {
                return ResourceManager.GetString("Literal_StatusDD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Block product review has been saved successfully..
        /// </summary>
        internal static string Message_BlockProductReviewSavedSuccessfully {
            get {
                return ResourceManager.GetString("Message_BlockProductReviewSavedSuccessfully", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Are you sure to clear all criteria?.
        /// </summary>
        internal static string Message_ClearAll {
            get {
                return ResourceManager.GetString("Message_ClearAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of rows could not be empty..
        /// </summary>
        internal static string Message_EmptyRowsNumber {
            get {
                return ResourceManager.GetString("Message_EmptyRowsNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created date from should be less or equal than created date to..
        /// </summary>
        internal static string Message_InvalidDateRange {
            get {
                return ResourceManager.GetString("Message_InvalidDateRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created date to should be date time..
        /// </summary>
        internal static string Message_InvalidEndDate {
            get {
                return ResourceManager.GetString("Message_InvalidEndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product id should be integer..
        /// </summary>
        internal static string Message_InvalidProductId {
            get {
                return ResourceManager.GetString("Message_InvalidProductId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Number of rows must be an integer between 1 and {0}..
        /// </summary>
        internal static string Message_InvalidRowsNumber {
            get {
                return ResourceManager.GetString("Message_InvalidRowsNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created date from should be date time..
        /// </summary>
        internal static string Message_InvalidStartDate {
            get {
                return ResourceManager.GetString("Message_InvalidStartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to None review is selected..
        /// </summary>
        internal static string Message_NoReviewSelected {
            get {
                return ResourceManager.GetString("Message_NoReviewSelected", resourceCulture);
            }
        }
    }
}
