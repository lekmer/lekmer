using Litium.Framework.Setting;

namespace Litium.Scensum.BackOffice.Setting
{
    public class CampaignControlSetting : SettingBase
    {
        private readonly string _groupName;

        public CampaignControlSetting(string groupName)
		{
			_groupName = groupName;
		}

		protected override string StorageName
		{
            get { return "CampaignControl"; }
		}

        public string ControlVirtualPath
		{
            get { return GetString(_groupName, "ControlVirtualPath", null); }
		}
    }
}