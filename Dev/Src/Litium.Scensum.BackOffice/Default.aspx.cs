using System;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice
{
	public partial class Default : PageController
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			Response.Redirect(PathHelper.GetStartUrl());
		}

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateForm()
		{
		}
	}
}