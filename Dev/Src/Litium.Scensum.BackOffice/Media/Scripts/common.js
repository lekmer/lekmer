﻿
function OpenClose(divGvId, hfIsOpenId, imgId) {
	hfIsOpen = document.getElementById(hfIsOpenId);
	imgExpandable = document.getElementById(imgId);
	divGV = document.getElementById(divGvId);
	if (hfIsOpen.value == 'false') {
		hfIsOpen.value = 'true';
		imgExpandable.src = '../../../Media/Images/Common/up.gif';
		divGV.style.display = 'block';
	}
	else {
		hfIsOpen.value = 'false';
		imgExpandable.src = '../../../Media/Images/Common/down.gif';
		divGV.style.display = 'none';
	}
}

// added possibility to initialize open/close image sources because '../../../Media/Images' doesn't suit for different page nesting levels.
function OpenClose(targetId, stateHolderId, stateImgId, openImgSrc, closeImgSrc) {
	stateHolderId = document.getElementById(stateHolderId);
	stateImgId = document.getElementById(stateImgId);
	targetId = document.getElementById(targetId);
	if (stateHolderId.value == 'false') {
		stateHolderId.value = 'true';
		stateImgId.src = closeImgSrc;
		targetId.style.display = 'block';
	}
	else {
		stateHolderId.value = 'false';
		stateImgId.src = openImgSrc;
		targetId.style.display = 'none';
	}
}

// select all checkoxes in grid.
function SelectAll1(checkBoxId, gridId) {
	var checkBox = document.getElementById(checkBoxId)
	var checked = checkBox.checked;

	var gridCheckBoxes = document.getElementsByTagName('input');
	for (var i = 0; i < gridCheckBoxes.length; i++) {
		var checkBox = gridCheckBoxes[i];
		if (checkBox.type === "checkbox" && checkBox.id.indexOf(gridId) > -1) {
			checkBox.checked = checked;
		}
	}
}

// Show/Hide block options.
function ShowBlockOption(checkBox, divBlockOptionId) {
    var checked = false;
    var tbody = checkBox.parentNode.parentNode.parentNode;
    var checkBoxes = $(tbody).find('input:checkbox')
    
    for (var i = 0; i < checkBoxes.length; i++) {

        var checkBoxTemp = checkBoxes[i];
        if (checkBoxTemp.id.indexOf("SelectCheckBox") > -1) {
            if (checkBoxTemp.checked === true) {
                checked = true;
                break;
            }
        }
    }
    var divBlockOption = document.getElementById(divBlockOptionId)
    divBlockOption.style.display = checked ? 'block' : 'none';
}

// unselect main CheckBox('SelectAll') in grid header.
function UnselectMain(checkBox, mainCheckBoxId) {
	if (!checkBox.checked) {
		document.getElementById(mainCheckBoxId).checked = false
	}
}

//set textbox maxlength where it has Multiline
function CheckTextBoxLength(e, el, maxLength) {
	switch (e.keyCode) {
		case 37: // left
			return true;
		case 38: // up
			return true;
		case 39: // right
			return true;
		case 40: // down
			return true;
		case 8: // backspace
			return true;
		case 46: // delete
			return true;
		case 27: // escape
			el.value = '';
			return true;
	}
	return (el.value.length < maxLength);
}

// save cursor position in TextBox.
function storeCaret(ftext) {
	$(ftext).focus();
	// IE support.
	if (document.selection) {
		if (ftext.createTextRange) {
			ftext.caretPos = document.selection.createRange().duplicate();
			var cursorPosition = getCursorPosition(ftext);
			setCaretStorage(ftext, cursorPosition);
		}
	}
	// MOZILLA support.
	else {
		setCaretStorage(ftext, ftext.selectionStart);
	}
}

// get cursor position (for IE only).
function getCursorPosition(contentBox) {
	var c = "\003";
	var caretPos = contentBox.caretPos;
	var duplicate = caretPos.duplicate();
	var pos = 0;
	duplicate.moveToElementText(contentBox);
	var orgText = caretPos.text;
	caretPos.text = c;	
	pos = (duplicate.text.indexOf(c));
	
	/*position is calculated including return/newline symbols and this should be counted and corrected*/
	if (pos > 0 && pos < duplicate.text.length) {
		var sub = duplicate.text.substring(0, pos);
		var encoded = escape(sub);
		var breaks = encoded.split("%0D%0A").length - 1;
		pos = pos - breaks;
	}
	
	caretPos.moveStart('character', -1);
	caretPos.text = orgText;
	if (pos < 0) {
		pos = contentBox.innerText.length;
	}
	return pos;
}

// insert value to TextBox.
function insert(contentBox, insertedValue) {
	contentBox.focus();
	var cursorPosition = getCaretStorage(contentBox);
	if (cursorPosition == '') {
		cursorPosition = contentBox.value.length;
	}
	setCaretPosition(contentBox, cursorPosition)
	// IE support.
	if (document.selection) {
		if (contentBox.createTextRange) {
			contentBox.caretPos = document.selection.createRange().duplicate();
			var pos = getCursorPosition(contentBox);
			var caretPos = contentBox.caretPos;
			caretPos.text = insertedValue;
			var newPos = pos + insertedValue.length;
			setCaretPosition(contentBox, newPos);
			setCaretStorage(contentBox, newPos);
		} else {
			contentBox.value += insertedValue;
		}
	}
	// MOZILLA support.
	else {
		var startPos = contentBox.selectionStart;
		var endPos = contentBox.selectionEnd;
		contentBox.value = contentBox.value.substring(0, startPos) + insertedValue + contentBox.value.substring(endPos, contentBox.value.length);
		var newPos = startPos + insertedValue.length;
		setCaretPosition(contentBox, newPos);
		setCaretStorage(contentBox, newPos);
	}
}

// set cursor to position after insert.
function setCaretPosition(elem, caretPos) {
	// IE support.
	if (elem.createTextRange) {
		var range = elem.createTextRange();
		range.move('character', caretPos);
		range.select();
	}
	// MOZILLA support.
	else {
		elem.focus();
		elem.setSelectionRange(caretPos, caretPos);
	}
}
// DONT USE.
function clearCaret(ftext) {
	if (ftext.createTextRange) {
		ftext.caretPos = ftext.value.length;
	} else {
		ftext.selectionStart = ftext.value.length;
	}
}

// get/set cursor position from/to hidden field (for post back support)
function getCaretStorage(elem) {
	return $(elem).parent().find("div#caret-storage").find("input:first").val();
}
function setCaretStorage(elem, caretPos) {
	$(elem).parent().find("div#caret-storage").find("input:first").val(caretPos);
}

// get/set cursor position from/to hidden field (for post back support)
function getCaretStorage(elem) {
	return $(elem).parent().find("div#caret-storage").find("input:first").val();
}
function setCaretStorage(elem, caretPos) {
	$(elem).parent().find("div#caret-storage").find("input:first").val(caretPos);
}

// Confirmation functions.
function DeleteConfirmation(objectName) {
	return confirm('Are you sure that you want to delete the ' + objectName + '?');
}
function DeleteConfirmationContentNode() {
	return confirm('Are you sure that you want to delete the node?\n(If it has children nodes they will be deleted too)');
}
function DeleteConfirmationForOrdinals(objectName, ordinalId) {
	var ordinalChange = document.getElementById(ordinalId);
	var ordinalWarning = '';
	if (ordinalChange.value == 'True')
	{ ordinalWarning = ' All order changes will be lost.' }
	var confirmResult = confirm('Are you sure that you want to delete the ' + objectName + '?' + ordinalWarning);
	if (confirmResult)
	{ ordinalChange.value = 'False' }
	return confirmResult;
}
function DeleteFolderConfirmation(objectName) {
	return confirm("This folder will be deleted alone with its inner folders and " + objectName + " in all Channels. Do you want to delete it?")
}
function DeleteCategoryConfirmation() {
	return confirm("This category will be deleted alone with its child categories. Do you want to delete it?")
}

function DeleteBrandConfirmation() {
    return confirm("By doing this all products on this brand will loose there brand. Do you want to delete this " + objectName + "? ")
}

function DeleteAreaConfirmation() {
	return confirm("There are content pages using this template. Do you want to delete this area anyway?")
}
function DeleteTemplateWithContentAreasConfirmation() {
	return confirm("There content pages using this template. Do you want to delete this template and content areas attached anyway?")
}

function DeleteTagConfirmation() {
    return confirm("Deleting the tag will immediately remove it for all products. Do you want to delete this tag anyway?")
}

// Disable form after click 'Save'.
function DisableForm() {
	Disable(null);
}
function DisableFormWithValidation(validationGroup) {
	if (ValidateForm(validationGroup)) {
		Disable(null);
	}
}
function DisablePopup(validationGroup, popupDivId) {
	if (ValidateForm(validationGroup)) {
		Disable(popupDivId);
	}
}
function DisablePopupProgress(validationGroup, progressId, popupDivId) {
	if (ValidateForm(validationGroup)) {
		UpdateZIndex(progressId, popupDivId)
	}
}
function ValidateForm(validationGroup) {
	Page_ClientValidate(validationGroup);
	return Page_IsValid;
}
function Disable(popupDivId) {
	var disabledDiv = document.getElementById('content-disabled-progress');
	if (popupDivId != null && disabledDiv != null) {
		var popup = document.getElementById(popupDivId);
		disabledDiv.style.zIndex = popup.style.zIndex + 1;
	}
	if (disabledDiv != null) {
		document.body.appendChild(disabledDiv);
	}
}
function UpdateZIndex(progressId, popupDivId) {
	if (progressId != null) {
		if (popupDivId != null) {
			var popup = document.getElementById(popupDivId);
			document.getElementById(new String(progressId)).style.zIndex = popup.style.zIndex + 2;
			popup.style.zIndex = popup.style.zIndex - 1;
		}
	}
}
function HideMessages(messagesId) {
	var messages = document.getElementById(messagesId);
	if (messages != null) {
		messages.style.display = 'none';
	}
}

// Check single RadioButton.
function SingleRadioBtn(rbtnId, gridId, hfDefaultProductId, productId) {
	var grid = document.getElementById(gridId);
	var gridRadioBtns = grid.getElementsByTagName('input');
	for (var i = 0; i < gridRadioBtns.length; i++) {
		var rbtn = gridRadioBtns[i];
		if (rbtn.type === "radio") {
			rbtn.checked = false;
		}
	}

	var checkedRadioBtn = document.getElementById(rbtnId);
	checkedRadioBtn.checked = true;

       var checkedRadioBtn = document.getElementById(rbtnId);
       checkedRadioBtn.checked = true;
       
       var hfDefaultProduct = document.getElementById(hfDefaultProductId);
       hfDefaultProduct.value = productId;
      }

      // <UserCredential user control>
      function clonePassword(passwordTextBoxId, passwordHiddenId) {
      	var pass = $("#" + passwordTextBoxId);
      	var passHidden = $("#" + passwordHiddenId);
      	if (pass == null || passHidden == null) return;
      	passHidden.val(pass.val());
      }

      function cloneConfirm(confirmTextBoxId, confirmHiddenId) {
      	var confirm = $("#" + confirmTextBoxId);
      	var confirmHidden = $("#" + confirmHiddenId);
      	if (confirm == null || confirmHidden == null) return;
      	confirmHidden.val(confirm.val());
      }

      function restorePassword(passwordTextBoxId, passwordHiddenId, confirmTextBoxId, confirmHiddenId) {
      	var pass = $("#" + passwordTextBoxId);
      	var passHidden = $("#" + passwordHiddenId);
      	if (pass != null && passHidden != null && passHidden.val() != "") {
      		pass.val(passHidden.val());
      	}
      	var confirm = $("#" + confirmTextBoxId);
      	var confirmHidden = $("#" + confirmHiddenId);
      	if (confirm != null && confirmHidden != null && confirmHidden.val() != "") {
      		confirm.val(confirmHidden.val());
      	}
      }
      // </UserCredential user control>

// <Script for dateTime masked editor>
//function ChangeMaskedStartDate() {
//	var date = $(".start-date-hidden").val();
//	var dateTime = $(".start-date-box").val();
//	var pos = dateTime.indexOf(' ');
//	var time = dateTime.substr(pos, dateTime.length - pos);
//	$(".start-date-box").val(date.concat(time));
//}
//function ChangeMaskedEndDate() {
//	var date = $(".end-date-hidden").val();
//	var dateTime = $(".end-date-box").val();
//	var pos = dateTime.indexOf(' ');
//	var time = dateTime.substr(pos, dateTime.length - pos);
//	$(".end-date-box").val(date.concat(time));
//}
// </Script for dateTime masked editor>



// <Script for campaign popups resizing according to the screen resolution>
function ResizePopup(divId, horizontalRatio, verticalRatio, headerToContentRatio) {
	var aWidth = getPageWidth();
	var aHeight = getPageHeight();
	var pWidth = aWidth * horizontalRatio;
	var pHeight = aHeight * verticalRatio;
	
	/*Div that contains popup*/
	var popupDiv = $("div[id$='" + divId + "']");
	
	/*Popup container*/
	var popupConteiner = popupDiv.attr('class') == "campaign-popup-container"
		? popupDiv
		: $(popupDiv).find("div.campaign-popup-container:first");
	popupConteiner.css({
		'width': pWidth,
		'height': pHeight,
		'position': 'fixed',
		'left': (aWidth - pWidth) * 0.5,
		'top': (aHeight - pHeight) * 0.25
	});

	/*Popup header*/
	var headerHeight = pHeight * headerToContentRatio;
	var minWidth = 7;
	$(popupDiv).find("div.campaign-popup-header:first").css({ 'width': '100%', 'height': headerHeight });
	$(popupDiv).find("div.campaign-popup-header-left:first").css({ 'width': minWidth, 'height': '100%' });
	$(popupDiv).find("div.campaign-popup-header-right:first").css({ 'width': minWidth, 'height': '100%' });
	$(popupDiv).find("div.campaign-popup-header-center:first").css({ 'width': pWidth - 2 * minWidth, 'height': '100%' });
	
	var closeButton = $(popupDiv).find("div.campaign-popup-header-center:first").find("input:first");
	var closeButtonHeight = parseInt(closeButton.css("height").replace('px', ''));
	$(popupDiv).find("div.campaign-popup-header-center:first").find("span, input").css('margin-top', (headerHeight - closeButtonHeight) * 0.5);

	/*Popup body*/
	var bodyHeight = pHeight * (1 - headerToContentRatio);
	var popupBody = $(popupDiv).find("div[class^='campaign-popup-content']:first");
	var borderTopAndBotWidth = parseInt(popupBody.css("border-top-width").replace('px', '')) + parseInt(popupBody.css("border-bottom-width").replace('px', ''));
	var borderLefAndRigWidth = parseInt(popupBody.css("border-left-width").replace('px', '')) + parseInt(popupBody.css("border-right-width").replace('px', ''));
	var paddingTopAndBotValue = parseInt(popupBody.css("padding-top").replace('px', '')) + parseInt(popupBody.css("padding-bottom").replace('px', ''));
	var paddingLefAndRigValue = parseInt(popupBody.css("padding-left").replace('px', '')) + parseInt(popupBody.css("padding-right").replace('px', ''));
	popupBody.css({
		'width': pWidth - borderLefAndRigWidth - paddingLefAndRigValue - 1,
		'height': bodyHeight - borderTopAndBotWidth - paddingTopAndBotValue
	});
}
function getPageWidth() {
	return window.innerWidth != null
		? window.innerWidth
		: document.documentElement && document.documentElement.clientWidth
			? document.documentElement.clientWidth
			: document.body != null
				? document.body.clientWidth 
				: window.screen.availWidth;
}
function getPageHeight() {
	return window.innerHeight != null
		? window.innerHeight
		: document.documentElement && document.documentElement.clientHeight
			? document.documentElement.clientHeight
			: document.body != null
				? document.body.clientHeight 
				: window.screen.availHeight;
} 
// </Script for popups resizing according to the screen resolution>


// Show/hide bulk update panel.
function ShowBulkUpdatePanel(checkBoxId, gridClientId, divClientId) {
	var parent = $("#" + gridClientId);
	var checked = $(parent).find("input[id*='" + checkBoxId + "']:checked").length > 0;
	$("#" + divClientId).css('display', checked ? 'block' : 'none');
}


