﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageSelect.aspx.cs" Inherits="Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager.ImageSelect" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
        <title>Add image</title>
    <link href="~/Media/Css/navigation.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/Grid.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/Channel.css" rel="stylesheet" type="text/css" />   
    <link href="~/Media/Css/Assortment.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/content.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/Campaign.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/Interface.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/Customer.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/Settings.css" rel="stylesheet" type="text/css" />
    <link href="~/Media/Css/SiteStructure.css" rel="stylesheet" type="text/css" /> 
    <link href="~/Media/Css/Media.css" rel="stylesheet" type="text/css" />  
	<link href="~/Media/Css/templated-tree-simple.css" rel="stylesheet" type="text/css" /> 
	<link href="~/Media/Css/Popup.css" rel="stylesheet" type="text/css" />
	<link href="~/Media/Css/Form.css" rel="stylesheet" type="text/css" />
	<link href="~/Media/Css/ContextMenu.css" rel="stylesheet" type="text/css" />
	<link href="~/Media/Css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css"/>
	 <link href="~/Media/Css/User.css" rel="stylesheet" type="text/css" />
	<link href="css/imagemanager.css" rel="stylesheet" type="text/css" /> 
	<link href="~/Media/Css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css"/>
	
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/dialog.js"></script>
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">        
            <asp:ScriptManager ID="sm" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"/>
            <div class="container">
                    <script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>"
	                    type="text/javascript"></script>		                    
                    <uc:ImageSelect id="imageSelect" runat="server"/>
                    <input id="someval" name="someval" type="hidden" class="text" runat="server" />                                    	
            </div>
    </form>
</body>
</html>
