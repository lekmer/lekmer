using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Media.TinyMCE.js.tiny_mce.plugins.imagemanager
{
	public partial class ImageSelect : PageController
	{
		private static string BackOfficeUrl
		{
			get
			{
				HttpContext context = HttpContext.Current;
				string protocol = context.Request.IsSecureConnection ? "https://" : "http://";

				string domain = context.Request.Url.Host;

				string directory = "/";
				if (!context.Request.ApplicationPath.Equals("/"))
				{
					directory = context.Request.ApplicationPath + "/";
				}

				return protocol + domain + directory;
			}
		}

		protected override void SetEventHandlers()
		{
			imageSelect.Selected += ImageSelectImageSelection;
		}

		private void ImageSelectImageSelection(object sender, ImageSelectEventArgs e)
		{
			IImage image = e.Image;
			string imageUrl = GetUrl(string.Format(
						CultureInfo.CurrentCulture, "{0}{1}/{2}.{3}",
						WebSetting.Instance.MediaUrl, image.Id, UrlCleaner.CleanUp(image.Title), image.FormatExtension));

			Response.Redirect(PathHelper.Media.GetDialogCloseUrl(imageUrl, image.AlternativeText, image.Title));
		}

		private static string GetUrl(string relativeUrl)
		{
			if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

			relativeUrl = Regex.Replace(relativeUrl, @"^\~\/", string.Empty);

			return BackOfficeUrl + relativeUrl;
		}
        
		protected override void PopulateForm()
		{
		}
	}
}
