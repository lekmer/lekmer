(function() {
    tinymce.PluginManager.requireLangPack('imagemanager');
    tinymce.create('tinymce.plugins.ImagePlugin',
    { init: function(ed, url) {
    ed.addCommand('mceImageManager', function() {
    ed.windowManager.open({
    file: url + '/ImageSelect.aspx',
    width: 830,
    height: 455,
    inline: 1
    }, {
    plugin_url: url, // Plugin absolute URL
    some_custom_arg: 'custom arg' // Custom argument
    });
}); 
    
ed.addButton('imagemanager', { title: 'Insert/Edit Images', cmd: 'mceImageManager', image: url + '/img/imagemanager.png' });
ed.onNodeChange.add(function(ed, cm, n) { cm.setActive('imagemanager', n.nodeName == 'IMG'); });
},
createControl: function(n, cm) { return null; }, 
getInfo: function() {
    return {
    longname: 'Image manager plugin', author: 'Some author'
    };
}
});
tinymce.PluginManager.add('imagemanager', tinymce.plugins.ImagePlugin);
})();