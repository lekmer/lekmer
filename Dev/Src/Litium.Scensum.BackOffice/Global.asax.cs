using System;
using System.Web;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;
using log4net.Config;

namespace Litium.Scensum.BackOffice
{
	public class Global : HttpApplication
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Start(object sender, EventArgs e)
		{
			CacheUpdateCleaner.CacheUpdateFeed = new CacheUpdateFeed();

			XmlConfigurator.Configure();

			Initializer.InitializeAll();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_Start(object sender, EventArgs e)
		{
			HttpCookie cookies = new HttpCookie(FileSystemStatePage.ViewStateCookiesKey, Guid.NewGuid().ToString());
			Response.Cookies.Add(cookies);
		}
	}
}