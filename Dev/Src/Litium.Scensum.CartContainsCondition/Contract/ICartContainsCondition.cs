﻿using Litium.Scensum.Campaign;

namespace Litium.Scensum.CartContainsCondition
{
	public interface ICartContainsCondition : ICondition
	{
		int MinQuantity { get; set; }
		bool AllowDuplicates { get; set; }
		bool IncludeAllProducts { get; set; }
		ProductIdDictionary IncludeProducts { get; set; }
		CategoryDictionary IncludeCategories { get; set; }
		ProductIdDictionary ExcludeProducts { get; set; }
		CategoryDictionary ExcludeCategories { get; set; }
	}
}
