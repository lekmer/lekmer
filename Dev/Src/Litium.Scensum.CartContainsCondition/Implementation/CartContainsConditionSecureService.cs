﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartContainsCondition.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.CartContainsCondition
{
	public class CartContainsConditionSecureService : IConditionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected CartContainsConditionRepository Repository { get; private set; }
		protected IConditionTypeSecureService ConditionTypeSecureService { get; private set; }

		public CartContainsConditionSecureService(
			IAccessValidator accessValidator,
			CartContainsConditionRepository repository,
			IConditionTypeSecureService conditionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			ConditionTypeSecureService = conditionTypeSecureService;
		}

		public virtual ICondition Create()
		{
			if (ConditionTypeSecureService == null)
				throw new InvalidOperationException("ConditionTypeSecureService cannot be null.");

			var condition = IoC.Resolve<ICartContainsCondition>();
			condition.ConditionType = ConditionTypeSecureService.GetByCommonName("CartContains");
			condition.IncludeProducts = new ProductIdDictionary();
			condition.ExcludeProducts = new ProductIdDictionary();
			condition.IncludeCategories = new CategoryDictionary();
			condition.ExcludeCategories = new CategoryDictionary();
			condition.Status = BusinessObjectStatus.New;
			return condition;
		}

		public virtual ICondition GetById(int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var condition = Repository.GetById(id);
			if (condition == null) return null;

			condition.IncludeProducts = Repository.GetIncludeProducts(id);
			condition.IncludeCategories = Repository.GetIncludeCategories(id);
			condition.ExcludeProducts = Repository.GetExcludeProducts(id);
			condition.ExcludeCategories = Repository.GetExcludeCategories(id);
			return condition;
		}

		public virtual void Save(ISystemUserFull systemUserFull, ICondition condition)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var cartContainsCondition = condition as ICartContainsCondition;
			if (cartContainsCondition == null) throw new InvalidOperationException("condition is not ICartContainsCondition type");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(cartContainsCondition);
				SaveIncludeProducts(cartContainsCondition);
				SaveIncludeCategories(cartContainsCondition);
				SaveExcludeProducts(cartContainsCondition);
				SaveExcludeCategories(cartContainsCondition);
				transactedOperation.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int id)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		protected virtual void SaveIncludeProducts(ICartContainsCondition condition)
		{
			Repository.DeleteIncludeProducts(condition.Id);

			if (condition.IncludeAllProducts)
			{
				return;
			}
			ProductIdDictionary productIdDictionary = condition.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertIncludeProduct(condition.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveExcludeProducts(ICartContainsCondition condition)
		{
			Repository.DeleteExcludeProducts(condition.Id);

			ProductIdDictionary productIdDictionary = condition.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertExcludeProduct(condition.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveIncludeCategories(ICartContainsCondition condition)
		{
			Repository.DeleteIncludeCategories(condition.Id);

			if (condition.IncludeAllProducts)
			{
				return;
			}
			CategoryDictionary categories = condition.IncludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, bool> category in categories)
			{
				Repository.InsertIncludeCategory(condition.Id, category.Key, category.Value);
			}
		}

		protected virtual void SaveExcludeCategories(ICartContainsCondition condition)
		{
			Repository.DeleteExcludeCategories(condition.Id);

			CategoryDictionary categories = condition.ExcludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}
			foreach (KeyValuePair<int, bool> category in categories)
			{
				Repository.InsertExcludeCategory(condition.Id, category.Key, category.Value);
			}
		}
	}
}