﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.CartContainsCondition.Repository
{
	public class CartContainsConditionRepository
	{
		protected virtual DataMapperBase<ICartContainsCondition> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartContainsCondition>(dataReader);
		}

		public virtual ICartContainsCondition GetById(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ConditionId", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetById");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionGetById]", parameters,
																		 dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(ICartContainsCondition condition)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ConditionId", condition.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MinQuantity", condition.MinQuantity, SqlDbType.Int),
					ParameterHelper.CreateParameter("@AllowDuplicates", condition.AllowDuplicates, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@IncludeAllProducts", condition.IncludeAllProducts, SqlDbType.Bit),
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.Save");
			new DataHandler().ExecuteCommand("[addon].[pCartContainsConditionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ConditionId", id, SqlDbType.Int),
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.Delete");
			new DataHandler().ExecuteCommand("[addon].[pCartContainsConditionDelete]", parameters, dbSettings);
		}
        
		public virtual ProductIdDictionary GetIncludeProducts(int conditionId)
		{
			var productIds = new ProductIdDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetIncludeProductsSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}
			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int conditionId)
		{
			var productIds = new ProductIdDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetExcludeProductsSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}
			return productIds;
		}

		public virtual CategoryDictionary GetIncludeCategories(int conditionId)
		{
			var categories = new CategoryDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetIncludeCategoriesSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}
			return categories;
		}

		public virtual CategoryDictionary GetExcludeCategories(int conditionId)
		{
			var categories = new CategoryDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetExcludeCategoriesSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}
			return categories;
		}

		public virtual CategoryDictionary GetIncludeCategoriesRecursive(int conditionId)
		{
			var categories = new CategoryDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetIncludeCategoriesSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionIncludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}
			return categories;
		}

		public virtual CategoryDictionary GetExcludeCategoriesRecursive(int conditionId)
		{
			var categories = new CategoryDictionary();
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ConditionId", conditionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.GetExcludeCategoriesSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[addon].[pCartContainsConditionExcludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}
			return categories;
		}

		public virtual void DeleteIncludeProducts(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[addon].[pCartContainsConditionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[addon].[pCartContainsConditionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[addon].[pCartContainsConditionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[addon].[pCartContainsConditionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void InsertIncludeProduct(int conditionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartContainsConditionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int conditionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartContainsConditionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertIncludeCategory(int conditionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartContainsConditionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int conditionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartContainsConditionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[addon].[pCartContainsConditionExcludeCategoryInsert]", parameters, dbSettings);
		}
	}
}
