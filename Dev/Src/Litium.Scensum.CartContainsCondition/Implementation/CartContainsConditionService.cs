﻿using System;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartContainsCondition.Repository;
using Litium.Scensum.Core;

namespace Litium.Scensum.CartContainsCondition
{
	public class CartContainsConditionService : IConditionPluginService
	{
		protected CartContainsConditionRepository Repository { get; private set; }

		public CartContainsConditionService(CartContainsConditionRepository cartContainsConditionRepository)
		{
			Repository = cartContainsConditionRepository;
		}

		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int conditionId).")]
		public virtual ICondition GetById(int conditionId)
		{
            if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var condition = Repository.GetById(conditionId);
			if (condition == null) return null;

			condition.IncludeProducts = Repository.GetIncludeProducts(conditionId);
			condition.IncludeCategories = Repository.GetIncludeCategoriesRecursive(conditionId);
			condition.ExcludeProducts = Repository.GetExcludeProducts(conditionId);
			condition.ExcludeCategories = Repository.GetExcludeCategoriesRecursive(conditionId);
			return condition;
		}

		public virtual ICondition GetById(IUserContext userContext, int conditionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var condition = Repository.GetById(conditionId);
			if (condition == null) return null;

			condition.IncludeProducts = Repository.GetIncludeProducts(conditionId);
			condition.IncludeCategories = Repository.GetIncludeCategoriesRecursive(conditionId);
			condition.ExcludeProducts = Repository.GetExcludeProducts(conditionId);
			condition.ExcludeCategories = Repository.GetExcludeCategoriesRecursive(conditionId);
			return condition;
		}
	}
}