﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Scensum.CartContainsCondition
{
	[Serializable]
	public class CartContainsCondition : Condition, ICartContainsCondition
	{
		private int _minQuantity;
		private bool _allowDuplicates;
		private bool _includeAllProducts;
		private ProductIdDictionary _includeProducts;
		private CategoryDictionary _includeCategories;
		private ProductIdDictionary _excludeProducts;
		private CategoryDictionary _excludeCategories;

		public int MinQuantity
		{
			get { return _minQuantity; }
			set 
			{
				CheckChanged(_minQuantity, value);
				_minQuantity = value;
			}
		}

		public bool AllowDuplicates
		{
			get { return _allowDuplicates; }
			set
			{
				CheckChanged(_allowDuplicates, value);
				_allowDuplicates = value;
			}
		}

		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}

		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}

		public CategoryDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}

		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}

		public CategoryDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}

		public override bool Fulfilled(IUserContext context)
		{
			var cart = context.Cart;
			if (cart == null) return false;

			var items = new Collection<ICartItem>();
			foreach (var item in cart.GetCartItems())
			{
				if (!IsRangeMember(item.Product)) continue;
				items.Add(item);
			}
			var quantity = AllowDuplicates
           		? items.Sum(i => i.Quantity)
           		: items.GroupBy(i => i.Product.Id).Count();
			return quantity >= MinQuantity;
		}

		private bool IsRangeMember(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");
			if (IncludeCategories == null) throw new InvalidOperationException("The IncludeCategories property must be set.");
			if (ExcludeProducts == null) throw new InvalidOperationException("The ExcludeProducts property must be set.");
			if (ExcludeCategories == null) throw new InvalidOperationException("The ExcludeCategories property must be set.");

			if (ExcludeCategories.ContainsKey(product.CategoryId))
			{
				return false;
			}
			if (ExcludeProducts.ContainsKey(product.Id))
			{
				return false;
			}
			if (IncludeAllProducts)
			{
				return true;
			}
			if (IncludeCategories.ContainsKey(product.CategoryId))
			{
				return true;
			}
			if (IncludeProducts.ContainsKey(product.Id))
			{
				return true;
			}
			return false;
		}

		public override object[] GetInfoArguments()
		{
			return new object[] { MinQuantity, AllowDuplicates ? string.Empty : " unique" };
		}
	}
}
