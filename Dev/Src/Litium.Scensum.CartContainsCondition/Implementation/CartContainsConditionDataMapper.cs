﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Scensum.CartContainsCondition.Mapper
{
	public class CartContainsConditionDataMapper : ConditionDataMapper<ICartContainsCondition>
	{
		public CartContainsConditionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICartContainsCondition Create()
		{
			ICartContainsCondition condition = base.Create();
			condition.MinQuantity = MapValue<int>("CartContainsCondition.MinQuantity");
			condition.AllowDuplicates = MapValue<bool>("CartContainsCondition.AllowDuplicates");
			condition.IncludeAllProducts = MapValue<bool>("CartContainsCondition.IncludeAllProducts");
			return condition;
		}
	}
}
