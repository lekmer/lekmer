﻿using System;
using System.Collections.Specialized;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Contract.Service;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Payment;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using Channel = Litium.Scensum.Core.Web.Channel;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
    public class BlockDibsResponseControl : BlockControlBase
    {
        private readonly IPaymentProvider _paymentProvider;
        private readonly IContentNodeService _contentNodeService;
        private readonly ICartSession _cartSession;
        private readonly IOrderSession _orderSession;
        private readonly IOrderService _orderService;
        private readonly IOrderStatusService _orderStatusService;
        private readonly IVoucherService _voucherService;

        public BlockDibsResponseControl(
            ITemplateFactory templateFactory, IBlockService blockService,
            IPaymentProvider paymentProvider, IContentNodeService contentNodeService,
            ICartSession cartSession, IOrderSession orderSession,
            IOrderService orderService, IOrderStatusService orderStatusService)
            : base(templateFactory, blockService)
        {
            _paymentProvider = paymentProvider;
            _contentNodeService = contentNodeService;
            _cartSession = cartSession;
            _orderSession = orderSession;
            _orderService = orderService;
            _orderStatusService = orderStatusService;
            _voucherService = IoC.Resolve<IVoucherService>();
        }

        /// <summary>
        /// Common name of the model for the template.
        /// </summary>
        protected override string ModelCommonName
        {
            get { return "DibsResponse"; }
        }

        protected override BlockContent RenderCore()
        {
            return new BlockContent(
                Template.GetFragment("Head").Render(),
                RenderBody());
        }

        private string RenderBody()
        {
            if (GetValueFromCollection("action", Request.QueryString) == "aborted")
            {
                return RenderCancelBody();
            }

            if (_paymentProvider.IsTransactionApproved(Channel.Current.CommonName, Request.QueryString))
            {
                if (_cartSession.Cart != null)
                    return RenderSuccessBody();
            }

            return RenderFailureBody(_cartSession.Cart);
        }

        //RenderCancelBody should check if the order has already been paid or not ,if it is then this means
        //rendering cancel body is unnecessary and the customer should actually be redirected to the confirmation page
        private string RenderCancelBody()
        {
            Fragment fragment;
            string redirectUrl;
            if (!OrderIsAlreadyConfirmed())
            {
                fragment = Template.GetFragment("ContentCancel");
                ResetOrder();
                redirectUrl = GetRedirectUrl("Checkout");
                fragment.AddVariable("Checkout.Src", redirectUrl);
            }
            else
            {
                fragment = Template.GetFragment("ContentSuccess");
                redirectUrl = GetRedirectUrl("CheckoutConfirm");
                fragment.AddVariable("Confirmation.Src", redirectUrl);
            }

            return fragment.Render();
        }



        //RenderFailureBody should check if the order has already been paid or not, if it is thne this means
        //rendering failur body is unnecessary and the customer should actually be redirected to the confirmation page
        private string RenderFailureBody(ICartFull cart)
        {
            Fragment fragment;
            string redirectUrl;
            if (!OrderIsAlreadyConfirmed() && cart != null)
            {
                fragment = Template.GetFragment("ContentFailure");
                ResetOrder();
                fragment.AddVariable("OrderNo", GetValueFromCollection("orderno", Request.QueryString));
                fragment.AddVariable("VerifyId", GetValueFromCollection("verifyid", Request.QueryString));
                fragment.AddVariable("ReplyText", GetValueFromCollection("replyText", Request.QueryString));
                redirectUrl = GetRedirectUrl("Checkout");
                fragment.AddVariable("Checkout.Src", redirectUrl);
            }

            else
            {
                fragment = Template.GetFragment("ContentSuccess");
                redirectUrl = GetRedirectUrl("CheckoutConfirm");
                fragment.AddVariable("Confirmation.Src", redirectUrl);
            }
            return fragment.Render();
        }

        private string RenderSuccessBody()
        {
            Fragment fragment = Template.GetFragment("ContentSuccess");

            UpdateOrderStatus();
            ResetCart();

            string redirectUrl = GetRedirectUrl("CheckoutConfirm");
            fragment.AddVariable("Confirmation.Src", redirectUrl);

            return fragment.Render();
        }

        private void UpdateOrderStatus()
        {
            int orderId = int.Parse(GetValueFromCollection("orderno", Request.QueryString));

            IOrderFull order = _orderService.GetFullById(UserContext.Current, orderId);
            var lekmerUserContext = (ILekmerUserContext)UserContext.Current;
            var campaignInfo = (ILekmerOrderCampaignInfo)order.CampaignInfo;

            if (lekmerUserContext.Voucher != null && campaignInfo != null && campaignInfo.VoucherDiscount != null)
            {
                var customerId = lekmerUserContext.Customer == null ? -1 : lekmerUserContext.Customer.Id;
                _voucherService.VoucherConsume(UserContext.Current.Channel.ApplicationName, customerId, lekmerUserContext.Voucher.VoucherCode, order.Id);
            }

            order.OrderStatus = _orderStatusService.GetByCommonName(
                UserContext.Current, "PaymentConfirmed");

            if (order.Payments.Count > 0)
            {
                string verifyId = GetValueFromCollection("verifyid", Request.QueryString);
                order.Payments.First().ReferenceId =
                    verifyId.IsNullOrEmpty() ? string.Empty : verifyId;
            }

            _orderService.SaveFull(UserContext.Current, order);
            ((ILekmerOrderService)_orderService).UpdateNumberInStock(UserContext.Current, order);
            _orderService.SendConfirm(UserContext.Current, order);

            _orderSession.OrderId = order.Id;

            IoC.Resolve<IVoucherSession>().Voucher = null;
            IoC.Resolve<IUserContextFactory>().FlushCache();
        }

        private bool OrderIsAlreadyConfirmed()
        {
            bool orderIsAlreadyConfirmed = false;
            int orderId = int.Parse(GetValueFromCollection("orderno", Request.QueryString));

            IOrderFull order = _orderService.GetFullById(UserContext.Current, orderId);
            if (order != null)
            {

                var statusConfirmed = _orderStatusService.GetByCommonName(UserContext.Current, "PaymentConfirmed");
                string orderRefrerenceId = string.Empty;
                if (order.Payments.Count > 0)
                {
                    orderRefrerenceId = order.Payments.First().ReferenceId;
                }

                if (order.OrderStatus == statusConfirmed && !string.IsNullOrEmpty(orderRefrerenceId))
                    orderIsAlreadyConfirmed = true;
            }
            return orderIsAlreadyConfirmed;
        }

        private static string GetValueFromCollection(string key, NameValueCollection collection)
        {
            string value = collection[key];
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(
                    string.Format("Parameter '{0}' is missing or invalid.", key), "collection");
            }
            return value;
        }

        private string GetRedirectUrl(string commonName)
        {
            var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

            if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
            {
                return ResolveUrl(contentNodeTreeItem.Url);
            }

            return ResolveUrl("~/");
        }

        private void ResetCart()
        {
            LekmerCartCookie.DeleteShoppingCardGuid();
            _cartSession.Cart = null;
        }

        private void ResetOrder()
        {
            int orderId = int.Parse(GetValueFromCollection("orderno", Request.QueryString));

            IOrderFull order = _orderService.GetFullById(UserContext.Current, orderId);
            var lekmerUserContext = (ILekmerUserContext)UserContext.Current;
            var campaignInfo = (ILekmerOrderCampaignInfo)order.CampaignInfo;

            if (lekmerUserContext.Voucher != null && campaignInfo != null && campaignInfo.VoucherDiscount != null)
            {
                _voucherService.VoucherRelease(lekmerUserContext.Voucher.VoucherCode);
            }

            order.OrderStatus = _orderStatusService.GetByCommonName(
                UserContext.Current, "RejectedByPaymentProvider");
            _orderService.SaveFull(UserContext.Current, order);

            _orderSession.OrderId = null;

            IoC.Resolve<IVoucherSession>().Voucher = null;
            IoC.Resolve<IUserContextFactory>().FlushCache();
        }
    }
}