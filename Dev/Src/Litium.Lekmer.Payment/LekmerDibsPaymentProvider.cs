﻿using System.Globalization;
using System.Text;
using System.Web;
using Litium.Lekmer.Payment.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Payment.Dibs;

namespace Litium.Lekmer.Payment
{
	public class LekmerDibsPaymentProvider : DibsPaymentProvider
	{
		public LekmerDibsPaymentProvider(IChannelService channelService, ICountryService countryService) : base(channelService, countryService)
		{
		}

		protected override decimal GetOrderPrice(IOrderFull order)
		{
			Price price = order.GetActualPriceSummary();
			decimal priceFreight = order.GetActualFreightCost();
			var priceIncludingVat = price.IncludingVat + priceFreight;
			var adjustedPriceIncludingVat = priceIncludingVat*100;
			return adjustedPriceIncludingVat;
		}


		protected override string CreateServiceUrl(
			ICustomerInformation customer,
			IOrderFull order,
			ICurrency currency,
			IPaymentType paymentType)
		{
			var channelService = IoC.Resolve<IChannelService>();
			var channel = channelService.GetById(order.ChannelId);
			var setting = new LekmerDibsSetting(channel.CommonName);

			var url = new ServiceUrl(setting.ServiceUrl);

			url.Add("merchant", setting.MerchantId);

			url.AddEncoded("ResponseUrl", setting.ResponseUrl);
			url.Add("pageSet", setting.PageSet);
			url.Add("method", setting.AquirerName);
			url.Add("currency", currency.Iso);
			url.AddEncoded("billingFirstName", customer.FirstName);
			url.AddEncoded("billingLastName", customer.LastName);

			var countryService = IoC.Resolve<ICountryService>();
			ICountry country = countryService.GetById(customer.DefaultBillingAddress.CountryId);
			url.AddEncoded("billingCountry", country.Iso);
			url.AddEncoded("billingAddress", customer.DefaultBillingAddress.StreetAddress);
			url.AddEncoded("billingCity", customer.DefaultBillingAddress.City);
			url.AddEncoded("eMail", customer.Email);

			url.Add("orderNo", order.Id);
			url.Add("customerNo", customer.Id);

			var orderDataFormatString = setting.DataFormatString;
			var orderData = string.Format(orderDataFormatString, GetOrderPrice(order));
			url.Add("data", orderData);

			if (setting.AuthorizeOnly)
			{
				url.Add("authOnly", "true");
			}

			if (setting.TestCode > 0)
			{
				url.Add("Test", setting.TestCode);
			}



			url.Add("uses3dsecure", setting.Use3DSecure);

			url.Add("MAC", ParseRequestMac(channel.CommonName, orderData, currency.Iso, order.Id.ToString()));

			return url.ToString();
		}

	}


	internal class ServiceUrl
	{
		private readonly StringBuilder _url = new StringBuilder();

		/// <summary>
		/// Initializes an instance of ServiceUrl.
		/// </summary>
		/// <param name="urlBase">An url without the QueryString part and without a question mark.</param>
		public ServiceUrl(string urlBase)
		{
			_url.Append(urlBase);
			_url.Append("?");
		}

		/// <summary>
		/// Adds a QueryString variable to the url.
		/// </summary>
		/// <param name="name">The name of the QueryString variable.</param>
		/// <param name="value">The value of the QueryString variable.</param>
		public void Add(string name, object value)
		{
			_url.AppendFormat(CultureInfo.CurrentCulture, "{0}={1}&", name, value);
		}

		/// <summary>
		/// Adds a QueryString variable with a url encoded value to the url.
		/// </summary>
		/// <param name="name">The name of the QueryString variable.</param>
		/// <param name="value">The value of the QueryString variable.</param>
		public void AddEncoded(string name, string value)
		{
			Add(name, HttpUtility.UrlEncode(value, Encoding.GetEncoding("iso-8859-1")));
		}

		public override string ToString()
		{
			return _url.ToString();
		}
	}
}