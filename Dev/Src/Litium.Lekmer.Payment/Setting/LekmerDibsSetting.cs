﻿using Litium.Framework.Setting;
using Litium.Scensum.Order.Payment.Dibs;

namespace Litium.Lekmer.Payment.Setting
{
	public class LekmerDibsSetting : DibsSetting
	{
		private readonly string _groupName;
		public LekmerDibsSetting(string channelCommonName) : base(channelCommonName)
		{
			_groupName = channelCommonName;
		}

		public string MerchantId
		{
			get { return GetString(_groupName, "MerchantId"); }
		}

	}
}