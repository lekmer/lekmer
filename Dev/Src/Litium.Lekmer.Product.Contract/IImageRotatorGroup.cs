﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    public interface IImageRotatorGroup : IBusinessObjectBase
    {
        int ImageGroupId { get; set; }
        int BlockId { get; set; }
        int? MainImageId { get; set; }
        int? ThumbnailImageId { get; set; }
        int? InternalLinkContentNodeId { get; set; }
        string ExternalLink { get; set; }
        bool IsMovie { get; set; }
        int? InternalLinkContentNodeId2 { get; set; }
        string ExternalLink2 { get; set; }
        bool IsMovie2 { get; set; }
        int? InternalLinkContentNodeId3 { get; set; }
        string ExternalLink3 { get; set; }
        bool IsMovie3 { get; set; }

        string Title { get; set; }
        string HtmalMarkup { get; set; }
        DateTime? StartDateTime { get; set; }
        DateTime? EndDateTime { get; set; }
        int Ordinal { get; set; }
    }
}
