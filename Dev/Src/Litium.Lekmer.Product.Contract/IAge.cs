﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IAge : IBusinessObjectBase
	{
		int Id { get; set; }
		int Months { get; set; }
		string Title { get; set; }
	}
}
