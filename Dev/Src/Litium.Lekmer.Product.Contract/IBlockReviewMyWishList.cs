﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
    public interface IBlockReviewMyWishList : IBlock
    {
        int ColumnCount { get; set; }
        int RowCount { get; set; }
    }
}
