namespace Litium.Lekmer.Product
{
	public interface ITag
	{
		int Id { get; set; }
		int TagGroupId { get; set; }
		string Value { get; set; }
	}
}