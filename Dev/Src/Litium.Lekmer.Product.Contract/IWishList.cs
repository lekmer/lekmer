﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
    public interface IWishList : IBusinessObjectBase
    {
        int Id { get; set; }
        Guid WishListKey { get; set; }
        string ProductWishList { get; set; }
    }
}
