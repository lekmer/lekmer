using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product.Contract
{
	public interface IBrandRecord : IBrand
	{
		Collection<IBrandSiteStructureRegistry> BrandSiteStructureRegistries { get; set; }
	}
}