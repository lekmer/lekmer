using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IBrandSiteStructureRegistry : IBusinessObjectBase
	{
		int BrandId { set; get; }
		int SiteStructureRegistryId { set; get; }
		int ContentNodeId { set; get; }
	}
}