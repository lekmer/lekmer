﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product.Contract
{
    [Serializable]
    public class BrandCollection : Collection<IBrand>
    {
        public BrandCollection()
        {
        }

        public BrandCollection(IList<IBrand> list) : base(list)
        {
        }

        public BrandCollection(IEnumerable<IBrand> list) : base(new List<IBrand>(list))
        {
        }

        public int TotalCount { get; set; }
    }
}