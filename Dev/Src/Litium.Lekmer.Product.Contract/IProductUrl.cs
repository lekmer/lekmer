﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductUrl : IBusinessObjectBase
	{
		int ProductId { get; set; }
		int LanguageId { get; set; }
		string UrlTitle { get; set; }
	}
}
