namespace Litium.Lekmer.Product
{
	public interface ISize
	{
		int Id { get; set; }
		decimal EU { get; set; }
		decimal USMale { get; set; }
		decimal USFemale { get; set; }
		decimal UKMale { get; set; }
		decimal UKFemale { get; set; }
		int Millimeter { get; set; }
	}
}