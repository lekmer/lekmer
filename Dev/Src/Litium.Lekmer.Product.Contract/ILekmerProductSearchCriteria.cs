﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Contract
{
    public interface ILekmerProductSearchCriteria : IProductSearchCriteria
    {
		string BrandId { get; set; }
    }
}