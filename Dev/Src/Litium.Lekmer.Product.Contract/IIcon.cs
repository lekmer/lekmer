﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
    public interface IIcon : IBusinessObjectBase
    {
        int Id { get; set; }
        string Title { get; set; }
        IImage Image { get; set; }
    }
}
