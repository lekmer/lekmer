﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductSize : IBusinessObjectBase
	{
		int ProductId { get; set; }
		int SizeId { get; set; }
		string ErpId { get; set; }
		int NumberInStock { get; set; }
		int? MillimeterDeviation { get; set; }
		decimal? OverrideEu { get; set; }
		decimal Eu { get; set; }
		decimal UsMale { get; set; }
		decimal UsFemale { get; set; }
		decimal UkMale { get; set; }
		decimal UkFemale { get; set; }
		int Millimeter { get; set; }
		bool IsBuyable { get; }
	}
}
