﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Contract
{
    public interface IBlockBrandListBrand : IBusinessObjectBase
    {
        int BlockId { get; set; }
        int BrandId { get; set; }
        int Ordinal { get; set; }
        string Title { get; set; }
    }
}