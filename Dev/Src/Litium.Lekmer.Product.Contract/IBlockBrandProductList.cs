﻿using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Contract
{
    public interface IBlockBrandProductList : IBlock
    {
        int? TemplateId { get; set; }
        int ColumnCount { get; set; }
        int RowCount { get; set; }
        int ProductSortOrderId { get; set; }
        IProductSortOrder ProductSortOrder { get; set; }
    }
}