﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface ISizeSecureService
	{
		Collection<ISize> GetAll();
	}
}
