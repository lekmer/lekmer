﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductUrlSecureService
	{
		Collection<IProductUrl> GetAllByProduct(int productId);
		IProductUrl Create(int productId, int languageId, string urlTitle);
		bool Check(IProductUrl productUrl);
		void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<IProductUrl> productUrls);
	}
}
