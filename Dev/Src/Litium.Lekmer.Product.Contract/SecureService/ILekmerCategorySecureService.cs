﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerCategorySecureService : ICategorySecureService
	{
		Collection<ITranslationGeneric> GetAllTranslationsByCategory(int id);
		void Update(ISystemUserFull systemUserFull, ICategoryRecord category, IEnumerable<ITranslationGeneric> translations);
	}
}
