﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
    public interface ITagGroupSecureService
    {
        Collection<ITagGroup> GetAll();
    }
}
