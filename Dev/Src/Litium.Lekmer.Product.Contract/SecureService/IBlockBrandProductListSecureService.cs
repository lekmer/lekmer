﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Contract
{
    public interface IBlockBrandProductListSecureService
    {
        IBlockBrandProductList Create();

        int Save(
            ISystemUserFull systemUserFull,
            IBlockBrandProductList block,
            Collection<IBlockBrandProductListBrand> blockBrands);

        IBlockBrandProductList GetById(int blockId);
    }
}