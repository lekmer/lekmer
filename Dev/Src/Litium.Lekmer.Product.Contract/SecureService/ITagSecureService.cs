﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface ITagSecureService
    {
        Collection<ITag> GetAllByTagGroup(int tagGroupId);
        Collection<ITag> GetAllByProductAndTagGroup(int productId, int tagGroupId);
        ITag Create(int groupId, string value);
        int Save(ISystemUserFull systemUserFull, ITag tag, IEnumerable<ITranslationGeneric> translations);
        void Delete(ISystemUserFull systemUserFull, int tagId);
        void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<int> tagIds);
        Collection<ITranslationGeneric> GetAllTranslationsByTag(int tagId);
    }
}
