﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface IIconSecureService
    {
        Collection<IIcon> GetAll();
        Collection<IIcon> GetAllByProduct(int productId);
        void Save(ISystemUserFull systemUserFull, int productId, IEnumerable<int> iconsId);
    }
}
