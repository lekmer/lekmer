﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface IBlockReviewMyWishListSecureService
    {
        IBlockReviewMyWishList Create();
        IBlockReviewMyWishList GetById(int id);
        int Save(ISystemUserFull systemUserFull, IBlockReviewMyWishList block);
        void Delete(ISystemUserFull systemUserFull, int blockId);
    }
}
