﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
  public interface IBlockLatestProductAddedToBasketSecureService
  {
      IBlockLatestProductAddedToBasket Create();
      IBlockLatestProductAddedToBasket GetById(int id);
      int Save(ISystemUserFull systemUserFull, IBlockLatestProductAddedToBasket block);
      void Delete(ISystemUserFull systemUserFull, int blockId);
    }
}
