﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface IBlockImageRotatorSecureService
    {
        IBlockImageRotator Create();
        IBlockImageRotator GetById(int id);
        int Save(ISystemUserFull systemUserFull, IBlockImageRotator block, Collection<IImageRotatorGroup> imageRotatorGroups);
        void Delete(ISystemUserFull systemUserFull, int blockId);
    }
}
