﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using System.Collections.Generic;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProductSecureService
	{
		void Save(ISystemUserFull systemUserFull,
			IProductRecord product,
			Collection<IRelationList> relationLists,
			IProductSeoSetting seoSetting,
			Collection<IProductImageGroupFull> imageGroups,
			IEnumerable<IStoreProduct> storeProductItems,
			Collection<ITranslationGeneric> webShopTitleTranslations,
			Collection<ITranslationGeneric> shortDescriptionTranslations,
			Collection<ITranslationGeneric> descriptionTranslations,
			Collection<ITranslationGeneric> seoTitleTranslations,
			Collection<ITranslationGeneric> seoDescriptionTranslations,
			Collection<ITranslationGeneric> seoKeywordsTranslations,
            Collection<ITranslationGeneric> measurementTranslations,
            Collection<int> icons,
            Collection<int> tags,
			IEnumerable<IProductSize> productSizes,
			IEnumerable<IProductUrl> productUrls);

		void UpdatePopularity(Collection<IChannel> channels, DateTime dateTimeFrom, DateTime dateTimeTo);
	}
}
