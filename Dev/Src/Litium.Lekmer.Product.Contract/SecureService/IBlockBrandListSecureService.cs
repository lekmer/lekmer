﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Contract.SecureService
{
    public interface IBlockBrandListSecureService
    {
        IBlockBrandList Create();
        IBlockBrandList GetById(int id);
        int Save(ISystemUserFull systemUserFull, IBlockBrandList block, Collection<IBlockBrandListBrand> blockBrands);
    }
}