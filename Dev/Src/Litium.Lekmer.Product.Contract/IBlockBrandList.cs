﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Contract
{
    public interface IBlockBrandList : IBlock
    {
        int ColumnCount { get; set; }
        int RowCount { get; set; }
        bool IncludeAllBrands { get; set; }
        int? LinkContentNodeId { get; set; }
    }
}