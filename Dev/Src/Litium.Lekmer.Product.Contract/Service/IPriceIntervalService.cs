using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IPriceIntervalService
	{
		Collection<IPriceInterval> GetAll(IUserContext context);
		IPriceInterval GetMatching(IUserContext context, decimal price);
	}
}