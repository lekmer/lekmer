﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductSizeService
	{
		Collection<IProductSize> GetAllByProduct(IUserContext userContext, int productId);
		IProductSize GetById(int productId, int sizeId);
		Collection<ProductSizeRelation> GetAll(IUserContext userContext);
	}
}
