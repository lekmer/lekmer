﻿using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Contract.Service
{
    public interface ILekmerProductService : IProductService
    {
    	ILekmerProduct Create();

        ProductCollection GetAllByBlock(
            IUserContext context,
            IBlockBrandProductList block,
            int pageNumber,
            int pageSize, int productSortOrderId);

    	ProductCollection PopulateViewProducts(IUserContext context, ProductIdCollection productIds);
    	ProductCollection GetViewAllForExport(IUserContext context, int page, int pageSize);
    	ProductCollection GetAll(
    		IUserContext context, int page, int pageSize);

    	int? GetIdByOldUrl(string oldUrl);
        void RefreshProductListingTemporaryData();
    }
}