﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Contract.Service
{
    public interface IBlockBrandListService
    {
        IBlockBrandList GetById(IUserContext context, int id);
    }
}