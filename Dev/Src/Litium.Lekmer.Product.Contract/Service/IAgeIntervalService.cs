using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IAgeIntervalService
	{
		Collection<IAgeInterval> GetAll(IUserContext context);
		IEnumerable<IAgeInterval> GetAllMatching(IUserContext context, int ageFromMonth, int ageToMonth);
	}
}