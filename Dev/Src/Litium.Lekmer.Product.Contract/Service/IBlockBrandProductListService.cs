﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Contract.Service
{
    public interface IBlockBrandProductListService
    {
        IBlockBrandProductList GetById(IUserContext context, int blockId);
    }
}