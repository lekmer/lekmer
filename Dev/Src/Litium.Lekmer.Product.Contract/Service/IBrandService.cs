﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Contract.Service
{
    public interface IBrandService
    {
        IBrand Create();
		BrandCollection GetAll(IUserContext context);
		IBrand GetById(IUserContext context, int brandId);
        BrandCollection GetAllByBlock(IUserContext context, IBlockBrandList block, int page, int pageSize);
        Collection<IBrand> GetAllByBlockBrandProductList(IUserContext context, IBlockBrandProductList block);
    	int? GetIdByTitle(IUserContext context, string title);
    }
}