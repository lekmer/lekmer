using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Contract.Service
{
	public interface IPriceListItemService
	{
		IPriceListItem Create();
	}
}