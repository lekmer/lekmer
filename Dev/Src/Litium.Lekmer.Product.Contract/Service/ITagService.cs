using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ITagService
	{
		Collection<ITag> GetAllByTagGroup(IUserContext context, int tagGroupId);
		Collection<ITag> GetAllByTagGroupAndProduct(IUserContext context, int tagGroupId, int productId);
	}
}