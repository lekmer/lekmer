﻿using System;
using Litium.Lekmer.Product.MessageArgs;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface IWishListService
    {
        IWishList Create();
        IWishList GetByKey(Guid key);
        void Save(IWishList wishList);
        void SendWishList(IUserContext context, WishListMessageArgs wishListMessageArgs);
    }
}
