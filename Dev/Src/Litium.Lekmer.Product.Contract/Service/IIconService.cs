﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface IIconService
    {
        Collection<IIcon> GetAllByProduct(IUserContext context, int productId);
    }
}
