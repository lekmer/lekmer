﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product.Contract.Service
{
    public interface IBlockBrandProductListBrandService
    {
        Collection<IBlockBrandProductListBrand> GetAllByBlock(IUserContext context, int blockId);
    }
}