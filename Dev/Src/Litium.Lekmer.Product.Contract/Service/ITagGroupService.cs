using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ITagGroupService
	{
		Collection<ITagGroup> GetAll(IUserContext context);
		Collection<ITagGroup> GetAllByProduct(IUserContext context, int productId);
	}
}