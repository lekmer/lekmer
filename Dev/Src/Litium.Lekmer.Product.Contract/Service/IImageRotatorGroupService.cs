﻿using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
    public interface IImageRotatorGroupService
    {
        ImageRotatorGroupCollection GetByBlockId(IUserContext context, int blockId);
    }
}
