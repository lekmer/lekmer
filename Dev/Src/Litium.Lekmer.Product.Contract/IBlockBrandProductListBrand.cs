﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Contract
{
    public interface IBlockBrandProductListBrand : IBusinessObjectBase
    {
        int BrandId { get; set; }
        int BlockId { get; set; }
    }
}