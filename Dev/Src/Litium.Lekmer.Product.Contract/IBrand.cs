﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product.Contract
{
    public interface IBrand : IBusinessObjectBase
    {
        int Id { get; set; }

        int? MediaId { get; set; }

        string Title { get; set; }

        string ExternalUrl { get; set; }

        string Description { get; set; }

        IImage Image { get; set; }

        string ErpId { get; set; }

		int? ContentNodeId { get; set; }
    }
}