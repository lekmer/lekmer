﻿namespace Litium.Lekmer.Product
{
	public class ProductSizeRelation
	{
		public int ProductId { get; set; }
		public int SizeId { get; set; }

		public ProductSizeRelation (int productId, int sizeId)
		{
			ProductId = productId;
			SizeId = sizeId;
		}
	}
}
