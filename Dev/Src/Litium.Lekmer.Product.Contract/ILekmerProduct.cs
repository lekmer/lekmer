﻿using System;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Contract
{
    public interface ILekmerProduct : IProduct
    {
        IBrand Brand { get; set; }
        int? BrandId { get; set; }
        DateTime? IsNewFrom { get; set; }
        DateTime? IsNewTo { get; set; }
		DateTime? CreatedDate { get; set;}
        bool IsBookable { get; set; }
		bool IsBuyable { get; }
		bool IsMonitorable { get; }
        bool IsNewProduct { get; }
		bool HasSizes { get; set; }
		string ParentPageUrl { get; set; }
		string LekmerUrl { get; }
		string UrlTitle { get; set; }
		int? ParentContentNodeId { get; set; }
		string LekmerErpId { get; set; }
    }
}
