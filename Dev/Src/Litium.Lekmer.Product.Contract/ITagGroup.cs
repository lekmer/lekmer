using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface ITagGroup
	{
		int Id { get; set; }

		string Title { get; set; }

		string CommonName { get; set; }

		Collection<ITag> Tags { get; set; }
	}
}