﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Contract
{
    public interface ILekmerProductView : ILekmerProduct, IProductView
    {
		int AgeFromMonth { get; set; }
		int AgeToMonth { get; set; }
        string Measurement { get; set; }
        int? NumberOfBatteries { get; set; }
        bool IsBatteryIncluded { get; set; }
        Collection<IIcon> Icons { get; set; }
        DateTime? ExpectedBackInStock { get; set; }
		IBatteryType BatteryType { get; set; }
		ISizeDeviation SizeDeviation { get; set; }
		Collection<IProductSize> ProductSizes { get; set; }
		Collection<ITagGroup> TagGroups { get; set; }
        IProductSize DefaultProductSize{get;set;}
    }
}