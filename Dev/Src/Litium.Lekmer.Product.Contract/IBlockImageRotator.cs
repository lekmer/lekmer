﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
    public interface IBlockImageRotator : IBlock
    {
        int? SecondaryTemplateId  { get; set; }
    }
}