﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
   public interface IVoucherCondition : ICondition
    {
       Collection<int> BatchIds { get; set; }
    }
}
