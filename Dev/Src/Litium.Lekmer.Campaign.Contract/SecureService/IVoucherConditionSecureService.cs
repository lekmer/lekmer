﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
    public interface IVoucherConditionSecureService
    {
        IVoucherCondition GetById(int id);
        Collection<int> GetBatchesByConditionId(int id);
        void Save(IVoucherCondition condition);
        void BatchInsert(IVoucherCondition condition);
        void Delete(int id);
    }
}
