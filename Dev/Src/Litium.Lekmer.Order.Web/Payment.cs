﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class Payment : ControlBase
	{
		public class PaymentTypeSelectedEventArgs : EventArgs
		{
			public int PaymentTypeId { get; private set; }

			public PaymentTypeSelectedEventArgs(int paymentTypeId)
			{
				PaymentTypeId = paymentTypeId;
			}
		}

		public class PartPaymentSelectedEventArgs : EventArgs
		{
			public int PartPaymentId { get; private set; }

			public PartPaymentSelectedEventArgs(int partPaymentId)
			{
				PartPaymentId = partPaymentId;
			}
		}


		public event EventHandler<PaymentTypeSelectedEventArgs> PaymentTypeSelected;


		public event EventHandler<PartPaymentSelectedEventArgs> PartPaymentSelected;

		private IPaymentTypeService _paymentTypeService;
		private Collection<IPaymentType> _paymentTypes;
		private ITemplateFactory _templateFactory;
		private Template _template;


		private const string CreditCardVisaId = "1";
		private const string KlarnaId = "1000002";
		private const string KlarnaPartPaymentId = "1000003";

		private string _paymentType;
		private string _paymentTypeFromRequest;
		private string _partPayment;


		protected string ModelCommonName
		{
			get { return "PaymentType"; }
		}

		public void Initialize()
		{
			_paymentTypeService = IoC.Resolve<IPaymentTypeService>();
			_templateFactory = IoC.Resolve<ITemplateFactory>();
			_template = _templateFactory.Create(ModelCommonName);
			_paymentTypes = _paymentTypeService.GetAll(UserContext.Current);
		}

		public string Render()
		{
			Initialize();
			Fragment fragment = _template.GetFragment("PaymentTypeList");
			if (_paymentTypes.Count > 0)
			{
				var paymentTypesBuilder = new StringBuilder();
				bool klarnaPartPaymentExist = false;
				int i = 0;
				foreach (var paymentType in _paymentTypes)
				{
					Fragment fragmentPaymentType = _template.GetFragment("PaymentType");
					paymentTypesBuilder.Append(MapPaymentType(fragmentPaymentType, paymentType, i++, _paymentTypes.Count));
					if (paymentType.Id.ToString() == KlarnaPartPaymentId)
					{
						klarnaPartPaymentExist = true;
					}
				}
				fragment.AddVariable("Iterate:PaymentType", paymentTypesBuilder.ToString, VariableEncoding.None);
				fragment.AddCondition("isKlarnaInvoice", IsKlarnaInvoice);
				fragment.AddCondition("isKlarnaPartPayment", IsKlarnaPartPayment);

				if (IsKlarnaPartPayment)
				{
					fragment.AddVariable("Iterate:PartPayment",
										 GetKlarnaAlternativ(UserContext.Current.Channel, KlarnaSum, fragment), VariableEncoding.None);
				}

				if (klarnaPartPaymentExist)
				{
					fragment.AddVariable("KlarnaSpecialPartPayment",
										 GetKlarnaSpecialAlternativ(UserContext.Current.Channel, KlarnaSum, fragment),
										 VariableEncoding.None);
					fragment.AddCondition("isSpecialKlarnaPartPaymentExist", true);
				}
				else
				{
					fragment.AddCondition("isSpecialKlarnaPartPaymentExist", false);
				}
				fragment.AddVariable("KlarnaInvoiceCharge", GetLowestMonthlyFee(UserContext.Current.Channel).ToString,
									 VariableEncoding.None);

				//fragment.AddVariable("KlarnaInvoiceCharge", KlarnaSum.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);

				var klarnaSetting = new KlarnaSetting(UserContext.Current.Channel.CommonName);
				fragment.AddVariable("EID", klarnaSetting.EID.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);
			}
			return fragment.Render();
		}

		private string MapPaymentType(Fragment fragmentPaymentType, IPaymentType paymentType, int index, int count)
		{
			bool selected = false;
			if (!string.IsNullOrEmpty(PaymentTypeFromRequest))
			{
				selected = PaymentTypeFromRequest == paymentType.Id.ToString();
			}
			else if (paymentType.CommonName == "Invoice")
			{
				selected = true;
			}

			if (selected && PaymentTypeSelected != null)
			{
				var paymentTypeSelectedEventArgs = new PaymentTypeSelectedEventArgs(paymentType.Id);
				PaymentTypeSelected(this, paymentTypeSelectedEventArgs);
			}
			fragmentPaymentType.AddCondition("IsFirst", index == 0);
			fragmentPaymentType.AddCondition("IsLast", index == count - 1);
			fragmentPaymentType.AddCondition("IsSelected", selected);
			fragmentPaymentType.AddVariable("PaymentType.Title", AliasHelper.GetAliasValue("Order.Checkout.Payment.PaymentType_" + paymentType.CommonName), VariableEncoding.None);
			fragmentPaymentType.AddVariable("PaymentType.Id", paymentType.Id.ToString);


			return fragmentPaymentType.Render();
		}


		public string PaymentTypeFromRequest
		{
			get
			{
				if (string.IsNullOrEmpty(_paymentTypeFromRequest))
				{
					_paymentTypeFromRequest = Request.Form["paymentType"];
				}
				return _paymentTypeFromRequest;
			}
			set { _paymentTypeFromRequest = value; }
		}

		public string PaymentType
		{
			get
			{
				if (string.IsNullOrEmpty(_paymentType))
				{
					//Replace KlarnaSpecialPartPaymentId on main type KlarnaPartPaymentId
					if (KlarnaPartPaymentId != Request.Form["paymentType"] && CreditCardVisaId != Request.Form["paymentType"]
						&& KlarnaId != Request.Form["paymentType"])
					{
						_paymentType = KlarnaPartPaymentId;
					}
					else
					{
						_paymentType = Request.Form["paymentType"];
					}
				}
				return _paymentType;
			}
			set { _paymentType = value; }
		}

		public string PartPayment
		{
			get
			{
				if (string.IsNullOrEmpty(_partPayment))
				{
					_partPayment = Request.Form["partPayment"];
				}
				return _partPayment;
			}
			set { _partPayment = value; }
		}

		private string _specialPartPayment;
		public string SpecialPartPayment
		{
			get
			{
				if (string.IsNullOrEmpty(_specialPartPayment))
				{
					_specialPartPayment = Request.Form["specialPartPayment"];
				}
				return _specialPartPayment;
			}
			set { _specialPartPayment = value; }
		}

		public int TotalOrderCost { get; set; }

		public bool IsCreditCard
		{
			get { return IsActivePayment(CreditCardVisaId); }
		}

		public bool IsKlarnaInvoice
		{
			get { return IsActivePayment(KlarnaId); }
		}

		public bool IsKlarnaPartPayment
		{
			get { return IsActivePayment(KlarnaPartPaymentId); }
		}
		public bool IsKlarnaSpecialPartPayment
		{
			get { return IsActivePayment(SpecialPartPayment); }
		}

		public int KlarnaSum { private get; set; }

		public virtual Collection<IOrderPayment> GetOrderPayments()
		{
			var orderPayment = IoC.Resolve<IOrderPayment>();
			orderPayment.PaymentTypeId = int.Parse(PaymentType);
			orderPayment.Price = 1; //TODO: Fix this, there are a constraint in the db saying it cant be 0
			orderPayment.Vat = 1; //TODO: Fix this, there are a constraint in the db saying it cant be 0
			return new Collection<IOrderPayment>
			       	{
			       		orderPayment
			       	};
		}


		public virtual Collection<IOrderPayment> GetOrderPayments(IOrderFull order)
		{
			var orderPayment = IoC.Resolve<IOrderPayment>();
			orderPayment.PaymentTypeId = int.Parse(PaymentType);
			orderPayment.Price = order.GetActualPriceSummary().IncludingVat;
			orderPayment.Vat = order.GetActualVatSummary(); ;
			return new Collection<IOrderPayment>
			       	{
			       		orderPayment
			       	};
		}

		private bool IsActivePayment(string paymentTypeId)
		{
			if (_paymentTypes != null && _paymentTypes.Count > 0)
			{
				if (!string.IsNullOrEmpty(PaymentTypeFromRequest))
				{
					return PaymentTypeFromRequest == paymentTypeId;
				}
			}
			return false;
		}

		public decimal GetPaymentCost()
		{
			return GetOrderPayment() != null ? GetOrderPayment().Cost : 0m;
		}

		public virtual ILekmerPaymentType GetOrderPayment()
		{
			if (PaymentType.IsNullOrEmpty()) return null;
			var orderPayment =
				(ILekmerPaymentType)
				_paymentTypeService.GetAll(UserContext.Current).SingleOrDefault(t => t.Id == int.Parse(PaymentType));
			return orderPayment;
		}

		/// <summary>
		/// Gets the lowest MonthlyFee for the partPartPayment alternatives.
		/// </summary>
		/// <param name="channel"></param>
		/// <returns>int</returns>
		public int GetLowestMonthlyFee(IChannel channel)
		{
			var klarnaSetting = new KlarnaSetting(channel.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');
			var partPaymentBuilder = new StringBuilder();
			int min = int.MaxValue;
			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(channel.CommonName, temp);
				var minimumMonthlyFee = pclassSetting.MonthlyFee / 100;

				if (minimumMonthlyFee < min) min = minimumMonthlyFee;
			}
			return min == int.MaxValue ? 0 : min;
		}

		/// <summary>
		/// Gets the lowest monthly part Payment cost (To be used with "pay from xx amount)
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="totalOrderCost"></param>
		/// <returns></returns>
		//public int GetLowestPartPayment(IChannel channel, int totalOrderCost)
		//{
		//    var klarnaSetting = new KlarnaSetting(channel.CommonName);
		//    var months = klarnaSetting.PartPaymentMonths.Split(',');
		//    var partPaymentBuilder = new StringBuilder();
		//    int min = int.MaxValue;
		//    foreach (var i in months)
		//    {
		//        int temp;
		//        int.TryParse(i, out temp);
		//        var pclassSetting = new KlarnaPClassSetting(channel.CommonName, temp);
		//        var minimumTotalCost = pclassSetting.MinimumTotalCost;
		//        var pclassId = pclassSetting.PClassId;

		//        if (totalOrderCost >= minimumTotalCost && pclassId > 0)
		//        {
		//            var monthlyCost = KlarnaClient.PopulatePartPayment(channel, pclassId, totalOrderCost, true) / 100;
		//            if (monthlyCost < min) min = monthlyCost;
		//        }
		//    }
		//    return min == int.MaxValue ? 0 : min;
		//}

		/// <summary>
		/// List the part payment alternatives. 
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="totalOrderCost"></param>
		/// <param name="fragment"></param>
		/// <returns></returns>
		public string GetKlarnaAlternativ(IChannel channel, int totalOrderCost, Fragment fragment)
		{
			var klarnaSetting = new KlarnaSetting(channel.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			var partPaymentBuilder = new StringBuilder();
			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(channel.CommonName, temp);
				var minimumTotalCost = pclassSetting.MinimumTotalCost;
				var pclassId = pclassSetting.PClassId;
				if (totalOrderCost >= minimumTotalCost)
				{
					var monthlyCost = KlarnaClient.PopulatePartPayment(channel, pclassId, totalOrderCost, true) / 100;
					//if (monthlyCost >= 50)
					//{
					string description = "";
					if (pclassSetting.Type == 1)
					{
						description = AliasHelper.GetAliasValue("Order.Checkout.Payment.KlarnaPartPaymentAccount").Replace("x",
																														   monthlyCost.
																															ToString());
					}
					else if (pclassSetting.Type == 2)
					{
						continue;
                        //description = AliasHelper.GetAliasValue("Order.Checkout.Payment.KlarnaCampaignPayment");
                        //description = description.Replace("y", pclassSetting.NoOfMonths.ToString());
					}
					else
					{
						description = AliasHelper.GetAliasValue("Order.Checkout.Payment.KlarnaPartPayment").Replace("x",
																													monthlyCost.
																														ToString());
						description = description.Replace("y", pclassSetting.NoOfMonths.ToString());
					}


					Fragment fragmentPaymentType = _template.GetFragment("PartPayment");
					partPaymentBuilder.Append(MapPartPayment(fragmentPaymentType, description, pclassId));
					//}
				}
			}
			return partPaymentBuilder.ToString();
		}

		/// <summary>
		/// List the part payment alternatives. 
		/// </summary>
		/// <param name="channel"></param>
		/// <param name="totalOrderCost"></param>
		/// <param name="fragment"></param>
		/// <returns></returns>
		public string GetKlarnaSpecialAlternativ(IChannel channel, int totalOrderCost, Fragment fragment)
		{
			var klarnaSetting = new KlarnaSetting(channel.CommonName);
			var months = klarnaSetting.PartPaymentMonths.Split(',');

			var partPaymentBuilder = new StringBuilder();
			foreach (var i in months)
			{
				int temp;
				int.TryParse(i, out temp);
				var pclassSetting = new KlarnaPClassSetting(channel.CommonName, temp);
				var minimumTotalCost = pclassSetting.MinimumTotalCost;
				var pclassId = pclassSetting.PClassId;
				if (totalOrderCost >= minimumTotalCost)
				{
					string description = "";
					if (pclassSetting.Type == 2)
					{
						description = AliasHelper.GetAliasValue("Order.Checkout.Payment.KlarnaCampaignPayment");
						//description = description.Replace("y", pclassSetting.NoOfMonths.ToString());
						fragment.AddVariable("SpecialPartPayment.Id", pclassId.ToString);
						Fragment fragmentPaymentType = _template.GetFragment("PaymentType");
						partPaymentBuilder.Append(MapSpecialPartPayment(fragmentPaymentType, description, pclassId));
						break;
					}
				}
			}
			return partPaymentBuilder.ToString();
		}

		private string MapSpecialPartPayment(Fragment fragmentPaymentType, string description, int pClassId)
		{
			bool selected = false;
			if (!string.IsNullOrEmpty(PaymentTypeFromRequest))
			{
				selected = PaymentTypeFromRequest == pClassId.ToString();
			}

			if (selected && PartPaymentSelected != null)
			{
				var partPaymentSelectedEventArgs = new PartPaymentSelectedEventArgs(pClassId);
				PartPaymentSelected(this, partPaymentSelectedEventArgs);
			}

			fragmentPaymentType.AddCondition("IsFirst", false);
			fragmentPaymentType.AddCondition("IsLast", false);
			fragmentPaymentType.AddCondition("IsSelected", selected);
			fragmentPaymentType.AddVariable("PaymentType.Title", description, VariableEncoding.None);
			fragmentPaymentType.AddVariable("PaymentType.Id", pClassId.ToString);

			return fragmentPaymentType.Render();
		}

		private string MapPartPayment(Fragment fragmentPartPayment, string description, int pClassId)
		{
			bool selected = false;
			if (!string.IsNullOrEmpty(PartPayment))
			{
				selected = PartPayment == pClassId.ToString();
			}

			if (selected && PartPaymentSelected != null)
			{
				var partPaymentSelectedEventArgs = new PartPaymentSelectedEventArgs(pClassId);
				PartPaymentSelected(this, partPaymentSelectedEventArgs);
			}

			fragmentPartPayment.AddCondition("IsSelected", selected);
			fragmentPartPayment.AddVariable("PartPayment.Title", description, VariableEncoding.None);
			fragmentPartPayment.AddVariable("PartPayment.Id", pClassId.ToString);


			return fragmentPartPayment.Render();
		}
	}
}