﻿using System.Collections.ObjectModel;
using System.Text;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class Delivery : ControlBase
	{
		private IDeliveryMethodService _deliveryMethodService;

		private Collection<IDeliveryMethod> _deliveryMethods;
		private ITemplateFactory _templateFactory;
		private Template _template;
		private string _paymentType;
		private string _deliveryMethod;
		private IFormatter _formatter;
		private const string CreditCardVisaId = "1";
		private const string KlarnaId = "1000002";
		private const string KlarnaPartPaymentId = "1000003";

		public string PaymentTypeId
		{
			get
			{
				if (string.IsNullOrEmpty(_paymentType))
				{
					//Replace KlarnaSpecialPartPaymentId on main type KlarnaPartPaymentId
					if (KlarnaPartPaymentId != Request.Form["paymentType"] && CreditCardVisaId != Request.Form["paymentType"]
						&& KlarnaId != Request.Form["paymentType"])
					{
						_paymentType = KlarnaPartPaymentId;
					}
					else
					{
						_paymentType = Request.Form["paymentType"];
					}
				}
				return _paymentType;
			}
			set { _paymentType = value; }
		}

		public string DeliveryMethod
		{
			get
			{
				if (string.IsNullOrEmpty(_deliveryMethod))
				{
					_deliveryMethod = Request.Form["deliveryMethod"];
				}
				return _deliveryMethod;
			}
			set { _deliveryMethod = value; }
		}

		public decimal GetActualDeliveryPrice(ICartFull cart)
		{
			var orderDelivery = GetOrderDelivery();
			if (orderDelivery != null)
			{
				if (cart.CampaignInfo != null && cart.CampaignInfo.CampaignFreightCost.HasValue)
				{
					return cart.CampaignInfo.CampaignFreightCost.Value;
				}
				return orderDelivery.FreightCost;
			}
			return 0m;
		}

		public virtual IDeliveryMethod GetOrderDelivery()
		{
			if (DeliveryMethod.IsNullOrEmpty()) return null;
			var orderDelivery = _deliveryMethodService.GetById(UserContext.Current, int.Parse(DeliveryMethod));
			return orderDelivery;
		}

		protected string ModelCommonName
		{
			get { return "DeliveryType"; }
		}

		public void Initialize()
		{
			_deliveryMethodService = IoC.Resolve<IDeliveryMethodService>();
			_templateFactory = IoC.Resolve<ITemplateFactory>();
			_template = _templateFactory.Create(ModelCommonName);
			_formatter = IoC.Resolve<IFormatter>();
		}

		public string Render()
		{
			Initialize();

			if (PaymentTypeId != null && PaymentTypeId != "0")
			{
				Fragment fragment = _template.GetFragment("DeliveryMethodList");

				RenderDeliveryMethods(fragment);

				return fragment.Render();
			}

			return "";
		}


		private void RenderDeliveryMethods(Fragment fragment)
		{
			var productsBuilder = new StringBuilder();

			_deliveryMethods = _deliveryMethodService.GetAllByPaymentType(UserContext.Current, int.Parse(PaymentTypeId));
			var i = 1;
			foreach (var method in _deliveryMethods)
			{
				productsBuilder.Append(MapDeliveryMethod(method, i == 1));
				i++;
			}
			fragment.AddVariable("Iterate:DeliveryMethod", productsBuilder.ToString, VariableEncoding.None);

			if (DeliveryMethod != null)
				fragment.AddVariable("Delivery.Price", _formatter.FormatPrice(Channel.Current, GetOrderDelivery().FreightCost));
		}


		private string MapDeliveryMethod(IDeliveryMethod deliveryMethod, bool first)
		{
			Fragment fragment = _template.GetFragment("DeliveryMethod");


			if (string.IsNullOrEmpty(DeliveryMethod)) DeliveryMethod = deliveryMethod.Id.ToString();

			bool selected = string.IsNullOrEmpty(DeliveryMethod) ? first : DeliveryMethod == deliveryMethod.Id.ToString();


			fragment.AddCondition("IsSelected", selected);
			fragment.AddVariable("DeliveryMethod.Title", deliveryMethod.Title);
			fragment.AddVariable("DeliveryMethod.Id", deliveryMethod.Id.ToString);

			fragment.AddVariable("DeliveryMethod.FreightCost",
			                     _formatter.FormatPrice(Channel.Current, deliveryMethod.FreightCost));
			return fragment.Render();
		}
	}
}