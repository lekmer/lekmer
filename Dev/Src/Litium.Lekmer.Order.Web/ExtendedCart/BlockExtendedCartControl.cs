using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Contract.Service;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
    public class BlockExtendedCartControl : BlockControlBase
    {
        private readonly ICartService _cartService;
        private readonly IFormatter _formatter;
        private readonly IOrderService _orderService;

        private readonly Dictionary<CartItemKey, bool> _updatedItems = new Dictionary<CartItemKey, bool>();
        private Delivery _delivery;
        private ExtendedCartForm _extendedCartForm;
        private int _maxQuantity;
        private Payment _payment;
        private VoucherDiscountForm _voucherDiscountForm;

        public BlockExtendedCartControl(ITemplateFactory templateFactory, IBlockService blockService,
                                        ICartService cartService, IOrderService orderService, IFormatter formatter)
            : base(templateFactory, blockService)
        {
            _cartService = cartService;
            _formatter = formatter;
            _orderService = orderService;
        }

        public VoucherDiscountForm VoucherDiscountForm
        {
            get { return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm()); }
        }

        public ExtendedCartForm ExtendedCartForm
        {
            get
            {
                if (_extendedCartForm == null) _extendedCartForm = CreateExtendedCartForm();
                return _extendedCartForm;
            }
        }

        protected virtual ExtendedCartForm CreateExtendedCartForm()
        {
            return new ExtendedCartForm(GetCheckoutPostUrl());
        }

        protected virtual string GetCheckoutPostUrl()
        {
            return ResolveUrl(ContentNodeTreeItem.Url);
        }

        protected virtual ICartFull GetCart()
        {
            RefreshCart();
            return IoC.Resolve<ICartSession>().Cart;
        }

        protected virtual void RefreshCart()
        {
            CartHelper.RefreshSessionCart();
        }

        /// <summary>
        /// Render the component's body and head.
        /// </summary>
        protected override BlockContent RenderCore()
        {
            Initialize();

            string body = RenderCart() ?? RenderEmptyCart();

            return new BlockContent(
                Template.GetFragment("Head").Render(), body);
        }

        private void Initialize()
        {
            _payment = new Payment();
            _payment.Initialize();
            _payment.PaymentTypeSelected += OnPaymentTypeSelected;

            _delivery = new Delivery();
            _delivery.Initialize();

            if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
            {
                _maxQuantity = 10;
            }
        }

        private void OnPaymentTypeSelected(object sender, Payment.PaymentTypeSelectedEventArgs e)
        {
            _delivery.PaymentTypeId = e.PaymentTypeId.ToString();
        }

        /// <summary>
        /// Renders the cart when there's no items added to it.
        /// </summary>
        /// <returns>Rendered empty cart.</returns>
        private string RenderEmptyCart()
        {
            return Template.GetFragment("Empty").Render();
        }

        /// <summary>
        /// Renders a cart with items.
        /// </summary>
        /// <returns>Rendered cart. Null if no items is added to the cart.</returns>
        private string RenderCart()
        {
            ICartFull cart = GetCart();
            if (cart == null)
            {
                return null;
            }
            Collection<ICartItem> cartItems = cart.GetGroupedCartItemsByProductAndPrice();
            if (cartItems.Count == 0)
            {
                return null;
            }

            if (IsPostBack && !IsVoucherPost() && IsChangeQuantityPost())
            {
                ChangeQuantityOfItems(cart);
                cart = GetCart();
                if (cart == null)
                {
                    return null;
                }
                cartItems = cart.GetGroupedCartItemsByProductAndPrice();
                ;
                if (cartItems.Count == 0)
                {
                    return null;
                }
            }

            Fragment fragmentContent = Template.GetFragment("Content");
            fragmentContent.AddVariable(
                "Iterate:CartItem",
                RenderCartItems(cartItems),
                VariableEncoding.None);
            Price cartPrice = cart.GetActualPriceSummary();
            fragmentContent.AddVariable(
                "Cart.PriceIncludingVatSummary",
                _formatter.FormatPrice(Channel.Current, cartPrice.IncludingVat));
            fragmentContent.AddVariable(
                "Cart.PriceExcludingVatSummary",
                _formatter.FormatPrice(Channel.Current, cartPrice.ExcludingVat));
            fragmentContent.AddVariable(
                "Cart.QuantitySummary",
                _formatter.FormatNumber(Channel.Current, cart.GetQuantitySummary()));

            string voucherWasUsedMessage = string.Empty;
            if (IoC.Resolve<IVoucherSession>().WasUsed)
            {
                voucherWasUsedMessage = AliasHelper.GetAliasValue("Order.Checkout.VoucherWasUsed");
                    // +" " + IoC.Resolve<IVoucherSession>().VoucherCode + ".";
            }
            fragmentContent.AddVariable("ErrorVoucherWasUsed", voucherWasUsedMessage, VariableEncoding.None);
            ExtendedCartForm.MapFieldNamesToFragment(fragmentContent);
            ExtendedCartForm.MapFieldValuesToFragment(fragmentContent);

            _payment.Render();
            _delivery.Render();

            fragmentContent.AddVariable("DeliveryMethod.FreightCost",
                                        _formatter.FormatPrice(Channel.Current, _delivery.GetActualDeliveryPrice(cart)));
            decimal totalPrice = cart.GetActualPriceSummary().IncludingVat + _delivery.GetActualDeliveryPrice(cart) +
                                 _payment.GetPaymentCost();
            int klarnaSum = (int) Math.Round(totalPrice)*100;

            fragmentContent.AddVariable("Checkout.TotalPriceWithFreight",
                                        FormatPriceForCheckout(Channel.Current, totalPrice));

            //fragmentContent.AddVariable("Checkout.TotalPriceWithFreight", _formatter.FormatPrice(Channel.Current, totalPrice));

            fragmentContent.AddCondition("isKlarnaPartPayment", _payment.IsKlarnaPartPayment);
            int monthlyPayment = PaymentHelper.GetLowestPartPayment(Channel.Current, klarnaSum);
            fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts",
                                        monthlyPayment.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);

            return fragmentContent.Render();
        }

        private bool IsVoucherPost()
        {
            string useVoucherValue = Request.Form[VoucherDiscountForm.UseVoucherFormName];
            return useVoucherValue == VoucherDiscountForm.UseVoucherFormValue ||
                   useVoucherValue == VoucherDiscountForm.UseVoucherFormText;
        }

        private bool IsChangeQuantityPost()
        {
            return PostMode.Equals("changequantity");
        }

        private void ChangeQuantityOfItems(ICartFull cart)
        {
            foreach (ICartItem cartItem in cart.GetGroupedCartItemsByProductAndPrice())
            {
                string key = "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat;

                int? newQuantity = Request.Form.GetInt32OrNull(key);
                if (!newQuantity.HasValue) continue;

                if (newQuantity.Value <= 0)
                {
                    //Delete item by productId and price
                    cart.DeleteItem(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat,
                                    cartItem.Quantity);
                }
                else if (newQuantity.Value != cartItem.Quantity)
                {
                    //Get actual quantity which need add or remove
                    int quantity = newQuantity.Value - cartItem.Quantity;
                    ((ILekmerCartFull) cart).ChangeItemQuantityForBlockExtendedCartControl(cartItem.Product, quantity,
                                                                                           ((ILekmerCartItem) cartItem).
                                                                                               SizeId,
                                                                                           ((ILekmerCartItem) cartItem).
                                                                                               ErpId);

                    var cartItemKey = new CartItemKey(cartItem.Product.Id,
                                                      cartItem.Product.CampaignInfo.Price.IncludingVat);
                    _updatedItems[cartItemKey] = true;
                }
            }
            _cartService.Save(UserContext.Current, cart);
            CartHelper.RefreshSessionCart();
        }

        protected virtual IProduct GetProduct(int productId)
        {
            return IoC.Resolve<IProductService>().GetById(UserContext.Current, productId);
        }

        /// <summary>
        /// Renders the items in a cart.
        /// </summary>
        /// <param name="cartItems">The items to render.</param>
        /// <returns>Rendered items.</returns>
        private string RenderCartItems(IEnumerable<ICartItem> cartItems)
        {
            var itemBuilder = new StringBuilder();

            // grouping by pair: productId, sizeId. sizeId is nullable.
            IEnumerable<IGrouping<KeyValuePair<int, int?>, ICartItem>> groupedCartItems =
                cartItems.GroupBy(item => new KeyValuePair<int, int?>(item.Product.Id, ((ILekmerCartItem) item).SizeId));

            foreach (ICartItem cartItem in cartItems)
            {
                itemBuilder.Append(RenderCartItem(cartItem, groupedCartItems));
            }

            return itemBuilder.ToString();
        }

        /// <summary>
        /// Renders a cart item.
        /// </summary>
        /// <param name="cartItem">The cart item to render.</param>
        /// <returns>Rendered item.</returns>
        private string RenderCartItem(ICartItem cartItem,
                                      IEnumerable<IGrouping<KeyValuePair<int, int?>, ICartItem>> groupedCartItems)
        {
            var cartItemKey = new CartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat);
            bool itemWasUpdated = _updatedItems.ContainsKey(cartItemKey);

            Fragment fragment = Template.GetFragment("CartItem");
            fragment.AddEntity(cartItem.Product);

            fragment.AddVariable("Product.CampaignInfo.Price_Formatted", FormatPriceForCheckout(Channel.Current,cartItem.Product.CampaignInfo.Price.IncludingVat));
            fragment.AddVariable("Product.Price_Formatted", FormatPriceForCheckout(Channel.Current, cartItem.Product.Price.PriceIncludingVat));


            fragment.AddVariable("Product.CampaignInfoPriceHidden",
                                 cartItem.Product.CampaignInfo.Price.IncludingVat.ToString);
            fragment.AddVariable(
                "CartItem.Quantity",
                _formatter.FormatNumber(Channel.Current, cartItem.Quantity));
          
             fragment.AddVariable(
                "CartItem.PriceIncludingVatSummary",
                FormatPriceForCheckout(Channel.Current, cartItem.GetActualPriceSummary().IncludingVat));
            fragment.AddVariable(
                "CartItem.PriceExcludingVatSummary",
                FormatPriceForCheckout(Channel.Current, cartItem.GetActualPriceSummary().ExcludingVat));

            fragment.AddCondition("Product.IsInStock",
                                  ((ILekmerOrderService) _orderService).CheckNumberInStock(UserContext.Current, cartItem,
                                                                                           groupedCartItems));

            var lekmerCartItem = (ILekmerCartItem) cartItem;
            int quantityLimit = ObtainQuantityLimit(lekmerCartItem);
            fragment.AddVariable("Product.NumberInStockForSize", quantityLimit.ToString(), VariableEncoding.None);
            fragment.AddVariable(
                "Iterate:QuantityOption",
                RenderQuantityOptions(cartItem.Quantity, quantityLimit),
                VariableEncoding.None);
            fragment.AddCondition("ItemWasUpdated", itemWasUpdated);
            fragment.AddCondition("IsNumberInStockTooSmall", cartItem.Quantity > quantityLimit);

            fragment.AddCondition("CartItem.HasSize", lekmerCartItem.SizeId.HasValue);
            fragment.AddEntity(GetSize(lekmerCartItem));

            fragment.AddRegexVariable(
                @"\[TagGroup\(\""(.+)\""\)\]",
                delegate(Match match)
                    {
                        string tagGroupCommonName = match.Groups[1].Value;
                        return RenderTagGroup(tagGroupCommonName, lekmerCartItem);
                    },
                VariableEncoding.None);

            return fragment.Render();
        }

        private string RenderQuantityOptions(int quantity, int quantitiesToDisplay)
        {
            var optionBuilder = new StringBuilder();
            for (int i = 0; i <= Math.Max(quantitiesToDisplay, quantity); i++)
            {
                Fragment fragmentOption = Template.GetFragment("QuantityOption");
                fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
                fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));
                fragmentOption.AddCondition("IsSelected", i == quantity);
                optionBuilder.AppendLine(fragmentOption.Render());
            }
            return optionBuilder.ToString();
        }

        private int ObtainQuantityLimit(ILekmerCartItem lekmerCartItem)
        {
            int numberInStock;
            if (lekmerCartItem.SizeId.HasValue)
            {
                Collection<IProductSize> productSizes =
                    IoC.Resolve<IProductSizeService>().GetAllByProduct(UserContext.Current, lekmerCartItem.Product.Id);
                IProductSize productSize = productSizes.Single(s => s.SizeId == lekmerCartItem.SizeId.Value);
                numberInStock = productSize.NumberInStock;
            }
            else
            {
                numberInStock = lekmerCartItem.Product.NumberInStock;
            }

            int quantityLimit = 0;
            if (numberInStock > 0) //buy
            {
                quantityLimit = Math.Min(_maxQuantity, numberInStock);
            }
            else if (numberInStock == 0 && ((ILekmerProduct) lekmerCartItem.Product).IsBookable) //book
            {
                quantityLimit = _maxQuantity;
            }
            return quantityLimit;
        }

        private static IProductSize GetSize(ILekmerCartItem item)
        {
            return !item.SizeId.HasValue
                       ? IoC.Resolve<IProductSize>()
                       : IoC.Resolve<IProductSizeService>().GetById(item.Product.Id, item.SizeId.Value);
        }

        private string RenderTagGroup(string commonName, ICartItem cartItem)
        {
            if (commonName == null) throw new ArgumentNullException("commonName");

            Collection<ITagGroup> tagGroups = IoC.Resolve<ITagGroupService>().GetAllByProduct(UserContext.Current,
                                                                                              cartItem.Product.Id);
            ITagGroup tagGroup =
                tagGroups.FirstOrDefault(
                    group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
            if (tagGroup == null)
            {
                return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
            }
            if (tagGroup.Tags.Count == 0)
            {
                return null;
            }
            Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
            fragmentTagGroup.AddVariable("TagGroup.Title",
                                         AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName),
                                         VariableEncoding.None);
            fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);
            return fragmentTagGroup.Render();
        }

        private string RenderTags(ITagGroup tagGroup)
        {
            var tagBuilder = new StringBuilder();
            int i = 0;
            foreach (ITag tag in tagGroup.Tags)
            {
                Fragment fragmentTag = Template.GetFragment("Tag");
                fragmentTag.AddVariable("Tag.Value", tag.Value);
                AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
                tagBuilder.AppendLine(fragmentTag.Render());
            }
            return tagBuilder.ToString();
        }

        private static void AddPositionCondition(Fragment fragment, int index, int count)
        {
            fragment.AddCondition("IsFirst", index == 0);
            fragment.AddCondition("IsLast", index == count - 1);
        }


        /// <summary>
        /// Special format on price for checkout.
        /// </summary>
        /// <param name="channel">The channel to format the price for.</param>
        /// <param name="price">The price to format.</param>
        /// <returns>The formatted price. Shows two decimals if price contains decimals otherwise no decimals</returns>
        private static string FormatPriceForCheckout(IChannel channel, decimal price)
        {
            if (channel == null) throw new ArgumentNullException("channel");

            CultureInfo cultureInfo = channel.CultureInfo;

            var numberFormatInfo = (NumberFormatInfo) cultureInfo.NumberFormat.Clone();

            int check = (int) Math.Round(price*100)%100;
            if (check != 0)
            {
                numberFormatInfo.NumberDecimalDigits = 2;
            }
            else
            {
                numberFormatInfo.NumberDecimalDigits = 0;
            }


            string result = price.ToString("N", numberFormatInfo);

            return string.Format(cultureInfo, channel.Currency.PriceFormat, result);

        }
    }
}