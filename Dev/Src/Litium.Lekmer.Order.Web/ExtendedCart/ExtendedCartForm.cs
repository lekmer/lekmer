using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class ExtendedCartForm : ControlBase
	{
		private VoucherDiscountForm _voucherDiscountForm;

		public VoucherDiscountForm VoucherDiscountForm
		{
			get
			{
				return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm());
			}
		}

		private readonly string _checkoutPostUrl;

		public ExtendedCartForm(string checkoutPostUrl)
		{
			_checkoutPostUrl = checkoutPostUrl;
		}

		public virtual string PostUrl
		{
			get { return _checkoutPostUrl; }
		}

		public virtual string PostModeValue
		{
			get { return "checkout"; }
		}


		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Name", PostModeName);

			VoucherDiscountForm.MapFieldNamesToFragment(fragment);
		}
		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);

			VoucherDiscountForm.MapFieldValuesToFragment(fragment);
		}
	}
}