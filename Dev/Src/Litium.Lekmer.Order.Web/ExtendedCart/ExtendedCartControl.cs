﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Order.Contract.Service;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Cart;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web.ExtendedCart
{
    public class ExtendedCartControl : CartControl
    {
        private readonly IFormatter _formatter;
        private readonly IOrderService _orderService;
        private readonly ICartService _cartService;

        private readonly Dictionary<CartItemKey, bool> _updatedItems = new Dictionary<CartItemKey, bool>();
        private int _maxQuantity;

        public ExtendedCartControl(ICartService cartService, ICartSession cartSession, IFormatter formatter,
                                   ITemplateFactory templateFactory)
            : base(cartService, cartSession, formatter, templateFactory)
        {
            _formatter = formatter;
            _cartService = cartService;
            _orderService = IoC.Resolve<IOrderService>();
        }

        protected override string ModelCommonName
        {
            get { return "ExtendedCartComponent"; }
        }


        private bool IsChangeQuantityPost()
        {
            return PostMode.Equals("changequantity");
        }

        protected virtual ICartFull GetCart()
        {
            RefreshCart();
            return IoC.Resolve<ICartSession>().Cart;
        }
        protected virtual void RefreshCart()
        {
            CartHelper.RefreshSessionCart();
        }

        public override ComponentContent Render()
        {
            Initialize();

            ICartFull cart = GetCart();
            if (cart == null)
            {
                return new ComponentContent();
            }
            Collection<ICartItem> cartItems = cart.GetGroupedCartItemsByProductAndPrice();
            if (cartItems.Count == 0)
            {
                return new ComponentContent();
            }

            if (IsPostBack && IsChangeQuantityPost())
            {
                ChangeQuantityOfItems(cart);
                cart = GetCart();
                if (cart == null)
                {
                    return new ComponentContent();
                }
                cartItems = cart.GetGroupedCartItemsByProductAndPrice(); ;
                if (cartItems.Count == 0)
                {
                    return new ComponentContent();
                }
            }

            return base.Render();
        }

        private void Initialize()
        {
            if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
            {
                _maxQuantity = 10;
            }
        }

        private void ChangeQuantityOfItems(ICartFull cart)
        {
            foreach (ICartItem cartItem in cart.GetGroupedCartItemsByProductAndPrice())
            {
                string key = "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat;

                int? newQuantity = Request.Form.GetInt32OrNull(key);
                if (!newQuantity.HasValue) continue;

                if (newQuantity.Value <= 0)
                {
                    //Delete item by productId and price
                    cart.DeleteItem(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.Quantity);
                }
                else if (newQuantity.Value != cartItem.Quantity)
                {
                    //Get actual quantity which need add or remove
                    int quantity = newQuantity.Value - cartItem.Quantity;
                    ((ILekmerCartFull)cart).ChangeItemQuantityForBlockExtendedCartControl(cartItem.Product, quantity,
                                                                                               ((ILekmerCartItem)cartItem).SizeId,
                                                                                               ((ILekmerCartItem)cartItem).ErpId);

                    var cartItemKey = new CartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat);
                    _updatedItems[cartItemKey] = true;
                }
            }
            _cartService.Save(UserContext.Current, cart);
            CartHelper.RefreshSessionCart();
        }

        protected override string RenderCartItems(Collection<ICartItem> cartItems)

        {
            var itemBuilder = new StringBuilder();

            // grouping by pair: productId, sizeId. sizeId is nullable.
            IEnumerable<IGrouping<KeyValuePair<int, int?>, ICartItem>> groupedCartItems =
                cartItems.GroupBy(item => new KeyValuePair<int, int?>(item.Product.Id, ((ILekmerCartItem) item).SizeId));

            foreach (ICartItem cartItem in cartItems)
            {
                itemBuilder.Append(RenderCartItem(cartItem, groupedCartItems));
            }

            return itemBuilder.ToString();
        }

        /// <summary>
        /// Renders a cart item.
        /// </summary>
        /// <param name="cartItem">The cart item to render.</param>
        /// <param name="groupedCartItems">Grouped Cart Items</param>
        /// <returns>Rendered item.</returns>
        private string RenderCartItem(ICartItem cartItem,
                                      IEnumerable<IGrouping<KeyValuePair<int, int?>, ICartItem>> groupedCartItems)
        {
            var cartItemKey = new CartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat);
            bool itemWasUpdated = _updatedItems.ContainsKey(cartItemKey);

            Fragment fragment = Template.GetFragment("CartItem");
            fragment.AddEntity(cartItem.Product);
            fragment.AddVariable("Product.CampaignInfoPriceHidden",
                                 cartItem.Product.CampaignInfo.Price.IncludingVat.ToString);
            fragment.AddVariable(
                "CartItem.Quantity",
                _formatter.FormatNumber(Channel.Current, cartItem.Quantity));
            fragment.AddVariable(
                "CartItem.PriceIncludingVatSummary",
                _formatter.FormatPrice(Channel.Current, cartItem.GetActualPriceSummary().IncludingVat));
            fragment.AddVariable(
                "CartItem.PriceExcludingVatSummary",
                _formatter.FormatPrice(Channel.Current, cartItem.GetActualPriceSummary().ExcludingVat));
            fragment.AddCondition("Product.IsInStock",
                                  ((ILekmerOrderService) _orderService).CheckNumberInStock(UserContext.Current, cartItem,
                                                                                           groupedCartItems));

            var lekmerCartItem = (ILekmerCartItem) cartItem;
            int quantityLimit = ObtainQuantityLimit(lekmerCartItem);
            fragment.AddVariable("Product.NumberInStockForSize", quantityLimit.ToString(), VariableEncoding.None);
            fragment.AddVariable(
                "Iterate:QuantityOption",
                RenderQuantityOptions(cartItem.Quantity, quantityLimit),
                VariableEncoding.None);
            fragment.AddCondition("ItemWasUpdated", itemWasUpdated);
            fragment.AddCondition("IsNumberInStockTooSmall", cartItem.Quantity > quantityLimit);

            fragment.AddCondition("CartItem.HasSize", lekmerCartItem.SizeId.HasValue);
            fragment.AddEntity(GetSize(lekmerCartItem));

            fragment.AddRegexVariable(
                @"\[TagGroup\(\""(.+)\""\)\]",
                delegate(Match match)
                    {
                        string tagGroupCommonName = match.Groups[1].Value;
                        return RenderTagGroup(tagGroupCommonName, lekmerCartItem);
                    },
                VariableEncoding.None);

            return fragment.Render();
        }

        private string RenderQuantityOptions(int quantity, int quantitiesToDisplay)
        {
            var optionBuilder = new StringBuilder();
            for (int i = 0; i <= Math.Max(quantitiesToDisplay, quantity); i++)
            {
                Fragment fragmentOption = Template.GetFragment("QuantityOption");
                fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
                fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));
                fragmentOption.AddCondition("IsSelected", i == quantity);
                optionBuilder.AppendLine(fragmentOption.Render());
            }
            return optionBuilder.ToString();
        }

        private int ObtainQuantityLimit(ILekmerCartItem lekmerCartItem)
        {
            int numberInStock;
            if (lekmerCartItem.SizeId.HasValue)
            {
                Collection<IProductSize> productSizes =
                    IoC.Resolve<IProductSizeService>().GetAllByProduct(UserContext.Current, lekmerCartItem.Product.Id);
                IProductSize productSize = productSizes.Single(s => s.SizeId == lekmerCartItem.SizeId.Value);
                numberInStock = productSize.NumberInStock;
            }
            else
            {
                numberInStock = lekmerCartItem.Product.NumberInStock;
            }

            int quantityLimit = 0;
            if (numberInStock > 0) //buy
            {
                quantityLimit = Math.Min(_maxQuantity, numberInStock);
            }
            else if (numberInStock == 0 && ((ILekmerProduct) lekmerCartItem.Product).IsBookable) //book
            {
                quantityLimit = _maxQuantity;
            }
            return quantityLimit;
        }

        private static IProductSize GetSize(ILekmerCartItem item)
        {
            return !item.SizeId.HasValue
                       ? IoC.Resolve<IProductSize>()
                       : IoC.Resolve<IProductSizeService>().GetById(item.Product.Id, item.SizeId.Value);
        }

        private string RenderTagGroup(string commonName, ICartItem cartItem)
        {
            if (commonName == null) throw new ArgumentNullException("commonName");

            Collection<ITagGroup> tagGroups = IoC.Resolve<ITagGroupService>().GetAllByProduct(UserContext.Current,
                                                                                              cartItem.Product.Id);
            ITagGroup tagGroup =
                tagGroups.FirstOrDefault(
                    group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
            if (tagGroup == null)
            {
                return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
            }
            if (tagGroup.Tags.Count == 0)
            {
                return null;
            }
            Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
            fragmentTagGroup.AddVariable("TagGroup.Title",
                                         AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName),
                                         VariableEncoding.None);
            fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);
            return fragmentTagGroup.Render();
        }

        private string RenderTags(ITagGroup tagGroup)
        {
            var tagBuilder = new StringBuilder();
            int i = 0;
            foreach (ITag tag in tagGroup.Tags)
            {
                Fragment fragmentTag = Template.GetFragment("Tag");
                fragmentTag.AddVariable("Tag.Value", tag.Value);
                AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
                tagBuilder.AppendLine(fragmentTag.Render());
            }
            return tagBuilder.ToString();
        }

        private static void AddPositionCondition(Fragment fragment, int index, int count)
        {
            fragment.AddCondition("IsFirst", index == 0);
            fragment.AddCondition("IsLast", index == count - 1);
        }
    }
}