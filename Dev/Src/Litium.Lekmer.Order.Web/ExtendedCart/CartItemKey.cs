namespace Litium.Lekmer.Order.Web
{
	public struct CartItemKey
	{
		private readonly int _productId;
		private readonly decimal _price;

		public CartItemKey(int productId, decimal price)
		{
			_productId = productId;
			_price = price;
		}

		public int ProductId
		{
			get { return _productId; }
		}

		public decimal Price
		{
			get { return _price; }
		}

		#region Equality members

		public bool Equals(CartItemKey other)
		{
			return other._productId == _productId && other._price == _price;
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (obj.GetType() != typeof (CartItemKey)) return false;
			return Equals((CartItemKey) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (_productId*397) ^ _price.GetHashCode();
			}
		}

		public static bool operator ==(CartItemKey left, CartItemKey right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(CartItemKey left, CartItemKey right)
		{
			return !left.Equals(right);
		}

		#endregion
	}
}