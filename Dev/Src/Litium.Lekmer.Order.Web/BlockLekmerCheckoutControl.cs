﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Avail.Web;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order.Contract.Service;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Lekmer.Voucher;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Payment;
using Litium.Scensum.Order.Web.Checkout;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
    internal class BlockLekmerCheckoutControl : BlockCheckoutControl
    {
        private Payment _payment;
        private Delivery _delivery;
        private LekmerCheckoutForm _lekmerCheckoutForm;
        private VoucherDiscountForm _voucherDiscountForm;
        private readonly IFormatter _formatter;
        private readonly IPaymentTypeService _paymentTypeService;
        private readonly IPaymentProvider _paymentProvider;
        private readonly IOrderService _orderService;
        private readonly IOrderSession _orderSession;
        private readonly IOrderStatusService _orderStatusService;
        private readonly IOrderItemStatusService _orderItemStatusService;
        private readonly IVoucherService _voucherService;

        public BlockLekmerCheckoutControl(
            ICustomerSession customerSession, ITemplateFactory templateFactory,
            IPaymentTypeService paymentTypeService, IPaymentProvider paymentProvider,
            IOrderService orderService, IOrderSession orderSession, IOrderStatusService orderStatusService,
            IOrderItemStatusService orderItemStatusService, IFormatter formatter)
            : base(customerSession, templateFactory)
        {
            _paymentTypeService = paymentTypeService;
            _paymentProvider = paymentProvider;
            _orderService = orderService;
            _orderSession = orderSession;
            _orderStatusService = orderStatusService;
            _orderItemStatusService = orderItemStatusService;
            _formatter = formatter;
            _voucherService = IoC.Resolve<IVoucherService>();
        }

        public LekmerCheckoutForm LekmerCheckoutForm
        {
            get
            {
                if (_lekmerCheckoutForm == null) _lekmerCheckoutForm = CreateLekmerCheckoutForm();
                return _lekmerCheckoutForm;
            }
        }

        public VoucherDiscountForm VoucherDiscountForm
        {
            get { return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm()); }
        }


        protected virtual LekmerCheckoutForm CreateLekmerCheckoutForm()
        {
            return new LekmerCheckoutForm(GetCheckoutPostUrl());
        }

        private void Initialize()
        {
            _payment = new Payment();
            _payment.Initialize();
            _payment.PaymentTypeSelected += OnPaymentTypeSelected;
            _delivery = new Delivery();
            _delivery.Initialize();
        }

        private void OnPaymentTypeSelected(object sender, Payment.PaymentTypeSelectedEventArgs e)
        {
            _delivery.PaymentTypeId = e.PaymentTypeId.ToString();
        }


        protected override BlockContent RenderCore()
        {
            Initialize();

            ICartFull cart = GetCart();
            if (cart == null || cart.GetCartItems().Count == 0)
            {
                return RenderNoActiveCart();
            }

            ICustomer customer = GetCustomer();

            var validationResult = new ValidationResult();
            string orderStatus = "PaymentPending";
            if (LekmerCheckoutForm.IsFormPostBack)
            {
                LekmerCheckoutForm.MapFromRequest();

                if (!Request.Form["GetAddress"].IsNullOrEmpty())
                {


                    if (Request.Form["customer-civicnumber"].IsNullOrEmpty())
                    {
                        validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.EnterCivicNumber"));
                    }
                    else
                    {
                        try
                        {
                            var addressFromKlarna = KlarnaClient.GetAddress(
                                Request.Form["customer-civicnumber"],
                                UserContext.Current.Channel);
                            MapInformationFromKlarna(addressFromKlarna);
                        }
                        catch (Exception e)
                        {
                            validationResult.Errors.Add(e.Message);
                        }
                    }
                }
                else if (!IsVoucherPost())
                {
                    if (IoC.Resolve<IVoucherSession>().WasUsed)
                    {
                        IoC.Resolve<IVoucherSession>().VoucherCode = null;
                        IoC.Resolve<IVoucherSession>().WasUsed = false;
                        return RenderCheckoutForm(cart, validationResult);
                    }

                    ((LekmerInformationForm)LekmerCheckoutForm.InformationForm).IsCreditCard = _payment.IsCreditCard;
                    validationResult = LekmerCheckoutForm.Validate(_payment.IsCreditCard);
                    if (_delivery.GetOrderDelivery() == null)
                    {
                        validationResult.Errors.Add("Inget leveranssätt valt.");
                    }
                    if (validationResult.IsValid)
                    {
                        LekmerCheckoutForm.MapToCustomer(customer);

                        var orderService = (ILekmerOrderService)IoC.Resolve<IOrderService>();
                        ILekmerOrderFull order = CreateOrder(cart, customer, orderService, orderStatus);
                        //if (!orderService.CheckNumberInStock(UserContext.Current, order))
                        //{
                        ////    validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.OrderItemQuantityTooBig"));
                        //}
                        if (validationResult.IsValid)
                        {
                            if (_payment.IsCreditCard)
                            {
                                orderStatus = "PaymentPending";
                            }
                            if (_payment.IsKlarnaPartPayment)
                            {
                                orderStatus = ProcessKlarnaPartPayment(validationResult, customer, orderService, order);
                            }
                            if (_payment.IsKlarnaSpecialPartPayment)
                            {
                                orderStatus = ProcessSpecialKlarnaPartPayment(validationResult, customer, orderService, order);
                            }
                            if (_payment.IsKlarnaInvoice)
                            {
                                orderStatus = ProcessKlarnaInvoicePayment(validationResult, customer, orderService, order);
                            }
                            if (validationResult.IsValid)
                            {
                                SaveOrder(orderService, order, orderStatus);
                                _orderSession.OrderId = order.Id;

                                RefreshCustomer();
                                if (_payment.IsCreditCard)
                                {
                                    RedirectToCreditCard(order);
                                    return new BlockContent();
                                }

                                //Flush cache for all payment types if not redirected to creditcardpayment.
                                IoC.Resolve<IVoucherSession>().Voucher = null;
                                IoC.Resolve<IUserContextFactory>().FlushCache();

                                if (orderStatus!="RejectedByPaymentProvider")
                                {
                                    order = (ILekmerOrderFull)orderService.GetFullById(UserContext.Current, order.Id);
                                    orderService.SendConfirm(UserContext.Current, order);
                                    ResetCart();
                                    LekmerCartCookie.DeleteShoppingCardGuid();
                                    return RenderConfirmResponse(order);
                                }
                                return new BlockContent();
                            }
                        }
                    }
                }
            }
            else if (PostMode.Equals("getDeliveryMethods") || !Request.Form["changePaymentType"].IsNullOrEmpty())
            {
                LekmerCheckoutForm.MapFromRequest();
                return RenderCheckoutForm(cart, string.Empty);
            }
            else
            {
                //if (!((ILekmerOrderService)_orderService).CheckNumberInStock(UserContext.Current, cart))
                //{
                //    validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.OrderItemQuantityTooBig"));
                //}

                LekmerCheckoutForm.SetDefaultValues(customer);
            }

            return RenderCheckoutForm(cart, validationResult);
        }

        private ILekmerOrderFull CreateOrder(ICartFull cart, ICustomer customer, ILekmerOrderService orderService, string orderStatus)
        {
            var order = (ILekmerOrderFull)orderService.CreateFull(UserContext.Current);

            order.MergeWith(_delivery.GetOrderDelivery());

            order.MergeWith(UserContext.Current, cart);
            order.MergeWith(UserContext.Current, _payment.GetOrderPayments(order));
            order.MergeWith(customer);
            order.MergeWith(AlternateVersion.UseAlternate);

            order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, orderStatus);
            var itemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, orderStatus);
            order.SetAllOrderItemsStatus(itemStatus);

            order.IP = Request.UserHostAddress;
            return order;
        }

        private string ProcessKlarnaPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderService orderService, ILekmerOrderFull order)
        {
            string orderStatus;
            try
            {
                int classId;
                int.TryParse(Request.Form["Partpaymentlist"], out classId);
                if (classId == 0)
                    throw new Exception(AliasHelper.GetAliasValue("Order.Checkout.Validation.PartPaymentNotProvided"));

                orderService.SaveFullExtended(UserContext.Current, order);

                string invoiceNo = KlarnaClient.ReserveAmount(UserContext.Current.Channel,
                                                              customer,
                                                              order,
                                                              classId,
                                                              (int)Math.Round(order.GetActualPriceTotal().IncludingVat) * 100,
                                                              null);
                order.Payments[0].ReferenceId = invoiceNo;
                orderStatus = "PaymentConfirmed";
            }
            catch (Exception e)
            {
                orderStatus = RejectOrder(orderService, order, validationResult, e);
            }
            return orderStatus;
        }
        private string ProcessSpecialKlarnaPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderService orderService, ILekmerOrderFull order)
        {
            string orderStatus;
            try
            {
                int classId;
                int.TryParse(Request.Form["paymentType"], out classId);
                if (classId == 0)
                    throw new Exception(AliasHelper.GetAliasValue("Order.Checkout.Validation.PartPaymentNotProvided"));

                orderService.SaveFullExtended(UserContext.Current, order);

                string invoiceNo = KlarnaClient.ReserveAmount(UserContext.Current.Channel,
                                                              customer,
                                                              order,
                                                              classId,
                                                              (int)Math.Round(order.GetActualPriceTotal().IncludingVat) * 100,
                                                              null);
                order.Payments[0].ReferenceId = invoiceNo;
                orderStatus = "PaymentConfirmed";
            }
            catch (Exception e)
            {
                orderStatus = RejectOrder(orderService, order, validationResult, e);
            }
            return orderStatus;
        }

        private string ProcessKlarnaInvoicePayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderService orderService, ILekmerOrderFull order)
        {
            string orderStatus;
            try
            {
                orderService.SaveFullExtended(UserContext.Current, order);

                string invoiceNo = KlarnaClient.ReserveAmount(UserContext.Current.Channel,
                                                              customer,
                                                              order,
                                                              -1,
                                                              (int)Math.Round(order.GetActualPriceTotal().IncludingVat) * 100,
                                                              null);
                order.Payments[0].ReferenceId = invoiceNo;
                orderStatus = "PaymentConfirmed";
            }
            catch (Exception e)
            {
                orderStatus = RejectOrder(orderService, order, validationResult, e);
            }
            return orderStatus;
        }

        private string RejectOrder(IOrderService orderService, IOrderFull order, ValidationResult validationResult, Exception e)
        {
            validationResult.Errors.Add(e.Message);
            var lekmerUserContext = (ILekmerUserContext)UserContext.Current;
            var campaignInfo = (ILekmerOrderCampaignInfo)order.CampaignInfo;

            if (lekmerUserContext.Voucher != null && campaignInfo != null && campaignInfo.VoucherDiscount != null)
            {
                _voucherService.VoucherRelease(lekmerUserContext.Voucher.VoucherCode);
            }
            const string orderStatus = "RejectedByPaymentProvider";
            order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, orderStatus);
            var itemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, orderStatus);
            order.SetAllOrderItemsStatus(itemStatus);
            orderService.SaveFull(UserContext.Current, order);
            return orderStatus;
        }

        private void SaveOrder(ILekmerOrderService orderService, IOrderFull order, string orderStatus)
        {
            order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, orderStatus);
            var orderItemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, orderStatus);

            order.SetAllOrderItemsStatus(orderItemStatus);

            order.IP = Request.UserHostAddress;
            ((ILekmerOrderFull)order).CustomerIdentificationKey = CustomerIdentificationCookie.SetCustomerIdentificationId(CustomerSession);
            if (order.Id != 0)
            {
                if (order.CampaignInfo != null)
                {
                    order.CampaignInfo.Campaigns = new Collection<IOrderCampaign>();
                }
                orderService.SaveFull(UserContext.Current, order);
                var lekmerUserContext = (ILekmerUserContext)UserContext.Current;
                var campaignInfo = (ILekmerOrderCampaignInfo)order.CampaignInfo;
                if (lekmerUserContext.Voucher != null && campaignInfo != null && campaignInfo.VoucherDiscount != null)
                {
                    var customerId = lekmerUserContext.Customer == null ? -1 : lekmerUserContext.Customer.Id;
                    _voucherService.VoucherConsume(UserContext.Current.Channel.ApplicationName, customerId, lekmerUserContext.Voucher.VoucherCode, order.Id);
                }
            }
            else
            {
                orderService.SaveFullExtended(UserContext.Current, order);
            }

            if (orderStatus == "PaymentConfirmed")
            {
                orderService.UpdateNumberInStock(UserContext.Current, order);
            }

        }

        private bool IsVoucherPost()
        {
            string useVoucherValue = Request.Form[VoucherDiscountForm.UseVoucherFormName];
            return useVoucherValue == VoucherDiscountForm.UseVoucherFormValue ||
                   useVoucherValue == VoucherDiscountForm.UseVoucherFormText;
        }

        private void MapInformationFromKlarna(IKlarnaAddress addressFromKlarna)
        {
            LekmerCheckoutForm.InformationForm.FirstName = addressFromKlarna.FirstName;
            LekmerCheckoutForm.InformationForm.LastName = addressFromKlarna.LastName;
            LekmerCheckoutForm.InformationForm.CivicNumber = Request.Form["customer-civicnumber"];
            ((LekmerInformationForm)LekmerCheckoutForm.InformationForm).LockFields();

            LekmerCheckoutForm.BillingAddressForm.City = addressFromKlarna.City;
            LekmerCheckoutForm.BillingAddressForm.Addressee = addressFromKlarna.FirstName + " " + addressFromKlarna.LastName;
            LekmerCheckoutForm.BillingAddressForm.StreetAddress = addressFromKlarna.StreetAddress;
            LekmerCheckoutForm.BillingAddressForm.StreetAddress2 = addressFromKlarna.StreetAddress2;
            LekmerCheckoutForm.BillingAddressForm.PostalCode = addressFromKlarna.PostalCode;
            ((LekmerAddressForm)LekmerCheckoutForm.BillingAddressForm).LockFields();
        }

        public BlockContent RenderCheckoutForm(ICartFull cart, string validationError)
        {
            Fragment fragmentContent = Template.GetFragment("Content");
            LekmerCheckoutForm.MapFieldNamesToFragment(fragmentContent);
            LekmerCheckoutForm.MapFieldValuesToFragment(fragmentContent);
            fragmentContent.AddEntity(Block);
            fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);

            _payment.Render();
            fragmentContent.AddVariable("Delivery", _delivery.Render(), VariableEncoding.None);

            var totalPrice = cart.GetActualPriceSummary().IncludingVat + _delivery.GetActualDeliveryPrice(cart) +
                             _payment.GetPaymentCost();
            var klarnaSum = (int)Math.Round(totalPrice) * 100;
            _payment.KlarnaSum = klarnaSum;

            fragmentContent.AddVariable("Payment", _payment.Render(), VariableEncoding.None);
            fragmentContent.AddVariable("PaymentType.Cost", _formatter.FormatPrice(Channel.Current, _payment.GetPaymentCost()));
            fragmentContent.AddVariable("DeliveryMethod.FreightCost",
                                        _formatter.FormatPrice(Channel.Current, _delivery.GetActualDeliveryPrice(cart)));
            fragmentContent.AddVariable("Checkout.TotalPriceWithFreight", _formatter.FormatPrice(Channel.Current, totalPrice));
            fragmentContent.AddCondition("isKlarnaPartPayment", _payment.IsKlarnaPartPayment);


			var monthlyPayment = PaymentHelper.GetLowestPartPayment(Channel.Current, klarnaSum);
            //KlarnaClient.PopulatePartPayment(Channel.Current, 100, klarnaSum, true)/100;
            fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", monthlyPayment.ToString(CultureInfo.InvariantCulture),
                                        VariableEncoding.None);

            Fragment fragmentHead = Template.GetFragment("Head");
            LekmerCheckoutForm.MapFieldNamesToFragment(fragmentHead);

            return new BlockContent(fragmentHead.Render(), fragmentContent.Render());
        }

        protected override BlockContent RenderCheckoutForm(ICartFull cart, ValidationResult validationResult)
        {
            string validationError = null;

            if (!validationResult.IsValid)
            {
                var validationControl = CreateValidationControl(validationResult.Errors);
                validationError = validationControl.Render();
            }

            return RenderCheckoutForm(cart, validationError);
        }

        private void RedirectToCreditCard(IOrderFull order)
        {
            int paymentTypeId = order.Payments.First().PaymentTypeId;
            IPaymentType paymentType = _paymentTypeService.GetAll(UserContext.Current)
                .SingleOrDefault(type => type.Id == paymentTypeId);

            string dibsRequest = _paymentProvider.CreateRequest(
                order.Customer.CustomerInformation, order, Channel.Current.Currency, paymentType);

            dibsRequest += "billingCountry=" + Server.UrlEncode(Channel.Current.Country.Title);

            Response.Redirect(dibsRequest);
        }

        /// <summary>
        /// Gets a new/existing customer.
        /// </summary>
        /// <returns></returns>
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        protected override ICustomer GetCustomer()
        {
            var customerService = IoC.Resolve<ICustomerService>();
            ICustomer customer = null;
            if (SignedInCustomerId.HasValue)
            {
                customer = customerService.GetById(UserContext.Current, SignedInCustomerId.Value);
            }
            if (customer == null)
            {
                customer = customerService.Create(UserContext.Current, true, false);
            }

            if (customer.CustomerInformation == null)
            {
                customer.CustomerInformation = IoC.Resolve<ICustomerInformationService>().Create(UserContext.Current);
            }

            if (customer.CustomerInformation.DefaultBillingAddress == null)
            {
                customer.CustomerInformation.DefaultBillingAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
            }
            customer.CustomerInformation.DefaultBillingAddress.CountryId = Channel.Current.Country.Id;

            return customer;
        }
    }
}