﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order.Web.OrderHistory;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerBlockOrderHistoryDetailControl : BlockOrderHistoryDetailControl
	{
		public LekmerBlockOrderHistoryDetailControl(ITemplateFactory templateFactory, IBlockService blockService) : base(templateFactory, blockService)
		{
		}

		protected override string RenderOrderItemList()
		{
			var itemBuilder = new StringBuilder();
			foreach (var orderItem in Order.GetOrderItems())
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddEntity(orderItem);
				var lekmerItem = (ILekmerOrderItem) orderItem;
				fragmentOrderItem.AddCondition("OrderItem.HasTagGroups", lekmerItem.TagGroups.Count > 0);
				fragmentOrderItem.AddRegexVariable(
					@"\[TagGroup\(\""(.+)\""\)\]",
					delegate(Match match)
					{
						string tagGroupCommonName = match.Groups[1].Value;
						return RenderTagGroup(tagGroupCommonName, lekmerItem);
					},
					VariableEncoding.None);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}
			return itemBuilder.ToString();
		}

		private string RenderTagGroup(string commonName, ILekmerOrderItem orderItem)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var tagGroup = orderItem.TagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}
			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}
			var fragmentTagGroup = Template.GetFragment("TagGroupItem");
			fragmentTagGroup.AddVariable("TagGroup.Title", AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName), VariableEncoding.None);
			fragmentTagGroup.AddCondition("OrderItem.HasTags", tagGroup.Tags.Count > 0);
			fragmentTagGroup.AddVariable("Iterate:TagItem", RenderTags(tagGroup), VariableEncoding.None);
			return fragmentTagGroup.Render();
		}

		private string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();
			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				var fragmentTag = Template.GetFragment("TagItem");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}
			return tagBuilder.ToString();
		}

		private static void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}
	}
}
