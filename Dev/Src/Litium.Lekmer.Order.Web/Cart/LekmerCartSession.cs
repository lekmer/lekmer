﻿using System.Web;
using System.Web.SessionState;
using Litium.Scensum.Core;
using Litium.Scensum.Order.Web.Cart;

namespace Litium.Lekmer.Order.Web.Cart
{
    public class LekmerCartSession : CartSession
    {
        public LekmerCartSession(IUserContextFactory userContextFactory)
            : base(userContextFactory)
        {
        }
        private static HttpSessionState Session
        {
            get { return HttpContext.Current.Session; }
        }

        public int? AvailProductId
        {
            get
            {
                if (Session == null) return null;
                return Session["AvailProductId"] as int?;
            }
            set
            {
                if (Session == null) return;
                Session["AvailProductId"] = value;
            }
        }
        public string TrackingCode
        {
            get
            {
                if (Session == null) return null;
                return Session["AvailTrackingCode"] as string;
            }
            set
            {
                if (Session == null) return;
                Session["AvailTrackingCode"] = value;
            }
        }
    }
}
