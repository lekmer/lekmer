﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Checkout;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutForm : CheckoutForm
	{
		private VoucherDiscountForm _voucherDiscountForm;

		public LekmerCheckoutForm(string checkoutPostUrl) : base(checkoutPostUrl)
		{
		}

		public ValidationResult Validate(bool isCreditCard)
		{
			var validationResult = new ValidationResult();

			var lekmerInformationForm = (LekmerInformationForm) InformationForm;

			validationResult.Errors.Append(lekmerInformationForm.Validate(isCreditCard).Errors);
			validationResult.Errors.Append(BillingAddressForm.Validate().Errors);

			if (!UseBillingAddressAsDeliveryAddress)
			{
				validationResult.Errors.Append(DeliveryAddressForm.Validate().Errors);
			}

			return validationResult;
		}


		public string AcceptOrderTermsFormName
		{
			get { return "checkout-acceptorderterms"; }
		}

		public virtual bool AcceptOrderTerms { get; set; }

		protected override InformationForm CreateInformationForm()
		{
			return new LekmerInformationForm();
		}

		protected override AddressForm CreateBillingAddressForm()
		{
			return new LekmerAddressForm("BillingAddress", "billingaddress");
		}


		public VoucherDiscountForm VoucherDiscountForm
		{
			get
			{
				return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm());
			}
		}

		//public override Collection<IOrderPayment> GetOrderPayments()
		//{
		//    var orderPayment = IoC.Resolve<IOrderPayment>();
		//    orderPayment.PaymentTypeId = int.Parse(Request.Form["paymentType"]);
		//    orderPayment.Price = 1; //TODO: Fix this, there are a constraint in the db saying it cant be 0
		//    orderPayment.Vat = 1; //TODO: Fix this, there are a constraint in the db saying it cant be 0
		//    return new Collection<IOrderPayment>
		//            {
		//                orderPayment
		//            };
		//}

		public override void MapFromRequest()
		{
			base.MapFromRequest();

			var accept = Request.Form[AcceptOrderTermsFormName];
			AcceptOrderTerms = !string.IsNullOrEmpty(accept) && (accept.ToLower() == "on" || accept.ToLower() == "agree");
		}

		public override void MapFieldNamesToFragment(Fragment fragment)
		{
			base.MapFieldNamesToFragment(fragment);

			VoucherDiscountForm.MapFieldNamesToFragment(fragment);

			fragment.AddVariable("Checkout.AcceptOrderTerms.Name", AcceptOrderTermsFormName);
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			VoucherDiscountForm.MapFieldValuesToFragment(fragment);

			fragment.AddCondition("Checkout.AcceptOrderTerms.Checked", AcceptOrderTerms);
		}
		/// <summary>
		/// Maps the form values to a customer.
		/// </summary>
		/// <param name="customer">The customer to map to.</param>
		public virtual void MapToCustomer(ICustomer customer)
		{
			InformationForm.MapFromFormToCustomerInformation(customer.CustomerInformation);
			BillingAddressForm.MapFromFormToAddress(customer.CustomerInformation.DefaultBillingAddress);

			if (!UseBillingAddressAsDeliveryAddress)
			{
				if (customer.CustomerInformation.DefaultDeliveryAddress==null || 
				    customer.CustomerInformation.DefaultDeliveryAddress.Id == customer.CustomerInformation.DefaultBillingAddress.Id)
				{
					customer.CustomerInformation.DefaultDeliveryAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
					customer.CustomerInformation.DefaultDeliveryAddress.CountryId = Channel.Current.Country.Id;
				}
				DeliveryAddressForm.MapFromFormToAddress(customer.CustomerInformation.DefaultDeliveryAddress);
			}
		}

		/// <summary>
		/// Sets default values of form.
		/// </summary>
		/// <param name="customer">Customer with information for setting form values.</param>
		public virtual void SetDefaultValues(ICustomer customer)
		{
			if (customer == null) throw new ArgumentNullException("customer");

			InformationForm.MapFromCustomerInformationToForm(customer.CustomerInformation);
			BillingAddressForm.MapFromAddressToForm(customer.CustomerInformation.DefaultBillingAddress);

			UseBillingAddressAsDeliveryAddress =
				customer.IsNew || customer.CustomerInformation.DefaultDeliveryAddress == null || customer.CustomerInformation.DefaultDeliveryAddress.IsNew ||
				customer.CustomerInformation.DefaultBillingAddressId == customer.CustomerInformation.DefaultDeliveryAddressId;
			if (!UseBillingAddressAsDeliveryAddress && customer.CustomerInformation.DefaultDeliveryAddress!=null)
			{
				DeliveryAddressForm.MapFromAddressToForm(customer.CustomerInformation.DefaultDeliveryAddress);
			}
		}
	}
}