using System.Collections.Specialized;
using Litium.Lekmer.Payment.Klarna;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerAddressForm : AddressForm
	{
		private bool _lockFields;

		public LekmerAddressForm(string formName, string fieldPrefix)
			: base(formName, fieldPrefix)
		{
		}

		public virtual string FirstName { get; set; }

		public string FirstNameFormName
		{
			get { return "customer-firstname"; }
		}

		public virtual string LastName { get; set; }

		public string LastNameFormName
		{
			get { return "customer-lastname"; }
		}

		public void MapFromAddressToForm(IKlarnaAddress address)
		{
			IKlarnaAddress klarnaAddress = address;
			base.MapFromAddressToForm(address);

			FirstName = klarnaAddress.FirstName;
			LastName = klarnaAddress.LastName;
		}

		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();
			NameValueCollection form = Request.Form;
			FirstName = form[FirstNameFormName].NullWhenTrimmedEmpty();
			LastName = form[LastNameFormName].NullWhenTrimmedEmpty();
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			if (_lockFields)
			{
				fragment.AddVariable("Form." + FormName + ".City.Readonly", "readonly");
				fragment.AddVariable("Form." + FormName + ".StreetAddress.Readonly", "readonly");
				fragment.AddVariable("Form." + FormName + ".StreetAddress2.Readonly", "readonly");
				fragment.AddVariable("Form." + FormName + ".PostalCode.Readonly", "readonly");
			}
		}

		public void LockFields()
		{
			_lockFields = true;
		}
	}
}