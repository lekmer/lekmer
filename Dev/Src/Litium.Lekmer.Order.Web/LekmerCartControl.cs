using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Web.Cart;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Cart;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCartControl : CartControl
	{
		public LekmerCartControl(
			ICartService cartService, ICartSession cartSession, IFormatter formatter, ITemplateFactory templateFactory)
			: base(cartService, cartSession, formatter, templateFactory)
		{
		}

		//protected override string RenderCart()
		//{
		//    string renderedCart = base.RenderCart();

		//    Session["CartItemAdded"] = null;

		//    return renderedCart;
		//}


		/// <summary>
		/// Renders a cart with items.
		/// </summary>
		/// <returns>Rendered cart. Null if no items is added to the cart.</returns>
		protected override string RenderCart()
		{
			ICartFull cart = IoC.Resolve<ICartSession>().Cart;
			if (cart == null)
			{
				return null;
			}
			Collection<ICartItem> cartItems = cart.GetGroupedCartItemsByProductAndPrice();
			if (cartItems.Count == 0)
			{
				return null;
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			var cartSession = ((LekmerCartSession)IoC.Resolve<ICartSession>());
			int? productId = cartSession.AvailProductId;

			string trackingCode = cartSession.TrackingCode;
			bool isRecommendedProduct = !string.IsNullOrEmpty(trackingCode);

			fragmentContent.AddCondition("AddToAvail", productId.HasValue);
			fragmentContent.AddCondition("Product.IsRecommended", isRecommendedProduct);

			if (productId.HasValue)
			{
				fragmentContent.AddVariable("Product.Id", productId.Value.ToString);
				cartSession.AvailProductId = null;
			}
			if (isRecommendedProduct)
			{
				fragmentContent.AddVariable("TrackingCode", trackingCode);
				cartSession.TrackingCode = null;
			}

		
			fragmentContent.AddVariable(
				"Iterate:CartItem",
				RenderCartItems(cartItems),
				VariableEncoding.None);
			Price cartPrice = cart.GetActualPriceSummary();
			fragmentContent.AddVariable(
				"Cart.PriceIncludingVatSummary",
				Formatter.FormatPrice(Channel.Current, cartPrice.IncludingVat));
			fragmentContent.AddVariable(
				"Cart.PriceExcludingVatSummary",
				Formatter.FormatPrice(Channel.Current, cartPrice.ExcludingVat));
			fragmentContent.AddVariable(
				"Cart.QuantitySummary",
				Formatter.FormatNumber(Channel.Current, cart.GetQuantitySummary()));

			Session["CartItemAdded"] = null;
			return fragmentContent.Render();
		}

		protected override string RenderCartItem(ICartItem cartItem)
		{
			var productIdAdded = Session["CartItemAdded"] as int?;
			bool itemWasAdded = productIdAdded.HasValue && productIdAdded.Value == cartItem.Product.Id;

			Fragment fragment = Template.GetFragment("CartItem");
			fragment.AddEntity(cartItem.Product);
			fragment.AddVariable("Product.CampaignInfoPriceHidden", cartItem.Product.CampaignInfo.Price.IncludingVat.ToString);
			fragment.AddVariable(
				"CartItem.Quantity",
				Formatter.FormatNumber(Channel.Current, cartItem.Quantity));
			fragment.AddVariable(
				"CartItem.PriceIncludingVatSummary",
				Formatter.FormatPrice(Channel.Current, cartItem.GetActualPriceSummary().IncludingVat));
			fragment.AddVariable(
				"CartItem.PriceExcludingVatSummary",
				Formatter.FormatPrice(Channel.Current, cartItem.GetActualPriceSummary().ExcludingVat));
			fragment.AddCondition("ItemWasAdded", itemWasAdded);

			return fragment.Render();
		}
	}
}