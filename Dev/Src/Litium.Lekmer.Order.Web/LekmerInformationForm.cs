using System;
using Litium.Lekmer.Order.Web.Validation;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerInformationForm : InformationForm
	{
		private bool _lockFields;

		public bool IsCreditCard { get; set; }

		public ValidationResult Validate(bool isCreditCard)
		{
			var validationResult = base.Validate();
			ValidateCellPhone(validationResult);
			if (!isCreditCard)
			{
				ValidateCivicNumber(validationResult);
			}
			return validationResult;
		}
		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();
			if (CivicNumber != null)
			{
				CivicNumber = CivicNumber.Trim();
			}
		}

		public override void MapFromFormToCustomerInformation(ICustomerInformation customerInformation)
		{
			if (!Validate(IsCreditCard).IsValid) throw new ValidationException("Couldn't validate the customer information.");

			customerInformation.FirstName = FirstName;
			customerInformation.LastName = LastName;
			customerInformation.CivicNumber = CivicNumber;
			customerInformation.PhoneNumber = PhoneNumber;
			customerInformation.CellPhoneNumber = CellPhoneNumber;
			customerInformation.Email = Email;
			customerInformation.CreatedDate = DateTime.Now;
		}


		public override ValidationResult Validate()
		{
			var validationResult = base.Validate();
			ValidateCellPhone(validationResult);
			ValidateCivicNumber(validationResult);
			return validationResult;
		}

		protected virtual void ValidateCellPhone(ValidationResult validationResult)
		{
			if (CellPhoneNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CellPhoneNumberNotProvided"));
			}
		}

		protected virtual void ValidateCivicNumber(ValidationResult validationResult)
		{
			if (CivicNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CivicNumberNotProvided"));
			}
			else
			{
				var iso = (ValidationUtil.CountryISO)Enum.Parse(typeof(ValidationUtil.CountryISO), UserContext.Current.Channel.Country.Iso, true);
				if (!ValidationUtil.IsValidCivicNumber(CivicNumber, iso))
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CivicNumberNotValid"));
				}
			}
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			if (_lockFields)
			{
				fragment.AddVariable("Form.CustomerInformation.FirstName.Readonly", "readonly");
				fragment.AddVariable("Form.CustomerInformation.LastName.Readonly", "readonly");
			}
		}

		public void LockFields()
		{
			_lockFields = true;
		}
	}
}