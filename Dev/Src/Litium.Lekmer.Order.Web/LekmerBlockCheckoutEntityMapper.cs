using System.Globalization;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Checkout;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerBlockCheckoutEntityMapper : BlockCheckoutEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockCheckout item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.Id", item.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}