using System;
using System.Globalization;
using System.Text;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class BlockOrderConfirmControl : BlockControlBase
	{
		private readonly IOrderSession _orderSession;
		private readonly IOrderService _orderService;
		private readonly ICountryService _countryService;

		public BlockOrderConfirmControl(
			ITemplateFactory templateFactory, IBlockService blockService,
			IOrderSession orderSession, IOrderService orderService, ICountryService countryService)
			: base(templateFactory, blockService)
		{
			_orderSession = orderSession;
			_orderService = orderService;
			_countryService = countryService;
		}

		protected override BlockContent RenderCore()
		{
			int? orderId = _orderSession.OrderId;
			if (!orderId.HasValue)
			{
				return RenderNoOrderInSession();
			}

			var order = _orderService.GetFullById(UserContext.Current, orderId.Value);
			if (order == null)
			{
				return RenderNoOrderInSession();
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Iterate:OrderItem", RenderOrderItemList(order), VariableEncoding.None);
			fragmentContent.AddEntity(order);

			return new BlockContent(
				RenderHead((ILekmerOrderFull) order), fragmentContent.Render());
		}

		private string RenderHead(ILekmerOrderFull order)
		{
			ICountry country = _countryService.GetById(order.BillingAddress.CountryId);

			Fragment fragmentHead = Template.GetFragment("Head");

			fragmentHead.AddVariable("Iterate:HeadOrderItem", RenderHeadOrderItems(order), VariableEncoding.None);

			fragmentHead.AddVariable(
				"Order.Id.Json",
				order.Id.ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddVariable(
				"Order.Number.Json",
				order.Number,
				VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddVariable(
				"Order.PriceTotal.Json",
				order.GetActualPriceTotal().IncludingVat.ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);

            decimal actualFreightCost = order.GetActualFreightCost();
		    decimal priceTotalexVatexFright = order.GetActualPriceTotal().ExcludingVat - actualFreightCost;

		    priceTotalexVatexFright = priceTotalexVatexFright - order.PaymentCost;
            fragmentHead.AddVariable(
                "Order.PriceTotalexVatexFright.Json",
                priceTotalexVatexFright.ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddVariable(
				"Order.VatTotal.Json",
				order.GetActualVatTotal().ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddVariable(
				"Order.FreightCost.Json", actualFreightCost.ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddVariable(
				"Order.BillingAddress.City.Json",
				order.BillingAddress.City.ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);
			fragmentHead.AddVariable(
				"Order.BillingAddress.Country.Json",
				country.Title.ToString(CultureInfo.InvariantCulture),
				VariableEncoding.JavaScriptStringEncode);

			return fragmentHead.Render();
		}

		private string RenderHeadOrderItems(ILekmerOrderFull order)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in order.GetOrderItems())
			{
				Fragment fragmentHeadOrderItem = Template.GetFragment("HeadOrderItem");

				fragmentHeadOrderItem.AddVariable(
					"OrderItem.ErpId.Json",
					orderItem.ErpId,
					VariableEncoding.JavaScriptStringEncode);
				fragmentHeadOrderItem.AddVariable(
					"OrderItem.Title.Json",
					orderItem.Title,
					VariableEncoding.JavaScriptStringEncode);
				fragmentHeadOrderItem.AddVariable(
					"OrderItem.Price.Json",
					orderItem.ActualPrice.IncludingVat.ToString(CultureInfo.InvariantCulture),
					VariableEncoding.JavaScriptStringEncode);
				fragmentHeadOrderItem.AddVariable(
					"OrderItem.Quantity.Json",
					orderItem.Quantity.ToString(CultureInfo.InvariantCulture),
					VariableEncoding.JavaScriptStringEncode);

				itemBuilder.AppendLine(fragmentHeadOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

		private string RenderOrderItemList(IOrderFull order)
		{
			var itemBuilder = new StringBuilder();

			foreach (IOrderItem orderItem in order.GetOrderItems())
			{
				Fragment fragmentOrderItem = Template.GetFragment("OrderItem");
				fragmentOrderItem.AddEntity(orderItem);
				itemBuilder.AppendLine(fragmentOrderItem.Render());
			}

			return itemBuilder.ToString();
		}

		private BlockContent RenderNoOrderInSession()
		{
			return new BlockContent(
				Template.GetFragment("NoOrderInSessionHead").Render(),
				Template.GetFragment("NoOrderInSession").Render());
		}
	}
}