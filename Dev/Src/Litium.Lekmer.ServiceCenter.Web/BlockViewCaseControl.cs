﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Incordia.Enterprise.CustomerService.WebService;
using Litium.Lekmer.ServiceCenter.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ServiceCenter.Web
{
    public class BlockViewCaseControl:BlockControlBase<IBlock>
    {
        private readonly IBlockService _blockService;
        private readonly IServiceCenterService _serviceCenterService;
        private readonly IFormatter _formatter;

        public BlockViewCaseControl(ITemplateFactory factory, IBlockService blockService, IServiceCenterService serviceCenterService, IFormatter formatter)
            : base(factory)
        {
            _blockService = blockService;
            _serviceCenterService = serviceCenterService;
            _formatter = formatter;
        }

        protected override IBlock GetBlockById(int blockId)
        {
            return _blockService.GetById(UserContext.Current, blockId);
        }

        protected override BlockContent RenderCore()
        {
            string head = Template.GetFragment("Head").Render();
            Fragment fragmentContent = Template.GetFragment("Content");

            var ticket = Request.QueryString["ticket"];
            if (ticket == null) throw new HttpException(404, "Not found");

            var customerCase = _serviceCenterService.GetCaseByGuid(ticket, Channel.Current.ApplicationName);
            ValidationResult validationResult = null;

            //Get case.
            if (customerCase==null)
            {
                throw new HttpException(404, "Not found");
            }
 
            var queryBuilder = new QueryBuilder(Request.QueryString);

            var shortTicket = customerCase.Ticket;
            if (IsPostBack && PostMode.Equals("customercase-reply"))
            {
                string replyMessage = Request.Form["Form.Message"];

                if(!string.IsNullOrEmpty(replyMessage))
                {
                    _serviceCenterService.AddMessageToCase(shortTicket, replyMessage);

                    queryBuilder.ReplaceOrAdd("ticket", ticket);
                    Response.Redirect("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);
                }
                else
                {
                    validationResult = new ValidationResult();
                    validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.ReplyMessageNotProvided"));
                }
            }

            string validationError = null;
            if (validationResult != null && !validationResult.IsValid)
            {
                var validationControl = new ValidationControl(validationResult.Errors);
                validationError = validationControl.Render();
            }

            string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);

            fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
            fragmentContent.AddVariable("Case.MessageList", RenderMessageList(customerCase), VariableEncoding.None);
            fragmentContent.AddVariable("Form.PostUrl", postUrl);
            fragmentContent.AddVariable("Case.Ticket", customerCase.Ticket);
            fragmentContent.AddVariable("Case.CreationDate", _formatter.FormatDateTime(Channel.Current,customerCase.CreationDate));
            fragmentContent.AddVariable("Case.Status",customerCase.Status);
            fragmentContent.AddEntity(Block);

            string content = fragmentContent.Render();

            return new BlockContent(head, content);

        }
        private string RenderMessageList(CSMCustomerCaseDetails customerCase)
        {
            StringBuilder messageBuilder = new StringBuilder();

            foreach (CSMCaseMessage message in customerCase.Messages)
            {
                Fragment fragmentMessage = Template.GetFragment("Message");
                fragmentMessage.AddVariable("Case.Message.CreationDate", _formatter.FormatDateTime(Channel.Current, message.CreationDate));
                fragmentMessage.AddVariable("Case.ReplyMessage.Text", HttpUtility.HtmlEncode(message.Message).Replace("\n", "<br />").Replace("\r", ""),VariableEncoding.None);
                fragmentMessage.AddVariable("Case.ReplyMessage.Type", message.Type);
                fragmentMessage.AddCondition("Case.ReplyMessage.IsQuestion", message.IsQuestion);

                messageBuilder.AppendLine(fragmentMessage.Render());
            }

            return messageBuilder.ToString();
        }

    }
}
