﻿using System.Collections.Specialized;
using System.Globalization;
using Litium.Lekmer.Order.Web.Validation;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;


namespace Litium.Lekmer.ServiceCenter.Web
{
    
    public class CaseForm : ControlBase
    {
        private readonly string _postUrl;

        public CaseForm(string postUrl)
		{
			_postUrl = postUrl;
		}

        public virtual bool IsFormPostBack
        {
            get { return IsPostBack && PostMode.Equals("case"); }
        }
        public virtual string PostUrl
        {
            get { return _postUrl; }
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int? CustomerId { get; set; }
        public string Message { get; set; }
        public int? OrderNumber { get; set; }

        public string CategoryId { get; set; }

        public ValidationResult Validate()
        {
            var validationResult = new ValidationResult();

            if (FirstName.IsNullOrTrimmedEmpty())
            {
                validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.FirstNameNotProvided"));
            }
            if (LastName.IsNullOrTrimmedEmpty())
            {
                validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.LastNameNotProvided"));
            }
            if (Message.IsNullOrTrimmedEmpty())
            {
                validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.MessageNotProvided"));
            }
            if (!ValidationUtil.IsValidEmail(Email))
            {
                validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.IncorrectEmail"));
            }

            //if (OrderNumber.HasValue)
            //{
            //    validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.OrderNumberNotProvided"));
            //}
            if (CategoryId.IsNullOrTrimmedEmpty())
            {
                validationResult.Errors.Add(AliasHelper.GetAliasValue("CustomerCase.Validation.CategoryNotProvided"));
            }

            return validationResult;
        }
        public void MapFromRequest()
        {
            NameValueCollection form = Request.Form;
            FirstName = form["customercase-firstname"];
            LastName = form["customercase-lastname"];
            Email = form["customercase-email"];
            CategoryId = form["customercase-categoryid"];
            Message = form["customercase-message"];
            OrderNumber = form.GetInt32OrNull("customercase-orderid");
            CustomerId = form.GetInt32OrNull("customercase-customernumber");
        }

        public virtual void MapToFragment(Fragment fragment)
        {
            fragment.AddVariable("Form.PostUrl", PostUrl);
            fragment.AddVariable("Form.FirstName.Value", FirstName);
            fragment.AddVariable("Form.LastName.Value", LastName);
            fragment.AddVariable("Form.Email.Value", Email);
            fragment.AddVariable("Form.Message.Value", Message);
            
            if (OrderNumber.HasValue)
                fragment.AddVariable("Form.OrderNumber.Value", OrderNumber.Value.ToString);
            else
                fragment.AddVariable("Form.OrderNumber.Value", "");

            if (CustomerId.HasValue)
                fragment.AddVariable("Form.CustomerNumber.Value", ((int)CustomerId).ToString);
            else
                fragment.AddVariable("Form.CustomerNumber.Value", "");
        }


    }
}
