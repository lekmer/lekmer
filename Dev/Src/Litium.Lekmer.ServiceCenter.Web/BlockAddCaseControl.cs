﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Incordia.Enterprise.CustomerService.WebService;
using Litium.Lekmer.ServiceCenter.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Lekmer.ServiceCenter.Web
{
    public class BlockAddCaseControl:BlockControlBase<IBlock>
    {
        private readonly IBlockService _blockService;
        private readonly IServiceCenterService _serviceCenterService;
        private List<CSMCategory> categoryList;
        private CaseForm _caseForm;

        private const int cDISPLAY_PRIVATE = 2;
        private const int cDISPLAY_PUBLIC = 4;

        public BlockAddCaseControl(ITemplateFactory factory, IBlockService blockService, IServiceCenterService serviceCenterService):base(factory)
        {
            _blockService = blockService;
            _serviceCenterService = serviceCenterService;
        }

        protected override IBlock GetBlockById(int blockId)
        {
            return _blockService.GetById(UserContext.Current, blockId);
        }

        protected override BlockContent RenderCore()
        {
            string head = Template.GetFragment("Head").Render();
            Fragment fragmentContent = Template.GetFragment("Content");

            ValidationResult validationResult = null;

            if (Request.QueryString["ticket"] != null)
            {
                fragmentContent.AddCondition("is_confirmed",true);
            }
            else if (CaseForm.IsFormPostBack)
            {
                CaseForm.MapFromRequest();
                validationResult = CaseForm.Validate();

                var categoryId = ConvertToCategoryId(CaseForm.CategoryId);

                if (validationResult.IsValid && categoryId != null)
                {

                    var code = _serviceCenterService.SaveCase(null, (int)categoryId, CaseForm.FirstName, CaseForm.LastName,
                                    CaseForm.Email, CaseForm.CustomerId, CaseForm.OrderNumber, "", "", CaseForm.Message, Channel.Current.Id,
                                    Channel.Current.ApplicationName);
                    RenderCaseSavedResponse(code);
                }
                fragmentContent.AddCondition("is_confirmed",false);
            }
            else
            {
                fragmentContent.AddCondition("is_confirmed", false);
            }

            return new BlockContent( head, RenderCaseForm(validationResult, fragmentContent));
        }

        #region Category list


        private List<CSMCategory> CategoryList
        {
            get
            {
                if (categoryList == null)
                {
                    const int display = cDISPLAY_PUBLIC + cDISPLAY_PRIVATE;
                    categoryList = _serviceCenterService.GetCSMCategories(display, Channel.Current.ApplicationName);
                }
                return categoryList;
            }
        }

        #endregion

        private string RenderCaseSavedResponse(string code)
        {
            var queryBuilder = new QueryBuilder(Request.QueryString);
            queryBuilder.ReplaceOrAdd("ticket", code);

            Response.Redirect("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);

            return null;
        }

        private string RenderCaseForm(ValidationResult validationResult, Fragment fragmentContent)
        {
            string validationError = null;
            if (validationResult != null && !validationResult.IsValid)
            {
                var validationControl = new ValidationControl(validationResult.Errors);
                validationError = validationControl.Render();
            }


            CaseForm.MapToFragment(fragmentContent);
            fragmentContent.AddVariable("Form.CategoryOptionsList", RenderOptionList(ConvertToCategoryId(CaseForm.CategoryId)),VariableEncoding.None);
            fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
            fragmentContent.AddEntity(Block);

            return fragmentContent.Render();
        }

        private string RenderOptionList(int? selectedCategoryId)
        {
            StringBuilder rows = new StringBuilder();

            foreach (CSMCategory category in CategoryList)
            {
                Fragment fragmentOption = Template.GetFragment("Option");
                fragmentOption.AddVariable("value", category.CategoryID.ToString());
                fragmentOption.AddVariable("text", category.Caption);

                if (selectedCategoryId.HasValue && selectedCategoryId.Value == category.CategoryID)
                {
                    fragmentOption.AddCondition("is_selected", true);
                }
                else
                {
                    fragmentOption.AddCondition("is_selected", false);
                }

                rows.AppendLine(fragmentOption.Render());
            }

            return rows.ToString();
        }


        private int? ConvertToCategoryId(string input)
        {
            int intCategoryId;
            if (!int.TryParse(input, out intCategoryId))
            {
                return null;
            }

            bool categoryExists =
                CategoryList.Exists(delegate(CSMCategory category) { return category.CategoryID == intCategoryId; });
            if (!categoryExists)
            {
                return null;
            }

            return intCategoryId;
        }

        private CaseForm CaseForm
        {
            get
            {
                if (_caseForm == null)
                {
                    var queryBuilder = new QueryBuilder(Request.QueryString);

                    string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);
                    _caseForm = new CaseForm(postUrl);
                }
                return _caseForm;
            }
        }
    }
}
