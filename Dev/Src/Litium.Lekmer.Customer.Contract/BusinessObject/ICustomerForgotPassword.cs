﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer
{
	public interface ICustomerForgotPassword : IBusinessObjectBase
	{
		int CustomerId { get; set; }
		Guid Guid { get; set; }
		DateTime CreatedDate { get; set; }
	}
}
