﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Order;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Cleaner.Service
{
	public class CartCleanerJob : BaseJob
	{
		public override string Group
		{
			get { return "Cache"; }
		}

		public override string Name
		{
			get { return "CartCleanerJob"; }
		}

		protected override void ExecuteAction()
		{
			var cartService = IoC.Resolve<ILekmerCartService>();

			cartService.DeleteExpiredItems();
		}
	}
}
