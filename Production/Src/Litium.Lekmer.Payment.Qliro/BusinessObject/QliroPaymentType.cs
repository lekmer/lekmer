namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroPaymentType : IQliroPaymentType
	{
		public int Id { get; set; }
		public int ChannelId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public decimal RegistrationFee { get; set; }
		public decimal SettlementFee { get; set; }
		public decimal InterestRate { get; set; }
		public int InterestType { get; set; }
		public QliroInterestCalculationType InterestCalculation { get; set; }
		public int NoOfMonths { get; set; }
		public decimal MinPurchaseAmount { get; set; }
	}
}