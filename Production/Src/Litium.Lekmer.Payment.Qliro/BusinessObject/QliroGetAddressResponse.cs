﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroGetAddressResponse : QliroResult, IQliroGetAddressResponse
	{
		public address[] Addresses { get; set; }
	}
}
