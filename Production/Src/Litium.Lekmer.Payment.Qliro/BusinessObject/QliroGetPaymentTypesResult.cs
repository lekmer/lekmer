﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroGetPaymentTypesResult : QliroResult, IQliroGetPaymentTypesResult
	{
		public Collection<IQliroPaymentType> PaymentTypes { get; set; }
	}
}
