﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroGetPaymentTypesResponse : QliroResult, IQliroGetPaymentTypesResponse
	{
		public paymentType[] PaymentTypes { get; set; }
	}
}
