﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Threading;
using log4net;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroManager
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private bool _isFinished;
		private readonly object _thisLock = new object();

		protected QliroReservationResponse ReservationResponse { get; set; }
		protected IQliroTransactionService QliroTransactionService { get; private set; }

		public QliroReservationRequest ReservationRequest { get; set; }

		public QliroManager(IQliroTransactionService qliroTransactionService)
		{
			QliroTransactionService = qliroTransactionService;
		}

		public virtual QliroReservationResponse ExecuteReservation()
		{
			ReservationResponse = new QliroReservationResponse();

			var thread = new Thread(Reservation)
			{
				Name = string.Format("Qliro_Order_{0}", ReservationRequest.OrderId)
			};

			thread.Start();
			thread.Join(ReservationRequest.QliroSetting.Customer7SecResponseTimeout);

			QliroReservationResponse result;

			lock (_thisLock)
			{
				result = ReservationResponse.IsCompleated ? ReservationResponse : null;
				_isFinished = true;
			}

			return result;
		}

		private void Reservation()
		{
			var duration = new Stopwatch();
			duration.Start();

			var reservationResponse = new QliroReservationResponse();

			try
			{
				ReservationRequest.CheckoutClient.InnerChannel.OperationTimeout = TimeSpan.FromMilliseconds(ReservationRequest.QliroSetting.Service7SecResponseTimeout);

				reservationResponse.ReservationReply = ReservationRequest.CheckoutClient.Reservation(
					ReservationRequest.LanguageCode,
					ReservationRequest.CivicNumber,
					ReservationRequest.PNoEncoding,
					ReservationRequest.Gender,
					ReservationRequest.Juridical,
					ReservationRequest.Amount,
					ReservationRequest.OrderId.ToString(CultureInfo.InvariantCulture),
					ReservationRequest.DeliveryAddr,
					ReservationRequest.BillingAddr,
					ReservationRequest.ClientIp,
					ReservationRequest.ProductionStatus,
					ReservationRequest.CurrencyCode,
					ReservationRequest.CountryCode,
					ReservationRequest.PreferredLanguage,
					ReservationRequest.QliroSetting.ClientRef,
					ReservationRequest.PaymentType,
					ReservationRequest.Comment,
					ReservationRequest.Email,
					ReservationRequest.CellularPhone,
					ReservationRequest.CommunicationChannel,
					ReservationRequest.InvoiceItems
				);
			}
			catch (Exception ex)
			{
				reservationResponse.ReservationException = ex;
			}

			duration.Stop();

			lock (_thisLock)
			{
				ReservationResponse.ReservationReply = reservationResponse.ReservationReply;
				ReservationResponse.ReservationException = reservationResponse.ReservationException;
				ReservationResponse.IsCompleated = true;
			}

			bool isFinished;

			lock (_thisLock)
			{
				isFinished = _isFinished;
			}

			if (isFinished)
			{
				try
				{
					SaveResponseResults(duration);
				}
				catch (Exception exception)
				{
					_log.FatalFormat("FATAL ERROR OCCURED. {0}", exception);
				}
			}
		}

		private void SaveResponseResults(Stopwatch duration)
		{
			int transactionId = QliroTransactionService.CreateReservationTimeout(
				ReservationRequest.QliroSetting.ClientRef,
				(int) ReservationRequest.ProductionStatus,
				ReservationRequest.CivicNumber,
				ReservationRequest.OrderId,
				ReservationRequest.Amount,
				ReservationRequest.CurrencyCode,
				ReservationRequest.PaymentType);

			IQliroReservationResult qliroResult;
			if (ReservationResponse.ReservationReply != null)
			{
				var reservationReply = ReservationResponse.ReservationReply;
				qliroResult = new QliroReservationResult
					{
						StatusCode = QliroTransactionStatus.Ok,
						ReturnCode = reservationReply.returnCode,
						ReservationId = reservationReply.reservationNo,
						InvoiceStatus = (QliroInvoiceStatus)reservationReply.invoiceStatus,
						CustomerMessage = reservationReply.customerMessage,
						Message = reservationReply.message,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds
					};
			}
			else
			{
				var statusCode = QliroTransactionStatus.UnspecifiedError;
				var exception = ReservationResponse.ReservationException;

				if (exception is TimeoutException)
				{
					statusCode = QliroTransactionStatus.TimeoutResponse;
				}

				qliroResult = new QliroReservationResult
					{
						StatusCode = statusCode,
						TransactionId = transactionId,
						Duration = duration.ElapsedMilliseconds,
						Message = ReservationResponse.ReservationException.ToString()
					};
			}

			QliroTransactionService.SaveReservationResponse(qliroResult);
		}
	}
}