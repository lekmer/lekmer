﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using Litium.Lekmer.Payment.Qliro.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Qliro
{
	public class QliroClient : IQliroClient
	{
		private IQliroSetting _qliroSetting;
		private IOrderItemsConverter _orderItemsConverter;
		private IInvoiceItemsValidation _invoiceItemsValidation;

		protected IQliroApi QliroApi { get; set; }

		public IQliroSetting QliroSetting
		{
			get { return _qliroSetting; }
			set
			{
				_qliroSetting = value;
				QliroApi.QliroSetting = value;
			}
		}

		public IOrderItemsConverter OrderItemsConverter
		{
			get { return _orderItemsConverter ?? (_orderItemsConverter = IoC.Resolve<IOrderItemsConverter>()); }
			set { _orderItemsConverter = value; }
		}
		public IInvoiceItemsValidation InvoiceItemsValidation
		{
			get { return _invoiceItemsValidation ?? (_invoiceItemsValidation = IoC.Resolve<IInvoiceItemsValidation>()); }
			set { _invoiceItemsValidation = value; }
		}

		public QliroClient(IQliroApi qliroApi)
		{
			QliroApi = qliroApi;
		}

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in channels Norway, Sweden and Denmark.
		/// </summary>
		public virtual IQliroGetAddressResult GetAddress(IChannel channel, string civicNumber, string ip, bool companyMode)
		{
			ValidateArgument(civicNumber);

			civicNumber = FormatCivicNumber(civicNumber, channel);

			juridicalType juridicalType = companyMode ? juridicalType.Company : juridicalType.Physical;

			IQliroGetAddressResponse response = QliroApi.GetAddress(civicNumber, juridicalType, ip, channel.Language.Iso);

			return ConvertResponseToGetAddressResult(response);
		}

		/// <summary>
		/// Gets payment types for specific channel.
		/// </summary>
		public virtual IQliroGetPaymentTypesResult GetPaymentTypes(IChannel channel)
		{
			IQliroGetPaymentTypesResponse response = QliroApi.GetPaymentTypes(channel.Language.Iso);

			return ConvertResponseToGetPaymentTypesResult(response);
		}

		/// <summary>
		/// Reserves an amount at Qliro.
		/// </summary>
		public virtual IQliroReservationResult ReserveAmount(IChannel channel, ICustomer customer, IOrderFull order, string paymentTypeId)
		{
			ValidateArguments(customer, order);

			var lekmerOrderFull = (ILekmerOrderFull)order;
			var lekmerChannel = (ILekmerChannel)channel;

			decimal totalAmount = CalculateOrderTotalAmount(lekmerOrderFull, lekmerChannel);

			string languageCode = GetLanguageCode(channel);
			string civicNumber = FormatCivicNumber(customer.CustomerInformation.CivicNumber, channel);
			pNoEncodingType pNoEncoding = GetCivicNumberEncoding(channel);
			genderType gender = GetGenterType(channel, customer);
			juridicalType juridicalType = customer.CustomerInformation.IsCompany() ? juridicalType.Company : juridicalType.Physical;
			string currencyCode = channel.Currency.Iso;
			string countryCode = channel.Country.Iso;
			string preferredLanguage = GetLanguageCode(channel);
			string cellularPhone = customer.CustomerInformation.CellPhoneNumber;
			paymentTypeId = paymentTypeId ?? string.Empty;

			address deliveryAddr = MakeAddress(channel, customer, (ILekmerOrderAddress)order.DeliveryAddress);
			address billingAddr = MakeAddress(channel, customer, (ILekmerOrderAddress)order.BillingAddress);

			invoiceItemType[] invoiceItems = MakeInvoiceItems(lekmerOrderFull, lekmerChannel);

			ValidateInvoiceItems(order.Id, invoiceItems, totalAmount);

			IQliroReservationResult result;
			if (QliroSetting.Use7SecRule)
			{
				result = QliroApi.ReserveAmountTimeout(
					languageCode,
					civicNumber,
					pNoEncoding,
					gender,
					juridicalType,
					totalAmount,
					order.Id,
					deliveryAddr,
					billingAddr,
					order.IP,
					currencyCode,
					countryCode,
					preferredLanguage,
					paymentTypeId,
					null, //comment
					order.Email,
					cellularPhone,
					invoiceItems);
			}
			else
			{
				result = QliroApi.ReserveAmount(
					languageCode,
					civicNumber,
					pNoEncoding,
					gender,
					juridicalType,
					totalAmount,
					order.Id,
					deliveryAddr,
					billingAddr,
					order.IP,
					currencyCode,
					countryCode,
					preferredLanguage,
					paymentTypeId,
					null, //comment
					order.Email,
					cellularPhone,
					invoiceItems);
			}

			return result;
		}

		public virtual IQliroGetStatusResult CheckOrderStatus(IChannel channel, int orderId, string reservationNumber)
		{
			string languageCode = channel.Language.Iso;

			IQliroGetStatusResult qliroResult = QliroApi.CheckOrderStatus(orderId, reservationNumber, languageCode);
			return qliroResult;
		}

		public virtual void Initialize(string channelCommonName)
		{
			QliroApi.Initialize(channelCommonName);
			QliroSetting.Initialize(channelCommonName);
		}

		private void ValidateArgument(string argument)
		{
			if (argument == null)
			{
				throw new ArgumentNullException("argument");
			}
		}

		private string GetLanguageCode(IChannel channel)
		{
			if (channel.Id == 4) // FI // Qliro only supports ‘sv’ and ‘en’ as language code.
			{
				return "SV";
			}

			return channel.Language.Iso;
		}

		/// <summary>
		/// Formats a civicnumber so Kreditor can accept it. In Sweden they accept "-" signs and a year format of 1950. 
		/// In Denmark, Norway and Finland they dont accept year format of 1950, here we need to remove 19. Also "-" signs
		/// are not allowed.
		/// </summary>
		private string FormatCivicNumber(string civicNumber, IChannel channel)
		{
			var countryIso = channel.Country.Iso.ToLower();
			if (string.Compare(countryIso, "se", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "fi", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber;
			}

			if (string.Compare(countryIso, "nl", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "dk", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "no", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				// Kreditor doesn't seem to like - signs.
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber.Replace("-", "");
			}

			// If country isn't supported, return input string.
			return civicNumber;
		}

		/// <summary>
		/// Converts a Qliro response item to a IAddress
		/// </summary>
		private IQliroGetAddressResult ConvertResponseToGetAddressResult(IQliroGetAddressResponse response)
		{
			var qliroAddresses = new Collection<IQliroAddress>();

			if (response.Addresses != null && response.Addresses.Length > 0)
			{
				foreach (address address in response.Addresses)
				{
					if (address != null)
					{
						qliroAddresses.Add(new QliroAddress
						{
							FirstName = address.firstName,
							LastName = address.lastName,
							StreetAddress = address.street,
							StreetAddress2 = address.coAddress,
							PostalCode = address.zipcode,
							City = address.city,
							CompanyName = address.companyName,
							IsCompany = address.companyName.HasValue()
						});
					}
				}
			}

			var result = new QliroGetAddressResult
			{
				ReturnCode = response.ReturnCode,
				Message = response.Message,
				CustomerMessage = response.CustomerMessage,
				QliroAddresses = qliroAddresses,
				Duration = response.Duration,
				TransactionId = response.TransactionId
			};

			return result;
		}

		private IQliroGetPaymentTypesResult ConvertResponseToGetPaymentTypesResult(IQliroGetPaymentTypesResponse response)
		{
			var qliroPaymentTypes = new Collection<IQliroPaymentType>();

			if (response.PaymentTypes != null && response.PaymentTypes.Length > 0)
			{
				foreach (paymentType paymentType in response.PaymentTypes)
				{
					if (paymentType != null)
					{
						qliroPaymentTypes.Add(new QliroPaymentType
						{
							Description = paymentType.description,
							Code = paymentType.code,
							RegistrationFee = paymentType.registrationFee,
							SettlementFee = paymentType.settlementFee,
							InterestRate = paymentType.interestRate,
							InterestType = paymentType.interestType,
							InterestCalculation = ConvertInterestCalculation(paymentType.interestCalculation),
							NoOfMonths = paymentType.noOfMonths,
							MinPurchaseAmount = paymentType.minPurchaseAmount
						});
					}
				}
			}

			var result = new QliroGetPaymentTypesResult
			{
				ReturnCode = response.ReturnCode,
				Message = response.Message,
				CustomerMessage = response.CustomerMessage,
				PaymentTypes = qliroPaymentTypes,
				Duration = response.Duration,
				TransactionId = response.TransactionId
			};

			return result;
		}

		private QliroInterestCalculationType ConvertInterestCalculation(interestCalculationType interestCalculation)
		{
			switch (interestCalculation)
			{
				case interestCalculationType.Flat:
					return QliroInterestCalculationType.Flat;
				case interestCalculationType.Annuity:
					return QliroInterestCalculationType.Annuity;
				default:
					throw new ArgumentOutOfRangeException("interestCalculation");
			}
		}

		private void ValidateArguments(ICustomer customer, IOrder order)
		{
			if (customer == null)
			{
				throw new ArgumentNullException("customer");
			}

			if (order == null)
			{
				throw new ArgumentNullException("order");
			}
		}

		private pNoEncodingType GetCivicNumberEncoding(IChannel channel)
		{
			switch (channel.Country.Iso.ToLower())
			{
				case "nl":
					return pNoEncodingType.Netherlands;
				case "dk":
					return pNoEncodingType.Denmark;
				case "fi":
					return pNoEncodingType.Finland;
				case "no":
					return pNoEncodingType.Norway;
				case "se":
					return pNoEncodingType.Sweden;
				default:
					throw new ArgumentException(string.Format("CountryISO: \"{0}\" not supported.", channel.Country.Iso));
			}
		}

		private genderType GetGenterType(IChannel channel, ICustomer customer)
		{
			var gender = genderType.Unknown;

			var lekmerCustomerInformation = customer.CustomerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				if (string.Compare(channel.Country.Iso.ToLower(), "nl", StringComparison.InvariantCultureIgnoreCase) == 0)
				{
					if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.Man)
					{
						gender = genderType.Male;
					}
					else if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.Woman)
					{
						gender = genderType.Female;
					}
				}
			}

			return gender;
		}

		private address MakeAddress(IChannel channel, ICustomer customer, ILekmerOrderAddress address)
		{
			string firstName;
			string lastName = string.Empty;
			string company = string.Empty;

			if (customer.CustomerInformation.IsCompany())
			{
				company = customer.CustomerInformation.LastName;
				firstName = address.Reference; // If you want to send a reference to a B2B-invoice this can be put in firstName.
			}
			else
			{
				firstName = customer.CustomerInformation.FirstName;
				lastName = customer.CustomerInformation.LastName;
			}

			var qliroAddress = new address
			{
				firstName = firstName,
				lastName = lastName,
				companyName = company,
				coAddress = null,
				street = address.StreetAddress,
				zipcode = address.PostalCode,
				city = address.City,
				countrycode = channel.Country.Iso,
				country = channel.Country.Title
			};

			return qliroAddress;
		}

		private invoiceItemType[] MakeInvoiceItems(ILekmerOrderFull lekmerOrderFull, ILekmerChannel lekmerChannel)
		{
			Collection<invoiceItemType> invoiceItems = OrderItemsConverter.Convert(lekmerOrderFull, lekmerChannel);

			return invoiceItems.ToArray();
		}

		private decimal CalculateOrderTotalAmount(ILekmerOrderFull lekmerOrderFull, ILekmerChannel lekmerChannel)
		{
			bool taxFreeZone = lekmerOrderFull.CheckForTaxFreeZone();
			Price actualPriceTotal = lekmerOrderFull.GetActualPriceTotal();
			decimal totalAmount = actualPriceTotal.IncludingVat;

			if (taxFreeZone)
			{
				totalAmount = actualPriceTotal.ExcludingVat;

				decimal vatPercentage = lekmerChannel.VatPercentage ?? 25;
				decimal freightCostInclVat = lekmerOrderFull.GetTotalFreightCost();
				decimal freightCostExclVat = PriceCalc.PriceExcludingVat1(freightCostInclVat, vatPercentage/100.0m);

				decimal freightCostVatAverage = lekmerOrderFull.GetTotalFreightCostVat(); // Calculated based on average order items VAT

				totalAmount = totalAmount - (freightCostInclVat - freightCostVatAverage) + freightCostExclVat;
			}

			return totalAmount;
		}

		private void ValidateInvoiceItems(int orderId, invoiceItemType[] invoiceItems, decimal orderTotalAmount)
		{
			InvoiceItemsValidation.Validate(orderId, invoiceItems, orderTotalAmount);
		}
	}
}