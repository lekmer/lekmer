﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Qliro.Setting
{
	public class QliroPendingIntervalSetting : SettingBase, IQliroPendingIntervalSetting
	{
		protected override string StorageName
		{
			get { return "Qliro"; }
		}

		protected virtual string GroupName
		{
			get { return "QliroPendingIntervals"; }
		}

		public virtual int OrderStatusCheckInterval
		{
			get { return GetInt32(GroupName, "OrderStatusCheckInterval", 6); }
		}

		public virtual int DecisionGuaranteedInterval
		{
			get { return GetInt32(GroupName, "DecisionGuaranteedInterval", 24); }
		}

		public virtual bool IsSendRejectMessage
		{
			get { return GetBoolean(GroupName, "IsSendRejectMessage", false); }
		}
	}
}