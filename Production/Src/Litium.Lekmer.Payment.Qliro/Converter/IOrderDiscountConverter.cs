﻿using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IOrderDiscountConverter
	{
		invoiceItemType Convert(ILekmerOrderFull lekmerOrderFull, bool taxFreeZone, decimal vatPercentage);
	}
}
