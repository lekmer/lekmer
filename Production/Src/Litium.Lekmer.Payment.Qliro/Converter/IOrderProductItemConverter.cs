﻿using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IOrderProductItemConverter
	{
		invoiceItemType Convert(ILekmerOrderItem orderItem, bool taxFreeZone);
	}
}
