﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Qliro.Contract.Checkout;
using Litium.Lekmer.Product;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Qliro
{
	public class OrderItemsConverter : IOrderItemsConverter
	{
		protected IOrderProductItemConverter OrderProductItemConverter { get; set; }
		protected IOrderPackageConverter OrderPackageConverter { get; set; }
		protected IOrderShipmentConverter OrderShipmentConverter { get; set; }
		protected IOrderDiscountConverter OrderDiscountConverter { get; set; }

		public OrderItemsConverter(
			IOrderProductItemConverter orderProductItemConverter,
			IOrderPackageConverter orderPackageConverter,
			IOrderShipmentConverter orderShipmentConverter,
			IOrderDiscountConverter orderDiscountConverter
		)
		{
			OrderProductItemConverter = orderProductItemConverter;
			OrderPackageConverter = orderPackageConverter;
			OrderShipmentConverter = orderShipmentConverter;
			OrderDiscountConverter = orderDiscountConverter;
		}

		public virtual Collection<invoiceItemType> Convert(ILekmerOrderFull lekmerOrderFull, ILekmerChannel lekmerChannel)
		{
			IEnumerable<IOrderItem> orderRowList = lekmerOrderFull.GetOrderItems();
			bool taxFreeZone = lekmerOrderFull.CheckForTaxFreeZone();
			decimal channelVatPercentage = lekmerChannel.VatPercentage ?? 25;

			var invoiceItems = new Collection<invoiceItemType>();

			foreach (ILekmerOrderItem orderRow in orderRowList)
			{
				if (ProductExtensions.IsProduct(orderRow.ProductTypeId))
				{
					invoiceItemType invoiceProductItem = OrderProductItemConverter.Convert(orderRow, taxFreeZone);
					invoiceItems.Add(invoiceProductItem);
				}
				else if (ProductExtensions.IsPackage(orderRow.ProductTypeId))
				{
					Collection<invoiceItemType> invoicePackageItems = OrderPackageConverter.Convert(orderRow, taxFreeZone);
					invoiceItems.AddRange(invoicePackageItems);
				}
				else
				{
					throw new InvalidOperationException("Unknown product type!");
				}
			}

			invoiceItemType invoiceShipmentItem = OrderShipmentConverter.Convert(lekmerOrderFull, taxFreeZone, channelVatPercentage);
			invoiceItems.Add(invoiceShipmentItem);

			invoiceItemType invoiceDiscountItem = OrderDiscountConverter.Convert(lekmerOrderFull, taxFreeZone, channelVatPercentage);
			if (invoiceDiscountItem != null)
			{
				invoiceItems.Add(invoiceDiscountItem);
			}

			return invoiceItems;
		}
	}
}
