﻿namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorAddInvoiceResult : ICollectorResult
	{
		decimal AvailableReservationAmount { get; set; }
		string InvoiceNo { get; set; }
		int InvoiceStatus { get; set; }
		string InvoiceUrl { get; set; }
	}
}
