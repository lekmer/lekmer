﻿namespace Litium.Lekmer.Payment.Collector.Setting
{
	public interface ICollectorSetting
	{
		string Url { get; }
		string UserName { get; }
		string Password { get; }
		int StoreId { get; }

		bool IgnoreCertificateValidation { get; }

		string FreightArticleNumber { get; }
		string OptionalFreightArticleNumber { get; }
		string DiapersFreightArticleNumber { get; }

		string PackageSendingMode { get; }

		bool Use7SecRule { get; }
		int DefaultResponseTimeout { get; }
		int Customer7SecResponseTimeout { get; }
		int Service7SecResponseTimeout { get; }

		void Initialize(string channelCommonName);
	}
}
