﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorTransactionType
	{
		None = 0,
		GetAddress = 1,
		AddInvoice = 2,
		InvoiceNotification = 3,
		AddInvoiceTimeout = 4,
		AddInvoiceTimeoutX2 = 5,
		AddInvoiceTimeoutX3 = 6
	}
}
