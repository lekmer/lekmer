﻿namespace Litium.Lekmer.Payment.Collector
{
	public enum CollectorTransactionStatus
	{
		Unknown = 0,
		Ok = 1,
		TimeoutResponse = 2,
		UnspecifiedError = 3,
		SoapFault = 4
	}
}
