﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorPaymentTypeService
	{
		Collection<ICollectorPaymentType> GetAllByChannel(IChannel channel);
		ICollectorPaymentType GetByCode(IChannel channel, string code);
	}
}
