﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Collector
{
	public interface ICollectorPaymentTypeSecureService
	{
		Collection<ICollectorPaymentType> GetAllByChannel(IChannel channel);

		int Save(ISystemUserFull systemUserFull, ICollectorPaymentType collectorPaymentType);
		void Delete(ISystemUserFull systemUserFull, int collectorPaymentTypeId);
	}
}
