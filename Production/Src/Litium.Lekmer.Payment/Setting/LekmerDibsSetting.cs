﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Setting
{
	public class LekmerDibsSetting : SettingBase, ILekmerDibsSetting
	{
		private string _groupName;

		protected override string StorageName
		{
			get { return "Dibs"; }
		}

		public string EncodingIso
		{
			get { return GetString(_groupName, "EncodingIso"); }
		}

		public string ResponseUrl
		{
			get { return GetString(_groupName, "ResponseUrl"); }
		}

		public string DataFormatString
		{
			get { return GetString(_groupName, "DataFormatString"); }
		}

		public bool AuthorizeOnly
		{
			get { return GetBoolean(_groupName, "AuthorizeOnly"); }
		}

		public int TestCode
		{
			get { return GetInt32(_groupName, "TestCode"); }
		}

		public bool Use3DSecure
		{
			get { return GetBoolean(_groupName, "Use3DSecure"); }
		}

		public string SecretKey
		{
			get { return GetString(_groupName, "SecretKey"); }
		}

		public string ServiceUrl
		{
			get { return GetString(_groupName, "ServiceUrl"); }
		}

		public string PageSet
		{
			get { return GetString(_groupName, "PageSet"); }
		}

		public string AquirerName
		{
			get { return GetString(_groupName, "AquirerName"); }
		}

		public string MerchantId
		{
			get { return GetString(_groupName, "MerchantId"); }
		}

		public string MacOrderIncoming
		{
			get { return GetString(_groupName, "MacOrderIncoming"); }
		}

		public bool AllowCapture
		{
			get { return GetBoolean(_groupName, "AllowCapture", false); }
		}

		public virtual void Initialize(string channelCommonName, string paymentType)
		{
			_groupName = string.Format("{0}-{1}", paymentType, channelCommonName);
		}
	}
}