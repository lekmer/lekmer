namespace Litium.Scensum.Review
{
	public interface IReviewStatus
	{
		int Id { get; set; }
		string Title { get; set; }
		string CommonName { get; set; }
	}
}