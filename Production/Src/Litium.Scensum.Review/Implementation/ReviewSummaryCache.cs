﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Scensum.Review
{
	public sealed class ReviewSummaryCache : ScensumCacheBase<ReviewSummaryKey, IReviewSummary>
	{
		#region Singleton

		private static readonly ReviewSummaryCache _instance = new ReviewSummaryCache();

		private ReviewSummaryCache()
		{
		}

		public static ReviewSummaryCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class ReviewSummaryKey : ICacheKey
	{
		public ReviewSummaryKey(int channelId, int productId)
		{
			ChannelId = channelId;
			ProductId = productId;
		}

		public int ChannelId { get; set; }

		public int ProductId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + ProductId; }
		}
	}
}