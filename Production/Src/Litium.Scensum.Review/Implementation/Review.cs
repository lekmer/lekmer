﻿using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Review
{
	[SuppressMessage("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces"), Serializable]
	public class Review : BusinessObjectBase, IReview
	{
		private int _id;
		private int _productId;
		private int _reviewStatusId;
		private int? _customerId;
		private byte _rating;
		private string _author;
		private string _message;
		private DateTime _createdDate;
		private int _channelId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int ReviewStatusId
		{
			get { return _reviewStatusId; }
			set
			{
				CheckChanged(_reviewStatusId, value);
				_reviewStatusId = value;
			}
		}

		public int? CustomerId
		{
			get { return _customerId; }
			set
			{
				CheckChanged(_customerId, value);
				_customerId = value;
			}
		}

		public byte Rating
		{
			get { return _rating; }
			set
			{
				CheckChanged(_rating, value);
				_rating = value;
			}
		}

		public string Author
		{
			get { return _author; }
			set
			{
				CheckChanged(_author, value);
				_author = value;
			}
		}

		public string Message
		{
			get { return _message; }
			set
			{
				CheckChanged(_message, value);
				_message = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}
	}
}