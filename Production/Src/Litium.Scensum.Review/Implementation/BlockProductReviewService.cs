using Litium.Scensum.Core;
using Litium.Scensum.Review.Repository;

namespace Litium.Scensum.Review
{
	public class BlockProductReviewService : IBlockProductReviewService
	{
		protected BlockProductReviewRepository Repository { get; private set; }

		public BlockProductReviewService(BlockProductReviewRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductReview GetById(IUserContext context, int id)
		{
			return BlockProductReviewCache.Instance.TryGetItem(
				new BlockProductReviewKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockProductReview GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}