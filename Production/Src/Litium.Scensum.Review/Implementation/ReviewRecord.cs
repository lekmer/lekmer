using System;

namespace Litium.Scensum.Review
{
	[Serializable]
	public class ReviewRecord : Review, IReviewRecord
	{
		private string _productTitle;
		private string _userName;

		public string ProductTitle
		{
			get { return _productTitle; }
			set
			{
				CheckChanged(_productTitle, value);
				_productTitle = value;
			}
		}

		public string UserName
		{
			get { return _userName; }
			set
			{
				CheckChanged(_userName, value);
				_userName = value;
			}
		}
	}
}