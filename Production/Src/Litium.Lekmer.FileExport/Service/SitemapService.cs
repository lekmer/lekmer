﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.FileExport.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using log4net;

namespace Litium.Lekmer.FileExport.Service
{
	public class SitemapService : ISitemapService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IContentNodeService ContentNodeService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }
		protected IFileExportSetting FileExportSetting { get; private set; }
		protected ICategoryService CategoryService { get; private set; }
		protected IBrandService BrandService { get; private set; }

		public SitemapService(IContentNodeService contentNodeService, ILekmerProductService lekmerProductService, ICategoryService categoryService, IBrandService brandService, IFileExportSetting fileExportSetting)
		{
			ContentNodeService = contentNodeService;
			LekmerProductService = lekmerProductService;
			FileExportSetting = fileExportSetting;
			CategoryService = categoryService;
			BrandService = brandService;
		}


		public virtual Collection<ISitemapItem> GetItems(IUserContext context)
		{
			_log.InfoFormat("Reading sitemap items is started. //{0}", context.Channel.CommonName);

			var sitemapItems = new Collection<ISitemapItem>();

			AppendPages(context, sitemapItems);
			AppendProducts(context, sitemapItems);
			AppendCategoryBrands(context, sitemapItems);

			_log.InfoFormat("Reading sitemap items is done. //{0}", context.Channel.CommonName);
			return sitemapItems;
		}


		/// <summary>
		/// Appends all pages to collection
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sitemapItems"></param>
		protected virtual void AppendPages(IUserContext context, Collection<ISitemapItem> sitemapItems)
		{
			_log.InfoFormat("Reading sitemap items - Pages.");

			IContentNodeTree tree = ContentNodeService.GetAllAsTree(context);

			AppendPageChildren(context, sitemapItems, tree.Root);
		}

		/// <summary>
		/// Appends all children to pages
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sitemapItems"></param>
		/// <param name="parent"></param>
		protected virtual void AppendPageChildren(IUserContext context, Collection<ISitemapItem> sitemapItems, IContentNodeTreeItem parent)
		{
			foreach (IContentNodeTreeItem item in parent.Children)
			{
				IContentNode contentNode = item.ContentNode;
				if (contentNode == null) continue;

				bool isOffline = (ContentNodeStatusInfo)contentNode.ContentNodeStatusId == ContentNodeStatusInfo.Offline;
				if (isOffline) continue;

				ISitemapItem sitemapItem = ConvertToSitemapItem(context, item, contentNode);

				if (sitemapItem != null)
				{
					sitemapItems.Add(sitemapItem);
				}

				AppendPageChildren(context, sitemapItems, item);
			}
		}

		/// <summary>
		/// Appends all products
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sitemapItems"></param>
		protected virtual void AppendProducts(IUserContext context, Collection<ISitemapItem> sitemapItems)
		{
			_log.InfoFormat("Reading sitemap items - Products. // all / all");

			int page = 1;
			int pageSize = FileExportSetting.PageSize;

			ProductCollection products = LekmerProductService.GetAll(context, page, pageSize);
			int countLeft = products.TotalCount;

			while (countLeft > 0)
			{
				products = LekmerProductService.GetAll(context, page, pageSize);

				foreach (IProduct product in products)
				{
					var lekmerProduct = (ILekmerProduct)product;

					ISitemapItem sitemapItem = ConvertToSitemapItem(context, lekmerProduct);

					sitemapItems.Add(sitemapItem);
				}

				countLeft = countLeft - pageSize;
				page++;

				_log.InfoFormat("Reading sitemap items - Products // {0} / {1}.", countLeft, products.TotalCount);
			}
		}

		protected virtual ISitemapItem ConvertToSitemapItem(IUserContext context, IContentNodeTreeItem treeItem, IContentNode contentNode)
		{
			var status = (ContentNodeStatusInfo)contentNode.ContentNodeStatusId;
			var nodeType = (ContentNodeTypeInfo)contentNode.ContentNodeTypeId;
			var access = (AccessInfo)contentNode.AccessId;
			bool isOnline = status == ContentNodeStatusInfo.Online;
			bool isPage = nodeType == ContentNodeTypeInfo.Page;
			bool isAccessibleForEveryone = access == AccessInfo.All;

			if (isPage)
			{
				var contentPage = contentNode as IContentPage;
				if (contentPage != null && contentPage.IsMaster)
				{
					return null;
				}
			}

			string url = treeItem.Url;

			if (isOnline && isPage && isAccessibleForEveryone && url.HasValue())
			{
				url = url.Substring(1); //Remove ~ in the beginning.
				if (contentNode.CommonName != null &&
					contentNode.CommonName.Equals("start", StringComparison.OrdinalIgnoreCase))
				{
					url = "/";
				}

				var sitemapItem = IoC.Resolve<ISitemapItem>();

				sitemapItem.Url = "http://" + context.Channel.ApplicationName + url;
				sitemapItem.Priority = 0.5m;
				sitemapItem.ChangeFreq = ExporterConstants.ChangeFrequencyDaily;

				return sitemapItem;
			}

			return null;
		}

		protected virtual ISitemapItem ConvertToSitemapItem(IUserContext context, ILekmerProduct lekmerProduct)
		{
			var sitemapItem = IoC.Resolve<ISitemapItem>();

			sitemapItem.Url = "http://" + context.Channel.ApplicationName + "/" + lekmerProduct.LekmerUrl;
			sitemapItem.Priority = 1;
			sitemapItem.ChangeFreq = ExporterConstants.ChangeFrequencyMonthly;

			return sitemapItem;
		}


		public virtual void AppendCategoryBrands(IUserContext context, Collection<ISitemapItem> sitemapItems)
		{
			_log.Info("Reading sitemap items - Category_Brand");
			var categoryTrees = CategoryService.GetAllAsTree(context);

			GetUrl(context, categoryTrees.Root, string.Empty, sitemapItems);

		}

		

		protected virtual void GetUrl(IUserContext context, ICategoryTreeItem category, string previousUrl, Collection<ISitemapItem> sitemapItems)
		{
			foreach (var item in category.Children)
			{
				var url = GetCategoryUrl(context, item, previousUrl);
				var brandCollection = BrandService.GetAllByCategory(context, item.Category.Id);
				var brandUrls = new Collection<string>();
				foreach (var brand in brandCollection)
				{
					if (brand.ContentNodeId != null)
					{
						var itemb = ContentNodeService.GetTreeItemById(context, brand.ContentNodeId.Value);
						if ((itemb != null)&&(!string.IsNullOrEmpty(itemb.Url)))
						{
							brandUrls.Add(RemoveLastSlash(RemoveFirstSlash(itemb.Url)));
						}
					}
				}
				foreach (var brand in brandUrls)
				{
					
						sitemapItems.Add(new SitemapItem()
						{
							Url = string.Format("{0}_{1}/", RemoveLastSlash(url), brand),
							Priority = 1,
							ChangeFreq = ExporterConstants.ChangeFrequencyMonthly
						});
					
				}
				GetUrl(context, item, url, sitemapItems);
			}
		}



		protected virtual string GetCategoryUrl(IUserContext context, ICategoryTreeItem categoryTreeItem, string previousObjectUrl)
		{
			int? contentNodeId = GetCategoryContentNodeId(context, categoryTreeItem);

			if (contentNodeId.HasValue)
			{
				string categoryUrl = GetContentNodeUrl(context, contentNodeId.Value);
				if (categoryUrl != null)
				{
					return ResolveUrlHttp(context, categoryUrl);
				}
			}

			if (string.IsNullOrEmpty(previousObjectUrl))
			{
				return string.Empty;
			}

			previousObjectUrl = RemoveLastSlash(previousObjectUrl);
			string appUrl = RemoveLastSlash(GetApplicationUrl(context));

			if (previousObjectUrl == appUrl)
			{
				return string.Empty;
			}

			string url = string.Empty;

			int position = previousObjectUrl.LastIndexOf('/');
			if (position > 0)
			{
				url = previousObjectUrl.Substring(0, position + 1);
			}

			return url;
		}

		protected virtual int? GetCategoryContentNodeId(IUserContext context, ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null || categoryTreeItem.Category == null)
			{
				return null;
			}

			if (categoryTreeItem.Category.ProductParentContentNodeId.HasValue)
			{
				return categoryTreeItem.Category.ProductParentContentNodeId.Value;
			}

			foreach (ICategoryTreeItem parentItem in categoryTreeItem.GetAncestors())
			{
				if (parentItem.Category != null && parentItem.Category.ProductParentContentNodeId.HasValue)
				{
					return parentItem.Category.ProductParentContentNodeId.Value;
				}
			}

			return null;
		}

		protected virtual string GetContentNodeUrl(IUserContext context, int contentNodeId)
		{
			var treeItem = ContentNodeService.GetTreeItemById(context, contentNodeId);
			if (treeItem == null || treeItem.Url.IsNullOrEmpty())
			{
				return null;
			}

			return treeItem.Url;
		}

		protected virtual string ResolveUrlHttp(IUserContext context, string relativeUrl)
		{
			if (relativeUrl == null) throw new ArgumentNullException("relativeUrl");

			if (!relativeUrl.StartsWith("~/", StringComparison.Ordinal))
			{
				return relativeUrl;
			}

			return GetApplicationUrl(context) + relativeUrl.Substring(1);
		}

		protected virtual string GetApplicationUrl(IUserContext context)
		{
			return "http://" + context.Channel.ApplicationName;
		}

		protected virtual string RemoveLastSlash(string url)
		{
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.Substring(0, url.Length - 1);
			}

			return url;
		}


		protected virtual string RemoveFirstSlash(string url)
		{
			if (url.StartsWith("~/", StringComparison.Ordinal))
			{
				url = url.Substring(2, url.Length - 2);
			}

			return url;
		}

	}
}
