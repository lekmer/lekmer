﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Lekmer.FileExport.Service;
using Litium.Lekmer.FileExport.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.FileExport.Exporter
{
	public class SitemapExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected ISitemapService SitemapService { get; private set; }
		protected IFileExportSetting FileExportSetting { get; private set; }

		public SitemapExporter(ILekmerChannelService lekmerChannelService, ISitemapService sitemapService, IFileExportSetting fileExportSetting)
		{
			LekmerChannelService = lekmerChannelService;
			SitemapService = sitemapService;
			FileExportSetting = fileExportSetting;
		}

		public virtual void Execute()
		{
			_log.Info("Execution is started.");

			IEnumerable<IUserContext> userContexts = GetUserContextForAllChannels();

			foreach (IUserContext context in userContexts)
			{
				if (!FileExportSetting.GetChannelNameReplacementExists(context.Channel.CommonName))
				{
					_log.Info("Skipping channel: " + context.Channel.CommonName); 
					continue;
				}

				Stopwatch stopwatch = Stopwatch.StartNew();

				var sitemapItems = SitemapService.GetItems(context);

				string filePath = GetFilePath(context);

				_log.InfoFormat("File saving... //{0}", filePath);

				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}

				using (Stream stream = File.OpenWrite(filePath))
				{
					var siteMapXmlFile = new SitemapXmlFile();
					siteMapXmlFile.Save(stream, sitemapItems);
				}

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}

			_log.Info("Executed.");
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}

		protected virtual string GetFilePath(IUserContext context)
		{
			string dirr = FileExportSetting.DestinationDirectorySitemap;
			string name = FileExportSetting.XmlFileNameSitemapExporter;

			string channelName = FileExportSetting.GetChannelNameReplacement(context.Channel.CommonName);
			name = name.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, name);
		}
	}
}
