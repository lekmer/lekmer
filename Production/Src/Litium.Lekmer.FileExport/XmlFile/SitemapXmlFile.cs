﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;

namespace Litium.Lekmer.FileExport
{
	public class SitemapXmlFile: BaseXmlFile
	{
		public void Save(Stream savedSiteMap, IEnumerable<ISitemapItem> sitemapItems)
		{
			var doc = CreateXmlDocument();

			XmlNode urlsetNode = AddRootNode(doc);

			foreach (ISitemapItem sitemapItem in sitemapItems)
			{
				AddSitemapItemNodes(doc, urlsetNode, sitemapItem);
			}

			doc.Save(savedSiteMap);
		}


		protected XmlNode AddRootNode(XmlDocument xmlDocument)
		{
			// urlset
			XmlNode urlsetNode = AddNode(xmlDocument, xmlDocument, ExporterConstants.NodeUrlSet);

			AddAttribute(xmlDocument, urlsetNode, ExporterConstants.NodeXmlns, ExporterConstants.ValueXmlns);
			AddAttribute(xmlDocument, urlsetNode, ExporterConstants.NodeExportDate, DateTime.Now.ToString(CultureInfo.InvariantCulture));

			return urlsetNode;
		}

		protected void AddSitemapItemNodes(XmlDocument xmlDocument, XmlNode parentNode, ISitemapItem sitemapItem)
		{
			// url
			XmlNode urlNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeUrl);

			// loc
			AddNode(xmlDocument, urlNode, ExporterConstants.NodeLocation, sitemapItem.Url);

			// lastmod
			if (sitemapItem.LastMod.HasValue)
			{
				AddNodeRaw(xmlDocument, urlNode, ExporterConstants.NodeLastModification, sitemapItem.LastMod.Value.ToString(CultureInfo.InvariantCulture));
			}

			// changefreq
			AddNode(xmlDocument, urlNode, ExporterConstants.NodeChangeFrequency, sitemapItem.ChangeFreq);

			// priority
			AddNodeRaw(xmlDocument, urlNode, ExporterConstants.NodePriority, sitemapItem.Priority.ToString(CultureInfo.InvariantCulture));
		}
	}
}
