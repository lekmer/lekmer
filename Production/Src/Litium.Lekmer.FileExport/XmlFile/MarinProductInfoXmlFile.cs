﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport.XmlFile
{
	public class MarinProductInfoXmlFile : BaseXmlFile
	{
		private IUserContext _context;

		private Dictionary<string, ITagGroup> _tagGroups;
		private Dictionary<int, ICampaign> _campaigns;
		private Dictionary<string, string> _categoryLevels;
		private Dictionary<string, IProductImageGroup> _imageGroups;
		private Collection<IImageSize> _imageSizes;
		private string _mediaUrl;

		private bool _isInitialized;
		private bool _isImagesInitialized;

		public void Initialize(Collection<ITagGroup> tagGroups, Collection<ICampaign> campaigns, Dictionary<string, string> categoryLevels)
		{
			if (tagGroups == null)
			{
				throw new ArgumentNullException("tagGroups");
			}

			if (campaigns == null)
			{
				throw new ArgumentNullException("campaigns");
			}

			if (categoryLevels == null)
			{
				throw new ArgumentNullException("categoryLevels");
			}

			_tagGroups = tagGroups.ToDictionary(g => g.CommonName);
			_campaigns = campaigns.ToDictionary(c => c.Id);
			_categoryLevels = categoryLevels;

			_isInitialized = true;
		}

		public void InitializeImages(Collection<IProductImageGroup> imageGroups, Collection<IImageSize> imageSizes)
		{
			if (imageGroups == null)
			{
				throw new ArgumentNullException("imageGroups");
			}
			if (imageSizes == null)
			{
				throw new ArgumentNullException("imageSizes");
			}

			_imageGroups = imageGroups.ToDictionary(g => g.CommonName);
			_imageSizes = imageSizes;
			_isImagesInitialized = true;
		}

		public void Save(Stream savedSiteMap, IUserContext context, IEnumerable<IProductExportInfo> products)
		{
			if (_isInitialized == false || _isImagesInitialized == false)
			{
				throw new InvalidOperationException("Initialize() and InitializeImages() should be called first.");
			}

			_context = context;
			_mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveExternalUrl(_context.Channel);

			var doc = CreateXmlDocument();

			XmlNode productsNode = AddProductsNode(doc);

			foreach (IProductExportInfo productExportInfo in products)
			{
				AddProductNodes(doc, productsNode, productExportInfo);
			}

			doc.Save(savedSiteMap);
		}

		protected XmlNode AddProductsNode(XmlDocument xmlDocument)
		{
			// Products
			XmlNode productsNode = xmlDocument.CreateElement(ExporterConstants.NodeProducts);

			// Products:currency
			XmlAttribute atributeCurrency = xmlDocument.CreateAttribute(ExporterConstants.NodeCurrency);
			atributeCurrency.Value = _context.Channel.Currency.Iso;
			if (productsNode.Attributes != null)
			{
				productsNode.Attributes.Append(atributeCurrency);
			}

			xmlDocument.AppendChild(productsNode);

			return productsNode;
		}

		protected void AddProductNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Product
			XmlNode productNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeProduct);
			// Product:ErpId
			AddAttribute(xmlDocument, productNode, ExporterConstants.NodeSku, productExportInfo.Product.ErpId);

			// Active (If product exists its active by default)
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeActive, ExporterConstants.NodeStatusTrue);

			// Main Category
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeMainCategory, productExportInfo.MainCategoryTitle);

			// Category
			AddNode(xmlDocument, productNode, ExporterConstants.NodeCategory, productExportInfo.ParentCategoryTitle);

			// SubCategory eg Sneakers
			AddNode(xmlDocument, productNode, ExporterConstants.NodeSubCategory, productExportInfo.CategoryTitle);

			// Manufacturer
			if (productExportInfo.Product.Brand != null && productExportInfo.Product.Brand.Title.HasValue())
			{
				string brandTitle = productExportInfo.Product.Brand.Title;
				AddNode(xmlDocument, productNode, ExporterConstants.NodeManufacturer, brandTitle);
			}

			// EAN
			string eanCode = ExporterConstants.ValueMissing;
			if (!productExportInfo.Product.EanCode.IsEmpty())
			{
				eanCode = productExportInfo.Product.EanCode;
			}
			AddNode(xmlDocument, productNode, ExporterConstants.NodeEan, eanCode);

			// ModelName
			AddNode(xmlDocument, productNode, ExporterConstants.NodeModelName, productExportInfo.Product.DisplayTitle);

			// ModelName split on "," at the request of Patric
			AddModelNameSplitNode(xmlDocument, productNode, productExportInfo);

			// Modelnumber (Supplier, Artno)
			AddSupplierNode(xmlDocument, productNode, productExportInfo);

			// ColorNode
			AddProductColorNode(xmlDocument, productNode, productExportInfo);

			bool affectedByCampaign = productExportInfo.Product.IsPriceAffectedByCampaign;

			// OnPromotion
			string promotion = affectedByCampaign ? "true" : "false";
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeOnPromotion, promotion);

			// PromotionName - CampaignNameNode
			AddCampaignNode(xmlDocument, productNode, productExportInfo);

			// Delivery time
			string deliveryTimeDays = _context.Channel.Currency.Iso.Equals("SEK") ? "1-3" : "3-5";
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeDeliveryTimeDays, deliveryTimeDays);

			// SelectMe, We want all products on campaign to be more visible
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeSelectMe, promotion);

			// PriceWithTax
			decimal priceIncludingVat = affectedByCampaign
				? productExportInfo.Product.CampaignInfo.Price.IncludingVat
				: productExportInfo.Product.Price.PriceIncludingVat;

			string priceText = priceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodePriceWithTax, priceText);

			// OriginalPriceWithTax
			string originalPriceText = productExportInfo.Product.Price.PriceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture);
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeOriginalPriceWithTax, originalPriceText);

			// InStock
			string inStock = productExportInfo.Product.TotalNumberInStock > 0 ? ExporterConstants.NodeStatusTrue : ExporterConstants.NodeStatusFalse;
			AddNodeRaw(xmlDocument, productNode, ExporterConstants.NodeInStocks, inStock);

			// Freight
			AddFreightNode(xmlDocument, productNode, productExportInfo, priceIncludingVat);

			// Description
			AddNode(xmlDocument, productNode, ExporterConstants.NodeDescription, productExportInfo.Product.Description);

			// QuantityForSale
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeQuantityForSale, productExportInfo.Product.TotalNumberInStock.ToString(CultureInfo.InvariantCulture));

			// ProductUrl
			string productUrl = "http://" + _context.Channel.ApplicationName + "/" + productExportInfo.Product.LekmerUrl;
			AddNode(xmlDocument, productNode, ExporterConstants.NodeProductUrl, productUrl);

			// CategoryUrlsNode
			AddCategoryUrlsNode(xmlDocument, productNode, productExportInfo);

			// CustomUrl3 - Brand  //brand Url
			if (productExportInfo.BrandUrl.HasValue())
			{
				AddNode(xmlDocument, productNode, ExporterConstants.NodeCustomUrl3, productExportInfo.BrandUrl);
			}

			// Images
			AddProductImagesNodes(xmlDocument, productNode, productExportInfo);

			//int salesAmount = productExportInfo.SalesAmount.HasValue ? productExportInfo.SalesAmount.Value : 0;
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeCustomNum1, salesAmount.ToString(CultureInfo.InvariantCulture));

			//int popularity = productExportInfo.Popularity.HasValue ? productExportInfo.Popularity.Value : 0;
			//AddNode(xmlDocument, productNode, ExporterConstants.NodeCustomNum2, popularity.ToString(CultureInfo.InvariantCulture));

			//<customNum1>24</customNum1> Amount of sales (last 30/90 days)
			//<customNum2>1</customNum2> Popularity ranking (last 30/90 days)
			//<customNum3>20</customNum3> Product margin %

			//+ All available tags for each product
		}

		protected virtual void AddModelNameSplitNode(XmlDocument xmlDocument, XmlNode productNode, IProductExportInfo productExportInfo)
		{
			var titles = productExportInfo.Product.DisplayTitle.Split(',');

			int i = 1;
			foreach (var title in titles)
			{
				AddNode(xmlDocument, productNode, ExporterConstants.ProductTitlekNode + i, title.Trim());
				i++;
			}
			
		}

		protected virtual void AddProductColorNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			string color = "none";

			// tag from groups
			if (productExportInfo.Product != null && productExportInfo.Product.TagGroups != null)
			{
				foreach (ITagGroup tagGroup in productExportInfo.Product.TagGroups)
				{
					if (_tagGroups.ContainsKey(tagGroup.CommonName))
					{
						if (tagGroup.Tags != null && tagGroup.Tags.Count > 0)
						{
							color = tagGroup.Tags[0].Value;
						}
					}
				}
			}

			// Color
			if (!color.Equals("none"))
			{
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeColor, color);
			}
		}

		protected virtual void AddCampaignNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			string campaignName = string.Empty;

			var campaignInfo = (ILekmerProductCampaignInfo) productExportInfo.Product.CampaignInfo;

			if (campaignInfo != null && campaignInfo.CampaignActionsApplied != null && campaignInfo.CampaignActionsApplied.Count > 0)
			{
				var campaignIds = campaignInfo.CampaignActionsApplied.GroupBy(a => a.ProductCampaignId).Select(c => c.First().ProductCampaignId);

				foreach (int campaignId in campaignIds)
				{
					if (_campaigns.ContainsKey(campaignId))
					{
						campaignName += _campaigns[campaignId].Title + ", ";
					}
				}
			}

			int lastCommaPosition = campaignName.LastIndexOf(",", StringComparison.Ordinal);
			if (lastCommaPosition > 0)
			{
				campaignName = campaignName.Substring(0, lastCommaPosition);
			}

			// CampaignName
			if (!string.IsNullOrEmpty(campaignName))
			{
				AddNode(xmlDocument, parentNode, ExporterConstants.NodePromotionName, campaignName);
			}
		}

		protected virtual void AddCategoryUrlsNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			if (_categoryLevels.ContainsKey(ExporterConstants.AllCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CategoryUrl, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}

				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.SubCategoryUrl, productExportInfo.ThirdCategoryLevelUrl); //subCategoryUrl
				}

				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CustomUrl1, productExportInfo.FirstCategoryLevelUrl); // customUrl1 - needs to be last in this feed
				}

				return;
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.SecondCategoryLevel))
			{
				if (productExportInfo.SecondCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CategoryUrl, productExportInfo.SecondCategoryLevelUrl); // categoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.ThirdCategoryLevel))
			{
				if (productExportInfo.ThirdCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.SubCategoryUrl, productExportInfo.ThirdCategoryLevelUrl); //subCategoryUrl
				}
			}

			if (_categoryLevels.ContainsKey(ExporterConstants.FirstCategoryLevel))
			{
				if (productExportInfo.FirstCategoryLevelUrl.HasValue())
				{
					AddNode(xmlDocument, parentNode, ExporterConstants.CustomUrl1, productExportInfo.FirstCategoryLevelUrl); // customUrl1 - needs to be last in this feed
				}
			}
		}

		protected virtual void AddSupplierNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			string supplierNumber = productExportInfo.Product.LekmerErpId;
			if (supplierNumber.IsEmpty())
			{
				supplierNumber = ExporterConstants.ValueMissing;
			}
			AddNode(xmlDocument, parentNode, ExporterConstants.NodeModelNumber, supplierNumber);
		}

		protected virtual void AddFreightNode(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo, decimal priceIncludingVat)
		{
			string cost = priceIncludingVat > 1000 ? "0.00" : "49.00";

			//if (_context.Channel.Currency.Iso.Equals("SEK"))
			//{
			//	cost = priceIncludingVat > 1000 ? "0.00" : "39.00"; //Schenker
			//}

			if (_context.Channel.Country.Iso.Equals("FI"))
			{
				cost = priceIncludingVat > 100 ? "0.00" : "4.90";
			}

			if (_context.Channel.Country.Iso.Equals("NL"))
			{
				cost = priceIncludingVat > 100 ? "0.00" : "2.90";
			}

			XmlNode productFreightNode = AddNodeRaw(xmlDocument, parentNode, ExporterConstants.NodeFreightCost, cost);
			AddAttribute(xmlDocument, productFreightNode, ExporterConstants.NodeCurrency, _context.Channel.Currency.Iso);
		}

		protected virtual void AddProductImagesNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productExportInfo)
		{
			// Images
			XmlNode productImagesNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeImages);

			// Default Image
			if (productExportInfo.Product.Image != null)
			{
				XmlNode imageNode = AddNode(xmlDocument, productImagesNode, ExporterConstants.NodeDefaultImage);

				AddProductImageNodes(xmlDocument, imageNode, productExportInfo.Product.Image);
			}

			// Images from groups
			if (productExportInfo.ImageGroups != null)
			{
				foreach (IProductImageGroupFull imageGroup in productExportInfo.ImageGroups)
				{
					if (_imageGroups.ContainsKey(imageGroup.CommonName))
					{
						if (imageGroup.ProductImages != null && imageGroup.ProductImages.Count > 0)
						{
							XmlNode groupNode = AddNode(xmlDocument, productImagesNode, ExporterConstants.NodeGroup);
							AddAttribute(xmlDocument, groupNode, ExporterConstants.NodeCommonName, imageGroup.CommonName);

							foreach (IProductImage productImage in imageGroup.ProductImages)
							{
								XmlNode imageNode = AddNode(xmlDocument, groupNode, ExporterConstants.NodeImage);

								AddProductImageNodes(xmlDocument, imageNode, productImage.Image);
							}
						}
					}
				}
			}

			/*
			<images>
				<default>
					<image CommonName="" Url="">
						<origin>Url<>
						<size CommonName="">Url<>
					<>
				<>
				<group CommonName="">
					<image CommonName="" Url="">
						<origin>Url<>
						<size CommonName=""><>
					<>
				<>
			<>
			*/
		}

		protected virtual void AddProductImageNodes(XmlDocument xmlDocument, XmlNode parentNode, IImage image)
		{
			string imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, image);

			AddNode(xmlDocument, parentNode, ExporterConstants.NodeOriginal, imageUrl);

			foreach (IImageSize imageSize in _imageSizes)
			{
				string imageSizeUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(_mediaUrl, image, imageSize.CommonName);

				XmlNode sizeNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeSize, imageSizeUrl);
				AddAttribute(xmlDocument, sizeNode, ExporterConstants.NodeCommonName, imageSize.CommonName);
			}

			/*
			<image CommonName="" Url="">
				<origin>Url<>
				<size CommonName=""><>
			<>
			*/
		}
	}
}
