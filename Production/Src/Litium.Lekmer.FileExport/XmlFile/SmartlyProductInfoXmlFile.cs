﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Litium.Lekmer.Common;
using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport
{
	public class SmartlyProductInfoXmlFile: BaseXmlFile
	{
		private IUserContext _context;
		private string _mediaUrl;
		
		public void Save(Stream smartlyStream, IUserContext context, IEnumerable<IProductExportInfo> products)
		{
			_context = context;
			_mediaUrl = IoC.Resolve<IMediaUrlService>().ResolveMediaArchiveExternalUrl(_context.Channel);

			var doc = CreateXmlDocument();

			XmlNode feedNode= AddFeedNode(doc);

			// title
			AddNode(doc, feedNode, ExporterConstants.NodeTitle, string.Format("{0} {1}", ExporterConstants.LekmerApplication, _context.Channel.Country.Iso));

			foreach (IProductExportInfo productExportInfo in products)
			{
				if (productExportInfo.Product.TotalNumberInStock <= 0) continue;
				AddProductNodes(doc, feedNode, productExportInfo);
			}

			doc.Save(smartlyStream);
		}

		protected virtual XmlNode AddFeedNode(XmlDocument xmlDocument)
		{
			// feed
			XmlNode feedNode = xmlDocument.CreateElement(ExporterConstants.NodeFeed);
			xmlDocument.AppendChild(feedNode);
			return feedNode;
		}

		protected virtual void AddProductNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productInfo)
		{
			var product = productInfo.Product;

			// Product
			XmlNode productNode = AddNode(xmlDocument, parentNode, ExporterConstants.NodeProduct);

			// ProductId
			AddNode(xmlDocument, productNode, ExporterConstants.NodeProductId, product.Id.ToString(CultureInfo.InvariantCulture));

			// Title
			AddCDataNode(xmlDocument, productNode, ExporterConstants.NodeTitle, product.DisplayTitle);

			// Price
			// Modify price depending on active campaign
			decimal priceIncludingVat = product.IsPriceAffectedByCampaign
				? product.CampaignInfo.Price.IncludingVat
				: product.Price.PriceIncludingVat;
			string priceText = string.Format("{0} {1}", priceIncludingVat.ToString("0.00", CultureInfo.InvariantCulture), _context.Channel.Currency.Iso);
			AddNode(xmlDocument, productNode, ExporterConstants.NodePrice, priceText);

			// Description
			AddCDataNode(xmlDocument, productNode, ExporterConstants.NodeDescription, product.Description);

			// Category
			string category = string.Format("{0} > {1} > {2}", productInfo.MainCategoryTitle, productInfo.ParentCategoryTitle, productInfo.CategoryTitle);
			AddCDataNode(xmlDocument, productNode, ExporterConstants.NodeCategory, category);

			// Images
			AddProductImagesNodes(xmlDocument, productNode, productInfo);
		}

		protected virtual void AddProductImagesNodes(XmlDocument xmlDocument, XmlNode parentNode, IProductExportInfo productInfo)
		{
			// Default Image
			if (productInfo.Product.Image != null)
			{
				string imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, productInfo.Product.Image);
				AddNode(xmlDocument, parentNode, ExporterConstants.NodeImageLink, imageUrl);
			}

			// Images from groups
			if (productInfo.ImageGroups != null)
			{
				foreach (IProductImageGroupFull imageGroup in productInfo.ImageGroups)
				{
					if (imageGroup.ProductImages == null || imageGroup.ProductImages.Count <= 0) continue;

					foreach (IProductImage productImage in imageGroup.ProductImages)
					{
						if (productInfo.Product.Image != null && productImage.Image.Id == productInfo.Product.Image.Id) continue;

						string imageUrl = MediaUrlFormer.ResolveOriginalSizeImageUrl(_mediaUrl, productImage.Image);
						AddNode(xmlDocument, parentNode, ExporterConstants.NodeImageMpa, imageUrl);
					}
				}
			}
		}
	}
}