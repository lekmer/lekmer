﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SIR.Mapper
{
	public class ProductInfoDataMapper : DataMapperBase<IProductInfo>
	{
		public ProductInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductInfo Create()
		{
			var productInfo = IoC.Resolve<IProductInfo>();

			productInfo.ArticleNumber = MapValue<string>("ArticleNumber");
			productInfo.ProductId = MapValue<int>("ProductId");
			productInfo.CategoryErpId = MapValue<string>("ProductGroup");
			productInfo.ArticleDescription = MapNullableValue<string>("ArticleDescription");
			productInfo.EanCode = MapNullableValue<string>("EanCode");
			productInfo.OurPrice = MapNullableValue<decimal?>("OurPrice");
			productInfo.ListPrice = MapNullableValue<decimal?>("ListPrice");
			productInfo.SupplierId = MapNullableValue<string>("Supplier");
			productInfo.Brand = MapNullableValue<string>("Category");
			productInfo.LekmerArtNo = MapNullableValue<string>("LekmerArtNo");
			productInfo.SupplierArtNo = MapNullableValue<string>("SupplierArtNo");
			productInfo.AveragePrice = MapNullableValue<decimal?>("AveragePrice");

			return productInfo;
		}
	}
}