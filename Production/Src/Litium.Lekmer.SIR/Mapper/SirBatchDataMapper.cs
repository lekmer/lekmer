﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.SIR.Mapper
{
	public class SirBatchDataMapper : DataMapperBase<ISirBatch>
	{
		public SirBatchDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISirBatch Create()
		{
			var batch = IoC.Resolve<ISirBatch>();

			batch.Id = MapValue<int>("SirBatchId");
			batch.ChangesFromDate = MapNullableValue<DateTime?>("ChangesFromDate");
			batch.StartedDate = MapValue<DateTime>("StartedDate");
			batch.CompletedDate = MapNullableValue<DateTime?>("CompletedDate");

			return batch;
		}
	}
}