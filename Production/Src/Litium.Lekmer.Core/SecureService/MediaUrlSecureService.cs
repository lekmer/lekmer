﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Core
{
	public class MediaUrlSecureService : IMediaUrlSecureService
	{
		protected ISubDomainSecureService SubDomainSecureService { get; private set; }

		public MediaUrlSecureService(ISubDomainSecureService subDomainSecureService)
		{
			SubDomainSecureService = subDomainSecureService;
		}


		public virtual string GetBackOfficeMediaUrl()
		{
			ISubDomain subDomain = RetrieveMediaArchiveBackOfficeSubDomain();

			string mediaUrl = subDomain != null 
				? LekmerUrlHelper.ResolveSubDomainUrl(subDomain)
				: LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);

			if (mediaUrl.StartsWith("http") == false)
			{
				mediaUrl = LekmerUrlHelper.BackOfficeBaseUrl + mediaUrl.Trim('/');
			}

			return mediaUrl.TrimEnd('/');
		}

		public virtual string ResolveMediaArchiveExternalUrl(IChannel channel)
		{
			ISubDomain mediaSubDomain = RetrieveMediaArchiveExternalSubDomain(channel);

			string mediaUrl = mediaSubDomain != null
				? LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain)
				: LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);

			// only absolute url
			if (LekmerUrlHelper.HasUriScheme(mediaUrl) == false)
			{
				mediaUrl = "http://" + channel.ApplicationName + "/" + mediaUrl.Trim('/');
			}

			return mediaUrl;
		}

		public virtual string ResolveMediaArchiveExternalUrl(IChannel channel, IImage image)
		{
			var mediaSubDomains = SubDomainSecureService.GetAllByContentType(channel, (int)ContentType.MediaArchiveExternal);

			string mediaUrl;

			if (mediaSubDomains != null && mediaSubDomains.Count > 0)
			{
				ISubDomain mediaSubDomain = mediaSubDomains[image.Id % mediaSubDomains.Count];
				mediaUrl = LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain);
			}
			else
			{
				mediaUrl = LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);
			}

			// only absolute url
			if (LekmerUrlHelper.HasUriScheme(mediaUrl) == false)
			{
				mediaUrl = "http://" + channel.ApplicationName + "/" + mediaUrl.Trim('/');
			}

			return mediaUrl;
		}


		protected virtual ISubDomain RetrieveMediaArchiveBackOfficeSubDomain()
		{
			Collection<ISubDomain> subDomains = SubDomainSecureService.GetAllByContentType((int) ContentType.MediaArchiveBackOffice);

			return subDomains.FirstOrDefault();
		}

		protected virtual ISubDomain RetrieveMediaArchiveExternalSubDomain(IChannel channel)
		{
			Collection<ISubDomain> mediaSubDomains = SubDomainSecureService.GetAllByContentType(channel, (int)ContentType.MediaArchiveExternal);

			return mediaSubDomains.FirstOrDefault();
		}
	}
}
