using System.Collections.ObjectModel;
using Litium.Lekmer.Core.Cache;
using Litium.Lekmer.Core.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public class SubDomainService : ISubDomainService
	{
		protected virtual SubDomainRepository Repository { get; private set; }

		public SubDomainService(SubDomainRepository subDomainRepository)
		{
			Repository = subDomainRepository;
		}

		public virtual Collection<ISubDomain> GetAllByContentType(IChannel channel, int contentTypeId)
		{
			return SubDomainCache.Instance.TryGetItem(
				new SubDomainKey(channel.Id, contentTypeId),
				() => Repository.GetAllByChannelAndContentType(channel, contentTypeId));
		}
	}
}