using System.Linq;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Core
{
	public class MediaUrlService : IMediaUrlService
	{
		protected ISubDomainService SubDomainService { get; set; }

		public MediaUrlService(ISubDomainService subDomainService)
		{
			SubDomainService = subDomainService;
		}


		public virtual string ResolveStaticMediaUrl(IChannel channel)
		{
			ISubDomain staticSubDomain = RetrieveStaticMediaSubDomain(channel);

			string staticUrl = staticSubDomain != null
				? LekmerUrlHelper.ResolveSubDomainUrl(staticSubDomain)
				: LekmerUrlHelper.ResolveRelativeUrl("~/media");

			return staticUrl;
		}

		public virtual string ResolveMediaArchiveUrl(IChannel channel)
		{
			ISubDomain mediaSubDomain = RetrieveMediaArchiveSubDomain(channel);

			string mediaUrl = mediaSubDomain != null
				? LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain)
				: LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);

			return mediaUrl;
		}

		public virtual string ResolveMediaArchiveUrl(IChannel channel, IMediaItem mediaItem)
		{
			var mediaSubDomains = IoC.Resolve<ISubDomainService>().GetAllByContentType(channel, (int)ContentType.MediaArchiveWeb);

			string mediaUrl;

			if (mediaSubDomains != null && mediaSubDomains.Count > 0)
			{
				ISubDomain mediaSubDomain = mediaSubDomains[mediaItem.Id % mediaSubDomains.Count];
				mediaUrl = LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain);
			}
			else
			{
				mediaUrl = LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);
			}

			return mediaUrl;
		}

		public virtual string ResolveMediaArchiveExternalUrl(IChannel channel)
		{
			ISubDomain mediaSubDomain = RetrieveMediaArchiveExternalSubDomain(channel);

			string mediaUrl = mediaSubDomain != null
				? LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain)
				: LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);

			// only absolute url
			if (LekmerUrlHelper.HasUriScheme(mediaUrl) == false)
			{
				mediaUrl = "http://" + channel.ApplicationName + "/" + mediaUrl.Trim('/');
			}

			return mediaUrl;
		}

		public virtual string ResolveMediaArchiveExternalUrl(IChannel channel, IMediaItem mediaItem)
		{
			var mediaSubDomains = IoC.Resolve<ISubDomainService>().GetAllByContentType(channel, (int)ContentType.MediaArchiveExternal);

			string mediaUrl;

			if (mediaSubDomains != null && mediaSubDomains.Count > 0)
			{
				ISubDomain mediaSubDomain = mediaSubDomains[mediaItem.Id % mediaSubDomains.Count];
				mediaUrl = LekmerUrlHelper.ResolveSubDomainUrl(mediaSubDomain);
			}
			else
			{
				mediaUrl = LekmerUrlHelper.ResolveRelativeUrl(WebSetting.Instance.MediaUrl);
			}

			// only absolute url
			if (LekmerUrlHelper.HasUriScheme(mediaUrl) == false)
			{
				mediaUrl = "http://" + channel.ApplicationName + "/" + mediaUrl.Trim('/');
			}

			return mediaUrl;
		}


		protected virtual ISubDomain RetrieveStaticMediaSubDomain(IChannel channel)
		{
			var staticSubDomains = IoC.Resolve<ISubDomainService>().GetAllByContentType(channel, (int)ContentType.StaticMedia);

			return staticSubDomains.FirstOrDefault();
		}

		protected virtual ISubDomain RetrieveMediaArchiveSubDomain(IChannel channel)
		{
			var mediaSubDomains = IoC.Resolve<ISubDomainService>().GetAllByContentType(channel, (int)ContentType.MediaArchiveWeb);

			return mediaSubDomains.FirstOrDefault();
		}

		protected virtual ISubDomain RetrieveMediaArchiveExternalSubDomain(IChannel channel)
		{
			var mediaSubDomains = IoC.Resolve<ISubDomainService>().GetAllByContentType(channel, (int)ContentType.MediaArchiveExternal);

			return mediaSubDomains.FirstOrDefault();
		}
	}
}