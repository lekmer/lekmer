﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Mapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core.Mapper
{
	public class LekmerChannelDataMapper : ChannelDataMapper
	{
		private DataMapperBase<IChannelFormatInfo> _channelFormatInfoDataMapper;
		private int _erpIdOrdinal;
		private int _vatPercentageOrdinal;

		public LekmerChannelDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_channelFormatInfoDataMapper = DataMapperResolver.Resolve<IChannelFormatInfo>(DataReader);

			_erpIdOrdinal = OrdinalOf("Channel.ErpId");
			_vatPercentageOrdinal = OrdinalOf("Channel.VatPercentage");
		}

		protected override IChannel Create()
		{
			IChannel channel = base.Create();

			var lekmerChannel = channel as ILekmerChannel;
			if (lekmerChannel != null)
			{
				lekmerChannel.FormatInfo = _channelFormatInfoDataMapper.MapRow();

				lekmerChannel.ErpId = MapNullableValue<string>(_erpIdOrdinal);
				lekmerChannel.VatPercentage = MapNullableValue<decimal?>(_vatPercentageOrdinal);

				lekmerChannel.Status = BusinessObjectStatus.Untouched;
			}

			return channel;
		}
	}
}
