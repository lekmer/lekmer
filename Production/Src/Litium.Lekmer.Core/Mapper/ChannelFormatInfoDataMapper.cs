﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core.Mapper
{
	public class ChannelFormatInfoDataMapper : DataMapperBase<IChannelFormatInfo>
	{
		private int _channelIdOrdinal;
		private int _timeFormatOrdinal;
		private int _weekDayFormatOrdinal;
		private int _dayFormatOrdinal;
		private int _dateTimeFormatOrdinal;
		private int _timezoneDiffOrdinal;

		public ChannelFormatInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_channelIdOrdinal = OrdinalOf("Channel.Id");
			_timeFormatOrdinal = OrdinalOf("Channel.TimeFormat");
			_weekDayFormatOrdinal = OrdinalOf("Channel.WeekDayFormat");
			_dayFormatOrdinal = OrdinalOf("Channel.DayFormat");
			_dateTimeFormatOrdinal = OrdinalOf("Channel.DateTimeFormat");
			_timezoneDiffOrdinal = OrdinalOf("Channel.TimeZoneDiff");
		}

		protected override IChannelFormatInfo Create()
		{
			var channelFormatInfo = IoC.Resolve<IChannelFormatInfo>();

			channelFormatInfo.ChannelId = MapNullableValue<int>(_channelIdOrdinal);
			channelFormatInfo.TimeFormat = MapNullableValue<string>(_timeFormatOrdinal);
			channelFormatInfo.WeekDayFormat = MapNullableValue<string>(_weekDayFormatOrdinal);
			channelFormatInfo.DayFormat = MapNullableValue<string>(_dayFormatOrdinal);
			channelFormatInfo.DateTimeFormat = MapNullableValue<string>(_dateTimeFormatOrdinal);
			channelFormatInfo.TimeZoneDiff = MapNullableValue<int>(_timezoneDiffOrdinal);

			channelFormatInfo.Status = BusinessObjectStatus.Untouched;
			return channelFormatInfo;
		}
	}
}
