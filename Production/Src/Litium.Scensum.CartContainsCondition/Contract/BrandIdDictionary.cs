﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Litium.Scensum.CartContainsCondition
{
	[Serializable]
	public class BrandIdDictionary : Dictionary<int, int>
	{
		public BrandIdDictionary()
		{
		}

		public BrandIdDictionary(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		public BrandIdDictionary(IEnumerable<int> brandIds)
		{
			foreach (int brandId in brandIds)
			{
				Add(brandId);
			}
		}

		public void Add(int brandId)
		{
			Add(brandId, brandId);
		}
	}
}