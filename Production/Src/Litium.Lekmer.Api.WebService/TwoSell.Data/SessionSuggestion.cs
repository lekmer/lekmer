﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[DataContract]
	[Serializable]
	public class SessionSuggestion
	{
		[DataMember]
		public string SessionId { get; set; }

		[DataMember]
		public Collection<ProductSuggestion> Products { get; set; }
	}
}
