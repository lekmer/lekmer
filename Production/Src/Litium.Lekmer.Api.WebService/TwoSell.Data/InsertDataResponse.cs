﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[DataContract]
	[Serializable]
	public class InsertDataResponse
	{
		[DataMember]
		public string Status { get; set; }

		[DataMember]
		public string Message { get; set; }
	}
}
