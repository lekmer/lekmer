﻿using System;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[DataContract]
	[Serializable]
	public class GetDataResponse
	{
		[DataMember]
		public string Status { get; set; }

		[DataMember]
		public string Message { get; set; }

		[DataMember]
		public SessionSuggestion Suggestion { get; set; }
	}
}
