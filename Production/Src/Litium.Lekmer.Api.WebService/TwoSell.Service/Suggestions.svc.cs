﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.TwoSell;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Api.WebService.TwoSell
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class Suggestions : ISuggestions
	{
		private IRecommenderListSecureService _recommenderListSecureService;
		public IRecommenderListSecureService RecommenderListSecureService
		{
			get { return _recommenderListSecureService ?? (_recommenderListSecureService = IoC.Resolve<IRecommenderListSecureService>()); }
			set { _recommenderListSecureService = value; }
		}

		public virtual InsertDataResponse InsertData(InsertDataRequest request)
		{
			Validate(request.Suggestion);

			IRecommenderList recommenderList = ConvertToReccommenderList(request.Suggestion);

			RecommenderListSecureService.Save(recommenderList);

			return new InsertDataResponse { Status = "OK" };
		}

		public virtual GetDataResponse GetData(string sessionId, string productId)
		{
			int? productIdValue = ConvertToNullableInt(productId);

			IRecommenderList recommenderList = RecommenderListSecureService.GetBySessionAndProduct(sessionId, productIdValue);
			SessionSuggestion sessionSuggestion = ConvertToSessionSuggestion(recommenderList);

			return new GetDataResponse
			{
				Status = "OK",
				Suggestion = sessionSuggestion
			};
		}


		protected virtual void Validate(SessionSuggestion suggestion)
		{
			if (suggestion == null)
			{
				throw new FaultException("The incoming 'suggestion' is null.", new FaultCode("ArgumentNull"));
			}

			if (suggestion.Products == null)
			{
				throw new FaultException("The incoming 'suggestion.Products' is null.", new FaultCode("ArgumentNull"));
			}

			bool produpes = suggestion.Products.GroupBy(p => p.ProductId).Any(p => p.Skip(1).Any());
			if (produpes)
			{
				throw new FaultException("The incoming 'suggestion' contains product duplicates.");
			}

			if (suggestion.Products.Select(ps => ps.SuggestionIds.GroupBy(p => p).Any(p => p.Skip(1).Any())).Any(pd => pd))
			{
				throw new FaultException("The incoming 'suggestion' contains suggestion duplicates.");
			}
		}


		protected virtual IRecommenderList ConvertToReccommenderList(SessionSuggestion suggestion)
		{
			IRecommenderList recommenderList = RecommenderListSecureService.Create();
			recommenderList.SessionId = suggestion.SessionId;

			foreach (ProductSuggestion productSuggestion in suggestion.Products)
			{
				IRecommenderItem recommenderItem = new RecommenderItem();
				recommenderItem.ProductId = ConvertToNullableInt(productSuggestion.ProductId);

				recommenderItem.Suggestions = new Collection<IRecommendationProduct>();

				int ordinal = 0;
				foreach (string suggestionId in productSuggestion.SuggestionIds)
				{
					IRecommendationProduct recommendationProduct = new RecommendationProduct();

					recommendationProduct.ProductId = int.Parse(suggestionId);

					ordinal += 10;
					recommendationProduct.Ordinal = ordinal;

					recommenderItem.Suggestions.Add(recommendationProduct);
				}

				recommenderList.Items.Add(recommenderItem);
			}

			return recommenderList;
		}

		protected virtual SessionSuggestion ConvertToSessionSuggestion(IRecommenderList recommenderList)
		{
			var sessionSuggestion = new SessionSuggestion();

			if (recommenderList != null)
			{
				sessionSuggestion.SessionId = recommenderList.SessionId;

				sessionSuggestion.Products = new Collection<ProductSuggestion>();

				foreach (IRecommenderItem recommenderItem in recommenderList.Items)
				{
					var productSuggestion = new ProductSuggestion();
					productSuggestion.ProductId = recommenderItem.ProductId.ToString();

					productSuggestion.SuggestionIds = new Collection<string>(
						recommenderItem.Suggestions.Select(x => x.ProductId.ToString(CultureInfo.InvariantCulture)).ToArray()
						);

					sessionSuggestion.Products.Add(productSuggestion);
				}
			}

			return sessionSuggestion;
		}

		protected virtual int? ConvertToNullableInt(string value)
		{
			if (value.IsEmpty())
			{
				return null;
			}

			int parsedValue;
			if (int.TryParse(value, out parsedValue))
			{
				return parsedValue;
			}

			return null;
		}
	}
}
