﻿namespace Litium.Lekmer.FileExport
{
	public static class ExporterConstants
	{
		public const string LekmerApplication = "Lekmer";
		public const string HeppoApplication = "Heppo";

		public const string ChangeFrequencyDaily = "daily";
		public const string ChangeFrequencyMonthly = "monthly";
		
		public const string NodeId = "id";
		public const string NodeProductId = "product_id";
		public const string NodeSize = "size";
		public const string NodeSizes = "sizes";
		public const string NodeProduct = "product";
		public const string NodeProducts = "products";
		public const string NodeExportDate = "export_date";
		public const string NodeCountry = "country";
		public const string NodeCountries = "countries";
		public const string NodeEan = "ean";
		public const string NodeBrand = "brand";
		public const string NodeTitle = "title";
		public const string NodeArtNo = "artno";
		public const string NodeModelNumber = "modelnumber";
		public const string NodeGender = "gender";
		public const string NodePrice = "price";
		public const string NodeOriginalPrice = "original_price";
		public const string NodeLink = "link";
		public const string NodeInStock = "in_stock";
		public const string NodeFreightCost = "freight_cost";
		public const string NodeDeliveryTimeDays = "delivery_time_days";
		public const string NodeDescription = "description";
		public const string NodeImage = "image";
		public const string NodeImages = "images";
		public const string NodeGroup = "group";
		public const string NodeDefaultImage = "default_image";
		public const string NodeOriginal = "original";

		public const string NodeUrl = "url";
		public const string NodeUrlSet = "urlset";
		public const string NodeLocation = "loc";
		public const string NodeLastModification = "lastmod";
		public const string NodeChangeFrequency = "changefreq";
		public const string NodePriority = "priority";

		public const string NodeIso = "iso";
		public const string NodeType = "type";
		public const string NodeCurrency = "currency";
		public const string NodeCommonName = "common_name";
		public const string NodeXmlns = "xmlns";
		public const string NodeStatusTrue = "true";
		public const string NodeStatusFalse = "false";
		public const string NodeSku = "sku";
		public const string NodeActive = "active";
		public const string NodeMainCategory = "mainCategory";
		public const string NodeCategory = "category";
		public const string NodeSubCategory = "subCategory";
		public const string NodeManufacturer = "manufacturer";
		public const string NodeModelName = "modelName";
		public const string NodeProductUrl = "productUrl";
		public const string NodeInStocks = "inStock";
		public const string NodeOnPromotion = "onPromotion";
		public const string NodeSelectMe = "selectMe";
		public const string NodeQuantityForSale = "quantityForSale";
		public const string NodePriceWithTax = "priceWithTax";
		public const string NodeOriginalPriceWithTax = "originalPriceWithTax";
		public const string NodeCustomUrl3 = "customUrl3";
		public const string NodeXmlnsKeybroker = "xmlns:xsi";
		public const string NodeXsiKeybroker = "xsi:noNamespaceSchemaLocation";
		public const string NodeCustomNum1 = "customNum1";
		public const string NodeCustomNum2 = "customNum2";

		public const string NodeColor = "color";
		public const string NodeCampaignName = "CampaignName";
		public const string NodePromotionName = "promotionName";

		public const string NoImageUrl = "media/images/lekmer/noimage250x250.png";

		public const string ValueMissing = "missing";
		public const string ValueXmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";

		public const string AllCategoryLevel = "All";
		public const string FirstCategoryLevel = "1";
		public const string SecondCategoryLevel = "2";
		public const string ThirdCategoryLevel = "3";

		public const string CategoryUrl = "categoryUrl";
		public const string SubCategoryUrl = "subCategoryUrl";
		public const string CustomUrl1 = "customUrl1";

		public const string NameNode = "name";
		public const string StockNode = "stock";
		public const string ProductTitlekNode = "ptitle";


		// Smartly
		public const string NodeFeed = "feed";
		public const string NodeImageLink = "image_link";
		public const string NodeImageMpa = "image_mpa";
	}
}
