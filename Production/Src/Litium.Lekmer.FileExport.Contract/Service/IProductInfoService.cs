﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport.Service
{
	public interface IProductInfoService
	{
		Collection<IProductExportInfo> GetCompleteProductList(IUserContext context, bool needGetCategoryUrl, bool needImages, bool needSizes);
		Collection<ITopProductListExportInfo> GetTopProductList(IUserContext context);

		Collection<IProductImageGroup> GetAllowedImageGroups();
		Collection<IImageSize> GetAllowedImageSizes();
		Collection<ITagGroup> GetAllowedTagGroups();
		Dictionary<string, string> GetAllowedCategoryLevelUrls();

		void SupplementWithPopularity(IUserContext context, Collection<IProductExportInfo> productExportInfoCollection);
	}
}