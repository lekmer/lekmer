using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Product;

namespace Litium.Lekmer.FileExport
{
	public interface IProductExportInfo
	{
		ILekmerProductView Product { get; set; }
		string CategoryTitle { get; set; }
		string ParentCategoryTitle { get; set; }
		string MainCategoryTitle { get; set; }
		Collection<IProductImageGroupFull> ImageGroups { get; set; }
		string FirstCategoryLevelUrl { get; set; }
		string SecondCategoryLevelUrl { get; set; }
		string ThirdCategoryLevelUrl { get; set; }
		int? Popularity { get; set; }
		int? SalesAmount { get; set; }
		string BrandUrl { get; set; }
	}
}