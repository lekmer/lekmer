using System;

namespace Litium.Lekmer.FileExport
{
	public interface ISitemapItem
	{
		string Url { get; set; }
		decimal Priority { get; set; }
		string ChangeFreq { get; set; }
		DateTime? LastMod { get; set; }
	}
}