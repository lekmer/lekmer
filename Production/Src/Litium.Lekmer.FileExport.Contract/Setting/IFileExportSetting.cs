﻿using System.Collections.Generic;

namespace Litium.Lekmer.FileExport.Setting
{
	public interface IFileExportSetting
	{
		string ApplicationName { get; }
		int PageSize { get; }
		string TopListFolderCommonName { get; }

		string DestinationDirectoryProduct { get; }
		string DestinationDirectorySmartly { get; }
		string DestinationDirectorySitemap { get; }
		string DestinationDirectoryTopList { get; }

		string XmlFileNameProductExporter { get; }
		string XmlFileNameSmartlyExporter { get; }
		string XmlFileNameSitemapExporter { get; }
		string XmlFileNameTopListExporter { get; }
		string XmlFileNameKeybrokerProductExporter { get; }
		string XmlFileNameMarinProductExporter { get; }
		string CsvFileNameMarinProductExporter { get; }
		string KeybrokerXmlSchemaDefinition { get; }

		string ReplaceChannelNames { get; }
		string GetChannelNameReplacement(string channelName);
		bool GetChannelNameReplacementExists(string channelName);

		ICollection<string> ImageGroups { get; }
		ICollection<string> ImageSizes { get; }
		ICollection<string> ColorTagGroup { get; }
		ICollection<string> CategoryLevelUrl { get; }

		// Marin CSV file settings
		bool IsExportXml { get; }
		bool IsExportCsv { get; }
		string CsvFileDelimiter { get; }
		string MarinFtpUrl { get; }
		string MarinFtpUsername { get; }
		string MarinFtpPassword { get; }
		int FileLifeCycleInDays { get; }
		string FileToDeleteMask { get; }

		ICollection<string> CsvFileImageGroups { get; }
		ICollection<string> CsvFileImageSizes { get; }
	}
}