﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Oculus.Web.BlockProductSimilarList
{
	public class BlockProductSimilarListControl : BlockProductPageControlBase<IBlockProductSimilarList>
	{
		private readonly IBlockProductSimilarListService _blockProductSimilarListService;
		private readonly IProductSimilarService _productSimilarService;
		private readonly IFilterProductService _filterProductService;
		private readonly IProductService _productService;
		private IPagingControl _pagingControl;
		private ProductCollection _products;

		public BlockProductSimilarListControl(ITemplateFactory templateFactory, IBlockProductSimilarListService blockProductSimilarListService, IProductSimilarService productSimilarService, IProductService productService, IFilterProductService filterProductService)
			: base(templateFactory)
		{
			_blockProductSimilarListService = blockProductSimilarListService;
			_productSimilarService = productSimilarService;
			_productService = productService;
			_filterProductService = filterProductService;
		}

		protected override IBlockProductSimilarList GetBlockById(int blockId)
		{
			return _blockProductSimilarListService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render product similar list control on this page ]");
			}

			Initialize();

			if (_products.Count <= 0)
			{
				return new BlockContent();
			}

			_pagingControl.TotalCount = _products.TotalCount;
			PagingContent pagingContent = _pagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");

			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Block.Title", Block.Title, VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);

			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}


		protected virtual void Initialize()
		{
			_pagingControl = CreatePagingControl();

			_products = GetSimilarProducts();
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderProductList()
		{
			var grid = new GridControl<IProduct>
			{
				Items = _products,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace"
			};

			return grid.Render();
		}

		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();

			pagingControl.PageBaseUrl = ResolveUrl("~" + System.Web.HttpContext.Current.Request.RelativeUrlWithoutQueryString());
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;

			pagingControl.Initialize();

			return pagingControl;
		}

		protected virtual ProductCollection GetSimilarProducts()
		{
			IProductSimilar productSimilar = _productSimilarService.GetAllByProductId(Product.Id);

			var similarProductIdCollection = new ProductIdCollection(productSimilar.SimilarProductCollection.Select(sp => sp.ProductId));

			// Get all online products.
			IEnumerable<FilterProduct> filteredProducts = _filterProductService.GetAllOnline(UserContext.Current, similarProductIdCollection);

			filteredProducts = FilterProductByPrice(filteredProducts);
			int productsCount = filteredProducts.Count();

			filteredProducts = FilterProductByPage(filteredProducts);

			ProductCollection similarProducts = _productService.PopulateProducts(UserContext.Current, new ProductIdCollection(filteredProducts.Select(p => p.Id).ToArray()));

			similarProducts.TotalCount = Block.Setting.ActualTotalItemCount <= productsCount
				? Block.Setting.ActualTotalItemCount
				: productsCount;

			return similarProducts;
		}

		protected virtual IEnumerable<FilterProduct> FilterProductByPrice(IEnumerable<FilterProduct> filterProducts)
		{
			decimal price = Product.CampaignInfo != null ? Product.CampaignInfo.Price.IncludingVat : Product.Price.PriceIncludingVat;

			switch ((PriceRangeType)Block.PriceRangeType)
			{
				case PriceRangeType.All:
					return filterProducts;
				case PriceRangeType.Lower:
					return filterProducts.Where(p => p.DiscountPrice < price);
				case PriceRangeType.LowerOrSame:
					return filterProducts.Where(p => p.DiscountPrice <= price);
				case PriceRangeType.Higher:
					return filterProducts.Where(p => p.DiscountPrice > price);
				case PriceRangeType.HigherOrSame:
					return filterProducts.Where(p => p.DiscountPrice >= price);
				default:
					throw new ArgumentOutOfRangeException(string.Format("Not supported price range {0}", (PriceRangeType)Block.PriceRangeType));
			}
		}

		protected virtual IEnumerable<FilterProduct> FilterProductByPage(IEnumerable<FilterProduct> filterProducts)
		{
			return filterProducts
				.Take(Block.Setting.ActualTotalItemCount)
				.Skip((_pagingControl.SelectedPage - 1) * _pagingControl.PageSize)
				.Take(_pagingControl.PageSize);
		}
	}
}