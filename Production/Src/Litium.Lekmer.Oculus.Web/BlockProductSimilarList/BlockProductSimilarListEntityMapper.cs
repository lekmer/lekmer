using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Oculus.Web.BlockProductSimilarList
{
	public class BlockProductSimilarListEntityMapper : LekmerBlockEntityMapper<IBlockProductSimilarList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductSimilarList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}