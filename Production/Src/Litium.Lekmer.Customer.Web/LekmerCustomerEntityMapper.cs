﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerCustomerEntityMapper : LekmerCustomerEntityMapper<ICustomer>
	{
		public LekmerCustomerEntityMapper(IFormatter formatter, ICountryService countryService)
			: base(formatter, countryService)
		{
		}
	}

	public class LekmerCustomerEntityMapper<T> : CustomerEntityMapper<T>
		where T : class, ICustomer
	{
		public LekmerCustomerEntityMapper(IFormatter formatter, ICountryService countryService)
			: base(formatter, countryService)
		{
		}

		protected override void AddAddressVariables(Fragment fragment, string prefix, IAddress address)
		{
			base.AddAddressVariables(fragment, prefix, address);

			var lekmerAddress = address as ILekmerAddress;

			if (lekmerAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerAddress.HouseExtension);
			}
		}
	}
}