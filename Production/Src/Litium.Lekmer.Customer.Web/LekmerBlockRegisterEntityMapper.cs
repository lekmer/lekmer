﻿using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	public class LekmerBlockRegisterEntityMapper : BlockRegisterEntityMapper
	{
		public override void AddEntityVariables(Fragment fragment, IBlockRegister item)
		{
			base.AddEntityVariables(fragment, item);

			LekmerBlockHelper.AddLekmerBlockVariables(fragment, item);
		}
	}
}