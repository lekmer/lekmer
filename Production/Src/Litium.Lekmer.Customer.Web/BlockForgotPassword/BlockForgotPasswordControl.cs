﻿using System;
using Litium.Scensum.Core.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using ValidationResult = Litium.Scensum.Core.ValidationResult;

namespace Litium.Lekmer.Customer.Web.BlockForgotPassword
{
	public class BlockForgotPasswordControl : BlockControlBase
	{
		private ForgotPasswordForm _forgotPasswordForm;
		private ForgotPasswordForm ForgotPasswordForm
		{
			get
			{
				return _forgotPasswordForm
					?? (_forgotPasswordForm = new ForgotPasswordForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		public BlockForgotPasswordControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		private bool WasMailSent { get; set; }

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			if (ForgotPasswordForm.IsFormPostBack)
			{
				ForgotPasswordForm.MapFromRequest();
				validationResult = ForgotPasswordForm.Validate();
				if (validationResult.IsValid)
				{
					SendConfirmation();
				}
			}

			return RenderForgotPasswordForm(validationResult);
		}

		private BlockContent RenderForgotPasswordForm(ValidationResult validationResult)
		{
			string validationError = null;

			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			ForgotPasswordForm.MapFieldsToFragment(fragmentContent);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
			fragmentContent.AddVariable("SucceedMessage", WasMailSent ? AliasHelper.GetAliasValue("Customer.ForgotPassword.SucceedMessage") : string.Empty, VariableEncoding.None);
			fragmentContent.AddCondition("WasMailSent", WasMailSent);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			ForgotPasswordForm.MapFieldsToFragment(fragment);
			return fragment.Render();
		}

		private void SendConfirmation()
		{
			var id = new ForgotPasswordMessenger().Send(ForgotPasswordForm);
			WasMailSent = !id.Equals(Guid.Empty);
		}
	}
}