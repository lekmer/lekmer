using Litium.Scensum.Customer.Web;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Customer detail form, lekmer version
	/// </summary>
	public class LekmerCustomerDetailForm : CustomerDetailForm
	{
		/// <summary>
		/// Initializes the <see cref="LekmerCustomerDetailForm"/>.
		/// </summary>
		/// <param name="customerDetailPostUrl">Url to post to.</param>
		public LekmerCustomerDetailForm(string customerDetailPostUrl) : base(customerDetailPostUrl)
		{
		}

		/// <summary>
		/// Creates an instance of <see cref="LekmerInformationForm"/>.
		/// </summary>
		/// <returns></returns>
		protected override InformationForm CreateInformationForm()
		{
			return new LekmerInformationForm();
		}

		/// <summary>
		/// Creates an instance of <see cref="LekmerAddressForm"/>.
		/// </summary>
		/// <returns></returns>
		protected override AddressForm CreateBillingAddressForm()
		{
			return new LekmerAddressForm("BillingAddress", "billingaddress");
		}

		/// <summary>
		/// Creates an instance of <see cref="LekmerAddressForm"/>.
		/// </summary>
		/// <returns></returns>
		protected override AddressForm CreateDeliveryAddressForm()
		{
			return new LekmerAddressForm("DeliveryAddress", "deliveryaddress");
		}
	}
}