using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web
{
	/// <summary>
	/// Control for editing a customer.
	/// </summary>
	public class LekmerBlockCustomerDetailControl : BlockCustomerDetailControl
	{
		/// <summary>
		/// Initializes the <see cref="LekmerBlockCustomerDetailControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="blockService"></param>
		public LekmerBlockCustomerDetailControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService)
		{
		}

		/// <summary>
		/// Creates an instance of <see cref="LekmerCustomerDetailForm"/>.
		/// </summary>
		/// <returns></returns>
		protected override CustomerDetailForm CreateCustomerDetailForm()
		{
			return new LekmerCustomerDetailForm(GetCustomerDetailFormPostUrl());
		}

		protected override BlockContent RenderCustomerDetailForm(ValidationResult validationResult)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			CustomerDetailForm.MapFieldNamesToFragment(fragmentContent);
			CustomerDetailForm.MapFieldValuesToFragment(fragmentContent);
			RenderValidationResult(fragmentContent, validationResult);
			RenderSaveSucceedNotification(fragmentContent, validationResult);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			CustomerDetailForm.MapFieldNamesToFragment(fragment);
			return fragment.Render();
		}
	}
}