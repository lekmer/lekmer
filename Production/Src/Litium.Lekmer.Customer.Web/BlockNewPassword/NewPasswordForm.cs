﻿using System.Collections.Specialized;
using System.Globalization;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Customer.Web.BlockNewPassword
{
	public class NewPasswordForm : ControlBase
	{
		private readonly string _postUrl;

		public NewPasswordForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		private static string CustomerIdFormName
		{
			get { return "newpassword-customerid"; }
		}

		private static string PasswordFormName
		{
			get { return "newpassword-password"; }
		}

		private static string PasswordVerifyFormName
		{
			get { return "newpassword-passwordverify"; }
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		private static string PostModeValue
		{
			get { return "newpassword"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string Password { get; set; }

		public string PasswordVerify { get; set; }

		public int CustomerId { get; set; }

		public ICustomer Customer { get; private set; }

		public void Clear()
		{
			Password = string.Empty;
			PasswordVerify = string.Empty;
			CustomerId = 0;
		}

		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			Password = form[PasswordFormName];
			PasswordVerify = form[PasswordVerifyFormName];
			int customerId;
			CustomerId = int.TryParse(form[CustomerIdFormName], out customerId) ? customerId : 0;
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Password.Name", PasswordFormName);
			fragment.AddVariable("Form.PasswordVerify.Name", PasswordVerifyFormName);
			fragment.AddVariable("Form.CustomerId.Name", CustomerIdFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Password.Value", Password);
			fragment.AddVariable("Form.PasswordVerify.Value", PasswordVerify);
			fragment.AddVariable("Form.CustomerId.Value", CustomerId.ToString(CultureInfo.InvariantCulture));
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();

			if (CustomerId < 1 || !GetCustomer())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.CustomerNotFound"));
			}
			else if (Password.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.PasswordEmpty"));
			}
			else if (!Password.Equals(PasswordVerify))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Customer.NewPassword.Validation.PasswordVerifyIncorrect"));
			}
			return validationResult;
		}

		private bool GetCustomer()
		{
			Customer = IoC.Resolve<ICustomerService>().GetById(UserContext.Current, CustomerId);
			return Customer != null;
		}
	}
}