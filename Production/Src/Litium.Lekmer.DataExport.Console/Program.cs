﻿using System;
using System.Reflection;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.DataExport.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main()
		{
			try
			{
				_log.InfoFormat("DataExport.Console started.");

				ExecuteDataExporters();

				_log.InfoFormat("DataExport.Console completed.");
			}
			catch (Exception ex)
			{
				_log.Error("DataExport.Console failed.", ex);
				throw;
			}
		}

		private static void ExecuteDataExporters()
		{
			var productPriceInfoExportService = IoC.Resolve<IProductPriceInfoExportService>();
			productPriceInfoExportService.ExportProductPriceInfo();

			//var categoryUrlExportService = IoC.Resolve<ICategoryUrlExportService>();
			//categoryUrlExportService.ExportCategoryUrl();
		}
	}
}