﻿namespace Litium.Lekmer.GoogleFeed
{
	public interface IGoogleFeedSetting
	{
		string ReplaceChannelNames { get; }
		string ChannelDeliveryMethod { get; }
		string DestinationDirectoryPath { get; }
		string XmlFileName { get; }
		string ZipFileExtension { get; }
		int PageSize { get; }
		string GoogleFeedTitle { get; }
		string GoogleFeedLink { get; }
		string GoogleFeedDescription { get; }

		string GetChannelNameReplacement(string channelName);
		bool GetChannelNameReplacementExists(string channelName);
		string GetChannelDeliveryMethod(string channelName);
	}
}