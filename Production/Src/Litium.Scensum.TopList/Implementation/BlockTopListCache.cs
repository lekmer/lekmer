﻿using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Scensum.TopList
{
	public sealed class BlockTopListCache : ScensumCacheBase<BlockTopListKey, IBlockTopList>
	{
		#region Singleton

		private static readonly BlockTopListCache _instance = new BlockTopListCache();

		private BlockTopListCache()
		{
		}

		public static BlockTopListCache Instance
		{
			get { return _instance; }
		}

		#endregion

		public void Remove(int blockId)
		{
			foreach (var channel in IoC.Resolve<IChannelSecureService>().GetAll())
			{
				Remove(new BlockTopListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockTopListKey : ICacheKey
	{
		public BlockTopListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}