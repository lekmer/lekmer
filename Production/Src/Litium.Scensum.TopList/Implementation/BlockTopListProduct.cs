﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	[Serializable]
	public class BlockTopListProduct : BusinessObjectBase, IBlockTopListProduct
	{
		private int _blockId;
		private IProduct _product;
		private int _position;

		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);

				_blockId = value;
			}
		}

		public IProduct Product
		{
			get { return _product; }
			set
			{
				CheckChanged(_product, value);

				_product = value;
			}
		}

		public int Position
		{
			get { return _position; }
			set
			{
				CheckChanged(_position, value);

				_position = value;
			}
		}
	}
}