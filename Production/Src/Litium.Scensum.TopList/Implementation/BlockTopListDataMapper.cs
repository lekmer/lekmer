﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.TopList.Mapper
{
	public class BlockTopListDataMapper : DataMapperBase<IBlockTopList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockTopListDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockTopList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockTopList = IoC.Resolve<IBlockTopList>();
			block.ConvertTo(blockTopList);
			blockTopList.IncludeAllCategories = MapValue<bool>("BlockTopList.IncludeAllCategories");
			blockTopList.OrderStatisticsDayCount = MapValue<int>("BlockTopList.OrderStatisticsDayCount");
			blockTopList.LinkContentNodeId = MapNullableValue<int?>("BlockTopList.LinkContentNodeId");
			blockTopList.CustomUrl = MapNullableValue<string>("BlockTopList.CustomUrl");
			blockTopList.Setting = _blockSettingDataMapper.MapRow();
			blockTopList.SetUntouched();
			return blockTopList;
		}
	}
}