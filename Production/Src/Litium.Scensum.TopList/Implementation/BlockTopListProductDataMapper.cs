using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList.Mapper
{
	public class BlockTopListProductDataMapper : DataMapperBase<IBlockTopListProduct>
	{
		private DataMapperBase<IProduct> _productDataMapper;

		public BlockTopListProductDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_productDataMapper = DataMapperResolver.Resolve<IProduct>(DataReader);
		}

		protected override IBlockTopListProduct Create()
		{
			var blockProduct = IoC.Resolve<IBlockTopListProduct>();
			blockProduct.BlockId = MapValue<int>("BlockTopListProduct.BlockId");
			blockProduct.Position = MapValue<int>("BlockTopListProduct.Position");
			blockProduct.Product = _productDataMapper.MapRow();
			blockProduct.SetUntouched();
			return blockProduct;
		}
	}
}