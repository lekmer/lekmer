﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListProductService
	{
		Collection<IBlockTopListProduct> GetAllByBlock(IUserContext context, IBlockTopList block);
	}
}