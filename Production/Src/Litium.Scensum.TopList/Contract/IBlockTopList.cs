﻿using Litium.Lekmer.SiteStructure;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopList : ILekmerBlock
	{
		bool IncludeAllCategories { get; set; }
		int OrderStatisticsDayCount { get; set; }
		int? LinkContentNodeId { get; set; }
		string CustomUrl { get; set; }
		string AliasDefaultCommonName { get; }
		IBlockSetting Setting { get; set; }
	}
}