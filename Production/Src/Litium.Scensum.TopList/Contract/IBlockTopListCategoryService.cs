using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Scensum.TopList
{
	public interface IBlockTopListCategoryService
	{
		Collection<ICategoryView> GetViewAllByBlock(IUserContext context, IBlockTopList block);
	}
}