namespace Litium.Lekmer.GFK.Contract
{
	public interface IGfkInfo
	{
		string Period { get; set; }
		string GroupNo { get; set; }
		string GroupText { get; set; }
		string ArticleText { get; set; }
		string Brand { get; set; }
		string ArticleNo { get; set; }
		string ArticleNoUniversal { get; set; }
		string ShopNo { get; set; }
		int SalesUnits { get; set; }
		decimal SalesValue { get; set; }
		int StockUnits { get; set; }
	}
}