﻿namespace Litium.Lekmer.GFK.Contract
{
	public interface IGfkSetting
	{
		string ReplaceChannelNames { get; }
		string DestinationDirectory { get; }
		string FileName { get; }
		int MaxDaysAfterPurchase { get; }
		int MinDaysAfterPurchase { get; }
		string Delimiter { get; }
		string FtpUrl { get; }
		string FtpDirectory { get; }
		int Port { get; }
		string Username { get; }
		string Password { get; }
		
		string GetChannelNameReplacement(string channelName);
		bool GetChannelNameReplacementExists(string channelName);
	}
}