﻿namespace Litium.Lekmer.GFK.Contract
{
	public interface IExporter
	{
		void Execute();
	}
}