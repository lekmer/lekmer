﻿using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.GFK.Contract
{
	public interface IGfkStatisticsExportService
	{
		string Period { get; set; }

		IEnumerable<IUserContext> GetUserContextForAllChannels();
		Dictionary<string, IGfkInfo> GetGfkInfoCollectionToExport(IUserContext context, string startDate, string endDate);
	}
}