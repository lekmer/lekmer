﻿using System.Web.Mvc;

namespace Litium.Lekmer.Order.MonitorWeb
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}