﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Timers;
using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Foundation;
using log4net;
using log4net.Config;

namespace Litium.Lekmer.InsuranceInfo.Service
{
	public partial class InsuranceInfoServiceHost : ServiceBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IInsuranceInfoExportSetting _insuranceInfoExportSetting;
		private Timer _timerWeekly;
		private Timer _timerDaily;

		protected IInsuranceInfoExportSetting InsuranceInfoExportSetting
		{
			get { return _insuranceInfoExportSetting ?? (_insuranceInfoExportSetting = IoC.Resolve<IInsuranceInfoExportSetting>()); }
		}

		public InsuranceInfoServiceHost()
		{
			InitializeComponent();

			XmlConfigurator.Configure();
			AppDomain.CurrentDomain.UnhandledException += UnhandledException;
		}


		protected override void OnStart(string[] args)
		{
			AttachDebugger();

			Exception serviceError = null;
			try
			{
				_log.Info("Starting Lekmer InsuranceInfo Service.");

				ScheduleExport();

				_log.Info("Lekmer InsuranceInfo Service started successfuly.");
			}
			catch (Exception exception)
			{
				serviceError = new Exception("Unable to start service. See inner exception for details.", exception);
			}
			finally
			{
				if (serviceError != null)
				{
					try
					{
						_log.Error("Failed to start Lekmer InsuranceInfo Service.", serviceError);
					}
					catch (Exception exception)
					{

						Trace.WriteLine("Failed to start service. An error occurred:");
						Trace.WriteLine(serviceError.ToString());
						Trace.WriteLine("Failed to log error:");
						Trace.WriteLine(exception.ToString());
					}
					finally
					{
						Stop();
					}
				}
			}
		}

		protected override void OnStop()
		{
			try
			{
				if (_timerDaily != null)
				{
					_timerDaily.Stop();
				}

				if (_timerWeekly != null)
				{
					_timerWeekly.Stop();
				}

				_log.Info("Lekmer InsuranceInfo Service stopped successfuly.");

				base.OnStop();
			}
			catch (Exception exception)
			{
				_log.Error("Error occurred while stopping Lekmer InsuranceInfo Service.", exception);
			}
		}

		protected virtual void UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			try
			{
				if (_timerDaily != null)
				{
					_timerDaily.Stop();
				}

				if (_timerWeekly != null)
				{
					_timerWeekly.Stop();
				}

				_log.Error("ERROR occurred during Lekmer InsuranceInfo Service execution. Stopping service", e.ExceptionObject is Exception ? e.ExceptionObject as Exception : null);
			}
			finally
			{
				Stop();
			}
		}


		protected virtual void ScheduleExport()
		{
			if (InsuranceInfoExportSetting.UseWeeklyExecution)
			{
				_timerWeekly = new Timer();

				_timerWeekly.Elapsed += OnTimerWeeklyElapsed;
				_timerWeekly.Interval = CalculateIntervalToNextWeeklyExecution();
				_timerWeekly.AutoReset = false;

				_timerWeekly.Start();
			}

			if (InsuranceInfoExportSetting.UseDailyExecution)
			{
				_timerDaily = new Timer();

				_timerDaily.Elapsed += OnTimerDailyElapsed;
				_timerDaily.Interval = CalculateIntervalToNextDailyExecution();
				_timerDaily.AutoReset = false;

				_timerDaily.Start();
			}
		}
		protected virtual void OnTimerWeeklyElapsed(object sender, ElapsedEventArgs e)
		{
			ExecuteExport();

			if (InsuranceInfoExportSetting.UseWeeklyExecution)
			{
				_timerWeekly.Interval = CalculateIntervalToNextWeeklyExecution();
				_timerWeekly.Start();
			}
		}
		protected virtual void OnTimerDailyElapsed(object sender, ElapsedEventArgs e)
		{
			ExecuteExport();

			if (InsuranceInfoExportSetting.UseDailyExecution)
			{
				_timerDaily.Interval = CalculateIntervalToNextDailyExecution();
				_timerDaily.Start();
			}
		}
		protected virtual int CalculateIntervalToNextWeeklyExecution()
		{
			TimeSpan delay;

			var timeFrom = DateTime.Now;

			DateTime timeTodayExecution = timeFrom.Date
				.AddHours(Math.Abs(InsuranceInfoExportSetting.WeeklyExecutionHour))
				.AddMinutes(Math.Abs(InsuranceInfoExportSetting.WeeklyExecutionMinute));

			var timeNextExecutions = new Collection<DateTime>();

			if (InsuranceInfoExportSetting.WeeklyExecutionMonday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Monday));
			}
			if (InsuranceInfoExportSetting.WeeklyExecutionTuesday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Tuesday));
			}
			if (InsuranceInfoExportSetting.WeeklyExecutionWednesday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Wednesday));
			}
			if (InsuranceInfoExportSetting.WeeklyExecutionThursday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Thursday));
			}
			if (InsuranceInfoExportSetting.WeeklyExecutionFriday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Friday));
			}
			if (InsuranceInfoExportSetting.WeeklyExecutionSaturday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Saturday));
			}
			if (InsuranceInfoExportSetting.WeeklyExecutionSunday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Sunday));
			}

			if (timeNextExecutions.Count == 0)
			{
				if (timeTodayExecution < timeFrom)
				{
					timeTodayExecution = timeTodayExecution.AddDays(1);
				}

				delay = timeTodayExecution - timeFrom;
			}
			else
			{
				delay = timeNextExecutions.Min() - timeFrom;
			}

			_log.Info("Time to next weekly execution: " + delay);

			return (int)delay.TotalMilliseconds;
		}

		protected virtual int CalculateIntervalToNextDailyExecution()
		{
			var timeFrom = DateTime.Now;

			DateTime timeX = timeFrom.Date
				.AddHours(Math.Abs(InsuranceInfoExportSetting.DailyExecutionHour))
				.AddMinutes(Math.Abs(InsuranceInfoExportSetting.DailyExecutionMinute));

			// If it's already past, wait until tomorrow.
			if (timeFrom > timeX)
			{
				timeX = timeX.AddDays(1);
			}

			TimeSpan delay = timeX - timeFrom;

			_log.Info("Time to next daily execution: " + delay);

			return (int)delay.TotalMilliseconds;
		}

		protected virtual void ExecuteExport()
		{
			try
			{
				var exporter = IoC.Resolve<IExporter>("InsuranceInfoExporter");
				exporter.Execute();

				_log.Info("Lekmer InsuranceInfo finished successfuly.");
			}
			catch (Exception exception)
			{
				_log.Error("Lekmer InsuranceInfo failed.", exception);
			}
		}

		protected virtual DateTime NextWeeklyExecutionDay(DateTime timeFrom, DateTime timeTodayExecution, DayOfWeek dayOfWeek)
		{
			int days = dayOfWeek - timeTodayExecution.DayOfWeek;
			if (days < 0)
			{
				days += 7;
			}

			DateTime timeNextExecution = timeTodayExecution.AddDays(days);

			if (timeNextExecution < timeFrom)
			{
				timeNextExecution = timeNextExecution.AddDays(7);
			}

			return timeNextExecution;
		}

		[Conditional("DEBUG")]
		private void AttachDebugger()
		{
			Debugger.Break();
		}
	}
}