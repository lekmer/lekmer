﻿namespace Litium.Lekmer.Esales.Setting
{
	public interface IAutoCompletePanelSetting : IEsalesPanelSetting
	{
		string AutoCompletePanelName { get; }
	}
}
