﻿namespace Litium.Lekmer.Esales
{
	public static class EsalesArguments
	{
		public const string Filter = "filter";
		public const string SearchAttributes = "search_attributes";
		public const string SearchPhrase = "search_phrase";
		public const string SearchPrefix = "search_prefix";
		public const string FilterPhrase = "filter_phrase";
		public const string SortBy = "sort_by";
		public const string WindowFirst = "window_first";
		public const string WindowLast = "window_last";
		public const string Products = "products";
		public const string CustomerKey = "customer_key";
		public const string ProductKey = "product_key";
		public const string Cart = "cart";
		public const string AdFilter = "ad_filter";
		public const string Site = "site";
		public const string MaxAds = "max_ads";
		public const string SearchFilter = "s_filter";
		public const string FilterNoStock = "filter_no_stock";
	}
}