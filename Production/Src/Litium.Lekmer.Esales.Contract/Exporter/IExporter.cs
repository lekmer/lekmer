﻿namespace Litium.Lekmer.Esales.Exporter
{
	public interface IExporter
	{
		void Execute();
	}
}
