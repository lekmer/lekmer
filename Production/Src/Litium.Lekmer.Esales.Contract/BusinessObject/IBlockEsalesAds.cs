﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesAds : IBlock
	{
		string PanelPath { get; set; }
		string Format { get; set; }
		string Gender { get; set; }
		string ProductCategory { get; set; }
		IBlockSetting Setting { get; set; }
	}
}