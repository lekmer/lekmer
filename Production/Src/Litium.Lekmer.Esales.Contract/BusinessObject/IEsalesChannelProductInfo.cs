using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesChannelProductInfo
	{
		IChannel Channel { get; set; }

		ILekmerProductView Product { get; set; }

		ICategory Category { get; set; }
		ICategory ParentCategory { get; set; }
		ICategory MainCategory { get; set; }
		Collection<ICampaign> Campaigns { get; set; }
	}
}