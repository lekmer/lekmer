﻿using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesRecommend : ILekmerBlock
	{
		int? RecommendationType { get; set; }
		string PanelPath { get; set; }
		string FallbackPanelPath { get; set; }
		IBlockSetting Setting { get; set; }
	}
}
