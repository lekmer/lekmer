﻿namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesControl
	{
		/// <summary>
		/// Esales panel request details
		/// </summary>
		IEsalesPanelRequest EsalesPanelRequest { get; }

		/// <summary>
		/// Esales panel request details
		/// </summary>
		IEsalesResponse EsalesResponse { get; set; }
	}
}
