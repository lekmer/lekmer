﻿namespace Litium.Lekmer.Esales
{
	public interface IEsalesModelParameter
	{
		string Name { get; set; }
		string Value { get; set; }
	}
}