﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesSetting : IBusinessObjectBase
	{
		int BlockId { get; set; }
		int? EsalesModelComponentId { get; set; }
	}
}