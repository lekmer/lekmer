using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesTopSellersCategoryV2 : IBusinessObjectBase
	{
		int BlockId { get; set; }
		ICategory Category { get; set; }
		bool IncludeSubcategories { get; set; }
	}
}