﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesCartService
	{
		ProductCollection FindRecommend(IUserContext context, string panelPath, int productId, ProductIdCollection cartProductIds, int itemsToReturn);
	}
}
