using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesProductListService
	{
		IBlockEsalesProductList GetById(IUserContext context, int blockId);
	}
}