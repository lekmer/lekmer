﻿using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesRecommendService
	{
		ProductCollection FindRecommend(IUserContext context, string panelPath, string fallbackPanelPath, string customerKey, int productId, ProductIdCollection cartProductIds, int itemsToReturn);
	}
}