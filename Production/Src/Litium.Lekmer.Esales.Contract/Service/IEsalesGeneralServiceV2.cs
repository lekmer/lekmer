﻿using System.Collections.Generic;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesGeneralServiceV2
	{
		object GetResults(string panelPath, Dictionary<string, string> parameters);
		ProductCollection FindProducts(IUserContext context, IPanelContent esalesResponse, string panelName, int returnItemsCount);
		string FindTicket(IEsalesResponse esalesResponse, string panelName);
	}
}