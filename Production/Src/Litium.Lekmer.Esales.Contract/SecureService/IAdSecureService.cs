﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IAdSecureService
	{
		IAd Create();

		IAd Save(ISystemUserFull systemUserFull, IAd ad);

		void Delete(ISystemUserFull systemUserFull, int adId);

		IAd GetById(int adId);

		Collection<IAd> GetAll();

		Collection<IAd> GetAllByFolder(int folderId);

		bool IsUniqueKey(int? adId, string key);

		Collection<IAd> Search(string searchCriteria);
	}
}