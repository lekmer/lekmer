using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	public interface IBlockEsalesRecommendSecureServiceV2
	{
		IBlockEsalesRecommendV2 Create();

		IBlockEsalesRecommendV2 GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockEsalesRecommendV2 block);
	}
}