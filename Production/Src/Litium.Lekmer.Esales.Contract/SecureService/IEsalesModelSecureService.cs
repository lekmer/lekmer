﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesModelSecureService
	{
		Collection<IEsalesModelComponent> GetModelComponentsAllByBlockType(int blockTypeId);
	}
}
