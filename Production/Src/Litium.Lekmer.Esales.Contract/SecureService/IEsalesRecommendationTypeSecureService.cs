using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IEsalesRecommendationTypeSecureService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IEsalesRecommendationType> GetAll();
	}
}