using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IProductHits : IResult
	{
		Collection<IProductHitInfo> ProductsInfo { get; set; }
	}
}