﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Lekmer.Esales;

namespace Litium.Lekmer.Esales
{
	public interface ICartEsalesRequest : IEsalesRequest
	{
		string ProductKey { get; set; }
		string CartProductIds { get; set; }
		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		IPaging Paging { get; set; }

	}
}
