using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it.
	/// </summary>
	public interface IRecommendEsalesRequest : IEsalesRequest
	{
		string ChannelId { get; set; }
		string CustomerKey { get; set; }
		string ProductKey { get; set; }
		string CartProductIds { get; set; }
		List<int> ExcludeProductIds { get; set; }
		List<int> IncludeCategoryIds { get; set; }
		List<int> IncludeParentCategoryIds { get; set; }
		List<int> IncludeMainCategoryIds { get; set; }

		/// <summary>
		/// Limits the result to a defined page.
		/// </summary>
		IPaging Paging { get; set; }
	}
}