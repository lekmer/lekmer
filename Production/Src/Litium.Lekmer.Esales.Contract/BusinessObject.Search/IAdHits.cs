using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface IAdHits : IResult
	{
		Collection<IAdInfo> AdsInfo { get; set; }
	}
}