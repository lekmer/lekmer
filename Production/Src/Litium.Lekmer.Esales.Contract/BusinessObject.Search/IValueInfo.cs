namespace Litium.Lekmer.Esales
{
	public interface IValueInfo
	{
		int Count { get; set; }
		string Text { get; set; }
		string Ticket { get; set; }
	}
}