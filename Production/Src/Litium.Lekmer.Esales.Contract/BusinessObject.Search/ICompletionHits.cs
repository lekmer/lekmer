using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface ICompletionHits : IResult
	{
		Collection<ICompletionInfo> CompletionsInfo { get; set; }
	}
}