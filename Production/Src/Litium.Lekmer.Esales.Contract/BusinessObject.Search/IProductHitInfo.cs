namespace Litium.Lekmer.Esales
{
	public interface IProductHitInfo
	{
		int Rank { get; set; }
		string Key { get; set; }
		string Ticket { get; set; }
	}
}