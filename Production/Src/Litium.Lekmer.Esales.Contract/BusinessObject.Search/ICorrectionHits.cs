using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	public interface ICorrectionHits : IResult
	{
		Collection<ICorrectionInfo> CorrectionsInfo { get; set; }
	}
}