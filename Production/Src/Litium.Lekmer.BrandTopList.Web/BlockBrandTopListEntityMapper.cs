using System.Globalization;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.BrandTopList.Web
{
	public class BlockBrandTopListEntityMapper : LekmerBlockEntityMapper<IBlockBrandTopList>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockBrandTopList item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}