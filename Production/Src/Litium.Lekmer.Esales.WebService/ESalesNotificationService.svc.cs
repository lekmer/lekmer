﻿using System;
using System.Reflection;
using System.ServiceModel.Activation;
using System.Web;
using Apptus.ESales.Connector;
using Apptus.Util.Http;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Esales.WebService
{
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	public class ESalesNotificationService : IESalesNotificationService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private  IEsalesSetting _eSalesSetting;

		protected IEsalesSetting EsalesSetting
		{
			get { return _eSalesSetting ?? (_eSalesSetting = IoC.Resolve<IEsalesSetting>()); }
		}

		public string Notify(string type, string ticket, string productKey, string rating, string id)
		{
			if (!EsalesSetting.Enabled)
			{
				return "Service is disabled, check settings.";
			}

			if (type.Equals("click", StringComparison.OrdinalIgnoreCase) || type.Equals("adding_to_cart", StringComparison.OrdinalIgnoreCase))
			{
				if (string.IsNullOrEmpty(ticket))
				{
					_log.Warn("Illegal argument exception: '" + type + "' " + LogRequestInfo());

					return "Argument ticket is mandatory";
				}
			}

			Apptus.ESales.Connector.Connector connector = (Apptus.ESales.Connector.Connector)HttpContext.Current.Application["_esales_connector"];
			if (connector == null)
			{
				string host = EsalesSetting.ControllerUrl;
				if (host == null)
				{
					throw new InvalidOperationException("ESales URL not set");
				}

				connector = OnPremConnector.GetOrCreate(host);
				HttpContext.Current.Application["_esales_connector"] = connector;
			}

			string sessionKey = (string)HttpContext.Current.Session["_esales_session_key"];
			// Default value for the session is the id from the Session. 
			// It can be overridden by a Session attribute.
			if (string.IsNullOrEmpty(sessionKey))
			{
				sessionKey = HttpContext.Current.Session.SessionID;
				HttpContext.Current.Session["_esales_session_key"] = sessionKey;
			}

			try
			{
				JavaScriptNotifier.Notify(connector, sessionKey, id, type, ticket, productKey, rating);
			}
			catch (IllegalArgumentException iae)
			{
				_log.Error("Illegal argument exception: " + iae.Message + LogRequestInfo());
				return iae.Message;
			}
			catch (ClusterUnavailableException cue)
			{
				Exception inner = cue.InnerException;

				string errorMessage;

				if (inner != null && inner is RequestFailedException)
				{
					errorMessage = inner.Message + ((RequestFailedException)inner).ResponseBody();
				}
				else
				{
					errorMessage = cue.Message;
				}

				_log.Error("Cluster unavailable exception: " + errorMessage + LogRequestInfo());
				return errorMessage;
			}
			catch (RequestFailedException ex)
			{
				string errorMessage = ex.Message + ex.ResponseBody();
				_log.Error("Request failed exception: " + errorMessage + LogRequestInfo());
				return errorMessage;
			}
			catch (Exception ex)
			{
				_log.Error("Unhandled exception" + LogRequestInfo(), ex);
				throw;
			}

			return "Success";
		}

		protected string LogRequestInfo()
		{
			string requestInfo = "; //Url: " + HttpContext.Current.Request.Url.AbsoluteUri;

			if (HttpContext.Current.Request.UrlReferrer != null)
			{
				requestInfo += "; //Referrer: " + HttpContext.Current.Request.UrlReferrer.AbsoluteUri;
			}

			return requestInfo;
		}
	}
}