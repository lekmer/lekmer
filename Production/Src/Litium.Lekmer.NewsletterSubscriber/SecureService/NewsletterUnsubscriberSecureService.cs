﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterUnsubscriberSecureService : INewsletterUnsubscriberSecureService
	{
		protected NewsletterUnsubscriberRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected INewsletterUnsubscriberOptionSecureService NewsletterUnsubscriberOptionSecureService { get; private set; }

		public NewsletterUnsubscriberSecureService(
			NewsletterUnsubscriberRepository repository,
			INewsletterUnsubscriberOptionSecureService newsletterUnsubscriberOptionSecureService,
			IAccessValidator accessValidator)
		{
			Repository = repository;
			NewsletterUnsubscriberOptionSecureService = newsletterUnsubscriberOptionSecureService;
			AccessValidator = accessValidator;
		}

		public virtual INewsletterUnsubscriber Create(int channelId)
		{
			var subscriber = IoC.Resolve<INewsletterUnsubscriber>();

			subscriber.CreatedDate = DateTime.Now;
			subscriber.ChannelId = channelId;
			subscriber.UnsubscriberOptions = new Collection<INewsletterUnsubscriberOption>();

			subscriber.Status = BusinessObjectStatus.New;

			return subscriber;
		}

		public virtual int Save(ISystemUserFull systemUserFull, INewsletterUnsubscriber unsubscriber)
		{
			if (unsubscriber == null) throw new ArgumentNullException("unsubscriber");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			using (var transactedOperation = new TransactedOperation())
			{
				unsubscriber.UpdatedDate = DateTime.Now;
				unsubscriber.Id = Repository.Save(unsubscriber);

				foreach (var unsubscriberOption in unsubscriber.UnsubscriberOptions)
				{
					unsubscriberOption.UnsubscriberId = unsubscriber.Id;

					if (unsubscriberOption.IsDeleted)
					{
						NewsletterUnsubscriberOptionSecureService.Delete(systemUserFull, unsubscriberOption.Id);
					}
					else if (unsubscriberOption.IsNew || unsubscriberOption.IsEdited)
					{
						unsubscriberOption.Id = NewsletterUnsubscriberOptionSecureService.Save(systemUserFull, unsubscriberOption);
						unsubscriberOption.SetUntouched();
					}
				}

				transactedOperation.Complete();
			}

			unsubscriber.UnsubscriberOptions = new Collection<INewsletterUnsubscriberOption>(unsubscriber.UnsubscriberOptions.Where(o => o.IsUntouched).DefaultIfEmpty().ToList());

			unsubscriber.SetUntouched();

			return unsubscriber.Id;
		}

		public virtual bool Delete(ISystemUserFull systemUserFull, int id)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerCustomer);

			int result;
			using (var transactedOperation = new TransactedOperation())
			{
				// Related Options are removed in SP
				result = Repository.Delete(id);
				transactedOperation.Complete();
			}
			return result > 0;
		}

		public virtual INewsletterUnsubscriber Get(int id)
		{
			INewsletterUnsubscriber unsubscriber = Repository.Get(id);

			if (unsubscriber != null)
			{
				unsubscriber.UnsubscriberOptions = NewsletterUnsubscriberOptionSecureService.GetByUnsubscriber(unsubscriber.Id);
				unsubscriber.SetUntouched();
			}

			return unsubscriber;
		}

		public virtual Collection<INewsletterUnsubscriber> SearchByEmail(int channelId, string email, int page, int pageSize, out int itemsCount)
		{
			var unsubscribers = Repository.SearchByEmail(channelId, email, page, pageSize, out itemsCount);
			foreach (var unsubscriber in unsubscribers)
			{
				unsubscriber.UnsubscriberOptions = NewsletterUnsubscriberOptionSecureService.GetByUnsubscriber(unsubscriber.Id);
			}
			return unsubscribers;
		}		
	}
}