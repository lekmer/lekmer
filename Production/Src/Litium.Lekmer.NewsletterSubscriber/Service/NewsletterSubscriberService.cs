﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterSubscriberService : INewsletterSubscriberService
	{
		protected NewsletterSubscriberRepository Repository { get; private set; }

		public NewsletterSubscriberService(NewsletterSubscriberRepository repository)
		{
			Repository = repository;
		}


		public virtual INewsletterSubscriber Create(IUserContext context)
		{
			var subscriber = IoC.Resolve<INewsletterSubscriber>();

			subscriber.ChannelId = context.Channel.Id;
			subscriber.CreatedDate = DateTime.Now;

			subscriber.Status = BusinessObjectStatus.New;

			return subscriber;
		}

		public virtual int Save(IUserContext context, INewsletterSubscriber subscriber)
		{
			if (subscriber == null) throw new ArgumentNullException("subscriber");

			using (var transactedOperation = new TransactedOperation())
			{
				subscriber.UpdatedDate = DateTime.Now;
				subscriber.Id = Repository.Save(subscriber);
				transactedOperation.Complete();
			}

			subscriber.SetUntouched();

			return subscriber.Id;
		}

		public virtual bool Delete(IUserContext context, string email)
		{
			int result;
			using (var transactedOperation = new TransactedOperation())
			{
				result = Repository.DeleteByEmail(context.Channel.Id, email);
				transactedOperation.Complete();
			}
			return result > 0;
		}

		public virtual void UpdateFromOrders()
		{
			Repository.EnsureNotNull();
			Repository.UpdateFromOrders();
		}

		public virtual void SetSentStatus(int subscriberId)
		{
			Repository.EnsureNotNull();
			Repository.SetSentStatus(subscriberId);
		}

		public virtual Collection<INewsletterSubscriber> GetAllWithNotSentStatus()
		{
			Repository.EnsureNotNull();
			return Repository.GetAllWithNotSentStatus();
		}
	}
}