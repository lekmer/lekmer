﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.NewsletterSubscriber.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public class NewsletterUnsubscriberService : INewsletterUnsubscriberService
	{
		protected NewsletterUnsubscriberRepository Repository { get; private set; }
		protected INewsletterUnsubscriberOptionService UnsubscriberOptionService { get; private set; }

		public NewsletterUnsubscriberService(NewsletterUnsubscriberRepository repository, INewsletterUnsubscriberOptionService unsubscriberOptionService)
		{
			Repository = repository;
			UnsubscriberOptionService = unsubscriberOptionService;
		}

		public virtual INewsletterUnsubscriber Create(IUserContext context)
		{
			var subscriber = IoC.Resolve<INewsletterUnsubscriber>();

			subscriber.CreatedDate = DateTime.Now;
			subscriber.ChannelId = context.Channel.Id;
			subscriber.UnsubscriberOptions = new Collection<INewsletterUnsubscriberOption>();

			subscriber.Status = BusinessObjectStatus.New;

			return subscriber;
		}

		public virtual INewsletterUnsubscriber GetByEmail(IUserContext context, string email)
		{
			INewsletterUnsubscriber unsubscriber = Repository.GetByEmail(context.Channel.Id, email);

			if (unsubscriber != null)
			{
				unsubscriber.UnsubscriberOptions = UnsubscriberOptionService.GetByUnsubscriber(unsubscriber.Id);
				unsubscriber.SetUntouched();
			}

			return unsubscriber;
		}

		public virtual Collection<INewsletterUnsubscriber> GetAllWithNotSentStatus()
		{
			Repository.EnsureNotNull();
			var unsubscribers = Repository.GetAllWithNotSentStatus();
			if (unsubscribers != null)
			{
				foreach (var unsubscriber in unsubscribers)
				{
					unsubscriber.UnsubscriberOptions = UnsubscriberOptionService.GetByUnsubscriber(unsubscriber.Id);
					unsubscriber.SetUntouched();
				}
			}
			return unsubscribers;
		}

		public virtual int Save(IUserContext context, INewsletterUnsubscriber unsubscriber)
		{
			if (unsubscriber == null) throw new ArgumentNullException("unsubscriber");

			using (var transactedOperation = new TransactedOperation())
			{
				unsubscriber.UpdatedDate = DateTime.Now;
				unsubscriber.Id = Repository.Save(unsubscriber);

				foreach (var unsubscriberOption in unsubscriber.UnsubscriberOptions)
				{
					unsubscriberOption.UnsubscriberId = unsubscriber.Id;

					if (unsubscriberOption.IsDeleted)
					{
						UnsubscriberOptionService.Delete(unsubscriberOption.Id);
					}
					else if (unsubscriberOption.IsNew || unsubscriberOption.IsEdited)
					{
						unsubscriberOption.Id = UnsubscriberOptionService.Save(unsubscriberOption);
						unsubscriberOption.SetUntouched();
					}
				}

				transactedOperation.Complete();
			}

			unsubscriber.UnsubscriberOptions = new Collection<INewsletterUnsubscriberOption>(unsubscriber.UnsubscriberOptions.Where(o => o.IsUntouched).DefaultIfEmpty().ToList());

			unsubscriber.SetUntouched();

			return unsubscriber.Id;
		}

		public virtual void SetSentStatus(int unsubscriberId)
		{
			Repository.EnsureNotNull();
			Repository.SetSentStatus(unsubscriberId);
		}

		public virtual bool Delete(IUserContext context, string email)
		{
			int result;
			using (var transactedOperation = new TransactedOperation())
			{
				// Related Options are removed in SP
				result = Repository.DeleteByEmail(context.Channel.Id, email);
				transactedOperation.Complete();
			}
			return result > 0;
		}
	}
}