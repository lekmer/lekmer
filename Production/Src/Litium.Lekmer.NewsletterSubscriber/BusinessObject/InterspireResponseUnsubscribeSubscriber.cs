﻿using System;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponseUnsubscribeSubscriber : InterspireResponse, IInterspireResponseUnsubscribeSubscriber
	{
	}
}