﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.NewsletterSubscriber
{
	[Serializable]
	public class InterspireResponseGetLists : InterspireResponse, IInterspireResponseGetLists
	{
		public Collection<IInterspireContactList> ContactLists { get; set; }
	}
}