﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.TwoSell
{
	public interface IRecommendationProductSecureService
	{
		IRecommendationProduct Create();

		IRecommendationProduct Save(IRecommendationProduct recommendationProduct);
		void Save(Collection<IRecommendationProduct> recommendationProducts);

		void DeleteAllByItem(int recommenderItemId);
	}
}