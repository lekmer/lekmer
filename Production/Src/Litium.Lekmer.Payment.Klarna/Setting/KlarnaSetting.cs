﻿using System.Collections.Generic;
using Litium.Framework.Setting;

namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public class KlarnaSetting : SettingBase, IKlarnaSetting
	{
		private const string _storageName = "Klarna";

		private string _groupName;

		protected override string StorageName
		{
			get { return _storageName; }
		}

		protected virtual string GroupName
		{
			get { return _groupName; }
		}

		protected IKlarnaSettingParser KlarnaSettingParser { get; private set; }


		public KlarnaSetting(IKlarnaSettingParser klarnaSettingParser)
		{
			KlarnaSettingParser = klarnaSettingParser;
		}


		public string SecretKey
		{
			get { return GetString(GroupName, "SecretKey"); }
		}

		public string Url
		{
			get { return GetString(GroupName, "Url"); }
		}

		public int Mode
		{
			get { return GetInt32(GroupName, "Mode"); }
		}

		public int Timeout
		{
			get { return GetInt32(GroupName, "Timeout"); }
		}

		public int EID
		{
			get { return GetInt32(GroupName, "EID"); }
		}

		public bool HandleTimeoutResponse
		{
			get { return GetBoolean(GroupName, "HandleTimeoutResponse"); }
		}

		public string PCStorage
		{
			get { return GetString(GroupName, "PCStorage"); }
		}

		public string PCURI
		{
			get { return GetString(GroupName, "PCURI"); }
		}


		public virtual IEnumerable<ITimeOutData> GetTimeoutSteps()
		{
			string timeoutSteps = GetString(_groupName, "TimeoutSteps", string.Empty);

			return KlarnaSettingParser.ParseTimeoutSteps(timeoutSteps);
		}

		public virtual void Initialize(string channelCommonName)
		{
			_groupName = channelCommonName;
		}


		public static string GetStorageName()
		{
			return _storageName;
		}
	}
}