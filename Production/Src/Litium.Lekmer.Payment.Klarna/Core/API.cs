﻿/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 *
 *  Author: Daniel Hansen
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using CookComputing.XmlRpc;

using Klarna.Core.Structs;
using Klarna.Core.PClasses;

namespace Klarna.Core
{
    /// <summary>
    /// Contains all the methods needed to integrate with Klarna
    /// </summary>
    public partial class API
    {
        #region Member variables

        protected XmlRpcClientProtocol xmlrpcClient;
        private bool isTest = false;

        public bool IsTest { get { return this.isTest; } set { this.isTest = value; } }


        /// <summary>
        /// Version of the .NET API
        /// </summary>
        private const string API_VERSION = "dotNET:api:2.6.1";

        /// <summary>
        /// Klarna protocol
        /// </summary>
        private const string KLARNA_PROTOCOL = "4.1";

        /// <summary>
        /// Url to the Candice server
        /// </summary>
        private string CandiceUrl { get { return "clientstat.klarna.com"; } }
        /// <summary>
        /// Port to the Candice server
        /// </summary>
        private int CandicePort { get { return 80; } }

        private UdpClient udpClient = new UdpClient();

        /// <summary>
        /// Url for connecting to live system
        /// </summary>
        private string LiveAddress { get { return "https://payment.klarna.com"; } }

        /// <summary>
        /// Port number for connecting to live system
        /// </summary>
        private int LivePort { get { return 443; } }

        /// <summary>
        /// Url and port number for connecting to test system
        /// </summary>
        protected string BetaAddress { get { return "http://payment.testdrive.klarna.com"; } }

        protected int BetaPort { get { return 80; } }

        public KlarnaServer mode;

		//public virtual KlarnaServer Mode
		//{
		//	get { return mode; }
		//	set
		//	{
		//		mode = value;
		//		if (mode == KlarnaServer.Live)
		//		{
		//			xmlrpcClient.Url = LiveAddress + ":" + LivePort;
		//		}
		//		else
		//		{
		//			xmlrpcClient.Url = BetaAddress + ":" + BetaPort;
		//		}
		//	}
		//}

        private int eid;

        public int EId { get { return eid; } }

        private string secret;

        public string Secret { get { return secret; } }

        private string ip = "";
        /// <summary>
        /// The customers IP. Its very important this is the customers
        /// IP address.
        /// </summary>
        public string ClientIP { get { return this.ip; } set { this.ip = value; } }

        //private IAPI api;
        private AddressStruct? shippingaddr;
        private AddressStruct? billingaddr;
        private Currency currency;
        private Country country;
        private Language language;
        private Encoding pnoEncoding;
        private GoodsList goodslist = new GoodsList();
        private ArticleNumberCollection artnocoll = new ArticleNumberCollection();
        private ExtraInfo einfo = new ExtraInfo();
        private ShipmentInfo sinfo = new ShipmentInfo();
        private TravelInfo tinfo = new TravelInfo();
        private IncomeExpense iexpense = new IncomeExpense();
        private BankInfo binfo = new BankInfo();
        private SessionID sid = new SessionID();
        private XMLStorage xmlstorage;
        private KlarnaConfig config = new KlarnaConfig();
        private string pcuri = "";
        private string pcstorage = "";
        private string reference = "";
        private string referenceCode = "";
        private string orderID1 = "";
        private string orderID2 = "";
        private string comment = "";

        /// <summary>
        /// PCStorage instance
        /// </summary>
        public string PCStorage { get { return this.pcstorage; } set { this.pcstorage = value; } }

        /// <summary>
        /// The storage URI for PClasses
        /// </summary>
        public string PCURI { get { return this.pcuri; } set { this.pcuri = value; } }

        /// <summary>
        /// Reference person to use when buying for a company
        /// </summary>
        public string Reference { get { return this.reference; } set { this.reference = value; } }

        /// <summary>
        /// The reference code for the sale. You can also use this field to write a message or
        /// other important information to the customer on the invoice.
        /// </summary>
        public string ReferenceCode { get { return this.referenceCode; } set { this.referenceCode = value; } }

        /// <summary>
        /// Order id
        /// </summary>
        public string OrderID1 { get { return this.orderID1; } set { this.orderID1 = value; } }

        /// <summary>
        /// Order id
        /// </summary>
        public string OrderID2 { get { return this.orderID2; } set { this.orderID2 = value; } }

        /// <summary>
        /// Can be used to write some text in the comment field on
        /// the invoice
        /// </summary>
        public string Comment { get { return this.comment; } set { this.comment = value; } }

        private bool keepData = false;
        /// <summary>
        /// Setting this property to True will cause the API to keep all
        /// data stored in the API after a call instead of clearing it, such as
        /// addresses, goodslist, and other order related information. Default is False
        /// </summary>
        public bool KeepData { get { return this.keepData; } set { this.keepData = value; } }

        #endregion

        #region Setter/Getter for structs

        /// <summary>
        ///  Setter/Getter for ExtraInfo
        /// </summary>
        public ExtraInfo ExtraInfo { get { return this.einfo; } set { this.einfo = value; } }

        /// <summary>
        ///  Setter/Getter for ShipmentInfo
        /// </summary>
        public ShipmentInfo ShipmentInfo { get { return this.sinfo; } set { this.sinfo = value; } }

        /// <summary>
        ///  Setter/Getter for TravelInfo
        /// </summary>
        public TravelInfo TravelInfo { get { return this.tinfo; } set { this.tinfo = value; } }

        /// <summary>
        ///  Setter/Getter for IncomeExpense
        /// </summary>
        public IncomeExpense IncomeExpense { get { return this.iexpense; } set { this.iexpense = value; } }

        /// <summary>
        ///  Setter/Getter for BankInfo
        /// </summary>
        public BankInfo BankInfo { get { return this.binfo; } set { this.binfo = value; } }

        /// <summary>
        ///  Setter/Getter for SessionID
        /// </summary>
        public SessionID SessionID { get { return this.sid; } set { this.sid = value; } }

        #endregion

        #region Setter/Getter for country specific variables
        /// <summary>
        ///     Setter/Getter for currency
        /// </summary>
        public Currency CurrencyCode { get { return this.currency; } set { this.currency = value; } }
        /// <summary>
        ///     Setter/Getter for country
        /// </summary>
        public Country CountryCode { get { return this.country; } set { this.country = value; } }
        /// <summary>
        ///     Setter/Getter for language
        /// </summary>
        public Language LanguageCode { get { return this.language; } set { this.language = value; } }
        /// <summary>
        ///     Setter/Getter for PNO encoding
        /// </summary>
        public Encoding PNOEncoding { get { return this.pnoEncoding; } set { this.pnoEncoding = value; } }

        #endregion


        /// <summary>
        /// Check so config fields are set. Example:
        /// try
        /// {
        ///     this.hasFields("eid", "secret");
        /// }
        /// catch
        /// {
        ///     Console.WriteLine("Missing fields:" + e.Message);
        /// }
        /// </summary>
        /// <param name="args">Array if string arguments</param>
        protected void HasFields(string[] args)
        {
            List<string> missingFields = new List<string>();
            foreach (string str in args)
            {
                if (this.config.EmptyFields(str))
                {
                    missingFields.Add(str);
                }
            }
            if (missingFields.Count > 0)
            {
                string temp = "";
                foreach (string s in missingFields)
                {
                    temp = temp + " " + s;
                }

                throw new KlarnaException("Missing config fields: (" + temp + ")");
            }
        }

        /// <summary>
        /// Initializes the Klarna object accordingly to the set config object
        /// </summary>
        protected void Init()
        {
            //this.api = XmlRpcProxyGen.Create<IAPI>();
            this.eid = this.config.EID;
            this.secret = this.config.Secret;
            try
            {
                this.HasFields(new string[] { "eid", "secret", "mode", "pcstorage", "pcuri" });
            }
            catch (Exception e)
            {
                throw e;
            }
            try
            {
                if (this.eid <= 0)
                {
                    throw new KlarnaException("Config field 'eid' is invalid!");
                }
                if (this.secret.Length == 0)
                {
                    throw new KlarnaException("Config field 'secret' is invalid!");
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            try
            {
                this.HasFields(new string[] { "country", "language", "currency", "encoding" });
            }
            catch (Exception e)
            {
                throw e;
            }

            this.currency = this.config.Currency;
            this.country = this.config.Country;
            this.language = this.config.Language;
            this.pnoEncoding = this.config.Encoding;
            this.pcstorage = this.config.PCStorage;
            this.pcuri = this.config.PCURI;
            this.Mode = this.config.Mode;
            if (this.config.Server != null) {
                xmlrpcClient.Url = this.config.Server.ToString();
            }
            this.ActivateInfo = new ActivateInfo();
        }

        // Constructor without arguments to set the values later on.
        public API()
        {
            this.xmlrpcClient = new XmlRpcClientProtocol();
        }

        public API(XmlRpcClientProtocol xmlrpc)
        {
            this.xmlrpcClient = xmlrpc;
        }

        /// <summary>
        /// Sets and initializes this Klarna object using the supplied config object
        /// </summary>
        /// <param name="config"></param>
        public void Config(KlarnaConfig config)
        {
            this.config = config;
            this.Init();
        }

        /// <summary>
        /// Method of ease for setting common config fields.
        /// </summary>
        /// <param name="eid">Merchant ID (Eid)</param>
        /// <param name="secret">Shared Secret</param>
        /// <param name="currency">Customer currency</param>
        /// <param name="country">Customer country</param>
        /// <param name="language">Customer language</param>
        /// <param name="pnoEncoding">Customer pno encoding</param>
        /// <param name="mode">Mode (Live/Beta)</param>
        /// <param name="pcStorage">"xml" is the only PCStorage supported at this time</param>
        /// <param name="pcURI">URI to where you want to store your pclass file</param>
        public void Config(int eid, string secret, Currency currency, Country country, Language language, Encoding pnoEncoding, API.KlarnaServer mode, string pcStorage, string pcURI)
        {
            //this.api = XmlRpcProxyGen.Create<IAPI>();
            this.eid = eid;
            this.secret = secret;
            this.currency = currency;
            this.country = country;
            this.language = language;
            this.pnoEncoding = pnoEncoding;
            this.pcstorage = pcStorage;
            this.pcuri = pcURI;

            // Will determine if we want to connect to
            // live or beta server
            this.Mode = mode;
        }

        /// <summary>
        /// Returns the country code for the country enum
        /// </summary>
        /// <returns>The country code</returns>
        public string GetCountryCode()
        {
            switch (this.country)
            {
                case API.Country.Sweden:
                    return "se";
                case API.Country.Norway:
                    return "no";
                case API.Country.Denmark:
                    return "dk";
                case API.Country.Finland:
                    return "fi";
                case API.Country.Germany:
                    return "de";
                case API.Country.Netherlands:
                    return "nl";
                case API.Country.Austria:
                    return "at";
            }
            return "";
        }

        /// <summary>
        /// Returns the country enum for the supplied country code
        /// </summary>
        /// <param name="code">The country code e.g. "se" or "no"</param>
        /// <returns></returns>
        public static API.Country GetCountryForCode(string countrycode)
        {
            switch (countrycode.ToLower())
            {
                case "se":
                    return API.Country.Sweden;
                case "no":
                    return API.Country.Norway;
                case "dk":
                    return API.Country.Denmark;
                case "fi":
                    return API.Country.Finland;
                case "de":
                    return API.Country.Germany;
                case "nl":
                    return API.Country.Netherlands;
                case "at":
                    return API.Country.Austria;
            }

            string method = MethodInfo.GetCurrentMethod().Name;
            throw new KlarnaException("Error in (" + method + "): Unkown Country! (" + countrycode + ")");
        }

        /// <summary>
        /// Returns the language code for the used language enum
        /// </summary>
        /// <returns>Language code</returns>
        public string GetLanguageCode()
        {
            switch (this.language)
            {
                case API.Language.Swedish:
                    return "sv";
                case API.Language.Norwegian:
                    return "nb";
                case API.Language.Danish:
                    return "da";
                case API.Language.Finnish:
                    return "fi";
                case API.Language.German:
                    return "de";
                case API.Language.Dutch:
                    return "nl";
            }
            return "";
        }

        /// <summary>
        /// Returns the language enum for the supplied language code
        /// </summary>
        /// <param name="languagecode"></param>
        /// <returns>Language constant</returns>
        private static API.Language GetLanguageForCode(string languagecode)
        {
            switch (languagecode.ToLower())
            {
                case "sv":
                    return API.Language.Swedish;
                case "nb":
                    return API.Language.Norwegian;
                case "da":
                    return API.Language.Danish;
                case "fi":
                    return API.Language.Finnish;
                case "de":
                    return API.Language.German;
                case "nl":
                    return API.Language.Dutch;
            }
            string method = MethodInfo.GetCurrentMethod().Name;
            throw new KlarnaException("Eror in (" + method + ")! Unknown Language ! (" + languagecode + ")");
        }

        /// <summary>
        /// Returns the currency code for the used currency enum
        /// </summary>
        /// <returns></returns>
        public string GetCurrencyCode()
        {
            switch (this.currency)
            {
                case API.Currency.SEK:
                    return "SEK";
                case API.Currency.NOK:
                    return "NOK";
                case API.Currency.DKK:
                    return "DKK";
                case API.Currency.EUR:
                    return "EUR";
            }
            return "";
        }

        /// <summary>
        /// Returns the currency enum for the supplied currency code.
        /// </summary>
        /// <param name="currencycode"></param>
        /// <returns></returns>
        private static API.Currency GetCurrencyForCode(string currencycode)
        {
            switch (currencycode.ToLower())
            {
                case "sek":
                    return API.Currency.SEK;
                case "nok":
                    return API.Currency.NOK;
                case "dkk":
                    return API.Currency.DKK;
                case "eur":
                    return API.Currency.EUR;
            }
            string method = MethodInfo.GetCurrentMethod().Name;
            throw new KlarnaException("Eror in (" + method + ")! Unknown Currency! (" + currencycode + ")");
        }

        /// <summary>
        /// Checks set country against set currency and return true if they match
        /// API.Country or API.Currency enums can be used or letter codes
        /// </summary>
        /// <param name="country">Country enum or null</param>
        /// <param name="currency">Currency enum or null</param>
        /// <returns>True if country and currency matches</returns>
        public bool CheckCountryCurrency(API.Country? country, API.Currency? currency)
        {
            if (country == null)
            {
                country = this.country;
            }

            if (currency == null)
            {
                currency = this.currency;
            }
            switch (country)
            {
                case API.Country.Sweden:
                    return currency.Equals(API.Currency.SEK);
                case API.Country.Norway:
                    return currency.Equals(API.Currency.NOK);
                case API.Country.Denmark:
                    return currency.Equals(API.Currency.DKK);
                case API.Country.Finland:
                case API.Country.Germany:
                case API.Country.Netherlands:
                case API.Country.Austria:
                    return currency.Equals(API.Currency.EUR);
            }

            return false;
        }

        public bool CheckCountryCurrency(string country, string currency)
        {
            return this.CheckCountryCurrency(
                API.GetCountryForCode(country),
                API.GetCurrencyForCode(currency));
        }

        /// <summary>
        /// Checks with currently set values if they match
        /// </summary>
        /// <returns>True if set country and currency matches</returns>
        public bool CheckCountryCurrency()
        {
            return CheckCountryCurrency((Country?)null, (Currency?)null);
        }

        /// <summary>
        /// Returns the currency enum for the specified country
        /// </summary>
        /// <param name="country">Country enum</param>
        /// <returns>Currency NULL enum if no match otherwise the correct currency enum</returns>
        public API.Currency? GetCurrencyForCountry(API.Country country)
        {
            switch (country)
            {
                case API.Country.Sweden:
                    return API.Currency.SEK;
                case API.Country.Norway:
                    return API.Currency.NOK;
                case API.Country.Denmark:
                    return API.Currency.DKK;
                case API.Country.Finland:
                case API.Country.Germany:
                case API.Country.Netherlands:
                case API.Country.Austria:
                    return API.Currency.EUR;
            }
            return null;
        }

        /// <summary>
        /// Returns the currency enum for the set country
        /// </summary>
        /// <returns>Currency NULL enum if no match otherwise the correct currency enum</returns>
        public API.Currency? GetCurrencyForCountry()
        {
            return GetCurrencyForCountry(this.country);
        }

        public API.Language? GetLanguageForCountry(API.Country country)
        {
            switch (country)
            {
                case API.Country.Sweden:
                    return API.Language.Swedish;
                case API.Country.Norway:
                    return API.Language.Norwegian;
                case API.Country.Denmark:
                    return API.Language.Danish;
                case API.Country.Finland:
                    return API.Language.Finnish;
                case API.Country.Germany:
                case API.Country.Austria:
                    return API.Language.German;
                case API.Country.Netherlands:
                    return API.Language.Dutch;
            }
            return null;
        }

        /// <summary>
        /// Returns a list of pclasses mathcing the country and pclass type.
        /// If PClass.PClassType.NULL sent, returns all pclasses
        /// </summary>
        /// <param name="ptype">PClass.PClassType</param>
        /// <returns>List of PClasses</returns>
        private List<PClass> pclassList_Old(PClass.PClassType ptype)
        {
            if (this.xmlstorage == null)
            {
                //Attempts to load previously downladed pclasses
                this.xmlstorage = new XMLStorage();
            }
            this.xmlstorage.Load(this.pcuri);
            try
            {
                Dictionary<int, List<PClass>> dictionary = new Dictionary<int, List<PClass>>();
                dictionary = this.xmlstorage.getPClasses(this.country, ptype);
                List<PClass> list = dictionary[this.eid];
                return list;
            }
            catch (Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new KlarnaException("Error in (" + method + ") " + e.Message);
            }
        }

        #region Methods used by both ways of integration

        private void _FetchPClasses(API.Country country, API.Language language, API.Currency currency)
        {
            string digest = this.CreateDigest(this.eid + ":" + (int)currency + ":" + this.secret);
            int months = 0;
            PClass pclass = null;
            PClass.PClassType type = new PClass.PClassType();
            // The date when the pclass expires. Default to Max value. Will get updated after the call
            DateTime expire = DateTime.MaxValue;
            object[] pClassObjects = (object[])this.CallKlarna("get_pclasses", this.eid, (int)currency, digest, (int)country, (int)language);
            foreach (object[] pClassData in pClassObjects)
            {
                switch (Convert.ToInt32(pClassData[8]))
                {
                    case 0:
                        type = PClass.PClassType.Campaign;
                        break;
                    case 1:
                        type = PClass.PClassType.Account;
                        break;
                    case 2:
                        type = PClass.PClassType.SpecialCampaign;
                        break;
                    case 3:
                        type = PClass.PClassType.FixedPrice;
                        break;
                    case 4:
                        type = PClass.PClassType.Delay;
                        break;
                    case 5:
                        type = PClass.PClassType.Mobile;
                        break;
                    default:
                        throw new KlarnaException(-999, "Unknown type retreived from FetchPClasses");
                }

                // Check the month field to make sure we
                // have a numeric months and not a string
                if ((string)pClassData[2] == "-")
                {
                    months = 0;
                }
                else
                {
                    months = Convert.ToInt32(pClassData[2]);
                }

                // Check the expire field
                if ((string)pClassData[9] != "-")
                {
                    string[] dateParts = ((string)pClassData[9]).Split(new char[] { '-' });

                    expire = new DateTime(Convert.ToInt32(dateParts[0]), Convert.ToInt32(dateParts[1]), Convert.ToInt32(dateParts[2]));
                }

                pclass = new PClass(this.eid, Convert.ToInt32(pClassData[0]), Convert.ToString(pClassData[1]), months, Convert.ToDouble(pClassData[3]) / 100,
                    Convert.ToDouble(pClassData[4]) / 100, Convert.ToDouble(pClassData[5]) / 100, Convert.ToDouble(pClassData[6]) / 100, (API.Country)pClassData[7], type, expire);
                //Add the pclass
                this.xmlstorage.AddPClass(pclass);
            }
        }

        /// <summary>
        /// Get all pclasses used with different types of campaigns and store
        /// them in xml format
        /// </summary>
        /// <param name="country">Country</param>
        /// <param name="language">Language</param>
        /// <param name="currency">Currency</param>
        /// <returns>A list of PClass objects</returns>
        public void FetchPClasses(API.Country country, API.Language language, API.Currency currency)
        {
            if (!(this.config is Klarna.Core.KlarnaConfig))
            {
                throw new KlarnaException(50001, "Klarna instance is not fully configured!");
            }

            this.xmlstorage = new XMLStorage();
            try
            {
                //Attempt to load previously stored, so they aren't accidentially removed
                this.xmlstorage.Load(this.pcuri);
            }
            catch (Exception e)
            {
                //Silently fail
            }

            this._FetchPClasses(country, language, currency);

            this.xmlstorage.Save(this.pcuri);
        }

        /// <summary>
        /// Get all pclasses for the country
        /// </summary>
        public void FetchPClasses(API.Country country)
        {
            this._FetchPClasses(country,
                (Language)GetLanguageForCountry(country),
                (Currency)GetCurrencyForCountry(country));
        }

        /// <summary>
        /// Get all pclasses used with different types of campaigns and store
        /// them in xml format for the set Country, Language and Currency values.
        /// </summary>
        public void FetchPClasses()
        {
            this.FetchPClasses(this.country, this.language, this.currency);
        }

        /// <summary>
        /// Removed the stored PClasses, if you need to update them.
        /// </summary>
        public void ClearPClasses()
        {
            if (this.config is Klarna.Core.KlarnaConfig)
            {
                try
                {
                    //Clear the pclasses
                    this.xmlstorage.Clear(this.pcuri);
                }
                catch (Exception e)
                {
                    string method = MethodInfo.GetCurrentMethod().Name;
                    throw new KlarnaException("Error in (" + method + ") : Klarna instance not fully configured");
                }
            }
        }

        /// <summary>
        /// Retrieves the specified PClasses
        /// PClass.PClassType.NULL will fetch all types
        /// </summary>
        /// <param name="ptype">PClass.PClassType</param>
        /// <returns>A list of PClasses</returns>
        public List<PClass> GetPClasses(PClass.PClassType ptype)
        {
            if (this.config is Klarna.Core.KlarnaConfig)
            {
                try
                {
                    //Get the pclasses based on type
                    return pclassList(ptype);
                }
                catch (Exception e)
                {
                    string method = MethodInfo.GetCurrentMethod().Name;
                    throw new KlarnaException("Error in (" + method + "): " + e.Message);
                }
            }
            else
            {
                throw new KlarnaException("KlarnaConfig is not fully configured!");
            }
        }

        /// <summary>
        /// Returns the specified PClass
        /// </summary>
        /// <param name="pid">THe PClass ID you want</param>
        /// <returns>PClass object</returns>
        public PClass GetPClass(int pid)
        {
            try
            {
                List<PClass> pclasses = pclassList(PClass.PClassType.NULL);
                foreach (PClass pclass in pclasses)
                {
                    if (pclass.PClassID.Equals(pid))
                    {
                        return pclass;
                    }
                }
                return null;
            }
            catch (Exception e)
            {
                string method = MethodInfo.GetCurrentMethod().Name;
                throw new KlarnaException("Error in (" + method + "): " + e.Message);
            }
        }

        /// <summary>
        /// Sorts the specified list of KlarnaPClasses
        /// </summary>
        /// <param name="list">A list of PClasses</param>
        /// <returns>A sorted list of PClasses</returns>
        public List<PClass> SortPClasses(List<PClass> list)
        {
            ComparePClass compare = new ComparePClass();
            list.Sort(compare);
            return list;
        }

        /// <summary>
        /// Returns the cheapest, per month, PClass related to the specified sum
        /// Note: This chooses the cheapest PClass for the current country
        /// Flags can be CalculateOn.CheckoutPage or CalculateOn.ProductPage
        /// </summary>
        /// <param name="sum">The product cost, or total sum of the cart</param>
        /// <param name="flags">Which type of page the info will be displayed on</param>
        /// <returns>The cheapest PClass or null</returns>
        public PClass GetCheapestPClass(double sum, CalculateOn flags)
        {
            Calc calc = new Calc();
            double lowestpp = 0.0;
            PClass lowest = null;
            foreach (PClass p in this.GetPClasses(PClass.PClassType.NULL))
            {
                double minpay = calc.CalcMonthlyCost(sum, p, flags);
                if (minpay < lowestpp || lowestpp == 0.0)
                {
                    lowestpp = minpay;
                    lowest = p;
                }
            }
            return lowest;
        }

        /// <summary>
        /// Retreives all the addresses that Klarnas has stored for the given
        /// SSN/PNO. This method does only function for Swedish PNO/SSN
        /// </summary>
        /// <param name="pno">The SSN/PNO of the customer</param>
        /// <param name="enc">PNO encoding</param>
        /// <param name="flag">Flag to indicate what we want to return</param>
        /// <returns>A List of Address objects</returns>
        public List<Address> GetAddresses(string pno, Encoding enc, GetAddressFlag flag)
        {
            Array addresses = (Array)CallKlarna("get_addresses", pno, this.eid, CreateDigest(this.eid + ":" + pno + ":" + this.secret), (int)enc, (int)flag, this.ip);
            List<Address> addressCollection = new List<Address>();
            Address tmp = null;
            foreach (string[] address in addresses)
            {
                tmp = new Address("", "", "", "", "", "", "");
                if (flag == GetAddressFlag.GA_Given)
                {
                    tmp.IsCompany = (address.Length == 5 ? true : false);
                    if (tmp.IsCompany)
                    {
                        tmp.Company = address[0];
                        tmp.Street = address[1];
                        tmp.ZipCode = address[2];
                        tmp.City = address[3];
                        tmp.Country = address[4];
                    }
                    else
                    {
                        tmp.Firstname = address[0];
                        tmp.Lastname = address[1];
                        tmp.Street = address[2];
                        tmp.ZipCode = address[3];
                        tmp.City = address[4];
                        tmp.Country = address[5];
                    }
                }
                else if (flag == GetAddressFlag.GA_Last)
                {
                    tmp.Lastname = address[0];
                    tmp.Street = address[1];
                    tmp.ZipCode = address[2];
                    tmp.City = address[3];
                    tmp.Country = address[4];

                }
                else if (flag == GetAddressFlag.GA_All)
                {
                    if (address[0].Length > 0)
                    {
                        tmp.Firstname = address[0];
                        tmp.Lastname = address[1];
                    }
                    else
                    {
                        tmp.IsCompany = true;
                        tmp.Company = address[1];
                    }

                    tmp.Street = address[2];
                    tmp.ZipCode = address[3];
                    tmp.City = address[4];
                    tmp.Country = address[5];
                }

                addressCollection.Add(tmp);
            }

            return addressCollection;
        }

        /// <summary>
        /// Reserves the specified number of OCR numbers
        /// </summary>
        /// <param name="numberOfOCR">The number of OCR numbers you want to reserve</param>
        /// <param name="country">The country the OCR should belong to</param>
        /// <returns>A list of OCR numbers</returns>
        public string[] ReserveOCR(int numberOfOCR, Country country)
        {
            string digest = this.CreateDigest(this.eid + ":" + numberOfOCR + ":" + this.secret);

            string[] OCR = (string[])CallKlarna("reserve_ocr_nums", numberOfOCR, this.eid, digest, (int)country);

            return OCR;
        }

        /// <summary>
        /// Reserves the specified number of OCR numbers and sends them to the specified email address
        /// </summary>
        /// <param name="numberOfOCR">The number of OCR numbers you want to reserve</param>
        /// <param name="email">Email to send the reserved OCR to</param>
        /// <param name="country">The country the OCR should belong to</param>
        /// <returns>True, if the OCRs were reserved and sent.</returns>
        public bool ReserveOCREmail(int numberOfOCR, string email, Country country)
        {
            string digest = this.CreateDigest(this.eid + ":" + numberOfOCR + ":" + this.secret);

            bool result = ((string)CallKlarna("reserve_ocr_nums_email", numberOfOCR, email, this.eid, digest, (int)country) == "ok");

            return result;
        }

        /// <summary>
        /// Associates a pno with a customer number when you want to make future purchases without a pno.
        /// </summary>
        /// <param name="pno">Social security number, Personal number, ...</param>
        /// <param name="customerNumber">The customer number.</param>
        /// <param name="enc">PNO encoding</param>
        /// <returns>True, if the customer number was associated with the pno.</returns>
        public bool SetCustomerNumber(string pno, string customerNumber, Encoding enc)
        {
            string digest = this.CreateDigest(this.eid + ":" + pno + ":" + customerNumber + ":" + this.secret);

            string response = (string)CallKlarna("set_customer_no", pno, customerNumber, this.eid, digest, (int)enc);

            return (response == "ok");
        }

        public bool SetCustomerNumber(string pno, string customerNumber)
        {
            return SetCustomerNumber(pno, customerNumber, this.PNOEncoding);
        }

        /// <summary>
        /// Retrieves a list of all the customer numbers associated with the specified pno.
        /// </summary>
        /// <param name="pno">Social security number, Personal number, ...</param>
        /// <param name="enc">PNO encoding</param>
        /// <returns>An array containing all customer numbers associated with that pno.</returns>
        public string[] GetCustomerNumber(string pno, Encoding enc)
        {
            string digest = this.CreateDigest(this.eid + ":" + pno + ":" + this.secret);

            string[] customerNumbers = (string[])CallKlarna("get_customer_no", pno, this.eid, digest, (int)enc);

            return customerNumbers;
        }

        public string[] GetCustomerNumber(string pno)
        {
            return GetCustomerNumber(pno, this.PNOEncoding);
        }

        /// <summary>
        /// Removes a customer number from association with a pno.
        /// </summary>
        /// <param name="customerNumber">The customer number</param>
        /// <returns>True, if the customer number association was removed.</returns>
        public bool RemoveCustomerNumber(string customerNumber)
        {
            string digest = this.CreateDigest(this.eid + ":" + customerNumber + ":" + this.secret);

            string response = (string)CallKlarna("remove_customer_no", customerNumber, this.eid, digest);

            return (response == "ok");
        }

        /// <summary>
        /// Returns the current order status for a specific reservation or invoice.
        /// </summary>
        /// <param name="id">Reservation or invoice number</param>
        /// <param name="type">API.OrderStatusForm.InvoiceOrReservation or API.OrderStatusFor.OrderID</param>
        /// <returns>The order status</returns>
        public OrderStatus CheckOrderStatus(string id, OrderStatusFor type)
        {
            string digest = this.CreateDigest(this.eid + ":" + id + ":" + this.secret);

            int orderStatus = (int)CallKlarna("check_order_status", this.eid, digest, id, (int)type);

            switch (orderStatus)
            {
                case 1:
                    return OrderStatus.Accepted;
                case 2:
                    return OrderStatus.Pending;
                case 3:
                    return OrderStatus.Denied;
                default:
                    throw new KlarnaException(-999, "Unknown order status returned");
            }
        }

        /// <summary>
        /// Updates email on all invoices (and reservations?) for specified pno and store/eid.
        /// </summary>
        /// <param name="pno">Social security number, Personal number, ...</param>
        /// <param name="email">Email address.</param>
        /// <returns>True, if the email was successfully updated for specified pno.</returns>
        public bool UpdateEmail(string pno, string email)
        {
            string digest = this.CreateDigest(pno + ":" + email + ":" + this.secret);

            string response = (string)CallKlarna("update_email", this.eid, digest, pno, email);

            return (response == "ok");
        }

        /// <summary>
        /// Clears all data after a successful call
        /// </summary>
        private void ClearData()
        {
            this.reference = "";
            this.referenceCode = "";
            this.orderID1 = "";
            this.orderID2 = "";
            this.ip = "";
            this.comment = "";
            this.sinfo = new ShipmentInfo();
            this.tinfo = new TravelInfo();
            this.iexpense = new IncomeExpense();
            this.binfo = new BankInfo();
            this.sid = new SessionID();
            this.einfo = new ExtraInfo();
            this.ActivateInfo = new ActivateInfo();
            this.artnocoll = new ArticleNumberCollection();
        }

        public bool HasAccount(string pno, Encoding enc)
        {
            string digest = this.CreateDigest(this.eid + ":" + pno + ":" + this.secret);

            string response = (string)CallKlarna("has_account", this.eid, pno, digest, (int)enc);

            switch (response)
            {
                case "true":
                    return true;
                case "false":
                    return false;
                default:
                    throw new KlarnaException("Unexpected return value (" + response + ")");
            }
        }

        public bool HasAccount(string pno)
        {
            return this.HasAccount(pno, this.PNOEncoding);
        }

        #endregion

        #region Methods for creating and handling passive invoices

        /// <summary>
        /// Sends the transaction to Klarna.
        /// </summary>
        /// <param name="pno">The customers PNO/SSN/Birth date</param>
        /// <param name="gender">What gender do the customer have</param>
        /// <param name="flags">Flag</param>
        /// <param name="pclass">What pclass. Pclass indicates what type of purchase it is.</param>
        /// <param name="enc">Pno encoding</param>
        /// <returns></returns>
        public string[] AddTransaction(string pno, API.Gender? gender, Flag flags, int pclass, Encoding enc)
        {
            string digest = "";
            string[] invoiceData = null;
            // Generate the digest
            foreach (Goods goodsitem in this.goodslist.GetGoodsList())
            {
                digest += goodsitem.Article.Title + ":";
            }

            digest += this.secret;
            digest = this.CreateDigest(digest);

            object[] invoiceInfo = (object[]) CallKlarna(
                "add_invoice",
                pno,
                (gender != null ? (object)(int)gender : (string)""),
                this.reference,
                this.referenceCode,
                this.orderID1,
                this.orderID2,
                this.shippingaddr,
                this.billingaddr,
                this.ClientIP,
                (int)flags,
                (int)CurrencyCode,
                (int)CountryCode,
                (int)LanguageCode,
                this.eid,
                digest,
                (int)enc,
                pclass,
                this.goodslist.GetGoodsList(),
                this.comment,
                ToXmlRpcStruct(ShipmentInfo),
                ToXmlRpcStruct(TravelInfo),
                ToXmlRpcStruct(IncomeExpense),
                ToXmlRpcStruct(BankInfo),
                ToXmlRpcStruct(sid),
                ToXmlRpcStruct(ExtraInfo)
            );

            invoiceData = new string[invoiceInfo.Length];

            for (int i = 0; i < invoiceData.Length; i++)
            {
                invoiceData[i] = invoiceInfo[i].ToString();
            }

            if (!this.keepData)
            {
                // Clear the internal goods list
                this.goodslist.Clear();

                // Clear the addresses so we dont use them again by accident
                this.shippingaddr = null;
                this.billingaddr = null;
            }

            return invoiceData;

        }

        /// <summary>
        /// Sends the transaction to Klarna.
        /// </summary>
        /// <param name="pno">The customers PNO/SSN/Birth date</param>
        /// <param name="gender">What gender do the customer have</param>
        /// <param name="flags">Flag</param>
        /// <param name="pclass">What pclass. Pclass indicates what type of purchase it is.</param>
        /// <returns></returns>
        public string[] AddTransaction(string pno, API.Gender? gender, Flag flags, int pclass)
        {
            return AddTransaction(pno, gender, flags, pclass, this.PNOEncoding);
        }

        /// <summary>
        /// Activates the invoice with the given invoice number and pclass
        /// </summary>
        /// <param name="invoiceNo">Passive invoice to activate</param>
        /// <param name="pclass">Pclass to activate the invoice with</param>
        /// <param name="sinfo">Struct containing shipment information</param>
        /// <returns>An URL to the activated invoice</returns>
        public string ActivateInvoice(string invoiceNo, int pclass)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNo + ":" + this.secret);

            string activeInvoiceURL = (string)CallKlarna("activate_invoice", this.eid, invoiceNo, digest, pclass, ToXmlRpcStruct(sinfo));

            return activeInvoiceURL;
        }

        #region ActivateInvoice overloads

        /// <summary>
        /// Activates the invoice with the given invoice number shipment info.
        /// </summary>
        /// <param name="invoiceNo">Passive invoice to activate</param>
        /// <param name="sinfo">Struct containing shipment information</param>
        /// <returns>An URL to the activated invoice</returns>
        [Obsolete]
        public string ActivateInvoice(string invoiceNo, int pclass, ShipmentInfo sinfo)
        {
            this.ShipmentInfo = sinfo;
            return ActivateInvoice(invoiceNo, pclass);
        }

        /// <summary>
        /// Activates the invoice with the given invoice number.
        /// </summary>
        /// <param name="invoiceNo">Passive invoice to activate</param>
        /// <returns>An URL to the activated invoice</returns>
        public string ActivateInvoice(string invoiceNo)
        {
            return ActivateInvoice(invoiceNo, -1);
        }

        #endregion

        /// <summary>
        /// Deletes an passive invoice
        /// </summary>
        /// <param name="invoiceNo">Passive invoice number to delete</param>
        /// <returns>true if invoice was deleted</returns>
        public bool DeleteInvoice(string invoiceNo)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNo + ":" + this.secret);

            string response = (string)CallKlarna("delete_invoice", this.eid, invoiceNo, digest);

            return (response == "ok");
        }

        /// <summary>
        /// Part activates a passive invoice
        /// </summary>
        /// <param name="invoiceNo">Passive invoice number to part activate</param>
        /// <param name="pclass">Pclass to use when activating the invoice</param>
        /// <returns>A XmlRpcStruct with invoice information</returns>
        public XmlRpcStruct ActivatePart(string invoiceNo, int pclass)
        {
            string digest = this.eid + ":" + invoiceNo + ":";
            foreach (ArticleNo artno in this.artnocoll.GetArticleNumbers())
            {
                digest += artno.ArticleNumber + ":" + artno.Quantity + ":";
            }
            digest = this.CreateDigest(digest + this.secret);

            XmlRpcStruct partInvoice = (XmlRpcStruct)CallKlarna(
                "activate_part",
                this.eid,
                invoiceNo,
                this.artnocoll.GetArticleNumbers(),
                digest,
                pclass,
                ToXmlRpcStruct(this.ShipmentInfo)
            );

            if (!this.keepData)
            {
                // Clears the collection
                this.artnocoll.Clear();
            }

            return partInvoice;
        }

        /// <summary>
        /// Update notes on an invoice that is not archived
        /// </summary>
        /// <param name="invoiceNumber">Invoice to update</param>
        /// <param name="note">Note to update invoice with</param>
        /// <returns>Invoice number</returns>
        public string UpdateNotes(string invoiceNumber, string note)
        {
            string digest = this.CreateDigest(invoiceNumber + ":" + note + ":" + this.secret);

            string invoiceNo = (string)CallKlarna("update_notes", this.eid, digest, invoiceNumber, note);

            return invoiceNo;
        }

        #region ActivatePart overloads

        /// <summary>
        /// Part activates a passive invoice with Pclass = -1
        /// </summary>
        /// <param name="invoiceNo">Passive invoice number to part activate</param>
        /// <returns>A URL to the activated invoice</returns>
        public XmlRpcStruct ActivatePart(string invoiceNo)
        {
            return ActivatePart(invoiceNo, (int)Flag.PclassInvoice);
        }

        #endregion

        /// <summary>
        /// Changes the quantity of a specific item in a passive invoice.
        /// </summary>
        /// <param name="invoiceNumber">Invoice to update</param>
        /// <param name="articleNumber">Articlenumber to change quantity for</param>
        /// <param name="quantity">The new quantity</param>
        /// <returns>Invoice number</returns>
        public string UpdateGoodsQuantity(string invoiceNumber, string articleNumber, int quantity)
        {
            string digest = this.CreateDigest(invoiceNumber + ":" + articleNumber + ":" + quantity + ":" + this.secret);

            string response = (string)CallKlarna("update_goods_qty", this.eid, digest, invoiceNumber, articleNumber, quantity);

            return response;
        }

        /// <summary>
        /// Changes the amount of a fee (e.g. the invoice fee) in a passive invoice.
        ///
        /// Type can be
        /// ChargeType.Shipping or ChargeType.Handling
        /// </summary>
        /// <param name="invoiceNumber">Invoice to update</param>
        /// <param name="type">Charge type</param>
        /// <param name="newAmount">The new amount for the charge</param>
        /// <returns>Invoice number</returns>
        public string UpdateChargeAmount(string invoiceNumber, ChargeType type, double newAmount)
        {
            string digest = this.CreateDigest(invoiceNumber + ":" + (int)type + ":" + (int)Math.Round(newAmount * 100) + ":" + this.secret);

            string response = (string)CallKlarna("update_charge_amount", this.eid, digest, invoiceNumber, (int)type, (int)Math.Round(newAmount * 100));

            return response;
        }

        #endregion

        #region Methods for handling active invoices

        /// <summary>
        /// Retrieves the total amount for an active invoice
        /// </summary>
        /// <param name="invoiceNo">Invoice number to check the amount for</param>
        /// <returns>The total amount</returns>
        public double InvoiceAmount(string invoiceNo)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNo + ":" + this.secret);

            int invoiceAmount = (int)CallKlarna("invoice_amount", this.eid, invoiceNo, digest);

            return (double)invoiceAmount / 100;
        }

        /// <summary>
        /// The invoice_address function is used to retrieve the address for an active invoice.
        /// </summary>
        /// <param name="invoiceNumber">Invoice for which to retreive address</param>
        /// <returns>The address</returns>
        public Address InvoiceAddress(string invoiceNumber)
        {
            Address address = new Address("", "", "", "", "", "", "");
            string digest = this.CreateDigest(this.eid + ":" + invoiceNumber + ":" + this.secret);

            string[] returnedAddress = (string[])CallKlarna("invoice_address", this.eid, invoiceNumber, digest);

            if (returnedAddress[0].Length > 0)
            {
                address.IsCompany = false;
                address.Firstname = returnedAddress[0];
                address.Lastname = returnedAddress[1];
            }
            else
            {
                address.IsCompany = true;
                address.Company = returnedAddress[1];
            }

            address.Street = returnedAddress[2];
            address.ZipCode = returnedAddress[3];
            address.City = returnedAddress[4];
            address.Country = returnedAddress[5];

            return address;

        }

        /// <summary>
        /// Changes the order number of a purchase that was set when the order was made online.
        /// </summary>
        /// <param name="invoiceNo">Invoice number to update</param>
        /// <param name="newOrderId">New order ID/number</param>
        /// <returns>Invoice number</returns>
        public string UpdateOrderNumber(string invoiceNo, string newOrderId)
        {
            string digest = this.CreateDigest(invoiceNo + ":" + newOrderId + ":" + this.secret);

            string invoiceNumber = (string)CallKlarna("update_orderno", this.eid, digest, invoiceNo, newOrderId);

            return invoiceNumber;
        }

        /// <summary>
        /// Sends an activated invoice to the customer via e-mail.
        /// The email is sent in plain text format and contains a link to a PDF-invoice.
        /// </summary>
        /// <remarks>Regular postal service is used if the customer has not entered his/her e-mail address when making the purchase (charges may apply).</remarks>
        /// <param name="invoiceNumber">Invoice number to email</param>
        /// <returns>Invoice number</returns>
        public string EmailInvoice(string invoiceNumber)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNumber + ":" + this.secret);

            string emailInvoice = (string)CallKlarna("email_invoice", this.eid, invoiceNumber, digest);

            return emailInvoice;
        }

        /// <summary>
        /// Requests a postal send-out of an activated invoice to a customer by Klarna (charges may apply).
        /// </summary>
        /// <param name="invoiceNumber">Invoice number to email</param>
        /// <returns>Invoice number</returns>
        public string SendInvoiceViaRegularMail(string invoiceNumber)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNumber + ":" + this.secret);

            string sendInvoice = (string)CallKlarna("send_invoice", this.eid, invoiceNumber, digest);

            return sendInvoice;
        }

        /// <summary>
        /// Gives discounts on active invoices.<br>
        /// If you are using standard integration and the purchase is not yet activated (you have not yet delivered the goods),
        /// just change the article list in our online interface Klarna Online.
        /// </summary>
        /// <param name="invoiceNumber">Invoice number</param>
        /// <param name="amount">The amount given as a discount</param>
        /// <param name="VAT">VAT in percent, e.g. 22.2 for 22.2%.</param>
        /// <param name="flags">Use Flag.IncVAT to specify that amount is incl. VAT or Flag.NoFlag for amount excl. VAT</param>
        /// <param name="description">Optional custom text to present as discount in the invoice.</param>
        /// <returns>Invoice number</returns>
        public string ReturnAmount(string invoiceNumber, double amount, double VAT, Flag flags, string description)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNumber + ":" + this.secret);

            if (string.IsNullOrEmpty(description))
            {
                description = "";
            }

            string response = (string)CallKlarna("return_amount", this.eid, invoiceNumber, (int)Math.Round(amount * 100), VAT, digest, (int)flags, description);

            return response;

        }

        /// <summary>
        /// Gives discounts on invoices.<br>
        /// If you are using standard integration and the purchase is not yet activated (you have not yet delivered the goods),
        /// just change the article list in our online interface Klarna Online.
        /// </summary>
        /// <param name="invoiceNumber">Invoice number</param>
        /// <param name="amount">The amount given as a discount incl. VAT</param>
        /// <param name="VAT">VAT in percent, e.g. 22.2 for 22.2%.</param>
        /// <returns>Invoice number</returns>
        public string ReturnAmount(string invoiceNumber, double amount, double VAT)
        {
            return ReturnAmount(invoiceNumber, amount, VAT, Flag.IncVAT, string.Empty);
        }

        /// <summary>
        /// Performs a complete refund on an invoice, part payment and mobile purchase.
        /// </summary>
        /// <param name="invoiceNumber">Invoice to return</param>
        /// <param name="creditNumber">Credit number</param>
        /// <returns>Invoice number</returns>
        public string CreditInvoice(string invoiceNumber, string creditNumber)
        {
            string digest = this.CreateDigest(this.eid + ":" + invoiceNumber + ":" + this.secret);

            string response = (string)CallKlarna("credit_invoice", this.eid, invoiceNumber, creditNumber, digest);

            return response;
        }

        /// <summary>
        /// Retrieves the amount of a specific goods from a purchase.
        /// </summary>
        /// <param name="invoiceNumber">Invoice number to get the amount from</param>
        /// <returns>The amount of the goods</returns>
        /// <remarks>The goods are added to the internal artnolist via AddArticleNumber(ArticleNo artno)</remarks>
        public double InvoicePartAmount(string invoiceNumber)
        {

            string digest = this.eid + ":" + invoiceNumber + ":";

            foreach (ArticleNo artno in artnocoll.GetArticleNumbers())
            {
                digest += artno.ArticleNumber + ":" + artno.Quantity + ":";
            }

            digest = this.CreateDigest(digest + this.secret);

            int amount = (int)CallKlarna("invoice_part_amount", this.eid, invoiceNumber, this.artnocoll.GetArticleNumbers(), digest);

            if (!this.keepData)
            {
                // Clear the collection
                this.artnocoll.Clear();
            }

            return (double)amount / 100;
        }

        #endregion

        #region Methods to make integration easier




        /// <summary>
        /// Method to add an article to the internal goodslist;
        /// </summary>
        /// <param name="Quantity">Quantity</param>
        /// <param name="ArticleNumber">Article number</param>
        /// <param name="title">Name of the product</param>
        /// <param name="price">Price of the product. Use a negative amount if it is a discount</param>
        /// <param name="vat">VAT</param>
        /// <param name="discount">Percental discount.</param>
        /// <param name="flags">Flag</param>
        public void AddArticle(int qty, string artno, string title, double price, double vat, double discount, API.GoodsIs flags)
        {
            this.AddGoods(qty, artno, title, price, vat, discount, flags);
        }

        /// <summary>
        /// Method to add an article to the internal goodslist. Same as AddArticle
        /// </summary>
        /// <param name="Quantity">Quantity</param>
        /// <param name="ArticleNumber">Article number</param>
        /// <param name="title">Name of the product</param>
        /// <param name="price">Price of the product. Use a negative amount if it is a discount</param>
        /// <param name="vat">VAT</param>
        /// <param name="discount">Percental discount.</param>
        /// <param name="flags">Flag</param>
        public void AddGoods(int qty, string artno, string title, double price, double vat, double discount, API.GoodsIs flags)
        {
            this.goodslist.AddArticle(qty, artno, title, price, vat, discount, flags);
        }

        /// <summary>
        /// Adds an article number to the internal collection
        /// </summary>
        /// <param name="ArticleNumber">ArticleNo struct to be added</param>
        public void AddArticleNumber(ArticleNo artno)
        {
            this.artnocoll.AddArticleNumber(artno.Quantity, artno.ArticleNumber);
        }

        /// <summary>
        /// Adds an article number to the internal collection
        /// </summary>
        /// <param name="quantity">Quantity of the goods</param>
        /// <param name="articleNumber">Goods article number</param>
        public void AddArticleNumber(int quantity, string articleNumber)
        {
            this.artnocoll.AddArticleNumber(quantity, articleNumber);
        }

        /// <summary>
        /// Stores the shipping and billing address used by Klarna internally in the API
        /// </summary>
        /// <param name="shipping">Shipping address to use in calls to Klarna</param>
        /// <param name="billing">Billing address to use in calls to Klarna</param>
        public void SetAddresses(ShippingAddress shipping, BillingAddress billing)
        {
            SetAddress(shipping);
            SetAddress(billing);
        }

        public void SetAddress(ShippingAddress shipping)
        {
            this.shippingaddr = new AddressStruct(shipping);
        }

        public void SetAddress(BillingAddress billing)
        {
            this.billingaddr = new AddressStruct(billing);
        }

        #endregion

        /// <summary>
        /// Enum for Klarnas country constants
        /// </summary>
        public enum Country : int
        {
            Austria = 15,
            Denmark = 59,
            Finland = 73,
            Germany = 81,
            Netherlands = 154,
            Norway = 164,
            Sweden = 209,
        };

        /// <summary>
        /// Enum for Klarnas language constants
        /// </summary>
        public enum Language : int
        {
            Danish = 27,
            Finnish = 37,
            German = 28,
            Norwegian = 97,
            Dutch = 101,
            Swedish = 138,
        };

        /// <summary>
        /// Enum for Klarnas currency constants
        /// </summary>
        public enum Currency : int
        {
            SEK = 0,
            NOK = 1,
            EUR = 2,
            DKK = 3,
        };

        /// <summary>
        /// Enum for Klarnas pno encoding constants
        /// </summary>
        public enum Encoding : int
        {
            Swedish = 2,
            Norwegian = 3,
            Finnish = 4,
            Danish = 5,
            German = 6,
            Dutch = 7,
            Austria = 8,
            CustNo = 1000
        };

        /// <summary>
        /// The different order statuses
        /// </summary>
        public enum OrderStatus : int
        {
            Accepted = 1,
            Pending = 2,
            Denied = 3
        };

        /// <summary>
        /// Enum for Klarnas gender constants
        /// </summary>
        public enum Gender : int
        {
            Female = 0,
            Male = 1,
        };

        /// <summary>
        /// Type of shipment
        /// </summary>
        public enum ShipmentType : int
        {
            NormalShipment = 1,
            ExpressShipment = 2
        };

        /// <summary>
        /// Flags used with get_addresses
        /// GA_All = Returns all first and lastnames
        /// GA_Last = Only returns the lastname
        /// GA_Given = Returns the given name and lastname
        /// </summary>
        public enum GetAddressFlag : int
        {
            GA_All = 1,
            GA_Last = 2,
            GA_Given = 5
        };

        public enum CalculateOn : int
        {
            CheckoutPage = 0,
            ProductPage = 1
        };

        /// <summary>
        /// Flags defining what kind of goods we are adding
        /// to our goodslist
        /// IncVAT = Price includes VAT
        /// Shipping = Goods are shipping, will appear in bold on it
        /// own row on the invoice
        /// Handling = Goods are handling, will appear in bold on its own
        /// row on the invoice
        /// Print1000 = Goods has quantity with 3 decimals
        /// Print100 = Goods has quantity with 2 decimals
        /// Print10 = Goods has quantity with 1 decimal
        /// NoFlag = Send without flags
        /// </summary>
        /// <remarks>
        /// Its a special case to integrate using quantity with decimals, please consult
        /// your support at Klarna for help with this.
        /// </remarks>
        [Flags]
        public enum GoodsIs : int
        {
            IncVAT = 32,
            Shipping = 8,
            Handling = 16,
            Print1000 = 1,
            Print100 = 2,
            Print10 = 4,
            NoFlag = 0
        };

        /// <summary>
        /// Defines the type of charge in the call to UpdateChargeAmount
        /// Shipping = We will update the shipping cost
        /// Handling = We will update the handling cost
        /// </summary>
        [Flags]
        public enum ChargeType : int
        {
            Shipping = 1,
            Handling = 2,
        }

        /// <summary>
        /// Other flags
        /// </summary>
        [Flags]
        public enum Flag : int
        {
            NoFlag = 0,
            IncVAT = 32,
            TestMode = 2,
            PclassInvoice = -1,
            AutoActivate = 1,
            [Obsolete]
            PrePay = 8,
            SensitiveOrder = 1024,
            ReturnOCR = 8192,
            MobilePhoneTransaction = 262144, // For add transaction
            SendPhonePIN = 524288, // For add transaction
            NewAmount = 0,
            AddAmount = 1,
            RSRVSendByMail = 4,
            RSRVSendByEmail = 8,
            RSRVPreserveReservation = 16,
            RSRVSensitiveOrder = 32,
            RSRVMobilePhoneTransaction = 512,
            RSRVSendPhonePIN = 1024,
        };

        /// <summary>
        /// Enum used together with CheckOrderStatus to select what
        /// to check order status for.
        /// </summary>
        public enum OrderStatusFor : int
        {
            InvoiceOrReservation = 0,
            OrderID = 1
        };

        /// <summary>
        /// Enum to select server.
        /// </summary>
        public enum KlarnaServer : int
        {
            Live = 1,
            Beta = 0
        };

        /// <summary>
        /// Changes url based on input parameter
        /// </summary>
        /// <param name="isLive">Indicate if we are making calls to Live server or not</param>
        private void SetServerParameters(bool isLive)
        {
            if (isLive)
                api.Url = LiveAddress + ":" + LivePort;
            else
                api.Url = BetaAddress + ":" + BetaPort;

        }

        /// <summary>
        /// Makes the actual call to klarna
        /// </summary>
        /// <param name="method">Method to be called</param>
        /// <param name="eid">Estore ID</param>
        /// <param name="secret">Shared secret</param>
        /// <param name="args">Arguments to be sent with the method</param>
        /// <returns>The result from the call</returns>
        public object CallKlarna(IXmlRpcProxy proxy, string method, params object[] args)
        {
            // Default values that will always be sent with the call to Klarna
            object[] klarnaData = new object[args.Length + 2];
            klarnaData[0] = KLARNA_PROTOCOL;
            klarnaData[1] = API_VERSION;

            // Start time for the call to Klarna
            DateTime startTime = DateTime.Now;

            // Merge the default data with the params sent
            // with the call
            Array.Copy(args, 0, klarnaData, 2, args.Length);

            TimeSpan duration;
            int status = 0;
            object result = null;

            Thread t = new Thread(SendDataToCandice);

            try
            {
                // Make the call to Klarna
                result = xmlrpcClient.Invoke(proxy, method, klarnaData);
                duration = DateTime.Now - startTime;
            }
            catch (XmlRpcFaultException fex)
            {
                duration = DateTime.Now - startTime;

                if (!this.keepData)
                {
                    // Int the event of an error, clear all collections
                    this.artnocoll.Clear();
                    this.goodslist.Clear();
                    this.ClearData();
                }

                // Start sending data to Klarna
                t.Start(GenerateCandiceData(method, fex.FaultCode, new TimeSpan(), duration));
                throw new KlarnaException(fex.FaultCode, fex.FaultString);
            }

            // Start sending data to Klarna
            t.Start(GenerateCandiceData(method, status, new TimeSpan(), duration));

            if (!this.keepData)
            {
                // Clear all data before returning the result
                this.ClearData();
            }

            // Return result
            return result;
        }

        public object CallKlarna(string method, params object[] args)
        {
            return CallKlarna(this.api, method, args);
        }

        /// <summary>
        /// This method assembles all the data used by Candice
        /// </summary>
        /// <param name="method">Method that are called</param>
        /// <param name="status">The return status on the call</param>
        /// <param name="selectTime">Time it took to select host</param>
        /// <param name="duration">Duration for the call to klarna</param>
        /// <returns>Assembled string to send to candice server</returns>
        private string GenerateCandiceData(string method, int status, TimeSpan selectTime, TimeSpan duration)
        {
            string candiceData = "";
            string digest = "";
            // Generate the data sent to Klarna
            candiceData += this.eid + "|";
            candiceData += method + "|";
            candiceData += (int)duration.TotalMilliseconds + "|";
            candiceData += (int)selectTime.TotalMilliseconds + "|";
            candiceData += status + "|";
            candiceData += this.api.Url + "|";
            digest += this.CreateDigest(candiceData + this.secret);
            candiceData += digest;

            return candiceData;
        }

        /// <summary>
        /// Sends statistical data to Klarna
        /// </summary>
        /// <param name="data">Data to be transmitted</param>
        private void SendDataToCandice(object data)
        {
            try
            {
                udpClient = new UdpClient();
                // Connect to the candice server
                udpClient.Connect(this.CandiceUrl, this.CandicePort);

                // Get the data to send as a byte array
                Byte[] dataToSend = System.Text.Encoding.ASCII.GetBytes((string)data);

                // Send the data
                udpClient.Send(dataToSend, dataToSend.Length);

                // Close the connection
                udpClient.Close();
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        /// Internal helper method to convert all our Additional Information classes
        /// (bank info, extra info etc) to XmlRpcStructs that the XML-RPC library
        /// can use.
        /// </summary>
        /// <param name="obj">Object inheriting XmlRpcStruct to convert.</param>
        /// <returns>XmlRpcStruct representation of the object.</returns>
        internal static XmlRpcStruct ToXmlRpcStruct(XmlRpcStruct obj) {
            XmlRpcStruct clone = new XmlRpcStruct();

            foreach (DictionaryEntry v in obj) {
                clone[v.Key] = v.Value;
            }

            return clone;
        }

        /// <summary>
        /// Comparator used to sort PClasses
        /// </summary>
        private class ComparePClass : Comparer<PClass>
        {
            public override int Compare(PClass x, PClass y)
            {
                if (x.Description == null && y.Description == null)
                    return 0;
                else if (x.Description == null && y.Description != null)
                    return 1;
                else if (x.Description != null && y.Description == null)
                    return -1;
                else if (y.Type == PClass.PClassType.SpecialCampaign && x.Type != PClass.PClassType.SpecialCampaign)
                    return 1;
                else if (y.Type != PClass.PClassType.SpecialCampaign && x.Type == PClass.PClassType.SpecialCampaign)
                    return -1;
                else
                {
                    int xx = -999;
                    int yy = -999;

                    try
                    {
                        xx = Convert.ToInt32(x.Description.Substring(0, 2));
                    }
                    catch (Exception e)
                    {
                        //Silently fail
                    }
                    try
                    {
                        yy = Convert.ToInt32(y.Description.Substring(0, 2));
                    }
                    catch (Exception e)
                    {
                        //Silently fail
                    }
                    if (xx > 0 && yy > 0)
                    {
                        return xx.CompareTo(yy) * -1;
                    }
                    else
                        return x.Description.CompareTo(y.Description) * -1;
                }

            }
        }

    }

}
