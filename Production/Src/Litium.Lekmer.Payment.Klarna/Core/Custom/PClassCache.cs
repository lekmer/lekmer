﻿using System.Collections.Generic;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Klarna.Core
{
	public sealed class PClassCache : ScensumCacheBase<PClassCacheKey, List<PClass>>
	{
		private static readonly PClassCache _instance = new PClassCache();

		private PClassCache()
		{
		}

		public static PClassCache Instance
		{
			get { return _instance; }
		}
	}

	public class PClassCacheKey : ICacheKey
	{
		public PClassCacheKey(API.Country country, PClass.PClassType ptype, int eid)
		{
			Country = country;
			Ptype = ptype;
			Eid = eid;
		}

		public API.Country Country { get; set; }

		public PClass.PClassType Ptype { get; set; }

		public int Eid { get; set; }

		public string Key
		{
			get
			{
				return string.Concat(
					Country.ToString(),
					"_",
					Ptype.ToString(),
					"_",
					Eid.ToString(CultureInfo.InstalledUICulture)
				);
			}
		}
	}
}