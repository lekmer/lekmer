/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using System.Text;

namespace Klarna.Core.Structs
{
    /// <summary>
    /// Activate info, optional fields for the Activate call.
    /// </summary>
    [XmlRpcMissingMapping(MappingAction.Ignore)]
    public class ActivateInfo : XmlRpcStruct
    {
        public override void Add(object key, object value)
        {
            base.Add(key, value);
        }

        public override object this[object key]
        {
            get
            {
                return base[key];
            }
            set
            {
                base[key] = value;
            }
        }

        public int? bclass {
            get {
                return (int?) this["bclass"];
            }
            set {
                if (value != null)
                    this["bclass"] = value;
            }
        }
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string cust_no {
            get {
                if (ContainsKey("cust_no")) {
                    return this["cust_no"].ToString();
                }
                return null;
            }
            set {
                if (!String.IsNullOrEmpty(value))
                    Add("cust_no", value);
            }
        }

        public int? flags {
            get {
                return (int?) this["flags"];
            }
            internal set {
                if (value != null)
                    Add("flags", value);
            }
        }

        public string ocr {
            get {
                if (ContainsKey("ocr"))
                    return this["ocr"].ToString();
                return null;
            }
            internal set {
                if (!String.IsNullOrEmpty(value))
                    Add("ocr", value);
            }
        }

        public string orderid1 {
            get {
                if (ContainsKey("orderid1"))
                    return this["orderid1"].ToString();
                return null;
            }
            set {
                if (!String.IsNullOrEmpty(value))
                    Add("orderid1", value);
            }
        }

        public string orderid2 {
            get {
                if (ContainsKey("orderid2"))
                    return this["orderid2"].ToString();
                return null;
            }
            set {
                if (!String.IsNullOrEmpty(value))
                    Add("orderid2", value);
            }
        }

        public string reference {
            get {
                if (ContainsKey("reference"))
                    return this["reference"].ToString();
                return null;
            }
            set {
                if (!String.IsNullOrEmpty(value))
                    Add("reference", value);
            }
        }

        public string reference_code {
            get {
                if (ContainsKey("reference_code"))
                    return this["reference_code"].ToString();
                return null;
            }
            set {
                if (!String.IsNullOrEmpty(value))
                    this["reference_code"] = value;
            }
        }

        public ShipmentInfo shipment_info { get; set; }

        internal void SetShipmentInfo() {
            this["shipment_info"] = Klarna.Core.API.ToXmlRpcStruct(this.shipment_info);
        }

        public ArticleNo[] article_numbers {
            get {
                if (ContainsKey("artnos"))
                    return (ArticleNo[]) this["artnos"];
                return null;
            }
            internal set {
                if (value != null)
                    this["artnos"] = value;
            }
        }

        public override bool Equals (object obj)
        {
            ActivateInfo compare;
            try {
                compare = (ActivateInfo) obj;
            } catch (InvalidCastException e) {
                return false;
            }

            foreach (DictionaryEntry entry in this) {
                if (!compare.ContainsKey(entry.Key)) {
                    return false;
                }
                if (!compare[entry.Key].Equals(entry.Value)) {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode ()
        {
            return ( (this.bclass == null ? 0 : (int) this.bclass)
                    ^ this.cust_no.GetHashCode()
                    ^ (this.flags == null ? 0 : (int) this.flags)
                    ^ this.ocr.GetHashCode()
                    ^ this.orderid1.GetHashCode()
                    ^ this.orderid2.GetHashCode()
                    ^ this.reference.GetHashCode()
                    ^ this.reference_code.GetHashCode()
                    ^ this.shipment_info.GetHashCode()
                    );
        }
    }
}
