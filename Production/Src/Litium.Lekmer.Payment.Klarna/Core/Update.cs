/**
 *  Copyright 2013 KLARNA AB. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are
 *  permitted provided that the following conditions are met:
 *
 *     1. Redistributions of source code must retain the above copyright notice, this list of
 *        conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright notice, this list
 *        of conditions and the following disclaimer in the documentation and/or other materials
 *        provided with the distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY KLARNA AB "AS IS" AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 *  FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL KLARNA AB OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 *  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation are those of the
 *  authors and should not be interpreted as representing official policies, either expressed
 *  or implied, of KLARNA AB.
 */
using System;
using System.Collections.Generic;
using CookComputing.XmlRpc;
using System.Collections;

using Klarna.Core.Structs;

namespace Klarna.Core
{
    public partial class API
    {
        #region Activate Info member
        internal UpdateInfo UpdateInfo { get; set; }
        #endregion

        /// <summary>
        /// Update the reservation matching the given reservation number.
        /// </summary>
        /// <param name='rno'>Reservation number</param>
        /// <see cref="Klarna.Core.Structs.ActivateInfo"/>
        /// <exception cref='KlarnaException'>
        /// Is thrown when the RNO is not specified, or if an error is recieved
        /// by Klarna Online
        /// </exception>
        /// <returns>
        /// A string array with risk status and reservation number.
        /// </returns>
        public bool Update (string rno)
        {
            // RNO in the only required field and not allowed to be null or empty.
            // No other validation to be done.
            if (string.IsNullOrEmpty(rno))
            {
                throw new KlarnaException (
                    "Error in Activate: RNO not specified"
                );
            }

            //Creating UpdateInfo class
            this.UpdateInfo = new UpdateInfo();

            //Adding shipping address to UpdateInfo if it had value
            if (!this.shippingaddr.HasValue)
            {
                this.UpdateInfo.Shipping = new AddressStruct();
            }
            else
            {
                this.UpdateInfo.Shipping = this.shippingaddr;
            }

            //Adding billing address to UpdateInfo if it had value
            if (!this.billingaddr.HasValue)
            {
                this.UpdateInfo.Billing = new AddressStruct();
            }
            else
            {
                this.UpdateInfo.Billing = this.billingaddr;
            }

            //Adding order ID to UpdateInfo
            this.UpdateInfo.OrderId1 = this.orderID1;
            this.UpdateInfo.OrderId2 = this.orderID2;

            //Adding Goods list to the UpdateInfo
            try
            {
                if (this.goodslist.GetGoodsList() != null)
                {
                    this.UpdateInfo.GoodsList = this.goodslist.GetGoodsList();
                }
            }
            catch
            {
                Goods[] tempArray = new Goods[0];
                this.UpdateInfo.GoodsList = tempArray;
            }

            string digest = this.CreateUpdateDigest(rno);

            // Make the call to KO
            string result = (string)CallKlarna (
                "update", this.eid, digest, rno, this.UpdateInfo
            );

            // If keepData is set to false, clear the data.
            if (!this.keepData) {
                this.ClearData ();
            }

            return (result == "ok");
        }

        /// <summary>
        /// All info that is sent in is part of the digest secret, in this order:
        /// [
        ///      proto_vsn, client_vsn, eid, rno, careof, street, zip, city,
        ///      country, fname, lname, careof, street, zip, city, country,
        ///      fname, lname, artno, qty, orderid1, orderid2
        /// ].
        /// The address part appears twice, that is one per address that
        /// changes. If no value is sent in for an optional field, there
        /// is no entry for this field in the digest secret. Shared secret
        /// is added at the end of the digest secret.
        /// </summary>
        /// <param name="rno">The rno</param>
        /// <returns>Resulting digest</returns>
        private string CreateUpdateDigest(string rno)
        {
            List<object> strings = new List<object>();

            // Build the digest for the update call. Null objects and empty
            // strings will be ignored.
            // The Proto VSN for the digest has to have it's . replaced by :
            // according to the specifications.
            strings.Add(API.KLARNA_PROTOCOL.Replace (".", ":"));
            strings.Add(API.API_VERSION);
            strings.Add(this.eid);
            strings.Add(rno);

            // Adding shipping address
            if (this.UpdateInfo.Shipping == null)
            {
                foreach (string part in addressPart())
                {
                    strings.Add(part);
                }
            }
            else
            {
                foreach (string part in addressPart((AddressStruct)this.UpdateInfo.Shipping))
                {
                    strings.Add(part);
                }
            }

            // Adding billing address
            if (this.UpdateInfo.Billing == null)
            {
                foreach (string part in addressPart())
                {
                    strings.Add(part);
                }
            }
            else
            {
                foreach (string part in addressPart((AddressStruct)this.UpdateInfo.Billing))
                {
                    strings.Add(part);
                }
            }

            // Adding goods list if it was not null
            if (this.UpdateInfo.GoodsList != null)
            {
                foreach (Goods goods in this.UpdateInfo.GoodsList)
                {
                    if (string.IsNullOrEmpty(goods.Article.ArticleNumber))
                    {
                        strings.Add(goods.Article.Title);
                    }
                    else
                    {
                        strings.Add(goods.Article.ArticleNumber);
                    }
                    strings.Add(goods.Quantity.ToString());
                }
            }

            // Adding OrderId1
            if (!string.IsNullOrEmpty(this.UpdateInfo.OrderId1))
            {
                strings.Add(this.UpdateInfo.OrderId1);
            }

            // Adding OrderId2
            if (!string.IsNullOrEmpty(this.UpdateInfo.OrderId2))
            {
                strings.Add(this.UpdateInfo.OrderId2);
            }

            // Adding secret
            strings.Add(this.secret);

            // Building and returning the digest
            return this.buildDigest(strings.ToArray());
        }

        /// <summary>
        /// All info that is sent in is part of the digest secret
        /// </summary>
        /// <returns>Resulting digest</returns>
        private List<string> addressPart()
        {
            List<string> digestParts = new List<string>();
            digestParts.Add("");
            return digestParts;
        }

        /// <summary>
        /// All info that is sent in is part of the digest secret
        /// </summary>
        /// <param name="address">The Shipping/Billing address structure</param>
        /// <returns>Resulting digest</returns>
        private List<string> addressPart(AddressStruct address)
        {
            List<string> digestParts = new List<string>();

            // Adding careof
            if (!string.IsNullOrEmpty(address.careof))
            {
                digestParts.Add(address.careof);
            }

            // Adding street
            if (!string.IsNullOrEmpty(address.street))
            {
                digestParts.Add(address.street);
            }

            // Adding zip
            if (!string.IsNullOrEmpty(address.zip))
            {
                digestParts.Add(address.zip);
            }

            // Adding city
            if (!string.IsNullOrEmpty(address.city))
            {
                digestParts.Add(address.city);
            }

            // Adding country
            if (!string.IsNullOrEmpty(address.country.ToString()))
            {
                digestParts.Add(address.country.ToString());
            }

            // Adding first name
            if (!string.IsNullOrEmpty(address.fname))
            {
                digestParts.Add(address.fname);
            }

            // Adding last name
            if (!string.IsNullOrEmpty(address.lname))
            {
                digestParts.Add(address.lname);
            }

            return digestParts;
        }
    }
}
