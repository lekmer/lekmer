﻿using System;
using System.Reflection;
using Litium.Lekmer.Payment.Klarna.Repository;
using log4net;

namespace Litium.Lekmer.Payment.Klarna
{
	public class KlarnaTransactionService : IKlarnaTransactionService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected KlarnaTransactionRepository Repository { get; private set; }

		public KlarnaTransactionService(KlarnaTransactionRepository repository)
		{
			Repository = repository;
		}

		public virtual int CreateGetAddress(int shopId, int mode, string civicNumber)
		{
			try
			{
				return Repository.CreateGetAddress(shopId, mode, (int)KlarnaTransactionType.GetAddress, civicNumber, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateReservation(int shopId, int mode, int transactionType, string civicNumber, int orderId, decimal amount, int currencyId, int pClass, int yearlySalary)
		{
			try
			{
				return Repository.CreateReservation(shopId, mode, transactionType, civicNumber, orderId, amount, currencyId, pClass, yearlySalary, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateCancelReservation(int shopId, int mode, int transactionType, int orderId, string reservationNumber)
		{
			try
			{
				return Repository.CreateCancelReservation(shopId, mode, transactionType, orderId, reservationNumber, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual int CreateCheckOrderStatus(int shopId, int mode, int transactionType, int orderId, string reservationNumber)
		{
			try
			{
				return Repository.CreateCheckOrderStatus(shopId, mode, transactionType, orderId, reservationNumber, DateTime.Now);
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
			return -1;
		}

		public virtual void SaveResult(IKlarnaResult klarnaResult)
		{
			try
			{
				if (klarnaResult.KlarnaTransactionId > 0)
				{
					Repository.SaveResult(klarnaResult);
				}
				else
				{
					_log.Error("SaveResult: KlarnaTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		public virtual void SaveReservationResponse(IKlarnaReservationResult klarnaReservationResult)
		{
			try
			{
				if (klarnaReservationResult.KlarnaTransactionId > 0)
				{
					Repository.SaveReservationResponse(klarnaReservationResult);
				}
				else
				{
					_log.Error("SaveReservationResponse: KlarnaTransactionId <= 0");
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}
	}
}
