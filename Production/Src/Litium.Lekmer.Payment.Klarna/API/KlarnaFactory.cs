﻿using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Klarna
{
	public static class KlarnaFactory
	{
		public static IKlarnaSetting CreateKlarnaSetting()
		{
			return CreateKlarnaSetting(Channel.Current.CommonName);
		}

		public static IKlarnaSetting CreateKlarnaSetting(string channelCommonName)
		{
			var klarnaSetting = IoC.Resolve<IKlarnaSetting>();

			klarnaSetting.Initialize(channelCommonName);

			return klarnaSetting;
		}

		public static IKlarnaSetting CreateKlarnaAdvancedSetting(string channelCommonName)
		{
			var klarnaSetting = IoC.Resolve<IKlarnaAdvancedSetting>();

			klarnaSetting.Initialize(channelCommonName);

			return klarnaSetting;
		}

		public static IKlarnaCheckoutSetting CreateKlarnaCheckoutSetting(string channelCommonName)
		{
			var klarnaSetting = IoC.Resolve<IKlarnaCheckoutSetting>();

			klarnaSetting.Initialize(channelCommonName);

			return klarnaSetting;
		}

		public static IKlarnaClient CreateKlarnaClient(string channelCommonName)
		{
			var klarnaClient = IoC.Resolve<IKlarnaClient>();

			klarnaClient.KlarnaSetting = CreateKlarnaSetting(channelCommonName);
			klarnaClient.Initialize(channelCommonName);

			return klarnaClient;
		}

		public static IKlarnaClient CreateKlarnaAdvancedClient(string channelCommonName)
		{
			var klarnaClient = IoC.Resolve<IKlarnaClient>();

			klarnaClient.KlarnaSetting = CreateKlarnaAdvancedSetting(channelCommonName);
			klarnaClient.Initialize(channelCommonName);

			return klarnaClient;
		}

		public static IKlarnaCheckoutClient CreateKlarnaCheckoutClient(string channelCommonName)
		{
			var klarnaClient = IoC.Resolve<IKlarnaCheckoutClient>();

			klarnaClient.KlarnaSetting = CreateKlarnaCheckoutSetting(channelCommonName);

			return klarnaClient;
		}
	}
}