﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Core
{
	public interface ISubDomain : IBusinessObjectBase
	{
		int Id { get; set; }
		string CommonName { get; set; }
		string DomainUrl { get; set; }
		int UrlType { get; set; }
		int ContentType { get; set; }
	}
}
