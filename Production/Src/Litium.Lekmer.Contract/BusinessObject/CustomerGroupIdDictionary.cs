﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Litium.Lekmer.Contract
{
	[Serializable]
	public class CustomerGroupIdDictionary : Dictionary<int, int>
	{
		public CustomerGroupIdDictionary()
		{
		}

		public CustomerGroupIdDictionary(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}

		public CustomerGroupIdDictionary(IEnumerable<int> customerGroupIds)
		{
			foreach (int customerGroupId in customerGroupIds)
			{
				Add(customerGroupId);
			}
		}

		public void Add(int customerGroupId)
		{
			Add(customerGroupId, customerGroupId);
		}
	}
}