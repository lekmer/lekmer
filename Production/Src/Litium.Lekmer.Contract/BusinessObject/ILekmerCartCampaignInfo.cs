using System.Collections.Generic;
using Litium.Scensum.Campaign;

namespace Litium.Lekmer.Campaign
{
	public interface ILekmerCartCampaignInfo : ICartCampaignInfo
	{
		Dictionary<int, decimal> CampaignFreightCostDictionary { get; }

		/// <summary>
		/// Configured total fixed discount on group of cart items given by voucher.
		/// </summary>
		decimal VoucherTotalFixedDiscountOnGroupOfCartItems { get; set; }

		/// <summary>
		/// Actually applied fixed discount on group of cart items given by voucher.
		/// Could be equal or less than configured total discount if bought cart items total cost is less than it
		/// </summary>
		decimal VoucherActualFixedDiscountOnGroupOfCartItems { get; set; }

		/// <summary>
		/// Applied discount on cart items described in 'Cart item percentage discount' action given by voucher.
		/// </summary>
		decimal VoucherDiscountOnCartItemPercentageDiscountActionItems { get; set; }

		/// <summary>
		/// Applied discount on cart items described in 'Cart item fixed discount' action given by voucher.
		/// </summary>
		decimal VoucherDiscountOnCartItemFixedDiscountActionItems { get; set; }
	}
}