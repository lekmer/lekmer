using Litium.Lekmer.Voucher;

namespace Litium.Lekmer.Campaign
{
	public interface ICartVoucherInfo
	{
		string VoucherCode { get; set; }
		VoucherValueType DiscountType { get; set; }
		/// <summary>
		/// Original discount value that comes from Voucher.
		/// </summary>
		decimal? DiscountValue { get; set; }
		/// <summary>
		/// Discount amount, which was applied in cart.
		/// </summary>
		decimal? AmountUsed { get; set; }
		/// <summary>
		/// Discount amount, with is still available after voucher was used in cart.
		/// </summary>
		decimal? AmountLeft { get; set; }
	}
}