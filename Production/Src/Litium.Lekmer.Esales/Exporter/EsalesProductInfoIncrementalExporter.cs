﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales.Connector;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.Esales.XmlFile;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.Esales.Exporter
{
	public class EsalesProductInfoIncrementalExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IEnumerable<IUserContext> UserContexts;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected IEsalesProductService EsalesProductService { get; private set; }
		protected IEsalesSetting EsalesSetting { get; private set; }
		protected IEsalesConnector EsalesConnector { get; private set; }
		protected IProductChangesService ProductChangesService { get; private set; }

		public EsalesProductInfoIncrementalExporter(ILekmerChannelService lekmerChannelService, IEsalesProductService esalesProductService,
			IEsalesSetting esalesSetting, IEsalesConnector esalesConnector, IProductChangesService productChangesService)
		{
			LekmerChannelService = lekmerChannelService;
			EsalesProductService = esalesProductService;
			EsalesSetting = esalesSetting;
			EsalesConnector = esalesConnector;
			ProductChangesService = productChangesService;
		}

		public virtual void Execute()
		{
			_log.Info("Execution is started.");

			UserContexts = GetUserContextForAllChannels();

			var productChanges = new Collection<IProductChangeEvent>();

			ExecuteEsalesProductList(productChanges);

			if (productChanges.Count > 0)
			{
				ExecuteEsalesImport();
			}

			ProductChangesService.SetStatus(productChanges, ProductChangeEventStatus.ActionApplied);

			_log.Info("Execution is done.");
		}

		public virtual void ExecuteEsalesProductList(Collection<IProductChangeEvent> allProductChanges)
		{
			_log.Info("Product data exporting is started.");

			var esalesProductInfoXmlFile = new EsalesProductInfoXmlFile();
			esalesProductInfoXmlFile.Initialize(EsalesSetting, EsalesImportType.Incremental);

			var productsInfo = IoC.Resolve<IEsalesProductInfoList>();

			_log.Info("Reading product data...");

			while (allProductChanges.Count < EsalesSetting.BatchSize)
			{
				Collection<IProductChangeEvent> productChanges = ProductChangesService.GetAllInQueue(EsalesSetting.PageSize);
				allProductChanges.AddRange(productChanges);

				if (productChanges.Count == 0)
				{
					break;
				}

				var productIds = new ProductIdCollection(productChanges.Select(p => p.ProductId).Distinct().ToList());

				productsInfo = EsalesProductService.GetNextProductListPortion(UserContexts, productsInfo, productIds);

				ProductChangesService.SetStatus(productChanges, ProductChangeEventStatus.InProgress);

				_log.InfoFormat("Reading product data  - {0}.", allProductChanges.Count);
			}

			_log.Info("Product data reading is done.");

			if (allProductChanges.Count == 0)
			{
				_log.Info("There are no product data.");
				return;
			}

			esalesProductInfoXmlFile.AddProducts(productsInfo.GetEsalesProductInfoCollection());

			string filePath = GetFilePath();

			_log.InfoFormat("Product data - file saving... //{0}", filePath);

			if (File.Exists(filePath))
			{
				File.Delete(filePath);
			}

			using (Stream stream = File.OpenWrite(filePath))
			{
				esalesProductInfoXmlFile.Save(stream);
			}

			_log.Info("Product data exporting is completed.");
		}

		public virtual void ExecuteEsalesImport()
		{
			_log.Info("Sending product data to Esales is started.");

			string filePath = GetFilePath();

			if (File.Exists(filePath))
			{
				EsalesConnector.ImportProducts(filePath);
			}

			_log.Info("Sending product data to Esales is completed.");
		}

		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			var channels = LekmerChannelService.GetAll().Where(c => EsalesSetting.GetChannelNameReplacementExists(c.CommonName));

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
				)
				.ToList();
		}

		protected virtual string GetFilePath()
		{
			string dirr = EsalesSetting.DestinationDirectoryProduct;
			string name = EsalesSetting.XmlFileNameProductIncremental;

			return Path.Combine(dirr, name);
		}
	}
}
