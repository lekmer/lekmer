﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales.XmlFile
{
	public class EsalesProductInfoXmlFile : BaseXmlFile
	{
		private readonly string _space = " ";

		private IEsalesSetting _esalesSetting;
		private EsalesImportType _esalesImportType;

		private bool _isInitialized;
		private int _sequenceNumber;

		private XmlDocument _xmlDocument;
		private XmlNode _removeNode;
		private XmlNode _addNode;


		public void Initialize(IEsalesSetting esalesSetting, EsalesImportType esalesImportType)
		{
			if (_isInitialized)
			{
				throw new InvalidOperationException("Initialize() should be called once.");
			}

			// Initialize settings

			_esalesSetting = esalesSetting;
			_esalesImportType = esalesImportType;

			// Initialize xml document

			_xmlDocument = CreateXmlDocument();

			XmlNode operationsNode = AddNode(_xmlDocument, _xmlDocument, EsalesXmlNodes.NodeOperations);
			_removeNode = AddRemoveNode(operationsNode);
			AddClearNode(operationsNode);

			_addNode = AddNode(_xmlDocument, operationsNode, EsalesXmlNodes.NodeAdd);

			_isInitialized = true;
		}

		public void AddProducts(IEnumerable<IEsalesProductInfo> products)
		{
			if (!_isInitialized)
			{
				throw new InvalidOperationException("Initialize() should be called first.");
			}

			AddProductRemoveNodes(products);

			foreach (IEsalesProductInfo productExportInfo in products)
			{
				if (productExportInfo.Available)
				{
					AddProductNodes(productExportInfo);
				}
			}
		}

		public void AddTags(Collection<IEsalesTagInfo> tags)
		{
			if (!_isInitialized)
			{
				throw new InvalidOperationException("Initialize() should be called first.");
			}

			foreach (IEsalesTagInfo tagExportInfo in tags)
			{
				AddTagEntityNodes(tagExportInfo);
			}
		}

		public void Save(Stream savedSiteMap)
		{
			if (!_isInitialized)
			{
				throw new InvalidOperationException("Initialize() should be called first.");
			}

			_xmlDocument.Save(savedSiteMap);
		}


		protected XmlNode AddRemoveNode(XmlNode parentNode)
		{
			// Remove section
			XmlNode removeNode = null;

			if (_esalesImportType == EsalesImportType.Incremental)
			{
				removeNode = AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeRemove);
			}

			return removeNode;
		}

		protected XmlNode AddClearNode(XmlNode parentNode)
		{
			// Clear section
			XmlNode clearNode = null;

			if (_esalesImportType == EsalesImportType.Full)
			{
				clearNode = AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeClear);

				AddNode(_xmlDocument, clearNode, EsalesXmlNodes.NodeProduct);
			}

			return clearNode;
		}


		protected void AddProductRemoveNodes(IEnumerable<IEsalesProductInfo> products)
		{
			if (_esalesImportType == EsalesImportType.Incremental)
			{
				// list of product ids to remove
				foreach (IEsalesProductInfo productEsalesInfo in products)
				{
					XmlNode productNode = AddNode(_xmlDocument, _removeNode, EsalesXmlNodes.NodeProduct);
					AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductKey, productEsalesInfo.ProductId.ToString(CultureInfo.InvariantCulture));
				}
			}
		}

		protected void AddProductNodes(IEsalesProductInfo esalesProductExportInfo)
		{
			// Product
			XmlNode productNode = AddNode(_xmlDocument, _addNode, EsalesXmlNodes.NodeProduct);

			// Sequence number
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeSequenceNumber, ++_sequenceNumber);

			// Entity type
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeEntityType, EsalesXmlNodes.ValueEntityTypeProduct);

			// Product type
			AddProductTypeNode(productNode, esalesProductExportInfo);

			// ProductId
			AddNodeRaw(_xmlDocument, productNode, EsalesXmlNodes.NodeProductKey, esalesProductExportInfo.EsalesKey);

			// Product erp id
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductErpId, esalesProductExportInfo.DefaultProductInfo.Product.ErpId);

			// Product status id
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductStatus, esalesProductExportInfo.DefaultProductInfo.Product.ProductStatusId);

			// Product is active
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductIsActive, esalesProductExportInfo.DefaultProductInfo.Product.ProductStatusId == 0);

			// Available in channels
			AddProductAvailableInChannelsNode(productNode, esalesProductExportInfo);

			// Product titles
			AddProductTitleNodes(productNode, esalesProductExportInfo);

			// Product titles splitted
			AddSplittedProductTitleNodes(productNode, esalesProductExportInfo);

			// Product descriptions
			AddProductDescriptionNodes(productNode, esalesProductExportInfo);

			// Product in stock
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductInStock, esalesProductExportInfo.DefaultProductInfo.Product.TotalNumberInStock > 0);

			// Product ean code
			if (esalesProductExportInfo.DefaultProductInfo.Product.EanCode.HasValue())
			{
				AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductEanCode, esalesProductExportInfo.DefaultProductInfo.Product.EanCode);
			}

			// Product created date
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductCreatedDate, esalesProductExportInfo.DefaultProductInfo.Product.CreatedDate);

			// Product is bookable
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeProductIsBookable, esalesProductExportInfo.DefaultProductInfo.Product.IsBookable);

			// Product age
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeAgeFromMonth, esalesProductExportInfo.DefaultProductInfo.Product.AgeFromMonth);
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeAgeToMonth, esalesProductExportInfo.DefaultProductInfo.Product.AgeToMonth);

			// Product is new
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeIsNewFrom, esalesProductExportInfo.DefaultProductInfo.Product.IsNewFrom);
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeIsNewTo, esalesProductExportInfo.DefaultProductInfo.Product.IsNewTo);
			AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeIsNew, esalesProductExportInfo.DefaultProductInfo.Product.IsNewProduct);

			// Product supplier article number
			if (esalesProductExportInfo.DefaultProductInfo.Product.LekmerErpId.HasValue())
			{
				AddNode(_xmlDocument, productNode, EsalesXmlNodes.NodeSupplierArticleNumber, esalesProductExportInfo.DefaultProductInfo.Product.LekmerErpId);
			}

			// Product prices
			AddProductPriceNodes(productNode, esalesProductExportInfo);

			// Product brand
			AddBrandNodes(productNode, esalesProductExportInfo);

			// Product categories
			AddCategoryNodes(productNode, esalesProductExportInfo);

			// Product tags
			AddTagNodes(productNode, esalesProductExportInfo);

			// Prodct sizes
			AddSizeNodes(productNode, esalesProductExportInfo);

			// Prodct campaignss
			AddCampaignNodes(productNode, esalesProductExportInfo);
		}

		protected void AddTagEntityNodes(IEsalesTagInfo esalesTagInfo)
		{
			// Product-tag
			XmlNode tagNode = AddNode(_xmlDocument, _addNode, EsalesXmlNodes.NodeProduct);

			// Sequence number
			AddNode(_xmlDocument, tagNode, EsalesXmlNodes.NodeSequenceNumber, ++_sequenceNumber);

			// Entity type
			AddNode(_xmlDocument, tagNode, EsalesXmlNodes.NodeEntityType, EsalesXmlNodes.ValueEntityTypeTag);

			// Tag id
			AddNodeRaw(_xmlDocument, tagNode, EsalesXmlNodes.NodeProductKey, esalesTagInfo.EsalesKey);
			AddNode(_xmlDocument, tagNode, EsalesXmlNodes.NodeTagId, esalesTagInfo.TagId);

			// Tag common name
			AddNode(_xmlDocument, tagNode, EsalesXmlNodes.NodeTagCommonName, esalesTagInfo.DefaultTagInfo.Tag.CommonName);

			// Tag group id
			AddNode(_xmlDocument, tagNode, EsalesXmlNodes.NodeTagGroupId, esalesTagInfo.DefaultTagInfo.Tag.TagGroupId);

			// Tag title x
			foreach (IEsalesChannelTagInfo tagInfo in esalesTagInfo.ChannelTagInfoCollection)
			{
				AddNode(_xmlDocument, tagNode, GetElementName(tagInfo.Channel, EsalesXmlNodes.NodeTagTitle), tagInfo.Tag.Value);
			}
		}


		protected virtual void AddProductTypeNode(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			var product = esalesProductExportInfo.DefaultProductInfo.Product;

			string nodeValue = string.Empty;

			if (product.IsProduct())
			{
				nodeValue = EsalesXmlNodes.ValueProductTypeProduct;
			}
			else if (product.IsPackage())
			{
				nodeValue = EsalesXmlNodes.ValueProductTypePackage;
			}

			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeProductType, nodeValue);
		}

		protected virtual void AddProductAvailableInChannelsNode(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			string value = string.Join(EsalesXmlNodes.ValueDelimeter, esalesProductExportInfo.ChannelProductInfoCollection.Select(p => p.Channel.Id.ToString(CultureInfo.InvariantCulture)).ToArray());

			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeAvailableInChannels, value);
		}

		protected virtual void AddProductTitleNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeProductTitle), productInfo.Product.DisplayTitle); //Display title could use Title or WebShopTitle depends what is not empty
			}
		}

		protected virtual void AddSplittedProductTitleNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				string title = productInfo.Product.DisplayTitle.Replace(",", string.Empty);
				var titleValues = title.Split(new[] { _space }, StringSplitOptions.RemoveEmptyEntries);

				var productTitleSplitted = new string[titleValues.Length];
				if (titleValues.Length > 0)
				{
					productTitleSplitted[0] = titleValues[0];
				}
				for (int i = 1; i < titleValues.Length; i++)
				{
					productTitleSplitted[i] = string.Concat(productTitleSplitted[i - 1], _space, titleValues[i]);
				}

				string values = string.Join(EsalesXmlNodes.TextDelimeter, productTitleSplitted);
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeSplittedProductTitle), values);
			}
		}

		protected virtual void AddProductDescriptionNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeProductDescription), productInfo.Product.Description);
			}
		}

		protected virtual void AddProductPriceNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				// Product original price
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeProductOriginalPrice), productInfo.Product.Price.PriceIncludingVat);
			}

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				// Product current price
				decimal currentPrice = productInfo.Product.IsPriceAffectedByCampaign
					? productInfo.Product.CampaignInfo.Price.IncludingVat
					: productInfo.Product.Price.PriceIncludingVat;

				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeProductCurrentPrice), currentPrice);
			}

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				var originalPrice = productInfo.Product.Price.PriceIncludingVat;
				decimal currentPrice = productInfo.Product.IsPriceAffectedByCampaign
					? productInfo.Product.CampaignInfo.Price.IncludingVat
					: productInfo.Product.Price.PriceIncludingVat;

				decimal discountAmount = originalPrice - currentPrice;
				var discountPercent = discountAmount/originalPrice;

				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeProductDiscountPercent), discountPercent);
			}
		}

		protected virtual void AddBrandNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			IBrand brand = esalesProductExportInfo.DefaultProductInfo.Product.Brand;

			if (brand == null)
			{
				return;
			}

			// Brand id
			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeBrandId, brand.Id);

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				// Brand title
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeBrandTitle), productInfo.Product.Brand.Title);
			}

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				// Brand description
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeBrandDescription), productInfo.Product.Brand.Description);
			}
		}

		protected virtual void AddCategoryNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			ICategory category = esalesProductExportInfo.DefaultProductInfo.Category;

			// Category id
			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeCategoryId, category.Id);

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				// Category title
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeCategoryTitle), productInfo.Category.Title);
			}

			ICategory parentCategory = esalesProductExportInfo.DefaultProductInfo.ParentCategory;

			if (parentCategory != null)
			{
				// Parent category id
				AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeParentCategoryId, parentCategory.Id);

				foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
				{
					// Parent category title
					AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeParentCategoryTitle), productInfo.ParentCategory.Title);
				}
			}

			ICategory mainCategory = esalesProductExportInfo.DefaultProductInfo.MainCategory;

			if (mainCategory != null)
			{
				// Main category id
				AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeMainCategoryId, mainCategory.Id);

				foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
				{
					// Main category title
					AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeMainCategoryTitle), productInfo.MainCategory.Title);
				}
			}
		}

		protected virtual void AddTagNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			Collection<ITagGroup> tagGroups = esalesProductExportInfo.DefaultProductInfo.Product.TagGroups;
			if (tagGroups == null || tagGroups.Count == 0)
			{
				return;
			}

			// Tag id
			string tagIdsValue = string.Join(EsalesXmlNodes.ValueDelimeter, esalesProductExportInfo.DefaultProductInfo.Product.TagGroups.SelectMany(tg => tg.Tags).Select(t => t.Id.ToString(CultureInfo.InvariantCulture)).ToArray());
			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeTagId, tagIdsValue);

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				// Tag title
				string tagValues = string.Join(EsalesXmlNodes.TextDelimeter, productInfo.Product.TagGroups.SelectMany(tg => tg.Tags).Select(t => t.Value).ToArray());
				AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeTagTitle), tagValues);
			}
		}

		protected virtual void AddSizeNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			Collection<IProductSize> productSizes = esalesProductExportInfo.DefaultProductInfo.Product.ProductSizes;
			if (productSizes == null || productSizes.Count == 0)
			{
				return;
			}

			// Size id
			string sizeIdsValue = string.Join(EsalesXmlNodes.ValueDelimeter, esalesProductExportInfo.DefaultProductInfo.Product.ProductSizes.Select(ps => ps.SizeInfo.Id.ToString(CultureInfo.InvariantCulture)).ToArray());
			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeSizeId, sizeIdsValue);

			// Size erp id
			string sizeErpIdsValue = string.Join(EsalesXmlNodes.ValueDelimeter, esalesProductExportInfo.DefaultProductInfo.Product.ProductSizes.Select(ps => ps.ErpId).ToArray());
			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeSizeErpId, sizeErpIdsValue);

			// Size stock value
			string sizeStockValue = string.Join(EsalesXmlNodes.ValueDelimeter, esalesProductExportInfo.DefaultProductInfo.Product.ProductSizes.Select(ps => ps.NumberInStock.ToString(CultureInfo.InvariantCulture)).ToArray());
			AddNode(_xmlDocument, parentNode, EsalesXmlNodes.NodeSizeStock, sizeStockValue);
		}

		protected virtual void AddCampaignNodes(XmlNode parentNode, IEsalesProductInfo esalesProductExportInfo)
		{
			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				if (productInfo.Campaigns != null && productInfo.Campaigns.Count > 0)
				{
					// Campaign id
					string campaignIdsValue = string.Join(EsalesXmlNodes.ValueDelimeter, productInfo.Campaigns.Select(c => c.Id.ToString(CultureInfo.InvariantCulture)).ToArray());
					AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeCampaignId), campaignIdsValue);
				}
			}

			foreach (IEsalesChannelProductInfo productInfo in esalesProductExportInfo.ChannelProductInfoCollection)
			{
				if (productInfo.Campaigns != null && productInfo.Campaigns.Count > 0)
				{
					// Campaign title
					string campaignValues = string.Join(EsalesXmlNodes.TextDelimeter, productInfo.Campaigns.Select(c => c.Title).ToArray());
					AddNode(_xmlDocument, parentNode, GetElementName(productInfo.Channel, EsalesXmlNodes.NodeCampaignTitle), campaignValues);
				}
			}
		}


		protected virtual string GetElementName(IChannel channel, string elementName)
		{
			return string.Concat(elementName, "_", channel.Id.ToString(CultureInfo.InvariantCulture));
		}
	}
}