﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesProductListDataMapper : DataMapperBase<IBlockEsalesProductList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;
		private DataMapperBase<IBlockEsalesSetting> _blockEsalesSettingDataMapper;

		public BlockEsalesProductListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
			_blockEsalesSettingDataMapper = DataMapperResolver.Resolve<IBlockEsalesSetting>(DataReader);
		}

		protected override IBlockEsalesProductList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockEsalesProductList = IoC.Resolve<IBlockEsalesProductList>();

			block.ConvertTo(blockEsalesProductList);

			blockEsalesProductList.Setting = _blockSettingDataMapper.MapRow();
			blockEsalesProductList.EsalesSetting = _blockEsalesSettingDataMapper.MapRow();

			blockEsalesProductList.SetUntouched();

			return blockEsalesProductList;
		}
	}
}
