﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales.Mapper
{
	public class BlockEsalesTopSellersDataMapperV2 : DataMapperBase<IBlockEsalesTopSellersV2>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockEsalesTopSellersDataMapperV2(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockEsalesTopSellersV2 Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockEsalesTopSellersV2 = IoC.Resolve<IBlockEsalesTopSellersV2>();
			block.ConvertTo(blockEsalesTopSellersV2);
			blockEsalesTopSellersV2.PanelName = MapNullableValue<string>("BlockEsalesTopSellersV2.PanelName");
			blockEsalesTopSellersV2.WindowLastEsalesValue = MapValue<int>("BlockEsalesTopSellersV2.WindowLastEsalesValue");
			blockEsalesTopSellersV2.IncludeAllCategories = MapValue<bool>("BlockEsalesTopSellersV2.IncludeAllCategories");
			blockEsalesTopSellersV2.LinkContentNodeId = MapNullableValue<int?>("BlockEsalesTopSellersV2.LinkContentNodeId");
			blockEsalesTopSellersV2.CustomUrl = MapNullableValue<string>("BlockEsalesTopSellersV2.CustomUrl");
			blockEsalesTopSellersV2.UrlTitle = MapNullableValue<string>("BlockEsalesTopSellersV2.UrlTitle");
			blockEsalesTopSellersV2.Setting = _blockSettingDataMapper.MapRow();
			blockEsalesTopSellersV2.SetUntouched();
			return blockEsalesTopSellersV2;
		}
	}
}