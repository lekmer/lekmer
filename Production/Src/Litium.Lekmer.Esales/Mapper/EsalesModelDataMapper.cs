﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class EsalesModelDataMapper : DataMapperBase<IEsalesModel>
	{
		public EsalesModelDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IEsalesModel Create()
		{
			var esalesModel = IoC.Resolve<IEsalesModel>();

			esalesModel.Id = MapValue<int>("EsalesModel.ModelId");
			esalesModel.Title = MapValue<string>("EsalesModel.Title");
			esalesModel.CommonName = MapValue<string>("EsalesModel.CommonName");
			esalesModel.Ordinal = MapValue<int>("EsalesModel.Ordinal");

			return esalesModel;
		}
	}
}
