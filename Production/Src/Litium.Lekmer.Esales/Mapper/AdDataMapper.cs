﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Mapper
{
	public class AdDataMapper : DataMapperBase<IAd>
	{
		public AdDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IAd Create()
		{
			var ad = IoC.Resolve<IAd>();

			ad.Id = MapValue<int>("Ad.AdId");
			ad.FolderId = MapValue<int>("Ad.FolderId");
			ad.Key = MapValue<string>("Ad.Key");
			ad.Title = MapValue<string>("Ad.Title");
			ad.DisplayFrom = MapNullableValue<DateTime?>("Ad.DisplayFrom");
			ad.DisplayTo = MapNullableValue<DateTime?>("Ad.DisplayTo");
			ad.ProductCategory = MapNullableValue<string>("Ad.ProductCategory");
			ad.Gender = MapNullableValue<string>("Ad.Gender");
			ad.Format = MapNullableValue<string>("Ad.Format");
			ad.SearchTags = MapNullableValue<string>("Ad.SearchTags");
			ad.CreatedDate = MapValue<DateTime>("Ad.CreatedDate");
			ad.UpdatedDate = MapValue<DateTime>("Ad.UpdatedDate");

			ad.SetUntouched();

			return ad;
		}
	}
}