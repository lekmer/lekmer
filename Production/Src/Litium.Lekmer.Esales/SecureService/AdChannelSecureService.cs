﻿	using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	public class AdChannelSecureService : IAdChannelSecureService
	{
		protected AdChannelRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public AdChannelSecureService(AdChannelRepository adChannelRepository, IAccessValidator accessValidator)
		{
			Repository = adChannelRepository;
			AccessValidator = accessValidator;
		}

		public virtual IAdChannel Create()
		{
			var adChannel = IoC.Resolve<IAdChannel>();
			adChannel.Status = BusinessObjectStatus.New;
			return adChannel;
		}

		public virtual IAdChannel Save(ISystemUserFull systemUserFull, IAdChannel adChannel)
		{
			if (adChannel == null)
			{
				throw new ArgumentNullException("adChannel");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.CampaignAds);

			using (var transaction = new TransactedOperation())
			{
				adChannel.Id = Repository.Save(adChannel);

				transaction.Complete();
			}

			return adChannel;
		}

		public virtual void DeleteAllByAd(ISystemUserFull systemUserFull, int adId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.CampaignAds);

			using (var transaction = new TransactedOperation())
			{
				Repository.DeleteAllByAd(adId);

				transaction.Complete();
			}
		}

		public virtual Collection<IAdChannel> GetAll()
		{
			Repository.EnsureNotNull();

			return Repository.GetAll();
		}

		public virtual Collection<IAdChannel> GetAllByAd(int adId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByAd(adId);
		}
	}
}