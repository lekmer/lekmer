using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesTopSellersSecureServiceV2 : IBlockEsalesTopSellersSecureServiceV2, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockEsalesTopSellersRepositoryV2 Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }
		protected IBlockEsalesTopSellersCategorySecureServiceV2 BlockTopListCategorySecureService { get; private set; }
		protected IBlockEsalesTopSellersProductSecureServiceV2 BlockTopListProductSecureService { get; private set; }


		public BlockEsalesTopSellersSecureServiceV2(
			IAccessValidator accessValidator,
			BlockEsalesTopSellersRepositoryV2 repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockSettingSecureService blockSettingSecureService,
			IBlockEsalesTopSellersCategorySecureServiceV2 blockTopListCategorySecureService,
			IBlockEsalesTopSellersProductSecureServiceV2 blockTopListProductSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockSettingSecureService = blockSettingSecureService;
			BlockTopListCategorySecureService = blockTopListCategorySecureService;
			BlockTopListProductSecureService = blockTopListProductSecureService;
		}

		public virtual IBlockEsalesTopSellersV2 Create()
		{
			if (AccessSecureService == null) throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			if (BlockSettingSecureService == null) throw new InvalidOperationException("BlockSettingSecureService must be set before calling Save.");

			var blockEsalesTopSellersV2 = IoC.Resolve<IBlockEsalesTopSellersV2>();
			blockEsalesTopSellersV2.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockEsalesTopSellersV2.Setting = BlockSettingSecureService.Create();
			blockEsalesTopSellersV2.Status = BusinessObjectStatus.New;
			return blockEsalesTopSellersV2;
		}

		public virtual IBlockEsalesTopSellersV2 GetById(int id)
		{
			return Repository.GetByIdSecure(id);
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockEsalesTopSellersV2 block)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (BlockSecureService == null) throw new InvalidOperationException("BlockSecureService must be set before calling Save.");
			if (BlockSettingSecureService == null) throw new InvalidOperationException("BlockSettingSecureService must be set before calling Save.");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transactedOperation.Complete();
			}

			BlockEsalesTopSellersCacheV2.Instance.Remove(block.Id);

			return block.Id;
		}

		public virtual int Save(
			ISystemUserFull systemUserFull,
			IBlockEsalesTopSellersV2 block,
			Collection<IBlockEsalesTopSellersCategoryV2> blockCategories,
			Collection<IBlockEsalesTopSellersProductV2> blockProducts,
			Collection<ITranslationGeneric> urlTitleTranslations)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();
			BlockTopListCategorySecureService.EnsureNotNull();
			BlockTopListProductSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				Save(systemUserFull, block);

				if (block.Id == -1)
				{
					return block.Id;
				}

				BlockTopListCategorySecureService.Save(block.Id, blockCategories);
				BlockTopListProductSecureService.Save(block.Id, blockProducts);
				SaveTranslations(block.Id, urlTitleTranslations);

				transactedOperation.Complete();
			}

			BlockEsalesTopSellersCacheV2.Instance.Remove(block.Id);

			return block.Id;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockEsalesTopSellersV2 = Create();
			blockEsalesTopSellersV2.ContentNodeId = contentNodeId;
			blockEsalesTopSellersV2.ContentAreaId = contentAreaId;
			blockEsalesTopSellersV2.BlockTypeId = blockTypeId;
			blockEsalesTopSellersV2.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockEsalesTopSellersV2.IncludeAllCategories = true;
			blockEsalesTopSellersV2.Title = title;
			blockEsalesTopSellersV2.TemplateId = null;
			blockEsalesTopSellersV2.Id = Save(systemUserFull, blockEsalesTopSellersV2);
			return blockEsalesTopSellersV2;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}

			BlockEsalesTopSellersCacheV2.Instance.Remove(blockId);
		}


		// Translations.
		protected virtual void SaveTranslations(int blockId, Collection<ITranslationGeneric> urlTitleTranslations)
		{
			foreach (var urlTitleTranslation in urlTitleTranslations)
			{
				Repository.SaveTranslations(blockId, urlTitleTranslation.LanguageId, urlTitleTranslation.Value);
			}
		}

		public virtual Collection<ITranslationGeneric> GetAllUrlTitleTranslationsByBlock(int blockId)
		{
			Repository.EnsureNotNull();
			return Repository.GetAllUrlTitleTranslationsByBlock(blockId);
		}
	}
}