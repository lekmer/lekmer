using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class CorrectionHits : Result, ICorrectionHits
	{
		public override EsalesResultType Type
		{
			get { return EsalesResultType.Corrections; }
		}

		public Collection<ICorrectionInfo> CorrectionsInfo { get; set; }
	}
}