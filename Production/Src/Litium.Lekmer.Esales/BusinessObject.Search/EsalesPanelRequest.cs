using System;
using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	/// <summary>
	/// Provides the data neccessary for the <see cref="IEsalesService"/> to build a query and execute it on one or more indexes. 
	/// </summary>
	[Serializable]
	public class EsalesPanelRequest : IEsalesPanelRequest
	{
		/// <summary>
		/// Esales panel path
		/// </summary>
		public string PanelPath { get; set; }

		/// <summary>
		/// Set of panel arguments
		/// </summary>
		public Dictionary<string, string> PanelArguments { get; set; }
	}
}