﻿using System.Collections.Generic;
using System.Globalization;
using Apptus.ESales.Connector;
using Litium.Lekmer.Common.Extensions;


namespace Litium.Lekmer.Esales
{
	public class CartEsalesRequest : EsalesRequest, ICartEsalesRequest
	{
		public override Dictionary<string, string> ComposeArguments()
		{
			var argMap = new ArgMap();

			if (ProductKey.HasValue())
			{
				argMap[EsalesArguments.ProductKey] = ProductKey;
			}

			if (CartProductIds.HasValue())
			{
				argMap[EsalesArguments.Cart] = CartProductIds;
			}

			if (Paging != null)
			{
				argMap.Add(EsalesArguments.WindowFirst, Paging.FirstItemNumber.ToString(CultureInfo.InvariantCulture));
				argMap.Add(EsalesArguments.WindowLast, Paging.LastItemNumber.ToString(CultureInfo.InvariantCulture));
			}

			return argMap;
		}
		public string ProductKey { get; set; }
		public string CartProductIds { get; set; }
		public IPaging Paging { get; set; }

	}
}
