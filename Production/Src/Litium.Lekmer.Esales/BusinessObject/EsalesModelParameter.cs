﻿using System;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesModelParameter : IEsalesModelParameter
	{
		public string Name { get; set; }
		public string Value { get; set; }
	}
}
