﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesRegistry : BusinessObjectBase, IEsalesRegistry
	{
		private int _id;
		private string _commonName;
		private string _title;
		private int? _channelId;
		private int? _siteStructureRegistryId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public int? ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public int? SiteStructureRegistryId
		{
			get { return _siteStructureRegistryId; }
			set
			{
				CheckChanged(_siteStructureRegistryId, value);
				_siteStructureRegistryId = value;
			}
		}
	}
}