﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesTopSellersProductV2 : BusinessObjectBase, IBlockEsalesTopSellersProductV2
	{
		private int _blockId;
		private IProduct _product;
		private int _position;

		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public IProduct Product
		{
			get { return _product; }
			set
			{
				CheckChanged(_product, value);
				_product = value;
			}
		}

		public int Position
		{
			get { return _position; }
			set
			{
				CheckChanged(_position, value);
				_position = value;
			}
		}
	}
}