using System;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesChannelTagInfo : IEsalesChannelTagInfo
	{
		public IChannel Channel { get; set; }

		public ITag Tag { get; set; }
	}
}