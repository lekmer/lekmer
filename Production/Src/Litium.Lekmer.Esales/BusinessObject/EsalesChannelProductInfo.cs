using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesChannelProductInfo : IEsalesChannelProductInfo
	{
		public IChannel Channel { get; set; }

		public ILekmerProductView Product { get; set; }

		public ICategory Category { get; set; }
		public ICategory ParentCategory { get; set; }
		public ICategory MainCategory { get; set; }
		public Collection<ICampaign> Campaigns { get; set; }
	}
}