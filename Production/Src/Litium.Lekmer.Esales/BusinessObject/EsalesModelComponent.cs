﻿using System;
using System.Collections.Generic;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesModelComponent : IEsalesModelComponent
	{
		public int Id { get; set; }
		public int ModelId { get; set; }
		public string CommonName { get; set; }
		public string Title { get; set; }
		public int Ordinal { get; set; }

		public Dictionary<string, IEsalesModelParameter> Parameters { get; set; }

		public IEsalesModel Model { get; set; }

		public virtual string TryGetParameterValue(string parameterName, string defaultValue)
		{
			if (Parameters == null || Parameters.ContainsKey(parameterName) == false)
			{
				return defaultValue;
			}

			return Parameters[parameterName].Value;
		}
	}
}
