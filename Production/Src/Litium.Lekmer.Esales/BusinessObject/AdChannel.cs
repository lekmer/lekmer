﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class AdChannel : BusinessObjectBase, IAdChannel
	{
		private int _id;
		private int _adId;
		private int _registryId;

		private string _title;
		private bool _showOnSite;
		private int? _landingPageId;
		private string _landingPageUrl;
		private string _imageUrl;
		private int? _imageId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int AdId
		{
			get { return _adId; }
			set
			{
				CheckChanged(_adId, value);
				_adId = value;
			}
		}

		public int RegistryId
		{
			get { return _registryId; }
			set
			{
				CheckChanged(_registryId, value);
				_registryId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public bool ShowOnSite
		{
			get { return _showOnSite; }
			set
			{
				CheckChanged(_showOnSite, value);
				_showOnSite = value;
			}
		}

		public string LandingPageUrl
		{
			get { return _landingPageUrl; }
			set
			{
				CheckChanged(_landingPageUrl, value);
				_landingPageUrl = value;
			}
		}

		public int? LandingPageId
		{
			get { return _landingPageId; }
			set
			{
				CheckChanged(_landingPageId, value);
				_landingPageId = value;
			}
		}

		public string ImageUrl
		{
			get { return _imageUrl; }
			set
			{
				CheckChanged(_imageUrl, value);
				_imageUrl = value;
			}
		}

		public int? ImageId
		{
			get { return _imageId; }
			set
			{
				CheckChanged(_imageId, value);
				_imageId = value;
			}
		}
	}
}