using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class BlockEsalesTopSellersCategoryV2 : BusinessObjectBase, IBlockEsalesTopSellersCategoryV2
	{
		private int _blockId;
		private ICategory _category;
		private bool _includeSubcategories;

		public int BlockId
		{
			get { return _blockId; }
			set
			{
				CheckChanged(_blockId, value);
				_blockId = value;
			}
		}

		public ICategory Category
		{
			get { return _category; }
			set
			{
				CheckChanged(_category, value);
				_category = value;
			}
		}

		public bool IncludeSubcategories
		{
			get { return _includeSubcategories; }
			set
			{
				CheckChanged(_includeSubcategories, value);
				_includeSubcategories = value;
			}
		}
	}
}