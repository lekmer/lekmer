﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	[Serializable]
	public class EsalesProductInfoList : IEsalesProductInfoList
	{
		private readonly Dictionary<int, IEsalesProductInfo> _productInfoDict = new Dictionary<int, IEsalesProductInfo>();

		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		public int TotalCount { get; set; }

		public void AddOrReplace(ProductIdCollection productIdCollection)
		{
			foreach (int productId in productIdCollection)
			{
				var productInfo = IoC.Resolve<IEsalesProductInfo>();

				productInfo.ProductId = productId;
				_productInfoDict[productId] = productInfo;
			}
		}

		public void Add(IChannel channel, ProductCollection productCollection)
		{
			foreach (IProduct product in productCollection)
			{
				if (!_productInfoDict.ContainsKey(product.Id))
				{
					var productInfo = IoC.Resolve<IEsalesProductInfo>();

					productInfo.ProductId = product.Id;
					_productInfoDict[product.Id] = productInfo;
				}

				_productInfoDict[product.Id].Add(channel, (ILekmerProductView)product);
			}
		}

		public Collection<IEsalesProductInfo> GetEsalesProductInfoCollection()
		{
			var esalesProductInfoCollection = new Collection<IEsalesProductInfo>();

			esalesProductInfoCollection.AddRange(_productInfoDict.Values);

			return esalesProductInfoCollection;
		}
	}
}