﻿using System;
using Apptus.ESales.Connector;
using Litium.Lekmer.Common;
using Litium.Lekmer.Esales.Setting;

namespace Litium.Lekmer.Esales.Connector
{
	public class EsalesConnector : IEsalesConnector
	{
		protected IEsalesSetting EsalesSetting { get; set; }
		protected ISessionStateService SessionStateService { get; set; }


		protected OnPremConnector EsalesPremConnector
		{
			get { return OnPremConnector.GetOrCreate(EsalesSetting.ControllerUrl); }
		}

		public Session EsalesSession
		{
			get { return EsalesPremConnector.Session(RetrieveSessionKey()); }
		}


		public EsalesConnector(IEsalesSetting esalesSetting, ISessionStateService sessionStateService)
		{
			EsalesSetting = esalesSetting;
			SessionStateService = sessionStateService;
		}


		public virtual void ImportAds(string fileName)
		{
			if (EsalesSetting.Enabled)
			{
				EsalesPremConnector.ImportAds(fileName, GetAdsImportId());
			}
		}

		public virtual void ImportProducts(string fileName)
		{
			if (EsalesSetting.Enabled)
			{
				EsalesPremConnector.ImportProducts(fileName, GetProductImportId());
			}
		}

		public virtual void Defragmentation()
		{
			if (EsalesSetting.Enabled)
			{
				EsalesPremConnector.Defragment();
			}
		}

		public virtual void Synchronize(bool ignore)
		{
			if (EsalesSetting.Enabled)
			{
				EsalesPremConnector.Synchronize(ignore);
			}
		}


		protected virtual string RetrieveSessionKey()
		{
			if (SessionStateService.Available)
			{
				return SessionStateService.SessionId;
			}

			return EsalesSetting.ApplicationName + "_" + DateTime.Now.ToString("yyyyMMdd_HH:mm:ss.FFFFFFF");
		}

		protected virtual string GetAdsImportId()
		{
			return "ads_" + DateTime.Now.ToString("yyyyMMdd_HH:mm:ss.FFFFFFF");
		}

		protected virtual string GetProductImportId()
		{
			return "products_" + DateTime.Now.ToString("yyyyMMdd_HH:mm:ss.FFFFFFF");
		}
	}
}