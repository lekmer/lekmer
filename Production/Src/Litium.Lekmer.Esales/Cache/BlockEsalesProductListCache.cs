﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Esales
{
	public sealed class BlockEsalesProductListCache : ScensumCacheBase<BlockEsalesProductListKey, IBlockEsalesProductList>
	{
		private static readonly BlockEsalesProductListCache _instance = new BlockEsalesProductListCache();

		private BlockEsalesProductListCache()
		{
		}

		public static BlockEsalesProductListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockEsalesProductListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockEsalesProductListKey : ICacheKey
	{
		public BlockEsalesProductListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}