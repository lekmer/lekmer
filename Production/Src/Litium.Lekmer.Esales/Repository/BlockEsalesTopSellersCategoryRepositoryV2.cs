﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesTopSellersCategoryRepositoryV2
	{
		protected virtual DataMapperBase<IBlockEsalesTopSellersCategoryV2> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesTopSellersCategoryV2>(dataReader);
		}


		public virtual void Save(IBlockEsalesTopSellersCategoryV2 blockCategory)
		{
			if (blockCategory == null) throw new ArgumentNullException("blockCategory");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockCategory.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", blockCategory.Category.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", blockCategory.IncludeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersCategoryRepositoryV2.Save");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2CategorySave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersCategoryRepositoryV2.DeleteAll");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2CategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockEsalesTopSellersCategoryV2> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersCategoryRepositoryV2.GetAllByBlockSecure");
			using (var dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2CategoryGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockEsalesTopSellersCategoryV2> GetAllByBlock(int blockId, int languageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersCategoryRepositoryV2.GetAllByBlock");
			using (var dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2CategoryGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}