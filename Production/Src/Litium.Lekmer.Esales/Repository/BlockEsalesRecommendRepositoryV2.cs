﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesRecommendRepositoryV2
	{
		protected virtual DataMapperBase<IBlockEsalesRecommendV2> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesRecommendV2>(dataReader);
		}


		public virtual void Save(IBlockEsalesRecommendV2 blockEsalesRecommendV2)
		{
			if (blockEsalesRecommendV2 == null)
			{
				throw new ArgumentNullException("blockEsalesRecommendV2");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockEsalesRecommendV2.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("PanelName", blockEsalesRecommendV2.PanelName, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("WindowLastEsalesValue", blockEsalesRecommendV2.WindowLastEsalesValue, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockEsalesRecommendRepositoryV2.Save");

			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesRecommendV2Save]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockEsalesRecommendRepositoryV2.Delete");

			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesRecommendV2Delete]", parameters, dbSettings);
		}


		public virtual IBlockEsalesRecommendV2 GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockEsalesRecommendRepositoryV2.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesRecommendV2GetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockEsalesRecommendV2 GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockEsalesRecommendRepositoryV2.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesRecommendV2GetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}