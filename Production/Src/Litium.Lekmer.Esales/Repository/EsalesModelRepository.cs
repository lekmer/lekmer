﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class EsalesModelRepository
	{
		protected virtual DataMapperBase<IEsalesModel> CreateModelDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IEsalesModel>(dataReader);
		}

		protected virtual DataMapperBase<IEsalesModelComponent> CreateModelComponentDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IEsalesModelComponent>(dataReader);
		}

		protected virtual DataMapperBase<IEsalesModelParameter> CreateModelParameterDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IEsalesModelParameter>(dataReader);
		}

		public virtual IEsalesModelComponent GetModelComponentById(int modelComponentId)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesModelRepository.GetModelComponentById");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ModelComponentId", modelComponentId, SqlDbType.Int)
			};

			IEsalesModelComponent modelComponent = null;

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pEsalesModelComponentGetById]", parameters, dbSettings))
			{
				DataMapperBase<IEsalesModel> modelDataMapper = CreateModelDataMapper(dataReader);
				DataMapperBase<IEsalesModelComponent> modelComponentDataMapper = CreateModelComponentDataMapper(dataReader);

				if (dataReader.Read())
				{
					modelComponent = modelComponentDataMapper.MapRow();
					modelComponent.Model = modelDataMapper.MapRow();
				}
			}

			return modelComponent;
		}

		public virtual Collection<IEsalesModelComponent> GetModelComponentsAllByBlockType(int blockTypeId)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesModelRepository.GetModelComponentsAllByBlockType");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockTypeId", blockTypeId, SqlDbType.Int)
			};

			var modelComponentList = new Collection<IEsalesModelComponent>();

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pEsalesModelComponentGetAllByBlockTypeIdSecure]", parameters, dbSettings))
			{
				DataMapperBase<IEsalesModel> modelDataMapper = CreateModelDataMapper(dataReader);
				DataMapperBase<IEsalesModelComponent> modelComponentDataMapper = CreateModelComponentDataMapper(dataReader);

				while (dataReader.Read())
				{
					IEsalesModelComponent modelComponent = modelComponentDataMapper.MapRow();
					modelComponent.Model = modelDataMapper.MapRow();

					modelComponentList.Add(modelComponent);
				}
			}

			return modelComponentList;
		}

		public virtual Collection<IEsalesModelParameter> GetModelParameterAllByModel(int siteStructureRegistryId, int modelComponentById)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesModelRepository.GetModelComponentsAllByBlockType");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SiteStructureRegistryId", siteStructureRegistryId, SqlDbType.Int),
				ParameterHelper.CreateParameter("ModelComponentId", modelComponentById, SqlDbType.Int)
			};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pEsalesModelParameterAllByModel]", parameters, dbSettings))
			{
				return CreateModelParameterDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IEsalesModelParameter> GetModelComponentParameterAllByModelComponent(int siteStructureRegistryId, int modelComponentById)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesModelRepository.GetModelComponentParameterAllByModelComponent");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("SiteStructureRegistryId", siteStructureRegistryId, SqlDbType.Int),
				ParameterHelper.CreateParameter("ModelComponentId", modelComponentById, SqlDbType.Int)
			};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pEsalesModelComponentParameterAllByModelComponent]", parameters, dbSettings))
			{
				return CreateModelParameterDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}
