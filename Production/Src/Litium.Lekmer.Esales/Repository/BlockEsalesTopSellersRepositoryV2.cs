﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Repository
{
	public class BlockEsalesTopSellersRepositoryV2
	{
		protected virtual DataMapperBase<IBlockEsalesTopSellersV2> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockEsalesTopSellersV2>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationsDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}


		public virtual void Save(IBlockEsalesTopSellersV2 block)
		{
			if (block == null) throw new ArgumentNullException("block");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("PanelName", block.PanelName, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("WindowLastEsalesValue", block.WindowLastEsalesValue, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllCategories", block.IncludeAllCategories, SqlDbType.Bit),
					ParameterHelper.CreateParameter("LinkContentNodeId", block.LinkContentNodeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomUrl", block.CustomUrl, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("UrlTitle", block.UrlTitle, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersRepositoryV2.Save");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2Save]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersRepositoryV2.Delete");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2Delete]", parameters, dbSettings);
		}

		public virtual IBlockEsalesTopSellersV2 GetByIdSecure(int id)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersRepositoryV2.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2GetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockEsalesTopSellersV2 GetById(int id, int languageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersRepositoryV2.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2GetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// Translations.
		public virtual void SaveTranslations(int blockId, int languageId, string urlTitle)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("UrlTitle", urlTitle, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersRepositoryV2.SaveTranslations");
			new DataHandler().ExecuteCommand("[esales].[pBlockEsalesTopSellersV2TranslationSave]", parameters, dbSettings);
		}

		public virtual Collection<ITranslationGeneric> GetAllUrlTitleTranslationsByBlock(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("BlockEsalesTopSellersRepositoryV2.GetAllUrlTitleTranslationsByBlock");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[esales].[pBlockEsalesTopSellersV2UrlTitleTranslationGetAll]", parameters, dbSettings))
			{
				return CreateTranslationsDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}