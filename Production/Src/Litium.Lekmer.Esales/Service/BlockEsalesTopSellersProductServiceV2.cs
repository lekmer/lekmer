﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Esales
{
	public class BlockEsalesTopSellersProductServiceV2 : IBlockEsalesTopSellersProductServiceV2
	{
		protected BlockEsalesTopSellersProductRepositoryV2 Repository { get; private set; }
		protected IProductService ProductService { get; private set; }

		public BlockEsalesTopSellersProductServiceV2(BlockEsalesTopSellersProductRepositoryV2 repository, IProductService productService)
		{
			Repository = repository;
			ProductService = productService;
		}

		public virtual Collection<IBlockEsalesTopSellersProductV2> GetAllByBlock(IUserContext context, int blockId)
		{
			return Repository.GetAllByBlock(blockId, context.Channel.Language.Id);
		}
	}
}