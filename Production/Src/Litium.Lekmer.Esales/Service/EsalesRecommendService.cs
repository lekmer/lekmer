﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Esales
{
	public class EsalesRecommendService : IEsalesRecommendService
	{
		protected IEsalesService EsalesService { get; private set; }
		protected IEsalesProductKeyService EsalesProductKeyService { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }

		public EsalesRecommendService(IEsalesService esalesService, IEsalesProductKeyService esalesProductKeyService, IProductService productService, ICategoryService categoryService)
		{
			EsalesService = esalesService;
			EsalesProductKeyService = esalesProductKeyService;
			ProductService = productService;
			CategoryService = categoryService;
		}

		public ProductCollection FindRecommend(IUserContext context, string panelPath, string fallbackPanelPath, string customerKey, int productId, ProductIdCollection cartProductIds, int itemsToReturn)
		{
			if (context == null) throw new ArgumentNullException("context");
			EsalesService.EnsureNotNull();

			ProductCollection recommendProducts = FindRecommendProducts(context, panelPath, customerKey, productId, cartProductIds, itemsToReturn);

			if (recommendProducts.Count < itemsToReturn)
			{
				ProductIdCollection productIds = CombineProductIds(productId, cartProductIds); // current product or cart products

				int fallBackItemsToReturn = itemsToReturn - recommendProducts.Count;

				ProductCollection fallBackProducts;
				if (productIds.Count > 0)
				{
					fallBackProducts = FindFallBackProductsByCategoryUp(context, fallbackPanelPath, productIds, recommendProducts, fallBackItemsToReturn);
				}
				else
				{
					fallBackProducts = FindFallBackProductsSimple(context, fallbackPanelPath, recommendProducts, fallBackItemsToReturn);
				}

				recommendProducts.AddRange(fallBackProducts);
			}

			return recommendProducts;
		}


		protected virtual ProductCollection FindRecommendProducts(IUserContext context, string panelPath, string customerKey, int productId, ProductIdCollection cartProductIds, int itemsToReturn)
		{
			var esalesRequest = IoC.Resolve<IRecommendEsalesRequest>();
			esalesRequest.PanelPath = panelPath;
			esalesRequest.ChannelId = context.Channel.Id.ToString(CultureInfo.InvariantCulture);
			esalesRequest.CustomerKey = customerKey;

			if(productId > 0)
			{
				esalesRequest.ProductKey = productId.ToString(CultureInfo.InvariantCulture);
			}

			esalesRequest.CartProductIds = Convert.ToStringIdentifierList(cartProductIds).TrimEnd(',');
			esalesRequest.Paging = new Paging(1, itemsToReturn);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			return FindProducts(context, esalesResponse, itemsToReturn);
		}

		protected virtual ProductCollection FindFallBackProductsSimple(IUserContext context, string panelPath, ProductCollection recommendProducts, int itemsToReturn)
		{
			var esalesRequest = IoC.Resolve<IRecommendEsalesRequest>();
			esalesRequest.PanelPath = panelPath;
			esalesRequest.ChannelId = context.Channel.Id.ToString(CultureInfo.InvariantCulture);

			esalesRequest.ExcludeProductIds = recommendProducts.Select(p => p.Id).ToList();

			esalesRequest.Paging = new Paging(1, itemsToReturn);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			ProductCollection fallbackProducts = FindProducts(context, esalesResponse, itemsToReturn);

			return fallbackProducts;
		}

		protected virtual ProductCollection FindFallBackProductsByCategoryUp(IUserContext context, string panelPath, ProductIdCollection productIds, ProductCollection recommendProducts, int itemsToReturn)
		{
			List<int> categoryIds, parentCategoryIds, mainCategoryIds;
			FindCategoryIds(context, productIds, out categoryIds, out parentCategoryIds, out mainCategoryIds);

			var esalesRequest = IoC.Resolve<IRecommendEsalesRequest>();
			esalesRequest.PanelPath = panelPath;
			esalesRequest.ChannelId = context.Channel.Id.ToString(CultureInfo.InvariantCulture);

			esalesRequest.IncludeCategoryIds = categoryIds;
			esalesRequest.ExcludeProductIds = CombineProductIds(productIds, recommendProducts); // Exclude current or cart products, together with recommended products.

			esalesRequest.Paging = new Paging(1, itemsToReturn);

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(esalesRequest);

			ProductCollection fallbackProducts = FindProducts(context, esalesResponse, itemsToReturn);

			if (fallbackProducts.Count < itemsToReturn)
			{
				// Fallback from parent folder

				int parentFallbackItemsToReturn = itemsToReturn - fallbackProducts.Count;

				esalesRequest.IncludeCategoryIds = null;
				esalesRequest.IncludeParentCategoryIds = null;
				esalesRequest.IncludeParentCategoryIds = parentCategoryIds;
				
				esalesRequest.ExcludeProductIds = CombineProductIds(productIds, recommendProducts, fallbackProducts); // Exclude current or cart products, recommended products and fallbacks.

				esalesRequest.Paging = new Paging(1, parentFallbackItemsToReturn);

				esalesResponse = EsalesService.RetrieveContent(esalesRequest);

				ProductCollection parentFallbackProducts = FindProducts(context, esalesResponse, parentFallbackItemsToReturn);

				fallbackProducts.AddRange(parentFallbackProducts);
			}

			if (fallbackProducts.Count < itemsToReturn)
			{
				// Fallback from main folder

				int mainFallbackItemsToReturn = itemsToReturn - fallbackProducts.Count;

				esalesRequest.IncludeCategoryIds = null;
				esalesRequest.IncludeParentCategoryIds = null;
				esalesRequest.IncludeMainCategoryIds = mainCategoryIds;

				esalesRequest.ExcludeProductIds = CombineProductIds(productIds, recommendProducts, fallbackProducts); // Exclude current or cart products, recommended products and fallbacks.

				esalesRequest.Paging = new Paging(1, mainFallbackItemsToReturn);

				esalesResponse = EsalesService.RetrieveContent(esalesRequest);

				ProductCollection mainFallbackProducts = FindProducts(context, esalesResponse, mainFallbackItemsToReturn);

				fallbackProducts.AddRange(mainFallbackProducts);
			}

			return fallbackProducts;
		}

		protected virtual ProductCollection FindProducts(IUserContext context, IEsalesResponse esalesResponse, int itemsToReturn)
		{
			Collection<IResult> productsResults = esalesResponse.FindResults(EsalesResultType.Products);

			if (productsResults == null)
			{
				return new ProductCollection();
			}

			var productHitsToReturn = new Collection<IProductHitInfo>();
			var productKeysDictionary = new Dictionary<string, string>();

			IEnumerable<IProductHitInfo> allProductHits = productsResults.Cast<IProductHits>().SelectMany(ph => ph.ProductsInfo);
			foreach (IProductHitInfo productHitInfo in allProductHits)
			{
				if (productHitsToReturn.Count >= itemsToReturn)
				{
					break;
				}

				if (!productKeysDictionary.ContainsKey(productHitInfo.Key))
				{
					productHitsToReturn.Add(productHitInfo);
					productKeysDictionary[productHitInfo.Key] = productHitInfo.Key;
				}
			}

			if (productHitsToReturn.Count > 0)
			{
				var productIds = new ProductIdCollection(productHitsToReturn.Select(p => EsalesProductKeyService.ParseProductKey(p.Key)));
				productIds.TotalCount = productHitsToReturn.Count;

				ProductCollection products = ProductService.PopulateProducts(context, productIds);

				Dictionary<int, string> tickets = productHitsToReturn.ToDictionary(p => EsalesProductKeyService.ParseProductKey(p.Key), p => p.Ticket);

				foreach (ILekmerProduct product in products)
				{
					product.EsalesTicket = tickets[product.Id];
				}

				return products;
			}

			return new ProductCollection();
		}

		protected virtual void FindCategoryIds(IUserContext context, ProductIdCollection productIds, out List<int> categoryIds, out List<int> parentCategoryIds, out List<int> mainCategoryIds)
		{
			var categoryIdDict = new Dictionary<int, int>();
			var parentCategoryIdDict = new Dictionary<int, int>();
			var mainCategoryIdDict = new Dictionary<int, int>();

			ProductCollection products = ProductService.PopulateProducts(context, productIds);
			ICategoryTree categoryTree = CategoryService.GetAllAsTree(context);

			foreach (IProduct product in products)
			{
				int categoryId = product.CategoryId;
				if (!categoryIdDict.ContainsKey(categoryId))
				{
					categoryIdDict.Add(categoryId, categoryId);

					// parent category id
					ICategoryTreeItem treeItem = categoryTree.FindItemById(categoryId);
					if (treeItem != null && treeItem.Parent != null)
					{
						treeItem = treeItem.Parent;
						if (!parentCategoryIdDict.ContainsKey(treeItem.Id))
						{
							parentCategoryIdDict.Add(treeItem.Id, treeItem.Id);
						}
					}

					// main category id
					if (treeItem != null && treeItem.Parent != null)
					{
						treeItem = treeItem.Parent;
						if (!mainCategoryIdDict.ContainsKey(treeItem.Id))
						{
							mainCategoryIdDict.Add(treeItem.Id, treeItem.Id);
						}
					}
				}
			}

			categoryIds = categoryIdDict.Keys.ToList();
			parentCategoryIds = parentCategoryIdDict.Keys.ToList();
			mainCategoryIds = mainCategoryIdDict.Keys.ToList();
		}

		protected virtual ProductIdCollection CombineProductIds(int productId, ProductIdCollection cartProductIds)
		{
			var productIds = new ProductIdCollection();
			if (productId > 0)
			{
				productIds.Add(productId);
			}
			if (cartProductIds != null)
			{
				productIds.AddRange(cartProductIds);
			}

			return productIds;
		}

		protected virtual List<int> CombineProductIds(ProductIdCollection productIds, ProductCollection recommenedProducts)
		{
			var combineProductIds = new List<int>();

			combineProductIds.AddRange(productIds);
			combineProductIds.AddRange(recommenedProducts.Select(p => p.Id).ToList());

			return combineProductIds;
		}

		protected virtual List<int> CombineProductIds(ProductIdCollection productIds, ProductCollection recommenedProducts, ProductCollection fallBacksProducts)
		{
			var combineProductIds = new List<int>();

			combineProductIds.AddRange(productIds);
			combineProductIds.AddRange(recommenedProducts.Select(p => p.Id).ToList());
			combineProductIds.AddRange(fallBacksProducts.Select(p => p.Id).ToList());

			return combineProductIds;
		}
	}
}