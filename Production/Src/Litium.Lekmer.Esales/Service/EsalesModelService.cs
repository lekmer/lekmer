﻿using System.Linq;
using Litium.Lekmer.Esales.Repository;

namespace Litium.Lekmer.Esales
{
	public class EsalesModelService : IEsalesModelService
	{
		protected EsalesModelRepository Repository { get; private set; }

		public EsalesModelService(EsalesModelRepository repository)
		{
			Repository = repository;
		}

		public virtual IEsalesModelComponent GetModelComponentById(int siteStructureRegistryId, int modelComponentById)
		{
			return EsalesModelComponentCache.Instance.TryGetItem(
				new EsalesModelComponentKey(siteStructureRegistryId, modelComponentById),
				() => GetModelComponentByIdCore(siteStructureRegistryId, modelComponentById));
		}

		protected virtual IEsalesModelComponent GetModelComponentByIdCore(int siteStructureRegistryId, int modelComponentById)
		{
			IEsalesModelComponent esalesModelComponent = Repository.GetModelComponentById(modelComponentById);

			esalesModelComponent.Model.Parameters = Repository.GetModelParameterAllByModel(siteStructureRegistryId, modelComponentById).ToDictionary(p => p.Name);

			esalesModelComponent.Parameters = Repository.GetModelComponentParameterAllByModelComponent(siteStructureRegistryId, modelComponentById).ToDictionary(p => p.Name);

			return esalesModelComponent;
		}
	}
}
