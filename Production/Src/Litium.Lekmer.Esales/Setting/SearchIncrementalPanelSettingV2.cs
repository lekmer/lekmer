﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchIncrementalPanelSettingV2 : EsalesPanelSettingV2, ISearchIncrementalPanelSettingV2
	{
		protected override string GroupName
		{
			get { return "SearchIncrementalPanel-" + ChannelCommonName; }
		}

		public SearchIncrementalPanelSettingV2(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public string AutocompletePanelName
		{
			get { return GetString(GroupName, "AutocompletePanelName"); }
		}
		public string DidYouMeanPanelName
		{
			get { return GetString(GroupName, "DidYouMeanPanelName"); }
		}
		public string ProductSuggestionsPanelName
		{
			get { return GetString(GroupName, "ProductSuggestionsPanelName"); }
		}
		public string CategoriesPanelName
		{
			get { return GetString(GroupName, "CategoriesPanelName"); }
		}
		public string BrandsPanelName
		{
			get { return GetString(GroupName, "BrandsPanelName"); }
		}
	}
}
