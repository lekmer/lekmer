﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Esales.Setting
{
	public abstract class EsalesPanelSettingV2 : SettingBase, IEsalesPanelSetting
	{
		protected string ChannelCommonName { get; set; }

		protected override string StorageName
		{
			get { return "EsalesPanelsV2"; }
		}

		protected abstract string GroupName { get; }

		protected EsalesPanelSettingV2(string channelCommonName)
		{
			ChannelCommonName = channelCommonName;
		}

		public virtual string PanelPath
		{
			get { return GetString(GroupName, "PanelPath"); }
		}
	}
}
