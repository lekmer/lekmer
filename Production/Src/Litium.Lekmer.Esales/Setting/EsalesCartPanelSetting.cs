﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Litium.Lekmer.Esales.Setting
{
	public class EsalesCartPanelSetting : EsalesPanelSettingV2 
	{
		public EsalesCartPanelSetting(string channelCommonName) : base(channelCommonName)
		{
		}

		protected override string GroupName
		{
			get { return string.Format("CartPagePanel-{0}",  ChannelCommonName); }
		}

	}
}
