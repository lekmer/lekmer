﻿namespace Litium.Lekmer.Esales.Setting
{
	public class SearchProductAutocompletePanelSetting : SearchProductPanelSetting, ISearchProductAutocompletePanelSetting
	{
		public SearchProductAutocompletePanelSetting(string channelCommonName)
			: base(channelCommonName)
		{
		}

		public override string PanelPath
		{
			get { return GetString(GroupName, "PanelPathAutocomplete"); }
		}
	}
}
