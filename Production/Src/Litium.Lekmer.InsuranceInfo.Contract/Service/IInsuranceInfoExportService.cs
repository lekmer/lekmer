﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using log4net;

namespace Litium.Lekmer.InsuranceInfo.Contract
{
	public interface IInsuranceInfoExportService
	{
		ILog Log { get; set; }

		IEnumerable<IUserContext> GetUserContextForAllChannels();
		string GetFilePath(string channelCommonName);
		Collection<IInsuranceInfo> GetInsuranceInfoCollectionToExport(IUserContext context);
		void UpdateSendInsuranceInfoFlag(List<int> orderIds);
	}
}