﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Cart.Service
{
	public class SavedCartReminderJob : BaseJob
	{
		private ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Dictionary<int, IUserContext> _contexts = new Dictionary<int, IUserContext>();

		private Collection<IChannel> _channels;
		protected Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = ((ILekmerChannelService)IoC.Resolve<IChannelService>()).GetAll()); }
		}

		private SavedCartReminderScheduleSetting _savedCartReminderScheduleSetting;
		protected SavedCartReminderScheduleSetting SavedCartReminderScheduleSetting
		{
			get { return _savedCartReminderScheduleSetting ?? (_savedCartReminderScheduleSetting = new SavedCartReminderScheduleSetting()); }
		}

		private readonly ISavedCartObjectService _savedCartObjectService = IoC.Resolve<ISavedCartObjectService>();
		private readonly ILekmerCartService _cartService = (ILekmerCartService)IoC.Resolve<ICartService>();
		private readonly ILekmerOrderService _orderService = (ILekmerOrderService) IoC.Resolve<IOrderService>();

		public override string Group
		{
			get { return "Cart"; }
		}

		public override string Name
		{
			get { return "SavedCartReminderJob"; }
		}

		protected override void ExecuteAction()
		{
			var savedCarts = _savedCartObjectService.GetSavedCartForReminder(SavedCartReminderScheduleSetting.SavedCartReminderInDays);
			foreach (var savedCart in savedCarts)
			{
				try
				{
					var isNeedReminder = true;

					var context = GetUserContext(savedCart.ChannelId);

					var cart = _cartService.GetByIdAndCartGuid(context, savedCart.CartId, savedCart.CartGuid);
					var cartItems = cart.GetCartItems();

					var orders = _orderService.GetOrdersByEmailAndDate(context, savedCart.Email, cart.CreatedDate.Date);
					foreach (var order in orders)
					{
						bool isSavedCart = true;
						var orderItems = order.GetOrderItems();
						foreach (var cartItem in cartItems)
						{
							var orderItem = orderItems.FirstOrDefault(i => i.ProductId == cartItem.Product.Id);
							if (orderItem != null) continue;

							isSavedCart = false;
							break;
						}

						if (!isSavedCart) continue;

						isNeedReminder = false;
						break;
					}

					if (isNeedReminder)
					{
						SendMessage(context.Channel, (ILekmerCartFull) cart, savedCart.Email);
						_log.InfoFormat("Reminder for SavedCart with cartId = {0} has been sent.", savedCart.CartId);
						savedCart.IsReminderSent = true;
						savedCart.IsNeedReminder = false;
					}
					else
					{
						_log.InfoFormat("The reminder for SavedCart with cartId = {0} not needed.", savedCart.CartId);
						savedCart.IsReminderSent = false;
						savedCart.IsNeedReminder = false;
					}

					_savedCartObjectService.UpdateSavedCart(savedCart);
				}
				catch (Exception ex)
				{
					var savedCartEmptyException = ex as SavedCartEmptyException;
					if (savedCartEmptyException != null)
					{
						_log.InfoFormat("SavedCart with cartId = {0} is empty", savedCart.CartId);
					}
					else
					{
						_log.ErrorFormat("Error occured while sending reminder for SavedCart with cartId = {0}. Error message: {1}", savedCart.CartId, ex);
					}
				}
			}
		}

		protected IUserContext GetUserContext(int channelId)
		{
			if (_contexts.ContainsKey(channelId))
			{
				return _contexts[channelId];
			}

			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = Channels.FirstOrDefault(c => c.Id == channelId);
			_contexts.Add(channelId, userContext);
			return userContext;
		}

		protected void SendMessage(IChannel channel, ILekmerCartFull cart, string email)
		{
			var messageArgs = new SaveCartReminderMessageArgs(channel, cart, email);
			var messenger = new SaveCartReminderMessenger();
			messenger.Send(messageArgs);
		}
	}
}