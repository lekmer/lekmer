using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Esales.Web
{
	public class BlockEsalesTopSellersV2EntityMapper : LekmerBlockEntityMapper<IBlockEsalesTopSellersV2>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockEsalesTopSellersV2 item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.RowCount", item.Setting.ActualRowCount.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Block.ColumnCount", item.Setting.ActualColumnCount.ToString(CultureInfo.InvariantCulture));

			AddNavigationUrl(fragment, item);
		}

		protected virtual void AddNavigationUrl(Fragment fragment, IBlockEsalesTopSellersV2 item)
		{
			var hasNavigationUrl = false;
			var navigationUrl = string.Empty;

			var customUrl = item.CustomUrl;
			if (!string.IsNullOrEmpty(customUrl))
			{
				hasNavigationUrl = true;
				navigationUrl = customUrl;
			}
			else if (item.LinkContentNodeId.HasValue)
			{
				var contentNode = IoC.Resolve<IContentNodeService>().GetTreeItemById(UserContext.Current, item.LinkContentNodeId.Value);
				if (contentNode != null && !string.IsNullOrEmpty(contentNode.Url))
				{
					hasNavigationUrl = true;
					navigationUrl = UrlHelper.ResolveUrl(contentNode.Url);
				}
			}

			var hasButtonText = false;
			var buttonText = string.Empty;
			if (!string.IsNullOrEmpty(item.UrlTitle))
			{
				hasButtonText = true;
				buttonText = item.UrlTitle;
			}
			
			fragment.AddCondition("Block.HasNavigationUrl", hasNavigationUrl);
			fragment.AddVariable("Block.NavigationUrl", navigationUrl);
			fragment.AddCondition("Block.HasButtonText", hasButtonText);
			fragment.AddVariable("Block.ButtonText", buttonText);
		}
	}
}