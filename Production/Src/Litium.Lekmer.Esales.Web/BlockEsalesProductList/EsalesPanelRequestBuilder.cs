using System.Collections.Generic;
using System.Globalization;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Esales.Web.BlockEsalesProductList
{
	public class EsalesPanelRequestBuilder
	{
		private const string KeyPanelPath = "panel-path";
		private const string DefaultPanelPath = "/category-page-";

		private const string KeyArgPopularWindowLast = "arg-popular-window-last";
		private const string DefaultArgPopularWindowLast = "popular_window_last";

		private IEsalesPanelRequest _panelRequest;

		public IEsalesModelComponent EsalesModelComponent { get; set; }

		public IChannel Channel { get; set; }

		public int ItemsToReturn { get; set; }

		public virtual IEsalesPanelRequest BuildEsalesPanelRequest()
		{
			_panelRequest = new EsalesPanelRequest();

			string panelPath = EsalesModelComponent.Model.TryGetParameterValue(KeyPanelPath, DefaultPanelPath + Channel.Country.Iso.ToLowerInvariant());
			_panelRequest.PanelPath = panelPath;

			_panelRequest.PanelArguments = new Dictionary<string, string>();

			string argPopularWindowLast = EsalesModelComponent.TryGetParameterValue(KeyArgPopularWindowLast, DefaultArgPopularWindowLast);
			_panelRequest.PanelArguments[argPopularWindowLast] = ItemsToReturn.ToString(CultureInfo.InvariantCulture);

			return _panelRequest;
		}
	}
}