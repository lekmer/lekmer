﻿using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Core;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Scensum.Foundation;
using Litium.Framework.Transaction;
using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Esales;
using Litium.Scensum.SiteStructure;
using PrivilegeConstant = Litium.Scensum.Core.PrivilegeConstant;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.ProductFilter
{
	public class BlockEsalesProductFilterSecureService : IBlockEsalesProductFilterSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected BlockEsalesProductFilterRepository Repository { get; private set; }
		protected BlockProductFilterRepository RepositoryFilterBase { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IBlockRatingSecureService BlockRatingSecureService { get; private set; }
		protected IProductTypeSecureService ProductTypeSecureService { get; private set; }
		protected IBlockEsalesSettingSecureService BlockEsalesSettingSecureService { get; private set; }

		public BlockEsalesProductFilterSecureService(
			BlockEsalesProductFilterRepository repository,
			BlockProductFilterRepository repositoryFilterBase,
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IBlockRatingSecureService blockRatingSecureService,
			IProductTypeSecureService productTypeSecureService,
			IBlockEsalesSettingSecureService blockEsalesSettingSecureService)
		{
			Repository = repository;
			RepositoryFilterBase = repositoryFilterBase;
			AccessValidator = accessValidator;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			BlockRatingSecureService = blockRatingSecureService;
			ProductTypeSecureService = productTypeSecureService;
			BlockEsalesSettingSecureService = blockEsalesSettingSecureService;
		}

		public virtual IBlockEsalesProductFilter Create()
		{
			var blockProductFilter = IoC.Resolve<IBlockEsalesProductFilter>();

			blockProductFilter.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductFilter.DefaultBrandIds = new Collection<int>();
			blockProductFilter.DefaultTagIds = new Collection<int>();
			blockProductFilter.TargetProductTypeIds = new Collection<int> { (int)ProductTypeEnum.Product, (int)ProductTypeEnum.Package }; // Target = Both by default
			blockProductFilter.EsalesSetting = BlockEsalesSettingSecureService.Create();

			blockProductFilter.Status = BusinessObjectStatus.New;

			return blockProductFilter;
		}

		public virtual IBlockEsalesProductFilter GetByIdSecure(int blockId)
		{
			var blockProductFilter = Repository.GetByIdSecure(blockId);

			blockProductFilter.DefaultBrandIds = RepositoryFilterBase.GetAllBrandsByBlock(blockId);
			blockProductFilter.DefaultTagIds = RepositoryFilterBase.GetAllTagsByBlock(blockId);
			blockProductFilter.TargetProductTypeIds = ProductTypeSecureService.GetIdAllByBlock(blockId);

			var ratings = BlockRatingSecureService.GetAllByBlock(blockId);
			if (ratings != null && ratings.Count > 0)
			{
				blockProductFilter.BlockRating = ratings[0];
			}

			return blockProductFilter;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockEsalesProductFilter blockEsalesProductFilter)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");
			if (blockEsalesProductFilter == null) throw new ArgumentNullException("blockEsalesProductFilter");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				blockEsalesProductFilter.Id = BlockSecureService.Save(systemUserFull, blockEsalesProductFilter);
				if (blockEsalesProductFilter.Id == -1)
				{
					return blockEsalesProductFilter.Id;
				}

				Repository.Save(blockEsalesProductFilter);

				RepositoryFilterBase.DeleteAllBrands(blockEsalesProductFilter.Id);
				foreach (int brandId in blockEsalesProductFilter.DefaultBrandIds)
				{
					RepositoryFilterBase.SaveBrand(blockEsalesProductFilter.Id, brandId);
				}

				RepositoryFilterBase.DeleteAllTags(blockEsalesProductFilter.Id);
				foreach (int tagId in blockEsalesProductFilter.DefaultTagIds)
				{
					RepositoryFilterBase.SaveTag(blockEsalesProductFilter.Id, tagId);
				}

				ProductTypeSecureService.SaveByBlock(systemUserFull, blockEsalesProductFilter.Id, blockEsalesProductFilter.TargetProductTypeIds);

				var ratings = new Collection<IBlockRating>();
				if (blockEsalesProductFilter.BlockRating != null)
				{
					ratings.Add(blockEsalesProductFilter.BlockRating);
				}
				BlockRatingSecureService.SaveAll(systemUserFull, blockEsalesProductFilter.Id, ratings);

				blockEsalesProductFilter.EsalesSetting.BlockId = blockEsalesProductFilter.Id;
				BlockEsalesSettingSecureService.Save(blockEsalesProductFilter.EsalesSetting);

				transaction.Complete();
			}

			BlockEsalesProductFilterCache.Instance.Flush();

			return blockEsalesProductFilter.Id;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockProductFilter = Create();

			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = contentAreaId;
			blockProductFilter.BlockTypeId = blockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockProductFilter.Title = title;
			blockProductFilter.TemplateId = null;
			blockProductFilter.Id = Save(systemUserFull, blockProductFilter);

			return blockProductFilter;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				BlockEsalesSettingSecureService.Delete(blockId);
				ProductTypeSecureService.DeleteByBlock(systemUserFull, blockId);
				RepositoryFilterBase.DeleteAllBrands(blockId);
				RepositoryFilterBase.DeleteAllTags(blockId);
				BlockRatingSecureService.Delete(systemUserFull, blockId);
				RepositoryFilterBase.Delete(blockId);
				transaction.Complete();
			}

			BlockEsalesProductFilterCache.Instance.Flush();
		}

		public virtual void DeleteTag(ISystemUserFull systemUserFull, int tagId)
		{
			if (systemUserFull == null) throw new ArgumentNullException("systemUserFull");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			RepositoryFilterBase.DeleteTag(tagId);

			BlockEsalesProductFilterCache.Instance.Flush();
		}
	}
}
