using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterProcessor : IFilterProcessor
	{
		private readonly IAgeIntervalService _ageIntervalService;
		private readonly IPriceIntervalService _priceIntervalService;

		public FilterProcessor(IAgeIntervalService ageIntervalService, IPriceIntervalService priceIntervalService)
		{
			_ageIntervalService = ageIntervalService;
			_priceIntervalService = priceIntervalService;
		}

		public FilterResult Process(IUserContext context, IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (products == null) throw new ArgumentNullException("products");
			if (query == null) throw new ArgumentNullException("query");

			var priceIntervals = _priceIntervalService.GetAll(context);
			var ageIntervals = _ageIntervalService.GetAll(context);

			products = FilterProducts(products, query);

			products = SortProducts(products, query);

			products = SortProductsByStockStatus(products);

			var statistics = new FilteredProductRangeStatistics();
			statistics.LoadAgeIntervals(ageIntervals);
			statistics.LoadPriceIntervals(priceIntervals);
			statistics.LoadProducts(products);

			return new FilterResult { Products = products, Statistics = statistics };
		}

		private static IEnumerable<FilterProduct> SortProducts(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			switch (query.SortOption)
			{
				case SortOption.PriceAsc:
					return products.OrderBy(product => product.DiscountPrice); // sort by end price instead of original.
				case SortOption.PriceDesc:
					return products.OrderByDescending(product => product.DiscountPrice);
				case SortOption.TitleDesc:
					// Products are in title order, just need to reverse it.
					return products.Reverse();
				case SortOption.Popularity:
					return products.OrderByDescending(product => product.Popularity);
				case SortOption.IsNewFrom:
					return products.OrderByDescending(product => product.IsNewFrom);
				case SortOption.DiscountPercentDesc:
					return products.OrderByDescending(product => product.DiscountPercent).ThenByDescending(product => product.Popularity);
				default:
					// Title order.
					return products;
			}
		}

		private static IEnumerable<FilterProduct> FilterProducts(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			products = FilterByProductIds(products, query);

			products = FilterByBrandIds(products, query);

			products = FilterByCategoryIds(products, query);

			products = FilterByTags(products, query);

			products = FilterByAgeInterval(products, query);

			products = FilterByPriceInterval(products, query);

			products = FilterBySizeIds(products, query);

			products = FilterByProductTypeIds(products, query);

			return products;
		}

		private static IEnumerable<FilterProduct> FilterByAgeInterval(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.AgeInterval == null)
			{
				return products;
			}

			if (query.AgeInterval.FromMonth.HasValue && query.AgeInterval.ToMonth.HasValue)
			{
				return products
					.Where(p => p.AgeIntervalMatches(query.AgeInterval.FromMonth.Value, query.AgeInterval.ToMonth.Value));
			}

			if (query.AgeInterval.FromMonth.HasValue)
			{
				return products
					.Where(p => p.AgeIntervalMatchesFrom(query.AgeInterval.FromMonth.Value));
			}

			if (query.AgeInterval.ToMonth.HasValue)
			{
				return products
					.Where(p => p.AgeIntervalMatchesTo(query.AgeInterval.ToMonth.Value));
			}

			return products;
		}

		private static IEnumerable<FilterProduct> FilterByPriceInterval(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.PriceInterval == null)
			{
				return products;
			}

			if (query.PriceInterval.From.HasValue && query.PriceInterval.To.HasValue)
			{
				return products
					.Where(p => p.DiscountPriceIsBetween(query.PriceInterval.From.Value, query.PriceInterval.To.Value));
			}

			if (query.PriceInterval.From.HasValue)
			{
				return products
					.Where(p => p.DiscountPriceIsFrom(query.PriceInterval.From.Value));
			}

			if (query.PriceInterval.To.HasValue)
			{
				return products
					.Where(p => p.DiscountPriceIsTo(query.PriceInterval.To.Value));
			}

			return products;
		}

		private static IEnumerable<FilterProduct> FilterByTags(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.GroupedTagIds.Count == 0) return products;

			foreach (IEnumerable<int> tagIds in query.GroupedTagIds)
			{
				if (tagIds == null) continue;

				products = FilterByTagIds(products, tagIds);
			}

			return products;
		}

		private static IEnumerable<FilterProduct> FilterByTagIds(IEnumerable<FilterProduct> products, IEnumerable<int> tagIds)
		{
			return products
				.Where(p =>
				{
					foreach (int tagId in tagIds)
					{
						if (p.HasTag(tagId)) return true;
					}
					return false;
				});
		}

		private static IEnumerable<FilterProduct> FilterByCategoryIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.Level1CategoryIds.Count == 0) return products;

			products = products
				.Where(p =>
				{
					foreach (int categoryId in query.Level1CategoryIds)
					{
						if (p.HasCategory(categoryId)) return true;
					}
					return false;
				});

			return FilterByLevel2And3CategoryIds(products, query);
		}

		private static IEnumerable<FilterProduct> FilterByLevel2And3CategoryIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.Level1CategoryIds.Count != 1 || query.Level2CategoryIds.Count <= 0) return products;

			products = products
				.Where(p =>
				{
					foreach (int categoryId in query.Level2CategoryIds)
					{
						if (p.HasCategory(categoryId)) return true;
					}
					return false;
				});

			return FilterByLevel3CategoryIds(query, products);
		}

		private static IEnumerable<FilterProduct> FilterByLevel3CategoryIds(FilterQuery query, IEnumerable<FilterProduct> products)
		{
			if (query.Level2CategoryIds.Count != 1 || query.Level3CategoryIds.Count <= 0) return products;

			return products
				.Where(p =>
				{
					foreach (int categoryId in query.Level3CategoryIds)
					{
						if (p.HasCategory(categoryId)) return true;
					}
					return false;
				});
		}

		private static IEnumerable<FilterProduct> FilterByBrandIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.BrandIds.Count == 0) return products;

			var brandIdDictionary = query.BrandIds
				.ToDictionary(id => id, id => id);

			return products
				.Where(p => p.BrandId.HasValue && brandIdDictionary.ContainsKey(p.BrandId.Value));
		}

		private static IEnumerable<FilterProduct> FilterByProductIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.ProductIds.Count == 0) return products;

			var productIdDictionary = query.ProductIds
				.ToDictionary(id => id, id => id);

			return products
				.Where(p => productIdDictionary.ContainsKey(p.Id));
		}

		private static IEnumerable<FilterProduct> FilterBySizeIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.SizeIds.Count == 0) return products;

			return products
				.Where(p =>
				{
					foreach (int sizeId in query.SizeIds)
					{
						if (p.HasSize(sizeId)) return true;
					}
					return false;
				});
		}

		private static IEnumerable<FilterProduct> FilterByProductTypeIds(IEnumerable<FilterProduct> products, FilterQuery query)
		{
			if (query.ProductTypeIds.Count == 0) return products;

			var productTypeIdDictionary = query.ProductTypeIds
				.ToDictionary(id => id, id => id);

			return products
				.Where(p => productTypeIdDictionary.ContainsKey(p.ProductTypeId));
		}

		private static IEnumerable<FilterProduct> SortProductsByStockStatus(IEnumerable<FilterProduct> products)
		{
			List<FilterProduct> sortedProducts = new List<FilterProduct>();
			List<FilterProduct> productsOutOfStock = new List<FilterProduct>();

			foreach (FilterProduct product in products)
			{
				if (product.NumberInStock > 0)
					sortedProducts.Add(product);
				else
					productsOutOfStock.Add(product);
			}

			sortedProducts.AddRange(productsOutOfStock);
			return sortedProducts;
		}
	}
}