using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.ProductFilter
{
	public interface IFilterProductService
	{
		Collection<FilterProduct> GetAll(IUserContext context);

		Collection<FilterProduct> GetAllOnline(IUserContext context, ProductIdCollection productIds);

		Collection<FilterProduct> GetAllNoCache(IUserContext context);

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "4#")]
		ProductCollection GetAllByQuery(IUserContext context, FilterQuery query, int pageNumber, int pageSize, out FilteredProductRangeStatistics statistics);

		[SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters")]
		ProductCollection GetAllViewByQuery(IUserContext context, FilterQuery query, int pageNumber, int pageSize, out FilteredProductRangeStatistics statistics);
	}
}