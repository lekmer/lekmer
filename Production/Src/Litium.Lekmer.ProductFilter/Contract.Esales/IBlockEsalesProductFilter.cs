﻿using Litium.Lekmer.Esales;

namespace Litium.Lekmer.ProductFilter
{
	public interface IBlockEsalesProductFilter : IBlockProductFilter
	{
		IBlockEsalesSetting EsalesSetting { get; set; }
	}
}