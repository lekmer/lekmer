using System;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter.Cache;
using Litium.Lekmer.ProductFilter.Repository;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Core;

namespace Litium.Lekmer.ProductFilter
{
	public class BlockEsalesProductFilterService : IBlockEsalesProductFilterService
	{
		protected BlockEsalesProductFilterRepository Repository { get; private set; }
		protected BlockProductFilterRepository RepositoryFilterBase { get; private set; }
		protected IBlockRatingService BlockRatingService { get; private set; }
		protected IProductTypeService ProductTypeService { get; private set; }

		public BlockEsalesProductFilterService(
			BlockEsalesProductFilterRepository repository,
			BlockProductFilterRepository repositoryFilterBase,
			IBlockRatingService blockRatingService,
			IProductTypeService productTypeService)
		{
			Repository = repository;
			RepositoryFilterBase = repositoryFilterBase;
			BlockRatingService = blockRatingService;
			ProductTypeService = productTypeService;
		}

		public virtual IBlockEsalesProductFilter GetById(IUserContext context, int blockId)
		{
			if (context == null) throw new ArgumentNullException("context");

			return BlockEsalesProductFilterCache.Instance.TryGetItem(
				new BlockEsalesProductFilterKey(context.Channel.Id, blockId),
				() => GetByIdCore(context.Channel, blockId));
		}

		protected virtual IBlockEsalesProductFilter GetByIdCore(IChannel channel, int blockId)
		{
			var blockProductFilter = Repository.GetById(channel, blockId);

			if (blockProductFilter != null)
			{
				blockProductFilter.DefaultBrandIds = RepositoryFilterBase.GetAllBrandsByBlock(blockId);
				blockProductFilter.DefaultTagIds = RepositoryFilterBase.GetAllTagsByBlock(blockId);
				blockProductFilter.TargetProductTypeIds = ProductTypeService.GetIdAllByBlock(blockId);
				
				var ratings = BlockRatingService.GetAllByBlock(blockId);
				if (ratings != null && ratings.Count > 0)
				{
					blockProductFilter.BlockRating = ratings[0];
				}
			}

			return blockProductFilter;
		}
	}
}