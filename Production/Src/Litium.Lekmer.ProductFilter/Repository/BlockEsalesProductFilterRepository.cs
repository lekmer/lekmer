﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.ProductFilter.Mapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ProductFilter.Repository
{
	public class BlockEsalesProductFilterRepository
	{
		protected virtual DataMapperBase<IBlockEsalesProductFilter> CreateDataMapper(IDataReader dataReader)
		{
			return new BlockEsalesProductFilterDataMapper(dataReader);
		}

		public IBlockEsalesProductFilter GetByIdSecure(int blockId)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesProductFilterRepository.GetByIdSecure");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
			};

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockEsalesProductFilterGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual int Save(IBlockEsalesProductFilter blockEsalesProductFilter)
		{
			var dbSettings = new DatabaseSetting("BlockEsalesProductFilterRepository.Save");

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@BlockId", blockEsalesProductFilter.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@SecondaryTemplateId", blockEsalesProductFilter.SecondaryTemplateId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@DefaultCategoryId", blockEsalesProductFilter.DefaultCategoryId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@DefaultPriceIntervalId", blockEsalesProductFilter.DefaultPriceIntervalId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@DefaultAgeIntervalId", blockEsalesProductFilter.DefaultAgeIntervalId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@DefaultSizeId", blockEsalesProductFilter.DefaultSizeId, SqlDbType.Int),
				//ParameterHelper.CreateParameter("@ProductListCookie", blockEsalesProductFilter.ProductListCookie, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("@DefaultSort", blockEsalesProductFilter.DefaultSort, SqlDbType.NVarChar)
			};

			var blockProductFilterId = new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pBlockEsalesProductFilterSave]", parameters, dbSettings);

			return blockProductFilterId;
		}

		public virtual IBlockEsalesProductFilter GetById(IChannel channel, int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("BlockId", id, SqlDbType.Int)
			};

			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect(
					"[lekmer].[pBlockEsalesProductFilterGetById]",
					parameters,
					new DatabaseSetting("BlockEsalesProductFilterRepository.GetById")))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}
