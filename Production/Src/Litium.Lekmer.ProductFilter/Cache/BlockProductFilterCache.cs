﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.ProductFilter.Cache
{
	public sealed class BlockProductFilterCache : ScensumCacheBase<BlockProductFilterKey, IBlockProductFilter>
	{
		private static readonly BlockProductFilterCache _instance = new BlockProductFilterCache();

		private BlockProductFilterCache()
		{
		}

		public static BlockProductFilterCache Instance
		{
			get { return _instance; }
		}
	}

	public class BlockProductFilterKey : ICacheKey
	{
		public BlockProductFilterKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }

		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return
					ChannelId.ToString(CultureInfo.InvariantCulture) + "-" +
					BlockId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}