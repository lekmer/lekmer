﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.ProductFilter
{
	public class FilterQuery : IFilterQuery
	{
		private readonly Collection<int> _brandIds = new Collection<int>();
		private readonly Collection<int> _level1CategoryIds = new Collection<int>();
		private readonly Collection<int> _level2CategoryIds = new Collection<int>();
		private readonly Collection<int> _level3CategoryIds = new Collection<int>();
		private readonly Collection<IEnumerable<int>> _groupedTagIds = new Collection<IEnumerable<int>>();
		private readonly Collection<int> _productIds = new Collection<int>();
		private readonly Collection<int> _sizeIds = new Collection<int>();
		private readonly Collection<int> _productTypeds = new Collection<int>();

		public Collection<int> BrandIds
		{
			get { return _brandIds; }
		}

		public Collection<int> Level1CategoryIds
		{
			get { return _level1CategoryIds; }
		}

		public Collection<int> Level2CategoryIds
		{
			get { return _level2CategoryIds; }
		}

		public Collection<int> Level3CategoryIds
		{
			get { return _level3CategoryIds; }
		}

		[SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
		public Collection<IEnumerable<int>> GroupedTagIds
		{
			get { return _groupedTagIds; }
		}

		public IAgeIntervalBand AgeInterval { get; set; }

		public IPriceIntervalBand PriceInterval { get; set; }

		public Collection<int> ProductIds
		{
			get { return _productIds; }
		}

		public SortOption SortOption { get; set; }

		public Collection<int> SizeIds
		{
			get { return _sizeIds; }
		}

		public Collection<int> ProductTypeIds
		{
			get { return _productTypeds; }
		}
	}
}