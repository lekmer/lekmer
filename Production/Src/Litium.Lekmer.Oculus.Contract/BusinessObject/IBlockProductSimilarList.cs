﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Oculus
{
	public interface IBlockProductSimilarList : IBlock
	{
		int PriceRangeType { get; set; }
		IBlockSetting Setting { get; set; }
	}
}