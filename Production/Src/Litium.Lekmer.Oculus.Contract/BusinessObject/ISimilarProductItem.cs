﻿namespace Litium.Lekmer.Oculus
{
	public interface ISimilarProductItem
	{
		int ProductId { get; set; }
		string ProductErpId { get; set; }
		decimal Score { get; set; }
	}
}