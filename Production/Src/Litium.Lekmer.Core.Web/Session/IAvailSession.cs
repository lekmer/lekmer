namespace Litium.Lekmer.Core.Web
{
	/// <summary>
	/// Session information regarding avail.
	/// </summary>
	public interface IAvailSession
	{
		int? AvailProductId { get; set; }
		string TrackingCode { get; set; }
		string AvailSessionId { get; set; }
	}
}