﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector.Repository
{
	public class CollectorPaymentTypeRepository
	{
		protected virtual DataMapperBase<ICollectorPaymentType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICollectorPaymentType>(dataReader);
		}

		public virtual Collection<ICollectorPaymentType> GetAllByChannel(int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CollectorPaymentTypeRepository.GetAllByChannel");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pCollectorPaymentTypeGetAllByChannel]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual int Save(ICollectorPaymentType collectorPaymentType)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CollectorPaymentTypeId", collectorPaymentType.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", collectorPaymentType.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Code", collectorPaymentType.Code, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Description", collectorPaymentType.Description, SqlDbType.NVarChar),

					ParameterHelper.CreateParameter("@StartFee", collectorPaymentType.StartFee, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@InvoiceFee", collectorPaymentType.InvoiceFee, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@MonthlyInterestRate", collectorPaymentType.MonthlyInterestRate, SqlDbType.Decimal),

					ParameterHelper.CreateParameter("@NoOfMonths", collectorPaymentType.NoOfMonths, SqlDbType.Int),
					ParameterHelper.CreateParameter("@MinPurchaseAmount", collectorPaymentType.MinPurchaseAmount, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@LowestPayment", collectorPaymentType.LowestPayment, SqlDbType.Decimal),

					ParameterHelper.CreateParameter("@PartPaymentType", collectorPaymentType.PartPaymentType, SqlDbType.Int),
					ParameterHelper.CreateParameter("@InvoiceType", collectorPaymentType.InvoiceType, SqlDbType.Int),

					ParameterHelper.CreateParameter("@Ordinal", collectorPaymentType.Ordinal, SqlDbType.Int),
				};

			var dbSettings = new DatabaseSetting("CollectorPaymentTypeRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pCollectorPaymentTypeSave]", parameters, dbSettings);
		}

		public virtual void Delete(int collectorPaymentTypeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CollectorPaymentTypeId", collectorPaymentTypeId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CollectorPaymentTypeRepository.Delete");

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorPaymentTypeDelete]", parameters, dbSettings);
		}
	}
}
