﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Payment.Collector.Cache;
using Litium.Lekmer.Payment.Collector.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorPaymentTypeService : ICollectorPaymentTypeService
	{
		protected CollectorPaymentTypeRepository Repository { get; private set; }

		public CollectorPaymentTypeService(CollectorPaymentTypeRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<ICollectorPaymentType> GetAllByChannel(IChannel channel)
		{
			if (channel == null)
			{
				throw new ArgumentNullException("channel");
			}

			return CollectorPaymentTypesCache.Instance.TryGetItem(
				new CollectorPaymentTypeKey(channel.Id),
				() => Repository.GetAllByChannel(channel.Id));
		}

		public virtual ICollectorPaymentType GetByCode(IChannel channel, string code)
		{
			Collection<ICollectorPaymentType> paymentTypes = GetAllByChannel(channel);
			if (paymentTypes == null)
			{
				return null;
			}

			return paymentTypes.FirstOrDefault(p => p.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase));
		}
	}
}
