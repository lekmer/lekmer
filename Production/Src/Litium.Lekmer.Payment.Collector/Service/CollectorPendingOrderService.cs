﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Payment.Collector.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorPendingOrderService : ICollectorPendingOrderService
	{
		protected CollectorPendingOrderRepository Repository { get; private set; }

		public CollectorPendingOrderService(CollectorPendingOrderRepository repository)
		{
			Repository = repository;
		}

		public virtual ICollectorPendingOrder Create()
		{
			var collectorPendingOrder = IoC.Resolve<ICollectorPendingOrder>();

			collectorPendingOrder.FirstAttempt = DateTime.Now;
			collectorPendingOrder.LastAttempt = DateTime.Now;

			return collectorPendingOrder;
		}

		public virtual int Insert(ICollectorPendingOrder collectorPendingOrder)
		{
			Repository.EnsureNotNull();

			return Repository.Insert(collectorPendingOrder);
		}

		public virtual Collection<ICollectorPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkToDate)
		{
			Repository.EnsureNotNull();

			return Repository.GetPendingOrdersForStatusCheck(checkToDate);
		}

		public virtual void Update(ICollectorPendingOrder collectorPendingOrder)
		{
			Repository.EnsureNotNull();

			Repository.Update(collectorPendingOrder);
		}

		public virtual void Delete(int collectorPendingOrderId)
		{
			Repository.EnsureNotNull();

			Repository.Delete(collectorPendingOrderId);
		}
	}
}
