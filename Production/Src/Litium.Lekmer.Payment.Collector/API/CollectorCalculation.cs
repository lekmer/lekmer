﻿using System;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorCalculation : ICollectorCalculation
	{
		public virtual decimal CalculateCheapestMonthlyInvoiceFee()
		{
			throw new NotImplementedException();
		}

		public virtual decimal CalculateMonthlyCost(decimal sum, ICollectorPaymentType paymentType)
		{
			switch (paymentType.PartPaymentType)
			{
				case CollectorPartPaymentType.InterestFree:
					return CalculateMonthlyCostForInterestFree(sum, paymentType);
				case CollectorPartPaymentType.Annuity:
					return CalculateMonthlyCostForAnnuity(sum, paymentType);
				case CollectorPartPaymentType.Account:
					return CalculateMonthlyCostForAccount(sum, paymentType);
				default:
					return 0;
			}
		}

		public virtual decimal CalculateLowestAmountToPay(decimal sum)
		{
			throw new NotImplementedException();
		}


		public virtual decimal CalculateMonthlyCostForInterestFree(decimal sum, ICollectorPaymentType paymentType)
		{
			if (paymentType.PartPaymentType != CollectorPartPaymentType.InterestFree)
			{
				return 0;
			}

			if (paymentType.MinPurchaseAmount > sum)
			{
				return 0;
			}

			if (paymentType.NoOfMonths == 0)
			{
				return 0;
			}

			// (Order sum + Origination fee + (invoice fee * months)) / months
			decimal monthlyCost = (sum + paymentType.StartFee + (paymentType.InvoiceFee * paymentType.NoOfMonths)) / paymentType.NoOfMonths;

			if (monthlyCost < paymentType.LowestPayment)
			{
				monthlyCost = paymentType.LowestPayment;
			}

			//The amount to pay per month should be rounded up to the nearest integer.
			return Math.Ceiling(monthlyCost);
		}

		public virtual decimal CalculateMonthlyCostForAnnuity(decimal sum, ICollectorPaymentType paymentType)
		{
			if (paymentType.PartPaymentType != CollectorPartPaymentType.Annuity)
			{
				return 0;
			}

			if (paymentType.MinPurchaseAmount > sum)
			{
				return 0;
			}

			// CapitalDebt*(MonthlyInterestRate / ((1 + MonthlyInterestRate)^Months - 1)) + CapitalDebt*MonthlyInterestRate + InvoiceFee
			decimal monthlyInterestRate = paymentType.MonthlyInterestRate / 100m;
			decimal monthlyCost = sum * (monthlyInterestRate / ((decimal)Math.Pow(1 + (double)monthlyInterestRate, paymentType.NoOfMonths) - 1)) + sum * monthlyInterestRate + paymentType.InvoiceFee;

			if (monthlyCost < paymentType.LowestPayment)
			{
				monthlyCost = paymentType.LowestPayment;
			}

			//The amount to pay per month should be rounded up to the nearest integer.
			return Math.Ceiling(monthlyCost);
		}

		public virtual decimal CalculateMonthlyCostForAccount(decimal sum, ICollectorPaymentType paymentType)
		{
			if (paymentType.PartPaymentType != CollectorPartPaymentType.Account)
			{
				return 0;
			}

			if (paymentType.NoOfMonths == 0)
			{
				return 0;
			}

			// (product cost / number of months) + (product cost * interest rate) + monthly fee
			// (total purchase amount / number of months) + (total purchase amount * interest rate) + monthly fee
			decimal monthlyCost = (sum / paymentType.NoOfMonths) + (sum * paymentType.MonthlyInterestRate / 100m) + paymentType.InvoiceFee;

			if (monthlyCost < paymentType.LowestPayment)
			{
				monthlyCost = paymentType.LowestPayment;
			}

			//The amount to pay per month should be rounded up to the nearest integer.
			return Math.Ceiling(monthlyCost);
		}
	}
}