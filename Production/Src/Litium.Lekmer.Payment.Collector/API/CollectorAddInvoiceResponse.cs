﻿using System;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorAddInvoiceResponse
	{
		public AddInvoiceResponseV31 Response { get; set; }
		public Exception AddInvoiceException { get; set; }
		public bool IsCompleated { get; set; } 
	}
}
