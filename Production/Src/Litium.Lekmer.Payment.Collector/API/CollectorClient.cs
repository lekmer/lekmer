﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;
using Litium.Lekmer.Payment.Collector.Setting;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template;
using log4net;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorClient : ICollectorClient
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private ICollectorSetting _collectorSetting;
		private IOrderItemConverter _orderProductItemConverter;
		private IOrderItemConverter _orderPackageNiceConverter;
		private IOrderItemConverter _orderPackageRudeConverter;

		protected ICollectorApi CollectorApi { get; set; }

		protected IAliasSharedService AliasSharedService { get; private set; }
		protected IChannelService ChannelService { get; private set; }
		protected ICollectorPaymentTypeService CollectorPaymentTypeService { get; private set; }
		protected IOrderItemConverter OrderProductItemConverter
		{
			get { return _orderProductItemConverter ?? (_orderProductItemConverter = IoC.Resolve<IOrderItemConverter>("OrderProductItemConverter")); }
			set { _orderProductItemConverter = value; }
		}
		protected IOrderItemConverter OrderPackageNiceConverter
		{
			get { return _orderPackageNiceConverter ?? (_orderPackageNiceConverter = IoC.Resolve<IOrderItemConverter>("OrderPackageNiceConverter")); }
			set { _orderPackageNiceConverter = value; }
		}
		protected IOrderItemConverter OrderPackageRudeConverter
		{
			get { return _orderPackageRudeConverter ?? (_orderPackageRudeConverter = IoC.Resolve<IOrderItemConverter>("OrderPackageRudeConverter")); }
			set { _orderPackageRudeConverter = value; }
		}

		public ICollectorSetting CollectorSetting
		{
			get { return _collectorSetting; }
			set
			{
				_collectorSetting = value;
				CollectorApi.CollectorSetting = value;
			}
		}

		public CollectorClient(
			ICollectorApi collectorApi,
			IChannelService channelService,
			IAliasSharedService aliasSharedService,
			ICollectorPaymentTypeService collectorPaymentTypeService
		)
		{
			CollectorApi = collectorApi;
			ChannelService = channelService;
			AliasSharedService = aliasSharedService;
			CollectorPaymentTypeService = collectorPaymentTypeService;
		}

		/// <summary>
		/// Gets an address for a specific civicnumber. Can only be used in Sweden.
		/// </summary>
		public virtual ICollectorGetAddressResult GetAddress(IChannel channel, string civicNumber, string clientIpAddress)
		{
			ValidateArgument(civicNumber);

			civicNumber = FormatCivicNumber(civicNumber, channel);

			ICollectorGetAddressResponse response = CollectorApi.GetAddress(civicNumber, clientIpAddress, channel.Country.Iso);

			return ConvertResponseToGetAddressResult(response);
		}

		/// <summary>
		/// Reserves an amount at Collector.
		/// </summary>
		public virtual ICollectorAddInvoiceResult AddInvoice(IChannel channel, ICustomer customer, IOrderFull order, string paymentTypeId, decimal totalAmount)
		{
			ValidateArguments(customer, order);

			var lekmerOrderFull = (ILekmerOrderFull)order;

			string civicNumber = FormatCivicNumber(customer.CustomerInformation.CivicNumber, channel);
			CollectorGender gender = GetGenterType(channel, customer);
			string currencyCode = channel.Currency.Iso;
			string countryCode = channel.Country.Iso;
			paymentTypeId = paymentTypeId ?? string.Empty;
			string companyReference = ParameterConverter.FormatParameter(((ILekmerOrderAddress)order.BillingAddress).Reference, 64);

			Contract.CollectorInvoiceServiceV31.Address deliveryAddress = MakeAddress(channel, customer, (ILekmerOrderAddress)order.DeliveryAddress);
			Contract.CollectorInvoiceServiceV31.Address billingAddrress = MakeAddress(channel, customer, (ILekmerOrderAddress)order.BillingAddress);

			InvoiceRow[] invoiceRows = MakeInvoiceItems(lekmerOrderFull);

			ValidateInvoiceItems(lekmerOrderFull, invoiceRows, totalAmount);

			CollectorInvoiceType invoiceType = GetInvoiceType(channel, paymentTypeId);

			ICollectorAddInvoiceResult result;
			if (CollectorSetting.Use7SecRule)
			{
				result = CollectorApi.AddInvoiceTimeout(
					order.IP,
					countryCode,
					currencyCode,
					order.CustomerId,
					deliveryAddress,
					gender,
					billingAddrress,
					invoiceRows,
					invoiceType,
					order.CreatedDate,
					order.Id,
					paymentTypeId,
					companyReference,
					civicNumber);
			}
			else
			{
				result = CollectorApi.AddInvoice(
					order.IP,
					countryCode,
					currencyCode,
					order.CustomerId,
					deliveryAddress,
					gender,
					billingAddrress,
					invoiceRows,
					invoiceType,
					order.CreatedDate,
					order.Id,
					paymentTypeId,
					companyReference,
					civicNumber);
			}

			return result;
		}

		public virtual void Initialize(string channelCommonName)
		{
			CollectorApi.Initialize(channelCommonName);
			CollectorSetting.Initialize(channelCommonName);
		}

		private void ValidateArgument(string argument)
		{
			if (argument == null)
			{
				throw new ArgumentNullException("argument");
			}
		}

		/// <summary>
		/// Formats a civicnumber so Kreditor can accept it. In Sweden they accept "-" signs and a year format of 1950. 
		/// In Denmark, Norway and Finland they dont accept year format of 1950, here we need to remove 19. Also "-" signs
		/// are not allowed.
		/// </summary>
		private string FormatCivicNumber(string civicNumber, IChannel channel)
		{
			var countryIso = channel.Country.Iso.ToLower();
			if (string.Compare(countryIso, "se", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "fi", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber;
			}

			if (string.Compare(countryIso, "nl", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "dk", StringComparison.InvariantCultureIgnoreCase) == 0 ||
				string.Compare(countryIso, "no", StringComparison.InvariantCultureIgnoreCase) == 0)
			{
				// It doesn't seem to like - signs.
				civicNumber = civicNumber.Replace(" ", "");
				return civicNumber.Replace("-", "");
			}

			// If country isn't supported, return input string.
			return civicNumber;
		}

		/// <summary>
		/// Converts a Collector response item to a IAddress
		/// </summary>
		private ICollectorGetAddressResult ConvertResponseToGetAddressResult(ICollectorGetAddressResponse response)
		{
			var collectorAddresses = new Collection<ICollectorAddress>();

			if (response.Addresses != null && response.Addresses.Length > 0)
			{
				foreach (BaseAddress address in response.Addresses)
				{
					if (address != null)
					{
						collectorAddresses.Add(new CollectorAddress
						{
							FirstName = response.FirstName,
							LastName = response.LastName,
							StreetAddress = address.Address1,
							StreetAddress2 = address.COAddress,
							PostalCode = address.PostalCode,
							City = address.City,
						});
					}
				}
			}

			var result = new CollectorGetAddressResult
			{
				CollectorAddresses = collectorAddresses,
				StatusCode = response.StatusCode,
				ErrorMessage = response.ErrorMessage,
				ResponseContent = response.ResponseContent,
				Duration = response.Duration,
				TransactionId = response.TransactionId
			};

			return result;
		}

		private void ValidateArguments(ICustomer customer, IOrder order)
		{
			if (customer == null)
			{
				throw new ArgumentNullException("customer");
			}

			if (order == null)
			{
				throw new ArgumentNullException("order");
			}
		}

		private CollectorGender GetGenterType(IChannel channel, ICustomer customer)
		{
			var gender = CollectorGender.Unknown;

			var lekmerCustomerInformation = customer.CustomerInformation as ILekmerCustomerInformation;
			if (lekmerCustomerInformation != null)
			{
				if (string.Compare(channel.Country.Iso.ToLower(), "nl", StringComparison.InvariantCultureIgnoreCase) == 0)
				{
					if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.Man)
					{
						gender = CollectorGender.Male;
					}
					else if (lekmerCustomerInformation.GenderTypeId == (int)GenderTypeInfo.Woman)
					{
						gender = CollectorGender.Female;
					}
				}
			}

			return gender;
		}

		private CollectorInvoiceType GetInvoiceType(IChannel channel, string paymentCode)
		{
			if (paymentCode.IsEmpty())
			{
				return CollectorInvoiceType.Default;
			}

			ICollectorPaymentType collectorPaymentType = CollectorPaymentTypeService.GetByCode(channel, paymentCode);

			return collectorPaymentType == null ? CollectorInvoiceType.Default : collectorPaymentType.InvoiceType;
		}

		private Contract.CollectorInvoiceServiceV31.Address MakeAddress(IChannel channel, ICustomer customer, ILekmerOrderAddress address)
		{
			string firstName = string.Empty;
			string lastName = string.Empty;
			string company = string.Empty;

			if (customer.CustomerInformation.IsCompany())
			{
				company = customer.CustomerInformation.LastName;
			}
			else
			{
				firstName = customer.CustomerInformation.FirstName;
				lastName = customer.CustomerInformation.LastName;
			}

			var collectorAddress = new Contract.CollectorInvoiceServiceV31.Address
			{
				Firstname = ParameterConverter.FormatParameter(firstName, 50),
				Lastname = ParameterConverter.FormatParameter(lastName, 50),
				CompanyName = ParameterConverter.FormatParameter(company, 50),
				Address1 = ParameterConverter.FormatParameter(address.StreetAddress, 50),
				Address2 = ParameterConverter.FormatParameter(address.StreetAddress2, 50),
				COAddress = string.Empty,
				PostalCode = ParameterConverter.FormatParameter(address.PostalCode, 50),
				City = ParameterConverter.FormatParameter(address.City, 50),
				PhoneNumber = ParameterConverter.FormatParameter(address.PhoneNumber, 20),
				CellPhoneNumber = ParameterConverter.FormatParameter(customer.CustomerInformation.CellPhoneNumber, 20),
				Email = ParameterConverter.FormatParameter(customer.CustomerInformation.Email, 260),
				CountryCode = channel.Country.Iso
			};

			return collectorAddress;
		}

		private InvoiceRow[] MakeInvoiceItems(ILekmerOrderFull lekmerOrderFull)
		{
			bool taxFreeZone = lekmerOrderFull.CheckForTaxFreeZone();

			Collection<IOrderItem> orderRowList = lekmerOrderFull.GetOrderItems();
			
			var invoiceItems = new Collection<InvoiceRow>();

			IOrderItemConverter orderPackageItemConverter = OrderPackageNiceConverter; // Nice
			if (CollectorSetting.PackageSendingMode.Equals("Rude", StringComparison.InvariantCultureIgnoreCase))
			{
				orderPackageItemConverter = OrderPackageRudeConverter;
			}

			foreach (ILekmerOrderItem orderRow in orderRowList)
			{
				if (ProductExtensions.IsProduct(orderRow.ProductTypeId))
				{
					invoiceItems.AddRange(OrderProductItemConverter.Convert(orderRow, taxFreeZone));
				}
				else if (ProductExtensions.IsPackage(orderRow.ProductTypeId))
				{
					invoiceItems.AddRange(orderPackageItemConverter.Convert(orderRow, taxFreeZone));
				}
				else
				{
					throw new InvalidOperationException("Unknown product type!");
				}
			}

			MakeDiscount(invoiceItems, lekmerOrderFull, taxFreeZone);

			MakeShipment(invoiceItems, lekmerOrderFull, taxFreeZone);

			return invoiceItems.ToArray();
		}

		private void ValidateInvoiceItems(ILekmerOrderFull lekmerOrderFull, IEnumerable<InvoiceRow> invoiceItems, decimal orderTotalAmount)
		{
			bool taxFreeZone = lekmerOrderFull.CheckForTaxFreeZone();

			if (taxFreeZone)
			{
				decimal itemsAmount = invoiceItems
					.Where(i => i.ArticleId != CollectorSetting.FreightArticleNumber)
					.Where(i => i.ArticleId != CollectorSetting.OptionalFreightArticleNumber)
					.Where(i => i.ArticleId != CollectorSetting.DiapersFreightArticleNumber)
					.Sum(i => i.UnitPrice * i.Quantity);

				if (itemsAmount != lekmerOrderFull.GetActualPriceSummary().ExcludingVat)
				{
					_log.FatalFormat("Order's total amount ({0}) doesn't equel to Collector items amount ({1})!!!", orderTotalAmount, itemsAmount);
				}
			}
			else
			{
				decimal itemsAmount = invoiceItems.Sum(i => i.UnitPrice * i.Quantity);

				if (itemsAmount != orderTotalAmount)
				{
					_log.FatalFormat("Order's total amount ({0}) doesn't equel to Collector items amount ({1})!!!", orderTotalAmount, itemsAmount);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="invoiceItems"></param>
		/// <param name="lekmerOrderFull"></param>
		/// <param name="taxFreeZone"> </param>
		private void MakeDiscount(IEnumerable<InvoiceRow> invoiceItems, ILekmerOrderFull lekmerOrderFull, bool taxFreeZone)
		{
			List<InvoiceRow> invoiceRows = invoiceItems.OrderByDescending(item => item.Quantity).ToList(); // Helps to get correct prices

			decimal orderRowTotalAmount = invoiceRows.Sum(row => row.UnitPrice * row.Quantity);
			decimal discountAmount = CalculateOrderDiscount(lekmerOrderFull, taxFreeZone);

			decimal orderRowTotalExpected = orderRowTotalAmount - discountAmount;

			// To avoid a negativ value on the order total price in HY, voucher is modified so it is never more than order price.
			if (discountAmount > orderRowTotalAmount)
			{
				discountAmount = orderRowTotalAmount;
			}

			// Modify product price on each orderRow with the proper percentage.
			decimal percentage = 0m;
			if (orderRowTotalAmount > 0)
			{
				percentage = discountAmount / orderRowTotalAmount; // 100 / 1200
			}

			foreach (InvoiceRow invoiceRow in invoiceRows)
			{
				decimal oldUnitPrice = invoiceRow.UnitPrice;

				invoiceRow.UnitPrice = invoiceRow.UnitPrice * (1 - percentage);
				invoiceRow.UnitPrice = invoiceRow.UnitPrice.Round();

				//tmpTotalSettlementPrice += invoiceRow.Price * invoiceRow.Quantity;

				orderRowTotalAmount -= oldUnitPrice * invoiceRow.Quantity;
				discountAmount -= (oldUnitPrice - invoiceRow.UnitPrice) * invoiceRow.Quantity;

				if (orderRowTotalAmount > 0)
				{
					percentage = discountAmount / orderRowTotalAmount;
				}
				else
				{
					percentage = 1; // Exit from the loop.
				}
			}

			decimal orderRowTotalAmountActual = invoiceRows.Sum(row => row.UnitPrice * row.Quantity);

			// if orderRowTotalAmountActual is diffrent from orderRowTotalExpected modifyed by voucher, modify last orderrow
			if (orderRowTotalAmountActual > orderRowTotalExpected)
			{
				decimal diff = orderRowTotalAmountActual - orderRowTotalExpected;

				InvoiceRow invoiceRow = invoiceRows.Last(row => row.UnitPrice > 0);
				invoiceRow.UnitPrice = invoiceRow.UnitPrice - diff;

				_log.Warn(string.Format("Collector: orderRowTotalAmountActual differes from orderRowTotalExpected, orderid: [{0}] was modified. Diff was: [{1}]", lekmerOrderFull.Id, diff));
			}
		}

		private void MakeShipment(Collection<InvoiceRow> invoiceItems, ILekmerOrderFull lekmerOrderFull, bool taxFreeZone)
		{
			var channel = (ILekmerChannel)ChannelService.GetById(lekmerOrderFull.ChannelId);

			decimal vatPercentage = channel.VatPercentage.HasValue ? ParameterConverter.FormatParameter(channel.VatPercentage.Value, 0) : 25;

			MakeShipmentDefault(invoiceItems, lekmerOrderFull, taxFreeZone, channel, vatPercentage);

			if (lekmerOrderFull.OptionalDeliveryMethodId.HasValue)
			{
				MakeShipmentOptionalDelivery(invoiceItems, lekmerOrderFull, taxFreeZone, channel, vatPercentage);
			}

			if (lekmerOrderFull.DiapersDeliveryMethodId.HasValue)
			{
				MakeShipmentDiapersDelivery(invoiceItems, lekmerOrderFull, taxFreeZone, channel, vatPercentage);
			}
		}

		private void MakeShipmentDefault(Collection<InvoiceRow> invoiceItems, ILekmerOrderFull lekmerOrderFull, bool taxFreeZone, ILekmerChannel channel, decimal vatPercentage)
		{
			decimal unitPrice = ParameterConverter.FormatParameter(lekmerOrderFull.GetActualFreightCost(), 2);
			decimal vat = vatPercentage;

			if (taxFreeZone)
			{
				unitPrice = PriceCalc.PriceExcludingVat1(unitPrice, vatPercentage / 100.0m);
				vat = 0m;
			}

			var freightItem = new InvoiceRow
			{
				ArticleId = CollectorSetting.FreightArticleNumber,
				Description = ParameterConverter.FormatParameter(GetAliasOrDefault("Order.Collector.DeliveryMethod.InvoiceDescription.Freight", "Freight", channel), 50),
				Quantity = 1,
				UnitPrice = unitPrice,
				VAT = vat
			};

			invoiceItems.Add(freightItem);
		}

		private void MakeShipmentOptionalDelivery(Collection<InvoiceRow> invoiceItems, ILekmerOrderFull lekmerOrderFull, bool taxFreeZone, ILekmerChannel channel, decimal vatPercentage)
		{
			decimal unitPrice = ParameterConverter.FormatParameter(lekmerOrderFull.GetOptionalFreightCost(), 2);
			decimal vat = vatPercentage;

			if (taxFreeZone)
			{
				unitPrice = PriceCalc.PriceExcludingVat1(unitPrice, vatPercentage / 100.0m);
				vat = 0m;
			}

			var optionalFreightItem = new InvoiceRow
			{
				ArticleId = CollectorSetting.OptionalFreightArticleNumber,
				Description = ParameterConverter.FormatParameter(GetAliasOrDefault("Order.Collector.DeliveryMethod.InvoiceDescription.OptionalFreight", "Optional freight", channel), 50),
				Quantity = 1,
				UnitPrice = unitPrice,
				VAT = vat
			};

			invoiceItems.Add(optionalFreightItem);
		}

		private void MakeShipmentDiapersDelivery(Collection<InvoiceRow> invoiceItems, ILekmerOrderFull lekmerOrderFull, bool taxFreeZone, ILekmerChannel channel, decimal vatPercentage)
		{
			decimal unitPrice = ParameterConverter.FormatParameter(lekmerOrderFull.GetDiapersFreightCost(), 2);
			decimal vat = vatPercentage;

			if (taxFreeZone)
			{
				unitPrice = PriceCalc.PriceExcludingVat1(unitPrice, vatPercentage / 100.0m);
				vat = 0m;
			}

			var diapersFreightItem = new InvoiceRow
			{
				ArticleId = CollectorSetting.DiapersFreightArticleNumber,
				Description = ParameterConverter.FormatParameter(GetAliasOrDefault("Order.Collector.DeliveryMethod.InvoiceDescription.DiapersFreight", "Diapers freight", channel), 50),
				Quantity = 1,
				UnitPrice = unitPrice,
				VAT = vat
			};

			invoiceItems.Add(diapersFreightItem);
		}

		private decimal CalculateOrderDiscount(ILekmerOrderFull lekmerOrderFull, bool taxFreeZone)
		{
			IOrderPayment orderPayment = lekmerOrderFull.Payments.FirstOrDefault();
			if (orderPayment == null)
			{
				return 0m;
			}

			decimal orderPaymentPrice = orderPayment.Price;
			decimal baseActualPriceSummary = lekmerOrderFull.GetBaseActualPriceSummary().IncludingVat;

			if (taxFreeZone) // ExclVat
			{
				orderPaymentPrice = orderPayment.Price - orderPayment.Vat;
				baseActualPriceSummary = lekmerOrderFull.GetBaseActualPriceSummary().ExcludingVat;
			}

			decimal discountAmount = orderPaymentPrice - baseActualPriceSummary;
			if (discountAmount >= 0)
			{
				return 0m;
			}

			return discountAmount * (-1);
		}

		private string GetAliasOrDefault(string aliasCommonName, string defaultValue, IChannel channel)
		{
			IAlias alias = AliasSharedService.GetByCommonName(channel, aliasCommonName);
			if (alias != null)
			{
				return alias.Value;
			}

			return defaultValue;
		}
	}
}