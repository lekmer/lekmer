﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorGetAddressResult : CollectorResult, ICollectorGetAddressResult
	{
		public Collection<ICollectorAddress> CollectorAddresses { get; set; }
	}
}