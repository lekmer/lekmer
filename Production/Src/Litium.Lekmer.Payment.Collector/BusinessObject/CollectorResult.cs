﻿namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorResult : ICollectorResult
	{
		public int TransactionId { get; set; }
		public int StatusCode { get; set; }
		public long Duration { get; set; }
		public string ErrorMessage { get; set; }
		public string ResponseContent { get; set; }
		public string FaultCode { get; set; }
		public string FaultMessage { get; set; }
	}
}
