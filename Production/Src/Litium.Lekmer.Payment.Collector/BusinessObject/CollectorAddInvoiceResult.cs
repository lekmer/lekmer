﻿namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorAddInvoiceResult : CollectorResult, ICollectorAddInvoiceResult
	{
		public decimal AvailableReservationAmount { get; set; }
		public string InvoiceNo { get; set; }
		public int InvoiceStatus { get; set; }
		public string InvoiceUrl { get; set; }
	}
}