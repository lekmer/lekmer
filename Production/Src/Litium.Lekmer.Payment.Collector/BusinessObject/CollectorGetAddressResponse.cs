﻿using Litium.Lekmer.Payment.Collector.Contract.CollectorInvoiceServiceV31;

namespace Litium.Lekmer.Payment.Collector
{
	public class CollectorGetAddressResponse : CollectorResult, ICollectorGetAddressResponse
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string CivicNumber { get; set; }
		public BaseAddress[] Addresses { get; set; }
	}
}
