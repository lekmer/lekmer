﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order;
using Litium.Lekmer.Order.Cookie;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Payment;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;
using Channel = Litium.Scensum.Core.Web.Channel;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockDibsResponseControl : BlockControlBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IPaymentProvider _paymentProvider;
		private readonly IContentNodeService _contentNodeService;
		private readonly ILekmerCartSession _cartSession;
		private readonly IOrderSession _orderSession;
		private readonly ILekmerOrderService _orderService;
		private readonly IOrderStatusService _orderStatusService;
		private readonly IOrderItemStatusService _orderItemStatusService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;

		private ILekmerOrderFull _order;
		protected ILekmerOrderFull Order
		{
			get { return _order ?? (_order = (ILekmerOrderFull) _orderService.GetFullById(UserContext.Current, DibsResponse.OrderId)); }
		}

		protected DibsResponse DibsResponse { get; private set; }
		protected bool DibsResponseValid { get; private set; }
		protected bool DibsTransactionApproved { get; private set; }

		public BlockDibsResponseControl(
			ITemplateFactory templateFactory, IBlockService blockService,
			IPaymentProvider paymentProvider, IContentNodeService contentNodeService,
			ICartSession cartSession, IOrderSession orderSession,
			IOrderService orderService, IOrderStatusService orderStatusService, IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService)
			: base(templateFactory, blockService)
		{
			_paymentProvider = paymentProvider;
			_contentNodeService = contentNodeService;
			_cartSession = (ILekmerCartSession) cartSession;
			_orderSession = orderSession;
			_orderService = (ILekmerOrderService)orderService;
			_orderStatusService = orderStatusService;
			_orderItemStatusService = orderItemStatusService;
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "DibsResponse"; }
		}

		protected override BlockContent RenderCore()
		{
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, RenderBody(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}


		private string RenderBody()
		{
			Initialize();

			if (DibsResponseValid == false)
			{
				LogError("Dibs response is not valid.");
				return RenderFailureBody();
			}

			// DibsResponseValid == true

			if (Order == null)
			{
				LogError("Can't find order.");
				return RenderFailureBody();
			}

			if (DibsResponse.Action == "aborted")
			{
				RejectOrder(Order);
				return RenderCancelBody();
			}

			if (DibsTransactionApproved == false)
			{
				RejectOrder(Order);
				return RenderFailureBody();
			}

			// DibsTransactionApproved == true

			ConfirmOrder(Order);
			ResetCart();

			return RenderSuccessBody();
		}

		private string RenderCancelBody()
		{
			Fragment fragment = Template.GetFragment("ContentCancel");

			string redirectUrl = GetRedirectUrl("Checkout");
			fragment.AddVariable("Checkout.Src", redirectUrl);

			return fragment.Render();
		}

		private string RenderFailureBody()
		{
			Fragment fragment = Template.GetFragment("ContentFailure");

			fragment.AddVariable("OrderNo", DibsResponse.OrderId.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("VerifyId", DibsResponse.VerifyId);
			fragment.AddVariable("ReplyText", DibsResponse.ReplyText);

			string redirectUrl = GetRedirectUrl("Checkout");
			fragment.AddVariable("Checkout.Src", redirectUrl);

			return fragment.Render();
		}

		private string RenderSuccessBody()
		{
			Fragment fragment = Template.GetFragment("ContentSuccess");

			string redirectUrl = GetRedirectUrl("CheckoutConfirm");
			fragment.AddVariable("Confirmation.Src", redirectUrl);

			return fragment.Render();
		}


		private void Initialize()
		{
			try
			{
				DibsResponse = new DibsResponse(Request.QueryString);

				DibsResponseValid = DibsResponse.Validate();
			}
			catch (Exception ex)
			{
				LogError("Can't initialize BlockDibsResponseControl. ", ex);
			}

			try
			{
				if (DibsResponseValid)
				{
					var isInitialised = InitializeProviderSetting();
					DibsTransactionApproved = isInitialised && _paymentProvider.IsTransactionApproved(Channel.Current.CommonName, Request.QueryString);
				}
			}
			catch (Exception ex)
			{
				LogError("Can't validate DIBS response. ", ex);

				DibsResponseValid = false;
			}
		}

		private void ConfirmOrder(ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentPending || order.OrderStatus.CommonName == OrderStatusName.RejectedByPaymentProvider)
			{
				_orderService.OrderVoucherConsume(UserContext.Current, order);

				order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(_orderItemStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed));

				if (order.Payments.Count > 0)
				{
					order.Payments.First().ReferenceId = DibsResponse.VerifyId.IsEmpty() ? string.Empty : DibsResponse.VerifyId;
				}

				_orderService.SaveFull(UserContext.Current, order);

				_orderService.UpdateNumberInStock(UserContext.Current, order);
				_giftCardViaMailInfoService.Save(UserContext.Current, order);
				_orderService.SendConfirm(UserContext.Current, order);

				_orderSession.OrderId = order.Id; // Used on confirmation page
			}
			else
			{
				string newReferenceMessage = string.Empty;
				string referenceId = string.Empty;

				if (order.Payments.Count > 0)
				{
					referenceId = order.Payments.First().ReferenceId;
				}

				if (referenceId != DibsResponse.VerifyId && DibsResponse.VerifyId.HasValue())
				{
					if (order.Payments.Count > 0)
					{
						order.Payments.First().ReferenceId = DibsResponse.VerifyId;
						_orderService.SaveFull(UserContext.Current, order);
						newReferenceMessage = " New reference id.";
					}
				}

				if (order.OrderStatus.CommonName == OrderStatusName.PaymentConfirmed || order.OrderStatus.CommonName == OrderStatusName.OrderInHy)
				{
					_orderSession.OrderId = order.Id; // Used on confirmation page
				}
				else
				{
					string message = string.Format("Unable confirm order - status differs from PaymentPending|RejectedByPaymentProvider.{0}", newReferenceMessage);
					LogError(message, order);
				}
			}
		}

		private void RejectOrder(ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentPending)
			{
				_orderService.OrderVoucherRelease(UserContext.Current, order);

				// Order status
				order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.RejectedByPaymentProvider);
				_orderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);

				_orderSession.OrderId = null; // Used on confirmation page
			}
			else if (order.OrderStatus.CommonName == OrderStatusName.RejectedByPaymentProvider)
			{
				_orderSession.OrderId = null; // Used on confirmation page
			}
			else
			{
				LogError("Unable reject order - status differs from PaymentPending.", order);
			}
		}

		private void ResetCart()
		{
			IoC.Resolve<ICartCookieService>().DeleteShoppingCardGuid();
			_cartSession.Clean();
			IoC.Resolve<ICheckoutSession>().CleanProfile();
			IoC.Resolve<IVoucherSession>().Voucher = null;
			IoC.Resolve<IUserContextFactory>().FlushCache();
		}

		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

			if (contentNodeTreeItem != null && contentNodeTreeItem.Url.HasValue())
			{
				return ResolveUrl(contentNodeTreeItem.Url);
			}

			return ResolveUrl("~/");
		}

		private bool InitializeProviderSetting()
		{
			if (Order == null)
			{
				LogError("Can't initialize Dibs settings. Order is NULL.");
				return false;
			}

			if (Order.Payments.Count <= 0)
			{
				LogError("Can't initialize Dibs settings. Payment missing.");
				return false;
			}

			var paymentTypeId = Order.Payments.First().PaymentTypeId;
			if (!Enum.IsDefined(typeof(PaymentMethodType), paymentTypeId))
			{
				LogError(string.Format("Can't initialize Dibs settings. Payment type id: {0} not defined.", paymentTypeId));
				return false;
			}

			PaymentMethodType paymentType = (PaymentMethodType)paymentTypeId;
			var paymentCommonName = paymentType.GetEnumDescription();
			if (string.IsNullOrEmpty(paymentCommonName))
			{
				LogError("Can't initialize Dibs settings. Payment common name missing.");
				return false;
			}

			((ILekmerDibsPaymentProvider)_paymentProvider).InitializeSettings(Channel.Current.CommonName, paymentCommonName);
			return true;
		}

		private void LogError(string message)
		{
			_log.ErrorFormat("{0}\r\nUrl: {1}", message, Request.Url.AbsoluteUri);
		}

		private void LogError(string message, Exception exception)
		{
			string errorMessage = string.Format("{0}\r\nUrl: {1}", message, Request.Url.AbsoluteUri);
			_log.Error(errorMessage, exception);
		}

		private void LogError(string message, ILekmerOrderFull order)
		{
			_log.ErrorFormat("{0}\r\nOrder id: {1}\r\nOrder status: {2}\r\nUrl: {3}", message, order.Id, order.OrderStatus.CommonName, Request.Url.AbsoluteUri);
		}
	}
}
