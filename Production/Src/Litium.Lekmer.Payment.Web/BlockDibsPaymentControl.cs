﻿using System.Linq;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Payment;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockDibsPaymentControl : BlockControlBase
	{
		private readonly IPaymentTypeService _paymentTypeService;
		private readonly IPaymentProvider _paymentProvider;
		private readonly IOrderSession _orderSession;
		private IOrderFull _order;

		private void Initialize()
		{
			_order = GetOrder();
		}

		public BlockDibsPaymentControl(
			ITemplateFactory templateFactory, IBlockService blockService,
			IPaymentTypeService paymentTypeService, IPaymentProvider paymentProvider,
			IOrderSession orderSession)
			: base(templateFactory, blockService)
		{
			_paymentTypeService = paymentTypeService;
			_paymentProvider = paymentProvider;
			_orderSession = orderSession;
		}

		protected override string ModelCommonName
		{
			get { return "DibsPayment"; }
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			string dibsPaymentUrl = null;
			if (_order != null)
			{
				int paymentTypeId = _order.Payments.First().PaymentTypeId;
				IPaymentType paymentType = _paymentTypeService.GetAll(UserContext.Current).SingleOrDefault(type => type.Id == paymentTypeId);
				string dibsRequest = _paymentProvider.CreateRequest(_order.Customer.CustomerInformation, _order, Channel.Current.Currency, paymentType);

				dibsPaymentUrl = dibsRequest + "billingCountry=" + Channel.Current.Country.Title;
			}

			Fragment fragment = Template.GetFragment("Content");
			fragment.AddVariable("DibsPayment.Src", dibsPaymentUrl, VariableEncoding.None);
			fragment.AddCondition("HasOrder", _order != null);

			return new BlockContent("", fragment.Render(), Template.GetFragment("Footer").Render() ?? string.Empty);
		}

		private IOrderFull GetOrder()
		{
			int? orderId = _orderSession.OrderId;
			return orderId.HasValue ? IoC.Resolve<IOrderService>().GetFullById(UserContext.Current, orderId.Value) : null;
		}
	}
}