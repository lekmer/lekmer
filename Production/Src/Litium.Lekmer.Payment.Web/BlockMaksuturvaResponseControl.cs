﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Payment.Maksuturva;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using log4net;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockMaksuturvaResponseControl : BlockControlBase
	{
		private const string HashAmpersand = "&";

		private StringBuilder _hash = new StringBuilder();
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly ILekmerCartSession _cartSession;
		private readonly IOrderSession _orderSession;
		private readonly ILekmerOrderService _orderService;
		private readonly IOrderStatusService _orderStatusService;
		private readonly IOrderItemStatusService _orderItemStatusService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;
		private readonly IContentNodeService _contentNodeService;
		private readonly IMaksuturvaPendingOrderService _maksuturvaPendingOrderService;
		private readonly IMaksuturvaSetting _maksuturvaSetting;

		public BlockMaksuturvaResponseControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ICartSession cartSession,
			IOrderSession orderSession,
			IOrderService orderService,
			IOrderStatusService orderStatusService,
			IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			IContentNodeService contentNodeService,
			IMaksuturvaPendingOrderService maksuturvaPendingOrderService,
			IMaksuturvaSetting maksuturvaSetting)
			: base(templateFactory, blockService)
		{
			_cartSession = (ILekmerCartSession) cartSession;
			_orderSession = orderSession;
			_orderService = (ILekmerOrderService) orderService;
			_orderStatusService = orderStatusService;
			_orderItemStatusService = orderItemStatusService;
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
			_contentNodeService = contentNodeService;
			_maksuturvaPendingOrderService = maksuturvaPendingOrderService;
			_maksuturvaSetting = maksuturvaSetting;
		}

		private int _orderId;
		private int OrderId
		{
			get
			{
				if (_orderId <= 0)
				{
					if (!int.TryParse(Request["pmt_id"], out _orderId))
					{
						throw new ArgumentException("Parameter 'pmt_id' is missing or invalid.");
					}
				}

				return _orderId;
			}
		}

		private ILekmerOrderFull _order;
		private ILekmerOrderFull Order
		{
			get { return _order ?? (_order = (ILekmerOrderFull) _orderService.GetFullById(UserContext.Current, OrderId)); }
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "MaksuturvaResponse"; }
		}

		protected override BlockContent RenderCore()
		{
			LogRequest();
			InitializeSetting();
			ProcessPayment();
			return new BlockContent("", "", "");
		}

		private void ProcessPayment()
		{
			if (Order == null)
			{
				_log.ErrorFormat("Can't find order.\r\nUrl: {0}", Request.Url.AbsoluteUri);
				return;
			}

			bool requestError;
			bool.TryParse(Request["error"], out requestError);

			bool requestCancel;
			bool.TryParse(Request["cancel"], out requestCancel);

			if (requestError || requestCancel)
			{
				UnsuccessfulPayment();
			}

			bool requestDelay;
			bool.TryParse(Request["delay"], out requestDelay);
			if (requestDelay)
			{
				DelayOrder();
			}

			SuccessfulPayment();
		}


		// Unsuccess should check if the order has already been paid or not,
		// if it is customer should actually be redirected to the confirmation page
		private void UnsuccessfulPayment()
		{
			if (OrderIsConfirmed())
			{
				ConfirmOrder(true);
			}
			else
			{
				RejectOrder();
			}
		}

		private void SuccessfulPayment()
		{
			if (OrderIsConfirmed())
			{
				ConfirmOrder(true);
			}
			else
			{
				if (ValidateHash())
				{
					UpdateOrderStatus();
					ConfirmOrder(true);
				}
				else
				{
					RejectOrder();
				}
			}
		}


		// Hash
		private bool ValidateHash()
		{
			AddToHash(Request["pmt_action"]);
			AddToHash(Request["pmt_version"]);
			AddToHash(Request["pmt_id"]);
			AddToHash(Request["pmt_reference"]);
			AddToHash(Request["pmt_amount"]);
			AddToHash(Request["pmt_currency"]);
			AddToHash(Request["pmt_sellercosts"]);
			AddToHash(Request["pmt_paymentmethod"]);
			AddToHash(Request["pmt_escrow"]);
			AddToHash(_maksuturvaSetting.SecretKey);

			var hash = CalculateSha1(_hash.ToString(), Encoding.GetEncoding("UTF-8"));

			return hash == Request["pmt_hash"];
		}
		private void AddToHash(string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				_hash.Append(value);
				_hash.Append(HashAmpersand);
			}
		}
		private string CalculateSha1(string text, Encoding enc)
		{
			var buffer = enc.GetBytes(text);
			var cryptoServiceProvider = new SHA1CryptoServiceProvider();
			var hash = BitConverter.ToString(cryptoServiceProvider.ComputeHash(buffer)).Replace("-", "");
			return hash;
		}

		// Reject order
		private void RejectOrder()
		{
			if (Order.OrderStatus.CommonName == OrderStatusName.PaymentPending)
			{
				_orderService.OrderVoucherRelease(UserContext.Current, Order);

				Order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.RejectedByPaymentProvider);
				_orderService.UpdateOrderStatus(Order.Id, Order.OrderStatus.Id);

				_maksuturvaPendingOrderService.Update(CreateMaksuturvaPendingOrder());
			}
			else
			{
				_log.InfoFormat("Order (orderId={0}) status was not changed because current status not 'PaymentPending'", OrderId.ToString(CultureInfo.InvariantCulture));
			}

			_orderSession.OrderId = null;
			Response.Redirect(GetRedirectUrl("Checkout"));
		}

		// Confirm order
		private void UpdateOrderStatus()
		{
			if (Order.OrderStatus.CommonName == OrderStatusName.PaymentPending || Order.OrderStatus.CommonName == OrderStatusName.RejectedByPaymentProvider)
			{
				Order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);
				Order.SetAllOrderItemsStatus(_orderItemStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed));

				if (Order.Payments.Count > 0)
				{
					string verifyId = Request["pmt_id"];
					Order.Payments.First().ReferenceId = verifyId.IsNullOrEmpty() ? string.Empty : verifyId;
				}

				_orderService.SaveFull(UserContext.Current, Order);
				_orderService.UpdateNumberInStock(UserContext.Current, Order);
				_giftCardViaMailInfoService.Save(UserContext.Current, Order);
				_orderService.SendConfirm(UserContext.Current, Order);

				_orderService.OrderVoucherConsume(UserContext.Current, Order);
			}
			else if (Order.OrderStatus.CommonName != OrderStatusName.PaymentConfirmed && Order.OrderStatus.CommonName != OrderStatusName.OrderInHy)
			{
				string message = string.Format("Unable confirm order {0} - status differs from PaymentPending|RejectedByPaymentProvider.", Order.Id);
				_log.ErrorFormat("{0}\r\nOrder status: {1}\r\nUrl: {2}", message, Order.OrderStatus.CommonName, Request.Url.AbsoluteUri);
			}
		}
		private void ConfirmOrder(bool isNeedDeleteFromPending)
		{
			IoC.Resolve<ICartCookieService>().DeleteShoppingCardGuid();
			_cartSession.Clean();
			IoC.Resolve<ICheckoutSession>().CleanProfile();
			IoC.Resolve<IVoucherSession>().Voucher = null;
			IoC.Resolve<IUserContextFactory>().FlushCache();

			if (isNeedDeleteFromPending)
			{
				_maksuturvaPendingOrderService.Delete(Order.Id);
			}

			_orderSession.OrderId = Order.Id;
			Response.Redirect(GetRedirectUrl("CheckoutConfirm"));
		}

		// Delay order
		private void DelayOrder()
		{
			if (!OrderIsConfirmed())
			{
				Order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentOnHold);

				if (Order.Payments.Count > 0)
				{
					string verifyId = Request["pmt_id"];
					Order.Payments.First().ReferenceId = verifyId.IsNullOrEmpty() ? string.Empty : verifyId;
				}

				_orderService.SaveFull(UserContext.Current, Order);
				_orderService.SendConfirm(UserContext.Current, Order);

				_maksuturvaPendingOrderService.Update(CreateMaksuturvaPendingOrder());
				ConfirmOrder(false);
			}
			else
			{
				ConfirmOrder(true);
			}
		}

		// Log data from request
		private void LogRequest()
		{
			string logRequest = string.Empty;
			foreach (var key in Request.QueryString.AllKeys)
			{
				logRequest += string.Format("{0} - {1}", key, Request.QueryString[key]) + "\r\n";
			}
			_log.InfoFormat("Maksuturva Request Parameters: \r\n {0}", logRequest);
		}

		private bool OrderIsConfirmed()
		{
			bool orderIsAlreadyConfirmed = false;

			if (Order != null)
			{
				string orderRefrerenceId = string.Empty;
				if (Order.Payments.Count > 0)
				{
					orderRefrerenceId = Order.Payments.First().ReferenceId;
				}

				if (Order.OrderStatus.CommonName == OrderStatusName.PaymentConfirmed && orderRefrerenceId.HasValue())
				{
					orderIsAlreadyConfirmed = true;
				}
			}

			return orderIsAlreadyConfirmed;
		}
		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = _contentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);

			if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
			{
				return UrlHelper.ResolveUrlHttps(contentNodeTreeItem.Url);
			}

			return UrlHelper.BaseUrlHttp;
		}

		private void InitializeSetting()
		{
			var channel = IoC.Resolve<IChannelService>().GetById(Order.ChannelId);
			_maksuturvaSetting.Initialize(channel.CommonName);
		}

		private IMaksuturvaPendingOrder CreateMaksuturvaPendingOrder()
		{
			var pendingOrder = _maksuturvaPendingOrderService.Get(OrderId);
			pendingOrder.StatusId = Order.OrderStatus.Id;
			pendingOrder.NextAttempt = _maksuturvaSetting.GetOrderCheckDate(Order.OrderStatus.CommonName, Order.CreatedDate, Order.CreatedDate);
			return pendingOrder;
		}
	}
}