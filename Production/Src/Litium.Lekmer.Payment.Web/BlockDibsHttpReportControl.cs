﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Payment;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using log4net;
using Channel = Litium.Scensum.Core.Web.Channel;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Payment.Web
{
	public class BlockDibsHttpReportControl : BlockControlBase
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly IPaymentProvider _paymentProvider;
		private readonly ILekmerOrderService _orderService;
		private readonly IOrderStatusService _orderStatusService;
		private readonly IOrderItemStatusService _orderItemStatusService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;

		private ILekmerOrderFull _order;
		protected ILekmerOrderFull Order
		{
			get { return _order ?? (_order = (ILekmerOrderFull) _orderService.GetFullById(UserContext.Current, DibsResponse.OrderId)); }
		}

		protected DibsResponse DibsResponse { get; private set; }
		protected bool DibsResponseValid { get; private set; }
		protected bool DibsTransactionApproved { get; private set; }

		public BlockDibsHttpReportControl(
			ITemplateFactory templateFactory, IBlockService blockService,
			IPaymentProvider paymentProvider, IOrderService orderService,
			IOrderStatusService orderStatusService, IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService)
			: base(templateFactory, blockService)
		{
			_paymentProvider = paymentProvider;
			_orderService = (ILekmerOrderService)orderService;
			_orderStatusService = orderStatusService;
			_orderItemStatusService = orderItemStatusService;
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
		}

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected override string ModelCommonName
		{
			get { return "DibsHttpReport"; }
		}

		protected override BlockContent RenderCore()
		{
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, RenderBody(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}


		private string RenderBody()
		{
			Initialize();

			if (DibsResponseValid == false)
			{
				LogError("Dibs response is not valid.");
				return RenderSuccessBody();
			}

			// DibsResponseValid == true

			if (Order == null)
			{
				LogError("Can't find order.");
				return RenderSuccessBody();
			}

			if (DibsTransactionApproved == false)
			{
				RejectOrder(Order);
				return RenderSuccessBody();
			}

			// DibsTransactionApproved == true

			ConfirmOrder(Order);

			return RenderSuccessBody();
		}

		private string RenderSuccessBody()
		{
			return "<!-- VerifyEasy_response:_OK -->";
		}


		private void Initialize()
		{
			try
			{
				DibsResponse = new DibsResponse(Request.Form);

				DibsResponseValid = DibsResponse.Validate();
			}
			catch (Exception ex)
			{
				LogError("Can't initialize BlockDibsResponseControl. ", ex);
			}

			try
			{
				if (DibsResponseValid)
				{
					var isInitialised = InitializeProviderSetting();
					DibsTransactionApproved = isInitialised && _paymentProvider.IsTransactionApproved(Channel.Current.CommonName, Request.Form);
				}
			}
			catch (Exception ex)
			{
				LogError("Can't validate DIBS response. ", ex);

				DibsResponseValid = false;
			}
		}

		private void ConfirmOrder(ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentPending || order.OrderStatus.CommonName == OrderStatusName.RejectedByPaymentProvider)
			{
				_orderService.OrderVoucherConsume(UserContext.Current, order);

				order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(_orderItemStatusService.GetByCommonName(UserContext.Current, OrderStatusName.PaymentConfirmed));

				if (order.Payments.Count > 0)
				{
					order.Payments.First().ReferenceId = DibsResponse.VerifyId.IsEmpty() ? string.Empty : DibsResponse.VerifyId;
				}

				_orderService.SaveFull(UserContext.Current, order);

				_orderService.UpdateNumberInStock(UserContext.Current, order);
				_giftCardViaMailInfoService.Save(UserContext.Current, order);
				_orderService.SendConfirm(UserContext.Current, order);
			}
			else
			{
				string newReferenceMessage = string.Empty;
				string referenceId = string.Empty;

				if (order.Payments.Count > 0)
				{
					referenceId = order.Payments.First().ReferenceId;
				}

				if (referenceId != DibsResponse.VerifyId && DibsResponse.VerifyId.HasValue())
				{
					if (order.Payments.Count > 0)
					{
						order.Payments.First().ReferenceId = DibsResponse.VerifyId;
						_orderService.SaveFull(UserContext.Current, order);
						newReferenceMessage = " New reference id.";
					}
				}

				string message = string.Format("Unable confirm order - status differs from PaymentPending|RejectedByPaymentProvider.{0}", newReferenceMessage);

				if (order.OrderStatus.CommonName == OrderStatusName.PaymentConfirmed || order.OrderStatus.CommonName == OrderStatusName.OrderInHy)
				{
					LogWarning(message, order);
				}
				else
				{
					LogError(message, order);
				}
			}
		}

		private void RejectOrder(ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentPending)
			{
				_orderService.OrderVoucherRelease(UserContext.Current, order);

				// Order status
				order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.RejectedByPaymentProvider);
				_orderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);
			}
			else if (order.OrderStatus.CommonName == OrderStatusName.RejectedByPaymentProvider)
			{
				// Do nothing
			}
			else
			{
				if (DibsResponse.ReplyText.Equals("Authentication Timed Out", StringComparison.InvariantCultureIgnoreCase))
				{
					LogWarning("Unable reject order - status differs from PaymentPending.", order);
				}
				else
				{
					LogError("Unable reject order - status differs from PaymentPending.", order);
				}
			}
		}

		private bool InitializeProviderSetting()
		{
			if (Order == null)
			{
				LogError("Can't initialize Dibs settings. Order is NULL.");
				return false;
			}

			if (Order.Payments.Count <= 0)
			{
				LogError("Can't initialize Dibs settings. Payment missing.");
				return false;
			}

			var paymentTypeId = Order.Payments.First().PaymentTypeId;
			if (!Enum.IsDefined(typeof (PaymentMethodType), paymentTypeId))
			{
				LogError(string.Format("Can't initialize Dibs settings. Payment type id: {0} not defined.", paymentTypeId));
				return false;
			}

			PaymentMethodType paymentType = (PaymentMethodType)paymentTypeId;
			var paymentCommonName = paymentType.GetEnumDescription();
			if (string.IsNullOrEmpty(paymentCommonName))
			{
				LogError("Can't initialize Dibs settings. Payment common name missing.");
				return false;
			}

			((ILekmerDibsPaymentProvider)_paymentProvider).InitializeSettings(Channel.Current.CommonName, paymentCommonName);
			return true;
		}

		private void LogError(string message)
		{
			_log.ErrorFormat("{0}\r\nUrl: {1}\r\nForm data: \r\n{2}", message, Request.Url.AbsoluteUri, LogRequestForm());
		}

		private void LogError(string message, Exception exception)
		{
			string errorMessage = string.Format("{0}\r\nUrl: {1}\r\nForm data: \r\n{2}", message, Request.Url.AbsoluteUri, LogRequestForm());
			_log.Error(errorMessage, exception);
		}

		private void LogError(string message, ILekmerOrderFull order)
		{
			_log.ErrorFormat("{0}\r\nOrder id: {1}\r\nOrder status: {2}\r\nUrl: {3}\r\nForm data: \r\n{4}", message, order.Id, order.OrderStatus.CommonName, Request.Url.AbsoluteUri, LogRequestForm());
		}

		private void LogWarning(string message, ILekmerOrderFull order)
		{
			_log.WarnFormat("{0}\\r\nOrder id: {1}r\nOrder status: {2}\r\nUrl: {3}\r\nForm data: \r\n{4}", message, order.Id, order.OrderStatus.CommonName, Request.Url.AbsoluteUri, LogRequestForm());
		}

		private string LogRequestForm()
		{
			var sb = new StringBuilder();

			foreach (string key in Request.Form.AllKeys)
			{
				sb.AppendFormat("{0}: {1}", key, Request.Form[key]);
				sb.AppendLine();
			}

			return sb.ToString();
		}
	}
}