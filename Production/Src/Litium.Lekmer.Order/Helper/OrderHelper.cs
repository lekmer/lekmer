using Litium.Lekmer.Core;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class OrderHelper
	{
		public virtual void RenderOrderPrices(Fragment fragment, ILekmerOrderFull item, ILekmerFormatter formatter, IChannel channel)
		{
			Price actualPriceTotal = item.GetActualPriceTotal();
			decimal actualVat = item.GetActualVatTotal();

			// price with channel decimals / possible rounding
			fragment.AddVariable("Order.TotalCostIncludingVat", formatter.FormatPrice(channel, actualPriceTotal.IncludingVat));
			fragment.AddVariable("Order.TotalCostExcludingVat", formatter.FormatPrice(channel, actualPriceTotal.ExcludingVat));
			fragment.AddVariable("Order.TotalVat", formatter.FormatPrice(channel, actualVat));

			// price with decimals / without rounding
			fragment.AddVariable("Order.TotalCostIncludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPriceTotal.IncludingVat));
			fragment.AddVariable("Order.TotalCostExcludingVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPriceTotal.ExcludingVat));
			fragment.AddVariable("Order.TotalVat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualVat));
		}
	}
}