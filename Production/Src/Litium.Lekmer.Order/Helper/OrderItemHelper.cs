using System.Linq;
using System.Reflection;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using log4net;
using UserContext = Litium.Scensum.Core.Web.UserContext;

namespace Litium.Lekmer.Order
{
	public class OrderItemHelper
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private ILekmerProductService _productService;
		protected ILekmerProductService ProductService
		{
			get { return _productService ?? (_productService = (ILekmerProductService) IoC.Resolve<IProductService>()); }
		}

		public void AddProductAndWarehouseInfo(Fragment fragment, int productId)
		{
			var product = (ILekmerProduct) ProductService.GetById(UserContext.Current, productId);

			if (product == null)
			{
				_log.ErrorFormat("Product {0} can not be found.", productId);
				fragment.AddCondition("OrderItem.IsAbove60L", false);
				return;
			}

			var isPackage = product.IsPackage();

			if (isPackage)
			{
				ProductCollection packageProducts = GetPackageProductCollection(product);
				fragment.AddCondition("OrderItem.IsAbove60L", packageProducts.Any(p => product.IsAbove60L));
			}
			else
			{
				fragment.AddCondition("OrderItem.IsAbove60L", product.IsAbove60L);
			}
		}

		protected virtual ProductCollection GetPackageProductCollection(ILekmerProduct item)
		{
			var productIds = ProductService.GetIdAllByPackageMasterProduct(UserContext.Current, item.Id);
			return ProductService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
		}

		public virtual void RenderOrderItemPrices(Fragment fragment, IOrderItem item, ILekmerFormatter formatter, IChannel channel)
		{
			decimal actualPriceIncludingVat = item.ActualPrice.IncludingVat;
			decimal priceSummaryIncludingVat = item.ActualPrice.IncludingVat * item.Quantity;
			decimal priceSummaryExcludingVat = item.ActualPrice.ExcludingVat * item.Quantity;

			// price with channel decimals / possible rounding
			fragment.AddVariable("OrderItem.Price", formatter.FormatPrice(channel, actualPriceIncludingVat));
			fragment.AddVariable("OrderItem.PriceSummary", formatter.FormatPrice(channel, priceSummaryIncludingVat));
			fragment.AddVariable("OrderItem.Vat", formatter.FormatPrice(channel, priceSummaryIncludingVat - priceSummaryExcludingVat));

			// price with decimals / without rounding
			fragment.AddVariable("OrderItem.Price_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, actualPriceIncludingVat));
			fragment.AddVariable("OrderItem.PriceSummary_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, priceSummaryIncludingVat));
			fragment.AddVariable("OrderItem.Vat_Formatted", formatter.FormatPriceTwoOrLessDecimals(channel, priceSummaryIncludingVat - priceSummaryExcludingVat));
		}
	}
}