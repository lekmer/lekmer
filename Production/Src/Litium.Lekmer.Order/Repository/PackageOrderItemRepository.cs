﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	[Serializable]
	public class PackageOrderItemRepository
	{
		protected virtual DataMapperBase<IPackageOrderItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IPackageOrderItem>(dataReader);
		}

		public virtual Collection<IPackageOrderItem> GetAllByOrder(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageOrderItemRepository.GetAllByOrder");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pPackageOrderItemGetAllByOrder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IPackageOrderItem> GetAllByOrderItem(int orderItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderItemId", orderItemId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("PackageOrderItemRepository.GetAllByOrderItem");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pPackageOrderItemGetAllByOrderItem]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Delete(int orderItemId)
		{
			IDataParameter[] parameters = 
			{
				ParameterHelper.CreateParameter("OrderItemId", orderItemId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("PackageOrderItemRepository.Delete");
			new DataHandler().ExecuteCommand("[orderlek].[pPackageOrderItemDelete]", parameters, dbSettings);
		}

		public virtual int Save(IPackageOrderItem packageOrderItem)
		{
			IDataParameter[] parameters = 
			{
				ParameterHelper.CreateParameter("PackageOrderItemId", packageOrderItem.PackageOrderItemId, SqlDbType.Int),
				ParameterHelper.CreateParameter("OrderItemId", packageOrderItem.OrderItemId, SqlDbType.Int),
				ParameterHelper.CreateParameter("OrderId", packageOrderItem.OrderId, SqlDbType.Int),
				ParameterHelper.CreateParameter("ProductId", packageOrderItem.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("Quantity", packageOrderItem.Quantity, SqlDbType.Int),
				ParameterHelper.CreateParameter("PackagePriceIncludingVat", packageOrderItem.PackagePrice.IncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("ActualPriceIncludingVat", packageOrderItem.ActualPrice.IncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("OriginalPriceIncludingVat", packageOrderItem.OriginalPrice.IncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("VAT", packageOrderItem.GetActualVatAmount(), SqlDbType.Decimal),
				ParameterHelper.CreateParameter("ErpId", packageOrderItem.ErpId, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("EanCode", packageOrderItem.EanCode, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("Title", packageOrderItem.Title, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("OrderItemStatusId", packageOrderItem.OrderItemStatus.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("SizeId", packageOrderItem.SizeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("SizeErpId", packageOrderItem.SizeErpId, SqlDbType.VarChar)
			};
			var dbSettings = new DatabaseSetting("PackageOrderItemRepository.Save");
			packageOrderItem.PackageOrderItemId = new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pPackageOrderItemSave]", parameters, dbSettings);
			return packageOrderItem.PackageOrderItemId;
		}
	}
}