using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class SavedCartObjectRepository
	{
		protected virtual DataMapperBase<ISavedCartObject> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISavedCartObject>(dataReader);
		}

		/// <summary>
		/// Save 'saved cart'
		/// </summary>
		public virtual void InsertSavedCart(ISavedCartObject savedCart)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartId", savedCart.CartId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", savedCart.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Email", savedCart.Email, SqlDbType.VarChar)
				};
			var dbSettings = new DatabaseSetting("SavedCartObjectRepository.InsertSavedCart");
			new DataHandler().ExecuteCommand("[orderlek].[pSavedCartSave]", parameters, dbSettings);
		}

		/// <summary>
		/// Update 'saved cart'
		/// </summary>
		public virtual void UpdateSavedCart(ISavedCartObject savedCart)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartId", savedCart.CartId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IsReminderSent", savedCart.IsReminderSent, SqlDbType.Bit),
					ParameterHelper.CreateParameter("IsNeedReminder", savedCart.IsNeedReminder, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("SavedCartObjectRepository.UpdateSavedCart");
			new DataHandler().ExecuteCommand("[orderlek].[pSavedCartUpdate]", parameters, dbSettings);
		}

		/// <summary>
		/// Get 'saved cart' for reminder
		/// </summary>
		public virtual Collection<ISavedCartObject> GetSavedCartForReminder(int savedCartReminderInDays)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SavedCartReminderInDays", savedCartReminderInDays, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SavedCartObjectRepository.GetSavedCartForReminder");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pSavedCartGetForReminder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}