using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerPaymentTypeRepository : PaymentTypeRepository
	{
		[Obsolete("Obsolete logic.", true)]
		public override IPaymentType GetById(int paymentTypeId)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		public virtual IPaymentType GetByIdSecure(int paymentTypeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("PaymentTypeId", paymentTypeId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerPaymentTypeRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pPaymentTypeGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}