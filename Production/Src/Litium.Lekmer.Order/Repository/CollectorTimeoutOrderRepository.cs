﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order
{
	public class CollectorTimeoutOrderRepository
	{
		protected virtual DataMapperBase<ICollectorTimeoutOrder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICollectorTimeoutOrder>(dataReader);
		}

		public virtual Collection<ICollectorTimeoutOrder> GetCollectorTimeoutOrders()
		{
			var dbSettings = new DatabaseSetting("CollectorTimeoutOrderRepository.GetCollectorTimeoutOrders");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pCollectorTimeoutOrderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Update(int transactionId)
		{
			var dbSettings = new DatabaseSetting("CollectorTimeoutOrderRepository.Update");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("TransactionId", transactionId, SqlDbType.Int)
				};

			new DataHandler().ExecuteCommand("[orderlek].[pCollectorTimeoutOrderUpdate]", parameters, dbSettings);
		}
	}
}