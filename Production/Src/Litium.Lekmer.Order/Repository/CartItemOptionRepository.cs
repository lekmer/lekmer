using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class CartItemOptionRepository
	{
		protected virtual DataMapperBase<ICartItemOption> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemOption>(dataReader);
		}

		public virtual void Save(ICartItemOption cartItemOption)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartItemOptionId", cartItemOption.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CartId", cartItemOption.CartId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", cartItemOption.Product.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeId", cartItemOption.SizeId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Quantity", cartItemOption.Quantity, SqlDbType.Int),
					ParameterHelper.CreateParameter("CreatedDate", cartItemOption.CreatedDate, SqlDbType.DateTime)
				};

			var dbSettings = new DatabaseSetting("CartItemOptionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemOptionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartItemOptionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartItemOptionId", cartItemOptionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartItemOptionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pCartItemOptionDelete]", parameters, dbSettings);
		}

		public virtual Collection<ICartItemOption> GetAllByCart(int cartId, ICustomer customer, int channelId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartId", cartId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CustomerId", customer != null ? (int?)customer.Id : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartItemOptionRepository.GetAllByCart");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCartItemOptionGetAllByCart]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}