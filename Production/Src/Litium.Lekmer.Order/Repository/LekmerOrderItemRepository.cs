﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	[Serializable]
	public class LekmerOrderItemRepository : OrderItemRepository
	{
		public override int Save(IOrderItem orderItem)
		{
			var lekmerOrderItem = (ILekmerOrderItem) orderItem;

			IDataParameter[] parameters = 
			{
				ParameterHelper.CreateParameter("@OrderItemId", orderItem.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@OrderId", orderItem.OrderId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Quantity", orderItem.Quantity, SqlDbType.Int),
				ParameterHelper.CreateParameter("@OriginalPriceIncludingVat", orderItem.OriginalPrice.IncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@ActualPriceIncludingVat", orderItem.ActualPrice.IncludingVat, SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@Vat", orderItem.GetActualVatAmount(), SqlDbType.Decimal),
				ParameterHelper.CreateParameter("@ProductId", orderItem.ProductId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ErpId", orderItem.ErpId, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@EanCode", orderItem.EanCode, SqlDbType.VarChar),
				ParameterHelper.CreateParameter("@Title", orderItem.Title, SqlDbType.NVarChar),
				ParameterHelper.CreateParameter("@OrderItemStatusId", orderItem.OrderItemStatus.Id, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ProductTypeId", lekmerOrderItem.ProductTypeId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@IsDropShip", lekmerOrderItem.IsDropShip, SqlDbType.Bit)
			};
			var dbSettings = new DatabaseSetting("LekmerOrderItemRepository.Save");
			orderItem.Id = new DataHandler().ExecuteCommandWithReturnValue("[order].[pOrderItemSave]", parameters, dbSettings);
			return orderItem.Id;
		}
	}
}