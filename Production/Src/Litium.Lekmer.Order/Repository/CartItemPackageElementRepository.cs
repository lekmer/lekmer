using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class CartItemPackageElementRepository
	{
		protected virtual DataMapperBase<ICartItemPackageElement> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemPackageElement>(dataReader);
		}

		public virtual int Save(ICartItemPackageElement cartItemPackageElement)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartItemPackageElementId", cartItemPackageElement.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("CartItemId", cartItemPackageElement.CartItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", cartItemPackageElement.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeId", cartItemPackageElement.SizeId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartItemPackageElementRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[orderlek].[pCartItemPackageElementSave]", parameters, dbSettings);
		}

		public virtual void DeleteByCartItem(int cartItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartItemId", cartItemId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartItemPackageElementRepository.DeleteByCartItem");

			new DataHandler().ExecuteCommand("[orderlek].[pCartItemPackageElementDeleteByCartItem]", parameters, dbSettings);
		}

		public virtual Collection<ICartItemPackageElement> GetAllByCart(int cartId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartId", cartId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CartItemPackageElementRepository.GetAllByCart");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pCartItemPackageElementGetAllByCart]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}