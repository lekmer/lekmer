using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Repository
{
	public class OrderItemSizeRepository
	{
		public void Save(IUserContext context, IOrderItemSize orderItemSize)
		{
			IDataParameter[] parameters = {
				ParameterHelper.CreateParameter("@OrderItemId", orderItemSize.OrderItemId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@SizeId", orderItemSize.SizeId.Value, SqlDbType.Int),
				ParameterHelper.CreateParameter("@ErpId", orderItemSize.ErpId, SqlDbType.VarChar, 50)
			};
			var dbSettings = new DatabaseSetting("LekmerOrderItemRepository.Save");
			new DataHandler().ExecuteCommand("lekmer.pOrderItemSizeSave", parameters, dbSettings);
		}

		public void Delete(IOrderItemSize orderItemSize)
		{
			IDataParameter[] parameters = 
			{
				ParameterHelper.CreateParameter("@OrderItemId", orderItemSize.OrderItemId, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("OrderItemSizeRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pOrderItemSizeDelete]", parameters, dbSettings);
		}
	}
}