using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order.Repository
{
	public class LekmerDeliveryMethodRepository : DeliveryMethodRepository
	{
		public virtual Collection<IDeliveryMethod> GetAll()
		{
			IDataParameter[] parameters = { };

			var dbSettings = new DatabaseSetting("LekmerDeliveryMethodRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDeliveryMethodGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual IDeliveryMethod GetOptionalByDeliveryMethod(int channelId, int deliveryMethodId)
		{
			var parameters = new IDataParameter[2]
			{
				ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@DeliveryMethodId", deliveryMethodId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerDeliveryMethodRepository.GetOptionalByDeliveryMethod");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[orderlek].[pDeliveryMethodGetOptionalByDeliveryMethod]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}