﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class SubPaymentTypeService : ISubPaymentTypeService
	{
		protected SubPaymentTypeRepository Repository { get; private set; }

		public SubPaymentTypeService(SubPaymentTypeRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<ISubPaymentType> GetByPayment(int paymentTypeId)
		{
			return SubPaymentTypeCollectionCache.Instance.TryGetItem(
				new SubPaymentTypeCollectionKey(paymentTypeId),
				delegate { return GetByPaymentCore(paymentTypeId); });
		}

		protected virtual Collection<ISubPaymentType> GetByPaymentCore(int paymentId)
		{
			return Repository.GetByPayment(paymentId);
		}
	}
}