﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Xml;
using Litium.Framework.Messaging;
using Litium.Framework.Transaction;
using Litium.Lekmer.Avail.Web.Helper;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Esales;
using Litium.Lekmer.NewsletterSubscriber;
using Litium.Lekmer.Order.Repository;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;
using System.Linq;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Cache;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderService : OrderService, ILekmerOrderService
	{
		protected LekmerOrderRepository _lekmerRepository;
		protected IProductService _productService;
		protected IProductSizeService _productSizeService;
		protected ILekmerOrderSharedService _lekmerOrderSharedService;
		protected IVoucherService VoucherService { get; private set; }
		protected IProductChangesService _productChangesService;
		protected IEsalesNotifyService _eSalesNotifyService;
		protected ITrustPilotSetting _trustPilotSetting { get; set; }
		protected IOrderVoucherInfoService OrderVoucherInfoService { get; private set; }
		protected IPackageService PackageService { get; private set; }
		protected INewsletterUnsubscriberService NewsletterUnsubscriberService { get; private set; }

		public LekmerOrderService(
			OrderRepository repository,
			IChannelService channelService,
			ICustomerService customerService,
			IOrderAddressService orderAddressService,
			IOrderPaymentService orderPaymentService,
			IOrderItemService orderItemService,
			IOrderCartCampaignService campaignService,
			IProductChangesService productChangesService,
			IEsalesNotifyService eSalesNotifyService,
			ITrustPilotSetting trustPilotSetting,
			IVoucherService voucherService,
			IOrderVoucherInfoService orderVoucherInfoService,
			IPackageService packageService,
			INewsletterUnsubscriberService newsletterUnsubscriberService)
			: base(
				repository,
				channelService,
				customerService,
				orderAddressService,
				orderPaymentService,
				orderItemService,
				campaignService)
		{
			_lekmerRepository = IoC.Resolve<LekmerOrderRepository>();
			_productService = IoC.Resolve<IProductService>();
			_productSizeService = IoC.Resolve<IProductSizeService>();
			_lekmerOrderSharedService = IoC.Resolve<ILekmerOrderSharedService>();
			VoucherService = voucherService;
			_productChangesService = productChangesService;
			_eSalesNotifyService = eSalesNotifyService;
			_trustPilotSetting = trustPilotSetting;
			OrderVoucherInfoService = orderVoucherInfoService;
			PackageService = packageService;
			NewsletterUnsubscriberService = newsletterUnsubscriberService;
		}


		public override int Save(IUserContext context, IOrderFull order)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				order.Id = base.Save(context, order);
				_lekmerOrderSharedService.Save(order);
				UpdateOrderNumber(context, order);

				transactedOperation.Complete();
			}
			return order.Id;
		}

		public override int SaveFull(IUserContext context, IOrderFull order)
		{
			if (order == null)
				throw new ArgumentNullException("order");
			if (CustomerService == null)
				throw new InvalidOperationException("CustomerService must be set before calling Save.");
			if (OrderItemService == null)
				throw new InvalidOperationException("OrderItemService must be set before calling Save.");
			if (ChannelService == null)
				throw new InvalidOperationException("ChannelService must be set before calling Save.");
			if (OrderAddressService == null)
				throw new InvalidOperationException("OrderAddressService must be set before calling Save.");
			if (OrderPaymentService == null)
				throw new InvalidOperationException("OrderPaymentService must be set before calling Save.");
			if (OrderVoucherInfoService == null)
				throw new InvalidOperationException("OrderVoucherInfoService must be set before calling Save.");

			using (var transactedOperation = new TransactedOperation())
			{
				order.CustomerId = CustomerService.Save(context, order.Customer);

				order.BillingAddressId = OrderAddressService.Save(context, order.BillingAddress);

				if (order.DeliveryAddress != null && order.DeliveryAddress.Id != order.BillingAddress.Id)
				{
					order.DeliveryAddressId = OrderAddressService.Save(context, order.DeliveryAddress);
				}
				else
				{
					order.MergeWith(order.BillingAddress, order.BillingAddress);
					order.DeliveryAddressId = order.BillingAddressId;
				}

				var lekmerOrder = (ILekmerOrderFull)order;
				lekmerOrder.AlternateAddressId = lekmerOrder.AlternateAddress != null
					? (int?) OrderAddressService.Save(context, lekmerOrder.AlternateAddress)
					: null;
				lekmerOrder.CivicNumber = order.Customer.CustomerInformation.CivicNumber;

				order.Id = Save(context, order);

				var orderItems = order.GetOrderItems();
				foreach (IOrderItem orderItem in orderItems)
				{
					orderItem.OrderId = order.Id;
					orderItem.Id = OrderItemService.Save(context, orderItem);
				}

				foreach (IOrderPayment payment in order.Payments)
				{
					payment.OrderId = order.Id;
					payment.Id = OrderPaymentService.Save(context, payment);
				}

				if (order.CampaignInfo != null)
				{
					OrderCartCampaignService.Save(context, order.Id, order.CampaignInfo);
				}

				if (lekmerOrder.VoucherInfo != null)
				{
					OrderVoucherInfoService.Save(lekmerOrder.Id, lekmerOrder.VoucherInfo);
				}

				transactedOperation.Complete();
			}
			return order.Id;
		}

		public virtual int SaveFullExtended(IUserContext context, IOrderFull order)
		{
			int orderId;
			using (var transactedOperation = new TransactedOperation())
			{
				orderId = SaveFull(context, order);

				transactedOperation.Complete();
			}

			var lekmerOrder = (ILekmerOrderFull)order;

			if (lekmerOrder.VoucherInfo != null && lekmerOrder.VoucherInfo.VoucherCode.HasValue())
			{
				VoucherService.VoucherReserve(lekmerOrder.VoucherInfo.VoucherCode);

				lekmerOrder.VoucherInfo.VoucherStatus = (int)OrderVoucherStatus.Reserved;

				UpdateVoucherStatus(order);
			}

			return orderId;
		}


		public virtual void UpdateNumberInStock(IUserContext context, IOrderFull order)
		{
			NotifyEsales(context, order);

			AvailLogPurchase(order);

			// grouping by pair: productId, sizeId. sizeId is nullable.
			var groupedOrderItems = order.GetOrderItems().GroupBy(item => string.Format("{0}-{1}-{2}", item.ProductId, ((ILekmerOrderItem)item).OrderItemSize.SizeId, ((ILekmerOrderItem)item).PackageOrderItems.GetPackageElementsHashCode()));
			foreach (var groupedOrderItem in groupedOrderItems)
			{
				var orderItem = (ILekmerOrderItem) groupedOrderItem.First();

				var productId = orderItem.ProductId;
				var quantity = groupedOrderItem.Sum(item => item.Quantity);

				if (ProductExtensions.IsProduct(orderItem.ProductTypeId))
				{
					var sizeId = orderItem.OrderItemSize.SizeId;
					if (sizeId.HasValue)
					{
						_lekmerRepository.UpdateNumberInStock(productId, sizeId.Value, quantity);
						RemoveFromCache(productId, sizeId.Value);
					}
					else
					{
						_lekmerRepository.UpdateNumberInStock(productId, quantity);
						RemoveFromCache(productId);
					}
					TrackProductChanges(context, order, productId);
				}
				else if (ProductExtensions.IsPackage(orderItem.ProductTypeId))
				{
					var itemWithoutSizes = new Collection<int>();
					var itemWithSizes = new Collection<IPackageElement>();
					orderItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);

					if (itemWithSizes == null || itemWithSizes.Count == 0)
					{
						_lekmerRepository.UpdatePackageWithoutSizesNumberInStock(productId, quantity);

						foreach (var item in orderItem.PackageOrderItems)
						{
							RemoveFromCache(item.ProductId);
							TrackProductChanges(context, order, item.ProductId);
						}
						RemoveFromCache(productId);
						TrackProductChanges(context, order, productId);
					}
					else
					{
						foreach (var item in itemWithoutSizes)
						{
							RemoveFromCache(item);
							TrackProductChanges(context, order, item);
						}

						foreach (var item in itemWithSizes)
						{
							RemoveFromCache(item.ProductId);
							TrackProductChanges(context, order, item.ProductId);
						}

						_lekmerRepository.UpdatePackageWithSizesNumberInStock(productId, quantity, CreatePackageItemsWithSizesXml(itemWithSizes), Convert.ToStringIdentifierList(itemWithoutSizes), Convert.DefaultListSeparator);

						RemoveFromCache(productId);
						TrackProductChanges(context, order, productId);
					}
				}
			}
		}

		public virtual bool CheckNumberInStock(IUserContext context, IOrderFull order)
		{
			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var groupedOrderItems = order.GetOrderItems().GroupBy(item => string.Format("{0}-{1}-{2}", item.ProductId, ((ILekmerOrderItem)item).OrderItemSize.SizeId, ((ILekmerOrderItem)item).PackageOrderItems.GetPackageElementsHashCode()));
			foreach (var groupedOrderItem in groupedOrderItems)
			{
				var orderItem = (ILekmerOrderItem)groupedOrderItem.First();

				var productId = orderItem.ProductId;
				var quantity = groupedOrderItem.Sum(item => item.Quantity);
				var sizeId = orderItem.OrderItemSize.SizeId;

				var itemWithoutSizes = new Collection<int>();
				var itemWithSizes = new Collection<IPackageElement>();
				orderItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);

				if (!CheckQuantity(context, productId, sizeId, quantity, itemWithoutSizes, itemWithSizes)) return false;
			}
			return true;
		}

		public virtual bool CheckNumberInStock(IUserContext context, ICartFull cart)
		{
			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var groupedCartItems = cart.GetCartItems().GroupBy(item => string.Format("{0}-{1}-{2}", item.Product.Id, ((ILekmerCartItem)item).SizeId, ((ILekmerCartItem)item).PackageElements.GetPackageElementsHashCode()));
			foreach (var groupedCartItem in groupedCartItems)
			{
				var lekmerCartItem = (ILekmerCartItem)groupedCartItem.First();

				var quantity = groupedCartItem.Sum(item => item.Quantity);
				var productId = lekmerCartItem.Product.Id;
				var sizeId = lekmerCartItem.SizeId;

				var itemWithoutSizes = new Collection<int>();
				var itemWithSizes = new Collection<IPackageElement>();
				lekmerCartItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);

				if (!CheckQuantity(context, productId, sizeId, quantity, itemWithoutSizes, itemWithSizes)) return false;
			}
			return true;
		}

		public virtual bool CheckNumberInStock(IUserContext context, ICartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems)
		{
			foreach (var groupedCartItem in groupedCartItems)
			{
				var lekmerCartItem = (ILekmerCartItem) groupedCartItem.First();
				var productId = lekmerCartItem.Product.Id;
				if (cartItem.Product.Id == productId)
				{
					var quantity = groupedCartItem.Sum(item => item.Quantity);
					var sizeId = lekmerCartItem.SizeId;

					var itemWithoutSizes = new Collection<int>();
					var itemWithSizes = new Collection<IPackageElement>();
					lekmerCartItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);

					return CheckQuantity(context, productId, sizeId, quantity, itemWithoutSizes, itemWithSizes);
				}
			}
			return true;
		}

		public virtual int GetAvaliableNumberInStock(IUserContext context, int productId, int? sizeId, Collection<int> itemWithoutSizes, Collection<IPackageElement> itemWithSizes)
		{
			var product = (ILekmerProduct)_productService.GetById(context, productId);
			return GetAvaliableNumberInStock(product, sizeId, itemWithoutSizes, itemWithSizes);
		}

		public virtual int GetAvaliableNumberInStock(ILekmerProduct product, int? sizeId, Collection<int> itemWithoutSizes, Collection<IPackageElement> itemWithSizes)
		{
			var numberInStock = 0;

			if (product.IsProduct())
			{
				numberInStock = sizeId.HasValue ? _productSizeService.GetById(product.Id, sizeId.Value).NumberInStock : product.NumberInStock;
			}

			if (product.IsPackage())
			{
				if (itemWithSizes == null || itemWithSizes.Count == 0)
				{
					numberInStock = product.NumberInStock;
				}
				else
				{
					numberInStock = PackageService.GetNumberInStockForPackageWithSizes(CreatePackageItemsWithSizesXml(itemWithSizes), Convert.ToStringIdentifierList(itemWithoutSizes), Convert.DefaultListSeparator);
				}
			}

			return numberInStock;
		}


		public override Collection<IOrder> GetAllByCustomer(IUserContext context, int customerId)
		{
			var ordersFull = Repository.GetAllByCustomer<IOrderFull>(customerId);
			var orders = new Collection<IOrder>();
			foreach (var orderFull in ordersFull)
			{
				FillOrder(context, orderFull);
				orders.Add(orderFull);
			}
			return orders;
		}


		public virtual void SetFeedbackToken(int orderId, Guid feedbackToken)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				_lekmerRepository.SetFeedbackToken(orderId, feedbackToken);

				transactedOperation.Complete();
			}
		}

		public virtual IOrderFull GetByFeedbackToken(IUserContext context, Guid feedbackToken)
		{
			var order = _lekmerRepository.GetByFeedbackToken(feedbackToken);

			if (order != null)
			{
				FillOrder(context, order);
			}
			return order;
		}

		public virtual IOrderFull GetByKcoId(IUserContext context, string kcoId)
		{
			return _lekmerRepository.GetByKcoId(kcoId);
		}

		public virtual IOrderFull GetFullByKcoId(IUserContext context, string kcoId)
		{
			var order = GetByKcoId(context, kcoId);
			if (order != null)
			{
				FillOrder(context, order);
			}
			return order;
		}


		public virtual Collection<IOrderFull> GetLatest(DateTime date)
		{
			return _lekmerRepository.GetLatest(date);
		}

		public virtual Collection<IOrder> GetToSendInsuranceInfo(IUserContext context, int maxDaysAfterPurchase, int minDaysAfterPurchase)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			var ordersFull = _lekmerRepository.GetToSendInsuranceInfo(context.Channel.Id, maxDaysAfterPurchase, minDaysAfterPurchase);
			var orders = new Collection<IOrder>();
			foreach (var orderFull in ordersFull)
			{
				FillOrder(context, orderFull);
				orders.Add(orderFull);
			}

			return orders;
		}

		public virtual Collection<IOrder> GetToSendGfkStatistics(IUserContext context, string startDate, string endDate)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}

			var ordersFull = _lekmerRepository.GetToSendGfkStatistics(context.Channel.Id, startDate, endDate);
			var orders = new Collection<IOrder>();
			foreach (var orderFull in ordersFull)
			{
				FillOrder(context, orderFull);
				orders.Add(orderFull);
			}

			return orders;
		}

		public virtual Collection<IOrder> GetToSendPacsoftInfo(IUserContext context)
		{
			if (context == null) throw new ArgumentNullException("context");

			var ordersFull = _lekmerRepository.GetToSendPacsoftInfo(context.Channel.Id);
			var orders = new Collection<IOrder>();
			foreach (var orderFull in ordersFull)
			{
				FillOrder(context, orderFull);
				orders.Add(orderFull);
			}

			return orders;
		}

		public virtual Collection<IOrder> GetOrders(IUserContext context, DateTime date, int minutesAfterPurchase)
		{
			var ordersFull = _lekmerRepository.GetOrders(date, minutesAfterPurchase);
			var orders = new Collection<IOrder>();
			foreach (var orderFull in ordersFull)
			{
				FillOrder(context, orderFull);
				orders.Add(orderFull);
			}
			return orders;
		}

		public virtual Collection<IOrderFull> GetOrdersByEmailAndDate(IUserContext context, string email, DateTime date)
		{
			var ordersFull = _lekmerRepository.GetOrdersByEmailAndDate(email, date);
			var orders = new Collection<IOrderFull>();
			foreach (var orderFull in ordersFull)
			{
				FillOrder(context, orderFull);
				orders.Add(orderFull);
			}
			return orders;
		}

		public virtual void UpdateSendInsuranceInfoFlag(List<int> orderIds)
		{
			if (orderIds == null)
			{
				throw new ArgumentNullException("orderIds");
			}

			_lekmerRepository.UpdateSendInsuranceInfoFlag(Convert.ToStringIdentifierList(orderIds), Convert.DefaultListSeparator);
		}

		public override void SendConfirm(IUserContext context, IOrderFull order)
		{
			IoC.Resolve<IMessenger<IOrderFull>>("OrderFullMessenger").Send(order);

			if (_trustPilotSetting.Active(context.Channel))
			{
				bool emailInBanList = false;

				// check if customer unsubscribed from review emails
				var newsletterUnsubscriber = NewsletterUnsubscriberService.GetByEmail(context, order.Email);
				if (newsletterUnsubscriber != null)
				{
					if (newsletterUnsubscriber.UnsubscriberOptions.Any(o => o.NewsletterTypeId == (int)NewsletterType.OrderReview))
					{
						emailInBanList = true;
					}
				}

				if (emailInBanList == false)
				{
					IoC.Resolve<IMessenger<IOrderFull>>("TrustPilotOrderFullMessenger").Send(order);
				}
			}
		}

		public virtual void SendRejectMail(IUserContext context, IOrderFull order)
		{
			IoC.Resolve<IMessenger<IOrderFull>>("OrderRejectMessenger").Send(order);
		}

		public virtual void UpdateOrderStatus(int orderId, int orderStatusId)
		{
			_lekmerRepository.UpdateOrderStatus(orderId, orderStatusId);
		}


		public virtual void UpdateVoucherStatus(IOrderFull order)
		{
			var lekmerOrder = (ILekmerOrderFull)order;
			if (lekmerOrder.VoucherInfo != null)
			{
				OrderVoucherInfoService.UpdateStatus(lekmerOrder.Id, lekmerOrder.VoucherInfo);
			}
		}

		/// <summary>
		/// Release voucher and update order voucher state.
		/// </summary>
		public virtual void OrderVoucherRelease(IUserContext context, ILekmerOrderFull order)
		{
			VoucherService.EnsureNotNull();

			var lekmerUserContext = (ILekmerUserContext)context;

			if (lekmerUserContext.Voucher != null)
			{
				VoucherService.VoucherRelease(lekmerUserContext.Voucher.VoucherCode);
			}

			if (order.VoucherInfo != null && order.VoucherInfo.VoucherCode.HasValue())
			{
				VoucherService.VoucherRelease(order.VoucherInfo.VoucherCode);

				order.VoucherInfo.VoucherStatus = (int)OrderVoucherStatus.Released;

				UpdateVoucherStatus(order);
			}
		}

		/// <summary>
		/// Consume voucher and update order voucher state
		/// </summary>
		/// <param name="context"></param>
		/// <param name="order"></param>
		public virtual void OrderVoucherConsume(IUserContext context, ILekmerOrderFull order)
		{
			VoucherService.EnsureNotNull();

			if (order.VoucherInfo != null && order.VoucherInfo.VoucherCode.HasValue())
			{
				VoucherService.VoucherConsume(
					order.VoucherInfo.VoucherCode,
					order.Id,
					order.VoucherInfo.AmountUsed.HasValue ? order.VoucherInfo.AmountUsed.Value : 0.0m,
					context.Channel.ApplicationName);

				order.VoucherInfo.VoucherStatus = (int)OrderVoucherStatus.Consumed;

				UpdateVoucherStatus(order);
			}
		}



		private void UpdateOrderNumber(IUserContext context, IOrderFull order)
		{
			order.Number = order.Id.ToString(CultureInfo.InvariantCulture);
			base.Save(context, order);
		}

		private void RemoveFromCache(int productId)
		{
			var channels = ((LekmerChannelService)ChannelService).GetAll();
			foreach (var channel in channels)
			{
				ProductCache.Instance.Remove(new ProductKey(channel.Id, productId));
				ProductViewCache.Instance.Remove(new ProductKey(channel.Id, productId));
			}
		}

		private void RemoveFromCache(int productId, int sizeId)
		{
			ProductSizeCache.Instance.Remove(new ProductSizeKey(productId, sizeId));
			ProductSizeCollectionCache.Instance.Remove(new ProductSizeCollectionKey(productId));

			var channels = ((LekmerChannelService)ChannelService).GetAll();
			foreach (var channel in channels)
			{
				ProductViewCache.Instance.Remove(new ProductKey(channel.Id, productId));
			}
		}

		private bool CheckQuantity(IUserContext context, int productId, int? sizeId, int quantity, Collection<int> itemWithoutSizes, Collection<IPackageElement> itemWithSizes)
		{
			var product = (ILekmerProduct)_productService.GetById(context, productId);
			var numberInStock = GetAvaliableNumberInStock(product, sizeId, itemWithoutSizes, itemWithSizes);
			if (numberInStock == 0 && product.IsBookable)
			{
				return true;
			}
			return numberInStock >= quantity;
		}

		protected override void FillOrder(IUserContext context, IOrderFull order)
		{
			if (OrderItemService == null)
				throw new InvalidOperationException("OrderItemService must be set before calling FillOrder.");
			if (OrderPaymentService == null)
				throw new InvalidOperationException("OrderPaymentService must be set before calling FillOrder.");
			if (OrderAddressService == null)
				throw new InvalidOperationException("OrderAddressService must be set before calling FillOrder.");

			foreach (IOrderItem orderItem in OrderItemService.GetAllByOrder(context, order.Id))
			{
				order.AddOrderItem(orderItem);
			}
			order.MergeWith(OrderPaymentService.GetAllByOrderId(context, order.Id));
			order.MergeWith(OrderAddressService.GetById(context, order.BillingAddressId),
							OrderAddressService.GetById(context, order.DeliveryAddressId));
			order.Customer = CustomerService.GetById(context, order.CustomerId);

			var lekmerOrder = (ILekmerOrderFull) order;
			lekmerOrder.MergeWith(lekmerOrder.AlternateAddressId.HasValue
								? OrderAddressService.GetById(context, lekmerOrder.AlternateAddressId.Value) as ILekmerOrderAddress
			                	: null);

			lekmerOrder.VoucherInfo = OrderVoucherInfoService.GetByOrderId(lekmerOrder.Id);
		}

		protected virtual void TrackProductChanges(IUserContext context, IOrderFull order, int productId)
		{
			// Track product changes
			IProductChangeEvent productChangeEvent = _productChangesService.Create(productId, "Stock reducing. Order " + order.Id.ToString(CultureInfo.InvariantCulture));
			_productChangesService.Insert(productChangeEvent);
		}

		protected virtual void NotifyEsales(IUserContext context, IOrderFull order)
		{
			_eSalesNotifyService.NotifyPayment(order);
		}

		protected virtual void AvailLogPurchase(IOrderFull order)
		{
			var availLoggingHelper = new AvailLoggingHelper();
			availLoggingHelper.SendAvailLogPurchase(order);
		}

		protected virtual string CreatePackageItemsWithSizesXml(Collection<IPackageElement> items)
		{
			var doc = new XmlDocument();
			XmlNode itemsNode = AddNode(doc, null, "items");

			foreach (var item in items)
			{
				XmlNode itemNode = AddNode(doc, itemsNode, "item");
				AddAttribute(doc, itemNode, "productId", item.ProductId.ToString(CultureInfo.InvariantCulture));
				AddAttribute(doc, itemNode, "sizeId", item.SizeId.Value.ToString(CultureInfo.InvariantCulture));
			}

			return doc.OuterXml;
		}

		private XmlNode AddNode(XmlDocument xmlDocument, XmlNode parentNode, string elementName)
		{
			XmlNode node = xmlDocument.CreateElement(elementName);

			if (parentNode != null)
			{
				parentNode.AppendChild(node);
			}
			else
			{
				xmlDocument.AppendChild(node);
			}

			return node;
		}

		private void AddAttribute(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlAttribute attribute = xmlDocument.CreateAttribute(elementName);
			attribute.Value = elementValue;
			if (parentNode.Attributes != null)
			{
				parentNode.Attributes.Append(attribute);
			}
		}
	}
}