﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Collector;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Order
{
	public class CollectorTimeoutOrderService : ICollectorTimeoutOrderService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Collection<IChannel> _channels;

		protected CollectorTimeoutOrderRepository Repository { get; private set; }
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ILekmerOrderService OrderService { get; private set; }
		protected IOrderStatusService OrderStatusService { get; private set; }
		protected IOrderItemStatusService OrderItemStatusService { get; private set; }
		protected IGiftCardViaMailInfoService GiftCardViaMailInfoService { get; private set; }
		protected ICollectorTransactionService CollectorTransactionService { get; private set; }

		public CollectorTimeoutOrderService(
			CollectorTimeoutOrderRepository repository,
			IChannelService channelService,
			IOrderService orderService,
			IOrderStatusService orderStatusService,
			IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			ICollectorTransactionService collectorTransactionService
		)
		{
			Repository = repository;
			ChannelService = (ILekmerChannelService)channelService;
			OrderService = (ILekmerOrderService)orderService;
			OrderStatusService = orderStatusService;
			OrderItemStatusService = orderItemStatusService;
			GiftCardViaMailInfoService = giftCardViaMailInfoService;
			CollectorTransactionService = collectorTransactionService;
		}

		public virtual void ManageCollectorTimeoutOrders()
		{
			// Get collector timeout orders
			Collection<ICollectorTimeoutOrder> timeoutOrders = Repository.GetCollectorTimeoutOrders();
			foreach (var timeoutOrder in timeoutOrders)
			{
				try
				{
					ManageOrder(timeoutOrder);
				}
				catch (Exception ex)
				{
					LogError("Unable manage collector timeout order.", ex, timeoutOrder.OrderId);
				}
			}
		}


		protected virtual void ManageOrder(ICollectorTimeoutOrder collectorTimeoutOrder)
		{
			IUserContext userContext = GetUserContext(collectorTimeoutOrder.ChannelId);

			var order = (ILekmerOrderFull)OrderService.GetFullById(userContext, collectorTimeoutOrder.OrderId);

			var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsCollectorPayment(p.PaymentTypeId));
			if (orderPayment == null)
			{
				throw new InvalidOperationException("Order does not contain any Collector payments!");
			}

			if (collectorTimeoutOrder.InvoiceNo.HasValue())
			{
				orderPayment.ReferenceId = collectorTimeoutOrder.InvoiceNo;
			}

			if (collectorTimeoutOrder.StatusCode.HasValue == false)
			{
				throw new InvalidOperationException("Collector timeout order does not contain status code!");
			}

			CollectorTransactionStatus statusCode = collectorTimeoutOrder.StatusCode.Value;

			if (statusCode == CollectorTransactionStatus.Ok)
			{
				ProcessCollectorOrderResponse(collectorTimeoutOrder, userContext, order);
			}
			else if (statusCode == CollectorTransactionStatus.TimeoutResponse)
			{
				//ProcessCollectorFirstTimeout();
				return;
			}
			else if (statusCode == CollectorTransactionStatus.UnspecifiedError)
			{
				RejectOrder(userContext, order);
			}
			else if (statusCode == CollectorTransactionStatus.SoapFault)
			{
				RejectOrder(userContext, order);
			}
			else
			{
				throw new InvalidOperationException("Collector transaction status code is not supported!");
			}

			Repository.Update(collectorTimeoutOrder.TransactionId);
		}

		protected virtual void ProcessCollectorFirstTimeout(ICollectorTimeoutOrder collectorTimeoutOrder, IUserContext userContext, ILekmerOrderFull order)
		{
			/*
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsCollectorPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					throw new InvalidOperationException("Order doesn't contain Collector payment!");
				}

				ICollectorClient collectorClient = CollectorFactory.CreateCollectorClient(userContext.Channel.CommonName);

				ICollectorAddInvoiceResult addInvoiceResult = collectorClient.AddInvoice(
					userContext.Channel,
					customer,
					order,
					orderPayment.CollectorPaymentCode,
					order.GetActualPriceTotal().IncludingVat
				);

				orderStatus = ProcessCollectorResponse(addInvoiceResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Collector person invoice payment.", ex);
				CollectorRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}
			*/
			//return orderStatus;
		}

		protected virtual void ProcessCollectorSecondTimeout(ICollectorTimeoutOrder collectorTimeoutOrder, IUserContext userContext, ILekmerOrderFull order)
		{
			
		}

		protected virtual void ProcessCollectorOrderResponse(ICollectorTimeoutOrder collectorTimeoutOrder, IUserContext userContext, ILekmerOrderFull order)
		{
			if (collectorTimeoutOrder.InvoiceStatus.HasValue == false)
			{
				throw new InvalidOperationException("Collector timeout order does not contain invoice status!");
			}

			CollectorInvoiceStatus invoiceStatus = collectorTimeoutOrder.InvoiceStatus.Value;

			if (invoiceStatus == CollectorInvoiceStatus.Activated || invoiceStatus == CollectorInvoiceStatus.Pending)
			{
				ConfirmOrder(userContext, order);
			}
			else if (invoiceStatus == CollectorInvoiceStatus.OnHold)
			{
				SetOrderOnHold(userContext, order);
			}
			else
			{
				RejectOrder(userContext, order);
			}
		}


		protected virtual void ConfirmOrder(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentTimeout)
			{
				OrderService.OrderVoucherConsume(userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(userContext, OrderStatusName.PaymentConfirmed));

				OrderService.SaveFull(userContext, order);

				OrderService.UpdateNumberInStock(userContext, order);
				GiftCardViaMailInfoService.Save(userContext, order);

				// Order confirmation has been sent already !!!
			}
			else
			{
				LogError("Unable confirm order - status differs from PaymentTimeout.", order);
			}
		}

		protected virtual void SetOrderOnHold(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentTimeout)
			{
				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.PaymentOnHold);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(userContext, OrderStatusName.PaymentOnHold));

				OrderService.SaveFull(userContext, order);
			}
			else
			{
				LogError("Unable set order on hold - status differs from PaymentTimeout.", order);
			}
		}

		protected virtual void RejectOrder(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentTimeout)
			{
				OrderService.OrderVoucherRelease(userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.RejectedByPaymentProvider);
				OrderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);

				OrderService.SendRejectMail(userContext, order);
			}
			else
			{
				LogError("Unable reject order - status differs from PaymentOnHold.", order);
			}
		}


		protected virtual IUserContext GetUserContext(int channelId)
		{
			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = GetChannel(channelId);
			return userContext;
		}

		protected virtual IChannel GetChannel(int channelId)
		{
			if (_channels == null)
			{
				_channels = ChannelService.GetAll();
			}

			var channel = _channels.FirstOrDefault(c => c.Id == channelId);

			if (channel == null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not find channel. Id = {0}", channelId));
			}

			return channel;
		}

		private void LogError(string message, ILekmerOrderFull order)
		{
			_log.ErrorFormat("{0}\r\nOrder id: {1} Status: {2}", message, order.Id, order.OrderStatus.CommonName);
		}

		private void LogError(string message, Exception exception, int orderId)
		{
			string errorMessage = string.Format("{0}\r\nOrder id: {1}", message, orderId);
			_log.Error(errorMessage, exception);
		}
	}
}