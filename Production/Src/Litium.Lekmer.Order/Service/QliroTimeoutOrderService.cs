﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Payment.Qliro;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using log4net;

namespace Litium.Lekmer.Order
{
	public class QliroTimeoutOrderService : IQliroTimeoutOrderService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private Collection<IChannel> _channels;

		protected QliroTimeoutOrderRepository Repository { get; private set; }
		protected ILekmerChannelService ChannelService { get; private set; }
		protected ILekmerOrderService OrderService { get; private set; }
		protected IOrderStatusService OrderStatusService { get; private set; }
		protected IOrderItemStatusService OrderItemStatusService { get; private set; }
		protected IGiftCardViaMailInfoService GiftCardViaMailInfoService { get; private set; }
		protected IQliroTransactionService QliroTransactionService { get; private set; }
		protected IQliroPendingOrderService QliroPendingOrderService { get; private set; }

		public QliroTimeoutOrderService(
			QliroTimeoutOrderRepository repository,
			IChannelService channelService,
			IOrderService orderService,
			IOrderStatusService orderStatusService,
			IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			IQliroTransactionService qliroTransactionService,
			IQliroPendingOrderService qliroPendingOrderService
		)
		{
			Repository = repository;
			ChannelService = (ILekmerChannelService)channelService;
			OrderService = (ILekmerOrderService)orderService;
			OrderStatusService = orderStatusService;
			OrderItemStatusService = orderItemStatusService;
			GiftCardViaMailInfoService = giftCardViaMailInfoService;
			QliroTransactionService = qliroTransactionService;
			QliroPendingOrderService = qliroPendingOrderService;
		}

		public virtual void ManageQliroTimeoutOrders()
		{
			// Get qliro timeout orders
			Collection<IQliroTimeoutOrder> timeoutOrders = Repository.GetQliroTimeoutOrders();
			foreach (var timeoutOrder in timeoutOrders)
			{
				try
				{
					ManageOrder(timeoutOrder);
				}
				catch (Exception ex)
				{
					LogError("Unable manage qliro timeout order.", ex, timeoutOrder.OrderId);
				}
			}
		}


		protected virtual void ManageOrder(IQliroTimeoutOrder qliroTimeoutOrder)
		{
			IUserContext userContext = GetUserContext(qliroTimeoutOrder.ChannelId);

			var order = (ILekmerOrderFull)OrderService.GetFullById(userContext, qliroTimeoutOrder.OrderId);

			var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsQliroPayment(p.PaymentTypeId));
			if (orderPayment == null)
			{
				throw new InvalidOperationException("Order does not contain any Qliro payments!");
			}

			if (qliroTimeoutOrder.ReservationNumber.HasValue())
			{
				orderPayment.ReferenceId = qliroTimeoutOrder.ReservationNumber;
			}

			if (qliroTimeoutOrder.StatusCode.HasValue == false)
			{
				throw new InvalidOperationException("Qliro timeout order does not contain status code!");
			}

			QliroTransactionStatus statusCode = qliroTimeoutOrder.StatusCode.Value;

			if (statusCode == QliroTransactionStatus.Ok)
			{
				ProcessQliroOrderResponse(qliroTimeoutOrder, userContext, order);
			}
			else if (statusCode == QliroTransactionStatus.TimeoutResponse)
			{
				ConfirmOrder(userContext, order);
			}
			else if (statusCode == QliroTransactionStatus.UnspecifiedError)
			{
				RejectOrder(userContext, order);
			}
			else
			{
				throw new InvalidOperationException("Qliro transaction status code is not supported!");
			}

			Repository.Update(qliroTimeoutOrder.TransactionId);
		}

		protected virtual void ProcessQliroOrderResponse(IQliroTimeoutOrder qliroTimeoutOrder, IUserContext userContext, ILekmerOrderFull order)
		{
			if (qliroTimeoutOrder.InvoiceStatus.HasValue == false)
			{
				throw new InvalidOperationException("Qliro timeout order does not contain invoice status!");
			}

			QliroInvoiceStatus invoiceStatus = qliroTimeoutOrder.InvoiceStatus.Value;

			if (invoiceStatus == QliroInvoiceStatus.Ok)
			{
				ConfirmOrder(userContext, order);
			}
			else if (invoiceStatus == QliroInvoiceStatus.Holding)
			{
				SetOrderOnHold(userContext, order);
			}
			else
			{
				RejectOrder(userContext, order);
			}
		}


		protected virtual void ConfirmOrder(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentTimeout)
			{
				OrderService.OrderVoucherConsume(userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.PaymentConfirmed);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(userContext, OrderStatusName.PaymentConfirmed));

				OrderService.SaveFull(userContext, order);

				OrderService.UpdateNumberInStock(userContext, order);
				GiftCardViaMailInfoService.Save(userContext, order);

				// Order confirmation has been sent already !!!
			}
			else
			{
				LogError("Unable confirm order - status differs from PaymentTimeout.", order);
			}
		}

		protected virtual void SetOrderOnHold(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentTimeout)
			{
				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.PaymentOnHold);
				order.SetAllOrderItemsStatus(OrderItemStatusService.GetByCommonName(userContext, OrderStatusName.PaymentOnHold));

				OrderService.SaveFull(userContext, order);

				ProcessQliroPendingOrder(order);
			}
			else
			{
				LogError("Unable set order on hold - status differs from PaymentTimeout.", order);
			}
		}

		protected virtual void RejectOrder(IUserContext userContext, ILekmerOrderFull order)
		{
			if (order.OrderStatus.CommonName == OrderStatusName.PaymentTimeout)
			{
				OrderService.OrderVoucherRelease(userContext, order);

				// Order status
				order.OrderStatus = OrderStatusService.GetByCommonName(userContext, OrderStatusName.RejectedByPaymentProvider);
				OrderService.UpdateOrderStatus(order.Id, order.OrderStatus.Id);

				OrderService.SendRejectMail(userContext, order);
			}
			else
			{
				LogError("Unable reject order - status differs from PaymentOnHold.", order);
			}
		}

		protected virtual void ProcessQliroPendingOrder(ILekmerOrderFull order)
		{
			try
			{
				var qliroPendingOrder = QliroPendingOrderService.Create();

				qliroPendingOrder.ChannelId = order.ChannelId;
				qliroPendingOrder.OrderId = order.Id;

				QliroPendingOrderService.Insert(qliroPendingOrder);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing qliro pending order.", ex);
			}
		}


		protected virtual IUserContext GetUserContext(int channelId)
		{
			var userContext = IoC.Resolve<IUserContext>();
			userContext.Channel = GetChannel(channelId);
			return userContext;
		}

		protected virtual IChannel GetChannel(int channelId)
		{
			if (_channels == null)
			{
				_channels = ChannelService.GetAll();
			}

			var channel = _channels.FirstOrDefault(c => c.Id == channelId);

			if (channel == null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "Can not find channel. Id = {0}", channelId));
			}

			return channel;
		}

		private void LogError(string message, ILekmerOrderFull order)
		{
			_log.ErrorFormat("{0}\r\nOrder id: {1} Status: {2}", message, order.Id, order.OrderStatus.CommonName);
		}

		private void LogError(string message, Exception exception, int orderId)
		{
			string errorMessage = string.Format("{0}\r\nOrder id: {1}", message, orderId);
			_log.Error(errorMessage, exception);
		}
	}
}