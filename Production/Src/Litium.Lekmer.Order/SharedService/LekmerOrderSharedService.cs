using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderSharedService : ILekmerOrderSharedService
	{
		protected LekmerOrderRepository Repository { get; private set; }

		public LekmerOrderSharedService(LekmerOrderRepository repository)
		{
			Repository = repository;
		}

		public void Save(IOrderFull order)
		{
			var lekmerOrder = (ILekmerOrderFull)order;
			Repository.Save(lekmerOrder);
		}
	}
}