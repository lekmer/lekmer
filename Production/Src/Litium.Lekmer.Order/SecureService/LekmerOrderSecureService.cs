using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order.Campaign;
using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Repository;

namespace Litium.Lekmer.Order
{
	public class LekmerOrderSecureService : OrderSecureService, ILekmerOrderSecureService
	{
		protected LekmerOrderRepository LekmerRepository;
		protected ILekmerOrderSharedService LekmerOrderSharedService;
		protected IOrderVoucherInfoSecureService OrderVoucherInfoSecureService { get; private set; }
		protected ILekmerOrderCartCampaignSecureService LekmerOrderCartCampaignSecureService { get; private set; }
		protected IGiftCardViaMailInfoSecureService GiftCardViaMailInfoSecureService { get; private set; }

		public LekmerOrderSecureService(
			IAccessValidator accessValidator,
			OrderRepository repository,
			IOrderAddressSecureService orderAddressSecureService,
			IOrderItemSecureService orderItemSecureService,
			IOrderPaymentSecureService orderPaymentSecureService,
			IOrderCampaignInfoSecureService orderCampaignInfoSecureService,
			IOrderCommentSecureService orderCommentSecureService,
			IOrderAuditSecureService orderAuditSecureService,
			ICustomerSecureService customerSecureService,
			IOrderStatusSecureService orderStatusSecureService,
			IOrderVoucherInfoSecureService orderVoucherInfoSecureService,
			IOrderCartCampaignSecureService orderCartCampaignSecureService,
			IGiftCardViaMailInfoSecureService giftCardViaMailInfoSecureService)
			: base(
				accessValidator,
				repository,
				orderAddressSecureService,
				orderItemSecureService,
				orderPaymentSecureService,
				orderCampaignInfoSecureService,
				orderCommentSecureService,
				orderAuditSecureService,
				customerSecureService,
				orderStatusSecureService)
		{
			LekmerRepository = IoC.Resolve<LekmerOrderRepository>();
			LekmerOrderSharedService = IoC.Resolve<ILekmerOrderSharedService>();
			OrderVoucherInfoSecureService = orderVoucherInfoSecureService;
			LekmerOrderCartCampaignSecureService = (ILekmerOrderCartCampaignSecureService) orderCartCampaignSecureService;
			GiftCardViaMailInfoSecureService = giftCardViaMailInfoSecureService;
		}

		public override int Save(
			ISystemUserFull systemUserFull,
			IOrderFull order,
			Collection<IOrderComment> orderComments,
			IOrderAddress billingAddress,
			IOrderAddress deliveryAddress,
			Collection<IOrderAudit> orderAudits,
			Collection<IOrderPayment> orderPayments)
		{
			if (order == null)
			{
				throw new ArgumentNullException("order");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				order.Id = base.Save(systemUserFull, order, orderComments, billingAddress, deliveryAddress, orderAudits, orderPayments);

				LekmerOrderSharedService.Save(order);

				UpdateOrderNumber(order);

				transactedOperation.Complete();
			}

			return order.Id;
		}

		public virtual int Save(
			ISystemUserFull systemUserFull,
			ILekmerOrderFull order,
			Collection<IOrderComment> orderComments,
			IOrderAddress billingAddress,
			IOrderAddress deliveryAddress,
			IOrderAddress alternateAddress,
			Collection<IOrderAudit> orderAudits,
			Collection<IOrderPayment> orderPayments)
		{
			if (order == null)
			{
				throw new ArgumentNullException("order");
			}

			using (var transactedOperation = new TransactedOperation())
			{
				order.Id = base.Save(systemUserFull, order, orderComments, billingAddress, deliveryAddress, orderAudits, orderPayments);

				if (alternateAddress != null)
				{
					order.AlternateAddressId = OrderAddressSecureService.Save(systemUserFull, alternateAddress);
				}
				
				LekmerOrderSharedService.Save(order);

				UpdateOrderNumber(order);

				transactedOperation.Complete();
			}

			return order.Id;
		}

		public virtual void SaveSplit(
			ISystemUserFull systemUserFull,
			ILekmerOrderFull order,
			Collection<IOrderComment> orderComments,
			IOrderAddress billingAddress,
			IOrderAddress deliveryAddress,
			IOrderAddress alternateAddress,
			Collection<IOrderAudit> orderAudits,
			Collection<IOrderPayment> orderPayments,
			ILekmerOrderFull splitOrder,
			Collection<IOrderComment> splitOrderComments,
			IOrderAddress splitOrderBillingAddress,
			IOrderAddress splitOrderDeliveryAddress,
			IOrderAddress splitOrderAlternateAddress,
			Collection<IOrderAudit> splitOrderAudits,
			Collection<IOrderPayment> splitOrderPayments)
		{
			if (order == null) throw new ArgumentNullException("order");
			if (orderComments == null) throw new ArgumentNullException("orderComments");
			if (orderPayments == null) throw new ArgumentNullException("orderPayments");
			if (splitOrder == null) throw new ArgumentNullException("splitOrder");
			if (splitOrderComments == null) throw new ArgumentNullException("splitOrderComments");
			if (splitOrderPayments == null) throw new ArgumentNullException("splitOrderPayments");

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.CustomerOrder);
			using (var transactedOperation = new TransactedOperation())
			{
				Save(systemUserFull, order, orderComments, billingAddress, deliveryAddress, alternateAddress, orderAudits, orderPayments);
				Save(systemUserFull, splitOrder, splitOrderComments, splitOrderBillingAddress, splitOrderDeliveryAddress, splitOrderAlternateAddress, splitOrderAudits, splitOrderPayments);

				transactedOperation.Complete();
			}
		}

		private void UpdateOrderNumber(IOrderFull order)
		{
			order.Number = order.Id.ToString(CultureInfo.InvariantCulture);
			Repository.Save(order);
		}

		public virtual bool CanChangePaymentCost(IOrderFull order, decimal previousPaymentCost)
		{
			return true;
		}

		public override void SendConfirm(IOrderFull order)
		{
			if (order == null) throw new ArgumentNullException("order");

			IoC.Resolve<LekmerOrderMessengerSecure>().Send(order);
		}

		public virtual Collection<IOrder> Search(ILekmerOrderSearchCriteria searchCriteria, int page, int pageSize, out int itemsCount)
		{
			if (searchCriteria == null)
			{
				throw new ArgumentNullException("searchCriteria");
			}

			return searchCriteria.OrderId > 0 
				? LekmerRepository.Search(searchCriteria, page, pageSize, out itemsCount) 
				: base.Search(searchCriteria, page, pageSize, out itemsCount);
		}

		/// <summary>
		/// Get orders with special status and older than parameter days
		/// </summary>
		public virtual Collection<IOrderFull> GetKlarnaCheckoutOrders(int days, string orderStatus)
		{
			var orders = LekmerRepository.GetKlarnaCheckoutOrders(days, orderStatus);
			foreach (var order in orders)
			{
				var customer = CustomerSecureService.GetById(order.CustomerId);
				FillOrder(order, customer);
			}
			return orders;
		}

		protected override void FillOrder(IOrderFull order, ICustomer customer)
		{
			if (OrderItemSecureService == null)
				throw new InvalidOperationException("OrderItemSecureService must be set.");
			if (OrderPaymentSecureService == null)
				throw new InvalidOperationException("OrderPaymentSecureService must be set.");
			if (OrderAddressSecureService == null)
				throw new InvalidOperationException("OrderAddressSecureService must be set.");
			if (OrderCampaignInfoSecureService == null)
				throw new InvalidOperationException("OrderCampaignInfoSecureService must be set.");
			if (OrderVoucherInfoSecureService == null)
				throw new InvalidOperationException("OrderVoucherInfoSecureService must be set.");

			foreach (IOrderItem orderItem in OrderItemSecureService.GetAllByOrder(order.Id))
			{
				order.AddOrderItem(orderItem);
			}
			order.MergeWith(OrderPaymentSecureService.GetAllByOrderId(order.Id));
			order.MergeWith(OrderAddressSecureService.GetById(order.BillingAddressId),
							OrderAddressSecureService.GetById(order.DeliveryAddressId));
			order.CampaignInfo = OrderCampaignInfoSecureService.GetByOrder(order.Id);
			order.Customer = customer;

			var lekmerOrder = (ILekmerOrderFull)order;
			lekmerOrder.MergeWith(lekmerOrder.AlternateAddressId.HasValue
								? OrderAddressSecureService.GetById(lekmerOrder.AlternateAddressId.Value) as ILekmerOrderAddress
								: null);

			lekmerOrder.VoucherInfo = OrderVoucherInfoSecureService.GetByOrderId(order.Id);
		}

		public void Delete(ISystemUserFull systemUserFull, IOrderFull order)
		{
			if (order == null) throw new ArgumentNullException("order");

			OrderItemSecureService.EnsureNotNull();
			OrderItemSecureService.EnsureNotNull();
			LekmerOrderCartCampaignSecureService.EnsureNotNull();
			GiftCardViaMailInfoSecureService.EnsureNotNull();
			OrderPaymentSecureService.EnsureNotNull();
			OrderVoucherInfoSecureService.EnsureNotNull();
			OrderAuditSecureService.EnsureNotNull();
			OrderCommentSecureService.EnsureNotNull();
			LekmerRepository.EnsureNotNull();
			OrderAddressSecureService.EnsureNotNull();
			CustomerSecureService.EnsureNotNull();

			var lekmerOrder = (ILekmerOrderFull) order;
			var lekmerOrderAddressSecureService = (LekmerOrderAddressSecureService) OrderAddressSecureService;

			using (var transactedOperation = new TransactedOperation())
			{
				foreach (var orderItem in order.GetOrderItems())
				{
					OrderItemSecureService.Delete(systemUserFull, orderItem);
				}

				var orderId = order.Id;

				LekmerOrderCartCampaignSecureService.DeleteByOrder(systemUserFull, orderId);
				GiftCardViaMailInfoSecureService.DeleteByOrder(systemUserFull, orderId);
				((LekmerOrderPaymentSecureService) OrderPaymentSecureService).DeleteByOrder(systemUserFull, orderId);
				OrderVoucherInfoSecureService.Delete(systemUserFull, orderId);
				((LekmerOrderAuditSecureService) OrderAuditSecureService).DeleteByOrder(systemUserFull, orderId);
				((LekmerOrderCommentSecureService) OrderCommentSecureService).DeleteByOrder(systemUserFull, orderId);
				LekmerRepository.Delete(orderId);
				lekmerOrderAddressSecureService.Delete(systemUserFull, order.DeliveryAddressId);
				lekmerOrderAddressSecureService.Delete(systemUserFull, order.BillingAddressId);
				if (lekmerOrder.AlternateAddressId.HasValue)
				{
					lekmerOrderAddressSecureService.Delete(systemUserFull, lekmerOrder.AlternateAddressId.Value);
				}
				((LekmerCustomerSecureService) CustomerSecureService).Delete(systemUserFull, order.CustomerId);

				transactedOperation.Complete();
			}
		}
	}
}