﻿using Litium.Lekmer.Order.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Repository;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Order.SecureService
{
	public class LekmerBlockCheckoutSecureService : BlockCheckoutSecureService
	{
		public LekmerBlockCheckoutSecureService(IAccessValidator accessValidator, BlockCheckoutRepository repository, IAccessSecureService accessSecureService, IBlockSecureService blockService)
			: base(accessValidator, repository, accessSecureService, blockService)
		{
		}

		public override int Save(ISystemUserFull systemUserFull, IBlockCheckout block)
		{
			int blockId = base.Save(systemUserFull, block);
			BlockCheckoutCache.Instance.Remove(blockId);

			return blockId;
		}

		public override void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			base.Delete(systemUserFull, blockId);
			BlockCheckoutCache.Instance.Remove(blockId);
		}
	}
}