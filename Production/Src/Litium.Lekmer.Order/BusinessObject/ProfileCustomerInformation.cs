﻿using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class ProfileCustomerInformation : IProfileCustomerInformation
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string CellPhoneNumber { get; set; }
		public string Email { get; set; }
		public int GenderTypeId { get; set; }
	}
}
