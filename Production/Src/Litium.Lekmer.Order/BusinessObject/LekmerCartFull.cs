﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Order.Setting;
using Litium.Lekmer.Product;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Convert = System.Convert;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerCartFull : CartFull, ILekmerCartFull
	{
		[NonSerialized]
		private ICartItemService _cartItemService;
		[NonSerialized]
		private ILekmerProductService _productService;
		[NonSerialized]
		private ICurrency _currency;
		[NonSerialized]
		private ILekmerCartSetting _cartSetting;


		protected ICartItemService CartItemService
		{
			get { return _cartItemService ?? (_cartItemService = IoC.Resolve<ICartItemService>()); }
		}

		protected ILekmerProductService ProductService
		{
			get { return _productService ?? (_productService = IoC.Resolve<ILekmerProductService>()); }
		}

		protected ICurrency Currency
		{
			get { return _currency ?? (_currency = Scensum.Core.Web.UserContext.Current.Channel.Currency); }
		}

		protected ILekmerCartSetting CartSetting
		{
			get { return _cartSetting ?? (_cartSetting = IoC.Resolve<ILekmerCartSetting>()); }
		}


		private Guid _versionToken;
		public Guid VersionToken
		{
			get { return _versionToken; }
			set { _versionToken = value; }
		}

		private ICartVoucherInfo _voucherInfo;
		public OrderItemsType ItemsType
		{
			get { return _itemsType; }
			set { _itemsType = value; }
		}

		private OrderItemsType _itemsType;
		public ICartVoucherInfo VoucherInfo
		{
			get { return _voucherInfo; }
			set
			{
				CheckChanged(_voucherInfo, value);
				_voucherInfo = value;
			}
		}

		private DateTime? _updatedDate;
		public DateTime? UpdatedDate
		{
			get { return _updatedDate; }
			set
			{
				CheckChanged(_updatedDate, value);
				_updatedDate = value;
			}
		}

		public string _ipAddress;
		public string IPAddress
		{
			get { return _ipAddress; }
			set
			{
				CheckChanged(_ipAddress, value);
				_ipAddress = value;
			}
		}


		public LekmerCartFull(ICartItemService cartItemService, ILekmerProductService productService)
		{
			_cartItemService = cartItemService;
			_productService = productService;
		}


		[Obsolete("Obsolete cart logic.", true)]
		public override Collection<ICartItem> GetGroupedCartItemsByProductAndPrice()
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		public virtual int GetGroupedCartItemsCount()
		{
			Collection<ICartItem> cartItems = GetCartItems();
			if (cartItems.Count == 0)
			{
				return 0;
			}

			int groupedCartItemsCount = cartItems.GroupBy(c => new LekmerCartItemKey(
				c.Product.Id,
				0m, // Ignore price here
				((ILekmerCartItem)c).SizeId,
				((ILekmerCartItem)c).PackageElements.GetPackageElementsHashCode())
			)
			.Count();

			return groupedCartItemsCount;
		}

		public virtual Collection<ILekmerCartItem> GetGroupedCartItemsByProductAndPriceAndSize()
		{
			var cartItems = ObjectCopier.Clone(GetCartItems());
			var groupedCartItems = cartItems.GroupBy(c => new LekmerCartItemKey(
				c.Product.Id,
				c.Product.CampaignInfo.Price.IncludingVat,
				((ILekmerCartItem)c).SizeId,
				((ILekmerCartItem)c).PackageElements.GetPackageElementsHashCode())
			);
			var bundledCartItems = new Collection<ILekmerCartItem>();
			foreach (var groupedCartItem in groupedCartItems)
			{
				var cartItem = (ILekmerCartItem)groupedCartItem.First();
				cartItem.Quantity = groupedCartItem.Sum(c => c.Quantity);
				bundledCartItems.Add(cartItem);
			}
			return bundledCartItems;
		}

		[Obsolete("Obsolete cart logic.", true)]
		public override void ChangeItemQuantity(int productId, int quantity)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		[Obsolete("Obsolete cart logic.", true)]
		public override void ChangeItemQuantity(IProduct product, int quantity)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		/// <summary>
		/// Changes the quantity of the product in the cart (adds or subtracts 'quantity' from its current quantity).
		/// If the product does not exist in the cart yet, it will be added.
		/// If the product has new size and cart already has same product with old size,
		/// then cartItem with old size will be delete if multiple sizes purchase is not allowed.
		/// </summary>
		public virtual void ChangeItemQuantity(IUserContext userContext, IProduct product, int quantity, int? sizeId, Collection<ICartItemPackageElement> cartItemPackageElements)
		{
			if (product == null) throw new ArgumentNullException("product");
			if (quantity < 0) throw new ArgumentOutOfRangeException("quantity", "Quantity must be greater than or equal to zero.");

			bool allowMultipleSizesPurchase = ProductService.AllowMultipleSizesPurchase(userContext, product);
			if (!allowMultipleSizesPurchase)
			{
				// Restriction: Cart can has only one size for product
				if (product.IsPackage())
				{
					DeletePackage(product.Id, cartItemPackageElements);
				}
				else
				{
					DeleteProductWithOldSize(product.Id, sizeId);
				}
			}

			quantity = CheckProductMaxQuantityPerOrder(userContext, product, sizeId, quantity);

			var cartItem = FindCartItemByProduct(product.Id, product.CampaignInfo.Price.IncludingVat, sizeId, cartItemPackageElements.GetPackageElementsHashCode());
			if (cartItem != null)
			{
				((ILekmerCartItem)cartItem).ChangeQuantityForFreeProduct(quantity);
			}
			else if (quantity > 0)
			{
				AddItem(product, quantity, sizeId, false, 0, cartItemPackageElements);
			}
		}

		/// <summary>
		/// Changes the quantity of the product in the cart (adds or subtracts 'quantity' from its current quantity).
		/// If the product does not exist in the cart yet, it will be added.
		/// </summary>
		public virtual void ChangeItemQuantityForBlockExtendedCartControl(IProduct product, int quantity, int? sizeId, bool isAffectedByCampaign, Collection<ICartItemPackageElement> cartItemPackageElements)
		{
			if (product == null) throw new ArgumentNullException("product");

			quantity = CheckProductMaxQuantityPerOrder(Scensum.Core.Web.UserContext.Current, product, sizeId, quantity);

			var cartItem = FindCartItemByProduct(product.Id, product.CampaignInfo.Price.IncludingVat, sizeId, cartItemPackageElements.GetPackageElementsHashCode());
			if (cartItem != null)
			{
				((ILekmerCartItem)cartItem).ChangeQuantityForFreeProduct(quantity);
			}
			else if (quantity > 0)
			{
				AddItem(product, quantity, sizeId, isAffectedByCampaign, 0, cartItemPackageElements);
			}
		}


		[Obsolete("Obsolete cart logic.", true)]
		public override void AddItem(int productId, int quantity)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		[Obsolete("Obsolete cart logic.", true)]
		public override void AddItem(IProduct product, int quantity)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		[Obsolete("Obsolete cart logic.", true)]
		public override void AddItem(IProduct product, int quantity, int cartItemId)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		public virtual void AddItem(IProduct product, int quantity, int? sizeId, bool isAffectedByCampaign, int cartItemId, Collection<ICartItemPackageElement> cartItemPackageElements)
		{
			if (product == null) throw new ArgumentNullException("product");
			if (quantity < 0) throw new ArgumentOutOfRangeException("quantity", "Quantity must be greater than or equal to zero.");

			if (IsItemsLimitExceeded())
			{
				return;
			}

			var item = (ILekmerCartItem)CartItemService.Create();

			item.CartId = Id;
			item.Product = product;
			item.Quantity = quantity;
			item.SizeId = sizeId;
			item.IsAffectedByCampaign = isAffectedByCampaign;
			item.Id = cartItemId;
			item.PackageElements = cartItemPackageElements;

			AddItem(item);
		}

		public override void SetItems(Collection<ICartItem> items)
		{
			if (items == null) throw new ArgumentNullException("items");

			// NOTE: Information about deleted items needs to be alive !
			base.SetItems(items.AddRange(GetDeletedCartItems()));
		}

		[Obsolete("Obsolete cart logic.", true)]
		public override void DeleteItem(int productId, decimal campaignInfoPrice, int quantity)
		{
			throw new InvalidOperationException("Not allowed to use.");
		}

		public virtual void DeleteItem(int productId, decimal campaignInfoPrice, int quantity, int? sizeId, int? packageElementsHashCode)
		{
			var cartItems = FindCartItemsByProduct(productId, campaignInfoPrice, sizeId, packageElementsHashCode).ToList();
			if (cartItems.Count > 0)
			{
				foreach (var cartItem in cartItems)
				{
					var cartItemQuantity = cartItem.Quantity;
					((ILekmerCartItem)cartItem).ChangeQuantityForFreeProduct(-quantity);
					quantity -= cartItemQuantity;
					if (quantity <= 0)
					{
						break;
					}
				}
			}
		}

		public override void DeleteAllItems()
		{
			// NOTE: Information about deleted items needs to be alive !
			var items = new Collection<ICartItem>();
			base.SetItems(items.AddRange(GetDeletedCartItems()));
		}

		public virtual void DeleteItemsAffectedByCampaign()
		{
			foreach (var cartItem in GetCartItems())
			{
				var lekmerCartItem = (ILekmerCartItem)cartItem;
				if (lekmerCartItem != null && lekmerCartItem.IsAffectedByCampaign)
				{
					cartItem.Delete();
				}
			}
		}


		public virtual Price GetBaseActualPriceSummary()
		{
			return base.GetActualPriceSummary();
		}

		public override Price GetActualPriceSummary()
		{
			ICurrency currency = GetCurrency();
			return GetActualPriceSummary(currency);
		}
		public virtual Price GetActualPriceSummary(ICurrency currency)
		{
			var priceSummary = GetBaseActualPriceSummary();
			if (CampaignInfo != null)
			{
				priceSummary = ApplyDiscount(priceSummary, currency);
				priceSummary = ApplyVoucherDiscount(priceSummary);
			}
			return priceSummary;
		}

		public virtual bool GetTotalFixedDiscount(IUserContext userContext, out decimal totalFixedDiscount)
		{
			bool isFixedDiscount = false;
			totalFixedDiscount = 0;

			if (CampaignInfo != null)
			{
				foreach (var action in CampaignInfo.CampaignActionsApplied)
				{
					if (action.ActionType.CommonName == "FixedCartDiscount")
					{
						decimal discountValue;
						LekmerCurrencyValueDictionary amounts = ((IFixedDiscountCartAction)action).Amounts;
						if (amounts != null && amounts.TryGetValue(userContext.Channel.Currency.Id, out discountValue))
						{
							isFixedDiscount = true;
							totalFixedDiscount = totalFixedDiscount + discountValue;
						}
					}
				}
			}

			return isFixedDiscount;
		}

		public virtual bool GetTotalPercentageDiscount(bool isFixedDiscount, decimal totalFixedDiscount, out decimal totalPercentageDiscount)
		{
			bool isPercentageDiscount = false;
			totalPercentageDiscount = 0;

			if (CampaignInfo != null)
			{
				string percentageDiscount = string.Empty;

				foreach (var action in CampaignInfo.CampaignActionsApplied)
				{
					if (action.ActionType.CommonName == "PercentageDiscount")
					{
						isPercentageDiscount = true;
						percentageDiscount = percentageDiscount + ((IPercentageDiscountCartAction)action).DiscountAmount + ';';
					}
				}

				if (!string.IsNullOrEmpty(percentageDiscount))
				{
					percentageDiscount = percentageDiscount.Substring(0, percentageDiscount.Length - 1);
					string[] splittedPercentageDiscount = percentageDiscount.Split(';');

					if (splittedPercentageDiscount.Length == 1)
					{
						totalPercentageDiscount = Convert.ToDecimal(splittedPercentageDiscount[0], CultureInfo.CurrentCulture);
					}
					else
					{
						Price originalPrice = base.GetActualPriceSummary();
						Price priceWithFixedDiscount;

						if (isFixedDiscount)
						{
							decimal vatPercentage = originalPrice.IncludingVat / originalPrice.ExcludingVat - 1;

							var priceIncludingVat = originalPrice.IncludingVat - totalFixedDiscount;
							var priceExcludingVat = priceIncludingVat / (1 + vatPercentage / 100);

							priceWithFixedDiscount = new Price(priceIncludingVat, priceExcludingVat);
						}
						else
						{
							priceWithFixedDiscount = originalPrice;
						}

						var actualPrice = CampaignInfo != null ? ApplyDiscount(originalPrice) : priceWithFixedDiscount;

						totalPercentageDiscount = CalculateDiscountPercent(priceWithFixedDiscount.IncludingVat, actualPrice.IncludingVat);
					}
				}
			}

			return isPercentageDiscount;
		}

		public virtual ICurrency GetCurrency()
		{
			return Currency;
		}


		protected virtual Price ApplyDiscount(Price priceSummary)
		{
			ICurrency currency = GetCurrency();
			return ApplyDiscount(priceSummary, currency);
		}
		protected virtual Price ApplyDiscount(Price priceSummary, ICurrency currency)
		{
			foreach (var campaignAction in CampaignInfo.CampaignActionsApplied)
			{
				if (campaignAction.ActionType.CommonName == "PercentageDiscount"
					|| campaignAction.ActionType.CommonName == "FixedCartDiscount")
				{
					priceSummary = ((ILekmerCartAction)campaignAction).ApplyDiscount(priceSummary, currency);
				}
			}

			return priceSummary;
		}

		protected virtual Price ApplyVoucherDiscount(Price priceSummary)
		{
			decimal? discount = VoucherInfo.DiscountValue;

			if (!discount.HasValue || priceSummary.ExcludingVat == 0)
			{
				return priceSummary;
			}

			if (VoucherInfo.DiscountType == VoucherValueType.Price)
			{
				decimal priceIncludingVat = priceSummary.IncludingVat;
				decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;

				priceIncludingVat -= discount.Value;
				if (priceIncludingVat < 0)
				{
					VoucherInfo.AmountUsed = priceSummary.IncludingVat;
					VoucherInfo.AmountLeft = 0m;
					return new Price(0, 0);
				}

				VoucherInfo.AmountUsed = discount.Value;
				var priceExcludingVat = priceIncludingVat / (1 + vatPercentage / 100);

				return new Price(priceIncludingVat, priceExcludingVat);
			}

			if (VoucherInfo.DiscountType == VoucherValueType.Percentage)
			{
				if (discount.Value >= 100)
				{
					VoucherInfo.AmountUsed = priceSummary.IncludingVat;
					return new Price(0, 0);
				}

				decimal priceIncludingVat = priceSummary.IncludingVat;
				decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;

				priceIncludingVat = priceIncludingVat * (1 - discount.Value / 100);
				var priceExcludingVat = priceIncludingVat / (1 + vatPercentage / 100);

				VoucherInfo.AmountUsed = priceSummary.IncludingVat - priceIncludingVat;

				return new Price(priceIncludingVat, priceExcludingVat);
			}

			if (VoucherInfo.DiscountType == VoucherValueType.GiftCard)
			{
				decimal priceIncludingVat = priceSummary.IncludingVat;
				decimal vatPercentage = priceSummary.IncludingVat / priceSummary.ExcludingVat - 1;

				priceIncludingVat -= discount.Value;
				if (priceIncludingVat < 0)
				{
					VoucherInfo.AmountUsed = priceSummary.IncludingVat;
					VoucherInfo.AmountLeft = discount.Value - priceSummary.IncludingVat;
					return new Price(0, 0);
				}

				VoucherInfo.AmountUsed = discount.Value;
				var priceExcludingVat = priceIncludingVat / (1 + vatPercentage / 100);

				return new Price(priceIncludingVat, priceExcludingVat);
			}

			throw new NotSupportedException("VoucherValueType '" + VoucherInfo.DiscountType + "' not supported.");
		}

		protected virtual void DeleteProductWithOldSize(int productId, int? sizeId)
		{
			var cartItems = GetCartItems();

			foreach (var cartItem in cartItems)
			{
				if (cartItem.Product.Id == productId && ((ILekmerCartItem)cartItem).SizeId != sizeId)
				{
					cartItem.Delete();
				}
			}
		}

		protected virtual void DeletePackage(int productId, Collection<ICartItemPackageElement> cartItemPackageElements)
		{
			var cartItems = GetCartItems();

			foreach (var cartItem in cartItems)
			{
				if (cartItem.Product.Id == productId
					&& cartItem.Product.IsPackage()
					&& ((ILekmerCartItem)cartItem).PackageElements.GetPackageElementsHashCode() != cartItemPackageElements.GetPackageElementsHashCode())
				{
					cartItem.Delete();
				}
			}
		}

		protected virtual ICartItem FindCartItemByProduct(int productId, decimal campaignInfoPrice, int? sizeId, int? packageElementsHashCode)
		{
			var cartItems = GetCartItems();

			return cartItems.Count == 0
				? null
				: cartItems.FirstOrDefault(cartItem =>
					cartItem.Product.Id.Equals(productId) &&
					cartItem.Product.CampaignInfo.Price.IncludingVat.Equals(campaignInfoPrice) &&
					((ILekmerCartItem)cartItem).SizeId == sizeId &&
					((ILekmerCartItem)cartItem).PackageElements.GetPackageElementsHashCode() == packageElementsHashCode);
		}

		protected virtual IEnumerable<ICartItem> FindCartItemsByProduct(int productId, decimal campaignInfoPrice, int? sizeId, int? packageElementsHashCode)
		{
			var cartItems = GetCartItems();

			return cartItems.Where(cartItem =>
				cartItem.Product.Id.Equals(productId) &&
				cartItem.Product.CampaignInfo.Price.IncludingVat.Equals(campaignInfoPrice) &&
				((ILekmerCartItem)cartItem).SizeId == sizeId &&
				((ILekmerCartItem)cartItem).PackageElements.GetPackageElementsHashCode() == packageElementsHashCode);
		}

		protected virtual decimal CalculateDiscountPercent(decimal originalPrice, decimal campaignPrice)
		{
			var discountInPercent = (originalPrice - campaignPrice) * 100 / originalPrice;
			return Math.Round(discountInPercent, 0, MidpointRounding.AwayFromZero);
		}

		public virtual decimal CalculateCartItemsWeight(IUserContext context)
		{
			var weight = 0m;

			var cartItems = GetCartItems();
			foreach (var cartItem in cartItems)
			{
				var product = (ILekmerProductView) ProductService.GetViewById(context, cartItem.Product.Id);
				var lekmerCartItem = (ILekmerCartItem)cartItem;
				if (lekmerCartItem.SizeId.HasValue && product.ProductSizes != null)
				{
					var productSize = product.ProductSizes.FirstOrDefault(s => s.SizeInfo.Id == lekmerCartItem.SizeId);
					if (productSize != null && productSize.Weight.HasValue)
					{
						weight = weight + productSize.Weight.Value * cartItem.Quantity;
	}
				}
				else if (product.Weight.HasValue)
				{
					weight = weight + product.Weight.Value * cartItem.Quantity;
				}
			}

			return weight;
		}

		public virtual Collection<ICartItemOption> GetFreeCartItems(IUserContext context)
		{
			var cartItemOptions = new Collection<ICartItemOption>();

			var actions = new Collection<IProductAutoFreeCartAction>();
			foreach (var action in CampaignInfo.CampaignActionsApplied)
			{
				if (action is IProductAutoFreeCartAction)
				{
					actions.Add((IProductAutoFreeCartAction)action);
				}
			}

			var cartItemOptionService = IoC.Resolve<ICartItemOptionService>();
			var cartItems = cartItemOptionService.GetAllByCart(context, Id);
			if (cartItems.Count > 0)
			{
				foreach (ICartItemOption cartItemOption in cartItems)
				{
					if (cartItemOption.Quantity > 0 && actions.FirstOrDefault(a => a.ProductId == cartItemOption.Product.Id) != null)
					{
						cartItemOptions.Add(cartItemOption);
					}
				}
			}

			return cartItemOptions;
		}

		public virtual bool IsTotalOverPredefinedValue(decimal actualPrice)
		{
			return actualPrice >= CartSetting.CompanyCartAmount;
		}

		public virtual bool IsItemsLimitExceeded()
		{
			int? maxNumberOfItems = CartSetting.MaxNumberOfItems;
			if (maxNumberOfItems.HasValue == false || maxNumberOfItems.Value < 0)
			{
				return false; // there is no limit
			}

			int itemsCount = GetGroupedCartItemsCount();

			return itemsCount >= maxNumberOfItems;
		}


		protected virtual int CheckProductMaxQuantityPerOrder(IUserContext userContext, IProduct product, int? sizeId, int quantity)
		{
			if (quantity <= 0)
				return quantity;

			var maxQuantityPerOrder = GetMaxQuantityPerOrder(product, userContext);
			if (!maxQuantityPerOrder.HasValue)
				return quantity;

			int totalQuantity;
			var cartItems = GetCartItems();
			if (sizeId.HasValue)
			{
				totalQuantity = cartItems
					.Where(x => x.Product.Id == product.Id && ((ILekmerCartItem) x).SizeId == sizeId)
					.Sum(x => x.Quantity);
			}
			else
			{
				totalQuantity = cartItems
					.Where(x => x.Product.Id == product.Id)
					.Sum(x => x.Quantity);
			}

			var futureQuantity = totalQuantity + quantity;
			if (futureQuantity <= maxQuantityPerOrder.Value)
				return quantity;

			var overflow = futureQuantity - maxQuantityPerOrder.Value;
			return quantity - overflow;
		}

		protected virtual int? GetMaxQuantityPerOrder(IProduct product, IUserContext context)
		{
			var lekmerProduct = product as ILekmerProduct;
			if (lekmerProduct == null)
				return null;

			if (lekmerProduct.MaxQuantityPerOrder.HasValue)
				return lekmerProduct.MaxQuantityPerOrder.Value;

			if (lekmerProduct.Brand != null && lekmerProduct.Brand.MaxQuantityPerOrder.HasValue)
			{
				return lekmerProduct.Brand.MaxQuantityPerOrder.Value;
			}

			var categoryTree = IoC.Resolve<ICategoryService>().GetAllAsTree(context);
			var category = categoryTree.FindItemById(lekmerProduct.CategoryId);
			var categoryMaxQuantityPerOrder = GetCategoryMaxQuantityPerOrder(category);
			if (categoryMaxQuantityPerOrder.HasValue)
			{
				return categoryMaxQuantityPerOrder.Value;
			}

			return CartSetting.MaxProductQuantityPerOrder;
		}

		protected virtual int? GetCategoryMaxQuantityPerOrder(ICategoryTreeItem categoryTreeItem)
		{
			if (categoryTreeItem == null)
			{
				return null;
			}

			var category = categoryTreeItem.Category as ILekmerCategory;
			if (category != null && category.MaxQuantityPerOrder.HasValue)
			{
				return category.MaxQuantityPerOrder.Value;
			}

			return GetCategoryMaxQuantityPerOrder(categoryTreeItem.Parent);
		}
	}
}