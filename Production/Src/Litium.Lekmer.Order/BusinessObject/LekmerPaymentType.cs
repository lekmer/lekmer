﻿using System;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerPaymentType : PaymentType, ILekmerPaymentType
	{
		private bool _isPersonDefault;
		private bool _isCompanyDefault;
		private decimal _cost;

		public bool IsPersonDefault
		{
			get
			{
				return _isPersonDefault;
			}
			set
			{
				CheckChanged(_isPersonDefault, value);
				_isPersonDefault = value;
			}
		}
		public bool IsCompanyDefault
		{
			get
			{
				return _isCompanyDefault;
			}
			set
			{
				CheckChanged(_isCompanyDefault, value);
				_isCompanyDefault = value;
			}
		}
		public decimal Cost
		{
			get
			{
				return _cost;
			}
			set 
			{
				CheckChanged(_cost, value);
				_cost = value;
			}
		}
	}
}
