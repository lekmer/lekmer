﻿using System;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class CheckoutProfile : ICheckoutProfile
	{
		public bool IsCompany { get; set; }

		public IProfileCustomerInformation CustomerInformation { get; set; }
		public IProfileAddress BillingAddress { get; set; }
		public IProfileAddress DeliveryAddress { get; set; }
		public bool UseAlternativeDeliveryAddress { get; set; }

		public IProfileCompanyInformation CompanyInformation { get; set; }
		public IProfileAddress CompanyAddress { get; set; }
		public IProfileAddress CompanyAlternateAddress { get; set; }
		public bool UseAlternateCompanyAddress { get; set; }

		public int PaymentMethodId { get; set; }
		public int KlarnaPartPaymentId { get; set; }
		public string MaksuturvaSubPaymentId { get; set; }
		public string QliroPartPaymentId { get; set; }
		public string CollectorPartPaymentId { get; set; }

		public int DeliveryMethodId { get; set; }
		public int OptionalDeliveryMethodId { get; set; }
	}
}
