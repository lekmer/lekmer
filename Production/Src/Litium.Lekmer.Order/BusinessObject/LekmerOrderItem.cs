using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Product;
using ProductTypeConst = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Order
{
	[Serializable]
	public class LekmerOrderItem : OrderItem, ILekmerOrderItem
	{
		[NonSerialized]
		private IProductSizeService _productSizeService;
		protected IProductSizeService ProductSizeService
		{
			get { return _productSizeService ?? (_productSizeService = IoC.Resolve<IProductSizeService>()); }
		}


		public IOrderItemSize OrderItemSize { get; set; }
		public IProductSize Size { get; set; }
		public int? BrandId { get; set; }
		public IBrand Brand { get; set; }
		public IImage Image { get; set; }
		public Collection<ITagGroup> TagGroups { get; set; }
		public int ProductTypeId { get; set; }
		public bool IsDropShip { get; set; }
		public Collection<IPackageOrderItem> PackageOrderItems { get; set; }

		public bool HasSize
		{
			get { return Size != null; }
		}

		public bool HasBrand
		{
			get { return Brand != null; }
		}

		public bool HasImage
		{
			get { return Image != null; }
		}

		public override void MergeWith(IUserContext context, ICartItem cartItem)
		{
			base.MergeWith(context, cartItem);

			var lekmerProduct = (ILekmerProduct) cartItem.Product;
			// Do not remove next line !!!
			ProductTypeId = lekmerProduct.ProductTypeId;
			IsDropShip = lekmerProduct.IsDropShip;

			var lekmerCartItem = (ILekmerCartItem)cartItem;
			if (lekmerCartItem.SizeId.HasValue)
			{
				OrderItemSize.SizeId = lekmerCartItem.SizeId;

				IProductSize productSize = ProductSizeService.GetById(ProductId, lekmerCartItem.SizeId.Value);
				if (productSize != null)
				{
					OrderItemSize.ErpId = productSize.ErpId;
				}
			}

			MergeWithPackage(context, cartItem);
			CalculatePackageItemsPrice();
		}

		/// <exception cref="ArgumentNullException">after Release 2.1: throws AryesgumentNullException if context == null.</exception>
		public override void MergeWith(IUserContext context, IProductCampaignInfo productCampaignInfo)
		{
			if (context == null)
			{
				// only in Release 2.1 while OrderFull.MergeWith(Collection<ICartItem> cartItems) still exists.
				return;
				// after Release 2.1:
				//throw new ArgumentNullException("context");
			}
			if (productCampaignInfo == null)
			{
				return;
			}

			var lekmerProductCampaignInfo = (ILekmerProductCampaignInfo) productCampaignInfo;

			CampaignInfo = IoC.Resolve<IOrderItemCampaignInfo>();
			CampaignInfo.Price = lekmerProductCampaignInfo.Price;
			CampaignInfo.Campaigns = new Collection<IOrderCampaign>();

			var campaignService = (ILekmerProductCampaignService) IoC.Resolve<IProductCampaignService>();
			Collection<IProductCampaign> productCampaigns = campaignService.GetDistinctCampaigns(context, lekmerProductCampaignInfo.CampaignActionsApplied);
			foreach (IProductCampaign productCampaign in productCampaigns)
			{
				var orderCampaign = IoC.Resolve<IOrderCampaign>();
				orderCampaign.Title = productCampaign.Title;
				orderCampaign.CampaignId = productCampaign.Id;
				CampaignInfo.Campaigns.Add(orderCampaign);
			}
		}

		//TODO: Decouple somehow....
		public virtual void MergeWithPackage(IUserContext context, ICartItem cartItem)
		{
			if (cartItem.Product.IsPackage() == false)
			{
				return;
			}
			
			PackageOrderItems = new Collection<IPackageOrderItem>();

			var productService = (ILekmerProductService) IoC.Resolve<IProductService>();
			ProductIdCollection productIds = productService.GetIdAllByPackageMasterProduct(context, cartItem.Product.Id);
			ProductCollection products = productService.PopulateProductsWithOnlyInPackage(context, productIds);

			foreach (var product in products)
			{
				var packageOrderItem = IoC.Resolve<IPackageOrderItem>();
				packageOrderItem.ProductId = product.Id;
				packageOrderItem.Quantity = Quantity;

				packageOrderItem.OriginalPrice = new Price(product.Price.PriceIncludingVat, product.Price.PriceExcludingVat);

				packageOrderItem.ActualPrice = product.CampaignInfo == null
					? packageOrderItem.OriginalPrice
					: new Price(product.CampaignInfo.Price.IncludingVat, product.CampaignInfo.Price.ExcludingVat);

				// temprary package price
				packageOrderItem.PackagePrice = packageOrderItem.ActualPrice;

				packageOrderItem.ErpId = product.ErpId;
				packageOrderItem.EanCode = product.EanCode;
				packageOrderItem.Title = product.Title;
				packageOrderItem.OrderItemStatus = OrderItemStatus;
				packageOrderItem.SizeId = null;
				packageOrderItem.SizeErpId = null;

				PackageOrderItems.Add(packageOrderItem);
			}

			var lekmerCartItem = (ILekmerCartItem)cartItem;
			if (lekmerCartItem.PackageElements != null)
			{
				var copyPackageElements = new Collection<ICartItemPackageElement>();
				foreach (var item in lekmerCartItem.PackageElements)
				{
					copyPackageElements.Add(new CartItemPackageElement{CartItemId = item.CartItemId, Id = item.Id, ProductId = item.ProductId, SizeId = item.SizeId});
				}

				foreach (IPackageOrderItem packageOrderItem in PackageOrderItems)
				{
					var cartItemPackageElement = copyPackageElements.FirstOrDefault(pe => pe.ProductId == packageOrderItem.ProductId);
					if (cartItemPackageElement != null && cartItemPackageElement.SizeId.HasValue)
					{
						packageOrderItem.SizeId = cartItemPackageElement.SizeId;

						IProductSize productSize = ProductSizeService.GetById(cartItemPackageElement.ProductId, cartItemPackageElement.SizeId.Value);
						if (productSize != null)
						{
							packageOrderItem.SizeErpId = productSize.ErpId;
						}
					}
					copyPackageElements.Remove(cartItemPackageElement);
				}
			}
		}

		public virtual void CalculatePackageItemsPrice()
		{
			if (PackageOrderItems == null || PackageOrderItems.Count == 0)
			{
				return;
			}

			Price actualPackagePrice = ActualPrice; // = 1000

			var actualProductsPrice = new Price(); // = 1200
			foreach (IPackageOrderItem packageOrderItem in PackageOrderItems)
			{
				actualProductsPrice += packageOrderItem.ActualPrice;
			}

			decimal differenceRateIncVat = actualPackagePrice.IncludingVat / actualProductsPrice.IncludingVat; // = 1000 / 1200
			decimal differenceRateExlVat = actualPackagePrice.ExcludingVat / actualProductsPrice.ExcludingVat; // = 1000 / 1200

			foreach (IPackageOrderItem packageOrderItem in PackageOrderItems)
			{
				decimal priceIncludingVat = (packageOrderItem.ActualPrice.IncludingVat * differenceRateIncVat).Round();
				decimal priceExcludingVat = (packageOrderItem.ActualPrice.ExcludingVat* differenceRateExlVat).Round();

				packageOrderItem.PackagePrice = new Price(priceIncludingVat, priceExcludingVat);

				// Exclude current item and get new rate.
				actualPackagePrice -= packageOrderItem.PackagePrice;
				actualProductsPrice -= packageOrderItem.ActualPrice;
				if (actualProductsPrice.IncludingVat > 0)
				{
					differenceRateIncVat = actualPackagePrice.IncludingVat / actualProductsPrice.IncludingVat;
					differenceRateExlVat = actualPackagePrice.ExcludingVat / actualProductsPrice.ExcludingVat;
				}
				else
				{
					differenceRateIncVat = 1; // Exit from the loop.
					differenceRateExlVat = 1; // Exit from the loop.
				}
			}
		}

		public virtual decimal GetOriginalVatAmount()
		{
			return OriginalPrice.IncludingVat - OriginalPrice.ExcludingVat;
		}

		public virtual decimal GetOriginalVatPercent()
		{
			decimal vatPercent = 0m;

			if (OriginalPrice.ExcludingVat != 0)
			{
				vatPercent = (GetOriginalVatAmount() / OriginalPrice.ExcludingVat) * 100;
				vatPercent = vatPercent.RoundToInteger();
			}

			return vatPercent;
		}

		public virtual decimal RowDiscount(bool isDiscountIncludingVat)
		{
			decimal discount;

			if (isDiscountIncludingVat)
			{
				discount = OriginalPrice.IncludingVat - ActualPrice.IncludingVat;
			}
			else
			{
				discount = OriginalPrice.ExcludingVat - ActualPrice.ExcludingVat;
			}

			if (discount < 0)
			{
				discount = 0;
			}

			return discount;
		}

		public virtual double RowDiscountPercent(bool isDiscountIncludingVat)
		{
			double discount;

			double discountPercent;
			if (isDiscountIncludingVat)
			{
				discount = (double)(OriginalPrice.IncludingVat - ActualPrice.IncludingVat);
				if (discount < 0)
				{
					discount = 0;
				}
				discountPercent = (discount * 100) / (double)OriginalPrice.IncludingVat;
			}
			else
			{
				discount = (double)(OriginalPrice.ExcludingVat - ActualPrice.ExcludingVat);
				if (discount < 0)
				{
					discount = 0;
				}
				discountPercent = (discount * 100) / (double)OriginalPrice.ExcludingVat;
			}

			return discountPercent;
		}

		public virtual void SplitPackageElements(ref Collection<int> itemWithoutSizes, ref Collection<IPackageElement> itemWithSizes)
		{
			if (PackageOrderItems == null || PackageOrderItems.Count == 0) return;

			foreach (var item in PackageOrderItems)
			{
				if (item.SizeId.HasValue && item.SizeId > 0)
				{
					itemWithSizes.Add(new PackageElement { ProductId = item.ProductId, SizeId = item.SizeId });
				}
				else
				{
					itemWithoutSizes.Add(item.ProductId);
				}
			}
		}
	}
}