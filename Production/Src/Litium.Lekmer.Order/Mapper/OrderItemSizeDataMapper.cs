using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Order.Mapper
{
	public class OrderItemSizeDataMapper : DataMapperBase<IOrderItemSize>
    {
		public OrderItemSizeDataMapper(IDataReader dataReader) : base(dataReader)
        {
        }

		protected override IOrderItemSize Create()
        {
			var orderItemSize = IoC.Resolve<IOrderItemSize>();
			orderItemSize.OrderItemId = MapNullableValue<int>("OrderItemSize.OrderItemId");
			orderItemSize.SizeId = MapNullableValue<int?>("OrderItemSize.SizeId");
			orderItemSize.ErpId = MapNullableValue<string>("OrderItemSize.ErpId");
			return orderItemSize;
        }
    }
}