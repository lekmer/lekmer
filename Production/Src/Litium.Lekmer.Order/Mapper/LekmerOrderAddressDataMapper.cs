using System.Data;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Mapper;

namespace Litium.Lekmer.Order.Mapper
{
	public class LekmerOrderAddressDataMapper : OrderAddressDataMapper
	{
		private int _houseNumber;
		private int _houseExtension;
		private int _reference;
		private int _doorCode;

		public LekmerOrderAddressDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_houseNumber = OrdinalOf("OrderAddress.HouseNumber");
			_houseExtension = OrdinalOf("OrderAddress.HouseExtension");
			_reference = OrdinalOf("OrderAddress.Reference");
			_doorCode = OrdinalOf("OrderAddress.DoorCode");
		}

		protected override IOrderAddress Create()
		{
			var orderAddress = base.Create() as ILekmerOrderAddress;

			if (orderAddress != null)
			{
				orderAddress.HouseNumber = MapNullableValue<string>(_houseNumber);
				orderAddress.HouseExtension = MapNullableValue<string>(_houseExtension);
				orderAddress.Reference = MapNullableValue<string>(_reference);
				orderAddress.DoorCode = MapNullableValue<string>(_doorCode);

				orderAddress.Status = BusinessObjectStatus.Untouched;
			}

			return orderAddress;
		}
	}
}
