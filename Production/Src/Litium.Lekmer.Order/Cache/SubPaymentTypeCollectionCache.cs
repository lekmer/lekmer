﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Order
{
	public sealed class SubPaymentTypeCollectionCache : ScensumCacheBase<SubPaymentTypeCollectionKey, Collection<ISubPaymentType>>
	{
		private static readonly SubPaymentTypeCollectionCache _instance = new SubPaymentTypeCollectionCache();

		private SubPaymentTypeCollectionCache()
		{
		}

		public static SubPaymentTypeCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class SubPaymentTypeCollectionKey : ICacheKey
	{
		public SubPaymentTypeCollectionKey(int paymentId)
		{
			PaymentId = paymentId;
		}

		public int PaymentId { get; set; }

		public string Key
		{
			get
			{
				return PaymentId.ToString(CultureInfo.InvariantCulture);
			}
		}
	}
}