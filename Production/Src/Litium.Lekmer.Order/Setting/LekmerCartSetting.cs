﻿using System.Collections.Generic;
using Litium.Framework.Setting;

namespace Litium.Lekmer.Order.Setting
{
	public class LekmerCartSetting : SettingBase, ILekmerCartSetting
	{
		private const string _groupName = "LekmerCart";

		protected override string StorageName
		{
			get { return "LekmerCart"; }
		}

		public virtual string ShoppingCardCookiename
		{
			get { return (GetString(_groupName, "ShoppingCardCookiename", "ShoppingCard")); }
		}

		public virtual int LifeCycleValue
		{
			get { return GetInt32(_groupName, "LifeCycleValue", 30); }
		}

		public virtual bool CheckoutProfileEnabled
		{
			get { return GetBoolean(_groupName, "CheckoutProfileEnabled", false); }
		}

		public virtual int CompanyCartAmount
		{
			get { return GetInt32(_groupName, "CompanyCartAmount", 5000); }
		}

		public virtual int DiapersBrand
		{
			get { return GetInt32(_groupName, "DiapersBrand", -1); }
		}

		public virtual int? MaxProductQuantityPerOrder
		{
			get
			{
				var value = GetInt32(_groupName, "MaxProductQuantityPerOrder", -1);
				return value < 0 ? null : (int?) value;
			}
		}

		public virtual int? MaxNumberOfItems
		{
			get
			{
				var value = GetInt32(_groupName, "MaxNumberOfItems", -1);
				return value < 0 ? null : (int?)value;
			}
		}

		public bool IsDoorCodeValidate
		{
			get { return GetBoolean(_groupName, "IsDoorCodeValidate", false); }
		}

		public int MaxDoorCodeSymbols
		{
			get { return GetInt32(_groupName, "MaxDoorCodeSymbols", 5); }
		}

		public string ApplyStarSymbolForChannels
		{
			get
			{
				return GetString(_groupName, "ApplyStarSymbolForChannels", string.Empty);
			}
		}

		public List<string> GetChannelsThatApplyStarSymbol()
		{
			return new List<string>(ApplyStarSymbolForChannels.Split(','));
		}
	}
}