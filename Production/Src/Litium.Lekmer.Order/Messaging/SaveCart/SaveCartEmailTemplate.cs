﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order
{
	public class SaveCartEmailTemplate : Template
	{
		public SaveCartEmailTemplate(int templateId, IChannel channel)
			: base(templateId, channel, false)
		{
		}

		public SaveCartEmailTemplate(IChannel channel)
			: base("SaveCartEmail", channel, false)
		{
		}
	}
}