﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Litium.Lekmer.Common;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.SiteStructure.Web
{
	public class LekmerAreaHandler : AreaHandler
	{
		private readonly IContentPageFull _contentPage;
		private readonly IContentNodeTreeItem _contentNodeTreeItem;
		private readonly IContentAreaFull _contentArea;
		private readonly ContentPageHandler _contentPageHandler;
		private readonly IContentPageFull _masterContentPage;
		private readonly Dictionary<int, IBlockControl> _risedBlockControls;

		private ICommonSession _commonSession;
		protected ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
			set { _commonSession = value; }
		}

		public LekmerAreaHandler(IContentPageFull contentPage, IContentNodeTreeItem contentNodeTreeItem, IContentAreaFull contentArea, IContentPageFull masterContentPage, ContentPageHandler contentPageHandler) : base(contentPage, contentNodeTreeItem, contentArea, masterContentPage, contentPageHandler)
		{
			_contentPage = contentPage;
			_masterContentPage = masterContentPage;
			_contentNodeTreeItem = contentNodeTreeItem;
			_contentArea = contentArea;
			_contentPageHandler = contentPageHandler;

			_risedBlockControls = new Dictionary<int, IBlockControl>();
		}

		public virtual void InstantiateBlockControls()
		{
			Collection<IBlock> masterBlocks = GetMasterBlocks();
			Collection<IBlock> combinedBlocks = CombineBlocks(_contentArea.Blocks, masterBlocks);

			bool isSignedIn = IoC.Resolve<ICustomerSession>().IsSignedIn;

			foreach (IBlock block in combinedBlocks)
			{
				if (!ShowBlock(block, isSignedIn))
				{
					continue;
				}

				CreateBlockControlInstance(block);
			}
		}

		public override BlockContent RenderBlock(IBlock block)
		{
			var blockControl = CreateBlockControlInstance(block);

			HttpContext context = HttpContext.Current;
			if (!context.Trace.IsEnabled)
			{
				return RenderInternal(blockControl);
			}

			string traceCategory = "Block " + block.BlockType.CommonName + " Id=" + block.Id;
			context.Trace.Write(traceCategory, "Begin Render");

			BlockContent content = RenderInternal(blockControl);

			context.Trace.Write(traceCategory, "End Render");

			return content;
		}

		/// <summary>
		/// Finds specified type of blocks.
		/// </summary>
		public virtual Collection<TBlockControl> FindBlockControls<TBlockControl>() where TBlockControl : class
		{
			IEnumerable<TBlockControl> blockControls = _risedBlockControls.Values.Where(b => b is TBlockControl).Cast<TBlockControl>();

			return new Collection<TBlockControl>(blockControls.ToList());
		}

		protected virtual IBlockControl CreateBlockControlInstance(IBlock block)
		{
			if (_risedBlockControls.ContainsKey(block.Id))
			{
				return _risedBlockControls[block.Id];
			}

			IBlockControl blockControl = CreateBlockControlInstanceCore(block);

			_risedBlockControls[block.Id] = blockControl;

			return blockControl;
		}

		protected virtual IBlockControl CreateBlockControlInstanceCore(IBlock block)
		{
			var blockControl = IoC.Resolve<IBlockControl>(block.BlockType.CommonName);

			blockControl.ContentPage = _contentPage;
			blockControl.ContentNodeTreeItem = _contentNodeTreeItem;
			blockControl.ContentArea = _contentArea;
			blockControl.ContentPageHandler = _contentPageHandler;
			blockControl.BlockId = block.Id;

			return blockControl;
		}

		protected override bool ShowBlock(IBlock block, bool isSignedIn)
		{
			if ((BlockStatusInfo) block.BlockStatusId != BlockStatusInfo.Online) return false;

			var access = (AccessInfo) block.AccessId;
			if (!isSignedIn && access == AccessInfo.SignedIn) return false;
			if (isSignedIn && access == AccessInfo.NotSignedIn) return false;

			var lekmerBlock = block as ILekmerBlock;
			if (lekmerBlock != null && !lekmerBlock.IsInTimeLimit) return false;

			if (CheckBlockTargetDevice(lekmerBlock) == false)
			{
				return false;
			}

			return true;
		}

		protected virtual bool CheckBlockTargetDevice(ILekmerBlock block)
		{
			if (CommonSession.IsViewportDesktop)
			{
				if (block.ShowOnDesktop.HasValue && block.ShowOnDesktop.Value == false)
				{
					return false;
				}
			}
			else if (CommonSession.IsViewportMobile)
			{
				if (block.ShowOnMobile.HasValue && block.ShowOnMobile.Value == false)
				{
					return false;
				}
			}

			return true;
		}
	}
}
