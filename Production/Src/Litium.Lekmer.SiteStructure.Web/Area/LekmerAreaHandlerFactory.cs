﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Lekmer.SiteStructure.Web
{
	public static class LekmerAreaHandlerFactory
	{
		public static AreaHandler CreateAreaHandlerInstance(
			IContentPageFull contentPage, 
			IContentNodeTreeItem contentNodeTreeItem, 
			IContentAreaFull contentArea, 
			ContentPageHandler contentPageHandler)
		{
			IContentPageFull masterPage = null;
			if (contentPage.MasterPageId.HasValue)
			{
				var contentPageService = IoC.Resolve<IContentPageService>();
				masterPage = contentPageService.GetFullById(UserContext.Current, contentPage.MasterPageId.Value);
			}

			return new LekmerAreaHandler(contentPage, contentNodeTreeItem, contentArea, masterPage, contentPageHandler);
		}
	}
}
