﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.CdonExport
{
	public class CdonExportRepository
	{
		public virtual string GetLastItem()
		{
			string importId = string.Empty;

			var dbSettings = new DatabaseSetting("CdonExportRepository.GetLastItem");

			using (var dataReader = new DataHandler().ExecuteSelect("[integration].[pCdonExportGetLastItem]", dbSettings))
			{
				if (dataReader.Read())
				{
					importId = dataReader.GetString(0);
				}
			}

			return importId;
		}

		public virtual int Save(string importId, CdonExportType type, CdonExportStatus status)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ImportId", importId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("@Type", type, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@StatusCommonName", status, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("CdonExportRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[integration].[pCdonExportSave]", parameters, dbSettings);
		}

		public virtual void Update(int cdonExportId, CdonExportStatus status)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@CdonExportId", cdonExportId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@StatusCommonName", status, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("CdonExportRepository.Update");

			new DataHandler().ExecuteCommand("[integration].[pCdonExportUpdate]", parameters, dbSettings);
		}

		public virtual ProductIdCollection GetVariantIdAllByProduct(int productId, int relationListTypeId)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@RelationListTypeId", relationListTypeId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CdonExportRepository.GetVariantIdAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pProductVariantGetIdAllByProductForCdonExport]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdCollection GetVariantIdAllByVariantErpId(int productId)
		{
			var productIds = new ProductIdCollection();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CdonExportRepository.GetVariantIdAllByVariantErpId");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[integration].[pProductVariantGetIdAllByVariantErpIdForCdonExport]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		public virtual Dictionary<int, int> ProductMappingGetAll()
		{
			var productMapping = new Dictionary<int, int>();

			var dbSettings = new DatabaseSetting("CdonExportRepository.ProductMappingGetAll");

			using (var dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportProductMappingGetAll]", dbSettings))
			{
				while (dataReader.Read())
				{
					int productId = dataReader.GetInt32(0);
					int articleId = dataReader.GetInt32(1);

					if (!productMapping.ContainsKey(articleId))
					{
						productMapping.Add(articleId, productId);
					}
				}
			}

			return productMapping;
		}

		public virtual int ProductMappingSave(int cdonProductId, int cdonArticleId)
		{
			var productId = 0;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CdonProductId", cdonProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CdonArticleId", cdonArticleId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CdonExportRepository.ProductMappingSave");

			using (var dataReader = new DataHandler().ExecuteSelect("[export].[pCdonExportProductMappingSave]", parameters, dbSettings))
			{
				if (dataReader.Read())
				{
					productId = dataReader.GetInt32(0);
				}
			}

			return productId;
		}
	}
}