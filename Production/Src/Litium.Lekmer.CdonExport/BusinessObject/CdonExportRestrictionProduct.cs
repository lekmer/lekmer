﻿using System;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonExportRestrictionProduct : BusinessObjectBase, ICdonExportRestrictionProduct
	{
		private int _productRegistryId;
		private int _productId;
		private string _restrictionReason;
		private int? _userId;
		private DateTime _createdDate;
		private int _channelId;

		public int ProductRegistryId
		{
			get { return _productRegistryId; }
			set
			{
				CheckChanged(_productRegistryId, value);
				_productRegistryId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public string RestrictionReason
		{
			get { return _restrictionReason; }
			set
			{
				CheckChanged(_restrictionReason, value);
				_restrictionReason = value;
			}
		}

		public int? UserId
		{
			get { return _userId; }
			set
			{
				CheckChanged(_userId, value);
				_userId = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}
	}
}