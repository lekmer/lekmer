﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport
{
	[Serializable]
	public class CdonExportRestriction : BusinessObjectBase, ICdonExportRestriction
	{
		private Collection<ICdonExportRestrictionItem> _includeProducts;
		private Collection<ICdonExportRestrictionItem> _excludeProducts;
		private Collection<ICdonExportRestrictionItem> _includeCategories;
		private Collection<ICdonExportRestrictionItem> _excludeCategories;
		private Collection<ICdonExportRestrictionItem> _includeBrands;
		private Collection<ICdonExportRestrictionItem> _excludeBrands;

		public Collection<ICdonExportRestrictionItem> IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}
		public Collection<ICdonExportRestrictionItem> ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}
		public Collection<ICdonExportRestrictionItem> IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}
		public Collection<ICdonExportRestrictionItem> ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}
		public Collection<ICdonExportRestrictionItem> IncludeBrands
		{
			get { return _includeBrands; }
			set
			{
				CheckChanged(_includeBrands, value);
				_includeBrands = value;
			}
		}
		public Collection<ICdonExportRestrictionItem> ExcludeBrands
		{
			get { return _excludeBrands; }
			set
			{
				CheckChanged(_excludeBrands, value);
				_excludeBrands = value;
			}
		}
	}
}