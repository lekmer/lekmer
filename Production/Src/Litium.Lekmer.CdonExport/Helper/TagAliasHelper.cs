﻿using System.Collections.Generic;
using System.Web.Script.Serialization;
using Litium.Lekmer.Product;
using Litium.Scensum.Template;

namespace Litium.Lekmer.CdonExport
{
	public class TagAliasHelper
	{
		private ICdonExportSetting _cdonExportSetting;
		private ITemplateSharedService _templateSharedService;
		private ITagGroupService _tagGroupService;

		private Dictionary<string, string> _infoPointTagGroups;
		private Dictionary<string, string> InfoPointTagGroups
		{
			get { return _infoPointTagGroups ?? (_infoPointTagGroups = _tagGroupService.InfoPointTagGroupGetAll()); }
		}

		public TagAliasHelper(ICdonExportSetting cdonExportSetting, ITemplateSharedService templateSharedService, ITagGroupService tagGroupService)
		{
			_cdonExportSetting = cdonExportSetting;
			_templateSharedService = templateSharedService;
			_tagGroupService = tagGroupService;
		}

		public virtual Dictionary<string, string> GetAliasForTags(string templateId)
		{
			var tagAliasCollection = new Dictionary<string, string>();

			int id;
			if (!int.TryParse(templateId, out id))
			{
				return tagAliasCollection;
			}

			var template = _templateSharedService.GetFullById(id);
			if (template == null || template.Fragments == null)
			{
				return tagAliasCollection;
			}

			foreach (var fragment in template.Fragments)
			{
				if (fragment.CommonName != _cdonExportSetting.FragmentName) continue;

				var jsonString = fragment.Content
					.Replace(_cdonExportSetting.AliasBegin, _cdonExportSetting.AliasBeginReplace)
					.Replace(_cdonExportSetting.AliasEnd, _cdonExportSetting.AliasEndReplace);

				var serializer = new JavaScriptSerializer();
				var json = serializer.Deserialize<Dictionary<string, object>>(jsonString);

				if (json.ContainsKey(_cdonExportSetting.MainKey))
				{
					var tagGroups = (Dictionary<string, object>)json[_cdonExportSetting.MainKey];
					foreach (var tagGroup in tagGroups)
					{
						if (InfoPointTagGroups.ContainsKey(tagGroup.Key))
						{
							var tags = (Dictionary<string, object>)tagGroup.Value;
							foreach (var tag in tags)
							{
								var key = InfoPointTagGroups[tagGroup.Key] + "_" + tag.Key;
								if (!tagAliasCollection.ContainsKey(key))
								{
									tagAliasCollection.Add(key, tag.Value.ToString());
								}
							}
						}
					}
				}
			}

			return tagAliasCollection;
		}
	}
}