﻿using System.Globalization;
using Litium.Framework.Search;
using Litium.Framework.Search.Indexing;
using Litium.Lekmer.Product;
using Litium.Lekmer.Search.Lucene.Querying;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch.Indexing;

namespace Litium.Lekmer.ProductSearch
{
	public class LekmerProductIndexMapper : ProductIndexMapper
	{
		protected override IndexDocument MapItem(IProductIndexItem product)
		{
			var document = new IndexDocument();

			document.Id = product.Id.ToString(CultureInfo.CurrentCulture);
			document.Title = product.Title;
			document.Body.Add(product.ShortDescription);
			document.Body.Add(product.Description);;
			document.Tags.Add(
				new Tag("OrderCount", product.OrderCount.ToString(CultureInfo.CurrentCulture)));
			document.Tags.Add(new Tag(LekmerFieldNames.LekmerErpId,((ILekmerProductIndexItem)product).LekmerErpId));
			document.Boost = CalculateBoost(product.OrderCount);
			return document;
		}
	}
}
