﻿using System.Collections.ObjectModel;
using Litium.Framework.Search.Indexing;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch.Indexing;

namespace Litium.Lekmer.ProductSearch.Indexing
{
	public class LekmerProductIndexJob : ProductIndexJob
	{
		public LekmerProductIndexJob(IChannelService channelService, IProductIndexItemService productService) : base(channelService, productService)
		{
		}
		protected override void Create(IIndexingService indexingService, ProductIndexItemCollection products)
		{
			Collection<IndexDocument> documents = new LekmerProductIndexMapper().Map(products);
			indexingService.Build(documents, IndexName, LanguageId, false);
		}

		protected override void Append(IIndexingService indexingService, ProductIndexItemCollection products)
		{
			Collection<IndexDocument> documents = new LekmerProductIndexMapper().Map(products);
			indexingService.Add(documents, IndexName, LanguageId, false);
		}
	}
}
