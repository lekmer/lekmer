﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Common.Repository
{
	public class BlockContactUsRepository
	{
		protected DataMapperBase<IBlockContactUs> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockContactUs>(dataReader);
		}

		public virtual void Save(IBlockContactUs block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Receiver", block.Receiver, SqlDbType.VarChar)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockContactUsRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockContactUsSave]", parameters, dbSettings);
		}

		public virtual IBlockContactUs GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockContactUsRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
					"[lekmer].[pBlockContactUsGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockContactUs GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockContactUsRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
					"[lekmer].[pBlockContactUsGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
			};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockContactUsRepository.Delete");        
			new DataHandler().ExecuteCommand("[lekmer].[pBlockContactUsDelete]", parameters, dbSettings);
		}
	}
}
