﻿using System.Web;

namespace Litium.Lekmer.Common.Extensions
{
	public static class HttpRequestExtension
	{
		public static bool IsSecureConnection(this HttpRequest httpRequest)
		{
			if (httpRequest == null)
			{
				return false;
			}

			if (httpRequest.IsSecureConnection)
			{
				return true;
			}

			if (httpRequest.ServerVariables["SERVER_PORT"] == LekmerWebSetting.Instance.SecureConnectionPort)
			{
				return true;
			}

			return false;
		}
	}
}
