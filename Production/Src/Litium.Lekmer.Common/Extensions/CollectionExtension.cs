﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Common.Extensions
{
	public static class CollectionExtension
	{
		public static Collection<T> AddRange<T>(this Collection<T> collection, Collection<T> addCollection)
		{
			foreach (T block in addCollection)
			{
				collection.Add(block);
			}
			return collection;
		}

		public static ICollection<T> AddRange<T>(this ICollection<T> collection, ICollection<T> addCollection)
		{
			foreach (T block in addCollection)
			{
				collection.Add(block);
			}
			return collection;
		}

		public static bool IsEmpty<T>(this ICollection<T> collection)
		{
			if (collection == null)
			{
				return true;
			}

			if (collection.Count == 0)
			{
				return true;
			}

			return false;
		}


		/// <summary>
		/// Returns the value as a string.
		/// </summary>
		/// <param name="results"></param>
		/// <param name="property"></param>
		/// <returns>The string value, or null if the property does not exit.</returns>
		public static string GetString(this SortedList<string, string[]> results, string property)
		{
			string[] value = null;
			results.TryGetValue(property, out value);
			if (value != null)
				return string.Join("|", value);
			return null;
		}

		/// <summary>
		/// Returns the value as a boolean.
		/// </summary>
		/// <param name="results"></param>
		/// <param name="property"></param>
		/// <returns>The boolean value, or null if the value is not a boolean type.</returns>
		public static bool? GetBool(this SortedList<string, string[]> results, string property)
		{
			bool value = false;
			if (bool.TryParse(results.GetString(property), out value))
				return value;
			return null;
		}

		/// <summary>
		/// Returns the value as a double.
		/// </summary>
		/// <param name="results"></param>
		/// <param name="property"></param>
		/// <returns>The double value, or null if the value is not a numeric type.</returns>
		public static double? GetDouble(this SortedList<string, string[]> results, string property)
		{
			double value = 0;
			if (double.TryParse(results.GetString(property), out value))
				return value;
			return null;
		}
	}
}
