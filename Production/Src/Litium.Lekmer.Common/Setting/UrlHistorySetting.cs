using Litium.Framework.Setting;

namespace Litium.Lekmer.Product.Setting
{
	public class UrlHistorySetting : SettingBase
	{
		private const string _storageName = "UrlHistory";
		private const string GROUP_NAME_PRODUCT = "Product";
		private const string GROUP_NAME_CONTENT_PAGE = "ContentPage";

		protected override string StorageName
		{
			get { return _storageName; }
		}

		public int ProductMaxCount()
		{
			return GetInt32(GROUP_NAME_PRODUCT, "MaxCount", 5);
		}

		public int ContentPageMaxCount()
		{
			return GetInt32(GROUP_NAME_CONTENT_PAGE, "MaxCount", 5);
		}
	}
}