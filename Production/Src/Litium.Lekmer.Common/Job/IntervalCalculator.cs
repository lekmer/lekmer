﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace Litium.Lekmer.Common.Job
{
	public class IntervalCalculator : IIntervalCalculator
	{
		public virtual TimeSpan NextWeeklyExecution(DateTime timeFrom, IScheduleSetting scheduleSetting)
		{
			DateTime timeTodayExecution = timeFrom.Date
				.AddHours(Math.Abs(scheduleSetting.WeeklyExecutionHour))
				.AddMinutes(Math.Abs(scheduleSetting.WeeklyExecutionMinute));

			var timeNextExecutions = new Collection<DateTime>();

			if (scheduleSetting.WeeklyExecutionMonday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Monday));
			}
			if (scheduleSetting.WeeklyExecutionTuesday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Tuesday));
			}
			if (scheduleSetting.WeeklyExecutionWednesday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Wednesday));
			}
			if (scheduleSetting.WeeklyExecutionThursday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Thursday));
			}
			if (scheduleSetting.WeeklyExecutionFriday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Friday));
			}
			if (scheduleSetting.WeeklyExecutionSaturday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Saturday));
			}
			if (scheduleSetting.WeeklyExecutionSunday)
			{
				timeNextExecutions.Add(NextWeeklyExecutionDay(timeFrom, timeTodayExecution, DayOfWeek.Sunday));
			}

			if (timeNextExecutions.Count == 0)
			{
				if (timeTodayExecution < timeFrom)
				{
					timeTodayExecution = timeTodayExecution.AddDays(1);
				}

				return timeTodayExecution - timeFrom;
			}

			return timeNextExecutions.Min() - timeFrom;
		}

		public virtual TimeSpan NextDailyExecution(DateTime timeFrom, IScheduleSetting scheduleSetting)
		{
			DateTime timeX = timeFrom.Date
				.AddHours(Math.Abs(scheduleSetting.DailyExecutionHour))
				.AddMinutes(Math.Abs(scheduleSetting.DailyExecutionMinute));

			// If it's already past, wait until tomorrow.
			if (timeFrom > timeX)
			{
				timeX = timeX.AddDays(1);
			}

			return timeX - timeFrom;
		}

		public virtual TimeSpan FirstRepeatExecution(DateTime timeFrom, IScheduleSetting scheduleSetting)
		{
			var interval = new TimeSpan(Math.Abs(scheduleSetting.RepeatExecutionIntervalHours), Math.Abs(scheduleSetting.RepeatExecutionIntervalMinutes), 0);

			if (interval.TotalMinutes < 1)
			{
				interval = TimeSpan.FromMinutes(1);
			}

			DateTime startTime = timeFrom.Date
				.AddHours(Math.Abs(scheduleSetting.RepeatExecutionStartHour))
				.AddMinutes(Math.Abs(scheduleSetting.RepeatExecutionStartMinute));

			if (startTime > timeFrom)
			{
				return startTime - timeFrom;
			}

			double minutes = (timeFrom - startTime).TotalMinutes % interval.TotalMinutes;

			return interval - TimeSpan.FromMinutes(minutes);
		}

		public virtual TimeSpan NextRepeatExecution(DateTime timeFrom, IScheduleSetting scheduleSetting)
		{
			var interval = new TimeSpan(Math.Abs(scheduleSetting.RepeatExecutionIntervalHours), Math.Abs(scheduleSetting.RepeatExecutionIntervalMinutes), 0);

			if (interval.TotalMinutes < 1)
			{
				interval = TimeSpan.FromMinutes(1);
			}

			return interval;
		}


		protected virtual DateTime NextWeeklyExecutionDay(DateTime timeFrom, DateTime timeTodayExecution, DayOfWeek dayOfWeek)
		{
			int days = dayOfWeek - timeTodayExecution.DayOfWeek;
			if (days < 0)
			{
				days += 7;
			}

			DateTime timeNextExecution = timeTodayExecution.AddDays(days);

			if (timeNextExecution < timeFrom)
			{
				timeNextExecution = timeNextExecution.AddDays(7);
			}

			return timeNextExecution;
		}
	}
}
