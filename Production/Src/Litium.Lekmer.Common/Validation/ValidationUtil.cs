﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Litium.Lekmer.Common
{
	public static class ValidationUtil
	{
		#region Regex

		private static string _emailValidationPattern = @"^([-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
		private static string _postalCodeNlValidationPattern = @"^[1-9][0-9]{3}[- ]?[a-zA-Z]{2}$";

		private const string _doorCodeGeneralPattern = @"^[0-9]+$";
		private const string _doorCodeExtendedGeneralPattern = @"^[*0-9]+$";

		#endregion

		/// <summary>
		/// Validates if the input string is a valid e-mail address.
		/// </summary>
		/// <param name="input">An e-mail address to be validated.</param>
		/// <returns>True if the input is a valid e-mail address, false if not.</returns>
		public static bool IsValidEmail(string input)
		{
			if (Regex.Match(input, _emailValidationPattern).Success == false)
			{
				return false;
			}

			/*
				The domain can be bracketed, unbracketed, or an IP address.
				An unbracketed domain consists of labels separated by periods and less than 253 characters.
				-Labels must be less than 63 characters.
				-Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
				A bracketed domain starts with [, ends with ] and the contents (after unescaping) must be less than 245 characters.
				-Labels must be less than 63 characters.
				-Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
			 * 
			 * Go to http://rumkin.com/software/email/rules.php for more details.
			*/

			string domain = input.Substring(input.IndexOf("@", StringComparison.OrdinalIgnoreCase) + 1);

			// No domain can start with a period, end with a period, or have two successive periods.
			if (domain.StartsWith(".", StringComparison.OrdinalIgnoreCase) || domain.EndsWith(".", StringComparison.OrdinalIgnoreCase) || domain.Contains(".."))
			{
				return false;
			}

			domain = domain.TrimStart('[').TrimEnd(']');

			// Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
			if (domain.Contains("--"))
			{
				return false;
			}

			// Labels must not start with a hyphen, end with a hyphen, or contain two successive hyphens.
			foreach (string label in domain.Split('.'))
			{
				if (label.StartsWith("-", StringComparison.OrdinalIgnoreCase) || label.EndsWith("-", StringComparison.OrdinalIgnoreCase))
				{
					return false;
				}
			}

			return true;
		}

		public static bool IsHouseDetailsRequired(string countryIso)
		{
			return countryIso.ToUpperInvariant() == "NL";
		}

		public static bool IsHouseNumberRequired(string countryIso)
		{
			return countryIso.ToUpperInvariant() == "NL";
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "countryIso")]
		public static bool IsHouseExtensionRequired(string countryIso)
		{
			// House extension could be empty in NL, other channels don't need it
			return false;
		}

		public static bool IsGenderTypeRequired(string countryIso)
		{
			return countryIso.ToUpperInvariant() == "NL";
		}

		public static bool IsValidGenderType(int genderTypeId)
		{
			return genderTypeId == (int)GenderTypeInfo.Unknown || genderTypeId == (int)GenderTypeInfo.Man || genderTypeId == (int)GenderTypeInfo.Woman;
		}

		/// <summary>
		/// Validates if the input string is a valid postal code.
		/// </summary>
		/// <param name="countryIso">A country iso for which need to validate input</param>
		/// <param name="input">A postal code to be validated.</param>
		/// <returns>True if the input is a valid postal code, false if not.</returns>
		public static bool IsValidPostalCode(string countryIso, string input)
		{
			return countryIso.ToUpperInvariant() != "NL" || Regex.Match(input, _postalCodeNlValidationPattern).Success;
		}

		/// <summary>
		/// Validates if the input string is a valid door code.
		/// </summary>
		/// <param name="countryIso">A country iso for which need to validate input</param>
		/// <param name="input">A door code to be validated.</param>
		/// <param name="channelsThatApplyStarSymbol">List of Channels that apply (*) for door code</param>
		/// <returns>True if the input is a valid door code, false if not.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists")]
		public static bool IsValidDoorCode(string countryIso, string input, List<string> channelsThatApplyStarSymbol)
		{
			return channelsThatApplyStarSymbol != null && channelsThatApplyStarSymbol.Contains(countryIso)
				? Regex.Match(input, _doorCodeExtendedGeneralPattern).Success
				: Regex.Match(input, _doorCodeGeneralPattern).Success;
		}
	}
}