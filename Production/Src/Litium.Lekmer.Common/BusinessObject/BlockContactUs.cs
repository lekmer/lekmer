﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Common
{
	[Serializable]
	public class BlockContactUs : BlockBase, IBlockContactUs
	{
		private string _receiver;

		public string Receiver
		{
			get { return _receiver; }
			set
			{
				CheckChanged(_receiver, value);
				_receiver = value;
			}
		}
	}
}
