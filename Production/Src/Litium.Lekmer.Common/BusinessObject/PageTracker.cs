using System;

namespace Litium.Lekmer.Common
{
	[Serializable]
	public class PageTracker : IPageTracker
	{
		public int Current { get; private set; }

		public int PageSize { get; private set; }

		public int ItemsLeft { get; private set; }

		public int ItemsTotalCount { get; private set; }

		public bool HasNext
		{
			get { return ItemsLeft > 0; }
		}

		public PageTracker()
		{
			PageSize = 100;
			ItemsTotalCount = 0;

			Current = 1;
			ItemsLeft = 0;
		}

		public IPageTracker Initialize(int pageSize, int itemsTotalCount)
		{
			PageSize = pageSize;
			ItemsTotalCount = itemsTotalCount;

			Current = 1;
			ItemsLeft = ItemsTotalCount;

			return this;
		}

		public void MoveToNext(int itemsTotalCount)
		{
			ItemsTotalCount = itemsTotalCount;
			ItemsLeft = ItemsTotalCount - PageSize * Current;
			if (ItemsLeft < 0)
			{
				ItemsLeft = 0;
			}
			Current++;
		}
	}
}