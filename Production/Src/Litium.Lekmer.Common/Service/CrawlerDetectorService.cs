﻿using System.Text.RegularExpressions;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.Common
{
	public class CrawlerDetectorService : ICrawlerDetectorService
	{
		private const string CrawlerPattern =
				@"slurp|ask|teoma|google|bot|yahoo|spider
				|archiver|curl|python|nambu|twitt|perl|sphere|PEAR|java
				|wordpress|radian|crawl|yandex|eventbox|monitor|mechanize|facebookexternal
				|CDONLog|Pimonster|Wget|nagios|search";

		private static readonly Regex _crawlerRegex = new Regex(CrawlerPattern, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace | RegexOptions.Compiled);

		public virtual bool IsMatch(string userAgent)
		{
			if (userAgent.IsEmpty())
			{
				return false;
			}

			return _crawlerRegex.IsMatch(userAgent);
		}
	}
}
