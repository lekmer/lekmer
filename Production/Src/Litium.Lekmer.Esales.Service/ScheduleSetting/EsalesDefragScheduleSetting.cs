﻿using Litium.Lekmer.Common.Job;

namespace Litium.Lekmer.Esales.Service
{
	public class EsalesDefragScheduleSetting : BaseScheduleSetting
	{
		protected override string StorageName
		{
			get { return "Esales"; }
		}

		protected override string GroupName
		{
			get { return "EsalesDefragJob"; }
		}
	}
}