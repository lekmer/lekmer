﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Service
{
	public class ProductChangesCleanUpJob : BaseJob
	{
		public override string Name
		{
			get { return "ProductChangesCleanUp"; }
		}

		protected override void ExecuteAction()
		{
			var productChangesService = IoC.Resolve<IProductChangesService>();
			productChangesService.DeleteExpiredItems();
		}
	}
}
