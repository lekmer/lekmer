﻿using Litium.Lekmer.Common.Job;
using Litium.Lekmer.Esales.Exporter;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Esales.Service
{
	public class EsalesExportIncrementalJob : BaseJob
	{
		public override string Name
		{
			get { return "ExportProductToEsalesIncremental"; }
		}

		protected override void ExecuteAction()
		{
			var esalesProductInfoIncrementalExporter = IoC.Resolve<IExporter>("EsalesProductInfoIncrementalExporter");
			esalesProductInfoIncrementalExporter.Execute();
		}
	}
}
