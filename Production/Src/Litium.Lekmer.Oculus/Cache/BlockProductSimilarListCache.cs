﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Oculus
{
	public sealed class BlockProductSimilarListCache : ScensumCacheBase<BlockProductSimilarListKey, IBlockProductSimilarList>
	{
		private static readonly BlockProductSimilarListCache _instance = new BlockProductSimilarListCache();

		private BlockProductSimilarListCache()
		{
		}

		public static BlockProductSimilarListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockProductSimilarListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockProductSimilarListKey : ICacheKey
	{
		public BlockProductSimilarListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}