﻿using Litium.Lekmer.Oculus.Repository;

namespace Litium.Lekmer.Oculus
{
	public class ProductSimilarService : IProductSimilarService
	{
		protected ProductSimilarRepository Repository { get; private set; }

		public ProductSimilarService(ProductSimilarRepository repository)
		{
			Repository = repository;
		}

		public IProductSimilar GetAllByProductId(int productId)
		{
			return ProductSimilarCache.Instance.TryGetItem(
				new ProductSimilarKey(productId),
				delegate { return Repository.GetAllByProductId(productId); });
		}
	}
}