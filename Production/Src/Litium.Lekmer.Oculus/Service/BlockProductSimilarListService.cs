﻿using Litium.Lekmer.Oculus.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Oculus
{
	public class BlockProductSimilarListService : IBlockProductSimilarListService
	{
		protected BlockProductSimilarListRepository Repository { get; private set; }

		public BlockProductSimilarListService(BlockProductSimilarListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductSimilarList GetById(IUserContext context, int id)
		{
			return BlockProductSimilarListCache.Instance.TryGetItem(
				new BlockProductSimilarListKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockProductSimilarList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}