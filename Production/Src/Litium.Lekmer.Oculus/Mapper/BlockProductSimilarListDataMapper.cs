﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Oculus.Mapper
{
	public class BlockProductSimilarListDataMapper : DataMapperBase<IBlockProductSimilarList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockProductSimilarListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockProductSimilarList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockTopList = IoC.Resolve<IBlockProductSimilarList>();

			block.ConvertTo(blockTopList);

			blockTopList.PriceRangeType = MapValue<int>("BlockProductSimilarList.PriceRangeType");
			blockTopList.Setting = _blockSettingDataMapper.MapRow();

			blockTopList.SetUntouched();

			return blockTopList;
		}
	}
}