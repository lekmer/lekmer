using System.Globalization;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Review.Web
{
	public class BlockProductReviewEntityMapper : LekmerBlockEntityMapper<IBlockProductReview>
	{
		public override void AddEntityVariables(Fragment fragment, IBlockProductReview item)
		{
			base.AddEntityVariables(fragment, item);

			fragment.AddVariable("Block.RowCount", item.RowCount.ToString(CultureInfo.InvariantCulture));
		}
	}
}