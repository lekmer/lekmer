﻿using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber.Service
{
	public class NewsletterUnsubscriberJob : BaseJob
	{
		public override string Group
		{
			get { return "Newsletter"; }
		}

		public override string Name
		{
			get { return "NewsletterUnsubscriberJob"; }
		}

		protected override void ExecuteAction()
		{
			var newsletterUnsubscriberExporter = IoC.Resolve<INewsletterUnsubscriberExporter>();
			newsletterUnsubscriberExporter.Execute();
		}
	}
}