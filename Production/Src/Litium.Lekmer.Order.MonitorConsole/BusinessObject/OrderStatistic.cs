﻿using System;

namespace Litium.Lekmer.Order.MonitorConsole
{
	public class OrderStatistic
	{
		public DateTime Date { get; set; }
		public int NumberOfOrders { get; set; }
		public decimal AverageOrderPrice { get; set; }
		public decimal TotalOrderValue { get; set; }
		public string Channel { get; set; }
	}
}