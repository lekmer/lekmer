﻿using System;
using System.Diagnostics;
using System.Reflection;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.SIR.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.InfoFormat("SIR.Console started.");

				var sirExporter = IoC.Resolve<IExporter>("SirExporter");
				sirExporter.Execute();

				_log.InfoFormat("SIR.Console completed.");
				stopwatch.Stop();
				_log.InfoFormat("SIR.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch (Exception ex)
			{
				_log.Error("SIR.Console failed.", ex);
			}
		}
	}
}