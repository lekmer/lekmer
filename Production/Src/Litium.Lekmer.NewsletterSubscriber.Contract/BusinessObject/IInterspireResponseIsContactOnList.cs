﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireResponseIsContactOnList : IInterspireResponse
	{
		bool ContactIsLocated { get; set; }
	}
}