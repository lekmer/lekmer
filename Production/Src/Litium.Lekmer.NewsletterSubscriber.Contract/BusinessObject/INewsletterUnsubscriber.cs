﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterUnsubscriber : IBusinessObjectBase
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		string Email { get; set; }
		DateTime CreatedDate { get; set; }
		DateTime UpdatedDate { get; set; }
		Collection<INewsletterUnsubscriberOption> UnsubscriberOptions { get; set; }
	}
}