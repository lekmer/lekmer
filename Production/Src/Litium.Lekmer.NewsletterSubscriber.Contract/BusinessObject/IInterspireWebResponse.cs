﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireWebResponse
	{
		int StatusCode { get; set; }
		string StatusDescription { get; set; }
		string Response { get; set; }
	}
}