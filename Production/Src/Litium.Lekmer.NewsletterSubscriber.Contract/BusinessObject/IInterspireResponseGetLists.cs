﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface IInterspireResponseGetLists : IInterspireResponse
	{
		Collection<IInterspireContactList> ContactLists { get; set; }
	}
}