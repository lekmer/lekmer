﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public static class NewsletterSubscriberConstants
	{
		public const string LekmerApplication = "Lekmer";
		public const string HeppoApplication = "Heppo";

		public const string XmlRequestNode = "xmlrequest";
		public const string UserNameNode = "username";
		public const string UserTokenNode = "usertoken";
		public const string RequestTypeNode = "requesttype";
		public const string RequestMethodNode = "requestmethod";
		public const string DetailsNode = "details";
		public const string EmailAddressNode = "emailaddress";
		public const string MailingListNode = "mailinglist";
		public const string EmailNode = "email";
		public const string ListNode = "list";
		public const string ListUpperNode = "List";
		public const string ListIdNode = "listid";
		public const string BanIdNode = "banid";
		public const string PageIdNode = "pageid";
		public const string PerpageNode = "perpage";
		public const string SearchInfoNode = "searchinfo";
		public const string FormatNode = "format";
		public const string ConfirmedNode = "confirmed";

		public const string RequestTypeNodeValueSubscribers = "subscribers";
		public const string RequestTypeNodeValueUser = "user";
		public const string RequestMethodNodeValueAddSubscriberToList = "AddSubscriberToList";
		public const string RequestMethodNodeValueDeleteSubscriber = "DeleteSubscriber";
		public const string RequestMethodNodeValueUnsubscribeSubscriber = "UnsubscribeSubscriber";
		public const string RequestMethodNodeValueGetLists = "GetLists";
		public const string RequestMethodNodeValueIsSubscriberOnList = "IsSubscriberOnList";
		public const string RequestMethodNodeValueAddBannedSubscriber = "AddBannedSubscriber";
		public const string RequestMethodNodeValueFetchBannedSubscribers = "FetchBannedSubscribers";
		public const string RequestMethodNodeValueRemoveBannedSubscriber = "RemoveBannedSubscriber";

		public const string FormatNodeValueHtml = "html";
		public const string ConfirmedNodeValueYes = "yes";

		public const string BanListId = "global";
		public const string AllValue = "all";
	}
}