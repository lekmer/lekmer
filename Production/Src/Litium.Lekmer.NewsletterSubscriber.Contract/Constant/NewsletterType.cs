﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public enum NewsletterType
	{
		Unknown = 0,
		InterspireContactList = 1,
		MonitorProduct = 2,
		OrderReview = 3
	}
}