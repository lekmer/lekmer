﻿namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterSubscriberExporter
	{
		void Execute();
	}
}