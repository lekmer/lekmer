﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.NewsletterSubscriber
{
	public interface INewsletterSubscriberSecureService
	{
		INewsletterSubscriber Create(int channelId);
		int Save(ISystemUserFull systemUserFull, INewsletterSubscriber subscriber);
		bool Delete(ISystemUserFull systemUserFull, int id);
		INewsletterSubscriber Get(int id);
		Collection<INewsletterSubscriber> SearchByEmail(int channelId, string email, int page, int pageSize, out int itemsCount);
	}
}