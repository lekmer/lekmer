﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.Cart;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web.ExtendedCart
{
	public class ExtendedCartControl : CartControl
	{
		private readonly IOrderService _orderService;
		private readonly ICartService _cartService;

		private readonly Dictionary<LekmerCartItemKey, bool> _updatedItems = new Dictionary<LekmerCartItemKey, bool>();
		private int _maxQuantity;
		private CartHelper _cartHelper;

		public ExtendedCartControl(ICartService cartService, ICartSession cartSession, IFormatter formatter,
								   ITemplateFactory templateFactory)
			: base(cartService, cartSession, formatter, templateFactory)
		{
			LekmerFormatter = (ILekmerFormatter)formatter;
			_cartService = cartService;
			_orderService = IoC.Resolve<IOrderService>();
		}

		protected override string ModelCommonName
		{
			get { return "ExtendedCartComponent"; }
		}

		protected ILekmerFormatter LekmerFormatter { get; private set; }

		private bool IsChangeQuantityPost()
		{
			return PostMode.Equals("changequantity");
		}

		protected virtual ILekmerCartFull GetCart()
		{
			LekmerCartHelper.RefreshSessionCart();
			return (ILekmerCartFull)IoC.Resolve<ICartSession>().Cart;
		}

		public override ComponentContent Render()
		{
			Initialize();

			ILekmerCartFull cart = GetCart();
			if (cart == null)
			{
				return new ComponentContent();
			}
			Collection<ILekmerCartItem> cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (cartItems.Count == 0)
			{
				return new ComponentContent();
			}

			if (IsPostBack && IsChangeQuantityPost())
			{
				ChangeQuantityOfItems(cart);
				cart = GetCart();
				if (cart == null)
				{
					return new ComponentContent();
				}
				cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
				if (cartItems.Count == 0)
				{
					return new ComponentContent();
				}
			}

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			string body = RenderCart() ?? RenderEmptyCart();
			return new ComponentContent(head, body, footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private void Initialize()
		{
			if (!int.TryParse(Template.GetSettingOrNull("MaxQuantity"), out _maxQuantity))
			{
				_maxQuantity = 10;
			}

			_cartHelper = new CartHelper();
		}

		private void ChangeQuantityOfItems(ILekmerCartFull cart)
		{
			foreach (ILekmerCartItem cartItem in cart.GetGroupedCartItemsByProductAndPriceAndSize())
			{
				string key = GetQuantityFormKey(cartItem);

				int? newQuantity = Request.Form.GetInt32OrNull(key);
				if (!newQuantity.HasValue) continue;

				if (newQuantity.Value <= 0)
				{
					//Delete item by productId and price
					cart.DeleteItem(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
				}
				else if (newQuantity.Value != cartItem.Quantity)
				{
					//Get actual quantity which need add or remove
					int quantity = newQuantity.Value - cartItem.Quantity;

					cart.ChangeItemQuantityForBlockExtendedCartControl(
						cartItem.Product,
						quantity,
						cartItem.SizeId,
						cartItem.IsAffectedByCampaign,
						cartItem.PackageElements);

					var cartItemKey = new LekmerCartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
					_updatedItems[cartItemKey] = true;
				}
			}

			_cartService.Save(UserContext.Current, cart);
			LekmerCartHelper.RefreshSessionCartHard();
		}

		protected override string RenderCart()
		{
			var cart = (ILekmerCartFull)IoC.Resolve<ICartSession>().Cart;
			if (cart == null)
			{
				return null;
			}
			Collection<ICartItem> cartItems = cart.GetGroupedCartItemsByProductAndPrice();
			if (cartItems.Count == 0)
			{
				return null;
			}

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Iterate:CartItem", RenderCartItems(cartItems), VariableEncoding.None);

			// Cart info
			_cartHelper.RenderCartInfo(fragmentContent, cart, LekmerFormatter);

			// Cart price
			_cartHelper.RenderCartPrices(fragmentContent, cart, LekmerFormatter);

			return fragmentContent.Render();
		}

		protected override string RenderCartItems(Collection<ICartItem> cartItems)
		{
			var itemBuilder = new StringBuilder();

			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var groupedCartItems = cartItems.GroupBy(item => string.Format("{0}-{1}-{2}", item.Product.Id, ((ILekmerCartItem)item).SizeId, ((ILekmerCartItem)item).PackageElements.GetPackageElementsHashCode()));

			foreach (ICartItem cartItem in cartItems)
			{
				itemBuilder.Append(RenderCartItem(cartItem, groupedCartItems));
			}

			return itemBuilder.ToString();
		}

		/// <summary>
		/// Renders a cart item.
		/// </summary>
		/// <param name="cartItem">The cart item to render.</param>
		/// <param name="groupedCartItems">Grouped Cart Items</param>
		/// <returns>Rendered item.</returns>
		private string RenderCartItem(ICartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems)
		{
			var cartItemKey = new LekmerCartItemKey(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, ((ILekmerCartItem)cartItem).SizeId, ((ILekmerCartItem)cartItem).PackageElements.GetPackageElementsHashCode());
			bool itemWasUpdated = _updatedItems.ContainsKey(cartItemKey);

			Fragment fragment = Template.GetFragment("CartItem");
			fragment.AddEntity(cartItem.Product);
			fragment.AddVariable("Product.CampaignInfoPriceHidden", cartItem.Product.CampaignInfo.Price.IncludingVat.ToString());
			fragment.AddVariable("CartItem.Quantity", Formatter.FormatNumber(Channel.Current, cartItem.Quantity));

			var lekmerCartItem = (ILekmerCartItem)cartItem;

			// Price
			_cartHelper.RenderCartItemPrices(fragment, lekmerCartItem, LekmerFormatter);

			fragment.AddCondition("Product.IsInStock", ((ILekmerOrderService) _orderService).CheckNumberInStock(UserContext.Current, cartItem, groupedCartItems));

			int quantityLimit = ObtainQuantityLimit(lekmerCartItem);
			fragment.AddVariable("Product.NumberInStockForSize", quantityLimit.ToString(CultureInfo.InvariantCulture), VariableEncoding.None);
			fragment.AddVariable("Iterate:QuantityOption", RenderQuantityOptions(cartItem.Quantity, quantityLimit), VariableEncoding.None);
			fragment.AddCondition("ItemWasUpdated", itemWasUpdated);
			fragment.AddCondition("IsNumberInStockTooSmall", cartItem.Quantity > quantityLimit);

			fragment.AddCondition("CartItem.HasSize", lekmerCartItem.SizeId.HasValue);
			fragment.AddEntity(GetSize(lekmerCartItem));

			bool isPackageWithSizes = _cartHelper.IsPackageWithSizes(lekmerCartItem);
			fragment.AddCondition("CartItem.IsPackageWithSizes", isPackageWithSizes);
			if (isPackageWithSizes)
			{
				fragment.AddVariable("CartItem.PackageElementsHashCode", lekmerCartItem.PackageElements.GetPackageElementsHashCode().ToString());
			}

			fragment.AddRegexVariable(
				@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
					{
						string tagGroupCommonName = match.Groups[1].Value;
						return RenderTagGroup(tagGroupCommonName, lekmerCartItem);
					},
				VariableEncoding.None);

			return fragment.Render();
		}

		private string RenderQuantityOptions(int quantity, int quantitiesToDisplay)
		{
			var optionBuilder = new StringBuilder();
			for (int i = 0; i <= Math.Max(quantitiesToDisplay, quantity); i++)
			{
				Fragment fragmentOption = Template.GetFragment("QuantityOption");
				fragmentOption.AddVariable("Name", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddVariable("Value", i.ToString(CultureInfo.InvariantCulture));
				fragmentOption.AddCondition("IsSelected", i == quantity);
				optionBuilder.AppendLine(fragmentOption.Render());
			}
			return optionBuilder.ToString();
		}

		private int ObtainQuantityLimit(ILekmerCartItem lekmerCartItem)
		{
			var itemWithoutSizes = new Collection<int>();
			var itemWithSizes = new Collection<IPackageElement>();
			lekmerCartItem.SplitPackageElements(ref itemWithoutSizes, ref itemWithSizes);
			int numberInStock = ((ILekmerOrderService)_orderService).GetAvaliableNumberInStock(UserContext.Current, lekmerCartItem.Product.Id, lekmerCartItem.SizeId, itemWithoutSizes, itemWithSizes);

			int quantityLimit = 0;
			if (numberInStock > 0) //buy
			{
				quantityLimit = Math.Min(_maxQuantity, numberInStock);
			}
			else if (numberInStock == 0 && ((ILekmerProduct) lekmerCartItem.Product).IsBookable) //book
			{
				quantityLimit = _maxQuantity;
			}
			return quantityLimit;
		}

		private static IProductSize GetSize(ILekmerCartItem item)
		{
			return !item.SizeId.HasValue
					   ? IoC.Resolve<IProductSize>()
					   : IoC.Resolve<IProductSizeService>().GetById(item.Product.Id, item.SizeId.Value);
		}

		private string RenderTagGroup(string commonName, ICartItem cartItem)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			Collection<ITagGroup> tagGroups = IoC.Resolve<ITagGroupService>().GetAllByProduct(UserContext.Current,
																							  cartItem.Product.Id);
			ITagGroup tagGroup =
				tagGroups.FirstOrDefault(
					group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}
			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}
			Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("TagGroup.Title",
										 AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName),
										 VariableEncoding.None);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);
			return fragmentTagGroup.Render();
		}

		private string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();
			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				Fragment fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}
			return tagBuilder.ToString();
		}

		private static void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}

		private static string GetQuantityFormKey(ILekmerCartItem cartItem)
		{
			if (cartItem.SizeId.HasValue)
			{
				return "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat + "-" + cartItem.SizeId;
			}

			if (cartItem.PackageElements != null && cartItem.PackageElements.Count > 0)
			{
				return "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat + "-" + cartItem.PackageElements.GetPackageElementsHashCode();
			}

			return "quantity-" + cartItem.Product.Id + "-" + cartItem.Product.CampaignInfo.Price.IncludingVat;
		}
	}
}