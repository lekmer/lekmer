﻿using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class SaveCartForm : ControlBase
	{
		private readonly string _postUrl;
		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "savecart"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string EmailFormName
		{
			get { return "savecart-email"; }
		}
		public string EmailFormValue { get; set; }


		public SaveCartForm(string postUrl)
		{
			_postUrl = postUrl;
		}


		public void MapFromRequest()
		{
			EmailFormValue = Request.Form[EmailFormName];
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Email.Name", EmailFormName);

			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Email.Value", EmailFormValue);
			fragment.AddCondition("Form.IsPostBack", IsFormPostBack);
		}

		public void ClearFrom()
		{
			EmailFormValue = string.Empty;
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();

			if (EmailFormValue.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Product.SaveCart.Validation.EmailEmpty"));
			}
			else if (!ValidationUtil.IsValidEmail(EmailFormValue))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Product.SaveCart.Validation.EmailIncorrect"));
			}

			return validationResult;
		}
	}
}