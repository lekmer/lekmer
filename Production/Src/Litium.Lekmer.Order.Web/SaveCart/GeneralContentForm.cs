using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class GeneralContentForm : ControlBase
	{
		private readonly string _postUrl;
		protected virtual string PostUrl
		{
			get { return _postUrl; }
		}

		protected virtual string PostModeValue
		{
			get { return "go-to-checkout"; }
		}

		public GeneralContentForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public bool IsFormPost()
		{
			return PostMode.Equals(PostModeValue);
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
		}
	}
}