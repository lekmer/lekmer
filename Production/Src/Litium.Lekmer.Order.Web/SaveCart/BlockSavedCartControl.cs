using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.Order.Web
{
	public class BlockSavedCartControl : BlockControlBase
	{
		private ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private CartHelper _cartHelper;

		protected ILekmerCartSession CartSession { get; private set; }
		protected ILekmerFormatter Formatter { get; private set; }
		protected ILekmerCartService CartService { get; private set; }
		protected IProductSizeService ProductSizeService { get; private set; }
		protected ITagGroupService TagGroupService { get; private set; }
		protected IContentNodeService ContentNodeService { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }

		private CurrentCartForm _currentCartForm;
		public CurrentCartForm CurrentCartForm
		{
			get { return _currentCartForm ?? (_currentCartForm = new CurrentCartForm(ResolveUrl("~" + Request.RelativeUrl()))); }
		}

		private SavedCartForm _savedCartForm;
		public SavedCartForm SavedCartForm
		{
			get { return _savedCartForm ?? (_savedCartForm = new SavedCartForm(ResolveUrl("~" + Request.RelativeUrl()))); }
		}

		private GeneralContentForm _generalContentForm;
		public GeneralContentForm GeneralContentForm
		{
			get { return _generalContentForm ?? (_generalContentForm = new GeneralContentForm(ResolveUrl("~" + Request.RelativeUrl()))); }
		}

		protected int Id
		{
			get
			{
				int id;
				if (int.TryParse(Request.QueryString["id"], out id))
				{
					return id;
				}
				return -1;
			}
		}

		protected Guid CartGuid
		{
			get
			{
				try
				{
					var cartGuid = Request.QueryString["guid"];
					return cartGuid.IsNullOrTrimmedEmpty() ? Guid.Empty : new Guid(cartGuid);
				}
				catch (Exception ex)
				{
					_log.ErrorFormat("{0}\r\nUrl: {1}", ex.Message, Request.Url.AbsoluteUri);
					return Guid.Empty;
				}
			}
		}

		public BlockSavedCartControl(
			ITemplateFactory templateFactory,
			IBlockService blockService,
			ILekmerCartSession cartSession,
			IFormatter formatter,
			ICartService cartService,
			IProductSizeService productSizeService,
			ITagGroupService tagGroupService,
			IContentNodeService contentNodeService,
			IProductService productService)
			: base(templateFactory, blockService)
		{
			CartSession = cartSession;
			Formatter = (ILekmerFormatter) formatter;
			CartService = (ILekmerCartService) cartService;
			ProductSizeService = productSizeService;
			TagGroupService = tagGroupService;
			ContentNodeService = contentNodeService;
			ProductService = (ILekmerProductService) productService;
		}

		protected override BlockContent RenderCore()
		{
			Initialize();

			ILekmerCartFull currentCart, savedCart;
			Collection<ILekmerCartItem> currentCartItems, savedCartItems;
			bool isCurrentCartEmpty = IsCurrentCartNullOrEmpty(out currentCart, out currentCartItems);
			bool isSavedCartEmpty = IsSavedCartNullOrEmpty(out savedCart, out savedCartItems);

			if (IsPostBack)
			{
				if (CurrentCartForm.IsCurrentCartPost() && CurrentCartForm.IsDeleteAllPost() && !isCurrentCartEmpty)
				{
					DeleteAllCurrentCart(currentCart);
					isCurrentCartEmpty = IsCurrentCartNullOrEmpty(out currentCart, out currentCartItems);
				}

				if (SavedCartForm.IsSavedCartPost() && !isSavedCartEmpty)
				{
					if (SavedCartForm.IsDeleteAllPost())
					{
						DeleteAllSavedCart(savedCart);
						isSavedCartEmpty = IsSavedCartNullOrEmpty(out savedCart, out savedCartItems);
					}
					else
					{
						DeleteItemSavedCart(savedCart);
						isSavedCartEmpty = IsSavedCartNullOrEmpty(out savedCart, out savedCartItems);
					}
				}

				if (GeneralContentForm.IsFormPost())
				{
					RedirectToCheckout(savedCartItems);
				}
			}

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			string body = RenderGeneralContent(isCurrentCartEmpty, currentCart, currentCartItems, isSavedCartEmpty, savedCart, savedCartItems);

			return new BlockContent(head, body, footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}
		private string RenderGeneralContent(
			bool isCurrentCartEmpty, ILekmerCartFull currentCart, Collection<ILekmerCartItem> currentItems,
			bool isSavedCartEmpty, ILekmerCartFull savedCart, Collection<ILekmerCartItem> savedItems)
		{
			Fragment fragment = Template.GetFragment("GeneralContent");

			fragment.AddCondition("CurrentCart.IsEmpty", isCurrentCartEmpty);
			fragment.AddCondition("SavedCart.IsEmpty", isSavedCartEmpty);

			if (isCurrentCartEmpty)
			{
				fragment.AddVariable("CurrentCart.Empty", RenderFragment("EmptyCurrentCart"), VariableEncoding.None);
			}
			else
			{
				fragment.AddVariable("CurrentCart.Content", RenderCurrentCart(currentCart, currentItems), VariableEncoding.None);
			}

			if (isSavedCartEmpty)
			{
				fragment.AddVariable("SavedCart.Empty", RenderFragment("EmptySavedCart"), VariableEncoding.None);
			}
			else
			{
				fragment.AddVariable("SavedCart.Content", RenderSavedCart(savedCart, savedItems), VariableEncoding.None);
			}

			GeneralContentForm.MapFieldsToFragment(fragment);

			return fragment.Render();
		}

		protected virtual string RenderCurrentCart(ILekmerCartFull cart, Collection<ILekmerCartItem> cartItems)
		{
			Fragment fragment = Template.GetFragment("ContentCurrentCart");

			fragment.AddVariable("Iterate:CurrentCartItem", RenderCartItems(cartItems, false), VariableEncoding.None);

			// Cart info
			_cartHelper.RenderCartInfo(fragment, cart, Formatter);

			// Cart price
			_cartHelper.RenderCartPrices(fragment, cart, Formatter);

			CurrentCartForm.MapFieldsToFragment(fragment);

			return fragment.Render();
		}
		protected virtual string RenderSavedCart(ILekmerCartFull cart, Collection<ILekmerCartItem> cartItems)
		{
			Fragment fragment = Template.GetFragment("ContentSavedCart");

			fragment.AddVariable("Iterate:SavedCartItem", RenderCartItems(cartItems, true), VariableEncoding.None);

			// Cart info
			_cartHelper.RenderCartInfo(fragment, cart, Formatter);

			// Cart price
			_cartHelper.RenderCartPrices(fragment, cart, Formatter);

			SavedCartForm.MapFieldsToFragment(fragment);

			return fragment.Render();
		}
		protected virtual string RenderCartItems(Collection<ILekmerCartItem> lekmerCartItems, bool isSavedCart)
		{
			var itemBuilder = new StringBuilder();
			foreach (ILekmerCartItem cartItem in lekmerCartItems)
			{
				itemBuilder.Append(RenderCartItem(cartItem, GroupByProductAndSizeAndHashCode(lekmerCartItems), isSavedCart));
			}
			return itemBuilder.ToString();
		}
		protected virtual string RenderCartItem(ILekmerCartItem cartItem, IEnumerable<IGrouping<string, ICartItem>> groupedCartItems, bool isSavedCart)
		{
			Fragment fragment = Template.GetFragment("CartItem");

			// Product.
			var product = cartItem.Product;
			fragment.AddEntity(product);
			fragment.AddCondition("CartItem.IsSavedCart", isSavedCart);

			// Size.
			fragment.AddCondition("CartItem.HasSize", cartItem.SizeId.HasValue);
			fragment.AddEntity(GetProductSize(cartItem));

			var priceHidden = product.CampaignInfo.Price.IncludingVat.ToString();
			fragment.AddVariable("Product.CampaignInfoPriceHidden", priceHidden);
			fragment.AddVariable("CartItem.Quantity", Formatter.FormatNumber(Channel.Current, cartItem.Quantity));

			// Price
			_cartHelper.RenderCartItemPrices(fragment, cartItem, Formatter);

			bool isPackageWithSizes = _cartHelper.IsPackageWithSizes(cartItem);
			fragment.AddCondition("CartItem.IsPackageWithSizes", isPackageWithSizes);
			string packageHash = cartItem.PackageElements != null && cartItem.PackageElements.Count > 0 ? cartItem.PackageElements.GetPackageElementsHashCode().ToString() : string.Empty;
			if (isPackageWithSizes)
			{
				fragment.AddVariable("CartItem.PackageElementsHashCode", packageHash);
			}

			fragment.AddRegexVariable(
				@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;
					return RenderTagGroup(tagGroupCommonName, cartItem);
				},
				VariableEncoding.None);

			if (isSavedCart)
			{
				string productId = product.Id.ToString(CultureInfo.InvariantCulture);
				string sizeId = cartItem.SizeId.HasValue ? cartItem.SizeId.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;
				string quantity = cartItem.Quantity.ToString(CultureInfo.InvariantCulture);
				fragment.AddVariable("CartItem.Delete.Form.Name", GetCartItemKey(productId, sizeId, priceHidden, quantity, packageHash), VariableEncoding.HtmlEncode);
			}

			return fragment.Render();
		}
		protected virtual string RenderTagGroup(string commonName, ILekmerCartItem cartItem)
		{
			if (commonName == null)
			{
				throw new ArgumentNullException("commonName");
			}

			Collection<ITagGroup> tagGroups = TagGroupService.GetAllByProduct(UserContext.Current, cartItem.Product.Id);
			ITagGroup tagGroup = tagGroups.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));
			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			if (tagGroup.Tags.Count == 0)
			{
				return null;
			}

			Fragment fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("TagGroup.Title", AliasHelper.GetAliasValue("Product.TagGroup." + tagGroup.CommonName), VariableEncoding.None);
			fragmentTagGroup.AddVariable("Iterate:Tag", RenderTags(tagGroup), VariableEncoding.None);

			return fragmentTagGroup.Render();
		}
		protected virtual string RenderTags(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();

			int i = 0;
			foreach (ITag tag in tagGroup.Tags)
			{
				Fragment fragmentTag = Template.GetFragment("Tag");
				fragmentTag.AddVariable("Tag.Value", tag.Value);
				AddPositionCondition(fragmentTag, i++, tagGroup.Tags.Count);
				tagBuilder.AppendLine(fragmentTag.Render());
			}

			return tagBuilder.ToString();
		}
		protected virtual void AddPositionCondition(Fragment fragment, int index, int count)
		{
			fragment.AddCondition("IsFirst", index == 0);
			fragment.AddCondition("IsLast", index == count - 1);
		}


		protected virtual void Initialize()
		{
			_cartHelper = new CartHelper();
		}

		protected virtual bool IsCurrentCartNullOrEmpty(out ILekmerCartFull cart, out Collection<ILekmerCartItem> cartItems)
		{
			cartItems = null;

			LekmerCartHelper.RefreshSessionCart();

			cart = CartSession.Cart as ILekmerCartFull;
			if (cart == null)
			{
				return true;
			}

			cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (cartItems.Count == 0)
			{
				return true;
			}

			return false;
		}
		protected virtual void DeleteAllCurrentCart(ILekmerCartFull cart)
		{
			foreach (ILekmerCartItem cartItem in cart.GetGroupedCartItemsByProductAndPriceAndSize())
			{
				cart.DeleteItem(cartItem.Product.Id, cartItem.Product.CampaignInfo.Price.IncludingVat, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
			}

			CartService.Save(UserContext.Current, cart);
			LekmerCartHelper.RefreshSessionCartHard();
		}

		protected virtual bool IsSavedCartNullOrEmpty(out ILekmerCartFull cart, out Collection<ILekmerCartItem> cartItems)
		{
			cart = null;
			cartItems = null;

			if (Id <= 0 || CartGuid == Guid.Empty)
			{
				return true;
			}

			cart = (ILekmerCartFull) CartService.GetByIdAndCartGuid(UserContext.Current, Id, CartGuid);
			if (cart == null)
			{
				return true;
			}

			cartItems = cart.GetGroupedCartItemsByProductAndPriceAndSize();
			if (cartItems.Count == 0)
			{
				return true;
			}

			return false;
		}
		protected virtual void DeleteAllSavedCart(ILekmerCartFull cart)
		{
			foreach (ILekmerCartItem cartItem in cart.GetGroupedCartItemsByProductAndPriceAndSize())
			{
				var product = cartItem.Product;
				cart.DeleteItem(product.Id, product.CampaignInfo.Price.IncludingVat, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
			}

			CartService.SaveCloneCart(UserContext.Current, cart);
		}
		protected virtual void DeleteItemSavedCart(ILekmerCartFull cart)
		{
			foreach (ILekmerCartItem cartItem in cart.GetGroupedCartItemsByProductAndPriceAndSize())
			{
				string key = GetCartItemKey(cartItem);
				if (!SavedCartForm.IsDeleteItemPost(key))
				{
					continue;
				}

				var product = cartItem.Product;
				cart.DeleteItem(product.Id, product.CampaignInfo.Price.IncludingVat, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements.GetPackageElementsHashCode());
			}

			CartService.SaveCloneCart(UserContext.Current, cart);
		}


		protected virtual void RedirectToCheckout(Collection<ILekmerCartItem> savedCartItems)
		{
			var cart = GetCart();
			if (cart == null) throw new ArgumentNullException("cart");

			foreach (var cartItem in savedCartItems)
			{
				if (!Validate(cartItem)) continue;

				if (cartItem.Product.IsPackage())
				{
					cartItem.CleanPackageElementsId();
				}
				cart.ChangeItemQuantity(UserContext.Current, cartItem.Product, cartItem.Quantity, cartItem.SizeId, cartItem.PackageElements);
				CartService.Save(UserContext.Current, cart);
				LekmerCartHelper.RefreshSessionCart();
			}

			Response.Redirect(GetRedirectUrl("Checkout"));
		}
		protected virtual ILekmerCartFull GetCart()
		{
			return (ILekmerCartFull) (CartSession.Cart ?? (CartSession.Cart = CartService.Create(UserContext.Current)));
		}
		protected virtual bool Validate(ILekmerCartItem cartItem)
		{
			if (cartItem.Product == null || cartItem.Quantity <= 0) return false;

			var lekmerProduct = (ILekmerProduct) cartItem.Product;
			int? sizeId = cartItem.SizeId;

			if (lekmerProduct.IsProduct())
			{
				if ((lekmerProduct.HasSizes && sizeId == null) || (!lekmerProduct.HasSizes && sizeId != null)) return false;
			}
			else if (cartItem.PackageElements != null)
			{
				ProductIdCollection productIds = ProductService.GetIdAllByPackageMasterProduct(UserContext.Current, lekmerProduct.Id);
				ProductCollection packageItems = ProductService.PopulateViewWithOnlyInPackageProducts(UserContext.Current, productIds);
				foreach (var packageElement in cartItem.PackageElements)
				{
					IProduct productView = packageItems.FirstOrDefault(i => i.Id == packageElement.ProductId);
					if (productView == null) return false;

					var item = (ILekmerProductView) productView;

					if (!item.HasSizes) continue;
					if (!packageElement.SizeId.HasValue) return false;
				}
			}

			return true;
		}


		protected virtual IEnumerable<IGrouping<string, ICartItem>> GroupByProductAndSizeAndHashCode(Collection<ILekmerCartItem> lekmerCartItems)
		{
			// grouping by pair: productId, sizeId, package items hash code. sizeId is nullable.
			var cartItems = new Collection<ICartItem>(lekmerCartItems.Cast<ICartItem>().ToArray());
			return cartItems.GroupBy(item => string.Format("{0}-{1}-{2}", item.Product.Id, ((ILekmerCartItem)item).SizeId, ((ILekmerCartItem)item).PackageElements.GetPackageElementsHashCode()));
		}
		protected virtual IProductSize GetProductSize(ILekmerCartItem item)
		{
			return item.SizeId.HasValue
				? ProductSizeService.GetById(item.Product.Id, item.SizeId.Value)
				: IoC.Resolve<IProductSize>();
		}
		protected virtual string GetCartItemKey(ILekmerCartItem cartItem)
		{
			var product = cartItem.Product;
			string productId = product.Id.ToString(CultureInfo.InvariantCulture);
			string sizeId = cartItem.SizeId.HasValue ? cartItem.SizeId.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;
			string quantity = cartItem.Quantity.ToString(CultureInfo.InvariantCulture);
			string priceHidden = product.CampaignInfo.Price.IncludingVat.ToString();
			string packageHash = cartItem.PackageElements != null && cartItem.PackageElements.Count > 0 ? cartItem.PackageElements.GetPackageElementsHashCode().ToString() : string.Empty;
			return GetCartItemKey(productId, sizeId, priceHidden, quantity, packageHash);
		}
		protected virtual string GetCartItemKey(string productId, string sizeId, string price, string quantity, string packageHash)
		{
			string key = SavedCartForm.DeleteCartItemFormName + "-" + productId + "-" + price + "-" + quantity;

			if (!sizeId.IsNullOrTrimmedEmpty())
			{
				key = key + "-" + sizeId;
			}

			if (!packageHash.IsNullOrTrimmedEmpty())
			{
				key = key + "-" + packageHash;
			}

			return key;
		}
		private string GetRedirectUrl(string commonName)
		{
			var contentNodeTreeItem = ContentNodeService.GetTreeItemByCommonName(UserContext.Current, commonName);
			if (contentNodeTreeItem != null && !string.IsNullOrEmpty(contentNodeTreeItem.Url))
			{
				return UrlHelper.ResolveUrlHttps(contentNodeTreeItem.Url);
			}
			return UrlHelper.BaseUrlHttp;
		}
	}
}