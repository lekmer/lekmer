﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public interface ILekmerCheckoutCompanyForm
	{
		string IsCompanyModeFormName { get; }
		bool IsCompanyMode { get; set; }
		bool RequestHasCompanyMode { get; }

		void MapFromRequestToForm();
		void MapFieldNamesToFragment(Fragment fragment);
		void MapFieldValuesToFragment(Fragment fragment);
		void MapFromFormToProfile(ICheckoutProfile checkoutProfile);
		void MapFromProfileToForm(ICheckoutProfile checkoutProfile);
	}
}