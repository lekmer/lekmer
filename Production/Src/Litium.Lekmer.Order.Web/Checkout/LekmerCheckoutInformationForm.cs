using System;
using Litium.Lekmer.Customer.Web;
using Litium.Lekmer.Order.Web.Validation;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutInformationForm : LekmerInformationForm
	{
		private bool _lockFields;

		public bool IsCivicNumberRequired { get; set; }

		public bool ManualEdit { get; set; }

		public void LockFields()
		{
			_lockFields = true;
		}


		public virtual void MapFromProfileToForm(IProfileCustomerInformation profileCustomerInformation)
		{
			if (profileCustomerInformation == null) throw new ArgumentNullException("profileCustomerInformation");
			
			FirstName = profileCustomerInformation.FirstName;
			LastName = profileCustomerInformation.LastName;

			PhoneNumber = profileCustomerInformation.PhoneNumber;
			CellPhoneNumber = profileCustomerInformation.CellPhoneNumber;

			Email = profileCustomerInformation.Email;

			GenderTypeId = profileCustomerInformation.GenderTypeId;
		}

		public override void MapFromRequestToForm()
		{
			base.MapFromRequestToForm();
			if (CivicNumber != null)
			{
				CivicNumber = CivicNumber.Trim();
			}
		}

		public override void MapFromFormToCustomerInformation(ICustomerInformation customerInformation)
		{
			if (!Validate(IsCivicNumberRequired).IsValid)
			{
				throw new ValidationException("Couldn't validate the customer information.");
			}

			base.MapFromFormToCustomerInformation(customerInformation);
		}

		public virtual void MapFromFormToProfile(IProfileCustomerInformation profileCustomerInformation)
		{
			if (profileCustomerInformation == null) throw new ArgumentNullException("profileCustomerInformation");

			profileCustomerInformation.FirstName = FirstName;
			profileCustomerInformation.LastName = LastName;

			profileCustomerInformation.PhoneNumber = PhoneNumber;
			profileCustomerInformation.CellPhoneNumber = CellPhoneNumber;

			profileCustomerInformation.Email = Email;

			profileCustomerInformation.GenderTypeId = GenderTypeId;
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			string readonlyValue = _lockFields ? "readonly" : string.Empty;

			fragment.AddVariable("Form.CustomerInformation.FirstName.Readonly", readonlyValue);
			fragment.AddVariable("Form.CustomerInformation.LastName.Readonly", readonlyValue);

			fragment.AddCondition("Form.CustomerInformation.ManualEdit", ManualEdit);
		}


		public override ValidationResult Validate()
		{
			var validationResult = base.Validate();
			ValidateCellPhone(validationResult);
			if (IsCivicNumberRequired)
			{
				ValidateCivicNumber(validationResult);
			}
			return validationResult;
		}

		public ValidationResult Validate(bool isCivicNumberRequired)
		{
			var validationResult = base.Validate();
			ValidateCellPhone(validationResult);
			if (isCivicNumberRequired)
			{
				ValidateCivicNumber(validationResult);
			}
			return validationResult;
		}

		protected virtual void ValidateCellPhone(ValidationResult validationResult)
		{
			if (CellPhoneNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CellPhoneNumberNotProvided"));
			}
		}

		protected virtual void ValidateCivicNumber(ValidationResult validationResult)
		{
			if (CivicNumber.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CivicNumberNotProvided"));
			}
			else
			{
				var iso = (ValidationUtil.CountryISO)Enum.Parse(typeof(ValidationUtil.CountryISO), UserContext.Current.Channel.Country.Iso, true);
				if (!ValidationUtil.IsValidCivicNumber(CivicNumber, iso))
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CivicNumberNotValid"));
				}
			}
		}
	}
}