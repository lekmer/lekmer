﻿using System;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Customer.Web;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Web.Checkout;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerCheckoutForm : CheckoutForm
	{
		private VoucherDiscountForm _voucherDiscountForm;
		private LekmerCheckoutCompanyInformationForm _companyInformationForm;
		private LekmerCheckoutCompanyAddressForm _companyAddressForm;
		private LekmerCheckoutAlternateAddressForm _alternateAddressForm;
		private LekmerCheckoutCompanyForm _companyForm;
		
		public LekmerCheckoutForm(string checkoutPostUrl) : base(checkoutPostUrl)
		{
		}

		/// <summary>
		/// The value of the 'use billing address as delivery address' form field.
		/// </summary>
		public virtual string UseBillingAddressAsDeliveryAddressFormValue
		{
			get { return "true"; }
		}

		/// <summary>
		/// The name of the 'use alternative delivery address' form field.
		/// </summary>
		public virtual string UseAlternativeDeliveryAddressFormName
		{
			get { return "checkout-deliveryaddress-usealternative"; }
		}

		/// <summary>
		/// The value of the 'use alternative delivery address' form field.
		/// </summary>
		public virtual string UseAlternativeDeliveryAddressFormValue
		{
			get { return "true"; }
		}

		public virtual string AcceptOrderTermsFormName
		{
			get { return "checkout-acceptorderterms"; }
		}

		public virtual string AcceptOrderTermsFormValue
		{
			get { return "agree"; }
		}

		public string IsKlarnaCheckoutModeFormName
		{
			get { return "checkout-isKlarna"; }
		}

		/// <summary>
		/// Value of the 'use alternative delivery address' form field.
		/// </summary>
		public virtual bool UseAlternativeDeliveryAddress { get; set; }

		public virtual bool AcceptOrderTerms { get; set; }


		protected override InformationForm CreateInformationForm()
		{
			return new LekmerCheckoutInformationForm();
		}

		protected override AddressForm CreateBillingAddressForm()
		{
			return new LekmerCheckoutAddressForm("BillingAddress", "billingaddress");
		}

		protected override AddressForm CreateDeliveryAddressForm()
		{
			return new LekmerAddressForm("DeliveryAddress", "deliveryaddress");
		}


		public virtual VoucherDiscountForm VoucherDiscountForm
		{
			get
			{
				return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm());
			}
		}

		public LekmerCheckoutCompanyInformationForm CompanyInformationForm
		{
			get { return _companyInformationForm ?? (_companyInformationForm = new LekmerCheckoutCompanyInformationForm()); }
		}

		public LekmerCheckoutCompanyAddressForm CompanyAddressForm
		{
			get { return _companyAddressForm ?? (_companyAddressForm = new LekmerCheckoutCompanyAddressForm("CompanyAddress", "companyaddress")); }
		}

		public LekmerCheckoutAlternateAddressForm AlternateAddressForm
		{
			get { return _alternateAddressForm ?? (_alternateAddressForm = new LekmerCheckoutAlternateAddressForm("AlternateAddress", "alternateaddress")); }
		}

		public LekmerCheckoutCompanyForm CompanyForm
		{
			get { return _companyForm ?? (_companyForm = new LekmerCheckoutCompanyForm()); }
		}

		public bool IsKlarnaCheckoutMode { get; set; }

		public bool IsDibsPayment { get; set; }

		public bool AllowAlternativeDeliveryAddress { get; set; }

		public bool IsCompanyMode
		{
			get { return CompanyForm.IsCompanyMode; }
		}

		public string UseAlternateCompanyAddressFormName
		{
			get { return "checkout-company-usealternateaddress"; }
		}

		public string UseAlternateCompanyAddressFormValue
		{
			get { return "true"; }
		}

		public bool UseAlternateCompanyAddress { get; set; }

		public string IsMultipleKlarnaAddressesFormName
		{
			get { return "checkout-company-ismultipleklarnaaddresses"; }
		}
		public bool IsMultipleKlarnaAddresses { get; set; }
		public string KlarnaAddressesFormName
		{
			get { return "klarna-addresses-form-name"; }
		}
		public string KlarnaAddressesFormValue { get; set; }
		public string KlarnaSelectedAddressFormName
		{
			get { return "klarna-selected-address-form-name"; }
		}
		public string KlarnaSelectedAddressFormValue { get; set; }

		public void MapFromRequest(bool allowAlternativeDeliveryAddress)
		{
			AllowAlternativeDeliveryAddress = allowAlternativeDeliveryAddress;
			MapFromRequest();
		}

		public override void MapFromRequest()
		{
			base.MapFromRequest();

			var useAlternative = Request.Form[UseAlternativeDeliveryAddressFormName];
			UseAlternativeDeliveryAddress =
				useAlternative.HasValue()
				&& (useAlternative.ToLower() == "on" || useAlternative.ToLower() == UseAlternativeDeliveryAddressFormValue)
				&& AllowAlternativeDeliveryAddress;
			//Note: "UseBillingAddressAsDeliveryAddress" is overwritten with "UseAlternativeDeliveryAddress" //CR
			UseBillingAddressAsDeliveryAddress = !UseAlternativeDeliveryAddress;

			var accept = Request.Form[AcceptOrderTermsFormName];
			AcceptOrderTerms = accept.HasValue() && (accept.ToLower() == "on" || accept.ToLower() == AcceptOrderTermsFormValue);

			IsKlarnaCheckoutMode = !string.IsNullOrEmpty(Request.Form[IsKlarnaCheckoutModeFormName]) && bool.Parse(Request.Form[IsKlarnaCheckoutModeFormName]);

			var useAlternateCompanyAddress = Request.Form[UseAlternateCompanyAddressFormName];
			UseAlternateCompanyAddress = useAlternateCompanyAddress.HasValue() && (useAlternateCompanyAddress.ToLower() == "on" || useAlternateCompanyAddress.ToLower() == UseAlternateCompanyAddressFormValue);
			IsMultipleKlarnaAddresses = !string.IsNullOrEmpty(Request.Form[IsMultipleKlarnaAddressesFormName]) && bool.Parse(Request.Form[IsMultipleKlarnaAddressesFormName]);
			KlarnaAddressesFormValue = Request.Form[KlarnaAddressesFormName];
			KlarnaSelectedAddressFormValue = Request.Form[KlarnaSelectedAddressFormName];

			CompanyForm.MapFromRequestToForm();
			CompanyInformationForm.MapFromRequestToForm();
			CompanyAddressForm.MapFromRequestToForm();
			AlternateAddressForm.MapFromRequestToForm();
		}

		public override void MapFieldNamesToFragment(Fragment fragment)
		{
			base.MapFieldNamesToFragment(fragment);

			VoucherDiscountForm.MapFieldNamesToFragment(fragment);

			fragment.AddVariable("Form.UseAlternativeDeliveryAddress.Name", UseAlternativeDeliveryAddressFormName);
			fragment.AddVariable("Checkout.AcceptOrderTerms.Name", AcceptOrderTermsFormName);
			fragment.AddVariable("Checkout.UseAlternateCompanyAddress.Name", UseAlternateCompanyAddressFormName);
			fragment.AddVariable("Checkout.IsKlarnaCheckoutMode.Name", IsKlarnaCheckoutModeFormName);

			fragment.AddVariable("Checkout.IsMultipleKlarnaAddresses.Name", IsMultipleKlarnaAddressesFormName);
			fragment.AddVariable("Checkout.KlarnaAddresses.Name", KlarnaAddressesFormName);
			fragment.AddVariable("Checkout.KlarnaSelectedAddress.Name", KlarnaSelectedAddressFormName);

			CompanyForm.MapFieldNamesToFragment(fragment);
			CompanyInformationForm.MapFieldNamesToFragment(fragment);
			CompanyAddressForm.MapFieldNamesToFragment(fragment);
			AlternateAddressForm.MapFieldNamesToFragment(fragment);
		}

		public override void MapFieldValuesToFragment(Fragment fragment)
		{
			base.MapFieldValuesToFragment(fragment);

			VoucherDiscountForm.MapFieldValuesToFragment(fragment);

			fragment.AddVariable("Form.UseBillingAddressAsDeliveryAddress.Value", UseBillingAddressAsDeliveryAddressFormValue);
			fragment.AddVariable("Form.UseAlternativeDeliveryAddress.Value", UseAlternativeDeliveryAddressFormValue);
			fragment.AddCondition("Form.UseAlternativeDeliveryAddress.Checked", UseAlternativeDeliveryAddress);
			fragment.AddVariable("Checkout.AcceptOrderTerms.Value", AcceptOrderTermsFormValue);
			fragment.AddCondition("Checkout.AcceptOrderTerms.Checked", AcceptOrderTerms);
			fragment.AddVariable("Checkout.UseAlternateCompanyAddress.Value", UseAlternateCompanyAddressFormValue);
			fragment.AddCondition("Checkout.UseAlternateCompanyAddress.Checked", UseAlternateCompanyAddress);
			fragment.AddVariable("Checkout.IsKlarnaCheckoutMode.Value", IsKlarnaCheckoutMode.ToString());

			var isMultipleKlarnaAddresses = CompanyForm.IsCompanyMode
				&& !IsDibsPayment
				&& IsMultipleKlarnaAddresses
				&& !CompanyInformationForm.OrganizationNumber.IsNullOrEmpty();
			fragment.AddCondition("Checkout.IsMultipleKlarnaAddresses", isMultipleKlarnaAddresses);
			fragment.AddVariable("Checkout.IsMultipleKlarnaAddresses.Value", isMultipleKlarnaAddresses.ToString());
			fragment.AddVariable("Checkout.KlarnaAddresses.Value", KlarnaAddressesFormValue ?? string.Empty);
			fragment.AddVariable("Checkout.KlarnaSelectedAddress.Value", KlarnaSelectedAddressFormValue ?? string.Empty);

			CompanyForm.MapFieldValuesToFragment(fragment);
			CompanyInformationForm.MapFieldValuesToFragment(fragment);
			CompanyAddressForm.MapFieldValuesToFragment(fragment);
			AlternateAddressForm.MapFieldValuesToFragment(fragment);
		}

		/// <summary>
		/// Maps the form values to a customer.
		/// </summary>
		/// <param name="customer">The customer to map to.</param>
		public override void MapToCustomer(ICustomer customer)
		{
			var information = (ILekmerCustomerInformation)customer.CustomerInformation;
			bool isCompanyMode = CompanyForm.IsCompanyMode;
			information.IsCompany = isCompanyMode;
			if (isCompanyMode)
			{
				CompanyInformationForm.MapFromFormToCustomerInformation(information);
				CompanyAddressForm.MapFromFormToAddress(information.DefaultBillingAddress);
				//Since there is no Addressee field in company checkout form set the value from company full name (last name)
				information.DefaultBillingAddress.Addressee = information.LastName;
				if (UseAlternateCompanyAddress || (!IsDibsPayment && !KlarnaSelectedAddressFormValue.IsNullOrEmpty()))
				{
					AlternateAddressForm.MapFromFormToAddress(information.AlternateAddress);
				}
				else
				{
					information.AlternateAddress = null;
				}
			}
			else
			{
				InformationForm.MapFromFormToCustomerInformation(information);
				BillingAddressForm.MapFromFormToAddress(information.DefaultBillingAddress);

				if (!UseBillingAddressAsDeliveryAddress)
				{
					if (information.DefaultDeliveryAddress == null ||
						information.DefaultDeliveryAddress.Id == information.DefaultBillingAddress.Id)
					{
						information.DefaultDeliveryAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
						information.DefaultDeliveryAddress.CountryId = Channel.Current.Country.Id;
					}
					DeliveryAddressForm.MapFromFormToAddress(information.DefaultDeliveryAddress);
				}
				else
				{
					information.DefaultDeliveryAddress = information.DefaultBillingAddress;
					information.DefaultDeliveryAddressId = information.DefaultBillingAddressId;
				}

				information.AlternateAddress = null;
			}
		}

		/// <summary>
		/// Sets default values of form.
		/// </summary>
		/// <param name="customer">Customer with information for setting form values.</param>
		public override void SetDefaultValues(ICustomer customer)
		{
			if (customer == null) throw new ArgumentNullException("customer");

			InformationForm.MapFromCustomerInformationToForm(customer.CustomerInformation);
			BillingAddressForm.MapFromAddressToForm(customer.CustomerInformation.DefaultBillingAddress);

			UseBillingAddressAsDeliveryAddress =
				customer.IsNew || 
				customer.CustomerInformation.DefaultDeliveryAddress == null || 
				customer.CustomerInformation.DefaultDeliveryAddress.IsNew ||
				customer.CustomerInformation.DefaultBillingAddressId == customer.CustomerInformation.DefaultDeliveryAddressId;

			UseAlternativeDeliveryAddress = !UseBillingAddressAsDeliveryAddress;

			if (!UseBillingAddressAsDeliveryAddress && customer.CustomerInformation.DefaultDeliveryAddress != null)
			{
				DeliveryAddressForm.MapFromAddressToForm(customer.CustomerInformation.DefaultDeliveryAddress);
			}
		}

		public virtual void MapToProfile()
		{
			var checkoutSession = IoC.Resolve<ICheckoutSession>();
			ICheckoutProfile checkoutProfile = checkoutSession.Profile;
			if (checkoutProfile == null)
			{
				checkoutSession.InitializeProfile();
				checkoutProfile = checkoutSession.Profile;
			}
			if (checkoutProfile == null)
			{
				return;
			}

			CompanyForm.MapFromFormToProfile(checkoutProfile); // IsCompanyMode

			((LekmerCheckoutInformationForm)InformationForm).MapFromFormToProfile(checkoutProfile.CustomerInformation);
			((LekmerAddressForm) BillingAddressForm).MapFromFormToProfile(checkoutProfile.BillingAddress);
			((LekmerAddressForm)DeliveryAddressForm).MapFromFormToProfile(checkoutProfile.DeliveryAddress);
			checkoutProfile.UseAlternativeDeliveryAddress = UseAlternativeDeliveryAddress;

			CompanyInformationForm.MapFromFormToProfile(checkoutProfile.CompanyInformation);
			CompanyAddressForm.MapFromFormToProfile(checkoutProfile.CompanyAddress);
			AlternateAddressForm.MapFromFormToProfile(checkoutProfile.CompanyAlternateAddress);
			checkoutProfile.UseAlternateCompanyAddress = UseAlternateCompanyAddress;
		}

		public virtual void MapFromProfile()
		{
			var checkoutSession = IoC.Resolve<ICheckoutSession>();
			ICheckoutProfile checkoutProfile = checkoutSession.Profile;
			if (checkoutProfile == null)
			{
				return;
			}

			CompanyForm.MapFromProfileToForm(checkoutProfile); // IsCompanyMode

			((LekmerCheckoutInformationForm)InformationForm).MapFromProfileToForm(checkoutProfile.CustomerInformation);
			((LekmerAddressForm)BillingAddressForm).MapFromProfileToForm(checkoutProfile.BillingAddress);
			((LekmerAddressForm)DeliveryAddressForm).MapFromProfileToForm(checkoutProfile.DeliveryAddress);
			UseAlternativeDeliveryAddress = checkoutProfile.UseAlternativeDeliveryAddress;
			UseBillingAddressAsDeliveryAddress = !UseAlternativeDeliveryAddress;

			CompanyInformationForm.MapFromProfileToForm(checkoutProfile.CompanyInformation);
			CompanyAddressForm.MapFromProfileToForm(checkoutProfile.CompanyAddress);
			AlternateAddressForm.MapFromProfileToForm(checkoutProfile.CompanyAlternateAddress);
			UseAlternateCompanyAddress = checkoutProfile.UseAlternateCompanyAddress;
		}

		public virtual ValidationResult Validate(bool isCivicNumberRequired)
		{
			var validationResult = new ValidationResult();
			if (IsCompanyMode)
			{
				CompanyInformationForm.IsCivicNumberRequired = isCivicNumberRequired;
				validationResult.Errors.Append(CompanyInformationForm.Validate().Errors);
				validationResult.Errors.Append(CompanyAddressForm.Validate().Errors);
				if (UseAlternateCompanyAddress || (!IsDibsPayment && !KlarnaSelectedAddressFormValue.IsNullOrEmpty()))
				{
					validationResult.Errors.Append(AlternateAddressForm.Validate().Errors);
				}
			}
			else
			{
				var lekmerCheckoutInformationForm = (LekmerCheckoutInformationForm)InformationForm;
				lekmerCheckoutInformationForm.IsCivicNumberRequired = isCivicNumberRequired;
				validationResult.Errors.Append(lekmerCheckoutInformationForm.Validate(isCivicNumberRequired).Errors);
				validationResult.Errors.Append(BillingAddressForm.Validate().Errors);
				if (!UseBillingAddressAsDeliveryAddress)
				{
					if (AllowAlternativeDeliveryAddress)
					{
						validationResult.Errors.Append(DeliveryAddressForm.Validate().Errors);
					}
					else
					{
						validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.UseAlternativeDeliveryAddressIncorrect"));
					}
				}
			}
			ValidateAcceptOrderTerms(validationResult);

			return validationResult;
		}

		protected virtual void ValidateAcceptOrderTerms(ValidationResult validationResult)
		{
			if (!AcceptOrderTerms)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.OrderTermsNotAccepted"));
			}
		}
	}
}