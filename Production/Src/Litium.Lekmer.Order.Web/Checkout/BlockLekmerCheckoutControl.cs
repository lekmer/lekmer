﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using Litium.Lekmer.Avail.Web;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Common.Web;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Customer.Web;
using Litium.Lekmer.Order.Cookie;
using Litium.Lekmer.Payment;
using Litium.Lekmer.Payment.Collector;
using Litium.Lekmer.Payment.Klarna;
using Litium.Lekmer.Payment.Maksuturva;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using Litium.Lekmer.Payment.Qliro;
using Litium.Lekmer.Payment.Skrill;
using Litium.Lekmer.Product.Web.Helper;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Order.Payment;
using Litium.Scensum.Order.Web.Checkout;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.Order.Web
{
	internal class BlockLekmerCheckoutControl : BlockCheckoutControl
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private readonly string _klarnaAddressTemplate = "{0}|{1}|{2}|{3}";
		private readonly char _klarnaAddressSeperator = '$';
		private readonly char _klarnaPartAddressSeperator = '|';

		private Payment _payment;
		private Delivery _delivery;
		private Collection<IPaymentType> _paymentTypes; 

		private LekmerCheckoutForm _lekmerCheckoutForm;
		private VoucherDiscountForm _voucherDiscountForm;

		private readonly ILekmerFormatter _formatter;
		private ILekmerDibsPaymentProvider _dibsPaymentProvider;
		private ISkrillPaymentProvider _skrillPaymentProvider;
		private IMaksuturvaPaymentProvider _maksuturvaPaymentProvider;
		private readonly ICheckoutSession _checkoutSession;
		private readonly IOrderSession _orderSession;
		private readonly IVoucherSession _voucherSession;
		private CartHelper _cartHelper;

		private ILekmerOrderService _orderService;
		private IPaymentTypeService _paymentTypeService;
		private readonly IOrderStatusService _orderStatusService;
		private readonly IOrderItemStatusService _orderItemStatusService;
		private readonly IGiftCardViaMailInfoService _giftCardViaMailInfoService;
		private readonly ICartCookieService _cartCookieService;
		private IKlarnaPendingOrderService _klarnaPendingOrderService;
		private IQliroPendingOrderService _qliroPendingOrderService;
		private IMaksuturvaPendingOrderService _maksuturvaPendingOrderService;
		private IMaksuturvaSetting _maksuturvaSetting;

		protected IAliasSharedService AliasSharedService { get; private set; }

		private bool _klarnaGetAddressTimeoutResponse;

		public ILekmerOrderService OrderService
		{
			get { return _orderService ?? (_orderService = (ILekmerOrderService)IoC.Resolve<IOrderService>()); }
		}

		public ILekmerDibsPaymentProvider DibsPaymentProvider
		{
			get { return _dibsPaymentProvider ?? (_dibsPaymentProvider = (ILekmerDibsPaymentProvider)IoC.Resolve<IPaymentProvider>()); }
		}

		public ISkrillPaymentProvider SkrillPaymentProvider
		{
			get { return _skrillPaymentProvider ?? (_skrillPaymentProvider = IoC.Resolve<ISkrillPaymentProvider>()); }
		}

		public IMaksuturvaPaymentProvider MaksuturvaPaymentProvider
		{
			get { return _maksuturvaPaymentProvider ?? (_maksuturvaPaymentProvider = IoC.Resolve<IMaksuturvaPaymentProvider>()); }
		}

		public IMaksuturvaSetting MaksuturvaSetting
		{
			get { return _maksuturvaSetting ?? (_maksuturvaSetting = IoC.Resolve<IMaksuturvaSetting>()); }
		}

		public IPaymentTypeService PaymentTypeService
		{
			get { return _paymentTypeService ?? (_paymentTypeService = IoC.Resolve<IPaymentTypeService>()); }
		}

		public IKlarnaPendingOrderService KlarnaPendingOrderService
		{
			get { return _klarnaPendingOrderService ?? (_klarnaPendingOrderService = IoC.Resolve<IKlarnaPendingOrderService>()); }
		}

		public IQliroPendingOrderService QliroPendingOrderService
		{
			get { return _qliroPendingOrderService ?? (_qliroPendingOrderService = IoC.Resolve<IQliroPendingOrderService>()); }
		}

		public IMaksuturvaPendingOrderService MaksuturvaPendingOrderService
		{
			get { return _maksuturvaPendingOrderService ?? (_maksuturvaPendingOrderService = IoC.Resolve<IMaksuturvaPendingOrderService>()); }
		}

		public Collection<IPaymentType> PaymentTypes
		{
			get { return _paymentTypes ?? (PaymentTypeService.GetAll(UserContext.Current)); }
		}

		public LekmerCheckoutForm LekmerCheckoutForm
		{
			get { return _lekmerCheckoutForm ?? (_lekmerCheckoutForm = new LekmerCheckoutForm(GetCheckoutPostUrl())); }
		}

		public VoucherDiscountForm VoucherDiscountForm
		{
			get { return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm()); }
		}


		public BlockLekmerCheckoutControl(
			ICustomerSession customerSession,
			ITemplateFactory templateFactory,
			IFormatter formatter,
			ICheckoutSession checkoutSession,
			IOrderSession orderSession,
			IOrderStatusService orderStatusService,
			IOrderItemStatusService orderItemStatusService,
			IGiftCardViaMailInfoService giftCardViaMailInfoService,
			ICartCookieService cartCookieService,
			IAliasSharedService aliasSharedService
		)
			: base(customerSession, templateFactory)
		{
			_formatter = (ILekmerFormatter)formatter;
			_checkoutSession = checkoutSession;
			_orderSession = orderSession;
			_orderStatusService = orderStatusService;
			_orderItemStatusService = orderItemStatusService;
			_voucherSession = IoC.Resolve<IVoucherSession>();
			_giftCardViaMailInfoService = giftCardViaMailInfoService;
			_cartCookieService = cartCookieService;
			AliasSharedService = aliasSharedService;
		}

		private void Initialize()
		{
			_payment = new Payment();
			_delivery = new Delivery();

			_payment.PaymentTypeSelected += OnPaymentTypeSelected;
			_payment.Initialize();
			_delivery.Initialize();

			_cartHelper = new CartHelper();
		}

		private void OnPaymentTypeSelected(object sender, Payment.PaymentTypeSelectedEventArgs e)
		{
			_delivery.PaymentTypeId = e.PaymentTypeId;
			_delivery.Render();

			SetPaymentOrderTotalSum();
		}

		private void SetPaymentOrderTotalSum()
		{
			ICartFull cart = GetCart();

			if (cart != null)
			{
				decimal paymentCost = _payment.GetPaymentCost();
				decimal deliveryPrice = _delivery.GetActualDeliveryPrice(cart);
				decimal optionalDeliveryPrice = _delivery.GetActualOptionalDeliveryPrice(cart);
				decimal diapersDeliveryPrice = _delivery.GetActualDiapersDeliveryPrice(cart);

				Price actualPriceSummary = cart.GetActualPriceSummary();
				decimal totalFreight = deliveryPrice + optionalDeliveryPrice + diapersDeliveryPrice;
				decimal totalPrice = actualPriceSummary.IncludingVat + totalFreight + paymentCost;

				_payment.OrderTotalSum = (double) totalPrice;
			}
		}


		protected override BlockContent RenderCore()
		{
			Initialize();

			UpdateOrderKcoStatus();

			ICartFull cart = GetCart();
			if (cart == null || cart.GetCartItems().Count == 0)
			{
				return RenderNoActiveCart();
			}
			CheckForItemsType(cart);

			ICustomer customer = GetCustomer();

			var validationResult = new ValidationResult();

			string orderStatus = OrderStatusName.PaymentPending;

			if (LekmerCheckoutForm.IsFormPostBack)
			{
				LekmerCheckoutForm.MapFromRequest(
					PaymentTypeHelper.IsDibsPayment(_payment.ActivePaymentTypeId) 
					|| PaymentTypeHelper.IsMaksuturvaPayment(_payment.ActivePaymentTypeId));

				if (!Request.Form["GetAddress"].IsEmpty())
				{
					string civicNumber = LekmerCheckoutForm.IsCompanyMode
						? LekmerCheckoutForm.CompanyInformationForm.OrganizationNumber
						: LekmerCheckoutForm.InformationForm.CivicNumber;

					if (civicNumber.IsEmpty())
					{
						validationResult.Errors.Add(AliasHelper.GetAliasValue(LekmerCheckoutForm.IsCompanyMode
							? "Order.Checkout.Validation.Company.EnterOrganizationNumber"
							: "Order.Checkout.Validation.EnterCivicNumber"));
					}
					else
					{
						ProcessGetAddressPost(civicNumber, validationResult);
					}
				}
				else if (!IsVoucherPost())
				{
					LekmerCheckoutForm.MapToProfile();

					if (_voucherSession.WasUsed)
					{
						_voucherSession.VoucherCode = null;
						_voucherSession.WasUsed = false;
						return RenderCheckoutForm(cart, validationResult);
					}

					validationResult.Errors.Append(ValidateItemsType(cart).Errors);

					LekmerCheckoutForm.IsDibsPayment = _payment.IsCreditCardPaymentActive || _payment.IsiDealPaymentActive;

					bool isCivicNumberRequired = _payment.IsKlarnaPaymentActive || _payment.IsQliroPaymentActive || _payment.IsCollectorPaymentActive;

					validationResult.Errors.Append(LekmerCheckoutForm.Validate(isCivicNumberRequired).Errors);

					validationResult.Errors.Append(_delivery.Validate(LekmerCheckoutForm.IsCompanyMode, cart).Errors);

					validationResult.Errors.Append(_payment.Validate().Errors);

					if (validationResult.IsValid)
					{
						LekmerCheckoutForm.MapToCustomer(customer);

						// Check for bad addresses, temporary code
						CheckForBadAddresses(customer);

						ILekmerOrderFull order = CreateOrder(cart, customer, orderStatus);

						if (!OrderService.CheckNumberInStock(UserContext.Current, order))
						{
							validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.OrderItemQuantityTooBig"));
						}

						if (validationResult.IsValid)
						{
							if (PaymentTypeHelper.IsRedirectionPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = OrderStatusName.PaymentPending;
							}
							else if (PaymentTypeHelper.IsKlarnaInvoicePayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessKlarnaInvoicePayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsKlarnaPartPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessKlarnaPartPayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsKlarnaSpecialPartPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessKlarnaSpecialPartPayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsQliroPersonInvoicePayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessQliroPersonInvoicePayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsQliroCompanyInvoicePayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessQliroCompanyInvoicePayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsQliroPartPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessQliroPartPayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsQliroSpecialPartPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessQliroSpecialPartPayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsCollectorPersonInvoicePayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessCollectorPersonInvoicePayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsCollectorPartPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessCollectorPartPayment(validationResult, customer, order);
							}
							else if (PaymentTypeHelper.IsCollectorSpecialPartPayment(_payment.ActivePaymentTypeId))
							{
								orderStatus = ProcessCollectorSpecialPartPayment(validationResult, customer, order);
							}
							else
							{
								validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentIncorrect"));
							}

							if (validationResult.IsValid)
							{
								SaveOrder(order, orderStatus);
								_orderSession.OrderId = order.Id;
								RefreshCustomer();

								if (PaymentTypeHelper.IsDibsPayment(_payment.ActivePaymentTypeId))
								{
									RedirectToCreditCard(order);
									return new BlockContent();
								}

								if (PaymentTypeHelper.IsMoneybookersPayment(_payment.ActivePaymentTypeId))
								{
									RedirectToMoneybookers(order);
									return new BlockContent();
								}

								if (PaymentTypeHelper.IsMaksuturvaPayment(_payment.ActivePaymentTypeId))
								{
									RedirectToMaksuturva(order);
									return new BlockContent();
								}

								//Flush cache for all payment types if not redirected to creditcardpayment.
								_voucherSession.Voucher = null;
								IoC.Resolve<IUserContextFactory>().FlushCache();

								if (orderStatus != OrderStatusName.RejectedByPaymentProvider)
								{
									order = (ILekmerOrderFull)OrderService.GetFullById(UserContext.Current, order.Id);
									OrderService.SendConfirm(UserContext.Current, order);

									ResetCart();
									_cartCookieService.DeleteShoppingCardGuid();
									_checkoutSession.CleanProfile();

									return RenderConfirmResponse(order);
								}

								return new BlockContent();
							}
						}
					}
				}
			}
			else if (PostMode.Equals("getDeliveryMethods")
				|| !Request.Form["changePaymentType"].IsEmpty()
				|| !Request.Form[LekmerCheckoutForm.IsKlarnaCheckoutModeFormName].IsEmpty()
				|| PostMode.Equals("changeDeliveryMethod"))
			{
				LekmerCheckoutForm.MapFromRequest(
					PaymentTypeHelper.IsDibsPayment(_payment.ActivePaymentTypeId)
					|| PaymentTypeHelper.IsMaksuturvaPayment(_payment.ActivePaymentTypeId));
				LekmerCheckoutForm.MapToProfile();
				return RenderCheckoutForm(cart, string.Empty);
			}
			else
			{
				//if (!((ILekmerOrderService)OrderService).CheckNumberInStock(UserContext.Current, cart))
				//{
				//    validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.OrderItemQuantityTooBig"));
				//}

				LekmerCheckoutForm.SetDefaultValues(customer);
				LekmerCheckoutForm.MapFromProfile();
			}

			return RenderCheckoutForm(cart, validationResult);
		}

		private ILekmerOrderFull CreateOrder(ICartFull cart, ICustomer customer, string orderStatusCommonName)
		{
			var orderStatus = _orderStatusService.GetByCommonName(UserContext.Current, orderStatusCommonName);
			var orderItemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, orderStatusCommonName);
			return CreateOrder(cart, customer, orderStatus, orderItemStatus);
		}
		private ILekmerOrderFull CreateOrder(ICartFull cart, ICustomer customer, IOrderStatus orderStatus, IOrderItemStatus orderItemStatus)
		{
			var order = (ILekmerOrderFull)OrderService.CreateFull(UserContext.Current);

			order.MergeWith(_delivery.GetOrderDelivery(cart), _delivery.GetOrderOptionalDelivery(cart), _delivery.GetOrderDiapersDelivery(cart));

			order.MergeWith(UserContext.Current, cart);
			order.MergeWith(UserContext.Current, _payment.GetOrderPayments(order));

			FinishOrderPayments(cart, order);

			order.MergeWith(customer);
			order.MergeWith(AlternateVersion.UseAlternate);

			order.OrderStatus = orderStatus;
			order.SetAllOrderItemsStatus(orderItemStatus);

			order.IP = Request.UserHostAddress;
			order.UserAgent = Request.UserAgent;

			return order;
		}
		private void FinishOrderPayments(ICartFull cart, IOrderFull orderFull)
		{
			var orderPayment = (ILekmerOrderPayment)orderFull.Payments.FirstOrDefault(p => p.PaymentTypeId == (int)PaymentMethodType.DibsCreditCard);
			if (orderPayment == null) return;

			IPaymentType paymentType = PaymentTypes.SingleOrDefault(type => type.Id == orderPayment.PaymentTypeId);
			if (paymentType == null) return;

			bool orderContainsItemsFromMixedWarehouses = _cartHelper.CheckForMixedWarehouses(cart);
			if (orderContainsItemsFromMixedWarehouses)
			{
				if (DibsPaymentProvider.CaptureIsAllowed(UserContext.Current.Channel.CommonName, paymentType.CommonName))
				{
					orderPayment.Captured = true;
				}
			}
		}

		// Klarna payments processing

		private string ProcessKlarnaInvoicePayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsKlarnaInvoicePayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					throw new KlarnaSpecifiedException(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IKlarnaClient klarnaClient = GetKlarnaClient(orderPayment.PaymentTypeId);

				IKlarnaReservationResult reservationResult = klarnaClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					-1,
					order.GetActualPriceTotal().IncludingVat,
					null);

				orderStatus = ProcessKlarnaResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing klarna invoice payment.", ex);
				KlarnaRejectOrderMessage(validationResult, ex);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessKlarnaPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsKlarnaPartPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					throw new KlarnaSpecifiedException(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
				}
				if (orderPayment.KlarnaPClass.HasValue == false)
				{
					throw new KlarnaSpecifiedException(AliasHelper.GetAliasValue("Order.Checkout.Validation.PartPaymentNotProvided"));
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IKlarnaClient klarnaClient = GetKlarnaClient(orderPayment.PaymentTypeId);

				IKlarnaReservationResult reservationResult = klarnaClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					orderPayment.KlarnaPClass.Value,
					order.GetActualPriceTotal().IncludingVat,
					null);

				orderStatus = ProcessKlarnaResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing klarna part payment.", ex);
				KlarnaRejectOrderMessage(validationResult, ex);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessKlarnaSpecialPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsKlarnaSpecialPartPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					throw new KlarnaSpecifiedException(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
				}
				if (orderPayment.KlarnaPClass.HasValue == false)
				{
					throw new KlarnaSpecifiedException(AliasHelper.GetAliasValue("Order.Checkout.Validation.PartPaymentNotProvided"));
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IKlarnaClient klarnaClient = GetKlarnaClient(orderPayment.PaymentTypeId);

				IKlarnaReservationResult reservationResult = klarnaClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					orderPayment.KlarnaPClass.Value,
					order.GetActualPriceTotal().IncludingVat,
					null);

				orderStatus = ProcessKlarnaResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing special klarna part payment.", ex);
				KlarnaRejectOrderMessage(validationResult, ex);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}
		
		// Qliro payments processing

		private string ProcessQliroPersonInvoicePayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsQliroPersonInvoicePayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IQliroClient qliroClient = QliroFactory.CreateQliroClient(UserContext.Current.Channel.CommonName);

				IQliroReservationResult reservationResult = qliroClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					null
				);

				orderStatus = ProcessQliroResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Qliro person invoice payment.", ex);
				QliroRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessQliroCompanyInvoicePayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsQliroCompanyInvoicePayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IQliroClient qliroClient = QliroFactory.CreateQliroClient(UserContext.Current.Channel.CommonName);

				IQliroReservationResult reservationResult = qliroClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					null
				);

				orderStatus = ProcessQliroResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Qliro company invoice payment.", ex);
				QliroRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessQliroPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsQliroPartPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}
				if (orderPayment.QliroPaymentCode.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.QliroPartPaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IQliroClient qliroClient = QliroFactory.CreateQliroClient(UserContext.Current.Channel.CommonName);

				IQliroReservationResult reservationResult = qliroClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					orderPayment.QliroPaymentCode
				);

				orderStatus = ProcessQliroResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Qliro part payment.", ex);
				QliroRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessQliroSpecialPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsQliroSpecialPartPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}
				if (orderPayment.QliroPaymentCode.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.QliroPartPaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				IQliroClient qliroClient = QliroFactory.CreateQliroClient(UserContext.Current.Channel.CommonName);

				IQliroReservationResult reservationResult = qliroClient.ReserveAmount(
					UserContext.Current.Channel,
					customer,
					order,
					orderPayment.QliroPaymentCode
				);

				orderStatus = ProcessQliroResponse(reservationResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Qliro special part payment.", ex);
				QliroRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		// Collector payments processing

		private string ProcessCollectorPersonInvoicePayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsCollectorPersonInvoicePayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				ICollectorClient collectorClient = CollectorFactory.CreateCollectorClient(UserContext.Current.Channel.CommonName);

				ICollectorAddInvoiceResult addInvoiceResult = collectorClient.AddInvoice(
					UserContext.Current.Channel,
					customer,
					order,
					null,
					order.GetActualPriceTotal().IncludingVat
				);

				orderStatus = ProcessCollectorResponse(addInvoiceResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Collector person invoice payment.", ex);
				CollectorRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessCollectorPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsCollectorPartPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}
				if (orderPayment.CollectorPaymentCode.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CollectorPartPaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				ICollectorClient collectorClient = CollectorFactory.CreateCollectorClient(UserContext.Current.Channel.CommonName);

				ICollectorAddInvoiceResult addInvoiceResult = collectorClient.AddInvoice(
					UserContext.Current.Channel,
					customer,
					order,
					orderPayment.CollectorPaymentCode,
					order.GetActualPriceTotal().IncludingVat
				);

				orderStatus = ProcessCollectorResponse(addInvoiceResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Collector part payment.", ex);
				CollectorRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessCollectorSpecialPartPayment(ValidationResult validationResult, ICustomer customer, ILekmerOrderFull order)
		{
			string orderStatus;
			try
			{
				var orderPayment = (ILekmerOrderPayment)order.Payments.FirstOrDefault(p => PaymentTypeHelper.IsCollectorSpecialPartPayment(p.PaymentTypeId));
				if (orderPayment == null)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.PaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}
				if (orderPayment.CollectorPaymentCode.IsEmpty())
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CollectorPartPaymentNotProvided"));
					orderStatus = RejectOrder(order);
					return orderStatus;
				}

				OrderService.SaveFullExtended(UserContext.Current, order);

				ICollectorClient collectorClient = CollectorFactory.CreateCollectorClient(UserContext.Current.Channel.CommonName);

				ICollectorAddInvoiceResult addInvoiceResult = collectorClient.AddInvoice(
					UserContext.Current.Channel,
					customer,
					order,
					orderPayment.CollectorPaymentCode,
					order.GetActualPriceTotal().IncludingVat
				);

				orderStatus = ProcessCollectorResponse(addInvoiceResult, validationResult, order);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing Collector special part payment.", ex);
				CollectorRejectOrderMessage(validationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		// Payment type checks
		private IKlarnaClient GetKlarnaClient(int paymentTypeId)
		{
			if (PaymentTypeHelper.IsKlarnaSimplePayment(paymentTypeId))
			{
				return KlarnaFactory.CreateKlarnaClient(UserContext.Current.Channel.CommonName);
			}
			if (PaymentTypeHelper.IsKlarnaAdvancedPayment(paymentTypeId))
			{
				return KlarnaFactory.CreateKlarnaAdvancedClient(UserContext.Current.Channel.CommonName);
			}

			return KlarnaFactory.CreateKlarnaClient(UserContext.Current.Channel.CommonName);
		}

		private string ProcessKlarnaResponse(IKlarnaReservationResult reservationResult, ValidationResult validationResult, ILekmerOrderFull order)
		{
			string orderStatus;

			if (reservationResult.ResponseCode == (int)KlarnaResponseCode.Ok)
			{
				order.Payments[0].ReferenceId = reservationResult.ReservationId;
				orderStatus = OrderStatusName.PaymentConfirmed;
			}
			else if (reservationResult.ResponseCode == (int)KlarnaResponseCode.TimeoutResponse)
			{
				orderStatus = OrderStatusName.PaymentConfirmed;
			}
			else if (reservationResult.ResponseCode == (int)KlarnaResponseCode.Pending)
			{
				order.Payments[0].ReferenceId = reservationResult.ReservationId;
				orderStatus = OrderStatusName.PaymentOnHold;

				ProcessKlarnaPendingOrder(order);
			}
			else
			{
				KlarnaRejectOrderMessage(validationResult, reservationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessQliroResponse(IQliroReservationResult reservationResult, ValidationResult validationResult, ILekmerOrderFull order)
		{
			string orderStatus;

			if (reservationResult.StatusCode == QliroTransactionStatus.TimeoutResponse)
			{
				orderStatus = OrderStatusName.PaymentTimeout;
			}
			else if (reservationResult.InvoiceStatus == QliroInvoiceStatus.Ok)
			{
				order.Payments[0].ReferenceId = reservationResult.ReservationId;
				orderStatus = OrderStatusName.PaymentConfirmed;
			}
			else if (reservationResult.InvoiceStatus == QliroInvoiceStatus.Timeout) // Response from Qliro
			{
				order.Payments[0].ReferenceId = reservationResult.ReservationId;
				orderStatus = OrderStatusName.PaymentConfirmed;
			}
			else if (reservationResult.InvoiceStatus == QliroInvoiceStatus.Holding)
			{
				order.Payments[0].ReferenceId = reservationResult.ReservationId;
				orderStatus = OrderStatusName.PaymentOnHold;

				ProcessQliroPendingOrder(order);
			}
			else
			{
				QliroRejectOrderMessage(validationResult, reservationResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private string ProcessCollectorResponse(ICollectorAddInvoiceResult addInvoiceResult, ValidationResult validationResult, ILekmerOrderFull order)
		{
			string orderStatus;

			if (addInvoiceResult.StatusCode == (int)CollectorTransactionStatus.TimeoutResponse)
			{
				orderStatus = OrderStatusName.PaymentTimeout;
			} 
			else if (addInvoiceResult.StatusCode == (int)CollectorTransactionStatus.Ok && (addInvoiceResult.InvoiceStatus == (int)CollectorInvoiceStatus.Activated || addInvoiceResult.InvoiceStatus == (int)CollectorInvoiceStatus.Pending))
			{
				order.Payments[0].ReferenceId = addInvoiceResult.InvoiceNo;
				orderStatus = OrderStatusName.PaymentConfirmed;
			}
			else if (addInvoiceResult.StatusCode == (int)CollectorTransactionStatus.Ok && addInvoiceResult.InvoiceStatus == (int)CollectorInvoiceStatus.OnHold)
			{
				order.Payments[0].ReferenceId = addInvoiceResult.InvoiceNo;
				orderStatus = OrderStatusName.PaymentOnHold;
			}
			else
			{
				CollectorRejectOrderMessage(validationResult, addInvoiceResult);
				orderStatus = RejectOrder(order);
			}

			return orderStatus;
		}

		private void ProcessKlarnaPendingOrder(ILekmerOrderFull order)
		{
			try
			{
				var klarnaPendingOrder = KlarnaPendingOrderService.Create();

				klarnaPendingOrder.ChannelId = order.ChannelId;
				klarnaPendingOrder.OrderId = order.Id;

				KlarnaPendingOrderService.Insert(klarnaPendingOrder);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing klarna pending order.", ex);
			}
		}

		private void ProcessQliroPendingOrder(ILekmerOrderFull order)
		{
			try
			{
				var qliroPendingOrder = QliroPendingOrderService.Create();

				qliroPendingOrder.ChannelId = order.ChannelId;
				qliroPendingOrder.OrderId = order.Id;

				QliroPendingOrderService.Insert(qliroPendingOrder);
			}
			catch (Exception ex)
			{
				_log.Error("Error while processing qliro pending order.", ex);
			}
		}

		private void KlarnaRejectOrderMessage(ValidationResult validationResult, IKlarnaReservationResult klarnaReservationResult)
		{
			if (klarnaReservationResult.ResponseCode == (int)KlarnaResponseCode.SpecifiedError)
			{
				validationResult.Errors.Add(klarnaReservationResult.FaultReason); //message from Klarna
			}
			else if (klarnaReservationResult.ResponseCode == (int)KlarnaResponseCode.UnspecifiedError)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.UnspecifiedException"));
			}
			else if (klarnaReservationResult.ResponseCode == (int)KlarnaResponseCode.Denied)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.DeniedResponseCode"));
			}
			else if (klarnaReservationResult.ResponseCode == (int)KlarnaResponseCode.Pending)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.PendingResponseCode"));
			}
		}

		private void KlarnaRejectOrderMessage(ValidationResult validationResult, Exception ex)
		{
			if (ex is KlarnaSpecifiedException)
			{
				validationResult.Errors.Add(ex.Message); //message from Klarna
			}
			else
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.UnspecifiedException"));
			}
		}

		private void QliroRejectOrderMessage(ValidationResult validationResult, IQliroReservationResult qliroReservationResult)
		{
			if (qliroReservationResult.StatusCode == QliroTransactionStatus.Ok && qliroReservationResult.CustomerMessage.HasValue())
			{
				validationResult.Errors.Add(qliroReservationResult.CustomerMessage); //message from Qliro
			}
			else
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Qliro.UnspecifiedException"));
			}
		}

		private void QliroRejectOrderMessage(ValidationResult validationResult)
		{
			validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Qliro.UnspecifiedException"));
		}

		private void CollectorRejectOrderMessage(ValidationResult validationResult, ICollectorAddInvoiceResult addInvoiceResult)
		{
			if (addInvoiceResult.StatusCode == (int)CollectorTransactionStatus.Ok)
			{
				validationResult.Errors.Add(addInvoiceResult.ErrorMessage);
			}
			else if (addInvoiceResult.StatusCode == (int)CollectorTransactionStatus.SoapFault)
			{
				string faultMessage = AliasSharedService.GetValueEncodedByCommonName(UserContext.Current.Channel, "Order.Collector.FaultCode." + addInvoiceResult.FaultCode);
				validationResult.Errors.Add(faultMessage.HasValue() ? faultMessage : AliasHelper.GetAliasValue("Order.Collector.UnspecifiedException"));
			}
			else
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Collector.UnspecifiedException"));
			}
		}

		private void CollectorRejectOrderMessage(ValidationResult validationResult)
		{
			validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Collector.UnspecifiedException"));
		}

		private string RejectOrder(IOrderFull order)
		{
			var lekmerOrder = (ILekmerOrderFull)order;

			OrderService.OrderVoucherRelease(UserContext.Current, lekmerOrder);

			string orderStatus = OrderStatusName.RejectedByPaymentProvider;
			order.OrderStatus = _orderStatusService.GetByCommonName(UserContext.Current, orderStatus);

			var itemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, orderStatus);
			order.SetAllOrderItemsStatus(itemStatus);

			OrderService.SaveFull(UserContext.Current, order);

			return orderStatus;
		}

		private void SaveOrder(IOrderFull order, string orderStatusCommonName)
		{
			var orderStatus = _orderStatusService.GetByCommonName(UserContext.Current, orderStatusCommonName);
			var orderItemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, orderStatusCommonName);
			SaveOrder(order, orderStatus, orderItemStatus);
		}
		private void SaveOrder(IOrderFull order, IOrderStatus orderStatus, IOrderItemStatus orderItemStatus)
		{
			order.OrderStatus = orderStatus;
			order.SetAllOrderItemsStatus(orderItemStatus);

			order.IP = Request.UserHostAddress;

			var lekmerOrder = (ILekmerOrderFull)order;

			lekmerOrder.CustomerIdentificationKey = CustomerIdentificationCookie.SetCustomerIdentificationId(CustomerSession);

			if (order.Id != 0)
			{
				if (order.CampaignInfo != null)
				{
					order.CampaignInfo.Campaigns = new Collection<IOrderCampaign>();
				}

				OrderService.SaveFull(UserContext.Current, order);
			}
			else
			{
				OrderService.SaveFullExtended(UserContext.Current, order);
			}

			// Order could be not confirmed at this stage - PaymentPending or PaymentOnHold
			if (orderStatus.CommonName == OrderStatusName.PaymentConfirmed)
			{
				OrderService.OrderVoucherConsume(UserContext.Current, lekmerOrder);

				OrderService.UpdateNumberInStock(UserContext.Current, order);

				_giftCardViaMailInfoService.Save(UserContext.Current, order);
			}
		}

		private bool IsVoucherPost()
		{
			string useVoucherValue = Request.Form[VoucherDiscountForm.UseVoucherFormName];

			return useVoucherValue == VoucherDiscountForm.UseVoucherFormValue
				|| useVoucherValue == VoucherDiscountForm.UseVoucherFormText;
		}

		private void ProcessGetAddressPost(string civicNumber, ValidationResult validationResult)
		{
			if (_payment.IsKlarnaPaymentActive)
			{
				ProcessGetAddressFromKlarna(civicNumber, validationResult);
			}
			else if (_payment.IsQliroPaymentActive)
			{
				ProcessGetAddressFromQliro(civicNumber, validationResult);
			}
			else
			{
				ProcessGetAddressFromQliro(civicNumber, validationResult); // By default
			}
		}

		private void ProcessGetAddressFromKlarna(string civicNumber, ValidationResult validationResult)
		{
			try
			{
				IKlarnaClient klarnaClient = GetKlarnaClient(_payment.ActivePaymentTypeId);

				var klarnaResult = klarnaClient.GetAddress(UserContext.Current.Channel, civicNumber, Request.UserHostAddress);

				if (klarnaResult.ResponseCode == (int)KlarnaResponseCode.Ok)
				{
					if (klarnaResult.KlarnaAddresses != null && klarnaResult.KlarnaAddresses.Count > 0)
					{
						MapInformationFromKlarna(klarnaResult.KlarnaAddresses, civicNumber);
					}
				}
				else if (klarnaResult.ResponseCode == (int)KlarnaResponseCode.TimeoutResponse)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.GetAddress.TimeoutResponse"));
					_klarnaGetAddressTimeoutResponse = true;
				}
				else if (klarnaResult.ResponseCode == (int)KlarnaResponseCode.SpecifiedError)
				{
					validationResult.Errors.Add(klarnaResult.FaultReason);
				}
				else if (klarnaResult.ResponseCode == (int)KlarnaResponseCode.UnspecifiedError)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.GetAddress.UnspecifiedException"));
				}
			}
			catch (Exception ex)
			{
				_log.Error("Error while getting address from Klarna.", ex);
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Klarna.GetAddress.UnspecifiedException"));
			}
		}

		private void ProcessGetAddressFromQliro(string civicNumber, ValidationResult validationResult)
		{
			try
			{
				var qliroResult = QliroFactory.CreateQliroClient(UserContext.Current.Channel.CommonName).GetAddress(UserContext.Current.Channel, civicNumber, Request.UserHostAddress, LekmerCheckoutForm.IsCompanyMode);

				if (qliroResult.ReturnCode == (int)QliroResponseCode.Ok)
				{
					if (qliroResult.QliroAddresses != null && qliroResult.QliroAddresses.Count > 0)
					{
						if (!IsAddressInfoEmpty(qliroResult.QliroAddresses[0]))
						{
							MapInformationFromQliro(qliroResult.QliroAddresses, civicNumber);
						}
						else
						{
							validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Qliro.GetAddress.EmptyAddress"));
						}
					}
				}
				else if (qliroResult.ReturnCode == (int)QliroResponseCode.SpecifiedError)
				{
					validationResult.Errors.Add(qliroResult.CustomerMessage);
				}
				else if (qliroResult.ReturnCode == (int)QliroResponseCode.UnspecifiedError)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Qliro.GetAddress.UnspecifiedException"));
				}
			}
			catch (Exception ex)
			{
				_log.Error("Error while getting address from Qliro.", ex);
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Qliro.GetAddress.UnspecifiedException"));
			}
		}

		private bool IsAddressInfoEmpty(IQliroAddress address)
		{
			return address.FirstName.IsEmpty()
				&& address.LastName.IsEmpty()
				&& address.StreetAddress.IsEmpty()
				&& address.StreetAddress2.IsEmpty()
				&& address.PostalCode.IsEmpty()
				&& address.City.IsEmpty()
				&& address.CompanyName.IsEmpty();
		}

		private void MapInformationFromKlarna(Collection<IKlarnaAddress> addressesFromKlarna, string civicnumber)
		{
			ICheckoutProfile checkoutProfile = GetCheckoutProfile();

			if (LekmerCheckoutForm.IsCompanyMode)
			{
				var companyAddress = addressesFromKlarna[0];

				if (companyAddress.IsCompany)
				{
					LekmerCheckoutForm.CompanyInformationForm.FullName = companyAddress.CompanyName;
				}
				else
				{
					LekmerCheckoutForm.CompanyInformationForm.Name = companyAddress.FirstName;
					LekmerCheckoutForm.CompanyInformationForm.FullName = companyAddress.LastName;
				}

				LekmerCheckoutForm.CompanyInformationForm.OrganizationNumber = civicnumber;
				LekmerCheckoutForm.CompanyInformationForm.LockFields();

				LekmerCheckoutForm.CompanyAddressForm.City = companyAddress.City;
				LekmerCheckoutForm.CompanyAddressForm.StreetAddress = companyAddress.StreetAddress;
				LekmerCheckoutForm.CompanyAddressForm.PostalCode = companyAddress.PostalCode;

				var isMultipleKlarnaAddresses = addressesFromKlarna.Count > 1;
				LekmerCheckoutForm.IsMultipleKlarnaAddresses = isMultipleKlarnaAddresses;
				LekmerCheckoutForm.KlarnaAddressesFormValue = isMultipleKlarnaAddresses ? PrepareKlarnaAddresses(addressesFromKlarna) : string.Empty;

				// Update profile fields, but not all
				if (checkoutProfile == null) return;

				if (companyAddress.IsCompany)
				{
					checkoutProfile.CompanyInformation.FullName = companyAddress.CompanyName;
				}
				else
				{
					checkoutProfile.CompanyInformation.Name = companyAddress.FirstName;
					checkoutProfile.CompanyInformation.FullName = companyAddress.LastName;
				}

				checkoutProfile.CompanyAddress.City = companyAddress.City;
				checkoutProfile.CompanyAddress.StreetAddress = companyAddress.StreetAddress;
				checkoutProfile.CompanyAddress.PostalCode = companyAddress.PostalCode;
			}
			else
			{
				var personAddress = addressesFromKlarna[0];

				LekmerCheckoutForm.InformationForm.FirstName = personAddress.FirstName;
				LekmerCheckoutForm.InformationForm.LastName = personAddress.LastName;
				LekmerCheckoutForm.InformationForm.CivicNumber = civicnumber;
				((LekmerCheckoutInformationForm)LekmerCheckoutForm.InformationForm).LockFields();

				LekmerCheckoutForm.BillingAddressForm.City = personAddress.City;
				LekmerCheckoutForm.BillingAddressForm.Addressee = personAddress.FirstName + " " + personAddress.LastName;
				LekmerCheckoutForm.BillingAddressForm.StreetAddress = personAddress.StreetAddress;
				LekmerCheckoutForm.BillingAddressForm.StreetAddress2 = personAddress.StreetAddress2;
				LekmerCheckoutForm.BillingAddressForm.PostalCode = personAddress.PostalCode;
				((LekmerCheckoutAddressForm)LekmerCheckoutForm.BillingAddressForm).LockFields();

				LekmerCheckoutForm.IsMultipleKlarnaAddresses = false;
				LekmerCheckoutForm.KlarnaAddressesFormValue = string.Empty;

				// Update profile fields, but not all
				if (checkoutProfile == null) return;

				checkoutProfile.CustomerInformation.FirstName = personAddress.FirstName;
				checkoutProfile.CustomerInformation.LastName = personAddress.LastName;

				checkoutProfile.BillingAddress.City = personAddress.City;
				checkoutProfile.BillingAddress.Addressee = personAddress.FirstName + " " + personAddress.LastName;
				checkoutProfile.BillingAddress.StreetAddress = personAddress.StreetAddress;
				checkoutProfile.BillingAddress.StreetAddress2 = personAddress.StreetAddress2;
				checkoutProfile.BillingAddress.PostalCode = personAddress.PostalCode;
			}
		}

		private void MapInformationFromQliro(Collection<IQliroAddress> addressesFromQliro, string civicnumber)
		{
			ICheckoutProfile checkoutProfile = GetCheckoutProfile();

			if (LekmerCheckoutForm.IsCompanyMode)
			{
				var companyAddress = addressesFromQliro[0];

				if (companyAddress.IsCompany)
				{
					LekmerCheckoutForm.CompanyInformationForm.FullName = companyAddress.CompanyName;
				}
				else
				{
					LekmerCheckoutForm.CompanyInformationForm.Name = companyAddress.FirstName;
					LekmerCheckoutForm.CompanyInformationForm.FullName = companyAddress.LastName;
				}

				LekmerCheckoutForm.CompanyInformationForm.OrganizationNumber = civicnumber;
				LekmerCheckoutForm.CompanyInformationForm.LockFields();

				LekmerCheckoutForm.CompanyAddressForm.City = companyAddress.City;
				LekmerCheckoutForm.CompanyAddressForm.StreetAddress = companyAddress.StreetAddress;
				LekmerCheckoutForm.CompanyAddressForm.PostalCode = companyAddress.PostalCode;

				var isMultipleQliroAddresses = addressesFromQliro.Count > 1;
				LekmerCheckoutForm.IsMultipleKlarnaAddresses = isMultipleQliroAddresses;
				LekmerCheckoutForm.KlarnaAddressesFormValue = isMultipleQliroAddresses ? PrepareQliroAddresses(addressesFromQliro) : string.Empty;

				// Update profile fields, but not all
				if (checkoutProfile == null) return;

				if (companyAddress.IsCompany)
				{
					checkoutProfile.CompanyInformation.FullName = companyAddress.CompanyName;
				}
				else
				{
					checkoutProfile.CompanyInformation.Name = companyAddress.FirstName;
					checkoutProfile.CompanyInformation.FullName = companyAddress.LastName;
				}

				checkoutProfile.CompanyAddress.City = companyAddress.City;
				checkoutProfile.CompanyAddress.StreetAddress = companyAddress.StreetAddress;
				checkoutProfile.CompanyAddress.PostalCode = companyAddress.PostalCode;
			}
			else
			{
				var personAddress = addressesFromQliro[0];

				LekmerCheckoutForm.InformationForm.FirstName = personAddress.FirstName;
				LekmerCheckoutForm.InformationForm.LastName = personAddress.LastName;
				LekmerCheckoutForm.InformationForm.CivicNumber = civicnumber;
				((LekmerCheckoutInformationForm)LekmerCheckoutForm.InformationForm).LockFields();

				LekmerCheckoutForm.BillingAddressForm.City = personAddress.City;
				LekmerCheckoutForm.BillingAddressForm.Addressee = personAddress.FirstName + " " + personAddress.LastName;
				LekmerCheckoutForm.BillingAddressForm.StreetAddress = personAddress.StreetAddress;
				LekmerCheckoutForm.BillingAddressForm.StreetAddress2 = personAddress.StreetAddress2;
				LekmerCheckoutForm.BillingAddressForm.PostalCode = personAddress.PostalCode;
				((LekmerCheckoutAddressForm)LekmerCheckoutForm.BillingAddressForm).LockFields();

				LekmerCheckoutForm.IsMultipleKlarnaAddresses = false;
				LekmerCheckoutForm.KlarnaAddressesFormValue = string.Empty;

				// Update profile fields, but not all
				if (checkoutProfile == null) return;

				checkoutProfile.CustomerInformation.FirstName = personAddress.FirstName;
				checkoutProfile.CustomerInformation.LastName = personAddress.LastName;

				checkoutProfile.BillingAddress.City = personAddress.City;
				checkoutProfile.BillingAddress.Addressee = personAddress.FirstName + " " + personAddress.LastName;
				checkoutProfile.BillingAddress.StreetAddress = personAddress.StreetAddress;
				checkoutProfile.BillingAddress.StreetAddress2 = personAddress.StreetAddress2;
				checkoutProfile.BillingAddress.PostalCode = personAddress.PostalCode;
			}
		}


		protected override BlockContent RenderCheckoutForm(ICartFull cart, ValidationResult validationResult)
		{
			string validationError = null;

			if (!validationResult.IsValid)
			{
				var validationControl = CreateValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			return RenderCheckoutForm(cart, validationError);
		}

		public BlockContent RenderCheckoutForm(ICartFull cart, string validationError)
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			var fragmentContentInfo = new FragmentVariableInfo(fragmentContent);

			fragmentContent.AddEntity(Block);
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);

			fragmentContent.AddVariable("Delivery", _delivery.Render(), VariableEncoding.None);

			decimal paymentCost = _payment.GetPaymentCost();
			decimal deliveryPrice = _delivery.GetActualDeliveryPrice(cart);
			decimal optionalDeliveryPrice = _delivery.GetActualOptionalDeliveryPrice(cart);
			decimal diapersDeliveryPrice = _delivery.GetActualDiapersDeliveryPrice(cart);
			bool optionalDeliveryIsUsed = _delivery.GetOrderOptionalDelivery(cart) != null;
			bool diapersDeliveryIsUsed = _delivery.GetOrderDiapersDelivery(cart) != null;

			Price actualPriceSummary = cart.GetActualPriceSummary();
			decimal totalFreight = deliveryPrice + optionalDeliveryPrice + diapersDeliveryPrice;
			decimal totalPrice = actualPriceSummary.IncludingVat + totalFreight + paymentCost;

			fragmentContent.AddVariable("Payment", _payment.Render(), VariableEncoding.None);
			fragmentContent.AddCondition("PaymentType.HasCost", paymentCost > 0);
			fragmentContent.AddVariable("PaymentType.Cost", _formatter.FormatPrice(Channel.Current, paymentCost));

			fragmentContent.AddCondition("DeliveryMethod.IsUsed", _delivery.CheckForDeliveryUsage(cart));
			fragmentContent.AddVariable("DeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, deliveryPrice));

			fragmentContent.AddCondition("OptionalDeliveryMethod.IsUsed", optionalDeliveryIsUsed);
			fragmentContent.AddVariable("OptionalDeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, optionalDeliveryPrice));

			fragmentContent.AddCondition("DiapersDeliveryMethod.IsUsed", diapersDeliveryIsUsed);
			fragmentContent.AddVariable("DiapersDeliveryMethod.FreightCost", _formatter.FormatPrice(Channel.Current, diapersDeliveryPrice));

			fragmentContent.AddVariable("Checkout.TotalPriceWithFreight", _formatter.FormatPrice(Channel.Current, totalPrice));
			fragmentContent.AddCondition("isKlarnaPartPayment", PaymentTypeHelper.IsKlarnaPartPayment(_payment.ActivePaymentTypeId));

			fragmentContent.AddCondition("Klarna.GetAddress.IsTimeoutResponse", _klarnaGetAddressTimeoutResponse);

			RenderMonthlypartPaymentCost(fragmentContent, fragmentContentInfo, totalPrice);

			fragmentContent.AddVariable("AddressList", RenderAddressList().Render(), VariableEncoding.None);

			RenderKlarnaCheckout(fragmentContent, cart, actualPriceSummary);
			fragmentContent.AddVariable("Checkout.KlarnaCheckoutPaymentId", ((int)PaymentMethodType.KlarnaCheckout).ToString(CultureInfo.InvariantCulture));


			fragmentContent.AddCondition("Cart.IsProductAbove60LExist", _cartHelper.CheckForMixedWarehouses(cart));

			CheckForManualEdit(cart, actualPriceSummary);

			fragmentContent.AddCondition("Cart.ContainsMixedItems", _cartHelper.CheckForMixedItems(cart));
			fragmentContent.AddCondition("Cart.ContainsOnlyDiapers", _cartHelper.ContainsOnlyDiapers(cart));

			LekmerCheckoutForm.IsDibsPayment = _payment.ActivePaymentTypeId == (int)PaymentMethodType.DibsCreditCard || _payment.ActivePaymentTypeId == (int)PaymentMethodType.DibsiDeal;
			LekmerCheckoutForm.MapFieldNamesToFragment(fragmentContent);
			LekmerCheckoutForm.MapFieldValuesToFragment(fragmentContent);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);

			fragment.AddEntity(Block);
			LekmerCheckoutForm.MapFieldNamesToFragment(fragment);

			return fragment.Render();
		}


		private void RenderMonthlypartPaymentCost(Fragment fragmentContent, FragmentVariableInfo fragmentContentInfo, decimal klarnaSum)
		{
			if (PaymentHelper.IsKlarnaSupported(Channel.Current))
			{
				if (fragmentContentInfo.HasCondition("ShowMonthlyPartPaymentCost") || fragmentContentInfo.HasVariable("KlarnaPartPaymentMonthlyCosts"))
				{
					try
					{
						decimal monthlyPayment = (decimal)PaymentHelper.GetLowestPartPayment(Channel.Current, klarnaSum);

						fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", monthlyPayment > 0);
						fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", _formatter.FormatPriceChannelOrLessDecimals(Channel.Current, monthlyPayment));
					}
					catch (Exception ex)
					{
						_log.Error("Error while getting lowest part payment.", ex);

						fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", false);
						fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", "[ Error while getting lowest part payment. ]", VariableEncoding.None);
					}
				}
			}
			else
			{
				fragmentContent.AddCondition("ShowMonthlyPartPaymentCost", false);
				fragmentContent.AddVariable("KlarnaPartPaymentMonthlyCosts", "[ Klarna payment not supported in current channel. ]");
			}
		}

		private void RedirectToCreditCard(IOrderFull order)
		{
			int paymentTypeId = order.Payments.First().PaymentTypeId;
			IPaymentType paymentType = PaymentTypes.SingleOrDefault(type => type.Id == paymentTypeId);

			string dibsRequest = DibsPaymentProvider.CreateRequest(order.Customer.CustomerInformation, order, Channel.Current.Currency, paymentType);

			Response.Redirect(dibsRequest);
		}

		private void RedirectToMoneybookers(IOrderFull order)
		{
			Response.Redirect(SkrillPaymentProvider.CreateRequest(order, order.Customer.CustomerInformation, Channel.Current.Currency));
		}

		private void RedirectToMaksuturva(IOrderFull order)
		{
			ProcessMaksuturvaPendingOrder(order);
			Response.Redirect(MaksuturvaPaymentProvider.GetMaksuturvaTemporaryUrl(order.ChannelId));
		}

		private void ProcessMaksuturvaPendingOrder(IOrderFull order)
		{
			MaksuturvaSetting.Initialize(UserContext.Current.Channel.CommonName);

			var pendingOrder = MaksuturvaPendingOrderService.Create();
			pendingOrder.OrderId = order.Id;
			pendingOrder.ChannelId = order.ChannelId;
			pendingOrder.StatusId = order.OrderStatus.Id;
			pendingOrder.FirstAttempt = null;
			pendingOrder.LastAttempt = null;
			pendingOrder.NextAttempt = MaksuturvaSetting.GetOrderCheckDate(order.OrderStatus.CommonName, order.CreatedDate, order.CreatedDate);
			MaksuturvaPendingOrderService.Insert(pendingOrder);
		}

		/// <summary>
		/// Gets a new/existing customer.
		/// </summary>
		/// <returns></returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected override ICustomer GetCustomer()
		{
			var customerService = IoC.Resolve<ICustomerService>();

			ICustomer customer = null;

			if (SignedInCustomerId.HasValue)
			{
				customer = customerService.GetById(UserContext.Current, SignedInCustomerId.Value);
			}

			if (customer == null)
			{
				customer = customerService.Create(UserContext.Current, true, false);
			}

			if (customer.CustomerInformation == null)
			{
				customer.CustomerInformation = IoC.Resolve<ICustomerInformationService>().Create(UserContext.Current);
			}

			if (customer.CustomerInformation.DefaultBillingAddress == null)
			{
				customer.CustomerInformation.DefaultBillingAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
			}

			customer.CustomerInformation.DefaultBillingAddress.CountryId = Channel.Current.Country.Id;

			var information = (ILekmerCustomerInformation)customer.CustomerInformation;
			if (information.AlternateAddress == null)
			{
				information.AlternateAddress = (ILekmerAddress)IoC.Resolve<IAddressService>().Create(UserContext.Current);
			}
			information.AlternateAddress.CountryId = Channel.Current.Country.Id;

			return customer;
		}

		protected override void RefreshCart()
		{
			LekmerCartHelper.RefreshSessionCart();
		}

		protected virtual Fragment RenderAddressList()
		{
			Fragment fragment = Template.GetFragment("AddressList");

			var stringBuilder = new StringBuilder();
			if (!LekmerCheckoutForm.KlarnaAddressesFormValue.IsEmpty())
			{
				stringBuilder.AppendLine(RenderDefaultAddress().Render());
				var klarnaAddresses = PrepareKlarnaAddresses(LekmerCheckoutForm.KlarnaAddressesFormValue);
				foreach (var address in klarnaAddresses)
				{
					stringBuilder.AppendLine(RenderAddress(address).Render());
				}
			}
			fragment.AddVariable("Iterate:Address", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}
		protected virtual Fragment RenderDefaultAddress()
		{
			var value = AliasHelper.GetAliasValue("Order.Checkout.DefaultAlternativeDeliveryAddress");
			return RenderAddress(value, value);
		}
		protected virtual Fragment RenderAddress(IKlarnaAddress address)
		{
			var text = string.Format("{0}, {1}, {2} {3}", address.CompanyName, address.StreetAddress, address.PostalCode, address.City);
			var value = string.Format(_klarnaAddressTemplate, address.CompanyName, address.StreetAddress, address.PostalCode, address.City);
			return RenderAddress(text, value);
		}
		protected virtual Fragment RenderAddress(string text, string value)
		{
			Fragment fragment = Template.GetFragment("Address");
			fragment.AddVariable("Checkout.KlarnaAddress.Text", text, VariableEncoding.None);
			fragment.AddVariable("Checkout.KlarnaAddress.Value", value, VariableEncoding.None);
			fragment.AddCondition("Checkout.KlarnaAddress.IsSelected", value == LekmerCheckoutForm.KlarnaSelectedAddressFormValue);
			return fragment;
		}

		protected virtual void RenderKlarnaCheckout(Fragment fragment, ICartFull cart, Price actualPriceSummary)
		{
			if (PaymentTypeHelper.IsKlarnaCheckoutPayment(_payment.ActivePaymentTypeId))
			{
				string klarnaCheckoutContent = string.Empty;
				bool isKlarnaCheckout = false;

				var items = ((ILekmerCartFull)cart).GetGroupedCartItemsByProductAndPriceAndSize().Cast<ICartItem>().ToList();

				decimal deliveryFreightCost = _delivery.GetActualDeliveryPrice(cart);
				decimal optionalDeliveryFreightCost = _delivery.GetActualOptionalDeliveryPrice(cart);
				decimal diapersDeliveryFreightCost = _delivery.GetActualDiapersDeliveryPrice(cart);
				decimal totalFreight = deliveryFreightCost + optionalDeliveryFreightCost + diapersDeliveryFreightCost;

				var discountAmount = GetDiscountAmount((ILekmerCartFull)cart, actualPriceSummary);
				Uri orderLocation = _checkoutSession.KlarnaCheckoutId;

				try
				{
					klarnaCheckoutContent = KlarnaFactory.CreateKlarnaCheckoutClient(UserContext.Current.Channel.CommonName).GetKlarnaCheckoutContent(
					UserContext.Current,
					items,
					totalFreight,
					discountAmount,
					ref orderLocation);
				}
				catch (Exception ex)
				{
					_log.Error("Error occurred while getting klarna checkout content.", ex);
				}

				_checkoutSession.KlarnaCheckoutId = orderLocation;

				if (orderLocation != null && klarnaCheckoutContent.HasValue())
				{
					isKlarnaCheckout = true;

					var uri = orderLocation.ToString();
					var splittedUri = uri.Split('/').ToList();
					var orderKlarnaId = splittedUri.Last();

					// Create new order with status = 'KcoPaymentPanding'
					var customer = GetCustomer();
					customer = SetDefaultValues(customer);

					var orderStatus = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.KcoPaymentPending);
					var orderItemStatus = _orderItemStatusService.GetByCommonName(UserContext.Current, OrderStatusName.KcoPaymentPending);
					ILekmerOrderFull order = CreateOrder(cart, customer, orderStatus, orderItemStatus);

					order.KcoId = orderKlarnaId;
					SaveOrder(order, orderStatus, orderItemStatus);
				}
				else
				{
					_log.Error("Klarna returns 'klarna_order' url = null or checkout content = null/empty");
				}

				AddKlarnaCheckoutVariables(fragment, isKlarnaCheckout, klarnaCheckoutContent);
			}
			else
			{
				AddKlarnaCheckoutVariables(fragment, false, string.Empty);
			}
		}

		private void AddKlarnaCheckoutVariables(Fragment fragment, bool isKlarnaCheckout, string klarnaCheckoutContent)
		{
			fragment.AddVariable("Payment.KlarnaCheckout.Snippet", klarnaCheckoutContent, VariableEncoding.None);
			fragment.AddCondition("Payment.KlarnaCheckout.IsSelected", isKlarnaCheckout);
		}

		private void UpdateOrderKcoStatus()
		{
			object orderLocation = _checkoutSession.KlarnaCheckoutId;

			if (orderLocation != null)
			{
				var uri = orderLocation.ToString();
				var splittedUri = uri.Split('/').ToList();
				var orderKlarnaId = splittedUri.Last();
				var orderByKlarna = OrderService.GetByKcoId(UserContext.Current, orderKlarnaId);
				if (orderByKlarna != null)
				{
					// Set order status = 'KcoDeleted'
					var orderStatusDeleted = _orderStatusService.GetByCommonName(UserContext.Current, OrderStatusName.KcoDeleted);
					OrderService.UpdateOrderStatus(orderByKlarna.Id, orderStatusDeleted.Id);
				}
			}
		}

		private decimal? GetDiscountAmount(ILekmerCartFull cart, Price actualPriceSummary)
		{
			decimal? discountAmount = null;

			Price price = cart.GetBaseActualPriceSummary() - actualPriceSummary;
			if (price.IncludingVat > 0)
			{
				discountAmount = price.IncludingVat;
			}

			return discountAmount;
		}

		private ICustomer SetDefaultValues(ICustomer customer)
		{
			customer.CustomerInformation.FirstName = string.Empty;
			customer.CustomerInformation.LastName = string.Empty;
			customer.CustomerInformation.Email = string.Empty;

			var information = (ILekmerCustomerInformation)customer.CustomerInformation;
			information.DefaultBillingAddress.Addressee = string.Empty;
			information.DefaultBillingAddress.StreetAddress = string.Empty;
			information.DefaultBillingAddress.PostalCode = string.Empty;
			information.DefaultBillingAddress.City = string.Empty;

			information.AlternateAddress = null;

			return customer;
		}

		private ICheckoutProfile GetCheckoutProfile()
		{
			var checkoutSession = IoC.Resolve<ICheckoutSession>();
			ICheckoutProfile checkoutProfile = checkoutSession.Profile;
			if (checkoutProfile == null)
			{
				checkoutSession.InitializeProfile();
				checkoutProfile = checkoutSession.Profile;
			}
			return checkoutProfile;
		}

		private void CheckForBadAddresses(ICustomer customer)
		{
			try
			{
				if(
					customer.CustomerInformation.DefaultBillingAddress.StreetAddress.Length == 1 ||
					customer.CustomerInformation.DefaultBillingAddress.PostalCode.Length == 1 ||
					customer.CustomerInformation.LastName.Length == 1
					)
				{
					_log.ErrorFormat("{0}\r\nUrl: {1}\r\nForm data: \r\n{2}", "Bad addresses.", Request.Url.AbsoluteUri, LogRequestForm());
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		private string LogRequestForm()
		{
			var sb = new StringBuilder();

			foreach (string key in Request.Form.AllKeys)
			{
				sb.AppendFormat("{0}: {1}", key, Request.Form[key]);
				sb.AppendLine();
			}

			return sb.ToString();
		}

		private string PrepareKlarnaAddresses(IEnumerable<IKlarnaAddress> addresses)
		{
			var stringBuilder = new StringBuilder();
			foreach (var address in addresses)
			{
				stringBuilder.Append(string.Format(_klarnaAddressTemplate, address.CompanyName, address.StreetAddress, address.PostalCode, address.City) + _klarnaAddressSeperator);
			}
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Remove(stringBuilder.Length - 1, 1);
			}
			return stringBuilder.ToString();
		}
		private IEnumerable<IKlarnaAddress> PrepareKlarnaAddresses(string addresses)
		{
			var klarnaAddresses = new Collection<IKlarnaAddress>();
			var addressesArray = addresses.Split(_klarnaAddressSeperator);
			foreach (var address in addressesArray)
			{
				var addressElements = address.Split(_klarnaPartAddressSeperator);
				klarnaAddresses.Add(new KlarnaAddress
				{
					StreetAddress = addressElements[1],
					PostalCode = addressElements[2],
					City = addressElements[3],
					CompanyName = addressElements[0]
				});
			}
			return klarnaAddresses;
		}

		private string PrepareQliroAddresses(IEnumerable<IQliroAddress> addresses)
		{
			var stringBuilder = new StringBuilder();
			foreach (var address in addresses)
			{
				if (!IsAddressInfoEmpty(address))
				{
					stringBuilder.Append(string.Format(_klarnaAddressTemplate, address.CompanyName, address.StreetAddress, address.PostalCode, address.City) + _klarnaAddressSeperator);
				}
			}
			if (stringBuilder.Length > 0)
			{
				stringBuilder.Remove(stringBuilder.Length - 1, 1);
			}
			return stringBuilder.ToString();
		}

		protected virtual bool IsActivePayment(PaymentMethodType paymentType)
		{
			return _payment.ActivePaymentTypeId == (int)paymentType;
		}

		protected virtual void CheckForItemsType(ICartFull cartFull)
		{
			if (_cartHelper.DiapersAreSellable(UserContext.Current.Channel))
			{
				var cart = (ILekmerCartFull)cartFull;
				if (cart.ItemsType.IsFlagSet(OrderItemsType.Diapers))
				{
					((LekmerAddressForm) LekmerCheckoutForm.BillingAddressForm).DoorCodeIsAvailable = true;
					((LekmerAddressForm) LekmerCheckoutForm.DeliveryAddressForm).DoorCodeIsAvailable = true;

					return;
				}
			}

			_delivery.Render();
			IDeliveryMethod optionalDelivery = _delivery.GetOrderOptionalDelivery(cartFull);
			if (optionalDelivery != null && optionalDelivery.Id == (int)DeliveryMethodType.DoorstepDelivery)
			{
				((LekmerAddressForm)LekmerCheckoutForm.BillingAddressForm).DoorCodeIsAvailable = true;
				((LekmerAddressForm)LekmerCheckoutForm.DeliveryAddressForm).DoorCodeIsAvailable = true;
			}
		}

		protected virtual ValidationResult ValidateItemsType(ICartFull cartFull)
		{
			var validationResult = new ValidationResult();

			if (UserContext.Current.Channel.Country.Iso.ToUpperInvariant() == "SE")
			{
				var cart = (ILekmerCartFull)cartFull;
				if (cart.ItemsType.IsFlagSet(OrderItemsType.Diapers) && LekmerCheckoutForm.IsCompanyMode)
				{
					validationResult.Errors.Add(AliasHelper.GetAliasValue("Order.Checkout.Validation.CompanyCanNotBuyDiapers"));
				}
			}

			return validationResult;
		}

		protected void CheckForManualEdit(ICartFull cartFull, Price actualPriceSummary)
		{
			var isTotalOverPredefinedValue = ((ILekmerCartFull)cartFull).IsTotalOverPredefinedValue(actualPriceSummary.IncludingVat);

			bool isKlarnaPayment = _payment.IsKlarnaPaymentActive;

			((LekmerCheckoutInformationForm)LekmerCheckoutForm.InformationForm).ManualEdit = (isKlarnaPayment == false); // Klarna - deny, Other - allow

			LekmerCheckoutForm.CompanyInformationForm.ManualEdit = (isKlarnaPayment == false) || (isTotalOverPredefinedValue == false); // Klarna > 5000 - deny, Klarna < 5000 - allow, Other - allow
		}
	}
}