﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Litium.Lekmer.Order.Web.Validation
{
	public static class ValidationUtil
	{
		#region Regex

		private const string ipValidationPattern = @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}";

		//yyyymmdd-XXXX
		//private const string civicNumberValidationPatternSwedish =
		//	@"^(?<date>(?<year>\d{4})(?<month>\d{2})(?<day>\d{2}))[-\+]?(?<additional>\d{3})(?<checkdigit>\d{1})$"; //Swedish 
		private const string civicNumberValidationPatternSwedish =
			@"^(\b(?:19|20)?\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[-+]?\d{4}\b)$"; //Swedish

		//ddmmyy-XXXX
		private const string civicNumberValidationPatternDanish =
			@"^(?<date>(?<day>\d{2})(?<month>\d{2})(?<year>\d{2}))[-\+]?(?<additional>\d{3})(?<checkdigit>\d{1})$"; //Danish 

		//ddmmyy-XXXXX
		private const string civicNumberValidationPatternNorwegian =
			@"^(?<date>(?<day>\d{2})(?<month>\d{2})(?<year>\d{2}))[-\+]?(?<additional>\d{5})$"; //Norwegian

		//ddmmyy-XXXX
		private const string civicNumberValidationPatternFinnish =
			@"^(?<date>(?<day>\d{2})(?<month>\d{2})(?<year>\d{2}))[-\+]?(?<additional>\d{3})(?<checkdigit>[A-Z\d]{1})$"; //Finnish 

		private const string civicNumberValidationPatternEU =
			@"^(?<date>(?<year>\d{4})(?<month>\d{2})(?<day>\d{2}))$"; //EU

		private const string civicNumberValidationPatternDate = @"^(?<year>\d{4})(?<month>\d{2})(?<day>\d{2})$";

		private const string svPostalNumberValidationPattern = @"^[1-9]\d{4}$"; //Swedish
		private const string noPostalNumberValidationPattern = @"^\d{4}$"; //Norway
		private const string dkPostalNumberValidationPattern = @"^\d{4}$"; //Denmark
		private const string fiPostalNumberValidationPattern = @"^\d{5}$"; //Finland
		private const string nlPostalNumberValidationPattern = @"^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$"; //Netherlands
		private const string frPostalNumberValidationPattern = @"^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$"; //France

		private const string phoneNumberValidationPattern = @"^(\+(\d{1,3}\s*)|)\d{1,4}(-|/|\s|)(\d\s*){5,10}$"; //Swedish 

		private const string urlValidationPattern =
			@"(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?";

		private const string organizationNumberValidationPattern = @"^\d{6}[-]\d{4}$"; //Swedish 

		#endregion

		/// <summary>
		/// Validates if the input string is a valid ip address.
		/// </summary>
		/// <param name="input">An ip address to validate.</param>
		/// <returns>True if the input is a valid ip address, false if not.</returns>
		public static bool IsValidIp(string input)
		{
			if (input == null) throw new ArgumentNullException("Input not supplied for IsValidIp");

			// Regexp check.
			if (!Regex.Match(input, ipValidationPattern).Success)
			{
				return false;
			}

			// Check parts so they are between 0 and 255.
			string[] ipParts = input.Split('.');

			foreach (string part in ipParts)
			{
				int intPart = int.Parse(part);

				if (intPart < 0 || intPart > 255)
				{
					return false;
				}
			}

			// Ip is valid!
			return true;
		}


		/// <summary>
		/// Validates a civic number.
		/// </summary>
		/// <param name="input"></param>
		/// <param name="country"></param>
		/// <returns></returns>
		public static bool IsValidCivicNumber(string input, CountryISO country)
		{
			switch (country)
			{
				case CountryISO.SE:
					bool isValid = IsValidSwedishCivicNumber(input);
					if (!isValid)
					{
						isValid = IsValidOrganizationNumber(input);
					}
					return isValid;
				case CountryISO.NO:
					return IsValidNorwegianCivicNumber(input);
				case CountryISO.DK:
					return IsValidDanishCivicNumber(input);
				case CountryISO.FI:
					return IsValidFinnishCivicNumber(input);
				case CountryISO.NL:
					return IsValidNetherlandsCivicNumber(input);
				case CountryISO.FR:
					return IsValidFranceCivicNumber(input);
				case CountryISO.EU:
				default:
					return IsValidDateCivicNumber(input);
			}
		}

		/// <summary>
		/// Validates a civic number.
		/// Checks so it is of the format 'yyyymmdd' or 'yyyymmdd'.
		/// Checks so it is a correct birth date that is not in the future.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidDateCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, civicNumberValidationPatternDate);
			if (!match.Success)
			{
				return false;
			}

			// Check date.
			string dateToTest =
				string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			DateTime birthDate;
			if (!DateTime.TryParse(dateToTest, out birthDate))
			{
				return false;
			}

			// Check if date is in future.
			if (birthDate <= DateTime.Now)
				return true;
			else
				return false;
		}

		/// <summary>
		/// Validates a swedish civic number.
		/// Checks so it is of the format 'yyyymmdd' or 'yyyymmdd'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidSwedishCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, civicNumberValidationPatternSwedish);
			//if (!match.Success)
			//{
			//    return false;
			//}

			return match.Success;
			//// Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			//DateTime birthDate;
			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//{
			//    return false;
			//}

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//// Check digit.
			//int checkDigit = int.Parse(match.Groups["checkdigit"].Value);
			//if (CalculateCheckDigit(match.Groups["date"].Value.Substring(2) + match.Groups["additional"].Value) == checkDigit)
			//    return true;
			//return false;
		}

		/// <summary>
		/// Validates a danish civic number.
		/// Checks so it is of the format 'ddmmyy' or 'ddmmyy'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidDanishCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, civicNumberValidationPatternDanish);
			if (!match.Success)
			{
				return false;
			}
			return true;

			//    // Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			////CultureInfo f = new CultureInfo("da-DK");
			//DateTime birthDate;

			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//{
			//    return false;
			//}

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//string dkCivic = match.Groups["day"].Value
			//    + match.Groups["month"].Value
			//    + match.Groups["year"].Value.Substring(2)
			//    + match.Groups["additional"].Value + match.Groups["checkdigit"].Value;


			//int[] mod11_array = new int[9] { 4, 3, 2, 7, 6, 5, 4, 3, 2 };
			//int sum = 0;

			//for (int i = 0; i < 9; i++)
			//{
			//    sum += int.Parse(dkCivic.Substring(i, 1)) * mod11_array[i];
			//}
			//int modulo = sum % 11;
			//int check_number = 0;

			//if (modulo > 0)
			//    check_number = 11 - modulo;

			//if (int.Parse(match.Groups["checkdigit"].Value) == check_number)
			//    return true;
			//else
			//    return false;

		}

		/// <summary>
		/// Validates a norwegian civic number.
		/// Checks so it is of the format 'ddmmyy' or 'ddmmyy'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidNorwegianCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, civicNumberValidationPatternNorwegian);
			if (!match.Success)
			{
				return false;
			}

			return true;

			//// Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			//DateTime birthDate;
			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//{
			//    return false;
			//}

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//input = match.Groups["day"].Value + match.Groups["month"].Value + match.Groups["year"].Value.Substring(2) + match.Groups["additional"].Value;

			//int sum1 = 0;
			//int sum2 = 0;

			//int[] weight1 = new int[11] { 3, 7, 6, 1, 8, 9, 4, 5, 2, 1, 0 };
			//int[] weight2 = new int[11] { 5, 4, 3, 2, 7, 6, 5, 4, 3, 2, 1 };

			////Length must be 11, ddmmyyXXXXX
			//if (input.Length == 11)
			//{
			//    for (int i = 0; i < 11; i++)
			//    {
			//        sum1 += int.Parse(input.Substring(i, 1)) * weight1[i];
			//        sum2 += int.Parse(input.Substring(i, 1)) * weight2[i];
			//    }
			//    if (sum1 % 11 != 0 || sum2 % 11 != 0)
			//        return false;
			//    return true;
			//}
			//else
			//    return false;
		}

		/// <summary>
		/// Validates a finnish civic number.
		/// Checks so it is of the format 'ddmmyy' or 'ddmmyy'.
		/// Checks so it is a correct birth date that is not in the future.
		/// Checks the check digit.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidFinnishCivicNumber(string input)
		{
			// Check regex pattern.
			Match match = Regex.Match(input, civicNumberValidationPatternFinnish);
			if (!match.Success)
			{
				return false;
			}

			return true;

			//// Check date.
			//string dateToTest =
			//    string.Format("{0}-{1}-{2}", match.Groups["year"].Value, match.Groups["month"].Value, match.Groups["day"].Value);
			//DateTime birthDate;
			//if (!DateTime.TryParseExact(dateToTest, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out birthDate))
			//    return false;

			//// Check if date is in future.
			//if (birthDate > DateTime.Now)
			//{
			//    return false;
			//}

			//string ctrlChars = "0123456789ABCDEFHJKLMNPRSTUVWXY";

			//input = match.Groups["day"].Value + match.Groups["month"].Value + match.Groups["year"].Value.Substring(2) +
			//        match.Groups["additional"].Value + match.Groups["checkdigit"].Value;
			//int summa = 0;
			//if (input.Length < 10)
			//    return false;
			//else
			//{
			//    int calc = int.Parse(input.Substring(0, 9));
			//    summa = calc % 31;
			//}

			//if (!match.Groups["checkdigit"].Value.Equals(ctrlChars.Substring(summa, 1)))
			//    return false;
			//return true;

		}

		/// <summary>
		/// Validates a Netherlands civic number.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidNetherlandsCivicNumber(string input)
		{
			//TODO: Add real validation
			return true;
		}

		/// <summary>
		/// Validates a France civic number.
		/// </summary>
		/// <param name="input">The civic number to validate.</param>
		/// <returns></returns>
		private static bool IsValidFranceCivicNumber(string input)
		{
			//TODO: Add real validation
			return true;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <author>Jonas Ljunggren</author>
		/// <date>2008-06-18</date>
		/// <param name="idNumber">The number to calculate a check digit from.</param>
		/// <returns></returns>
		private static int CalculateCheckDigit(string idNumber)
		{
			int controlNumber = 0;

			char[] idNumberChars = idNumber.ToCharArray();

			int newNumber = 0;

			bool include = true;

			for (int charCount = 0; charCount < idNumberChars.Length; charCount++)
			{
				int number = Convert.ToInt32(idNumberChars[charCount].ToString());
				if (include)
					number *= 2;

				include = !include;

				if (number > 9)
					number -= 9;

				newNumber += number;
			}

			string newNumberAsString = newNumber.ToString();

			string last = newNumberAsString.Substring(newNumberAsString.Length - 1);
			controlNumber = 10 - Convert.ToInt32(last);

			if (controlNumber == 10)
				controlNumber = 0;

			return controlNumber;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="input">Valideates organization number pattern xxxxxx-xxxx</param>
		/// <returns></returns>
		public static bool IsValidOrganizationNumber(string input)
		{
			return Regex.Match(input, organizationNumberValidationPattern).Success;
		}

		public static bool IsValidURL(string input)
		{
			return Regex.Match(input, urlValidationPattern).Success;
		}

		public static bool IsValidPostalNumber(string input, CountryISO country)
		{
			string postalNumberMatchPattern;
			switch (country)
			{
				case CountryISO.SE:
					postalNumberMatchPattern = svPostalNumberValidationPattern;
					break;
				case CountryISO.NO:
					postalNumberMatchPattern = noPostalNumberValidationPattern;
					break;
				case CountryISO.DK:
					postalNumberMatchPattern = dkPostalNumberValidationPattern;
					break;
				case CountryISO.FI:
					postalNumberMatchPattern = fiPostalNumberValidationPattern;
					break;
				case CountryISO.NL:
					postalNumberMatchPattern = nlPostalNumberValidationPattern;
					break;
				case CountryISO.FR:
					postalNumberMatchPattern = frPostalNumberValidationPattern;
					break;
				default:
					// IF we don't have a validation pattern return true for every string.
					return true;
			}
			return Regex.Match(input, postalNumberMatchPattern).Success;
		}

		public enum CountryISO
		{
			SE = 0,
			NO = 1,
			DK = 2,
			FI = 3,
			NL = 4,
			FR = 5,
			EU = 6
		}
	}
}
