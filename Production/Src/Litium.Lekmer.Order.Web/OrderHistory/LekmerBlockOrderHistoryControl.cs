﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Web.OrderHistory;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Order.Web
{
	public class LekmerBlockOrderHistoryControl : BlockOrderHistoryControl
	{
		public LekmerBlockOrderHistoryControl(ITemplateFactory templateFactory, IBlockService blockService) : base(templateFactory, blockService)
		{
		}

		protected override BlockContent RenderCore()
		{
			var customerSession = IoC.Resolve<ICustomerSession>();
			if (!customerSession.IsSignedIn)
			{
				return new BlockContent();
			}

			int customerId = customerSession.SignedInCustomer.Id;

			var orderService = IoC.Resolve<IOrderService>();
			var orders = orderService.GetAllByCustomer(UserContext.Current, customerId);

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddCondition("HasOrders", orders.Count > 0);
			fragmentContent.AddVariable("OrderList", RenderOrderList(orders), VariableEncoding.None);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		protected override string RenderOrderList(Collection<IOrder> orders)
		{
			if (orders == null) throw new ArgumentNullException("orders");
			if (orders.Count == 0) return null;

			var orderBuilder = new StringBuilder();

			foreach (IOrderFull order in orders)
			{
				Fragment fragmentOrderItem = Template.GetFragment("Order");
				fragmentOrderItem.AddEntity(order);
				orderBuilder.AppendLine(fragmentOrderItem.Render());
			}

			Fragment fragmentOrderList = Template.GetFragment("OrderList");
			fragmentOrderList.AddVariable("Iterate:Order", orderBuilder.ToString(), VariableEncoding.None);
			return fragmentOrderList.Render();
		}
	}
}