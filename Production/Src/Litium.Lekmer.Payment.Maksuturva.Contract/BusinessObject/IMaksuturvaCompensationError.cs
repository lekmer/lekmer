﻿using System;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public interface IMaksuturvaCompensationError
	{
		int Id { get; set; }
		string ChannelCommonName { get; set; }
		DateTime FetchDate { get; set; }
		DateTime? LastAttempt { get; set; }
	}
}