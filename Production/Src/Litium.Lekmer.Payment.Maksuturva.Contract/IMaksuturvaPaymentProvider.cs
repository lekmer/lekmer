﻿using System;
using System.Collections.Generic;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public interface IMaksuturvaPaymentProvider
	{
		string GetMaksuturvaTemporaryUrl(int channelId);
		string GetMaksuturvaPaymentUrl(int channelId);
		Dictionary<string, string> GetMaksuturvaPaymentData(IUserContext context, IOrderFull order);

		MaksuturvaResponseCode GetMaksuturvaPaymentStatus(string channelCommonName, int paymentId);

		string GetCompensationByTimeInterval(string channelCommonName, DateTime fetchDate);
	}
}