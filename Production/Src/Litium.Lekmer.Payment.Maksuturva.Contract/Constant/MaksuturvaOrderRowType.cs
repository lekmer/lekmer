﻿namespace Litium.Lekmer.Payment.Maksuturva
{
	public enum MaksuturvaOrderRowType
	{
		Product = 1,
		PostalCost = 2,
		HandlingCost = 3,
		CustomizedProduct = 4,
		ServicePerformed = 5,
		DiscountMoneyAmount = 6
	}
}