﻿namespace Litium.Lekmer.TwosellExport.Contract
{
	public static class ExporterConstants
	{
		public const string NodeProducts = "products";
		public const string NodeExportDate = "export_date";
		public const string NodeCountries = "countries";
		public const string NodeCountry = "country";
		public const string NodeIso = "iso";
		public const string NodeProduct = "product";
		public const string NodeType = "type";
		public const string NodeId = "id";
		public const string NodeProductId = "product_id";
		public const string NodeBrand = "brand";
		public const string NodeTitle = "title";
		public const string NodePrice = "price";
		public const string NodeLink = "link";
		public const string NodeInStock = "in_stock";
		public const string NodeDeliveryTimeDays = "delivery_time_days";
		public const string NodeDescription = "description";
		public const string NodeEan = "ean";
		public const string NodeArtNo = "artno";
		public const string NodeOriginalPrice = "original_price";
		public const string NodeFreightCost = "freight_cost";
		public const string NodeColor = "color";
		public const string NodeOnPromotion = "onPromotion";
		public const string NodePromotionName = "promotionName";
		public const string NodeMainCategory = "mainCategory";
		public const string NodeCategory = "category";
		public const string NodeSubCategory = "subCategory";
		public const string CustomUrl1 = "customUrl1";
		public const string CategoryUrl = "categoryUrl";
		public const string SubCategoryUrl = "subCategoryUrl";
		public const string NodeAgeFromMonth = "age_from_month";
		public const string NodeAgeToMonth = "age_to_month";
		public const string NodeSizeId = "size_id";
		public const string NodeSizeStock = "size_stock";
		public const string NodeImages = "images";
		public const string NodeDefaultImage = "default_image";
		public const string NodeGroup = "group";
		public const string NodeCommonName = "common_name";
		public const string NodeImage = "image";
		public const string NodeOriginal = "original";
		public const string NodeSize = "size";

		public const string NodeCurrency = "currency";

		public const string ValueMissing = "missing";

		public const string AllCategoryLevel = "All";
		public const string FirstCategoryLevel = "1";
		public const string SecondCategoryLevel = "2";
		public const string ThirdCategoryLevel = "3";

		public const string ValueDelimeter = ",";
	}
}
