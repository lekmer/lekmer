﻿using System.ComponentModel;

namespace Litium.Lekmer.TwosellExport.Contract
{
	public enum FieldNames
	{
		[Description("Date")]
		Date = 1,
		[Description("Order Id")]
		OrderId = 2,
		[Description("Product Id")]
		ProductId = 3,
		[Description("Product Erp Id")]
		ProductErpId = 4,
		[Description("Price")]
		Price = 5,
		[Description("Customer IP")]
		CustomerId = 6,
		[Description("Email")]
		Email = 7,
		[Description("Session Id")]
		SessionId = 8
	}
}