﻿using System;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CurrencyValueRange
	{
		private decimal _monetaryValueFrom;
		private decimal _monetaryValueTo;

		public int CurrencyId { get; set; }

		public decimal MonetaryValueFrom
		{
			get
			{
				return _monetaryValueFrom;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}

				_monetaryValueFrom = value;
			}
		}

		public decimal MonetaryValueTo
		{
			get
			{
				return _monetaryValueTo;
			}
			set
			{
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException("value");
				}

				_monetaryValueTo = value;
			}
		}
	}
}