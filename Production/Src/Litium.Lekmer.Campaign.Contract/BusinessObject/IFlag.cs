﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public interface IFlag : IBusinessObjectBase
	{
		int Id { get; set; }
		string Title { get; set; }
		string Class { get; set; }
		int Ordinal { get; set; }
	}
}