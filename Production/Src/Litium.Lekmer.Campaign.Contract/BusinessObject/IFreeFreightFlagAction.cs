﻿namespace Litium.Lekmer.Campaign
{
	public interface IFreeFreightFlagAction : ILekmerProductAction
	{
		LekmerCurrencyValueDictionary ProductPrices { get; set; }
	}
}