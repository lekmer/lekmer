﻿using System;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignToTag
	{
		int Id { get; set; }
		int CampaignId { get; set; }
		DateTime? ProcessingDate { get; set; }
	}
}