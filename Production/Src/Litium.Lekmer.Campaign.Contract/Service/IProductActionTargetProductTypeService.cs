﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Campaign
{
	public interface IProductActionTargetProductTypeService
	{
		Collection<IProductActionTargetProductType> GetAllByCampaign(int campaignId);
	}
}