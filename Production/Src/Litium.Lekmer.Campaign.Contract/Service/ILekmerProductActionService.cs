﻿namespace Litium.Lekmer.Campaign
{
	public interface ILekmerProductActionService
	{
		IProductActionAppliedItem CreateProductActionAppliedItem();
	}
}