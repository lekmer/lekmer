﻿using System.Collections.ObjectModel;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface IConditionTargetProductTypeSecureService
	{
		Collection<IConditionTargetProductType> GetAllByCampaign(int campaignId);
		void Save(ISystemUserFull systemUserFull, ICondition condition);
		void Delete(ISystemUserFull systemUserFull, int conditionId);
	}
}