﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public interface ICampaignLandingPageSecureService
	{
		ICampaignLandingPage Save(ISystemUserFull systemUserFull, ICampaignLandingPage campaignLandingPage, Collection<ITranslationGeneric> webTitleTranslations, Collection<ITranslationGeneric> descriptionTranslations);
		void Delete(ISystemUserFull systemUserFull, int campaignId);
		void DeleteCampaignRegistryCampaignLandingPage(ISystemUserFull systemUserFull, int campaignLandingPageId, IEnumerable<int> campaignRegistryIds);
		ICampaignLandingPage GetByCampaignIdSecure(int campaignId);

		// Translations.
		void SaveTranslation(int campaignLandingPageId, int languageId, string webTitle, string description);
		void DeleteTranslations(int campaignLandingPageId);
		Collection<ITranslationGeneric> GetAllWebTitleTranslations(int campaignLandingPageId);
		Collection<ITranslationGeneric> GetAllDescriptionTranslations(int campaignLandingPageId);
	}
}