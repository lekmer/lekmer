﻿namespace Litium.Lekmer.Campaign
{
	public enum GiftCardViaMailInfoStatus
	{
		ReadyToSend = 1,
		Sent = 2,
		ExceedsQuantity = 3,
		EmptyVoucherCode = 4
	}
}