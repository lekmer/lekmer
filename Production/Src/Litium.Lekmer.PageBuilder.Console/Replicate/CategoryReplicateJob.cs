using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryReplicateJob : JobBase
	{
		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();
		private readonly IContentPageSecureService _contentPageSecureService = IoC.Resolve<IContentPageSecureService>();

		private readonly ILekmerCategorySecureService _categorySecureService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();

		private readonly ICategorySiteStructureRegistrySecureService _categorySiteStructureRegistrySecureService = IoC.Resolve<ICategorySiteStructureRegistrySecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _languageId;
		private readonly int _siteStructureRegistryId;
		private readonly int _siteStructureRegistryIdSource;
		private NodeTreeReplicationIndex _replicationIndex;

		public CategoryReplicateJob(Configuration configuration, ISystemUserFull systemUser, int languageId, int siteStructureRegistryId, int siteStructureRegistryIdSource)
			: base(configuration)
		{
			_systemUser = systemUser;
			_languageId = languageId;
			_siteStructureRegistryId = siteStructureRegistryId;
			_siteStructureRegistryIdSource = siteStructureRegistryIdSource;
		}

		internal override void Initialize()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			_replicationIndex = new NodeTreeReplicationIndex();

			Collection<IContentNode> replicatedNodes = _contentNodeSecureService.GetAllByRegistry(_siteStructureRegistryId);
			_replicationIndex.Initialize(replicatedNodes);
		}

		protected override void ExecuteCore()
		{
			if (Configuration.ReplicateConfiguration == null)
			{
				return;
			}

			Console.WriteLine("-replication categories starts");

			var categories = GetCategoryChildren(null);

			foreach (INode node in categories)
			{
				SaveCategoryContentNodeConnection(node);
			}

			Console.WriteLine("-replication categories done");
		}

		private void SaveCategoryContentNodeConnection(INode node)
		{
			SaveCategoryContentNodeConnection(node.Id);

			var categories = GetCategoryChildren(node.Id);

			if (categories != null)
			{
				foreach (INode subNode in categories)
				{
					SaveCategoryContentNodeConnection(subNode);
				}
			}
		}

		private IEnumerable<INode> GetCategoryChildren(int? parentCategoryId)
		{
			return _categorySecureService.GetTree(parentCategoryId)
				.Where(node => node.ParentId == parentCategoryId);
		}

		private void SaveCategoryContentNodeConnection(int categoryId)
		{
			var categorySiteStructureRegistrySource = _categorySiteStructureRegistrySecureService.GetAllByCategory(categoryId)
				.FirstOrDefault(creg => creg.SiteStructureRegistryId == _siteStructureRegistryIdSource);

			if (categorySiteStructureRegistrySource != null)
			{
				ICategorySiteStructureRegistry replicatedCategorySiteStructureRegistry = _categorySiteStructureRegistrySecureService.Create();

				replicatedCategorySiteStructureRegistry.CategoryId = categoryId;
				replicatedCategorySiteStructureRegistry.SiteStructureRegistryId = _siteStructureRegistryId;

				if (categorySiteStructureRegistrySource.ProductParentContentNodeId.HasValue)
				{
					replicatedCategorySiteStructureRegistry.ProductParentContentNodeId = _replicationIndex.GetReplicatedNodeId(categorySiteStructureRegistrySource.ProductParentContentNodeId.Value);
				}

				if (categorySiteStructureRegistrySource.ProductTemplateContentNodeId.HasValue)
				{
					replicatedCategorySiteStructureRegistry.ProductTemplateContentNodeId = _replicationIndex.GetReplicatedNodeId(categorySiteStructureRegistrySource.ProductTemplateContentNodeId.Value);
				}

				if (replicatedCategorySiteStructureRegistry.ProductParentContentNodeId.HasValue == false || replicatedCategorySiteStructureRegistry.ProductParentContentNodeId.Value <= 0)
				{
					Console.WriteLine("--Warning, some CategorySiteStructureRegistry is not replicated !!!");
				}
				else
				{
					_categorySiteStructureRegistrySecureService.Save(_systemUser, replicatedCategorySiteStructureRegistry);

					UpdateCategoryPageTitleAndUrl(categoryId, replicatedCategorySiteStructureRegistry.ProductParentContentNodeId.Value);
				}
				
			}
		}

		private void UpdateCategoryPageTitleAndUrl(int categoryId, int contentNodeId)
		{
			ICategory category = _categorySecureService.GetById(categoryId);

			IContentPage contentPage = _contentPageSecureService.GetById(contentNodeId);

			if (category == null)
			{
				return;
			}

			if (contentPage == null)
			{
				return;
			}

			string categoryTitle = GetCategoryTitle(category);

			if (contentPage.Title != categoryTitle || contentPage.UrlTitle != UrlCleaner.CleanUp(categoryTitle))
			{
				contentPage.Title = categoryTitle;

				contentPage.UrlTitle = UrlCleaner.CleanUp(categoryTitle);

				_contentPageSecureService.Save(_systemUser, contentPage);
			}
		}

		private string GetCategoryTitle(ICategory category)
		{
			string title = category.Title;

			var translations = _categorySecureService.GetAllTranslationsByCategory(category.Id);
			var translation = translations.FirstOrDefault(t => t.LanguageId == _languageId);
			if (translation != null && !translation.Value.IsNullOrTrimmedEmpty())
			{
				title = translation.Value;
			}

			return title;
		}
	}
}