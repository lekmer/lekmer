﻿using System.Collections.Generic;

namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryConfiguration
	{
		public int ParentContentNodeId { get; set; }
		public int ContentNodeTemplateId { get; set; }
		public int? MasterPageId { get; set; }
		public List<int?> ParentCategoryIdList { get; set; }
		public bool ContentNodeForParentCategories { get; set; }

		public bool ContentNodeForCategoryLevel1 { get; set; }
		public bool ContentNodeForCategoryLevel2 { get; set; }
		public bool ContentNodeForCategoryLevel3 { get; set; }

		public int NewCategoryIdFrom { get; set; }

		public int FilterBlockAreaId { get; set; }
		public int FilterBlockOrdinal { get; set; }
		public int FilterBlockTypeId { get; set; }
		public string FilterBlockTitle { get; set; }

		public int FilterBlockSecondaryTemplateId { get; set; }
		public int FilterBlockTemplateId { get; set; }

		public int FilterBlockSecondaryTemplateId_Leksaker { get; set; }
		public int FilterBlockTemplateId_Leksaker { get; set; }

		public int FilterBlockSecondaryTemplateId_BarnBaby { get; set; }
		public int FilterBlockTemplateId_BarnBaby { get; set; }

		public int FilterBlockSecondaryTemplateId_Barnrum { get; set; }
		public int FilterBlockTemplateId_Barnrum { get; set; }

		public int FilterBlockSecondaryTemplateId_Barnkläder { get; set; }
		public int FilterBlockTemplateId_Barnkläder { get; set; }


		public int RichtextBlockAreaId { get; set; }
		public int RichtextBlockOrdinal { get; set; }
		public int RichtextBlockTypeId { get; set; }

		public int RichtextBlockTemplateId { get; set; }
	}
}