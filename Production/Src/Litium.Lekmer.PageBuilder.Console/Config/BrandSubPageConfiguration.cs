using System;
using Litium.Lekmer.ProductFilter;

namespace Litium.Lekmer.PageBuilder
{
	[Serializable]
	internal class BrandSubPageConfiguration
	{
		public BlockProductFilter SubPageFilterBlockTemplate { get; set; }

		public int Ordinal { get; set; }

		public string Title { get; set; }

		public int TemplateId { get; set; }

		public int MasterTemplateId { get; set; }
	}
}