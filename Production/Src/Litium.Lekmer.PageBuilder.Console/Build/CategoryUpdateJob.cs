﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryUpdateJob : JobBase
	{
		private readonly ILekmerCategorySecureService _categorySecureService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();
		private readonly IContentPageSecureService _contentPageSecureService = IoC.Resolve<IContentPageSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _languageId;
		private readonly int _siteStructureRegistryId;

		public CategoryUpdateJob(Configuration configuration, ISystemUserFull systemUser, int languageId, int siteStructureRegistryId)
			: base(configuration)
		{
			_systemUser = systemUser;
			_languageId = languageId;
			_siteStructureRegistryId = siteStructureRegistryId;
		}

		protected override void ExecuteCore()
		{
			Console.WriteLine("CategoryUpdateJob running)");

			if (Configuration.CategoryConfiguration == null) return;
			if (Configuration.CategoryConfiguration.ParentCategoryIdList == null) return;

			foreach (int? categoryLevel1Id in Configuration.CategoryConfiguration.ParentCategoryIdList) // Categories Level 1
			{
				if (categoryLevel1Id.HasValue == false) continue;

				ICategoryRecord categoryLevel1 = GetCategory(categoryLevel1Id.Value);
				int? contentNodeLevel1Id = GetCategoryContentNodeId(categoryLevel1);
				if (contentNodeLevel1Id.HasValue == false) continue;

				var categoriesLevel2 = GetCategoryChildren(categoryLevel1Id);

				foreach (ICategoryRecord categoryLevel2 in categoriesLevel2) // Categories Level 2
				{
					int? contentNodeLevel2Id = GetCategoryContentNodeId(categoryLevel2);
					if (contentNodeLevel2Id.HasValue == false) continue;

					if (Configuration.CategoryConfiguration.NewCategoryIdFrom > categoryLevel2.Id) continue;

					UpdateCategoryPage(categoryLevel2, contentNodeLevel2Id.Value);

					var categoriesLevel3 = GetCategoryChildren(categoryLevel2.Id);

					foreach (ICategoryRecord categoryLevel3 in categoriesLevel3)
					{
						int? contentNodeLevel3Id = GetCategoryContentNodeId(categoryLevel3);
						if (contentNodeLevel3Id.HasValue == false) continue;

						if (Configuration.CategoryConfiguration.NewCategoryIdFrom > categoryLevel3.Id) continue;

						UpdateCategoryPage(categoryLevel3, contentNodeLevel3Id.Value);
					}
				}
			}
		}


		private ICategoryRecord GetCategory(int categoryId)
		{
			return _categorySecureService.GetCategoryRecordById(categoryId);
		}

		private IEnumerable<ICategoryRecord> GetCategoryChildren(int? parentCategoryId)
		{
			return _categorySecureService.GetTree(parentCategoryId)
				.Where(node => node.ParentId == parentCategoryId)
				.Select(node => _categorySecureService.GetCategoryRecordById(node.Id))
				.OrderBy(category => category.Title);
		}

		private int? GetCategoryContentNodeId(ICategoryRecord category)
		{
			var categorySiteStructureRegistry = category.CategorySiteStructureRegistries.Single(creg => creg.SiteStructureRegistryId == _siteStructureRegistryId);

			return categorySiteStructureRegistry.ProductParentContentNodeId;
		}

		private string GetCategoryTitle(ICategoryRecord category)
		{
			string title = category.Title;

			var translations = _categorySecureService.GetAllTranslationsByCategory(category.Id);
			var translation = translations.FirstOrDefault(t => t.LanguageId == _languageId);
			if (translation != null && !translation.Value.IsNullOrTrimmedEmpty())
			{
				title = translation.Value;
			}

			return title;
		}

		private void UpdateCategoryPage(ICategoryRecord category, int contentNodeId)
		{
			IContentPage contentPage = _contentPageSecureService.GetById(contentNodeId);

			if (contentPage != null)
			{
				if (contentPage.UrlTitle != UrlCleaner.CleanUp(contentPage.Title))
				{
					Console.WriteLine("erp-id: {0}\t page-id: {1}\t title: {2}", category.ErpId, contentPage.Id, contentPage.Title);
					Console.WriteLine(contentPage.UrlTitle);
					Console.WriteLine(UrlCleaner.CleanUp(contentPage.Title));

					Console.WriteLine("Update ? (Yes - y)");

					if (Console.ReadLine() == "y")
					{
						contentPage.UrlTitle = UrlCleaner.CleanUp(contentPage.Title);

						_contentPageSecureService.Save(_systemUser, contentPage);

						Console.WriteLine("Updated");
					}
				}
			}
		}
	}
}