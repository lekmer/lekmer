﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Common;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.PageBuilder
{
	internal class CategoryBuildJob : JobBase
	{
		private readonly ILekmerCategorySecureService _categorySecureService = (ILekmerCategorySecureService)IoC.Resolve<ICategorySecureService>();

		private readonly ICategorySiteStructureRegistrySecureService _categorySiteStructureRegistrySecureService = IoC.Resolve<ICategorySiteStructureRegistrySecureService>();
		private readonly IContentPageSecureService _contentPageSecureService = IoC.Resolve<IContentPageSecureService>();
		private readonly IContentNodeSecureService _contentNodeSecureService = IoC.Resolve<IContentNodeSecureService>();

		private readonly IBlockProductFilterSecureService _blockProductFilterSecureService = IoC.Resolve<IBlockProductFilterSecureService>();
		private readonly IBlockRichTextSecureService _blockRichTextSecureService = IoC.Resolve<IBlockRichTextSecureService>();

		private readonly ISystemUserFull _systemUser;
		private readonly int _languageId;
		private readonly int _siteStructureRegistryId;
		private int _masterPageTemplateId;

		public CategoryBuildJob(Configuration configuration, ISystemUserFull systemUser, int languageId, int siteStructureRegistryId)
			: base(configuration)
		{
			_systemUser = systemUser;
			_languageId = languageId;
			_siteStructureRegistryId = siteStructureRegistryId;
		}

		internal override void Initialize()
		{
			if (Configuration.CategoryConfiguration == null) return;

			if (Configuration.CategoryConfiguration.MasterPageId.HasValue)
			{
				IContentPage masterPage = _contentPageSecureService.GetById(Configuration.CategoryConfiguration.MasterPageId.Value);
				_masterPageTemplateId = masterPage.TemplateId;
			}
			else
			{
				_masterPageTemplateId = Configuration.CategoryConfiguration.ContentNodeTemplateId;
			}
		}

		protected override void ExecuteCore()
		{
			Console.WriteLine("CategoryBuildJob running)");

			if (Configuration.CategoryConfiguration == null) return;
			if (Configuration.CategoryConfiguration.ParentCategoryIdList == null) return;

			foreach (int? categoryLevel1Id in Configuration.CategoryConfiguration.ParentCategoryIdList) // Categories Level 1
			{
				if (categoryLevel1Id.HasValue == false) continue;

				ICategoryRecord categoryLevel1 = GetCategory(categoryLevel1Id.Value);
				int? contentNodeLevel1Id = GetCategoryContentNodeId(categoryLevel1);
				if (contentNodeLevel1Id.HasValue == false) continue;

				var categoriesLevel2 = GetCategoryChildren(categoryLevel1Id);
				int ordinalLevel2 = _contentNodeSecureService.GetAllByParent(contentNodeLevel1Id.Value).Max(cn => cn.Ordinal) + 10;

				foreach (ICategoryRecord categoryLevel2 in categoriesLevel2) // Categories Level 2
				{
					int? contentNodeLevel2Id = GetCategoryContentNodeId(categoryLevel2);
					if (contentNodeLevel2Id.HasValue == false)
					{
						if (Configuration.CategoryConfiguration.ContentNodeForCategoryLevel2 == false) continue;

						if (Configuration.CategoryConfiguration.NewCategoryIdFrom > categoryLevel2.Id) continue;

						Console.WriteLine("categoryLevel2 {0})", categoryLevel2.Id);

						contentNodeLevel2Id = AddCategoryPageLevel2(categoryLevel2, contentNodeLevel1Id.Value, ordinalLevel2);
						ordinalLevel2 += 10;
					}

					var categoriesLevel3 = GetCategoryChildren(categoryLevel2.Id);
					int ordinalLevel3 = _contentNodeSecureService.GetAllByParent(contentNodeLevel2Id.Value).DefaultIfEmpty().Max(cn => cn == null ? 0 : cn.Ordinal) + 10;

					foreach (ICategoryRecord categoryLevel3 in categoriesLevel3)
					{
						int? contentNodeLevel3Id = GetCategoryContentNodeId(categoryLevel3);
						if (contentNodeLevel3Id.HasValue == false)
						{
							if (Configuration.CategoryConfiguration.ContentNodeForCategoryLevel3 == false) continue;

							if (Configuration.CategoryConfiguration.NewCategoryIdFrom > categoryLevel3.Id) continue;

							Console.WriteLine("categoryLevel3 {0})", categoryLevel3.Id);

							AddCategoryPageLevel3(categoryLevel3, contentNodeLevel2Id.Value, ordinalLevel3);
							ordinalLevel3 += 10;
						}
					}
				}
			}
		}


		private ICategoryRecord GetCategory(int categoryId)
		{
			return _categorySecureService.GetCategoryRecordById(categoryId);
		}

		private IEnumerable<ICategoryRecord> GetCategoryChildren(int? parentCategoryId)
		{
			return _categorySecureService.GetTree(parentCategoryId)
				.Where(node => node.ParentId == parentCategoryId)
				.Select(node => _categorySecureService.GetCategoryRecordById(node.Id))
				.OrderBy(category => category.Title);
		}

		private int? GetCategoryContentNodeId(ICategoryRecord category)
		{
			var categorySiteStructureRegistry = category.CategorySiteStructureRegistries.Single(creg => creg.SiteStructureRegistryId == _siteStructureRegistryId);

			return categorySiteStructureRegistry.ProductParentContentNodeId;
		}

		private string GetCategoryTitle(ICategoryRecord category)
		{
			string title = category.Title;

			var translations = _categorySecureService.GetAllTranslationsByCategory(category.Id);
			var translation = translations.FirstOrDefault(t => t.LanguageId == _languageId);
			if (translation != null && !translation.Value.IsNullOrTrimmedEmpty())
			{
				title = translation.Value;
			}

			return title;
		}


		private int AddCategoryPageLevel2(ICategoryRecord category, int parentContentNodeId, int ordinal)
		{
			IContentPage contentPage = AddCategoryPage(category, parentContentNodeId, ordinal);

			SaveCategoryContentNodeConnection(category, contentPage.Id);

			AddFilterBlockToPage(contentPage.Id, category);

			AddRichTextBlockToPage(contentPage.Id, category);

			Console.WriteLine("Level2 erp-id: {0} page-id: {1} title: {2}", category.ErpId, contentPage.Id, contentPage.Title);

			return contentPage.Id;
		}

		private int AddCategoryPageLevel3(ICategoryRecord category, int parentContentNodeId, int ordinal)
		{
			IContentPage contentPage = AddCategoryPage(category, parentContentNodeId, ordinal);

			Console.WriteLine("Level3 erp-id: {0} page-id: {1} title: {2}", category.ErpId, contentPage.Id, contentPage.Title);

			SaveCategoryContentNodeConnection(category, contentPage.Id);

			AddFilterBlockToPage(contentPage.Id, category);

			return contentPage.Id;
		}

		private IContentPage AddCategoryPage(ICategoryRecord category, int parentContentNodeId, int ordinal)
		{
			string title = GetCategoryTitle(category);

			IContentPage contentPage = _contentPageSecureService.Create();
			contentPage.ParentContentNodeId = parentContentNodeId;
			contentPage.ContentNodeTypeId = (int)ContentNodeTypeInfo.Page;
			contentPage.ContentNodeStatusId = (int)ContentNodeStatusInfo.Online;
			contentPage.Title = title;
			contentPage.Ordinal = ordinal;
			contentPage.CommonName = null;
			contentPage.AccessId = (int)AccessInfo.All;
			contentPage.SiteStructureRegistryId = _siteStructureRegistryId;
			contentPage.TemplateId = _masterPageTemplateId;
			contentPage.UrlTitle = UrlCleaner.CleanUp(title);
			contentPage.ContentPageTypeId = 1;
			contentPage.IsMaster = false;
			contentPage.MasterPageId = Configuration.CategoryConfiguration.MasterPageId;

			_contentPageSecureService.Save(_systemUser, contentPage);

			return contentPage;
		}

		private void AddFilterBlockToPage(int contentNodeId, ICategoryRecord category)
		{
			var blockProductFilter = _blockProductFilterSecureService.Create();

			blockProductFilter.ContentNodeId = contentNodeId;
			blockProductFilter.ContentAreaId = Configuration.CategoryConfiguration.FilterBlockAreaId;
			blockProductFilter.BlockTypeId = Configuration.CategoryConfiguration.FilterBlockTypeId;
			blockProductFilter.BlockStatusId = (int)BlockStatusInfo.Online;

			blockProductFilter.Title = Configuration.CategoryConfiguration.FilterBlockTitle;

			if (category.ErpId.StartsWith("C_10")) //Barn och Baby
			{
				blockProductFilter.TemplateId = Configuration.CategoryConfiguration.FilterBlockTemplateId_BarnBaby;
				blockProductFilter.SecondaryTemplateId = Configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_BarnBaby;
			}
			else if (category.ErpId.StartsWith("C_20")) //Inredning - Barnrum
			{
				blockProductFilter.TemplateId = Configuration.CategoryConfiguration.FilterBlockTemplateId_Barnrum;
				blockProductFilter.SecondaryTemplateId = Configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_Barnrum;
			}
			else if (category.ErpId.StartsWith("C_30")) //Leksaker
			{
				blockProductFilter.TemplateId = Configuration.CategoryConfiguration.FilterBlockTemplateId_Leksaker;
				blockProductFilter.SecondaryTemplateId = Configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_Leksaker;
			}
			else if (category.ErpId.StartsWith("C_50")) //Barnkläder
			{
				blockProductFilter.TemplateId = Configuration.CategoryConfiguration.FilterBlockTemplateId_Barnkläder;
				blockProductFilter.SecondaryTemplateId = Configuration.CategoryConfiguration.FilterBlockSecondaryTemplateId_Barnkläder;
			}

			blockProductFilter.DefaultCategoryId = category.Id;

			_blockProductFilterSecureService.Save(_systemUser, blockProductFilter);
		}

		private void AddRichTextBlockToPage(int contentNodeId, ICategoryRecord category)
		{
			string title = category.Title;

			var blockRichText = _blockRichTextSecureService.Create();
			blockRichText.Ordinal = Configuration.CategoryConfiguration.RichtextBlockOrdinal;
			blockRichText.ContentNodeId = contentNodeId;
			blockRichText.ContentAreaId = Configuration.CategoryConfiguration.RichtextBlockAreaId;
			blockRichText.BlockTypeId = Configuration.CategoryConfiguration.RichtextBlockTypeId;
			blockRichText.BlockStatusId = (int)BlockStatusInfo.Online;
			blockRichText.Title = title;
			blockRichText.TemplateId = Configuration.CategoryConfiguration.RichtextBlockTemplateId;
			blockRichText.Content = string.Empty;

			_blockRichTextSecureService.Save(_systemUser, blockRichText);
		}

		private void SaveCategoryContentNodeConnection(ICategoryRecord category, int contentNodeId)
		{
			var categorySiteStructureRegistry = category.CategorySiteStructureRegistries
				.Single(creg => creg.SiteStructureRegistryId == _siteStructureRegistryId);

			categorySiteStructureRegistry.ProductParentContentNodeId = contentNodeId;

			_categorySiteStructureRegistrySecureService.Save(_systemUser, categorySiteStructureRegistry);
		}
	}
}