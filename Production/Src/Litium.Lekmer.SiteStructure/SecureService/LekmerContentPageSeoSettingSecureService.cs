﻿using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Cache;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure
{
	public class LekmerContentPageSeoSettingSecureService : ContentPageSeoSettingSecureService
	{
		public LekmerContentPageSeoSettingSecureService(IAccessValidator accessValidator, ContentPageSeoSettingRepository repository)
			: base(accessValidator, repository)
		{
		}

		public override void Save(ISystemUserFull systemUserFull, IContentPageSeoSetting contentPageSeoSetting)
		{
			base.Save(systemUserFull, contentPageSeoSetting);
			ContentPageFullCache.Instance.Remove(contentPageSeoSetting.Id);
		}

		public override void Delete(ISystemUserFull systemUserFull, int contentNodeId)
		{
			base.Delete(systemUserFull, contentNodeId);
			ContentPageFullCache.Instance.Remove(contentNodeId);
		}
	}
}
