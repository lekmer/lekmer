﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.SiteStructure.Cache
{
	public sealed class BlockSettingCache : ScensumCacheBase<BlockSettingKey, IBlockSetting>
	{
		// Singleton

		private static readonly BlockSettingCache _instance = new BlockSettingCache();

		private BlockSettingCache()
		{
		}

		public static BlockSettingCache Instance
		{
			get { return _instance; }
		}
	}

	public class BlockSettingKey : ICacheKey
	{
		public BlockSettingKey(int blockId)
		{
			BlockId = blockId;
		}

		public int BlockId { get; set; }

		public string Key
		{
			get { return string.Format("block_{0}", BlockId); }
		}
	}
}