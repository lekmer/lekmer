﻿using Litium.Lekmer.SiteStructure.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Repository;

namespace Litium.Lekmer.SiteStructure
{
	public class LekmerContentNodeService : ContentNodeService, ILekmerContentNodeService
	{
		public LekmerContentNodeService(ContentNodeRepository repository, IContentNodeFolderService contentNodeFolderService, IContentNodeShortcutLinkService contentNodeShortcutLinkService, IContentNodeUrlLinkService contentNodeUrlLinkService, IContentPageService contentPageService)
			: base(repository, contentNodeFolderService, contentNodeShortcutLinkService, contentNodeUrlLinkService, contentPageService)
		{
		}

		public virtual ITreeItemsHash GetAllAsTreeItemsHash(IUserContext context)
		{
			return TreeItemsHashCache.Instance.TryGetItem(
				new TreeItemsHashKey(context.Channel.Id),
				delegate
					{
						var hash = IoC.Resolve<ITreeItemsHash>();
						hash.Initialize(GetAllAsTree(context));
						return hash;
					});
		}

		protected override IContentNodeTree GetAllAsTreeCore(IUserContext context)
		{
			var tree = base.GetAllAsTreeCore(context);

			TreeItemsHashCache.Instance.Flush();

			return tree;
		}
	}
}
