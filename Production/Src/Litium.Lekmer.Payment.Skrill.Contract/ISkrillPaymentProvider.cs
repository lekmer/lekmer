﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Payment.Skrill
{
	public interface ISkrillPaymentProvider
	{
		string CreateRequest(IOrderFull order, ICustomerInformation customer, ICurrency currency);
	}
}