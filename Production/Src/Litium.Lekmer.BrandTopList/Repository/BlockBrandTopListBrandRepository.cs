﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListBrandRepository
	{
		protected virtual DataMapperBase<IBlockBrandTopListBrand> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBrandTopListBrand>(dataReader);
		}

		public virtual void Save(IBlockBrandTopListBrand blockBrand)
		{
			if (blockBrand == null)
			{
				throw new ArgumentNullException("blockBrand");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockBrand.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", blockBrand.Brand.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("Position", blockBrand.Position, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListBrandRepository.Save");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandTopListBrandSave]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListBrandRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[lekmer].[pBlockBrandTopListBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IBlockBrandTopListBrand> GetAllByBlockSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListBrandRepository.GetAllByBlockSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandTopListBrandGetAllByBlockSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBlockBrandTopListBrand> GetAllByBlock(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockBrandTopListBrandRepository.GetAllByBlock");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockBrandTopListBrandGetAllByBlock]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}