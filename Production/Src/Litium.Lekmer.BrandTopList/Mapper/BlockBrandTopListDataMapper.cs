﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListDataMapper : DataMapperBase<IBlockBrandTopList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockBrandTopListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();

			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockBrandTopList Create()
		{
			IBlockBrandTopList blockBrandTopList = IoC.Resolve<IBlockBrandTopList>();

			IBlock block = _blockDataMapper.MapRow();
			block.ConvertTo(blockBrandTopList);

			blockBrandTopList.OrderStatisticsDayCount = MapValue<int>("BlockBrandTopList.OrderStatisticsDayCount");
			blockBrandTopList.IncludeAllCategories = MapValue<bool>("BlockBrandTopList.IncludeAllCategories");
			blockBrandTopList.LinkContentNodeId = MapNullableValue<int?>("BlockBrandTopList.LinkContentNodeId");
			blockBrandTopList.Setting = _blockSettingDataMapper.MapRow();

			blockBrandTopList.SetUntouched();

			return blockBrandTopList;
		}
	}
}