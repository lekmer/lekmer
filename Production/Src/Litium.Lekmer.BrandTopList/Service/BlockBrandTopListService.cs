﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Core;

namespace Litium.Lekmer.BrandTopList
{
	public class BlockBrandTopListService : IBlockBrandTopListService
	{
		protected BlockBrandTopListRepository Repository { get; private set; }
		protected IBlockBrandTopListBrandService BlockBrandTopListBrandService { get; private set; }
		protected IBlockBrandTopListCategoryService BlockBrandTopListCategoryService { get; private set; }

		public BlockBrandTopListService(BlockBrandTopListRepository repository,
			IBlockBrandTopListBrandService blockBrandTopListBrandService,
			IBlockBrandTopListCategoryService blockBrandTopListCategoryService)
		{
			Repository = repository;
			BlockBrandTopListBrandService = blockBrandTopListBrandService;
			BlockBrandTopListCategoryService = blockBrandTopListCategoryService;
		}

		public virtual IBlockBrandTopList GetById(IUserContext context, int id)
		{
			return BlockBrandTopListCache.Instance.TryGetItem(
				new BlockBrandTopListKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockBrandTopList GetByIdCore(IUserContext context, int id)
		{
			IBlockBrandTopList blockBrandTopList = Repository.GetById(context.Channel.Language.Id, id);

			blockBrandTopList.BrandTopListBrands = BlockBrandTopListBrandService.GetAllByBlock(context, blockBrandTopList);
			blockBrandTopList.BlockBrandTopListCategory = BlockBrandTopListCategoryService.GetAllByBlock(context, blockBrandTopList);

			blockBrandTopList.SetUntouched();

			return blockBrandTopList;
		}
	}
}