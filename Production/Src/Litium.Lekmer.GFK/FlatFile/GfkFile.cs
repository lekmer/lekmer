﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Litium.Lekmer.GFK.Contract;

namespace Litium.Lekmer.GFK
{
	public class GfkFile
	{
		private readonly string _defaulttabDelimiter = "\\t";
		private string _delimiter;
		private Dictionary<string, IGfkInfo> _gfkInfoCollection;
		private string _currency;

		private bool _isInitialized;

		public void Initialize(IGfkSetting setting, Dictionary<string, IGfkInfo> gfkInfoCollection, string currency)
		{
			_delimiter = setting.Delimiter;
			_gfkInfoCollection = gfkInfoCollection;
			_currency = currency;

			_isInitialized = true;
		}

		public void Save(Stream savedSiteMap)
		{
			if (_isInitialized == false)
			{
				throw new InvalidOperationException("Initialize(...) and should be called first.");
			}

			var writer = new StreamWriter(savedSiteMap);

			foreach (var gfkInfo in _gfkInfoCollection.Values)
			{
				writer.WriteLine(AddRow(gfkInfo));
			}

			writer.Flush();
			writer.Close();
		}

		protected string AddRow(IGfkInfo gfkInfo)
		{
			string row = string.Empty;

			// Period
			AddValue(ref row, gfkInfo.Period);
			// GroupNo
			AddValue(ref row, gfkInfo.GroupNo);
			// GroupText
			AddValue(ref row, gfkInfo.GroupText);
			// ArticleText
			AddValue(ref row, gfkInfo.ArticleText);
			// Brand
			AddValue(ref row, gfkInfo.Brand);
			// ArticleNo
			AddValue(ref row, gfkInfo.ArticleNo);
			// ArticleNoUniversal
			AddValue(ref row, gfkInfo.ArticleNoUniversal);
			// ShopNo
			AddValue(ref row, gfkInfo.ShopNo);
			// SalesUnits
			AddValue(ref row, gfkInfo.SalesUnits.ToString(CultureInfo.InvariantCulture));
			// SalesValue
			var salesValue = gfkInfo.SalesValue.ToString(CultureInfo.InvariantCulture) + " " + _currency;
			AddValue(ref row, salesValue);
			// StockUnits
			AddValue(ref row, gfkInfo.StockUnits.ToString(CultureInfo.InvariantCulture));

			int lastDelimiterPosition = row.LastIndexOf(_delimiter, StringComparison.Ordinal);
			if (lastDelimiterPosition > 0)
			{
				row = row.Substring(0, lastDelimiterPosition);
			}

			if (_delimiter == _defaulttabDelimiter)
			{
				row = row.Replace("\\t", "\t");
			}
			return row;
		}

		protected void AddValue(ref string row, string value)
		{
			row += value + _delimiter;
		}
	}
}