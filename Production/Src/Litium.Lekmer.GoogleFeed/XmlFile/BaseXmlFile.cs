﻿using System.Xml;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.GoogleFeed
{
	public class BaseXmlFile
	{
		protected virtual XmlDocument CreateXmlDocument()
		{
			var doc = new XmlDocument();

			XmlNode docNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
			doc.AppendChild(docNode);

			return doc;
		}

		protected virtual XmlNode AddNode(XmlDocument doc, XmlNode parent, string name)
		{
			return AddNode(doc, parent, name, string.Empty, string.Empty);
		}
		protected virtual XmlNode AddNode(XmlDocument doc, XmlNode parent, string name, string prefix, string namespaceUri)
		{
			XmlNode node = doc.CreateElement(prefix, name, namespaceUri);
			parent.AppendChild(node);
			return node;
		}


		protected virtual XmlNode AddNode(XmlDocument doc, XmlNode parent, string name, string value)
		{
			return AddNodeRaw(doc, parent, name, value.StripIllegalXmlCharacters());
		}


		protected virtual XmlNode AddNodeRaw(XmlDocument doc, XmlNode parent, string name, string value)
		{
			return AddNodeRaw(doc, parent, name, value, string.Empty, string.Empty);
		}
		protected virtual XmlNode AddNodeRaw(XmlDocument doc, XmlNode parent, string name, string value, string prefix, string namespaceUri)
		{
			XmlNode node = doc.CreateElement(prefix, name, namespaceUri);
			node.AppendChild(doc.CreateTextNode(value));
			parent.AppendChild(node);
			return node;
		}


		protected virtual XmlNode AddCDataNode(XmlDocument doc, XmlNode parent, string name, string value)
		{
			return AddCDataNode(doc, parent, name, value, string.Empty, string.Empty);
		}
		protected virtual XmlNode AddCDataNode(XmlDocument doc, XmlNode parent, string name, string value, string prefix, string namespaceUri)
		{
			XmlNode node = doc.CreateElement(prefix, name, namespaceUri);
			node.AppendChild(doc.CreateCDataSection(value.StripIllegalXmlCharacters()));
			parent.AppendChild(node);
			return node;
		}


		protected virtual void AddAttribute(XmlDocument xmlDocument, XmlNode parentNode, string elementName, string elementValue)
		{
			XmlAttribute attribute = xmlDocument.CreateAttribute(elementName);
			attribute.Value = elementValue;
			if (parentNode.Attributes != null)
			{
				parentNode.Attributes.Append(attribute);
			}
		}
	}
}