﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.GoogleFeed
{
	public class GoogleFeedSetting : SettingBase, IGoogleFeedSetting
	{
		protected override string StorageName
		{
			get { return "GoogleFeed"; }
		}
		protected virtual string GroupName
		{
			get { return "GoogleFeedSettings"; }
		}

		public virtual string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}
		public virtual string ChannelDeliveryMethod
		{
			get { return GetString(GroupName, "ChannelDeliveryMethod"); }
		}
		public string DestinationDirectoryPath
		{
			get { return GetString(GroupName, "DestinationDirectoryPath"); }
		}
		public string XmlFileName
		{
			get { return GetString(GroupName, "XmlFileName"); }
		}
		public string ZipFileExtension
		{
			get { return GetString(GroupName, "ZipFileExtension"); }
		}
		public int PageSize
		{
			get { return GetInt32(GroupName, "PageSize", 100); }
		}
		public string GoogleFeedTitle
		{
			get { return GetString(GroupName, "GoogleFeedTitle"); }
		}
		public string GoogleFeedLink
		{
			get { return GetString(GroupName, "GoogleFeedLink"); }
		}
		public string GoogleFeedDescription
		{
			get { return GetString(GroupName, "GoogleFeedDescription"); }
		}

		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);
			if (dictionary.ContainsKey(channelName))
			{
				return dictionary[channelName];
			}

			return channelName;
		}
		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames);
			if (dictionary.ContainsKey(channelName))
			{
				return true;
			}

			return false;
		}
		public string GetChannelDeliveryMethod(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ChannelDeliveryMethod);
			if (dictionary.ContainsKey(channelName))
			{
				return dictionary[channelName];
			}

			return channelName;
		}

		// value1,value2, ... , valueN => List<value>
		protected virtual ICollection<string> ConvertToList(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			var dictionary = new Dictionary<string, string>();

			ICollection<string> pairs = ConvertToList(textValue);
			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}
	}
}