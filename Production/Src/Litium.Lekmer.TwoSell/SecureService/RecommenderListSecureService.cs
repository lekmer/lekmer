﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.TwoSell.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwoSell
{
	public class RecommenderListSecureService : IRecommenderListSecureService
	{
		protected RecommenderListRepository Repository { get; private set; }
		protected IRecommenderItemSecureService RecommenderItemSecureService { get; private set; }

		public RecommenderListSecureService(
			RecommenderListRepository repository,
			IRecommenderItemSecureService recommenderItemSecureService
		)
		{
			Repository = repository;
			RecommenderItemSecureService = recommenderItemSecureService;
		}

		public IRecommenderList Create()
		{
			var recommenderList = IoC.Resolve<IRecommenderList>();

			recommenderList.Items = new Collection<IRecommenderItem>();

			return recommenderList;
		}

		public virtual IRecommenderList Save(IRecommenderList recommenderList)
		{
			if (recommenderList == null)
			{
				throw new ArgumentNullException("recommenderList");
			}

			Repository.EnsureNotNull();

			using (var transaction = new TransactedOperation())
			{
				int recommenderListId = Repository.Save(recommenderList);
				recommenderList.Id = recommenderListId;

				if (recommenderListId > 0)
				{
					foreach (IRecommenderItem recommenderItem in recommenderList.Items)
					{
						recommenderItem.RecommenderListId = recommenderListId;
					}

					RecommenderItemSecureService.Save(recommenderList.Items);
				}

				transaction.Complete();
			}

			return recommenderList;
		}

		public virtual IRecommenderList GetBySessionAndProduct(string sessionId, int? productId)
		{
			Repository.EnsureNotNull();

			return Repository.GetBySessionAndProduct(sessionId, productId);
		}
	}
}