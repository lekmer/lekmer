﻿using Litium.Lekmer.Product;
using Litium.Lekmer.TwoSell.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.TwoSell
{
	public class RecommenderProductService : IRecommenderProductService
	{
		protected RecommenderProductRepository Repository { get; private set; }
		protected ILekmerProductService ProductService { get; private set; }

		public RecommenderProductService(
			RecommenderProductRepository repository,
			ILekmerProductService productService

		)
		{
			Repository = repository;
			ProductService = productService;
		}

		public ProductCollection GetProductsBySessionAndProduct(IUserContext context, string sessionId, int productId, int itemsToReturn)
		{
			ProductIdCollection productIdCollection = Repository.GetRecommendationProductIdBySessionAndProduct(sessionId, productId, itemsToReturn);

			ProductCollection productCollection = ProductService.PopulateProducts(context, productIdCollection);

			return productCollection;
		}
	}
}