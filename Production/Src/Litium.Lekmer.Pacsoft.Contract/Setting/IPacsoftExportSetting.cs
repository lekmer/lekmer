﻿namespace Litium.Lekmer.Pacsoft.Contract
{
	public interface IPacsoftExportSetting
	{
		string DestinationDirectory { get; }
		string FileName { get; }
		string Encoding { get; }

		string ProductErpId { get; }
		string SupplierId { get; }
		int DaysAfterPurchase { get; }
		string QuickId { get; }
		string RcvId { get; }
		string Srvid { get; }
		string SmsFrom { get; }
		string Contents { get; }

		string FtpUrl { get; }
		string Username { get; }
		string Password { get; }

		string ReplaceChannelNames { get; }

		bool GetChannelNameReplacementExists(string channelName);
		string GetChannelNameReplacement(string channelName);
		string GetChannelUserNameReplacement(string channelName);
		string GetChannelPasswordReplacement(string channelName);
	}
}