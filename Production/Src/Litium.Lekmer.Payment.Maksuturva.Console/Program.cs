﻿using System;
using System.Diagnostics;
using System.Reflection;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Payment.Maksuturva.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.InfoFormat("Maksuturva.Console started.");

				var exporter = IoC.Resolve<IExporter>("SettlementsDibsExporter");
				exporter.Execute();
				_log.InfoFormat("Maksuturva.Console completed.");

				stopwatch.Stop();
				_log.InfoFormat("Maksuturva.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch (Exception ex)
			{
				_log.Error("Maksuturva.Console failed.", ex);
			}
		}
	}
}