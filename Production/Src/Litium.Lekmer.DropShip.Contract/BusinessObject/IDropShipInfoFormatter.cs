﻿namespace Litium.Lekmer.DropShip
{
	public interface IDropShipInfoFormatter
	{
		IDropShipInfo DropShipInfoSource { get; set; }

		string Id { get; }
		string ExternalOrderId { get; }
		string OfferId { get; }
		string Email { get; }
		string Phone { get; }
		string SocialSecurityNumber { get; }
		string BillingFirstName { get; }
		string BillingLastName { get; }
		string BillingCompany { get; }
		string BillingCo { get; }
		string BillingStreet { get; }
		string BillingStreetNr { get; }
		string BillingEntrance { get; }
		string BillingFloor { get; }
		string BillingApartmentNumber { get; }
		string BillingZip { get; }
		string BillingCity { get; }
		string BillingCountry { get; }
		string DeliveryFirstName { get; }
		string DeliveryLastName { get; }
		string DeliveryCompany { get; }
		string DeliveryCo { get; }
		string DeliveryStreet { get; }
		string DeliveryStreetNr { get; }
		string DeliveryFloor { get; }
		string DeliveryApartmentNumber { get; }
		string DeliveryEntrance { get; }
		string DeliveryZip { get; }
		string DeliveryCity { get; }
		string DeliveryCountry { get; }
		string Discount { get; }
		string Comment { get; }
		string SupplierNo { get; }
		string OrderId { get; }
		string OrderDate { get; }
		string CustomerNumber { get; }
		string NotificationNumber { get; }
		string PaymentMethod { get; }
		string ProductArticleNumber { get; }
		string ProductTitle { get; }
		string Quantity { get; }
		string Price { get; }
		string ProductDiscount { get; }
		string ProductSoldSum { get; }
		string ProductStatus { get; }
		string InvoiceDate { get; }
		string VatProc { get; }
	}
}