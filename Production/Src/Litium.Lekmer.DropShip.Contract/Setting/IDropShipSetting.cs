﻿namespace Litium.Lekmer.DropShip
{
	public interface IDropShipSetting
	{
		string IncomingDirectory { get; }
		string ProcessingDirectory { get; }
		string DoneDirectory { get; }

		string DestinationDirectory { get; }
		string LogoPath { get; }
		string CsvFileName { get; }
		string PdfFileName { get; }
		string Delimiter { get; }

		string FromName { get; }
		string FromEmail { get; }
		string BccEmail { get; }
		string MailSubject { get; }
		string MailSubjectSecond { get; }
		string MailBody { get; }
		string MailBodySecond { get; }
		bool EnableSsl { get; }
	}
}