﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.DropShip
{
	public class DropShipInfoDataMapper : DataMapperBase<IDropShipInfo>
	{
		public DropShipInfoDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IDropShipInfo Create()
		{
			var dropShipInfo = IoC.Resolve<IDropShipInfo>();
			dropShipInfo.Id = MapValue<int>("DropShip.Id");
			dropShipInfo.ExternalOrderId = MapNullableValue<string>("DropShip.ExternalOrderId");
			dropShipInfo.OfferId = MapNullableValue<string>("DropShip.OfferId");
			dropShipInfo.Email = MapNullableValue<string>("DropShip.Email");
			dropShipInfo.Phone = MapNullableValue<string>("DropShip.Phone");
			dropShipInfo.SocialSecurityNumber = MapNullableValue<string>("DropShip.SocialSecurityNumber");
			dropShipInfo.BillingFirstName = MapNullableValue<string>("DropShip.BillingFirstName");
			dropShipInfo.BillingLastName = MapNullableValue<string>("DropShip.BillingLastName");
			dropShipInfo.BillingCompany = MapNullableValue<string>("DropShip.BillingCompany");
			dropShipInfo.BillingCo = MapNullableValue<string>("DropShip.BillingCo");
			dropShipInfo.BillingStreet = MapNullableValue<string>("DropShip.BillingStreet");
			dropShipInfo.BillingStreetNr = MapNullableValue<string>("DropShip.BillingStreetNr");
			dropShipInfo.BillingEntrance = MapNullableValue<string>("DropShip.BillingEntrance");
			dropShipInfo.BillingFloor = MapNullableValue<string>("DropShip.BillingFloor");
			dropShipInfo.BillingApartmentNumber = MapNullableValue<string>("DropShip.BillingApartmentNumber");
			dropShipInfo.BillingZip = MapNullableValue<string>("DropShip.BillingZip");
			dropShipInfo.BillingCity = MapNullableValue<string>("DropShip.BillingCity");
			dropShipInfo.BillingCountry = MapNullableValue<string>("DropShip.BillingCountry");
			dropShipInfo.DeliveryFirstName = MapNullableValue<string>("DropShip.DeliveryFirstName");
			dropShipInfo.DeliveryLastName = MapNullableValue<string>("DropShip.DeliveryLastName");
			dropShipInfo.DeliveryCompany = MapNullableValue<string>("DropShip.DeliveryCompany");
			dropShipInfo.DeliveryCo = MapNullableValue<string>("DropShip.DeliveryCo");
			dropShipInfo.DeliveryStreet = MapNullableValue<string>("DropShip.DeliveryStreet");
			dropShipInfo.DeliveryStreetNr = MapNullableValue<string>("DropShip.DeliveryStreetNr");
			dropShipInfo.DeliveryFloor = MapNullableValue<string>("DropShip.DeliveryFloor");
			dropShipInfo.DeliveryEntrance = MapNullableValue<string>("DropShip.DeliveryEntrance");
			dropShipInfo.DeliveryZip = MapNullableValue<string>("DropShip.DeliveryZip");
			dropShipInfo.DeliveryCity = MapNullableValue<string>("DropShip.DeliveryCity");
			dropShipInfo.DeliveryCountry = MapNullableValue<string>("DropShip.DeliveryCountry");
			dropShipInfo.Discount = MapNullableValue<string>("DropShip.Discount");
			dropShipInfo.Comment = MapNullableValue<string>("DropShip.Comment");
			dropShipInfo.SupplierNo = MapNullableValue<string>("DropShip.SupplierNo");
			dropShipInfo.OrderId = MapNullableValue<string>("DropShip.OrderId");
			dropShipInfo.OrderDate = MapNullableValue<string>("DropShip.OrderDate");
			dropShipInfo.CustomerNumber = MapNullableValue<string>("DropShip.CustomerNumber");
			dropShipInfo.NotificationNumber = MapNullableValue<string>("DropShip.NotificationNumber");
			dropShipInfo.PaymentMethod = MapNullableValue<string>("DropShip.PaymentMethod");
			dropShipInfo.ProductArticleNumber = MapNullableValue<string>("DropShip.ProductArticleNumber");
			dropShipInfo.ProductTitle = MapNullableValue<string>("DropShip.ProductTitle");
			dropShipInfo.Quantity = MapNullableValue<string>("DropShip.Quantity");
			dropShipInfo.Price = MapNullableValue<string>("DropShip.Price");
			dropShipInfo.ProductDiscount = MapNullableValue<string>("DropShip.ProductDiscount");
			dropShipInfo.ProductSoldSum = MapNullableValue<string>("DropShip.ProductSoldSum");
			dropShipInfo.ProductStatus = MapNullableValue<string>("DropShip.ProductStatus");
			dropShipInfo.InvoiceDate = MapNullableValue<string>("DropShip.InvoiceDate");
			dropShipInfo.VatProc = MapNullableValue<string>("DropShip.VATproc");
			dropShipInfo.CreatedDate = MapValue<DateTime>("DropShip.CreatedDate");
			dropShipInfo.SendDate = MapNullableValue<DateTime?>("DropShip.SendDate");
			dropShipInfo.CsvFileName = MapNullableValue<string>("DropShip.CsvFileName");
			dropShipInfo.PdfFileName = MapNullableValue<string>("DropShip.PdfFileName");
			return dropShipInfo;
		}
	}
}