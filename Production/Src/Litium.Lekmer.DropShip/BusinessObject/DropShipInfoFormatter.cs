using System.Globalization;

namespace Litium.Lekmer.DropShip
{
	public class DropShipInfoFormatter : IDropShipInfoFormatter
	{
		public IDropShipInfo DropShipInfoSource { get; set; }

		public string Id
		{
			get { return DropShipInfoSource.Id.ToString(CultureInfo.InvariantCulture); }
		}

		public string ExternalOrderId
		{
			get { return DropShipInfoSource.ExternalOrderId; }
		}
		public string OfferId
		{
			get { return DropShipInfoSource.OfferId ?? string.Empty; }
		}
		public string Email
		{
			get { return DropShipInfoSource.Email ?? string.Empty; }
		}
		public string Phone
		{
			get { return DropShipInfoSource.Phone ?? string.Empty; }
		}
		public string SocialSecurityNumber
		{
			get { return DropShipInfoSource.SocialSecurityNumber ?? string.Empty; }
		}
		public string BillingFirstName
		{
			get { return DropShipInfoSource.BillingFirstName ?? string.Empty; }
		}
		public string BillingLastName
		{
			get { return DropShipInfoSource.BillingLastName ?? string.Empty; }
		}
		public string BillingCompany
		{
			get { return DropShipInfoSource.BillingCompany ?? string.Empty; }
		}
		public string BillingCo
		{
			get { return DropShipInfoSource.BillingCo ?? string.Empty; }
		}
		public string BillingStreet
		{
			get { return DropShipInfoSource.BillingStreet ?? string.Empty; }
		}
		public string BillingStreetNr
		{
			get { return DropShipInfoSource.BillingStreetNr ?? string.Empty; }
		}
		public string BillingEntrance
		{
			get { return DropShipInfoSource.BillingEntrance ?? string.Empty; }
		}
		public string BillingFloor
		{
			get { return DropShipInfoSource.BillingFloor ?? string.Empty; }
		}
		public string BillingApartmentNumber
		{
			get { return DropShipInfoSource.BillingApartmentNumber ?? string.Empty; }
		}
		public string BillingZip
		{
			get { return DropShipInfoSource.BillingZip ?? string.Empty; }
		}
		public string BillingCity
		{
			get { return DropShipInfoSource.BillingCity ?? string.Empty; }
		}
		public string BillingCountry
		{
			get { return DropShipInfoSource.BillingCountry ?? string.Empty; }
		}
		public string DeliveryFirstName
		{
			get { return DropShipInfoSource.DeliveryFirstName ?? string.Empty; }
		}
		public string DeliveryLastName
		{
			get { return DropShipInfoSource.DeliveryLastName ?? string.Empty; }
		}
		public string DeliveryCompany
		{
			get { return DropShipInfoSource.DeliveryCompany ?? string.Empty; }
		}
		public string DeliveryCo
		{
			get { return DropShipInfoSource.DeliveryCo ?? string.Empty; }
		}
		public string DeliveryStreet
		{
			get { return DropShipInfoSource.DeliveryStreet ?? string.Empty; }
		}
		public string DeliveryStreetNr
		{
			get { return DropShipInfoSource.DeliveryStreetNr ?? string.Empty; }
		}
		public string DeliveryFloor
		{
			get { return DropShipInfoSource.DeliveryFloor ?? string.Empty; }
		}
		public string DeliveryApartmentNumber
		{
			get { return DropShipInfoSource.DeliveryApartmentNumber ?? string.Empty; }
		}
		public string DeliveryEntrance
		{
			get { return DropShipInfoSource.DeliveryEntrance ?? string.Empty; }
		}
		public string DeliveryZip
		{
			get { return DropShipInfoSource.DeliveryZip ?? string.Empty; }
		}
		public string DeliveryCity
		{
			get { return DropShipInfoSource.DeliveryCity ?? string.Empty; }
		}
		public string DeliveryCountry
		{
			get { return DropShipInfoSource.DeliveryCountry ?? string.Empty; }
		}
		public string Discount
		{
			get { return DropShipInfoSource.Discount ?? string.Empty; }
		}
		public string Comment
		{
			get { return DropShipInfoSource.Comment ?? string.Empty; }
		}
		public string SupplierNo
		{
			get { return DropShipInfoSource.SupplierNo ?? string.Empty; }
		}
		public string OrderId
		{
			get { return DropShipInfoSource.OrderId ?? string.Empty; }
		}
		public string OrderDate
		{
			get { return DropShipInfoSource.OrderDate ?? string.Empty; }
		}
		public string CustomerNumber
		{
			get { return DropShipInfoSource.CustomerNumber ?? string.Empty; }
		}
		public string NotificationNumber
		{
			get { return DropShipInfoSource.NotificationNumber ?? string.Empty; }
		}
		public string PaymentMethod
		{
			get { return DropShipInfoSource.PaymentMethod ?? string.Empty; }
		}
		public string ProductArticleNumber
		{
			get { return DropShipInfoSource.ProductArticleNumber ?? string.Empty; }
		}
		public string ProductTitle
		{
			get
			{
				string title = DropShipInfoSource.ProductTitle ?? string.Empty;
				if (title != null && title.Length > 40)
				{
					title = title.Substring(0, 40);
				}
				return title;
			}
		}
		public string Quantity
		{
			get { return DropShipInfoSource.Quantity ?? string.Empty; }
		}
		public string Price
		{
			get { return DropShipInfoSource.Price ?? string.Empty; }
		}
		public string ProductDiscount
		{
			get { return DropShipInfoSource.ProductDiscount ?? string.Empty; }
		}
		public string ProductSoldSum
		{
			get { return DropShipInfoSource.ProductSoldSum ?? string.Empty; }
		}
		public string ProductStatus
		{
			get { return DropShipInfoSource.ProductStatus ?? string.Empty; }
		}
		public string InvoiceDate
		{
			get { return DropShipInfoSource.InvoiceDate ?? string.Empty; }
		}
		public string VatProc
		{
			get { return DropShipInfoSource.VatProc ?? string.Empty; }
		}
	}
}