﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.DropShip
{
	public class DropShipExport : ITask
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IDropShipService DropShipService { get; private set; }
		protected IDropShipSharedService DropShipSharedService { get; private set; }

		public DropShipExport(IDropShipService dropShipService, IDropShipSharedService dropShipSharedService)
		{
			DropShipService = dropShipService;
			DropShipSharedService = dropShipSharedService;
		}

		public void Execute()
		{
			_log.Info("DropShipExport started.");
			ExecuteDropShipExport();
			_log.Info("DropShipExport complited.");
		}

		protected virtual void ExecuteDropShipExport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			_log.Info("Get DropShip data...");
			var dropShipCollection = DropShipService.GetAllNotSent();
			if (dropShipCollection.Count <= 0)
			{
				stopwatch.Stop();
				_log.Info("There are no drop ship data to send");
				return;
			}

			var supplierNotSent = new Collection<string>();
			var groupedBySupplier = dropShipCollection.GroupBy(ds => ds.SupplierNo).ToDictionary(ds => ds.Key, ds => new Collection<IDropShipInfo>(ds.ToArray()));
			foreach (var item in groupedBySupplier)
			{
				var csvFilePath = DropShipSharedService.SaveCsvFile(item);
				_log.InfoFormat("CSV file saved... //{0}", csvFilePath);

				var pdfFilePath = DropShipSharedService.SavePdfFile(item);
				_log.InfoFormat("PDF file saved... //{0}", pdfFilePath);

				try
				{
					DropShipSharedService.SendMessage(item.Key, csvFilePath, pdfFilePath, false);
					foreach (var dropShipInfo in item.Value)
					{
						dropShipInfo.CsvFileName = Path.GetFileName(csvFilePath);
						dropShipInfo.PdfFileName = Path.GetFileName(pdfFilePath);
					}
				}
				catch (Exception ex)
				{
					supplierNotSent.Add(item.Key);
					_log.Fatal(string.Format(CultureInfo.InvariantCulture, "An error occured when trying to send message to supplier '{0}'.\r\n {1}", item.Key, ex.Message), ex);
				}
			}

			// Update DropShip send date.
			foreach (var dropShipInfo in dropShipCollection)
			{
				if (supplierNotSent.Contains(dropShipInfo.SupplierNo)) continue;
				DropShipService.Update(dropShipInfo.Id, dropShipInfo.CsvFileName, dropShipInfo.PdfFileName);
			}

			stopwatch.Stop();
			_log.InfoFormat("Elapsed time {0}", stopwatch.Elapsed);
		}
	}
}