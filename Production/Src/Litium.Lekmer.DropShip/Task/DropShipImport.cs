﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.DropShip
{
	public class DropShipImport : ITask
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IDropShipSetting Setting { get; private set; }
		protected IDropShipService DropShipService { get; private set; }

		public DropShipImport(IDropShipSetting setting, IDropShipService dropShipService)
		{
			Setting = setting;
			DropShipService = dropShipService;
		}

		public void Execute()
		{
			_log.Info("DropShipImport started.");
			ExecuteDropShipImport();
			_log.Info("DropShipImport complited.");
		}

		protected virtual void ExecuteDropShipImport()
		{
			Stopwatch stopwatch = Stopwatch.StartNew();

			var processingDir = Setting.ProcessingDirectory;
			var incomingDir = Setting.IncomingDirectory;
			var doneDir = Setting.DoneDirectory;

			if (IsAllowImport(processingDir) && IsNewFilesExist(incomingDir))
			{
				var files = GetFiles(incomingDir);
				CopyAllFiles(files, processingDir);
				ImportData(files);
				DeleteFiles(files);
				MoveAllFiles(processingDir, doneDir);
			}

			stopwatch.Stop();
			_log.InfoFormat("Elapsed time {0}", stopwatch.Elapsed);
		}

		protected virtual bool IsAllowImport(string sourcePath)
		{
			var files = Directory.GetFiles(sourcePath);
			if (files.Length == 0) return true;

			try
			{
				foreach (var file in files)
				{
					if (IsFileUsed(file))
					{
						return false;
					}
				}
				return true;
			}
			catch (Exception ex)
			{
				_log.Error(ex);
				return false;
			}
		}

		protected virtual bool IsNewFilesExist(string sourcePath)
		{
			var files = Directory.GetFiles(sourcePath);
			if (files.Length == 0) return false;

			try
			{
				foreach (var file in files)
				{
					if (IsFileUsed(file))
					{
						return false;
					}
				}
				return true;
			}
			catch (Exception ex)
			{
				_log.Error(ex);
				return false;
			}
		}

		protected virtual bool IsFileUsed(string file)
		{
			try
			{
				using (new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None))
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
				return true;
			}
		}

		protected virtual Collection<string> GetFiles(string sourcePath)
		{
			var filesToImport = new Collection<string>();

			var files = Directory.GetFiles(sourcePath);
			if (files.Length == 0) return filesToImport;

			try
			{
				foreach (var file in files)
				{
					if (IsFileUsed(file)) continue;
					filesToImport.Add(file);
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}

			return filesToImport;
		}

		protected virtual void CopyAllFiles(Collection<string> files, string targetPath)
		{
			CreateDirectory(targetPath);
			try
			{
				foreach (var file in files)
				{
					var fileName = Path.GetFileName(file);
					var destFile = Path.Combine(targetPath, fileName);
					File.Copy(file, destFile, true);
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		protected virtual void DeleteFiles(Collection<string> files)
		{
			foreach (var file in files)
			{
				if (!File.Exists(file)) continue;

				try
				{
					File.Delete(file);
				}
				catch (Exception ex)
				{
					_log.Error(ex);
				}
			}
		}

		protected virtual void MoveAllFiles(string sourcePath, string targetPath)
		{
			CreateDirectory(targetPath);
			try
			{
				var files = Directory.GetFiles(sourcePath);
				foreach (var file in files)
				{
					var fileName = Path.GetFileName(file);
					var destFile = Path.Combine(targetPath, fileName);
					if (File.Exists(destFile))
					{
						File.Delete(destFile);
					}
					File.Move(file, destFile);
				}
			}
			catch (Exception ex)
			{
				_log.Error(ex);
			}
		}

		protected virtual void CreateDirectory(string path)
		{
			if (!Directory.Exists(path))
			{
				Directory.CreateDirectory(path);
			}
		}

		protected virtual void ImportData(Collection<string> files)
		{
			foreach (var file in files)
			{
				try
				{
					DropShipService.ImportDropShipData(file);
				}
				catch (Exception ex)
				{
					_log.Error(ex);
				}
			}
		}
	}
}