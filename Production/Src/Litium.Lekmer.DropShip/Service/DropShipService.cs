﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.DropShip
{
	public class DropShipService : IDropShipService
	{
		protected DropShipRepository Repository { get; private set; }

		public DropShipService(DropShipRepository repository)
		{
			Repository = repository;
		}

		public virtual void ImportDropShipData(string filePath)
		{
			Repository.ImportDropShipData(filePath);
		}

		public virtual Collection<IDropShipInfo> GetAllNotSent()
		{
			return Repository.GetAllNotSent();
		}

		public Collection<IDropShipInfo> GetAllByOrderAndSupplier(int orderId, string supplierNo)
		{
			return Repository.GetAllByOrderAndSupplier(orderId, supplierNo);
		}

		public virtual void Update(int id, string csvFileName, string pdfFileName)
		{
			Repository.Update(id, csvFileName, pdfFileName);
		}
	}
}