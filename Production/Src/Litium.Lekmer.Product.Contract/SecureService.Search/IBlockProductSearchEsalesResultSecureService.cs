﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductSearchEsalesResultSecureService
	{
		IBlockProductSearchEsalesResult Create();

		int Save(ISystemUserFull systemUserFull, IBlockProductSearchEsalesResult block);

		IBlockProductSearchEsalesResult GetById(int blockId);
	}
}