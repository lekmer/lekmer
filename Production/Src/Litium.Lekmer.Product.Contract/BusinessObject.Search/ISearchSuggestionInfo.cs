namespace Litium.Lekmer.Product
{
	public interface ISearchSuggestionInfo
	{
		string Text { get; set; }
		string Ticket { get; set; }
	}
}