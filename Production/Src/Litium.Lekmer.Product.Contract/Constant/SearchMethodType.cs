namespace Litium.Lekmer.Product
{
	public enum SearchMethodType
	{
		None = 0,
		Regular = 1,
		Filter = 2
	}
}