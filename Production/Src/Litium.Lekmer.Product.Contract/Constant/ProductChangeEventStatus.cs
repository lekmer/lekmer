﻿namespace Litium.Lekmer.Product
{
	public enum ProductChangeEventStatus
	{
		InQueue = 0,
		InProgress = 1,
		ActionApplied = 2
	}
}