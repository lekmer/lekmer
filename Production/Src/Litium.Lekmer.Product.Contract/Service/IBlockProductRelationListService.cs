﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductRelationListService
	{
		IBlockProductRelationList GetById(IUserContext context, int id);
	}
}
