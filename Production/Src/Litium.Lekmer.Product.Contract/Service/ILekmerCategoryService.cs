﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerCategoryService : ICategoryService
	{
		Collection<ICategoryView> GetViewByIdAll(IUserContext context, int categoryId);
		Collection<ICategoryView> GetViewAllByBlock(IUserContext context, IBlockCategoryProductList block);
	}
}