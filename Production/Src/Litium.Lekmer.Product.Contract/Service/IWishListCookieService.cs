﻿using System;

namespace Litium.Lekmer.Product
{
	public interface IWishListCookieService
	{
		Guid CreateNewWishListKey();
		Guid GetWishListKey();
		string SetWishListKey(Guid wishListKey);
		void RemoveWishListKey();
	}
}