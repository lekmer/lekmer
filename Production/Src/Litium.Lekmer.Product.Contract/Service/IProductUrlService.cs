using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductUrlService
	{
		int? GetIdByUrlTitle(IUserContext context, string urlTitle);
	}
}
