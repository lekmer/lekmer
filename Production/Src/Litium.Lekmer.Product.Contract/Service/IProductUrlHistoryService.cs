using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductUrlHistoryService
	{
		Collection<IProductUrlHistory> GetAllByUrlTitle(IUserContext context, string urlTitle);

		Collection<IProductUrlHistory> GetAllByLanguage(IUserContext context);

		void Update(int productUrlHistoryId);

		void DeleteExpiredItems();

		void CleanUp(int maxCount);
	}
}