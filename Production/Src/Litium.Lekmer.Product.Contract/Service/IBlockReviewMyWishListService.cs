﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockReviewMyWishListService
	{
		IBlockReviewMyWishList GetById(IUserContext context, int id);
	}
}

