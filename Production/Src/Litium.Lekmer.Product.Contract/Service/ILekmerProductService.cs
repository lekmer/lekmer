﻿using System.Collections.Generic;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProductService : IProductService
	{
		ILekmerProduct Create();

		// Online products
		IProduct GetById(IUserContext context, int productId, bool allowSellOnlyInPackage);
		IProductView GetViewById(IUserContext context, int productId, bool allowSellOnlyInPackage, bool isNeedEsalesTicket);
		ProductCollection GetAll(IUserContext context, int page, int pageSize);
		ProductCollection GetAllByBlock(IUserContext context, IBlockCategoryProductList block, int pageNumber, int pageSize);
		ProductCollection GetAllByBlock(IUserContext context, IBlockBrandProductList block, int pageNumber, int pageSize, int productSortOrderId);
		ProductCollection GetAllByBlock(IUserContext context, IBlockProductList block, int pageNumber, int pageSize);
		ProductCollection GetAllByBlockAndProduct(IUserContext context, IBlockProductRelationList block, int productId, bool showVariants, int pageNumber, int pageSize);
		ProductCollection GetAllByProductAndRelationType(IUserContext context, int productId, string relationType);
		ProductCollection GetViewAllForExport(IUserContext context, int page, int pageSize);

		ProductCollection PopulateProductsWithOnlyInPackage(IUserContext context, ProductIdCollection productIds);

		ProductCollection PopulateViewProducts(IUserContext context, ProductIdCollection productIds);
		ProductCollection PopulateViewWithOnlyInPackageProducts(IUserContext context, ProductIdCollection productIds);
		ProductCollection PopulateViewProductsForExport(IUserContext context, ProductIdCollection productIds);

		ProductCollection SupplementProductsWithSizes(IEnumerable<ILekmerProductView> products);

		ProductIdCollection GetVariantIdAllByProduct(int productId);

		// Products without status filter
		IProduct GetByIdWithoutStatusFilter(IUserContext context, int productId);
		IProduct GetByIdWithoutStatusFilter(IUserContext context, int productId, bool supplementProduct);
		IProduct GetByIdWithoutStatusFilter(IUserContext context, int productId, bool supplementProduct, bool allowSellOnlyInPackage);
		ProductCollection GetAllWithoutStatusFilter(IUserContext context, int page, int pageSize);
		ProductCollection PopulateProductsWithoutStatusFilter(IUserContext context, ProductIdCollection productIds);
		ProductCollection PopulateViewProductsWithoutStatusFilter(IUserContext context, ProductIdCollection productIds);
		ProductCollection PopulateViewProductsWithoutStatusFilter(IUserContext context, ProductIdCollection productIds, bool supplementProductView);

		// Products without any filter
		ProductIdCollection GetIdAllWithoutAnyFilter(int page, int pageSize, bool? isSellOnlyInPackage);
		ProductIdCollection GetIdAllWithoutAnyFilterForCdonExport(int page, int pageSize, int monthPurchasedAgo);
		ProductCollection GetAllWithoutAnyFilter(IUserContext context, int page, int pageSize, bool? isSellOnlyInPackage);
		ProductCollection PopulateProductsWithoutAnyFilter(IUserContext context, ProductIdCollection productIds);
		ProductCollection PopulateViewProductsWithoutAnyFilter(IUserContext context, ProductIdCollection productIds, bool supplementProductView);

		// Defines if multiple sizes purchase is allowed for product according to its category settings
		bool AllowMultipleSizesPurchase(IUserContext context, IProduct product);

		ProductIdCollection GetIdAllByPackageMasterProduct(IUserContext context, int productId);
	}
}