﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IWishListPackageItemService
	{
		IWishListPackageItem Create();
		Collection<IWishListPackageItem> GetAllByWishListItem(int wishListItemId);
		int Save(IWishListPackageItem item);
		void Update(IWishListPackageItem item);
	}
}