﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IIconService
	{
		Collection<IIcon> GetAll(IUserContext context, int productId, int categoryId, int? brandId);
	}
}