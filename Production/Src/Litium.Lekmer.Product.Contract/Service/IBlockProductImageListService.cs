﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductImageListService
	{
		IBlockProductImageList GetById(IUserContext context, int id);
	}
}
