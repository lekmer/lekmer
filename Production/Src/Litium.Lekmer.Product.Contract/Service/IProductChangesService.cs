using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	public interface IProductChangesService
	{
		IProductChangeEvent Create(int productId, string reference);

		int Insert(IProductChangeEvent productChangeEvent);

		Collection<IProductChangeEvent> GetAll(ProductChangeEventStatus status);
		Collection<IProductChangeEvent> GetAllInQueue(int pageSize);

		Collection<IProductChangeEvent> GetCdonExportAll(ProductChangeEventStatus status);
		Collection<IProductChangeEvent> GetCdonExportAllInQueue(int pageSize);

		void SetStatus(Collection<IProductChangeEvent> productChangeEvents, ProductChangeEventStatus status);
		void SetCdonExportStatus(Collection<IProductChangeEvent> productChangeEvents, ProductChangeEventStatus status);

		void DeleteExpiredItems();
	}
}