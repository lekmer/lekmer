using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductRegistry : IBusinessObjectBase
	{
		int ProductRegistryId { get; set; }
		string Title { get; set; }
	}
}