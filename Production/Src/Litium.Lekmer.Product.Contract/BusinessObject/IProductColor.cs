namespace Litium.Lekmer.Product
{
	public interface IProductColor
	{
		int ColorId { get; set; }
		int ProductId { get; set; }
	}
}