﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IPackage : IBusinessObjectBase
	{
		int PackageId { get; set; }
		int MasterProductId { get; set; }
		IProduct MasterProduct { get; set; }
		string GeneralInfo { get; set; }
		Collection<IProduct> PackageProducts { get; set; }
	}
}