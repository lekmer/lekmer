﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableTranslation : IBusinessObjectBase
	{
		int SizeTableId { get; set; }
		int LanguageId { get; set; }
		string Title { get; set; }
		string Description { get; set; }
		string Column1Title { get; set; }
		string Column2Title { get; set; }
	}
}