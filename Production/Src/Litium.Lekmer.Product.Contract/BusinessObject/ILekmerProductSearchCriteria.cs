﻿using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerProductSearchCriteria : IProductSearchCriteria
	{
		string BrandId { get; set; }
		string ProductTypeId { get; set; }
	}
}