using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISize : IBusinessObjectBase
	{
		int Id { get; set; }
		string ErpId { get; set; }
		string ErpTitle { get; set; }
		decimal Eu { get; set; }
		string EuTitle { get; set; }
		decimal UsMale { get; set; }
		decimal UsFemale { get; set; }
		decimal UkMale { get; set; }
		decimal UkFemale { get; set; }
		int Millimeter { get; set; }
		int Ordinal { get; set; }
	}
}