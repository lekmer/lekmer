﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IProductSizeRelation : IBusinessObjectBase
	{
		int ProductId { get; set; }
		int SizeId { get; set; }
	}
}