﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface ISizeTable : IBusinessObjectBase, ISizeTableIncludeItems
	{
		int Id { get; set; }
		int SizeTableFolderId { get; set; }
		string CommonName { get; set; }
		string Title { get; set; }
		string Description { get; set; }
		string Column1Title { get; set; }
		string Column2Title { get; set; }

		Collection<ISizeTableRow> Rows { get; set; }
		Collection<ISizeTableMediaItem> MediaItems { get; set; }
	}
}