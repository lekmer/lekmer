﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public interface IContentMessageTranslation : IBusinessObjectBase
	{
		int ContentMessageId { get; set; }
		int LanguageId { get; set; }
		string Message { get; set; }
	}
}