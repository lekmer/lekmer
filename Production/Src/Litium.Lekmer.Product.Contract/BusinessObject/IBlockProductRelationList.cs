﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductRelationList : ILekmerBlock
	{
		IProductSortOrder ProductSortOrder { get; set; }
		IBlockSetting Setting { get; set; }
	}
}