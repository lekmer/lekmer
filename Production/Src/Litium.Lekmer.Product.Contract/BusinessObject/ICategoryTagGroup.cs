namespace Litium.Lekmer.Product
{
	public interface ICategoryTagGroup
	{
		int CategoryId { get; set; }
		int TagGroupId { get; set; }
	}
}