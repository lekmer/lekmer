using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface ILekmerPriceListItem : IPriceListItem
	{
		decimal GetVatPercent();
	}
}