﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class PackageCollection : Collection<IPackage>
	{
		public PackageCollection()
		{
		}

		public PackageCollection(IList<IPackage> list)
			: base(list)
		{
		}

		public PackageCollection(IEnumerable<IPackage> list)
			: base(new List<IPackage>(list))
		{
		}

		public int TotalCount { get; set; }
	}
}