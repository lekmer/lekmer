﻿using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Product
{
	public interface IBlockImageRotator : ILekmerBlock
	{
		int? SecondaryTemplateId { get; set; }
	}
}