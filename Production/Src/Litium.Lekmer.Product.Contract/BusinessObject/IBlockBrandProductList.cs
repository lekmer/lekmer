﻿using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public interface IBlockBrandProductList : ILekmerBlock
	{
		int? TemplateId { get; set; }
		int ProductSortOrderId { get; set; }
		IProductSortOrder ProductSortOrder { get; set; }
		IBlockSetting Setting { get; set; }
	}
}