using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductSearchEsalesResultService
	{
		IBlockProductSearchEsalesResult GetById(IUserContext context, int blockId);
	}
}