using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IEsalesDidYouMeanService
	{
		Collection<string> FindCorrections(IUserContext context, string text, int numberOfItems);
	}
}