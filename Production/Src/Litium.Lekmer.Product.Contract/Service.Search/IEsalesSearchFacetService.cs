using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IEsalesSearchFacetService
	{
		ISearchFacet SearchFacetWithFilter(IUserContext context, string text);
		ISearchFacet SearchFacet(IUserContext context, string text);
	}
}