using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IEsalesIncrementalSearchService
	{
		ISearchSuggestions SearchSuggestions(IUserContext context, string text, int pageNumber, int pageSize);
		ISearchSuggestions SearchProductsWithFilter(IUserContext context, string text, int pageNumber, int pageSize);
		ISearchSuggestions SearchProducts(IUserContext context, string text, int pageNumber, int pageSize);
	}
}