﻿using Litium.Scensum.Product;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Product
{
	public static class ProductExtensions
	{
		/// <summary>
		/// Determines whether type of specified product is 'product'.
		/// </summary>
		public static bool IsProduct<T>(this T product) where T:IProduct
		{
			var lekmerProduct = (ILekmerProduct)product;
			return IsProduct(lekmerProduct.ProductTypeId);
		}

		/// <summary>
		/// Determines whether type of specified product is 'package'.
		/// </summary>
		public static bool IsPackage<T>(this T product) where T : IProduct
		{
			var lekmerProduct = (ILekmerProduct)product;
			return IsPackage(lekmerProduct.ProductTypeId);
		}


		/// <summary>
		/// Determines whether specified product type is 'product'.
		/// </summary>
		public static bool IsProduct(int productTypeId)
		{
			return productTypeId == (int)ProductTypeEnum.Product;
		}

		/// <summary>
		/// Determines whether specified product type is 'package'.
		/// </summary>
		public static bool IsPackage(int productTypeId)
		{
			return productTypeId == (int)ProductTypeEnum.Package;
		}



		/// <summary>
		/// Determines whether specified product is 'active'.
		/// </summary>
		public static bool IsActive<T>(this T product) where T : IProduct
		{
			var lekmerProduct = (ILekmerProduct)product;
			return IsActive(lekmerProduct.StockStatusId);
		}

		/// <summary>
		/// Determines whether specified product is 'passive'.
		/// </summary>
		public static bool IsPassive<T>(this T product) where T : IProduct
		{
			var lekmerProduct = (ILekmerProduct)product;
			return IsPassive(lekmerProduct.StockStatusId);
		}

		/// <summary>
		/// Determines whether specified product size is 'active'.
		/// </summary>
		public static bool IsActiveSize<T>(this T productSize) where T : IProductSize
		{
			return IsActive(productSize.StockStatusId);
		}

		/// <summary>
		/// Determines whether specified product sze is 'passive'.
		/// </summary>
		public static bool IsPassiveSize<T>(this T productSize) where T : IProductSize
		{
			return IsPassive(productSize.StockStatusId);
		}


		/// <summary>
		/// Determines whether specified product is 'active'.
		/// </summary>
		public static bool IsActive(int stockStatusId)
		{
			return stockStatusId == (int)StockStatus.Active;
		}

		/// <summary>
		/// Determines whether specified product is 'passive'.
		/// </summary>
		public static bool IsPassive(int stockStatusId)
		{
			return stockStatusId == (int)StockStatus.Passive;
		}
	}
}
