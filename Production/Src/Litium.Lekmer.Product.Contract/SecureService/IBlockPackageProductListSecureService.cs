using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockPackageProductListSecureService
	{
		IBlockPackageProductList Create();

		IBlockPackageProductList GetById(int id);

		int Save(ISystemUserFull systemUserFull, IBlockPackageProductList block);
	}
}