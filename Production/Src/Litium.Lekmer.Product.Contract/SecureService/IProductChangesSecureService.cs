using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductChangesSecureService
	{
		IProductChangeEvent Create(ISystemUserFull systemUserFull, int productId);

		int Insert(IProductChangeEvent productChangeEvent);
	}
}