﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IBlockProductImageListSecureService
	{
		IBlockProductImageList Create();
		int Save(ISystemUserFull systemUserFull, IBlockProductImageList block);
		IBlockProductImageList GetById(int blockId);
	}
}
