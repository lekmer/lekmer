﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableMediaSecureService
	{
		ISizeTableMediaRecord CreateRecord();

		Collection<ISizeTableMediaRecord> GetAllBySizeTable(int sizeTableId);

		void Save(ISystemUserFull systemUserFull, Collection<ISizeTableMediaRecord> sizeTableMediaRecords);
		void Save(ISystemUserFull systemUserFull, ISizeTableMediaRecord sizeTableMediaRecord);

		void Delete(ISystemUserFull systemUserFull, int sizeTableMediaRecordId);
	}
}