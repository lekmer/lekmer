﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeTableMediaRegistrySecureService
	{
		ISizeTableMediaRegistry Create();
		Collection<ISizeTableMediaRegistry> GetAllBySizeTable(int sizeTableId);
		void Save(ISystemUserFull systemUserFull, Collection<ISizeTableMediaRegistry> sizeTableMediaRegistries);
		void Delete(ISystemUserFull systemUserFull, int sizeTableMediaRegistryId);
	}
}