﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductUrlHistorySecureService
	{
		Collection<IProductUrlHistory> GetAllByProduct(int productId);

		IProductUrlHistory Create(int productId, int languageId, string urlTitle, int typeId);

		List<int> Save(ISystemUserFull systemUserFull, Collection<IProductUrlHistory> productUrlHistoryCollection);

		void DeleteById(ISystemUserFull systemUserFull, int productUrlHistoryId);

		void DeleteAllByProduct(ISystemUserFull systemUserFull, int productId);

		void DeleteAllByProductAndUrlTitle(ISystemUserFull systemUserFull, int productId, int languageId, string urlTitle);
	}
}