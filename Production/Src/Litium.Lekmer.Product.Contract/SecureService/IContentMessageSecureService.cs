﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IContentMessageSecureService
	{
		IContentMessage Save(ISystemUserFull systemUserFull, IContentMessage contentMessage, Collection<IContentMessageTranslation> translations);

		void Delete(ISystemUserFull systemUserFull, int id);

		IContentMessage GetById(int id);

		Collection<IContentMessage> GetAllByFolder(int folderId);

		Collection<IContentMessage> Search(string searchCriteria);

		// Includes.

		IContentMessage GetAllIncludes(IContentMessage contentMessage);

		void SaveIncludes(int contentMessageId, IEnumerable<int> productIds, IEnumerable<int> categoryIds, IEnumerable<int> brandIds, IEnumerable<int> supplierIds);

		void ClearIncludes(List<int> productIds, List<int> categoryIds, List<int> brandIds, List<int> supplierIds);

		// Translations.

		Collection<IContentMessageTranslation> GetAllTranslations(int contentMessageId);
	}
}