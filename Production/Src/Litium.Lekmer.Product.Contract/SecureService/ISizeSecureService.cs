﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface ISizeSecureService
	{
		ISize Create();
		Collection<ISize> GetAll();
		ISize GetById(int sizeId);
		void SetOrdinals(ISystemUserFull user, IEnumerable<ISize> sizes);
		int Save(ISystemUserFull user, ISize size);
		int Delete(ISystemUserFull user, int sizeId);
	}
}
