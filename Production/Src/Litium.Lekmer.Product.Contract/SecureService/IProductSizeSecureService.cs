﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public interface IProductSizeSecureService
	{
		Collection<IProductSize> GetAllByProduct(int productId);
		void Save(ISystemUserFull systemUserFull, IEnumerable<IProductSize> productSizes);
	}
}
