using System;
using Litium.Scensum.Campaign;
using Litium.Scensum.CartItemPriceAction.Repository;
using Litium.Scensum.Core;

namespace Litium.Scensum.CartItemPriceAction
{
	public class CartItemPriceActionService : ICartActionPluginService
	{
		protected CartItemPriceActionRepository Repository { get; private set; }

		public CartItemPriceActionService(CartItemPriceActionRepository cartItemPriceActionRepository)
		{
			Repository = cartItemPriceActionRepository;
		}

		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int cartActionId).")]
		public ICartAction GetById(int cartActionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var action = Repository.GetById(cartActionId);
			if (action == null) return null;

			action.Amounts = Repository.GetCartItemPricesByAction(cartActionId);
			action.IncludeProducts = Repository.GetIncludeProducts(cartActionId);
			action.IncludeCategories = Repository.GetIncludeCategories(cartActionId);
			action.ExcludeProducts = Repository.GetExcludeProducts(cartActionId);
			action.ExcludeCategories = Repository.GetExcludeCategories(cartActionId);
			return action;
		}

		public ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			if (Repository == null) throw new InvalidOperationException("Repository cannot be null.");

			var action = Repository.GetById(cartActionId);
			if (action == null) return null;

			action.Amounts = Repository.GetCartItemPricesByAction(cartActionId);
			action.IncludeProducts = Repository.GetIncludeProducts(cartActionId);
			action.IncludeCategories = Repository.GetIncludeCategories(cartActionId);
			action.ExcludeProducts = Repository.GetExcludeProducts(cartActionId);
			action.ExcludeCategories = Repository.GetExcludeCategories(cartActionId);
			return action;
		}
	}
}