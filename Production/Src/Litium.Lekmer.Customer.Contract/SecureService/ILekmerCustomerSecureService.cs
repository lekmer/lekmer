﻿using Litium.Scensum.Core;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomerSecureService : ICustomerSecureService
	{
		void Delete(ISystemUserFull systemUserFull, int customerId);
	}
}