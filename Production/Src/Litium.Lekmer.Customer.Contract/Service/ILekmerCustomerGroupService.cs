﻿using System.Collections.ObjectModel;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	public interface ILekmerCustomerGroupService
	{
		Collection<ICustomerGroup> GetAllByCustomer(int customerId);
	}
}