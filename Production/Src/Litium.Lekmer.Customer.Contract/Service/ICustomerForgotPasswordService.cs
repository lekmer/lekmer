﻿using System;

namespace Litium.Lekmer.Customer
{
	public interface ICustomerForgotPasswordService
	{
		ICustomerForgotPassword Create(int customerId);
		ICustomerForgotPassword GetById(Guid id);
		void Save(ICustomerForgotPassword customerForgotPassword);
		void Delete(int customerId);
	}
}
