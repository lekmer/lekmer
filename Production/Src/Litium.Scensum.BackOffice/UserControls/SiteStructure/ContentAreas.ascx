<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentAreas.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.ContentAreas" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>

<div id="content-areas">
	<asp:UpdatePanel ID="ContentAreasUpdatePanel" runat="server">
		<ContentTemplate>
			<asp:UpdatePanel ID="ContentAreaUpdatePanel" UpdateMode="Conditional" runat="server">
				<ContentTemplate>
					<asp:GridView ID="ContentAreaGrid" SkinID="grid" runat="server" Width="100%" AutoGenerateColumns="false">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="49%">
								<ItemTemplate>
									<uc:LinkButtonTrimmed ID="lbTitle" runat="server" MaxLength="40" CommandName="EditContentArea" 
										Text='<%# Eval("Title")%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="<%$ Resources:General,Literal_CommonName %>" DataField="CommonName" ItemStyle-Width="48%" />
							<asp:TemplateField ItemStyle-Width="3%" ItemStyle-HorizontalAlign="Center">
								<ItemTemplate>
									<asp:ImageButton ID="imbDelete" runat="server" CommandName="ContentAreaDelete" 
										ImageUrl="~/Media/Images/Common/delete.gif" />
								</ItemTemplate>
							</asp:TemplateField>								
						</Columns>
					</asp:GridView>
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="ModifyAreaButton" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
			
			<div class="buttons">
				<br />
				<uc:ImageLinkButton ID="AddAreaButton" runat="server" Text="<%$ Resources:SiteStructure,Button_AddArea %>" SkinID="DefaultButton" />
				<br />
				<br />
			</div>
			
			<asp:UpdatePanel ID="AreaPopupUpdatePanel" UpdateMode="Conditional" runat="server">
				<ContentTemplate>
					<div id="AreaEditDiv" runat="server" class="mainDiv" style="display: none;">
						<asp:Panel ID="AreaPanel" runat="server" DefaultButton="ModifyAreaButton">
							<div id="divAreaHeader" class="headerDiv">
								<asp:Label ID="AreaEditionModeLabel" runat="server" Text="" />
								<asp:Button ID="AreaEditCancelButton" runat="server" Text="X" />
							</div>
							<div id="template-area-subMainDiv">
							<uc:MessageContainer ID="ContentAreaMessageContainer" MessageType="Failure" HideMessagesControlId="btnModifyArea" runat="server" />	
								<div class="input-box">
									<span><asp:Literal runat="server" Text="<%$ Resources:General,Literal_Title%>" />&nbsp; *</span>
									<asp:RequiredFieldValidator runat="server" ID="AreaTitleRequiredFieldValidator" ControlToValidate="TitleTextBox"
										ErrorMessage="Title should not be empty"  Display ="None" ValidationGroup="vgContentArea" />
									<br />
									<asp:TextBox ID="TitleTextBox" runat="server" />
								</div>
								<div class="input-box">
									<span> <asp:Literal runat="server" Text="<%$ Resources:General,Literal_CommonName%>" /> &nbsp;*</span>
									<asp:RequiredFieldValidator runat="server" ID="AreaCommonNameRequiredFieldValidator" ControlToValidate="CommonNameTextBox"
										ErrorMessage="<%$ Resources:SiteStructureMessage,CommonNameEmpty %>" ValidationGroup="vgContentArea"
										 Display ="None" />
									<br />
									<asp:TextBox ID="CommonNameTextBox" runat="server" />
								</div>
								<div id="popup-template-area-errors" class="right">
									<uc:ImageLinkButton ID="ModifyAreaButton" runat="server" Text="<%$ Resources:General,Button_OK%>" ValidationGroup="vgContentArea"
										UseSubmitBehavior="false" SkinID="DefaultButton" />
									<uc:AjaxValidationSummary runat="server" ID="ContentAreaValidationSummary" CssClass="advance-validation-summary" DisplayMode="BulletList"
										HeaderText="Content area fields errors:" ValidationGroup="vgContentArea" ForeColor= "Black" />
									<asp:HiddenField ID="ContentAreaIdHiddenField" runat="server" />
									<asp:HiddenField ID="ContentAreaIndexHiddenField" runat="server" />
									
								</div>
							</div>
						</asp:Panel>
					</div>
					<asp:Button runat="server" ID="AreaModalPopupHiddenTargetControlButton" Style="display: none" />
					<ajaxToolkit:ModalPopupExtender ID="ContentAreaPopup" runat="server"
						TargetControlID="AreaModalPopupHiddenTargetControlButton" PopupControlID="AreaEditDiv"
						CancelControlID="AreaEditCancelButton" BackgroundCssClass="popup-background" />
				</ContentTemplate>
				<Triggers>
					<asp:AsyncPostBackTrigger ControlID="AddAreaButton" EventName="Click" />
				</Triggers>
			</asp:UpdatePanel>
		</ContentTemplate>
	</asp:UpdatePanel>
</div>