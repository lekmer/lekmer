﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlockTargetDevice.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.BlockTargetDevice" %>

<span class="block-header"><%=Resources.Lekmer.Block_TargetDevice%></span>
<fieldset>
	<div class="target-device-wrapper">
		<div class="target-device-container">
			<div class="target-device-desktop">
				<span><%=Resources.Lekmer.Block_TargetDevice_Desktop%></span>
				<br />
				<asp:CheckBox ID="DesktopCheckBox" runat="server"></asp:CheckBox>
			</div>
			<div class="target-device-mobile">
				<span><%=Resources.Lekmer.Block_TargetDevice_Mobile%></span>
				<br />
				<asp:CheckBox ID="MobileCheckBox" runat="server"></asp:CheckBox>
			</div>
		</div>
	</div>
</fieldset>