using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public class ProductPageProcessor : ISelectorProcessor
	{
		private IEnumerable<IContentPage> _selectablePages;
		
		private int SiteStructureRegistryId { get; set; }

		public ProductPageProcessor(int siteStructureRegistryId)
		{
			SiteStructureRegistryId = siteStructureRegistryId;
		}

		public IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId)
		{
			return PopulateProductPages();
		}

		public bool IsAvailable(ISiteStructureNode node)
		{
			return GetProductPages().FirstOrDefault(contentPage => contentPage.Id == node.Id) != null;
		}

		private IEnumerable<ISiteStructureNode> PopulateProductPages()
		{
			var productPages = GetProductPages();
			IEnumerable<ISiteStructureNode> selectedNodes = new Collection<ISiteStructureNode>();
			var contentNodeService = IoC.Resolve<IContentNodeSecureService>();
			foreach (var page in productPages)
			{
				selectedNodes = selectedNodes.Union(contentNodeService.GetTree(page.Id, false));
		}
			var result = new Collection<ISiteStructureNode>();
			foreach (var node in selectedNodes)
				{
				var nodeId = node.Id;
				if (result.Any(n => n.Id == nodeId)) continue;
				node.HasChildren = selectedNodes.Any(n => n.ParentId == nodeId);
					result.Add(node);
				}
			return result;
		}

		private IEnumerable<IContentPage> GetProductPages()
		{
			if (_selectablePages == null)
			{
				var productPages = IoC.Resolve<IContentPageSecureService>().GetByType("Product page", SiteStructureRegistryId);
				_selectablePages = productPages.Where(p => !p.IsMaster);
			}
			return _selectablePages;
		}
	}
}