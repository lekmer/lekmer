using System;
using System.Collections.ObjectModel;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	[Serializable]
	public class ContentAreasState
	{
		public int TemplateId { get; set; }

		public int? CopyOfId { get; set; }

		private Collection<IContentArea> _contentAreas;
		public Collection<IContentArea> ContentAreas
		{
			get
			{
				if (_contentAreas == null)
				{
					_contentAreas = new Collection<IContentArea>();
				}
				return _contentAreas;
			}
		}
		private Collection<IContentArea> _contentAreasToDelete;
		public Collection<IContentArea> ContentAreasToDelete
		{
			get
			{
				if (_contentAreasToDelete == null)
				{
					_contentAreasToDelete = new Collection<IContentArea>();
				}
				return _contentAreasToDelete;
			}
		}
		private Collection<bool> _confirmationMessageTypes;
		public Collection<bool> ConfirmationMessageTypes
		{
			get
			{
				if (_confirmationMessageTypes == null)
				{
					_confirmationMessageTypes = new Collection<bool>();
				}
				return _confirmationMessageTypes;
			}
		}
	}
}
