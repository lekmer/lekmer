﻿using System.Collections.ObjectModel;
using System.Configuration;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class BlockTargetDevice : UserControlController
	{
		public ILekmerBlock Block { get; set; }

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
			if (Block == null)
			{
				throw new ConfigurationErrorsException("Block property is not initialized");
			}

			PopulateData(Block);
		}

		private void PopulateData(ILekmerBlock block)
		{
			DesktopCheckBox.Checked = block.ShowOnDesktop ?? true;
			MobileCheckBox.Checked = block.ShowOnMobile ?? true;
		}

		public void SetTargetDevices(ILekmerBlock block)
		{
			block.ShowOnDesktop = DesktopCheckBox.Checked;
			block.ShowOnMobile = MobileCheckBox.Checked;
		}
	}
}