﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="BlockEsalesSetting.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.BlockEsalesSetting"
%>

<div class="esales-model">
	<span class="block-header"><%=Resources.Lekmer.Block_EsalesSettings%></span>
	<br class="clear" />
	<div class="input-box full-width ">
		<span><%= Resources.Lekmer.Literal_ChooseEsalesModel%>&nbsp;*</span>
		<asp:RequiredFieldValidator runat="server" ID="EsalesModelRequiredValidator" ControlToValidate="EsalesModelList"
			ErrorMessage="<%$ Resources:LekmerMessage, Message_EsalesModelNotSelected %>" Display="None" />
		<br />
		<asp:DropDownList ID="EsalesModelList" runat="server" DataTextField="Value" DataValueField="Key" />
	</div>
</div>
