<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContentPageSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.SiteStructure.ContentPageSelector" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree.SiteStructure"
    TagPrefix="CustomControls" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>

<asp:UpdatePanel ID="ProductPageUpdatePanel" runat="server" UpdateMode="Conditional">
		<Triggers>
			<asp:AsyncPostBackTrigger  ControlID="SavePageButton" EventName="Click"/>
		</Triggers>
		<ContentTemplate>
		<span id="SelectionSpan" runat="server" class="media-item-information-value" style="width: 90%;	color: Black;">
				<uc:LiteralEncoded runat="server" ID="PageSelectionLiteral" />
		</span>
	    <span id="AllowClearSelectionSpan" runat="server" class="media-item-information-value" style="width: 90%; color:Gray;">
				<uc:LiteralEncoded runat="server" ID="PageAllowClearSelectionLiteral" />
		</span>  
        <asp:ImageButton ID="PageButton" runat="server" ImageUrl="~/Media/Images/SiteStructure/folder.png" ImageAlign="AbsMiddle" CausesValidation="False"/>
         <div id="PageDiv" runat="server" class="mainDiv block-save" style="display: none;">
			<asp:Panel ID="Panel1" runat="server" DefaultButton="SavePageButton">
				<div id="divHeader" class="headerDiv">
					<asp:Label ID="Label2" runat="server" Text="<%$ Resources:SiteStructure, Literal_ContentNodes %>" />
					<asp:Button ID="CancelPageButton" runat="server" Text="X" />
				</div>
				<div id="page-select-subdiv">
					<asp:UpdatePanel ID="LeftUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="select-folder-header">
								<%= Resources.SiteStructure.Literal_Pages %>
							</div>
							<div id="select-folder">
								<asp:UpdatePanel ID="TreeUpdatePanel" runat="server">
									<ContentTemplate>
										<CustomControls:SiteStructureTreeView Id="NodesTreeView" runat="server" DisplayTextControl="lbName"
											NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="ss-node-type"
											MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
											NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
											NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
											NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
											MenuContainerElementId="node-menu" FolderImageUrlOnline="~/Media/Images/SiteStructure/folder.png"
											FolderImageUrlOffline="~/Media/Images/SiteStructure/folder-off.png" FolderImageUrlHidden="~/Media/Images/SiteStructure/folder-hid.png"
											MasterPageImageUrl="~/Media/Images/SiteStructure/master.png" SubPageImageUrlOnline="~/Media/Images/SiteStructure/sub-page.png"
											SubPageImageUrlOffline="~/Media/Images/SiteStructure/sub-page-off.png" SubPageImageUrlHidden="~/Media/Images/SiteStructure/sub-page-hid.png"
											PageImageUrlOnline="~/Media/Images/SiteStructure/page.png" PageImageUrlOffline="~/Media/Images/SiteStructure/page-off.png"
											PageImageUrlHidden="~/Media/Images/SiteStructure/page-hid.png" ShortcutImageUrlOnline="~/Media/Images/SiteStructure/shortcut.png"
											ShortcutImageUrlOffline="~/Media/Images/SiteStructure/shortcut-off.png" ShortcutImageUrlHidden="~/Media/Images/SiteStructure/shortcut-hid.png"
											UrlLinkImageUrlOnline="~/Media/Images/SiteStructure/link.png" UrlLinkImageUrlOffline="~/Media/Images/SiteStructure/link-off.png"
											UrlLinkImageUrlHidden="~/Media/Images/SiteStructure/link-hid.png" TypeImageContainerId="img"
											TypeImageContainerCssClass="ss-node-type" MenuToOnlineContainerId="to-online"
											MenuToOfflineContainerId="to-offline" MenuToHiddenContainerId="to-hidden" MenuContainerElementCssClass="menu-container"
											MenuContainerShadowElementCssClass="menu-shadow" MenuContainerElementTypeOfPageCssClass="menu-container-typeof-page"
											MenuContainerShadowElementTypeOfPageCssClass="menu-shadow-typeof-page" MenuContainerShadowElementId="menu-shadow"
											MenuPageSpecificContainerId="menu-pages-container" MenuItemsContainerElementId="menu-container"
											NodeNavigationAnchorId="aName" NodeSelectedCssClass="treeview-node-selected">
											<HeaderTemplate>
												<div id="clear-selection" style="padding-left:20px; padding-top:3px; height:17px;  border-bottom-width:2px;">
													<asp:LinkButton runat="server" ID="lbClearSelection" Font-Underline="false" OnClick="ClearSelectionButtonClick" Text="<%$ Resources:SiteStructure, Button_ClearSelection %>" ></asp:LinkButton>
												</div>
											</HeaderTemplate>
											<NodeTemplate>
												<div class="tree-item-cell-expand">
													<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
													<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
												</div>
												<div class="tree-item-cell-main">
													<img runat="server" id="img" class="ss-node-type tree-node-img" />
													<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
												</div>
												<br />
											</NodeTemplate>
										</CustomControls:SiteStructureTreeView>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
							<div class="right clear">
								<uc:ImageLinkButton ID="SavePageButton"  style="text-decoration:none;" runat="server" Text="<%$ Resources:General,Button_Ok %>" SkinID="DefaultButton" />
								<uc:ImageLinkButton ID="CancelSavePageButton" runat="server" style="text-decoration:none;" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>			
			</asp:Panel>
		</div>
		<asp:HiddenField runat="server" ID="ModalPopupExtenderHiddenField" />
		
<ajaxToolkit:ModalPopupExtender 
	ID="TreeModalPopupExtender" 
	runat="server"  
	TargetControlID="ModalPopupExtenderHiddenField"
	PopupControlID="PageDiv"
	CancelControlID="CancelPageButton"
	BackgroundCssClass="popup-background"/>
	
	</ContentTemplate>
</asp:UpdatePanel>
