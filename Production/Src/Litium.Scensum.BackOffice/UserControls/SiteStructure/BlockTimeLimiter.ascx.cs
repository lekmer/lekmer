﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Globalization;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;
using Resources;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class BlockTimeLimiter : UserControlController
	{
		public ILekmerBlock Block { get; set; }

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
			if (Block == null)
				throw new ConfigurationErrorsException("Block property is not initialized");

			PopulateData(Block);
		}

		private void PopulateData(ILekmerBlock block)
		{
			StartDateTextBox.Text = ConvertDateTime(block.StartDate);
			EndDateTextBox.Text = ConvertDateTime(block.EndDate);
			StartIntervalTextBox.Text = ConvertTime(block.StartDailyInterval);
			EndIntervalTextBox.Text = ConvertTime(block.EndDailyInterval);
		}

		private static string ConvertDateTime(DateTime? dateTime)
		{
			return dateTime.HasValue
				       ? dateTime.Value.ToString(ShortDateTimePattern, CultureInfo.CurrentCulture)
				       : string.Empty;
		}

		private static string ConvertTime(TimeSpan? timeSpan)
		{
			return timeSpan.HasValue
				       ? new DateTime(timeSpan.Value.Ticks).ToShortTimeString()
				       : string.Empty;
		}

		public Collection<string> TrySetTimeLimit(ILekmerBlock block)
		{
			var startDateString = StartDateTextBox.Text.Trim();
			var endDateString = EndDateTextBox.Text.Trim();
			var startIntervalString = StartIntervalTextBox.Text.Trim();
			var endIntervalString = EndIntervalTextBox.Text.Trim();

			DateTime? startDate;
			DateTime? endDate;
			TimeSpan? startInterval;
			TimeSpan? endInterval;

			bool isStartDateValid = TryParseDateTime(startDateString, out startDate);
			bool isEndDateValid = TryParseDateTime(endDateString, out endDate);
			bool isStartIntervalValid = TryParseTime(startIntervalString, out startInterval);
			bool isEndIntervalValid = TryParseTime(endIntervalString, out endInterval);

			var errors = new Collection<string>();

			if (!isStartDateValid)
			{
				errors.Add(GetIncorrectDateTimeMessage(LekmerMessage.StartDateTimeIncorrect));
			}
			if (!isEndDateValid)
			{
				errors.Add(GetIncorrectDateTimeMessage(LekmerMessage.EndDateTimeIncorrect));
			}
			if (isStartDateValid && isEndDateValid && startDate > endDate)
			{
				errors.Add(LekmerMessage.DateTimeIntervalIncorrect);
			}
			if (!isStartIntervalValid)
			{
				errors.Add(GetIncorrectTimeMessage(LekmerMessage.StartDailyIntervalIncorrect));
			}
			if (!isEndIntervalValid)
			{
				errors.Add(GetIncorrectTimeMessage(LekmerMessage.EndDailyIntervalIncorrect));
			}
			if (isStartIntervalValid && isEndIntervalValid && !IsDailyIntervalCorrect(startInterval, endInterval))
			{
				errors.Add(LekmerMessage.DailyIntervalIncorrect);
			}

			if (errors.Count == 0)
			{
				block.StartDate = startDate;
				block.EndDate = endDate;
				block.StartDailyInterval = startInterval;
				block.EndDailyInterval = endInterval;

				PopulateData(block);
			}
			return errors;
		}

		private static bool TryParseDateTime(string input, out DateTime? result)
		{
			result = null;
			if (string.IsNullOrEmpty(input))
			{
				return true;
			}

			var validFormats = new[]
				{
					CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
					ShortDateTimePattern
				};
			DateTime dateTime;
			bool isDateTime = DateTime.TryParseExact(input, validFormats, CultureInfo.CurrentCulture, DateTimeStyles.None,
			                                         out dateTime);

			if (!isDateTime)
			{
				return false;
			}

			result = dateTime;
			return true;
		}

		private static bool TryParseTime(string input, out TimeSpan? result)
		{
			result = null;
			if (string.IsNullOrEmpty(input))
			{
				return true;
			}

			DateTime time;
			bool isTime = DateTime.TryParseExact(input, CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern,
			                                     CultureInfo.CurrentCulture, DateTimeStyles.None, out time);

			if (!isTime)
			{
				return false;
			}

			result = time.TimeOfDay;
			return true;
		}

		private static bool IsDailyIntervalCorrect(TimeSpan? start, TimeSpan? end)
		{
			if (start.HasValue && end.HasValue)
			{
				return start <= end;
			}
			return !start.HasValue && !end.HasValue;
		}

		private static string GetIncorrectDateTimeMessage(string beginning)
		{
			return string.Format(CultureInfo.CurrentCulture, beginning,
			                     CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
			                     ShortDateTimePattern);
		}

		private static string GetIncorrectTimeMessage(string beginning)
		{
			return string.Format(CultureInfo.CurrentCulture, beginning,
			                     CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
		}

		private static string ShortDateTimePattern
		{
			get
			{
				return string.Format(CultureInfo.CurrentCulture, "{0} {1}",
				                     CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern,
				                     CultureInfo.CurrentCulture.DateTimeFormat.ShortTimePattern);
			}
		}
	}
}