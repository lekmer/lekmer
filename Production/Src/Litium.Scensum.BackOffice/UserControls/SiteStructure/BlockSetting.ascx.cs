﻿using System.Configuration;
using System.Globalization;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.UserControls.SiteStructure
{
	public partial class BlockSetting : UserControlController
	{
		public IBlockSetting Setting { get; set; }
		public string ValidationGroup { get; set; }

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
			if (Setting == null) throw new ConfigurationErrorsException("Setting property is not initialized");
			PopulateData(Setting);
		}

		public void SetSettings(IBlockSetting setting)
		{
			setting.ColumnCount = ParseStringToInt(NumberOfColumnsTextBox.Text).Value;
			setting.RowCount = ParseStringToInt(NumberOfRowsTextBox.Text).Value;
			setting.TotalItemCount = ParseStringToInt(NumberOfProductsTextBox.Text).Value;
			setting.ColumnCountMobile = ParseStringToInt(NumberOfColumnsMobileTextBox.Text);
			setting.RowCountMobile = ParseStringToInt(NumberOfRowsMobileTextBox.Text);
			setting.TotalItemCountMobile = ParseStringToInt(NumberOfProductsMobileTextBox.Text);

			PopulateData(setting);
		}

		private void PopulateData(IBlockSetting setting)
		{
			NumberOfColumnsTextBox.Text = ParseIntToString(setting.ColumnCount);
			NumberOfRowsTextBox.Text = ParseIntToString(setting.RowCount);
			NumberOfProductsTextBox.Text = ParseIntToString(setting.TotalItemCount);
			NumberOfColumnsMobileTextBox.Text = ParseIntToString(setting.ColumnCountMobile);
			NumberOfRowsMobileTextBox.Text = ParseIntToString(setting.RowCountMobile);
			NumberOfProductsMobileTextBox.Text = ParseIntToString(setting.TotalItemCountMobile);

			ColumnsNumberRangeValidator.ValidationGroup = ColumnsRequiredValidator.ValidationGroup =
				RowsNumberRangeValidator.ValidationGroup = RowsRequiredValidator.ValidationGroup =
				TotalProductsRangeValidator.ValidationGroup = TotalProductsRequiredFieldValidator.ValidationGroup =
				ColumnsNumberMobileRangeValidator.ValidationGroup = RowsNumberMobileRangeValidator.ValidationGroup =
				TotalProductsMobileRangeValidator.ValidationGroup = ValidationGroup;

			RowsNumberRangeValidator.MaximumValue = RowsNumberMobileRangeValidator.MaximumValue =
				ColumnsNumberRangeValidator.MaximumValue = ColumnsNumberMobileRangeValidator.MaximumValue =
				TotalProductsRangeValidator.MaximumValue = TotalProductsMobileRangeValidator.MaximumValue = int.MaxValue.ToString(CultureInfo.CurrentCulture);
		}

		protected int? ParseStringToInt(string value)
		{
			return !string.IsNullOrEmpty(value) ? int.Parse(value, CultureInfo.CurrentCulture) : (int?)null;
		}
		protected string ParseIntToString(int? value)
		{
			return value != null ? value.ToString() : string.Empty;
		}
	}
}