using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Customer.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.UserControls.Customer
{
	public partial class CustomerGroupSelector : StateUserControlController<CustomerGroupSelectorState>
	{	
		private IEnumerable<INode> _nodes;
		private Collection<ICustomerGroupStatus> _customerGroupStatuses;

		public event EventHandler<CustomerGroupSelectEventArgs> SelectEvent;
        
		protected override void SetEventHandlers()
		{
			CustomerGroupTree.NodeCommand += OnNodeCommand;
			CustomerGroupGrid.PageIndexChanging += OnPageIndexChanging;
			CustomerGroupGrid.RowDataBound += OnRowDataBound;
			CustomerGroupGrid.DataBound += OnDataBound;
            AddButton.Click += OnAdd;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if(!IsPostBack)
				State = new CustomerGroupSelectorState();

			if (State.DenySelection.HasValue)
				CustomerGroupTree.DenySelection = State.DenySelection.Value;
		}

		protected virtual void OnAdd(object sender, EventArgs e)
		{
			var ids = CustomerGroupGrid.GetSelectedIds();
			var customerGroups = new Collection<ICustomerGroup>();
			var service = IoC.Resolve<ICustomerGroupSecureService>();
			foreach (var id in ids)
			{
				customerGroups.Add(service.GetById(id));
			}

			if (customerGroups.Count > 0 && SelectEvent != null)
			{
				SelectEvent(this, new CustomerGroupSelectEventArgs { CustomerGroups = customerGroups });
				CustomerGroupSelectPopup.Hide();
			}
		}

		protected virtual void OnDataBound(object sender, EventArgs e)
		{
			AddButton.Enabled = CustomerGroupGrid.Rows.Count > 0;
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView) sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			CustomerGroupGrid.PageIndex = e.NewPageIndex;
			PopulateGrid(State.SelectedNodeId);
		}

		protected override void PopulateControl()
		{
			PopulateTree(null);
			PopulateGrid(null);
		}

		protected virtual void PopulateTree(int? folderId)
		{
			CustomerGroupTree.DataSource = GetFolders(folderId);
			CustomerGroupTree.RootNodeTitle = Resources.Customer.Label_CustomerGroups;
			CustomerGroupTree.DataBind();
			CustomerGroupTree.SelectedNodeId = folderId ?? CustomerGroupTree.RootNodeId;
		}

		protected virtual void PopulateGrid(int? folderId)
		{
			var id = folderId != CustomerGroupTree.RootNodeId
				? folderId
				: null;
			CustomerGroupGrid.DataSource = IoC.Resolve<ICustomerGroupSecureService>().GetAllByFolder(id);
			CustomerGroupGrid.DataBind();
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					SetSelection(false);
					break;
				case "Navigate":
					PopulateGrid(e.Id);
					SetSelection(true);
					State.SelectedNodeId = e.Id;
					break;
			}
		}

		protected virtual IEnumerable<INode> GetFolders(int? nodeId)
		{
			return _nodes ?? (_nodes = IoC.Resolve<ICustomerGroupFolderSecureService>().GetTree(nodeId));
		}

		protected virtual string GetCustomerGropStatus(object statusId)
		{
			if (_customerGroupStatuses == null)
				_customerGroupStatuses = IoC.Resolve<ICustomerGroupStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _customerGroupStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"')");
			}
		}

		protected virtual void SetSelection(bool isSelected)
		{
			State.DenySelection = CustomerGroupTree.DenySelection = !isSelected;
		}
	}

	[Serializable]
	public sealed class CustomerGroupSelectorState
	{
		public int? SelectedNodeId { get; set; }
		public bool? DenySelection { get; set; }
	}
}