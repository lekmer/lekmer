﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Foundation;
using Convert=System.Convert;
using Litium.Scensum.BackOffice.CommonItems;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Order
{
	public partial class OrderPaymentsControl : StateUserControlController<Collection<IOrderPayment>>
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderPayment> OrderPayments
		{
			get
			{
				return State;
			}
			set
			{
				State = value;
			}
		}

		private IOrderFull _orderFull;
		public IOrderFull OrderFull
		{
			get
			{
				return _orderFull
					?? (_orderFull = IoC.Resolve<IOrderSecureService>().GetFullById(GetId()));
			}
			set
			{
				_orderFull = value;
			}
		}

		private IChannel _orderChannel;
		protected IChannel OrderChannel
		{
			get
			{
				return _orderChannel
					?? (_orderChannel = IoC.Resolve<IChannelSecureService>().GetById(OrderFull.ChannelId));
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1305:SpecifyIFormatProvider", MessageId = "System.Convert.ToInt32(System.Object)")]
		private int CurrentEditingPaymentId
		{
			get
			{
				return Convert.ToInt32(ViewState["cpId"], CultureInfo.CurrentCulture);
			}
			set
			{
				ViewState["cpId"] = value;
			}
		}

		public bool EnableAdding { get; set; }
		public bool EnableEditing { get; set; }
		public bool EnableDeleting { get; set; }

		public event EventHandler<EventArgs> PaymentsChanged;
		public event EventHandler<CannotDoEventArgs> CannotDo;

		protected override void SetEventHandlers()
		{
			ValueTextBoxValidator.ServerValidate += DecimalValidate;
			VatTextBoxValidator.ServerValidate += DecimalValidate;
			
			OrderPaymentGrid.RowCommand += OrderPaymentGridOnRowCommand;
			OrderPaymentGrid.RowDataBound += OrderPaymentGrid_RowDataBound;
			OkButton.Click += OkButton_Click;
			AddPaymentLinkButton.Click += AddPaymentLinkButton_Click;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1820:TestForEmptyStringsUsingStringLength")]
		protected void DecimalValidate(object source, ServerValidateEventArgs args)
		{
			decimal result;
			string value = args.Value;
			args.IsValid = (value.HasValue() && value.TryParseLocalDecimalAsNumber(out result) && (result > 0));
		}

		protected void AddPaymentLinkButton_Click(object sender, EventArgs e)
		{
			if (!IoC.Resolve<IOrderPaymentSecureService>().CanAdd(OrderFull))
			{
				if (null != CannotDo)
				{
					CannotDo(this, new CannotDoEventArgs(Cannot.Add));
				}
				return;
			}
			BindEditPopup(null);
			PaymentPopup.Show();
		}

		protected void OkButton_Click(object sender, EventArgs e)
		{
			if (!Page.IsValid)
			{
				PaymentPopup.Show();
				return;
			}

			if (State.SingleOrDefault(item => item.Id == CurrentEditingPaymentId) != null)//we edit existing payment
			{
				if (!IoC.Resolve<IOrderPaymentSecureService>().CanEdit(OrderFull, State.SingleOrDefault(item => item.Id == CurrentEditingPaymentId)))
				{
					SystemMessageContainer.Add(Resources.CustomerMessage.CannotEditPayment);
					PaymentPopup.Show();
					return;
				}

				IOrderPayment payment = State.Single(item => item.Id == CurrentEditingPaymentId);
				FillPayment(payment);
				
			}
			else//we create new payment
			{
				IOrderPayment payment = IoC.Resolve<IOrderPaymentSecureService>().Create();
				payment.Id = CurrentEditingPaymentId;
				FillPayment(payment);
				State.Add(payment);
			}

			PopulatePayment();

			if (PaymentsChanged != null)
			{
				PaymentsChanged(this, EventArgs.Empty);
			}
		}

		private void FillPayment(IOrderPayment payment)
		{
			payment.Price = ValueTextBox.Text.ToLocalDecimal();
			payment.PaymentTypeId = PaymentTypeDropDownList.SelectedValue.ToLocalInt32();
			payment.Vat = VatTextBox.Text.ToLocalDecimal();
			payment.ReferenceId = TransactionIdTextBox.Text;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
		protected void OrderPaymentGridOnRowCommand(object sender, CommandEventArgs e)
		{
			if (e.CommandName == "DeletePayment")
			{
				
				int id = Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);

				if (IoC.Resolve<IOrderPaymentSecureService>().CanDelete(OrderFull, State.SingleOrDefault(item => item.Id == id)))
				{

					State.Single(item => item.Id == id).Status = BusinessObjectStatus.Deleted;
					PopulateControl();

					if (PaymentsChanged != null)
					{
						PaymentsChanged(this, EventArgs.Empty);
					}
				}
				else
				{
					if (null != CannotDo)
					{
						CannotDo(this, new CannotDoEventArgs(Cannot.Delete));
					}
				}
			}
			else if (e.CommandName == "EditPayment")
			{
				int id = Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
				BindEditPopup(id);
				PaymentPopup.Show();
			}
			
		}

		private void OrderPaymentGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType == DataControlRowType.DataRow)
			{
				EnableFeatures(row);

				var paymentTypeSecureService = IoC.Resolve<IPaymentTypeSecureService>();
				var paymentTypeId = ((OrderPayment) e.Row.DataItem).PaymentTypeId;
				var paymentType = paymentTypeSecureService.GetById(paymentTypeId);
				var paymentTypeTitle = paymentType.Title;

				var paymentLinkButton = (LinkButton)row.FindControl("PaymentLinkButton");
				paymentLinkButton.Text = paymentTypeTitle;

				var paymentLiteral = (Literal)row.FindControl("PaymentLiteral");
				paymentLiteral.Text = paymentTypeTitle;
			}
		}

		private void BindEditPopup(int? id)
		{
			if (id.HasValue) //edit existing payment
			{
				CurrentEditingPaymentId = id.Value;

				var payment = (ILekmerOrderPayment)State.Single(item => item.Id == id);
	
				ValueTextBox.Text = payment.Price.ToLocalStringAsNumber();
				VatTextBox.Text = payment.Vat.ToLocalStringAsNumber();
				TransactionIdTextBox.Text = payment.ReferenceId;
				if (payment.Captured.HasValue)
				{
					CapturedPlaceHolder.Visible = true;
					CapturedTextBox.Text = payment.Captured.Value ? "Yes" : "No";
				}
				if (payment.KlarnaEid.HasValue)
				{
					KlarnaEidPlaceHolder.Visible = true;
					KlarnaEidTextBox.Text = payment.KlarnaEid.Value.ToLocalString();
				}
				if (payment.KlarnaPClass.HasValue)
				{
					KlarnaPClassPlaceHolder.Visible = true;
					KlarnaPClassTextBox.Text = payment.KlarnaPClass.Value.ToLocalString();
				}
				if (payment.MaksuturvaCode.HasValue())
				{
					MaksuturvaCodePlaceHolder.Visible = true;
					MaksuturvaCodeTextBox.Text = payment.MaksuturvaCode;
				}
				if (payment.QliroClientRef.HasValue())
				{
					QliroClientRefPlaceHolder.Visible = true;
					QliroClientRefTextBox.Text = payment.QliroClientRef;
				}
				if (payment.QliroPaymentCode.HasValue())
				{
					QliroPaymentCodePlaceHolder.Visible = true;
					QliroPaymentCodeTextBox.Text = payment.QliroPaymentCode;
				}

				if (payment.CollectorStoreId.HasValue)
				{
					CollectorStoreIdPlaceHolder.Visible = true;
					CollectorStoreIdTextBox.Text = payment.CollectorStoreId.Value.ToLocalString();
				}
				if (payment.CollectorPaymentCode.HasValue())
				{
					CollectorPaymentCodePlaceHolder.Visible = true;
					CollectorPaymentCodeTextBox.Text = payment.CollectorPaymentCode;
				}

				PaymentTypeDropDownList.SelectedValue = payment.PaymentTypeId.ToLocalString();
			}
			else //will create new payment on ok
			{
				int newPaymentId;
				if (State.Any(item => item.Id < 0))
				{
					newPaymentId = State.Min(item => item.Id) - 1;
				}
				else
				{
					newPaymentId = -1;
				}

				CurrentEditingPaymentId = newPaymentId;

				ValueTextBox.Text = string.Empty;
				TransactionIdTextBox.Text = string.Empty;
				VatTextBox.Text = string.Empty;
				PaymentTypeDropDownList.SelectedIndex = 0;

				CapturedPlaceHolder.Visible = false;
				CapturedTextBox.Text = string.Empty;
				KlarnaEidPlaceHolder.Visible = false;
				KlarnaEidTextBox.Text = string.Empty;
				KlarnaPClassPlaceHolder.Visible = false;
				KlarnaPClassTextBox.Text = string.Empty;
				MaksuturvaCodePlaceHolder.Visible = false;
				MaksuturvaCodeTextBox.Text = string.Empty;
				QliroClientRefPlaceHolder.Visible = false;
				QliroClientRefTextBox.Text = string.Empty;
				QliroPaymentCodePlaceHolder.Visible = false;
				QliroPaymentCodeTextBox.Text = string.Empty;
				CollectorStoreIdPlaceHolder.Visible = false;
				CollectorStoreIdTextBox.Text = string.Empty;
				CollectorPaymentCodePlaceHolder.Visible = false;
				CollectorPaymentCodeTextBox.Text = string.Empty;
			}
		}

		protected override void PopulateControl()
		{
			PopulatePayment();
			PopulatePaymentTypes();
			EnableFeatures();
		}

		private void PopulatePayment()
		{
			OrderPaymentGrid.DataSource = State.Where(item => item.Status != BusinessObjectStatus.Deleted);
			OrderPaymentGrid.DataBind();
		}

		private void PopulatePaymentTypes()
		{
			PaymentTypeDropDownList.DataSource = IoC.Resolve<IPaymentTypeSecureService>().GetAll();
			PaymentTypeDropDownList.DataBind();
		}

		public void ReBind()
		{
			PopulateControl();
		}
		
		private void EnableFeatures()
		{
			AddPaymentLinkButton.Visible = EnableAdding;
			OrderPaymentGrid.Columns[2].Visible = EnableDeleting;
		}
		private void EnableFeatures(Control row)
		{
			var paymentLinkButton = (LinkButton)row.FindControl("PaymentLinkButton");
			paymentLinkButton.Visible = EnableEditing;

			var paymentLiteral = (Literal)row.FindControl("PaymentLiteral");
			paymentLiteral.Visible = !EnableEditing;
		}

		protected string FormatPrice(decimal value)
		{
			var formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>();
			return formatter.FormatPriceTwoOrLessDecimals(OrderChannel, value);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected int GetId()
		{
			return Request.QueryString.GetInt32("Id");
		}
	}

	public class CannotDoEventArgs : EventArgs
	{
		public CannotDoEventArgs(Cannot cannotDo)
		{
			Cannot = cannotDo;
		}

		public Cannot Cannot { get; set; }
	}

	public enum Cannot
	{
		Add,
		Edit,
		Delete
	}
}
