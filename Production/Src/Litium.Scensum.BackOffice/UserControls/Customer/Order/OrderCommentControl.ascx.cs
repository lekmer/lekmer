﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Setting;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Scensum.BackOffice.UserControls.Customer.Order
{
	public partial class OrderCommentControl : StateUserControlController<OrderCommentControlState>
	{
		public event EventHandler OnError;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderComment> OrderComments
		{
			get
			{
				return State.OrderComments;
			}
			set
			{
				State.OrderComments = value;
			}
		}

		public IOrderFull Order
		{
			get
			{
				return State.Order;
			}
			set
			{
				State.Order = value;
			}
		}

		public bool EnableAdding { get; set; }

		public OrderCommentControl()
		{
			State = new OrderCommentControlState();
		}

		protected override void SetEventHandlers()
		{
			AddNewCommentButon.Click += AddNewCommentButton_Click;
			SaveButton.Click += SaveButton_Click;
		}

		protected void AddNewCommentButton_Click(object sender, EventArgs e)
		{
			if (!IoC.Resolve<IOrderCommentSecureService>().CanAdd(State.Order))
			{
				if (OnError != null)
				{
					OnError(this, EventArgs.Empty);
					return;
				}
			}
			else
			{
				CommentEditTextBox.Text = string.Empty;
				CommentPopup.Show();
			}
		}
		protected void SaveButton_Click(object sender, EventArgs e)
		{
			var commentText = CommentEditTextBox.Text;
			if (commentText.Length > 250)
			{
				var message = string.Format(CultureInfo.InvariantCulture, Resources.Customer.Literal_CommentsTextTooLong, 250, commentText.Length);
				SystemMessageContainer.Add(message);
				CommentPopup.Show();
				return;
			}

			IOrderComment orderComment = IoC.Resolve<IOrderCommentSecureService>().Create();
			orderComment.Author = SignInHelper.SignedInSystemUser;
			orderComment.Comment = commentText;
			orderComment.CreationDate = DateTime.Now;
			if (State.OrderComments.Any(item => item.Id < 0))
			{
				orderComment.Id = State.OrderComments.Min(item => item.Id) - 1;
			}
			else
			{
				orderComment.Id = -1;
			}
			State.OrderComments.Add(orderComment);

			CommentList.DataSource = State.OrderComments;
			CommentList.DataBind();
		}

		protected override void PopulateControl()
		{
			CommentList.DataSource = State.OrderComments;
			CommentList.DataBind();

			AddNewCommentButon.Visible = EnableAdding;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re")]
	
	public void REBind()
		{
			PopulateControl();
		}
	}

	[Serializable]
	public class OrderCommentControlState
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IOrderComment> OrderComments { get; set; }
		public IOrderFull Order { get; set; }
	}
}