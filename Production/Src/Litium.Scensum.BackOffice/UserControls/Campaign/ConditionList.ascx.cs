using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
	public partial class ConditionList : CampaignConfigurationList<ICondition>
	{
		private Dictionary<int, string> _currencies;
		public Dictionary<int, string> Currencies
		{
			get
			{
				if (_currencies != null)
				{
					return _currencies;
				}

				_currencies = new Dictionary<int, string>();

				var currencies = IoC.Resolve<ICurrencySecureService>().GetAll();
				foreach (var currency in currencies)
				{
					_currencies.Add(currency.Id, currency.Iso);
				}

				return _currencies;
			}
		}

		protected override void SetEventHandlers()
		{
			ConditionsGrid.RowDataBound += OnGridRowDataBound;
			ConditionsGrid.RowCommand += OnGridRowCommand;
		}

		protected override void PopulateControl() { }

		public override void DataBind(Collection<ICondition> items)
		{
			ConditionsGrid.DataSource = items;
			ConditionsGrid.DataBind();
			State = items;
		}

		protected virtual void OnGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType.Equals(DataControlRowType.DataRow))
			{
				var condition = (ICondition)e.Row.DataItem;

				if (condition.Status.Equals(BusinessObjectStatus.Deleted))
				{
					e.Row.Visible = false;
					return;
				}

				var editButton = (LinkButton)e.Row.FindControl("EditButton");
				editButton.Text = condition.ConditionType.Title;
				editButton.CommandArgument = condition.Guid.ToString();

				var descriptionLiteral = (Literal)e.Row.FindControl("DescriptionLiteral");
				descriptionLiteral.Text = GetFriendlyDescription(condition);

				var removeButton = (ImageButton)e.Row.FindControl("RemoveButton");
				removeButton.CommandArgument = condition.Guid.ToString();
			}
		}

		protected virtual void OnGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			var argument = new CommandEventArgs("ConditionGuid", e.CommandArgument);
			switch (e.CommandName)
			{
				case "Configure":
					OnListItemEdit(argument);
					break;
				case "Remove":
					OnListItemRemove(argument);
					break;
			}
		}

		protected virtual string GetFriendlyDescription(ICondition condition)
		{
			string formatString = Resources.CampaignPlugin.ResourceManager.GetString(condition.ConditionType.CommonName);
			if (formatString == null)
			{
				throw new InvalidOperationException(
					string.Format(
						CultureInfo.CurrentCulture,
						"Resource missing in CampaignPlugin.resx resource file, for condition with common name {0}.",
						condition.ConditionType.CommonName));
			}

			var conditionArguments = condition.GetInfoArguments();
			int currencyId;
			if (conditionArguments.Length > 1 && conditionArguments[1] != null && int.TryParse(conditionArguments[1].ToString(), out currencyId))
			{
				conditionArguments[1] = Currencies[currencyId] ?? conditionArguments[1];
			}

			return string.Format(
				CultureInfo.CurrentCulture,
				formatString,
				conditionArguments);
		}
	}
}