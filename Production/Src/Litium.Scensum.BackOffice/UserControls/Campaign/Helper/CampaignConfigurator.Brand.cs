using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Foundation;
using CustomButton = Litium.Scensum.Web.Controls.Button.ImageLinkButton;
using GridControl = System.Web.UI.WebControls.GridView;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Helper
{
	public class CampaignConfiguratorBrand
	{
		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		private CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		private BrandCollection _brandCollection;
		public BrandCollection BrandCollection
		{
			get { return _brandCollection ?? (_brandCollection = IoC.Resolve<IBrandSecureService>().GetAll()); }
		}

		public GridControl Grid { get; set; }
		public GridControl SearchGrid { get; set; }
		public CustomButton OkButton { get; set; }
		public CustomButton RemoveSelectionGridButton { get; set; }
		public UpdatePanel GridUpdatePanel { get; set; }
		public UpdatePanel PopupUpdatePanel { get; set; }
		public HtmlGenericControl ApplyToAllDiv { get; set; }
		public CheckBox IncludeAllCheckbox { get; set; }
		public BrandIdDictionary Brands { get; set; }

		public void SetEventHandlers()
		{
			Grid.RowDataBound += GridRowDataBound;
			Grid.RowCommand += GridRowCommand;

			SearchGrid.RowDataBound += SearchGridRowDataBound;

			OkButton.Click += OkButtonClick;
			RemoveSelectionGridButton.Click += RemoveSelectionGridButtonClick;
		}

		public void DataBind(BrandIdDictionary brands)
		{
			DataBindSearchGrid();
			DataBindGrid(CampaignConfiguratorHelper.ResolveBrands(brands));
		}

		public virtual void DataBindGrid(Collection<IBrand> brands)
		{
			Grid.DataSource = brands;
			Grid.DataBind();
			GridUpdatePanel.Update();
		}

		protected virtual void DataBindSearchGrid()
		{
			SearchGrid.DataSource = BrandCollection;
			SearchGrid.DataBind();
			PopupUpdatePanel.Update();
		}

		protected virtual void GridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ApplyToAllDiv, false);
		}

		protected virtual void GridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveBrand"))
			{
				RemoveBrand(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void SearchGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, null, false);
		}

		protected virtual void OkButtonClick(object sender, EventArgs e)
		{
			var brandIds = CampaignConfiguratorHelper.GetSelectedIdsFromGrid(SearchGrid, "IdHiddenField");

			foreach (var brandId in brandIds)
			{
				if (!Brands.ContainsKey(brandId))
				{
					Brands.Add(brandId);
				}
			}

			DataBindGrid(CampaignConfiguratorHelper.ResolveBrands(Brands));

			if (IncludeAllCheckbox != null && Brands.Count > 0)
			{
				IncludeAllCheckbox.Checked = false;
			}

			CampaignConfiguratorHelper.ClearSelectedIdsFromGrid(SearchGrid);
		}

		protected virtual void RemoveSelectionGridButtonClick(object sender, EventArgs e)
		{
			foreach (var brandId in GetSelectedFromGrid())
			{
				Brands.Remove(brandId);
			}

			DataBindGrid(CampaignConfiguratorHelper.ResolveBrands(Brands));
		}

		protected virtual void RemoveBrand(int brandId)
		{
			Brands.Remove(brandId);

			DataBindGrid(CampaignConfiguratorHelper.ResolveBrands(Brands));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedFromGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(Grid, "IdHiddenField");
		}
	}
}