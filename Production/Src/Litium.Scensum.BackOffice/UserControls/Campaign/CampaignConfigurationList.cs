using System;
using System.Collections.ObjectModel;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
    public abstract class CampaignConfigurationList<T> : StateUserControlController<Collection<T>>
    {
        public event EventHandler<CommandEventArgs> ListItemEdit;
        public event EventHandler<CommandEventArgs> ListItemRemove;

        protected virtual void OnListItemEdit(CommandEventArgs e)
        {
            if (ListItemEdit != null)
            {
                ListItemEdit(this, e);
            }
        }

        protected virtual void OnListItemRemove(CommandEventArgs e)
        {
            if (ListItemRemove != null)
            {
                ListItemRemove(this, e);
            }
        }

        public abstract void DataBind(Collection<T> items);
    }
}