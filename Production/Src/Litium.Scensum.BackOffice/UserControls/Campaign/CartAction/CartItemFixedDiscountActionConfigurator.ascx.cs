﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.UserControls.Campaign.Helper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemFixedDiscountActionConfigurator : CartActionControl<ICartItemFixedDiscountAction>
	{
		private readonly Collection<ICategory> _categoriesPool = new Collection<ICategory>();

		private CampaignConfiguratorHelper _campaignConfiguratorHelper;
		protected CampaignConfiguratorHelper CampaignConfiguratorHelper
		{
			get { return _campaignConfiguratorHelper ?? (_campaignConfiguratorHelper = new CampaignConfiguratorHelper()); }
		}

		private BrandCollection _brandCollection;
		public BrandCollection BrandCollection
		{
			get { return _brandCollection ?? (_brandCollection = IoC.Resolve<IBrandSecureService>().GetAll()); }
		}

		protected override void SetEventHandlers()
		{
			IncludeAllProductsCheckbox.CheckedChanged += OnIncludeAllProductsCheckboxChanged;
			ProductGridPageSizeSelect.SelectedIndexChanged += ProductGridPageSizeSelectSelectedIndexChanged;
			SetIncludeProductEventHandlers();
			SetExcludeProductEventHandlers();
			SetIncludeCategoryEventHandlers();
			SetExcludeCategoryEventHandlers();
			SetIncludeBrandEventHandlers();
			SetExcludeBrandEventHandlers();
		}

		protected override void PopulateControl() { }

		public override void DataBind(ICartAction dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var action = (ICartItemFixedDiscountAction) dataSource;
			State = action;

			AmountsManager.DataSource = State.Amounts;
			AmountsManager.DataBind();
			DiscountTypeTextBox.Text = action.ActionType.Title;
			IncludeAllProductsCheckbox.Checked = action.IncludeAllProducts;
			DataBindIncludeProducts(action);
			DataBindExcludeProducts(action);
			DataBindIncludeCategories(action);
			DataBindExcludeCategories(action);
			DataBindIncludeBrands(action);
			DataBindExcludeBrands(action);

			TargetProductTypeSelectorControl.DataSource = action.TargetProductTypes;
			TargetProductTypeSelectorControl.DataBind();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
			RegisterCollapsiblePanelScripts();
			SetVisibility();
		}

		protected virtual void OnIncludeAllProductsCheckboxChanged(object sender, EventArgs e)
		{
			if (IncludeAllProductsCheckbox.Checked)
			{
				DataBindProductIncludeGrid(new ProductIdDictionary());
				DataBindCategoryIncludeGrid(new Collection<ICategory>());
				DataBindBrandIncludeGrid(new Collection<IBrand>());
			}
			else
			{
				DataBindProductIncludeGrid(State.IncludeProducts);
				DataBindCategoryIncludeGrid(CampaignConfiguratorHelper.ResolveCategories(State.IncludeCategories));
				DataBindBrandIncludeGrid(CampaignConfiguratorHelper.ResolveBrands(State.IncludeBrands));
			}
		}

		protected virtual void ProductGridPageSizeSelectSelectedIndexChanged(object sender, EventArgs e)
		{
			ProductIncludeGrid.PageIndex = 0;
			DataBindProductIncludeGrid(State.IncludeProducts);

			ProductExcludeGrid.PageIndex = 0;
			DataBindProductExcludeGrid(State.ExcludeProducts);
		}

		public override bool TryGet(out ICartAction entity)
		{
			if (IsValid())
			{
				if (IncludeAllProductsCheckbox.Checked)
				{
					State.IncludeProducts.Clear();
					State.IncludeCategories.Clear();
					State.IncludeBrands.Clear();
				}

				var action = State;
				action.Amounts = AmountsManager.CollectCurrencyValues();
				action.IncludeAllProducts = IncludeAllProductsCheckbox.Checked;

				action.TargetProductTypes = TargetProductTypeSelectorControl.CollectSelectedValues();

				entity = action;
				return true;
			}

			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;
			Collection<string> amountsValidationMessages;
			if (!AmountsManager.IsValid(out amountsValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in amountsValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			Collection<string> targetProductTypesValidationMessages;
			if (!TargetProductTypeSelectorControl.IsValid(out targetProductTypesValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in targetProductTypesValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}
		
		private Collection<ICategory> _categories;
		protected string GetCategory(int categoryId)
		{
			if (_categories == null)
			{
				_categories = new Collection<ICategory>();
			}

			var category = _categories.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				category = categorySecureService.GetById(categoryId);
				_categories.Add(category);
			}
			return category.Title;
		}

		private Collection<IProductStatus> _productStatuses;
		protected virtual string GetStatus(object statusId)
		{
			if (_productStatuses == null)
			{
				_productStatuses = IoC.Resolve<IProductStatusSecureService>().GetAll();
			}

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _productStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		private Collection<IProductType> _productTypes;
		protected virtual string GetProductType(object product)
		{
			var lekmerProduct = (ILekmerProduct)product;

			if (_productTypes == null)
			{
				_productTypes = IoC.Resolve<IProductTypeSecureService>().GetAll();
			}

			var type = _productTypes.FirstOrDefault(s => s.Id == lekmerProduct.ProductTypeId);
			return type != null ? type.Title : string.Empty;
		}

		private void SetVisibility()
		{
			ProductIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductIncludeGrid) ? "block" : "none";
			CategoryIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryIncludeGrid) ? "block" : "none";
			BrandIncludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandIncludeGrid) ? "block" : "none";

			ProductExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(ProductExcludeGrid) ? "block" : "none";
			CategoryExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(CategoryExcludeGrid) ? "block" : "none";
			BrandExcludeApplyToAllSelectedDiv.Style["display"] = CampaignConfiguratorHelper.HasAnySelectedItem(BrandExcludeGrid) ? "block" : "none";

			IncludePanel.Style["display"] = ProductIncludeGrid.Rows.Count > 0 || CategoryIncludeGrid.Rows.Count > 0 || BrandIncludeGrid.Rows.Count > 0 ? "block" : "none";
			ExcludePanel.Style["display"] = ProductExcludeGrid.Rows.Count > 0 || CategoryExcludeGrid.Rows.Count > 0 || BrandExcludeGrid.Rows.Count > 0 ? "block" : "none";
		}

		protected virtual void RegisterClientScript()
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmProductRemove", "function ConfirmProductRemove() {return DeleteConfirmation('" + Resources.Product.Literal_product + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmCategoryRemove", "function ConfirmCategoryRemove() {return DeleteConfirmation('" + Resources.Product.Literal_CategoryLowercase + "');}", true);
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_confirmBrandRemove", "function ConfirmBrandRemove() {return DeleteConfirmation('" + Resources.Product.Literal_DeleteBrand + "');}", true);

			const string script = "$(function() {" +
								"ResizePopup('ActionPopupDiv', 0.8, 0.8,  0.06);" +
								"ResizePopup('ProductIncludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('ProductExcludePopupDiv', 0.8, 0.7, 0.07);" +
								"ResizePopup('CategoryIncludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('CategoryExcludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('BrandIncludePopupDiv', 0.4, 0.45, 0.09);" +
								"ResizePopup('BrandExcludePopupDiv', 0.4, 0.45, 0.09);" +
								"});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void RegisterCollapsiblePanelScripts()
		{
			string includeCollapsiblescript = "OpenClose('" + IncludePanelDiv.ClientID + "','" + IncludePanelStateHidden.ClientID + "','" + IncludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			IncludeCollapsibleDiv.Attributes.Add("onclick", includeCollapsiblescript);
			IncludeCollapsibleCenterDiv.Attributes.Add("onclick", includeCollapsiblescript);

			string excludeCollapsiblescript = "OpenClose('" + ExcludePanelDiv.ClientID + "','" + ExcludePanelStateHidden.ClientID + "','" + ExcludePanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			ExcludeCollapsibleDiv.Attributes.Add("onclick", excludeCollapsiblescript);
			ExcludeCollapsibleCenterDiv.Attributes.Add("onclick", excludeCollapsiblescript);

			string targetCollapsiblescript = "OpenClose('" + TargetPanelDiv.ClientID + "','" + TargetPanelStateHidden.ClientID + "','" + TargetPanelIndicatorImage.ClientID + "','" + ResolveClientUrl(CampaignConfiguratorHelper.DOWN_IMG) + "','" + ResolveClientUrl(CampaignConfiguratorHelper.UP_IMG) + "');";
			TargetCollapsibleDiv.Attributes.Add("onclick", targetCollapsiblescript);
		}

		protected virtual string GetCategoryPath(object cat)
		{
			var category = cat as ICategory;
			if (category == null) return string.Empty;

			var path = new StringBuilder(category.Title);
			while (category.ParentCategoryId.HasValue)
			{
				path.Insert(0, " \\ ");
				var parent = RetrieveCategory(category.ParentCategoryId.Value);
				path.Insert(0, parent.Title);
				category = parent;
			}
			return path.ToString();
		}

		protected virtual ICategory RetrieveCategory(int categoryId)
		{
			var category = _categoriesPool.FirstOrDefault(c => c.Id == categoryId);
			if (category == null)
			{
				category = IoC.Resolve<ICategorySecureService>().GetById(categoryId);
				_categoriesPool.Add(category);
			}
			return category;
		}
	}
}