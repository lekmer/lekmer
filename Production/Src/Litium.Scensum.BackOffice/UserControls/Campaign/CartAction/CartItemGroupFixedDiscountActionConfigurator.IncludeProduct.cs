﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Assortment.Products;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class CartItemGroupFixedDiscountActionConfigurator
	{
		private void SetIncludeProductEventHandlers()
		{
			ProductIncludePopupSearchButton.Click += OnProductIncludeSearch;
			ProductIncludeGrid.PageIndexChanging += ProductIncludeGridPageIndexChanging;
			ProductIncludeGrid.RowDataBound += OnProductIncludeGridRowDataBound;
			ProductIncludeGrid.RowCommand += OnProductIncludeGridRowCommand;
			ProductIncludePopupOkButton.Click += OnIncludeProducts;
			ProductIncludePopupAddAllButton.Click += OnIncludeAllProducts;
			RemoveSelectionFromProductIncludeGridButton.Click += OnRemoveSelectionFromProductIncludeGrid;
		}

		private void DataBindIncludeProducts(ICartItemGroupFixedDiscountAction action)
		{
			DataBindProductIncludeGrid(action.IncludeProducts);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId = "criteria")]
		protected virtual void OnProductIncludeSearch(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				ProductIncludeSearchResultControl.DataBind(criteria);
			}
		}

		protected virtual void ProductIncludeGridPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ProductIncludeGrid.PageIndex = e.NewPageIndex;
			DataBindProductIncludeGrid(State.IncludeProducts);
		}

		protected virtual void OnProductIncludeGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			CampaignConfiguratorHelper.SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row, ProductIncludeApplyToAllSelectedDiv, false);
		}

		protected virtual void OnProductIncludeGridRowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("RemoveProduct"))
			{
				RemoveIncludedProduct(int.Parse(e.CommandArgument.ToString(), CultureInfo.CurrentCulture));
			}
		}

		protected virtual void RemoveIncludedProduct(int productId)
		{
			State.IncludeProducts.Remove(productId);
			DataBindProductIncludeGrid(State.IncludeProducts);
		}

		protected virtual void OnIncludeProducts(object sender, EventArgs e)
		{
			var products = ProductIncludeSearchResultControl.GetSelectedProducts();
			foreach (var product in products)
			{
				if (!State.IncludeProducts.ContainsKey(product.Id))
				{
					State.IncludeProducts.Add(product.Id);
				}
			}
			DataBindProductIncludeGrid(State.IncludeProducts);
			if (products.Count > 0)
			{
				IncludeAllProductsCheckbox.Checked = false;
			}
		}

		private void OnIncludeAllProducts(object sender, EventArgs e)
		{
			if (ProductIncludeSearchFormControl.Validate())
			{
				var productSecureService = IoC.Resolve<IProductSecureService>();

				var criteriaList = new List<IProductSearchCriteria>();
				var criteria = ProductIncludeSearchFormControl.CreateSearchCriteria();
				criteriaList.Add(criteria);
				var searchCriteriaInclusive = ProductDataSource.ConvertToXml(criteriaList);
				var searchCriteriaExclusive = ProductDataSource.ConvertToXml(new List<IProductSearchCriteria>());

				var products = productSecureService.AdvancedSearch(
					ChannelHelper.CurrentChannel.Id,
					searchCriteriaInclusive,
					searchCriteriaExclusive,
					1,
					int.MaxValue);

				foreach (var product in products)
				{
					if (!State.IncludeProducts.ContainsKey(product.Id))
					{
						State.IncludeProducts.Add(product.Id);
					}
				}
				DataBindProductIncludeGrid(State.IncludeProducts);
				if (products.Count > 0)
				{
					IncludeAllProductsCheckbox.Checked = false;
				}
			}
		}

		protected virtual void OnRemoveSelectionFromProductIncludeGrid(object sender, EventArgs e)
		{
			foreach (var productId in GetSelectedProductsFromIncludeGrid())
			{
				State.IncludeProducts.Remove(productId);
			}
			DataBindProductIncludeGrid(State.IncludeProducts);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<int> GetSelectedProductsFromIncludeGrid()
		{
			return CampaignConfiguratorHelper.GetSelectedIdsFromGrid(ProductIncludeGrid, "IdHiddenField");
		}

		protected virtual void DataBindProductIncludeGrid(ProductIdDictionary productIds)
		{
			ProductIncludeGrid.PageSize = Convert.ToInt32(ProductGridPageSizeSelect.SelectedValue);

			IncludeProductDataSource.SelectParameters.Clear();
			IncludeProductDataSource.SelectParameters.Add("maximumRows", ProductGridPageSizeSelect.SelectedValue.ToString(CultureInfo.CurrentCulture));
			IncludeProductDataSource.SelectParameters.Add("startRowIndex", ProductIncludeGrid.PageIndex.ToString(CultureInfo.CurrentCulture));
			IncludeProductDataSource.SelectParameters.Add("productIdsString", Foundation.Convert.ToStringIdentifierList(productIds.Keys.ToList()));

			ProductIncludeGrid.DataBind();
			ProductIncludeGridUpdatePanel.Update();
		}
	}
}