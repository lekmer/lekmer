﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.CartAction
{
	public partial class GiftCardViaMailCartActionConfigurator : CartActionControl<IGiftCardViaMailCartAction>
	{
		protected override void SetEventHandlers(){}

		protected override void PopulateControl(){}

		public override void DataBind(ICartAction dataSource)
		{
			if (dataSource == null)
			{
				throw new ArgumentNullException("dataSource");
			}

			var action = (IGiftCardViaMailCartAction) dataSource;

			DiscountTypeTextBox.Text = action.ActionType.Title;
			SendingIntervalTextBox.Text = action.SendingInterval.ToString(CultureInfo.CurrentCulture);
			BindTemplates(action.TemplateId);

			CurrencyValueManager.DataSource = action.Amounts;
			CurrencyValueManager.DataBind();

			State = action;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			RegisterClientScript();
		}

		public override bool TryGet(out ICartAction entity)
		{
			if (IsValid())
			{
				var action = State;

				action.SendingInterval = int.Parse(SendingIntervalTextBox.Text, CultureInfo.CurrentCulture);
				int templateId;
				action.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
				action.Amounts = CurrencyValueManager.CollectCurrencyValues();

				entity = action;
				return true;
			}

			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			Collection<string> validationMessages;
			if (!CurrencyValueManager.IsValid(out validationMessages))
			{
				isValid = false;
				foreach (string validationMessage in validationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			int sendingInterval;
			if (!int.TryParse(SendingIntervalTextBox.Text, out sendingInterval))
			{
				isValid = false;
				ValidationMessages.Add(Resources.CampaignMessage.SendingIntervalInvalid);
			}
			else if (sendingInterval < 0 || sendingInterval > int.MaxValue)
			{
				isValid = false;
				ValidationMessages.Add(string.Format(Resources.CampaignMessage.SendingIntervalInvalidRange, int.MaxValue));
			}

			return isValid;
		}

		protected virtual void RegisterClientScript()
		{
			const string script = "$(function() {ResizePopup('ActionPopupDiv', 0.45, 0.55,  0.09);});";
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "js_ppdc_popupResize", script, true);
		}

		protected virtual void BindTemplates(int? templateId)
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("GiftCardViaEmail");
			TemplateList.DataBind();

			var listItem = new ListItem(string.Empty, string.Empty);
			TemplateList.Items.Insert(0, listItem);

			if (templateId.HasValue)
			{
				var item = TemplateList.Items.FindByValue(templateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}
	}
}