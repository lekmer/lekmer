﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreightValueActionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.FreightValueActionConfigurator" %>
<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>
<%@ Register TagPrefix="scensum" TagName="DeliveryMethodsSelector" Src="~/UserControls/Common/DeliveryMethodsSelector.ascx" %>

<br style="clear:both;" />

<div class="left">
	<scensum:CurrencyValueManager runat="server" id="AmountsManager" />
</div>

<div class="left">
	<scensum:DeliveryMethodsSelector runat="server" id="DeliveryMethodsSelectorControl" />
</div>
