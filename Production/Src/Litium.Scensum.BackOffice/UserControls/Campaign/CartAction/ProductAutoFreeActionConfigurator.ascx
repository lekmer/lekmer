﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="ProductAutoFreeActionConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.CartAction.ProductAutoFreeActionConfigurator" %>

<%@ Import Namespace="System.Globalization"%>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type="text/javascript">
	function confirmDeleteProducts() {
		return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
	}
</script>

<div class="campaign-full-left">
	<div class="input-box campaign-action-value">
		<asp:Label ID="MaxQuantityLabel" Text="<%$Resources:CartItemPriceAction, Literal_MaxQuantity%>" runat="server"/>
		<br />
		<asp:TextBox ID="MaxQuantityTextBox" runat="server"/>
	</div>
</div>

<br class="clear" /><br />

<div class="campaign-full-left">

<!-- Include Toolbar Start -->

	<div id="includeToolbar">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.gif" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenIncludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content">
			<asp:Panel ID="IncludePanel" runat="server">
				<div style="padding-top:15px;clear:both;"></div>
				<asp:Label ID="LabelProducts" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold" />
				<br />
				<asp:UpdatePanel ID="ProductIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<sc:GridViewWithCustomPager
							ID="ProductIncludeGrid"
							SkinID="grid"
							runat="server"
							AllowPaging="true"
							PageSize="250"
							AutoGenerateColumns="false"
							Width="100%"
							DataSourceID="IncludeProductDataSource">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="49%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Label_Categories %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TypeLiteral" Text='<%# GetProductType(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price  %>" HeaderStyle-HorizontalAlign="Right"
									ItemStyle-HorizontalAlign="Right" ItemStyle-Width="10%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="PriceLiteral" Text='<%# Convert.ToDecimal(Eval("Price.PriceIncludingVat")).ToString("C", CultureInfo.CurrentCulture) %>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmProductRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
						<asp:ObjectDataSource ID="IncludeProductDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />

						<div runat="server" id="ProductIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>

<!-- Include Toolbar End -->

</div>

<!-- Product Search Popup Start -->
	<!-- Include Start -->

<div id="ProductIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="ProductIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="ProductIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="campaign-product-search">
					<div class="content-box">
						<asp:Panel ID="SearchPanel" DefaultButton="ProductIncludePopupSearchButton" runat="server">
						<uc:ProductSearchForm ID="ProductIncludeSearchFormControl" runat="server" />
						<br style="clear:both;" />
						<br />
						<div class="campaign-popup-buttons no-padding">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
						</div>
						</asp:Panel>
						<br class="clear"/>
						<div id="errorDiv" runat="server" Visible="False" style="color: red;">
							<span><%= Resources.CampaignMessage.IncorrectCountOfSelectedProduct %></span>
						</div>
						<br />
						<uc:ProductSearchResult ID="ProductIncludeSearchResultControl" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<span><%= Resources.CampaignMessage.FreeProductCountMessage %></span>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>

<ajaxToolkit:ModalPopupExtender 
	ID="ProductIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeProductSearchPopupButton"
	PopupControlID="ProductIncludePopupDiv" 
	CancelControlID="ProductIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	
	<!-- Include End -->
<!-- Product Search Popup End -->