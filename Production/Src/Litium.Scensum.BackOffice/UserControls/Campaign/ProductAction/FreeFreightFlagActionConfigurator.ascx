﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FreeFreightFlagActionConfigurator.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Campaign.ProductAction.FreeFreightFlagActionConfigurator" %>
<%@ Register TagPrefix="scensum" TagName="CurrencyValueManager" Src="~/UserControls/Common/CurrencyValueManager.ascx" %>

<br style="clear:both;" />
<span class="bold">
	<%= Resources.Lekmer.Literal_FreeFreightFlagActionTitle%>
</span>
<br style="clear:both;" />
<scensum:CurrencyValueManager runat="server" ID="ProductPricesManager" />