﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Scensum.BackOffice.UserControls.Campaign.Condition
{
	public partial class CartValueConditionConfigurator : ConditionControl<ICartValueCondition>
	{
		protected class CurrencyRepeaterDataSourceItem
		{
			public ICurrency Currency { get; set; }
			public string MonetaryValue { get; set; }
		}

		protected override void SetEventHandlers()
		{
		}

		protected override void PopulateControl()
		{
		}

		public override void DataBind(ICondition dataSource)
		{
			if (dataSource == null) throw new ArgumentNullException("dataSource");

			var condition = (ILekmerCartValueCondition) dataSource;

			CartValuesManager.DataSource = condition.CartValues;
			CartValuesManager.DataBind();

			State = condition;
		}

		public override bool TryGet(out ICondition entity)
		{
			if (IsValid())
			{
				var condition = (ILekmerCartValueCondition) State;
				condition.CartValues = CartValuesManager.CollectCurrencyValues();
				entity = condition;
				return true;
			}
			entity = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			Collection<string> cartValuesValidationMessages;
			if (!CartValuesManager.IsValid(out cartValuesValidationMessages))
			{
				isValid = false;
				foreach (string validationMessage in cartValuesValidationMessages)
				{
					ValidationMessages.Add(validationMessage);
				}
			}

			return isValid;
		}
	}
}
