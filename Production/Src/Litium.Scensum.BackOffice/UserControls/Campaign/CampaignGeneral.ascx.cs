using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Modules.Campaigns;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Scensum.BackOffice.UserControls.Campaign
{
	public partial class CampaignGeneral : StateUserControlController<CampaignGeneralState>
	{
		protected bool FieldsInitialized;
		private readonly StringBuilder _validationMessage = new StringBuilder();
		public string ValidationMessage { get { return _validationMessage.ToString(); } }

		public bool TryGet(out ICampaign campaign)
		{
			if (IsValid())
			{
				campaign = State.Campaign;
				campaign.Title = TitleTextBox.Text;
				campaign.CampaignStatus.Id = int.Parse(StatusList.SelectedValue, CultureInfo.CurrentCulture);
				campaign.StartDate = string.IsNullOrEmpty(StartDateTextBox.Text) ? null : (DateTime?)DateTime.Parse(StartDateTextBox.Text, CultureInfo.CurrentCulture);
				campaign.EndDate = string.IsNullOrEmpty(EndDateTextBox.Text) ? null : (DateTime?)DateTime.Parse(EndDateTextBox.Text, CultureInfo.CurrentCulture);
				campaign.Exclusive = ExclusiveCheckBox.Checked;
				return true;
			}
			campaign = null;
			return false;
		}

		protected virtual bool IsValid()
		{
			bool isValid = true;

			if(!IsValidTitle())
			{
				_validationMessage.Append(Resources.CampaignMessage.TitleInvalid);
				isValid = false;
			}

			if (!IsValidStartDate())
			{
				_validationMessage.Append(Resources.CampaignMessage.StartDateInvalid);
				isValid = false;
			}

			if (!IsValidEndDate())
			{
				_validationMessage.Append(Resources.CampaignMessage.EndDateInvalid);
				isValid = false;
			}

			if (!IsValidDateRange())
			{
				_validationMessage.Append(Resources.GeneralMessage.EndDateGreaterStartDate);
				isValid = false;
			}
			
			if (!IsValidFolder())
			{
				_validationMessage.Append(Resources.CampaignMessage.FolderInvalid);
				isValid = false;
			}

			return isValid;
		}

		protected virtual bool IsValidTitle()
		{
			string title = TitleTextBox.Text.Trim();

			if (string.IsNullOrEmpty(title)) return false;
			if (title.Length > 500) return false;

			return true;
		}

		protected virtual bool IsValidStartDate()
		{
			string dateTimeString = StartDateTextBox.Text.Trim();
			DateTime date;

			if (string.IsNullOrEmpty(dateTimeString)) return true;
			if (!DateTime.TryParse(dateTimeString, out date)) return false;

			return true;
		}

		protected virtual bool IsValidEndDate()
		{
			string dateTimeString = EndDateTextBox.Text.Trim();
			DateTime date;

			if (string.IsNullOrEmpty(dateTimeString)) return true;
			if (!DateTime.TryParse(dateTimeString, out date)) return false;

			return true;
		}

		protected virtual bool IsValidDateRange()
		{
			DateTime startDate;
			DateTime endDate;

			if (DateTime.TryParse(StartDateTextBox.Text.Trim(), out startDate)
				&& DateTime.TryParse(EndDateTextBox.Text.Trim(), out endDate)
				&& endDate < startDate) return false;
			
			return true;
		}

		protected virtual bool IsValidFolder()
		{
			return State.Campaign.FolderId > 0;
		}

		public virtual void DataBind(ICampaign campaign, CampaignType campaignType)
		{
			if (!FieldsInitialized)
			{
				PopulateControl();
			}

			TitleTextBox.Text = campaign.Title;
			StatusList.SelectedValue = campaign.CampaignStatus.Id.ToString(CultureInfo.CurrentCulture);
			ExclusiveCheckBox.Checked = campaign.Exclusive;
			StartDateTextBox.Text = campaign.StartDate.HasValue
										? campaign.StartDate.Value.ToString(CultureInfo.CurrentCulture)
										: string.Empty;
			EndDateTextBox.Text = campaign.EndDate.HasValue
										? campaign.EndDate.Value.ToString(CultureInfo.CurrentCulture)
										: string.Empty;

			State.Campaign = campaign;
			State.CurrentCampaignType = campaignType;

			PopulatePath(campaign);
		}

		protected override void SetEventHandlers()
		{
			FolderSelector.NodeCommand += OnFolderSelectorCommand;
			FolderSelector.SelectedFolderChangedEvent += OnSelectedFolderChanged;
		}

		protected override void PopulateControl()
		{
			InitializeState();
			InitializeStatusList();
			FieldsInitialized = true;
		}

		protected virtual void InitializeStatusList()
		{
			StatusList.DataSource = GetCampaignStatusCollection();
			StatusList.DataValueField = "Id";
			StatusList.DataTextField = "CommonName";
			StatusList.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<ICampaignStatus> GetCampaignStatusCollection()
		{
			return IoC.Resolve<ICampaignStatusSecureService>().GetAll();
		}

		protected virtual void OnFolderSelectorCommand(object sender, CommandEventArgs e)
		{
			FolderSelector.DataSource = GetPathSource(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
			FolderSelector.DataBind();
		}

		protected virtual void OnSelectedFolderChanged(object sender, Media.Events.SelectedFolderChangedEventArgs e)
		{
			State.Campaign.FolderId = e.SelectedFolderId.Value;
		}

		protected virtual void PopulatePath(ICampaign campaign)
		{
			var parentId = campaign.FolderId == 0 ? null : (int?)campaign.FolderId;
			FolderSelector.SelectedNodeId = parentId;
			FolderSelector.DataSource = GetPathSource(parentId);
			FolderSelector.DataBind();
			FolderSelector.PopulatePath(parentId);
		}

		protected virtual IEnumerable<INode> GetPathSource(int? folderId)
		{
			var service = IoC.Resolve<ICampaignFolderSecureService>();
			return State.CurrentCampaignType == CampaignType.ProductCampaign 
				? service.GetProductCampaignFolderTree(folderId)
				: service.GetCartCampaignFolderTree(folderId);
		}

		protected virtual void InitializeState()
		{
			if (State == null)
			{
				State = new CampaignGeneralState();
			}
		}
	}

	[Serializable]
	public sealed class CampaignGeneralState
	{
		public ICampaign Campaign { get; set; }
		public CampaignType CurrentCampaignType { get; set; }
	}
}