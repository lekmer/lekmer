﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.UserControls.Lekmer
{
	public class LekmerMasterPageProcessor : ILekmerSelectorProcessor
	{
		private IEnumerable<IContentPage> _selectablePages;
		private int SiteStructureRegistryId { get; set; }

		public LekmerMasterPageProcessor(int siteStructureRegistryId)
		{
			SiteStructureRegistryId = siteStructureRegistryId;
		}

		public virtual bool IsAvailable(ISiteStructureNode node)
		{
			return node.IsMasterPage;
		}

		public IEnumerable<ISiteStructureNode> GetDataSource(int? selectedId)
		{
			return PopulateMasterPages();
		}

		private IEnumerable<ISiteStructureNode> PopulateMasterPages()
		{
			var masterPages = GetMasterPages();
			IEnumerable<ISiteStructureNode> selectedNodes = new Collection<ISiteStructureNode>();
			var contentNodeService = IoC.Resolve<IContentNodeSecureService>();
			foreach (var page in masterPages)
			{
				selectedNodes = selectedNodes.Union(contentNodeService.GetTree(page.Id, false));
			}
			var result = new Collection<ISiteStructureNode>();
			foreach (var node in selectedNodes)
			{
				var nodeId = node.Id;
				if (result.Any(n => n.Id == nodeId)) continue;
				node.HasChildren = selectedNodes.Any(n => n.ParentId == nodeId);
				result.Add(node);
			}
			return result;
		}

		private IEnumerable<IContentPage> GetMasterPages()
		{
			if (_selectablePages == null)
			{
				_selectablePages = IoC.Resolve<IContentPageSecureService>().GetAllMaster(SiteStructureRegistryId);
			}
			return _selectablePages;
		}
	}
}
