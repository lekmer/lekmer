﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.UrlHistory
{
	public partial class ContentPageUrlHistoryControl : StateUserControlController<UrlHistoryState>
	{
		private Collection<IContentPageUrlHistory> _contentPageUrlHistoryList;


		protected virtual UrlHistoryState SafeState
		{
			get
			{
				return State ?? (State = new UrlHistoryState());
			}
		}
		public int ContentPageId
		{
			get
			{
				return SafeState.BusinessObjectId;
			}
			set
			{
				SafeState.BusinessObjectId = value;
			}
		}
		public ImageButton TriggerImageButton
		{
			get
			{
				return UrlHistoryButton;
			}
		}


		public void DataBindManual()
		{
			base.DataBind();

			SetUrlHistory();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			UrlHistoryPopup.BehaviorID = ClientID + "_urlHistoryPopup";
			UrlHistoryPopup.PopupControlID = UrlHistoryDiv.ClientID;

			var clientScript = Page.ClientScript;

			if (!clientScript.IsClientScriptBlockRegistered("closePopup"))
			{
				clientScript.RegisterClientScriptBlock(Page.GetType(), "closePopup", @"
					function closePopup(b) {
						$find(b).hide();
						return false;
					};
				", true);
			}
		}
		protected void Cancel(object sender, EventArgs e)
		{
			DataBindManual();

			ClosePopup();
		}
		protected void AddNewOldUrlHistory(object sender, EventArgs e)
		{
			Collection<IContentPageUrlHistory> items = GetUrlHistoryItems(false);

			var newOldUrlTitle = NewOldUrlTitle.Text;

			if (newOldUrlTitle.IsEmpty())
			{
				return;
			}

			if (items.FirstOrDefault(u => u.UrlTitleOld == newOldUrlTitle) == null)
			{
				var newUrlHistory = IoC.Resolve<IContentPageUrlHistorySecureService>().Create(ContentPageId, newOldUrlTitle, (int) HistoryLifeIntervalType.Month);
				items.Add(newUrlHistory);

				_contentPageUrlHistoryList = items;

				DataBindManual();
			}

			NewOldUrlTitle.Text = string.Empty;
		}
		protected void Delete(object sender, EventArgs e)
		{
			var imageButton = (ImageButton)sender;
			var urlHistoryIdControl = imageButton.Parent.FindControl("UrlHistoryId") as HiddenField;
			if (urlHistoryIdControl == null)
			{
				return;
			}

			int urlHistoryId;
			if (!int.TryParse(urlHistoryIdControl.Value, out urlHistoryId))
			{
				return;
			}

			Collection<IContentPageUrlHistory> items = GetUrlHistoryItems(false);

			if (urlHistoryId > 0)
			{
				IoC.Resolve<IContentPageUrlHistorySecureService>().DeleteById(SignInHelper.SignedInSystemUser, urlHistoryId);
				_contentPageUrlHistoryList = new Collection<IContentPageUrlHistory>(items.Where(u => u.Id != urlHistoryId).ToList());
			}
			else
			{
				var urlHistoryControl = imageButton.Parent.FindControl("OldUrlTitle") as TextBox;
				if (urlHistoryControl == null)
				{
					return;
				}

				string urlTitle = urlHistoryControl.Text;

				_contentPageUrlHistoryList = new Collection<IContentPageUrlHistory>(items.Where(u => (u.UrlTitleOld != urlTitle)).ToList());
			}

			DataBindManual();
		}
		protected void Save(object sender, EventArgs e)
		{
			Collection<IContentPageUrlHistory> items = GetUrlHistoryItems(true);

			if (items.Count > 0)
			{
				IoC.Resolve<IContentPageUrlHistorySecureService>().Save(SignInHelper.SignedInSystemUser, items);

				_contentPageUrlHistoryList = null;
				DataBindManual();
			}

			ClosePopup();
		}
		protected override void DataBindChildren()
		{
			SetUrlHistory();

			base.DataBindChildren();
		}


		protected virtual void SetUrlHistory()
		{
			UrlHistoryRepeater.DataSource = GetAllUrlHistoryByContentPage();
			UrlHistoryRepeater.DataBind();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<IContentPageUrlHistory> GetAllUrlHistoryByContentPage()
		{
			return _contentPageUrlHistoryList ?? (_contentPageUrlHistoryList = IoC.Resolve<IContentPageUrlHistorySecureService>().GetAllByContentPage(ContentPageId));
		}

		protected virtual Collection<IContentPageUrlHistory> GetUrlHistoryItems(bool getOnlyNew)
		{
			var items = new Collection<IContentPageUrlHistory>();

			foreach (RepeaterItem repeaterItem in UrlHistoryRepeater.Items)
			{
				if (repeaterItem.ItemType != ListItemType.Item && repeaterItem.ItemType != ListItemType.AlternatingItem)
				{
					continue;
				}

				var urlHistory = GetUrlHistory(repeaterItem, getOnlyNew);
				if (urlHistory != null)
				{
					items.Add(urlHistory);
				}
			}

			return items;
		}

		protected virtual IContentPageUrlHistory GetUrlHistory(Control item, bool getOnlyNew)
		{
			var urlHistoryIdControl = item.FindControl("UrlHistoryId") as HiddenField;
			if (urlHistoryIdControl == null)
			{
				return null;
			}

			int urlHistoryId;
			if (!int.TryParse(urlHistoryIdControl.Value, out urlHistoryId))
			{
				return null;
			}

			if (getOnlyNew && urlHistoryId > 0) // if urlHistoryId > 0 => this is new item.
			{
				return null;
			}

			var urlHistoryControl = item.FindControl("OldUrlTitle") as TextBox;
			if (urlHistoryControl == null)
			{
				return null;
			}

			string urlTitle = urlHistoryControl.Text;

			var contentPageUrlHistory = IoC.Resolve<IContentPageUrlHistorySecureService>().Create(ContentPageId, urlTitle, (int)HistoryLifeIntervalType.Month);
			contentPageUrlHistory.Id = urlHistoryId;

			return contentPageUrlHistory;
		}

		protected override void SetEventHandlers()
		{
		}
		protected override void PopulateControl()
		{
		}

		private void ClosePopup()
		{
			NewOldUrlTitle.Text = string.Empty;

			string startupScript = "closePopup('" + UrlHistoryPopup.BehaviorID + "')";
			ScriptManager.RegisterStartupScript(UrlHistoryUpdatePanel, GetType(), "close", startupScript, true);
		}
	}
}