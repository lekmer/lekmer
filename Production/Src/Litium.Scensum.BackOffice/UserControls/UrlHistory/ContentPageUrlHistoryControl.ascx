﻿<%@ Control Language="C#" CodeBehind="ContentPageUrlHistoryControl.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.UrlHistory.ContentPageUrlHistoryControl" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:ImageButton runat="server" ID="UrlHistoryButton" ImageUrl="~/Media/Images/Assortment/add-product-image.png" AlternateText="Url Title History" />
<ajaxToolkit:ModalPopupExtender ID="UrlHistoryPopup" runat="server" PopupControlID="UrlHistoryDiv" BackgroundCssClass="popup-background" Y="100" TargetControlID="UrlHistoryButton" />

<div id="UrlHistoryDiv" runat="server" class="translation-popup-container" style="z-index: 10010; display: none;">
	<div id="translation-popup-header">
		<div id="translation-popup-header-left">
		</div>
		<div id="translation-popup-header-center">
			<span>Url Title History</span>
			<asp:Button ID="CloseButton" runat="server" Text="x" OnClick="Cancel" />
		</div>
		<div id="translation-popup-header-right">
		</div>
	</div>

	<div id="url-history-popup-content">
		<asp:UpdatePanel ID="UrlHistoryUpdatePanel" runat="server" UpdateMode="Conditional">
			<ContentTemplate>
				<div id="SimpleDiv" runat="server">
					<div runat="server" id="UrlHistoryListDiv" style="overflow-y: scroll; max-height: 500px;">
						<div id="value-translate" class="clear translation-item">
							<div class="clear">&nbsp;</div>

							<div class="right" style="width: 90%;">
								<asp:Repeater ID="UrlHistoryRepeater" runat="server" >
									<ItemTemplate>
										<div id="value-translate">
											<asp:TextBox ID="OldUrlTitle" runat="server" Text='<%# Eval("UrlTitleOld") %>' ReadOnly="True" />
											<asp:HiddenField ID="UrlHistoryId" runat="server" Value='<%# Eval("Id") %>' />
											<asp:ImageButton runat="server" ID="DeleteButton" ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" OnClientClick='<%# "return DeleteConfirmation(\"" + Eval("UrlTitleOld") + "\");" %>' CssClass="url-history-delete-button" OnClick="Delete" />
										</div>
									</ItemTemplate>
								</asp:Repeater>
							</div>

							<div class="clear">&nbsp;</div>

							<div class="right" style="width: 90%;">
								<asp:TextBox ID="NewOldUrlTitle" runat="server" CssClass="left" style="margin-right: 5px;" />
								<uc:ImageLinkButton ID="AddButton" runat="server" Text="Add" SkinID="DefaultButton" OnClick="AddNewOldUrlHistory" />
							</div>

							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</div>
			</ContentTemplate>
			<Triggers>
				<asp:AsyncPostBackTrigger ControlID="CloseButton" />
				<asp:AsyncPostBackTrigger ControlID="SaveButton" />
				<asp:AsyncPostBackTrigger ControlID="CancelButton" />
			</Triggers>
		</asp:UpdatePanel>

		<div class="translation-popup-acton-button">
			<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Save" SkinID="DefaultButton" UseSubmitBehaviour="true" OnClick="Save" />
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="Cancel" SkinID="DefaultButton" OnClick="Cancel" />
		</div>
	</div>
</div>