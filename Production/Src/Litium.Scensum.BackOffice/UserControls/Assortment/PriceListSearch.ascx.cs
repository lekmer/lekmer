﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Assortment.Events;
using Litium.Scensum.BackOffice.UserControls.GridView;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class PriceListSearch : UserControlController
	{
		private Collection<IPriceListStatus> _pricelistStatuses;
		private Collection<ICurrency> _currencies;

		public event EventHandler<PriceListSelectEventArgs> SelectEvent;
		
		protected override void SetEventHandlers()
		{
			PricelistSearchGrid.PageIndexChanging += OnPageIndexChanging;
			PricelistSearchGrid.RowDataBound += OnGridRowDataBound;
			PricelistSearchGrid.DataBound += OnGridDataBound;

			AddButton.Click += OnAdd;
			CancelButton.ServerClick += OnCancel;
            SearchButton.Click += OnSearch;
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			PricelistSearchGrid.PageIndex = e.NewPageIndex;
		}

		protected virtual void OnGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((System.Web.UI.WebControls.GridView)sender, e.Row);
		}

		protected virtual void OnGridDataBound(object sender, EventArgs e)
		{
			AddButton.Enabled = PricelistSearchGrid.Rows.Count > 0;
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			PricelistSearchGrid.PageIndex = 0;
			PricelistDataSource.SelectParameters.Clear();
			PricelistDataSource.SelectParameters.Add("maximumRows", PricelistSearchGrid.PageSize.ToString(CultureInfo.CurrentCulture));
			PricelistDataSource.SelectParameters.Add("startRowIndex", (PricelistSearchGrid.PageIndex * PricelistSearchGrid.PageSize).ToString(CultureInfo.CurrentCulture));
			PricelistDataSource.SelectParameters.Add("title", TitleTextBox.Text);
			PricelistDataSource.SelectParameters.Add("currencyId", CurrencyList.SelectedValue);
			PricelistDataSource.SelectParameters.Add("statusId", StatusList.SelectedValue);
		}

		protected override void PopulateControl()
		{
			PrepareSearchData();
		}

		protected virtual void PrepareSearchData()
		{
			CurrencyList.DataSource = IoC.Resolve<ICurrencySecureService>().GetAll();
			CurrencyList.DataBind();
			CurrencyList.Items.Insert(0, new ListItem(string.Empty, string.Empty) {Selected = true});

			StatusList.DataSource = IoC.Resolve<IPriceListStatusSecureService>().GetAll();
			StatusList.DataBind();
			StatusList.Items.Insert(0, new ListItem(string.Empty, string.Empty) { Selected = true });
		}

		protected virtual void OnAdd(object sender, EventArgs e)
		{
			var ids = PricelistSearchGrid.GetSelectedIds();
			var pricelists = new Collection<IPriceList>();
			var service = IoC.Resolve<IPriceListSecureService>();
			foreach (var id in ids)
			{
				pricelists.Add(service.GetById(id));
			}

			if (pricelists.Count > 0)
			{
				PricelistDataSource.SelectParameters.Clear();
				SelectEvent(this, new PriceListSelectEventArgs{ PriceLists = pricelists });
				PricelistsSearchPopup.Hide();
			}
		}

		protected virtual void OnCancel(object sender, EventArgs e)
		{
			PricelistDataSource.SelectParameters.Clear();
		}

		protected virtual string GetPricelistStatus(object statusId)
		{
			if (_pricelistStatuses == null)
				_pricelistStatuses = IoC.Resolve<IPriceListStatusSecureService>().GetAll();

			int id = System.Convert.ToInt32(statusId, CultureInfo.CurrentCulture);
			var status = _pricelistStatuses.FirstOrDefault(s => s.Id == id);
			return status != null ? status.Title : string.Empty;
		}

		protected virtual string GetCurrency(object currencyId)
		{
			if (_currencies == null)
				_currencies = IoC.Resolve<ICurrencySecureService>().GetAll();

			int id = System.Convert.ToInt32(currencyId, CultureInfo.CurrentCulture);
			var currency = _currencies.FirstOrDefault(c => c.Id == id);
			return currency != null ? currency.Iso : string.Empty;
		}

		protected virtual void SetSelectionFunction(System.Web.UI.WebControls.GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox)row.FindControl("SelectAllCheckBox");
				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"')");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");
				if (cbSelect == null || selectAllCheckBox == null) return;
				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"')");
			}
		}
	}
}