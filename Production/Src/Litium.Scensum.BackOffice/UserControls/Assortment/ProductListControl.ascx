﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductListControl.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.ProductListControl" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="ProductSearch" Src="~/UserControls/Assortment/ProductSearch.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<script type="text/javascript">
	function confirmDeleteProducts() {
		return DeleteConfirmation("<%= Resources.Campaign.Literal_ProductsConfirmRemove %>");
	}
</script>

<asp:UpdatePanel ID="upProductList" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<Scensum:Grid runat="server" ID="Articles" FixedColumns="Type|Title">
			<sc:GridViewWithCustomPager
				ID="GVSave"
				SkinID="grid"
				runat="server"
				PageSize="<%$AppSettings:DefaultGridPageSize%>"
				AllowPaging="true"
				AutoGenerateColumns="false"
				Width="100%">
				<Columns>
					<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
						<HeaderTemplate>
							<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
						</HeaderTemplate>
						<ItemTemplate>
							<asp:CheckBox ID="SelectCheckBox" runat="server" />
							<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="<%$ Resources:General, Literal_ArtNo %>" DataField="ErpId" ItemStyle-Width="10%" />
					<asp:TemplateField HeaderText="Title" ItemStyle-Width="30%">
						<ItemTemplate>
							<uc:HyperLinkEncoded runat="server" ID="hlEdit" Text='<%# Eval("DisplayTitle")%>' NavigateUrl='<%# Litium.Scensum.BackOffice.Controller.PathHelper.Assortment.Product.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Category %>" ItemStyle-Width="25%">
						<ItemTemplate>
							<%# GetCategory((int)Eval("CategoryId"))%>
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField HeaderText="<%$ Resources:Product, Literal_Ean %>" DataField="EanCode" ItemStyle-Width="12%" />
					<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
						<ItemTemplate>
							<asp:Label ID="lblProductStatus" runat="server" />
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
						<ItemTemplate>
							<asp:Label ID="lblType" runat="server" />
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price  %>" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="5%">
						<ItemTemplate>
							<asp:Label ID="lblPrice" Text="" runat="server" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
						<ItemTemplate>
							<asp:ImageButton runat="server" ID="ibtnDelete" CommandName="DeleteProduct" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_product + "\");" %>' />
						</ItemTemplate>
					</asp:TemplateField>
				</Columns>
			</sc:GridViewWithCustomPager>
		</Scensum:Grid>
		<br style="clear: both;" />
		<div id="AllSelectedDiv" runat="server" style="display: none;">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectedButton" Text="<%$Resources:General, Button_RemoveSelected %>" OnClientClick="return confirmDeleteProducts();" runat="server" SkinID="DefaultButton" />
		</div>
		<uc:ProductSearch runat="server" ID="productSearch" />
	</ContentTemplate>
</asp:UpdatePanel>
