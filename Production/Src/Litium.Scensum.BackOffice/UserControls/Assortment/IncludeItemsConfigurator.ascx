﻿<%@ Control
	Language="C#"
	AutoEventWireup="true"
	CodeBehind="IncludeItemsConfigurator.ascx.cs"
	Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.IncludeItemsConfigurator" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchForm" Src="~/UserControls/Assortment/ProductSearchForm.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProductSearchResult" Src="~/UserControls/Assortment/ProductSearchResult.ascx" %>
<%@ Register TagPrefix="uc" TagName="CategoryTree" Src="~/UserControls/Tree/SelectTreeView.ascx" %>

<div class="campaign-full-left">
<!-- Include Toolbar Start -->
	<div id="includeToolbar" style="width: 100%;">
		<div class="collapsible-items left">
			<div id="IncludeCollapsibleDiv" runat="server" class="collapsible-item left">
				<div class="collapsible-item-header left">
					<asp:Image ID="IncludeImage" runat="server" ImageUrl="~/Media/Images/Campaign/include.png" />
					&nbsp; 
					<span><%=Resources.Campaign.Literal_Include%></span>
				</div>
				<div id="IncludeCollapsibleCenterDiv" runat="server" class="action-block left">
					<div class="left">
						<span class="collapsible-caption"><%=Resources.General.Button_Add%></span>
					</div>
					<div class="link-button">
						<asp:Button ID="OpenIncludeProductSearchPopupButton" Text="<%$Resources:Product, Literal_product%>" runat="server" />
						<asp:Button ID="OpenIncludeCategorySearchPopupButton" Text="<%$Resources:Product, Button_Category%>" runat="server" />
						<asp:Button ID="OpenIncludeBrandSearchPopupButton" Text="<%$Resources:Product, Literal_Brand%>" runat="server" />
						<asp:Button ID="OpenIncludeSupplierSearchPopupButton" Text="<%$Resources:Lekmer, Literal_Supplier%>" runat="server" />
					</div>
				</div>
				<div class="image-block right">
					<asp:HiddenField ID="IncludePanelStateHidden" runat="server" Value="true" />	
					<asp:Image ID="IncludePanelIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" />
				</div>
			</div>
		</div>
		<br />
		<div id="IncludePanelDiv" runat="server" class="campaign-include-content" style="width: 97%;">
			<div class="column">
				<div class="input-box">
					<asp:Label ID="ProductGridPageSize" Text="<%$Resources:Campaign, Literal_ProductGridPageSize%>" runat="server" />
					<br />
					<asp:DropDownList ID="ProductGridPageSizeSelect" runat="server" AutoPostBack="True">
						<asp:ListItem Value="10">10</asp:ListItem>
						<asp:ListItem Value="100">100</asp:ListItem>
						<asp:ListItem Selected="True" Value="250">250</asp:ListItem>
						<asp:ListItem Value="500">500</asp:ListItem>
					</asp:DropDownList>
				</div>
			</div>
			<asp:Panel ID="IncludePanel" runat="server">
				<div style="padding-top:10px;clear:both;"></div>
				<asp:Label ID="LabelProducts" Text="<%$Resources:Product, Literal_Products%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="ProductIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<sc:GridViewWithCustomPager
							ID="ProductIncludeGrid"
							SkinID="grid"
							runat="server"
							AllowPaging="true"
							AutoGenerateColumns="false"
							Width="100%"
							DataSourceID="IncludeProductDataSource">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_ErpId %>" ItemStyle-Width="13%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="ErpLiteral" Text='<%# Eval("ErpId")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="49%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("DisplayTitle")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Category %>" ItemStyle-Width="15%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="CategoryLiteral" Text='<%# GetCategory((int)Eval("CategoryId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="StatusLiteral" Text='<%# GetStatus(Eval("ProductStatusId"))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="5%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TypeLiteral" Text='<%# GetProductType(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveProduct" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmProductRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</sc:GridViewWithCustomPager>
						<asp:ObjectDataSource ID="IncludeProductDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SearchMethodWithSortingByErpId" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductDataSource" />

						<div runat="server" id="ProductIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromProductIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected products');" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>

				<div style="padding-top:10px;clear:both;"></div>
				<asp:Label ID="Label1" Text="<%$Resources:Product, Label_Categories%>" runat="server" CssClass="text-bold"></asp:Label>
				<br />
				<asp:UpdatePanel ID="CategoryIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="CategoryIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="25%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Path %>" ItemStyle-Width="55%">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="PathLiteral" Text='<%# GetCategoryPathIncludeGrid(DataBinder.GetDataItem(Container))%>'></uc:LiteralEncoded>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveCategory" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmCategoryRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="CategoryIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left;padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromCategoryIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected categories');" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>

				<div style="padding-top:10px;clear:both;"></div>
				<asp:Label Text="<%$Resources:Product, Literal_Brands%>" runat="server" CssClass="text-bold" />
				<br />
				<asp:UpdatePanel ID="BrandIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="BrandIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveBrand" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmBrandRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="BrandIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left; padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromBrandIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected brands');" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>

				<div style="padding-top:10px;clear:both;"></div>
				<asp:Label Text="<%$Resources:Lekmer, Literal_Suppliers%>" runat="server" CssClass="text-bold" />
				<br />
				<asp:UpdatePanel ID="SupplierIncludeGridUpdatePanel" UpdateMode="Conditional" runat="server">
					<ContentTemplate>
						<asp:GridView ID="SupplierIncludeGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<HeaderTemplate>
										<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Name %>">
									<ItemTemplate>
										<uc:LiteralEncoded runat="server" ID="NameLiteral" Text='<%# Eval("Name")%>'/>
									</ItemTemplate>
								</asp:TemplateField>

								<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
									<ItemTemplate>
										<asp:ImageButton runat="server" ID="RemoveButton" CommandName="RemoveSupplier" CommandArgument='<%# Eval("Id") %>'
											ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
											OnClientClick="return ConfirmSupplierRemove();" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<div runat="server" id="SupplierIncludeApplyToAllSelectedDiv" class="apply-to-all-selected">
							<div style="float:left; padding-top:6px;">
								<div class="apply-to-all">
									<span><%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
								</div>
							</div>
							<div style="float:left">
								<uc:ImageLinkButton UseSubmitBehaviour="true" ID="RemoveSelectionFromSupplierIncludeGridButton" Text="<%$Resources:General, Button_Delete %>" OnClientClick="return DeleteConfirmation('selected suppliers');" runat="server" SkinID="DefaultButton"/>
							</div>
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</asp:Panel>
		</div>
	</div>
<!-- Include Toolbar End -->
</div>

<!-- Category Search Popup Start -->
	<!-- Include Start -->
<div id="CategoryIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="CategoryIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="CategoryIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<uc:CategoryTree runat="server" ID="CategoryIncludePopupCategoryTree" />
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryIncludePopupOkButton" Text="<%$Resources:General, Button_Ok %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoryIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="CategoryIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeCategorySearchPopupButton"
	PopupControlID="CategoryIncludePopupDiv" 
	CancelControlID="CategoryIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	<!-- Include End -->
<!-- Category Search Popup End -->

<!-- Product Search Popup Start -->
	<!-- Include Start -->
<div id="ProductIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="ProductIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable">
		<asp:UpdatePanel ID="ProductIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<div class="campaign-product-search">
					<div class="content-box">
						<asp:Panel ID="SearchPanel" DefaultButton="ProductIncludePopupSearchButton" runat="server">
						<uc:ProductSearchForm ID="ProductIncludeSearchFormControl" runat="server" />
						<br style="clear:both;" />
						<br />
						<div class="campaign-popup-buttons no-padding">
							<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupSearchButton" Text="Search" runat="server" SkinID="DefaultButton"/>
						</div>
						</asp:Panel>
						<br class="clear"/>
						<br />
						<uc:ProductSearchResult ID="ProductIncludeSearchResultControl" runat="server" />
					</div>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupAddAllButton" Text="<%$Resources:General, Button_AddAllInRange %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="ProductIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="ProductIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeProductSearchPopupButton"
	PopupControlID="ProductIncludePopupDiv" 
	CancelControlID="ProductIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	<!-- Include End -->
<!-- Product Search Popup End -->

<!-- Brand Search Popup Start -->
	<!-- Include Start -->
<div id="BrandIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="BrandIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable" style="padding: 10px">
		<asp:UpdatePanel ID="BrandIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<asp:GridView ID="BrandIncludeSearchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server" />
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>

						<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Title")%>'/>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="BrandIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="BrandIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeBrandSearchPopupButton"
	PopupControlID="BrandIncludePopupDiv" 
	CancelControlID="BrandIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	<!-- Include End -->
<!-- Brand Search Popup End -->

<!-- Supplier Search Popup Start -->
	<!-- Include Start -->
<div id="SupplierIncludePopupDiv" class="campaign-popup-container" runat="server" style="z-index: 10010; display: none;">
	<div class="campaign-popup-header">
		<div class="campaign-popup-header-left">
		</div>
		<div class="campaign-popup-header-center">
			<span><%= Resources.General.Literal_Search%></span>
			<input type="button" id="SupplierIncludePopupCloseButton" runat="server" value="x"/>
		</div>
		<div class="campaign-popup-header-right">
		</div>
	</div>
	<div class="campaign-popup-content-scrollable" style="padding: 10px">
		<asp:UpdatePanel ID="SupplierIncludePopupUpdatePanel" UpdateMode="Conditional" runat="server">
			<ContentTemplate>
				<asp:GridView ID="SupplierIncludeSearchGrid" AutoGenerateColumns="false" runat="server" SkinID="grid" Width="100%">
					<Columns>
						<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
							<HeaderTemplate>
								<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
							</HeaderTemplate>
							<ItemTemplate>
								<asp:CheckBox ID="SelectCheckBox" runat="server" />
								<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
							</ItemTemplate>
						</asp:TemplateField>

						<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_Name %>">
							<ItemTemplate>
								<uc:LiteralEncoded runat="server" ID="NameLiteral" Text='<%# Eval("Name")%>'/>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</ContentTemplate>
		</asp:UpdatePanel>
		<br />
		<div class="campaign-popup-buttons wide-padding">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SupplierIncludePopupOkButton" Text="<%$Resources:General, Button_Add %>" runat="server" SkinID="DefaultButton"/>
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SupplierIncludePopupCancelButton" Text="<%$Resources:General, Button_Cancel %>" runat="server" SkinID="DefaultButton"/>
		</div>
	</div>
</div>
<ajaxToolkit:ModalPopupExtender 
	ID="SupplierIncludeSearchPopup" 
	runat="server" 
	TargetControlID="OpenIncludeSupplierSearchPopupButton"
	PopupControlID="SupplierIncludePopupDiv" 
	CancelControlID="SupplierIncludePopupCloseButton" 
	BackgroundCssClass="PopupBackground" />
	<!-- Include End -->
<!-- Supplier Search Popup End -->