﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Translation;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class TagList : StateUserControlController<TagListState>
	{
		private const string collaps = "~/Media/Images/Common/up-tag-group.gif";
		private const string expand = "~/Media/Images/Common/right-tag-group.gif";

		public int ProductId { get; set; }
		public ITagGroup TagGroup { get; set; }
		public Collection<ICategoryTagGroup> DefaultTagGroups { get; set; }
		public Collection<ITag> SelectedTags
		{
			get
			{
				return State.SelectedTags;
			}
		}
		public Collection<ITag> InitialProductTags
		{
			get
			{
				return State.InitialProductTags;
			}
		}

		protected override void SetEventHandlers()
		{
			MoveToSelectedButton.Click += MoveToSelectedButtonClick;
			MoveToAvailableButton.Click += MoveToAvailableButtonClick;
			TagGroupLink.Click += TagGroupLinkClick;
			TagPopupCloseButton.ServerClick += TagPopupCancelButtonClick;
			TagPopupCancelButton.Click += TagPopupCancelButtonClick;
			TagPopupOkButton.Click += TagPopupOkButtonClick;
			TagGrid.RowCommand += TagGridRowCommand;
			TagGrid.RowDataBound += TagGridRowDataBound;
			NewTagButton.Click += NewTagButtonClick;
		}

		public void BindData()
		{
			PopulateControl();
		}

		protected override void PopulateControl()
		{
			InitializeState();
			PopulateAvailableTags();
			PopulateSelectedTags();
			TagGroupLink.Text = TagGroup.Title;
			TagGroupTitleLiteral.Text = TagGroup.Title;

			InitializeDefaultTagGroups();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			TagPopupExtender.BehaviorID = ClientID + "_tagPopup";

			CollapseButton.ImageUrl = ShowTagGroup.Checked ? collaps : expand;
			TagsDiv.Style.Add("display", ShowTagGroup.Checked ? "block" : "none");
		}

		protected void MoveToSelectedButtonClick(object sender, EventArgs e)
		{
			foreach (var i in AvailableTagsList.GetSelectedIndices())
			{
				var selectedItem = AvailableTagsList.Items[i];
				var tagId = int.Parse(selectedItem.Value, CultureInfo.CurrentCulture);
				if (State.SelectedTags.Any(t => t.Id == tagId)) continue;
				var tag = State.AvailableTags.FirstOrDefault(t => t.Id == tagId);
				if (tag == null) continue;
				State.SelectedTags.Add(tag);
				State.AvailableTags.Remove(tag);
			}

			PopulateAvailableTags();
			PopulateSelectedTags();
		}

		protected void MoveToAvailableButtonClick(object sender, EventArgs e)
		{
			foreach (var i in SelectedTagsList.GetSelectedIndices())
			{
				var selectedItem = SelectedTagsList.Items[i];
				var tagId = int.Parse(selectedItem.Value, CultureInfo.CurrentCulture);
				var tag = State.SelectedTags.FirstOrDefault(t => t.Id == tagId);
				if (tag == null) continue;
				if (!State.AvailableTags.Any(t => t.Id == tagId))
				{
					State.AvailableTags.Add(tag);
				}
				State.SelectedTags.Remove(tag);
			}

			PopulateAvailableTags();
			PopulateSelectedTags();
		}

		protected void TagGroupLinkClick(object sender, EventArgs e)
		{
			var translations = RetrieveAllTranslations();
			State.InitializeTranslations(translations);
			PopulateTagGrid();
			TagGroupLink.Attributes.Add("onclick", "$find('" + TagPopupExtender.BehaviorID + "').show(); return false;");
			TagPopupExtender.Show();
		}

		protected void TagPopupCancelButtonClick(object sender, EventArgs e)
		{
			ResetPopup();
		}

		protected void TagPopupOkButtonClick(object sender, EventArgs e)
		{
			UpdateValues();

			if (!ValidateDuplicates())
			{
				TagPopupExtender.Show();
				return;
			}

			var tagService = IoC.Resolve<ITagSecureService>();
			foreach (var tag in State.AllTags)
			{
				var tagId = tag.Id;
				var translations = State.CurrentTranslations.Where(t => t.Id == tagId);
				tagService.Save(SignInHelper.SignedInSystemUser, tag, translations);
			}

			var allSelectedTags = new Collection<ITag>();
			foreach (var selectedTag in State.SelectedTags)
			{
				var tagId = selectedTag.Id;
				var tag = State.AllTags.FirstOrDefault(t => t.Id == tagId);
				if (tag == null)
				{
					continue;
				}

				selectedTag.Value = tag.Value;
				allSelectedTags.Add(tag);
			}

			State.AvailableTags = CloneHelper.Clone((State.AllTags.Except(allSelectedTags)));
			State.TagGroup.Tags = CloneHelper.Clone(State.AllTags);
			State.OriginalTranslations = CloneHelper.Clone(State.CurrentTranslations);

			PopulateAvailableTags();
			PopulateSelectedTags();
			ResetPopup();
		}

		protected void NewTagButtonClick(object sender, EventArgs e)
		{
			TagPopupExtender.Show();

			var tagValue = NewTagBox.Text.Trim();
			var tagCommonName = NewTagCommonNameBox.Text.Trim();
			if (!IsValid(tagValue, tagCommonName))
			{
				return;
			}

			UpdateValues();

			var tagService = IoC.Resolve<ITagSecureService>();
			var newTag = tagService.Create(State.TagGroup.Id, tagValue, tagCommonName);
			newTag.Id = State.AllTags.Any(t => t.Id < 0) ? State.AllTags.Min(t => t.Id) - 1 : -1;
			State.AllTags.Add(newTag);
			PopulateTagGrid();
			NewTagBox.Text = string.Empty;
			NewTagCommonNameBox.Text = string.Empty;
		}

		protected void TagGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			var row = e.Row;
			if (row.RowType != DataControlRowType.DataRow)
			{
				return;
			}

			var tag = (ITag) row.DataItem;
			var tagValueControl = (TextBox) row.FindControl("TagValueBox");
			var translator = (GenericTranslator) row.FindControl("TagValueTranslator");

			translator.DefaultValueControlClientId = tagValueControl.ClientID;
			translator.BusinessObjectId = tag.Id;
			translator.DataSource = new Collection<ITranslationGeneric>(State.CurrentTranslations.Where(t => t.Id == tag.Id).ToList());
			translator.DataBindManual();
		}

		protected void TagGridRowCommand(object sender, CommandEventArgs e)
		{
			TagPopupExtender.Show();

			if (e.CommandName != "DeleteTag")
			{
				return;
			}

			var tagId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);

			if (DeleteTag(tagId))
			{
				UpdateValues();

				State.RemoveTag(tagId);

				PopulateAvailableTags();
				PopulateSelectedTags();
				PopulateTagGrid();
			}
		}

		private void InitializeState()
		{
			var tagService = IoC.Resolve<ITagSecureService>();
			var productTags = tagService.GetAllByProductAndTagGroup(ProductId, TagGroup.Id);
			var availableTags = new Collection<ITag>();
			foreach (var tag in TagGroup.Tags)
			{
				var tagId = tag.Id;
				if (productTags.Any(t => t.Id == tagId)) continue;
				availableTags.Add(tag);
			}
			State = new TagListState(ProductId, TagGroup, availableTags, productTags, DefaultTagGroups);
		}

		private void InitializeDefaultTagGroups()
		{
			var script = "CollapseExpand('"
				+ TagsDiv.ClientID + "', '"
				+ ShowTagGroup.ClientID + "', '"
				+ CollapseButton.ClientID + "', '"
				+ ResolveClientUrl(collaps) + "','"
				+ ResolveClientUrl(expand) + "'); "
				+ "return false;";
			CollapseButton.Attributes.Add("onclick", script);

			ShowTagGroup.Checked = State.DefaultTagGroups.FirstOrDefault(tg => tg.TagGroupId == TagGroup.Id) != null;
		}

		private void PopulateSelectedTags()
		{
			SelectedTagsList.DataSource = State.SelectedTags.OrderBy(t => t.Value);
			SelectedTagsList.DataBind();
		}

		private void PopulateAvailableTags()
		{
			AvailableTagsList.DataSource = State.AvailableTags.OrderBy(t => t.Value);
			AvailableTagsList.DataBind();
		}

		private IEnumerable<ITranslationGeneric> RetrieveAllTranslations()
		{
			var translations = new Collection<ITranslationGeneric>();
			var tagService = IoC.Resolve<ITagSecureService>();
			foreach (var tag in State.TagGroup.Tags)
			{
				foreach (var translation in tagService.GetAllTranslationsByTag(tag.Id))
				{
					translations.Add(translation);
				}
			}
			return translations;
		}

		private void PopulateTagGrid()
		{
			TagGrid.DataSource = State.AllTags.OrderBy(t => t.Value);
			TagGrid.DataBind();
		}

		private bool IsValid(string tagValue, string tagCommonName)
		{
			bool isValid = true;

			if (string.IsNullOrEmpty(tagValue))
			{
				TagPopupMessenger.Add(Resources.LekmerMessage.ProductEdit_TagValueEmpty);
				isValid = false;
			}

			if (string.IsNullOrEmpty(tagCommonName))
			{
				TagPopupMessenger.Add(Resources.LekmerMessage.ProductEdit_TagCommonNameEmpty);
				isValid = false;
			}

			if (State.AllTags.Any(t => t.Value.ToUpper() == tagValue.ToUpper()))
			{
				TagPopupMessenger.Add(Resources.LekmerMessage.ProductEdit_TagValueExists);
				isValid = false;
			}

			if (State.AllTags.Any(t => t.CommonName.ToUpper() == tagCommonName.ToUpper()))
			{
				TagPopupMessenger.Add(Resources.LekmerMessage.ProductEdit_TagCommonNameExists);
				isValid = false;
			}

			return isValid;
		}

		private bool ValidateDuplicates()
		{
			bool isValid = true;

			var groupetTagsByValue = State.AllTags.GroupBy(t => t.Value.ToUpper());
			var duplicatedValues = GetDuplicates(groupetTagsByValue);
			if (!string.IsNullOrEmpty(duplicatedValues))
			{
				TagPopupMessenger.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.ProductEdit_TagValueDuplicated, duplicatedValues.Substring(0, duplicatedValues.Length - 2)));
				isValid = false;
			}

			var groupetTagsByCommonName = State.AllTags.GroupBy(t => t.CommonName.ToUpper());
			var duplicatedCommonNames = GetDuplicates(groupetTagsByCommonName);
			if (!string.IsNullOrEmpty(duplicatedCommonNames))
			{
				TagPopupMessenger.Add(string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.ProductEdit_TagCommonNameDuplicated, duplicatedCommonNames.Substring(0, duplicatedCommonNames.Length - 2)));
				isValid = false;
			}

			return isValid;
		}

		private string GetDuplicates(IEnumerable<IGrouping<string, ITag>> groupetTags)
		{
			var duplicated = string.Empty;

			if (groupetTags.Count() < State.AllTags.Count)
			{
				foreach (var groupetTag in groupetTags.Where(i => i.Count() > 1))
				{
					duplicated += "'" + groupetTag.Key + "', ";
				}
			}

			return duplicated;
		}

		private void ResetPopup()
		{
			TagPopupMessenger.Attributes.Add("style:display", "none");
			NewTagBox.Text = string.Empty;
			NewTagCommonNameBox.Text = string.Empty;

			State.AllTags = CloneHelper.Clone(State.TagGroup.Tags);
			State.CurrentTranslations = CloneHelper.Clone(State.OriginalTranslations);

			PopulateTagGrid();
		}

		private bool DeleteTag(int tagId)
		{
			var tagSecureService = IoC.Resolve<ITagSecureService>();

			try
			{
				tagSecureService.Delete(SignInHelper.SignedInSystemUser, tagId);
			}
			catch (Exception ex)
			{
				TagPopupMessenger.Add(string.Format(Resources.LekmerMessage.ProductEdit_TagValue_Delete_Failure, ex.Message));
				return false;
			}

			return true;
		}

		private void UpdateValues()
		{
			var translations = new List<ITranslationGeneric>();
			foreach (GridViewRow row in TagGrid.Rows)
			{
				if (row.RowType != DataControlRowType.DataRow)
				{
					continue;
				}

				var translator = (GenericTranslator)row.FindControl("TagValueTranslator");
				translations.AddRange(translator.GetTranslations());

				var tagId = translator.BusinessObjectId;
				var tagValueControl = (TextBox)row.FindControl("TagValueBox");
				var tagCommonNameControl = (TextBox)row.FindControl("TagCommonNameBox");
				var tag = State.AllTags.FirstOrDefault(t => t.Id == tagId);
				if (tag != null)
				{
					tag.Value = tagValueControl.Text;
					tag.CommonName = tagCommonNameControl.Text;
				}
			}

			State.CurrentTranslations = new Collection<ITranslationGeneric>(translations);
		}
	}

	[Serializable]
	public sealed class TagListState
	{
		public int ProductId { get; private set; }
		public ITagGroup TagGroup { get; set; }
		public Collection<ITag> AvailableTags { get; set; }
		public Collection<ITag> SelectedTags { get; set; }
		public Collection<ITag> InitialProductTags { get; set; }
		public Collection<ITag> AllTags { get; set; }
		public Collection<ITranslationGeneric> OriginalTranslations { get; set; }
		public Collection<ITranslationGeneric> CurrentTranslations { get; set; }
		public Collection<ICategoryTagGroup> DefaultTagGroups { get; set; }

		public TagListState(int productId, ITagGroup tagGroup, IEnumerable<ITag> availableTags, Collection<ITag> selectedTags, Collection<ICategoryTagGroup> defaultTagGroups)
		{
			ProductId = productId;
			TagGroup = tagGroup;
			AvailableTags = CloneHelper.Clone(availableTags);
			SelectedTags = CloneHelper.Clone(selectedTags);
			InitialProductTags = CloneHelper.Clone(selectedTags);
			AllTags = CloneHelper.Clone(tagGroup.Tags);
			OriginalTranslations = new Collection<ITranslationGeneric>();
			CurrentTranslations = new Collection<ITranslationGeneric>();
			DefaultTagGroups = defaultTagGroups;
		}

		public void InitializeTranslations(IEnumerable<ITranslationGeneric> translations)
		{
			OriginalTranslations = CloneHelper.Clone(translations);
			CurrentTranslations = CloneHelper.Clone(translations);
		}

		public void RemoveTag(int tagId)
		{
			var tagToRemove = AvailableTags.FirstOrDefault(t => t.Id == tagId);
			if (tagToRemove != null)
			{
				AvailableTags.Remove(tagToRemove);
			}

			tagToRemove = SelectedTags.FirstOrDefault(t => t.Id == tagId);
			if (tagToRemove != null)
			{
				SelectedTags.Remove(tagToRemove);
			}

			tagToRemove = AllTags.FirstOrDefault(t => t.Id == tagId);
			if (tagToRemove != null)
			{
				AllTags.Remove(tagToRemove);
			}

			tagToRemove = TagGroup.Tags.FirstOrDefault(t => t.Id == tagId);
			if (tagToRemove != null)
			{
				TagGroup.Tags.Remove(tagToRemove);
			}

			var translation = OriginalTranslations.FirstOrDefault(t => t.Id == tagId);
			if (translation != null)
			{
				OriginalTranslations.Remove(translation);
			}

			translation = CurrentTranslations.FirstOrDefault(t => t.Id == tagId);
			if (translation != null)
			{
				CurrentTranslations.Remove(translation);
			}
		}
	}

	public static class CloneHelper
	{
		public static Collection<ITag> Clone(IEnumerable<ITag> source)
		{
			var destination = new Collection<ITag>();
			var tagService = IoC.Resolve<ITagSecureService>();
			foreach (var tag in source)
			{
				var tagClone = tagService.Create(tag.TagGroupId, tag.Value, tag.CommonName);
				tagClone.Id = tag.Id;
				destination.Add(tagClone);
			}
			return destination;
		}

		public static Collection<ITranslationGeneric> Clone(IEnumerable<ITranslationGeneric> source)
		{
			var destination = new Collection<ITranslationGeneric>();
			foreach (var translation in source)
			{
				var translationClone = IoC.Resolve<ITranslationGeneric>();
				translationClone.Id = translation.Id;
				translationClone.LanguageId = translation.LanguageId;
				translationClone.Value = translation.Value;
				destination.Add(translationClone);
			}
			return destination;
		}
	}
}