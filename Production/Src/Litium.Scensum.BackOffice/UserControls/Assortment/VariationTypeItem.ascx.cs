using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.UserControls.Assortment
{
	public partial class VariationTypeItem : System.Web.UI.UserControl
	{
		#region Step
		private const int stepMove = 10;
		#endregion

		private const string VariationGuidsViewStateKey = "VariationGuidsViewStateKey";
		private const string VariationTypeGuidViewStateKey = "VariationTypeGuidViewStateKey";

		public event EventHandler<VariationTypeEventArgs> VariationTypeAction;
		public event EventHandler<VariationValueChangedEventArgs> VariationValueChanged;
		public event EventHandler<VariationTitleChangedEventArgs> VariationTitleChanged;
		public string ValidationGroup { get; set; }
		public Guid VariationTypeGuid { get; set; }
		private Collection<Guid> VariationGuids { get; set; }

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			tbTitle.TextChanged += TBTitle_TextChanged;
			imbVariationTypeUp.Click += UpClick;
			imbVariationTypeDown.Click += DownClick;
			imbVariationTypeDelete.Click += DeleteClick;

			btnVariationCriterieAdd.Click += btnVariationCriterieAdd_Click;
			gvVariations.RowDataBound += GVVariations_RowDataBound;
			gvVariations.RowCommand += GVVariations_RowCommand;
		}

		private void btnVariationCriterieAdd_Click(object sender, EventArgs e)
		{
			string criterieValue = Foundation.Convert.RemoveRedundantSpaces(tbCriterieValue.Text);
			if (!string.IsNullOrEmpty(criterieValue))
			{
				VariationTypeWrapper wraper = GetGridItems();
				IVariation variationF = wraper.VariationType.Variations.FirstOrDefault(item => item.Value == criterieValue);
				if (variationF != null)
				{
					tbCriterieValue.Text = string.Empty;
					lblSameValue.Visible = true;
					return;
				}
				IVariation variation = IoC.Resolve<IVariationSecureService>().Create();
				variation.Ordinal = GetMaxGridOrdinal() + stepMove;
				variation.Value = criterieValue;
				wraper.VariationType.Variations.Add(variation);
				Populate(wraper);
			}
			tbCriterieValue.Text = string.Empty;
		}

		#region Grid Evant

		private static void GVVariations_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow)
				return;
			ImageButton ib = (ImageButton)row.FindControl("ibtnDelete");
			ib.Attributes.Add("onclick", "return confirm('" + Resources.ProductMessage.VariationsConfirmDelete + "', null);");

			LinkButton lbtnEdit = (LinkButton)row.FindControl("lbtnEdit");
			lbtnEdit.Text = HttpUtility.HtmlEncode(((IVariation)row.DataItem).Value);
		}

		private void GVVariations_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			int ordinal;
			if(!int.TryParse(e.CommandArgument.ToString(), out ordinal))
			{
				ordinal = 0;
			}

			switch (e.CommandName)
			{
				case "UpOrdinal":
					MoveVariation(ordinal, (int)(stepMove * (-1.5)));
					break;
				case "DownOrdinal":
					MoveVariation(ordinal, (int)(stepMove * 1.5));
					break;
				case "RefreshOrder":
					RefreshOrder();
					break;
				case "DeleteVariation":
					DeleteVariation(ordinal);
					break;
				case "EditVariation":
					EditVariation(ordinal);
					break;
			}
		}

		private void RefreshOrder()
		{
			VariationTypeWrapper wraper = GetGridItems();
			Populate(wraper);
		}

		private Collection<IVariation> Sort(VariationTypeWrapper variationTypeWrapper, int ordinalsStep)
		{
			List<IVariation> items = new List<IVariation>(variationTypeWrapper.VariationType.Variations);

			for (int i = variationTypeWrapper.VariationGuids.Count; i < items.Count; i++)
			{
				variationTypeWrapper.VariationGuids.Add(Guid.NewGuid());
			}

			// Reorder variation guids
			List<VariationGuidPair> pairs = new List<VariationGuidPair>();
			for (int i = 0; i < items.Count; i++)
			{
				pairs.Add(new VariationGuidPair {Variation = items[i], Guid = variationTypeWrapper.VariationGuids[i] });
			}

			// Sort
			pairs.Sort(delegate(VariationGuidPair block1, VariationGuidPair block2)
			{
				if (block1.Variation.Ordinal < block2.Variation.Ordinal)
					return -1;
				if (block1.Variation.Ordinal > block2.Variation.Ordinal)
					return 1;
				return 0;
			});

            // Saving new values
			items.Clear();
			variationTypeWrapper.VariationGuids.Clear();
			foreach (VariationGuidPair pair in pairs)
			{
				items.Add(pair.Variation);
				variationTypeWrapper.VariationGuids.Add(pair.Guid);
			}
			VariationGuids = variationTypeWrapper.VariationGuids;

			for (int i = 0; i < items.Count; i++)
			{
				items[i].Ordinal = (i + 1) * ordinalsStep;
			}
			return new Collection<IVariation>(items);
		}

		private void MoveVariation(int ordinal, int step)
		{
			VariationTypeWrapper wraper = GetGridItems();
			if (wraper.VariationType.Variations != null)
			{
				wraper.VariationType.Variations.FirstOrDefault(item => item.Ordinal == ordinal).Ordinal += step;
				Populate(wraper);
			}
		}

		private void DeleteVariation(int ordinal)
		{
			VariationTypeWrapper wraper = GetGridItems();
			if (wraper.VariationType.Variations != null)
			{
				List<IVariation> variations = new List<IVariation>(wraper.VariationType.Variations);
				int index = variations.IndexOf(variations.Find(v => v.Ordinal == ordinal));
				wraper.VariationType.Variations.RemoveAt(index);
				wraper.VariationGuids.RemoveAt(index);
				Populate(wraper);
			}
		}

		private void EditVariation(int ordinal)
		{

			IVariation variation = GetGridItems().VariationType.Variations.FirstOrDefault(item => item.Ordinal == ordinal);
			variation.Ordinal =
				GetSource().VariationType.Variations.FirstOrDefault(item => item.Id == variation.Id && item.Value == variation.Value)
					.Ordinal;
			VariationValueChangedEventArgs variationValueChangedEventArgs =
				new VariationValueChangedEventArgs
				{
					Ordinal = System.Convert.ToInt32(hdfVariationTypeOrdinal.Value, CultureInfo.CurrentCulture),
					Value = variation.Value,
					ValueOrdinal = System.Convert.ToInt32(variation.Ordinal)
				};

			if (null != VariationValueChanged)
			{
				VariationValueChanged(this, variationValueChangedEventArgs);
			}

		}

		#endregion

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			tbCriterieValue.Attributes.Add("onblur", "HideVariationMessage('" + lblSameValue.ClientID + "');");
		}

		private void TBTitle_TextChanged(object sender, EventArgs e)
		{
			if (null != VariationTitleChanged)
			{
				VariationTitleChanged(this, new VariationTitleChangedEventArgs { Ordinal = System.Convert.ToInt32(hdfVariationTypeOrdinal.Value, CultureInfo.CurrentCulture), Title = Foundation.Convert.RemoveRedundantSpaces(tbTitle.Text) });
			}
		}

		private void UpClick(object sender, EventArgs e)
		{
			if (null != VariationTypeAction)
				VariationTypeAction(this, new VariationTypeEventArgs { Ordinal = System.Convert.ToInt32(hdfVariationTypeOrdinal.Value, CultureInfo.CurrentCulture), VariationTypeEventType = VariationTypeEventType.Up });
		}

		private void DownClick(object sender, EventArgs e)
		{
			if (null != VariationTypeAction)
				VariationTypeAction(this, new VariationTypeEventArgs { Ordinal = System.Convert.ToInt32(hdfVariationTypeOrdinal.Value, CultureInfo.CurrentCulture), VariationTypeEventType = VariationTypeEventType.Down });
		}

		private void DeleteClick(object sender, EventArgs e)
		{
			if (null != VariationTypeAction)
				VariationTypeAction(this, new VariationTypeEventArgs { Ordinal = System.Convert.ToInt32(hdfVariationTypeOrdinal.Value, CultureInfo.CurrentCulture), VariationTypeEventType = VariationTypeEventType.Delete });
		}

		public void Populate(VariationTypeWrapper variationTypeWraper)
		{
			rfvVariationTypeTitle.ValidationGroup = ValidationGroup;
			tbTitle.Text = variationTypeWraper.VariationType.Title;
			lblHeader.Text = Resources.Product.Literal_VariantionCriteria + (variationTypeWraper.VariationType.Ordinal + 1);
			rfvVariationTypeTitle.ErrorMessage = Resources.ProductMessage.VariationTypeTitleEmptyIn + lblHeader.Text + ".";
			lblSameTitleError.Visible = variationTypeWraper.HasSameTitle;
			hdfVariationTypeOrdinal.Value = variationTypeWraper.VariationType.Ordinal.ToString(CultureInfo.CurrentCulture);
			lblSameValue.Visible = false;

			if (variationTypeWraper.VariationGuids.Count < 1)
			{
				variationTypeWraper.VariationGuids.Clear();
				for (int i = 0; i < variationTypeWraper.VariationType.Variations.Count; i++)
				{
					variationTypeWraper.VariationGuids.Add(Guid.NewGuid());
				}
				VariationGuids = variationTypeWraper.VariationGuids;
			}

			gvVariations.DataSource = Sort(variationTypeWraper, 10);
			gvVariations.DataBind();
		}

		private VariationTypeWrapper GetGridItems()
		{
			VariationTypeWrapper variationTypeWraper = new VariationTypeWrapper();
			IVariationTypeFullSecureService variationTypeSecureService = IoC.Resolve<IVariationTypeFullSecureService>();

			IVariationTypeFull variationType = variationTypeSecureService.Create();
			variationType.Title = tbTitle.Text;
			variationType.Ordinal = System.Convert.ToInt32(hdfVariationTypeOrdinal.Value, CultureInfo.CurrentCulture);
			IVariationSecureService variationSecureService = IoC.Resolve<IVariationSecureService>();

			foreach (GridViewRow row in gvVariations.Rows)
			{
				IVariation variation = variationSecureService.Create();
				variation.Value = HttpUtility.HtmlDecode(((LinkButton)row.FindControl("lbtnEdit")).Text);
				variation.Ordinal = int.Parse(((TextBox)row.FindControl("tbOrdinal")).Text, CultureInfo.CurrentCulture);
				variation.VariationTypeName = variationType.Title;
				variationType.Variations.Add(variation);
			}

			variationTypeWraper.Guid = VariationTypeGuid;

			variationTypeWraper.VariationGuids.Clear();
			foreach (Guid guid in VariationGuids)
			{
				variationTypeWraper.VariationGuids.Add(guid);
			}

			variationTypeWraper.VariationType = variationType;
			variationTypeWraper.HasSameTitle = lblSameTitleError.Visible;
			return variationTypeWraper;
		}

		private int GetMaxGridOrdinal()
		{
			VariationTypeWrapper wraper = GetGridItems();
			if (wraper.VariationType.Variations != null && wraper.VariationType.Variations.Count > 0)
			{
				return wraper.VariationType.Variations.Max(item => item.Ordinal);
			}
			return 0;
		}

		public VariationTypeWrapper GetSource()
		{
			VariationTypeWrapper variationTypeWraper = GetGridItems();
			Sort(variationTypeWraper, 1);
			return variationTypeWraper;
		}

		protected override object SaveViewState()
		{
			ViewState[VariationGuidsViewStateKey] = VariationGuids;
			ViewState[VariationTypeGuidViewStateKey] = VariationTypeGuid;
			return base.SaveViewState();
		}

		protected override void  LoadViewState(object savedState)
		{
			base.LoadViewState(savedState);
			VariationGuids = (Collection<Guid>) ViewState[VariationGuidsViewStateKey] ?? new Collection<Guid>();
			VariationTypeGuid = (Guid) ViewState[VariationTypeGuidViewStateKey];
		}

		internal class VariationGuidPair
		{
			public IVariation Variation { get; set; }
			public Guid Guid { get; set; }
		}
	}

	public enum VariationTypeEventType { Undefined, Up, Down, Delete, TitleChange }

	public class VariationTypeEventArgs : EventArgs
	{
		public int Ordinal { get; set; }
		public VariationTypeEventType VariationTypeEventType { get; set; }
	}

	public class VariationTitleChangedEventArgs : EventArgs
	{
		public string Title { get; set; }
		public int Ordinal { get; set; }
	}

	public class VariationValueChangedEventArgs : EventArgs
	{
		public string Value { get; set; }
		public int Ordinal { get; set; }
		public bool HasError { get; set; }
		public int ValueOrdinal { get; set; }
	}
}

