﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using Litium.Lekmer.RatingReview;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class RatingGroupSelectEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IRatingGroup> RatingGroups { get; set; }
	}
}
