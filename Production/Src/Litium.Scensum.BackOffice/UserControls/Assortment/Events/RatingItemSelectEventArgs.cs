﻿using System;
using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.BackOffice.Modules.Assortment.Ratings;

namespace Litium.Scensum.BackOffice.UserControls.Assortment.Events
{
	public class RatingItemSelectEventArgs : EventArgs
	{
		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public RatingStateItem RatingStateItem
		{
			get;
			set;
		}
	}
}