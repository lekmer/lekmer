﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SizeTableRowForm.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Assortment.SizeTableRowForm" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<script type="text/javascript">
	function ClosePopup() {
		$("div#popup-sizetable-row-header-center input[name*='CancelButton']").click();
		return false;
	}
</script>

<asp:UpdatePanel ID="SizeTableRowFormUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<div id="SizeTableRowDiv" runat="server" class="popup-sizetable-row-container" style="z-index: 10010; display: none;">
			<div id="popup-sizetable-row-header">
				<div id="popup-sizetable-row-header-left">
				</div>

				<div id="popup-sizetable-row-header-center">
					<span><%= Resources.Lekmer.Literal_SizeTableRow %></span>
					<input type="button" id="CancelButton" runat="server" value="x"/>
				</div>

				<div id="popup-sizetable-row-header-right">
				</div>
			</div>

			<div id="popup-sizetable-row-content">
				<uc:ScensumValidationSummary ForeColor= "Black" runat="server" CssClass="advance-validation-summary" ID="ValidationSummary" DisplayMode="List" ValidationGroup="SizeTableRowValidationGroup" />
				<div>
					<div class="column">
						<div class="input-box">
							<span><%=Resources.Lekmer.Literal_Size%> *</span>
							<asp:RequiredFieldValidator ID="SizeValidator" runat="server" ControlToValidate="SizesList" Display ="None" InitialValue="0" ErrorMessage="<%$ Resources:LekmerMessage, SizeTableRow_SizeEmpty %>" ValidationGroup="SizeTableRowValidationGroup" />
							<br />
							<asp:DropDownList ID="SizesList" runat="server" />
						</div>
						<div class="input-box">
							<span><%=Resources.Lekmer.Literal_Col1Value%> *</span>
							<asp:RequiredFieldValidator ID="Col1TextBoxValidator" runat="server" ControlToValidate="Col1TextBox" Display ="None" ErrorMessage="<%$ Resources:LekmerMessage, SizeTableRow_Column1ValueEmpty%>" ValidationGroup="SizeTableRowValidationGroup" />
							<uc:GenericTranslator ID="Col1Translator" runat="server" />
							<br />
							<asp:HiddenField ID="IdHiddenField" runat="server" />
							<asp:TextBox ID="Col1TextBox" runat="server" MaxLength="50" />
						</div>
						<div class="input-box">
							<span><%=Resources.Lekmer.Literal_Col2Value%></span>
							<uc:GenericTranslator ID="Col2Translator" runat="server" />
							<br />
							<asp:TextBox ID="Col2TextBox" runat="server" MaxLength="50" />
						</div>
					</div>
				</div>
				<br class="clear" />
				<br />
				<div class="clear right">
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="AddButton" runat="server" Text="<%$ Resources:General, Button_Ok %>" SkinID="DefaultButton" ValidationGroup="SizeTableRowValidationGroup" />
					<uc:ImageLinkButton  UseSubmitBehaviour="true" ID="CancelBottomButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" OnClientClick="ClosePopup();return false;" />
				</div>
			</div>
		</div>

		<div style="float:left;">
			<uc:ImageLinkButton ID="AddSizeTableRowButton" runat="server" Text="<%$ Resources:Lekmer, Literal_AddSizeTableRow %>" UseSubmitBehaviour="false" SkinID="DefaultButton" />
			<ajaxToolkit:ModalPopupExtender
				ID="SizeTableRowPopup"
				runat="server"
				TargetControlID="AddSizeTableRowButton"
				PopupControlID="SizeTableRowDiv"
				BackgroundCssClass="popup-background"
				Y="20"
				CancelControlID="CancelButton"/>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>