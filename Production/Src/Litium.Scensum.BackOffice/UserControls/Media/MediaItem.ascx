<%@ Import Namespace="Litium.Scensum.BackOffice.UserControls.Media"%>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MediaItem.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Media.MediaItem" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>

<asp:HiddenField ID="hfMediaItemId" runat="server" />
<% if (DisplayMode == DisplayMode.Icons)
 { %>
	<div id="divItemContainer" runat="server" class="media-catalog-item-container">	
		<div class="media-catalog-name-container">
			<div class="media-catalog-name">
			<a runat="server" id="A2">
				<uc:LiteralEncoded runat="server" ID="litName"></uc:LiteralEncoded></a>
			</div>
			<div class="media-catalog-delete">
				<a runat="server" id="lnkDelete" title="Delete"
					onclick='<%# "return DeleteConfirmation(\"" + Resources.Media.Literal_mediaItem + "\");" %>'><img style=" vertical-align: super; padding: 0px;" src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" /></a>
			</div>
		</div>
		<div class="light-box">
			<div class="media-item-image">
				<br />
				<table width="100%" height="187px">
					<tr width="100%" height="100%">
						<td width="100%" height="100%">
							 <a id="lnkDetails"  runat="server" target="_blank" >
								<img runat="server" id="imgMedia" alt="goto details" class="media-item-img"/>											    
								<img ID="imgZoomIcon" runat="server" class="media-zoom-image"/>
							</a>
						</td>
					</tr>
				</table>
			</div>
		</div>
		<div id ="divDimensions" runat ="server" class="media-dimensions">
			<asp:Literal runat="server" ID="litDimensions"></asp:Literal>
			<br />
			<asp:Literal runat="server" ID="FileSizeLiteral"></asp:Literal>
			<br />
		</div>
	</div>
<%}
   else
{%>
<div>
	<div class="light-box">
		<div class="media-body-listview-thumbnail">
			<table width="100%" height="59px">
				<tr width="100%" height="100%">
					<td width="100%" height="100%">
						<a id="lnkDetailsList" runat="server" target="_blank">
							<img runat="server"  style="border:none" id="imgImageList"/>											    
							<img ID="imgZoom" runat="server" class="media-zoom-image" />
						</a>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="media-body-listview-name">
	<div>
		<uc:LinkButtonTrimmed runat="server" MaxLength="40"  ID="hlEdit"></uc:LinkButtonTrimmed>
			</div>
	</div>
	<div class="media-body-listview-type">
		<div>
			<asp:Literal runat="server" ID="litMediaTypeList"></asp:Literal>
		</div>
	</div>
	<div class="media-body-listview-dimensions">
		<div>
			<asp:Literal runat="server" ID="litDimensionsList"></asp:Literal>
			<br />
			<asp:Literal runat="server" ID="FileSizeListLiteral"></asp:Literal>
		</div>
	</div>
	<div class="media-body-listview-delete">
		<div>
		<a runat="server" id="lnkListDelete"  style="border:none;" title="Delete"
					onclick='<%# "return DeleteConfirmation(\"" + Resources.Media.Literal_mediaItem + "\");" %>'><img style="padding-top: 7px;" src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" /></a>
		</div>	
	</div>
</div>
<% } %>