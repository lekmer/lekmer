using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Web.Controls.Button;

namespace Litium.Scensum.BackOffice.UserControls.Media
{
	public partial class MediaList : ControlBase
	{
		private LinkButton sortByTitleButton;
		private LinkButton sortByTypeButton;
		private LinkButton orderByAscButton;
		private LinkButton orderByDescButton;
		private LinkButton listViewModeButton;
		private LinkButton iconsViewModeButton;

		private MediaSortMode SortBy
		{
			set
			{
				ViewState["SortBy"] = (int)value;
			}
			get
			{
				var ob = ViewState["SortBy"];
				return ob == null 
					? MediaSortMode.Title
					: (MediaSortMode)Convert.ToInt32(ViewState["SortBy"], CultureInfo.CurrentCulture);
			}
		}
		private bool? OrderByDesc
		{
			set
			{
				ViewState["OrderByAsc"] = value;
			}
			get
			{
				return (bool?)ViewState["OrderByAsc"];
			}
		}
		public int? FolderId
		{
			set
			{
				ViewState["MediaListFolderId"] = value;
			}
			get
			{
				return (int?)ViewState["MediaListFolderId"];
			}
		}
		public event EventHandler<MediaNavigationEventArgs> Navigate;
		public event EventHandler<MediaNavigationEventArgs> Delete;
		public event EventHandler DisplayModeChanged;

		protected void Page_Load(object sender, EventArgs e)
		{
			dataPager.PreRender += dataPager_PreRender;
			if(!IsPostBack)
			{
				PopulateImages();
			}
			sortByTitleButton = lvMedia.FindControl("lbtnSortByName") as LinkButton;
			if (sortByTitleButton != null)
			{
				sortByTitleButton.Click += SortByNameClick;
			}
			sortByTypeButton = lvMedia.FindControl("SortByTypeButon") as LinkButton;
			if (sortByTypeButton != null)
			{
				sortByTypeButton.Click += SortByFileTypeClick;
			}
			orderByAscButton = lvMedia.FindControl("lbtnOrderByAsc") as LinkButton;
			if (orderByAscButton != null)
			{
				orderByAscButton.Click += OrderByAscClick;
			}
			orderByDescButton = lvMedia.FindControl("lbtnOrderByDes") as LinkButton;
			if (orderByDescButton != null)
			{
				orderByDescButton.Click += OrderByDescClick;
			}
			iconsViewModeButton = lvMedia.FindControl("_lnkIconsView") as ImageLinkButton;
			if (iconsViewModeButton != null)
			{
				iconsViewModeButton.Click += IconsViewModeClick;
			}
			listViewModeButton = lvMedia.FindControl("_lnkListView") as ImageLinkButton;
			if (listViewModeButton != null)
			{
				listViewModeButton.Click += ListViewModeClick;
			}
		}
		protected void UCMediaItem_Navigate(object sender, MediaNavigationEventArgs e)
		{
			if (null != Navigate) Navigate(sender, e);
		}
		protected void UCMediaItem_Delete(object sender, MediaNavigationEventArgs e)
		{
			if (null != Delete) Delete(sender, e);
		}
		private void SortByNameClick(object sender, EventArgs e)
		{
			SortBy = MediaSortMode.Title;
			SetSortByAsc();
			Sort();
			ChangeEnabled(sortByTypeButton, sortByTitleButton);
		}
		private void SortByFileTypeClick(object sender, EventArgs e)
		{
			SortBy = MediaSortMode.FileType;
			SetSortByAsc();
			Sort();
			ChangeEnabled(sortByTitleButton, sortByTypeButton);
		}
        private void OrderByAscClick(object sender, EventArgs e)
		{
			OrderByDesc = false;
			Sort();
			ChangeEnabled(orderByDescButton, orderByAscButton);
		}
		private void OrderByDescClick(object sender, EventArgs e)
		{
			OrderByDesc = true;
			Sort();
			ChangeEnabled(orderByAscButton, orderByDescButton);
		}
		private void IconsViewModeClick(object sender, EventArgs e)
		{
			DisplayMode = DisplayMode.Icons;
			PopulateImages();
			ChangeEnabled(listViewModeButton, iconsViewModeButton);
			if (DisplayModeChanged != null) DisplayModeChanged(sender, e);
		}
		private void ListViewModeClick(object sender, EventArgs e)
		{
			DisplayMode = DisplayMode.List;
			PopulateImages();
			ChangeEnabled(iconsViewModeButton, listViewModeButton);
			if (DisplayModeChanged != null) DisplayModeChanged(sender, e);
		}
		private void SetSortByAsc()
		{
			if (!OrderByDesc.HasValue)
			{
				OrderByDesc = false;
			}
		}
		private void dataPager_PreRender(object sender, EventArgs e)
		{
			if (dataPager.TotalRowCount <= dataPager.PageSize)
			{
				dataPager.Visible = PagingDiv.Visible = false;
				return;
			}
			
			if (dataPager.StartRowIndex == 0)
			{
				var script = @"$('input.paging-left').attr('disabled', 'disabled'); 
					$('input.paging-left:first').attr('src', '" + ResolveClientUrl("~/Media/Images/Grid/Paging/first-inactive.gif") + @"'); 
				    $('input.paging-left:eq(1)').attr('src', '" + ResolveClientUrl("~/Media/Images/Grid/Paging/prev-inactive.gif") + @"');";
				ScriptManager.RegisterStartupScript(this, GetType(), "js_my_paging", script, true);
			}
			else if (dataPager.TotalRowCount - dataPager.StartRowIndex <= dataPager.MaximumRows)
			{
				var script = @"$('input.paging-right').attr('disabled', 'disabled'); 
					$('input.paging-right:first').attr('src', '" + ResolveClientUrl("~/Media/Images/Grid/Paging/next-inactive.gif") + @"'); 
				    $('input.paging-right:eq(1)').attr('src', '" + ResolveClientUrl("~/Media/Images/Grid/Paging/last-inactive.gif") + @"');";
				ScriptManager.RegisterStartupScript(this, GetType(), "js_my_paging", script, true);
			}

			PagingDiv.Attributes.Add("class", DisplayMode == DisplayMode.List ? "media-list-paging" : "media-icons-paging");
		}
		private void Sort()
		{
			dataPager.SetPageProperties(0, dataPager.PageSize, false);
			PopulateImages();
		}
		private void PopulateImages()
		{
			if (!FolderId.HasValue || FolderId.Value <= 0) return;
			ids.SelectParameters.Clear();
			ids.SelectParameters.Add("id", FolderId.ToString());
			ids.SelectParameters.Add("sortBy", SortBy.ToString());
			ids.SelectParameters.Add("sortByDesc", OrderByDesc.HasValue ? OrderByDesc.ToString() : null);
		}
		protected virtual void ChangeEnabled(LinkButton buttonToEnable, LinkButton buttonToDisable)
		{
			buttonToEnable.Enabled = true;
			buttonToEnable.Style.Add("color", "#004CD5");
            buttonToDisable.Enabled = false;
			buttonToDisable.Style.Add("color", "#999999");
		}
	}
	
	public enum MediaSortMode
	{
		Title = 0,
		FileType = 1
	}
}