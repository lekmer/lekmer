﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MediaItemSelect.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Media.MediaItemSelect" %>

<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="CustomControls" Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	function <%=ClientID %>UnCheckOther(spanChk) {
		var CurrentRdbID = spanChk.id;
		Parent = document.getElementById('<%= gvImages.ClientID %>');
		var items = Parent.getElementsByTagName('input');
		for (i = 0; i < items.length; i++) {
			if (items[i].id != CurrentRdbID && items[i].type == "radio") {
				if (items[i].checked) {
					items[i].checked = false;
				}
			}
		}
	}

	function ClearPopup() {
		ClearPopupMessage('<%= messager.ClientID %>');
	}

	function <%=ClientID %>UploadImageTabClick() {
		$('<%= string.Concat("#",divTabUpload.ClientID)%>').attr('class', 'upload-image ui-tabs-panel');
		$('<%= string.Concat("#",divTabArchive.ClientID)%>').attr('class', 'arhive-images ui-tabs-panel ui-tabs-hide');

		$('<%= string.Concat("#",liUploadImageTab.ClientID)%>').attr('class', 'ui-tabs-selected');
		$('<%= string.Concat("#",liArchiveImageTabImageTab.ClientID)%>').attr('class', '');
	}

	function <%=ClientID %>ArchiveImageTabClick() {
		$('<%= string.Concat("#",divTabUpload.ClientID)%>').attr('class', 'arhive-images ui-tabs-panel ui-tabs-hide');
		$('<%= string.Concat("#",divTabArchive.ClientID)%>').attr('class', 'upload-image ui-tabs-panel');

		$('<%= string.Concat("#",liUploadImageTab.ClientID)%>').attr('class', '');
		$('<%= string.Concat("#",liArchiveImageTabImageTab.ClientID)%>').attr('class', 'ui-tabs-selected');
	}

	function ClearPopupMessage(errorsDivId) {
		var errorsDiv = document.getElementById(new String(errorsDivId).concat('_divMessages'));
		if (errorsDiv != null) {
			errorsDiv.style.display = 'none';
		}
	}

	function CloseImageSelect() {
		try {
			//if we are on /BlockRichTextEdit.aspx
			tinyMCEPopup.close();
		}
		catch (e) {
			//if we are on /ProductEdit.aspx
			$('input._inpClose').click();
		}
	}
</script>

<div id="divTabsAddImages" runat="server" class="ui-tabs-nav">
	<ul>
		<li id="liUploadImageTab" class="" runat="server" style="cursor: pointer;"><a onclick='<%=ClientID %>UploadImageTabClick();'><span><div id="divUploadImageTab" runat="server"><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:Media, Literal_UploadImage%>" /></div></span></a></li>
		<li id="liArchiveImageTabImageTab" class="ui-tabs-selected pointer" runat="server" style="cursor: pointer;"><a onclick='<%=ClientID %>ArchiveImageTabClick();'><span><div id="divArchiveImage" runat="server"><asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:Media, Literal_ArchiveImages%>" /></div></span></a></li>
	</ul>

	<br clear="all" />

	<div class="tabs-add-images-container">
		<div id="divTabUpload" runat="server" style="" class="upload-image ui-tabs-panel ui-tabs-hide">
			<div class="upload-image">
				<div class="upload-image-container">
					<asp:UpdatePanel ID="messagerPanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<uc:messagecontainer id="messager" messagetype="Failure" hidemessagescontrolid="btnSaveNewImage" runat="server" />
						</ContentTemplate>
					</asp:UpdatePanel>

					<uc:ajaxvalidationsummary forecolor="Black" runat="server" cssclass="advance-validation-summary" id="avsNewImage" displaymode="List" validationgroup="vgNewImage" />

					<span class="bold"><asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:General, Literal_PlaceInFolder%>" /> *</span>
					<uc:nodeselect id="ucNodeSelector" runat="server" />
					<br />
					<div class="bold">
						<asp:Literal ID="Literal3" runat="server" Text="<%$ Resources:Media, Literal_PathToMedia%>" /> *
					</div>
					<asp:FileUpload ID="fileUpload" Width="99%" runat="server" size="130" ContentEditable="false" CssClass="product-media-file-upload" />

					<div class="product-media-upload-file">
						<asp:UpdatePanel ID="UploadPanel" runat="server" UpdateMode="Conditional">
							<ContentTemplate>
								<uc:imagelinkbutton id="btnUpload" runat="server" text="<%$ Resources:Media, Button_UploadFiles%>" skinid="DefaultButton" />
							</ContentTemplate>
							<Triggers>
								<asp:PostBackTrigger ControlID="btnUpload" />
							</Triggers>
						</asp:UpdatePanel>
					</div>
				</div>
			</div>
		</div>

		<div id="divTabArchive" runat="server" style="" class="upload-image ui-tabs-panel">
			<div class="arhive-images">
				<div class="product-media-folder-list" style="">
					<asp:UpdatePanel ID="upLeft" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<customcontrols:templatedtreeview runat="server" id="mediaTree" displaytextcontrol="lbName"
								nodeexpanderhiddencssclass="tree-item-expander-hidden" nodeimgcssclass="tree-node-img"
								maincontainercssclass="treeview-main-container" nodechildcontainercssclass="treeview-node-child"
								nodeexpandcollapsecontrolcssclass="tree-icon" nodemaincontainercssclass="treeview-node"
								nodeparentcontainercssclass="treeview-node-parent" nodeexpandedimageurl="~/Media/Images/Tree/tree-collapse.png"
								nodecollapsedimageurl="~/Media/Images/Tree/tree-expand.png" menucallerelementcssclass="tree-menu-caller">
								<HeaderTemplate>
									<div class="treeview-header">
										<asp:Literal ID="Literal9" runat="server" Text="<%$ Resources:Media, Literal_Gallery%>" />
									</div>
								</HeaderTemplate>
								<NodeTemplate>
									<div class="tree-item-cell-expand">
										<img src='<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>' alt="" class="tree-icon" />
										<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
									</div>
									<div class="tree-item-cell-main">
										<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
										<asp:LinkButton runat="server" ID="lbName" CommandName="Navigate"></asp:LinkButton>
									</div>
									<br />
								</NodeTemplate>
							</customcontrols:templatedtreeview>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<div class="product-media-folder-image-list">
					<asp:UpdatePanel ID="upArchiveImages" runat="server" UpdateMode="Always">
						<ContentTemplate>
							<sc:gridviewwithcustompager id="gvImages" skinid="grid" runat="server" autogeneratecolumns="false"
								allowpaging="true" pagesize="5"
								width="100%" gridlines="None"
								datasourceid="ids">
								<Columns >
									<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="4%"  HeaderStyle-CssClass="grid-row-wo-padding" ItemStyle-CssClass="grid-row-wo-padding">
										<ItemTemplate>
											<input ID="rbSelect" onclick="javascript: UnCheckOther(this);"  runat="server" type="radio" />
											<asp:HiddenField ID="hfId" Value='<%#Eval("Id") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Image" HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row-wo-padding"  ItemStyle-Height="50px" ItemStyle-Width="6%" >
										<ItemTemplate>
											<asp:Image id="imgArchiveImage" Width='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailWidth %>' Height='<%# Litium.Scensum.BackOffice.Setting.MediaSetting.Instance.ThumbnailHeight %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField HeaderText="<%$ Resources:General, Literal_Title%>" HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row" DataField="Title" ItemStyle-Width="32%" />
									<asp:TemplateField HeaderText="File info" HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row" ItemStyle-Width="42%">
										<ItemTemplate>
											<span class="bold"><asp:Literal ID="Literal10" runat="server" Text="<%$ Resources:Media, Literal_FileType%>" />: &nbsp; </span>
											<asp:Label ID="lblArchiveImageFileType" runat="server" ></asp:Label><br />
											<span class="bold"><asp:Literal ID="Literal11" runat="server" Text="<%$ Resources:Media, Literal_Dimensions%>" />: </span>
											<asp:Label ID="lblArchiveImageDimensions" runat="server" ></asp:Label><br />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</sc:gridviewwithcustompager>
							<asp:ObjectDataSource ID="ids" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="GetMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.MediaItemDataSource"></asp:ObjectDataSource>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>
				<br class="clear" style="clear: both;" />
				<br />
				<div class="right">
					<uc:imagelinkbutton id="btnSaveArchiveImage" runat="server" text="<%$ Resources:General, Button_Next%>" skinid="DefaultButton" />
					<div class="button-container" onclick="CloseImageSelect();" style="cursor: pointer">
						<span class="button-left-border"></span><span class="button-middle"><asp:Literal ID="Literal12" runat="server" Text="<%$ Resources:General, Button_Cancel%>"></asp:Literal></span>
						<span class="button-right-border"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>