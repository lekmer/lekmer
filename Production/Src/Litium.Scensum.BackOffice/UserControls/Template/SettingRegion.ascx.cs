using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.UserControls.Template
{
	public partial class SettingRegion : StateUserControlController<bool>
	{
		private Collection<ITemplateSetting> _templateSettings;
		public Collection<ITemplateSetting> TemplateSettings
		{
			get
			{
				if (_templateSettings == null) _templateSettings = new Collection<ITemplateSetting>();
				return _templateSettings;
			}
		}

		public bool ShowAlternate
		{
			get { return State; }
			set { State = value; }
		}

		protected override void SetEventHandlers()
		{
			TemplateControlHelper.TemplateControlEvent += TemplateControlHelperTemplateControlEvent;
		}
		protected override void PopulateControl()
		{
			SettingCollapsiblePanel.ControlToExpand = SettingsDiv.ClientID;
		}

		private void TemplateControlHelperTemplateControlEvent(object sender, TemplateControlEventArgs e)
		{
			ShowAlternate = e.ShowAlternate;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!Page.IsPostBack)
			{
				if (TemplateSettings != null && TemplateSettings.Count > 0)
				{
					SettingRepeater.DataSource = TemplateSettings;
					SettingRepeater.DataBind();

					AlternateSettingRepeater.DataSource = TemplateSettings;
					AlternateSettingRepeater.DataBind();
				}
				else
				{
					SettingsDiv.Visible = false;
					SettingCollapsiblePanel.Visible = false;
					return;
				}
			}

			if (ShowAlternate)
			{
				SettingLabel.Visible = true;
				AlternateFragmentsDiv.Attributes.Add("style", "display : block;");
				SettingDiv.Attributes.Add("class", "fragment-item-a");
			}
			else
			{
				SettingLabel.Visible = false;
				AlternateFragmentsDiv.Attributes.Add("style", "display : none;");
				SettingDiv.Attributes.Add("class", "fragment-item");
			}
		}

		public Collection<ITemplateSetting> GetSettings()
		{
			Collection<ITemplateSetting> settings = GetTemplateSettings(SettingRepeater, "ucSetting");
			Collection<ITemplateSetting> alternateSettings = GetTemplateSettings(AlternateSettingRepeater, "ucAlternateSetting");

			foreach (ITemplateSetting setting in settings)
			{
				ITemplateSetting tempSetting = setting;
				ITemplateSetting tempAlternateSetting = alternateSettings.FirstOrDefault(alternatesetting => alternatesetting.ModelSettingId == tempSetting.ModelSettingId);

				if (tempAlternateSetting != null)
				{
					setting.AlternateValue = tempAlternateSetting.AlternateValue;
				}
			}
			return settings;
		}
		private static Collection<ITemplateSetting> GetTemplateSettings(Repeater rptSettings, string controlName)
		{
			var templateSettings = new Collection<ITemplateSetting>();
			foreach (RepeaterItem settingItem in rptSettings.Items)
			{
				var setting = settingItem.FindControl(controlName) as Setting;
				if (setting != null)
				{
					templateSettings.Add(setting.GetSetting());
				}
			}
			return templateSettings;
		}
	}
}