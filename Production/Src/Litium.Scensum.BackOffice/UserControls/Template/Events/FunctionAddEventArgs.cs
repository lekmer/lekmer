using System;

namespace Litium.Scensum.BackOffice.UserControls.Template.Events
{
	public class FunctionAddEventArgs : EventArgs
	{
		public int ModelFragmentId
		{
			get; set;
		}
	}
}
