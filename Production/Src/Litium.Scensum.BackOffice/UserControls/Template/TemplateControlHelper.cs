using System;

namespace Litium.Scensum.BackOffice.UserControls.Template
{
	public static class TemplateControlHelper
	{
		public static event EventHandler<TemplateControlEventArgs> TemplateControlEvent;

		public static void OnShowAlternate(object sender, bool showAlternate)
		{
			OnShowAlternateInternal(sender, new TemplateControlEventArgs(showAlternate));
		}

		private static void OnShowAlternateInternal(object sender, TemplateControlEventArgs e)
		{
			if (TemplateControlEvent != null)
			{
				TemplateControlEvent(sender, e);
			}
		}
	}
}