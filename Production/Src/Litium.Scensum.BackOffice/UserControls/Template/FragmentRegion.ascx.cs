using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Template.Events;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.UserControls.Template
{
	public partial class FragmentRegion : StateUserControlController<bool>
	{
		public event EventHandler<FunctionAddEventArgs> FunctionAdded;

		public string Header { get; set; }
		public IEnumerable<ITemplateFragment> Fragments { private get; set; }
		public bool ShowAlternate
		{
			get { return State; }
			set { State = value; }
		}

		protected override void SetEventHandlers()
		{
			TemplateControlHelper.TemplateControlEvent += TemplateControlHelperTemplateControlEvent;
		}
		protected override void PopulateControl()
		{
			RegionCollapsiblePanel.ControlToExpand = FragmentsDiv.ClientID;
		}
		public void FragmentRegionFunctionAdded(object sender, FunctionAddEventArgs e)
		{
			if (FunctionAdded != null)
			{
				FunctionAdded(this, e);
			}
		}
		private void TemplateControlHelperTemplateControlEvent(object sender, TemplateControlEventArgs e)
		{
			ShowAlternate = e.ShowAlternate;
		}
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (!Page.IsPostBack)
			{
				if (Fragments != null && Fragments.Count() > 0)
				{
					FragmentRepeater.DataSource = Fragments;
					FragmentRepeater.DataBind();

					AlternateFragmentRepeater.DataSource = Fragments;
					AlternateFragmentRepeater.DataBind();
				}
				else
				{
					FragmentsDiv.Visible = false;
					RegionCollapsiblePanel.Visible = false;
					return;
				}

				RegionCollapsiblePanel.Title = Header;
			}

			if (ShowAlternate)
			{
				FragmentLabel.Visible = true;
				AlternateFragmentsDiv.Attributes.Add("style", "display : block;");
				FragmentDiv.Attributes.Add("class", "fragment-item-a");
			}
			else
			{
				FragmentLabel.Visible = false;
				AlternateFragmentsDiv.Attributes.Add("style", "display : none;");
				FragmentDiv.Attributes.Add("class", "fragment-item");
			}
		}

		public Collection<ITemplateFragment> GetTemplateFragments()
		{
			Collection<ITemplateFragment> fragments = GetTemplateFragments(FragmentRepeater, "ucFragment");
			Collection<ITemplateFragment> alternateFragments = GetTemplateFragments(AlternateFragmentRepeater, "ucAlternateFragment");

			foreach (ITemplateFragment fragment in fragments)
			{
				ITemplateFragment tempFragment = fragment;
				ITemplateFragment tempAlternateFragment = alternateFragments.FirstOrDefault(alternateFragment => alternateFragment.ModelFragmentId == tempFragment.ModelFragmentId);

				if (tempAlternateFragment != null)
				{
					fragment.AlternateContent = tempAlternateFragment.AlternateContent;
				}
			}
			return fragments;
		}
		private static Collection<ITemplateFragment> GetTemplateFragments(Repeater rptFragments, string controlName)
		{
			var templateFragments = new Collection<ITemplateFragment>();
			foreach (RepeaterItem fragmentItem in rptFragments.Items)
			{
				var fragment = fragmentItem.FindControl(controlName) as Fragment;
				if (fragment != null)
				{
					templateFragments.Add(fragment.GetFragment());
				}
			}
			return templateFragments;
		}
	}
}
