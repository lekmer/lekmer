﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategoryNodeSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Tree.CategoryNodeSelector" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register assembly="Litium.Scensum.Web.Controls" namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" tagprefix="TemplatedTreeView" %>

<asp:UpdatePanel ID="CategoryPathUpdatePanel" runat="server" UpdateMode="Conditional">
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click"/>
	</Triggers>
	<ContentTemplate>
		<asp:TextBox ID="CategoryPathTextBox" runat="server" ReadOnly="true" CssClass="media-item-information-value" />       
		<asp:ImageButton ID="CategoryButton" runat="server" ImageUrl="~/Media/Images/Assortment/category_img.png" ImageAlign="AbsMiddle" CausesValidation="False" height="16px"/>
		
		<div id="CategoryDiv" runat="server" class="mainDiv block-save" style="display: none;">
			<asp:Panel runat="server" DefaultButton="SaveButton">
				<div id="divHeader" class="headerDiv">
					<asp:Label runat="server" Text="Select category" />
					<asp:Button ID="CancelButton" runat="server" Text="X" />
				</div>
				<div id="page-select-subdiv">
					<asp:UpdatePanel ID="CategoryUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="select-folder-header">
								Categories
							</div>
							<div id="select-folder">
								<asp:UpdatePanel ID="UpdatePanel1" runat="server">
									<ContentTemplate>
										<TemplatedTreeView:TemplatedTreeView ID="CategoriesTree" runat="server" DisplayTextControl="TitleButton"
											UseRootNode="false" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
											MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
											NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
											NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
											NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
											NodeSelectedCssClass="treeview-node-selected" Width="250px">
											<HeaderTemplate>
												<div id="clear-selection" style="padding-left:20px; padding-top:3px; height:17px;  border-bottom-width:2px;">
													<asp:LinkButton runat="server" ID="lbClearSelection" Font-Underline="false" OnClick="ClearSelectionButtonClick" Text="<%$ Resources:SiteStructure, Button_ClearSelection %>" ></asp:LinkButton>
												</div>
											</HeaderTemplate>
											<NodeTemplate>
												<div class="tree-item-cell-expand">
													<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
													<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
												</div>
												<div class="tree-item-cell-main" style="max-width:178px;">
													<img src="<%=ResolveUrl("~/Media/Images/Assortment/category_img.png") %>" alt="" class="tree-node-img" height="16px" />
													<asp:LinkButton runat="server" ID="TitleButton" CommandName="Navigate"></asp:LinkButton></div>
												<br />
											</NodeTemplate>
										</TemplatedTreeView:TemplatedTreeView>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
							<div class="right clear" style="width:100px;">
								<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" />
								<uc:ImageLinkButton ID="CancelSaveButton" runat="server" Text="Cancel" SkinID="DefaultButton"/>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>			
			</asp:Panel>
		</div>
		
		<ajaxToolkit:ModalPopupExtender 
			ID="TreeModalPopupExtender"
			runat="server"  
			TargetControlID="CategoryButton"
			PopupControlID="CategoryDiv"
			CancelControlID="CancelButton"
			BackgroundCssClass="popup-background"
		/>
	</ContentTemplate>
</asp:UpdatePanel>