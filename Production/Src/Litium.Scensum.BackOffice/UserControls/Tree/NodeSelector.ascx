<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="NodeSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Tree.NodeSelector" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree"
    TagPrefix="CustomControls" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems"%>

<!--[if IE]> <div style="z-index:100"> <![endif]-->
<asp:UpdatePanel ID="FolderPathUpdatePanel" runat="server" UpdateMode="Conditional">
	<Triggers>
		<asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click"/>
	</Triggers>
	<ContentTemplate>
        <asp:TextBox ID="FolderTextBox" runat="server" ReadOnly="true" CssClass="media-item-information-value" />       
		<asp:ImageButton ID="FolderButton" runat="server" ImageUrl="~/Media/Images/Tree/folder.png" ImageAlign="AbsMiddle" CausesValidation="False"/>
		
		<div id="FolderDiv" runat="server" class="mainDiv block-save" style="display: none;">
			<asp:Panel runat="server" DefaultButton="SaveButton">
				<%--<div id="divHeader" class="headerDiv">--%>
				<div class="divHeader headerDiv">
					<asp:Label runat="server" Text="Select folder" />
					<asp:Button ID="CancelButton" runat="server" Text="X" />
				</div>
				<%--<div id="page-select-subdiv">--%>
				<div class="page-select-subdiv">
					<asp:UpdatePanel ID="LeftUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="select-folder-header">
								Folders
							</div>
							<%--<div id="select-folder">--%>
							<div class="select-folder">
								<asp:UpdatePanel ID="UpdatePanel1" runat="server">
									<ContentTemplate>
										<CustomControls:TemplatedTreeView Id="NodesTreeView" runat="server" DisplayTextControl="TitleLinkButton"
											NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
											MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
											NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
											NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
											NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png" MenuCallerElementCssClass="tree-menu-caller"
											MenuContainerElementId="node-menu" MenuCloseElementId="menu-close">								
											<HeaderTemplate>
											<% if (AllowClearSelection)
											{ %>
												<div id="clear-selection" style="padding-left:20px; padding-top:3px; height:17px;  border-bottom-width:2px;">
													<asp:LinkButton runat="server" ID="lbClearSelection" Font-Underline="false" OnClick="ClearSelectionButtonClick" Text="Clear Selection" ></asp:LinkButton>
												</div>
											<%} %>
											</HeaderTemplate>									
											<NodeTemplate>
												<div class="tree-item-cell-expand">
													<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
													<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
												</div>
												<div class="tree-item-cell-main">
													<img src="<%=ResolveUrl("~/Media/Images/Tree/folder.png") %>" alt="" class="tree-node-img" />
													<asp:LinkButton runat="server" ID="TitleLinkButton" CommandName="Navigate" />
												</div>
												<br />
											</NodeTemplate>
										</CustomControls:TemplatedTreeView>
									</ContentTemplate>
								</asp:UpdatePanel>
							</div>
							<div class="right clear">
								<uc:ImageLinkButton ID="SaveButton" runat="server" Text="Ok" SkinID="DefaultButton" />
								<uc:ImageLinkButton ID="CancelSaveButton" runat="server" Text="Cancel" SkinID="DefaultButton"/>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>			
			</asp:Panel>
		</div>
		
		<ajaxToolkit:ModalPopupExtender 
			ID="TreeModalPopupExtender"
			runat="server"  
			TargetControlID="FolderButton"
			PopupControlID="FolderDiv"
			CancelControlID="CancelButton"
			BackgroundCssClass="popup-background"
		/>
	
	</ContentTemplate>
</asp:UpdatePanel>
<!--[if IE]> </div> <![endif]-->