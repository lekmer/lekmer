﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SelectTreeView.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Tree.SelectTreeView" %>
<%@ Register assembly="Litium.Scensum.Web.Controls" namespace="Litium.Scensum.Web.Controls.Tree.TemplatedTree" tagprefix="TemplatedTreeView" %>

<asp:UpdatePanel ID="TreeUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
		<TemplatedTreeView:TemplatedTreeView ID="Tree" runat="server" DisplayTextControl="TitleButton"
			UseRootNode="true" NodeExpanderHiddenCssClass="tree-item-expander-hidden" NodeImgCssClass="tree-node-img"
			MainContainerCssClass="treeview-main-container" NodeChildContainerCssClass="treeview-node-child"
			NodeExpandCollapseControlCssClass="tree-icon" NodeMainContainerCssClass="treeview-node"
			NodeParentContainerCssClass="treeview-node-parent" NodeExpandedImageUrl="~/Media/Images/Tree/tree-collapse.png"
			NodeCollapsedImageUrl="~/Media/Images/Tree/tree-expand.png"
			NodeSelectedCssClass="treeview-node-selected" Width="440px"
		>
			<HeaderTemplate>

			</HeaderTemplate>
			<NodeTemplate>
				<div class="tree-item-cell-expand">
					<img src="<%=ResolveUrl("~/Media/Images/Tree/tree-expand.png") %>" alt="" class="tree-icon" />
					<asp:Button ID="Expander" runat="server" CommandName="Expand" CssClass="tree-item-expander-hidden"/>
				</div>
				<div class="tree-item-cell-main">
					<asp:HiddenField runat="server" ID="IdHiddenField" />
					<asp:CheckBox runat="server"
						ID="SelectCheckBox"
						AutoPostBack="true"
						OnCheckedChanged="TreeNodeCheckedChanged" />
					<asp:LinkButton runat="server" ID="TitleButton" CommandName="Navigate" />
				</div>
				<br />
			</NodeTemplate>
		</TemplatedTreeView:TemplatedTreeView>
	</ContentTemplate>
</asp:UpdatePanel>