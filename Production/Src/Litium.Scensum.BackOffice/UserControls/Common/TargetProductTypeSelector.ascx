﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TargetProductTypeSelector.ascx.cs" Inherits="Litium.Scensum.BackOffice.UserControls.Common.TargetProductTypeSelector" %>

<asp:Repeater runat="server" ID="ProductTypeRepeater">
	<ItemTemplate>
		<div>
			<asp:HiddenField runat="server" ID="ProductTypeIdHidden" Value='<%# Eval("Id") %>' />
			<asp:CheckBox runat="server" ID="ProductTypeCheckbox" Checked='<%# Eval("Selected") %>' Text='<%# Eval("Title") %>' />
		</div>
	</ItemTemplate>
</asp:Repeater>