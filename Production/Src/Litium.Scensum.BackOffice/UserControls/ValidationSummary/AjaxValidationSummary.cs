using System;
using System.Web.UI;

namespace Litium.Scensum.BackOffice.UserControls
{
	public class AjaxValidationSummary : ScensumValidationSummary
	{
		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			ScriptManager.RegisterStartupScript(Page, Page.GetType(), ID, ";", true);
		}  
	}
}