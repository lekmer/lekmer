﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Scensum.BackOffice.CommonItems;
using Litium.Scensum.BackOffice.UserControls.User.Events;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;


namespace Litium.Scensum.BackOffice.UserControls.User
{
	public partial class UserSearchCriteria : UserControl
	{
		public event EventHandler<UserSearchCriteriaEventArgs> SearchEvent;
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			btnSearch.Click += BtnSearch_Click;
			cvActivationDate.ServerValidate += ValidateDateTime;
			cvExpDate.ServerValidate += ValidateDateTime;
			cvDateCompare.ServerValidate += CompareDateTime;
		}

		private void BtnSearch_Click(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				return;
			}
			SearchEvent(this, new UserSearchCriteriaEventArgs { SearchCriteria = CreateSearchCriteria() });
		}
		private static void ValidateDateTime(object sender, ServerValidateEventArgs args)
		{
			DateTime dateTime;
			if (!DateTime.TryParse(args.Value, out dateTime))
			{
				args.IsValid = false;
				return;
			}

			DateTime minDate = Constants.MinDate;
			DateTime maxDate = Constants.MaxDate;
			if (dateTime < minDate || dateTime > maxDate)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}
		private void CompareDateTime(object sender, ServerValidateEventArgs args)
		{
			if (string.IsNullOrEmpty(tbActivationDate.Text) || !cvActivationDate.IsValid || !cvExpDate.IsValid)
			{
				args.IsValid = true;
				return;
			}
			DateTime createdFrom = DateTime.Parse(tbActivationDate.Text, CultureInfo.CurrentCulture);
			DateTime createdTo = DateTime.Parse(args.Value, CultureInfo.CurrentCulture);
			if (createdFrom > createdTo)
			{
				args.IsValid = false;
				return;
			}
			args.IsValid = true;
		}
		private bool ValidateData()
		{
			Page.Validate("vgUserSearch");
			return Page.IsValid;
		}
		public IUserSearchCriteria CreateSearchCriteria()
		{
			string actDate = string.IsNullOrEmpty(tbActivationDate.Text) ? string.Empty : tbActivationDate.Text.ToString(CultureInfo.CurrentCulture);
			string expDate = string.IsNullOrEmpty(tbExpDate.Text) ? string.Empty : tbExpDate.Text.ToString(CultureInfo.CurrentCulture);
			IUserSearchCriteria searchCriteria = IoC.Resolve<IUserSearchCriteria>();
			searchCriteria.Name = tbName.Text;
			searchCriteria.UserName = tbUserName.Text;
			searchCriteria.ActivationDate = actDate;
			searchCriteria.ExpirationDate = expDate;
			searchCriteria.UserStatusId = !string.IsNullOrEmpty(ddlStatus.SelectedValue) ? ddlStatus.SelectedValue : string.Empty;
			return searchCriteria;
		}
		public void RestoreSearchCriteriaFields(IUserSearchCriteria searchCriteria)
		{
			PopulateStatusList();

			if (searchCriteria == null)
			{
				return;
			}
			tbName.Text = searchCriteria.Name;
			tbUserName.Text=searchCriteria.UserName;
			tbActivationDate.Text = searchCriteria.ActivationDate;
			tbExpDate.Text = searchCriteria.ExpirationDate;
			ddlStatus.SelectedValue = searchCriteria.UserStatusId;
		}

		private void PopulateStatusList()
		{
			var userStatusSecureService = IoC.Resolve<ISystemUserStatusSecureService>();
			ddlStatus.DataSource = userStatusSecureService.GetAll();
			ddlStatus.DataValueField = "Id";
			ddlStatus.DataTextField = "Title";
			ddlStatus.DataBind();
			ddlStatus.Items.Insert(0, new ListItem(string.Empty, string.Empty));
		}
	}
}
