using System.Web;

namespace Litium.Scensum.BackOffice.Base
{
	public sealed class SearchCriteriaState<T>
		where T : class
	{
		#region Singleton

		private SearchCriteriaState()
		{
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes")]
		public static SearchCriteriaState<T> Instance
		{
			get { return SingletonCreator.CreatorInstance; }
		}

		private static class SingletonCreator
		{
			private static readonly SearchCriteriaState<T> _instance = new SearchCriteriaState<T>();

			public static SearchCriteriaState<T> CreatorInstance
			{
				get { return _instance; }
			}
		}

		#endregion

		private const string _keySuffix = "_SEARCH_CRITERIA";

		/// <summary>
		/// Save search criteria for this page
		/// </summary>
		/// <param name="searchCriteria"></param>
		public void Save(T searchCriteria)
		{
			HttpContext.Current.Session[_keySuffix.Insert(0, HttpContext.Current.Request.Url.LocalPath)] = searchCriteria;
		}

		/// <summary>
		/// Save search criteria for proper page
		/// </summary>
		/// <param name="pageName"></param>
		/// <param name="searchCriteria"></param>
		public void Save(string pageName, T searchCriteria)
		{
			HttpContext.Current.Session[_keySuffix.Insert(0, pageName)] = searchCriteria;
		}

		/// <summary>
		/// Get search criteria for this page
		/// </summary>
		/// <returns></returns>
		public T Get()
		{
			return HttpContext.Current.Session[_keySuffix.Insert(0, HttpContext.Current.Request.Url.LocalPath)] as T;
		}
		
		/// <summary>
		/// Get search criteria for proper page
		/// </summary>
		/// <param name="pageName"></param>
		/// <returns></returns>
		public T Get(string pageName)
		{
			return HttpContext.Current.Session[_keySuffix.Insert(0, pageName)] as T;
		}
	}
}
