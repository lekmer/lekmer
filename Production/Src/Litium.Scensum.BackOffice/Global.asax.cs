using System;
using System.Globalization;
using System.Reflection;
using System.Web;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using log4net;
using log4net.Config;

namespace Litium.Scensum.BackOffice
{
	public class Global : HttpApplication
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Start(object sender, EventArgs e)
		{
			InitializeCache();

			GlobalContext.Properties["SiteName"] = System.Web.Hosting.HostingEnvironment.SiteName;
			XmlConfigurator.Configure();

			Initializer.InitializeAll();
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Session_Start(object sender, EventArgs e)
		{
			//HttpCookie cookies = new HttpCookie(FileSystemStatePage.ViewStateCookiesKey, Guid.NewGuid().ToString());
			//Response.Cookies.Add(cookies);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2109:ReviewVisibleEventHandlers")]
		protected void Application_Error(object sender, EventArgs e)
		{
			HttpContext context = HttpContext.Current;

			var exception = context.Server.GetLastError();
			if (exception != null)
			{
				_log.Error(string.Format(CultureInfo.InvariantCulture, "\r\nServer error (alien).\r\nUrl: {0}\r\nUser ID: {1}\r\n", context.Request.Url.AbsoluteUri, SignInHelper.SignedInSystemUser.Id), exception);
			}
		}

		private void InitializeCache()
		{
			CacheUpdateCleaner.CacheUpdateFeed = IoC.Resolve<ICacheUpdateFeed>();
		}
	}
}