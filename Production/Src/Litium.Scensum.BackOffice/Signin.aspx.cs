using System;
using System.Configuration;
using System.Web.UI;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;
using Litium.Lekmer.Product;
using Litium.Lekmer.Product.Repository;

namespace Litium.Scensum.BackOffice
{
	public partial class Signin : Page
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			btnSignIn.Click += BtnSignIn_Click;

#if DEBUG
            tbUserName.Text = "admin@litium.se";
#endif
		}

		protected override void InitializeCulture()
		{
			string cultureName = ConfigurationManager.AppSettings["Culture"];
			if (!string.IsNullOrEmpty(cultureName))
			{
				Culture = UICulture = cultureName;
			}
			
			base.InitializeCulture();
		}

		protected virtual void BtnSignIn_Click(object sender, EventArgs e)
		{
			if (ValidateSystemUser())
			{
				Page.Response.Redirect(PathHelper.GetStartUrl());
			}
		}
		protected virtual bool ValidateSystemUser()
		{
			Page.Validate("SignIn");
			if (!Page.IsValid)
			{
				return false;
			}

			var systemUserSecureService = IoC.Resolve<ISystemUserSecureService>();
			ISystemUserFull userFull;
			ValidationResult validationResult = systemUserSecureService.Validate(tbUserName.Text, tbPassword.Text, out userFull);
			if (!validationResult.IsValid)
			{
				ShowErrors(validationResult.Errors);
				return false;
			}
			SignInHelper.SignedInSystemUser = userFull;
			return true;
		}

		private void ShowErrors(ErrorCollection errorCollection)
		{
			errors.AddRange(errorCollection);
		}
	}
}