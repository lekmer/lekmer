<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
    CodeBehind="PageEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.PageEdit" %>

<%@ Import Namespace="Litium.Scensum.BackOffice" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="Litium.Scensum.BackOffice.CommonItems" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="CollapsiblePanel" Src="~/UserControls/Common/CollapsiblePanel.ascx" %>
<%@ Register Assembly="Litium.Scensum.BackOffice" Namespace="Litium.Scensum.BackOffice.UserControls.ContextMenu"
    TagPrefix="sc" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
    <asp:UpdatePanel ID="ButtonsUpdatePanel" runat="server">
        <ContentTemplate>
            <uc:MessageContainer ID="errors" MessageType="Failure" HideMessagesControlId="SaveButton"
                runat="server" />
            <uc:AjaxValidationSummary runat="server" ID="ValidationSummary" ValidationGroup="vgOrdinals"
                ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="PageEditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">

    <script type="text/javascript">
	function CreateBlock(popupId, rowIndex, areaId) {
		var confirmResult = true;
		if (confirmResult) {
			var blockName = document.getElementById('<%= BlockNameTextBox.ClientID%>');
			blockName.value = '';
			HideBlockMessage();

			var hfItem = document.getElementById('<%= ItemIdHiddenField.ClientID %>');
			hfItem.value = rowIndex;

			var hfActiveAreaId = document.getElementById('<%= ActiveAreaIdHiddenField.ClientID %>');
			hfActiveAreaId.value = areaId;

			var popup = $find(popupId)
			popup.show();
		}
	}

	function HideBlockMessage() {
		var errorsDiv = document.getElementById(new String('<%= ErrorSameNameContainer.ClientID %>').concat('_divMessages'));
		if (errorsDiv != null) {
			errorsDiv.style.display = 'none'
		}
		var errorsDiv = document.getElementById('<%= ErrorSameNameDiv.ClientID %>');
		if (errorsDiv != null) {
			errorsDiv.style.display = 'none';
		}
		var v1 = document.getElementById('<%= BlockNameValidator.ClientID %>');
		if (v1 != null) {
			v1.style.display = 'none';
		}
		var avsBlock = document.getElementById('<%= BlockValidationSummary.ClientID %>');
		if (avsBlock != null) {
			avsBlock.style.display = 'none';
		}
		var ddl = document.getElementById('<%= BlockTypeList.ClientID %>');
		ddl.selectedIndex = 0;
	}

	function ShowBlockMessage() {
		var errorsDiv = document.getElementById('<%= ErrorSameNameDiv.ClientID %>');
		if (errorsDiv != null) {
			errorsDiv.style.display = 'inline';
		}
		var v1 = document.getElementById('<%= BlockNameValidator.ClientID %>');
		if (v1 != null) {
			v1.style.display = 'inline';
		}
		var avsBlock = document.getElementById('<%= BlockValidationSummary.ClientID %>');
		if (avsBlock != null) {
			avsBlock.style.display = 'inline';
		}
	}

	function BlockMenuMoveClick(ateaFromId) {
		var ddlSelectContainer = document.getElementById('<%= SelectContainerList.ClientID %>');
		ddlSelectContainer.value = ateaFromId;
	}

	function MasterBlockCollapseExpand(gridId, stateHolderId, buttonId, collapseImg, expandImg) {
		var stateHolder = document.getElementById(stateHolderId);
		var button = document.getElementById(buttonId);
		var grid = $("table[id^='" + gridId + "']:first");
		var target = grid.find('tr.master-block-data-row');
		if (!stateHolder.checked) {
			stateHolder.checked = true;
			button.src = collapseImg;
			target.show();
		}
		else {
			stateHolder.checked = false;
			button.src = expandImg;
			target.hide();
		}
	}
    </script>

    <asp:UpdatePanel ID="EditUpdatePanel" runat="server">
        <ContentTemplate>
            <div class="content-box" style="width: 626px; padding-bottom: 0px">
                <div id="image" class="left">
                    <asp:Image runat="server" ID="TemplateImage" Style="max-width: 150px; max-height: 150px;" />
                </div>
                <div class="left">
                    <div id="MasterLinkDiv" runat="server" class="left" style="padding: 0 0 15px 15px;"
                        visible="false">
                        <%= Resources.SiteStructure.Literal_MasterPage %><br />
                        <uc:HyperLinkEncoded ID="MasterLink" runat="server" ForeColor="Blue"></uc:HyperLinkEncoded>
                    </div>
                    <br runat="server" id="MasterLinkSpace" class="clear" visible="false" />
                    <div id="page-layout-title" class="left">
                        <%= Resources.SiteStructure.Literal_PageTemplate %><br />
                        <asp:Label ID="TemplateTitleLabel" runat="server"> </asp:Label>
                    </div>
                </div>
                <div class="left">
                    <asp:Repeater runat="server" ID="MasterPageChildren">
                        <HeaderTemplate>
							<div style="margin-left: 15px;">
								<%= Resources.SiteStructure.Literal_MasterPageIsUsedBy %>
							</div>
                            <div id="page-edit-master-children">
                                <ul>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <li>
								<a href="<%# ResolveUrl(PathHelper.SiteStructure.Page.GetPageEditUrl((int)Eval("Id")))%>">
                                &nbsp;<%#Eval("Title")%>
								</a>
							</li>
                        </ItemTemplate>
                        <FooterTemplate>
								</ul>
                            </div>
						</FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div style="clear: both; padding-top: 10px">
                <asp:Repeater ID="AreaGrid" runat="server">
                    <ItemTemplate>
                        <div style="margin-bottom: 15px">
                            <asp:Panel ID="EditPanel" runat="server">
                                <asp:HiddenField ID="AreaIdHiddenField" runat="server" Value='<%#Eval("Id") %>' />
                                <uc:CollapsiblePanel runat="server" ID="AreaCollapsiblePanel" HasAction="true" CommandArgument='<%# Eval("Id") %>'
                                    Title='<%# Eval("Title") %>' ActionCaption="Create" ActionButtonCaption="Block"
                                    ActionButtonImageUrl="./../../Media/Images/SiteStructure/block-add.png" />
                                <div id="BlockGridDiv" runat="server" class="site-structure-area-block-items left"
                                    style="display: none; table-layout: fixed;">
                                    <asp:GridView ID="BlocksGrid" runat="server" AutoGenerateColumns="false" SkinID="site-structure-grid"
                                        GridLines="None" Width="100%" OnRowDataBound="GVRowDataBound" OnRowCommand="GVRowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-CssClass="grid-header-left" ItemStyle-CssClass="grid-border-left"
                                                HeaderStyle-Width="25px" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                                ItemStyle-Width="25px">
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="SelectAllCheckBox" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="SelectCheckBox" runat="server" />
                                                    <asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
                                                    <asp:ImageButton ID="MasterCollapseButton" runat="server" ImageUrl="~/Media/Images/Tree/tree-collapse.png"
                                                        Visible="false" BorderStyle="Solid" BorderWidth="1px" BorderColor="#F2F2F2" />
                                                    <div style="display: none">
                                                        <asp:CheckBox ID="MasterShow" runat="server" Checked="true" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row"
                                                HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="220px">
                                                <ItemTemplate>
                                                    <div style="width: 100%; overflow: hidden;">
                                                        <div style="float: left; max-width: 230px; overflow: hidden;">
                                                            <asp:HiddenField runat="server" ID="BlockIdHiddenField" Value='<%# Eval("Id") %>' />
                                                            <asp:HiddenField runat="server" ID="BlockTypeIdHiddenField" Value='<%# Eval("BlockTypeId") %>' />
                                                            <asp:HiddenField runat="server" ID="BlockTemplateIdHiddenField" Value='<%# Eval("TemplateId") %>' />
                                                            <uc:HyperLinkEncoded runat="server" ID="EditLink" Text='<%# Eval("Title")%>' />&nbsp
                                                        </div>
                                                        <div style="float: left;">
                                                            <uc:ContextMenu ID="ContextMenuControl" runat="server" CallerImageSrc="../../../Media/Images/Common/context-menu.png"
                                                                CallerOverImageSrc="../../../Media/Images/Common/context-menu-over.png" MenuContainerCssClass="context-menu-container"
                                                                MenuShadowCssClass="context-menu-shadow" MenuCallerCssClass="context-menu-caller">
                                                                <div class="context-menu-header">
                                                                    <%= Resources.General.Literal_Manage %>
                                                                </div>
                                                                <div class="menu-row-separator">
                                                                </div>
                                                                <div class="context-menu-row">
                                                                    <img src="<%=ResolveUrl("~/Media/Images/sitestructure/block-move-to-area.png") %>" />
                                                                    <asp:LinkButton ID="BlockMenuMoveButton" runat="server" CommandName="MoveBlock" CommandArgument='<%# Eval("Id") %>'
                                                                        Text="<%$ Resources:SiteStructure, Button_Move %>" />
                                                                </div>
                                                                <div class="context-menu-row">
                                                                    <img src="<%=ResolveUrl("~/Media/Images/Common/delete.gif") %>" />
                                                                    <asp:LinkButton ID="BlockMenuDeleteButton" runat="server" OnClientClick="return DeleteConfirmation('block')"
                                                                        CommandName="DeleteBlock" CommandArgument='<%# Eval("Id") %>' Text="<%$ Resources:General, Button_Delete %>" />
                                                                </div>
                                                            </uc:ContextMenu>
                                                        </div>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row"
                                                HeaderText="<%$ Resources:General, Literal_Type %>" ItemStyle-Width="115px">
                                                <ItemTemplate>
                                                    <asp:Label ID="TypeLabel" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row"
                                                HeaderText="<%$ Resources:SiteStructure, Literal_Access %>" ItemStyle-Width="115px">
                                                <ItemTemplate>
                                                    <sc:ContextMenu3 ID="AccessList" runat="server"
														ImageExpandSrc="~/Media/Images/Common/context-menu.png"
                                                        ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png"
                                                        MenuHeaderText="Access"
                                                        ShowSelectedIcon="True">
                                                    </sc:ContextMenu3>
                                                    <asp:Label runat="server" ID="AccessLabel" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="grid-header" ItemStyle-CssClass="grid-row"
                                                HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="75px">
                                                <ItemTemplate>
                                                    <div class="status-list">
                                                        <sc:ContextMenu3 ID="StatusList" runat="server"
															ImageExpandSrc="~/Media/Images/Common/context-menu.png"
                                                            ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png"
                                                            MenuHeaderText="Status"
															ShowSelectedIcon="True">
                                                        </sc:ContextMenu3>
                                                    </div>
                                                    <asp:Label runat="server" ID="StatusLabel" Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"
                                                ItemStyle-Width="85px" HeaderStyle-CssClass="grid-header-with-asterisk" ItemStyle-CssClass="grid-row-with-asterisk-nocolor">
                                                <HeaderTemplate>
                                                    <div class="inline">
                                                        <%= Resources.General.Literal_SortOrder %>
                                                        <asp:ImageButton ID="RefreshOrderButton" runat="server" CommandName="RefreshBlockOrder"
                                                            CommandArgument='<%#Eval("ContentAreaId") %>' ImageUrl="~/Media/Images/Common/refresh.png"
                                                            ImageAlign="AbsMiddle" AlternateText="Refresh" ValidationGroup="vgOrdinals" />
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div runat="server" id="OrdinalDiv" class="inline">
                                                        <asp:RequiredFieldValidator runat="server" ID="OrdinalValidator" ControlToValidate="OrdinalTextBox"
                                                            Text="*" ErrorMessage="<%$ Resources:GeneralMessage, OrdinalMandatory %>" ValidationGroup="vgOrdinals" />
                                                        <asp:RangeValidator runat="server" ID="OrdinalRangeValidator" ControlToValidate="OrdinalTextBox"
                                                            Type="Integer" MaximumValue='<%# int.MaxValue %>' MinimumValue='<%# int.MinValue %>'
                                                            Text="*" ErrorMessage="<%$ Resources:GeneralMessage, OrdinalShouldInteger%>"
                                                            ValidationGroup="vgOrdinals" />
                                                        <asp:TextBox runat="server" ID="OrdinalTextBox" Text='<%#Eval("Ordinal") %>' />
                                                        <asp:ImageButton runat="server" ID="UpButton" CommandName="UpOrdinal" CommandArgument='<%# Eval("Id") %>'
                                                            ImageUrl="~/Media/Images/SiteStructure/up.png" ImageAlign="AbsMiddle" AlternateText="<%$ Resources:General, Button_Up %>"
                                                            ValidationGroup="vgOrdinals" />
                                                        <asp:ImageButton runat="server" ID="DownButton" CommandName="DownOrdinal" CommandArgument='<%# Eval("Id") %>'
                                                            ImageUrl="~/Media/Images/SiteStructure/down.png" ImageAlign="AbsMiddle" AlternateText="<%$ Resources:General, Button_Down%>"
                                                            ValidationGroup="vgOrdinals" />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ItemStyle-CssClass="grid-border-right" HeaderStyle-HorizontalAlign="Center"
                                                HeaderStyle-CssClass="grid-header-right" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25px">
                                                <ItemTemplate>
                                                    <asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteBlock" CommandArgument='<%# Eval("Id") %>'
                                                        ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <div runat="server" id="ApplyToAllSelectedDiv" style="display: none;" class="apply-to-all-selected">
                                        <div style="float: left; padding-top: 6px;">
                                            <div class="apply-to-all">
                                                <span>
                                                    <%= Resources.General.Literal_ApplyToAllSelectedItems %></span>
                                            </div>
                                            <div class="all-access-list">
                                                <sc:ContextMenu3 ID="AccessList" runat="server" ImageExpandSrc="~/Media/Images/Common/context-menu.png"
                                                    ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png" MenuHeaderText="Access">
                                                </sc:ContextMenu3>
                                            </div>
                                            <div class="all-status-list">
                                                <sc:ContextMenu3 ID="StatusList" runat="server" ImageExpandSrc="~/Media/Images/Common/context-menu.png"
                                                    ImageExpandMouseOverSrc="~/Media/Images/Common/context-menu-over.png" MenuHeaderText="Status">
                                                </sc:ContextMenu3>
                                            </div>
                                        </div>
                                        <div style="float: left">
                                            <uc:ImageLinkButton runat="server" ID="SetForSelectedButton" UseSubmitBehaviour="true"
                                                Text="<%$ Resources:General, Button_Set %>" SkinID="DefaultButton" CommandArgument='<%# Eval("Id") %>'
                                                OnClick="SetForSelectedClick" />
                                            <uc:ImageLinkButton runat="server" ID="DeleteSelectedButton" UseSubmitBehaviour="true"
                                                Text="<%$ Resources:General, Button_Delete%>" SkinID="DefaultButton" CommandArgument='<%# Eval("Id") %>'
                                                OnClientClick='<%#"return DeleteConfirmation(\"" + Resources.SiteStructure.Literal_selectedBlocksConfirmDelete + "\");"%>'
                                                OnClick="DeleteSelectedClick" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="clear">
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <div class="area-cancel-action">
                <div class="buttons left">
                    <uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"
                        SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmationContentNode();" />
                </div>
                <div class="buttons right">
                    <uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
                        SkinID="DefaultButton" ValidationGroup="vgOrdinals" />
                    <uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
                        CausesValidation="false" SkinID="DefaultButton" />
                </div>
            </div>
            <asp:UpdatePanel ID="BlockCreateUpdatePanel" runat="server">
                <ContentTemplate>
                    <div id="SaveDiv" runat="server" class="mainDiv" style="display: none;">
                        <asp:Panel runat="server" DefaultButton="SaveBlockButton">
                            <div id="divHeader" class="headerDiv">
                                <asp:Label ID="CreateBlockLabel" runat="server" Text="<%$ Resources:SiteStructure, Literal_CreateBlock %>" />
                                <asp:Button ID="CancelBlockButton" runat="server" Text="X" />
                            </div>
                            <div id="create-block-subdiv">
                                <div runat="server" id="ErrorSameNameDiv" style="clear: both">
                                    <uc:AjaxValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
                                        ID="BlockValidationSummary" DisplayMode="List" ValidationGroup="vgBlock" />
                                    <uc:MessageContainer ID="ErrorSameNameContainer" MessageType="Failure" HideMessagesControlId="SaveBlockButton"
                                        runat="server" />
                                </div>
                                <div class="column">
                                    <div class="input-box">
                                        <span>
                                            <%= Resources.General.Literal_Title %>&nbsp; *</span>&nbsp;
                                        <asp:RequiredFieldValidator runat="server" ID="BlockNameValidator" ControlToValidate="BlockNameTextBox"
                                            ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Text="*" ValidationGroup="vgBlock" />
                                        <br />
                                        <asp:TextBox ID="BlockNameTextBox" runat="server" />
                                    </div>
                                </div>
                                <div id="page-type-popup" class="column">
                                    <div class="input-box">
                                        <span>
                                            <%= Resources.General.Literal_Type %>&nbsp; *</span><br />
                                        <asp:DropDownList ID="BlockTypeList" runat="server" CssClass="right" />
                                    </div>
                                </div>
                                <div class="right clear">
                                    <asp:HiddenField ID="ItemIdHiddenField" runat="server" />
                                    <asp:HiddenField ID="ActiveAreaIdHiddenField" runat="server" />
                                    <uc:ImageLinkButton ID="SaveBlockButton" runat="server" Text="<%$ Resources:General,Button_Save  %>"
                                        SkinID="DefaultButton" OnClientClick="ShowBlockMessage();" ValidationGroup="vgBlock" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="HiddenField" runat="server" />
            <br class="clear" />
            <uc:MessageContainer ID="SaveMesagerContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
                runat="server" />
            <ajaxToolkit:ModalPopupExtender ID="SaveDivPopup" BehaviorID="createBlock" runat="server"
                TargetControlID="HiddenField" PopupControlID="SaveDiv" CancelControlID="CancelBlockButton"
                BackgroundCssClass="popup-background" />
            <asp:UpdatePanel ID="BlockMoveUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:HiddenField ID="MovedBlockIdHiddenField" runat="server" />
                    <asp:HiddenField ID="AreaBlockFromHiddenField" runat="server" />
                    <asp:HiddenField ID="IsMenuHiddenField" runat="server" Value="0" />
                    <div id="BlockMoveDiv" runat="server" class="block-move">
                        <div id="MoveBlockMove" class="mainDiv move-block-popap" runat="server" style="display: none;">
                            <asp:Panel ID="MoveBlockSavePanel" runat="server" DefaultButton="SaveBlockButton">
                                <div id="headerMovePopup" class="headerDiv">
                                    <asp:Label ID="MoveBlockHeaderLabel" runat="server" Text="<%$ Resources:SiteStructure,Literal_MoveBlock %>" />
                                    <asp:Button ID="MoveBlockCloseButton" runat="server" Text="X" />
                                </div>
                                <div id="main-move-popap">
                                    <div runat="server" id="ErrorMoveSameNameDiv" style="clear: both">
                                        <uc:MessageContainer ID="ErrorMoveSameNameContainer" MessageType="Failure" HideMessagesControlId="SaveBlockButton"
                                            runat="server" />
                                    </div>
                                    <div>
                                        <div class="input-box">
                                            <div>
                                                <span>
                                                    <%= Resources.SiteStructure.Literal_SelectionContainerPanel %></span><br />
                                            </div>
                                            <asp:DropDownList ID="SelectContainerList" runat="server" DataValueField="Id" DataTextField="Title">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="right clear" style="width: 120px">
                                            <uc:ImageLinkButton ID="SaveMoveBlockButton" runat="server" Text="<%$ Resources:General,Button_Ok %>"
                                                SkinID="DefaultButton" />
                                            <uc:ImageLinkButton ID="CancelMoveBlockButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>"
                                                SkinID="DefaultButton" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:HiddenField ID="MovePopupHiddenField" runat="server" />
            <ajaxToolkit:ModalPopupExtender ID="MoveBlockPopup" BehaviorID="BlockMoveUpdatePanel"
                runat="server" TargetControlID="MovePopupHiddenField" PopupControlID="MoveBlockMove"
                CancelControlID="CancelMoveBlockButton" BackgroundCssClass="popup-background" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
