using Litium.Lekmer.BackOffice.Controller;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class Default : LekmerPageController
	{
		protected override void SetEventHandlers() { }
        protected override void PopulateForm()
        {
			((Pages)Master).DenyTreeSelection();
        }
	}
}