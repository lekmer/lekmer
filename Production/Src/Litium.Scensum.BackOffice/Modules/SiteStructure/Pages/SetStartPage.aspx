<%@ Page Language="C#" MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master" CodeBehind="SetStartPage.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.SetStartPage" %>
<%@ Register Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" TagName="ContentPageSelector" TagPrefix="uc" %>
<asp:Content ID="SetStartPageContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="PagePanel" runat="server" DefaultButton="SaveButton">
		<br />
		<div id="set-start-page" class="set-start-page">
			<span class="set-start-page-header"><%= Resources.SiteStructure.Literal_StartPage %></span><br />
			<span class="set-start-page-value">
				<div style="width:300px">
					<uc:ContentPageSelector ID="ContentPageSelectorControl" runat="server" AllowClearSelection="false" ShowRegistryInPath="true"/>
				</div>
			</span>
		</div>
		<br />
		<br />
		<asp:UpdatePanel ID="ButtonUpdatePanel" runat="server">
			<ContentTemplate>
				<div class="buttons right">
					<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save%>" SkinID="DefaultButton" />
					<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel%>" SkinID="DefaultButton" />
				</div>
				<br />
				<br />
				<uc:MessageContainer ID="messager" MessageType="Success" HideMessagesControlId="SaveButton" runat="server" />
			</ContentTemplate>
		</asp:UpdatePanel>
	</asp:Panel>
</asp:Content>
