using System;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages.Validator;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.BackOffice.Modules.SiteStructure.Pages
{
	public partial class ShortcutEdit : LekmerStatePageController<int>, IEditor 
	{
		protected virtual int? GetParentNodeId()
		{
			return Request.QueryString.GetInt32OrNull("pId");
		}

        protected virtual bool HasMessage()
        {
            bool? hasMessage = Request.QueryString.GetBooleanOrNull("HasMessage");
            return hasMessage.HasValue && hasMessage.Value;
        }

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
		    int? parentNodeId = GetParentNodeId();
            if ((parentNodeId == null || parentNodeId <= 0) && (GetIdOrNull() == null))
				Master.DenyTreeSelection();
			DestinationasterixLabel.Visible = false;
		}

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            if (!WasChangeRegistry())
            {
                PopulateDestinationTreeView(null);
            }
        }
		
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			ParentTreeViewControl.NodeCommand += OnParentTreeNodeCommand;
			DistinationTreeViewControl.NodeCommand += OnDestinationTreeNodeCommand;
            DeleteButton.Click += DeleteButton_Click;
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathHelper.SiteStructure.Page.GetDeleteUrl(GetId()));
        }

		protected override void PopulateForm()
		{
            PlacementPanel.Visible = false;
            
            PopulateData();

            DeleteButton.Visible = GetIdOrNull().HasValue;
		}

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
        	ScriptManager.RegisterStartupScript(DestinationUpdatePanel, DestinationUpdatePanel.GetType(), "jsForbidSelection", "ForbidSelection();", true);
			if (DistinationTreeViewControl.SelectedNodeId <= 0)
			{
				DistinationTreeViewControl.DenySelection = true;
			}
        	if (!GetIdOrNull().HasValue)
            {
                return;
            }
            if (HasMessage() && !messager.HasMessages)
            {
                messager.MessageType = InfoType.Success;
				messager.Add(Resources.SiteStructureMessage.ContentNodeSavedSuccessfully);
            }
			if (!IsPostBack)
			{
				Master.UpdateMasterTreeAndBreadcrumbs(GetId());
			}
        }

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!DistinationTreeViewControl.SelectedNodeId.HasValue || DistinationTreeViewControl.SelectedNodeId < 0)//if "SiteStructure" root node (_id =-1)
			{
                messager.Add(Resources.SiteStructureMessage.DestinationNotSelected);
                DistinationTreeViewControl.DenySelection = true;
				DestinationasterixLabel.Visible = true;
				return;
			}
            IContentNodeShortcutLink contentNodeShortcutLink = GetNewContentNodeWithoutSelectDestination();
            if (contentNodeShortcutLink == null)
            {
                throw new BusinessObjectNotExistsException(GetId());
            }
            contentNodeShortcutLink.LinkContentNodeId = DistinationTreeViewControl.SelectedNodeId;
			
			var contentNodeShortcutLinkSecureService = IoC.Resolve<IContentNodeShortcutLinkSecureService>();
            if (!ContentNodeSaveValidator.IsCommonNameUnique(contentNodeShortcutLink))
            {
                messager.Add(Resources.SiteStructureMessage.ContentNodeCommomNameExist);
                return;
            }
			int result = contentNodeShortcutLinkSecureService.Save(SignInHelper.SignedInSystemUser, contentNodeShortcutLink);
			
            Response.Redirect(PathHelper.SiteStructure.Page.GetShortcutEditWithMessageUrl(result));
		}

        private IContentNodeShortcutLink GetNewContentNodeWithoutSelectDestination()
        {
            IContentNodeShortcutLink contentNodeShortcutLink;
            int? nodeId = GetIdOrNull();
            int? parentNodeId = GetParentNodeId();
            if (nodeId == null)
            {
                contentNodeShortcutLink = IoC.Resolve<IContentNodeShortcutLinkSecureService>().Create();
                contentNodeShortcutLink.ParentContentNodeId = parentNodeId.HasValue
                    ? (parentNodeId < 0 ? null : parentNodeId)
                    : (ParentTreeViewControl.SelectedNodeId < 0 ? null : ParentTreeViewControl.SelectedNodeId);
                contentNodeShortcutLink.ContentNodeTypeId = (int)ContentNodeTypeInfo.ShortcutLink;
                contentNodeShortcutLink.Ordinal = 0;
                contentNodeShortcutLink.ContentNodeStatusId = OnLineRadioButton.Checked
                                                    ? (int)ContentNodeStatusInfo.Online
                                                    : (int)ContentNodeStatusInfo.Offline;
                contentNodeShortcutLink.SiteStructureRegistryId = contentNodeShortcutLink.ParentContentNodeId.HasValue
                    ? IoC.Resolve<IContentNodeSecureService>().GetById(contentNodeShortcutLink.ParentContentNodeId.Value).SiteStructureRegistryId
                    : -1 * (parentNodeId.HasValue ? parentNodeId.Value : ParentTreeViewControl.SelectedNodeId.Value);
            }
            else
            {
                contentNodeShortcutLink = IoC.Resolve<IContentNodeShortcutLinkSecureService>().GetById(nodeId.Value);
            }

            contentNodeShortcutLink.Title = TitleTextBox.Text;
            contentNodeShortcutLink.CommonName = CommonNameTextBox.Text;
            contentNodeShortcutLink.AccessId = IoC.Resolve<IAccessSecureService>().GetByCommonName(AllRadioButton.Checked ? "All" : SignedInRadioButton.Checked ? "SignedIn" : "NotSignedIn").Id;
            return contentNodeShortcutLink;
        }

		protected virtual void OnParentTreeNodeCommand(object sender, CommandEventArgs e)
		{
			var nodeId = System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture);
			ParentTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			ParentTreeViewControl.DataBind();
		}

		protected virtual void OnDestinationTreeNodeCommand(object sender, CommandEventArgs e)
		{
			PopulateDestinationTreeView(System.Convert.ToInt32(e.CommandArgument, CultureInfo.CurrentCulture));
		}

		protected void PopulateDestinationTreeView(int? id)
        {
			bool isNew = GetIdOrNull() == null;
            int registryId = GetNewContentNodeWithoutSelectDestination().SiteStructureRegistryId;

            if (!isNew)
            {
            	int nodeId = GetId();
				DistinationTreeViewControl.DataSource = Master.GetTreeNodes(id ?? - 1 * registryId, false).
                    Where(item => item.Id != nodeId && item.ParentId != nodeId);
            }
            else
            {
				DistinationTreeViewControl.DataSource = Master.GetTreeNodes(id ?? -1 * registryId, false);
            }
            State = registryId;
			DistinationTreeViewControl.SelectedNodeId = id ?? -1*registryId;
			if (!IsPostBack && DistinationTreeViewControl.SelectedNodeId>0)
			{
				DistinationTreeViewControl.PopulatePath(DistinationTreeViewControl.SelectedNodeId);
			}
			DistinationTreeViewControl.DataBind();
			DistinationTreeViewControl.DenySelection = DistinationTreeViewControl.SelectedNodeId < 0;
        }

        private bool WasChangeRegistry()
        {
            return State == GetNewContentNodeWithoutSelectDestination().SiteStructureRegistryId;
        }

		protected void PopulatePlacementTreeView(int? id)
		{
			var nodeId = id ?? Master.CurrentRootId;
			ParentTreeViewControl.SelectedNodeId = nodeId;
			ParentTreeViewControl.DataSource = Master.GetTreeNodes(nodeId, true);
			ParentTreeViewControl.DataBind();
			ParentTreeViewControl.PopulatePath(nodeId);
		}

		protected void PopulateData()
        {
        	if (GetIdOrNull() == null)
			{
                Master.Breadcrumbs.Clear();
                Master.Breadcrumbs.Add(Resources.SiteStructure.Literal_CreateShortcut);
                
                if (!GetParentNodeId().HasValue)
				{
					PlacementPanel.Visible = true;
					PopulatePlacementTreeView(null);
				}
				PopulateDestinationTreeView(null);
			}
			else
			{
                IContentNodeShortcutLink contentNodeShortcutLink = IoC.Resolve<IContentNodeShortcutLinkSecureService>().GetById(GetId());

				TitleTextBox.Text = contentNodeShortcutLink.Title;
				CommonNameTextBox.Text = contentNodeShortcutLink.CommonName;

				if (null != contentNodeShortcutLink.LinkContentNodeId)
				ParentTreeViewControl.SelectedNodeId = contentNodeShortcutLink.ParentContentNodeId;
				StatusDiv.Visible = false;
				IAccess access = IoC.Resolve<IAccessSecureService>().GetById(contentNodeShortcutLink.AccessId);
				(access.CommonName == "All" ? AllRadioButton : access.CommonName == "SignedIn" ? SignedInRadioButton : NotSignedInRadioButton).Checked = true;
				PopulateDestinationTreeView(contentNodeShortcutLink.LinkContentNodeId);
			}
		}
    }
}
