﻿<%@ Page Language="C#" MasterPageFile="Ads.Master" CodeBehind="AdEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.SiteStructure.EsalesAds.AdEdit" AutoEventWireup="true"%>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="NodeSelect" Src="~/UserControls/Tree/NodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="AdImage" Src="~/Modules/SiteStructure/EsalesAds/Controls/AdImage.ascx" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="AdEditContent" runat="server" ContentPlaceHolderID="AdPlaceHolder">

	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>

	<script type="text/javascript">
		function DefaultImageLitbox() {
			media_url = '<%=ResolveUrl("~/Media") %>';
			$('.image-show a').each(function () {
				$(this).lightBox();
			});
		}

		$(function () {
			DefaultImageLitbox();
		});

		function ResetDefaultMediaMessages() {
			$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
		}
	</script>

	<div class="ad-info">
		<div class="ad-info-general rows">
			<div class="row">
				<div class="column half-width">
					<div class="input-box label-row">
						<asp:Label runat="server" AssociatedControlID="KeyTextBox"><%=Resources.EsalesAds.Literal_Key%> * (<%=Resources.EsalesAds.Literal_KeyHint%>)</asp:Label>
						<asp:TextBox ID="KeyTextBox" runat="server" MaxLength="50" />
					</div>
				</div>
				<div class="column half-width">
					<div class="input-box">
						<span ><%= Resources.General.Literal_PlaceInFolder %> *</span>
						<uc:NodeSelect ID="FolderSelector" runat="server" UseRootNode="false" />
					</div>
				</div>
				<div class="clear-zero-div">&nbsp;</div>
			</div>

			<div class="row">
				<div class="column">
					<div class="input-box label-row half-width">
						<asp:Label runat="server" AssociatedControlID="TitleTextBox"><%=Resources.General.Literal_Title%> *</asp:Label>
						<asp:TextBox ID="TitleTextBox" runat="server" />
					</div>
					
					<div class="input-box label-row left">
						<asp:Label runat="server" AssociatedControlID="StartDateTextBox"><%=Resources.General.Literal_StartDate%></asp:Label>
						<asp:TextBox ID="StartDateTextBox" runat="server"></asp:TextBox>
						<asp:ImageButton ID="StartDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle"/>
						<ajaxToolkit:CalendarExtender ID="StartDateCalendarControl" runat="server" TargetControlID="StartDateTextBox" PopupButtonID="StartDateButton" />
					</div>

					<div class="input-box label-row left">
						<asp:Label runat="server" AssociatedControlID="EndDateTextBox"><%=Resources.General.Literal_EndDate%></asp:Label>
						<asp:TextBox ID="EndDateTextBox" runat="server"></asp:TextBox>
						<asp:ImageButton ID="EndDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" ImageAlign="AbsMiddle"/>
						<ajaxToolkit:CalendarExtender ID="EndDateCalendarControl" runat="server" TargetControlID="EndDateTextBox" PopupButtonID="EndDateButton" />
					</div>

					<div class="clear-zero-div">&nbsp;</div>
				</div>

				<div class="column">
					<div class="input-box">
						<span><%=Resources.EsalesAds.Literal_ShowOnSite%> *</span>
						<asp:Repeater runat="server" ID="EsalesRegistryRepeater">
							<ItemTemplate>
								<div>
									<asp:HiddenField runat="server" ID="ChannelIdHidden" Value='<%# Eval("Id") %>' />
									<asp:CheckBox runat="server" ID="ChannelCheckbox" Text='<%# Eval("Title") %>' />
								</div>
							</ItemTemplate>
						</asp:Repeater>
					</div>
				</div>

				<div class="clear-zero-div">&nbsp;</div>
			</div>

			<div class="row full-width">
				<div class="input-box label-row">
					<asp:Label runat="server" AssociatedControlID="ProductCategoryTextBox"><%=Resources.EsalesAds.Literal_ProductCategory%></asp:Label>
					<asp:TextBox ID="ProductCategoryTextBox" runat="server" />
				</div>

				<div class="input-box label-row">
					<asp:Label runat="server" AssociatedControlID="GenderTextBox"><%=Resources.EsalesAds.Literal_Gender%></asp:Label>
					<asp:TextBox ID="GenderTextBox" runat="server" />
				</div>

				<div class="input-box label-row">
					<asp:Label runat="server" AssociatedControlID="FormatTextBox"><%=Resources.EsalesAds.Literal_Format%></asp:Label>
					<asp:TextBox ID="FormatTextBox" runat="server" />
				</div>

				<div class="input-box label-row">
					<asp:Label runat="server" AssociatedControlID="SearchTagsTextBox"><%=Resources.EsalesAds.Literal_SearchTags%></asp:Label>
					<asp:TextBox ID="SearchTagsTextBox" runat="server" />
				</div>

				<div class="clear-zero-div">&nbsp;</div>
			</div>

		</div>

		<br class="clear"/>

		<!-- Channel Properties Start -->

		<div class="ad-info-channels">
			<asp:Repeater runat="server" ID="AdChannelRepeater">
				<ItemTemplate>
					<div class="ad-info-channel">

						<div class="collapsible-panel">
							<div id="CollapsibleHeaderDiv" runat="server" class="collapsible-header">
								<div class="collapsible-header-item">
									<span><%# Eval("Title") %></span>
								</div>
								<div class="collapsible-header-image-block">
									<asp:HiddenField ID="CollapsibleStateHidden" runat="server" Value="true" />
									<asp:Image ID="CollapsibleIndicatorImage" runat="server" CssClass="right" ImageUrl="~/Media/Images/Common/up.gif" AlternateText="Up" />
								</div>
								<div class="clear-zero-div">&nbsp;</div>
							</div>

							<asp:UpdatePanel ID="CollapsibleUpdatePanel" UpdateMode="Conditional" runat="server">
								<ContentTemplate>
									<div id="CollapsibleContentDiv" runat="server" class="collapsible-content">
										<asp:HiddenField runat="server" ID="ChannelIdHidden" Value='<%# Eval("Id") %>' />
										<div class="row">
											<div class="input-box full-width label-row">
												<asp:Label runat="server" AssociatedControlID="ChannelTitleTextBox"><%=Resources.General.Literal_Title%> *</asp:Label>
												<asp:TextBox ID="ChannelTitleTextBox" runat="server" />
											</div>

											<div class="input-box full-width label-row">
												<asp:Label runat="server" AssociatedControlID="LandingPageUrlTextBox"><%=Resources.EsalesAds.Literal_LandingPageUrl%> *</asp:Label>
												<asp:TextBox ID="LandingPageUrlTextBox" runat="server" />
											</div>

											<div class="input-box">
												<span><%=Resources.EsalesAds.Literal_LandingPage%> *</span>
												<uc:ContentPageSelector ID="ContentNodeSelector" runat="server" ShowRegistryInPath="True" AllowClearSelection="true" NoSelectionText="<%# Resources.Lekmer.ContentPageSelector_None %>" />
											</div>

											<div class="input-box full-width label-row">
												<asp:Label runat="server" AssociatedControlID="ImageUrlTextBox"><%=Resources.EsalesAds.Literal_ImageUrl%> *</asp:Label>
												<asp:TextBox ID="ImageUrlTextBox" runat="server" />
											</div>
										</div>

										<div class="row">
											<uc:AdImage runat="server" id="AdImage" />
										</div>

										<div class="clear-zero-div">&nbsp;</div>
									</div>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
						<div class="clear-zero-div">&nbsp;</div>
					</div>
				</ItemTemplate>
			</asp:Repeater>
		</div>
		<!-- Channel Properties End -->

	</div>

	<br class="clear" />

	<div class="buttons left">
		<uc:ImageLinkButton ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>" SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('ad');" />
	</div>

	<div class="buttons right">
		<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton"/>
		<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton"/>
	</div>

</asp:Content>