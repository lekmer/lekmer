﻿using System.ComponentModel;

namespace Litium.Scensum.BackOffice.Modules.Campaigns
{
	public enum ProductDiscountPricesSpreadsheetColumns
	{
		[Description("HYID")]
		HYID,
		[Description("Pris SE")]
		PrisSE = 1,
		[Description("Pris NO")]
		PrisNO = 1000001,
		[Description("Pris DK")]
		PrisDK = 1000002,
		[Description("Pris FI")]
		PrisFI = 1000003,
		[Description("Pris NL")]
		PrisNL = 1000005
	}
}