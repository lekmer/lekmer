﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Esales;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockEsalesRecommendV2Edit : LekmerPageController, IEditor
	{
		private IBlockEsalesRecommendV2 _block;
		private IBlockEsalesRecommendSecureServiceV2 _blockEsalesRecommendSecureServiceV2;

		private IBlockEsalesRecommendV2 Block
		{
			get { return _block ?? (_block = BlockService.GetById(GetBlockId())); }
		}

		private IBlockEsalesRecommendSecureServiceV2 BlockService
		{
			get { return _blockEsalesRecommendSecureServiceV2 ?? (_blockEsalesRecommendSecureServiceV2 = IoC.Resolve<IBlockEsalesRecommendSecureServiceV2>()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockEsalesRecommendV2);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var block = Block;
			BlockTitleTextBox.Text = block.Title;
			BlockSetting.Setting = block.Setting;
			EsalesSubPanelNameTextBox.Text = block.PanelName;
			WindowLastEsalesValueTextBox.Text = block.WindowLastEsalesValue.ToString(CultureInfo.CurrentCulture);

			PopulateTemplateList();
			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(Block.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockEsalesRecommendV2 blockEsalesRecommendV2 = BlockService.GetById(GetBlockId());

			if (blockEsalesRecommendV2 == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockEsalesRecommendV2.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockEsalesRecommendV2.Title = BlockTitleTextBox.Text;
			BlockSetting.SetSettings(blockEsalesRecommendV2.Setting);
			blockEsalesRecommendV2.PanelName = EsalesSubPanelNameTextBox.Text;
			blockEsalesRecommendV2.WindowLastEsalesValue = int.Parse(WindowLastEsalesValueTextBox.Text, CultureInfo.CurrentCulture);

			BlockService.Save(SignInHelper.SignedInSystemUser, blockEsalesRecommendV2);
			if (blockEsalesRecommendV2.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlock);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTemplateList()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockEsalesRecommendV2");
			TemplateList.DataBind();

			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);

			if (Block.TemplateId.HasValue)
			{
				ListItem item = TemplateList.Items.FindByValue(Block.TemplateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}