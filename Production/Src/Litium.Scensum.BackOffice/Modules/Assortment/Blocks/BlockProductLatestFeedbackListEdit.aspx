﻿<%@ Page
	Language="C#"
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockProductLatestFeedbackListEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductLatestFeedbackListEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ID="ValidationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" runat="server" ValidationGroup="vgBlockProductLatestFeedbackList" />
</asp:Content>

<asp:Content ID="EditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><%=Resources.General.Literal_Title%>&nbsp;*</span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator runat="server" 
				ID="BlockTitleValidator" 
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$Resources:GeneralMessage, TitleEmpty %>" 
				Display ="None" 
				ValidationGroup="vgBlockProductLatestFeedbackList" />
			<br />
			<asp:TextBox ID="BlockTitleTextBox" runat="server" />
			<br />

			<span>
				<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:RatingReview, Literal_NumberOfItems%>" />&nbsp;*
			</span>&nbsp;
			<asp:RangeValidator runat="server" ID="NumberOfItemsRangeValidator" ControlToValidate="NumberOfItemsTextBox" Type="Integer" MinimumValue="1" MaximumValue="1000"
				ErrorMessage="<%$ Resources:RatingReview, Message_NumberOfItemsGreaterZero %>" Display="None" ValidationGroup="vgBlockProductLatestFeedbackList" />
			<asp:RequiredFieldValidator runat="server" ID="NumberOfItemsRequiredValidator" ControlToValidate="NumberOfItemsTextBox"
				ErrorMessage="<%$ Resources:RatingReview, Message_NumberOfItemsEmpty %>" Display="None" ValidationGroup="vgBlockProductLatestFeedbackList" />
			<br />
			<asp:TextBox ID="NumberOfItemsTextBox" runat="server" />
			<br />

			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings %></span>
				<br />
				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
							<span><%=Resources.General.Literal_ChooseTemplate%></span>
							<br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<br class="clear" />

		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgBlockProductLatestFeedbackList" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>

	</asp:Panel>
</asp:Content>