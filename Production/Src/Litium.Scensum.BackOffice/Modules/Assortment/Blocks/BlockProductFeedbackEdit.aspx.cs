﻿using System;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductFeedbackEdit : LekmerPageController, IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.RatingReview.Literal_BlockProductFeedbackEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var blockProductFeedback = IoC.Resolve<IBlockProductFeedbackSecureService>().GetById(GetBlockId());
			BlockTitleTextBox.Text = blockProductFeedback.Title;
			AllowReviewCheckBox.Checked = blockProductFeedback.AllowReview;

			TemplateList.DataSource = IoC.Resolve<ITemplateSecureService>().GetAllByModel("BlockProductFeedback");
			TemplateList.DataBind();

			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");

			TemplateList.Items.Insert(0, listItem);

			var item = TemplateList.Items.FindByValue(blockProductFeedback.TemplateId.ToString());
			if (item != null)
			{
				item.Selected = true;
			}

			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockProductFeedback = IoC.Resolve<IBlockProductFeedbackSecureService>().GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductFeedback.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductFeedbackSecureService>();

			var blockProductFeedback = blockService.GetById(GetBlockId());
			if (blockProductFeedback == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockProductFeedback.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductFeedback.Title = BlockTitleTextBox.Text;
			blockProductFeedback.AllowReview = AllowReviewCheckBox.Checked;

			blockService.Save(SignInHelper.SignedInSystemUser, blockProductFeedback);
			if (blockProductFeedback.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.RatingReview.Message_BlockProductFeedbackSavedSuccessfully);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}