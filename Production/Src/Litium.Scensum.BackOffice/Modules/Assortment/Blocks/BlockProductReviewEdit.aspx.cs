﻿using System;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Review;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductReviewEdit : LekmerPageController, IEditor
	{
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			
			Master.Breadcrumbs.Clear();
			Master.Breadcrumbs.Add(Resources.Review.Literal_BlockProductReviewEdit);
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var blockService = IoC.Resolve<IBlockProductReviewSecureService>();
			var blockProductReview = blockService.GetById(GetBlockId());

			BlockTitleTextBox.Text = blockProductReview.Title;
			NumberOfRowsTextBox.Text = blockProductReview.RowCount.ToString(CultureInfo.CurrentCulture);

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockProductReview");
			TemplateList.DataBind();
			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			var item = TemplateList.Items.FindByValue(blockProductReview.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			RowsNumberRangeValidator.MaximumValue = Int16.MaxValue.ToString(CultureInfo.CurrentCulture);
			RowsNumberRangeValidator.ErrorMessage = string.Format(CultureInfo.CurrentCulture,
				Resources.Review.Message_InvalidRowsNumber, Int16.MaxValue);

			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductReviewSecureService>();
			var blockProductReview = blockService.GetById(GetBlockId());
            Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductReview.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			var blockService = IoC.Resolve<IBlockProductReviewSecureService>();
			var blockProductReview = blockService.GetById(GetBlockId());
            if (blockProductReview == null) throw new BusinessObjectNotExistsException(GetBlockId());

			int templateId;
			blockProductReview.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductReview.Title = BlockTitleTextBox.Text;
			blockProductReview.RowCount = int.Parse(NumberOfRowsTextBox.Text, CultureInfo.CurrentCulture);
            blockService.Save(SignInHelper.SignedInSystemUser, blockProductReview);
			if (blockProductReview.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);
				SystemMessageContainer.Add(Resources.Review.Message_BlockProductReviewSavedSuccessfully);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}
