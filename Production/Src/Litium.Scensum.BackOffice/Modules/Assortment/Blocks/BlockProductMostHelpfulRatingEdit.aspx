﻿<%@ Page 
	Language="C#"
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockProductMostHelpfulRatingEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockProductMostHelpfulRatingEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="RatingSelector" Src="~/UserControls/Assortment/RatingSelector.ascx" %>

<asp:Content ContentPlaceHolderID="MessageContainer" runat="server">
	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ID="ValidationSummary" runat="server" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="vgMostHelpfulRating" ForeColor= "Black" />
</asp:Content>

<asp:Content ID="EditContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<script src="<%=ResolveUrl("~/Media/Scripts/common.js") %>" type="text/javascript"></script>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="content-box">
			<span><%=Resources.General.Literal_Title%></span>&nbsp;
			<uc:GenericTranslator ID="Translator" runat="server" />&nbsp;
			<asp:RequiredFieldValidator runat="server" 
				ID="BlockTitleValidator" 
				ControlToValidate="BlockTitleTextBox" 
				ErrorMessage="<%$Resources:GeneralMessage, TitleEmpty %>" 
				Display ="None" 
				ValidationGroup="vgMostHelpfulRating" />

			<br />

			<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" />

			<br />

			<div class="block-setting-container">
				<span class="bold"><%= Resources.General.Label_Settings %></span>

				<br />

				<div class="block-setting-content" style="padding-top:0">
					<div class="column">
						<div class="input-box">
							<span><%=Resources.General.Literal_ChooseTemplate%></span>
							<br />
							<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<br class="clear" />

		<div style="margin-top: 10px;">
			<span class="bold"><%=Resources.RatingReview.Literal_Rating%></span>
			<br class="clear" />
			<asp:UpdatePanel ID="RatingUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<sc:GridViewWithCustomPager 
						runat="server" ID="RatingGrid"
						SkinID="grid"
						AutoGenerateColumns="false"
						AllowPaging="true"
						PageSize="<%$AppSettings:DefaultGridPageSize%>"
						Width="100%">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
								<ItemTemplate>
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									<uc:HyperLinkEncoded ID="TitleLink" runat="server" Text='<%# Eval("Title") %>' NavigateUrl='<%# GetRatingEditUrl(Eval("Id")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
								<ItemTemplate>
									<uc:LiteralEncoded ID="CommonNameLiteral" runat="server" Text='<%# Eval("CommonName") %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="Path">
								<ItemTemplate>
									<asp:Label ID="PathLabel" runat="server" Text='<%# GetRatingPath(Eval("RatingFolderId")) %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteRating" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>

					<br />

					<div class="right">
						<uc:RatingSelector ID="RatingSelector" runat="server" DenyMultipleSelection="True" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<br class="clear" />

		<div id="category-rating-group" style="margin-bottom: 25px;">
			<asp:UpdatePanel ID="RatingItemsUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<span class="bold"><%= Resources.RatingReview.Literal_RatingItems %></span>
					<br />
					<sc:GridViewWithCustomPager ID="RatingItemsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
						AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" Width="100%">
						<Columns>
							<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
								<ItemTemplate>
									<asp:CheckBox ID="SelectCheckBox" runat="server" />
									<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
								<ItemTemplate>
									<uc:LiteralEncoded runat="server" Text='<%# string.IsNullOrEmpty(Eval("Title").ToString()) ? Eval("Score") : Eval("Title") %>' />
								</ItemTemplate>
							</asp:TemplateField>

							<asp:TemplateField HeaderText="<%$ Resources:RatingReview, Literal_Score %>">
								<ItemTemplate>
									<uc:LiteralEncoded runat="server" Text='<%# Eval("Score") %>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</sc:GridViewWithCustomPager>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<br clear="all" />

		<div id="product-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgMostHelpfulRating" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" CausesValidation="false" />
		</div>

	</asp:Panel>
</asp:Content>