using System;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;
using IBlockProductImageList = Litium.Lekmer.Product.IBlockProductImageList;
using IBlockProductImageListSecureService = Litium.Lekmer.Product.IBlockProductImageListSecureService;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockProductImageListEdit : LekmerPageController, IEditor
	{
		private IBlockProductImageList _block;

		private IBlockProductImageListSecureService _blockProductImageListService;
		private IBlockProductImageListSecureService BlockProductImageListService
		{
			get { return _blockProductImageListService ?? (_blockProductImageListService = IoC.Resolve<IBlockProductImageListSecureService>()); }
		}

		private IBlockTitleTranslationSecureService _blockTitleTranslationService;
		private IBlockTitleTranslationSecureService BlockTitleTranslationService
		{
			get { return _blockTitleTranslationService ?? (_blockTitleTranslationService = IoC.Resolve<IBlockTitleTranslationSecureService>()); }
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockProductImageListEdit);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			ImageGroupGrid.RowDataBound += ImageGroupGridRowDataBound;
		}

		protected virtual void ImageGroupGridRowDataBound(object sender, GridViewRowEventArgs e)
		{
			GridViewRow row = e.Row;
			if (row.RowType != DataControlRowType.DataRow) return;

			var productImageGroup = (IProductImageGroup) row.DataItem;
			var rb = (RadioButton) row.FindControl("SelectGroupRadioButton");
			rb.Checked = GetBlock().ProductImageGroupId == productImageGroup.Id;
			rb.Attributes.Add("onclick", "SelectRadioButton('" + rb.ClientID + "', '" + ImageGroupGrid.ClientID + "')");
		}

		protected override void PopulateForm()
		{
			IBlockProductImageList blockProductImageList = BlockProductImageListService.GetById(GetBlockId());
			BlockTitleTextBox.Text = blockProductImageList.Title;
			BlockSetting.Setting = blockProductImageList.Setting;

			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockProductImageList");
			TemplateList.DataBind();
			var listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			ListItem item = TemplateList.Items.FindByValue(blockProductImageList.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			var productImageGroupSecureService = IoC.Resolve<IProductImageGroupSecureService>();
			ImageGroupGrid.DataSource = productImageGroupSecureService.GetAll();
			ImageGroupGrid.DataBind();

			PopulateTranslation(GetBlockId());
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockProductImageList blockProductImageList = BlockProductImageListService.GetById(GetBlockId());

			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockProductImageList.ContentNodeId));
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockProductImageList blockProductImageList = BlockProductImageListService.GetById(GetBlockId());

			if (blockProductImageList == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockProductImageList.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockProductImageList.Title = BlockTitleTextBox.Text;
			BlockSetting.SetSettings(blockProductImageList.Setting);
			blockProductImageList.ProductImageGroupId = GetProductImageGroupFromGrid();
			BlockProductImageListService.Save(SignInHelper.SignedInSystemUser, blockProductImageList);
			if (blockProductImageList.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				BlockTitleTranslationService.Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductImageList);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual IBlockProductImageList GetBlock()
		{
			return _block ?? (_block = BlockProductImageListService.GetById(GetBlockId()));
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetProductImageGroupFromGrid()
		{
			int? groupId = null;
			foreach (GridViewRow row in ImageGroupGrid.Rows)
			{
				var rb = (RadioButton)row.FindControl("SelectGroupRadioButton");
				if (rb.Checked)
				{
					var hf = (HiddenField)row.FindControl("GroupIdHiddenField");
					groupId = int.Parse(hf.Value, CultureInfo.CurrentCulture);
				}
			}
			return groupId;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = BlockTitleTranslationService.GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}
