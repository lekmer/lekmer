﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Esales;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Blocks
{
	public partial class BlockEsalesCartEdit : LekmerPageController, IEditor
	{
		private IBlockList _block;
		private IBlockListSecureService _blockListSecureService;


		private IBlockList Block
		{
			get { return _block ?? (_block = BlockService.GetById(GetBlockId())); }
		}

		private IBlockListSecureService BlockService
		{
			get { return _blockListSecureService ?? (_blockListSecureService = IoC.Resolve<IBlockListSecureService>()); }
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected virtual void PopulateTemplateList()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockEsalesCart");
			TemplateList.DataBind();

			ListItem useThemeItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			useThemeItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, useThemeItem);

			if (Block.TemplateId.HasValue)
			{
				ListItem item = TemplateList.Items.FindByValue(Block.TemplateId.Value.ToString(CultureInfo.InvariantCulture));
				if (item != null)
				{
					item.Selected = true;
				}
			}
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");

			var master = Master as Pages;
			if (master != null)
			{
				master.Breadcrumbs.Clear();
				master.Breadcrumbs.Add(Resources.ProductMessage.BlockEsalesCart);
			}
		}

		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
		}

		protected override void PopulateForm()
		{
			var block = Block;
			BlockTitleTextBox.Text = block.Title;
			BlockSetting.Setting = block.Setting;

			PopulateTemplateList();
			PopulateTranslation(GetBlockId());
		}

		public void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(Block.ContentNodeId));
		}

		public void OnSave(object sender, EventArgs e)
		{
			IBlockList blockEsalesCart = BlockService.GetById(GetBlockId());

			if (blockEsalesCart == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}

			int templateId;
			blockEsalesCart.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockEsalesCart.Title = BlockTitleTextBox.Text;
			BlockSetting.SetSettings(blockEsalesCart.Setting);
			int recommendationTypeId;

			BlockService.Save(SignInHelper.SignedInSystemUser, blockEsalesCart);
			if (blockEsalesCart.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockProductAvailCombine);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}
	}
}