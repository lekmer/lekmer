﻿<%@ Page
	Language="C#"
	MasterPageFile="~/Modules/SiteStructure/Pages/Pages.Master"
	CodeBehind="BlockBrandTopListEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Blocks.BlockBrandTopListEdit" %>

<%@ MasterType VirtualPath="~/Modules/SiteStructure/Pages/Pages.Master" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" Src="~/UserControls/Tree/SelectTreeView.ascx" TagName="SelectTreeView" %>
<%@ Register TagPrefix="uc" TagName="SiteStructureNodeSelector" Src="~/UserControls/Tree/SiteStructureNodeSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="BlockSetting" Src="~/UserControls/SiteStructure/BlockSetting.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<script type="text/javascript" language="javascript">
		function confirmDeleteCategories() {
			return DeleteConfirmation("<%= Resources.Campaign.Literal_CategoriesConfirmRemove %>");
		}

		function SelectAllBelow(mainCheckBoxId, gridId) {
			$("#" + gridId).find("input[id$='SelectCheckBox']").attr('checked', $("#" + mainCheckBoxId).attr('checked'));
		}
	</script>

	<link href="../../../Media/Css/TopList.css" rel="stylesheet" type="text/css" />

	<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton" runat="server" />
	<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValdationSummary" DisplayMode="List" ValidationGroup="vgBlockBrandTopList" />
</asp:Content>

<asp:Content ID="BlockTopListContent" ContentPlaceHolderID="SiteStructureForm" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div class="toplist-content">
			<div class="block-assortment">
				<div class="input-box">
					<span>
						<asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:General,Literal_Title%>" />
					</span>
					&nbsp;
					<uc:GenericTranslator ID="Translator" runat="server" />
					&nbsp;
					<asp:RequiredFieldValidator runat="server" ID="BlockTitleValidator" ControlToValidate="BlockTitleTextBox" ErrorMessage="<%$ Resources:GeneralMessage,TitleEmpty %>" Display="None" ValidationGroup="vgBlockBrandTopList" />
					<br />
					<asp:TextBox ID="BlockTitleTextBox" Width="150px" runat="server" />
				</div>

				<br />

				<div class="column">
					<div class="input-box">
						<span>
							<asp:Literal runat="server" Text= "<%$ Resources:Lekmer,Literal_NavigationLink%>" />
						</span>
						<uc:SiteStructureNodeSelector ID="LinkNodeSelector" runat="server" AllowClearSelection="True" />
					</div>
				</div>

				<div class="column">
					<div class="input-box">
						<span>
							<asp:Literal ID="OrderStatisticsDayCountLiteral" runat="server" Text="<%$ Resources:TopList,Literal_OrderStatisticsDayCount%>" />
						</span>

						<asp:RangeValidator runat="server" ID="OrderStatisticsDayCountRangeValidator" ControlToValidate="OrderStatisticsDayCountTextBox" Type="Integer" MinimumValue="1" 
							MaximumValue="10000" ErrorMessage="<%$ Resources:TopList, Message_OrderStatisticsDayCountWithinRange %>" Display="None" ValidationGroup="vgBlockBrandTopList" />
						<asp:RequiredFieldValidator runat="server" ID="OrderStatisticsDayCountValidator" ControlToValidate="OrderStatisticsDayCountTextBox"
							ErrorMessage="<%$ Resources:TopList, Message_OrderStatisticsDayCountEmpty %>" Display="None" ValidationGroup="vgBlockBrandTopList" />
						<br />

						<asp:TextBox ID="OrderStatisticsDayCountTextBox" runat="server" />
					</div>
				</div>

				<br clear="all" />
				<br />

				<div class="toplist-categories">
					<asp:UpdatePanel ID="CategoriesUpdatePanel" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<asp:RadioButton ID="WholeRangeRadio" runat="server" Font-Bold="true" AutoPostBack="true" Text="<%$ Resources:TopList,Literal_WholeRange%>" GroupName="CategoriesRadioGroup" />
							<br />
							<asp:RadioButton ID="CategoriesRadio" runat="server" Font-Bold="true" AutoPostBack="true" Text="<%$ Resources:TopList,Literal_Categories%>" GroupName="CategoriesRadioGroup" />
							<br />
							<div id="CategoriesDiv" runat="server">
								<div>
									<sc:GridViewWithCustomPager ID="CategoriesGrid" SkinID="grid" runat="server" PageSize="<%$AppSettings:DefaultGridPageSize%>" AllowPaging="true" AutoGenerateColumns="false" Width="100%">
										<Columns>
											<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<HeaderTemplate>
													<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
												</HeaderTemplate>

												<ItemTemplate>
													<asp:CheckBox ID="SelectCheckBox" runat="server" />
													<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Category.Id") %>' runat="server" />
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="72%">
												<ItemTemplate>
													<uc:LiteralEncoded runat="server" ID="TitleLiteral" Text='<%# Eval("Category.Title")%>'></uc:LiteralEncoded>
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderText="<%$ Resources:TopList, Literal_IncludeSubcategory %>" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="22%">
												<ItemTemplate>
													<asp:CheckBox ID="IncludeSubcategoryCheckBox" runat="server" Checked='<%# Eval("IncludeSubcategories")%>' AutoPostBack="true" OnCheckedChanged="OnIncludeSubcategoriesChanged" />
												</ItemTemplate>
											</asp:TemplateField>

											<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
												<ItemTemplate>
													<asp:ImageButton runat="server" ID="DeleteButton" CommandName="DeleteCategory" CommandArgument='<%# Eval("Category.Id") %>'
														UseSubmitBehaviour="true" ImageUrl="~/Media/Images/Common/delete.gif" AlternateText="<%$ Resources:General, Button_Delete %>"
														OnClientClick='<%# "return DeleteConfirmation(\"" + Resources.Product.Literal_Category + "\");" %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</sc:GridViewWithCustomPager>
								</div>

								<div id="CategoriesAllSelectedDiv" runat="server" style="display: none;" class="toplist-apply-to-all-selected">
									<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CategoriesRemoveSelectedButton" Text="<%$Resources:General, Button_RemoveSelected %>" 
										OnClientClick="return confirmDeleteCategories();" runat="server" SkinID="DefaultButton" />
								</div>
								<br style="clear: both;" />
								<div class="right">
									<uc:ImageLinkButton ID="CategoriesShowPopupButton" UseSubmitBehaviour="true" runat="server" Text="<%$ Resources:General,Button_Add%>" SkinID="DefaultButton" CausesValidation="false" />
								</div>
							</div>

							<ajaxToolkit:ModalPopupExtender ID="CategoriesPopup" runat="server" TargetControlID="CategoriesShowPopupButton"
								PopupControlID="CategoriesPopupPanel" CancelControlID="CategoriesCancelPopupButton" BackgroundCssClass="PopupBackground" RepositionMode="RepositionOnWindowResize" />

							<div id="CategoriesPopupPanel" class="PopupContainer" style="display: none;" runat="server">
								<div class="PopupHeader">
									<span>
										<asp:Literal runat="server" Text="<%$ Resources:Product,Label_AddCategoriesToBlock%>" />
									</span>
									<asp:Button ID="CategoriesCancelPopupButton" CausesValidation="false" runat="server" Text="X" />
								</div>

								<div class="PopupBody">
									<uc:SelectTreeView runat="server" ID="CategoriesTree" />
									<br />
									<uc:ImageLinkButton ID="CategoriesOkPopupButton" UseSubmitBehaviour="true" runat="server" Text="Ok" SkinID="DefaultButton" CausesValidation="false" CssClass="PopupButton" />
								</div>
							</div>
						</ContentTemplate>
					</asp:UpdatePanel>
				</div>

				<br clear="all"/>

				<div id="Settings" runat="server">
					<label class="block-header">
						<asp:Literal runat="server" Text="<%$ Resources:General,Label_Settings%>" />
					</label>

					<fieldset>
						<div class="column">
							<div class="input-box">
								<span>
									<asp:Literal runat="server" Text="<%$ Resources:General,Literal_ChooseTemplate%>" />
								</span>
								<br />
								<asp:DropDownList ID="TemplateList" runat="server" DataTextField="Title" DataValueField="Id" />
							</div>
						</div>

						<br class="clear" />
						<uc:BlockSetting runat="server" Id="BlockSetting" ValidationGroup="vgBlockBrandTopList"/>
					</fieldset>

					<br class="clear" />
				</div>
			</div>

			<br />

			<div class="right">
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General,Button_Save%>" SkinID="DefaultButton" ValidationGroup="vgBlockBrandTopList" />
				<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel%>" SkinID="DefaultButton" CausesValidation="false" />
			</div>
		</div>
	</asp:Panel>
</asp:Content>