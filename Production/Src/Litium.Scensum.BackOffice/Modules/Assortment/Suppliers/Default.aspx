﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Suppliers.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="SuppliersTab" ContentPlaceHolderID="body" runat="server">
	<sc:ToolBoxPanel ID="SuppliersToolBoxPanel" runat="server" Text="Suppliers" ShowSeparator="true" style="float:left">
		<asp:Panel runat="server" ID="SearchPanel" DefaultButton="SearchButton">
			<div class="fontSearch" style="padding-bottom:8px; padding-right:5px; padding-top: 10px; float:left;">
				<span style="float:left; font-size:16px; padding:0px 15px;">
					<%= Resources.General.Literal_Search %>
				</span>
				<asp:TextBox ID="SearchTextBox" runat="server" Style="float: left;" />
			</div>

			<div style="float: left; padding:8px 0px; font-size: 0.7em; font-weight: normal;">
				<uc:ImageLinkButton ID="SearchButton" runat="server" Text="<%$ Resources:General, Button_Find %>" SkinID="HeaderPanelButton"  UseSubmitBehaviour="false" />
			</div>
		</asp:Panel>
	</sc:ToolBoxPanel>

	<div class="stores-place-holder">
		<sc:GridViewWithCustomPager
			ID="SuppliersGrid"
			SkinID="grid"
			AutoGenerateColumns="false"
			Width="100%"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			PagerSettings-Mode="NumericFirstLast"
			runat="server">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_SupplierNo %>" ItemStyle-Width="10%">
					<ItemTemplate>
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
						<uc:HyperLinkEncoded ID="SupplierLink" runat="server" Text='<%# Eval("SupplierNo")%>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:Lekmer,Literal_Name %>" DataField="Name" ItemStyle-Width="30%" />
				<asp:BoundField HeaderText="<%$ Resources:Customer,Literal_Email %>" DataField="Email" ItemStyle-Width="25%" />
				<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_IsDropShip %>" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
					<ItemTemplate>
						<asp:CheckBox ID="IsDripShipCheckBox" runat="server" Enabled="False" Checked='<%# Eval("IsDropShip")%>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:Lekmer,Literal_DropShipEmail %>" DataField="DropShipEmail" ItemStyle-Width="25%" />
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
</asp:Content>