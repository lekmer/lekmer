using System;
using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RelationLists
{
	public partial class RelationListEdit : LekmerPageController, IEditor
	{
		private ICategory _category;

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
            DeleteButton.Click += DeleteButton_Click;
			
		}

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            // GetIdOrNull().HasValue 
            IoC.Resolve<IRelationListSecureService>().Delete(SignInHelper.SignedInSystemUser, GetId());
            RedirectBack();
        }

		protected override void PopulateForm()
		{
            var relationListTypeSecureService = IoC.Resolve<IRelationListTypeSecureService>();
		    IRelationList relationList = null;

            TypeList.DataSource = relationListTypeSecureService.GetAll();
            TypeList.DataBind();
            if (GetIdOrNull() != null)
            {
                relationList = IoC.Resolve<IRelationListSecureService>().GetById(GetId());
                RelationListTitleTextBox.Text = relationList.Title;
                TypeList.Items.FindByValue(relationList.RelationListTypeId.ToString(CultureInfo.CurrentCulture)).Selected = true;
            }
            else
            {
                TypeList.Items[0].Selected = true;
            }

		    SetRelationListBreadcrumb(relationList);

			var productSecureService = IoC.Resolve<IProductSecureService>();
			RelationListProducts.Products = GetIdOrNull() == null ? new Collection<IProduct>() : productSecureService.GetAllByRelationList(ChannelHelper.CurrentChannel.Id, GetId());
			RelationListUsedTo.Products = GetIdOrNull() == null ? new Collection<IProduct>() : productSecureService.GetAllUsingRelationList(ChannelHelper.CurrentChannel.Id, GetId()); 
			if (JustCreated()!=null)
			{
				if ((bool)JustCreated())
				message.Add(Resources.ProductMessage.RelationListSavedSuccessfully);
			}

		    DeleteButton.Visible = GetIdOrNull().HasValue;
		}

        private void SetRelationListBreadcrumb(IRelationList relationList)
        {
            if (relationList == null)
                RelationListTitleLabel.Text = Resources.ProductMessage.CreateRelationList;
            else
                RelationListTitleLabel.Text = relationList.Title;
        }

	    protected virtual bool? JustCreated()
		{
			return Request.QueryString.GetBooleanOrNull("JustCreated");
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
			Response.Redirect(PathHelper.Assortment.Relation.GetDefaultUrl());
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			var relationListSecureService = IoC.Resolve<IRelationListSecureService>();
			IRelationList relationList = GetIdOrNull() != null ? relationListSecureService.GetById(GetId()) : relationListSecureService.Create();
			if (relationList == null)
			{
				throw new BusinessObjectNotExistsException(GetId());
			}

			relationList.Title = RelationListTitleTextBox.Text;
			relationList.RelationListTypeId = int.Parse(TypeList.SelectedValue, CultureInfo.CurrentCulture);
			bool justCreated = false;
			if (relationList.Id == 0)
			{
				relationList.SetNew();
				justCreated = true;
			}
			relationListSecureService.Save(SignInHelper.SignedInSystemUser, relationList, RelationListProducts.Products, RelationListUsedTo.Products);
			if (justCreated)
			{
				Response.Redirect(PathHelper.Assortment.Relation.GetEditUrlForJustCreated(relationList.Id));
			}

		    SetRelationListBreadcrumb(relationList);

			message.Add(Resources.ProductMessage.RelationListSavedSuccessfully);
		}
		
		protected void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer != null)
			{
				Response.Redirect(PathHelper.Assortment.Relation.GetEditReferrerUrl(referrer));
			}
			else
			{
				Response.Redirect(PathHelper.Assortment.Relation.GetDefaultUrl());
			}
		}

		protected string GetCategory(int categoryId)
		{
			if (_category == null)
			{
				var categorySecureService = IoC.Resolve<ICategorySecureService>();
				_category = categorySecureService.GetById(categoryId);
			}
			return _category.Title;
		}
	}
}
