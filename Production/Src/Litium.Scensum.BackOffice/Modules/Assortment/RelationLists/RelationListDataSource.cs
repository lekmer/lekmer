using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.RelationLists
{
	public class RelationListDataSource
	{
		private int _rowCount;

		public int RelationListId { get; set; }

		public int SelectCount()
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "typeId"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "title"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex, string title, string typeId)
		{
			return _rowCount;
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
	System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public Collection<IRelationList> SelectMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		public Collection<IRelationList> SelectMethod(int maximumRows, int startRowIndex, string title, string typeId)
		{
			IRelationListSecureService relationListSecureService = IoC.Resolve<IRelationListSecureService>();
			IRelationListSearchCriteria searchCriteria = IoC.Resolve<IRelationListSearchCriteria>();
			searchCriteria.Title = title;
			searchCriteria.TypeId = typeId;
			var relationLists = relationListSecureService.Search(searchCriteria, startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = relationLists.TotalCount;
			return relationLists;
		}
	}
}

