﻿<%@ Page Language="C#" MasterPageFile="ContentMessage.Master" CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Messages.Default" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="MessageContent" ContentPlaceHolderID="MessageContainer" runat="server">
	<asp:UpdatePanel id="UpdatePanelMessage" runat="server">
		<ContentTemplate>
			<uc:MessageContainer ID="SystemMessageContainer" runat="server" MessageType="Failure" HideMessagesControlId="SaveButton" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

<asp:Content ID="DefaultContent" runat="server" ContentPlaceHolderID="ContentMessagePlaceHolder">
	<script type="text/javascript">
		function confirmDelete() {
			return DeleteConfirmation("<%= Resources.Lekmer.Literal_ContentMessage %>");
		}
	</script>

	<div class="product-campaigns">
		<sc:GridViewWithCustomPager 
			ID="ContentMessagesGrid"
			SkinID="grid"
			runat="server"
			AutoGenerateColumns="false"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			Width="100%">

			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General, Literal_CommonName %>">
					<ItemTemplate>
						<uc:HyperLinkEncoded ID="CommonNameLink" runat="server" Text='<%# Eval("CommonName") %>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField HeaderText="Path">
					<ItemTemplate>
						<asp:Label ID="PathLabel" runat="server" Text='<%# IsSearchResult ? GetPath(Eval("ContentMessageFolderId")) : string.Empty %>' />
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
					<ItemTemplate>
						<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteContentMessage" CommandArgument='<%# Eval("Id") %>' ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return confirmDelete();" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>
	</div>
</asp:Content>