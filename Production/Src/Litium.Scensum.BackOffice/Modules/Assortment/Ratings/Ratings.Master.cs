﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Ratings
{
	public partial class RatingsMaster : MasterPage
	{
		// Private Members.

		private Collection<string> _breadcrumbAppend;


		// Public Properties.

		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				return _breadcrumbAppend ?? (_breadcrumbAppend = new Collection<string>());
			}
		}

		public virtual bool DenySelection
		{
			get
			{
				return RatingFoldersTree.DenySelection;
			}
			set
			{
				RatingFoldersTree.DenySelection = value;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session["SelectedRatingFolderId"];
			}
			set
			{
				Session["SelectedRatingFolderId"] = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return RatingFoldersTree.RootNodeId;
			}
		}

		public virtual string RootNodeTitle
		{
			get
			{
				return RatingFoldersTree.RootNodeTitle;
			}
		}


		// Protected Properties.

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}


		// Protected Methods.

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);

			SearchButton.Click += OnSearch;
			CreateButton.Click += OnRatingCreateFromPanel;
			RatingFoldersTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);

			if (SelectedFolderId == null)
			{
				SelectedFolderId = RootNodeId;
			}

			if (!IsPostBack)
			{
				PopulateTree(null);
				RestoreSearchField();
				BuildBreadcrumbs();
			}

			SetupTabAndPanel();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (RatingFoldersTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}

			ScriptManager.RegisterStartupScript(
				LeftUpdatePanel,
				LeftUpdatePanel.GetType(),
				"root menu",
				string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", RatingFoldersTree.ClientID),
				true);
		}

		protected virtual void RestoreSearchField()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(LekmerPathHelper.Rating.GetSearchResultUrl());

			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		protected virtual void BuildBreadcrumbs()
		{
			if (IsSearchResult)
			{
				Breadcrumbs.Text = Resources.General.Literal_SearchResults;
				return;
			}

			string breadcrumbs = string.Empty;
			const string bredcrumbsSeperator = " > ";

			if (SelectedFolderId.HasValue && SelectedFolderId.Value != RootNodeId)
			{
				var folders = GetNodes(SelectedFolderId);
				var folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
				while (folder != null)
				{
					breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", bredcrumbsSeperator, folder.Title, breadcrumbs);

					int? parentNodeId = folder.ParentId;

					folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
				}
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, bredcrumbsSeperator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", RootNodeTitle, breadcrumbs);
			Breadcrumbs.Text = breadcrumbs;
		}

		protected virtual void OnRatingCreateFromPanel(object sender, EventArgs e)
		{
			SelectedFolderId = null;

			Response.Redirect(LekmerPathHelper.Rating.GetCreateUrl(RatingFoldersTree.RootNodeId));
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			SelectedFolderId = null;

			var searchUrl = LekmerPathHelper.Rating.GetSearchResultUrl();
			SearchCriteriaState<string>.Instance.Save(searchUrl, SearchTextBox.Text);

			Response.Redirect(searchUrl);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);

			if (e.Id == -1)
			{
				OnFolderCreate(sender, e);
			}
			else
			{
				switch (e.EventName)
				{
					case "Expand":
						DenySelection = true;
						break;
					case "Navigate":
						SelectedFolderId = e.Id;
						RedirectToDefaultPage();
						break;
				}
			}
		}

		protected virtual void OnFolderCreate(object sender, EventArgs e)
		{
			SelectedFolderId = RatingFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.RatingFolder.GetCreateUrl(SelectedFolderId.Value));
			}
		}

		protected virtual void OnFolderEdit(object sender, EventArgs e)
		{
			SelectedFolderId = RatingFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.RatingFolder.GetEditUrl(SelectedFolderId.Value));
			}
		}

		protected virtual void OnFolderDelete(object sender, EventArgs e)
		{
			if (RatingFoldersTree.MenuLastClickedNodeId == null)
			{
				return;
			}

			var folderId = RatingFoldersTree.MenuLastClickedNodeId.Value;

			var ratingFolderSecureService = IoC.Resolve<IRatingFolderSecureService>();

			int? parentFolderId = ratingFolderSecureService.GetById(folderId).ParentRatingFolderId;

			if (!ratingFolderSecureService.TryDelete(SignInHelper.SignedInSystemUser, folderId))
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingFolderDeleteFailed);
				return;
			}

			SystemMessageContainer.MessageType = InfoType.Success;
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);

			SelectedFolderId = parentFolderId.HasValue ? parentFolderId.Value : RootNodeId;

			PopulateTree(null);

			RedirectToDefaultPage();
		}

		protected virtual void OnRatingCreate(object sender, EventArgs e)
		{
			SelectedFolderId = RatingFoldersTree.MenuLastClickedNodeId;

			if (SelectedFolderId.HasValue)
			{
				Response.Redirect(LekmerPathHelper.Rating.GetCreateUrl(SelectedFolderId.Value));
			}
		}


		// Public Methods.

		public virtual void PopulateTree(int? folderId)
		{
			RatingFoldersTree.DataSource = GetNodes(folderId);
			RatingFoldersTree.RootNodeTitle = Resources.RatingReview.Literal_RatingFolders;
			RatingFoldersTree.DataBind();

			LeftUpdatePanel.Update();

			RatingFoldersTree.SelectedNodeId = folderId ?? SelectedFolderId;
		}

		public virtual void SetupTabAndPanel()
		{
			if (Master != null)
			{
				var topMaster = (Master.Start)(Master).Master;
				if (topMaster != null)
				{
					topMaster.SetActiveTab("Assortment", "Ratings");
				}
			}

			RatingPanel.Text = Resources.RatingReview.Literal_Ratings;
			CreateButton.Text = Resources.RatingReview.Literal_Rating;
		}

		public virtual Collection<INode> GetNodes(int? folderId)
		{
			var id = folderId ?? ((SelectedFolderId == RootNodeId) ? null : SelectedFolderId);

			return IoC.Resolve<IRatingFolderSecureService>().GetTree(id);
		}

		public virtual void UpdateSelection(int nodeId)
		{
			RatingFoldersTree.SelectedNodeId = SelectedFolderId = nodeId;
			LeftUpdatePanel.Update();
		}

		public virtual void UpdateBreadcrumbs(string value)
		{
			Breadcrumbs.Text = value;
		}

		public virtual void UpdateBreadcrumbs(int? folderId)
		{
			SelectedFolderId = folderId;
			BuildBreadcrumbs();
		}

		public virtual void RedirectToDefaultPage()
		{
			Response.Redirect(LekmerPathHelper.Rating.GetDefaultUrl());
		}
	}
}