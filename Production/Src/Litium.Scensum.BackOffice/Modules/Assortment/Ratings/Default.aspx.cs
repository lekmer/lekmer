﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Ratings
{
	public partial class Default : LekmerPageController
	{
		private Collection<IRatingFolder> _allFolders;

		public virtual RatingsMaster MasterPage
		{
			get
			{
				return (RatingsMaster)Master;
			}
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}

		protected override void SetEventHandlers()
		{
			RatingsGrid.RowCommand += OnRowCommand;
			RatingsGrid.RowDataBound += OnRowDataBound;
			RatingsGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateRatings();
		}

		protected virtual void OnRowCommand(object sender, CommandEventArgs e)
		{
			int id;
			if (!int.TryParse(e.CommandArgument.ToString(), out id))
			{
				return;
			}

			try
			{
				switch (e.CommandName)
				{
					case "DeleteRating":
						DeleteRating(id);
						PopulateRatings();
						SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful, InfoType.Success);
						break;
				}
			}
			catch
			{
				SystemMessageContainer.Add(Resources.RatingReview.RatingUpdateFailed);
			}
			
		}

		protected virtual void OnRowDataBound(object sender, GridViewRowEventArgs e)
		{
			SetSelectionFunction((GridView)sender, e.Row);
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			RatingsGrid.PageIndex = e.NewPageIndex;

			PopulateRatings();
		}

		protected virtual void DeleteRating(int ratingId)
		{
			IoC.Resolve<IRatingSecureService>().Delete(SignInHelper.SignedInSystemUser, ratingId);
		}

		protected virtual void PopulateRatings()
		{
			if (IsSearchResult)
			{
				RatingsGrid.DataSource = SearchRatings();
				RatingsGrid.Columns[3].Visible = true;
			}
			else
			{
				RatingsGrid.DataSource = RetrieveRatings();
				RatingsGrid.Columns[3].Visible = false;
			}

			RatingsGrid.DataBind();

			RatingsGrid.Visible = RatingsGrid.DataSource != null;
		}

		protected virtual string GetEditUrl(object ratingId)
		{
			return LekmerPathHelper.Rating.GetEditUrl(System.Convert.ToInt32(ratingId, CultureInfo.CurrentCulture));
		}

		protected virtual Collection<IRating> RetrieveRatings()
		{
			int? folderId = MasterPage.SelectedFolderId != MasterPage.RootNodeId ? MasterPage.SelectedFolderId : null;
			if (folderId.HasValue)
			{
				return IoC.Resolve<IRatingSecureService>().GetAllByFolder(folderId.Value);
			}

			return null;
		}

		protected virtual Collection<IRating> SearchRatings()
		{
			var searchUrl = LekmerPathHelper.Rating.GetSearchResultUrl();

			string searchCriteria = SearchCriteriaState<string>.Instance.Get(searchUrl);

			if (string.IsNullOrEmpty(searchCriteria))
			{
				return new Collection<IRating>();
			}

			return IoC.Resolve<IRatingSecureService>().Search(searchCriteria);
		}

		protected virtual string GetPath(object folderId)
		{
			if (_allFolders == null)
			{
				var service = IoC.Resolve<IRatingFolderSecureService>();
				_allFolders = service.GetAll();
			}

			var ratingHelper = new FolderPathHelper();
			return ratingHelper.GetPath(_allFolders, System.Convert.ToInt32(folderId, CultureInfo.CurrentCulture));
		}

		protected virtual void SetSelectionFunction(GridView grid, GridViewRow row)
		{
			if (row.RowType == DataControlRowType.Header)
			{
				var selectAllCheckBox = (CheckBox) row.FindControl("SelectAllCheckBox");

				selectAllCheckBox.Checked = false;
				selectAllCheckBox.Attributes.Add("onclick", "SelectAll1('" + selectAllCheckBox.ClientID + @"','" + grid.ClientID + @"');");
			}
			else if (row.RowType == DataControlRowType.DataRow)
			{
				var cbSelect = (CheckBox)row.FindControl("SelectCheckBox");
				var selectAllCheckBox = (CheckBox)grid.HeaderRow.FindControl("SelectAllCheckBox");

				if (cbSelect == null || selectAllCheckBox == null)
				{
					return;
				}

				cbSelect.Attributes.Add("onclick", "javascript:UnselectMain(this,'" + selectAllCheckBox.ClientID + @"');");
			}
		}
	}
}