﻿<%@ Page
	Title=""
	Language="C#"
	MasterPageFile="~/Modules/Assortment/Brands/BrandMaster.Master"
	CodeBehind="BrandEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Brands.BrandEdit" %>

<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register TagPrefix="uc" TagName="RegistryRestriction" Src="~/UserControls/Assortment/RegistryRestriction.ascx" %>
<%@ Register TagPrefix="uc" TagName="BrandMedia" Src="~/Modules/Assortment/Brands/Controls/BrandMedia.ascx" %>
<%@ Register TagPrefix="uc" TagName="ContentNodeSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="DeliveryTimeConfigurator" Src="~/UserControls/Assortment/DeliveryTimeConfigurator.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">
	<div>
		<uc:MessageContainer ID="Messager" MessageType="Warning" HideMessagesControlId="SetStatusButton" runat="server" />
		<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary" ID="ValidatorSummary" DisplayMode="List" ValidationGroup="BrandValidationGroup" />
	</div>
</asp:Content>

<asp:Content ID="BrandEditContent" ContentPlaceHolderID="BrandPlaceHolder" runat="server">
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />

	<script type="text/javascript">
		$(document).ready(function () { $('#tabs').tabs(); });

		function submitForm() {
			tinyMCE.triggerSave();
		}

		function ConfirmBrandDelete() {
			return DeleteConfirmation('<%= Resources.Product.Literal_DeleteBrand %>');
		}
	</script>

	<asp:Panel ID="BrandEditPanel" runat="server" DefaultButton="SaveButton">
		<div class="brand-edit-container">
			<div class="brand-edit-header">
				<uc:LiteralEncoded ID="BrandHeader" runat="server"></uc:LiteralEncoded>
			</div>
			<div id="tabs">
				<ul>
					<li><a href="#fragment-1"><span><%= Resources.Product.Tab_ProductEdit_Info %></span></a></li>
					<li><a href="#fragment-2"><span><%= Resources.Product.Tab_ProductEdit_SiteStructure %></span></a></li>
				</ul>
				<br clear="all" />
				<div class="brand-tabs-main-container" id="tabs-main-container">
					<div id="fragment-1">
						<div class="column">
							<div style="padding: 10px 0px 0px 0px;">
								<span><%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp
								<uc:GenericTranslator ID="BrandTitleTranslator" runat="server" />&nbsp;
								<asp:RequiredFieldValidator runat="server" ID="BrandTitleValidator" ControlToValidate="BrandTitleBox"
									ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="BrandValidationGroup" />
								<br />
								<asp:TextBox ID="BrandTitleBox" runat="server" MaxLength="100" Width="430px"></asp:TextBox>
								<br />
								<span><%= Resources.Product.Literal_BrandExternalUrl %></span><br />
								<asp:TextBox ID="BrandUrlBox" runat="server" Width="430px"></asp:TextBox>
								<br />
								
								<span><%= Resources.Lekmer.Literal_MonitorThreshold %></span>&nbsp;
								<asp:RangeValidator ID="MonitorThresholdRangeValidator" runat="server" ControlToValidate="MonitorThresholdBox"
									Display="None" ErrorMessage="<%$ Resources:LekmerMessage, MonitorThresholdIncorrect %>" 
									MinimumValue="1" MaximumValue="1000" Type="Integer" ValidationGroup="BrandValidationGroup">
								</asp:RangeValidator>
								<br />
								<asp:TextBox ID="MonitorThresholdBox" runat="server" Width="430px"></asp:TextBox>
								<br />
								
								<span><%= Resources.Lekmer.Literal_MaxQuantityPerOrder %></span>&nbsp;
								<asp:RangeValidator ID="MaxQuantityPerOrderRangeValidator" runat="server" ControlToValidate="MaxQuantityPerOrderBox"
									Display="None" ErrorMessage="<%$ Resources:LekmerMessage, MaxQuantityPerOrderIncorrect %>" 
									MinimumValue="1" MaximumValue="1000000000" Type="Integer" ValidationGroup="BrandValidationGroup">
								</asp:RangeValidator>
								<br />
								<asp:TextBox ID="MaxQuantityPerOrderBox" runat="server" Width="430px"></asp:TextBox>
								<br />
								<div class="input-box">
									<div style="float:left">
										<span><%=Resources.Lekmer.Literal_DeliveryTimeFrom%></span><br />
										<asp:TextBox ID="DeliveryTimeFromBox" runat="server" Width="125px" Enabled="False" />
									</div>
									<div style="float:left; padding-left: 5px">
										<span><%=Resources.Lekmer.Literal_DeliveryTimeTo%></span><br />
										<asp:TextBox ID="DeliveryTimeToBox" runat="server" Width="125px" Enabled="False" />
									</div>
									<div style="float:left; padding-left: 5px">
										<span><%=Resources.Lekmer.Literal_DeliveryTimeFormat%></span><br />
										<asp:TextBox ID="DeliveryTimeFormatBox" runat="server" Width="125px" Enabled="False" />
										<uc:DeliveryTimeConfigurator ID="DeliveryTimeConfigurator" runat="server" />
									</div>
									<br style="clear: both;"/>
								</div>
								<asp:CheckBox ID="OfflineCheckBox" runat="server" Text="<%$ Resources:LekmerMessage, BrandEdit_Offline%>"/>

								<br/>
								<uc:BrandMedia runat="server" Id="BrandMediaControl" />
							</div>
							<br class="clear" />
							<div style="width: 448px;">
								<span class="bold"><%= Resources.Lekmer.Literal_Icons %></span>
								<br/>
								<asp:Repeater ID="IconRepeater" runat="server">
									<ItemTemplate>
										<div class="product-icon-wrapper">
											<asp:HiddenField ID="IconIdHidden" runat="server" Value='<%# Eval("Id")%>' />
											<asp:CheckBox ID="IconCheckBox" runat="server" />
											<asp:Image ID="IconImage" runat="server" ImageAlign="AbsMiddle" CssClass="product-icon-img"
												ImageUrl='<%# ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(Convert.ToInt32(Eval("Image.Id")), Eval("Image.FormatExtension").ToString())) %>' />
										</div>
									</ItemTemplate>
								</asp:Repeater>
							</div>
						</div>
						<div class="column">
							<div style="padding: 10px 0px 0px 10px;">
								<span><%= Resources.General.Literal_Description %></span>&nbsp
								<asp:ImageButton runat="server" ID="BrandDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
								<br />
								<uc:LekmerTinyMceEditor ID="BrandDescriptionEditor" runat="server" SkinID="tinyMCE" />
							</div>
							<br class="clear" />
							<div style="padding: 10px 0px 0px 10px;">
								<span><%= Resources.Product.Literal_PackageInfo %></span>&nbsp
								<asp:ImageButton runat="server" ID="BrandPackageInfoTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
								<br />
								<uc:LekmerTinyMceEditor ID="BrandPackageInfoEditor" runat="server" SkinID="tinyMCE" />
							</div>
						</div>
					</div>
					<div id="fragment-2">
						<div style="margin-top: 10px;">
							<span class="bold"><%= Resources.General.Literal_SiteStructureRegistryList %></span>
							<asp:GridView ID="SiteStructureRegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
								<Columns>
									<asp:TemplateField HeaderText="<%$ Resources:General,Literal_SiteStructureRegistry %>" ItemStyle-Width="15%">
										<ItemTemplate>
											<asp:HiddenField ID="RegistryIdHiddenField" runat="server" Value='<%#Eval("Id")%>' />
											<asp:Label ID="TitleLabel" runat="server"></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="<%$ Resources:Lekmer, Literal_ContentNode %>" ItemStyle-Width="85%">
										<ItemTemplate>
											<uc:ContentNodeSelector ID="ContentNodeSelector" runat="server" AllowClearSelection="true" NoSelectionText="<%$ Resources:Lekmer, ContentPageSelector_None %>" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
							<br class="clear" />
							<uc:RegistryRestriction ID="ProductRegistryRestriction" runat="server" Width="100%" />
						</div>
					</div>
				</div>

				<br class="clear" />

				<div id="model-buttons" class="buttons right">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Save %>"
						SkinID="DefaultButton" ValidationGroup="BrandValidationGroup" />
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
						SkinID="DefaultButton" />
				</div>
				<br clear="all" />
			</div>
		</div>
	</asp:Panel>

	<uc:GenericWysiwygTranslator ID="BrandDescriptionTranslator" runat="server" />
	<uc:GenericWysiwygTranslator ID="BrandPackageInfoTranslator" runat="server" />
</asp:Content>