﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Resources;
using Convert = System.Convert;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Reviews
{
	public partial class ReviewEdit : LekmerPageController, IEditor
	{
		private int _id;
		private Collection<IChannel> _channels;
		private Collection<IRatingReviewStatus> _statuses;
		private IRatingReviewFeedbackSecureService _ratingReviewFeedbackService;
		private List<string> _validationMessage = new List<string>();

		protected int Id
		{
			get { return _id > 0 ? _id : (_id = GetId()); }
		}
		protected Collection<IChannel> Channels
		{
			get { return _channels ?? (_channels = IoC.Resolve<IChannelSecureService>().GetAll()); }
		}
		protected Collection<IRatingReviewStatus> Statuses
		{
			get { return _statuses ?? (_statuses = IoC.Resolve<IRatingReviewStatusSecureService>().GetAll()); }
		}
		protected IRatingReviewFeedbackSecureService RatingReviewFeedbackService
		{
			get { return _ratingReviewFeedbackService ?? (_ratingReviewFeedbackService = IoC.Resolve<IRatingReviewFeedbackSecureService>()); }
		}


		protected override void SetEventHandlers()
		{
			SaveButton.Click += OnSave;
			CancelButton.Click += OnCancel;
			DeleteButton.Click += OnDelete;
		}
		protected override void PopulateForm()
		{
			PopulateStatuses();

			var feedback = RatingReviewFeedbackService.GetRecordById(Id);

			ProductLiteral.Text = feedback.ProductId.ToString(CultureInfo.InvariantCulture);
			ProductLink.Text = feedback.ProductTitle;
			ProductLink.NavigateUrl = PathHelper.Assortment.Product.GetEditUrl(feedback.ProductId);
			CreatedLiteral.Text = string.Format("{0} by {1}", feedback.CreatedDate, feedback.Review.AuthorName);
			AuthorLink.Text = string.IsNullOrEmpty(feedback.UserName) ? string.Empty : string.Format("({0} - {1})", Resources.Review.Literal_User_Email, feedback.UserName);
			AuthorLink.NavigateUrl = string.Format("mailto:{0}", feedback.UserName);
			EmailLink.Text = feedback.Review.Email == null || feedback.Review.Email == feedback.UserName ? string.Empty : string.Format("({0} - {1})", Resources.Review.Literal_Review_Email, feedback.Review.Email);
			EmailLink.NavigateUrl = string.Format("mailto:{0}", feedback.Review.Email);
			StatusList.SelectedValue = feedback.RatingReviewStatusId.ToString(CultureInfo.InvariantCulture);
			InappropriateCheckBox.Checked = feedback.Inappropriate;
			TitleTextBox.Text = feedback.Review.Title;
			MessageTextBox.Text = feedback.Review.Message;

			if (feedback.OrderId.HasValue)
			{
				OrderLiteral.Text = feedback.OrderId.Value.ToString(CultureInfo.InvariantCulture);
				OrderInfoDiv.Visible = true;
			}

			var channel = Channels.FirstOrDefault(c => c.Id == feedback.ChannelId);
			if (channel != null)
			{
				ChannelLiteral.Text = channel.CommonName;
			}

			PopulateRatings(feedback);
		}


		// Page events.

		public virtual void OnSave(object sender, EventArgs e)
		{
			if (!ValidateData())
			{
				foreach (string warning in _validationMessage)
				{
					Messager.Add(warning);
				}
				Messager.MessageType = InfoType.Warning;
				return;
			}

			var isSaved = RatingReviewFeedbackService.Save(SignInHelper.SignedInSystemUser, Id, TitleTextBox.Text.Trim(), MessageTextBox.Text.Trim(), Convert.ToInt32(StatusList.SelectedValue), InappropriateCheckBox.Checked);
			if (isSaved)
			{
				Messager.Add(GeneralMessage.SaveSuccessful, InfoType.Success);
			}
			else
			{
				Messager.Add(GeneralMessage.SaveFailed, InfoType.Failure);
			}
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			RedirectBack();
		}
		public virtual void OnDelete(object sender, EventArgs e)
		{
			RatingReviewFeedbackService.Delete(SignInHelper.SignedInSystemUser, new Collection<int>{Id});
			RedirectBack();
		}


		// Private methods.

		protected virtual void PopulateStatuses()
		{
			StatusList.DataValueField = "Id";
			StatusList.DataTextField = "Title";
			StatusList.DataSource = Statuses;
			StatusList.DataBind();
		}
		protected virtual void PopulateRatings(IRatingReviewFeedbackRecord feedback)
		{
			RatingRepeater.DataSource = feedback.RatingItemProductVotes;
			RatingRepeater.DataBind();
		}
		protected virtual void RedirectBack()
		{
			string referrer = Page.Request.QueryString["Referrer"];
			if (referrer == null)
			{
				Response.Redirect(Request.Url.PathAndQuery);
			}

			Response.Redirect(LekmerPathHelper.GetReferrerUrl(referrer));
		}


		// Validation.

		protected virtual bool ValidateData()
		{
			bool isValid = true;

			if (!IsValidTitle())
			{
				_validationMessage.Add(Resources.Review.Message_EmptyTitle);
				isValid = false;
			}

			if (!IsValidMessage())
			{
				_validationMessage.Add(Resources.Review.Message_EmptyReview);
				isValid = false;
			}

			return isValid;
		}
		protected virtual bool IsValidTitle()
		{
			string title = TitleTextBox.Text.Trim();
			if (string.IsNullOrEmpty(title))
			{
				return false;
			}
			return title.Length <= 50;
		}
		protected virtual bool IsValidMessage()
		{
			string review = MessageTextBox.Text.Trim();
			if (string.IsNullOrEmpty(review))
			{
				return false;
			}
			return review.Length <= 3000;
		}
	}
}