using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Products
{
	public class ProductDataSource
	{
		private int _rowCount;

		public int SelectCount()
		{
			return _rowCount;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public Collection<IProduct> SearchMethodWithSortingByErpId(int maximumRows, int startRowIndex)
		{
			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "title"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "statusId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "priceTo"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "priceFrom"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "erpId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "eanCode"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "categoryId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "brandId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "productTypeId")]
		public int SelectCount(int maximumRows, int startRowIndex, string categoryId, string erpId, string title, string statusId, string priceFrom, string priceTo, string eanCode, string brandId, string productTypeId)
		{
			return _rowCount;
		}
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex, string categoryId, string erpId, string title, string statusId, string priceFrom, string priceTo, string eanCode, string brandId, string productTypeId)
		{
			ILekmerProductSearchCriteria searchCriteria = (ILekmerProductSearchCriteria)IoC.Resolve<IProductSearchCriteria>();
			searchCriteria.CategoryId = categoryId;
			searchCriteria.Title = title;
			searchCriteria.PriceFrom = priceFrom;
			searchCriteria.PriceTo = priceTo;
			searchCriteria.ErpId = erpId;
			searchCriteria.StatusId = statusId;
			searchCriteria.EanCode = eanCode;
			searchCriteria.BrandId = brandId;
			searchCriteria.ProductTypeId = productTypeId;

			IProductSecureService productSecureService = IoC.Resolve<IProductSecureService>();
			var products = productSecureService.Search(
				ChannelHelper.CurrentChannel.Id,
				searchCriteria, startRowIndex / maximumRows + 1,
				maximumRows);
			_rowCount = products.TotalCount;
			return products;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "searchCriteriaInclusive"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "searchCriteriaExclusive"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex, string searchCriteriaInclusive, string searchCriteriaExclusive)
		{
			return _rowCount;
		}
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex, string searchCriteriaInclusive, string searchCriteriaExclusive)
		{
			IProductSecureService productSecureService = IoC.Resolve<IProductSecureService>();
			var products = productSecureService.AdvancedSearch(
				ChannelHelper.CurrentChannel.Id,
				searchCriteriaInclusive,
				searchCriteriaExclusive, startRowIndex / maximumRows + 1,
				maximumRows);
			_rowCount = products.TotalCount;
			return products;
		}

		public int SelectCount(int maximumRows, int startRowIndex, string productIdsString)
		{
			return _rowCount;
		}
		public Collection<IProduct> SearchMethod(int maximumRows, int startRowIndex, string productIdsString)
		{
			int productIdsCount;
			var products = Search(maximumRows, startRowIndex, productIdsString, out productIdsCount);
			_rowCount = productIdsCount;
			return products;
		}
		public Collection<IProduct> SearchMethodWithSortingByErpId(int maximumRows, int startRowIndex, string productIdsString)
		{
			int productIdsCount;
			var products = Search(maximumRows, startRowIndex, productIdsString, out productIdsCount);

			_rowCount = productIdsCount;

			// Sort by ErpId.
			var productsList = products.ToList();
			productsList.Sort(delegate(IProduct element1, IProduct element2)
			{
				return String.Compare(element1.ErpId, element2.ErpId, StringComparison.Ordinal);
			});

			return new Collection<IProduct>(productsList);
		}
		private Collection<IProduct> Search(int maximumRows, int startRowIndex, string productIdsString, out int productIdsCount)
		{
			productIdsCount = 0;
			var products = new Collection<IProduct>();

			if (string.IsNullOrEmpty(productIdsString))
			{
				return products;
			}

			if (productIdsString[productIdsString.Length - 1] == Foundation.Convert.DefaultListSeparator)
			{
				productIdsString = productIdsString.Substring(0, productIdsString.Length - 1);
			}

			var productSecureService = IoC.Resolve<IProductSecureService>();
			var lekmerProductSecureService = (ILekmerProductSecureService) productSecureService;

			List<string> idsSrtingList = productIdsString.Split(',').ToList();
			var productIds = new List<int>();
			foreach (var productId in idsSrtingList.ConvertAll(Int32.Parse))
			{
				var isProductDeleted = lekmerProductSecureService.IsProductDeleted(productId);
				if (isProductDeleted) continue;

				productIds.Add(productId);
			}

			if (productIds.Count > 0)
			{
				int fromIndex = startRowIndex;
				int toIndex = startRowIndex + maximumRows;

				if (productIds.Count >= fromIndex)
				{
					int count;
					if (productIds.Count >= toIndex)
					{
						count = maximumRows;
					}
					else if (productIds.Count == fromIndex)
					{
						fromIndex = fromIndex - maximumRows;
						count = maximumRows;
					}
					else
					{
						count = productIds.Count - fromIndex;
					}

					for (int i = fromIndex; i < fromIndex + count; i++)
					{
						var product = productSecureService.GetById(ChannelHelper.CurrentChannel.Id, productIds[i]);
						if (product == null)
						{
							throw new InvalidOperationException(
							string.Format(
							CultureInfo.CurrentCulture,
							"Product with ID {0} could not be found for channel with ID {1}.",
							productIds[i], ChannelHelper.CurrentChannel.Id));
						}
						products.Add(product);
					}
				}
			}

			productIdsCount = productIds.Count;
			return products;
		}

		public int SelectCount1(int maximumRows, int startRowIndex, string productIdsString, string productDiscountPricesString)
		{
			return _rowCount;
		}
		public Dictionary<IProduct, decimal> SearchMethod1(int maximumRows, int startRowIndex, string productIdsString, string productDiscountPricesString)
		{
			var products = new Dictionary<IProduct, decimal>();

			if (string.IsNullOrEmpty(productIdsString) || string.IsNullOrEmpty(productDiscountPricesString))
			{
				return products;
			}

			if (productIdsString[productIdsString.Length - 1] == Foundation.Convert.DefaultListSeparator)
			{
				productIdsString = productIdsString.Substring(0, productIdsString.Length - 1);
			}

			if (productDiscountPricesString[productDiscountPricesString.Length - 1] == ';')
			{
				productDiscountPricesString = productDiscountPricesString.Substring(0, productDiscountPricesString.Length - 1);
			}

			var productSecureService = IoC.Resolve<IProductSecureService>();
			var lekmerProductSecureService = (ILekmerProductSecureService)productSecureService;

			List<string> idsSrtingList = productIdsString.Split(',').ToList();
			var productIds = new List<int>();
			foreach (var productId in idsSrtingList.ConvertAll(Int32.Parse))
			{
				var isProductDeleted = lekmerProductSecureService.IsProductDeleted(productId);
				if (isProductDeleted) continue;

				productIds.Add(productId);
			}

			List<string> pricesSrtingList = productDiscountPricesString.Split(';').ToList();
			List<decimal> productDiscountPrices = pricesSrtingList.ConvertAll(Decimal.Parse);

			if (productIds.Count > 0 && productIds.Count == productDiscountPrices.Count)
			{
				int fromIndex = startRowIndex;
				int toIndex = startRowIndex + maximumRows;

				if (productIds.Count >= fromIndex)
				{
					int count;
					if (productIds.Count >= toIndex)
					{
						count = maximumRows;
					}
					else if (productIds.Count == fromIndex)
					{
						fromIndex = fromIndex - maximumRows;
						count = maximumRows;
					}
					else
					{
						count = productIds.Count - fromIndex;
					}

					for (int i = fromIndex; i < fromIndex + count; i++)
					{
						var product = productSecureService.GetById(ChannelHelper.CurrentChannel.Id, productIds[i]);
						if (product == null)
						{
							throw new InvalidOperationException(
							string.Format(
							CultureInfo.CurrentCulture,
							"Product with ID {0} could not be found for channel with ID {1}.",
							productIds[i], ChannelHelper.CurrentChannel.Id));
						}
						products.Add(product, productDiscountPrices[i]);
					}
				}
			}

			_rowCount = productIds.Count;

			var keyValuePairs = products.OrderBy(i => i.Key.ErpId);
			return keyValuePairs.ToDictionary(pair => pair.Key, pair => pair.Value);
		}

		public static string ConvertToXml(IEnumerable<IProductSearchCriteria> productSearchCriteria)
		{
			StringWriter stringWriter = new StringWriter(CultureInfo.CurrentCulture);
			XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
			xmlWriter.WriteStartElement("criterias");
			foreach (IProductSearchCriteria searchCriteria in productSearchCriteria)
			{
				xmlWriter.WriteStartElement("criteria");


				if (!string.IsNullOrEmpty(((ILekmerProductSearchCriteria)searchCriteria).ProductTypeId))
					xmlWriter.WriteAttributeString("productTypeId", ((ILekmerProductSearchCriteria)searchCriteria).ProductTypeId);

				if (!string.IsNullOrEmpty(((ILekmerProductSearchCriteria)searchCriteria).BrandId))
					xmlWriter.WriteAttributeString("brandId", ((ILekmerProductSearchCriteria)searchCriteria).BrandId);

				if (!string.IsNullOrEmpty(searchCriteria.CategoryId))
					xmlWriter.WriteAttributeString("categoryId", searchCriteria.CategoryId);

				if (!string.IsNullOrEmpty(searchCriteria.Title))
					xmlWriter.WriteAttributeString("title", searchCriteria.Title);

				if (!string.IsNullOrEmpty(searchCriteria.PriceFrom))
					xmlWriter.WriteAttributeString("priceFrom", searchCriteria.PriceFrom);

				if (!string.IsNullOrEmpty(searchCriteria.PriceTo))
					xmlWriter.WriteAttributeString("priceTo", searchCriteria.PriceTo);

				if (!string.IsNullOrEmpty(searchCriteria.ErpId))
					xmlWriter.WriteAttributeString("erpId", searchCriteria.ErpId);

				if (!string.IsNullOrEmpty(searchCriteria.StatusId))
					xmlWriter.WriteAttributeString("statusId", searchCriteria.StatusId);

				if (!string.IsNullOrEmpty(searchCriteria.EanCode))
					xmlWriter.WriteAttributeString("eanCode", searchCriteria.EanCode);

				xmlWriter.WriteEndElement();
			}
			xmlWriter.WriteEndElement();
			return stringWriter.ToString();
		}
	}
}