﻿<%@ Page Language="C#" ValidateRequest="false" EnableEventValidation="false" MasterPageFile="~/Master/Main.Master"
	CodeBehind="ProductEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Products.ProductEdit" %>
<%@ Import Namespace="System.Globalization"%>
<%@ Import Namespace="Litium.Scensum.BackOffice.Controller"%>
<%@ Register TagPrefix="CustomControls" Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="tinymce" Namespace="Moxiecode.TinyMCE.Web" Assembly="Moxiecode.TinyMCE" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree" TagPrefix="TemplatedTreeView" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/Lekmer/LekmerContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>
<%@ Register TagPrefix="uc" TagName="ProductMedia" Src="~/Modules/Assortment/Products/Controls/ProductMedia.ascx" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="uc" TagName="GenericTranslator" Src="~/UserControls/Translation/GenericTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericWysiwygTranslator" Src="~/UserControls/Translation/GenericWysiwygTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="GenericMultilineTranslator" Src="~/UserControls/Translation/GenericMultilineTranslator.ascx" %>
<%@ Register TagPrefix="uc" TagName="TagList" Src="~/UserControls/Assortment/TagList.ascx" %>
<%@ Register TagPrefix="uc" TagName="UrlHistory" Src="~/UserControls/UrlHistory/ProductUrlHistoryControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="RatingGroupSelector" Src="~/UserControls/Assortment/RatingGroupSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="RegistryRestriction" Src="~/UserControls/Assortment/RegistryRestriction.ascx" %>
<%@ Register TagPrefix="uc" TagName="DeliveryTimeConfigurator" Src="~/UserControls/Assortment/DeliveryTimeConfigurator.ascx" %>

<asp:Content ID="ProductEdit" ContentPlaceHolderID="body" runat="server">
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery-ui-personalized-1.5.3.min.js") %>" type="text/javascript"></script>
	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<script type="text/javascript"> 
		$(document).ready(function() { $('div#assortment-variation').css({ visibility: 'visible' }); });
		$(document).ready(function() { $('#tabs').tabs(); });

		function ShowHide(cbUseWebShopTitleId, tbWebShopTitleId, tbWebShopTitleTranslationId, tbTitleId) {
			var cbUseWebShopTitle = document.getElementById(cbUseWebShopTitleId);
			var tbWebShopTitle = document.getElementById(tbWebShopTitleId);
			var tbWebShopTitleTranslation = document.getElementById(tbWebShopTitleTranslationId);
			var tbTitle = document.getElementById(tbTitleId);
			var rfvTitle = document.getElementById('<%= WebShopTitleValidator.ClientID %>');
			
			ValidatorEnable(rfvTitle, cbUseWebShopTitle.checked);

			if (cbUseWebShopTitle.checked) {
				tbTitle.style.display = "none";
				tbWebShopTitle.style.display = "block";
				tbWebShopTitleTranslation.style.display = "block";
			}
			else {
				tbTitle.style.display = "block";
				tbWebShopTitle.style.display = "none";
				tbWebShopTitleTranslation.style.display = "none";
			}
		}
		
		function ClearPopup(errorsDivId) {}

		function submitForm() {
			tinyMCE.triggerSave();
		}

		function ClearSuccessSeveMessage() {
			ClearPopupMessage('<%=SystemMessageContainer.ClientID %>');
		}

		function SizeDeviationApplyToAll() {
			var deviation = $(this).closest('td').find('.size-deviation-box').val();
			alert(deviation);
		}

		function ConfirmRatingGroupDelete() {
			return DeleteConfirmation("<%= Resources.RatingReview.Literal_RatingGroup %>");
		}

		function MakeNew() {
			Date.prototype.yyyymmdd = function () {
				var yyyy = this.getFullYear().toString();
				var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
				var dd = this.getDate().toString();
				return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]); // padding
			};

			var from = new Date();
			var toDate = new Date(new Date(from).setMonth(from.getMonth() + 1));

			document.getElementById('<%=NewFromBox.ClientID %>').value = from.yyyymmdd();
			document.getElementById('<%=NewToBox.ClientID %>').value = toDate.yyyymmdd();
		}
	</script>
	
	<script src="<%=ResolveUrl("~/Media/Scripts/jquery.lightbox-0.5.js") %>" type="text/javascript"></script>
	
	<script type="text/javascript">
		function DefaultImageLitbox() {
			media_url = '<%=ResolveUrl("~/Media") %>';
			$('#image-show a').lightBox();
			$('#image-left-show a').lightBox();
		}
		$(function() {
			DefaultImageLitbox();
		});
	</script>

	<Scensum:ToolBoxPanel ID="ProductHeaderToolBoxPanel" Text="Products" runat="server" />

	<div style= "width:978px; ">
		<uc:MessageContainer runat="server" ID="SystemMessageContainer" MessageType="Warning" HideMessagesControlId="SaveButton" />
		<uc:ScensumValidationSummary runat="server" ID="ValidationSummary" ForeColor="Black" CssClass="advance-validation-summary" DisplayMode="List" ValidationGroup="vgPage" />
	</div>

	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<span class="product-title" >
			<uc:LiteralEncoded runat="server" ID="ProductTitleLiteral" />
		</span>

		<uc:GenericWysiwygTranslator ID="ProductShortDescriptionTranslator" runat="server" />
		<uc:GenericWysiwygTranslator ID="ProductDescriptionTranslator" runat="server" />

		<uc:GenericMultilineTranslator ID="ProductSeoDescriptionTranslator" runat="server" />
		<uc:GenericMultilineTranslator ID="ProductSeoKeywordsTranslator" runat="server" />

		<br />
		<div id="tabs">
			<ul>
				<li><a href="#fragment-1"><span><%= Resources.Product.Tab_ProductEdit_Info %></span></a></li>
				<li><a href="#fragment-2"><span title="<%= Resources.ProductMessage.SeoTitle %>"><%= Resources.Product.Tab_ProductEdit_Seo %></span></a></li>
				<li><a href="#fragment-3"><span><%= Resources.Product.Tab_ProductEdit_Media %></span></a></li>
				<li><a href="#fragment-5"><span><%= Resources.Product.Tab_ProductEdit_Pricing %></span></a></li>
				<li><a href="#fragment-6"><span><%= Resources.Product.Tab_ProductEdit_Relation %></span></a></li>
				<li><a href="#fragment-7"><span><%= Resources.Product.Tab_ProductEdit_SiteStructure %></span></a></li>
				<li><a href="#fragment-8"><span><%= Resources.Product.Tab_ProductEdit_Store %></span></a></li>
				<li><a href="#fragment-9"><span><%= Resources.Lekmer.Tab_ProductEdit_Settings %></span></a></li>
				<li><a href="#fragment-10"><span><%= Resources.Lekmer.Tab_ProductEdit_Tags %></span></a></li>
				<li><a href="#fragment-11"><span><%= Resources.Lekmer.Tab_ProductEdit_Sizes %></span></a></li>
			</ul>
			<br clear="all" />
			<div class="tabs-main-container" id="tabs-main-container">
				<div id="fragment-1">
					<div class="tabs-inner-tab">
						<span class="product-field-caption"><%= Resources.General.Literal_Title %>&nbsp;*</span>&nbsp;
						<uc:GenericTranslator ID="ProductWebShopTitleTranslator" runat="server" />
						<asp:RequiredFieldValidator runat="server" 
							ID="WebShopTitleValidator" 
							ControlToValidate="WebShopTitleTextBox"
							ErrorMessage="<%$ Resources:ProductMessage,WebShopTitleEmpty %>"
							Display ="None" 
							ValidationGroup="vgPage" />
						<br />
						<asp:CheckBox ID="UseWebShopTitleCheckBox" runat="server" />
						<span><%= Resources.Product.Literal_UseWebTitle %></span>
						<div id="variant-title" class="left clear">
							<asp:TextBox ID="TitleTextBox" runat="server" ReadOnly="true" CssClass="left" />
							<asp:TextBox ID="WebShopTitleTextBox" runat="server" CssClass="left" />
						</div>
						<br clear="all" />
						<br clear="all" />
						<span class="product-field-caption"><%= Resources.Product.Literal_ShortArticleDescription %></span>&nbsp;
						<asp:ImageButton runat="server" ID="ShortDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
						<uc:LekmerTinyMceEditor ID="ShortArticleDescriptionEditor" runat="server" SkinID="tinyMCE"/>
						<br clear="all" />
						<span class="product-field-caption"><%= Resources.Product.Literal_LongArticleDescription %></span>&nbsp;
						<asp:ImageButton runat="server" ID="DescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" />
						<uc:LekmerTinyMceEditor ID="LongArticleDescriptionEditor" runat="server" SkinID="tinyMCE"/>
						<br clear="all" />
					</div>
				</div>
				<div id="fragment-2">
					<div class="tabs-inner-tab">
						<span class="bold"><%= Resources.Product.Literal_SpecifyPageTitle %></span>&nbsp;
						<uc:GenericTranslator ID="ProductSeoTitleTranslator" runat="server" /><br />
						<span><%= Resources.Product.Literal_PageTitleImportanSEO %></span><br />
						<asp:TextBox ID="SeoTitleTextBox" MaxLength="500" onkeydown="return CheckTextBoxLength(event, this, 500);" TextMode="MultiLine" Rows="5" runat="server" CssClass="variation-seo" />
						<br clear="all" />
						<span class="bold"><%= Resources.General.Literal_Description %></span>&nbsp;
						<asp:ImageButton runat="server" ID="SeoDescriptionTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" /><br />
						<span><%= Resources.Product.Literal_MetaDescription %></span><br />
						<asp:TextBox ID="SeoDescrTextBox" MaxLength="500" onkeydown="return CheckTextBoxLength(event, this, 500);" TextMode="MultiLine" Rows="5" runat="server" CssClass="variation-seo" />
						<br clear="all" />
						<span class="bold"><%= Resources.General.Literal_Keywords %></span>&nbsp;
						<asp:ImageButton runat="server" ID="SeoKeywordsTranslateButton" ImageUrl="~/Media/Images/Interface/translate.png" AlternateText="Translate" /><br />
						<span><%= Resources.Product.Literal_MetaKeywords %></span><br />
						<asp:TextBox ID="SeoKeywordsTextBox" MaxLength="500" onkeydown="return CheckTextBoxLength(event, this, 500);" TextMode="MultiLine" Rows="5" runat="server" CssClass="variation-seo" />
					</div>
				</div>
				<div id="fragment-3">
					<uc:ProductMedia ID="ProductMediaControl" runat="server" />
				</div>
				<div id="fragment-5">
					<div id="variant-price" class="left">
						<span class="bold"><%= Resources.Product.Literal_Price %></span><br />
						<asp:TextBox ID="PriceTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
						<br/>
						<span class="bold"><%= Resources.Lekmer.Literal_PurchasePrice %></span><br />
						<asp:TextBox ID="PurchasePriceTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
						<br/>
						<span class="bold"><%= Resources.Lekmer.Literal_AveragePrice %></span><br />
						<asp:TextBox ID="AveragePriceTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
					</div>
					<div id="variant-vat" class="left">
						<span class="bold"><%= Resources.Product.Literal_VatType %></span><br />
						<asp:TextBox ID="PriceVatTypeTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
					</div>
					<div class="clear">
					</div>
					<span class="bold"><%= Resources.Product.Literal_PriceLists %></span><br />
					<asp:GridView ID="PriceListsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%" DataSourceID="PriceListObjectDataSource">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="30%">
								<ItemTemplate>
									<%# GetPriceList((int)Eval("PriceListID")).Title %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_StartDate %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<%# (null == GetPriceList((int)Eval("PriceListID")).StartDateTime) ? string.Empty : ((DateTime)GetPriceList((int)Eval("PriceListID")).StartDateTime).ToShortDateString() %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText= "<%$ Resources:General,Literal_EndDate %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<%# (null == GetPriceList((int)Eval("PriceListID")).EndDateTime) ? string.Empty : ((DateTime)GetPriceList((int)Eval("PriceListID")).EndDateTime).ToShortDateString() %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Status %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<%# GetPriceListStatus(GetPriceList((int)Eval("PriceListID")).PriceListStatusId)%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" ItemStyle-Width="15%">
								<ItemTemplate>
									<%# GetPrice((int)Eval("PriceListID"), Convert.ToDecimal(Eval("PriceIncludingVat"))) %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_VatType %>" ItemStyle-Width="10%">
								<ItemTemplate>
									<%# GetPriceVat(Convert.ToDecimal(Eval("VatPercentage"))) %>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
					<asp:ObjectDataSource ID="PriceListObjectDataSource" runat="server" EnablePaging="false" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.Products.PriceListDataSource">
					</asp:ObjectDataSource>
					<br/>
					<span class="bold"><%= Resources.Lekmer.Product_RecommendedPrice %></span><br />
					<asp:GridView ID="RecommendedPriceGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
						<Columns>
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Product_ChannelTitle %>" DataField="ChannelTitle" ItemStyle-Width="33%"/>
							<asp:BoundField HeaderText="<%$ Resources:Lekmer, Product_Currency %>" DataField="CurrencyTitle" ItemStyle-Width="33%"/>
							<asp:TemplateField HeaderText="<%$ Resources:Product, Literal_Price %>" ItemStyle-Width="33%">
								<ItemTemplate>
									<asp:HiddenField ID="ChannelIdHidden" runat="server" Value='<%# Eval("ChannelId") %>'/>
									<asp:TextBox ID="PriceBox" runat="server" Text='<%# Eval("Price") %>' Width="150"></asp:TextBox>
									<asp:RangeValidator ID="RecommendedPriceValidator" runat="server" ControlToValidate="PriceBox" Type="Double" MinimumValue="0" ErrorMessage="<%$ Resources:LekmerMessage,Product_RecommendedPriceIncorrect %>" Text="*" ValidationGroup="vgPage"></asp:RangeValidator>
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
				</div>
				<div id="fragment-6">
					<span class="bold"><%= Resources.Product.Literal_RelationList %></span><br />
					<asp:UpdatePanel ID="RelationUpdatePanel" runat="server" UpdateMode="Conditional">
						<triggers>
							<asp:PostBackTrigger ControlID="AddRelationsButton" />
						</triggers>
						<ContentTemplate>
					<asp:GridView
						ID="RelationsGrid"
						SkinID="grid"
						runat="server"
						AutoGenerateColumns="false"
						Width="100%"
						GridLines="None"
						PagerSettings-Mode="NumericFirstLast">
						<Columns>
							<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>" ItemStyle-Width="50%">
								<ItemTemplate>
									<uc:HyperLinkEncoded runat="server" ID="EditLink" NavigateUrl='<%# PathHelper.Assortment.Relation.GetEditUrlForDefault(int.Parse(Eval("Id").ToString())) %>' Text='<%# Eval("Title")%>' />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField HeaderText="<%$ Resources:General, Literal_Type %>" DataField="RelationListTypeName" ItemStyle-Width="46%" />
							<asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="4%">
								<ItemTemplate>
									<asp:ImageButton
										runat="server" 
										ID="DeleteButton"
										CommandName="DeleteRL" 
										CommandArgument='<%# Eval("Id") %>' 
										ImageUrl="~/Media/Images/Common/delete.gif" 
										AlternateText="<%$ Resources:General, Button_Delete %>" 
										OnClientClick='<%# "return DeleteConfirmation(\""+ Resources.Product.Literal_RelationList + "\");"%>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
					</asp:GridView>
					</ContentTemplate>
					</asp:UpdatePanel>
						<div class="right default-image-bottom">
							<br/>
							<uc:ImageLinkButton runat="server" ID="RelationBrowseButton" Text="Add" UseSubmitBehaviour="false" SkinID="DefaultButton"/>
						</div>
					<ajaxToolkit:ModalPopupExtender ID="RelationsPopup" runat="server" TargetControlID="RelationBrowseButton" CancelControlID="_inpCloseRelations"
						PopupControlID="RelationsDiv" BackgroundCssClass="popup-background">
					</ajaxToolkit:ModalPopupExtender>
				</div>
				<div id="fragment-7">
					<div id="product-parent-content-node" Style="width: 100%;">
						<span class="bold"><%= Resources.General.Literal_SiteStructureRegistryList %></span>
						<asp:GridView ID="SiteStructureRegistryGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"	Width="100%">
							<Columns>
								<asp:TemplateField HeaderText="<%$ Resources:General,Literal_SiteStructureRegistry %>" ItemStyle-Width="30%">
									<ItemTemplate>
										<asp:HiddenField ID="RegistryIdHiddenField" runat="server" Value='<%#Eval("SiteStructureRegistryId")%>' />
										<asp:Label ID="TitleLabel" runat="server"></asp:Label>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General,Literal_ParentContentNode %>" ItemStyle-Width="35%">
									<ItemTemplate>
										<uc:ContentPageSelector ID="ParentContentNodeSelector" runat="server" AllowClearSelection="true" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:General,Literal_TemplateContentNode %>" ItemStyle-Width="35%">
									<ItemTemplate>
										<uc:ContentPageSelector ID="TemplateContentNodeSelector" runat="server" AllowClearSelection="true" OnlyProductPagesSelectable="true" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</div>
					<br clear="all" />
					<br />
					<uc:RegistryRestriction ID="ProductRegistryRestriction" runat="server"/>
				</div>
				<div id="fragment-8">
					<div style="width: 100%;">
						<span class="bold"><%= Resources.Product.Literal_Stores %></span>
						<asp:GridView ID="StoresGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
							<Columns>
								<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="44%">
									<ItemTemplate>
										<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
										<uc:HyperLinkEncoded ID="StoreEditLink" runat="server" Text='<%# Eval("Title")%>' NavigateUrl='<%# PathHelper.Assortment.Store.GetEditUrl(int.Parse(Eval("Id").ToString())) %>' />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:General,Literal_ErpId %>" DataField="ErpId" ItemStyle-Width="20%" />
								<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Status %>" ItemStyle-Width="10%">
									<ItemTemplate>
										<%# GetStoreStatus((int)Eval("StatusId"))%>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product,Literal_Quantity %>" ItemStyle-Width="13%">
									<ItemTemplate>
										<asp:RangeValidator runat="server" ID="StoreProductQuantityValidator" 
											ControlToValidate="StoreProductQuantityBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' 
											MinimumValue="0" Text="*" ValidationGroup="vgPage" />
										<asp:TextBox ID="StoreProductQuantityBox" runat="server" Width="60px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Product,Literal_Threshold %>" ItemStyle-Width="13%">
									<ItemTemplate>
										<asp:RangeValidator runat="server" ID="StoreProductThresholdValidator" 
											ControlToValidate="StoreProductThresholdBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' 
											MinimumValue="0" Text="*" ValidationGroup="vgPage" />
										<asp:TextBox ID="StoreProductThresholdBox" runat="server" Width="60px"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
					</div>
				</div>
				<div id="fragment-9">
					<div class="tabs-inner-tab">
						<div class="data-column">
							<div class="data-row">
								<span><%= Resources.Lekmer.Literal_Brand %></span>
								<br/>
								<asp:DropDownList runat="server" ID="BrandList" DataValueField="Id" DataTextField="Title" Width="284px" />
							</div>
							<br style="clear:both;" />
							<div class="data-row">
								<div class="data-column">
									<span><%= Resources.Lekmer.Literal_AgeFrom %></span>
									<br/>
									<asp:DropDownList runat="server" ID="AgeFromList" DataValueField="Months" DataTextField="Title" Width="120px"/>
								</div>
								<div class="data-column">
								-
								</div>
								<div class="data-column">
									<span><%= Resources.Lekmer.Literal_AgeTo %></span>
									<br/>
									<asp:DropDownList runat="server" ID="AgeToList" DataValueField="Months" DataTextField="Title" Width="120px"/>
									<asp:CompareValidator runat="server" ID="AgeCompareValidator" ControlToCompare="AgeFromList" 
										ControlToValidate="AgeToList" Type="Integer" Operator="GreaterThan" 
										ErrorMessage="<%$ Resources:LekmerMessage, ProductEdit_AgeIntervalIncorrect %>" 
										Display="None" ValidationGroup="vgPage">
									</asp:CompareValidator>
								</div>
							</div>
							<br style="clear:both;" />
							<div class="data-row">
								<span><%= Resources.Lekmer.Literal_Measurement%></span>&nbsp;
								<uc:GenericTranslator ID="MeasurementTranslator" runat="server" />&nbsp;
								<br />
								<asp:TextBox ID="MeasurementBox" Width="280px" runat="server"></asp:TextBox>
							</div>
							<br style="clear:both;" />
							<div class="data-row">
								<div class="data-column">
									<span><%= Resources.Lekmer.Literal_BatteryType %></span>
									<br/>
									<asp:DropDownList runat="server" ID="BatteryTypeList" DataValueField="Id" DataTextField="Title" Width="120px"/>
								</div>
								<div class="data-column">
									<span><%= Resources.Lekmer.Literal_NofBatteries %></span>
									<br/>
									<asp:TextBox ID="BatteryNumberBox" runat="server" Width="72px"></asp:TextBox>
									<asp:RangeValidator ID="BatteryNumberRangeValidator" runat="server" ControlToValidate="BatteryNumberBox"
										Display="None" ErrorMessage="<%$ Resources:LekmerMessage, ProductEdit_BatteryNumberIncorrect %>" 
										MinimumValue="1" MaximumValue="100" Type="Integer" ValidationGroup="vgPage">
									</asp:RangeValidator>
								</div>
								<div class="data-column">
									<span><%= Resources.Lekmer.Literal_IsBatteryIncluded %></span>
									<br/>
									<asp:CheckBox ID="BatteryIncludedCheckBox" runat="server" />
								</div>
							</div>
							<br style="clear:both;" />
							<div class="data-row" style="width: 305px;">
								<span><%= Resources.Lekmer.Literal_Icons %> <%= Resources.Lekmer.Literal_Icons_Inherit %></span>
								<br/>
								<asp:Repeater ID="IconRepeater" runat="server">
									<ItemTemplate>
										<div class="product-icon-wrapper">
											<asp:HiddenField ID="IconIdHidden" runat="server" Value='<%# Eval("Id" )%>' />
											<asp:CheckBox ID="IconCheckBox" runat="server" />
											<asp:Image ID="IconImage" runat="server" ImageAlign="AbsMiddle" CssClass="product-icon-img"
												ImageUrl='<%# ResolveUrl(PathHelper.Media.GetMediaOriginalLoaderUrl(Convert.ToInt32(Eval("Image.Id")), Eval("Image.FormatExtension").ToString())) %>' />
										</div>
									</ItemTemplate>
								</asp:Repeater>
							</div>
						</div>
						<div class="data-column">
							<div class="data-row">
								<div class="data-column">
									<span><%=Resources.Lekmer.Literal_NewFrom%></span>
									<br />
									<asp:TextBox ID="NewFromBox" runat="server" Width="106px"></asp:TextBox>
									<asp:ImageButton ID="NewFromButtoon" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
										ImageAlign="AbsMiddle"/>
									<ajaxToolkit:CalendarExtender ID="NewFromCalendar" runat="server" TargetControlID="NewFromBox" 
										PopupButtonID="NewFromButtoon" />
								</div>
								<div class="data-column">
								-
								</div>
								<div class="data-column">
									<span><%=Resources.Lekmer.Literal_NewTo%></span>
									<br />
									<asp:TextBox ID="NewToBox" runat="server" Width="106px"></asp:TextBox>
									<asp:ImageButton ID="NewToButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
										ImageAlign="AbsMiddle"/>
									<ajaxToolkit:CalendarExtender ID="NewToCalendar" runat="server" TargetControlID="NewToBox" 
										PopupButtonID="NewToButton" />
								</div>

								<div class="makenew-container">
									<br />
									<input type="button" onclick=" MakeNew()" value="<%=Resources.Lekmer.Literal_MakeNew%>" />


								</div>
							</div>

							<br style="clear: both;" />

							<div class="data-row">
								<div class="data-column">
									<span><%=Resources.Lekmer.Literal_IsBookable%></span>
									<br />
									<asp:CheckBox ID="IsBookableCheckBox" runat="server" />
								</div>
								<div class="data-column" style="margin-left: 100px;">
									<span><%=Resources.Lekmer.Literal_ShowVariants%></span>
									<br />
									<asp:CheckBox ID="ShowVariantsCheckBox" runat="server" />
								</div>
							</div>

							<br style="clear: both;" />

							<div class="data-row">
								<div class="data-column">
									<span><%=Resources.Lekmer.Literal_ExpectedBackInStock%></span>
									<br />
									<asp:TextBox ID="ExpectedBox" runat="server" Width="106px"></asp:TextBox>
									<asp:ImageButton ID="ExpectedButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
										ImageAlign="AbsMiddle"/>
									<ajaxToolkit:CalendarExtender ID="ExpectedCalendar" runat="server" TargetControlID="ExpectedBox" 
										PopupButtonID="ExpectedButton" />
								</div>
								<div class="data-column" style="margin-left: 25px;">
									<span><%=Resources.Lekmer.Literal_MonitorThreshold%></span>
									<br />
									<asp:TextBox ID="MonitorThresholdBox" runat="server" Width="106px"></asp:TextBox>
									<asp:RangeValidator ID="MonitorThresholdRangeValidator" runat="server" ControlToValidate="MonitorThresholdBox"
										Display="None" ErrorMessage="<%$ Resources:LekmerMessage, MonitorThresholdIncorrect %>" 
										MinimumValue="1" MaximumValue="1000" Type="Integer" ValidationGroup="vgPage">
									</asp:RangeValidator>
								</div>
							</div>

							<br style="clear: both;" />

							<div class="data-row">
								<div class="data-column">
									<span><%=Resources.Lekmer.Literal_CreatedDate%></span>
									<br />
									<asp:TextBox ID="CreatedDateBox" runat="server" Width="106px"></asp:TextBox>
									<asp:ImageButton ID="CreatedDateButton" runat="server" ImageUrl="~/Media/Images/Customer/date.png" 
										ImageAlign="AbsMiddle"/>
									<ajaxToolkit:CalendarExtender ID="CreatedDateCalendar" runat="server" TargetControlID="CreatedDateBox" 
										PopupButtonID="CreatedDateButton" />
								</div>
								<div class="data-column" style="margin-left: 25px;">
									<span><%=Resources.Lekmer.Literal_MaxQuantityPerOrder%></span>
									<br />
									<asp:TextBox ID="MaxQuantityPerOrderBox" runat="server" Width="106px"></asp:TextBox>
									<asp:RangeValidator ID="MaxQuantityPerOrderRangeValidator" runat="server" ControlToValidate="MaxQuantityPerOrderBox"
										Display="None" ErrorMessage="<%$ Resources:LekmerMessage, MaxQuantityPerOrderIncorrect %>" 
										MinimumValue="1" MaximumValue="1000000000" Type="Integer" ValidationGroup="vgPage">
									</asp:RangeValidator>
								</div>
							</div>
							
							<br style="clear: both;" />

							<div class="data-row">
								<div style="float:left;">
									<span><%=Resources.Lekmer.Literal_DeliveryTimeFrom%></span><br />
									<asp:TextBox ID="DeliveryTimeFromBox" runat="server" Width="95px" Enabled="False" />
								</div>
								<%--<div class="data-column" style="width: 28px;" align="center">
									<span>-</span>
								</div>--%>
								<div style="float:left; padding-left: 5px">
									<span><%=Resources.Lekmer.Literal_DeliveryTimeTo%></span><br />
									<asp:TextBox ID="DeliveryTimeToBox" runat="server" Width="95px" Enabled="False" />
								</div>
								<div style="float:left; padding-left: 5px">
									<span><%=Resources.Lekmer.Literal_DeliveryTimeFormat%></span><br />
									<asp:TextBox ID="DeliveryTimeFormatBox" runat="server" Width="95px" Enabled="False" />
									<uc:DeliveryTimeConfigurator ID="DeliveryTimeConfigurator" runat="server" />
								</div>
							</div>

							<br style="clear: both;" />

							<div class="data-row">
								<div class="data-column">
									<span><%=Resources.Lekmer.Literal_UrlTitle%></span>
									<uc:UrlHistory runat="server" ID="ProductUrlHistoryControl"/>
									<br />
									<asp:Repeater runat="server" ID="ProductUrlRepeater">
										<ItemTemplate>
											<div class="data-row">
												<div class="data-column" style="width: 50px;">
													<asp:HiddenField runat="server" ID="IdHidden" Value='<%# Eval("Id") %>'/>
													<asp:Literal runat="server" ID="LanguageTitleLiteral" Text='<%# Eval("Title") %>'></asp:Literal>
												</div>
												<div class="data-column">
													<asp:RegularExpressionValidator runat="server" ID="ProductUrlExpressionValidator"
															ControlToValidate="UrlTitleBox" ValidationExpression="^[\s\wåäöæø]+$" ValidationGroup="vgPage"
														ErrorMessage='<%# string.Format(CultureInfo.CurrentCulture, Resources.LekmerMessage.ProductEdit_UrlTitleIncorrect, Eval("Title")) %>' Display="None"/>
													<asp:TextBox runat="server" ID="UrlTitleBox" Text='<%# GetUrlTitle(Eval("Id")) %>' Width="200px"></asp:TextBox>
												</div>
											</div>
											<br />
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>

							<br style="clear: both;" />

							<div class="data-row" style="width: 330px;">
								<div class="data-column">
									<asp:UpdatePanel ID="RatingGroupsUpdatePanel" runat="server" UpdateMode="Conditional">
										<ContentTemplate>
											<span class="bold"><%= Resources.RatingReview.Literal_RatingGroups %></span>
											<br />
											<sc:GridViewWithCustomPager ID="RatingGroupsGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
												AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>" Width="100%">
												<Columns>
													<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
														<HeaderTemplate>
															<asp:CheckBox ID="SelectAllCheckBox" runat="server" />
														</HeaderTemplate>
														<ItemTemplate>
															<asp:CheckBox ID="SelectCheckBox" runat="server" />
															<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("RatingGroup.Id") %>' runat="server" />
														</ItemTemplate>
													</asp:TemplateField>
													<asp:TemplateField HeaderText="<%$ Resources:General, Literal_Title %>">
														<ItemTemplate>
															<uc:HyperLinkEncoded runat="server" Text='<%# Eval("RatingGroup.Title") %>' NavigateUrl='<%# GetRatingGropEditUrl(Eval("RatingGroup.Id")) %>'/>
														</ItemTemplate>
													</asp:TemplateField>
													<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="3%">
														<ItemTemplate>
															<asp:ImageButton ID="DeleteButton" runat="server" CommandName="DeleteGroup" CommandArgument='<%# Eval("RatingGroup.Id") %>'
																ImageUrl="~/Media/Images/Common/delete.gif" OnClientClick="return ConfirmRatingGroupDelete();" />
														</ItemTemplate>
													</asp:TemplateField>
												</Columns>
											</sc:GridViewWithCustomPager>

											<div id="AllSelectedDiv" runat="server" style="display: none;" class="apply-to-all-selected-2">
												<uc:ImageLinkButton runat="server" ID="RemoveRatingGroupsButton"
													Text="<%$ Resources:General, Button_RemoveSelected %>" SkinID="DefaultButton" OnClientClick="return ConfirmRatingGroupDelete();" />
											</div>

											<br />

											<div class="right">
												<uc:RatingGroupSelector ID="RatingGroupSelector" runat="server" />
											</div>
										</ContentTemplate>
									</asp:UpdatePanel>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="fragment-10">
					<div>
						<div style="float: right;">
							<uc:ImageLinkButton ID="EditTagGroupsDefaultSettingsButton" runat="server" Text="<%$ Resources:LekmerMessage, ProductEdit_EditTagGroupsDefaultSettings %>" SkinID="DefaultButton" />
						</div>
						<asp:Repeater ID="TagRepeater" runat="server">
							<ItemTemplate>
								<uc:TagList ID="TagList" runat="server"></uc:TagList>
							</ItemTemplate>
						</asp:Repeater>
					</div>
				</div>
				<div id="fragment-11">
					<div class="tabs-inner-tab">
						<span class="bold"><%= Resources.Lekmer.Literal_Dimension %></span>
						<br />
						<asp:GridView ID="SizeGrid" SkinID="grid" runat="server" AutoGenerateColumns="false" Width="100%">
							<Columns>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_Eu %>" ItemStyle-HorizontalAlign="Right">
									<ItemTemplate>
										<%# FormatSize(Eval("SizeInfo.Eu"))%>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_EuTitle %>" ItemStyle-HorizontalAlign="Right">
									<ItemTemplate>
										<%# Eval("SizeInfo.EuTitle")%>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:Lekmer,Literal_InStock %>" DataField="NumberInStock" ItemStyle-HorizontalAlign="Right" />
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_MM %>" ItemStyle-HorizontalAlign="Right">
									<ItemTemplate>
										<%# FormatSize(Eval("SizeInfo.Millimeter"))%>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_OverrideMillimeter %>" ItemStyle-HorizontalAlign="Center">
									<ItemTemplate>
										<asp:RangeValidator runat="server" ID="MillimeterValidator" 
											ControlToValidate="MillimeterTextBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' 
											MinimumValue="0" ValidationGroup="vgPage" 
											ErrorMessage="<%$ Resources:LekmerMessage,ProductEdit_MillimeterIncorrect %>" Display="None"/>
										<asp:TextBox ID="MillimeterTextBox" runat="server" Text='<%# Eval("OverrideMillimeter") %>' CssClass="override-eu-box"></asp:TextBox>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_Deviation %>">
									<ItemTemplate>
										<div class="size-deviation-container">
											<div class="size-deviation-left">
												<asp:RangeValidator runat="server" ID="SizeDeviationValidator"
													ControlToValidate="DeviationBox" Type="Integer" MaximumValue='<%# int.MaxValue %>' 
													MinimumValue='<%# int.MinValue %>' ValidationGroup="vgPage" 
													ErrorMessage="<%$ Resources:LekmerMessage,ProductEdit_SizeDeviationIncorrect %>" Display="None"/>
												<asp:TextBox ID="DeviationBox" runat="server" Text='<%# Eval("MillimeterDeviation") %>' CssClass="size-deviation-box"></asp:TextBox>
											</div>
											<div class="size-deviation-right">
												<uc:ContextMenu ID="SizeDeviationContextMenu" runat="server" CallerImageSrc="~/Media/Images/Common/context-menu.png"
													CallerOverImageSrc="~/Media/Images/Common/context-menu-over.png" MenuContainerCssClass="context-menu-container"
													MenuShadowCssClass="context-menu-shadow" MenuCallerCssClass="context-menu-caller">
													<div class="context-menu-header">
														<%= Resources.General.Literal_Manage %>
													</div>
													<div class="menu-row-separator">
													</div>
													<div class="context-menu-row">
														<a id="SizeDeviationApply" class="size-deviation-menu-button" onclick="javascript:$('input.size-deviation-box').val($(this).parents('td:first').find('input.size-deviation-box:first').val()); $('div.context-menu-container, div.context-menu-shadow').hide();"><%= Resources.Lekmer.Literal_ApplyToAllRows%></a>
													</div>
												</uc:ContextMenu>
											</div>
										</div>
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="<%$ Resources:Lekmer,Literal_OverrideEu %>" ItemStyle-HorizontalAlign="Center">
									<ItemTemplate>
										<asp:RangeValidator runat="server" ID="OverrideEuValidator" 
											ControlToValidate="OverrideEuBox" Type="Double" MaximumValue='<%# decimal.MaxValue %>' 
											MinimumValue="0" ValidationGroup="vgPage" 
											ErrorMessage="<%$ Resources:LekmerMessage,ProductEdit_OverrideEuIncorrect %>" Display="None"/>
										<asp:TextBox ID="OverrideEuBox" runat="server" Text='<%# Eval("OverrideEu") %>' CssClass="override-eu-box"></asp:TextBox>
										<asp:HiddenField ID="IdHidden" runat="server" Value='<%# Eval("SizeInfo.Id") %>' />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
						</asp:GridView>
						<br />
						<span class="bold"><%= Resources.Lekmer.Literal_SizeDeviation %></span>
						<br />
						<asp:RadioButtonList runat="server" ID="SizeDeviationList" DataValueField="Id" DataTextField="Title">
						</asp:RadioButtonList>
					</div>
				</div>
			</div>

			<div id="product-main-description">
				<div>
					<span><%= Resources.General.Literal_Type %>: </span>
					<uc:HyperLinkEncoded ID="ProductTypeLink" runat="server" />
					&nbsp;&nbsp;&nbsp;
					<asp:CheckBox id="SellOnlyInPackage" runat="server" />
					<span><%= Resources.Lekmer.Literal_SellOnlyInPackage %></span>
				</div>
				<div>
					<asp:CheckBox ID="IsDropShipCheckBox" runat="server" />
					<span><%= Resources.Lekmer.Literal_IsDropShip %></span>
				</div>
				<div>
					<span><%= Resources.General.Literal_Status %></span><br />
					<asp:DropDownList ID="StatusList" DataTextField="Title" DataValueField="Id" runat="server" />
				</div>
				<div>
					<span><%= Resources.Product.Literal_ErpIdIdArticleId %></span><br />
					<asp:TextBox ID="ErpTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
				</div>
				<div>
					<span><%= Resources.Product.Literal_LekmerErpID%></span><br />
					<asp:TextBox ID="LekmerErpTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
				</div>
				<div>
					<span><%= Resources.Lekmer.Literal_SupplierArticleNumber %></span><br />
					<asp:TextBox ID="SupplierArticleNoTextBox" runat="server" ReadOnly="true" CssClass="readonly" />
				</div>
				<div>
					<span><%= Resources.Product.Literal_Ean %></span><br />
					<asp:TextBox ID="EanTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
				</div>
				<div>
					<span><%= Resources.Product.Literal_StockStatus%></span><br />
					<asp:TextBox ID="StockStatusTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
				</div>
				<div>
					<span><%= Resources.Product.Literal_NumberInStock%></span><br />
					<asp:TextBox ID="NumberInStockTextBox" ReadOnly="true" runat="server" CssClass="readonly" />
				</div>
				<div>
					<span><%= Resources.Product.Literal_Category%></span><br />
					<asp:TextBox ID="CategoryTextBox" ReadOnly="true" CssClass="readonly" runat="server" />
				</div>
				<div>
					<span><%= Resources.Lekmer.Literal_SupplierNumber %></span><br />
					<asp:TextBox ID="SupplierIdTextBox" runat="server" ReadOnly="true" CssClass="readonly" />
				</div>
				<div id="image-left-show">
					<span><%= Resources.Product.Literal_DefaultImage%></span><br /> 
					<asp:UpdatePanel runat="server" ID="DefaultImageUpdatePanel" UpdateMode="Always">
						<ContentTemplate>
							<a id="ImageLeftAnchor" runat="server" target="blank">
								<asp:Image runat="server" style="margin-top: 5px;" id="DefaultImage" ImageUrl="~/Media/Images/Assortment/defaultProduct.jpg" />
								<img ID="DefaultZoomImage" src="~/Media/Images/Common/zoom.png" runat="server" class="zoom-image" />
							</a>
						</ContentTemplate>
					</asp:UpdatePanel> 
				</div>
			</div>

			<br clear="all" />

			<div id="product-edit-main-action-buttons">
				<uc:ImageLinkButton UseSubmitBehaviour="true" runat="server" ID="SaveButton" Text="<%$ Resources:General,Button_Save %>" SkinID="DefaultButton" ValidationGroup="vgPage" OnClientClick="ClearSuccessSeveMessage();"/>
				<uc:ImageLinkButton UseSubmitBehaviour="true" runat="server" ID="CancelButton" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
			</div>
		</div>
	</asp:Panel>

	<div id="RelationsDiv" runat="server" class="product-popup-relations-container" style="z-index: 10010; display: none;">
		<div id="product-popup-relations-header">
			<div id="product-popup-relations-header-left">
			</div>

			<div id="product-popup-relations-header-center">
				<span><%= Resources.Product.Literal_AddRelations%></span>
				<input type="button" id="_inpCloseRelations" value="x" />
			</div>

			<div id="product-popup-relations-header-right">
			</div>
		</div>
		<div id="product-popup-relations-content">
			<asp:UpdatePanel ID="RelationsSearchFormUpdatePanel" runat="server" UpdateMode="Conditional">
				<ContentTemplate>
					<div id="product-relation-list-search">
						<asp:Panel runat="server" ID="SearchPanel" DefaultButton="RelationSearchButton">
							<div class="rlSearchHeader">
								<span><%= Resources.General.Literal_Search %></span>
							</div>
							<div class="rlSearchForm">
								<div class="column">
									<div class="input-box">
										<span><%= Resources.General.Literal_Title %></span>
										<br />
										<asp:TextBox ID="RelationNameTextBox" runat="server" CssClass="productRelationSearchName" ></asp:TextBox>
									</div>
								</div>
								<div class="column">
									<div class="input-box">
										<span><%= Resources.General.Literal_Type %></span>
										<br />
										<asp:DropDownList ID="RelationTypeList" runat="server" CssClass="productRelationSearchType" DataValueField="Id" DataTextField="CommonName"></asp:DropDownList>
									</div>
								</div>
								<div class="rlSearchButton">
									<uc:ImageLinkButton ID="RelationSearchButton" runat="server" Text="<%$ Resources:General, Button_Search %>" SkinID="DefaultButton" />
								</div>
							</div>
						</asp:Panel>
					</div>
					<br />
					<div id="articles-search-results">
						<sc:GridViewWithCustomPager
							ID="RelationSearchResultGrid"
							SkinID="grid"
							runat="server"
							AutoGenerateColumns="false"
							AllowPaging="true"
							PageSize="<%$AppSettings:DefaultGridPageSize%>"
							DataSourceID="RelationListsObjectDataSource"
							Width="99%"
							GridLines="None">
							<Columns>
								<asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="4%">
									<HeaderTemplate>
										<asp:CheckBox id="AllRelationsCheckBox" runat="server" />
									</HeaderTemplate>
									<ItemTemplate>
										<asp:CheckBox ID="SelectCheckBox" runat="server" />
										<asp:HiddenField ID="RelationIdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="<%$ Resources:General, Literal_Title %>" DataField="Title" ItemStyle-Width="50%" />
								<asp:BoundField HeaderText="<%$ REsources:General, Literal_Type%>" DataField="RelationListTypeName" ItemStyle-Width="46%" />
							</Columns>
						</sc:GridViewWithCustomPager>
					</div>
					<br/>

					<div class="right">
						<uc:ImageLinkButton UseSubmitBehaviour="true" ID="AddRelationsButton" Width ="70px" runat="server" Text="<%$Resources:General,Button_Add %>" Visible = "false" SkinID="DefaultButton"/>
						<uc:ImageLinkButton ID="CancelRelationsButton" Width ="70px" OnClientClick="document.getElementById('_inpCloseRelations').click();" runat="server" Text="<%$ Resources:General, Button_Cancel %>" SkinID="DefaultButton" />
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<asp:ObjectDataSource ID="RelationListsObjectDataSource" runat="server" EnablePaging="true" SelectCountMethod="SelectCount" SelectMethod="SelectMethod" TypeName="Litium.Scensum.BackOffice.Modules.Assortment.RelationLists.RelationListDataSource" />

	</div>
</asp:Content>
