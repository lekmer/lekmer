﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductMedia.ascx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Products.Controls.ProductMedia" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree" TagPrefix="TemplatedTreeView" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="ContentPageSelector" Src="~/UserControls/SiteStructure/ContentPageSelector.ascx" %>
<%@ Register TagPrefix="uc" TagName="ImageSelect" Src="~/UserControls/Media/ImageSelect.ascx"%>
<%@ Register TagPrefix="uc" TagName="ImageGroup" Src="~/Modules/Assortment/Products/Controls/ImageGroups.ascx" %>

<script type="text/javascript">
	function ResetDefaultMediaMessages(){
		$("div.product-popup-images-body div[id*='_divMessages']").css('display', 'none');
	}    
</script>

<asp:UpdatePanel ID="ImagesUpdatePanel" runat="server" UpdateMode="Conditional">
	<ContentTemplate>
	<div>
	<div class="prodduct-image">
		<div class="left">
			<div class="input-box">
			<span class="bold"><%= Resources.Product.Literal_DefaultImage %></span>
				<div class="default-image">										        
                    <div id="image-show">
                        <a id="ZoomProductImageAnchor"  runat="server" target="blank">
                            <asp:Image ID="DefaultImage"  ImageUrl="~/Media/Images/Assortment/defaultProduct.jpg" runat="server" />											    
					        <img ID="ZoomProductImage"  src="~/Media/Images/Common/zoom.png" runat="server" class="zoom-image" />
					    </a> 
                    </div>
				</div>
			</div>
		</div>
		<div class="left">
		<div >
		<div class="left default-image-first-column" >
				
				<span class="bold"><%= Resources.General.Literal_Title %></span>	<br />	
				<uc:LiteralEncoded ID="ImageTitleLiteral"  runat="server" ></uc:LiteralEncoded>
				<br />		<br />							
				<span class="bold"><%= Resources.General.Literal_AlternativeText %></span>	<br />		
				<uc:LiteralEncoded ID="ImageAlternativeTextLiteral"  runat="server" ></uc:LiteralEncoded>
				<br />								
			
		</div>
			<div class="left default-image-second-column">
			
				<span class="bold"><%= Resources.General.Literal_FileExtension %></span>	<br />		
				<asp:Label ID="ImageFileExtensionLabel" runat="server"  ></asp:Label>
				<br />	<br />	
			
				<span class="bold"><%= Resources.General.Literal_Dimensions %></span>	<br />		
				<asp:Label ID="ImageDimensionsLabel" runat="server" ></asp:Label>
				<br />								
			<br />	
		  </div>
		  </div>
		  <div class="left default-image-bottom">
		  <br/>
		    <uc:ImageLinkButton  runat="server" ID="ImgBrowseButton" Text="<%$ Resources:Product,Button_ChangeImage %>" UseSubmitBehaviour="false" SkinID="DefaultButton" OnClientClick="ClearPopup();"/>									
			<uc:ImageLinkButton  runat="server" ID="DeleteImageButton" Text="<%$ Resources:Product,Button_DeleteImage %>"  SkinID="DefaultButton" />
		</div>	
		 </div>
	</div>
	<ajaxToolkit:ModalPopupExtender ID="ImagesPopup" runat="server" TargetControlID="ImgBrowseButton" PopupControlID="ImagesDiv" 
		BackgroundCssClass="popup-background" CancelControlID="_inpCloseImages" OnCancelScript="ResetDefaultMediaMessages();">
    </ajaxToolkit:ModalPopupExtender>
 
</div>
</ContentTemplate>
</asp:UpdatePanel>
<uc:ImageGroup ID="ImageGroupControl" runat="server" />
	<div id="ImagesDiv" runat="server" class="product-popup-images-container" style="z-index: 10010;
		display: none;">
		<div id="product-popup-images-header">
			<div id="product-popup-images-header-left">
					
			</div>
			<div id="product-popup-images-header-center">
				<span><%= Resources.Product.Literal_AddImages %></span>
				<input type="button" id="_inpCloseImages" class="_inpClose" value="x"  />
			</div>
			<div id="product-popup-images-header-right">
				
			</div>
		</div>
		<br clear="all" />
		<div class="product-popup-images-body">
	    <uc:ImageSelect id="ImageSelectControl" runat="server"/>
	    </div>
    </div>    
